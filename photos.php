<?php

    ini_set('display_errors', 'on'); // Display all errors on screen
    header("Cache-Control: public");
    header("Expires: " . date("r", time() + 3600));
    header('Content-Type: text/html; charset=UTF-8');
    ob_start();
    @session_start();
    //    define('DS', DIRECTORY_SEPARATOR);
    define('DS', '/');
    define('HOST', dirname(__FILE__)); // Root path
    define('MULTI_LANGUAGE', false);
    define('APPLICATION', 'frontend'); // Choose application - backend|frontend
    define('PROFILER', false); // On/off profiler
    define('START_TIME', microtime(true)); // For profiler. Don't touch!
    define('START_MEMORY', memory_get_usage()); // For profiler. Don't touch!

    require_once 'loader.php';

    set_time_limit(3600);


    $ftpHost = '109.87.24.5';
    $ftpLogin = "babykroha.ua";
    $ftpPasswd = "FTP172839654Site";

    $ftp = ftp_connect($ftpHost, '21', 3600) or die("Couldn't connect to $ftpHost");

    $login = ftp_login($ftp, $ftpLogin, $ftpPasswd);

    if (!$ftp || !$login) {
        die("FTP Connection Failed");
    }

    ftp_pasv($ftp, true);

    ftp_chdir($ftp, 'foto');

    $contents = ftp_nlist($ftp, '.');

    foreach ($contents as $file) {

        if ($file == '.' || $file == '..') {
            continue;
        }

        $product = \Modules\Catalog\Models\Items::getRow((int)$file);

        if ($product->id) {
            $need = \Core\Config::get('images.catalog');
            $oldImages = \Core\QB\DB::select()->from('catalog_images')->where('catalog_id', '=', $product->id)->find_all()->as_array();
            if (count($oldImages)) {
                continue;
//                $oldIDs = [];
//                foreach ($oldImages as $oldImage) {
//                    $oldIDs[] = $oldImage->id;
//                    foreach ($need as $one) {
//                        $oldFile = HOST . \Core\HTML::media(DS . 'images' . DS . 'catalog' . DS . \Core\Arr::get($one, 'path') . DS . $oldImage->image, false);
//                        @unlink($oldFile);
//                    }
//                }
//                \Core\QB\DB::delete('catalog_images')->where('catalog_id', '=', $product->id)->execute();
            }

            ftp_chdir($ftp, $file);

            $photoList = ftp_nlist($ftp, '.');

            foreach ($photoList as $ind => $item) {

                $filename = md5($product->name . '_catalog' . (time()+$ind)) . '.jpg';

                foreach ($need as $key => $one) {
                    $path = HOST . \Core\HTML::media(DS . 'images' . DS . 'catalog' . DS . \Core\Arr::get($one, 'path'), false);
                    \Core\Files::createFolder($path, 0777);
                    $currentFile = $path . DS . $filename;
                    $newFile = ftp_get($ftp, $currentFile, $item, FTP_BINARY);
                    $img = imagecreatefromjpeg($currentFile);
                    $new_width = \Core\Arr::get($one, 'width');
                    $new_height = \Core\Arr::get($one, 'height');
                    if (\Core\Arr::get($one, 'resize')) {
                        $newImg = imagescale($img, $new_width);
                        $imageSave = imagejpeg($newImg, $currentFile, \Core\Arr::get($one, 'quality', 80));
                        imagedestroy($newImg);
                    } else {
                        $imageSave = imagejpeg($img, $currentFile, \Core\Arr::get($one, 'quality', 80));
                    }

                    imagedestroy($img);
                }


                if ($ind === 0) {
                    \Core\QB\DB::insert('catalog_images', ['sort', 'catalog_id', 'main', 'image'])->values([($key+1), $product->id, '1', $filename])->execute();
                    \Core\QB\DB::update('catalog')->set(['image' => $filename])->where('id', '=', $product->id)->execute();

                } else {
                    \Core\QB\DB::insert('catalog_images', ['sort', 'catalog_id', 'main', 'image'])->values([($key+1), $product->id, '0', $filename])->execute();
                }
            }

            ftp_chdir($ftp, '..');
        }

    }

//    $products = \Core\QB\DB::select()->from('catalog')->where('status', '=', 1)->find_all()->as_array();
//
//
//    //parse Products
//    foreach ($products as $obj) {
//
//        if (in_array($obj->id, $contents)) {
//
//            $need = \Core\Config::get('images.catalog');
//            $oldImages = \Core\QB\DB::select()->from('catalog_images')->where('catalog_id', '=', $obj->id)->find_all()->as_array();
//            if (count($oldImages)) {
//                $oldIDs = [];
//                foreach ($oldImages as $oldImage) {
//                    $oldIDs[] = $oldImage->id;
//                    foreach ($need as $one) {
//                        $file = HOST . \Core\HTML::media(DS . 'images' . DS . 'catalog' . DS . \Core\Arr::get($one, 'path') . DS . $oldImage->image, false);
//                        @unlink($file);
//                    }
//                }
//                \Core\QB\DB::delete('catalog_images')->where('catalog_id', '=', $obj->id)->execute();
//            }
//
//            ftp_chdir($ftp, $obj->id);
//
//            $photoList = ftp_nlist($ftp, '.');
//
//            foreach ($photoList as $ind => $item) {
//
//                $filename = md5($obj->name . '_catalog' . (time()+$ind)) . '.jpg';
//
//                foreach ($need as $key => $one) {
//                    $path = HOST . \Core\HTML::media(DS . 'images' . DS . 'catalog' . DS . \Core\Arr::get($one, 'path'), false);
//                    \Core\Files::createFolder($path, 0777);
//                    $file = $path . DS . $filename;
//                    $newFile = ftp_get($ftp, $file, $item, FTP_BINARY);
//                    $img = imagecreatefromjpeg($file);
//                    $new_width = \Core\Arr::get($one, 'width');
//                    $new_height = \Core\Arr::get($one, 'height');
//                    if (\Core\Arr::get($one, 'resize')) {
//                        $newImg = imagescale($img, $new_width);
//                        $imageSave = imagejpeg($newImg, $file, \Core\Arr::get($one, 'quality', 80));
//                        imagedestroy($newImg);
//                    } else {
//                        $imageSave = imagejpeg($img, $file, \Core\Arr::get($one, 'quality', 80));
//                    }
//
//                    imagedestroy($img);
//                }
//
//
//                if ($ind === 0) {
//                    \Core\QB\DB::insert('catalog_images', ['sort', 'catalog_id', 'main', 'image'])->values([($key+1), $obj->id, '1', $filename])->execute();
//                    \Core\QB\DB::update('catalog')->set(['image' => $filename])->where('id', '=', $obj->id)->execute();
//
//                } else {
//                    \Core\QB\DB::insert('catalog_images', ['sort', 'catalog_id', 'main', 'image'])->values([($key+1), $obj->id, '0', $filename])->execute();
//                }
//            }
//
//            ftp_chdir($ftp, '..');
//        }
//
//    }

    ftp_close($ftp);

    echo "Products is done <br/>";
