webpackJsonp([9],[
/* 0 */,
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * Простой прелоадер
 * @module
 */

// ----------------------------------------
// Imports
// ----------------------------------------

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _createTimer = __webpack_require__(18);

var _createTimer2 = _interopRequireDefault(_createTimer);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

// ----------------------------------------
// Private
// ----------------------------------------

/**
 * Дофелтный конфиг для всех экземпляров прелоадера
 * @type {Object}
 * @prop {string} mainClass - основной CSS класс, который добавляется к контейнеру прелодара
 * @prop {string} subClass - Дополнительный CSS класс - который будет добавлен, если имеет логически позитивное значение
 * @prop {string} showClass - CSS класс, который добавляется к контейнеру прелодара, при его показе
 * @prop {string} hideClass - CSS класс, который добавляется к контейнеру прелодара, перед его закрытием
 * @prop {number} removeDelay - время задержки, после которой будут удалены разметка прелоадре и CSS классы у контейнера
 * @prop {string} markup - Разметка прелодера
 * @private
 */
var defaultConfig = {
	mainClass: 'preloader',
	subClass: '',
	showClass: 'preloader--show',
	hideClass: 'preloader--hide',
	removeDelay: 300,
	markup: '<div class="preloader__block"></div>'
};

// ----------------------------------------
// Public
// ----------------------------------------

/**
 * Создание прелодаров
 * @param {JQuery} $container - контейнер для прелоадера
 * @param {Object} [userConfig={}] - Пользвательский конфиг для создваемого экземпляра
 * @prop {JQuery} [$container] - контейнер для прелоадера
 * @prop {Object} [config] - прелоадера
 * @prop {Object|number} timer - id таймера
 * @prop {boolean} empty
 * @example
 * let preloader = new Preloader($myElement);
 * preloader.show();
 * window.setTimeout(() => preloader.hide(), 1000);
 */

var Preloader = function () {
	function Preloader($container) {
		var userConfig = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

		_classCallCheck(this, Preloader);

		if (!($container && $container.length)) {
			this.empty = true;
			return this;
		}
		if ($container.data('preloader') instanceof Preloader) {
			$container.data('preloader', null);
		}
		this.empty = false;
		this.$container = $container;
		this.config = $.extend(true, {}, Preloader.config, userConfig);
		this.timer = (0, _createTimer2.default)();
		this.$container.data('preloader', this);
	}

	/**
  * Показ прелодера
  * @param {string} [place="append"] - append / prepend / before / after
  * @sourceCode
  */


	_createClass(Preloader, [{
		key: 'show',
		value: function show() {
			var place = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'append';

			if (this.empty) {
				console.warn('Empty preloader cannot be shown!');
				return false;
			}
			var _config = this.config,
			    mainClass = _config.mainClass,
			    subClass = _config.subClass,
			    showClass = _config.showClass,
			    markup = _config.markup;


			this.$container.each(function (i, el) {
				var $container = $(el);
				$container.addClass(mainClass);
				if (subClass) {
					$container.addClass(subClass);
				}
				$container.data('$preloaderMarkup', $(markup));
				$container[place]($container.data('$preloaderMarkup'));
				window.setTimeout(function () {
					return $container.addClass(showClass);
				}, 10);
			});
		}

		/**
   * Скрытие прелодера
   * @param {boolean} [removeMarkup=true] - удалить разметку прелодера после скрытия
   * @sourceCode
   */

	}, {
		key: 'hide',
		value: function hide() {
			var _this = this;

			var removeMarkup = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;

			if (this.empty) {
				console.warn('Empty preloader cannot be hidden!');
				return false;
			}
			var _config2 = this.config,
			    mainClass = _config2.mainClass,
			    subClass = _config2.subClass,
			    showClass = _config2.showClass,
			    hideClass = _config2.hideClass;

			this.$container.addClass(hideClass);

			this.timer(function () {
				if (removeMarkup) {
					_this.$container.each(function (i, el) {
						if ($(el).data('$preloaderMarkup') && $(el).data('$preloaderMarkup').length) {
							$(el).data('$preloaderMarkup').remove();
						}
					});
				}
				_this.$container.removeClass([mainClass, showClass, hideClass]);
				if (subClass) {
					_this.$container.removeClass(subClass);
				}
			}, this.config.removeDelay);
		}

		/**
   * Получение глобального конфига для всех прелодаров
   * @returns {Object}
   * @sourceCode
   */

	}], [{
		key: 'config',
		get: function get() {
			return defaultConfig;
		}

		/**
   * Установка глобального конфига
   * @param {Object} value
   * @sourceCode
   * @example
   * Preloader.config = {mainClass: 'my-preloader'}
   */
		,
		set: function set(value) {
			$.extend(true, defaultConfig, value);
		}
	}]);

	return Preloader;
}();

// ----------------------------------------
// Exports
// ----------------------------------------

exports.default = Preloader;

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * @module
 */

// ----------------------------------------
// Imports
// ----------------------------------------

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _wezomModuleLoader = __webpack_require__(35);

var _wezomModuleLoader2 = _interopRequireDefault(_wezomModuleLoader);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// ----------------------------------------
// Public
// ----------------------------------------

var moduleLoader = new _wezomModuleLoader2.default({
	debug: true,
	importPromise: function importPromise(moduleName) {
		return __webpack_require__(36)("./" + moduleName);
	},
	initSelector: '.js-init',
	initFunctionName: 'loaderInit',
	loadingClass: '_module-loading',
	loadedClass: '_module-loaded',
	list: {
		/* eslint-disable key-spacing */
		'jquery-validation--module-loader': 'form',
		'inputmask--module-loader': '[data-phonemask]',
		'magnific-popup--module-loader': '[data-mfp]',
		'wrap-media--module-loader': '[data-wrap-media]',
		'prismjs--module-loader': '[data-prismjs]',
		'lozad--module-loader': ['[data-lozad]', 'picture'],
		'scroll-window--module-loader': '[data-scroll-window]',
		'draggable-table--module-loader': '[data-draggable-table]',
		'malihu-scrollbar--module-loader': '[data-scrollbar]'
	}
});

// ----------------------------------------
// Exports
// ----------------------------------------

exports.default = moduleLoader;

/***/ }),
/* 3 */,
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * Инит валидации форм
 * @module
 */

// ----------------------------------------
// Imports
// ----------------------------------------

Object.defineProperty(exports, "__esModule", {
	value: true
});

__webpack_require__(37);

__webpack_require__(21);

__webpack_require__(8);

__webpack_require__(38);

var _validateHandlers = __webpack_require__(41);

var _validateHandlers2 = _interopRequireDefault(_validateHandlers);

var _validateGetResponse = __webpack_require__(42);

var _validateGetResponse2 = _interopRequireDefault(_validateGetResponse);

var _Preloader = __webpack_require__(1);

var _Preloader2 = _interopRequireDefault(_Preloader);

var _mfpInit = __webpack_require__(11);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// ----------------------------------------
// Private
// ----------------------------------------

/**
 * @const {Object}
 * @private
 * @sourceCode
 */
var configDefault = {
	ignore: ':hidden, .js-ignore',
	get classes() {
		return {
			error: 'has-error',
			valid: 'is-valid',
			labelError: 'label-error',
			formError: 'form--error',
			formValid: 'form--valid',
			formPending: 'form--pending'
		};
	},
	get errorClass() {
		return this.classes.error;
	},
	get validClass() {
		return this.classes.error;
	},

	/**
  * Валидировать элементы при потере фокуса.
  * Или false или функция
  * @type {Boolean|Function}
  * @prop {HTMLElement} element
  * @prop {Event} event
  * @see {@link https://jqueryvalidation.org/validate/#onfocusout}
  */
	onfocusout: function onfocusout(element) {
		if (element.value.length || element.classList.contains(this.settings.classes.error)) {
			this.element(element);
		}
	},


	/**
  * Валидировать элементы при keyup.
  * Или false или функция
  * @type {Boolean|Function}
  * @prop {HTMLElement} element
  * @prop {Event} event
  * @see {@link https://jqueryvalidation.org/validate/#onkeyup}
  */
	onkeyup: function onkeyup(element) {
		if (element.classList.contains(this.settings.classes.error)) {
			this.element(element);
		}
	},


	/**
  * Подсветка элементов с ошибками
  * @param {HTMLElement} element
  */
	highlight: function highlight(element) {
		element.classList.remove(this.settings.classes.valid);
		element.classList.add(this.settings.classes.error);
	},


	/**
  * Удаление подсветки элементов с ошибками
  * @param {HTMLElement} element
  */
	unhighlight: function unhighlight(element) {
		element.classList.remove(this.settings.classes.error);
		element.classList.add(this.settings.classes.valid);
	},


	/**
  * Обработчик сабмита формы
  * @param {HTMLFormElement} form
  * @returns {boolean}
  */
	submitHandler: function submitHandler(form) {
		var $form = $(form);
		var actionUrl = $form.data('ajax');

		if (!actionUrl) {
			return true;
		}

		var preloader = new _Preloader2.default($form);
		preloader.show();

		var formData = new window.FormData();
		formData.append('xhr-lang', $('html').attr('lang') || 'ru');

		// игнорируемые типы инпутов
		var ignoredInputsType = ['submit', 'reset', 'button', 'image'];

		$form.find('input, textarea, select').each(function (i, element) {
			var value = element.value,
			    type = element.type;

			if (~ignoredInputsType.indexOf(type)) {
				return true;
			}

			var $element = $(element);
			var elementNode = $element.nodeName();
			var elementName = $element.data('name') || null;

			if (elementName === null) {
				return true;
			}

			switch (elementNode) {
				case 'input':
					if (type === 'file' && element.files && element.files.length) {
						for (var _i = 0; _i < element.files.length; _i++) {
							var file = element.files[_i];
							formData.append(elementName, file);
						}
					} else {
						if ((type === 'checkbox' || type === 'radio') && !element.checked) {
							break;
						}
						formData.append(elementName, value);
					}
					break;

				case 'textarea':
					formData.append(elementName, value);
					break;

				case 'select':
					var multiName = /\[\]$/;
					if ((multiName.test(elementName) || element.multiple) && element.selectedOptions && element.selectedOptions.length) {
						for (var _i2 = 0; _i2 < element.selectedOptions.length; _i2++) {
							var option = element.selectedOptions[_i2];
							if (option.disabled) {
								continue;
							}
							formData.append(elementName, option.value);
						}
					} else {
						formData.append(elementName, value);
					}
					break;
			}
		});

		var xhr = new window.XMLHttpRequest();
		xhr.open('POST', actionUrl);
		xhr.onreadystatechange = function () {
			if (xhr.readyState === 4) {
				var status = xhr.status,
				    statusText = xhr.statusText,
				    response = xhr.response;
				// все плохо

				if (status !== 200) {
					console.log(xhr);
					preloader.hide();
					return false;
				}

				if (typeof response === 'string') {
					response = JSON.parse(response);
				}

				if (actionUrl === '//' + location.host + '/ajax/cartSizes') {
					var $sizeCountBlock = $('.js-product-counts');
					if ($sizeCountBlock.length) {
						for (var size in response.active_count) {
							var $sizeButton = $sizeCountBlock.find('.js-product-count-item[data-size=' + size + ']');
							$sizeButton.find('span').first().removeClass('product__size-text').addClass('size__item');
							$sizeButton.find('.js-product-count').removeClass('product__size-count').addClass('size__count').text(response.active_count[size]);
						}
					}
				}

				(0, _validateGetResponse2.default)($form, response, statusText, xhr);
			}
		};

		xhr.send(formData);
		return false;
	}
};

// ----------------------------------------
// Public
// ----------------------------------------

/**
 * @param {JQuery} $elements
 */
function validate($elements) {
	$elements.each(function (i, el) {
		var $form = $(el);
		if ($form.hasInitedKey('validate-inited')) {
			return true;
		}

		var config = $.extend(true, {}, configDefault, {
			// current element custom config
		});

		$form.validate(config);
		(0, _validateHandlers2.default)($form, $form.data('validator'));
	});
}

// ----------------------------------------
// Exports
// ----------------------------------------

exports.default = validate;

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


// ----------------------------------------
// Imports
// ----------------------------------------

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.productImgSlider = exports.productPhotoSlider = exports.productSlider = exports.mainSlider = undefined;

__webpack_require__(48);

// ----------------------------------------
// Private
// ----------------------------------------

var $mainSlider = $('.main-slider');
var $productSlider = $('.product-slider');
// let $similarSlider = $('.similar-slider');
var $container = $('.container');
var $dotsControls = $('.main-slider__controls');
var $dotsHolder = $('.main-slider__controls-holder');
var $productPhotoSlider = $('.product__gallery-main');
var $productNavSlider = $('.gallery-preview__wrap');

function dotsPosition(that) {
	var padding = ($(window).width() - $container.outerWidth()) / 2;
	var dotsPosTop = $dotsHolder.position().top;

	that.find('.slick-list').css('padding-left', padding);
	$dotsControls.css('top', dotsPosTop).css('left', padding);
}

function setPadding(that) {
	if (!that.hasClass('small')) {
		var padding = $(window).width() > 1023 ? parseInt(($(window).width() - $container.outerWidth()) / 2) : 0;
		that.find('.slick-list').css('padding-left', padding).css('padding-right', padding);
	}
}

// ----------------------------------------
// Public
// ----------------------------------------

function mainSlider() {
	if (!$mainSlider.length) {
		return false;
	}

	$mainSlider.on('init', function (event, slick, direction) {
		dotsPosition($(this));
	});

	$mainSlider.slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		infinite: true,
		dots: true,
		arrows: false,
		autoplay: false,
		fade: true,
		appendDots: $('.main-slider__controls'),
		customPaging: function customPaging(slider, i) {
			var num = i + 1 <= 9 ? '0'.concat(i + 1) : i + 1;
			return '<a class="main-slider__dot">' + num + '</a>';
		},
		prevArrow: '<button type=\'button\' class=\'slick-arrow slick-prev\'></button>',
		nextArrow: '<button type=\'button\' class=\'slick-arrow slick-next\'></button>',
		responsive: [{
			breakpoint: 768,
			settings: {
				autoplay: true,
				pauseOnHover: false
			}
		}]
	});

	$(window).resize(function () {
		dotsPosition($mainSlider);
	});
}

function productImgSlider() {
	var $productImgSlider = $('.product-img-slider');

	if (!$productImgSlider.length) {
		return false;
	}
	$productImgSlider.not('.slick-initialized').each(function (i, e) {
		var $this = $(e);

		$this.slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			infinite: false,
			dots: false,
			arrows: true,
			autoplay: false,
			fade: true,
			appendArrows: $this.parent().find('.product-item__buttons-box'),
			prevArrow: '<button type=\'button\' class=\'slick-arrow slick-prev\'></button>',
			nextArrow: '<button type=\'button\' class=\'slick-arrow slick-next\'></button>'

			// responsive: [{
			// 	breakpoint: 768,
			// 	settings: {
			// 		autoplay: true,
			// 		pauseOnHover: false
			// 	}
			// }]
		});
	});
}

function productSlider() {
	if (!$productSlider.length) {
		return false;
	}

	$productSlider.each(function (i) {
		var $this = $(this);

		$this.on('init', function (event, slick, direction) {
			setPadding($this);
		});

		$this.slick({
			slidesToShow: 4,
			slidesToScroll: 1,
			infinite: false,
			arrows: true,
			dots: true,
			appendDots: $this.parent().find('.product-slider__counter'),
			customPaging: function customPaging(slider, i) {
				var num = i + 1;
				return '<span class="first-num">' + num + '</span><span class="second-num"> / ' + slider.slideCount + '</span>';
			},
			appendArrows: $this.parent().find('.product-slider__buttons-box'),
			prevArrow: '<button type=\'button\' class=\'slick-arrow slick-prev\'></button>',
			nextArrow: '<button type=\'button\' class=\'slick-arrow slick-next\'></button>',
			responsive: [{
				breakpoint: 1025,
				settings: {
					slidesToShow: 3
				}
			}, {
				breakpoint: 768,
				settings: {
					slidesToShow: 2
				}
			}, {
				breakpoint: 640,
				settings: {
					slidesToShow: 2
				}
			}, {
				breakpoint: 480,
				settings: {
					slidesToShow: 1
				}
			}]
		});

		$(window).resize(function () {
			setPadding($this);
		});
	});
}

// function similarSlider () {
// 	if (!$similarSlider.length) {
// 		return false;
// 	}
//
// 	$similarSlider.slick({
// 		slidesToShow: 4,
// 		slidesToScroll: 1,
// 		infinite: true,
// 		dots: true,
// 		appendDots: $similarControls,
// 		prevArrow: `<button type='button' class='slick-arrow slick-prev'></button>`,
// 		nextArrow: `<button type='button' class='slick-arrow slick-next'></button>`,
// 		responsive: [
// 			{
// 				breakpoint: 1025,
// 				settings: {
// 					slidesToShow: 2
// 				}
// 			}, {
// 				breakpoint: 641,
// 				settings: {
// 					slidesToShow: 1
// 				}
// 			}
// 		]
// 	});
// }

function productPhotoSlider() {
	if (!$productPhotoSlider.length) {
		return false;
	}

	var photoSlider = $('.product__gallery-main').slick({
		slidesToShow: 1,
		infinite: false,
		fade: true,
		dots: false,
		arrows: false,
		accessibility: false,
		asNavFor: '.gallery-preview__wrap'
	});

	$productNavSlider.slick({
		slidesToShow: 4,
		slidesToScroll: 1,
		asNavFor: '.product__gallery-main',
		focusOnSelect: true,
		vertical: true,
		infinite: false,
		prevArrow: '<button type="button" class="slick-arrow slick-prev"></button>',
		nextArrow: '<button type="button" class="slick-arrow slick-next"></button>',

		responsive: [{
			breakpoint: 768,
			settings: {
				vertical: false,
				slidesToShow: 6,
				infinite: true
			}
		}, {
			breakpoint: 480,
			settings: {
				vertical: false,
				slidesToShow: 3,
				infinite: true
			}
		}]
	});

	photoSlider.on('beforeChange', function (event, slick, currentSlide, nextSlide) {});
}

// ----------------------------------------
// Exports
// ----------------------------------------

exports.mainSlider = mainSlider;
exports.productSlider = productSlider;
exports.productPhotoSlider = productPhotoSlider;
exports.productImgSlider = productImgSlider;

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * Модуль для работы с куки
 * @module
 */

// ----------------------------------------
// Private
// ----------------------------------------

Object.defineProperty(exports, "__esModule", {
	value: true
});
var _window = window,
    encoder = _window.encodeURIComponent,
    decoder = _window.decodeURIComponent;

// ----------------------------------------
// Public
// ----------------------------------------

/**
 * Работа с `document.cookie`
 * @namespace cookieData
 */

var cookieData = {
	/**
  * Добавление записи
  * @param {string} key
  * @param {*} value
  * @param {Object} [options={}]
  * @param {Date|number|string} [options.expires]
  * @param {string} [options.path]
  * @param {string} [options.domain]
  * @param {boolean} [options.secure]
  * @see {@link https://learn.javascript.ru/cookie#функция-setcookie-name-value-options}
  * @sourceCode
  */
	add: function add(key, value) {
		var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};

		// if (!this.allowUsage && (key !== this.allowUsageKey)) {
		// 	console.warn('Cookie usage is disallowed!');
		// 	return false;
		// }
		var expires = options.expires;

		if (typeof expires === 'number' && expires) {
			var d = new Date();
			d.setTime(d.getTime() + expires * 1000);
			expires = options.expires = d;
		}

		if (expires && expires.toUTCString) {
			options.expires = expires.toUTCString();
		}

		value = encoder(value);
		var updatedCookie = key + '=' + value;

		for (var propName in options) {
			updatedCookie += '; ' + propName;
			var propValue = options[propName];
			if (propValue !== true) {
				updatedCookie += '=' + propValue;
			}
		}

		document.cookie = updatedCookie;
	},


	/**
  * Получение куки по ключу
  * @param key
  * @return {string|undefined}
  * @see {@link https://learn.javascript.ru/cookie#функция-getcookie-name}
  * @sourceCode
  */
	receive: function receive(key) {
		var matches = document.cookie.match(new RegExp('(?:^|; )' + key.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + '=([^;]*)' // eslint-disable-line no-useless-escape
		));
		return matches ? decoder(matches[1]) : undefined;
	},


	/**
  * Удаление куки по ключу
  * @param {string} key
  * @see {@link https://learn.javascript.ru/cookie#функция-deletecookie-name}
  * @sourceCode
  */
	delete: function _delete(key) {
		this.add(key, '', { expires: -1 });
	},


	/**
  * Задать вопрос о пользовательском согласии использавния куки
  * @return {boolean}
  * @sourceCode
  */
	askUsage: function askUsage() {
		var _this = this;

		this.$infoBlock = this.$infoBlock || $('[data-cookie-info]');
		this.$infoButton = this.$infoButton || $('[data-cookie-submit]');

		if (this.$infoBlock.length && this.$infoButton.length) {
			if (this.allowUsage) {
				this.$infoBlock.remove();
				return false;
			}

			var hiddenClass = 'is-hidden';
			this.$infoButton.on('click', function () {
				_this.$infoBlock.addClass(hiddenClass);
				_this.add(_this.allowUsageKey, _this.allowUsageValue);
				window.setTimeout(function () {
					_this.$infoBlock.remove();
				}, _this.allowUsageDelay);
			});

			window.setTimeout(function () {
				_this.$infoBlock.removeClass(hiddenClass);
			}, this.allowUsageDelay);
			return true;
		}
	},


	/**
  * Проверка пользовательского согласия
  * на использования файлов cookie
  * @return {boolean}
  * @protected
  * @sourceCode
  */
	get allowUsage() {
		return this.receive(this.allowUsageKey) === this.allowUsageValue;
	},

	/**
  * Ключ проверки пользовательского согласия
  * на использования файлов cookie
  * @type {string}
  * @default 'allow-cookie-usage'
  * @protected
  * @sourceCode
  */
	get allowUsageKey() {
		return 'allow-cookie-usage';
	},

	/**
  * Значение ключа проверки пользовательского согласия
  * на использования файлов cookie
  * @type {string}
  * @default 'true'
  * @protected
  * @sourceCode
  */
	get allowUsageValue() {
		return 'true';
	},

	/**
  * Задержка (ms) для проверки пользовательского согласия
  * на использования файлов cookie
  * @type {number}
  * @default 2000
  * @protected
  * @sourceCode
  */
	get allowUsageDelay() {
		return 2000;
	}
};

// ----------------------------------------
// Exports
// ----------------------------------------

exports.default = cookieData;

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


// ----------------------------------------
// Imports
// ----------------------------------------

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.initSelect = undefined;

__webpack_require__(14);

var _validateInit = __webpack_require__(4);

var _validateInit2 = _interopRequireDefault(_validateInit);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// ----------------------------------------
// Private
// ----------------------------------------

var selectSelector = '.select2';
var wrap = void 0;
var placeholder = void 0;
var $closeBtn = $('.filter-close-btn');
var isLang = false;

function select2Init(lang) {
    var $container = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : $('body');

    $container.find(selectSelector).each(function (index) {
        var $this = $(this);
        wrap = $this.closest('.select2-wrap');
        placeholder = $this.data('placeholder');
        var $form = $this.closest('.js-form');

        if ($this.hasClass('ajaxSelect2')) {
            $this.select2({
                minimumResultsForSearch: 1,
                dropdownParent: wrap,
                placeholder: placeholder,
                width: 'resolve',
                language: lang,
                ajax: {
                    url: $this.data('url'),
                    type: 'POST',
                    data: function data(params) {
                        var query = {
                            search: params.term,
                            cityRef: $('.js-courier-city').val()
                        };

                        return query;
                    },
                    processResults: function processResults(data) {
                        return {
                            results: data.result
                        };
                    }
                }
            });
        } else if ($this.hasClass('ajaxSelect3')) {
            $this.select2({
                minimumResultsForSearch: 1,
                dropdownParent: wrap,
                placeholder: placeholder,
                width: 'resolve',
                language: lang,
                ajax: {
                    url: $this.data('url'),
                    type: 'POST',
                    data: function data(params) {
                        var query = {
                            search: params.term,
                            cityRef: $('.js-courier-city-logged').val()
                        };

                        return query;
                    },
                    processResults: function processResults(data) {
                        return {
                            results: data.result
                        };
                    }
                }
            });
        } else {
            $this.select2({
                minimumResultsForSearch: 1,
                dropdownParent: wrap,
                placeholder: placeholder,
                width: 'resolve',
                language: lang
            });
            $('.js-delivery-np-home').val('');
            $('.js-delivery-np-street').val('');
        }

        $this.on('select2:select', function () {
            var $container = $this.next('.select2-container');
            var $wrap = $container.closest('.select2-wrap');
            $container.addClass('selected');
            $wrap.addClass('selected');

            var $form = $(this.form);
            var Validator = $form.data('validator');
            if (Validator) {
                Validator.element($this);
            }
        });

        $this.on('select2:unselect', function () {
            var $container = $this.next('.select2-container');
            var $wrap = $container.closest('.select2-wrap');
            $container.removeClass('selected');
            $wrap.removeClass('selected');

            var $form = $(this.form);
            var Validator = $form.data('validator');
            if (Validator) {
                Validator.element($this);
            }
        });
    });
}

// ----------------------------------------
// Public
// ----------------------------------------

function initSelect() {
    var $container = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : $('body');

    var lang = $('html').attr('lang');
    if (lang && !isLang) {
        isLang = true;
        $.getScript('/Media/assets/js/static/select2/' + lang + '.js', function () {
            select2Init(lang, $container);
        });
    } else {
        select2Init(lang, $container);
    }

    $closeBtn.on('click', function () {
        var $parent = $(this).closest('.select2-wrap');
        var $selected = $parent.find(selectSelector);
        $selected.val('').trigger('change');
        $selected.trigger('select2:unselect');
    });
}

// ----------------------------------------
// Exports
// ----------------------------------------

exports.initSelect = initSelect;

/***/ }),
/* 8 */,
/* 9 */,
/* 10 */,
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 *
 * @module
 */

// ----------------------------------------
// Imports
// ----------------------------------------

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.mfpGallery = exports.mfpInline = exports.mfpIframe = exports.mfpAjax = undefined;

__webpack_require__(8);

__webpack_require__(43);

__webpack_require__(44);

__webpack_require__(45);

var _moduleLoader = __webpack_require__(2);

var _moduleLoader2 = _interopRequireDefault(_moduleLoader);

var _sliders = __webpack_require__(5);

var _zoomit = __webpack_require__(12);

var _zoomit2 = _interopRequireDefault(_zoomit);

var _airDatepicker = __webpack_require__(13);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// ----------------------------------------
// Public
// ----------------------------------------

/**
 * @param {JQuery} $elements
 * @sourceCode
 */
function mfpAjax($elements) {
	$elements.each(function (i, el) {
		var $element = $(el);
		if ($element.hasInitedKey('mfpAjax-inited')) {
			return true;
		}

		var config = $.extend(true, {
			type: 'ajax',
			mainClass: 'mfp-animate-zoom-in',
			closeMarkup: '<button class=\'mfp-close\' id=\'custom-close-btn\'></button>',
			removalDelay: 300,
			autoFocusLast: false,
			callbacks: {
				elementParse: function elementParse(item) {
					var _item$el$data = item.el.data(),
					    url = _item$el$data.url,
					    _item$el$data$type = _item$el$data.type,
					    type = _item$el$data$type === undefined ? 'POST' : _item$el$data$type,
					    _item$el$data$param = _item$el$data.param,
					    data = _item$el$data$param === undefined ? {} : _item$el$data$param;

					this.st.ajax.settings = { url: url, type: type, data: data };
				},

				ajaxContentAdded: function ajaxContentAdded() {
					var $container = this.contentContainer || [];
					if ($container) {
						_moduleLoader2.default.init($container);

						if ($('.login-popup').length > 0) {
							var checkReq = function checkReq(input) {
								rozn.prop('required', false);

								if (!rozn.prop('checked') && !opt.prop('checked') && !drop.prop('checked')) {
									rozn.prop('required', true);
								}
							};

							(0, _airDatepicker.airDatepicker)();

							var rozn = $('input[name="rozn"]'),
							    opt = $('input[name="opt"]'),
							    drop = $('input[name="drop"]'),
							    roznBirth = $('.rozn-birthday');

							rozn.on('click', function () {
								checkReq();
								opt.prop('checked', false);
								drop.prop('checked', false);

								if (rozn.prop('checked')) {
									roznBirth.show();
								} else {
									roznBirth.hide().find('input').val('');
								}
							});

							opt.on('click', function () {
								checkReq();
								rozn.prop('checked', false);
								roznBirth.hide().find('input').val('');
							});

							drop.on('click', function () {
								checkReq();
								rozn.prop('checked', false);
								roznBirth.hide().find('input').val('');
							});

							$('.login-popup__submit').on('click', function () {
								if ($("#birthday").val().length || $("#babyName").val().length) {
									$("#birthday, #babyName").prop('required', true);
								} else {
									$("#birthday, #babyName").removeAttr('required').removeClass('has-error');
								}
							});
						}

						if ($('.sizes-gallery-popup').length > 0) {
							var _productPhotoSlider = function _productPhotoSlider() {

								var photoSlider = $('.sizes-gallery-popup .product__gallery-main').slick({
									slidesToShow: 1,
									infinite: false,
									fade: true,
									dots: false,
									arrows: false,
									accessibility: false,
									asNavFor: '.sizes-gallery-popup .gallery-preview__wrap'
								});

								$productNavSlider.slick({
									slidesToShow: 4,
									slidesToScroll: 1,
									asNavFor: '.sizes-gallery-popup .product__gallery-main',
									focusOnSelect: true,
									vertical: true,
									infinite: false,
									prevArrow: '<button type="button" class="slick-arrow slick-prev"></button>',
									nextArrow: '<button type="button" class="slick-arrow slick-next"></button>',

									responsive: [{
										breakpoint: 768,
										settings: {
											vertical: false,
											slidesToShow: 6,
											infinite: true
										}
									}, {
										breakpoint: 480,
										settings: {
											vertical: false,
											slidesToShow: 3,
											infinite: true
										}
									}]
								});

								photoSlider.on('beforeChange', function (event, slick, currentSlide, nextSlide) {});

								(0, _zoomit2.default)();
							};

							var $productNavSlider = $('.sizes-gallery-popup').find('.gallery-preview__wrap');

							_productPhotoSlider();

							$(".sizes-gallery-popup ").one('click', function () {
								$('.product__gallery-item img').each(function () {
									var aspectRatio = $(this).width() / $(this).height();
									$(this).data("aspect-ratio", aspectRatio);

									if (aspectRatio > 1) {
										$(this).closest('.product__gallery-item').addClass('landscape');
									} else if (aspectRatio < 1) {
										$(this).closest('.product__gallery-item').addClass('portrait');
									}
								});
							});
						}

						if ($('.sizes-big-popup').length > 0) {
							$('.sizes-big-popup .mfp-close').on('click', function () {
								setTimeout(function () {
									$('.product__gallery-item').trigger('click');
								}, 500);
							});
						}

						if ($('.review-popup').length > 0) {
							(0, _airDatepicker.airDatepicker)();
						}
					}

					$('body').on('click', '.popup-information-block .cart-registration__callback', function () {
						$('.header-phones__callback').trigger('click');
					});
				}
			}
		}, {
			// current element custom config
		});

		$element.magnificPopup(config);
	});
}

/**
 * @param {JQuery} $elements
 * @sourceCode
 */
function mfpIframe($elements) {
	$elements.each(function (i, el) {
		var $element = $(el);
		if ($element.hasInitedKey('mfpIframe-inited')) {
			return true;
		}

		var config = $.extend(true, {
			type: 'iframe',
			removalDelay: 300,
			mainClass: 'mfp-animate-zoom-in',
			closeBtnInside: true
		}, {
			// current element custom config
		});

		$element.magnificPopup(config);
	});
}

/**
 * @param {JQuery} $elements
 * @sourceCode
 */
function mfpInline($elements) {
	$elements.each(function (i, el) {
		var $element = $(el);
		if ($element.hasInitedKey('mfpInline-inited')) {
			return true;
		}

		var config = $.extend(true, {
			type: 'inline',
			removalDelay: 300,
			mainClass: 'mfp-animate-zoom-in',
			closeBtnInside: true
		}, {
			// current element custom config
		});

		$element.magnificPopup(config);
	});
}

/**
 * @param {JQuery} $elements
 * @sourceCode
 */
function mfpGallery($elements) {
	$elements.each(function (i, el) {
		var $element = $(el);
		if ($element.hasInitedKey('mfpGallery-inited')) {
			return true;
		}

		var config = $.extend(true, {
			type: 'image',
			removalDelay: 300,
			mainClass: 'mfp-animate-zoom-in',
			delegate: '[data-mfp-src]',
			closeMarkup: '<button class=\'mfp-close\' id=\'custom-close-btn\'></button>',
			closeBtnInside: true,
			gallery: {
				enabled: true,
				preload: [0, 2],
				navigateByImgClick: true,
				arrowMarkup: '<button title="%title%" type="button" class="mfp-arrow mfp-arrow-%dir%"></button>'
			}
		}, {
			// current element custom config
		});

		$element.magnificPopup(config);
	});
}

// ----------------------------------------
// Exports
// ----------------------------------------

exports.mfpAjax = mfpAjax;
exports.mfpIframe = mfpIframe;
exports.mfpInline = mfpInline;
exports.mfpGallery = mfpGallery;

/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


// ----------------------------------------
// Imports
// ----------------------------------------

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = zoomit;

__webpack_require__(49);

// ----------------------------------------
// Private
// ----------------------------------------

/**
 * @type {Object}
 * @private
 */

function zoomit() {
  $('.zoom-img').each(function () {
    $(this).zoomIt();
  });
};

/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


// ----------------------------------------
// Imports
// ----------------------------------------

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.airDatepicker = undefined;

__webpack_require__(50);

// ----------------------------------------
// Public
// ----------------------------------------

function airDatepicker() {
	$(".js-birthday").datepicker({
		maxDate: new Date()
	});

	$('.js-birthday').on('input', function () {
		$(this).val($(this).val().replace(/[A-Za-zА-Яа-яЁё,0-9]/, ''));
	});
}

// ----------------------------------------
// Exports
// ----------------------------------------

exports.airDatepicker = airDatepicker;

/***/ }),
/* 14 */,
/* 15 */,
/* 16 */,
/* 17 */,
/* 18 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * Создание таймеров с внутреним замыканием
 * @module
 * @example
import createTimer from './create-timer';

let timer = createTimer(500);
$(window).on('resize', function () {
	timer(() => {
		console.log('done');
	});
});

// only clear timeout
timer.clear();
 */

// ----------------------------------------
// Public
// ----------------------------------------

/**
 * @param {number} [defaultDelay=300]
 * @return {Function} запуск таймеров
 */

Object.defineProperty(exports, "__esModule", {
	value: true
});
function createTimer() {
	var defaultDelay = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 300;

	var timerId = null;
	var _window = window,
	    clearTimeout = _window.clearTimeout,
	    setTimeout = _window.setTimeout;

	/**
  * @param {Function} fn
  * @param {number} [delay=defaultDelay]
  * @prop {Function} clear
  */

	function timer(fn) {
		var delay = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : defaultDelay;

		clearTimeout(timerId);
		timerId = setTimeout(fn, delay);
	}

	timer.clear = function () {
		return clearTimeout(timerId);
	};
	return timer;
}

// ----------------------------------------
// Exports
// ----------------------------------------

exports.default = createTimer;

/***/ }),
/* 19 */,
/* 20 */,
/* 21 */,
/* 22 */,
/* 23 */,
/* 24 */,
/* 25 */,
/* 26 */,
/* 27 */,
/* 28 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * @fileOverview Основной файл инициализации модулей
 */

// ----------------------------------------
// Imports
// ----------------------------------------

__webpack_require__(29);

__webpack_require__(34);

__webpack_require__(16);

var _moduleLoader = __webpack_require__(2);

var _moduleLoader2 = _interopRequireDefault(_moduleLoader);

var _wstabs = __webpack_require__(51);

var _wstabs2 = _interopRequireDefault(_wstabs);

var _cookieData = __webpack_require__(6);

var _cookieData2 = _interopRequireDefault(_cookieData);

var _validateInit = __webpack_require__(4);

var _validateInit2 = _interopRequireDefault(_validateInit);

var _menu = __webpack_require__(53);

var _sliders = __webpack_require__(5);

var _fixedHeader = __webpack_require__(55);

var _rangeSlider = __webpack_require__(56);

var _select = __webpack_require__(7);

var _subMenu = __webpack_require__(59);

var _common = __webpack_require__(60);

var _map = __webpack_require__(61);

var _cart = __webpack_require__(62);

var _selectWord = __webpack_require__(63);

var _headerSearch = __webpack_require__(64);

var _product = __webpack_require__(65);

var _pagination = __webpack_require__(67);

var _catalogSelect = __webpack_require__(68);

var _chooseAjax = __webpack_require__(69);

var _chooseAjax2 = _interopRequireDefault(_chooseAjax);

var _ajaxSelect = __webpack_require__(70);

var _ajaxSelect2 = _interopRequireDefault(_ajaxSelect);

var _showNoty = __webpack_require__(71);

var _showNoty2 = _interopRequireDefault(_showNoty);

var _currency = __webpack_require__(76);

var _currency2 = _interopRequireDefault(_currency);

var _userType = __webpack_require__(77);

var _userType2 = _interopRequireDefault(_userType);

var _payInfoToggle = __webpack_require__(78);

var _payInfoToggle2 = _interopRequireDefault(_payInfoToggle);

var _zoomit = __webpack_require__(12);

var _zoomit2 = _interopRequireDefault(_zoomit);

var _backToCatalog = __webpack_require__(79);

var _backToCatalog2 = _interopRequireDefault(_backToCatalog);

var _airDatepicker = __webpack_require__(13);

var _dellChild = __webpack_require__(80);

var _changeSizeInTable = __webpack_require__(81);

var _clickMe = __webpack_require__(82);

var _deliveryRegistration = __webpack_require__(83);

var _exportTable = __webpack_require__(84);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// ----------------------------------------
// Public
// ----------------------------------------

window.jQuery(document).ready(function ($) {
	_moduleLoader2.default.init($(document));
	_cookieData2.default.askUsage(); // если нужно использовать куки
	_wstabs2.default.init();
	// wsTabs.setActive(); // если нужно принудительно активировать табы
	(0, _validateInit2.default)($('.js-form'));
	(0, _menu.mainMenu)();
	(0, _menu.mobileMenu)();
	(0, _airDatepicker.airDatepicker)();
	(0, _sliders.mainSlider)();
	(0, _sliders.productImgSlider)();
	(0, _sliders.productSlider)();
	(0, _rangeSlider.initRangeSlider)();
	(0, _select.initSelect)();
	(0, _sliders.productPhotoSlider)();
	(0, _subMenu.subMenuToggle)('.filters__item-top', '.filters__item', '.filters__child-wrap');
	(0, _fixedHeader.fixedHeader)();
	(0, _exportTable.exportTable)();
	(0, _fixedHeader.subMenuShow)();
	(0, _subMenu.subMenuToggle)('.faq__item-top', '.faq__item', '.faq__item-bottom');
	(0, _subMenu.subMenuToggle)('.history__item-top', '.history__item', '.history__item-bottom');
	(0, _common.addToFavorite)();
	(0, _common.toggleCartInfo)();
	(0, _map.initMap)();
	(0, _menu.filtersMenu)();
	(0, _cart.cartInit)();
	(0, _selectWord.selectWord)();
	(0, _common.loadMore)();
	(0, _headerSearch.headerSearchToggle)();
	(0, _headerSearch.searchAutocomplete)();
	(0, _product.productMedia)();
	(0, _common.productCounter)();
	(0, _common.scrollTop)();
	(0, _common.headerPhonesMob)();
	(0, _common.toggleDeliveryInfo)();
	(0, _cart.cartDefaultClose)();
	(0, _common.likeAdd)();
	(0, _common.dislikeAdd)();
	(0, _pagination.paginationInit)();
	(0, _catalogSelect.catalogSelect)();
	(0, _catalogSelect.catalogMenu)();
	(0, _catalogSelect.catalogMenuItem)();
	(0, _catalogSelect.catalogClose)();
	(0, _chooseAjax2.default)();
	(0, _ajaxSelect2.default)();
	(0, _showNoty2.default)();
	(0, _currency2.default)();
	(0, _userType2.default)();
	(0, _payInfoToggle2.default)();
	(0, _zoomit2.default)();
	(0, _backToCatalog2.default)();
	(0, _dellChild.dellChild)();
	(0, _changeSizeInTable.changeSizeInTable)();
	(0, _clickMe.clickMe)();
	(0, _deliveryRegistration.deliveryRegistration)();
});

if (false) {
	import('#/_modules/wezom-log');
}

/***/ }),
/* 29 */,
/* 30 */,
/* 31 */,
/* 32 */,
/* 33 */,
/* 34 */,
/* 35 */,
/* 36 */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./draggable-table--module-loader": [
		17,
		8
	],
	"./draggable-table--module-loader.js": [
		17,
		8
	],
	"./inputmask--module-loader": [
		19,
		3
	],
	"./inputmask--module-loader.js": [
		19,
		3
	],
	"./jquery-validation--module-loader": [
		20,
		7
	],
	"./jquery-validation--module-loader.js": [
		20,
		7
	],
	"./lozad--module-loader": [
		22,
		2
	],
	"./lozad--module-loader.js": [
		22,
		2
	],
	"./magnific-popup--module-loader": [
		23,
		6
	],
	"./magnific-popup--module-loader.js": [
		23,
		6
	],
	"./malihu-scrollbar--module-loader": [
		24,
		1
	],
	"./malihu-scrollbar--module-loader.js": [
		24,
		1
	],
	"./prismjs--module-loader": [
		25,
		5
	],
	"./prismjs--module-loader.js": [
		25,
		5
	],
	"./scroll-window--module-loader": [
		26,
		0
	],
	"./scroll-window--module-loader.js": [
		26,
		0
	],
	"./wrap-media--module-loader": [
		27,
		4
	],
	"./wrap-media--module-loader.js": [
		27,
		4
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 36;
module.exports = webpackAsyncContext;

/***/ }),
/* 37 */,
/* 38 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * Расширение библиотеки валидации
 * @module
 */

// ----------------------------------------
// Imports
// ----------------------------------------

var _validateClassRules = __webpack_require__(39);

var _validateClassRules2 = _interopRequireDefault(_validateClassRules);

var _validateWezomMethods = __webpack_require__(40);

var _validateWezomMethods2 = _interopRequireDefault(_validateWezomMethods);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// ----------------------------------------
// Private
// ----------------------------------------

/**
 * Получение имени типа
 * @param {string} type
 * @param {boolean} [multipleFileOrSelect]
 * @return {string}
 * @private
 */
function formGetTypeName(type, multipleFileOrSelect) {
	var typeName = void 0;
	switch (type) {
		case 'select-one':
		case 'select-multiple':
			typeName = '_select';
			break;
		case 'radio':
		case 'checkbox':
			typeName = '_checker';
			break;
		case 'file':
			typeName = multipleFileOrSelect ? '_files' : '_file';
			break;
		default:
			typeName = '';
	}
	return typeName;
}

/**
 * Получение сообщения в зависимости от элемента
 * @param {HTMLElement} element
 * @param {string} method
 * @return {string}
 * @private
 */
function formGetMethodMsgName(element, method) {
	var value = element.value;
	var methodName = void 0;
	switch (method) {
		case 'required':
		case 'maxlength':
		case 'minlength':
		case 'rangelength':
			methodName = method + formGetTypeName(element.type, element.multiple);
			break;
		case 'password':
			if (/^(\s|\t)*/.test(value)) {
				methodName = method + '_space';
			} else {
				methodName = method;
			}
			break;
		case 'url':
			if (!/^(http(s)?:)?\/\//i.test(value)) {
				methodName = method + '_protocol';
			} else if (!/((?!\/).)\..*$/.test(value)) {
				methodName = method + '_domen';
			} else if (!/\..{2,}$/.test(value)) {
				methodName = method + '_domen-length';
			} else {
				methodName = method;
			}
			break;
		case 'email':
			if (/(@\.|\.$)/.test(value)) {
				methodName = method + '_dot';
			} else if (/@.*(\\|\/)/.test(value)) {
				methodName = method + '_slash';
			} else if (!/@/.test(value)) {
				methodName = method + '_at';
			} else if (/^@/.test(value)) {
				methodName = method + '_at-start';
			} else if (value.split('@').length > 2) {
				methodName = method + '_at-multiple';
			} else if (/@$/.test(value)) {
				methodName = method + '_at-end';
			} else {
				methodName = method;
			}
			break;
		default:
			methodName = method;
	}
	return methodName;
}

// ----------------------------------------
// Public
// ----------------------------------------

/**
 * Объект расширений
 * @const {Object}
 */
var extendConfig = {
	/**
  * Прототипы плагина
  * @var {Object} messages
  * @sourceCode |+30
  */
	get messages() {
		var translates = window.jsTranslations && window.jsTranslations['jquery-validation'] || {};
		if (Object.keys(translates).length === 0) {
			return console.warn('Переводы для jquery-validation - отсутствуют!');
		}

		for (var key in translates) {
			var value = translates[key];

			switch (key) {
				case 'maxlength':
				case 'maxlength_checker':
				case 'maxlength_select':
				case 'minlength':
				case 'minlength_checker':
				case 'minlength_select':
				case 'rangelength':
				case 'rangelength_checker':
				case 'rangelength_select':
				case 'range':
				case 'min':
				case 'max':
				case 'filetype':
				case 'filesize':
				case 'filesizeEach':
				case 'pattern':
					translates[key] = $.validator.format(value);
					break;
			}
		}

		return translates;
	},

	/**
  * Прототипы плагина
  * @var {Object} prototype
  */
	prototype: {
		/**
   * Вывод дефолтных сообщений
   * @param {Element} element
   * @param {Object|string} rule
   * @return {string}
   * @method prototype::defaultMessage
   */
		defaultMessage: function defaultMessage(element, rule) {
			if (typeof rule === 'string') {
				rule = { method: rule };
			}

			var message = this.findDefined(this.customMessage(element.name, rule.method), this.customDataMessage(element, rule.method), !this.settings.ignoreTitle && element.title || undefined, // eslint-disable-line no-mixed-operators
			$.validator.messages[formGetMethodMsgName(element, rule.method)], '<strong>Warning: No message defined for ' + element.name + '</strong>');

			var pattern = /\$?\{(\d+)\}/g;

			if (typeof message === 'function') {
				message = message.call(this, rule.parameters, element);
			} else if (pattern.test(message)) {
				message = $.validator.format(message.replace(pattern, '{$1}'), rule.parameters);
			}

			return message;
		}
	},

	/**
  * Методы валидации
  * @var {Object} methods
  */
	methods: _validateWezomMethods2.default
};

for (var key in extendConfig) {
	$.extend($.validator[key], extendConfig[key]);
}

$.validator.addClassRules(_validateClassRules2.default);

/***/ }),
/* 39 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * Расширение автоматической валидации по имени CSS классов
 * @module
 */

// ----------------------------------------
// Public
// ----------------------------------------

/**
 * Список правил для валидации элементов по одному классу
 * Каждое свойство - css класс
 * @type {Object}
 * @sourceCode
 */

Object.defineProperty(exports, "__esModule", {
	value: true
});
var classRules = {
	'validate-as-login': {
		minlength: 2,
		login: true,
		maxlength: 6,
		required: true
	},
	'validate-as-file-images': {
		filetype: 'png|jpe?g|gif|svg',
		// filesizeeach: 500,
		filesize: 500,
		maxupload: 2,
		required: true
	}
};

// ----------------------------------------
// Exports
// ----------------------------------------

exports.default = classRules;

/***/ }),
/* 40 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * Дополнительные методы валидации
 * @module
 */

// ----------------------------------------
// Public
// ----------------------------------------

/**
 * Методы валидации
 * @var {Object} methods
 */

Object.defineProperty(exports, "__esModule", {
	value: true
});
var methods = {
	/**
  * Валидация email
  * @param {string} value
  * @param {HTMLInputElement|HTMLTextAreaElement} element
  * @param {Object} param
  * @return {boolean}
  * @method methods::email
  * @sourceCode |+4
  */
	email: function email(value, element) {
		var pattern = /^(([a-zA-Z0-9&+-=_])+((\.([a-zA-Z0-9&+-=_]){1,})+)?){1,64}@([a-zA-Z0-9]([a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?\.)+[a-zA-Z]{2,6}$/;
		return this.optional(element) || pattern.test(value);
	},


	/**
  * Валидация по паттерну
  * @param {string} value
  * @param {HTMLInputElement|HTMLTextAreaElement} element
  * @param {string|RegExp} param
  * @return {boolean}
  * @method methods::pattern
  * @sourceCode |+9
  */
	pattern: function pattern(value, element, param) {
		if (this.optional(element)) {
			return true;
		}
		if (typeof param === 'string') {
			param = new RegExp('^(?:' + param + ')$');
		}
		return param.test(value);
	},


	/**
  * Валидация паролей
  * @param {string} value
  * @param {HTMLInputElement} element
  * @param {Object} param
  * @return {boolean}
  * @method methods::password
  * @sourceCode |+3
  */
	password: function password(value, element) {
		return this.optional(element) || /^\S.*$/.test(value);
	},


	/**
  * Проверка общего объема всех файла в KB
  * @param {string} value
  * @param {HTMLInputElement} element
  * @param {Object} param
  * @return {boolean}
  * @method methods::filesize
  * @sourceCode |+7
  */
	filesize: function filesize(value, element, param) {
		var kb = 0;
		for (var i = 0; i < element.files.length; i++) {
			kb += element.files[i].size;
		}
		return this.optional(element) || kb / 1024 <= param;
	},


	/**
  * Максимальное количество файлов для загрузки
  * @param {string} value
  * @param {HTMLInputElement} element
  * @param {Object} param
  * @return {boolean}
  * @method methods::maxupload
  * @sourceCode |+3
  */
	maxupload: function maxupload(value, element, param) {
		return element.files.length <= param;
	},


	/**
  * Проверка объема каждого файла в KB
  * @param {string} value
  * @param {HTMLInputElement} element
  * @param {Object} param
  * @return {boolean}
  * @method methods::filesizeeach
  * @sourceCode |+10
  */
	filesizeeach: function filesizeeach(value, element, param) {
		var flag = true;
		for (var i = 0; i < element.files.length; i++) {
			if (element.files[i].size / 1024 > param) {
				flag = false;
				break;
			}
		}
		return this.optional(element) || flag;
	},


	/**
  * Проверка типа файла по расширению
  * @param {string} value
  * @param {HTMLInputElement} element
  * @param {Object} param
  * @return {boolean}
  * @method methods::filetype
  * @sourceCode |+25
  */
	filetype: function filetype(value, element, param) {
		var result = void 0;
		var extensions = 'png|jpe?g|gif|svg|doc|pdf|zip|rar|tar|html|swf|txt|xls|docx|xlsx|odt';
		if (element.files.length < 1) {
			return true;
		}

		param = typeof param === 'string' ? param.replace(/,/g, '|') : extensions;
		if (element.multiple) {
			var files = element.files;
			for (var i = 0; i < files.length; i++) {
				var _value = files[i].name;
				var valueMatch = _value.match(new RegExp('.(' + param + ')$', 'i'));

				result = this.optional(element) || valueMatch;
				if (result === null) {
					break;
				}
			}
		} else {
			var _valueMatch = value.match(new RegExp('\\.(' + param + ')$', 'i'));
			result = this.optional(element) || _valueMatch;
		}
		return result;
	},


	/**
  * Проверка валидности одного из указанных элементов
  * _этот или другой - должен быть валидным_
  * @param {string} value
  * @param {HTMLInputElement|HTMLTextAreaElement|HTMLSelectElement} element
  * @param {Object} param
  * @return {boolean}
  * @method methods::or
  * @sourceCode |+4
  */
	or: function or(value, element, param) {
		var $module = $(element.form);
		return $module.find(param + ':filled').length;
	},


	/**
  * Проверка валидности слов (чаще всего используется для имени или фамилии)
  * @param {string} value
  * @param {HTMLInputElement|HTMLTextAreaElement} element
  * @return {boolean}
  * @method methods::word
  * @sourceCode |+4
  */
	word: function word(value, element) {
		var testValue = /^[a-zA-Zа-яА-ЯіІїЇєёЁЄґҐĄąĆćĘęŁłŃńÓóŚśŹźŻż\'\`\- ]*$/.test(value); // eslint-disable-line no-useless-escape
		return this.optional(element) || testValue;
	},


	/**
  * Проверка валидности логинов
  * @param {string} value
  * @param {HTMLInputElement|HTMLTextAreaElement} element
  * @param {Object} param
  * @return {boolean}
  * @method methods::login
  * @sourceCode |+4
  */
	login: function login(value, element) {
		var testValue = /^[0-9a-zA-Zа-яА-ЯіІїЇєЄёЁґҐ][0-9a-zA-Zа-яА-ЯіІїЇєЄґҐĄąĆćĘęŁłŃńÓóŚśŹźŻż\-\._]+$/.test(value); // eslint-disable-line no-useless-escape
		return this.optional(element) || testValue;
	},


	/**
  * Валидация номера телефона (укр)
  * @param {string} value
  * @param {HTMLInputElement|HTMLTextAreaElement} element
  * @return {boolean}
  * @method methods::phoneua
  * @sourceCode |+3
  */
	phoneua: function phoneua(value, element) {
		function testValue(value) {
			if (/^(\+)?38/.test(value)) {
				return (/^(((\+?)(38))\s?)(([0-9]{3})|(\([0-9]{3}\)))(\-|\s)?(([0-9]{3})(\-|\s)?([0-9]{2})(\-|\s)?([0-9]{2})|([0-9]{2})(\-|\s)?([0-9]{2})(\-|\s)?([0-9]{3})|([0-9]{2})(\-|\s)?([0-9]{3})(\-|\s)?([0-9]{2}))$/.test(value)
				); // eslint-disable-line no-useless-escape
			}
			return (/^(([0-9]{3})|(\([0-9]{3}\)))(\-|\s)?(([0-9]{3})(\-|\s)?([0-9]{2})(\-|\s)?([0-9]{2})|([0-9]{2})(\-|\s)?([0-9]{2})(\-|\s)?([0-9]{3})|([0-9]{2})(\-|\s)?([0-9]{3})(\-|\s)?([0-9]{2}))$/.test(value)
			); // eslint-disable-line no-useless-escape
		}

		return this.optional(element) || testValue(value);
	},


	/**
  * Валидация номера телефона (общая)
  * @param {string} value
  * @param {HTMLInputElement|HTMLTextAreaElement} element
  * @param {Object} param
  * @return {boolean}
  * @method methods::phone
  * @sourceCode |+3
  */
	phone: function phone(value, element) {
		return this.optional(element) || /^(((\+?)(\d{1,3}))\s?)?(([0-9]{0,4})|(\([0-9]{3}\)))(\-|\s)?(([0-9]{3})(\-|\s)?([0-9]{2})(\-|\s)?([0-9]{2})|([0-9]{2})(\-|\s)?([0-9]{2})(\-|\s)?([0-9]{3})|([0-9]{2})(\-|\s)?([0-9]{3})(\-|\s)?([0-9]{2}))$/.test(value); // eslint-disable-line no-useless-escape
	},


	/**
  * Проверки пробелов в значении
  * @param {string} value
  * @return {boolean}
  * @method methods::nospace
  * @sourceCode |+4
  */
	nospace: function nospace(value) {
		var str = String(value).replace(/\s|\t|\r|\n/g, '');
		return str.length > 0;
	},


	/**
  * Проверки валидности элемента
  * @param {string} value
  * @param {HTMLInputElement|HTMLTextAreaElement|HTMLSelectElement} element
  * @return {boolean}
  * @method methods::validTrue
  * @sourceCode |+3
  */
	validdata: function validdata(value, element) {
		return $(element).data('valid') === true;
	}
};

// ----------------------------------------
// Exports
// ----------------------------------------

exports.default = methods;

/***/ }),
/* 41 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * Пользовательские обработчики формы
 * @module
 */

// ----------------------------------------
// Public
// ----------------------------------------

/**
 * @param {JQuery} $form
 * @param {Object} Validator {@link https://jqueryvalidation.org/?s=validator}
 * @param {Function} Validator.resetForm {@link https://jqueryvalidation.org/Validator.resetForm/}
 * @param {Function} Validator.element {@link https://jqueryvalidation.org/Validator.element/}
 */

Object.defineProperty(exports, "__esModule", {
	value: true
});
function validateHandlers($form, Validator) {
	// сабмит формы, блок отправки при ошибке скриптов
	var method = $form.attr('method') || 'post';
	$form.prop('method', method);
	$form.on('submit', function (event) {
		var action = $form.attr('action');
		if (!action) {
			event.preventDefault();
		}
	});

	// сброс формы
	$form.on('reset', function () {
		return Validator.resetForm();
	});

	// проверка файлов, при смене
	$form.on('change', 'input[type="file"]', function () {
		Validator.element(this);
	});

	// дальше ваш код
}

// ----------------------------------------
// Exports
// ----------------------------------------

exports.default = validateHandlers;

/***/ }),
/* 42 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * Обработка ответа от сервера
 * @module
 */

// ----------------------------------------
// Imports
// ----------------------------------------

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _Preloader = __webpack_require__(1);

var _Preloader2 = _interopRequireDefault(_Preloader);

var _moduleLoader = __webpack_require__(2);

var _moduleLoader2 = _interopRequireDefault(_moduleLoader);

__webpack_require__(11);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// ----------------------------------------
// Private
// ----------------------------------------

/**
 * Вывод уведомлений
 * @param {JQuery} $form
 * @param {string} message
 * @param {boolean} [success]
 * @private
 */

var magnificPopup = $.magnificPopup.instance;
var url = '/Views/Widgets/Popup/Thank.php';

function showMessage($form, message, success) {
	if (magnificPopup.isOpen) {
		var $popup = $form.closest('.popup');

		if ($('.login-popup').length) {
			$popup.find('.grid').hide();
			$popup.append('<div class="grid popup-noty"><div class="gcell--24"><p class=\'thank-popup__text thank-popup__text--inner\'>' + message + '</p></div><button class="this-close" id="custom-close-btn" style="color:red; font-size: 15px">\u0412\u0435\u0440\u043D\u0443\u0442\u044C\u0441\u044F \u043D\u0430\u0437\u0430\u0434</button></div>');

			$('.this-close').on('click', function () {
				$popup.find('.grid').show();
				$('.popup-noty').remove();
			});
		} else {
			$popup.html('<p class=\'thank-popup__text thank-popup__text--inner\'>' + message + '</p><button class=\'mfp-close\' id=\'custom-close-btn\'></button>');
		}

		return false;
	}

	$.magnificPopup.open({
		items: {
			src: url
		},
		type: 'ajax',
		removalDelay: 300,
		mainClass: 'zoom-in',
		closeMarkup: '<button class=\'mfp-close\' id=\'custom-close-btn\'></button>',
		callbacks: {
			ajaxContentAdded: function ajaxContentAdded() {
				$('.thank-popup__text').html(message);
			}
		}
	});
}

function showWidgetMessage($form, message, widget) {
	if (magnificPopup.isOpen) {
		var $popup = $form.closest('.popup');

		$popup.html('<p class=\'thank-popup__text thank-popup__text--inner\'>' + message + '</p><button class=\'mfp-close\' id=\'custom-close-btn\'></button>' + widget);

		return false;
	}

	$.magnificPopup.open({
		items: {
			src: url
		},
		type: 'ajax',
		removalDelay: 300,
		mainClass: 'zoom-in',
		closeMarkup: '<button class=\'mfp-close\' id=\'custom-close-btn\'></button>',
		callbacks: {
			ajaxContentAdded: function ajaxContentAdded() {
				$('.thank-popup__text').html(message);
			}
		}
	});
}

function closePopup() {
	if (magnificPopup.isOpen) {
		$.magnificPopup.close();
	}
}

function sum(arr) {
	var reducer = function reducer(accumulator, currentValue) {
		return parseInt(accumulator) + parseInt(currentValue);
	};

	return arr.reduce(reducer);
}

// Thousand separator

function setThousands(numberText, newSeparator, separator) {
	newSeparator = newSeparator || '.';
	separator = separator || '.';
	numberText = '' + numberText;
	numberText = numberText.split(separator);

	var numberPenny = numberText[1] || '';
	var numberValue = numberText[0];

	var thousandsValue = [];
	var counter = 0;

	for (var i = numberValue.length - 1; i >= 0; i--) {
		var num = numberValue[i];
		thousandsValue.push(num);

		if (++counter === 3 && i) {
			thousandsValue.push(' ');
			counter = 0;
		}
	}

	thousandsValue = thousandsValue.reverse().join('');
	if (numberPenny.length) {
		return [thousandsValue, numberPenny].join(newSeparator);
	}
	return thousandsValue;
}

function updateStaticCart(response) {
	if ($('.js-cart-static').length && typeof response.static !== 'undefined') {
		$('.js-cart-static').html(response.static);

		if (!response.count) {
			$('[data-total]').addClass('_hide');
			$('[data-total-empty]').removeClass('_hide');
		}

		if ($('.js-total-price').length && typeof response.totalPrice !== 'undefined') {
			$('.js-total-price').text(response.totalPrice.toFixed(2));
		}

		_moduleLoader2.default.init($('.js-cart-static'));
	}

	if ($('.js-cart').length && typeof response.html !== 'undefined') {
		var $html = $(response.html);
		$('.js-cart').html($html);

		_moduleLoader2.default.init($('.js-cart'));
	}

	if ($('.js-cart-count').length && typeof response.count !== 'undefined') {
		$('.js-cart-count').html(response.count);
	}
}

function changeSizes($form, sizes, counts) {
	var $wrap = $('.js-product-counts');
	var $items = $wrap.find('.js-product-count-item');
	var $cartItem = $wrap.closest('.js-cart-item');

	var $counter = $cartItem.find('.js-product-count-all');
	var $counterInput = $cartItem.find('.js-product-input');
	var $price = $cartItem.find('.js-product-price');

	var count = sum(counts);
	var price = $price.data('price') * count;

	$cartItem.data('sizes', sizes);
	$cartItem.data('counts', counts);
	$cartItem.data('count', count);
	$counter.text(count);
	$counterInput.val(count);
	$price.text(setThousands(price.toFixed(2)));

	$items.removeClass('active');

	$items.each(function (i, el) {
		var $item = $(this);
		var $count = $(this).find('.js-product-count');

		var param = $item.data('param');
		param.param.counts = counts;

		$item.attr('data-param', JSON.stringify(param));
		$count.text('');

		sizes.forEach(function (item, i) {
			if ($item.data('size') === sizes[i]) {
				if (counts[i] > 0) {
					$count.html(counts[i]);
					$item.addClass('active');
				}
			}
		});
	});
}

// ----------------------------------------
// Public
// ----------------------------------------

/**
 * @param {JQuery} $form
 * @param {Object} response
 * @param {string} statusText
 * @param {Object} xhr
 * @param {string} response.response - текстовое сообщение
 * @param {boolean} response.success - успешный запрос
 * @param {string} [response.redirect] - урл для редиректа, если равен текущему урлу - перегражаем страницу
 * @param {boolean} [response.reload] - перегрузить страницу
 * @param {boolean} [response.reset] - сбросить форму
 * @param {Array} [response.clear] - сбросить форму
 * @private
 */
function validateGetResponse($form, response, statusText, xhr) {
	var preloader = $form.data('preloader');
	if (preloader instanceof _Preloader2.default) {
		preloader.hide();
	}

	// обрабатываем ответ
	// ------------------
	if (typeof response === 'string') {
		response = JSON.parse(response);
	}

	if (response.reload || window.location.href === response.redirect) {
		return window.location.reload();
	}

	if (response.redirect) {
		return window.location.href = response.redirect;
	}

	if (response.success) {
		if (response.clear) {
			if (Array.isArray(response.clear)) {
				response.clear.forEach(function (clearSelector) {
					$form.find(clearSelector).val('');
				});
			} else {
				// игнорируемые типы инпутов
				var ignoredInputsType = ['submit', 'reset', 'button', 'image'];
				$form.find('input, textarea, select').each(function (i, element) {
					if (~ignoredInputsType.indexOf(element.type)) {
						return true;
					}
					element.value = '';
				});
			}
		} else if (response.reset) {
			$form.trigger('reset');
		}
	}
	if (response.sizes) {
		changeSizes($form, response.sizes, response.counts);
	}
	if (response.cart) {
		updateStaticCart(response);
	}
	if (response.response) {
		if (response.widget) {
			showWidgetMessage($form, response.message, response.response);
		} else {
			showMessage($form, response.response, response.success);
		}
	}
	if (response.close) {
		closePopup();
	}
	if (response.form) {
		var $html = $('<div class="_hide">' + response.form + '</div>');
		$form.append($html);
		$html.find('form').get(0).submit();
	}
}

// ----------------------------------------
// Exports
// ----------------------------------------

exports.default = validateGetResponse;

/***/ }),
/* 43 */,
/* 44 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * Расширение дефолтных параметров плагина
 * @module
 */

// ----------------------------------------
// Public
// ----------------------------------------

(function ($) {
	var jsTranslations = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

	var mfp = jsTranslations['magnific-popup'] || {};
	if (Object.keys(mfp).length === 0) {
		return console.warn('Переводы для magnificPopup - отсутствуют!');
	}

	$.extend(true, $.magnificPopup.defaults, {
		tClose: mfp.tClose,
		tLoading: mfp.tLoading,
		gallery: {
			tPrev: mfp.tPrev,
			tNext: mfp.tNext,
			tCounter: mfp.tCounter
		},
		image: {
			tError: mfp.tErrorImage
		},
		ajax: {
			tError: mfp.tError
		},
		inline: {
			tNotFound: mfp.tNotFound
		}
	});
})(window.jQuery, window.jsTranslations);

/***/ }),
/* 45 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(46);
if(typeof content === 'string') content = [[module.i, content, '']];
// Prepare cssTransformation
var transform;

var options = {"insertAt":{"before":"#webpack-style-loader-insert-before-this"},"hmr":true}
options.transform = transform
// add the styles to the DOM
var update = __webpack_require__(10)(content, options);
if(content.locals) module.exports = content.locals;
// Hot Module Replacement
if(false) {
	// When the styles change, update the <style> tags
	if(!content.locals) {
		module.hot.accept("!!../../../../../node_modules/css-loader/index.js??ref--2-1!../../../../../node_modules/postcss-loader/lib/index.js??ref--2-2!../../../../../node_modules/sass-loader/lib/loader.js??ref--2-3!./mfp-extend.scss", function() {
			var newContent = require("!!../../../../../node_modules/css-loader/index.js??ref--2-1!../../../../../node_modules/postcss-loader/lib/index.js??ref--2-2!../../../../../node_modules/sass-loader/lib/loader.js??ref--2-3!./mfp-extend.scss");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
	}
	// When the module is disposed, remove the <style> tags
	module.hot.dispose(function() { update(); });
}

/***/ }),
/* 46 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(9)(false);
// imports


// module
exports.push([module.i, "/* Magnific Popup CSS */\n.mfp-bg {\n  top: 0;\n  left: 0;\n  width: 100%;\n  height: 100%;\n  z-index: 1042;\n  overflow: hidden;\n  position: fixed;\n  background: #0b0b0b;\n  opacity: 0.8; }\n\n.mfp-wrap {\n  top: 0;\n  left: 0;\n  width: 100%;\n  height: 100%;\n  z-index: 1043;\n  position: fixed;\n  outline: none !important;\n  -webkit-backface-visibility: hidden; }\n\n.mfp-container {\n  text-align: center;\n  position: absolute;\n  width: 100%;\n  height: 100%;\n  left: 0;\n  top: 0;\n  padding: 0 8px;\n  box-sizing: border-box; }\n\n.mfp-container:before {\n  content: '';\n  display: inline-block;\n  height: 100%;\n  vertical-align: middle; }\n\n.mfp-align-top .mfp-container:before {\n  display: none; }\n\n.mfp-content {\n  position: relative;\n  display: inline-block;\n  vertical-align: middle;\n  margin: 0 auto;\n  text-align: left;\n  z-index: 1045; }\n\n.mfp-inline-holder .mfp-content,\n.mfp-ajax-holder .mfp-content {\n  width: 100%;\n  cursor: auto; }\n\n.mfp-ajax-cur {\n  cursor: progress; }\n\n.mfp-zoom-out-cur, .mfp-zoom-out-cur .mfp-image-holder .mfp-close {\n  cursor: zoom-out; }\n\n.mfp-zoom {\n  cursor: pointer;\n  cursor: zoom-in; }\n\n.mfp-auto-cursor .mfp-content {\n  cursor: auto; }\n\n.mfp-close,\n.mfp-arrow,\n.mfp-preloader,\n.mfp-counter {\n  -webkit-user-select: none;\n  -moz-user-select: none;\n  -ms-user-select: none;\n  user-select: none; }\n\n.mfp-loading.mfp-figure {\n  display: none; }\n\n.mfp-hide {\n  display: none !important; }\n\n.mfp-preloader {\n  color: #CCC;\n  position: absolute;\n  top: 50%;\n  width: auto;\n  text-align: center;\n  margin-top: -0.8em;\n  left: 8px;\n  right: 8px;\n  z-index: 1044; }\n  .mfp-preloader a {\n    color: #CCC; }\n    .mfp-preloader a:hover {\n      color: #FFF; }\n\n.mfp-s-ready .mfp-preloader {\n  display: none; }\n\n.mfp-s-error .mfp-content {\n  display: none; }\n\nbutton.mfp-close, button.mfp-arrow {\n  overflow: visible;\n  cursor: pointer;\n  background: transparent;\n  border: 0;\n  -webkit-appearance: none;\n  display: block;\n  outline: none;\n  padding: 0;\n  z-index: 1046;\n  box-shadow: none;\n  touch-action: manipulation; }\n\nbutton::-moz-focus-inner {\n  padding: 0;\n  border: 0; }\n\n.mfp-close {\n  width: 44px;\n  height: 44px;\n  line-height: 44px;\n  position: absolute;\n  right: 0;\n  top: 0;\n  text-decoration: none;\n  text-align: center;\n  opacity: 0.65;\n  padding: 0 0 18px 10px;\n  color: #FFF;\n  font-style: normal;\n  font-size: 28px;\n  font-family: Arial, Baskerville, monospace; }\n  .mfp-close:hover, .mfp-close:focus {\n    opacity: 1; }\n  .mfp-close:active {\n    top: 1px; }\n\n.mfp-close-btn-in .mfp-close {\n  color: #333; }\n\n.mfp-image-holder .mfp-close,\n.mfp-iframe-holder .mfp-close {\n  color: #FFF;\n  right: -6px;\n  text-align: right;\n  padding-right: 6px;\n  width: 100%; }\n\n.mfp-counter {\n  position: absolute;\n  top: 0;\n  right: 0;\n  color: #CCC;\n  font-size: 12px;\n  line-height: 18px;\n  white-space: nowrap; }\n\n.mfp-arrow {\n  position: absolute;\n  opacity: 0.65;\n  margin: 0;\n  top: 50%;\n  margin-top: -55px;\n  padding: 0;\n  width: 90px;\n  height: 110px;\n  -webkit-tap-highlight-color: transparent; }\n  .mfp-arrow:active {\n    margin-top: -54px; }\n  .mfp-arrow:hover, .mfp-arrow:focus {\n    opacity: 1; }\n  .mfp-arrow:before, .mfp-arrow:after {\n    content: '';\n    display: block;\n    width: 0;\n    height: 0;\n    position: absolute;\n    left: 0;\n    top: 0;\n    margin-top: 35px;\n    margin-left: 35px;\n    border: medium inset transparent; }\n  .mfp-arrow:after {\n    border-top-width: 13px;\n    border-bottom-width: 13px;\n    top: 8px; }\n  .mfp-arrow:before {\n    border-top-width: 21px;\n    border-bottom-width: 21px;\n    opacity: 0.7; }\n\n.mfp-arrow-left {\n  left: 0; }\n  .mfp-arrow-left:after {\n    border-right: 17px solid #FFF;\n    margin-left: 31px; }\n  .mfp-arrow-left:before {\n    margin-left: 25px;\n    border-right: 27px solid #3F3F3F; }\n\n.mfp-arrow-right {\n  right: 0; }\n  .mfp-arrow-right:after {\n    border-left: 17px solid #FFF;\n    margin-left: 39px; }\n  .mfp-arrow-right:before {\n    border-left: 27px solid #3F3F3F; }\n\n.mfp-iframe-holder {\n  padding-top: 40px;\n  padding-bottom: 40px; }\n  .mfp-iframe-holder .mfp-content {\n    line-height: 0;\n    width: 100%;\n    max-width: 900px; }\n  .mfp-iframe-holder .mfp-close {\n    top: -40px; }\n\n.mfp-iframe-scaler {\n  width: 100%;\n  height: 0;\n  overflow: hidden;\n  padding-top: 56.25%; }\n  .mfp-iframe-scaler iframe {\n    position: absolute;\n    display: block;\n    top: 0;\n    left: 0;\n    width: 100%;\n    height: 100%;\n    box-shadow: 0 0 8px rgba(0, 0, 0, 0.6);\n    background: #000; }\n\n/* Main image in popup */\nimg.mfp-img {\n  width: auto;\n  max-width: 100%;\n  height: auto;\n  display: block;\n  line-height: 0;\n  box-sizing: border-box;\n  padding: 40px 0 40px;\n  margin: 0 auto; }\n\n/* The shadow behind the image */\n.mfp-figure {\n  line-height: 0; }\n  .mfp-figure:after {\n    content: '';\n    position: absolute;\n    left: 0;\n    top: 40px;\n    bottom: 40px;\n    display: block;\n    right: 0;\n    width: auto;\n    height: auto;\n    z-index: -1;\n    box-shadow: 0 0 8px rgba(0, 0, 0, 0.6);\n    background: #444; }\n  .mfp-figure small {\n    color: #BDBDBD;\n    display: block;\n    font-size: 12px;\n    line-height: 14px; }\n  .mfp-figure figure {\n    margin: 0; }\n\n.mfp-bottom-bar {\n  margin-top: -36px;\n  position: absolute;\n  top: 100%;\n  left: 0;\n  width: 100%;\n  cursor: auto; }\n\n.mfp-title {\n  text-align: left;\n  line-height: 18px;\n  color: #F3F3F3;\n  word-wrap: break-word;\n  padding-right: 36px; }\n\n.mfp-image-holder .mfp-content {\n  max-width: 100%; }\n\n.mfp-gallery .mfp-image-holder .mfp-figure {\n  cursor: pointer; }\n\n.mfp-animate-zoom-in .mfp-iframe-scaler {\n  overflow: initial; }\n  .mfp-animate-zoom-in .mfp-iframe-scaler .mfp-close {\n    width: 44px; }\n\n.mfp-animate-zoom-in .mfp-iframe-scaler iframe,\n.mfp-animate-zoom-in .mfp-figure,\n.mfp-animate-zoom-in .mfp-arrow,\n.mfp-animate-zoom-in .mfp-content > * {\n  opacity: 0;\n  transform: scale(0.8);\n  transition: transform 0.2s ease-in-out, opacity 0.2s ease-in-out; }\n\n.mfp-animate-zoom-in.mfp-ready .mfp-iframe-scaler iframe,\n.mfp-animate-zoom-in.mfp-ready .mfp-figure,\n.mfp-animate-zoom-in.mfp-ready .mfp-arrow,\n.mfp-animate-zoom-in.mfp-ready .mfp-content > * {\n  opacity: 1;\n  transform: scale(1); }\n\n.mfp-animate-zoom-in.mfp-removing .mfp-iframe-scaler iframe,\n.mfp-animate-zoom-in.mfp-removing .mfp-figure,\n.mfp-animate-zoom-in.mfp-removing .mfp-arrow,\n.mfp-animate-zoom-in.mfp-removing .mfp-content > * {\n  opacity: 0;\n  transform: scale(0.8); }\n\n.mfp-animate-zoom-in.mfp-bg {\n  opacity: 0;\n  transition: opacity 0.3s ease-in-out; }\n  .mfp-animate-zoom-in.mfp-bg.mfp-ready {\n    opacity: .8; }\n  .mfp-animate-zoom-in.mfp-bg.mfp-removing {\n    opacity: 0; }\n  @media all and (max-width: 900px) {\n  .mfp-arrow {\n    transform: scale(0.75); }\n  .mfp-arrow-left {\n    transform-origin: 0; }\n  .mfp-arrow-right {\n    transform-origin: 100%; }\n  .mfp-container {\n    padding-left: 6px;\n    padding-right: 6px; } }\n  @media screen and (max-width: 800px) and (orientation: landscape), screen and (max-height: 300px) {\n  /**\n       * Remove all paddings around the image on small screen\n       */\n  .mfp-img-mobile .mfp-image-holder {\n    padding-left: 0;\n    padding-right: 0; }\n  .mfp-img-mobile img.mfp-img {\n    padding: 0; }\n  .mfp-img-mobile .mfp-figure:after {\n    top: 0;\n    bottom: 0; }\n  .mfp-img-mobile .mfp-figure small {\n    display: inline;\n    margin-left: 5px; }\n  .mfp-img-mobile .mfp-bottom-bar {\n    background: rgba(0, 0, 0, 0.6);\n    bottom: 0;\n    margin: 0;\n    top: auto;\n    padding: 3px 5px;\n    position: fixed;\n    box-sizing: border-box; }\n    .mfp-img-mobile .mfp-bottom-bar:empty {\n      padding: 0; }\n  .mfp-img-mobile .mfp-counter {\n    right: 5px;\n    top: 3px; }\n  .mfp-img-mobile .mfp-close {\n    top: 0;\n    right: 0;\n    width: 35px;\n    height: 35px;\n    line-height: 35px;\n    background: rgba(0, 0, 0, 0.6);\n    position: fixed;\n    text-align: center;\n    padding: 0; } }\n", ""]);

// exports


/***/ }),
/* 47 */,
/* 48 */,
/* 49 */,
/* 50 */,
/* 51 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * Wezom Standard Tabs
 * @module
 */

// ----------------------------------------
// Imports
// ----------------------------------------

Object.defineProperty(exports, "__esModule", {
	value: true
});

__webpack_require__(52);

// ----------------------------------------
// Private
// ----------------------------------------

/**
 * Не реагировать на клик
 * @param {JQuery} $button
 * @return {boolean|undefined}
 * @private
 */
function noReact($button) {
	return $button.hasClass(wsTabs.cssClass.active) || $button.hasClass(wsTabs.cssClass.disabled) || $button.prop('disabled');
}

/**
 * @param {JQuery} $button
 * @param {JQuery} $context
 * @private
 */
function changeTab($button, $context) {
	var myNs = $button.data(wsTabs.keys.ns);
	var myName = $button.data(wsTabs.keys.button);

	var buttonsSelector = '[data-' + wsTabs.keys.ns + '="' + myNs + '"][data-' + wsTabs.keys.button + ']';
	var buttonSyncSelector = '[data-' + wsTabs.keys.ns + '="' + myNs + '"][data-' + wsTabs.keys.button + '="' + myName + '"]';
	var blocksSelector = '[data-' + wsTabs.keys.ns + '="' + myNs + '"][data-' + wsTabs.keys.block + ']';
	var blockSelector = '[data-' + wsTabs.keys.ns + '="' + myNs + '"][data-' + wsTabs.keys.block + '="' + myName + '"]';

	/**
  * @type {JQuery}
  */
	var $block = $button.getMyElements(wsTabs.keys.myBlock, blockSelector);
	if (noReact($button)) {
		$button.add($block).trigger(wsTabs.events.again);
		return false;
	}

	/**
  * @type {JQuery}
  */
	var $siblingBlocks = $block.getMyElements(wsTabs.keys.myBlocks, blocksSelector, $context, true);

	/**
  * @type {JQuery}
  */
	var $siblingButtons = $button.getMyElements(wsTabs.keys.myButtons, buttonsSelector, $context, true);
	var $syncButtons = $siblingButtons.filter(buttonSyncSelector);
	if ($syncButtons.length) {
		$siblingButtons = $siblingButtons.not($syncButtons);
	}

	$siblingButtons.add($siblingBlocks).removeClass(wsTabs.cssClass.active).trigger(wsTabs.events.off);
	$button.add($syncButtons).add($block).addClass(wsTabs.cssClass.active).trigger(wsTabs.events.on);
}

/**
 * Активация табов, если нету активных
 * @param {JQuery} $buttons
 * @param {JQuery} $context
 * @private
 */
function setActiveIfNotHave($buttons, $context) {
	var ns = $buttons.data(wsTabs.keys.ns);
	var selector = '[data-' + wsTabs.keys.ns + '="' + ns + '"]';
	var $group = $buttons.filter(selector);

	if ($group.length) {
		var $active = $group.filter('.' + wsTabs.cssClass.active);
		if (!$active.length) {
			changeTab($group.eq(0), $context);
		}

		if ($group.length < $buttons.length) {
			setActiveIfNotHave($buttons.not(selector), $context);
		}
	}
}

/**
 * Сброс зависимолстей
 * @param {JQuery} $list
 * @param {Array.<string>} keys
 * @private
 */
function _dropDependencies($list, keys) {
	$list.each(function (i, el) {
		var $item = $(el);
		wsTabs.keys.forEach(function (key) {
			$item.data(key, null);
		});
	});
}

// ----------------------------------------
// Public
// ----------------------------------------

/**
 * @namespace
 */
var wsTabs = {
	/**
  * События
  * @enum {string}
  * @sourceCode
  */
	events: {
		on: 'wstabs-on',
		off: 'wstabs-off',
		again: 'wstabs-again'
	},

	/**
  * CSS классы
  * @enum {string}
  * @sourceCode
  */
	cssClass: {
		active: 'is-active',
		disabled: 'is-disabled'
	},

	/**
  * Ключи
  * @enum {string}
  * @sourceCode
  */
	keys: {
		ns: 'wstabs-ns',
		button: 'wstabs-button',
		block: 'wstabs-block',
		myBlock: '$myWsTabsBlock',
		myBlocks: '$myWsTabsBlocks',
		myButtons: '$myWsTabsButtons'
	},

	/**
  * Инициализация
  * @param {JQuery} [$context=$(document)]
  * @sourceCode
  */
	init: function init() {
		var $context = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : $(document);

		$context.on('click', '[data-' + wsTabs.keys.button + ']', { $context: $context }, function (event) {
			event.preventDefault();
			changeTab($(this), $context);
		});
	},


	/**
  * Принудительная активация табов, если нету активных
  * @param {JQuery} [$context=$(document)]
  * @sourceCode
  */
	setActive: function setActive() {
		var $context = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : $(document);

		var $buttons = $context.find('[data-' + wsTabs.keys.button + ']');
		setActiveIfNotHave($buttons, $context);
	},


	/**
  * Сброс всех связей.
  * Актуально при динамическом добавление новый кнопок и блоков в уже существующие группы табов
  * @param {JQuery} [$context=$(document)]
  * @sourceCode
  */
	dropDependencies: function dropDependencies() {
		var $context = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : $(document);

		var $buttons = $context.find('[data-' + wsTabs.keys.button + ']');
		var $blocks = $context.find('[data-' + wsTabs.keys.block + ']');
		_dropDependencies($buttons, [wsTabs.keys.myBlock, wsTabs.keys.myButtons]);
		_dropDependencies($blocks, [wsTabs.keys.myBlocks]);
	}
};

// ----------------------------------------
// Exports
// ----------------------------------------

exports.default = wsTabs;

/***/ }),
/* 52 */,
/* 53 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


// ----------------------------------------
// Imports
// ----------------------------------------

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.filtersMenu = exports.mobileMenu = exports.mainMenu = undefined;

__webpack_require__(54);

// ----------------------------------------
// Private
// ----------------------------------------

var $filtersMenu = $('#filters-menu');
var $openBnt = $('.menu-open');
var $closeBtn = $('.sidebar-close');
var $mobileOpenBtn = $('.mobile-menu-open');
var $mobileCloseBtn = $('.mobile-menu-close');
var $filtersOpenBtn = $('.filters-menu-open');
var $filtersCloseBtn = $('.filters-menu-close');

// ----------------------------------------
// Public
// ----------------------------------------

function mainMenu() {
	var $menu = $('#menu').mmenu({
		'slidingSubmenus': false,
		'extensions': ['fx-menu-slide', 'border-none', 'pagedim-black'],
		'navbar': {
			add: false
		}
	});

	var API = $menu.data('mmenu');

	$openBnt.on('click', function () {
		API.open();
	});

	$closeBtn.click(function () {
		API.close();
	});
}

function mobileMenu() {
	// if ($(window).width() > 1024) {
	// 	return false;
	// }

	var $menu = $('#mobile-menu').mmenu({
		'slidingSubmenus': false,
		'extensions': ['border-none', 'pagedim-black', 'fx-menu-slide'],
		'navbar': {
			add: false
		}
	});

	var API = $menu.data('mmenu');

	$mobileOpenBtn.on('click', function () {
		API.open();
	});

	$mobileCloseBtn.click(function () {
		API.close();
	});
}

function filtersMenu() {
	if (!$filtersMenu.length) {
		return false;
	}

	if ($(window).width() > 1024) {
		return false;
	}

	var $menu = $filtersMenu.mmenu({
		'slidingSubmenus': false,
		'extensions': ['border-none', 'pagedim-black', 'position-front'],
		'navbar': {
			add: false
		}
	});
	$menu.append($('.mobile__summary'));

	var API = $menu.data('mmenu');

	$filtersOpenBtn.on('click', function () {
		API.open();
	});

	$filtersCloseBtn.click(function () {
		API.close();
	});
}

// ----------------------------------------
// Exports
// ----------------------------------------

exports.mainMenu = mainMenu;
exports.mobileMenu = mobileMenu;
exports.filtersMenu = filtersMenu;

/***/ }),
/* 54 */,
/* 55 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


// ----------------------------------------
// Private
// ----------------------------------------

Object.defineProperty(exports, "__esModule", {
	value: true
});
var header = $('.header__top'); // Меню
var scrollPrev = 0; // Предыдущее значение скролла
var $menuChildItem = $('.header-menu__item.child');
var body = $('body');

// ----------------------------------------
// Public
// ----------------------------------------

function fixedHeader() {
	body.attr('data-param', $('.header').height());

	if ($(window).width() > 1023 && body.hasClass('fx')) {
		$(window).scroll(function () {
			var scrolled = $(window).scrollTop(); // Высота скролла в px
			var firstScrollUp = false; // Параметр начала сколла вверх
			var firstScrollDown = false; // Параметр начала сколла вниз
			var catalogMenu = $('.js-catalog');
			var height = body.attr('data-param');

			// Если скроллим
			if (scrolled > 0) {
				// Если текущее значение скролла > предыдущего, т.е. скроллим вниз
				if (scrolled > scrollPrev) {
					firstScrollUp = false; // Обнуляем параметр начала скролла вверх
					// Если меню видно
					if (scrolled < header.height() + header.offset().top) {
						// Если только начали скроллить вниз
						if (firstScrollDown === false) {
							var topPosition = header.offset().top; // Фиксируем текущую позицию меню
							header.removeClass('__fixed');
							header.css({
								'top': '0px'
							});
							firstScrollDown = true;
						}

						// Если меню НЕ видно
					} else {
						// Позиционируем меню фиксированно вне экрана
						header.css({
							'top': '-' + header.height() + 'px'
						});
					}

					// Если текущее значение скролла < предыдущего, т.е. скроллим вверх
				} else {
					firstScrollDown = false; // Обнуляем параметр начала скролла вниз
					// Если меню не видно
					if (scrolled > header.offset().top) {
						// Если только начали скроллить вверх
						if (firstScrollUp === false) {
							var _topPosition = header.offset().top; // Фиксируем текущую позицию меню
							header.addClass('__fixed');
							header.css({
								'top': '0px'
							});
							firstScrollUp = true;
						}
					}
				}
				// Присваеваем текущее значение скролла предыдущему
				scrollPrev = scrolled;
			} else {
				header.removeClass('__fixed');
				header.css({
					'top': '0px'
				});
			}

			catalogMenu.css('top', header.height());

			if (header.hasClass('__fixed')) {
				body.css('padding-top', height + 'px');
			} else {
				body.css('padding-top', 0);
			}
		});
	}
}

function subMenuShow() {
	$menuChildItem.on('mouseenter', function (e) {
		var $target = $(this);
		var speed = $target.hasClass('child') ? 0 : 400;
		$('.header-menu__child-wrap').each(function () {
			$(this).stop().css('display', 'none');
		});
		$target.find('.header-menu__child-wrap').fadeIn(speed);
	}).on('mouseleave', function () {
		var $subMenu = $(this).find('.header-menu__child-wrap');
		$subMenu.stop().fadeOut(150);
	});
}

// ----------------------------------------
// Exports
// ----------------------------------------

exports.fixedHeader = fixedHeader;
exports.subMenuShow = subMenuShow;

/***/ }),
/* 56 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


// ----------------------------------------
// Imports
// ----------------------------------------

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.initRangeSlider = undefined;

__webpack_require__(57);

__webpack_require__(58);

// ----------------------------------------
// Private
// ----------------------------------------

var $rangeSlider = $('.price-slider');

// ----------------------------------------
// Public
// ----------------------------------------

function initRangeSlider() {
	if (!$rangeSlider.length) {
		return false;
	}

	$rangeSlider.each(function (item) {
		var $this = $(this);
		var $wrap = $this.closest('.filters__price-group');
		var $minCost = $wrap.find('.min-cost');
		var $maxCost = $wrap.find('.max-cost');
		var min = parseFloat($this.data('min'));
		var max = parseFloat($this.data('max'));
		var start = parseFloat($this.data('start'));
		var end = parseFloat($this.data('end'));

		var startValue = start !== undefined ? start : min;
		var endValue = end !== undefined ? end : max;

		$(this).slider({
			min: min,
			max: max,
			range: true,
			values: [startValue, endValue],

			stop: function stop(event, ui) {
				$minCost.val(ui.values[0]);
				$maxCost.val(ui.values[1]);
			},

			slide: function slide(event, ui) {
				$minCost.val(ui.values[0]);
				$maxCost.val(ui.values[1]);
			},

			change: function change(event, ui) {
				var $filterWrapper = $(this).parents('.main-filter-wrapper');
				var $counter = $('.filter-summary-wrapper');
				var $input = $(this).parents('.filters__item');
				var position = $input.offset();

				var category_id = $filterWrapper.data('category');
				var filters = {};
				$('.filter-input:checked').each(function () {
					var name = $(this).attr('name');
					if (!filters.hasOwnProperty(name)) filters[name] = [];

					var value = $(this).val();

					if (!filters[name].includes(value)) filters[name].push(value);
				});

				var $minRangeInput = $filterWrapper.find('.min-cost');
				var $maxRangeInput = $filterWrapper.find('.max-cost');
				var minRangeCost = $minRangeInput.val();
				var maxRangeCost = $maxRangeInput.val();
				var minPrice = $minRangeInput.data('min');
				var maxPrice = $maxRangeInput.data('max');
				if (minPrice < minRangeCost || maxPrice > maxRangeCost) {
					filters['mincost'] = minRangeCost;
					filters['maxcost'] = maxRangeCost;
				}

				$.ajax({
					type: 'post',
					url: '/ajax/prepareFilter',
					data: {
						category_id: category_id,
						filters: filters
					},
					dataType: 'json',
					success: function success(response) {
						if (response.success) {
							$counter.find('.count').text(response.count);
							$counter.find('.summary-submit').data('url', response.url);

							if ($(window).width() > 1023) {
								$counter.css({
									'left': parseInt(position.left + $input.width()) - $counter.width() / 2,
									'top': position.top,
									'display': 'block'
								});
							}
						}
					}
				});
			}
		});

		$minCost.change(function () {
			var minValue = $(this).val();
			var maxValue = $maxCost.val();

			if (parseInt(minValue) > parseInt(maxValue)) {
				minValue = maxValue;
				$minCost.val(minValue);
			}

			$rangeSlider.slider('values', 0, minValue);
		});

		$maxCost.change(function () {
			var minValue = $minCost.val();
			var maxValue = $(this).val();

			if (maxValue > max) {
				maxValue = max;
				$maxCost.val(max);
			}

			if (parseInt(minValue) > parseInt(maxValue)) {
				maxValue = minValue;
				$maxCost.val(maxValue);
			}

			$rangeSlider.slider('values', 1, maxValue);
		});

		$minCost.keypress(function (event) {
			var key = void 0;
			var keyChar = void 0;

			if (event.keyCode) {
				key = event.keyCode;
			} else if (event.which) {
				key = event.which;
			}

			if (key === null || key === 0 || key === 8 || key === 13 || key === 9 || key === 46 || key === 37 || key === 39) {
				return true;
			}
			keyChar = String.fromCharCode(key);

			if (!/^[0-9]+$/.test(keyChar)) {
				return false;
			}
		});

		$maxCost.keypress(function (event) {
			var key = void 0;
			var keyChar = void 0;

			if (event.keyCode) {
				key = event.keyCode;
			} else if (event.which) {
				key = event.which;
			}

			if (key === null || key === 0 || key === 8 || key === 13 || key === 9 || key === 46 || key === 37 || key === 39) {
				return true;
			}
			keyChar = String.fromCharCode(key);

			if (!/^[0-9]+$/.test(keyChar)) {
				return false;
			}
		});
	});
}

// ----------------------------------------
// Exports
// ----------------------------------------

exports.initRangeSlider = initRangeSlider;

/***/ }),
/* 57 */,
/* 58 */,
/* 59 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


// ----------------------------------------
// Public
// ----------------------------------------

Object.defineProperty(exports, "__esModule", {
	value: true
});
function subMenuToggle(item, wrap, slide, btn) {
	var $wrap = void 0;
	var $openWrap = $(wrap + '.open');
	var $this = void 0;

	if ($openWrap) {
		$openWrap.find(slide).slideDown('fast');
	}

	if (btn) {
		$(item).find(btn).click(function (e) {
			$this = $(this);
			$wrap = $this.closest($(wrap));

			if ($wrap.find(slide).length) {
				e.preventDefault();

				$wrap.find(slide).slideToggle('fast');
				$wrap.toggleClass('open');
				$this.closest(item).toggleClass('open');
			}
		});
	} else {
		$(item).click(function (e) {
			$this = $(this);
			$wrap = $this.closest($(wrap));

			if ($wrap.find(slide).length) {
				e.preventDefault();

				$wrap.find(slide).slideToggle('fast');
				$wrap.toggleClass('open');
				$this.toggleClass('open');
			}
		});
	}
}

// ----------------------------------------
// Exports
// ----------------------------------------

exports.subMenuToggle = subMenuToggle;

/***/ }),
/* 60 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


// ----------------------------------------
// Imports
// ----------------------------------------

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.toggleDeliveryInfo = exports.headerPhonesMob = exports.scrollTop = exports.likeAdd = exports.dislikeAdd = exports.productCounter = exports.loadMore = exports.toggleCartInfo = exports.addToCompare = exports.addToFavorite = undefined;

var _Preloader = __webpack_require__(1);

var _Preloader2 = _interopRequireDefault(_Preloader);

var _moduleLoader = __webpack_require__(2);

var _moduleLoader2 = _interopRequireDefault(_moduleLoader);

__webpack_require__(15);

var _sliders = __webpack_require__(5);

var _select = __webpack_require__(7);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// ----------------------------------------
// Private
// ----------------------------------------

var $cartInfoRadio = $('.cart-info__radio');
var $cartDeliveryContent = $('.cart-info__delivery-content');
// let $productMinus = $('.js-product-minus');
// let $productPlus = $('.js-product-plus');

// Thousand separator

function setThousands(numberText, newSeparator, separator) {
	newSeparator = newSeparator || '.';
	separator = separator || '.';
	numberText = '' + numberText;
	numberText = numberText.split(separator);

	var numberPenny = numberText[1] || '';
	var numberValue = numberText[0];

	var thousandsValue = [];
	var counter = 0;

	for (var i = numberValue.length - 1; i >= 0; i--) {
		var num = numberValue[i];
		thousandsValue.push(num);

		if (++counter === 3 && i) {
			thousandsValue.push(' ');
			counter = 0;
		}
	}

	thousandsValue = thousandsValue.reverse().join('');
	if (numberPenny.length) {
		return [thousandsValue, numberPenny].join(newSeparator);
	}
	return thousandsValue;
}

function toggle(that) {
	var _this = that;

	$cartDeliveryContent.slideUp('fast');

	if ($(_this).prop('checked')) {
		var $content = $(_this).parent().next('.cart-info__delivery-content');

		$content.slideDown('fast');
	}
}

function counterUpdate($item, val) {
	var $input = $item.find('.js-product-input');
	var $count = $item.find('.js-product-count');
	var $countItem = $item.find('.js-product-count-item');
	var $priceAll = $item.find('.js-product-price');
	var $countAll = $item.find('.js-product-count-all');

	var wholesale = $item.hasClass('wholesale');
	var maxCountAll = parseInt($input.data('max'));
	var countOldAll = $.isNumeric($input.val()) ? parseInt($input.val()) : 1;
	var cost = parseFloat($priceAll.data('price'));
	var count = 0;
	var countNewAll = countOldAll;

	$('.js-product-plus').on('click', function () {
		var thisPlus = $(this).siblings('.js-product-input');

		if (thisPlus.val() == thisPlus.data('max')) {
			var maxNoty = $('.maximum-noty');
			maxNoty.show();

			setTimeout(function () {
				maxNoty.hide();
			}, 2000);
		}
	});

	if ($count.length) {
		var minCountAll = wholesale && $count.length ? $count.length : 1;
		var counts = [];

		$count.each(function () {
			var $countItem = $(this);
			var countOld = parseInt($countItem.text());
			var maxCount = parseInt($countItem.data('max'));
			var countNew = countOld;

			if (val === 'plus') {
				countNew = countOld < maxCount ? countOld + 1 : maxCount;
			} else if (val === 'minus') {
				countNew = countOld > 1 ? countOld - 1 : maxCount ? 1 : 0;
			} else {
				countNew = countOldAll > maxCount ? maxCount : countOldAll < 1 ? 1 : countOldAll;
			}

			count += countNew;

			counts.push(countNew + '');

			$countItem.html(countNew ? countNew : ''); // eslint-disable-line no-unneeded-ternary
		});

		$countItem.each(function () {
			var $countItem = $(this);
			var param = $countItem.data('param');
			param.param.counts = counts;
			$countItem.data('param', param);
		});

		if (val === 'plus') {
			countNewAll = countOldAll < maxCountAll ? countOldAll + 1 : maxCountAll;
		} else if (val === 'minus') {
			countNewAll = countOldAll > 1 ? countOldAll - 1 : minCountAll;
		} else {
			countNewAll = countOldAll > maxCountAll ? maxCountAll : countOldAll < 1 ? minCountAll : countOldAll;
		}

		$input.val(count);

		$item.data('counts', counts);
		$item.data('count', countNewAll);

		$priceAll.html(setThousands((count * cost).toFixed(2)));
		$countAll.html(count);
	} else {
		if (wholesale) {
			$item.closest('form').find('.js-product-input').each(function () {
				var $th = $(this);

				var maxCountItem = parseInt($th.data('max'));
				var countNewItem = countOldAll;
				var countNow = $th.val();

				if (val === 'plus') {
					countNewItem = ++countNow < maxCountItem ? countNow : maxCountItem;
				} else if (val === 'minus') {
					countNewItem = --countNow > 0 ? countNow : 0;
				} else {
					countNewItem = countOldAll > maxCountItem ? maxCountItem : countOldAll < 0 ? 0 : countOldAll;
				}

				$th.val(countNewItem);
			});
		}

		if (val === 'plus') {
			countNewAll = countOldAll < maxCountAll ? countOldAll + 1 : maxCountAll;
		} else if (val === 'minus') {
			countNewAll = countOldAll > 0 ? countOldAll - 1 : 0;
		} else {
			countNewAll = countOldAll > maxCountAll ? maxCountAll : countOldAll < 0 ? 0 : countOldAll;
		}

		$input.val(countNewAll);

		$item.data('counts', countNewAll);
		$item.data('count', countNewAll);

		$priceAll.html(setThousands((countNewAll * cost).toFixed(2)));
		$countAll.html(countNewAll);
	}

	if ($input.val() > 0) {
		$('body').on('click', '.js-accept', function () {
			setTimeout(function () {
				$('.js-accept').closest('.sizes-popup').find('.mfp-close').trigger('click');
				$('.header-cart__btn.js-cart-open').trigger('click');
			}, 1000);
		});
	}
}

$('body').on('click change', '.js-form-trigger', function () {
	var $this = $(this);
	var $item = $this.closest('.js-product-item');
	var $itemInput = '.js-product-input';

	if ($this.hasClass('js-product-minus')) {
		counterUpdate($item, 'minus');
		$this.closest('form').submit();
	}

	if ($this.hasClass('js-product-plus')) {

		if ($this.siblings($itemInput).val() == $this.siblings($itemInput).data('max')) {

			var maxNoty = $('.maximum-noty');
			maxNoty.show();

			counterUpdate($item, false);

			setTimeout(function () {
				maxNoty.hide();
			}, 2000);
		} else {
			counterUpdate($item, 'plus');
			$this.closest('form').submit();
		}
	}
});

$('.child-creation__button').on('click', function () {
	var wrapper = $('.child-selection__wrap');
	var btn = $('.child-creation__button');

	if (wrapper.hasClass('active')) {
		wrapper.removeClass('active');
		wrapper.slideUp(500);
		btn.css('border-color', '#a1a1a1');
	} else {
		wrapper.addClass('active');
		wrapper.slideDown(500);
		btn.css('border-color', '#f8d4cd');

		(0, _select.initSelect)();

		$('.selection-show').find('select').each(function (i, e) {
			$(e).select2({
				minimumResultsForSearch: Infinity,
				dropdownParent: $(e).closest('.mfp-container'),
				width: 'resolve',
				language: $('html').attr('lang'),
				positionDropdown: false
			});
		});
	}
});

$.magnificPopup.instance.close = function () {
	$('.child-selection__wrap').removeClass('active');
	$('.child-creation__button').css('border-color', '#a1a1a1');
	$.magnificPopup.proto.close.call(this);
};

$('body').on('click', '.child-selection__tab', function () {
	var contentId = $(this).attr('data-id');
	var content = $(this).parents('.child-selection__wrap').find("[data-content-id='" + contentId + "']");

	$(this).parents('.child-selection__gender').find('.active').removeClass('active');
	$(this).addClass('active');
	$('.child-age').removeClass('is-active');
	content.addClass('is-active');

	(0, _select.initSelect)();

	$('.selection-show').find('select').each(function (i, e) {
		$(e).select2({
			minimumResultsForSearch: Infinity,
			dropdownParent: $(e).closest('.mfp-container'),
			width: 'resolve',
			language: $('html').attr('lang'),
			positionDropdown: false
		});
	});
});

$('body').on('change', '.child-selection__content', function (e) {
	e.preventDefault();

	var $filterWrapper = $(this).parents('.section').find('.main-filter-wrapper');

	if ($(window).width() < 1024) {
		$filterWrapper = $(this).parents('body').find('.catalog__filter--mobile');
	}

	var category_id = $filterWrapper.data('category');
	var filters = {};

	$filterWrapper.find('.filter-input:checked').each(function () {
		var name = $(this).attr('name');
		if (!filters.hasOwnProperty(name)) filters[name] = [];

		var value = $(this).val();

		if (!filters[name].includes(value)) filters[name].push(value);
	});

	var $minRangeInput = $filterWrapper.find('.min-cost');
	var $maxRangeInput = $filterWrapper.find('.max-cost');
	var minRangeCost = $minRangeInput.val();
	var maxRangeCost = $maxRangeInput.val();
	var minPrice = $minRangeInput.data('min');
	var maxPrice = $maxRangeInput.data('max');
	if (minPrice < minRangeCost || maxPrice > maxRangeCost) {
		filters['mincost'] = minRangeCost;
		filters['maxcost'] = maxRangeCost;
	}

	var sex = $('.child-selection__tab.active').attr('data-value');
	var age = $('.child-age.is-active').find('.age-selection').val();

	filters['polrebenka'] = [sex];
	filters['age'] = [age];

	var search = null;
	var params = new URLSearchParams(window.location.search);
	if (params.has('search')) search = params.get('search');

	$.ajax({
		type: 'post',
		url: '/ajax/prepareFilter',
		data: {
			category_id: category_id,
			filters: filters,
			search: search
		},
		dataType: 'json',
		success: function success(response) {
			if (response.success && response.url) {
				window.location.href = response.url;
			}
		}
	});
});

$('.second__block').on('click', function () {
	var parent = $(this).parents('.delivery-info__item');
	var content = parent.find('.delivery-info__content');
	var span = parent.find('.open_delivery span');

	if (content.hasClass('none__block')) {
		content.removeClass('none__block');
		content.slideDown(500);
		$(this).addClass('opened');
		span.html('-');
	} else {
		content.addClass('none__block');
		content.slideUp(500);
		$(this).removeClass('opened');
		span.html('+');
	}
});

$('.filters__item').on('click', 'a', function (event) {

	if (!$(this).hasClass('filters__child-name')) {
		event.preventDefault();
		if ($(this).hasClass('active')) {
			$(this).removeClass('active');
		} else {
			$(this).addClass('active');
		}

		var $filterWrapper = $(this).parents('.main-filter-wrapper');
		var $counter = $('.filter-summary-wrapper');
		var $input = $(this).parents('.filters__item');
		var position = $input.offset();

		var category_id = $filterWrapper.data('category');
		var filters = {};

		$filterWrapper.find('.active').each(function () {
			var name = $(this).attr('data-type');
			if (!filters.hasOwnProperty(name)) filters[name] = [];

			var value = $(this).attr('data-value');

			if (!filters[name].includes(value)) filters[name].push(value);
		});

		var $minRangeInput = $filterWrapper.find('.min-cost');
		var $maxRangeInput = $filterWrapper.find('.max-cost');
		var minRangeCost = $minRangeInput.val();
		var maxRangeCost = $maxRangeInput.val();
		var minPrice = $minRangeInput.data('min');
		var maxPrice = $maxRangeInput.data('max');
		if (minPrice < minRangeCost || maxPrice > maxRangeCost) {
			filters['mincost'] = minRangeCost;
			filters['maxcost'] = maxRangeCost;
		}

		var search = null;
		var params = new URLSearchParams(window.location.search);
		if (params.has('search')) search = params.get('search');

		if ($.isEmptyObject(filters) && !$('.catalog__chosen-item').length) {
			$counter.css('display', 'none');
			return;
		}

		$.ajax({
			type: 'post',
			url: '/ajax/prepareFilter',
			data: {
				category_id: category_id,
				filters: filters,
				search: search
			},
			dataType: 'json',
			success: function success(response) {
				if (response.success) {
					$counter.find('.count').text(response.count);
					$counter.find('.summary-submit').attr('data-url', response.url);

					if ($(window).width() > 1023) {
						$counter.css({
							'left': parseInt(position.left + $input.width()) - $counter.width() / 2,
							'top': position.top,
							'display': 'block'
						});
					}
				}
			}
		});
	}
});

$('.summary-submit').on('click', function () {
	location.href = $(this).data('url');
});

$('select[name=per-page]').on('change', function () {
	var defaultLimit = $(this).data('default_limit');
	var queryParams = new URLSearchParams(location.search);
	var count = $(this).val();

	if (count != defaultLimit) {
		queryParams.set('per_page', count);
	} else {
		queryParams.delete('per_page');
	}

	var query = '?' + queryParams.toString();

	$('.js-load-more').data('limit', count);
	location.href = $(this).data('base') + query;
});

function productMinus(e) {
	var $this = $(this);
	var $item = $this.closest('.js-product-item');

	counterUpdate($item, 'minus');
}

function productPlus(e) {
	var $this = $(this);
	var $item = $this.closest('.js-product-item');

	counterUpdate($item, 'plus');
}

function productInput(e) {
	var $this = $(this);
	var $item = $this.closest('.js-product-item');

	counterUpdate($item);

	$this.keypress(function (event) {
		var key = void 0;
		var keyChar = void 0;

		if (event.keyCode) {
			key = event.keyCode;
		} else if (event.which) {
			key = event.which;
		}

		if (key === null || key === 0 || key === 8 || key === 13 || key === 9 || key === 46 || key === 37 || key === 39) {
			return true;
		}
		keyChar = String.fromCharCode(key);

		if (!/^[0-9]+$/.test(keyChar)) {
			return false;
		}
	});
}

function productClean(e) {
	var $this = $(this);
	var $item = $this.closest('.js-product-item');
	var wholesale = $item.hasClass('wholesale');
	var $all = $this.closest('.js-product-all');
	var $input = $item.find('.js-product-input');

	if ($this.hasClass('all')) {
		var _$input = $all.find('.js-product-input');

		_$input.each(function (item) {
			$(this).val(wholesale ? 1 : 0);
		});
	} else {
		$input.val(wholesale ? 1 : 0);
	}
}

// ----------------------------------------
// Public
// ----------------------------------------

function addToFavorite() {
	$('body').on('click', '.js-favorite-btn', function () {
		var $this = $(this);
		var $item = $this.closest('.js-cart-item');
		var id = $item.data('id');
		var url = $this.data('url');

		$.ajax({
			url: url,
			type: 'post',
			data: {
				id: id
			},
			dataType: 'json',
			success: function success(response) {
				if (response.success) {
					$('.js-favorite-btn').each(function () {
						var $th = $(this);
						var $it = $th.closest('.js-cart-item');
						if ($it.data('id') === $item.data('id')) {
							if (response.add) {
								$th.addClass('active');
							} else if (response.remove) {
								$th.removeClass('active');
							}
						}
					});

					$('.js-favorite-count').text(response.count);
				}
			}
		});
	});
}

function addToCompare() {
	$('body').on('click', '.js-compare-btn', function () {
		var $this = $(this);
		var $item = $this.closest('.js-cart-item');
		var id = $item.data('id');
		var url = $this.data('url');

		$.ajax({
			url: url,
			type: 'post',
			data: {
				id: id
			},
			dataType: 'json',
			success: function success(response) {
				if (response.success) {
					$('.js-compare-btn').each(function () {
						var $th = $(this);
						var $it = $th.closest('.js-cart-item');
						if ($it.data('id') === $item.data('id')) {
							if (response.add) {
								$th.addClass('active');
							} else if (response.remove) {
								$th.removeClass('active');
							}
						}
					});

					$('.js-compare-count').text(response.count);
				}
			}
		});
	});
}

function likeAdd() {
	$('body').on('click', '.js-review-like', function () {
		var $this = $(this);
		var $item = $this.closest('.js-review');
		var id = $item.data('id');
		var url = $this.data('url');
		var $dislike = $item.find('.js-review-dislike');
		var $counter = $this.find('.js-like-count');
		var $dislikeCounter = $item.find('.js-dislike-count');

		$.ajax({
			url: url,
			type: 'post',
			data: {
				id: id
			},
			dataType: 'json',
			success: function success(response) {
				if (response.success) {
					if (response.add) {
						$dislike.removeClass('active');
						$this.addClass('active');
					} else if (response.remove) {
						$this.removeClass('active');
					}

					$counter.text(response.likeCount);
					$dislikeCounter.text(response.dislikeCount);
				}
			}
		});
	});
}

function dislikeAdd() {
	$('body').on('click', '.js-review-dislike', function () {
		var $this = $(this);
		var $item = $this.closest('.js-review');
		var id = $item.data('id');
		var url = $this.data('url');
		var $like = $item.find('.js-review-like');
		var $counter = $this.find('.js-dislike-count');
		var $likeCounter = $item.find('.js-like-count');

		$.ajax({
			url: url,
			type: 'post',
			data: {
				id: id
			},
			dataType: 'json',
			success: function success(response) {
				if (response.success) {
					if (response.add) {
						$like.removeClass('active');
						$this.addClass('active');
					} else if (response.remove) {
						$this.removeClass('active');
					}

					$counter.text(response.dislikeCount);
					$likeCounter.text(response.likeCount);
				}
			}
		});
	});
}

function toggleCartInfo() {
	$cartInfoRadio.each(function (index) {
		if (!$(this).prop('checked')) {
			var $content = $(this).parent().next('.cart-info__delivery-content');

			$content.slideUp('fast');
		}
	});

	$cartInfoRadio.on('change', function () {
		toggle(this);
	});
}

function loadMore() {
	var $loadMoreWrap = $('.js-load-wrap');
	var preloader = new _Preloader2.default($loadMoreWrap);

	$('.js-load-more').click(function () {

		var $this = $(this);
		var url = $this.data('url');
		var page = $this.data('page');
		var limit = $this.data('limit');
		var params = $this.data('params');

		var startPage = $(".pagination__item.active").first().attr('data-page');
		var finishPage = $(".pagination__item.active").last().attr('data-page');

		$.cookie('page_numberFinish', ++finishPage);

		if (!$.cookie('page_numberStart') || $.cookie('page_numberStart') === 'null') {
			$.cookie('page_numberStart', startPage);
		}

		$this.prop('disabled', true);

		$.ajax({
			type: 'post',
			url: url,
			data: {
				page: page,
				limit: limit,
				params: params
			},
			dataType: 'json',
			success: function success(response) {
				if (response.success) {
					$this.data('page', response.page);

					$loadMoreWrap.append(response.html);

					if (response.pagination) {
						$('.js-pagination-container').html(response.pagination);
					}

					if (response.last) {
						$this.hide();
					}
				}

				setTimeout(function () {
					$this.prop('disabled', false);
				}, 500);

				_moduleLoader2.default.init($(document));

				(0, _sliders.productImgSlider)();
			}
		});
	});
}

// function cartHolderSize () {
// 	let bottom = $('.cart-window__bottom').outerHeight();
// 	let content = $('.cart-window__content').outerHeight();
// 	let top = $('.cart-window__top').outerHeight();
// 	let holder = $('.cart-holder').outerHeight();
// }

function productCounter() {
	var $container = $('body');
	$container.on('click', '.js-product-minus', productMinus);
	$container.on('click', '.js-product-plus', productPlus);
	$container.on('keyup', '.js-product-input', productInput);
	$container.on('click', '.js-product-clean', productClean);
}

function scrollTop() {
	var $footer = $('.footer');
	var $footerOffsetTop = $footer.offset().top - $footer.height();
	var $trigger = $('.toTop-js');
	var activeClass = 'active';

	$(window).on('scroll', function () {
		var $offsetTop = $(document).scrollTop();

		if ($offsetTop > $(window).height() / 3) {
			$trigger.addClass(activeClass);
		} else {
			$trigger.removeClass(activeClass);
		}
		if ($offsetTop > $footerOffsetTop) {
			$trigger.removeClass(activeClass);
		}
	});

	$trigger.on("click", function () {
		$('html, body').animate({ scrollTop: 0 }, 600, 'linear');
	});
}

function headerPhonesMob() {
	var $wrap = $('.header-phones__wrap');
	var $trigger = $('.header-phones__svg');

	$trigger.on('click', function () {
		$wrap.addClass('active');
	});

	$(document).mouseup(function (e) {
		if (!$wrap.is(e.target) && $wrap.has(e.target).length === 0) {
			$wrap.removeClass('active');;
		}
	});
}

function toggleDeliveryInfo() {
	var $trigger = $('.js-delivery-trigger');
	var $wrap = $('.js-delivery-item');
	var $text = $('.js-delivery-text');

	$trigger.on('click', function () {
		$(this).toggleClass('rotate');
		$(this).closest($wrap).find($text).slideToggle(300);
	});
}
// ----------------------------------------
// Exports
// ----------------------------------------

exports.addToFavorite = addToFavorite;
exports.addToCompare = addToCompare;
exports.toggleCartInfo = toggleCartInfo;
exports.loadMore = loadMore;
exports.productCounter = productCounter;
exports.dislikeAdd = dislikeAdd;
exports.likeAdd = likeAdd;
exports.scrollTop = scrollTop;
exports.headerPhonesMob = headerPhonesMob;
exports.toggleDeliveryInfo = toggleDeliveryInfo;

/***/ }),
/* 61 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


// ----------------------------------------
// Private
// ----------------------------------------

Object.defineProperty(exports, "__esModule", {
	value: true
});
var $map = $('.map');
var $clickTarget = $('.contacts-item__address-box');
var icon = '/Media/assets/css/static/pic/marker-icon.png';
var mapStyle = [{
	'featureType': 'water',
	'elementType': 'geometry.fill',
	'stylers': [{
		'color': '#d3d3d3'
	}]
}, {
	'featureType': 'transit',
	'stylers': [{
		'color': '#808080'
	}, {
		'visibility': 'off'
	}]
}, {
	'featureType': 'road.highway',
	'elementType': 'geometry.stroke',
	'stylers': [{
		'visibility': 'on'
	}, {
		'color': '#b3b3b3'
	}]
}, {
	'featureType': 'road.highway',
	'elementType': 'geometry.fill',
	'stylers': [{
		'color': '#ffffff'
	}]
}, {
	'featureType': 'road.local',
	'elementType': 'geometry.fill',
	'stylers': [{
		'visibility': 'on'
	}, {
		'color': '#ffffff'
	}, {
		'weight': 1.8
	}]
}, {
	'featureType': 'road.local',
	'elementType': 'geometry.stroke',
	'stylers': [{
		'color': '#d7d7d7'
	}]
}, {
	'featureType': 'poi',
	'elementType': 'geometry.fill',
	'stylers': [{
		'visibility': 'on'
	}, {
		'color': '#ebebeb'
	}]
}, {
	'featureType': 'administrative',
	'elementType': 'geometry',
	'stylers': [{
		'color': '#a7a7a7'
	}]
}, {
	'featureType': 'road.arterial',
	'elementType': 'geometry.fill',
	'stylers': [{
		'color': '#ffffff'
	}]
}, {
	'featureType': 'road.arterial',
	'elementType': 'geometry.fill',
	'stylers': [{
		'color': '#ffffff'
	}]
}, {
	'featureType': 'landscape',
	'elementType': 'geometry.fill',
	'stylers': [{
		'visibility': 'on'
	}, {
		'color': '#efefef'
	}]
}, {
	'featureType': 'road',
	'elementType': 'labels.text.fill',
	'stylers': [{
		'color': '#696969'
	}]
}, {
	'featureType': 'administrative',
	'elementType': 'labels.text.fill',
	'stylers': [{
		'visibility': 'on'
	}, {
		'color': '#737373'
	}]
}, {
	'featureType': 'poi',
	'elementType': 'labels.icon',
	'stylers': [{
		'visibility': 'off'
	}]
}, {
	'featureType': 'poi',
	'elementType': 'labels',
	'stylers': [{
		'visibility': 'off'
	}]
}, {
	'featureType': 'road.arterial',
	'elementType': 'geometry.stroke',
	'stylers': [{
		'color': '#d6d6d6'
	}]
}, {
	'featureType': 'road',
	'elementType': 'labels.icon',
	'stylers': [{
		'visibility': 'off'
	}]
}, {}, {
	'featureType': 'poi',
	'elementType': 'geometry.fill',
	'stylers': [{
		'color': '#dadada'
	}]
}];

function setPlace(latitude, longitude) {
	var place = new google.maps.LatLng(latitude, longitude); // eslint-disable-line no-undef

	return place;
}

// ----------------------------------------
// Public
// ----------------------------------------

function initMap() {
	if (!$map.length) {
		return false;
	}

	var location = {
		latitude: '',
		longitude: ''
	};

	$clickTarget.on('click', function () {
		var $mapWrap = $(this).closest('.contacts-item__top-wrap').next('.contacts-item__bottom');
		var $mapTop = $(this).closest('.contacts-item__top-wrap');
		var _map = $mapWrap.find('.map');
		var index = $mapTop.data('index');

		$mapWrap.slideToggle('fast');
		$mapTop.toggleClass('open');

		location = {
			latitude: _map.data('lat'),
			longitude: _map.data('lng')
		};

		var map = new google.maps.Map(document.getElementsByClassName('map')[index], { // eslint-disable-line no-undef
			zoom: 15,
			center: setPlace(location.latitude, location.longitude),
			scrollwheel: false,
			styles: mapStyle
		});

		var marker = new google.maps.Marker({ // eslint-disable-line no-undef, no-unused-vars
			position: setPlace(location.latitude, location.longitude),
			map: map,
			optimized: false,
			icon: icon,
			title: ''
		});
	});
}

// ----------------------------------------
// Exports
// ----------------------------------------

exports.initMap = initMap;

/***/ }),
/* 62 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


// ----------------------------------------
// Imports
// ----------------------------------------

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.cartDefaultClose = exports.cartInit = undefined;

var _Preloader = __webpack_require__(1);

var _Preloader2 = _interopRequireDefault(_Preloader);

var _moduleLoader = __webpack_require__(2);

var _moduleLoader2 = _interopRequireDefault(_moduleLoader);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// ----------------------------------------
// Private
// ----------------------------------------

var openCartBtn = '.js-cart-open';
var closeCartBtn = '.js-cart-close';
var cartWrap = '.cart-wrap';
var $cartInner = $('.cart-inner');
var magnificPopup = $.magnificPopup.instance;
var url = '/Views/Widgets/Popup/Thank.php';

function showMessage(message) {
	if (magnificPopup.isOpen) {
		$.magnificPopup.close();
	}

	$.magnificPopup.open({
		items: {
			src: url
		},
		type: 'ajax',
		removalDelay: 300,
		mainClass: 'zoom-in',
		closeMarkup: '<button class=\'mfp-close\' id=\'custom-close-btn\'></button>',
		callbacks: {
			ajaxContentAdded: function ajaxContentAdded() {
				$('.thank-popup__text').html(message);
			}
		}
	});
}

function toggle(block, value, speed) {
	$(block).animate({
		right: value
	}, speed);
}

function openCart() {
	toggle(cartWrap, '0px', 450);
	setTimeout(function () {
		$(cartWrap).addClass('open');
	}, 500);
	setTimeout(function () {
		$('body').addClass('overflow');
	}, 500);
	_moduleLoader2.default.init($(cartWrap));
}

function closeCart() {
	$(cartWrap).removeClass('open');
	setTimeout(function () {
		toggle(cartWrap, '-100%', 300);
	}, 500);
	$('body').removeClass('overflow');
}

function cartRemove(e) {
	var $item = $(this).closest('.js-cart-item');
	var remove = $(this).data('url');
	var $confirm = $item.find('.js-remove-confirm');
	var $confirmBtn = $confirm.find('.js-confirm');
	var $unConfirmBtn = $confirm.find('.js-unconfirm');

	if ($confirm.hasClass('is-show')) {
		$confirm.fadeOut('fast').removeClass('is-show');
		return false;
	} else {
		$confirm.fadeIn('fast').addClass('is-show');
	}

	$confirmBtn.on('click', function () {
		if (remove) {
			cartUpdate($item, 0, remove);
		} else {
			cartUpdate($item, 0);
		}
	});

	$unConfirmBtn.on('click', function () {
		$confirm.fadeOut('fast').removeClass('is-show');
	});

	$(document).click(function (e) {
		if ($confirm.hasClass('is-show')) {
			if (!$item.is(e.target) && $item.has(e.target).length === 0) {
				$confirm.fadeOut('fast').removeClass('is-show');
			}
		}
	});
}

function cartMinus(e) {
	var $this = $(this);
	var $item = $this.closest('.js-cart-item');
	var $input = $item.find('.js-cart-input');

	var text = $input.val();
	var max = $input.data('max') || 1000000;
	var count = 1;
	if ($.isNumeric(text)) {
		count = parseInt(text);
		count--;
		count = count > 1 ? count > parseInt(max) ? max : count : 1;
	}
	$input.val(count);

	cartUpdate($item, count);
}

function cartPlus(e) {
	var $this = $(this);
	var $item = $this.closest('.js-cart-item');
	var $input = $item.find('.js-cart-input');

	var text = $input.val();
	var max = $input.data('max') || 1000000;
	var count = 1;
	if ($.isNumeric(text)) {
		count = parseInt(text);
		count++;
		count = count > 1 ? count > parseInt(max) ? max : count : 1;
	}
	$input.val(count);

	cartUpdate($item, count);
}

function cartInput(e) {
	var $this = $(this);
	var $item = $this.closest('.js-cart-item');
	var $input = $item.find('.js-cart-input');

	var text = $input.val();
	var max = $input.data('max') || 1000000;
	if (!$.isNumeric(text) || parseInt(text) < 1) {
		$input.val(1);
	}
	if (parseInt(text) > parseInt(max)) {
		$input.val(max);
	}
	var count = parseInt($input.val());

	cartUpdate($item, count);
}

function cartAdd(e) {
	var $item = $(this).closest('.js-cart-item');
	var count = $item.data('count') || 1;

	cartUpdate($item, count);
}

function cartUpdate($item, count) {
	var remove = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;

	var url = remove || $item.data('url');
	var staticItem = $item.data('static');
	var sizes = $item.data('sizes');
	var counts = $item.data('counts');
	var id = $item.data('id');
	var preloaderContainer = staticItem !== undefined ? $('.js-cart-static') : $('.cart-inner');

	if (url && id) {
		var preloader = new _Preloader2.default(preloaderContainer);
		preloader.show();

		var data = {
			id: id
		};

		if (!remove) {
			data.count = count;
		}

		if (sizes !== undefined) {
			data.sizes = sizes;
		}

		if (counts !== undefined) {
			data.counts = counts;
		}

		$.ajax({
			url: url,
			type: 'post',
			data: data,
			dataType: 'json',
			success: function success(response) {
				preloader.hide();

				if (response.success) {
					if ($('.js-cart-count').length && typeof response.count !== 'undefined') {
						$('.js-cart-count').html(response.count);
					}

					if ($('.js-cart-price').length && typeof response.price !== 'undefined') {
						$('.js-cart-price').html(response.price);
					}

					if ($('.js-cart-static').length && typeof response.static !== 'undefined') {
						$('.js-cart-static').html(response.static);

						_moduleLoader2.default.init($('.js-cart-static'));

						if (!response.count) {
							$('[data-total]').addClass('_hide');
							$('[data-total-empty]').removeClass('_hide');
						}
					}

					if ($('.js-cart').length && typeof response.html !== 'undefined') {
						var $html = $(response.html);
						$('.js-cart').html($html);

						_moduleLoader2.default.init($('.js-cart'));

						if (!staticItem) {
							openCart();
						}
					}

					if (response.price) {
						$('.header-cart__summ span b').text(response.price);
					} else {
						$('.header-cart__summ span b').text(0);
					}
				} else {
					if (response.response) {
						showMessage(response.response);
					}
				}
			}
		});
	}
}

function cartClean(e) {
	var url = $(this).data('url');
	var $item = $(this).closest('.cart-window__top');

	var $confirm = $item.find('.js-remove-confirm');
	var $confirmBtn = $confirm.find('.js-confirm');
	var $unConfirmBtn = $confirm.find('.js-unconfirm');

	if ($confirm.hasClass('is-show')) {
		$confirm.fadeOut('fast').removeClass('is-show');
		return false;
	} else {
		$confirm.fadeIn('fast').addClass('is-show');
	}

	$confirmBtn.on('click', function () {
		if (url) {
			$.ajax({
				url: url,
				type: 'post',
				dataType: 'json',
				success: function success(response) {
					if (response.success) {
						if ($('.js-cart-count').length && typeof response.count !== 'undefined') {
							$('.js-cart-count').html(response.count);
						}

						if ($('.js-cart-price').length && typeof response.price !== 'undefined') {
							$('.js-cart-price').html(response.price);
						}

						if ($('.js-cart-static').length && typeof response.static !== 'undefined') {
							$('.js-cart-static').html(response.static);

							_moduleLoader2.default.init($('.js-cart-static'));

							if (!response.count) {
								$('[data-total]').addClass('_hide');
								$('[data-total-empty]').removeClass('_hide');
							}
						}

						if ($('.js-cart').length && typeof response.html !== 'undefined') {
							var $html = $(response.html);
							$('.js-cart').html($html);

							_moduleLoader2.default.init($('.js-cart'));

							openCart();
						}

						if (response.totalPrice) {
							$('.js-total-price').text(response.totalPrice.toFixed(2));
						}
					} else {
						if (response.response) {
							showMessage(response.response);
						}
					}
				}
			});
		}
	});

	$unConfirmBtn.on('click', function () {
		$confirm.fadeOut('fast').removeClass('is-show');
	});

	$(document).click(function (e) {
		if ($confirm.hasClass('is-show')) {
			if (!$item.is(e.target) && $item.has(e.target).length === 0) {
				$confirm.fadeOut('fast').removeClass('is-show');
			}
		}
	});
}

function cartShow(target) {
	var url = $(this).data('url');
	var data = {};
	var bigPopup = $('.sizes-big-popup').length;

	if (url) {
		$.ajax({
			url: url,
			type: 'post',
			data: data,
			dataType: 'json',
			success: function success(response) {
				if (response.success) {
					if ($('.js-cart-count').length && typeof response.count !== 'undefined') {
						$('.js-cart-count').html(response.count);
					}

					if ($('.js-cart-price').length && typeof response.price !== 'undefined') {
						$('.js-cart-price').html(response.price);
					}

					if ($('.js-cart-static').length && typeof response.static !== 'undefined') {
						$('.js-cart-static').html(response.static);

						_moduleLoader2.default.init($('.js-cart-static'));

						if (!response.count) {
							$('[data-total]').addClass('_hide');
							$('[data-total-empty]').removeClass('_hide');
						}
					}

					if ($('.js-cart').length && typeof response.html !== 'undefined') {
						var $html = $(response.html);
						$('.js-cart').html($html);

						_moduleLoader2.default.init($('.js-cart'));

						if (!bigPopup) {
							openCart();
						}
					}

					if (response.price) {
						$('.header-cart__summ span b').text(response.price);
					}
				} else {
					if (response.response) {
						showMessage(response.response);
					}
				}
			}
		});
	}
}

function cartOrder(e) {
	var url = $(this).data('url');
	var $item = $(this).closest('.js-cart-item');
	var id = $item.data('id');
	var count = $item.data('count') !== undefined ? $item.data('count') : 1;

	var data = {
		id: id,
		count: count
	};

	$.ajax({
		url: url,
		type: 'post',
		data: data,
		dataType: 'json',
		success: function success(response) {
			if (response.success) {
				if ($('.js-cart-count').length && typeof response.count !== 'undefined') {
					$('.js-cart-count').html(response.count);
				}

				if ($('.js-cart-price').length && typeof response.price !== 'undefined') {
					$('.js-cart-price').html(response.price);
				}

				if ($('.js-cart-static').length && typeof response.static !== 'undefined') {
					$('.js-cart-static').html(response.static);

					_moduleLoader2.default.init($('.js-cart-static'));

					if (!response.count) {
						$('[data-total]').addClass('_hide');
						$('[data-total-empty]').removeClass('_hide');
					}
				}

				if ($('.js-cart').length && typeof response.html !== 'undefined') {
					var $html = $(response.html);
					$('.js-cart').html($html);

					_moduleLoader2.default.init($('.js-cart'));

					openCart();
				}

				if (response.totalPrice) {
					$('.js-total-price').text(response.totalPrice.toFixed(2));
				}
			} else {
				if (response.response) {
					showMessage(response.response);
				}
			}
		}
	});
}

// ----------------------------------------
// Public
// ----------------------------------------

var cartInit = function cartInit() {
	var $container = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : $('body');

	$container.on('click.cart', '.js-cart-remove', cartRemove);
	$container.on('click.cart', '.js-cart-minus', cartMinus);
	$container.on('click.cart', '.js-cart-plus', cartPlus);
	$container.on('keyup.cart', '.js-cart-input', cartInput);
	$container.on('click.cart', '.js-cart-add', cartAdd);
	$container.on('click.cart', '.js-cart-clean', cartClean);
	$container.on('click', '.js-cart-redact', cartShow);
	$container.on('click', '.js-cart-order', cartOrder);
	$container.on('click', openCartBtn, cartShow);
	$container.on('click', closeCartBtn, closeCart);
};

function cartDefaultClose() {
	$(document).click(function (e) {
		if ($(cartWrap).hasClass('open')) {
			if (!$cartInner.is(e.target) && $cartInner.has(e.target).length === 0 && !$('.mfp-container').is(e.target) && $('.mfp-container').has(e.target).length === 0) {
				closeCart();
			}
		}
	});
}

// ----------------------------------------
// Exports
// ----------------------------------------

exports.cartInit = cartInit;
exports.cartDefaultClose = cartDefaultClose;

/***/ }),
/* 63 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


// ----------------------------------------
// Public
// ----------------------------------------

Object.defineProperty(exports, "__esModule", {
	value: true
});
function selectWord($selector, selectClass, word) {
	$selector = $selector || $('.js-select');
	selectClass = selectClass || 'is-select';
	word = word || $('.js-select-word').data('select');

	if ($selector.length && word) {
		$selector.each(function () {
			var $this = $(this);
			var html = $this.html();
			$this.html(html.replace(new RegExp(word, 'ig'), '<span class="' + selectClass + '">$&</span>'));
		});
	}
}

// ----------------------------------------
// Exports
// ----------------------------------------

exports.selectWord = selectWord;

/***/ }),
/* 64 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


// ----------------------------------------
// Private
// ----------------------------------------

Object.defineProperty(exports, "__esModule", {
	value: true
});
var $header = $('.header'); // Меню
var $headerSearch = $('.header__search-wrap');
var $headerSearchResult = $('.header__search-results');
var $headerSearchEmpty = $('.header__search-empty');
var $headerSearchInput = $('.header__search-input');
var searchUrl = $headerSearch.data('url');
var top = $header.outerHeight();

// ----------------------------------------
// Public
// ----------------------------------------

function headerSearchToggle() {
	$('body').on('click', '.header-search__btn', function () {
		$headerSearch.css({
			'top': top + 'px',
			'height': 'calc(100vh - ' + top + 'px)'
		});
		$(this).toggleClass('open');

		setTimeout(function () {
			$('.header__search-input').focus();
		}, 500);

		$headerSearch.slideToggle('fast');

		if ($header.hasClass('open')) {
			$header.toggleClass('open');
			$headerSearchInput.val('');
			$headerSearchResult.removeClass('is-show');
		} else {
			setTimeout(function () {
				$header.toggleClass('open');
			}, 400);
		}
	});
}

function searchAutocomplete() {
	if (!$headerSearchInput.length) {
		return false;
	}

	$headerSearchInput.on('keyup', function (e) {
		var $this = $(this);
		var text = $this.val();

		if (text.length >= 2) {
			$.ajax({
				url: searchUrl,
				data: text,
				dataType: 'json',
				success: function success(response) {
					if (response.success) {
						var html = '';
						if (response.items.length > 0) {
							for (var i = 0; i < response.items.length; i++) {
								html += '<div class="search-result__item">' + (response.items[i].img ? '<img class="search-result__img" src="' + response.items[i].img + '">' : '') + '<div class="search-result__info">' + (response.items[i].code ? '<span class="search-result__code">\u041A\u043E\u0434 \u0442\u043E\u0432\u0430\u0440\u0430: <span class="dark">' + response.items[i].code + '</span> </span>' : '') + '<a class="search-result__name" href="' + response.items[i].href + '">' + response.items[i].name + '</a>' + (response.items[i].price ? '<span class="search-result__price">' + response.items[i].price + '</span>' : '') + '</div>' + '</div>';
							}
							$('.header__search-results .mCSB_container').html(html);
							$headerSearchResult.addClass('is-show');
							$headerSearchEmpty.removeClass('is-show');
						} else {
							$headerSearchEmpty.addClass('is-show');
							$headerSearchResult.removeClass('is-show');
						}
					}
					if (response.error) {
						console.log('error');
					}
				}
			});
		} else {
			$headerSearchResult.removeClass('is-show');
			$headerSearchEmpty.removeClass('is-show');
		}
	});
}

// ----------------------------------------
// Exports
// ----------------------------------------

exports.headerSearchToggle = headerSearchToggle;
exports.searchAutocomplete = searchAutocomplete;

/***/ }),
/* 65 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


// ----------------------------------------
// Imports
// ----------------------------------------

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.productMedia = undefined;

__webpack_require__(66);

// ----------------------------------------
// Private
// ----------------------------------------

var threesixtyFlag = false;
var $productImage = $('.product__gallery');
var $product360 = $('.product__gallery-360');

function threesixtyInit() {
	$('.js-threesixty').each(function (index, el) {
		var $this = $(el);
		var totalFrames = $this.attr('data-totalFrames') ? parseInt($this.attr('data-totalFrames')) : 180;
		var imagePath = $this.attr('data-imagePath') ? $this.attr('data-imagePath') : 'images/360/';
		var ext = $this.attr('data-ext') ? $this.attr('data-ext') : '.png';
		var filePrefix = $this.attr('data-filePrefix') ? $this.attr('data-filePrefix') : '';

		var ts = $this.ThreeSixty({
			currentFrame: 1,
			imgList: '.threesixty__images',
			progress: '.threesixty__spinner',
			navigation: false,
			totalFrames: totalFrames,
			// endFrame: endFrame,
			imagePath: imagePath,
			ext: ext,
			filePrefix: filePrefix,
			speedMultiplier: -7,
			// autoplayDirection: -1,
			onReady: function onReady() {
				$('.js-threesixty-buttons').addClass('is-show');

				$this.find('.js-threesixty-next').on('click', function (e) {
					ts.next();
				});
				$this.find('.js-threesixty-prev').on('click', function (e) {
					ts.previous();
				});
				$this.find('.js-threesixty-play').on('click', function (e) {
					ts.play();
				});
				$this.find('.js-threesixty-pause').on('click', function (e) {
					ts.stop();
				});
			}
		});
	});
}

// ----------------------------------------
// Public
// ----------------------------------------

function productMedia() {
	var $mediaLink = $('.js-product-media-link');

	$mediaLink.on('click', function () {
		if (!threesixtyFlag) {
			threesixtyInit();
			threesixtyFlag = true;
		}

		if ($mediaLink.hasClass('is-image')) {
			$mediaLink.removeClass('is-image');
			$productImage.addClass('is-hide');
			$product360.removeClass('is-hide');
		} else {
			$mediaLink.addClass('is-image');
			$productImage.removeClass('is-hide');
			$product360.addClass('is-hide');
		}
	});
}

// ----------------------------------------
// Exports
// ----------------------------------------

exports.productMedia = productMedia;

/***/ }),
/* 66 */,
/* 67 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


// ----------------------------------------
// Imports
// ----------------------------------------

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.paginationInit = undefined;

var _Preloader = __webpack_require__(1);

var _Preloader2 = _interopRequireDefault(_Preloader);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// Public
// ----------------------------------------

function paginationInit() {
	var $wrap = $('.product__reviews');
	var preloader = new _Preloader2.default($wrap);

	$('body').on('click', '[data-pagination-item]', function () {
		var $this = $(this);
		var page = $this.data('page');
		var $pagination = $this.closest('.js-pagination');
		var url = $pagination.data('url');
		var id = $pagination.data('id');
		preloader.show();

		$.ajax({
			url: url,
			type: 'post',
			data: {
				page: page,
				id: id
			},
			dataType: 'json',
			success: function success(response) {
				if (response.success) {
					preloader.hide();
					$('[data-pagination-item]').removeClass('active');
					$this.addClass('active');
					$wrap.html(response.html);
				}
			}
		});
	});
}

// ----------------------------------------
// Exports
// ----------------------------------------

exports.paginationInit = paginationInit;

/***/ }),
/* 68 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


// Public
// ----------------------------------------

Object.defineProperty(exports, "__esModule", {
    value: true
});
function catalogSelect() {
    var $selectWrap = $('.catalog__items-select-box');
    var $select = $selectWrap.find('.select2');

    $select.on('select2:select', function (e) {
        var $this = $(this);
        var val = $this.val();
        if (val) {
            window.location.href = val;
        }
    });
}

function catalogMenu() {
    $(window).on('load', function () {
        if (screen.width > 1024) {

            var headerHeight = $('.header__top').height();

            $(window).scroll(function () {
                headerHeight = $('.header__top').height();
            });

            var $trigger = $('.js-catalog-button');
            var $catalog = $('.js-catalog');

            $trigger.on('click', function () {
                $catalog.css({ 'min-height': 'calc(100vh - ' + headerHeight + 'px)', 'top': headerHeight + 'px' });
                $('.catalog').append('<div class="lightbox"></div>');
                $('body').addClass('overflow-hidden');
                $catalog.fadeIn();
            });

            $(document).mouseup(function (e) {
                if (!$catalog.is(e.target) && $catalog.has(e.target).length === 0) {
                    $('.lightbox').remove();
                    $catalog.fadeOut();
                    $('body').removeClass('overflow-hidden');
                }
            });
        } else {
            var _$trigger = $('.js-mob-menu');
            var _$catalog = $('.js-mob-catalog');
            var $headerTop = $('.header__top-content--mob');

            _$trigger.click(function () {
                $(this).toggleClass('active');
                $headerTop.toggleClass('active');
                _$catalog.fadeToggle();
            });
        }
    });
}

function catalogMenuItem() {
    $(window).on('load', function () {
        if (screen.width > 1024) {
            var headerHeight = $('.header').height();
            var $trigger = $('[data-catalog-item]');
            var $catalog = $('.js-catalog-category');
            var catalogSub = $('[data-sub]');

            $trigger.click(function () {
                var targetCategory = $(this).data('catalog-item');

                $(this).addClass('active').siblings().removeClass('active');
                catalogSub.fadeOut();
                $('.catalog').append('<div class="lightbox-category"></div>');
                $catalog.css({ 'min-height': 'calc(100vh - ' + headerHeight + 'px)' }).fadeIn();
                $('.catalog-menu__sub[data-sub=' + targetCategory + ']').fadeIn();
                return false;
            });

            $(document).mouseup(function (e) {
                if (!$catalog.is(e.target) && $catalog.has(e.target).length === 0) {
                    $('.lightbox-category').remove();
                    $trigger.removeClass('active');
                    $catalog.fadeOut();
                }
            });
        } else {
            var $catalogItem = $('[data-catalog-item]');
            var $catalogItemChildren = $('.catalog__item-children');
            var $backBtn = $('.catalog__item-back');

            $catalogItem.on('click', function () {
                $(this).siblings($catalogItemChildren).addClass('active');
            });

            $backBtn.on('click', function () {
                $(this).closest($catalogItemChildren).removeClass('active');
            });
        }
    });
}

function catalogClose() {
    var $trigger = $('.js-close');
    var $catalog = $('.catalog-menu');

    $trigger.click(function () {
        $catalog.fadeOut();
        $('.lightbox').remove();
        $('.lightbox-category').remove();
        $('body').removeClass('overflow-hidden');
    });
}

// ----------------------------------------
// Exports
// ----------------------------------------

exports.catalogSelect = catalogSelect;
exports.catalogMenu = catalogMenu;
exports.catalogMenuItem = catalogMenuItem;
exports.catalogClose = catalogClose;

/***/ }),
/* 69 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


// ----------------------------------------
// Imports
// ----------------------------------------

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _Preloader = __webpack_require__(1);

var _Preloader2 = _interopRequireDefault(_Preloader);

var _select = __webpack_require__(7);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// ----------------------------------------
// Public
// ----------------------------------------

function chooseAjax() {
	var $content = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : $('body');

	var $target = $content.find('.js-choose-ajax');

	function sendAjax($this, $container) {
		var url = $this.data('url');
		var name = $this.attr('name') || $this.data('name');
		var value = $this.val() || $this.data('val');
		var preloader = new _Preloader2.default($container);

		if (url && name) {
			preloader.show();

			$.ajax({
				url: url,
				type: 'post',
				data: {
					name: name,
					value: value
				},
				dataType: 'json',
				success: function success(response) {
					if (response.success) {
						if (response.items) {
							response.items.forEach(function (item) {
								if (item.html && item.selector) {
									var $html = $(item.html);
									var $selector = $(item.selector);

									if ($selector.length) {
										var $select2 = $container.find('select.select2');

										$select2.each(function () {
											var $th = $(this);
											if ($th.data('select2')) {
												$th.select2('destroy');
											}
										});

										$selector.html($html);

										$select2.each(function () {
											var $th = $(this).parent();
											(0, _select.initSelect)($th);
										});

										chooseAjax($html);
									}
								}
							});
						}
					}

					preloader.hide();
				}
			});
		}
	}

	if ($target.length) {
		$target.each(function () {
			var $this = $(this);
			var $container = $this.closest('.js-choose-ajax-container');
			var tagName = this.tagName.toLowerCase();

			if ($this.data('select2')) {
				$this.on('select2:select', function (e) {
					sendAjax($this, $container);
				});
			} else if (tagName === 'select' || tagName === 'input' && ($this.attr('type') === 'radio' || $this.attr('type') === 'checkbox')) {
				$this.on('change', function (e) {
					sendAjax($this, $container);
				});
			} else if (tagName === 'textarea' || tagName === 'input' && $this.attr('type') !== 'radio' && $this.attr('type') !== 'checkbox') {
				$this.on('keyup', function (e) {
					sendAjax($this, $container);
				});
			} else {
				$this.on('click', function (e) {
					sendAjax($this, $container);
				});
			}
		});
	}
}

// ----------------------------------------
// Exports
// ----------------------------------------

exports.default = chooseAjax;

/***/ }),
/* 70 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


// ----------------------------------------
// Imports
// ----------------------------------------

Object.defineProperty(exports, "__esModule", {
	value: true
});

__webpack_require__(14);

var _validateInit = __webpack_require__(4);

var _validateInit2 = _interopRequireDefault(_validateInit);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// ----------------------------------------
// Private
// ----------------------------------------

var selectSelector = '.ajaxSelect2';

function initAjaxSelect2() {
	var $container = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : $('body');

	$container.find(selectSelector).each(function (index) {
		var $this = $(this);

		$this.select2({
			ajax: {
				url: $this.data('url'),
				data: function data(params) {
					var query = {
						search: params.term,
						cityRef: $this.data['city']
					};

					return query;
				}
			}
		});
	});
}

// ----------------------------------------
// Exports
// ----------------------------------------

exports.default = initAjaxSelect2;

/***/ }),
/* 71 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


// ----------------------------------------
// Imports
// ----------------------------------------

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _notyExtend = __webpack_require__(72);

var _notyExtend2 = _interopRequireDefault(_notyExtend);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// ----------------------------------------
// Public
// ----------------------------------------

function messageInit() {
	window.generate = function (message, type, time) {
		var noty = new _notyExtend2.default({
			type: type === 'success' ? 'info' : 'error',
			text: message,
			timeout: time
		});
		noty.show();
	};
}

// ----------------------------------------
// Exports
// ----------------------------------------

exports.default = messageInit;

/***/ }),
/* 72 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 *
 * @module
 */

// ----------------------------------------
// Imports
// ----------------------------------------

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _noty = __webpack_require__(73);

var _noty2 = _interopRequireDefault(_noty);

__webpack_require__(74);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// ----------------------------------------
// Private
// ----------------------------------------

_noty2.default.overrideDefaults({
	theme: 'mint',
	layout: 'bottomRight',
	timeout: 5000,
	progressBar: true,
	closeWith: ['click']
});

// ----------------------------------------
// Exports
// ----------------------------------------

exports.default = _noty2.default;

/***/ }),
/* 73 */,
/* 74 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(75);
if(typeof content === 'string') content = [[module.i, content, '']];
// Prepare cssTransformation
var transform;

var options = {"insertAt":{"before":"#webpack-style-loader-insert-before-this"},"hmr":true}
options.transform = transform
// add the styles to the DOM
var update = __webpack_require__(10)(content, options);
if(content.locals) module.exports = content.locals;
// Hot Module Replacement
if(false) {
	// When the styles change, update the <style> tags
	if(!content.locals) {
		module.hot.accept("!!../../../../../node_modules/css-loader/index.js??ref--2-1!../../../../../node_modules/postcss-loader/lib/index.js??ref--2-2!../../../../../node_modules/sass-loader/lib/loader.js??ref--2-3!./noty-extend.scss", function() {
			var newContent = require("!!../../../../../node_modules/css-loader/index.js??ref--2-1!../../../../../node_modules/postcss-loader/lib/index.js??ref--2-2!../../../../../node_modules/sass-loader/lib/loader.js??ref--2-3!./noty-extend.scss");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
	}
	// When the module is disposed, remove the <style> tags
	module.hot.dispose(function() { update(); });
}

/***/ }),
/* 75 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(9)(false);
// imports


// module
exports.push([module.i, ".noty_layout_mixin, #noty_layout__top, #noty_layout__topLeft, #noty_layout__topCenter, #noty_layout__topRight, #noty_layout__bottom, #noty_layout__bottomLeft, #noty_layout__bottomCenter, #noty_layout__bottomRight, #noty_layout__center, #noty_layout__centerLeft, #noty_layout__centerRight {\n  position: fixed;\n  margin: 0;\n  padding: 0;\n  z-index: 9999999;\n  transform: translateZ(0) scale(1, 1);\n  -webkit-backface-visibility: hidden;\n  backface-visibility: hidden;\n  -webkit-font-smoothing: subpixel-antialiased;\n  filter: blur(0);\n  -webkit-filter: blur(0);\n  max-width: 90%; }\n\n#noty_layout__top {\n  top: 0;\n  left: 5%;\n  width: 90%; }\n\n#noty_layout__topLeft {\n  top: 20px;\n  left: 20px;\n  width: 325px; }\n\n#noty_layout__topCenter {\n  top: 5%;\n  left: 50%;\n  width: 325px;\n  transform: translate(calc(-50% - .5px)) translateZ(0) scale(1, 1); }\n\n#noty_layout__topRight {\n  top: 20px;\n  right: 20px;\n  width: 325px; }\n\n#noty_layout__bottom {\n  bottom: 0;\n  left: 5%;\n  width: 90%; }\n\n#noty_layout__bottomLeft {\n  bottom: 20px;\n  left: 20px;\n  width: 325px; }\n\n#noty_layout__bottomCenter {\n  bottom: 5%;\n  left: 50%;\n  width: 325px;\n  transform: translate(calc(-50% - .5px)) translateZ(0) scale(1, 1); }\n\n#noty_layout__bottomRight {\n  bottom: 20px;\n  right: 20px;\n  width: 325px; }\n\n#noty_layout__center {\n  top: 50%;\n  left: 50%;\n  width: 325px;\n  transform: translate(calc(-50% - .5px), calc(-50% - .5px)) translateZ(0) scale(1, 1); }\n\n#noty_layout__centerLeft {\n  top: 50%;\n  left: 20px;\n  width: 325px;\n  transform: translate(0, calc(-50% - .5px)) translateZ(0) scale(1, 1); }\n\n#noty_layout__centerRight {\n  top: 50%;\n  right: 20px;\n  width: 325px;\n  transform: translate(0, calc(-50% - .5px)) translateZ(0) scale(1, 1); }\n\n.noty_progressbar {\n  display: none; }\n\n.noty_has_timeout.noty_has_progressbar .noty_progressbar {\n  display: block;\n  position: absolute;\n  left: 0;\n  bottom: 0;\n  height: 3px;\n  width: 100%;\n  background-color: #646464;\n  opacity: 0.2;\n  filter: alpha(opacity=10); }\n\n.noty_bar {\n  -webkit-backface-visibility: hidden;\n  transform: translate(0, 0) scale(1, 1);\n  -webkit-font-smoothing: subpixel-antialiased;\n  overflow: hidden; }\n\n.noty_effects_open {\n  opacity: 0;\n  transform: translate(50%);\n  animation: noty_anim_in 0.5s cubic-bezier(0.68, -0.55, 0.265, 1.55);\n  animation-fill-mode: forwards; }\n\n.noty_effects_close {\n  animation: noty_anim_out 0.5s cubic-bezier(0.68, -0.55, 0.265, 1.55);\n  animation-fill-mode: forwards; }\n\n.noty_fix_effects_height {\n  animation: noty_anim_height 75ms ease-out; }\n\n.noty_close_with_click {\n  cursor: pointer; }\n\n.noty_close_button {\n  position: absolute;\n  top: 2px;\n  right: 2px;\n  font-weight: bold;\n  width: 20px;\n  height: 20px;\n  text-align: center;\n  line-height: 20px;\n  background-color: rgba(0, 0, 0, 0.05);\n  border-radius: 2px;\n  cursor: pointer;\n  transition: all .2s ease-out; }\n\n.noty_close_button:hover {\n  background-color: rgba(0, 0, 0, 0.1); }\n\n.noty_modal {\n  position: fixed;\n  width: 100%;\n  height: 100%;\n  background-color: #000;\n  z-index: 10000;\n  opacity: .3;\n  left: 0;\n  top: 0; }\n\n.noty_modal.noty_modal_open {\n  opacity: 0;\n  animation: noty_modal_in .3s ease-out; }\n\n.noty_modal.noty_modal_close {\n  animation: noty_modal_out .3s ease-out;\n  animation-fill-mode: forwards; }\n\n@keyframes noty_modal_in {\n  100% {\n    opacity: .3; } }\n\n@keyframes noty_modal_out {\n  100% {\n    opacity: 0; } }\n\n@keyframes noty_anim_in {\n  100% {\n    transform: translate(0);\n    opacity: 1; } }\n\n@keyframes noty_anim_out {\n  100% {\n    transform: translate(50%);\n    opacity: 0; } }\n\n@keyframes noty_anim_height {\n  100% {\n    height: 0; } }\n\n.noty_theme__mint.noty_bar {\n  margin: 4px 0;\n  overflow: hidden;\n  border-radius: 2px;\n  position: relative; }\n  .noty_theme__mint.noty_bar .noty_body {\n    padding: 10px;\n    font-size: 14px; }\n  .noty_theme__mint.noty_bar .noty_buttons {\n    padding: 10px; }\n\n.noty_theme__mint.noty_type__alert,\n.noty_theme__mint.noty_type__notification {\n  background-color: #fff;\n  border-bottom: 1px solid #D1D1D1;\n  color: #2F2F2F; }\n\n.noty_theme__mint.noty_type__warning {\n  background-color: #FFAE42;\n  border-bottom: 1px solid #E89F3C;\n  color: #fff; }\n\n.noty_theme__mint.noty_type__error {\n  background-color: #DE636F;\n  border-bottom: 1px solid #CA5A65;\n  color: #fff; }\n\n.noty_theme__mint.noty_type__info,\n.noty_theme__mint.noty_type__information {\n  background-color: #7F7EFF;\n  border-bottom: 1px solid #7473E8;\n  color: #fff; }\n\n.noty_theme__mint.noty_type__success {\n  background-color: #AFC765;\n  border-bottom: 1px solid #A0B55C;\n  color: #fff; }\n\n.noty_theme__mint.noty_type__info, .noty_theme__mint.noty_type__information {\n  background: #28a745;\n  color: #fff;\n  border-bottom: none; }\n  .noty_theme__mint.noty_type__info.noty_has_progressbar .noty_progressbar, .noty_theme__mint.noty_type__information.noty_has_progressbar .noty_progressbar {\n    background-color: #fde52d;\n    opacity: 0.6; }\n\n.noty_theme__mint.noty_type__error {\n  background: #ed4956;\n  color: #fff;\n  border-bottom: none; }\n  .noty_theme__mint.noty_type__error.noty_has_progressbar .noty_progressbar {\n    background-color: #fde52d;\n    opacity: 0.4; }\n", ""]);

// exports


/***/ }),
/* 76 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


// ----------------------------------------
// Imports
// ----------------------------------------

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _cookieData = __webpack_require__(6);

var _cookieData2 = _interopRequireDefault(_cookieData);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// ----------------------------------------
// Public
// ----------------------------------------

function currencyInit() {
	$('.js-currency').on('click', function () {
		var currency = $(this).data('currency');
		_cookieData2.default.add('currency', currency, {
			expires: 3600 * 24 * 30,
			path: '/'
		});
		window.location.href = window.location.href;
	});
}

// ----------------------------------------
// Exports
// ----------------------------------------

exports.default = currencyInit;

/***/ }),
/* 77 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


// ----------------------------------------
// Imports
// ----------------------------------------

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _cookieData = __webpack_require__(6);

var _cookieData2 = _interopRequireDefault(_cookieData);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// ----------------------------------------
// Public
// ----------------------------------------

function userTypeInit() {
	$('.js-user-type').on('click', function () {
		var userType = $(this).data('user-type');
		_cookieData2.default.add('user-type', userType, {
			expires: 3600 * 24 * 30,
			path: '/'
		});
		window.location.href = window.location.href;
	});
}

// ----------------------------------------
// Exports
// ----------------------------------------

exports.default = userTypeInit;

/***/ }),
/* 78 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


// ----------------------------------------
// Imports
// ----------------------------------------

// ----------------------------------------
// Public
// ----------------------------------------

Object.defineProperty(exports, "__esModule", {
	value: true
});
function payToggle() {
	var wrap = $('[data-pay-wrap]');
	var inputs = wrap.find('input');
	var input = $('[data-pay-cart]');
	var info = $('[data-pay-cart-block]');
	var payType = $('.cart-info__label');

	if (wrap.length) {
		inputs.not(input).on('change', function () {
			info.slideUp(150);
		});
		input.on('change', function () {
			if (input.is(':checked')) {
				info.slideDown(150);
			}
		});

		payType.on('click', function () {
			$('.cart-info__label.is-checked').removeClass('is-checked');
			$(this).addClass('is-checked');
		});
	}
}

// ----------------------------------------
// Exports
// ----------------------------------------

exports.default = payToggle;

/***/ }),
/* 79 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


// ----------------------------------------
// Imports
// ----------------------------------------

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.default = backToCatalog;

__webpack_require__(15);

var _moduleLoader = __webpack_require__(2);

var _moduleLoader2 = _interopRequireDefault(_moduleLoader);

var _Preloader = __webpack_require__(1);

var _Preloader2 = _interopRequireDefault(_Preloader);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// ----------------------------------------
// Private
// ----------------------------------------

/**
 * @type {Object}
 * @private
 */

function backToCatalog() {
	$('.for-cookie').on('click', '.product-item', function () {
		var scroll = $(this).offset().top;
		$.cookie('page_scroll', scroll);

		var startPage = $(".pagination__item.active").first().attr('data-page');
		var finishPage = $(".pagination__item.active").last().attr('data-page');

		$.cookie('page_numberStart', startPage);
		$.cookie('page_numberFinish', ++finishPage);
	});

	if ($.cookie('page_numberFinish') > 0) {

		var $this = $('.js-load-more');
		var $loadMoreWrap = $('.js-load-wrap');
		var preloader = new _Preloader2.default($loadMoreWrap);

		var page = $.cookie('page_numberFinish');
		var params = $this.data('params');

		params.startPage = $.cookie('page_numberStart');;
		params.endPage = --page;

		var url = $this.data('backtocatalogurl');

		$.ajax({
			type: 'post',
			url: url,
			data: {
				page: page,
				params: params
			},
			dataType: 'json',
			success: function success(response) {
				if (response.success) {
					$this.data('page', response.page);

					$loadMoreWrap.append(response.html);

					if (response.pagination) {
						$('.js-pagination-container').html(response.pagination);
					}

					if (response.last) {
						$this.hide();
					}

					setTimeout(function () {
						$('html, body').animate({ scrollTop: $.cookie('page_scroll') }, 1000);
						clearCookies();
					}, 500);

					preloader.hide();
				}

				_moduleLoader2.default.init($(document));
			}
		});
	}

	$('body').on('click', '.pagination__item', function () {
		clearCookies();
	});

	function clearCookies() {
		$.cookie('page_numberStart', null);
		$.cookie('page_numberFinish', null);
		$.cookie('page_scroll', null);
	}
};

/***/ }),
/* 80 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


// ----------------------------------------
// Imports
// ----------------------------------------


// ----------------------------------------
// Public
// ----------------------------------------

Object.defineProperty(exports, "__esModule", {
	value: true
});
function dellChild() {
	$("body").on('click', '.js-dellInfo', function (event) {
		event.preventDefault();
		var id = $(this).closest('.form-group').find('input[data-dell-id]').data('dell-id');
		var action = $(this).closest('.form-group').data('ajax');

		$.ajax({
			url: action,
			type: 'POST',
			dataType: 'JSON',
			data: {
				id: id
			},
			success: function success(data) {
				if (data.success) {
					if (data.response) {
						generate(data.response, 'success');
					}
					if (data.reload) {
						window.location.reload();
					} else {
						preloader();
					}
				} else {
					if (data.response) {
						generate(data.response, 'warning');
					}
					preloader();
				}
			}
		});
	});
}

// ----------------------------------------
// Exports
// ----------------------------------------

exports.dellChild = dellChild;

/***/ }),
/* 81 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


// Public
// ----------------------------------------

Object.defineProperty(exports, "__esModule", {
	value: true
});
function changeSizeInTable() {
	$('body').on('click', '.table-size__popup .product__sizes-item', function () {
		var tabName = $(this).text();
		$('.table-size__popup .product__sizes-item').removeClass('is-active');

		$('.tab').hide();
		$(this).addClass('is-active');

		$('.tab-' + tabName).show();
	});
}

// ----------------------------------------
// Exports
// ----------------------------------------

exports.changeSizeInTable = changeSizeInTable;

/***/ }),
/* 82 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


// ----------------------------------------
// Imports
// ----------------------------------------

// ----------------------------------------
// Private
// ----------------------------------------

// ----------------------------------------
// Public
// ----------------------------------------

Object.defineProperty(exports, "__esModule", {
	value: true
});
function clickMe() {
	if ($('.click_me').length) {
		setTimeout(function () {
			$('.click_me').trigger('click');
		}, 1000);
	}

	$('body').on('click', '.open__callback', function () {
		$('.header-phones__callback').trigger('click');
	});
}

// ----------------------------------------
// Exports
// ----------------------------------------

exports.clickMe = clickMe;

/***/ }),
/* 83 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


// ----------------------------------------
// Imports
// ----------------------------------------


// ----------------------------------------
// Public
// ----------------------------------------

Object.defineProperty(exports, "__esModule", {
	value: true
});
function deliveryRegistration() {
	var $container = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : $('body');

	$container.find('.scroll-box').each(function () {});

	var rozn = $('input[name="rozn"]'),
	    opt = $('input[name="opt"]'),
	    drop = $('input[name="drop"]'),
	    noReg = $('input[name="no-profile"]');

	function checkReq(input) {
		rozn.prop('required', false);

		if (!rozn.prop('checked') && !opt.prop('checked') && !drop.prop('checked')) {
			rozn.prop('required', true);
		}
	}

	rozn.on('click', function () {
		checkReq();
		opt.prop('checked', false);
		drop.prop('checked', false);
	});

	opt.on('click', function () {
		checkReq();
		rozn.prop('checked', false);
	});

	drop.on('click', function () {
		checkReq();
		rozn.prop('checked', false);
	});

	noReg.on('click', function () {
		rozn.prop('checked', false);
		opt.prop('checked', false);
		drop.prop('checked', false);

		if ($(this).prop('checked')) {
			$('.cart-info__item').find('.cart-registration__status').hide();
		} else {
			$('.cart-info__item').find('.cart-registration__status').show();
		}
	});

	if (noReg.prop('checked')) {
		rozn.prop('checked', false);
		opt.prop('checked', false);
		drop.prop('checked', false);
		$('.cart-info__item').find('.cart-registration__status').hide();
	}
}

// ----------------------------------------
// Exports
// ----------------------------------------

exports.deliveryRegistration = deliveryRegistration;

/***/ }),
/* 84 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


// ----------------------------------------
// Imports
// ----------------------------------------

Object.defineProperty(exports, "__esModule", {
		value: true
});
exports.exportTable = undefined;

var _setCheckers = __webpack_require__(85);

var _setCheckers2 = _interopRequireDefault(_setCheckers);

var _panelWithBtns = __webpack_require__(86);

var _panelWithBtns2 = _interopRequireDefault(_panelWithBtns);

var _listToggle = __webpack_require__(87);

var _listToggle2 = _interopRequireDefault(_listToggle);

var _productsDownload = __webpack_require__(88);

var _productsDownload2 = _interopRequireDefault(_productsDownload);

var _priceFromTo = __webpack_require__(89);

var _priceFromTo2 = _interopRequireDefault(_priceFromTo);

var _priceFromClient = __webpack_require__(90);

var _priceFromClient2 = _interopRequireDefault(_priceFromClient);

var _infoBlock = __webpack_require__(91);

var _infoBlock2 = _interopRequireDefault(_infoBlock);

var _ajaxFilter = __webpack_require__(92);

var _ajaxFilter2 = _interopRequireDefault(_ajaxFilter);

var _save = __webpack_require__(93);

var _save2 = _interopRequireDefault(_save);

var _copyLink = __webpack_require__(94);

var _copyLink2 = _interopRequireDefault(_copyLink);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// ----------------------------------------
// Public
// ----------------------------------------

function exportTable() {
		if ($('[data-export-table]').length) {

				// Простановка чекеров + отправка id категории
				(0, _setCheckers2.default)();

				// Панель кнопок для чекеров
				(0, _panelWithBtns2.default)();

				// Разворачивание списков
				(0, _listToggle2.default)();

				// Подгрузка товаров при расскрытии списка с применением фильтра
				(0, _productsDownload2.default)();

				// Слайдер цены от и до
				(0, _priceFromTo2.default)();

				// Слайдер цен клиента
				(0, _priceFromClient2.default)();

				// Info блок
				(0, _infoBlock2.default)();

				// Ajax на фильтр
				(0, _ajaxFilter2.default)();

				// Сохранение
				(0, _save2.default)();

				// Копирование URL
				(0, _copyLink2.default)();
		}
}

// ----------------------------------------
// Exports
// ----------------------------------------

exports.exportTable = exportTable;

/***/ }),
/* 85 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 *
 * @module
 */

// ----------------------------------------
// Imports
// ----------------------------------------

// ----------------------------------------
// Private
// ----------------------------------------

/**
 * @param {JQuery} $element
 * @return {string}
 * @private
 */

// ----------------------------------------
// Public
// ----------------------------------------

/**
 * @param {JQuery} $elements
 */

Object.defineProperty(exports, "__esModule", {
	value: true
});
function setCheckers() {
	jQuery.fn.reverse = [].reverse;

	$("body").on('click', 'input:checkbox', function () {
		var $this = $(this),
		    isChecked = $this.is(":checked"),
		    url = $('.js-click-product-categories').data('click'),
		    categoriesId = [],
		    sort = $('#sort').val(),
		    availability = $('.export__availability input').prop('checked'),
		    amount = $('#amount').val();

		$this.parent().next().find("input:checkbox").prop("checked", isChecked);

		$this.parents("ul").prev("a").find("input:checkbox").reverse().each(function (a, b) {
			$(b).prop("checked", function () {
				return $(b).parent("a").next("ul").find(":checked").length;
			});
		});

		// Отправка id категории при checketed
		if ($this.is(':checked')) {
			$this.closest('li').find('.js-product-show').each(function () {
				categoriesId.push($(this).data('id'));
			});

			$.ajax({
				url: url,
				type: 'post',
				data: {
					categoriesId: categoriesId,
					sort: sort,
					availability: availability,
					amount: amount,
					checkedCategories: true
				},
				dataType: 'json'
			});
		} else {
			$this.closest('li').find('.js-product-show').each(function () {
				categoriesId.push($(this).data('id'));
			});

			$.ajax({
				url: url,
				type: 'post',
				data: {
					categoriesId: categoriesId,
					checkedCategories: false
				},
				dataType: 'json'
			});
		}
	});

	// Отправка id товара
	$('body').on('click', '.js-product-in-table', function () {
		var $this = $(this),
		    url = $('.js-click-product-categories').data('click'),
		    productId = [],
		    sort = $('#sort').val(),
		    availability = $('.export__availability input').prop('checked'),
		    amount = $('#amount').val();

		if ($this.is(':checked')) {
			productId.push($this.data('id'));

			$.ajax({
				url: url,
				type: 'post',
				data: {
					sort: sort,
					availability: availability,
					amount: amount,
					productId: productId,
					checkedProduct: true
				},
				dataType: 'json'
			});
		} else {
			productId.push($this.data('id'));

			$.ajax({
				url: url,
				type: 'post',
				data: {
					productId: productId,
					checkedProduct: false
				},
				dataType: 'json'
			});
		}
	});

	$("body").on('click', 'input:checkbox', function (e) {
		var $this = $(this),
		    level = $this.closest('.nesting-level').data('level'),
		    allInputs = $this.closest('.nesting-level').find('li input').length,
		    checkedInputs = $this.closest('.nesting-level').find('li input:checked').length;

		if (level === 1) {
			if ($this.is(':checked')) {
				$this.attr('data-notAll', true).closest('.nesting-level[data-level="1"]').find('input').attr('data-notAll', true);
			}
		} else if (level === 2) {
			if (allInputs === checkedInputs) {
				$this.closest('.nesting-level[data-level="1"]').find('.checkbox__icon--main-category').next().find('input').attr('data-notAll', true);
			} else {
				$this.closest('.nesting-level[data-level="1"]').find('.checkbox__icon--main-category').next().find('input').attr('data-notAll', false);
			}
		} else if (level === 3) {
			if (allInputs === checkedInputs) {
				$this.closest('.nesting-level[data-level="3"]').prev().find('input').attr('data-notAll', true).closest('.nesting-level[data-level="1"]').find('.checkbox__icon--main-category').next().find('input').attr('data-notAll', true);
			} else {
				$this.closest('.nesting-level[data-level="3"]').prev().find('input').attr('data-notAll', false).closest('.nesting-level[data-level="1"]').find('.checkbox__icon--main-category').next().find('input').attr('data-notAll', false);
			}
		} else {
			if (allInputs === checkedInputs) {
				$this.closest('.nesting-level').prev().find('input').attr('data-notAll', true).closest('.nesting-level[data-level="1"]').find('.checkbox__icon--main-category').next().find('input').attr('data-notAll', true);
			} else {
				$this.closest('.nesting-level').prev().find('input').attr('data-notAll', false).closest('.nesting-level[data-level="1"]').find('.checkbox__icon--main-category').next().find('input').attr('data-notAll', false);
			}
		}
	});
}

// ----------------------------------------
// Exports
// ----------------------------------------

exports.default = setCheckers;

/***/ }),
/* 86 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 *
 * @module
 */

// ----------------------------------------
// Imports
// ----------------------------------------

// ----------------------------------------
// Private
// ----------------------------------------

/**
 * @param {JQuery} $element
 * @return {string}
 * @private
 */

// ----------------------------------------
// Public
// ----------------------------------------

/**
 * @param {JQuery} $elements
 */

Object.defineProperty(exports, "__esModule", {
	value: true
});
function panelWithBtns() {
	$('.js-set-checkers').on('click', function () {
		var action = $(this).data('set-checkers'),
		    url = $('.js-click-product-categories').data('click');

		function setCheckers(status) {
			$(".export__checkers input[type='checkbox']").each(function () {
				$(this).prop('checked', status);
			});
		}

		function toggleCheckers(status) {
			$("input[type='checkbox']:not(:checked)").each(function () {
				$(this).closest('li').css('display', status);
			});
		}

		switch (action) {
			case 'all':
				setCheckers(true);

				var checkeredEmptyCategories = [],
				    sort = $('#sort').val(),
				    availability = $('.export__availability input').prop('checked'),
				    amount = $('#amount').val();

				$('.js-product-show').each(function () {
					checkeredEmptyCategories.push($(this).next().find('input').data('id'));
				});

				$.ajax({
					url: url,
					type: 'post',
					data: {
						sort: sort,
						availability: availability,
						amount: amount,
						checkeredEmptyCategories: checkeredEmptyCategories
					},
					dataType: 'json'
				});

				break;
			case 'none':
				setCheckers(false);

				$.ajax({
					url: url,
					type: 'post',
					data: {
						dellAll: true
					},
					dataType: 'json'
				});

				break;
			case 'onlyCheck':
				toggleCheckers('none');
				break;
			default:
				toggleCheckers('block');
		}
	});
}

// ----------------------------------------
// Exports
// ----------------------------------------

exports.default = panelWithBtns;

/***/ }),
/* 87 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 *
 * @module
 */

// ----------------------------------------
// Imports
// ----------------------------------------

// ----------------------------------------
// Private
// ----------------------------------------

/**
 * @param {JQuery} $element
 * @return {string}
 * @private
 */

// ----------------------------------------
// Public
// ----------------------------------------

/**
 * @param {JQuery} $elements
 */

Object.defineProperty(exports, "__esModule", {
	value: true
});
function listToggle() {
	$('.checkbox__icon').on('click', function (e) {
		var icon = $(this);

		if (icon.hasClass('checkbox__icon--main-category opened')) {
			icon.toggleClass('opened').closest('li').find('.export__checkers').addClass('is-hide').each(function () {
				$(this).find('.checkbox__icon.opened').removeClass('opened');
			});
		} else {
			icon.toggleClass('opened').parent().children('ul').toggleClass('is-hide');
		}
	});
}

// ----------------------------------------
// Exports
// ----------------------------------------

exports.default = listToggle;

/***/ }),
/* 88 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 *
 * @module
 */

// ----------------------------------------
// Imports
// ----------------------------------------

// ----------------------------------------
// Private
// ----------------------------------------

/**
 * @param {JQuery} $element
 * @return {string}
 * @private
 */

// ----------------------------------------
// Public
// ----------------------------------------

/**
 * @param {JQuery} $elements
 */

Object.defineProperty(exports, "__esModule", {
	value: true
});
function productsDownload() {
	$('.js-product-show').on('click', function (e) {
		var $this = $(this),
		    id = $this.data('id'),
		    url = $('.export__checkers').data('url'),
		    sort = $('#sort').val(),
		    amount = $('#amount').val(),
		    availability = $('.export__availability input').prop('checked'),
		    checkeredProducts = [],
		    select = null,
		    range = [],
		    lengthAll = $this.closest('li').find('.export__checkers input').length,
		    lengthChecketed = $this.closest('li').find('.export__checkers input:checked').length;

		if ($this.next().find('input').is(':checked')) {
			// console.log('Чекнутая категория');
			select = null;

			if (lengthAll == lengthChecketed) {
				select = id;
			}
		} else {
			// console.log('Не чекнутая категория');

			select = null;
			if ($this.closest('li').find('.export__checkers').length) {
				// console.log('есть товар');
				if (lengthAll == lengthChecketed) {
					// console.log('все выбрано');
					// select = id;
				} else {
						// console.log('ничего не выбрано');
						// select = null
					}
			} else {
				// console.log('нет товров');
				select = null;
			}
		}

		// console.log(select);

		if ($(this).hasClass('opened')) {
			$this.parent().find('.export__checkers input:checked').each(function () {
				checkeredProducts.push($(this).data('id'));
			});

			$('.export__slider--client').each(function () {
				var name = $(this).find('input').attr('id'),
				    value = $(this).find('input').val();

				range.push([name, value]);
			});

			$.ajax({
				url: url,
				type: 'post',
				data: {
					category_id: id,
					sort: sort,
					availability: availability,
					amount: amount,
					select: select,
					checkeredProducts: checkeredProducts,
					range: range
				},
				dataType: 'json',
				success: function success(response) {
					if (response.success) {
						if ($this.closest('li').find('.export__checkers').length) {
							$this.closest('li').find('.export__checkers').replaceWith(response.html);
						} else {
							$this.closest('li').append(response.html);
						}
					}
				}
			});
		}
	});
}

// ----------------------------------------
// Exports
// ----------------------------------------

exports.default = productsDownload;

/***/ }),
/* 89 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 *
 * @module
 */

// ----------------------------------------
// Imports
// ----------------------------------------

// ----------------------------------------
// Private
// ----------------------------------------

/**
 * @param {JQuery} $element
 * @return {string}
 * @private
 */

// ----------------------------------------
// Public
// ----------------------------------------

/**
 * @param {JQuery} $elements
 */

Object.defineProperty(exports, "__esModule", {
	value: true
});
function priceFromTo() {
	if ($('.export__slider').length) {
		var amountSlider = $('#amount'),
		    sliderRange = $("#slider-range"),
		    min = amountSlider.data('min'),
		    max = amountSlider.data('max'),
		    currency = $('input[data-currency]').data('currency');

		sliderRange.slider({
			range: true,
			min: min,
			max: max,
			values: [min, max],
			slide: function slide(event, ui) {
				$("#amount").val(ui.values[0] + ' ' + currency + " - " + ui.values[1] + ' ' + currency);
			}
		});

		amountSlider.val(sliderRange.slider("values", 0) + ' ' + currency + " - " + sliderRange.slider("values", 1) + ' ' + currency);
	}
}

// ----------------------------------------
// Exports
// ----------------------------------------

exports.default = priceFromTo;

/***/ }),
/* 90 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 *
 * @module
 */

// ----------------------------------------
// Imports
// ----------------------------------------

// ----------------------------------------
// Private
// ----------------------------------------

/**
 * @param {JQuery} $element
 * @return {string}
 * @private
 */

// ----------------------------------------
// Public
// ----------------------------------------

/**
 * @param {JQuery} $elements
 */

Object.defineProperty(exports, "__esModule", {
	value: true
});
function priceFromClient() {
	if ($('.export__slider--client').length) {
		$('.export__slider--client').each(function () {
			var $this = $(this),
			    name = $this.find('input').attr('id'),
			    min = $('#' + name).data('min'),
			    max = $('#' + name).data('max'),
			    percent = $this.data('percent') || 0;

			$("#slider-range" + name).slider({
				min: min,
				max: max,
				values: [percent],
				slide: function slide(event, ui) {
					$("#" + name).val(ui.values[0] + "%");
				}
			});

			$("#" + name).val(percent + '%');
		});
	}
}

// ----------------------------------------
// Exports
// ----------------------------------------

exports.default = priceFromClient;

/***/ }),
/* 91 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 *
 * @module
 */

// ----------------------------------------
// Imports
// ----------------------------------------

// ----------------------------------------
// Private
// ----------------------------------------

/**
 * @param {JQuery} $element
 * @return {string}
 * @private
 */

// ----------------------------------------
// Public
// ----------------------------------------

/**
 * @param {JQuery} $elements
 */

Object.defineProperty(exports, "__esModule", {
	value: true
});
function infoBlock() {
	$('.export__info-icon').on('click', function (e) {
		var $this = $(this);

		$('.export__info-icon').next().hide();
		$this.next().toggle();
	});

	$('body').on('click', function (e) {
		var div = $('.export__info-icon');

		if (!div.is(e.target) && div.has(e.target).length === 0) {
			div.next().hide();
		}
	});
}

// ----------------------------------------
// Exports
// ----------------------------------------

exports.default = infoBlock;

/***/ }),
/* 92 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 *
 * @module
 */

// ----------------------------------------
// Imports
// ----------------------------------------

// ----------------------------------------
// Private
// ----------------------------------------

/**
 * @param {JQuery} $element
 * @return {string}
 * @private
 */

// ----------------------------------------
// Public
// ----------------------------------------

/**
 * @param {JQuery} $elements
 */

Object.defineProperty(exports, "__esModule", {
	value: true
});
function ajaxFilter() {
	$('.js-set-filter').on('click', function () {
		var url = $(this).data('url'),
		    sort = $('#sort').val(),
		    amount = $('#amount').val(),
		    availability = $('.export__availability input').prop('checked'),
		    openedCategories = [],
		    range = [],
		    checkeredProducts = [];

		$('.js-product-show.opened').parent().find('.export__product-name input:checked').each(function () {
			checkeredProducts.push($(this).data('id'));
		});

		$('.js-product-show.opened').each(function () {
			openedCategories.push($(this).next().find('input').data('id'));
		});

		$('.export__slider--client').each(function () {
			var name = $(this).find('input').attr('id'),
			    value = $(this).find('input').val();

			range.push([name, value]);
		});

		$.ajax({
			url: url,
			type: 'post',
			data: {
				sort: sort,
				availability: availability,
				amount: amount,
				checkeredProducts: checkeredProducts,
				openedCategories: openedCategories,
				range: range
			},
			dataType: 'json',

			success: function success(response) {
				if (response.success) {
					$.each(response.products, function (index, value) {
						var block = $('.js-product-show[data-id="' + index + '"]').parent();
						block.find('.export__checkers').remove();
						block.append(value);
					});

					$('.export__filter-noty').show("slow");
					setTimeout(function () {
						$('.export__filter-noty').hide("slow");
					}, 3000);

					$(".export__checkers input[type='checkbox']").not(".nesting-level--products input[type='checkbox']").each(function () {
						$(this).prop('checked', false);
					});

					$.each(response.checkedCategories, function (id, notAll) {
						$('input[data-id="' + id + '"]').prop('checked', 'checked').attr('data-notAll', !notAll);
					});

					$('[data-level="1"]').each(function () {
						var $this = $(this),
						    allInputs = $this.find('[data-level="2"] li a input').length,
						    checkedInputs = $this.find('[data-level="2"] li a input:checked').length;

						if (allInputs === checkedInputs) {
							$this.find('.checkbox__icon--main-category').next().find('input').prop('checked', 'checked').attr('data-notAll', true);
						} else if (checkedInputs > 0) {
							$this.find('.checkbox__icon--main-category').next().find('input').prop('checked', true).attr('data-notAll', false);
						} else if (checkedInputs === 0) {
							$this.find('.checkbox__icon--main-category').next().find('input').prop('checked', false).attr('data-notAll', true);
						}
					});

					$('[data-level="3"]').each(function () {
						var $this = $(this),
						    allInputs = $this.find('li a input').length,
						    checkedInputs = $this.find('li a input:checked').length;

						if (allInputs === checkedInputs) {
							$this.prev().find('input').prop('checked', 'checked').attr('data-notAll', true);
						} else if (checkedInputs > 0) {
							$this.prev().find('input').prop('checked', true).attr('data-notAll', false);
						} else if (checkedInputs === 0) {
							$this.prev().find('input').prop('checked', false).attr('data-notAll', true);
						}
					});
				}
			}
		});
	});
}

// ----------------------------------------
// Exports
// ----------------------------------------

exports.default = ajaxFilter;

/***/ }),
/* 93 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 *
 * @module
 */

// ----------------------------------------
// Imports
// ----------------------------------------

// ----------------------------------------
// Private
// ----------------------------------------

/**
 * @param {JQuery} $element
 * @return {string}
 * @private
 */

// ----------------------------------------
// Public
// ----------------------------------------

/**
 * @param {JQuery} $elements
 */

Object.defineProperty(exports, "__esModule", {
	value: true
});
function save() {
	$('.js-saveXML').on('click', function (e) {
		var url = $(this).data('url'),
		    promId = $(this).attr('data-promid'),
		    shopName = $('input[name="shopName"]').val(),
		    companyName = $('input[name="companyName"]').val(),
		    availability = $('.export__availability input').prop('checked'),
		    checkeredEmptyCategories = [],
		    products = [],
		    range = [];

		if (shopName.length > 0 && companyName.length > 0) {
			e.preventDefault();

			$('.js-product-show:not(.opened)').each(function () {
				var $this = $(this);

				if ($this.parent().children('.export__checkers').length && $this.next().find('input').is(":checked")) {
					checkeredEmptyCategories.push($this.data('id'));
				}
			});

			$('.export__table-body').each(function () {
				$(this).find('input:checked').each(function () {
					var id = $(this).data('id'),
					    newPrice = $(this).closest('.export__table-body').find('.gcell--3[data-newprice]').attr('data-newprice');

					products.push([id, newPrice]);
				});
			});

			$('.export__slider--client').each(function () {
				var name = $(this).find('input').attr('id'),
				    value = $(this).find('input').val();

				range.push([name, value]);
			});

			$.ajax({
				url: url,
				type: 'post',
				data: {
					shopName: shopName,
					promId: promId,
					availability: availability,
					range: range,
					companyName: companyName,
					checkeredEmptyCategories: checkeredEmptyCategories,
					products: products
				},
				dataType: 'json',
				beforeSend: function beforeSend() {
					$('.preloader.l-wrapper').css('display', 'flex');
				},
				success: function success(response) {
					if (response.success) {
						$('.preloader.l-wrapper').hide();

						$('.export__saved-noty').show("slow");
						setTimeout(function () {
							$('.export__saved-noty').hide("slow");
						}, 3000);

						setTimeout(function () {
							location.reload();
						}, 2500);
					}
				}
			});
		}
	});
}

// ----------------------------------------
// Exports
// ----------------------------------------

exports.default = save;

/***/ }),
/* 94 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 *
 * @module
 */

// ----------------------------------------
// Imports
// ----------------------------------------

// ----------------------------------------
// Private
// ----------------------------------------

/**
 * @param {JQuery} $element
 * @return {string}
 * @private
 */

// ----------------------------------------
// Public
// ----------------------------------------

/**
 * @param {JQuery} $elements
 */

Object.defineProperty(exports, "__esModule", {
  value: true
});
function copyLink() {
  $('.js-copy-url').on('click', function () {
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val($('input[name="url-for-file"]').val()).select();
    document.execCommand("copy");
    $temp.remove();
  });
}

// ----------------------------------------
// Exports
// ----------------------------------------

exports.default = copyLink;

/***/ })
],[28]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9fSFRNTC9zcmMvanMvX21vZHVsZXMvUHJlbG9hZGVyLmpzIiwid2VicGFjazovLy8uL19IVE1ML3NyYy9qcy9tb2R1bGUtbG9hZGVyLmpzIiwid2VicGFjazovLy8uL19IVE1ML3NyYy9qcy9fbW9kdWxlcy9qcXVlcnktdmFsaWRhdGlvbi92YWxpZGF0ZS1pbml0LmpzIiwid2VicGFjazovLy8uL19IVE1ML3NyYy9qcy9fbW9kdWxlcy9zbGlkZXJzLmpzIiwid2VicGFjazovLy8uL19IVE1ML3NyYy9qcy9fbW9kdWxlcy9jb29raWUtZGF0YS5qcyIsIndlYnBhY2s6Ly8vLi9fSFRNTC9zcmMvanMvX21vZHVsZXMvc2VsZWN0LmpzIiwid2VicGFjazovLy8uL19IVE1ML3NyYy9qcy9fbW9kdWxlcy9tYWduaWZpYy1wb3B1cC9tZnAtaW5pdC5qcyIsIndlYnBhY2s6Ly8vLi9fSFRNTC9zcmMvanMvX21vZHVsZXMvem9vbWl0LmpzIiwid2VicGFjazovLy8uL19IVE1ML3NyYy9qcy9fbW9kdWxlcy9haXJEYXRlcGlja2VyLmpzIiwid2VicGFjazovLy8uL19IVE1ML3NyYy9qcy9fbW9kdWxlcy9jcmVhdGUtdGltZXIuanMiLCJ3ZWJwYWNrOi8vLy4vX0hUTUwvc3JjL2pzL2VudHJ5LmpzIiwid2VicGFjazovLy8uL19IVE1ML3NyYy9qcy9fbW9kdWxlcy1sb2FkZXJzIGxhenkgXlxcLlxcLy4qJCIsIndlYnBhY2s6Ly8vLi9fSFRNTC9zcmMvanMvX21vZHVsZXMvanF1ZXJ5LXZhbGlkYXRpb24vZXh0ZW5kL3ZhbGlkYXRlLWV4dGVuZC5qcyIsIndlYnBhY2s6Ly8vLi9fSFRNTC9zcmMvanMvX21vZHVsZXMvanF1ZXJ5LXZhbGlkYXRpb24vZXh0ZW5kL3ZhbGlkYXRlLWNsYXNzLXJ1bGVzLmpzIiwid2VicGFjazovLy8uL19IVE1ML3NyYy9qcy9fbW9kdWxlcy9qcXVlcnktdmFsaWRhdGlvbi9leHRlbmQvdmFsaWRhdGUtd2V6b20tbWV0aG9kcy5qcyIsIndlYnBhY2s6Ly8vLi9fSFRNTC9zcmMvanMvX21vZHVsZXMvanF1ZXJ5LXZhbGlkYXRpb24vdmFsaWRhdGUtaGFuZGxlcnMuanMiLCJ3ZWJwYWNrOi8vLy4vX0hUTUwvc3JjL2pzL19tb2R1bGVzL2pxdWVyeS12YWxpZGF0aW9uL3ZhbGlkYXRlLWdldC1yZXNwb25zZS5qcyIsIndlYnBhY2s6Ly8vLi9fSFRNTC9zcmMvanMvX21vZHVsZXMvbWFnbmlmaWMtcG9wdXAvbWZwLWV4dGVuZC5qcyIsIndlYnBhY2s6Ly8vLi9fSFRNTC9zcmMvanMvX21vZHVsZXMvbWFnbmlmaWMtcG9wdXAvbWZwLWV4dGVuZC5zY3NzPzhlMDMiLCJ3ZWJwYWNrOi8vLy4vX0hUTUwvc3JjL2pzL19tb2R1bGVzL21hZ25pZmljLXBvcHVwL21mcC1leHRlbmQuc2NzcyIsIndlYnBhY2s6Ly8vLi9fSFRNTC9zcmMvanMvX21vZHVsZXMvd3N0YWJzLmpzIiwid2VicGFjazovLy8uL19IVE1ML3NyYy9qcy9fbW9kdWxlcy9tZW51LmpzIiwid2VicGFjazovLy8uL19IVE1ML3NyYy9qcy9fbW9kdWxlcy9maXhlZC1oZWFkZXIuanMiLCJ3ZWJwYWNrOi8vLy4vX0hUTUwvc3JjL2pzL19tb2R1bGVzL3JhbmdlLXNsaWRlci5qcyIsIndlYnBhY2s6Ly8vLi9fSFRNTC9zcmMvanMvX21vZHVsZXMvc3ViLW1lbnUuanMiLCJ3ZWJwYWNrOi8vLy4vX0hUTUwvc3JjL2pzL19tb2R1bGVzL2NvbW1vbi5qcyIsIndlYnBhY2s6Ly8vLi9fSFRNTC9zcmMvanMvX21vZHVsZXMvbWFwLmpzIiwid2VicGFjazovLy8uL19IVE1ML3NyYy9qcy9fbW9kdWxlcy9jYXJ0LmpzIiwid2VicGFjazovLy8uL19IVE1ML3NyYy9qcy9fbW9kdWxlcy9zZWxlY3Qtd29yZC5qcyIsIndlYnBhY2s6Ly8vLi9fSFRNTC9zcmMvanMvX21vZHVsZXMvaGVhZGVyLXNlYXJjaC5qcyIsIndlYnBhY2s6Ly8vLi9fSFRNTC9zcmMvanMvX21vZHVsZXMvcHJvZHVjdC0zNjAuanMiLCJ3ZWJwYWNrOi8vLy4vX0hUTUwvc3JjL2pzL19tb2R1bGVzL3BhZ2luYXRpb24uanMiLCJ3ZWJwYWNrOi8vLy4vX0hUTUwvc3JjL2pzL19tb2R1bGVzL2NhdGFsb2ctc2VsZWN0LmpzIiwid2VicGFjazovLy8uL19IVE1ML3NyYy9qcy9fbW9kdWxlcy9jaG9vc2UtYWpheC5qcyIsIndlYnBhY2s6Ly8vLi9fSFRNTC9zcmMvanMvX21vZHVsZXMvYWpheFNlbGVjdDIuanMiLCJ3ZWJwYWNrOi8vLy4vX0hUTUwvc3JjL2pzL19tb2R1bGVzL3Nob3ctbm90eS5qcyIsIndlYnBhY2s6Ly8vLi9fSFRNTC9zcmMvanMvX21vZHVsZXMvbm90eS9ub3R5LWV4dGVuZC5qcyIsIndlYnBhY2s6Ly8vLi9fSFRNTC9zcmMvanMvX21vZHVsZXMvbm90eS9ub3R5LWV4dGVuZC5zY3NzP2FmZTMiLCJ3ZWJwYWNrOi8vLy4vX0hUTUwvc3JjL2pzL19tb2R1bGVzL25vdHkvbm90eS1leHRlbmQuc2NzcyIsIndlYnBhY2s6Ly8vLi9fSFRNTC9zcmMvanMvX21vZHVsZXMvY3VycmVuY3kuanMiLCJ3ZWJwYWNrOi8vLy4vX0hUTUwvc3JjL2pzL19tb2R1bGVzL3VzZXItdHlwZS5qcyIsIndlYnBhY2s6Ly8vLi9fSFRNTC9zcmMvanMvX21vZHVsZXMvcGF5LWluZm8tdG9nZ2xlLmpzIiwid2VicGFjazovLy8uL19IVE1ML3NyYy9qcy9fbW9kdWxlcy9iYWNrVG9DYXRhbG9nLmpzIiwid2VicGFjazovLy8uL19IVE1ML3NyYy9qcy9fbW9kdWxlcy9kZWxsQ2hpbGQuanMiLCJ3ZWJwYWNrOi8vLy4vX0hUTUwvc3JjL2pzL19tb2R1bGVzL2NoYW5nZVNpemVJblRhYmxlLmpzIiwid2VicGFjazovLy8uL19IVE1ML3NyYy9qcy9fbW9kdWxlcy9jbGlja01lLmpzIiwid2VicGFjazovLy8uL19IVE1ML3NyYy9qcy9fbW9kdWxlcy9kZWxpdmVyeVJlZ2lzdHJhdGlvbi5qcyIsIndlYnBhY2s6Ly8vLi9fSFRNTC9zcmMvanMvX21vZHVsZXMvZXhwb3J0LXRhYmxlLmpzIiwid2VicGFjazovLy8uL19IVE1ML3NyYy9qcy9fbW9kdWxlcy9leHBvcnQtbW9kdWxlL3NldC1jaGVja2Vycy5qcyIsIndlYnBhY2s6Ly8vLi9fSFRNTC9zcmMvanMvX21vZHVsZXMvZXhwb3J0LW1vZHVsZS9wYW5lbC13aXRoLWJ0bnMuanMiLCJ3ZWJwYWNrOi8vLy4vX0hUTUwvc3JjL2pzL19tb2R1bGVzL2V4cG9ydC1tb2R1bGUvbGlzdC10b2dnbGUuanMiLCJ3ZWJwYWNrOi8vLy4vX0hUTUwvc3JjL2pzL19tb2R1bGVzL2V4cG9ydC1tb2R1bGUvcHJvZHVjdHMtZG93bmxvYWQuanMiLCJ3ZWJwYWNrOi8vLy4vX0hUTUwvc3JjL2pzL19tb2R1bGVzL2V4cG9ydC1tb2R1bGUvcHJpY2UtZnJvbS10by5qcyIsIndlYnBhY2s6Ly8vLi9fSFRNTC9zcmMvanMvX21vZHVsZXMvZXhwb3J0LW1vZHVsZS9wcmljZS1mcm9tLWNsaWVudC5qcyIsIndlYnBhY2s6Ly8vLi9fSFRNTC9zcmMvanMvX21vZHVsZXMvZXhwb3J0LW1vZHVsZS9pbmZvLWJsb2NrLmpzIiwid2VicGFjazovLy8uL19IVE1ML3NyYy9qcy9fbW9kdWxlcy9leHBvcnQtbW9kdWxlL2FqYXgtZmlsdGVyLmpzIiwid2VicGFjazovLy8uL19IVE1ML3NyYy9qcy9fbW9kdWxlcy9leHBvcnQtbW9kdWxlL3NhdmUuanMiLCJ3ZWJwYWNrOi8vLy4vX0hUTUwvc3JjL2pzL19tb2R1bGVzL2V4cG9ydC1tb2R1bGUvY29weS1saW5rLmpzIl0sIm5hbWVzIjpbImRlZmF1bHRDb25maWciLCJtYWluQ2xhc3MiLCJzdWJDbGFzcyIsInNob3dDbGFzcyIsImhpZGVDbGFzcyIsInJlbW92ZURlbGF5IiwibWFya3VwIiwiUHJlbG9hZGVyIiwiJGNvbnRhaW5lciIsInVzZXJDb25maWciLCJsZW5ndGgiLCJlbXB0eSIsImRhdGEiLCJjb25maWciLCIkIiwiZXh0ZW5kIiwidGltZXIiLCJwbGFjZSIsImNvbnNvbGUiLCJ3YXJuIiwiZWFjaCIsImkiLCJlbCIsImFkZENsYXNzIiwid2luZG93Iiwic2V0VGltZW91dCIsInJlbW92ZU1hcmt1cCIsInJlbW92ZSIsInJlbW92ZUNsYXNzIiwidmFsdWUiLCJtb2R1bGVMb2FkZXIiLCJkZWJ1ZyIsImltcG9ydFByb21pc2UiLCJtb2R1bGVOYW1lIiwiaW5pdFNlbGVjdG9yIiwiaW5pdEZ1bmN0aW9uTmFtZSIsImxvYWRpbmdDbGFzcyIsImxvYWRlZENsYXNzIiwibGlzdCIsImNvbmZpZ0RlZmF1bHQiLCJpZ25vcmUiLCJjbGFzc2VzIiwiZXJyb3IiLCJ2YWxpZCIsImxhYmVsRXJyb3IiLCJmb3JtRXJyb3IiLCJmb3JtVmFsaWQiLCJmb3JtUGVuZGluZyIsImVycm9yQ2xhc3MiLCJ2YWxpZENsYXNzIiwib25mb2N1c291dCIsImVsZW1lbnQiLCJjbGFzc0xpc3QiLCJjb250YWlucyIsInNldHRpbmdzIiwib25rZXl1cCIsImhpZ2hsaWdodCIsImFkZCIsInVuaGlnaGxpZ2h0Iiwic3VibWl0SGFuZGxlciIsImZvcm0iLCIkZm9ybSIsImFjdGlvblVybCIsInByZWxvYWRlciIsInNob3ciLCJmb3JtRGF0YSIsIkZvcm1EYXRhIiwiYXBwZW5kIiwiYXR0ciIsImlnbm9yZWRJbnB1dHNUeXBlIiwiZmluZCIsInR5cGUiLCJpbmRleE9mIiwiJGVsZW1lbnQiLCJlbGVtZW50Tm9kZSIsIm5vZGVOYW1lIiwiZWxlbWVudE5hbWUiLCJmaWxlcyIsImZpbGUiLCJjaGVja2VkIiwibXVsdGlOYW1lIiwidGVzdCIsIm11bHRpcGxlIiwic2VsZWN0ZWRPcHRpb25zIiwib3B0aW9uIiwiZGlzYWJsZWQiLCJ4aHIiLCJYTUxIdHRwUmVxdWVzdCIsIm9wZW4iLCJvbnJlYWR5c3RhdGVjaGFuZ2UiLCJyZWFkeVN0YXRlIiwic3RhdHVzIiwic3RhdHVzVGV4dCIsInJlc3BvbnNlIiwibG9nIiwiaGlkZSIsIkpTT04iLCJwYXJzZSIsImxvY2F0aW9uIiwiaG9zdCIsIiRzaXplQ291bnRCbG9jayIsInNpemUiLCJhY3RpdmVfY291bnQiLCIkc2l6ZUJ1dHRvbiIsImZpcnN0IiwidGV4dCIsInNlbmQiLCJ2YWxpZGF0ZSIsIiRlbGVtZW50cyIsImhhc0luaXRlZEtleSIsIiRtYWluU2xpZGVyIiwiJHByb2R1Y3RTbGlkZXIiLCIkZG90c0NvbnRyb2xzIiwiJGRvdHNIb2xkZXIiLCIkcHJvZHVjdFBob3RvU2xpZGVyIiwiJHByb2R1Y3ROYXZTbGlkZXIiLCJkb3RzUG9zaXRpb24iLCJ0aGF0IiwicGFkZGluZyIsIndpZHRoIiwib3V0ZXJXaWR0aCIsImRvdHNQb3NUb3AiLCJwb3NpdGlvbiIsInRvcCIsImNzcyIsInNldFBhZGRpbmciLCJoYXNDbGFzcyIsInBhcnNlSW50IiwibWFpblNsaWRlciIsIm9uIiwiZXZlbnQiLCJzbGljayIsImRpcmVjdGlvbiIsInNsaWRlc1RvU2hvdyIsInNsaWRlc1RvU2Nyb2xsIiwiaW5maW5pdGUiLCJkb3RzIiwiYXJyb3dzIiwiYXV0b3BsYXkiLCJmYWRlIiwiYXBwZW5kRG90cyIsImN1c3RvbVBhZ2luZyIsInNsaWRlciIsIm51bSIsImNvbmNhdCIsInByZXZBcnJvdyIsIm5leHRBcnJvdyIsInJlc3BvbnNpdmUiLCJicmVha3BvaW50IiwicGF1c2VPbkhvdmVyIiwicmVzaXplIiwicHJvZHVjdEltZ1NsaWRlciIsIiRwcm9kdWN0SW1nU2xpZGVyIiwibm90IiwiZSIsIiR0aGlzIiwiYXBwZW5kQXJyb3dzIiwicGFyZW50IiwicHJvZHVjdFNsaWRlciIsInNsaWRlQ291bnQiLCJwcm9kdWN0UGhvdG9TbGlkZXIiLCJwaG90b1NsaWRlciIsImFjY2Vzc2liaWxpdHkiLCJhc05hdkZvciIsImZvY3VzT25TZWxlY3QiLCJ2ZXJ0aWNhbCIsImN1cnJlbnRTbGlkZSIsIm5leHRTbGlkZSIsImVuY29kZXIiLCJlbmNvZGVVUklDb21wb25lbnQiLCJkZWNvZGVyIiwiZGVjb2RlVVJJQ29tcG9uZW50IiwiY29va2llRGF0YSIsImtleSIsIm9wdGlvbnMiLCJleHBpcmVzIiwiZCIsIkRhdGUiLCJzZXRUaW1lIiwiZ2V0VGltZSIsInRvVVRDU3RyaW5nIiwidXBkYXRlZENvb2tpZSIsInByb3BOYW1lIiwicHJvcFZhbHVlIiwiZG9jdW1lbnQiLCJjb29raWUiLCJyZWNlaXZlIiwibWF0Y2hlcyIsIm1hdGNoIiwiUmVnRXhwIiwicmVwbGFjZSIsInVuZGVmaW5lZCIsImRlbGV0ZSIsImFza1VzYWdlIiwiJGluZm9CbG9jayIsIiRpbmZvQnV0dG9uIiwiYWxsb3dVc2FnZSIsImhpZGRlbkNsYXNzIiwiYWxsb3dVc2FnZUtleSIsImFsbG93VXNhZ2VWYWx1ZSIsImFsbG93VXNhZ2VEZWxheSIsInNlbGVjdFNlbGVjdG9yIiwid3JhcCIsInBsYWNlaG9sZGVyIiwiJGNsb3NlQnRuIiwiaXNMYW5nIiwic2VsZWN0MkluaXQiLCJsYW5nIiwiaW5kZXgiLCJjbG9zZXN0Iiwic2VsZWN0MiIsIm1pbmltdW1SZXN1bHRzRm9yU2VhcmNoIiwiZHJvcGRvd25QYXJlbnQiLCJsYW5ndWFnZSIsImFqYXgiLCJ1cmwiLCJwYXJhbXMiLCJxdWVyeSIsInNlYXJjaCIsInRlcm0iLCJjaXR5UmVmIiwidmFsIiwicHJvY2Vzc1Jlc3VsdHMiLCJyZXN1bHRzIiwicmVzdWx0IiwibmV4dCIsIiR3cmFwIiwiVmFsaWRhdG9yIiwiaW5pdFNlbGVjdCIsImdldFNjcmlwdCIsIiRwYXJlbnQiLCIkc2VsZWN0ZWQiLCJ0cmlnZ2VyIiwibWZwQWpheCIsImNsb3NlTWFya3VwIiwicmVtb3ZhbERlbGF5IiwiYXV0b0ZvY3VzTGFzdCIsImNhbGxiYWNrcyIsImVsZW1lbnRQYXJzZSIsIml0ZW0iLCJwYXJhbSIsInN0IiwiYWpheENvbnRlbnRBZGRlZCIsImNvbnRlbnRDb250YWluZXIiLCJpbml0IiwiY2hlY2tSZXEiLCJpbnB1dCIsInJvem4iLCJwcm9wIiwib3B0IiwiZHJvcCIsInJvem5CaXJ0aCIsInJlbW92ZUF0dHIiLCJvbmUiLCJhc3BlY3RSYXRpbyIsImhlaWdodCIsIm1hZ25pZmljUG9wdXAiLCJtZnBJZnJhbWUiLCJjbG9zZUJ0bkluc2lkZSIsIm1mcElubGluZSIsIm1mcEdhbGxlcnkiLCJkZWxlZ2F0ZSIsImdhbGxlcnkiLCJlbmFibGVkIiwicHJlbG9hZCIsIm5hdmlnYXRlQnlJbWdDbGljayIsImFycm93TWFya3VwIiwiem9vbWl0Iiwiem9vbUl0IiwiYWlyRGF0ZXBpY2tlciIsImRhdGVwaWNrZXIiLCJtYXhEYXRlIiwiY3JlYXRlVGltZXIiLCJkZWZhdWx0RGVsYXkiLCJ0aW1lcklkIiwiY2xlYXJUaW1lb3V0IiwiZm4iLCJkZWxheSIsImNsZWFyIiwialF1ZXJ5IiwicmVhZHkiLCJmb3JtR2V0VHlwZU5hbWUiLCJtdWx0aXBsZUZpbGVPclNlbGVjdCIsInR5cGVOYW1lIiwiZm9ybUdldE1ldGhvZE1zZ05hbWUiLCJtZXRob2QiLCJtZXRob2ROYW1lIiwic3BsaXQiLCJleHRlbmRDb25maWciLCJtZXNzYWdlcyIsInRyYW5zbGF0ZXMiLCJqc1RyYW5zbGF0aW9ucyIsIk9iamVjdCIsImtleXMiLCJ2YWxpZGF0b3IiLCJmb3JtYXQiLCJwcm90b3R5cGUiLCJkZWZhdWx0TWVzc2FnZSIsInJ1bGUiLCJtZXNzYWdlIiwiZmluZERlZmluZWQiLCJjdXN0b21NZXNzYWdlIiwibmFtZSIsImN1c3RvbURhdGFNZXNzYWdlIiwiaWdub3JlVGl0bGUiLCJ0aXRsZSIsInBhdHRlcm4iLCJjYWxsIiwicGFyYW1ldGVycyIsIm1ldGhvZHMiLCJhZGRDbGFzc1J1bGVzIiwiY2xhc3NSdWxlcyIsIm1pbmxlbmd0aCIsImxvZ2luIiwibWF4bGVuZ3RoIiwicmVxdWlyZWQiLCJmaWxldHlwZSIsImZpbGVzaXplIiwibWF4dXBsb2FkIiwiZW1haWwiLCJvcHRpb25hbCIsInBhc3N3b3JkIiwia2IiLCJmaWxlc2l6ZWVhY2giLCJmbGFnIiwiZXh0ZW5zaW9ucyIsInZhbHVlTWF0Y2giLCJvciIsIiRtb2R1bGUiLCJ3b3JkIiwidGVzdFZhbHVlIiwicGhvbmV1YSIsInBob25lIiwibm9zcGFjZSIsInN0ciIsIlN0cmluZyIsInZhbGlkZGF0YSIsInZhbGlkYXRlSGFuZGxlcnMiLCJhY3Rpb24iLCJwcmV2ZW50RGVmYXVsdCIsInJlc2V0Rm9ybSIsImluc3RhbmNlIiwic2hvd01lc3NhZ2UiLCJzdWNjZXNzIiwiaXNPcGVuIiwiJHBvcHVwIiwiaHRtbCIsIml0ZW1zIiwic3JjIiwic2hvd1dpZGdldE1lc3NhZ2UiLCJ3aWRnZXQiLCJjbG9zZVBvcHVwIiwiY2xvc2UiLCJzdW0iLCJhcnIiLCJyZWR1Y2VyIiwiYWNjdW11bGF0b3IiLCJjdXJyZW50VmFsdWUiLCJyZWR1Y2UiLCJzZXRUaG91c2FuZHMiLCJudW1iZXJUZXh0IiwibmV3U2VwYXJhdG9yIiwic2VwYXJhdG9yIiwibnVtYmVyUGVubnkiLCJudW1iZXJWYWx1ZSIsInRob3VzYW5kc1ZhbHVlIiwiY291bnRlciIsInB1c2giLCJyZXZlcnNlIiwiam9pbiIsInVwZGF0ZVN0YXRpY0NhcnQiLCJzdGF0aWMiLCJjb3VudCIsInRvdGFsUHJpY2UiLCJ0b0ZpeGVkIiwiJGh0bWwiLCJjaGFuZ2VTaXplcyIsInNpemVzIiwiY291bnRzIiwiJGl0ZW1zIiwiJGNhcnRJdGVtIiwiJGNvdW50ZXIiLCIkY291bnRlcklucHV0IiwiJHByaWNlIiwicHJpY2UiLCIkaXRlbSIsIiRjb3VudCIsInN0cmluZ2lmeSIsImZvckVhY2giLCJ2YWxpZGF0ZUdldFJlc3BvbnNlIiwicmVsb2FkIiwiaHJlZiIsInJlZGlyZWN0IiwiQXJyYXkiLCJpc0FycmF5IiwiY2xlYXJTZWxlY3RvciIsInJlc2V0IiwiY2FydCIsImdldCIsInN1Ym1pdCIsIm1mcCIsImRlZmF1bHRzIiwidENsb3NlIiwidExvYWRpbmciLCJ0UHJldiIsInROZXh0IiwidENvdW50ZXIiLCJpbWFnZSIsInRFcnJvciIsInRFcnJvckltYWdlIiwiaW5saW5lIiwidE5vdEZvdW5kIiwibm9SZWFjdCIsIiRidXR0b24iLCJ3c1RhYnMiLCJjc3NDbGFzcyIsImFjdGl2ZSIsImNoYW5nZVRhYiIsIiRjb250ZXh0IiwibXlOcyIsIm5zIiwibXlOYW1lIiwiYnV0dG9uIiwiYnV0dG9uc1NlbGVjdG9yIiwiYnV0dG9uU3luY1NlbGVjdG9yIiwiYmxvY2tzU2VsZWN0b3IiLCJibG9jayIsImJsb2NrU2VsZWN0b3IiLCIkYmxvY2siLCJnZXRNeUVsZW1lbnRzIiwibXlCbG9jayIsImV2ZW50cyIsImFnYWluIiwiJHNpYmxpbmdCbG9ja3MiLCJteUJsb2NrcyIsIiRzaWJsaW5nQnV0dG9ucyIsIm15QnV0dG9ucyIsIiRzeW5jQnV0dG9ucyIsImZpbHRlciIsIm9mZiIsInNldEFjdGl2ZUlmTm90SGF2ZSIsIiRidXR0b25zIiwic2VsZWN0b3IiLCIkZ3JvdXAiLCIkYWN0aXZlIiwiZXEiLCJkcm9wRGVwZW5kZW5jaWVzIiwiJGxpc3QiLCJzZXRBY3RpdmUiLCIkYmxvY2tzIiwiJGZpbHRlcnNNZW51IiwiJG9wZW5CbnQiLCIkbW9iaWxlT3BlbkJ0biIsIiRtb2JpbGVDbG9zZUJ0biIsIiRmaWx0ZXJzT3BlbkJ0biIsIiRmaWx0ZXJzQ2xvc2VCdG4iLCJtYWluTWVudSIsIiRtZW51IiwibW1lbnUiLCJBUEkiLCJjbGljayIsIm1vYmlsZU1lbnUiLCJmaWx0ZXJzTWVudSIsImhlYWRlciIsInNjcm9sbFByZXYiLCIkbWVudUNoaWxkSXRlbSIsImJvZHkiLCJmaXhlZEhlYWRlciIsInNjcm9sbCIsInNjcm9sbGVkIiwic2Nyb2xsVG9wIiwiZmlyc3RTY3JvbGxVcCIsImZpcnN0U2Nyb2xsRG93biIsImNhdGFsb2dNZW51Iiwib2Zmc2V0IiwidG9wUG9zaXRpb24iLCJzdWJNZW51U2hvdyIsIiR0YXJnZXQiLCJzcGVlZCIsInN0b3AiLCJmYWRlSW4iLCIkc3ViTWVudSIsImZhZGVPdXQiLCIkcmFuZ2VTbGlkZXIiLCJpbml0UmFuZ2VTbGlkZXIiLCIkbWluQ29zdCIsIiRtYXhDb3N0IiwibWluIiwicGFyc2VGbG9hdCIsIm1heCIsInN0YXJ0IiwiZW5kIiwic3RhcnRWYWx1ZSIsImVuZFZhbHVlIiwicmFuZ2UiLCJ2YWx1ZXMiLCJ1aSIsInNsaWRlIiwiY2hhbmdlIiwiJGZpbHRlcldyYXBwZXIiLCJwYXJlbnRzIiwiJGlucHV0IiwiY2F0ZWdvcnlfaWQiLCJmaWx0ZXJzIiwiaGFzT3duUHJvcGVydHkiLCJpbmNsdWRlcyIsIiRtaW5SYW5nZUlucHV0IiwiJG1heFJhbmdlSW5wdXQiLCJtaW5SYW5nZUNvc3QiLCJtYXhSYW5nZUNvc3QiLCJtaW5QcmljZSIsIm1heFByaWNlIiwiZGF0YVR5cGUiLCJsZWZ0IiwibWluVmFsdWUiLCJtYXhWYWx1ZSIsImtleXByZXNzIiwia2V5Q2hhciIsImtleUNvZGUiLCJ3aGljaCIsImZyb21DaGFyQ29kZSIsInN1Yk1lbnVUb2dnbGUiLCJidG4iLCIkb3BlbldyYXAiLCJzbGlkZURvd24iLCJzbGlkZVRvZ2dsZSIsInRvZ2dsZUNsYXNzIiwiJGNhcnRJbmZvUmFkaW8iLCIkY2FydERlbGl2ZXJ5Q29udGVudCIsInRvZ2dsZSIsIl90aGlzIiwic2xpZGVVcCIsIiRjb250ZW50IiwiY291bnRlclVwZGF0ZSIsIiRjb3VudEl0ZW0iLCIkcHJpY2VBbGwiLCIkY291bnRBbGwiLCJ3aG9sZXNhbGUiLCJtYXhDb3VudEFsbCIsImNvdW50T2xkQWxsIiwiaXNOdW1lcmljIiwiY29zdCIsImNvdW50TmV3QWxsIiwidGhpc1BsdXMiLCJzaWJsaW5ncyIsIm1heE5vdHkiLCJtaW5Db3VudEFsbCIsImNvdW50T2xkIiwibWF4Q291bnQiLCJjb3VudE5ldyIsIiR0aCIsIm1heENvdW50SXRlbSIsImNvdW50TmV3SXRlbSIsImNvdW50Tm93IiwiJGl0ZW1JbnB1dCIsIndyYXBwZXIiLCJJbmZpbml0eSIsInBvc2l0aW9uRHJvcGRvd24iLCJwcm90byIsImNvbnRlbnRJZCIsImNvbnRlbnQiLCJzZXgiLCJhZ2UiLCJVUkxTZWFyY2hQYXJhbXMiLCJoYXMiLCJzcGFuIiwiaXNFbXB0eU9iamVjdCIsImRlZmF1bHRMaW1pdCIsInF1ZXJ5UGFyYW1zIiwic2V0IiwidG9TdHJpbmciLCJwcm9kdWN0TWludXMiLCJwcm9kdWN0UGx1cyIsInByb2R1Y3RJbnB1dCIsInByb2R1Y3RDbGVhbiIsIiRhbGwiLCJhZGRUb0Zhdm9yaXRlIiwiaWQiLCIkaXQiLCJhZGRUb0NvbXBhcmUiLCJsaWtlQWRkIiwiJGRpc2xpa2UiLCIkZGlzbGlrZUNvdW50ZXIiLCJsaWtlQ291bnQiLCJkaXNsaWtlQ291bnQiLCJkaXNsaWtlQWRkIiwiJGxpa2UiLCIkbGlrZUNvdW50ZXIiLCJ0b2dnbGVDYXJ0SW5mbyIsImxvYWRNb3JlIiwiJGxvYWRNb3JlV3JhcCIsInBhZ2UiLCJsaW1pdCIsInN0YXJ0UGFnZSIsImZpbmlzaFBhZ2UiLCJsYXN0IiwicGFnaW5hdGlvbiIsInByb2R1Y3RDb3VudGVyIiwiJGZvb3RlciIsIiRmb290ZXJPZmZzZXRUb3AiLCIkdHJpZ2dlciIsImFjdGl2ZUNsYXNzIiwiJG9mZnNldFRvcCIsImFuaW1hdGUiLCJoZWFkZXJQaG9uZXNNb2IiLCJtb3VzZXVwIiwiaXMiLCJ0YXJnZXQiLCJ0b2dnbGVEZWxpdmVyeUluZm8iLCIkdGV4dCIsIiRtYXAiLCIkY2xpY2tUYXJnZXQiLCJpY29uIiwibWFwU3R5bGUiLCJzZXRQbGFjZSIsImxhdGl0dWRlIiwibG9uZ2l0dWRlIiwiZ29vZ2xlIiwibWFwcyIsIkxhdExuZyIsImluaXRNYXAiLCIkbWFwV3JhcCIsIiRtYXBUb3AiLCJfbWFwIiwibWFwIiwiTWFwIiwiZ2V0RWxlbWVudHNCeUNsYXNzTmFtZSIsInpvb20iLCJjZW50ZXIiLCJzY3JvbGx3aGVlbCIsInN0eWxlcyIsIm1hcmtlciIsIk1hcmtlciIsIm9wdGltaXplZCIsIm9wZW5DYXJ0QnRuIiwiY2xvc2VDYXJ0QnRuIiwiY2FydFdyYXAiLCIkY2FydElubmVyIiwicmlnaHQiLCJvcGVuQ2FydCIsImNsb3NlQ2FydCIsImNhcnRSZW1vdmUiLCIkY29uZmlybSIsIiRjb25maXJtQnRuIiwiJHVuQ29uZmlybUJ0biIsImNhcnRVcGRhdGUiLCJjYXJ0TWludXMiLCJjYXJ0UGx1cyIsImNhcnRJbnB1dCIsImNhcnRBZGQiLCJzdGF0aWNJdGVtIiwicHJlbG9hZGVyQ29udGFpbmVyIiwiY2FydENsZWFuIiwiY2FydFNob3ciLCJiaWdQb3B1cCIsImNhcnRPcmRlciIsImNhcnRJbml0IiwiY2FydERlZmF1bHRDbG9zZSIsInNlbGVjdFdvcmQiLCIkc2VsZWN0b3IiLCJzZWxlY3RDbGFzcyIsIiRoZWFkZXIiLCIkaGVhZGVyU2VhcmNoIiwiJGhlYWRlclNlYXJjaFJlc3VsdCIsIiRoZWFkZXJTZWFyY2hFbXB0eSIsIiRoZWFkZXJTZWFyY2hJbnB1dCIsInNlYXJjaFVybCIsIm91dGVySGVpZ2h0IiwiaGVhZGVyU2VhcmNoVG9nZ2xlIiwiZm9jdXMiLCJzZWFyY2hBdXRvY29tcGxldGUiLCJpbWciLCJjb2RlIiwidGhyZWVzaXh0eUZsYWciLCIkcHJvZHVjdEltYWdlIiwiJHByb2R1Y3QzNjAiLCJ0aHJlZXNpeHR5SW5pdCIsInRvdGFsRnJhbWVzIiwiaW1hZ2VQYXRoIiwiZXh0IiwiZmlsZVByZWZpeCIsInRzIiwiVGhyZWVTaXh0eSIsImN1cnJlbnRGcmFtZSIsImltZ0xpc3QiLCJwcm9ncmVzcyIsIm5hdmlnYXRpb24iLCJzcGVlZE11bHRpcGxpZXIiLCJvblJlYWR5IiwicHJldmlvdXMiLCJwbGF5IiwicHJvZHVjdE1lZGlhIiwiJG1lZGlhTGluayIsInBhZ2luYXRpb25Jbml0IiwiJHBhZ2luYXRpb24iLCJjYXRhbG9nU2VsZWN0IiwiJHNlbGVjdFdyYXAiLCIkc2VsZWN0Iiwic2NyZWVuIiwiaGVhZGVySGVpZ2h0IiwiJGNhdGFsb2ciLCIkaGVhZGVyVG9wIiwiZmFkZVRvZ2dsZSIsImNhdGFsb2dNZW51SXRlbSIsImNhdGFsb2dTdWIiLCJ0YXJnZXRDYXRlZ29yeSIsIiRjYXRhbG9nSXRlbSIsIiRjYXRhbG9nSXRlbUNoaWxkcmVuIiwiJGJhY2tCdG4iLCJjYXRhbG9nQ2xvc2UiLCJjaG9vc2VBamF4Iiwic2VuZEFqYXgiLCIkc2VsZWN0MiIsInRhZ05hbWUiLCJ0b0xvd2VyQ2FzZSIsImluaXRBamF4U2VsZWN0MiIsIm1lc3NhZ2VJbml0IiwiZ2VuZXJhdGUiLCJ0aW1lIiwibm90eSIsInRpbWVvdXQiLCJvdmVycmlkZURlZmF1bHRzIiwidGhlbWUiLCJsYXlvdXQiLCJwcm9ncmVzc0JhciIsImNsb3NlV2l0aCIsImN1cnJlbmN5SW5pdCIsImN1cnJlbmN5IiwicGF0aCIsInVzZXJUeXBlSW5pdCIsInVzZXJUeXBlIiwicGF5VG9nZ2xlIiwiaW5wdXRzIiwiaW5mbyIsInBheVR5cGUiLCJiYWNrVG9DYXRhbG9nIiwiZW5kUGFnZSIsImNsZWFyQ29va2llcyIsImRlbGxDaGlsZCIsImNoYW5nZVNpemVJblRhYmxlIiwidGFiTmFtZSIsImNsaWNrTWUiLCJkZWxpdmVyeVJlZ2lzdHJhdGlvbiIsIm5vUmVnIiwiZXhwb3J0VGFibGUiLCJzZXRDaGVja2VycyIsImlzQ2hlY2tlZCIsImNhdGVnb3JpZXNJZCIsInNvcnQiLCJhdmFpbGFiaWxpdHkiLCJhbW91bnQiLCJwcmV2IiwiYSIsImIiLCJjaGVja2VkQ2F0ZWdvcmllcyIsInByb2R1Y3RJZCIsImNoZWNrZWRQcm9kdWN0IiwibGV2ZWwiLCJhbGxJbnB1dHMiLCJjaGVja2VkSW5wdXRzIiwicGFuZWxXaXRoQnRucyIsInRvZ2dsZUNoZWNrZXJzIiwiY2hlY2tlcmVkRW1wdHlDYXRlZ29yaWVzIiwiZGVsbEFsbCIsImxpc3RUb2dnbGUiLCJjaGlsZHJlbiIsInByb2R1Y3RzRG93bmxvYWQiLCJjaGVja2VyZWRQcm9kdWN0cyIsInNlbGVjdCIsImxlbmd0aEFsbCIsImxlbmd0aENoZWNrZXRlZCIsInJlcGxhY2VXaXRoIiwicHJpY2VGcm9tVG8iLCJhbW91bnRTbGlkZXIiLCJzbGlkZXJSYW5nZSIsInByaWNlRnJvbUNsaWVudCIsInBlcmNlbnQiLCJpbmZvQmxvY2siLCJkaXYiLCJhamF4RmlsdGVyIiwib3BlbmVkQ2F0ZWdvcmllcyIsInByb2R1Y3RzIiwibm90QWxsIiwic2F2ZSIsInByb21JZCIsInNob3BOYW1lIiwiY29tcGFueU5hbWUiLCJuZXdQcmljZSIsImJlZm9yZVNlbmQiLCJjb3B5TGluayIsIiR0ZW1wIiwiZXhlY0NvbW1hbmQiXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBOztBQUVBOzs7OztBQUtBO0FBQ0E7QUFDQTs7Ozs7Ozs7QUFFQTs7Ozs7Ozs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7Ozs7Ozs7O0FBV0EsSUFBSUEsZ0JBQWdCO0FBQ25CQyxZQUFXLFdBRFE7QUFFbkJDLFdBQVUsRUFGUztBQUduQkMsWUFBVyxpQkFIUTtBQUluQkMsWUFBVyxpQkFKUTtBQUtuQkMsY0FBYSxHQUxNO0FBTW5CQyxTQUFRO0FBTlcsQ0FBcEI7O0FBU0E7QUFDQTtBQUNBOztBQUVBOzs7Ozs7Ozs7Ozs7OztJQWFNQyxTO0FBQ0wsb0JBQWFDLFVBQWIsRUFBMEM7QUFBQSxNQUFqQkMsVUFBaUIsdUVBQUosRUFBSTs7QUFBQTs7QUFDekMsTUFBSSxFQUFFRCxjQUFjQSxXQUFXRSxNQUEzQixDQUFKLEVBQXdDO0FBQ3ZDLFFBQUtDLEtBQUwsR0FBYSxJQUFiO0FBQ0EsVUFBTyxJQUFQO0FBQ0E7QUFDRCxNQUFJSCxXQUFXSSxJQUFYLENBQWdCLFdBQWhCLGFBQXdDTCxTQUE1QyxFQUF1RDtBQUN0REMsY0FBV0ksSUFBWCxDQUFnQixXQUFoQixFQUE2QixJQUE3QjtBQUNBO0FBQ0QsT0FBS0QsS0FBTCxHQUFhLEtBQWI7QUFDQSxPQUFLSCxVQUFMLEdBQWtCQSxVQUFsQjtBQUNBLE9BQUtLLE1BQUwsR0FBY0MsRUFBRUMsTUFBRixDQUFTLElBQVQsRUFBZSxFQUFmLEVBQW1CUixVQUFVTSxNQUE3QixFQUFxQ0osVUFBckMsQ0FBZDtBQUNBLE9BQUtPLEtBQUwsR0FBYSw0QkFBYjtBQUNBLE9BQUtSLFVBQUwsQ0FBZ0JJLElBQWhCLENBQXFCLFdBQXJCLEVBQWtDLElBQWxDO0FBQ0E7O0FBRUQ7Ozs7Ozs7Ozt5QkFLd0I7QUFBQSxPQUFsQkssS0FBa0IsdUVBQVYsUUFBVTs7QUFDdkIsT0FBSSxLQUFLTixLQUFULEVBQWdCO0FBQ2ZPLFlBQVFDLElBQVIsQ0FBYSxrQ0FBYjtBQUNBLFdBQU8sS0FBUDtBQUNBO0FBSnNCLGlCQUt3QixLQUFLTixNQUw3QjtBQUFBLE9BS2xCWixTQUxrQixXQUtsQkEsU0FMa0I7QUFBQSxPQUtQQyxRQUxPLFdBS1BBLFFBTE87QUFBQSxPQUtHQyxTQUxILFdBS0dBLFNBTEg7QUFBQSxPQUtjRyxNQUxkLFdBS2NBLE1BTGQ7OztBQU92QixRQUFLRSxVQUFMLENBQWdCWSxJQUFoQixDQUFxQixVQUFDQyxDQUFELEVBQUlDLEVBQUosRUFBVztBQUMvQixRQUFJZCxhQUFhTSxFQUFFUSxFQUFGLENBQWpCO0FBQ0FkLGVBQVdlLFFBQVgsQ0FBb0J0QixTQUFwQjtBQUNBLFFBQUlDLFFBQUosRUFBYztBQUNiTSxnQkFBV2UsUUFBWCxDQUFvQnJCLFFBQXBCO0FBQ0E7QUFDRE0sZUFBV0ksSUFBWCxDQUFnQixrQkFBaEIsRUFBb0NFLEVBQUVSLE1BQUYsQ0FBcEM7QUFDQUUsZUFBV1MsS0FBWCxFQUFrQlQsV0FBV0ksSUFBWCxDQUFnQixrQkFBaEIsQ0FBbEI7QUFDQVksV0FBT0MsVUFBUCxDQUFrQjtBQUFBLFlBQU1qQixXQUFXZSxRQUFYLENBQW9CcEIsU0FBcEIsQ0FBTjtBQUFBLEtBQWxCLEVBQXdELEVBQXhEO0FBQ0EsSUFURDtBQVVBOztBQUVEOzs7Ozs7Ozt5QkFLMkI7QUFBQTs7QUFBQSxPQUFyQnVCLFlBQXFCLHVFQUFOLElBQU07O0FBQzFCLE9BQUksS0FBS2YsS0FBVCxFQUFnQjtBQUNmTyxZQUFRQyxJQUFSLENBQWEsbUNBQWI7QUFDQSxXQUFPLEtBQVA7QUFDQTtBQUp5QixrQkFLd0IsS0FBS04sTUFMN0I7QUFBQSxPQUtyQlosU0FMcUIsWUFLckJBLFNBTHFCO0FBQUEsT0FLVkMsUUFMVSxZQUtWQSxRQUxVO0FBQUEsT0FLQUMsU0FMQSxZQUtBQSxTQUxBO0FBQUEsT0FLV0MsU0FMWCxZQUtXQSxTQUxYOztBQU0xQixRQUFLSSxVQUFMLENBQWdCZSxRQUFoQixDQUF5Qm5CLFNBQXpCOztBQUVBLFFBQUtZLEtBQUwsQ0FBVyxZQUFNO0FBQ2hCLFFBQUlVLFlBQUosRUFBa0I7QUFDakIsV0FBS2xCLFVBQUwsQ0FBZ0JZLElBQWhCLENBQXFCLFVBQUNDLENBQUQsRUFBSUMsRUFBSixFQUFXO0FBQy9CLFVBQUlSLEVBQUVRLEVBQUYsRUFBTVYsSUFBTixDQUFXLGtCQUFYLEtBQWtDRSxFQUFFUSxFQUFGLEVBQU1WLElBQU4sQ0FBVyxrQkFBWCxFQUErQkYsTUFBckUsRUFBNkU7QUFDNUVJLFNBQUVRLEVBQUYsRUFBTVYsSUFBTixDQUFXLGtCQUFYLEVBQStCZSxNQUEvQjtBQUNBO0FBQ0QsTUFKRDtBQUtBO0FBQ0QsVUFBS25CLFVBQUwsQ0FBZ0JvQixXQUFoQixDQUE0QixDQUFDM0IsU0FBRCxFQUFZRSxTQUFaLEVBQXVCQyxTQUF2QixDQUE1QjtBQUNBLFFBQUlGLFFBQUosRUFBYztBQUNiLFdBQUtNLFVBQUwsQ0FBZ0JvQixXQUFoQixDQUE0QjFCLFFBQTVCO0FBQ0E7QUFDRCxJQVpELEVBWUcsS0FBS1csTUFBTCxDQUFZUixXQVpmO0FBYUE7O0FBRUQ7Ozs7Ozs7O3NCQUtxQjtBQUNwQixVQUFPTCxhQUFQO0FBQ0E7O0FBRUQ7Ozs7Ozs7O29CQU9tQjZCLEssRUFBTztBQUN6QmYsS0FBRUMsTUFBRixDQUFTLElBQVQsRUFBZWYsYUFBZixFQUE4QjZCLEtBQTlCO0FBQ0E7Ozs7OztBQUdGO0FBQ0E7QUFDQTs7a0JBRWV0QixTOzs7Ozs7O0FDbkpmOztBQUVBOzs7O0FBSUE7QUFDQTtBQUNBOzs7Ozs7QUFFQTs7Ozs7O0FBRUE7QUFDQTtBQUNBOztBQUVBLElBQU11QixlQUFlLGdDQUFpQjtBQUNyQ0MsUUFBTyxJQUQ4QjtBQUVyQ0MsZ0JBQWU7QUFBQSxTQUFjLDRCQUFPLEdBQXdCQyxVQUEvQixDQUFkO0FBQUEsRUFGc0I7QUFHckNDLGVBQWMsVUFIdUI7QUFJckNDLG1CQUFrQixZQUptQjtBQUtyQ0MsZUFBYyxpQkFMdUI7QUFNckNDLGNBQWEsZ0JBTndCO0FBT3JDQyxPQUFNO0FBQ0w7QUFDQSxzQ0FBb0MsTUFGL0I7QUFHTCw4QkFBb0Msa0JBSC9CO0FBSUwsbUNBQW9DLFlBSi9CO0FBS0wsK0JBQW9DLG1CQUwvQjtBQU1MLDRCQUFvQyxnQkFOL0I7QUFPTCwwQkFBb0MsQ0FBQyxjQUFELEVBQWlCLFNBQWpCLENBUC9CO0FBUUwsa0NBQW9DLHNCQVIvQjtBQVNMLG9DQUFvQyx3QkFUL0I7QUFVTCxxQ0FBcUM7QUFWaEM7QUFQK0IsQ0FBakIsQ0FBckI7O0FBcUJBO0FBQ0E7QUFDQTs7a0JBRWVSLFk7Ozs7Ozs7O0FDekNmOztBQUVBOzs7OztBQUtBO0FBQ0E7QUFDQTs7Ozs7O0FBRUE7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7O0FBS0EsSUFBTVMsZ0JBQWdCO0FBQ3JCQyxTQUFRLHFCQURhO0FBRXJCLEtBQUlDLE9BQUosR0FBZTtBQUNkLFNBQU87QUFDTkMsVUFBTyxXQUREO0FBRU5DLFVBQU8sVUFGRDtBQUdOQyxlQUFZLGFBSE47QUFJTkMsY0FBVyxhQUpMO0FBS05DLGNBQVcsYUFMTDtBQU1OQyxnQkFBYTtBQU5QLEdBQVA7QUFRQSxFQVhvQjtBQVlyQixLQUFJQyxVQUFKLEdBQWtCO0FBQ2pCLFNBQU8sS0FBS1AsT0FBTCxDQUFhQyxLQUFwQjtBQUNBLEVBZG9CO0FBZXJCLEtBQUlPLFVBQUosR0FBa0I7QUFDakIsU0FBTyxLQUFLUixPQUFMLENBQWFDLEtBQXBCO0FBQ0EsRUFqQm9COztBQW1CckI7Ozs7Ozs7O0FBUUFRLFdBM0JxQixzQkEyQlRDLE9BM0JTLEVBMkJBO0FBQ3BCLE1BQUlBLFFBQVF0QixLQUFSLENBQWNuQixNQUFkLElBQXdCeUMsUUFBUUMsU0FBUixDQUFrQkMsUUFBbEIsQ0FBMkIsS0FBS0MsUUFBTCxDQUFjYixPQUFkLENBQXNCQyxLQUFqRCxDQUE1QixFQUFxRjtBQUNwRixRQUFLUyxPQUFMLENBQWFBLE9BQWI7QUFDQTtBQUNELEVBL0JvQjs7O0FBaUNyQjs7Ozs7Ozs7QUFRQUksUUF6Q3FCLG1CQXlDWkosT0F6Q1ksRUF5Q0g7QUFDakIsTUFBSUEsUUFBUUMsU0FBUixDQUFrQkMsUUFBbEIsQ0FBMkIsS0FBS0MsUUFBTCxDQUFjYixPQUFkLENBQXNCQyxLQUFqRCxDQUFKLEVBQTZEO0FBQzVELFFBQUtTLE9BQUwsQ0FBYUEsT0FBYjtBQUNBO0FBQ0QsRUE3Q29COzs7QUErQ3JCOzs7O0FBSUFLLFVBbkRxQixxQkFtRFZMLE9BbkRVLEVBbUREO0FBQ25CQSxVQUFRQyxTQUFSLENBQWtCekIsTUFBbEIsQ0FBeUIsS0FBSzJCLFFBQUwsQ0FBY2IsT0FBZCxDQUFzQkUsS0FBL0M7QUFDQVEsVUFBUUMsU0FBUixDQUFrQkssR0FBbEIsQ0FBc0IsS0FBS0gsUUFBTCxDQUFjYixPQUFkLENBQXNCQyxLQUE1QztBQUNBLEVBdERvQjs7O0FBd0RyQjs7OztBQUlBZ0IsWUE1RHFCLHVCQTREUlAsT0E1RFEsRUE0REM7QUFDckJBLFVBQVFDLFNBQVIsQ0FBa0J6QixNQUFsQixDQUF5QixLQUFLMkIsUUFBTCxDQUFjYixPQUFkLENBQXNCQyxLQUEvQztBQUNBUyxVQUFRQyxTQUFSLENBQWtCSyxHQUFsQixDQUFzQixLQUFLSCxRQUFMLENBQWNiLE9BQWQsQ0FBc0JFLEtBQTVDO0FBQ0EsRUEvRG9COzs7QUFpRXJCOzs7OztBQUtBZ0IsY0F0RXFCLHlCQXNFTkMsSUF0RU0sRUFzRUE7QUFDcEIsTUFBSUMsUUFBUS9DLEVBQUU4QyxJQUFGLENBQVo7QUFDQSxNQUFJRSxZQUFZRCxNQUFNakQsSUFBTixDQUFXLE1BQVgsQ0FBaEI7O0FBRUEsTUFBSSxDQUFDa0QsU0FBTCxFQUFnQjtBQUNmLFVBQU8sSUFBUDtBQUNBOztBQUVELE1BQUlDLFlBQVksd0JBQWNGLEtBQWQsQ0FBaEI7QUFDQUUsWUFBVUMsSUFBVjs7QUFFQSxNQUFJQyxXQUFXLElBQUl6QyxPQUFPMEMsUUFBWCxFQUFmO0FBQ0FELFdBQVNFLE1BQVQsQ0FBZ0IsVUFBaEIsRUFBNEJyRCxFQUFFLE1BQUYsRUFBVXNELElBQVYsQ0FBZSxNQUFmLEtBQTBCLElBQXREOztBQUVBO0FBQ0EsTUFBSUMsb0JBQW9CLENBQ3ZCLFFBRHVCLEVBRXZCLE9BRnVCLEVBR3ZCLFFBSHVCLEVBSXZCLE9BSnVCLENBQXhCOztBQU9BUixRQUFNUyxJQUFOLENBQVcseUJBQVgsRUFBc0NsRCxJQUF0QyxDQUEyQyxVQUFDQyxDQUFELEVBQUk4QixPQUFKLEVBQWdCO0FBQUEsT0FDckR0QixLQURxRCxHQUN0Q3NCLE9BRHNDLENBQ3JEdEIsS0FEcUQ7QUFBQSxPQUM5QzBDLElBRDhDLEdBQ3RDcEIsT0FEc0MsQ0FDOUNvQixJQUQ4Qzs7QUFFMUQsT0FBSSxDQUFDRixrQkFBa0JHLE9BQWxCLENBQTBCRCxJQUExQixDQUFMLEVBQXNDO0FBQ3JDLFdBQU8sSUFBUDtBQUNBOztBQUVELE9BQUlFLFdBQVczRCxFQUFFcUMsT0FBRixDQUFmO0FBQ0EsT0FBSXVCLGNBQWNELFNBQVNFLFFBQVQsRUFBbEI7QUFDQSxPQUFJQyxjQUFjSCxTQUFTN0QsSUFBVCxDQUFjLE1BQWQsS0FBeUIsSUFBM0M7O0FBRUEsT0FBSWdFLGdCQUFnQixJQUFwQixFQUEwQjtBQUN6QixXQUFPLElBQVA7QUFDQTs7QUFFRCxXQUFRRixXQUFSO0FBQ0MsU0FBSyxPQUFMO0FBQ0MsU0FBSUgsU0FBUyxNQUFULElBQW1CcEIsUUFBUTBCLEtBQTNCLElBQW9DMUIsUUFBUTBCLEtBQVIsQ0FBY25FLE1BQXRELEVBQThEO0FBQzdELFdBQUssSUFBSVcsS0FBSSxDQUFiLEVBQWdCQSxLQUFJOEIsUUFBUTBCLEtBQVIsQ0FBY25FLE1BQWxDLEVBQTBDVyxJQUExQyxFQUErQztBQUM5QyxXQUFJeUQsT0FBTzNCLFFBQVEwQixLQUFSLENBQWN4RCxFQUFkLENBQVg7QUFDQTRDLGdCQUFTRSxNQUFULENBQWdCUyxXQUFoQixFQUE2QkUsSUFBN0I7QUFDQTtBQUNELE1BTEQsTUFLTztBQUNOLFVBQUksQ0FBQ1AsU0FBUyxVQUFULElBQXVCQSxTQUFTLE9BQWpDLEtBQTZDLENBQUNwQixRQUFRNEIsT0FBMUQsRUFBbUU7QUFDbEU7QUFDQTtBQUNEZCxlQUFTRSxNQUFULENBQWdCUyxXQUFoQixFQUE2Qi9DLEtBQTdCO0FBQ0E7QUFDRDs7QUFFRCxTQUFLLFVBQUw7QUFDQ29DLGNBQVNFLE1BQVQsQ0FBZ0JTLFdBQWhCLEVBQTZCL0MsS0FBN0I7QUFDQTs7QUFFRCxTQUFLLFFBQUw7QUFDQyxTQUFJbUQsWUFBWSxPQUFoQjtBQUNBLFNBQUksQ0FBQ0EsVUFBVUMsSUFBVixDQUFlTCxXQUFmLEtBQStCekIsUUFBUStCLFFBQXhDLEtBQXFEL0IsUUFBUWdDLGVBQTdELElBQWdGaEMsUUFBUWdDLGVBQVIsQ0FBd0J6RSxNQUE1RyxFQUFvSDtBQUNuSCxXQUFLLElBQUlXLE1BQUksQ0FBYixFQUFnQkEsTUFBSThCLFFBQVFnQyxlQUFSLENBQXdCekUsTUFBNUMsRUFBb0RXLEtBQXBELEVBQXlEO0FBQ3hELFdBQUkrRCxTQUFTakMsUUFBUWdDLGVBQVIsQ0FBd0I5RCxHQUF4QixDQUFiO0FBQ0EsV0FBSStELE9BQU9DLFFBQVgsRUFBcUI7QUFDcEI7QUFDQTtBQUNEcEIsZ0JBQVNFLE1BQVQsQ0FBZ0JTLFdBQWhCLEVBQTZCUSxPQUFPdkQsS0FBcEM7QUFDQTtBQUNELE1BUkQsTUFRTztBQUNOb0MsZUFBU0UsTUFBVCxDQUFnQlMsV0FBaEIsRUFBNkIvQyxLQUE3QjtBQUNBO0FBQ0Q7QUFoQ0Y7QUFrQ0EsR0FoREQ7O0FBa0RBLE1BQUl5RCxNQUFNLElBQUk5RCxPQUFPK0QsY0FBWCxFQUFWO0FBQ0FELE1BQUlFLElBQUosQ0FBUyxNQUFULEVBQWlCMUIsU0FBakI7QUFDQXdCLE1BQUlHLGtCQUFKLEdBQXlCLFlBQVk7QUFDcEMsT0FBSUgsSUFBSUksVUFBSixLQUFtQixDQUF2QixFQUEwQjtBQUFBLFFBQ3BCQyxNQURvQixHQUNZTCxHQURaLENBQ3BCSyxNQURvQjtBQUFBLFFBQ1pDLFVBRFksR0FDWU4sR0FEWixDQUNaTSxVQURZO0FBQUEsUUFDQUMsUUFEQSxHQUNZUCxHQURaLENBQ0FPLFFBREE7QUFFekI7O0FBQ0EsUUFBSUYsV0FBVyxHQUFmLEVBQW9CO0FBQ25CekUsYUFBUTRFLEdBQVIsQ0FBWVIsR0FBWjtBQUNBdkIsZUFBVWdDLElBQVY7QUFDQSxZQUFPLEtBQVA7QUFDQTs7QUFFRCxRQUFJLE9BQU9GLFFBQVAsS0FBb0IsUUFBeEIsRUFBa0M7QUFDakNBLGdCQUFXRyxLQUFLQyxLQUFMLENBQVdKLFFBQVgsQ0FBWDtBQUNBOztBQUVELFFBQUkvQixjQUFjLE9BQU9vQyxTQUFTQyxJQUFoQixHQUF1QixpQkFBekMsRUFBNEQ7QUFDM0QsU0FBSUMsa0JBQWtCdEYsRUFBRSxvQkFBRixDQUF0QjtBQUNBLFNBQUdzRixnQkFBZ0IxRixNQUFuQixFQUEyQjtBQUMxQixXQUFLLElBQUkyRixJQUFULElBQWlCUixTQUFTUyxZQUExQixFQUF3QztBQUN2QyxXQUFJQyxjQUFjSCxnQkFBZ0I5QixJQUFoQixDQUFxQixzQ0FBc0MrQixJQUF0QyxHQUE2QyxHQUFsRSxDQUFsQjtBQUNBRSxtQkFBWWpDLElBQVosQ0FBaUIsTUFBakIsRUFBeUJrQyxLQUF6QixHQUFpQzVFLFdBQWpDLENBQTZDLG9CQUE3QyxFQUFtRUwsUUFBbkUsQ0FBNEUsWUFBNUU7QUFDQWdGLG1CQUFZakMsSUFBWixDQUFpQixtQkFBakIsRUFBc0MxQyxXQUF0QyxDQUFrRCxxQkFBbEQsRUFBeUVMLFFBQXpFLENBQWtGLGFBQWxGLEVBQWlHa0YsSUFBakcsQ0FBc0daLFNBQVNTLFlBQVQsQ0FBc0JELElBQXRCLENBQXRHO0FBQ0E7QUFDRDtBQUNEOztBQUVELHVDQUFvQnhDLEtBQXBCLEVBQTJCZ0MsUUFBM0IsRUFBcUNELFVBQXJDLEVBQWlETixHQUFqRDtBQUNBO0FBQ0QsR0EzQkQ7O0FBNkJBQSxNQUFJb0IsSUFBSixDQUFTekMsUUFBVDtBQUNBLFNBQU8sS0FBUDtBQUNBO0FBL0tvQixDQUF0Qjs7QUFrTEE7QUFDQTtBQUNBOztBQUVBOzs7QUFHQSxTQUFTMEMsUUFBVCxDQUFtQkMsU0FBbkIsRUFBOEI7QUFDN0JBLFdBQVV4RixJQUFWLENBQWUsVUFBQ0MsQ0FBRCxFQUFJQyxFQUFKLEVBQVc7QUFDekIsTUFBTXVDLFFBQVEvQyxFQUFFUSxFQUFGLENBQWQ7QUFDQSxNQUFJdUMsTUFBTWdELFlBQU4sQ0FBbUIsaUJBQW5CLENBQUosRUFBMkM7QUFDMUMsVUFBTyxJQUFQO0FBQ0E7O0FBRUQsTUFBTWhHLFNBQVNDLEVBQUVDLE1BQUYsQ0FBUyxJQUFULEVBQWUsRUFBZixFQUFtQndCLGFBQW5CLEVBQWtDO0FBQ2hEO0FBRGdELEdBQWxDLENBQWY7O0FBSUFzQixRQUFNOEMsUUFBTixDQUFlOUYsTUFBZjtBQUNBLGtDQUFpQmdELEtBQWpCLEVBQXdCQSxNQUFNakQsSUFBTixDQUFXLFdBQVgsQ0FBeEI7QUFDQSxFQVpEO0FBYUE7O0FBRUQ7QUFDQTtBQUNBOztrQkFFZStGLFE7Ozs7Ozs7QUMxT2Y7O0FBRUE7QUFDQTtBQUNBOzs7Ozs7O0FBRUE7O0FBRUE7QUFDQTtBQUNBOztBQUVBLElBQUlHLGNBQWNoRyxFQUFFLGNBQUYsQ0FBbEI7QUFDQSxJQUFJaUcsaUJBQWlCakcsRUFBRSxpQkFBRixDQUFyQjtBQUNBO0FBQ0EsSUFBSU4sYUFBYU0sRUFBRSxZQUFGLENBQWpCO0FBQ0EsSUFBSWtHLGdCQUFnQmxHLEVBQUUsd0JBQUYsQ0FBcEI7QUFDQSxJQUFJbUcsY0FBY25HLEVBQUUsK0JBQUYsQ0FBbEI7QUFDQSxJQUFJb0csc0JBQXNCcEcsRUFBRSx3QkFBRixDQUExQjtBQUNBLElBQUlxRyxvQkFBb0JyRyxFQUFFLHdCQUFGLENBQXhCOztBQUVBLFNBQVNzRyxZQUFULENBQXVCQyxJQUF2QixFQUE2QjtBQUM1QixLQUFJQyxVQUFVLENBQUN4RyxFQUFFVSxNQUFGLEVBQVUrRixLQUFWLEtBQW9CL0csV0FBV2dILFVBQVgsRUFBckIsSUFBZ0QsQ0FBOUQ7QUFDQSxLQUFJQyxhQUFhUixZQUFZUyxRQUFaLEdBQXVCQyxHQUF4Qzs7QUFFQU4sTUFBSy9DLElBQUwsQ0FBVSxhQUFWLEVBQXlCc0QsR0FBekIsQ0FBNkIsY0FBN0IsRUFBNkNOLE9BQTdDO0FBQ0FOLGVBQWNZLEdBQWQsQ0FBa0IsS0FBbEIsRUFBeUJILFVBQXpCLEVBQXFDRyxHQUFyQyxDQUF5QyxNQUF6QyxFQUFpRE4sT0FBakQ7QUFDQTs7QUFFRCxTQUFTTyxVQUFULENBQXFCUixJQUFyQixFQUEyQjtBQUMxQixLQUFJLENBQUNBLEtBQUtTLFFBQUwsQ0FBYyxPQUFkLENBQUwsRUFBNkI7QUFDNUIsTUFBSVIsVUFBV3hHLEVBQUVVLE1BQUYsRUFBVStGLEtBQVYsS0FBb0IsSUFBckIsR0FBNkJRLFNBQVMsQ0FBQ2pILEVBQUVVLE1BQUYsRUFBVStGLEtBQVYsS0FBb0IvRyxXQUFXZ0gsVUFBWCxFQUFyQixJQUFnRCxDQUF6RCxDQUE3QixHQUEyRixDQUF6RztBQUNBSCxPQUFLL0MsSUFBTCxDQUFVLGFBQVYsRUFBeUJzRCxHQUF6QixDQUE2QixjQUE3QixFQUE2Q04sT0FBN0MsRUFBc0RNLEdBQXRELENBQTBELGVBQTFELEVBQTJFTixPQUEzRTtBQUNBO0FBQ0Q7O0FBRUQ7QUFDQTtBQUNBOztBQUVBLFNBQVNVLFVBQVQsR0FBdUI7QUFDdEIsS0FBSSxDQUFDbEIsWUFBWXBHLE1BQWpCLEVBQXlCO0FBQ3hCLFNBQU8sS0FBUDtBQUNBOztBQUVEb0csYUFBWW1CLEVBQVosQ0FBZSxNQUFmLEVBQXVCLFVBQVVDLEtBQVYsRUFBaUJDLEtBQWpCLEVBQXdCQyxTQUF4QixFQUFtQztBQUN6RGhCLGVBQWF0RyxFQUFFLElBQUYsQ0FBYjtBQUNBLEVBRkQ7O0FBSUFnRyxhQUFZcUIsS0FBWixDQUFrQjtBQUNqQkUsZ0JBQWMsQ0FERztBQUVqQkMsa0JBQWdCLENBRkM7QUFHakJDLFlBQVUsSUFITztBQUlqQkMsUUFBTSxJQUpXO0FBS2pCQyxVQUFRLEtBTFM7QUFNakJDLFlBQVUsS0FOTztBQU9qQkMsUUFBTSxJQVBXO0FBUWpCQyxjQUFZOUgsRUFBRSx3QkFBRixDQVJLO0FBU2pCK0gsZ0JBQWMsc0JBQVVDLE1BQVYsRUFBa0J6SCxDQUFsQixFQUFxQjtBQUNsQyxPQUFJMEgsTUFBUTFILElBQUksQ0FBTCxJQUFXLENBQVosR0FBaUIsSUFBSTJILE1BQUosQ0FBVzNILElBQUksQ0FBZixDQUFqQixHQUFzQ0EsSUFBSSxDQUFwRDtBQUNBLFVBQU8saUNBQWlDMEgsR0FBakMsU0FBUDtBQUNBLEdBWmdCO0FBYWpCRSxpRkFiaUI7QUFjakJDLGlGQWRpQjtBQWVqQkMsY0FBWSxDQUNYO0FBQ0NDLGVBQVksR0FEYjtBQUVDOUYsYUFBVTtBQUNUb0YsY0FBVSxJQUREO0FBRVRXLGtCQUFjO0FBRkw7QUFGWCxHQURXO0FBZkssRUFBbEI7O0FBMEJBdkksR0FBRVUsTUFBRixFQUFVOEgsTUFBVixDQUFpQixZQUFZO0FBQzVCbEMsZUFBYU4sV0FBYjtBQUNBLEVBRkQ7QUFHQTs7QUFFRCxTQUFTeUMsZ0JBQVQsR0FBNkI7QUFDNUIsS0FBSUMsb0JBQW9CMUksRUFBRSxxQkFBRixDQUF4Qjs7QUFFQSxLQUFJLENBQUMwSSxrQkFBa0I5SSxNQUF2QixFQUErQjtBQUM5QixTQUFPLEtBQVA7QUFDQTtBQUNEOEksbUJBQWtCQyxHQUFsQixDQUFzQixvQkFBdEIsRUFBNENySSxJQUE1QyxDQUFpRCxVQUFVQyxDQUFWLEVBQVlxSSxDQUFaLEVBQWU7QUFDL0QsTUFBSUMsUUFBUTdJLEVBQUU0SSxDQUFGLENBQVo7O0FBRUFDLFFBQU14QixLQUFOLENBQVk7QUFDWEUsaUJBQWMsQ0FESDtBQUVYQyxtQkFBZ0IsQ0FGTDtBQUdYQyxhQUFVLEtBSEM7QUFJWEMsU0FBTSxLQUpLO0FBS1hDLFdBQVEsSUFMRztBQU1YQyxhQUFVLEtBTkM7QUFPWEMsU0FBTSxJQVBLO0FBUVhpQixpQkFBY0QsTUFBTUUsTUFBTixHQUFldkYsSUFBZixDQUFvQiw0QkFBcEIsQ0FSSDtBQVNYMkUsY0FBVyxvRUFUQTtBQVVYQyxjQUFXOztBQUVYO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBbEJXLEdBQVo7QUFxQkEsRUF4QkQ7QUEwQkE7O0FBRUQsU0FBU1ksYUFBVCxHQUEwQjtBQUN6QixLQUFJLENBQUMvQyxlQUFlckcsTUFBcEIsRUFBNEI7QUFDM0IsU0FBTyxLQUFQO0FBQ0E7O0FBRURxRyxnQkFBZTNGLElBQWYsQ0FBb0IsVUFBVUMsQ0FBVixFQUFhO0FBQ2hDLE1BQUlzSSxRQUFRN0ksRUFBRSxJQUFGLENBQVo7O0FBRUE2SSxRQUFNMUIsRUFBTixDQUFTLE1BQVQsRUFBaUIsVUFBVUMsS0FBVixFQUFpQkMsS0FBakIsRUFBd0JDLFNBQXhCLEVBQW1DO0FBQ25EUCxjQUFXOEIsS0FBWDtBQUNBLEdBRkQ7O0FBSUFBLFFBQU14QixLQUFOLENBQVk7QUFDWEUsaUJBQWMsQ0FESDtBQUVYQyxtQkFBZ0IsQ0FGTDtBQUdYQyxhQUFVLEtBSEM7QUFJWEUsV0FBUSxJQUpHO0FBS1hELFNBQU0sSUFMSztBQU1YSSxlQUFZZSxNQUFNRSxNQUFOLEdBQWV2RixJQUFmLENBQW9CLDBCQUFwQixDQU5EO0FBT1h1RSxpQkFBYyxzQkFBVUMsTUFBVixFQUFrQnpILENBQWxCLEVBQXFCO0FBQ2xDLFFBQUkwSCxNQUFNMUgsSUFBSSxDQUFkO0FBQ0EsV0FBTyw2QkFBNkIwSCxHQUE3QixHQUFtQyxxQ0FBbkMsR0FBMkVELE9BQU9pQixVQUFsRixHQUErRixTQUF0RztBQUNBLElBVlU7QUFXWEgsaUJBQWNELE1BQU1FLE1BQU4sR0FBZXZGLElBQWYsQ0FBb0IsOEJBQXBCLENBWEg7QUFZWDJFLGtGQVpXO0FBYVhDLGtGQWJXO0FBY1hDLGVBQVksQ0FDWDtBQUNDQyxnQkFBWSxJQURiO0FBRUM5RixjQUFVO0FBQ1QrRSxtQkFBYztBQURMO0FBRlgsSUFEVyxFQU9YO0FBQ0NlLGdCQUFZLEdBRGI7QUFFQzlGLGNBQVU7QUFDVCtFLG1CQUFjO0FBREw7QUFGWCxJQVBXLEVBWVI7QUFDRmUsZ0JBQVksR0FEVjtBQUVGOUYsY0FBVTtBQUNUK0UsbUJBQWM7QUFETDtBQUZSLElBWlEsRUFpQlI7QUFDRmUsZ0JBQVksR0FEVjtBQUVGOUYsY0FBVTtBQUNUK0UsbUJBQWM7QUFETDtBQUZSLElBakJRO0FBZEQsR0FBWjs7QUF3Q0F2SCxJQUFFVSxNQUFGLEVBQVU4SCxNQUFWLENBQWlCLFlBQVk7QUFDNUJ6QixjQUFXOEIsS0FBWDtBQUNBLEdBRkQ7QUFHQSxFQWxERDtBQW1EQTs7QUFFRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxTQUFTSyxrQkFBVCxHQUErQjtBQUM5QixLQUFJLENBQUM5QyxvQkFBb0J4RyxNQUF6QixFQUFpQztBQUNoQyxTQUFPLEtBQVA7QUFDQTs7QUFFRCxLQUFJdUosY0FBY25KLEVBQUUsd0JBQUYsRUFBNEJxSCxLQUE1QixDQUFrQztBQUNuREUsZ0JBQWMsQ0FEcUM7QUFFbkRFLFlBQVUsS0FGeUM7QUFHbkRJLFFBQU0sSUFINkM7QUFJbkRILFFBQU0sS0FKNkM7QUFLbkRDLFVBQVEsS0FMMkM7QUFNbkR5QixpQkFBZSxLQU5vQztBQU9uREMsWUFBVTtBQVB5QyxFQUFsQyxDQUFsQjs7QUFVQWhELG1CQUFrQmdCLEtBQWxCLENBQXdCO0FBQ3ZCRSxnQkFBYyxDQURTO0FBRXZCQyxrQkFBZ0IsQ0FGTztBQUd2QjZCLFlBQVUsd0JBSGE7QUFJdkJDLGlCQUFlLElBSlE7QUFLdkJDLFlBQVUsSUFMYTtBQU12QjlCLFlBQVUsS0FOYTtBQU92QlUsYUFBVyxnRUFQWTtBQVF2QkMsYUFBVyxnRUFSWTs7QUFVdkJDLGNBQVksQ0FDWDtBQUNDQyxlQUFZLEdBRGI7QUFFQzlGLGFBQVU7QUFDVCtHLGNBQVUsS0FERDtBQUVUaEMsa0JBQWMsQ0FGTDtBQUdURSxjQUFVO0FBSEQ7QUFGWCxHQURXLEVBUVI7QUFDRmEsZUFBWSxHQURWO0FBRUY5RixhQUFVO0FBQ1QrRyxjQUFVLEtBREQ7QUFFVGhDLGtCQUFjLENBRkw7QUFHVEUsY0FBVTtBQUhEO0FBRlIsR0FSUTtBQVZXLEVBQXhCOztBQTZCQTBCLGFBQVloQyxFQUFaLENBQWUsY0FBZixFQUErQixVQUFVQyxLQUFWLEVBQWlCQyxLQUFqQixFQUF3Qm1DLFlBQXhCLEVBQXNDQyxTQUF0QyxFQUFpRCxDQUUvRSxDQUZEO0FBR0E7O0FBRUQ7QUFDQTtBQUNBOztRQUVRdkMsVSxHQUFBQSxVO1FBQVk4QixhLEdBQUFBLGE7UUFBZUUsa0IsR0FBQUEsa0I7UUFBb0JULGdCLEdBQUFBLGdCOzs7Ozs7O0FDOVB2RDs7QUFFQTs7Ozs7QUFLQTtBQUNBO0FBQ0E7Ozs7O2NBS0kvSCxNO0lBRmlCZ0osTyxXQUFwQkMsa0I7SUFDb0JDLE8sV0FBcEJDLGtCOztBQUdEO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7QUFJQSxJQUFNQyxhQUFhO0FBQ2xCOzs7Ozs7Ozs7Ozs7QUFZQW5ILElBYmtCLGVBYWJvSCxHQWJhLEVBYVJoSixLQWJRLEVBYWE7QUFBQSxNQUFkaUosT0FBYyx1RUFBSixFQUFJOztBQUM5QjtBQUNBO0FBQ0E7QUFDQTtBQUNBLE1BQUlDLFVBQVVELFFBQVFDLE9BQXRCOztBQUVBLE1BQUksT0FBT0EsT0FBUCxLQUFtQixRQUFuQixJQUErQkEsT0FBbkMsRUFBNEM7QUFDM0MsT0FBSUMsSUFBSSxJQUFJQyxJQUFKLEVBQVI7QUFDQUQsS0FBRUUsT0FBRixDQUFVRixFQUFFRyxPQUFGLEtBQWNKLFVBQVUsSUFBbEM7QUFDQUEsYUFBVUQsUUFBUUMsT0FBUixHQUFrQkMsQ0FBNUI7QUFDQTs7QUFFRCxNQUFJRCxXQUFXQSxRQUFRSyxXQUF2QixFQUFvQztBQUNuQ04sV0FBUUMsT0FBUixHQUFrQkEsUUFBUUssV0FBUixFQUFsQjtBQUNBOztBQUVEdkosVUFBUTJJLFFBQVEzSSxLQUFSLENBQVI7QUFDQSxNQUFJd0osZ0JBQW1CUixHQUFuQixTQUEwQmhKLEtBQTlCOztBQUVBLE9BQUssSUFBSXlKLFFBQVQsSUFBcUJSLE9BQXJCLEVBQThCO0FBQzdCTywyQkFBc0JDLFFBQXRCO0FBQ0EsT0FBSUMsWUFBWVQsUUFBUVEsUUFBUixDQUFoQjtBQUNBLE9BQUlDLGNBQWMsSUFBbEIsRUFBd0I7QUFDdkJGLDJCQUFxQkUsU0FBckI7QUFDQTtBQUNEOztBQUVEQyxXQUFTQyxNQUFULEdBQWtCSixhQUFsQjtBQUNBLEVBMUNpQjs7O0FBNENsQjs7Ozs7OztBQU9BSyxRQW5Ea0IsbUJBbURUYixHQW5EUyxFQW1ESjtBQUNiLE1BQUljLFVBQVVILFNBQVNDLE1BQVQsQ0FBZ0JHLEtBQWhCLENBQXNCLElBQUlDLE1BQUosY0FDeEJoQixJQUFJaUIsT0FBSixDQUFZLDhCQUFaLEVBQTRDLE1BQTVDLENBRHdCLGNBQ3NDO0FBRHRDLEdBQXRCLENBQWQ7QUFHQSxTQUFPSCxVQUFVakIsUUFBUWlCLFFBQVEsQ0FBUixDQUFSLENBQVYsR0FBZ0NJLFNBQXZDO0FBQ0EsRUF4RGlCOzs7QUEwRGxCOzs7Ozs7QUFNQUMsT0FoRWtCLG1CQWdFVm5CLEdBaEVVLEVBZ0VMO0FBQ1osT0FBS3BILEdBQUwsQ0FBU29ILEdBQVQsRUFBYyxFQUFkLEVBQWtCLEVBQUNFLFNBQVMsQ0FBQyxDQUFYLEVBQWxCO0FBQ0EsRUFsRWlCOzs7QUFvRWxCOzs7OztBQUtBa0IsU0F6RWtCLHNCQXlFTjtBQUFBOztBQUNYLE9BQUtDLFVBQUwsR0FBa0IsS0FBS0EsVUFBTCxJQUFtQnBMLEVBQUUsb0JBQUYsQ0FBckM7QUFDQSxPQUFLcUwsV0FBTCxHQUFtQixLQUFLQSxXQUFMLElBQW9CckwsRUFBRSxzQkFBRixDQUF2Qzs7QUFFQSxNQUFJLEtBQUtvTCxVQUFMLENBQWdCeEwsTUFBaEIsSUFBMEIsS0FBS3lMLFdBQUwsQ0FBaUJ6TCxNQUEvQyxFQUF1RDtBQUN0RCxPQUFJLEtBQUswTCxVQUFULEVBQXFCO0FBQ3BCLFNBQUtGLFVBQUwsQ0FBZ0J2SyxNQUFoQjtBQUNBLFdBQU8sS0FBUDtBQUNBOztBQUVELE9BQU0wSyxjQUFjLFdBQXBCO0FBQ0EsUUFBS0YsV0FBTCxDQUFpQmxFLEVBQWpCLENBQW9CLE9BQXBCLEVBQTZCLFlBQU07QUFDbEMsVUFBS2lFLFVBQUwsQ0FBZ0IzSyxRQUFoQixDQUF5QjhLLFdBQXpCO0FBQ0EsVUFBSzVJLEdBQUwsQ0FBUyxNQUFLNkksYUFBZCxFQUE2QixNQUFLQyxlQUFsQztBQUNBL0ssV0FBT0MsVUFBUCxDQUFrQixZQUFNO0FBQ3ZCLFdBQUt5SyxVQUFMLENBQWdCdkssTUFBaEI7QUFDQSxLQUZELEVBRUcsTUFBSzZLLGVBRlI7QUFHQSxJQU5EOztBQVFBaEwsVUFBT0MsVUFBUCxDQUFrQixZQUFNO0FBQ3ZCLFVBQUt5SyxVQUFMLENBQWdCdEssV0FBaEIsQ0FBNEJ5SyxXQUE1QjtBQUNBLElBRkQsRUFFRyxLQUFLRyxlQUZSO0FBR0EsVUFBTyxJQUFQO0FBQ0E7QUFDRCxFQWpHaUI7OztBQW1HbEI7Ozs7Ozs7QUFPQSxLQUFJSixVQUFKLEdBQWtCO0FBQ2pCLFNBQU8sS0FBS1YsT0FBTCxDQUFhLEtBQUtZLGFBQWxCLE1BQXFDLEtBQUtDLGVBQWpEO0FBQ0EsRUE1R2lCOztBQThHbEI7Ozs7Ozs7O0FBUUEsS0FBSUQsYUFBSixHQUFxQjtBQUNwQixTQUFPLG9CQUFQO0FBQ0EsRUF4SGlCOztBQTBIbEI7Ozs7Ozs7O0FBUUEsS0FBSUMsZUFBSixHQUF1QjtBQUN0QixTQUFPLE1BQVA7QUFDQSxFQXBJaUI7O0FBc0lsQjs7Ozs7Ozs7QUFRQSxLQUFJQyxlQUFKLEdBQXVCO0FBQ3RCLFNBQU8sSUFBUDtBQUNBO0FBaEppQixDQUFuQjs7QUFtSkE7QUFDQTtBQUNBOztrQkFFZTVCLFU7Ozs7Ozs7QUMvS2Y7O0FBRUE7QUFDQTtBQUNBOzs7Ozs7O0FBRUE7O0FBQ0E7Ozs7OztBQUVBO0FBQ0E7QUFDQTs7QUFFQSxJQUFJNkIsaUJBQWlCLFVBQXJCO0FBQ0EsSUFBSUMsYUFBSjtBQUNBLElBQUlDLG9CQUFKO0FBQ0EsSUFBSUMsWUFBWTlMLEVBQUUsbUJBQUYsQ0FBaEI7QUFDQSxJQUFJK0wsU0FBUyxLQUFiOztBQUVBLFNBQVNDLFdBQVQsQ0FBc0JDLElBQXRCLEVBQW9EO0FBQUEsUUFBeEJ2TSxVQUF3Qix1RUFBWE0sRUFBRSxNQUFGLENBQVc7O0FBQ25ETixlQUFXOEQsSUFBWCxDQUFnQm1JLGNBQWhCLEVBQWdDckwsSUFBaEMsQ0FBcUMsVUFBVTRMLEtBQVYsRUFBaUI7QUFDckQsWUFBSXJELFFBQVE3SSxFQUFFLElBQUYsQ0FBWjtBQUNBNEwsZUFBTy9DLE1BQU1zRCxPQUFOLENBQWMsZUFBZCxDQUFQO0FBQ0FOLHNCQUFjaEQsTUFBTS9JLElBQU4sQ0FBVyxhQUFYLENBQWQ7QUFDQSxZQUFJaUQsUUFBUThGLE1BQU1zRCxPQUFOLENBQWMsVUFBZCxDQUFaOztBQUVBLFlBQUl0RCxNQUFNN0IsUUFBTixDQUFlLGFBQWYsQ0FBSixFQUFtQztBQUN6QjZCLGtCQUFNdUQsT0FBTixDQUFjO0FBQ1ZDLHlDQUEwQixDQURoQjtBQUVWQyxnQ0FBZ0JWLElBRk47QUFHVkMsNkJBQWFBLFdBSEg7QUFJVnBGLHVCQUFPLFNBSkc7QUFLVjhGLDBCQUFVTixJQUxBO0FBTVZPLHNCQUFNO0FBQ0ZDLHlCQUFLNUQsTUFBTS9JLElBQU4sQ0FBVyxLQUFYLENBREg7QUFFRjJELDBCQUFNLE1BRko7QUFHRjNELDBCQUFNLGNBQVU0TSxNQUFWLEVBQWtCO0FBQ3BCLDRCQUFJQyxRQUFRO0FBQ1JDLG9DQUFRRixPQUFPRyxJQURQO0FBRVJDLHFDQUFTOU0sRUFBRSxrQkFBRixFQUFzQitNLEdBQXRCO0FBRkQseUJBQVo7O0FBS0EsK0JBQU9KLEtBQVA7QUFDSCxxQkFWQztBQVdGSyxvQ0FBZ0Isd0JBQVVsTixJQUFWLEVBQWdCO0FBQzVCLCtCQUFPO0FBQ0htTixxQ0FBU25OLEtBQUtvTjtBQURYLHlCQUFQO0FBR0g7QUFmQztBQU5JLGFBQWQ7QUF3QkgsU0F6QlAsTUF5QmEsSUFBSXJFLE1BQU03QixRQUFOLENBQWUsYUFBZixDQUFKLEVBQW1DO0FBQ3RDNkIsa0JBQU11RCxPQUFOLENBQWM7QUFDVkMseUNBQTBCLENBRGhCO0FBRVZDLGdDQUFnQlYsSUFGTjtBQUdWQyw2QkFBYUEsV0FISDtBQUlWcEYsdUJBQU8sU0FKRztBQUtWOEYsMEJBQVVOLElBTEE7QUFNVk8sc0JBQU07QUFDRkMseUJBQUs1RCxNQUFNL0ksSUFBTixDQUFXLEtBQVgsQ0FESDtBQUVGMkQsMEJBQU0sTUFGSjtBQUdGM0QsMEJBQU0sY0FBVTRNLE1BQVYsRUFBa0I7QUFDcEIsNEJBQUlDLFFBQVE7QUFDUkMsb0NBQVFGLE9BQU9HLElBRFA7QUFFUkMscUNBQVM5TSxFQUFFLHlCQUFGLEVBQTZCK00sR0FBN0I7QUFGRCx5QkFBWjs7QUFLQSwrQkFBT0osS0FBUDtBQUNILHFCQVZDO0FBV0ZLLG9DQUFnQix3QkFBVWxOLElBQVYsRUFBZ0I7QUFDNUIsK0JBQU87QUFDSG1OLHFDQUFTbk4sS0FBS29OO0FBRFgseUJBQVA7QUFHSDtBQWZDO0FBTkksYUFBZDtBQXdCSCxTQXpCTSxNQXlCQTtBQUNIckUsa0JBQU11RCxPQUFOLENBQWM7QUFDVkMseUNBQXlCLENBRGY7QUFFVkMsZ0NBQWdCVixJQUZOO0FBR1ZDLDZCQUFhQSxXQUhIO0FBSVZwRix1QkFBTyxTQUpHO0FBS1Y4RiwwQkFBVU47QUFMQSxhQUFkO0FBT1RqTSxjQUFFLHNCQUFGLEVBQTBCK00sR0FBMUIsQ0FBOEIsRUFBOUI7QUFDQS9NLGNBQUUsd0JBQUYsRUFBNEIrTSxHQUE1QixDQUFnQyxFQUFoQztBQUNNOztBQUVQbEUsY0FBTTFCLEVBQU4sQ0FBUyxnQkFBVCxFQUEyQixZQUFZO0FBQ3RDLGdCQUFJekgsYUFBYW1KLE1BQU1zRSxJQUFOLENBQVcsb0JBQVgsQ0FBakI7QUFDQSxnQkFBSUMsUUFBUTFOLFdBQVd5TSxPQUFYLENBQW1CLGVBQW5CLENBQVo7QUFDQXpNLHVCQUFXZSxRQUFYLENBQW9CLFVBQXBCO0FBQ0EyTSxrQkFBTTNNLFFBQU4sQ0FBZSxVQUFmOztBQUVBLGdCQUFNc0MsUUFBUS9DLEVBQUUsS0FBSzhDLElBQVAsQ0FBZDtBQUNBLGdCQUFNdUssWUFBWXRLLE1BQU1qRCxJQUFOLENBQVcsV0FBWCxDQUFsQjtBQUNBLGdCQUFJdU4sU0FBSixFQUFlO0FBQ2RBLDBCQUFVaEwsT0FBVixDQUFrQndHLEtBQWxCO0FBQ0E7QUFDRCxTQVhEOztBQWFBQSxjQUFNMUIsRUFBTixDQUFTLGtCQUFULEVBQTZCLFlBQVk7QUFDeEMsZ0JBQUl6SCxhQUFhbUosTUFBTXNFLElBQU4sQ0FBVyxvQkFBWCxDQUFqQjtBQUNBLGdCQUFJQyxRQUFRMU4sV0FBV3lNLE9BQVgsQ0FBbUIsZUFBbkIsQ0FBWjtBQUNBek0sdUJBQVdvQixXQUFYLENBQXVCLFVBQXZCO0FBQ0FzTSxrQkFBTXRNLFdBQU4sQ0FBa0IsVUFBbEI7O0FBRUEsZ0JBQU1pQyxRQUFRL0MsRUFBRSxLQUFLOEMsSUFBUCxDQUFkO0FBQ0EsZ0JBQU11SyxZQUFZdEssTUFBTWpELElBQU4sQ0FBVyxXQUFYLENBQWxCO0FBQ0EsZ0JBQUl1TixTQUFKLEVBQWU7QUFDZEEsMEJBQVVoTCxPQUFWLENBQWtCd0csS0FBbEI7QUFDQTtBQUNELFNBWEQ7QUFZQSxLQTdGRDtBQThGQTs7QUFFRDtBQUNBO0FBQ0E7O0FBRUEsU0FBU3lFLFVBQVQsR0FBNkM7QUFBQSxRQUF4QjVOLFVBQXdCLHVFQUFYTSxFQUFFLE1BQUYsQ0FBVzs7QUFDNUMsUUFBSWlNLE9BQU9qTSxFQUFFLE1BQUYsRUFBVXNELElBQVYsQ0FBZSxNQUFmLENBQVg7QUFDQSxRQUFJMkksUUFBUSxDQUFDRixNQUFiLEVBQXFCO0FBQ3BCQSxpQkFBUyxJQUFUO0FBQ0EvTCxVQUFFdU4sU0FBRixDQUFZLHFDQUFxQ3RCLElBQXJDLEdBQTRDLEtBQXhELEVBQStELFlBQVk7QUFDMUVELHdCQUFZQyxJQUFaLEVBQWtCdk0sVUFBbEI7QUFDQSxTQUZEO0FBR0EsS0FMRCxNQUtPO0FBQ05zTSxvQkFBWUMsSUFBWixFQUFrQnZNLFVBQWxCO0FBQ0E7O0FBRURvTSxjQUFVM0UsRUFBVixDQUFhLE9BQWIsRUFBc0IsWUFBWTtBQUNqQyxZQUFJcUcsVUFBVXhOLEVBQUUsSUFBRixFQUFRbU0sT0FBUixDQUFnQixlQUFoQixDQUFkO0FBQ0EsWUFBSXNCLFlBQVlELFFBQVFoSyxJQUFSLENBQWFtSSxjQUFiLENBQWhCO0FBQ0E4QixrQkFBVVYsR0FBVixDQUFjLEVBQWQsRUFBa0JXLE9BQWxCLENBQTBCLFFBQTFCO0FBQ0FELGtCQUFVQyxPQUFWLENBQWtCLGtCQUFsQjtBQUNBLEtBTEQ7QUFNQTs7QUFFRDtBQUNBO0FBQ0E7O1FBRVFKLFUsR0FBQUEsVTs7Ozs7Ozs7OztBQy9JUjs7QUFFQTs7Ozs7QUFLQTtBQUNBO0FBQ0E7Ozs7Ozs7QUFFQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7OztBQUNBOztBQUNBOzs7O0FBQ0E7Ozs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7QUFJQSxTQUFTSyxPQUFULENBQWtCN0gsU0FBbEIsRUFBNkI7QUFDNUJBLFdBQVV4RixJQUFWLENBQWUsVUFBQ0MsQ0FBRCxFQUFJQyxFQUFKLEVBQVc7QUFDekIsTUFBTW1ELFdBQVczRCxFQUFFUSxFQUFGLENBQWpCO0FBQ0EsTUFBSW1ELFNBQVNvQyxZQUFULENBQXNCLGdCQUF0QixDQUFKLEVBQTZDO0FBQzVDLFVBQU8sSUFBUDtBQUNBOztBQUVELE1BQU1oRyxTQUFTQyxFQUFFQyxNQUFGLENBQVMsSUFBVCxFQUFlO0FBQzdCd0QsU0FBTSxNQUR1QjtBQUU3QnRFLGNBQVcscUJBRmtCO0FBRzdCeU8sK0VBSDZCO0FBSTdCQyxpQkFBYyxHQUplO0FBSzdCQyxrQkFBZSxLQUxjO0FBTTdCQyxjQUFXO0FBQ1ZDLGdCQURVLHdCQUNJQyxJQURKLEVBQ1U7QUFBQSx5QkFLZkEsS0FBS3pOLEVBQUwsQ0FBUVYsSUFBUixFQUxlO0FBQUEsU0FFbEIyTSxHQUZrQixpQkFFbEJBLEdBRmtCO0FBQUEsNENBR2xCaEosSUFIa0I7QUFBQSxTQUdsQkEsSUFIa0Isc0NBR1gsTUFIVztBQUFBLDZDQUlsQnlLLEtBSmtCO0FBQUEsU0FJWHBPLElBSlcsdUNBSUosRUFKSTs7QUFNbkIsVUFBS3FPLEVBQUwsQ0FBUTNCLElBQVIsQ0FBYWhLLFFBQWIsR0FBd0IsRUFBQ2lLLFFBQUQsRUFBTWhKLFVBQU4sRUFBWTNELFVBQVosRUFBeEI7QUFDQSxLQVJTOztBQVNWc08sc0JBQWtCLDRCQUFZO0FBQzdCLFNBQUkxTyxhQUFhLEtBQUsyTyxnQkFBTCxJQUF5QixFQUExQztBQUNBLFNBQUkzTyxVQUFKLEVBQWdCO0FBQ2YsNkJBQWE0TyxJQUFiLENBQWtCNU8sVUFBbEI7O0FBRUEsVUFBSU0sRUFBRSxjQUFGLEVBQWtCSixNQUFsQixHQUEyQixDQUEvQixFQUFrQztBQUFBLFdBUXhCMk8sUUFSd0IsR0FRakMsU0FBU0EsUUFBVCxDQUFrQkMsS0FBbEIsRUFBeUI7QUFDeEJDLGFBQUtDLElBQUwsQ0FBVSxVQUFWLEVBQXNCLEtBQXRCOztBQUVBLFlBQUksQ0FBQ0QsS0FBS0MsSUFBTCxDQUFVLFNBQVYsQ0FBRCxJQUF5QixDQUFDQyxJQUFJRCxJQUFKLENBQVMsU0FBVCxDQUExQixJQUFpRCxDQUFDRSxLQUFLRixJQUFMLENBQVUsU0FBVixDQUF0RCxFQUEyRTtBQUMxRUQsY0FBS0MsSUFBTCxDQUFVLFVBQVYsRUFBc0IsSUFBdEI7QUFDQTtBQUNELFFBZGdDOztBQUNqQzs7QUFFQSxXQUFJRCxPQUFPek8sRUFBRSxvQkFBRixDQUFYO0FBQUEsV0FDQzJPLE1BQU0zTyxFQUFFLG1CQUFGLENBRFA7QUFBQSxXQUVDNE8sT0FBTzVPLEVBQUUsb0JBQUYsQ0FGUjtBQUFBLFdBR0M2TyxZQUFZN08sRUFBRSxnQkFBRixDQUhiOztBQWFBeU8sWUFBS3RILEVBQUwsQ0FBUSxPQUFSLEVBQWlCLFlBQVk7QUFDNUJvSDtBQUNBSSxZQUFJRCxJQUFKLENBQVMsU0FBVCxFQUFvQixLQUFwQjtBQUNBRSxhQUFLRixJQUFMLENBQVUsU0FBVixFQUFxQixLQUFyQjs7QUFHQSxZQUFJRCxLQUFLQyxJQUFMLENBQVUsU0FBVixDQUFKLEVBQXlCO0FBQ3hCRyxtQkFBVTNMLElBQVY7QUFDQSxTQUZELE1BRU87QUFDTjJMLG1CQUFVNUosSUFBVixHQUFpQnpCLElBQWpCLENBQXNCLE9BQXRCLEVBQStCdUosR0FBL0IsQ0FBbUMsRUFBbkM7QUFDQTtBQUNELFFBWEQ7O0FBYUE0QixXQUFJeEgsRUFBSixDQUFPLE9BQVAsRUFBZ0IsWUFBWTtBQUMzQm9IO0FBQ0FFLGFBQUtDLElBQUwsQ0FBVSxTQUFWLEVBQXFCLEtBQXJCO0FBQ0FHLGtCQUFVNUosSUFBVixHQUFpQnpCLElBQWpCLENBQXNCLE9BQXRCLEVBQStCdUosR0FBL0IsQ0FBbUMsRUFBbkM7QUFDQSxRQUpEOztBQU1BNkIsWUFBS3pILEVBQUwsQ0FBUSxPQUFSLEVBQWlCLFlBQVk7QUFDNUJvSDtBQUNBRSxhQUFLQyxJQUFMLENBQVUsU0FBVixFQUFxQixLQUFyQjtBQUNBRyxrQkFBVTVKLElBQVYsR0FBaUJ6QixJQUFqQixDQUFzQixPQUF0QixFQUErQnVKLEdBQS9CLENBQW1DLEVBQW5DO0FBQ0EsUUFKRDs7QUFNQS9NLFNBQUUsc0JBQUYsRUFBMEJtSCxFQUExQixDQUE2QixPQUE3QixFQUFzQyxZQUFXO0FBQ2hELFlBQUluSCxFQUFFLFdBQUYsRUFBZStNLEdBQWYsR0FBcUJuTixNQUFyQixJQUErQkksRUFBRSxXQUFGLEVBQWUrTSxHQUFmLEdBQXFCbk4sTUFBeEQsRUFBK0Q7QUFDOURJLFdBQUUsc0JBQUYsRUFBMEIwTyxJQUExQixDQUErQixVQUEvQixFQUEyQyxJQUEzQztBQUNBLFNBRkQsTUFFTztBQUNOMU8sV0FBRSxzQkFBRixFQUEwQjhPLFVBQTFCLENBQXFDLFVBQXJDLEVBQWlEaE8sV0FBakQsQ0FBNkQsV0FBN0Q7QUFDQTtBQUNELFFBTkQ7QUFPQTs7QUFFRCxVQUFJZCxFQUFFLHNCQUFGLEVBQTBCSixNQUExQixHQUFtQyxDQUF2QyxFQUEwQztBQUFBLFdBSWhDc0osbUJBSmdDLEdBSXpDLFNBQVNBLG1CQUFULEdBQThCOztBQUU3QixZQUFJQyxjQUFjbkosRUFBRSw2Q0FBRixFQUFpRHFILEtBQWpELENBQXVEO0FBQ3hFRSx1QkFBYyxDQUQwRDtBQUV4RUUsbUJBQVUsS0FGOEQ7QUFHeEVJLGVBQU0sSUFIa0U7QUFJeEVILGVBQU0sS0FKa0U7QUFLeEVDLGlCQUFRLEtBTGdFO0FBTXhFeUIsd0JBQWUsS0FOeUQ7QUFPeEVDLG1CQUFVO0FBUDhELFNBQXZELENBQWxCOztBQVVBaEQsMEJBQWtCZ0IsS0FBbEIsQ0FBd0I7QUFDdkJFLHVCQUFjLENBRFM7QUFFdkJDLHlCQUFnQixDQUZPO0FBR3ZCNkIsbUJBQVUsNkNBSGE7QUFJdkJDLHdCQUFlLElBSlE7QUFLdkJDLG1CQUFVLElBTGE7QUFNdkI5QixtQkFBVSxLQU5hO0FBT3ZCVSxvQkFBVyxnRUFQWTtBQVF2QkMsb0JBQVcsZ0VBUlk7O0FBVXZCQyxxQkFBWSxDQUNYO0FBQ0NDLHNCQUFZLEdBRGI7QUFFQzlGLG9CQUFVO0FBQ1QrRyxxQkFBVSxLQUREO0FBRVRoQyx5QkFBYyxDQUZMO0FBR1RFLHFCQUFVO0FBSEQ7QUFGWCxVQURXLEVBUVI7QUFDRmEsc0JBQVksR0FEVjtBQUVGOUYsb0JBQVU7QUFDVCtHLHFCQUFVLEtBREQ7QUFFVGhDLHlCQUFjLENBRkw7QUFHVEUscUJBQVU7QUFIRDtBQUZSLFVBUlE7QUFWVyxTQUF4Qjs7QUE2QkEwQixvQkFBWWhDLEVBQVosQ0FBZSxjQUFmLEVBQStCLFVBQVVDLEtBQVYsRUFBaUJDLEtBQWpCLEVBQXdCbUMsWUFBeEIsRUFBc0NDLFNBQXRDLEVBQWlELENBRS9FLENBRkQ7O0FBSUE7QUFDQSxRQWxEd0M7O0FBRXpDLFdBQUlwRCxvQkFBb0JyRyxFQUFFLHNCQUFGLEVBQTBCd0QsSUFBMUIsQ0FBK0Isd0JBQS9CLENBQXhCOztBQWtEQTBGOztBQUVBbEosU0FBRSx1QkFBRixFQUEyQitPLEdBQTNCLENBQStCLE9BQS9CLEVBQXdDLFlBQVc7QUFDbEQvTyxVQUFFLDRCQUFGLEVBQWdDTSxJQUFoQyxDQUFxQyxZQUFXO0FBQy9DLGFBQUkwTyxjQUFjaFAsRUFBRSxJQUFGLEVBQVF5RyxLQUFSLEtBQWtCekcsRUFBRSxJQUFGLEVBQVFpUCxNQUFSLEVBQXBDO0FBQ0FqUCxXQUFFLElBQUYsRUFBUUYsSUFBUixDQUFhLGNBQWIsRUFBNkJrUCxXQUE3Qjs7QUFFQSxhQUFJQSxjQUFjLENBQWxCLEVBQXFCO0FBQ3BCaFAsWUFBRSxJQUFGLEVBQVFtTSxPQUFSLENBQWdCLHdCQUFoQixFQUEwQzFMLFFBQTFDLENBQW1ELFdBQW5EO0FBQ0EsVUFGRCxNQUVPLElBQUl1TyxjQUFjLENBQWxCLEVBQXFCO0FBQzNCaFAsWUFBRSxJQUFGLEVBQVFtTSxPQUFSLENBQWdCLHdCQUFoQixFQUEwQzFMLFFBQTFDLENBQW1ELFVBQW5EO0FBQ0E7QUFDRCxTQVREO0FBVUEsUUFYRDtBQWNBOztBQUVELFVBQUlULEVBQUUsa0JBQUYsRUFBc0JKLE1BQXRCLEdBQStCLENBQW5DLEVBQXNDO0FBQ3JDSSxTQUFFLDZCQUFGLEVBQWlDbUgsRUFBakMsQ0FBb0MsT0FBcEMsRUFBNkMsWUFBVztBQUN2RHhHLG1CQUFXLFlBQVc7QUFDckJYLFdBQUUsd0JBQUYsRUFBNEIwTixPQUE1QixDQUFvQyxPQUFwQztBQUNBLFNBRkQsRUFFRyxHQUZIO0FBR0EsUUFKRDtBQUtBOztBQUVELFVBQUkxTixFQUFFLGVBQUYsRUFBbUJKLE1BQW5CLEdBQTRCLENBQWhDLEVBQW1DO0FBQ2xDO0FBQ0E7QUFDRDs7QUFFREksT0FBRSxNQUFGLEVBQVVtSCxFQUFWLENBQWEsT0FBYixFQUFzQix1REFBdEIsRUFBK0UsWUFBVztBQUN6Rm5ILFFBQUUsMEJBQUYsRUFBOEIwTixPQUE5QixDQUFzQyxPQUF0QztBQUNBLE1BRkQ7QUFHQTtBQXRKUztBQU5rQixHQUFmLEVBOEpaO0FBQ0Y7QUFERSxHQTlKWSxDQUFmOztBQWtLQS9KLFdBQVN1TCxhQUFULENBQXVCblAsTUFBdkI7QUFDQSxFQXpLRDtBQTBLQTs7QUFFRDs7OztBQUlBLFNBQVNvUCxTQUFULENBQW9CckosU0FBcEIsRUFBK0I7QUFDOUJBLFdBQVV4RixJQUFWLENBQWUsVUFBQ0MsQ0FBRCxFQUFJQyxFQUFKLEVBQVc7QUFDekIsTUFBTW1ELFdBQVczRCxFQUFFUSxFQUFGLENBQWpCO0FBQ0EsTUFBSW1ELFNBQVNvQyxZQUFULENBQXNCLGtCQUF0QixDQUFKLEVBQStDO0FBQzlDLFVBQU8sSUFBUDtBQUNBOztBQUVELE1BQU1oRyxTQUFTQyxFQUFFQyxNQUFGLENBQVMsSUFBVCxFQUFlO0FBQzdCd0QsU0FBTSxRQUR1QjtBQUU3Qm9LLGlCQUFjLEdBRmU7QUFHN0IxTyxjQUFXLHFCQUhrQjtBQUk3QmlRLG1CQUFnQjtBQUphLEdBQWYsRUFLWjtBQUNGO0FBREUsR0FMWSxDQUFmOztBQVNBekwsV0FBU3VMLGFBQVQsQ0FBdUJuUCxNQUF2QjtBQUNBLEVBaEJEO0FBaUJBOztBQUVEOzs7O0FBSUEsU0FBU3NQLFNBQVQsQ0FBb0J2SixTQUFwQixFQUErQjtBQUM5QkEsV0FBVXhGLElBQVYsQ0FBZSxVQUFDQyxDQUFELEVBQUlDLEVBQUosRUFBVztBQUN6QixNQUFNbUQsV0FBVzNELEVBQUVRLEVBQUYsQ0FBakI7QUFDQSxNQUFJbUQsU0FBU29DLFlBQVQsQ0FBc0Isa0JBQXRCLENBQUosRUFBK0M7QUFDOUMsVUFBTyxJQUFQO0FBQ0E7O0FBRUQsTUFBTWhHLFNBQVNDLEVBQUVDLE1BQUYsQ0FBUyxJQUFULEVBQWU7QUFDN0J3RCxTQUFNLFFBRHVCO0FBRTdCb0ssaUJBQWMsR0FGZTtBQUc3QjFPLGNBQVcscUJBSGtCO0FBSTdCaVEsbUJBQWdCO0FBSmEsR0FBZixFQUtaO0FBQ0Y7QUFERSxHQUxZLENBQWY7O0FBU0F6TCxXQUFTdUwsYUFBVCxDQUF1Qm5QLE1BQXZCO0FBQ0EsRUFoQkQ7QUFpQkE7O0FBRUQ7Ozs7QUFJQSxTQUFTdVAsVUFBVCxDQUFxQnhKLFNBQXJCLEVBQWdDO0FBQy9CQSxXQUFVeEYsSUFBVixDQUFlLFVBQUNDLENBQUQsRUFBSUMsRUFBSixFQUFXO0FBQ3pCLE1BQUltRCxXQUFXM0QsRUFBRVEsRUFBRixDQUFmO0FBQ0EsTUFBSW1ELFNBQVNvQyxZQUFULENBQXNCLG1CQUF0QixDQUFKLEVBQWdEO0FBQy9DLFVBQU8sSUFBUDtBQUNBOztBQUVELE1BQU1oRyxTQUFTQyxFQUFFQyxNQUFGLENBQVMsSUFBVCxFQUFlO0FBQzdCd0QsU0FBTSxPQUR1QjtBQUU3Qm9LLGlCQUFjLEdBRmU7QUFHN0IxTyxjQUFXLHFCQUhrQjtBQUk3Qm9RLGFBQVUsZ0JBSm1CO0FBSzdCM0IsK0VBTDZCO0FBTTdCd0IsbUJBQWdCLElBTmE7QUFPN0JJLFlBQVM7QUFDUkMsYUFBUyxJQUREO0FBRVJDLGFBQVMsQ0FBQyxDQUFELEVBQUksQ0FBSixDQUZEO0FBR1JDLHdCQUFvQixJQUhaO0FBSVJDLGlCQUFhO0FBSkw7QUFQb0IsR0FBZixFQWFaO0FBQ0Y7QUFERSxHQWJZLENBQWY7O0FBaUJBak0sV0FBU3VMLGFBQVQsQ0FBdUJuUCxNQUF2QjtBQUNBLEVBeEJEO0FBeUJBOztBQUVEO0FBQ0E7QUFDQTs7UUFFUTROLE8sR0FBQUEsTztRQUFTd0IsUyxHQUFBQSxTO1FBQVdFLFMsR0FBQUEsUztRQUFXQyxVLEdBQUFBLFU7Ozs7Ozs7QUM3UnZDOztBQUVBO0FBQ0E7QUFDQTs7Ozs7a0JBYXdCTyxNOztBQVh4Qjs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7O0FBS2UsU0FBU0EsTUFBVCxHQUFrQjtBQUNoQzdQLElBQUUsV0FBRixFQUFlTSxJQUFmLENBQW9CLFlBQVc7QUFDOUJOLE1BQUUsSUFBRixFQUFROFAsTUFBUjtBQUNBLEdBRkQ7QUFHQSxFOzs7Ozs7O0FDckJEOztBQUVBO0FBQ0E7QUFDQTs7Ozs7OztBQUVBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQSxTQUFTQyxhQUFULEdBQTBCO0FBQ3pCL1AsR0FBRSxjQUFGLEVBQWtCZ1EsVUFBbEIsQ0FBNkI7QUFDNUJDLFdBQVMsSUFBSTlGLElBQUo7QUFEbUIsRUFBN0I7O0FBSUFuSyxHQUFFLGNBQUYsRUFBa0JtSCxFQUFsQixDQUFxQixPQUFyQixFQUE4QixZQUFXO0FBQ3hDbkgsSUFBRSxJQUFGLEVBQVErTSxHQUFSLENBQVkvTSxFQUFFLElBQUYsRUFBUStNLEdBQVIsR0FBYy9CLE9BQWQsQ0FBc0Isc0JBQXRCLEVBQThDLEVBQTlDLENBQVo7QUFDQSxFQUZEO0FBR0E7O0FBRUQ7QUFDQTtBQUNBOztRQUVRK0UsYSxHQUFBQSxhOzs7Ozs7Ozs7OztBQzFCUjs7QUFFQTs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpQkE7QUFDQTtBQUNBOztBQUVBOzs7Ozs7OztBQUlBLFNBQVNHLFdBQVQsR0FBMEM7QUFBQSxLQUFwQkMsWUFBb0IsdUVBQUwsR0FBSzs7QUFDekMsS0FBSUMsVUFBVSxJQUFkO0FBRHlDLGVBRVIxUCxNQUZRO0FBQUEsS0FFcEMyUCxZQUZvQyxXQUVwQ0EsWUFGb0M7QUFBQSxLQUV0QjFQLFVBRnNCLFdBRXRCQSxVQUZzQjs7QUFJekM7Ozs7OztBQUtBLFVBQVNULEtBQVQsQ0FBZ0JvUSxFQUFoQixFQUEwQztBQUFBLE1BQXRCQyxLQUFzQix1RUFBZEosWUFBYzs7QUFDekNFLGVBQWFELE9BQWI7QUFDQUEsWUFBVXpQLFdBQVcyUCxFQUFYLEVBQWVDLEtBQWYsQ0FBVjtBQUNBOztBQUVEclEsT0FBTXNRLEtBQU4sR0FBYztBQUFBLFNBQU1ILGFBQWFELE9BQWIsQ0FBTjtBQUFBLEVBQWQ7QUFDQSxRQUFPbFEsS0FBUDtBQUNBOztBQUVEO0FBQ0E7QUFDQTs7a0JBRWVnUSxXOzs7Ozs7Ozs7Ozs7Ozs7O0FDakRmOztBQUVBOzs7O0FBSUE7QUFDQTtBQUNBOztBQUVBOztBQUNBOztBQUNBOztBQUVBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBV0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOzs7O0FBSUE7QUFDQTtBQUNBOztBQUVBeFAsT0FBTytQLE1BQVAsQ0FBYy9GLFFBQWQsRUFBd0JnRyxLQUF4QixDQUE4QixhQUFLO0FBQ2xDLHdCQUFhcEMsSUFBYixDQUFrQnRPLEVBQUUwSyxRQUFGLENBQWxCO0FBQ0Esc0JBQVdTLFFBQVgsR0FGa0MsQ0FFWDtBQUN2QixrQkFBT21ELElBQVA7QUFDQTtBQUNBLDZCQUFTdE8sRUFBRSxVQUFGLENBQVQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSw2QkFBYyxvQkFBZCxFQUFvQyxnQkFBcEMsRUFBc0Qsc0JBQXREO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsNkJBQWMsZ0JBQWQsRUFBZ0MsWUFBaEMsRUFBOEMsbUJBQTlDO0FBQ0EsNkJBQWMsb0JBQWQsRUFBb0MsZ0JBQXBDLEVBQXNELHVCQUF0RDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0c7QUFDQTtBQUNBO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0F2REQ7O0FBeURBLElBQUksS0FBSixFQUFtQjtBQUNsQixRQUFPLHNCQUFQO0FBQ0EsQzs7Ozs7Ozs7Ozs7OztBQzFIRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsRUFBRTtBQUNGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxQzs7Ozs7Ozs7QUN0RkE7O0FBRUE7Ozs7O0FBS0E7QUFDQTtBQUNBOztBQUVBOzs7O0FBQ0E7Ozs7OztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7OztBQU9BLFNBQVMyUSxlQUFULENBQTBCbE4sSUFBMUIsRUFBZ0NtTixvQkFBaEMsRUFBc0Q7QUFDckQsS0FBSUMsaUJBQUo7QUFDQSxTQUFRcE4sSUFBUjtBQUNDLE9BQUssWUFBTDtBQUNBLE9BQUssaUJBQUw7QUFDQ29OLGNBQVcsU0FBWDtBQUNBO0FBQ0QsT0FBSyxPQUFMO0FBQ0EsT0FBSyxVQUFMO0FBQ0NBLGNBQVcsVUFBWDtBQUNBO0FBQ0QsT0FBSyxNQUFMO0FBQ0NBLGNBQVdELHVCQUF1QixRQUF2QixHQUFrQyxPQUE3QztBQUNBO0FBQ0Q7QUFDQ0MsY0FBVyxFQUFYO0FBYkY7QUFlQSxRQUFPQSxRQUFQO0FBQ0E7O0FBRUQ7Ozs7Ozs7QUFPQSxTQUFTQyxvQkFBVCxDQUErQnpPLE9BQS9CLEVBQXdDME8sTUFBeEMsRUFBZ0Q7QUFDL0MsS0FBSWhRLFFBQVFzQixRQUFRdEIsS0FBcEI7QUFDQSxLQUFJaVEsbUJBQUo7QUFDQSxTQUFRRCxNQUFSO0FBQ0MsT0FBSyxVQUFMO0FBQ0EsT0FBSyxXQUFMO0FBQ0EsT0FBSyxXQUFMO0FBQ0EsT0FBSyxhQUFMO0FBQ0NDLGdCQUFhRCxTQUFTSixnQkFBZ0J0TyxRQUFRb0IsSUFBeEIsRUFBOEJwQixRQUFRK0IsUUFBdEMsQ0FBdEI7QUFDQTtBQUNELE9BQUssVUFBTDtBQUNDLE9BQUksWUFBWUQsSUFBWixDQUFpQnBELEtBQWpCLENBQUosRUFBNkI7QUFDNUJpUSxpQkFBYUQsU0FBUyxRQUF0QjtBQUNBLElBRkQsTUFFTztBQUNOQyxpQkFBYUQsTUFBYjtBQUNBO0FBQ0Q7QUFDRCxPQUFLLEtBQUw7QUFDQyxPQUFJLENBQUMscUJBQXFCNU0sSUFBckIsQ0FBMEJwRCxLQUExQixDQUFMLEVBQXVDO0FBQ3RDaVEsaUJBQWFELFNBQVMsV0FBdEI7QUFDQSxJQUZELE1BRU8sSUFBSSxDQUFDLGlCQUFpQjVNLElBQWpCLENBQXNCcEQsS0FBdEIsQ0FBTCxFQUFtQztBQUN6Q2lRLGlCQUFhRCxTQUFTLFFBQXRCO0FBQ0EsSUFGTSxNQUVBLElBQUksQ0FBQyxXQUFXNU0sSUFBWCxDQUFnQnBELEtBQWhCLENBQUwsRUFBNkI7QUFDbkNpUSxpQkFBYUQsU0FBUyxlQUF0QjtBQUNBLElBRk0sTUFFQTtBQUNOQyxpQkFBYUQsTUFBYjtBQUNBO0FBQ0Q7QUFDRCxPQUFLLE9BQUw7QUFDQyxPQUFJLFlBQVk1TSxJQUFaLENBQWlCcEQsS0FBakIsQ0FBSixFQUE2QjtBQUM1QmlRLGlCQUFhRCxTQUFTLE1BQXRCO0FBQ0EsSUFGRCxNQUVPLElBQUksYUFBYTVNLElBQWIsQ0FBa0JwRCxLQUFsQixDQUFKLEVBQThCO0FBQ3BDaVEsaUJBQWFELFNBQVMsUUFBdEI7QUFDQSxJQUZNLE1BRUEsSUFBSSxDQUFDLElBQUk1TSxJQUFKLENBQVNwRCxLQUFULENBQUwsRUFBc0I7QUFDNUJpUSxpQkFBYUQsU0FBUyxLQUF0QjtBQUNBLElBRk0sTUFFQSxJQUFJLEtBQUs1TSxJQUFMLENBQVVwRCxLQUFWLENBQUosRUFBc0I7QUFDNUJpUSxpQkFBYUQsU0FBUyxXQUF0QjtBQUNBLElBRk0sTUFFQSxJQUFJaFEsTUFBTWtRLEtBQU4sQ0FBWSxHQUFaLEVBQWlCclIsTUFBakIsR0FBMEIsQ0FBOUIsRUFBaUM7QUFDdkNvUixpQkFBYUQsU0FBUyxjQUF0QjtBQUNBLElBRk0sTUFFQSxJQUFJLEtBQUs1TSxJQUFMLENBQVVwRCxLQUFWLENBQUosRUFBc0I7QUFDNUJpUSxpQkFBYUQsU0FBUyxTQUF0QjtBQUNBLElBRk0sTUFFQTtBQUNOQyxpQkFBYUQsTUFBYjtBQUNBO0FBQ0Q7QUFDRDtBQUNDQyxnQkFBYUQsTUFBYjtBQTNDRjtBQTZDQSxRQUFPQyxVQUFQO0FBQ0E7O0FBRUQ7QUFDQTtBQUNBOztBQUVBOzs7O0FBSUEsSUFBTUUsZUFBZTtBQUNwQjs7Ozs7QUFLQSxLQUFJQyxRQUFKLEdBQWdCO0FBQ2YsTUFBSUMsYUFBYzFRLE9BQU8yUSxjQUFQLElBQXlCM1EsT0FBTzJRLGNBQVAsQ0FBc0IsbUJBQXRCLENBQTFCLElBQXlFLEVBQTFGO0FBQ0EsTUFBSUMsT0FBT0MsSUFBUCxDQUFZSCxVQUFaLEVBQXdCeFIsTUFBeEIsS0FBbUMsQ0FBdkMsRUFBMEM7QUFDekMsVUFBT1EsUUFBUUMsSUFBUixDQUFhLCtDQUFiLENBQVA7QUFDQTs7QUFFRCxPQUFLLElBQUkwSixHQUFULElBQWdCcUgsVUFBaEIsRUFBNEI7QUFDM0IsT0FBSXJRLFFBQVFxUSxXQUFXckgsR0FBWCxDQUFaOztBQUVBLFdBQVFBLEdBQVI7QUFDQyxTQUFLLFdBQUw7QUFDQSxTQUFLLG1CQUFMO0FBQ0EsU0FBSyxrQkFBTDtBQUNBLFNBQUssV0FBTDtBQUNBLFNBQUssbUJBQUw7QUFDQSxTQUFLLGtCQUFMO0FBQ0EsU0FBSyxhQUFMO0FBQ0EsU0FBSyxxQkFBTDtBQUNBLFNBQUssb0JBQUw7QUFDQSxTQUFLLE9BQUw7QUFDQSxTQUFLLEtBQUw7QUFDQSxTQUFLLEtBQUw7QUFDQSxTQUFLLFVBQUw7QUFDQSxTQUFLLFVBQUw7QUFDQSxTQUFLLGNBQUw7QUFDQSxTQUFLLFNBQUw7QUFDQ3FILGdCQUFXckgsR0FBWCxJQUFrQi9KLEVBQUV3UixTQUFGLENBQVlDLE1BQVosQ0FBbUIxUSxLQUFuQixDQUFsQjtBQUNBO0FBbEJGO0FBb0JBOztBQUVELFNBQU9xUSxVQUFQO0FBQ0EsRUF0Q21COztBQXdDcEI7Ozs7QUFJQU0sWUFBVztBQUNWOzs7Ozs7O0FBT0FDLGdCQVJVLDBCQVFNdFAsT0FSTixFQVFldVAsSUFSZixFQVFxQjtBQUM5QixPQUFJLE9BQU9BLElBQVAsS0FBZ0IsUUFBcEIsRUFBOEI7QUFDN0JBLFdBQU8sRUFBQ2IsUUFBUWEsSUFBVCxFQUFQO0FBQ0E7O0FBRUQsT0FBSUMsVUFBVSxLQUFLQyxXQUFMLENBQ2IsS0FBS0MsYUFBTCxDQUFtQjFQLFFBQVEyUCxJQUEzQixFQUFpQ0osS0FBS2IsTUFBdEMsQ0FEYSxFQUViLEtBQUtrQixpQkFBTCxDQUF1QjVQLE9BQXZCLEVBQWdDdVAsS0FBS2IsTUFBckMsQ0FGYSxFQUdiLENBQUMsS0FBS3ZPLFFBQUwsQ0FBYzBQLFdBQWYsSUFBOEI3UCxRQUFROFAsS0FBdEMsSUFBK0NsSCxTQUhsQyxFQUc2QztBQUMxRGpMLEtBQUV3UixTQUFGLENBQVlMLFFBQVosQ0FBcUJMLHFCQUFxQnpPLE9BQXJCLEVBQThCdVAsS0FBS2IsTUFBbkMsQ0FBckIsQ0FKYSxFQUtiLDZDQUE2QzFPLFFBQVEyUCxJQUFyRCxHQUE0RCxXQUwvQyxDQUFkOztBQVFBLE9BQUlJLFVBQVUsZUFBZDs7QUFFQSxPQUFJLE9BQU9QLE9BQVAsS0FBbUIsVUFBdkIsRUFBbUM7QUFDbENBLGNBQVVBLFFBQVFRLElBQVIsQ0FBYSxJQUFiLEVBQW1CVCxLQUFLVSxVQUF4QixFQUFvQ2pRLE9BQXBDLENBQVY7QUFDQSxJQUZELE1BRU8sSUFBSStQLFFBQVFqTyxJQUFSLENBQWEwTixPQUFiLENBQUosRUFBMkI7QUFDakNBLGNBQVU3UixFQUFFd1IsU0FBRixDQUFZQyxNQUFaLENBQW1CSSxRQUFRN0csT0FBUixDQUFnQm9ILE9BQWhCLEVBQXlCLE1BQXpCLENBQW5CLEVBQXFEUixLQUFLVSxVQUExRCxDQUFWO0FBQ0E7O0FBRUQsVUFBT1QsT0FBUDtBQUNBO0FBOUJTLEVBNUNTOztBQTZFcEI7Ozs7QUFJQVU7QUFqRm9CLENBQXJCOztBQW9GQSxLQUFLLElBQUl4SSxHQUFULElBQWdCbUgsWUFBaEIsRUFBOEI7QUFDN0JsUixHQUFFQyxNQUFGLENBQVNELEVBQUV3UixTQUFGLENBQVl6SCxHQUFaLENBQVQsRUFBMkJtSCxhQUFhbkgsR0FBYixDQUEzQjtBQUNBOztBQUVEL0osRUFBRXdSLFNBQUYsQ0FBWWdCLGFBQVosK0I7Ozs7Ozs7QUN2TUE7O0FBRUE7Ozs7O0FBS0E7QUFDQTtBQUNBOztBQUVBOzs7Ozs7Ozs7O0FBTUEsSUFBTUMsYUFBYTtBQUNsQixzQkFBcUI7QUFDcEJDLGFBQVcsQ0FEUztBQUVwQkMsU0FBTyxJQUZhO0FBR3BCQyxhQUFXLENBSFM7QUFJcEJDLFlBQVU7QUFKVSxFQURIO0FBT2xCLDRCQUEyQjtBQUMxQkMsWUFBVSxtQkFEZ0I7QUFFMUI7QUFDQUMsWUFBVSxHQUhnQjtBQUkxQkMsYUFBVyxDQUplO0FBSzFCSCxZQUFVO0FBTGdCO0FBUFQsQ0FBbkI7O0FBZ0JBO0FBQ0E7QUFDQTs7a0JBRWVKLFU7Ozs7Ozs7QUNyQ2Y7O0FBRUE7Ozs7O0FBS0E7QUFDQTtBQUNBOztBQUVBOzs7Ozs7OztBQUlBLElBQU1GLFVBQVU7QUFDZjs7Ozs7Ozs7O0FBU0FVLE1BVmUsaUJBVVJsUyxLQVZRLEVBVURzQixPQVZDLEVBVVE7QUFDdEIsTUFBSStQLFVBQVUsMkhBQWQ7QUFDQSxTQUFPLEtBQUtjLFFBQUwsQ0FBYzdRLE9BQWQsS0FBMEIrUCxRQUFRak8sSUFBUixDQUFhcEQsS0FBYixDQUFqQztBQUNBLEVBYmM7OztBQWVmOzs7Ozs7Ozs7QUFTQXFSLFFBeEJlLG1CQXdCTnJSLEtBeEJNLEVBd0JDc0IsT0F4QkQsRUF3QlU2TCxLQXhCVixFQXdCaUI7QUFDL0IsTUFBSSxLQUFLZ0YsUUFBTCxDQUFjN1EsT0FBZCxDQUFKLEVBQTRCO0FBQzNCLFVBQU8sSUFBUDtBQUNBO0FBQ0QsTUFBSSxPQUFPNkwsS0FBUCxLQUFpQixRQUFyQixFQUErQjtBQUM5QkEsV0FBUSxJQUFJbkQsTUFBSixVQUFrQm1ELEtBQWxCLFFBQVI7QUFDQTtBQUNELFNBQU9BLE1BQU0vSixJQUFOLENBQVdwRCxLQUFYLENBQVA7QUFDQSxFQWhDYzs7O0FBa0NmOzs7Ozs7Ozs7QUFTQW9TLFNBM0NlLG9CQTJDTHBTLEtBM0NLLEVBMkNFc0IsT0EzQ0YsRUEyQ1c7QUFDekIsU0FBTyxLQUFLNlEsUUFBTCxDQUFjN1EsT0FBZCxLQUEwQixTQUFTOEIsSUFBVCxDQUFjcEQsS0FBZCxDQUFqQztBQUNBLEVBN0NjOzs7QUErQ2Y7Ozs7Ozs7OztBQVNBZ1MsU0F4RGUsb0JBd0RMaFMsS0F4REssRUF3REVzQixPQXhERixFQXdEVzZMLEtBeERYLEVBd0RrQjtBQUNoQyxNQUFJa0YsS0FBSyxDQUFUO0FBQ0EsT0FBSyxJQUFJN1MsSUFBSSxDQUFiLEVBQWdCQSxJQUFJOEIsUUFBUTBCLEtBQVIsQ0FBY25FLE1BQWxDLEVBQTBDVyxHQUExQyxFQUErQztBQUM5QzZTLFNBQU0vUSxRQUFRMEIsS0FBUixDQUFjeEQsQ0FBZCxFQUFpQmdGLElBQXZCO0FBQ0E7QUFDRCxTQUFPLEtBQUsyTixRQUFMLENBQWM3USxPQUFkLEtBQTJCK1EsS0FBSyxJQUFMLElBQWFsRixLQUEvQztBQUNBLEVBOURjOzs7QUFnRWY7Ozs7Ozs7OztBQVNBOEUsVUF6RWUscUJBeUVKalMsS0F6RUksRUF5RUdzQixPQXpFSCxFQXlFWTZMLEtBekVaLEVBeUVtQjtBQUNqQyxTQUFPN0wsUUFBUTBCLEtBQVIsQ0FBY25FLE1BQWQsSUFBd0JzTyxLQUEvQjtBQUNBLEVBM0VjOzs7QUE2RWY7Ozs7Ozs7OztBQVNBbUYsYUF0RmUsd0JBc0ZEdFMsS0F0RkMsRUFzRk1zQixPQXRGTixFQXNGZTZMLEtBdEZmLEVBc0ZzQjtBQUNwQyxNQUFJb0YsT0FBTyxJQUFYO0FBQ0EsT0FBSyxJQUFJL1MsSUFBSSxDQUFiLEVBQWdCQSxJQUFJOEIsUUFBUTBCLEtBQVIsQ0FBY25FLE1BQWxDLEVBQTBDVyxHQUExQyxFQUErQztBQUM5QyxPQUFJOEIsUUFBUTBCLEtBQVIsQ0FBY3hELENBQWQsRUFBaUJnRixJQUFqQixHQUF3QixJQUF4QixHQUErQjJJLEtBQW5DLEVBQTBDO0FBQ3pDb0YsV0FBTyxLQUFQO0FBQ0E7QUFDQTtBQUNEO0FBQ0QsU0FBTyxLQUFLSixRQUFMLENBQWM3USxPQUFkLEtBQTBCaVIsSUFBakM7QUFDQSxFQS9GYzs7O0FBaUdmOzs7Ozs7Ozs7QUFTQVIsU0ExR2Usb0JBMEdML1IsS0ExR0ssRUEwR0VzQixPQTFHRixFQTBHVzZMLEtBMUdYLEVBMEdrQjtBQUNoQyxNQUFJaEIsZUFBSjtBQUNBLE1BQUlxRyxhQUFhLHNFQUFqQjtBQUNBLE1BQUlsUixRQUFRMEIsS0FBUixDQUFjbkUsTUFBZCxHQUF1QixDQUEzQixFQUE4QjtBQUM3QixVQUFPLElBQVA7QUFDQTs7QUFFRHNPLFVBQVEsT0FBT0EsS0FBUCxLQUFpQixRQUFqQixHQUE0QkEsTUFBTWxELE9BQU4sQ0FBYyxJQUFkLEVBQW9CLEdBQXBCLENBQTVCLEdBQXVEdUksVUFBL0Q7QUFDQSxNQUFJbFIsUUFBUStCLFFBQVosRUFBc0I7QUFDckIsT0FBSUwsUUFBUTFCLFFBQVEwQixLQUFwQjtBQUNBLFFBQUssSUFBSXhELElBQUksQ0FBYixFQUFnQkEsSUFBSXdELE1BQU1uRSxNQUExQixFQUFrQ1csR0FBbEMsRUFBdUM7QUFDdEMsUUFBSVEsU0FBUWdELE1BQU14RCxDQUFOLEVBQVN5UixJQUFyQjtBQUNBLFFBQUl3QixhQUFhelMsT0FBTStKLEtBQU4sQ0FBWSxJQUFJQyxNQUFKLENBQVcsT0FBT21ELEtBQVAsR0FBZSxJQUExQixFQUFnQyxHQUFoQyxDQUFaLENBQWpCOztBQUVBaEIsYUFBUyxLQUFLZ0csUUFBTCxDQUFjN1EsT0FBZCxLQUEwQm1SLFVBQW5DO0FBQ0EsUUFBSXRHLFdBQVcsSUFBZixFQUFxQjtBQUNwQjtBQUNBO0FBQ0Q7QUFDRCxHQVhELE1BV087QUFDTixPQUFJc0csY0FBYXpTLE1BQU0rSixLQUFOLENBQVksSUFBSUMsTUFBSixDQUFXLFNBQVNtRCxLQUFULEdBQWlCLElBQTVCLEVBQWtDLEdBQWxDLENBQVosQ0FBakI7QUFDQWhCLFlBQVMsS0FBS2dHLFFBQUwsQ0FBYzdRLE9BQWQsS0FBMEJtUixXQUFuQztBQUNBO0FBQ0QsU0FBT3RHLE1BQVA7QUFDQSxFQWxJYzs7O0FBb0lmOzs7Ozs7Ozs7O0FBVUF1RyxHQTlJZSxjQThJWDFTLEtBOUlXLEVBOElKc0IsT0E5SUksRUE4SUs2TCxLQTlJTCxFQThJWTtBQUMxQixNQUFJd0YsVUFBVTFULEVBQUVxQyxRQUFRUyxJQUFWLENBQWQ7QUFDQSxTQUFPNFEsUUFBUWxRLElBQVIsQ0FBYTBLLFFBQVEsU0FBckIsRUFBZ0N0TyxNQUF2QztBQUNBLEVBakpjOzs7QUFtSmY7Ozs7Ozs7O0FBUUErVCxLQTNKZSxnQkEySlQ1UyxLQTNKUyxFQTJKRnNCLE9BM0pFLEVBMkpPO0FBQ3JCLE1BQUl1UixZQUFZLHVEQUF1RHpQLElBQXZELENBQTREcEQsS0FBNUQsQ0FBaEIsQ0FEcUIsQ0FDK0Q7QUFDcEYsU0FBTyxLQUFLbVMsUUFBTCxDQUFjN1EsT0FBZCxLQUEwQnVSLFNBQWpDO0FBQ0EsRUE5SmM7OztBQWdLZjs7Ozs7Ozs7O0FBU0FqQixNQXpLZSxpQkF5S1I1UixLQXpLUSxFQXlLRHNCLE9BektDLEVBeUtRO0FBQ3RCLE1BQUl1UixZQUFZLGlGQUFpRnpQLElBQWpGLENBQXNGcEQsS0FBdEYsQ0FBaEIsQ0FEc0IsQ0FDd0Y7QUFDOUcsU0FBTyxLQUFLbVMsUUFBTCxDQUFjN1EsT0FBZCxLQUEwQnVSLFNBQWpDO0FBQ0EsRUE1S2M7OztBQThLZjs7Ozs7Ozs7QUFRQUMsUUF0TGUsbUJBc0xOOVMsS0F0TE0sRUFzTENzQixPQXRMRCxFQXNMVTtBQUN4QixXQUFTdVIsU0FBVCxDQUFvQjdTLEtBQXBCLEVBQTJCO0FBQzFCLE9BQUksV0FBV29ELElBQVgsQ0FBZ0JwRCxLQUFoQixDQUFKLEVBQTRCO0FBQzNCLFdBQU8sdU1BQXNNb0QsSUFBdE0sQ0FBMk1wRCxLQUEzTTtBQUFQLE1BRDJCLENBQytMO0FBQzFOO0FBQ0QsVUFBTyx1TEFBc0xvRCxJQUF0TCxDQUEyTHBELEtBQTNMO0FBQVAsS0FKMEIsQ0FJZ0w7QUFDMU07O0FBRUQsU0FBTyxLQUFLbVMsUUFBTCxDQUFjN1EsT0FBZCxLQUEwQnVSLFVBQVU3UyxLQUFWLENBQWpDO0FBQ0EsRUEvTGM7OztBQWlNZjs7Ozs7Ozs7O0FBU0ErUyxNQTFNZSxpQkEwTVIvUyxLQTFNUSxFQTBNRHNCLE9BMU1DLEVBME1RO0FBQ3RCLFNBQU8sS0FBSzZRLFFBQUwsQ0FBYzdRLE9BQWQsS0FBMEIsOE1BQThNOEIsSUFBOU0sQ0FBbU5wRCxLQUFuTixDQUFqQyxDQURzQixDQUNzTztBQUM1UCxFQTVNYzs7O0FBOE1mOzs7Ozs7O0FBT0FnVCxRQXJOZSxtQkFxTk5oVCxLQXJOTSxFQXFOQztBQUNmLE1BQUlpVCxNQUFNQyxPQUFPbFQsS0FBUCxFQUFjaUssT0FBZCxDQUFzQixjQUF0QixFQUFzQyxFQUF0QyxDQUFWO0FBQ0EsU0FBT2dKLElBQUlwVSxNQUFKLEdBQWEsQ0FBcEI7QUFDQSxFQXhOYzs7O0FBME5mOzs7Ozs7OztBQVFBc1UsVUFsT2UscUJBa09KblQsS0FsT0ksRUFrT0dzQixPQWxPSCxFQWtPWTtBQUMxQixTQUFPckMsRUFBRXFDLE9BQUYsRUFBV3ZDLElBQVgsQ0FBZ0IsT0FBaEIsTUFBNkIsSUFBcEM7QUFDQTtBQXBPYyxDQUFoQjs7QUF1T0E7QUFDQTtBQUNBOztrQkFFZXlTLE87Ozs7Ozs7QUMxUGY7O0FBRUE7Ozs7O0FBS0E7QUFDQTtBQUNBOztBQUVBOzs7Ozs7Ozs7O0FBTUEsU0FBUzRCLGdCQUFULENBQTJCcFIsS0FBM0IsRUFBa0NzSyxTQUFsQyxFQUE2QztBQUM1QztBQUNBLEtBQU0wRCxTQUFTaE8sTUFBTU8sSUFBTixDQUFXLFFBQVgsS0FBd0IsTUFBdkM7QUFDQVAsT0FBTTJMLElBQU4sQ0FBVyxRQUFYLEVBQXFCcUMsTUFBckI7QUFDQWhPLE9BQU1vRSxFQUFOLENBQVMsUUFBVCxFQUFtQixpQkFBUztBQUMzQixNQUFNaU4sU0FBU3JSLE1BQU1PLElBQU4sQ0FBVyxRQUFYLENBQWY7QUFDQSxNQUFJLENBQUM4USxNQUFMLEVBQWE7QUFDWmhOLFNBQU1pTixjQUFOO0FBQ0E7QUFDRCxFQUxEOztBQU9BO0FBQ0F0UixPQUFNb0UsRUFBTixDQUFTLE9BQVQsRUFBa0I7QUFBQSxTQUFNa0csVUFBVWlILFNBQVYsRUFBTjtBQUFBLEVBQWxCOztBQUVBO0FBQ0F2UixPQUFNb0UsRUFBTixDQUFTLFFBQVQsRUFBbUIsb0JBQW5CLEVBQXlDLFlBQVk7QUFDcERrRyxZQUFVaEwsT0FBVixDQUFrQixJQUFsQjtBQUNBLEVBRkQ7O0FBSUE7QUFDQTs7QUFFRDtBQUNBO0FBQ0E7O2tCQUVlOFIsZ0I7Ozs7Ozs7QUMzQ2Y7O0FBRUE7Ozs7O0FBS0E7QUFDQTtBQUNBOzs7Ozs7QUFFQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7Ozs7O0FBUUEsSUFBSWpGLGdCQUFnQmxQLEVBQUVrUCxhQUFGLENBQWdCcUYsUUFBcEM7QUFDQSxJQUFJOUgsTUFBTSxnQ0FBVjs7QUFFQSxTQUFTK0gsV0FBVCxDQUFzQnpSLEtBQXRCLEVBQTZCOE8sT0FBN0IsRUFBc0M0QyxPQUF0QyxFQUErQztBQUM5QyxLQUFJdkYsY0FBY3dGLE1BQWxCLEVBQTBCO0FBQ3pCLE1BQUlDLFNBQVM1UixNQUFNb0osT0FBTixDQUFjLFFBQWQsQ0FBYjs7QUFFQSxNQUFJbk0sRUFBRSxjQUFGLEVBQWtCSixNQUF0QixFQUE4QjtBQUM3QitVLFVBQU9uUixJQUFQLENBQVksT0FBWixFQUFxQnlCLElBQXJCO0FBQ0EwUCxVQUFPdFIsTUFBUCxDQUFjLGlIQUErR3dPLE9BQS9HLHVNQUFkOztBQUVBN1IsS0FBRSxhQUFGLEVBQWlCbUgsRUFBakIsQ0FBb0IsT0FBcEIsRUFBNkIsWUFBVztBQUN2Q3dOLFdBQU9uUixJQUFQLENBQVksT0FBWixFQUFxQk4sSUFBckI7QUFDQWxELE1BQUUsYUFBRixFQUFpQmEsTUFBakI7QUFDQSxJQUhEO0FBSUEsR0FSRCxNQVFPO0FBQ044VCxVQUFPQyxJQUFQLENBQVksNkRBQTJEL0MsT0FBM0Qsc0VBQVo7QUFDQTs7QUFFRCxTQUFPLEtBQVA7QUFDQTs7QUFFRDdSLEdBQUVrUCxhQUFGLENBQWdCeEssSUFBaEIsQ0FBcUI7QUFDcEJtUSxTQUFPO0FBQ05DLFFBQUtySTtBQURDLEdBRGE7QUFJcEJoSixRQUFNLE1BSmM7QUFLcEJvSyxnQkFBYyxHQUxNO0FBTXBCMU8sYUFBVyxTQU5TO0FBT3BCeU8sOEVBUG9CO0FBUXBCRyxhQUFXO0FBQ1ZLLHFCQUFrQiw0QkFBWTtBQUM3QnBPLE1BQUUsb0JBQUYsRUFBd0I0VSxJQUF4QixDQUE2Qi9DLE9BQTdCO0FBQ0E7QUFIUztBQVJTLEVBQXJCO0FBY0E7O0FBRUQsU0FBU2tELGlCQUFULENBQTRCaFMsS0FBNUIsRUFBbUM4TyxPQUFuQyxFQUE0Q21ELE1BQTVDLEVBQW9EO0FBQ25ELEtBQUk5RixjQUFjd0YsTUFBbEIsRUFBMEI7QUFDekIsTUFBSUMsU0FBUzVSLE1BQU1vSixPQUFOLENBQWMsUUFBZCxDQUFiOztBQUVBd0ksU0FBT0MsSUFBUCxDQUFZLDZEQUEyRC9DLE9BQTNELHlFQUFxSW1ELE1BQWpKOztBQUVBLFNBQU8sS0FBUDtBQUNBOztBQUVEaFYsR0FBRWtQLGFBQUYsQ0FBZ0J4SyxJQUFoQixDQUFxQjtBQUNwQm1RLFNBQU87QUFDTkMsUUFBS3JJO0FBREMsR0FEYTtBQUlwQmhKLFFBQU0sTUFKYztBQUtwQm9LLGdCQUFjLEdBTE07QUFNcEIxTyxhQUFXLFNBTlM7QUFPcEJ5Tyw4RUFQb0I7QUFRcEJHLGFBQVc7QUFDVksscUJBQWtCLDRCQUFZO0FBQzdCcE8sTUFBRSxvQkFBRixFQUF3QjRVLElBQXhCLENBQTZCL0MsT0FBN0I7QUFDQTtBQUhTO0FBUlMsRUFBckI7QUFjQTs7QUFFRCxTQUFTb0QsVUFBVCxHQUF1QjtBQUN0QixLQUFJL0YsY0FBY3dGLE1BQWxCLEVBQTBCO0FBQ3pCMVUsSUFBRWtQLGFBQUYsQ0FBZ0JnRyxLQUFoQjtBQUNBO0FBQ0Q7O0FBRUQsU0FBU0MsR0FBVCxDQUFjQyxHQUFkLEVBQW1CO0FBQ2xCLEtBQU1DLFVBQVUsU0FBVkEsT0FBVSxDQUFDQyxXQUFELEVBQWNDLFlBQWQ7QUFBQSxTQUErQnRPLFNBQVNxTyxXQUFULElBQXdCck8sU0FBU3NPLFlBQVQsQ0FBdkQ7QUFBQSxFQUFoQjs7QUFFQSxRQUFPSCxJQUFJSSxNQUFKLENBQVdILE9BQVgsQ0FBUDtBQUNBOztBQUVEOztBQUVBLFNBQVNJLFlBQVQsQ0FBdUJDLFVBQXZCLEVBQW1DQyxZQUFuQyxFQUFpREMsU0FBakQsRUFBNEQ7QUFDM0RELGdCQUFlQSxnQkFBZ0IsR0FBL0I7QUFDQUMsYUFBWUEsYUFBYSxHQUF6QjtBQUNBRixjQUFhLEtBQUtBLFVBQWxCO0FBQ0FBLGNBQWFBLFdBQVd6RSxLQUFYLENBQWlCMkUsU0FBakIsQ0FBYjs7QUFFQSxLQUFJQyxjQUFjSCxXQUFXLENBQVgsS0FBaUIsRUFBbkM7QUFDQSxLQUFJSSxjQUFjSixXQUFXLENBQVgsQ0FBbEI7O0FBRUEsS0FBSUssaUJBQWlCLEVBQXJCO0FBQ0EsS0FBSUMsVUFBVSxDQUFkOztBQUVBLE1BQUssSUFBSXpWLElBQUl1VixZQUFZbFcsTUFBWixHQUFxQixDQUFsQyxFQUFxQ1csS0FBSyxDQUExQyxFQUE2Q0EsR0FBN0MsRUFBa0Q7QUFDakQsTUFBSTBILE1BQU02TixZQUFZdlYsQ0FBWixDQUFWO0FBQ0F3VixpQkFBZUUsSUFBZixDQUFvQmhPLEdBQXBCOztBQUVBLE1BQUksRUFBRStOLE9BQUYsS0FBYyxDQUFkLElBQW1CelYsQ0FBdkIsRUFBMEI7QUFDekJ3VixrQkFBZUUsSUFBZixDQUFvQixHQUFwQjtBQUNBRCxhQUFVLENBQVY7QUFDQTtBQUNEOztBQUVERCxrQkFBaUJBLGVBQWVHLE9BQWYsR0FBeUJDLElBQXpCLENBQThCLEVBQTlCLENBQWpCO0FBQ0EsS0FBSU4sWUFBWWpXLE1BQWhCLEVBQXdCO0FBQ3ZCLFNBQU8sQ0FBQ21XLGNBQUQsRUFBaUJGLFdBQWpCLEVBQThCTSxJQUE5QixDQUFtQ1IsWUFBbkMsQ0FBUDtBQUNBO0FBQ0QsUUFBT0ksY0FBUDtBQUNBOztBQUVELFNBQVNLLGdCQUFULENBQTJCclIsUUFBM0IsRUFBcUM7QUFDcEMsS0FBSS9FLEVBQUUsaUJBQUYsRUFBcUJKLE1BQXJCLElBQStCLE9BQU9tRixTQUFTc1IsTUFBaEIsS0FBMkIsV0FBOUQsRUFBMkU7QUFDMUVyVyxJQUFFLGlCQUFGLEVBQXFCNFUsSUFBckIsQ0FBMEI3UCxTQUFTc1IsTUFBbkM7O0FBRUEsTUFBSSxDQUFDdFIsU0FBU3VSLEtBQWQsRUFBcUI7QUFDcEJ0VyxLQUFFLGNBQUYsRUFBa0JTLFFBQWxCLENBQTJCLE9BQTNCO0FBQ0FULEtBQUUsb0JBQUYsRUFBd0JjLFdBQXhCLENBQW9DLE9BQXBDO0FBQ0E7O0FBRUQsTUFBSWQsRUFBRSxpQkFBRixFQUFxQkosTUFBckIsSUFBK0IsT0FBT21GLFNBQVN3UixVQUFoQixLQUErQixXQUFsRSxFQUErRTtBQUM5RXZXLEtBQUUsaUJBQUYsRUFBcUIyRixJQUFyQixDQUEwQlosU0FBU3dSLFVBQVQsQ0FBb0JDLE9BQXBCLENBQTRCLENBQTVCLENBQTFCO0FBQ0E7O0FBRUQseUJBQWFsSSxJQUFiLENBQWtCdE8sRUFBRSxpQkFBRixDQUFsQjtBQUNBOztBQUVELEtBQUlBLEVBQUUsVUFBRixFQUFjSixNQUFkLElBQXdCLE9BQU9tRixTQUFTNlAsSUFBaEIsS0FBeUIsV0FBckQsRUFBa0U7QUFDakUsTUFBSTZCLFFBQVF6VyxFQUFFK0UsU0FBUzZQLElBQVgsQ0FBWjtBQUNBNVUsSUFBRSxVQUFGLEVBQWM0VSxJQUFkLENBQW1CNkIsS0FBbkI7O0FBRUEseUJBQWFuSSxJQUFiLENBQWtCdE8sRUFBRSxVQUFGLENBQWxCO0FBQ0E7O0FBRUQsS0FBSUEsRUFBRSxnQkFBRixFQUFvQkosTUFBcEIsSUFBOEIsT0FBT21GLFNBQVN1UixLQUFoQixLQUEwQixXQUE1RCxFQUF5RTtBQUN4RXRXLElBQUUsZ0JBQUYsRUFBb0I0VSxJQUFwQixDQUF5QjdQLFNBQVN1UixLQUFsQztBQUNBO0FBQ0Q7O0FBRUQsU0FBU0ksV0FBVCxDQUFzQjNULEtBQXRCLEVBQTZCNFQsS0FBN0IsRUFBb0NDLE1BQXBDLEVBQTRDO0FBQzNDLEtBQUl4SixRQUFRcE4sRUFBRSxvQkFBRixDQUFaO0FBQ0EsS0FBSTZXLFNBQVN6SixNQUFNNUosSUFBTixDQUFXLHdCQUFYLENBQWI7QUFDQSxLQUFJc1QsWUFBWTFKLE1BQU1qQixPQUFOLENBQWMsZUFBZCxDQUFoQjs7QUFFQSxLQUFJNEssV0FBV0QsVUFBVXRULElBQVYsQ0FBZSx1QkFBZixDQUFmO0FBQ0EsS0FBSXdULGdCQUFnQkYsVUFBVXRULElBQVYsQ0FBZSxtQkFBZixDQUFwQjtBQUNBLEtBQUl5VCxTQUFTSCxVQUFVdFQsSUFBVixDQUFlLG1CQUFmLENBQWI7O0FBRUEsS0FBSThTLFFBQVFuQixJQUFJeUIsTUFBSixDQUFaO0FBQ0EsS0FBSU0sUUFBUUQsT0FBT25YLElBQVAsQ0FBWSxPQUFaLElBQXVCd1csS0FBbkM7O0FBRUFRLFdBQVVoWCxJQUFWLENBQWUsT0FBZixFQUF3QjZXLEtBQXhCO0FBQ0FHLFdBQVVoWCxJQUFWLENBQWUsUUFBZixFQUF5QjhXLE1BQXpCO0FBQ0FFLFdBQVVoWCxJQUFWLENBQWUsT0FBZixFQUF3QndXLEtBQXhCO0FBQ0FTLFVBQVNwUixJQUFULENBQWMyUSxLQUFkO0FBQ0FVLGVBQWNqSyxHQUFkLENBQWtCdUosS0FBbEI7QUFDQVcsUUFBT3RSLElBQVAsQ0FBWThQLGFBQWF5QixNQUFNVixPQUFOLENBQWMsQ0FBZCxDQUFiLENBQVo7O0FBRUFLLFFBQU8vVixXQUFQLENBQW1CLFFBQW5COztBQUVBK1YsUUFBT3ZXLElBQVAsQ0FBWSxVQUFVQyxDQUFWLEVBQWFDLEVBQWIsRUFBaUI7QUFDNUIsTUFBSTJXLFFBQVFuWCxFQUFFLElBQUYsQ0FBWjtBQUNBLE1BQUlvWCxTQUFTcFgsRUFBRSxJQUFGLEVBQVF3RCxJQUFSLENBQWEsbUJBQWIsQ0FBYjs7QUFFQSxNQUFJMEssUUFBUWlKLE1BQU1yWCxJQUFOLENBQVcsT0FBWCxDQUFaO0FBQ0FvTyxRQUFNQSxLQUFOLENBQVkwSSxNQUFaLEdBQXFCQSxNQUFyQjs7QUFFQU8sUUFBTTdULElBQU4sQ0FBVyxZQUFYLEVBQXlCNEIsS0FBS21TLFNBQUwsQ0FBZW5KLEtBQWYsQ0FBekI7QUFDQWtKLFNBQU96UixJQUFQLENBQVksRUFBWjs7QUFFQWdSLFFBQU1XLE9BQU4sQ0FBYyxVQUFDckosSUFBRCxFQUFPMU4sQ0FBUCxFQUFhO0FBQzFCLE9BQUk0VyxNQUFNclgsSUFBTixDQUFXLE1BQVgsTUFBdUI2VyxNQUFNcFcsQ0FBTixDQUEzQixFQUFxQztBQUNwQyxRQUFJcVcsT0FBT3JXLENBQVAsSUFBWSxDQUFoQixFQUFtQjtBQUNsQjZXLFlBQU94QyxJQUFQLENBQVlnQyxPQUFPclcsQ0FBUCxDQUFaO0FBQ0E0VyxXQUFNMVcsUUFBTixDQUFlLFFBQWY7QUFDQTtBQUNEO0FBQ0QsR0FQRDtBQVFBLEVBbEJEO0FBbUJBOztBQUVEO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7Ozs7Ozs7OztBQWFBLFNBQVM4VyxtQkFBVCxDQUE4QnhVLEtBQTlCLEVBQXFDZ0MsUUFBckMsRUFBK0NELFVBQS9DLEVBQTJETixHQUEzRCxFQUFnRTtBQUMvRCxLQUFNdkIsWUFBWUYsTUFBTWpELElBQU4sQ0FBVyxXQUFYLENBQWxCO0FBQ0EsS0FBSW1ELHdDQUFKLEVBQW9DO0FBQ25DQSxZQUFVZ0MsSUFBVjtBQUNBOztBQUVEO0FBQ0E7QUFDQSxLQUFJLE9BQU9GLFFBQVAsS0FBb0IsUUFBeEIsRUFBa0M7QUFDakNBLGFBQVdHLEtBQUtDLEtBQUwsQ0FBV0osUUFBWCxDQUFYO0FBQ0E7O0FBRUQsS0FBSUEsU0FBU3lTLE1BQVQsSUFBbUI5VyxPQUFPMEUsUUFBUCxDQUFnQnFTLElBQWhCLEtBQXlCMVMsU0FBUzJTLFFBQXpELEVBQW1FO0FBQ2xFLFNBQU9oWCxPQUFPMEUsUUFBUCxDQUFnQm9TLE1BQWhCLEVBQVA7QUFDQTs7QUFFRCxLQUFJelMsU0FBUzJTLFFBQWIsRUFBdUI7QUFDdEIsU0FBUWhYLE9BQU8wRSxRQUFQLENBQWdCcVMsSUFBaEIsR0FBdUIxUyxTQUFTMlMsUUFBeEM7QUFDQTs7QUFFRCxLQUFJM1MsU0FBUzBQLE9BQWIsRUFBc0I7QUFDckIsTUFBSTFQLFNBQVN5TCxLQUFiLEVBQW9CO0FBQ25CLE9BQUltSCxNQUFNQyxPQUFOLENBQWM3UyxTQUFTeUwsS0FBdkIsQ0FBSixFQUFtQztBQUNsQ3pMLGFBQVN5TCxLQUFULENBQWU4RyxPQUFmLENBQXVCLHlCQUFpQjtBQUN2Q3ZVLFdBQU1TLElBQU4sQ0FBV3FVLGFBQVgsRUFBMEI5SyxHQUExQixDQUE4QixFQUE5QjtBQUNBLEtBRkQ7QUFHQSxJQUpELE1BSU87QUFDTjtBQUNBLFFBQUl4SixvQkFBb0IsQ0FDdkIsUUFEdUIsRUFFdkIsT0FGdUIsRUFHdkIsUUFIdUIsRUFJdkIsT0FKdUIsQ0FBeEI7QUFNQVIsVUFBTVMsSUFBTixDQUFXLHlCQUFYLEVBQXNDbEQsSUFBdEMsQ0FBMkMsVUFBQ0MsQ0FBRCxFQUFJOEIsT0FBSixFQUFnQjtBQUMxRCxTQUFJLENBQUNrQixrQkFBa0JHLE9BQWxCLENBQTBCckIsUUFBUW9CLElBQWxDLENBQUwsRUFBOEM7QUFDN0MsYUFBTyxJQUFQO0FBQ0E7QUFDRHBCLGFBQVF0QixLQUFSLEdBQWdCLEVBQWhCO0FBQ0EsS0FMRDtBQU1BO0FBQ0QsR0FwQkQsTUFvQk8sSUFBSWdFLFNBQVMrUyxLQUFiLEVBQW9CO0FBQzFCL1UsU0FBTTJLLE9BQU4sQ0FBYyxPQUFkO0FBQ0E7QUFDRDtBQUNELEtBQUkzSSxTQUFTNFIsS0FBYixFQUFvQjtBQUNuQkQsY0FBWTNULEtBQVosRUFBbUJnQyxTQUFTNFIsS0FBNUIsRUFBbUM1UixTQUFTNlIsTUFBNUM7QUFDQTtBQUNELEtBQUk3UixTQUFTZ1QsSUFBYixFQUFtQjtBQUNsQjNCLG1CQUFpQnJSLFFBQWpCO0FBQ0E7QUFDRCxLQUFJQSxTQUFTQSxRQUFiLEVBQXVCO0FBQ3RCLE1BQUlBLFNBQVNpUSxNQUFiLEVBQXFCO0FBQ3BCRCxxQkFBa0JoUyxLQUFsQixFQUF5QmdDLFNBQVM4TSxPQUFsQyxFQUEwQzlNLFNBQVNBLFFBQW5EO0FBQ0EsR0FGRCxNQUVLO0FBQ0p5UCxlQUFZelIsS0FBWixFQUFtQmdDLFNBQVNBLFFBQTVCLEVBQXNDQSxTQUFTMFAsT0FBL0M7QUFDQTtBQUNEO0FBQ0QsS0FBSTFQLFNBQVNtUSxLQUFiLEVBQW9CO0FBQ25CRDtBQUNBO0FBQ0QsS0FBSWxRLFNBQVNqQyxJQUFiLEVBQW1CO0FBQ2xCLE1BQUkyVCxRQUFRelcsRUFBRSx3QkFBd0IrRSxTQUFTakMsSUFBakMsR0FBeUMsUUFBM0MsQ0FBWjtBQUNBQyxRQUFNTSxNQUFOLENBQWFvVCxLQUFiO0FBQ0FBLFFBQU1qVCxJQUFOLENBQVcsTUFBWCxFQUFtQndVLEdBQW5CLENBQXVCLENBQXZCLEVBQTBCQyxNQUExQjtBQUNBO0FBQ0Q7O0FBRUQ7QUFDQTtBQUNBOztrQkFFZVYsbUI7Ozs7Ozs7O0FDcFNmOztBQUVBOzs7OztBQUtBO0FBQ0E7QUFDQTs7QUFFQSxDQUFDLFVBQVV2WCxDQUFWLEVBQWtDO0FBQUEsS0FBckJxUixjQUFxQix1RUFBSixFQUFJOztBQUNsQyxLQUFJNkcsTUFBTTdHLGVBQWUsZ0JBQWYsS0FBb0MsRUFBOUM7QUFDQSxLQUFJQyxPQUFPQyxJQUFQLENBQVkyRyxHQUFaLEVBQWlCdFksTUFBakIsS0FBNEIsQ0FBaEMsRUFBbUM7QUFDbEMsU0FBT1EsUUFBUUMsSUFBUixDQUFhLDJDQUFiLENBQVA7QUFDQTs7QUFFREwsR0FBRUMsTUFBRixDQUFTLElBQVQsRUFBZUQsRUFBRWtQLGFBQUYsQ0FBZ0JpSixRQUEvQixFQUF5QztBQUN4Q0MsVUFBUUYsSUFBSUUsTUFENEI7QUFFeENDLFlBQVVILElBQUlHLFFBRjBCO0FBR3hDN0ksV0FBUztBQUNSOEksVUFBT0osSUFBSUksS0FESDtBQUVSQyxVQUFPTCxJQUFJSyxLQUZIO0FBR1JDLGFBQVVOLElBQUlNO0FBSE4sR0FIK0I7QUFReENDLFNBQU87QUFDTkMsV0FBUVIsSUFBSVM7QUFETixHQVJpQztBQVd4Q25NLFFBQU07QUFDTGtNLFdBQVFSLElBQUlRO0FBRFAsR0FYa0M7QUFjeENFLFVBQVE7QUFDUEMsY0FBV1gsSUFBSVc7QUFEUjtBQWRnQyxFQUF6QztBQWtCQSxDQXhCRCxFQXdCR25ZLE9BQU8rUCxNQXhCVixFQXdCa0IvUCxPQUFPMlEsY0F4QnpCLEU7Ozs7OztBQ1hBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsZUFBZSxZQUFZLG9EQUFvRDtBQUMvRTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBLGdDQUFnQyxVQUFVLEVBQUU7QUFDNUMsQzs7Ozs7O0FDekJBO0FBQ0E7OztBQUdBO0FBQ0EsNERBQTZELFdBQVcsWUFBWSxnQkFBZ0IsaUJBQWlCLGtCQUFrQixxQkFBcUIsb0JBQW9CLHdCQUF3QixpQkFBaUIsRUFBRSxlQUFlLFdBQVcsWUFBWSxnQkFBZ0IsaUJBQWlCLGtCQUFrQixvQkFBb0IsNkJBQTZCLHdDQUF3QyxFQUFFLG9CQUFvQix1QkFBdUIsdUJBQXVCLGdCQUFnQixpQkFBaUIsWUFBWSxXQUFXLG1CQUFtQiwyQkFBMkIsRUFBRSwyQkFBMkIsZ0JBQWdCLDBCQUEwQixpQkFBaUIsMkJBQTJCLEVBQUUsMENBQTBDLGtCQUFrQixFQUFFLGtCQUFrQix1QkFBdUIsMEJBQTBCLDJCQUEyQixtQkFBbUIscUJBQXFCLGtCQUFrQixFQUFFLHFFQUFxRSxnQkFBZ0IsaUJBQWlCLEVBQUUsbUJBQW1CLHFCQUFxQixFQUFFLHVFQUF1RSxxQkFBcUIsRUFBRSxlQUFlLG9CQUFvQixvQkFBb0IsRUFBRSxtQ0FBbUMsaUJBQWlCLEVBQUUsNkRBQTZELDhCQUE4QiwyQkFBMkIsMEJBQTBCLHNCQUFzQixFQUFFLDZCQUE2QixrQkFBa0IsRUFBRSxlQUFlLDZCQUE2QixFQUFFLG9CQUFvQixnQkFBZ0IsdUJBQXVCLGFBQWEsZ0JBQWdCLHVCQUF1Qix1QkFBdUIsY0FBYyxlQUFlLGtCQUFrQixFQUFFLHNCQUFzQixrQkFBa0IsRUFBRSw4QkFBOEIsb0JBQW9CLEVBQUUsaUNBQWlDLGtCQUFrQixFQUFFLCtCQUErQixrQkFBa0IsRUFBRSx3Q0FBd0Msc0JBQXNCLG9CQUFvQiw0QkFBNEIsY0FBYyw2QkFBNkIsbUJBQW1CLGtCQUFrQixlQUFlLGtCQUFrQixxQkFBcUIsK0JBQStCLEVBQUUsOEJBQThCLGVBQWUsY0FBYyxFQUFFLGdCQUFnQixnQkFBZ0IsaUJBQWlCLHNCQUFzQix1QkFBdUIsYUFBYSxXQUFXLDBCQUEwQix1QkFBdUIsa0JBQWtCLDJCQUEyQixnQkFBZ0IsdUJBQXVCLG9CQUFvQiwrQ0FBK0MsRUFBRSx3Q0FBd0MsaUJBQWlCLEVBQUUsdUJBQXVCLGVBQWUsRUFBRSxrQ0FBa0MsZ0JBQWdCLEVBQUUsa0VBQWtFLGdCQUFnQixnQkFBZ0Isc0JBQXNCLHVCQUF1QixnQkFBZ0IsRUFBRSxrQkFBa0IsdUJBQXVCLFdBQVcsYUFBYSxnQkFBZ0Isb0JBQW9CLHNCQUFzQix3QkFBd0IsRUFBRSxnQkFBZ0IsdUJBQXVCLGtCQUFrQixjQUFjLGFBQWEsc0JBQXNCLGVBQWUsZ0JBQWdCLGtCQUFrQiw2Q0FBNkMsRUFBRSx1QkFBdUIsd0JBQXdCLEVBQUUsd0NBQXdDLGlCQUFpQixFQUFFLHlDQUF5QyxrQkFBa0IscUJBQXFCLGVBQWUsZ0JBQWdCLHlCQUF5QixjQUFjLGFBQWEsdUJBQXVCLHdCQUF3Qix1Q0FBdUMsRUFBRSxzQkFBc0IsNkJBQTZCLGdDQUFnQyxlQUFlLEVBQUUsdUJBQXVCLDZCQUE2QixnQ0FBZ0MsbUJBQW1CLEVBQUUscUJBQXFCLFlBQVksRUFBRSwyQkFBMkIsb0NBQW9DLHdCQUF3QixFQUFFLDRCQUE0Qix3QkFBd0IsdUNBQXVDLEVBQUUsc0JBQXNCLGFBQWEsRUFBRSw0QkFBNEIsbUNBQW1DLHdCQUF3QixFQUFFLDZCQUE2QixzQ0FBc0MsRUFBRSx3QkFBd0Isc0JBQXNCLHlCQUF5QixFQUFFLHFDQUFxQyxxQkFBcUIsa0JBQWtCLHVCQUF1QixFQUFFLG1DQUFtQyxpQkFBaUIsRUFBRSx3QkFBd0IsZ0JBQWdCLGNBQWMscUJBQXFCLHdCQUF3QixFQUFFLCtCQUErQix5QkFBeUIscUJBQXFCLGFBQWEsY0FBYyxrQkFBa0IsbUJBQW1CLDZDQUE2Qyx1QkFBdUIsRUFBRSw0Q0FBNEMsZ0JBQWdCLG9CQUFvQixpQkFBaUIsbUJBQW1CLG1CQUFtQiwyQkFBMkIseUJBQXlCLG1CQUFtQixFQUFFLG9EQUFvRCxtQkFBbUIsRUFBRSx1QkFBdUIsa0JBQWtCLHlCQUF5QixjQUFjLGdCQUFnQixtQkFBbUIscUJBQXFCLGVBQWUsa0JBQWtCLG1CQUFtQixrQkFBa0IsNkNBQTZDLHVCQUF1QixFQUFFLHVCQUF1QixxQkFBcUIscUJBQXFCLHNCQUFzQix3QkFBd0IsRUFBRSx3QkFBd0IsZ0JBQWdCLEVBQUUscUJBQXFCLHNCQUFzQix1QkFBdUIsY0FBYyxZQUFZLGdCQUFnQixpQkFBaUIsRUFBRSxnQkFBZ0IscUJBQXFCLHNCQUFzQixtQkFBbUIsMEJBQTBCLHdCQUF3QixFQUFFLG9DQUFvQyxvQkFBb0IsRUFBRSxnREFBZ0Qsb0JBQW9CLEVBQUUsNkNBQTZDLHNCQUFzQixFQUFFLHdEQUF3RCxrQkFBa0IsRUFBRSxpS0FBaUssZUFBZSwwQkFBMEIscUVBQXFFLEVBQUUseU1BQXlNLGVBQWUsd0JBQXdCLEVBQUUscU5BQXFOLGVBQWUsMEJBQTBCLEVBQUUsaUNBQWlDLGVBQWUseUNBQXlDLEVBQUUsMkNBQTJDLGtCQUFrQixFQUFFLDhDQUE4QyxpQkFBaUIsRUFBRSx1Q0FBdUMsZ0JBQWdCLDZCQUE2QixFQUFFLHFCQUFxQiwwQkFBMEIsRUFBRSxzQkFBc0IsNkJBQTZCLEVBQUUsb0JBQW9CLHdCQUF3Qix5QkFBeUIsRUFBRSxFQUFFLHVHQUF1Ryx3SEFBd0gsc0JBQXNCLHVCQUF1QixFQUFFLGlDQUFpQyxpQkFBaUIsRUFBRSx1Q0FBdUMsYUFBYSxnQkFBZ0IsRUFBRSx1Q0FBdUMsc0JBQXNCLHVCQUF1QixFQUFFLHFDQUFxQyxxQ0FBcUMsZ0JBQWdCLGdCQUFnQixnQkFBZ0IsdUJBQXVCLHNCQUFzQiw2QkFBNkIsRUFBRSw2Q0FBNkMsbUJBQW1CLEVBQUUsa0NBQWtDLGlCQUFpQixlQUFlLEVBQUUsZ0NBQWdDLGFBQWEsZUFBZSxrQkFBa0IsbUJBQW1CLHdCQUF3QixxQ0FBcUMsc0JBQXNCLHlCQUF5QixpQkFBaUIsRUFBRSxFQUFFOztBQUVya1E7Ozs7Ozs7Ozs7OztBQ1BBOztBQUVBOzs7OztBQUtBO0FBQ0E7QUFDQTs7Ozs7O0FBRUE7O0FBRUE7QUFDQTtBQUNBOztBQUVBOzs7Ozs7QUFNQSxTQUFTeUgsT0FBVCxDQUFrQkMsT0FBbEIsRUFBMkI7QUFDMUIsUUFBT0EsUUFBUS9SLFFBQVIsQ0FBaUJnUyxPQUFPQyxRQUFQLENBQWdCQyxNQUFqQyxLQUE0Q0gsUUFBUS9SLFFBQVIsQ0FBaUJnUyxPQUFPQyxRQUFQLENBQWdCMVUsUUFBakMsQ0FBNUMsSUFBMEZ3VSxRQUFRckssSUFBUixDQUFhLFVBQWIsQ0FBakc7QUFDQTs7QUFFRDs7Ozs7QUFLQSxTQUFTeUssU0FBVCxDQUFvQkosT0FBcEIsRUFBNkJLLFFBQTdCLEVBQXVDO0FBQ3RDLEtBQUlDLE9BQU9OLFFBQVFqWixJQUFSLENBQWFrWixPQUFPekgsSUFBUCxDQUFZK0gsRUFBekIsQ0FBWDtBQUNBLEtBQUlDLFNBQVNSLFFBQVFqWixJQUFSLENBQWFrWixPQUFPekgsSUFBUCxDQUFZaUksTUFBekIsQ0FBYjs7QUFFQSxLQUFJQyw2QkFBMkJULE9BQU96SCxJQUFQLENBQVkrSCxFQUF2QyxVQUE4Q0QsSUFBOUMsZ0JBQTZETCxPQUFPekgsSUFBUCxDQUFZaUksTUFBekUsTUFBSjtBQUNBLEtBQUlFLGdDQUE4QlYsT0FBT3pILElBQVAsQ0FBWStILEVBQTFDLFVBQWlERCxJQUFqRCxnQkFBZ0VMLE9BQU96SCxJQUFQLENBQVlpSSxNQUE1RSxVQUF1RkQsTUFBdkYsT0FBSjtBQUNBLEtBQUlJLDRCQUEwQlgsT0FBT3pILElBQVAsQ0FBWStILEVBQXRDLFVBQTZDRCxJQUE3QyxnQkFBNERMLE9BQU96SCxJQUFQLENBQVlxSSxLQUF4RSxNQUFKO0FBQ0EsS0FBSUMsMkJBQXlCYixPQUFPekgsSUFBUCxDQUFZK0gsRUFBckMsVUFBNENELElBQTVDLGdCQUEyREwsT0FBT3pILElBQVAsQ0FBWXFJLEtBQXZFLFVBQWlGTCxNQUFqRixPQUFKOztBQUVBOzs7QUFHQSxLQUFJTyxTQUFTZixRQUFRZ0IsYUFBUixDQUFzQmYsT0FBT3pILElBQVAsQ0FBWXlJLE9BQWxDLEVBQTJDSCxhQUEzQyxDQUFiO0FBQ0EsS0FBSWYsUUFBUUMsT0FBUixDQUFKLEVBQXNCO0FBQ3JCQSxVQUFRcFcsR0FBUixDQUFZbVgsTUFBWixFQUFvQnBNLE9BQXBCLENBQTRCc0wsT0FBT2lCLE1BQVAsQ0FBY0MsS0FBMUM7QUFDQSxTQUFPLEtBQVA7QUFDQTs7QUFFRDs7O0FBR0EsS0FBSUMsaUJBQWlCTCxPQUFPQyxhQUFQLENBQXFCZixPQUFPekgsSUFBUCxDQUFZNkksUUFBakMsRUFBMkNULGNBQTNDLEVBQTJEUCxRQUEzRCxFQUFxRSxJQUFyRSxDQUFyQjs7QUFFQTs7O0FBR0EsS0FBSWlCLGtCQUFrQnRCLFFBQVFnQixhQUFSLENBQXNCZixPQUFPekgsSUFBUCxDQUFZK0ksU0FBbEMsRUFBNkNiLGVBQTdDLEVBQThETCxRQUE5RCxFQUF3RSxJQUF4RSxDQUF0QjtBQUNBLEtBQUltQixlQUFlRixnQkFBZ0JHLE1BQWhCLENBQXVCZCxrQkFBdkIsQ0FBbkI7QUFDQSxLQUFJYSxhQUFhM2EsTUFBakIsRUFBeUI7QUFDeEJ5YSxvQkFBa0JBLGdCQUFnQjFSLEdBQWhCLENBQW9CNFIsWUFBcEIsQ0FBbEI7QUFDQTs7QUFFREYsaUJBQWdCMVgsR0FBaEIsQ0FBb0J3WCxjQUFwQixFQUFvQ3JaLFdBQXBDLENBQWdEa1ksT0FBT0MsUUFBUCxDQUFnQkMsTUFBaEUsRUFBd0V4TCxPQUF4RSxDQUFnRnNMLE9BQU9pQixNQUFQLENBQWNRLEdBQTlGO0FBQ0ExQixTQUFRcFcsR0FBUixDQUFZNFgsWUFBWixFQUEwQjVYLEdBQTFCLENBQThCbVgsTUFBOUIsRUFBc0NyWixRQUF0QyxDQUErQ3VZLE9BQU9DLFFBQVAsQ0FBZ0JDLE1BQS9ELEVBQXVFeEwsT0FBdkUsQ0FBK0VzTCxPQUFPaUIsTUFBUCxDQUFjOVMsRUFBN0Y7QUFDQTs7QUFFRDs7Ozs7O0FBTUEsU0FBU3VULGtCQUFULENBQTZCQyxRQUE3QixFQUF1Q3ZCLFFBQXZDLEVBQWlEO0FBQ2hELEtBQUlFLEtBQUtxQixTQUFTN2EsSUFBVCxDQUFja1osT0FBT3pILElBQVAsQ0FBWStILEVBQTFCLENBQVQ7QUFDQSxLQUFJc0Isc0JBQW9CNUIsT0FBT3pILElBQVAsQ0FBWStILEVBQWhDLFVBQXVDQSxFQUF2QyxPQUFKO0FBQ0EsS0FBSXVCLFNBQVNGLFNBQVNILE1BQVQsQ0FBZ0JJLFFBQWhCLENBQWI7O0FBRUEsS0FBSUMsT0FBT2piLE1BQVgsRUFBbUI7QUFDbEIsTUFBSWtiLFVBQVVELE9BQU9MLE1BQVAsT0FBa0J4QixPQUFPQyxRQUFQLENBQWdCQyxNQUFsQyxDQUFkO0FBQ0EsTUFBSSxDQUFDNEIsUUFBUWxiLE1BQWIsRUFBcUI7QUFDcEJ1WixhQUFVMEIsT0FBT0UsRUFBUCxDQUFVLENBQVYsQ0FBVixFQUF3QjNCLFFBQXhCO0FBQ0E7O0FBRUQsTUFBSXlCLE9BQU9qYixNQUFQLEdBQWdCK2EsU0FBUy9hLE1BQTdCLEVBQXFDO0FBQ3BDOGEsc0JBQW1CQyxTQUFTaFMsR0FBVCxDQUFhaVMsUUFBYixDQUFuQixFQUEyQ3hCLFFBQTNDO0FBQ0E7QUFDRDtBQUNEOztBQUVEOzs7Ozs7QUFNQSxTQUFTNEIsaUJBQVQsQ0FBMkJDLEtBQTNCLEVBQWtDMUosSUFBbEMsRUFBd0M7QUFDdkMwSixPQUFNM2EsSUFBTixDQUFXLFVBQUNDLENBQUQsRUFBSUMsRUFBSixFQUFXO0FBQ3JCLE1BQUkyVyxRQUFRblgsRUFBRVEsRUFBRixDQUFaO0FBQ0F3WSxTQUFPekgsSUFBUCxDQUFZK0YsT0FBWixDQUFvQixlQUFPO0FBQzFCSCxTQUFNclgsSUFBTixDQUFXaUssR0FBWCxFQUFnQixJQUFoQjtBQUNBLEdBRkQ7QUFHQSxFQUxEO0FBTUE7O0FBRUQ7QUFDQTtBQUNBOztBQUVBOzs7QUFHQSxJQUFNaVAsU0FBUztBQUNkOzs7OztBQUtBaUIsU0FBUTtBQUNQOVMsTUFBSSxXQURHO0FBRVBzVCxPQUFLLFlBRkU7QUFHUFAsU0FBTztBQUhBLEVBTk07O0FBWWQ7Ozs7O0FBS0FqQixXQUFVO0FBQ1RDLFVBQVEsV0FEQztBQUVUM1UsWUFBVTtBQUZELEVBakJJOztBQXNCZDs7Ozs7QUFLQWdOLE9BQU07QUFDTCtILE1BQUksV0FEQztBQUVMRSxVQUFRLGVBRkg7QUFHTEksU0FBTyxjQUhGO0FBSUxJLFdBQVMsZ0JBSko7QUFLTEksWUFBVSxpQkFMTDtBQU1MRSxhQUFXO0FBTk4sRUEzQlE7O0FBb0NkOzs7OztBQUtBaE0sS0F6Q2Msa0JBeUNnQjtBQUFBLE1BQXhCOEssUUFBd0IsdUVBQWJwWixFQUFFMEssUUFBRixDQUFhOztBQUM3QjBPLFdBQVNqUyxFQUFULENBQVksT0FBWixhQUE4QjZSLE9BQU96SCxJQUFQLENBQVlpSSxNQUExQyxRQUFxRCxFQUFDSixrQkFBRCxFQUFyRCxFQUFpRSxVQUFVaFMsS0FBVixFQUFpQjtBQUNqRkEsU0FBTWlOLGNBQU47QUFDQThFLGFBQVVuWixFQUFFLElBQUYsQ0FBVixFQUFtQm9aLFFBQW5CO0FBQ0EsR0FIRDtBQUlBLEVBOUNhOzs7QUFnRGQ7Ozs7O0FBS0E4QixVQXJEYyx1QkFxRHFCO0FBQUEsTUFBeEI5QixRQUF3Qix1RUFBYnBaLEVBQUUwSyxRQUFGLENBQWE7O0FBQ2xDLE1BQUlpUSxXQUFXdkIsU0FBUzVWLElBQVQsWUFBdUJ3VixPQUFPekgsSUFBUCxDQUFZaUksTUFBbkMsT0FBZjtBQUNBa0IscUJBQW1CQyxRQUFuQixFQUE2QnZCLFFBQTdCO0FBQ0EsRUF4RGE7OztBQTBEZDs7Ozs7O0FBTUE0QixpQkFoRWMsOEJBZ0U0QjtBQUFBLE1BQXhCNUIsUUFBd0IsdUVBQWJwWixFQUFFMEssUUFBRixDQUFhOztBQUN6QyxNQUFJaVEsV0FBV3ZCLFNBQVM1VixJQUFULFlBQXVCd1YsT0FBT3pILElBQVAsQ0FBWWlJLE1BQW5DLE9BQWY7QUFDQSxNQUFJMkIsVUFBVS9CLFNBQVM1VixJQUFULFlBQXVCd1YsT0FBT3pILElBQVAsQ0FBWXFJLEtBQW5DLE9BQWQ7QUFDQW9CLG9CQUFpQkwsUUFBakIsRUFBMkIsQ0FBQzNCLE9BQU96SCxJQUFQLENBQVl5SSxPQUFiLEVBQXNCaEIsT0FBT3pILElBQVAsQ0FBWStJLFNBQWxDLENBQTNCO0FBQ0FVLG9CQUFpQkcsT0FBakIsRUFBMEIsQ0FBQ25DLE9BQU96SCxJQUFQLENBQVk2SSxRQUFiLENBQTFCO0FBQ0E7QUFyRWEsQ0FBZjs7QUF3RUE7QUFDQTtBQUNBOztrQkFFZXBCLE07Ozs7Ozs7O0FDN0xmOztBQUVBO0FBQ0E7QUFDQTs7Ozs7OztBQUVBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQSxJQUFJb0MsZUFBZXBiLEVBQUUsZUFBRixDQUFuQjtBQUNBLElBQUlxYixXQUFXcmIsRUFBRSxZQUFGLENBQWY7QUFDQSxJQUFJOEwsWUFBWTlMLEVBQUUsZ0JBQUYsQ0FBaEI7QUFDQSxJQUFJc2IsaUJBQWlCdGIsRUFBRSxtQkFBRixDQUFyQjtBQUNBLElBQUl1YixrQkFBa0J2YixFQUFFLG9CQUFGLENBQXRCO0FBQ0EsSUFBSXdiLGtCQUFrQnhiLEVBQUUsb0JBQUYsQ0FBdEI7QUFDQSxJQUFJeWIsbUJBQW1CemIsRUFBRSxxQkFBRixDQUF2Qjs7QUFFQTtBQUNBO0FBQ0E7O0FBRUEsU0FBUzBiLFFBQVQsR0FBcUI7QUFDcEIsS0FBSUMsUUFBUTNiLEVBQUUsT0FBRixFQUFXNGIsS0FBWCxDQUFpQjtBQUM1QixxQkFBbUIsS0FEUztBQUU1QixnQkFBYyxDQUNiLGVBRGEsRUFFYixhQUZhLEVBR2IsZUFIYSxDQUZjO0FBTzVCLFlBQVU7QUFDVGpaLFFBQUs7QUFESTtBQVBrQixFQUFqQixDQUFaOztBQVlBLEtBQUlrWixNQUFNRixNQUFNN2IsSUFBTixDQUFXLE9BQVgsQ0FBVjs7QUFFQXViLFVBQVNsVSxFQUFULENBQVksT0FBWixFQUFxQixZQUFZO0FBQ2hDMFUsTUFBSW5YLElBQUo7QUFDQSxFQUZEOztBQUlBb0gsV0FBVWdRLEtBQVYsQ0FBZ0IsWUFBWTtBQUMzQkQsTUFBSTNHLEtBQUo7QUFDQSxFQUZEO0FBR0E7O0FBRUQsU0FBUzZHLFVBQVQsR0FBdUI7QUFDdEI7QUFDQTtBQUNBOztBQUVBLEtBQUlKLFFBQVEzYixFQUFFLGNBQUYsRUFBa0I0YixLQUFsQixDQUF3QjtBQUNuQyxxQkFBbUIsS0FEZ0I7QUFFbkMsZ0JBQWMsQ0FDYixhQURhLEVBRWIsZUFGYSxFQUdiLGVBSGEsQ0FGcUI7QUFPbkMsWUFBVTtBQUNUalosUUFBSztBQURJO0FBUHlCLEVBQXhCLENBQVo7O0FBWUEsS0FBSWtaLE1BQU1GLE1BQU03YixJQUFOLENBQVcsT0FBWCxDQUFWOztBQUVBd2IsZ0JBQWVuVSxFQUFmLENBQWtCLE9BQWxCLEVBQTJCLFlBQVk7QUFDdEMwVSxNQUFJblgsSUFBSjtBQUNBLEVBRkQ7O0FBSUE2VyxpQkFBZ0JPLEtBQWhCLENBQXNCLFlBQVk7QUFDakNELE1BQUkzRyxLQUFKO0FBQ0EsRUFGRDtBQUdBOztBQUVELFNBQVM4RyxXQUFULEdBQXdCO0FBQ3ZCLEtBQUksQ0FBQ1osYUFBYXhiLE1BQWxCLEVBQTBCO0FBQ3pCLFNBQU8sS0FBUDtBQUNBOztBQUVELEtBQUlJLEVBQUVVLE1BQUYsRUFBVStGLEtBQVYsS0FBb0IsSUFBeEIsRUFBOEI7QUFDN0IsU0FBTyxLQUFQO0FBQ0E7O0FBRUQsS0FBSWtWLFFBQVFQLGFBQWFRLEtBQWIsQ0FBbUI7QUFDOUIscUJBQW1CLEtBRFc7QUFFOUIsZ0JBQWMsQ0FDYixhQURhLEVBRWIsZUFGYSxFQUdiLGdCQUhhLENBRmdCO0FBTzlCLFlBQVU7QUFDVGpaLFFBQUs7QUFESTtBQVBvQixFQUFuQixDQUFaO0FBV0FnWixPQUFNdFksTUFBTixDQUFhckQsRUFBRSxrQkFBRixDQUFiOztBQUVBLEtBQUk2YixNQUFNRixNQUFNN2IsSUFBTixDQUFXLE9BQVgsQ0FBVjs7QUFFQTBiLGlCQUFnQnJVLEVBQWhCLENBQW1CLE9BQW5CLEVBQTRCLFlBQVk7QUFDdkMwVSxNQUFJblgsSUFBSjtBQUNBLEVBRkQ7O0FBSUErVyxrQkFBaUJLLEtBQWpCLENBQXVCLFlBQVk7QUFDbENELE1BQUkzRyxLQUFKO0FBQ0EsRUFGRDtBQUdBOztBQUVEO0FBQ0E7QUFDQTs7UUFFUXdHLFEsR0FBQUEsUTtRQUFVSyxVLEdBQUFBLFU7UUFBWUMsVyxHQUFBQSxXOzs7Ozs7OztBQ2pIOUI7O0FBRUE7QUFDQTtBQUNBOzs7OztBQUVBLElBQUlDLFNBQVNqYyxFQUFFLGNBQUYsQ0FBYixDLENBQWdDO0FBQ2hDLElBQUlrYyxhQUFhLENBQWpCLEMsQ0FBb0I7QUFDcEIsSUFBSUMsaUJBQWlCbmMsRUFBRSwwQkFBRixDQUFyQjtBQUNBLElBQUlvYyxPQUFPcGMsRUFBRSxNQUFGLENBQVg7O0FBRUE7QUFDQTtBQUNBOztBQUVBLFNBQVNxYyxXQUFULEdBQXdCO0FBQ3ZCRCxNQUFLOVksSUFBTCxDQUFVLFlBQVYsRUFBd0J0RCxFQUFFLFNBQUYsRUFBYWlQLE1BQWIsRUFBeEI7O0FBRUEsS0FBSWpQLEVBQUVVLE1BQUYsRUFBVStGLEtBQVYsS0FBb0IsSUFBcEIsSUFBNkIyVixLQUFLcFYsUUFBTCxDQUFjLElBQWQsQ0FBakMsRUFBc0Q7QUFDckRoSCxJQUFFVSxNQUFGLEVBQVU0YixNQUFWLENBQWlCLFlBQVk7QUFDNUIsT0FBSUMsV0FBV3ZjLEVBQUVVLE1BQUYsRUFBVThiLFNBQVYsRUFBZixDQUQ0QixDQUNVO0FBQ3RDLE9BQUlDLGdCQUFnQixLQUFwQixDQUY0QixDQUVEO0FBQzNCLE9BQUlDLGtCQUFrQixLQUF0QixDQUg0QixDQUdDO0FBQzdCLE9BQUlDLGNBQWMzYyxFQUFFLGFBQUYsQ0FBbEI7QUFDQSxPQUFJaVAsU0FBU21OLEtBQUs5WSxJQUFMLENBQVUsWUFBVixDQUFiOztBQUVBO0FBQ0EsT0FBSWlaLFdBQVcsQ0FBZixFQUFrQjtBQUNqQjtBQUNBLFFBQUlBLFdBQVdMLFVBQWYsRUFBMkI7QUFDMUJPLHFCQUFnQixLQUFoQixDQUQwQixDQUNIO0FBQ3ZCO0FBQ0EsU0FBSUYsV0FBV04sT0FBT2hOLE1BQVAsS0FBa0JnTixPQUFPVyxNQUFQLEdBQWdCL1YsR0FBakQsRUFBc0Q7QUFDckQ7QUFDQSxVQUFJNlYsb0JBQW9CLEtBQXhCLEVBQStCO0FBQzlCLFdBQUlHLGNBQWNaLE9BQU9XLE1BQVAsR0FBZ0IvVixHQUFsQyxDQUQ4QixDQUNTO0FBQ3ZDb1YsY0FBT25iLFdBQVAsQ0FBbUIsU0FBbkI7QUFDQW1iLGNBQU9uVixHQUFQLENBQVc7QUFDVixlQUFPO0FBREcsUUFBWDtBQUdBNFYseUJBQWtCLElBQWxCO0FBQ0E7O0FBRUQ7QUFDQSxNQVpELE1BWU87QUFDTjtBQUNBVCxhQUFPblYsR0FBUCxDQUFXO0FBQ1YsY0FBTyxNQUFNbVYsT0FBT2hOLE1BQVAsRUFBTixHQUF3QjtBQURyQixPQUFYO0FBR0E7O0FBRUQ7QUFDQSxLQXZCRCxNQXVCTztBQUNOeU4sdUJBQWtCLEtBQWxCLENBRE0sQ0FDbUI7QUFDekI7QUFDQSxTQUFJSCxXQUFXTixPQUFPVyxNQUFQLEdBQWdCL1YsR0FBL0IsRUFBb0M7QUFDbkM7QUFDQSxVQUFJNFYsa0JBQWtCLEtBQXRCLEVBQTZCO0FBQzVCLFdBQUlJLGVBQWNaLE9BQU9XLE1BQVAsR0FBZ0IvVixHQUFsQyxDQUQ0QixDQUNXO0FBQ3ZDb1YsY0FBT3hiLFFBQVAsQ0FBZ0IsU0FBaEI7QUFDQXdiLGNBQU9uVixHQUFQLENBQVc7QUFDVixlQUFPO0FBREcsUUFBWDtBQUdBMlYsdUJBQWdCLElBQWhCO0FBQ0E7QUFFRDtBQUNEO0FBQ0Q7QUFDQVAsaUJBQWFLLFFBQWI7QUFDQSxJQTNDRCxNQTJDTztBQUNOTixXQUFPbmIsV0FBUCxDQUFtQixTQUFuQjtBQUNBbWIsV0FBT25WLEdBQVAsQ0FBVztBQUNWLFlBQU87QUFERyxLQUFYO0FBR0E7O0FBRUQ2VixlQUFZN1YsR0FBWixDQUFnQixLQUFoQixFQUF1Qm1WLE9BQU9oTixNQUFQLEVBQXZCOztBQUVBLE9BQUlnTixPQUFPalYsUUFBUCxDQUFnQixTQUFoQixDQUFKLEVBQWdDO0FBQy9Cb1YsU0FBS3RWLEdBQUwsQ0FBUyxhQUFULEVBQXdCbUksU0FBUyxJQUFqQztBQUNBLElBRkQsTUFFTztBQUNObU4sU0FBS3RWLEdBQUwsQ0FBUyxhQUFULEVBQXdCLENBQXhCO0FBQ0E7QUFDRCxHQWpFRDtBQWtFQTtBQUNEOztBQUVELFNBQVNnVyxXQUFULEdBQXdCO0FBQ3ZCWCxnQkFBZWhWLEVBQWYsQ0FBa0IsWUFBbEIsRUFBZ0MsVUFBVXlCLENBQVYsRUFBYTtBQUM1QyxNQUFJbVUsVUFBVS9jLEVBQUUsSUFBRixDQUFkO0FBQ0EsTUFBSWdkLFFBQVFELFFBQVEvVixRQUFSLENBQWlCLE9BQWpCLElBQTRCLENBQTVCLEdBQWdDLEdBQTVDO0FBQ0FoSCxJQUFFLDBCQUFGLEVBQThCTSxJQUE5QixDQUFtQyxZQUFZO0FBQzlDTixLQUFFLElBQUYsRUFBUWlkLElBQVIsR0FBZW5XLEdBQWYsQ0FBbUIsU0FBbkIsRUFBOEIsTUFBOUI7QUFDQSxHQUZEO0FBR0FpVyxVQUFRdlosSUFBUixDQUFhLDBCQUFiLEVBQXlDMFosTUFBekMsQ0FBZ0RGLEtBQWhEO0FBQ0EsRUFQRCxFQU9HN1YsRUFQSCxDQU9NLFlBUE4sRUFPb0IsWUFBWTtBQUMvQixNQUFJZ1csV0FBV25kLEVBQUUsSUFBRixFQUFRd0QsSUFBUixDQUFhLDBCQUFiLENBQWY7QUFDQTJaLFdBQVNGLElBQVQsR0FBZ0JHLE9BQWhCLENBQXdCLEdBQXhCO0FBQ0EsRUFWRDtBQVdBOztBQUVEO0FBQ0E7QUFDQTs7UUFFUWYsVyxHQUFBQSxXO1FBQWFTLFcsR0FBQUEsVzs7Ozs7OztBQzFHckI7O0FBRUE7QUFDQTtBQUNBOzs7Ozs7O0FBRUE7O0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBLElBQUlPLGVBQWVyZCxFQUFFLGVBQUYsQ0FBbkI7O0FBRUE7QUFDQTtBQUNBOztBQUVBLFNBQVNzZCxlQUFULEdBQTRCO0FBQzNCLEtBQUksQ0FBQ0QsYUFBYXpkLE1BQWxCLEVBQTBCO0FBQ3pCLFNBQU8sS0FBUDtBQUNBOztBQUVEeWQsY0FBYS9jLElBQWIsQ0FBa0IsVUFBVTJOLElBQVYsRUFBZ0I7QUFDakMsTUFBSXBGLFFBQVE3SSxFQUFFLElBQUYsQ0FBWjtBQUNBLE1BQUlvTixRQUFRdkUsTUFBTXNELE9BQU4sQ0FBYyx1QkFBZCxDQUFaO0FBQ0EsTUFBSW9SLFdBQVduUSxNQUFNNUosSUFBTixDQUFXLFdBQVgsQ0FBZjtBQUNBLE1BQUlnYSxXQUFXcFEsTUFBTTVKLElBQU4sQ0FBVyxXQUFYLENBQWY7QUFDQSxNQUFJaWEsTUFBTUMsV0FBVzdVLE1BQU0vSSxJQUFOLENBQVcsS0FBWCxDQUFYLENBQVY7QUFDQSxNQUFJNmQsTUFBTUQsV0FBVzdVLE1BQU0vSSxJQUFOLENBQVcsS0FBWCxDQUFYLENBQVY7QUFDQSxNQUFJOGQsUUFBUUYsV0FBVzdVLE1BQU0vSSxJQUFOLENBQVcsT0FBWCxDQUFYLENBQVo7QUFDQSxNQUFJK2QsTUFBTUgsV0FBVzdVLE1BQU0vSSxJQUFOLENBQVcsS0FBWCxDQUFYLENBQVY7O0FBRUEsTUFBSWdlLGFBQWNGLFVBQVUzUyxTQUFYLEdBQXdCMlMsS0FBeEIsR0FBZ0NILEdBQWpEO0FBQ0EsTUFBSU0sV0FBWUYsUUFBUTVTLFNBQVQsR0FBc0I0UyxHQUF0QixHQUE0QkYsR0FBM0M7O0FBRUEzZCxJQUFFLElBQUYsRUFBUWdJLE1BQVIsQ0FBZTtBQUNkeVYsUUFBS0EsR0FEUztBQUVkRSxRQUFLQSxHQUZTO0FBR2RLLFVBQU8sSUFITztBQUlkQyxXQUFRLENBQUNILFVBQUQsRUFBYUMsUUFBYixDQUpNOztBQU1kZCxTQUFNLGNBQVU3VixLQUFWLEVBQWlCOFcsRUFBakIsRUFBcUI7QUFDMUJYLGFBQVN4USxHQUFULENBQWFtUixHQUFHRCxNQUFILENBQVUsQ0FBVixDQUFiO0FBQ0FULGFBQVN6USxHQUFULENBQWFtUixHQUFHRCxNQUFILENBQVUsQ0FBVixDQUFiO0FBQ0EsSUFUYTs7QUFXZEUsVUFBTyxlQUFVL1csS0FBVixFQUFpQjhXLEVBQWpCLEVBQXFCO0FBQzNCWCxhQUFTeFEsR0FBVCxDQUFhbVIsR0FBR0QsTUFBSCxDQUFVLENBQVYsQ0FBYjtBQUNBVCxhQUFTelEsR0FBVCxDQUFhbVIsR0FBR0QsTUFBSCxDQUFVLENBQVYsQ0FBYjtBQUNBLElBZGE7O0FBZ0JkRyxXQUFRLGdCQUFVaFgsS0FBVixFQUFpQjhXLEVBQWpCLEVBQXFCO0FBQzVCLFFBQUlHLGlCQUFpQnJlLEVBQUUsSUFBRixFQUFRc2UsT0FBUixDQUFnQixzQkFBaEIsQ0FBckI7QUFDQSxRQUFJdkgsV0FBVy9XLEVBQUUseUJBQUYsQ0FBZjtBQUNBLFFBQUl1ZSxTQUFTdmUsRUFBRSxJQUFGLEVBQVFzZSxPQUFSLENBQWdCLGdCQUFoQixDQUFiO0FBQ0EsUUFBSTFYLFdBQVcyWCxPQUFPM0IsTUFBUCxFQUFmOztBQUVBLFFBQUk0QixjQUFjSCxlQUFldmUsSUFBZixDQUFvQixVQUFwQixDQUFsQjtBQUNBLFFBQUkyZSxVQUFVLEVBQWQ7QUFDQXplLE1BQUUsdUJBQUYsRUFBMkJNLElBQTNCLENBQWdDLFlBQVc7QUFDMUMsU0FBSTBSLE9BQU9oUyxFQUFFLElBQUYsRUFBUXNELElBQVIsQ0FBYSxNQUFiLENBQVg7QUFDQSxTQUFJLENBQUNtYixRQUFRQyxjQUFSLENBQXVCMU0sSUFBdkIsQ0FBTCxFQUFtQ3lNLFFBQVF6TSxJQUFSLElBQWdCLEVBQWhCOztBQUVuQyxTQUFJalIsUUFBUWYsRUFBRSxJQUFGLEVBQVErTSxHQUFSLEVBQVo7O0FBRUEsU0FBSSxDQUFDMFIsUUFBUXpNLElBQVIsRUFBYzJNLFFBQWQsQ0FBdUI1ZCxLQUF2QixDQUFMLEVBQW9DMGQsUUFBUXpNLElBQVIsRUFBY2lFLElBQWQsQ0FBbUJsVixLQUFuQjtBQUNwQyxLQVBEOztBQVNBLFFBQUk2ZCxpQkFBaUJQLGVBQWU3YSxJQUFmLENBQW9CLFdBQXBCLENBQXJCO0FBQ0EsUUFBSXFiLGlCQUFpQlIsZUFBZTdhLElBQWYsQ0FBb0IsV0FBcEIsQ0FBckI7QUFDQSxRQUFJc2IsZUFBZUYsZUFBZTdSLEdBQWYsRUFBbkI7QUFDQSxRQUFJZ1MsZUFBZUYsZUFBZTlSLEdBQWYsRUFBbkI7QUFDQSxRQUFJaVMsV0FBV0osZUFBZTllLElBQWYsQ0FBb0IsS0FBcEIsQ0FBZjtBQUNBLFFBQUltZixXQUFXSixlQUFlL2UsSUFBZixDQUFvQixLQUFwQixDQUFmO0FBQ0EsUUFBSWtmLFdBQVdGLFlBQVgsSUFBMkJHLFdBQVdGLFlBQTFDLEVBQXdEO0FBQ3ZETixhQUFRLFNBQVIsSUFBcUJLLFlBQXJCO0FBQ0FMLGFBQVEsU0FBUixJQUFxQk0sWUFBckI7QUFDQTs7QUFFRC9lLE1BQUV3TSxJQUFGLENBQU87QUFDTi9JLFdBQU0sTUFEQTtBQUVOZ0osVUFBSyxxQkFGQztBQUdOM00sV0FBTTtBQUNMMGUsOEJBREs7QUFFTEM7QUFGSyxNQUhBO0FBT05TLGVBQVUsTUFQSjtBQVFOekssY0FBUyxpQkFBUzFQLFFBQVQsRUFBbUI7QUFDM0IsVUFBSUEsU0FBUzBQLE9BQWIsRUFBc0I7QUFDckJzQyxnQkFBU3ZULElBQVQsQ0FBYyxRQUFkLEVBQXdCbUMsSUFBeEIsQ0FBNkJaLFNBQVN1UixLQUF0QztBQUNBUyxnQkFBU3ZULElBQVQsQ0FBYyxpQkFBZCxFQUFpQzFELElBQWpDLENBQXNDLEtBQXRDLEVBQTZDaUYsU0FBUzBILEdBQXREOztBQUVBLFdBQUd6TSxFQUFFVSxNQUFGLEVBQVUrRixLQUFWLEtBQXFCLElBQXhCLEVBQThCO0FBQzdCc1EsaUJBQVNqUSxHQUFULENBQWE7QUFDWixpQkFBUUcsU0FBU0wsU0FBU3VZLElBQVQsR0FBZ0JaLE9BQU85WCxLQUFQLEVBQXpCLElBQTJDc1EsU0FBU3RRLEtBQVQsS0FBbUIsQ0FEMUQ7QUFFWixnQkFBT0csU0FBU0MsR0FGSjtBQUdaLG9CQUFXO0FBSEMsU0FBYjtBQUtBO0FBQ0Q7QUFDRDtBQXJCSyxLQUFQO0FBdUJBO0FBbkVhLEdBQWY7O0FBc0VBMFcsV0FBU2EsTUFBVCxDQUFnQixZQUFZO0FBQzNCLE9BQUlnQixXQUFXcGYsRUFBRSxJQUFGLEVBQVErTSxHQUFSLEVBQWY7QUFDQSxPQUFJc1MsV0FBVzdCLFNBQVN6USxHQUFULEVBQWY7O0FBRUEsT0FBSTlGLFNBQVNtWSxRQUFULElBQXFCblksU0FBU29ZLFFBQVQsQ0FBekIsRUFBNkM7QUFDNUNELGVBQVdDLFFBQVg7QUFDQTlCLGFBQVN4USxHQUFULENBQWFxUyxRQUFiO0FBQ0E7O0FBRUQvQixnQkFBYXJWLE1BQWIsQ0FBb0IsUUFBcEIsRUFBOEIsQ0FBOUIsRUFBaUNvWCxRQUFqQztBQUNBLEdBVkQ7O0FBWUE1QixXQUFTWSxNQUFULENBQWdCLFlBQVk7QUFDM0IsT0FBSWdCLFdBQVc3QixTQUFTeFEsR0FBVCxFQUFmO0FBQ0EsT0FBSXNTLFdBQVdyZixFQUFFLElBQUYsRUFBUStNLEdBQVIsRUFBZjs7QUFFQSxPQUFJc1MsV0FBVzFCLEdBQWYsRUFBb0I7QUFDbkIwQixlQUFXMUIsR0FBWDtBQUNBSCxhQUFTelEsR0FBVCxDQUFhNFEsR0FBYjtBQUNBOztBQUVELE9BQUkxVyxTQUFTbVksUUFBVCxJQUFxQm5ZLFNBQVNvWSxRQUFULENBQXpCLEVBQTZDO0FBQzVDQSxlQUFXRCxRQUFYO0FBQ0E1QixhQUFTelEsR0FBVCxDQUFhc1MsUUFBYjtBQUNBOztBQUVEaEMsZ0JBQWFyVixNQUFiLENBQW9CLFFBQXBCLEVBQThCLENBQTlCLEVBQWlDcVgsUUFBakM7QUFDQSxHQWZEOztBQWlCQTlCLFdBQVMrQixRQUFULENBQWtCLFVBQVVsWSxLQUFWLEVBQWlCO0FBQ2xDLE9BQUkyQyxZQUFKO0FBQ0EsT0FBSXdWLGdCQUFKOztBQUVBLE9BQUluWSxNQUFNb1ksT0FBVixFQUFtQjtBQUNsQnpWLFVBQU0zQyxNQUFNb1ksT0FBWjtBQUNBLElBRkQsTUFFTyxJQUFJcFksTUFBTXFZLEtBQVYsRUFBaUI7QUFDdkIxVixVQUFNM0MsTUFBTXFZLEtBQVo7QUFDQTs7QUFFRCxPQUFJMVYsUUFBUSxJQUFSLElBQWdCQSxRQUFRLENBQXhCLElBQTZCQSxRQUFRLENBQXJDLElBQTBDQSxRQUFRLEVBQWxELElBQXdEQSxRQUFRLENBQWhFLElBQXFFQSxRQUFRLEVBQTdFLElBQW1GQSxRQUFRLEVBQTNGLElBQWlHQSxRQUFRLEVBQTdHLEVBQWlIO0FBQ2hILFdBQU8sSUFBUDtBQUNBO0FBQ0R3VixhQUFVdEwsT0FBT3lMLFlBQVAsQ0FBb0IzVixHQUFwQixDQUFWOztBQUVBLE9BQUksQ0FBQyxXQUFXNUYsSUFBWCxDQUFnQm9iLE9BQWhCLENBQUwsRUFBK0I7QUFDOUIsV0FBTyxLQUFQO0FBQ0E7QUFDRCxHQWxCRDs7QUFvQkEvQixXQUFTOEIsUUFBVCxDQUFrQixVQUFVbFksS0FBVixFQUFpQjtBQUNsQyxPQUFJMkMsWUFBSjtBQUNBLE9BQUl3VixnQkFBSjs7QUFFQSxPQUFJblksTUFBTW9ZLE9BQVYsRUFBbUI7QUFDbEJ6VixVQUFNM0MsTUFBTW9ZLE9BQVo7QUFDQSxJQUZELE1BRU8sSUFBSXBZLE1BQU1xWSxLQUFWLEVBQWlCO0FBQ3ZCMVYsVUFBTTNDLE1BQU1xWSxLQUFaO0FBQ0E7O0FBRUQsT0FBSTFWLFFBQVEsSUFBUixJQUFnQkEsUUFBUSxDQUF4QixJQUE2QkEsUUFBUSxDQUFyQyxJQUEwQ0EsUUFBUSxFQUFsRCxJQUF3REEsUUFBUSxDQUFoRSxJQUFxRUEsUUFBUSxFQUE3RSxJQUFtRkEsUUFBUSxFQUEzRixJQUFpR0EsUUFBUSxFQUE3RyxFQUFpSDtBQUNoSCxXQUFPLElBQVA7QUFDQTtBQUNEd1YsYUFBVXRMLE9BQU95TCxZQUFQLENBQW9CM1YsR0FBcEIsQ0FBVjs7QUFFQSxPQUFJLENBQUMsV0FBVzVGLElBQVgsQ0FBZ0JvYixPQUFoQixDQUFMLEVBQStCO0FBQzlCLFdBQU8sS0FBUDtBQUNBO0FBQ0QsR0FsQkQ7QUFtQkEsRUF2SkQ7QUF3SkE7O0FBRUQ7QUFDQTtBQUNBOztRQUVRakMsZSxHQUFBQSxlOzs7Ozs7Ozs7QUN0TFI7O0FBRUE7QUFDQTtBQUNBOzs7OztBQUVBLFNBQVNxQyxhQUFULENBQXdCMVIsSUFBeEIsRUFBOEJyQyxJQUE5QixFQUFvQ3VTLEtBQXBDLEVBQTJDeUIsR0FBM0MsRUFBZ0Q7QUFDL0MsS0FBSXhTLGNBQUo7QUFDQSxLQUFJeVMsWUFBWTdmLEVBQUU0TCxPQUFPLE9BQVQsQ0FBaEI7QUFDQSxLQUFJL0MsY0FBSjs7QUFFQSxLQUFJZ1gsU0FBSixFQUFlO0FBQ2RBLFlBQVVyYyxJQUFWLENBQWUyYSxLQUFmLEVBQXNCMkIsU0FBdEIsQ0FBZ0MsTUFBaEM7QUFDQTs7QUFFRCxLQUFJRixHQUFKLEVBQVM7QUFDUjVmLElBQUVpTyxJQUFGLEVBQVF6SyxJQUFSLENBQWFvYyxHQUFiLEVBQWtCOUQsS0FBbEIsQ0FBd0IsVUFBVWxULENBQVYsRUFBYTtBQUNwQ0MsV0FBUTdJLEVBQUUsSUFBRixDQUFSO0FBQ0FvTixXQUFRdkUsTUFBTXNELE9BQU4sQ0FBY25NLEVBQUU0TCxJQUFGLENBQWQsQ0FBUjs7QUFFQSxPQUFJd0IsTUFBTTVKLElBQU4sQ0FBVzJhLEtBQVgsRUFBa0J2ZSxNQUF0QixFQUE4QjtBQUM3QmdKLE1BQUV5TCxjQUFGOztBQUVBakgsVUFBTTVKLElBQU4sQ0FBVzJhLEtBQVgsRUFBa0I0QixXQUFsQixDQUE4QixNQUE5QjtBQUNBM1MsVUFBTTRTLFdBQU4sQ0FBa0IsTUFBbEI7QUFDQW5YLFVBQU1zRCxPQUFOLENBQWM4QixJQUFkLEVBQW9CK1IsV0FBcEIsQ0FBZ0MsTUFBaEM7QUFDQTtBQUNELEdBWEQ7QUFZQSxFQWJELE1BYU87QUFDTmhnQixJQUFFaU8sSUFBRixFQUFRNk4sS0FBUixDQUFjLFVBQVVsVCxDQUFWLEVBQWE7QUFDMUJDLFdBQVE3SSxFQUFFLElBQUYsQ0FBUjtBQUNBb04sV0FBUXZFLE1BQU1zRCxPQUFOLENBQWNuTSxFQUFFNEwsSUFBRixDQUFkLENBQVI7O0FBRUEsT0FBSXdCLE1BQU01SixJQUFOLENBQVcyYSxLQUFYLEVBQWtCdmUsTUFBdEIsRUFBOEI7QUFDN0JnSixNQUFFeUwsY0FBRjs7QUFFQWpILFVBQU01SixJQUFOLENBQVcyYSxLQUFYLEVBQWtCNEIsV0FBbEIsQ0FBOEIsTUFBOUI7QUFDQTNTLFVBQU00UyxXQUFOLENBQWtCLE1BQWxCO0FBQ0FuWCxVQUFNbVgsV0FBTixDQUFrQixNQUFsQjtBQUNBO0FBQ0QsR0FYRDtBQVlBO0FBQ0Q7O0FBRUQ7QUFDQTtBQUNBOztRQUVRTCxhLEdBQUFBLGE7Ozs7Ozs7QUNoRFI7O0FBRUE7QUFDQTtBQUNBOzs7Ozs7O0FBRUE7Ozs7QUFDQTs7OztBQUNBOztBQUNBOztBQUNBOzs7O0FBRUE7QUFDQTtBQUNBOztBQUVBLElBQUlNLGlCQUFpQmpnQixFQUFFLG1CQUFGLENBQXJCO0FBQ0EsSUFBSWtnQix1QkFBdUJsZ0IsRUFBRSw4QkFBRixDQUEzQjtBQUNBO0FBQ0E7O0FBRUE7O0FBRUEsU0FBU3lWLFlBQVQsQ0FBc0JDLFVBQXRCLEVBQWtDQyxZQUFsQyxFQUFnREMsU0FBaEQsRUFBMkQ7QUFDMURELGdCQUFlQSxnQkFBZ0IsR0FBL0I7QUFDQUMsYUFBWUEsYUFBYSxHQUF6QjtBQUNBRixjQUFhLEtBQUtBLFVBQWxCO0FBQ0FBLGNBQWFBLFdBQVd6RSxLQUFYLENBQWlCMkUsU0FBakIsQ0FBYjs7QUFFQSxLQUFJQyxjQUFjSCxXQUFXLENBQVgsS0FBaUIsRUFBbkM7QUFDQSxLQUFJSSxjQUFjSixXQUFXLENBQVgsQ0FBbEI7O0FBRUEsS0FBSUssaUJBQWlCLEVBQXJCO0FBQ0EsS0FBSUMsVUFBVSxDQUFkOztBQUVBLE1BQUssSUFBSXpWLElBQUl1VixZQUFZbFcsTUFBWixHQUFxQixDQUFsQyxFQUFxQ1csS0FBSyxDQUExQyxFQUE2Q0EsR0FBN0MsRUFBa0Q7QUFDakQsTUFBSTBILE1BQU02TixZQUFZdlYsQ0FBWixDQUFWO0FBQ0F3VixpQkFBZUUsSUFBZixDQUFvQmhPLEdBQXBCOztBQUVBLE1BQUksRUFBRStOLE9BQUYsS0FBYyxDQUFkLElBQW1CelYsQ0FBdkIsRUFBMEI7QUFDekJ3VixrQkFBZUUsSUFBZixDQUFvQixHQUFwQjtBQUNBRCxhQUFVLENBQVY7QUFDQTtBQUNEOztBQUVERCxrQkFBaUJBLGVBQWVHLE9BQWYsR0FBeUJDLElBQXpCLENBQThCLEVBQTlCLENBQWpCO0FBQ0EsS0FBSU4sWUFBWWpXLE1BQWhCLEVBQXdCO0FBQ3ZCLFNBQU8sQ0FBQ21XLGNBQUQsRUFBaUJGLFdBQWpCLEVBQThCTSxJQUE5QixDQUFtQ1IsWUFBbkMsQ0FBUDtBQUNBO0FBQ0QsUUFBT0ksY0FBUDtBQUNBOztBQUVELFNBQVNvSyxNQUFULENBQWdCNVosSUFBaEIsRUFBc0I7QUFDckIsS0FBSTZaLFFBQVE3WixJQUFaOztBQUVBMlosc0JBQXFCRyxPQUFyQixDQUE2QixNQUE3Qjs7QUFFQSxLQUFJcmdCLEVBQUVvZ0IsS0FBRixFQUFTMVIsSUFBVCxDQUFjLFNBQWQsQ0FBSixFQUE4QjtBQUM3QixNQUFJNFIsV0FBV3RnQixFQUFFb2dCLEtBQUYsRUFBU3JYLE1BQVQsR0FBa0JvRSxJQUFsQixDQUF1Qiw4QkFBdkIsQ0FBZjs7QUFFQW1ULFdBQVNSLFNBQVQsQ0FBbUIsTUFBbkI7QUFDQTtBQUNEOztBQUVELFNBQVNTLGFBQVQsQ0FBdUJwSixLQUF2QixFQUE4QnBLLEdBQTlCLEVBQW1DO0FBQ2xDLEtBQUl3UixTQUFTcEgsTUFBTTNULElBQU4sQ0FBVyxtQkFBWCxDQUFiO0FBQ0EsS0FBSTRULFNBQVNELE1BQU0zVCxJQUFOLENBQVcsbUJBQVgsQ0FBYjtBQUNBLEtBQUlnZCxhQUFhckosTUFBTTNULElBQU4sQ0FBVyx3QkFBWCxDQUFqQjtBQUNBLEtBQUlpZCxZQUFZdEosTUFBTTNULElBQU4sQ0FBVyxtQkFBWCxDQUFoQjtBQUNBLEtBQUlrZCxZQUFZdkosTUFBTTNULElBQU4sQ0FBVyx1QkFBWCxDQUFoQjs7QUFFQSxLQUFJbWQsWUFBWXhKLE1BQU1uUSxRQUFOLENBQWUsV0FBZixDQUFoQjtBQUNBLEtBQUk0WixjQUFjM1osU0FBU3NYLE9BQU96ZSxJQUFQLENBQVksS0FBWixDQUFULENBQWxCO0FBQ0EsS0FBSStnQixjQUFjN2dCLEVBQUU4Z0IsU0FBRixDQUFZdkMsT0FBT3hSLEdBQVAsRUFBWixJQUE0QjlGLFNBQVNzWCxPQUFPeFIsR0FBUCxFQUFULENBQTVCLEdBQXFELENBQXZFO0FBQ0EsS0FBSWdVLE9BQU9yRCxXQUFXK0MsVUFBVTNnQixJQUFWLENBQWUsT0FBZixDQUFYLENBQVg7QUFDQSxLQUFJd1csUUFBUSxDQUFaO0FBQ0EsS0FBSTBLLGNBQWNILFdBQWxCOztBQUVBN2dCLEdBQUUsa0JBQUYsRUFBc0JtSCxFQUF0QixDQUF5QixPQUF6QixFQUFrQyxZQUFZO0FBQzdDLE1BQUk4WixXQUFXamhCLEVBQUUsSUFBRixFQUFRa2hCLFFBQVIsQ0FBaUIsbUJBQWpCLENBQWY7O0FBRUEsTUFBSUQsU0FBU2xVLEdBQVQsTUFBa0JrVSxTQUFTbmhCLElBQVQsQ0FBYyxLQUFkLENBQXRCLEVBQTRDO0FBQzNDLE9BQUlxaEIsVUFBVW5oQixFQUFFLGVBQUYsQ0FBZDtBQUNBbWhCLFdBQVFqZSxJQUFSOztBQUVBdkMsY0FBVyxZQUFZO0FBQ3RCd2dCLFlBQVFsYyxJQUFSO0FBQ0EsSUFGRCxFQUVHLElBRkg7QUFHQTtBQUNELEVBWEQ7O0FBYUEsS0FBSW1TLE9BQU94WCxNQUFYLEVBQW1CO0FBQ2xCLE1BQUl3aEIsY0FBZVQsYUFBYXZKLE9BQU94WCxNQUFyQixHQUErQndYLE9BQU94WCxNQUF0QyxHQUErQyxDQUFqRTtBQUNBLE1BQUlnWCxTQUFTLEVBQWI7O0FBRUFRLFNBQU85VyxJQUFQLENBQVksWUFBWTtBQUN2QixPQUFJa2dCLGFBQWF4Z0IsRUFBRSxJQUFGLENBQWpCO0FBQ0EsT0FBSXFoQixXQUFXcGEsU0FBU3VaLFdBQVc3YSxJQUFYLEVBQVQsQ0FBZjtBQUNBLE9BQUkyYixXQUFXcmEsU0FBU3VaLFdBQVcxZ0IsSUFBWCxDQUFnQixLQUFoQixDQUFULENBQWY7QUFDQSxPQUFJeWhCLFdBQVdGLFFBQWY7O0FBRUEsT0FBSXRVLFFBQVEsTUFBWixFQUFvQjtBQUNuQndVLGVBQVdGLFdBQVdDLFFBQVgsR0FBdUJELFdBQVcsQ0FBbEMsR0FBdUNDLFFBQWxEO0FBQ0EsSUFGRCxNQUVPLElBQUl2VSxRQUFRLE9BQVosRUFBcUI7QUFDM0J3VSxlQUFXRixXQUFXLENBQVgsR0FBZ0JBLFdBQVcsQ0FBM0IsR0FBaUNDLFdBQVcsQ0FBWCxHQUFlLENBQTNEO0FBQ0EsSUFGTSxNQUVBO0FBQ05DLGVBQVlWLGNBQWNTLFFBQWYsR0FBMkJBLFFBQTNCLEdBQXdDVCxjQUFjLENBQWYsR0FBb0IsQ0FBcEIsR0FBd0JBLFdBQTFFO0FBQ0E7O0FBRUR2SyxZQUFTaUwsUUFBVDs7QUFFQTNLLFVBQU9YLElBQVAsQ0FBWXNMLFdBQVcsRUFBdkI7O0FBRUFmLGNBQVc1TCxJQUFYLENBQWdCMk0sV0FBV0EsUUFBWCxHQUFzQixFQUF0QyxFQWxCdUIsQ0FrQm9CO0FBQzNDLEdBbkJEOztBQXFCQWYsYUFBV2xnQixJQUFYLENBQWdCLFlBQVk7QUFDM0IsT0FBSWtnQixhQUFheGdCLEVBQUUsSUFBRixDQUFqQjtBQUNBLE9BQUlrTyxRQUFRc1MsV0FBVzFnQixJQUFYLENBQWdCLE9BQWhCLENBQVo7QUFDQW9PLFNBQU1BLEtBQU4sQ0FBWTBJLE1BQVosR0FBcUJBLE1BQXJCO0FBQ0E0SixjQUFXMWdCLElBQVgsQ0FBZ0IsT0FBaEIsRUFBeUJvTyxLQUF6QjtBQUNBLEdBTEQ7O0FBT0EsTUFBSW5CLFFBQVEsTUFBWixFQUFvQjtBQUNuQmlVLGlCQUFjSCxjQUFjRCxXQUFkLEdBQTZCQyxjQUFjLENBQTNDLEdBQWdERCxXQUE5RDtBQUNBLEdBRkQsTUFFTyxJQUFJN1QsUUFBUSxPQUFaLEVBQXFCO0FBQzNCaVUsaUJBQWNILGNBQWMsQ0FBZCxHQUFtQkEsY0FBYyxDQUFqQyxHQUFzQ08sV0FBcEQ7QUFDQSxHQUZNLE1BRUE7QUFDTkosaUJBQWVILGNBQWNELFdBQWYsR0FBOEJBLFdBQTlCLEdBQThDQyxjQUFjLENBQWYsR0FBb0JPLFdBQXBCLEdBQWtDUCxXQUE3RjtBQUNBOztBQUVEdEMsU0FBT3hSLEdBQVAsQ0FBV3VKLEtBQVg7O0FBRUFhLFFBQU1yWCxJQUFOLENBQVcsUUFBWCxFQUFxQjhXLE1BQXJCO0FBQ0FPLFFBQU1yWCxJQUFOLENBQVcsT0FBWCxFQUFvQmtoQixXQUFwQjs7QUFFQVAsWUFBVTdMLElBQVYsQ0FBZWEsYUFBYSxDQUFDYSxRQUFReUssSUFBVCxFQUFldkssT0FBZixDQUF1QixDQUF2QixDQUFiLENBQWY7QUFDQWtLLFlBQVU5TCxJQUFWLENBQWUwQixLQUFmO0FBQ0EsRUEvQ0QsTUErQ087QUFDTixNQUFJcUssU0FBSixFQUFlO0FBQ2R4SixTQUFNaEwsT0FBTixDQUFjLE1BQWQsRUFBc0IzSSxJQUF0QixDQUEyQixtQkFBM0IsRUFBZ0RsRCxJQUFoRCxDQUFxRCxZQUFZO0FBQ2hFLFFBQUlraEIsTUFBTXhoQixFQUFFLElBQUYsQ0FBVjs7QUFFQSxRQUFJeWhCLGVBQWV4YSxTQUFTdWEsSUFBSTFoQixJQUFKLENBQVMsS0FBVCxDQUFULENBQW5CO0FBQ0EsUUFBSTRoQixlQUFlYixXQUFuQjtBQUNBLFFBQUljLFdBQVdILElBQUl6VSxHQUFKLEVBQWY7O0FBRUEsUUFBSUEsUUFBUSxNQUFaLEVBQW9CO0FBQ25CMlUsb0JBQWUsRUFBRUMsUUFBRixHQUFhRixZQUFiLEdBQTRCRSxRQUE1QixHQUF1Q0YsWUFBdEQ7QUFDQSxLQUZELE1BRU8sSUFBSTFVLFFBQVEsT0FBWixFQUFxQjtBQUMzQjJVLG9CQUFlLEVBQUVDLFFBQUYsR0FBYSxDQUFiLEdBQWlCQSxRQUFqQixHQUE0QixDQUEzQztBQUNBLEtBRk0sTUFFQTtBQUNORCxvQkFBZ0JiLGNBQWNZLFlBQWYsR0FBK0JBLFlBQS9CLEdBQWdEWixjQUFjLENBQWYsR0FBb0IsQ0FBcEIsR0FBd0JBLFdBQXRGO0FBQ0E7O0FBRURXLFFBQUl6VSxHQUFKLENBQVEyVSxZQUFSO0FBQ0EsSUFoQkQ7QUFpQkE7O0FBRUQsTUFBSTNVLFFBQVEsTUFBWixFQUFvQjtBQUNuQmlVLGlCQUFjSCxjQUFjRCxXQUFkLEdBQTZCQyxjQUFjLENBQTNDLEdBQWdERCxXQUE5RDtBQUNBLEdBRkQsTUFFTyxJQUFJN1QsUUFBUSxPQUFaLEVBQXFCO0FBQzNCaVUsaUJBQWNILGNBQWMsQ0FBZCxHQUFtQkEsY0FBYyxDQUFqQyxHQUFzQyxDQUFwRDtBQUNBLEdBRk0sTUFFQTtBQUNORyxpQkFBZUgsY0FBY0QsV0FBZixHQUE4QkEsV0FBOUIsR0FBOENDLGNBQWMsQ0FBZixHQUFvQixDQUFwQixHQUF3QkEsV0FBbkY7QUFDQTs7QUFFRHRDLFNBQU94UixHQUFQLENBQVdpVSxXQUFYOztBQUVBN0osUUFBTXJYLElBQU4sQ0FBVyxRQUFYLEVBQXFCa2hCLFdBQXJCO0FBQ0E3SixRQUFNclgsSUFBTixDQUFXLE9BQVgsRUFBb0JraEIsV0FBcEI7O0FBRUFQLFlBQVU3TCxJQUFWLENBQWVhLGFBQWEsQ0FBQ3VMLGNBQWNELElBQWYsRUFBcUJ2SyxPQUFyQixDQUE2QixDQUE3QixDQUFiLENBQWY7QUFDQWtLLFlBQVU5TCxJQUFWLENBQWVvTSxXQUFmO0FBQ0E7O0FBRUQsS0FBSXpDLE9BQU94UixHQUFQLEtBQWUsQ0FBbkIsRUFBc0I7QUFDckIvTSxJQUFFLE1BQUYsRUFBVW1ILEVBQVYsQ0FBYSxPQUFiLEVBQXNCLFlBQXRCLEVBQW9DLFlBQVk7QUFDL0N4RyxjQUFXLFlBQVk7QUFDdEJYLE1BQUUsWUFBRixFQUFnQm1NLE9BQWhCLENBQXdCLGNBQXhCLEVBQXdDM0ksSUFBeEMsQ0FBNkMsWUFBN0MsRUFBMkRrSyxPQUEzRCxDQUFtRSxPQUFuRTtBQUNBMU4sTUFBRSxnQ0FBRixFQUFvQzBOLE9BQXBDLENBQTRDLE9BQTVDO0FBQ0EsSUFIRCxFQUdHLElBSEg7QUFJQSxHQUxEO0FBTUE7QUFDRDs7QUFFRDFOLEVBQUUsTUFBRixFQUFVbUgsRUFBVixDQUFhLGNBQWIsRUFBNkIsa0JBQTdCLEVBQWlELFlBQVk7QUFDNUQsS0FBSTBCLFFBQVE3SSxFQUFFLElBQUYsQ0FBWjtBQUNBLEtBQUltWCxRQUFRdE8sTUFBTXNELE9BQU4sQ0FBYyxrQkFBZCxDQUFaO0FBQ0EsS0FBSXlWLGFBQWEsbUJBQWpCOztBQUVBLEtBQUkvWSxNQUFNN0IsUUFBTixDQUFlLGtCQUFmLENBQUosRUFBd0M7QUFDdkN1WixnQkFBY3BKLEtBQWQsRUFBcUIsT0FBckI7QUFDQXRPLFFBQU1zRCxPQUFOLENBQWMsTUFBZCxFQUFzQjhMLE1BQXRCO0FBQ0E7O0FBRUQsS0FBSXBQLE1BQU03QixRQUFOLENBQWUsaUJBQWYsQ0FBSixFQUF1Qzs7QUFFdEMsTUFBSTZCLE1BQU1xWSxRQUFOLENBQWVVLFVBQWYsRUFBMkI3VSxHQUEzQixNQUFvQ2xFLE1BQU1xWSxRQUFOLENBQWVVLFVBQWYsRUFBMkI5aEIsSUFBM0IsQ0FBZ0MsS0FBaEMsQ0FBeEMsRUFBZ0Y7O0FBRS9FLE9BQUlxaEIsVUFBVW5oQixFQUFFLGVBQUYsQ0FBZDtBQUNBbWhCLFdBQVFqZSxJQUFSOztBQUVBcWQsaUJBQWNwSixLQUFkLEVBQXFCLEtBQXJCOztBQUVBeFcsY0FBVyxZQUFZO0FBQ3RCd2dCLFlBQVFsYyxJQUFSO0FBQ0EsSUFGRCxFQUVHLElBRkg7QUFJQSxHQVhELE1BV087QUFDTnNiLGlCQUFjcEosS0FBZCxFQUFxQixNQUFyQjtBQUNBdE8sU0FBTXNELE9BQU4sQ0FBYyxNQUFkLEVBQXNCOEwsTUFBdEI7QUFDQTtBQUVEO0FBRUQsQ0E5QkQ7O0FBZ0NBalksRUFBRSx5QkFBRixFQUE2Qm1ILEVBQTdCLENBQWdDLE9BQWhDLEVBQXlDLFlBQVc7QUFDbkQsS0FBSTBhLFVBQVU3aEIsRUFBRSx3QkFBRixDQUFkO0FBQ0EsS0FBSTRmLE1BQU01ZixFQUFFLHlCQUFGLENBQVY7O0FBRUEsS0FBSTZoQixRQUFRN2EsUUFBUixDQUFpQixRQUFqQixDQUFKLEVBQWdDO0FBQy9CNmEsVUFBUS9nQixXQUFSLENBQW9CLFFBQXBCO0FBQ0ErZ0IsVUFBUXhCLE9BQVIsQ0FBZ0IsR0FBaEI7QUFDQVQsTUFBSTlZLEdBQUosQ0FBUSxjQUFSLEVBQXdCLFNBQXhCO0FBQ0EsRUFKRCxNQUlPO0FBQ04rYSxVQUFRcGhCLFFBQVIsQ0FBaUIsUUFBakI7QUFDQW9oQixVQUFRL0IsU0FBUixDQUFrQixHQUFsQjtBQUNBRixNQUFJOVksR0FBSixDQUFRLGNBQVIsRUFBd0IsU0FBeEI7O0FBR0E7O0FBRUE5RyxJQUFFLGlCQUFGLEVBQXFCd0QsSUFBckIsQ0FBMEIsUUFBMUIsRUFBb0NsRCxJQUFwQyxDQUF5QyxVQUFVQyxDQUFWLEVBQVlxSSxDQUFaLEVBQWM7QUFDdEQ1SSxLQUFFNEksQ0FBRixFQUFLd0QsT0FBTCxDQUFhO0FBQ1pDLDZCQUF5QnlWLFFBRGI7QUFFWnhWLG9CQUFnQnRNLEVBQUU0SSxDQUFGLEVBQUt1RCxPQUFMLENBQWEsZ0JBQWIsQ0FGSjtBQUdaMUYsV0FBTyxTQUhLO0FBSVo4RixjQUFVdk0sRUFBRSxNQUFGLEVBQVVzRCxJQUFWLENBQWUsTUFBZixDQUpFO0FBS1p5ZSxzQkFBa0I7QUFMTixJQUFiO0FBT0EsR0FSRDtBQVNBO0FBQ0QsQ0ExQkQ7O0FBNEJBL2hCLEVBQUVrUCxhQUFGLENBQWdCcUYsUUFBaEIsQ0FBeUJXLEtBQXpCLEdBQWlDLFlBQVk7QUFDNUNsVixHQUFFLHdCQUFGLEVBQTRCYyxXQUE1QixDQUF3QyxRQUF4QztBQUNBZCxHQUFFLHlCQUFGLEVBQTZCOEcsR0FBN0IsQ0FBaUMsY0FBakMsRUFBaUQsU0FBakQ7QUFDQTlHLEdBQUVrUCxhQUFGLENBQWdCOFMsS0FBaEIsQ0FBc0I5TSxLQUF0QixDQUE0QjdDLElBQTVCLENBQWlDLElBQWpDO0FBQ0EsQ0FKRDs7QUFNQXJTLEVBQUUsTUFBRixFQUFVbUgsRUFBVixDQUFhLE9BQWIsRUFBc0IsdUJBQXRCLEVBQStDLFlBQVc7QUFDekQsS0FBSThhLFlBQVlqaUIsRUFBRSxJQUFGLEVBQVFzRCxJQUFSLENBQWEsU0FBYixDQUFoQjtBQUNBLEtBQUk0ZSxVQUFVbGlCLEVBQUUsSUFBRixFQUFRc2UsT0FBUixDQUFnQix3QkFBaEIsRUFBMEM5YSxJQUExQyxDQUErQyx1QkFBdUJ5ZSxTQUF2QixHQUFtQyxJQUFsRixDQUFkOztBQUVBamlCLEdBQUUsSUFBRixFQUFRc2UsT0FBUixDQUFnQiwwQkFBaEIsRUFBNEM5YSxJQUE1QyxDQUFpRCxTQUFqRCxFQUE0RDFDLFdBQTVELENBQXdFLFFBQXhFO0FBQ0FkLEdBQUUsSUFBRixFQUFRUyxRQUFSLENBQWlCLFFBQWpCO0FBQ0FULEdBQUUsWUFBRixFQUFnQmMsV0FBaEIsQ0FBNEIsV0FBNUI7QUFDQW9oQixTQUFRemhCLFFBQVIsQ0FBaUIsV0FBakI7O0FBRUE7O0FBRUFULEdBQUUsaUJBQUYsRUFBcUJ3RCxJQUFyQixDQUEwQixRQUExQixFQUFvQ2xELElBQXBDLENBQXlDLFVBQVVDLENBQVYsRUFBWXFJLENBQVosRUFBYztBQUN0RDVJLElBQUU0SSxDQUFGLEVBQUt3RCxPQUFMLENBQWE7QUFDWkMsNEJBQXlCeVYsUUFEYjtBQUVaeFYsbUJBQWdCdE0sRUFBRTRJLENBQUYsRUFBS3VELE9BQUwsQ0FBYSxnQkFBYixDQUZKO0FBR1oxRixVQUFPLFNBSEs7QUFJWjhGLGFBQVV2TSxFQUFFLE1BQUYsRUFBVXNELElBQVYsQ0FBZSxNQUFmLENBSkU7QUFLWnllLHFCQUFrQjtBQUxOLEdBQWI7QUFPQSxFQVJEO0FBU0EsQ0FwQkQ7O0FBc0JBL2hCLEVBQUUsTUFBRixFQUFVbUgsRUFBVixDQUFhLFFBQWIsRUFBdUIsMkJBQXZCLEVBQW9ELFVBQVV5QixDQUFWLEVBQVk7QUFDL0RBLEdBQUV5TCxjQUFGOztBQUVBLEtBQUlnSyxpQkFBaUJyZSxFQUFFLElBQUYsRUFBUXNlLE9BQVIsQ0FBZ0IsVUFBaEIsRUFBNEI5YSxJQUE1QixDQUFpQyxzQkFBakMsQ0FBckI7O0FBRUEsS0FBSXhELEVBQUVVLE1BQUYsRUFBVStGLEtBQVYsS0FBb0IsSUFBeEIsRUFBOEI7QUFDN0I0WCxtQkFBaUJyZSxFQUFFLElBQUYsRUFBUXNlLE9BQVIsQ0FBZ0IsTUFBaEIsRUFBd0I5YSxJQUF4QixDQUE2QiwwQkFBN0IsQ0FBakI7QUFDQTs7QUFFRCxLQUFJZ2IsY0FBY0gsZUFBZXZlLElBQWYsQ0FBb0IsVUFBcEIsQ0FBbEI7QUFDQSxLQUFJMmUsVUFBVSxFQUFkOztBQUVBSixnQkFBZTdhLElBQWYsQ0FBb0IsdUJBQXBCLEVBQTZDbEQsSUFBN0MsQ0FBa0QsWUFBVztBQUM1RCxNQUFJMFIsT0FBT2hTLEVBQUUsSUFBRixFQUFRc0QsSUFBUixDQUFhLE1BQWIsQ0FBWDtBQUNBLE1BQUksQ0FBQ21iLFFBQVFDLGNBQVIsQ0FBdUIxTSxJQUF2QixDQUFMLEVBQW1DeU0sUUFBUXpNLElBQVIsSUFBZ0IsRUFBaEI7O0FBRW5DLE1BQUlqUixRQUFRZixFQUFFLElBQUYsRUFBUStNLEdBQVIsRUFBWjs7QUFFQSxNQUFJLENBQUMwUixRQUFRek0sSUFBUixFQUFjMk0sUUFBZCxDQUF1QjVkLEtBQXZCLENBQUwsRUFBb0MwZCxRQUFRek0sSUFBUixFQUFjaUUsSUFBZCxDQUFtQmxWLEtBQW5CO0FBQ3BDLEVBUEQ7O0FBU0EsS0FBSTZkLGlCQUFpQlAsZUFBZTdhLElBQWYsQ0FBb0IsV0FBcEIsQ0FBckI7QUFDQSxLQUFJcWIsaUJBQWlCUixlQUFlN2EsSUFBZixDQUFvQixXQUFwQixDQUFyQjtBQUNBLEtBQUlzYixlQUFlRixlQUFlN1IsR0FBZixFQUFuQjtBQUNBLEtBQUlnUyxlQUFlRixlQUFlOVIsR0FBZixFQUFuQjtBQUNBLEtBQUlpUyxXQUFXSixlQUFlOWUsSUFBZixDQUFvQixLQUFwQixDQUFmO0FBQ0EsS0FBSW1mLFdBQVdKLGVBQWUvZSxJQUFmLENBQW9CLEtBQXBCLENBQWY7QUFDQSxLQUFJa2YsV0FBV0YsWUFBWCxJQUEyQkcsV0FBV0YsWUFBMUMsRUFBd0Q7QUFDdkROLFVBQVEsU0FBUixJQUFxQkssWUFBckI7QUFDQUwsVUFBUSxTQUFSLElBQXFCTSxZQUFyQjtBQUNBOztBQUVELEtBQUlvRCxNQUFNbmlCLEVBQUUsOEJBQUYsRUFBa0NzRCxJQUFsQyxDQUF1QyxZQUF2QyxDQUFWO0FBQ0EsS0FBSThlLE1BQU1waUIsRUFBRSxzQkFBRixFQUEwQndELElBQTFCLENBQStCLGdCQUEvQixFQUFpRHVKLEdBQWpELEVBQVY7O0FBRUEwUixTQUFRLFlBQVIsSUFBd0IsQ0FBQzBELEdBQUQsQ0FBeEI7QUFDQTFELFNBQVEsS0FBUixJQUFpQixDQUFDMkQsR0FBRCxDQUFqQjs7QUFFQSxLQUFJeFYsU0FBUyxJQUFiO0FBQ0EsS0FBTUYsU0FBUyxJQUFJMlYsZUFBSixDQUFvQjNoQixPQUFPMEUsUUFBUCxDQUFnQndILE1BQXBDLENBQWY7QUFDQSxLQUFJRixPQUFPNFYsR0FBUCxDQUFXLFFBQVgsQ0FBSixFQUEwQjFWLFNBQVNGLE9BQU9zTCxHQUFQLENBQVcsUUFBWCxDQUFUOztBQUUxQmhZLEdBQUV3TSxJQUFGLENBQU87QUFDTi9JLFFBQU0sTUFEQTtBQUVOZ0osT0FBSyxxQkFGQztBQUdOM00sUUFBTTtBQUNMMGUsMkJBREs7QUFFTEMsbUJBRks7QUFHTDdSO0FBSEssR0FIQTtBQVFOc1MsWUFBVSxNQVJKO0FBU056SyxXQUFTLGlCQUFTMVAsUUFBVCxFQUFtQjtBQUMzQixPQUFJQSxTQUFTMFAsT0FBVCxJQUFvQjFQLFNBQVMwSCxHQUFqQyxFQUFzQztBQUNyQy9MLFdBQU8wRSxRQUFQLENBQWdCcVMsSUFBaEIsR0FBdUIxUyxTQUFTMEgsR0FBaEM7QUFDQTtBQUNEO0FBYkssRUFBUDtBQWVBLENBekREOztBQTJEQXpNLEVBQUUsZ0JBQUYsRUFBb0JtSCxFQUFwQixDQUF1QixPQUF2QixFQUFnQyxZQUFXO0FBQzFDLEtBQUk0QixTQUFTL0ksRUFBRSxJQUFGLEVBQVFzZSxPQUFSLENBQWdCLHNCQUFoQixDQUFiO0FBQ0EsS0FBSTRELFVBQVVuWixPQUFPdkYsSUFBUCxDQUFZLHlCQUFaLENBQWQ7QUFDQSxLQUFJK2UsT0FBT3haLE9BQU92RixJQUFQLENBQVkscUJBQVosQ0FBWDs7QUFFQSxLQUFJMGUsUUFBUWxiLFFBQVIsQ0FBaUIsYUFBakIsQ0FBSixFQUFvQztBQUNuQ2tiLFVBQVFwaEIsV0FBUixDQUFvQixhQUFwQjtBQUNBb2hCLFVBQVFwQyxTQUFSLENBQWtCLEdBQWxCO0FBQ0E5ZixJQUFFLElBQUYsRUFBUVMsUUFBUixDQUFpQixRQUFqQjtBQUNBOGhCLE9BQUszTixJQUFMLENBQVUsR0FBVjtBQUNBLEVBTEQsTUFLTztBQUNOc04sVUFBUXpoQixRQUFSLENBQWlCLGFBQWpCO0FBQ0F5aEIsVUFBUTdCLE9BQVIsQ0FBZ0IsR0FBaEI7QUFDQXJnQixJQUFFLElBQUYsRUFBUWMsV0FBUixDQUFvQixRQUFwQjtBQUNBeWhCLE9BQUszTixJQUFMLENBQVUsR0FBVjtBQUNBO0FBRUQsQ0FqQkQ7O0FBbUJBNVUsRUFBRSxnQkFBRixFQUFvQm1ILEVBQXBCLENBQXVCLE9BQXZCLEVBQWdDLEdBQWhDLEVBQXFDLFVBQVNDLEtBQVQsRUFBZ0I7O0FBRXBELEtBQUksQ0FBQ3BILEVBQUUsSUFBRixFQUFRZ0gsUUFBUixDQUFpQixxQkFBakIsQ0FBTCxFQUE4QztBQUM3Q0ksUUFBTWlOLGNBQU47QUFDQSxNQUFJclUsRUFBRSxJQUFGLEVBQVFnSCxRQUFSLENBQWlCLFFBQWpCLENBQUosRUFBZ0M7QUFDL0JoSCxLQUFFLElBQUYsRUFBUWMsV0FBUixDQUFvQixRQUFwQjtBQUNBLEdBRkQsTUFFTztBQUNOZCxLQUFFLElBQUYsRUFBUVMsUUFBUixDQUFpQixRQUFqQjtBQUNBOztBQUVELE1BQUk0ZCxpQkFBaUJyZSxFQUFFLElBQUYsRUFBUXNlLE9BQVIsQ0FBZ0Isc0JBQWhCLENBQXJCO0FBQ0EsTUFBSXZILFdBQVcvVyxFQUFFLHlCQUFGLENBQWY7QUFDQSxNQUFJdWUsU0FBU3ZlLEVBQUUsSUFBRixFQUFRc2UsT0FBUixDQUFnQixnQkFBaEIsQ0FBYjtBQUNBLE1BQUkxWCxXQUFXMlgsT0FBTzNCLE1BQVAsRUFBZjs7QUFFQSxNQUFJNEIsY0FBY0gsZUFBZXZlLElBQWYsQ0FBb0IsVUFBcEIsQ0FBbEI7QUFDQSxNQUFJMmUsVUFBVSxFQUFkOztBQUVBSixpQkFBZTdhLElBQWYsQ0FBb0IsU0FBcEIsRUFBK0JsRCxJQUEvQixDQUFvQyxZQUFZO0FBQy9DLE9BQUkwUixPQUFPaFMsRUFBRSxJQUFGLEVBQVFzRCxJQUFSLENBQWEsV0FBYixDQUFYO0FBQ0EsT0FBSSxDQUFDbWIsUUFBUUMsY0FBUixDQUF1QjFNLElBQXZCLENBQUwsRUFBbUN5TSxRQUFRek0sSUFBUixJQUFnQixFQUFoQjs7QUFFbkMsT0FBSWpSLFFBQVFmLEVBQUUsSUFBRixFQUFRc0QsSUFBUixDQUFhLFlBQWIsQ0FBWjs7QUFFQSxPQUFJLENBQUNtYixRQUFRek0sSUFBUixFQUFjMk0sUUFBZCxDQUF1QjVkLEtBQXZCLENBQUwsRUFBb0MwZCxRQUFRek0sSUFBUixFQUFjaUUsSUFBZCxDQUFtQmxWLEtBQW5CO0FBQ3BDLEdBUEQ7O0FBU0EsTUFBSTZkLGlCQUFpQlAsZUFBZTdhLElBQWYsQ0FBb0IsV0FBcEIsQ0FBckI7QUFDQSxNQUFJcWIsaUJBQWlCUixlQUFlN2EsSUFBZixDQUFvQixXQUFwQixDQUFyQjtBQUNBLE1BQUlzYixlQUFlRixlQUFlN1IsR0FBZixFQUFuQjtBQUNBLE1BQUlnUyxlQUFlRixlQUFlOVIsR0FBZixFQUFuQjtBQUNBLE1BQUlpUyxXQUFXSixlQUFlOWUsSUFBZixDQUFvQixLQUFwQixDQUFmO0FBQ0EsTUFBSW1mLFdBQVdKLGVBQWUvZSxJQUFmLENBQW9CLEtBQXBCLENBQWY7QUFDQSxNQUFJa2YsV0FBV0YsWUFBWCxJQUEyQkcsV0FBV0YsWUFBMUMsRUFBd0Q7QUFDdkROLFdBQVEsU0FBUixJQUFxQkssWUFBckI7QUFDQUwsV0FBUSxTQUFSLElBQXFCTSxZQUFyQjtBQUNBOztBQUVELE1BQUluUyxTQUFTLElBQWI7QUFDQSxNQUFNRixTQUFTLElBQUkyVixlQUFKLENBQW9CM2hCLE9BQU8wRSxRQUFQLENBQWdCd0gsTUFBcEMsQ0FBZjtBQUNBLE1BQUlGLE9BQU80VixHQUFQLENBQVcsUUFBWCxDQUFKLEVBQTBCMVYsU0FBU0YsT0FBT3NMLEdBQVAsQ0FBVyxRQUFYLENBQVQ7O0FBRTFCLE1BQUloWSxFQUFFd2lCLGFBQUYsQ0FBZ0IvRCxPQUFoQixLQUE0QixDQUFDemUsRUFBRSx1QkFBRixFQUEyQkosTUFBNUQsRUFBb0U7QUFDbkVtWCxZQUFTalEsR0FBVCxDQUFhLFNBQWIsRUFBd0IsTUFBeEI7QUFDQTtBQUNBOztBQUVEOUcsSUFBRXdNLElBQUYsQ0FBTztBQUNOL0ksU0FBTSxNQURBO0FBRU5nSixRQUFLLHFCQUZDO0FBR04zTSxTQUFNO0FBQ0wwZSw0QkFESztBQUVMQyxvQkFGSztBQUdMN1I7QUFISyxJQUhBO0FBUU5zUyxhQUFVLE1BUko7QUFTTnpLLFlBQVMsaUJBQVUxUCxRQUFWLEVBQW9CO0FBQzVCLFFBQUlBLFNBQVMwUCxPQUFiLEVBQXNCO0FBQ3JCc0MsY0FBU3ZULElBQVQsQ0FBYyxRQUFkLEVBQXdCbUMsSUFBeEIsQ0FBNkJaLFNBQVN1UixLQUF0QztBQUNBUyxjQUFTdlQsSUFBVCxDQUFjLGlCQUFkLEVBQWlDRixJQUFqQyxDQUFzQyxVQUF0QyxFQUFrRHlCLFNBQVMwSCxHQUEzRDs7QUFFQSxTQUFJek0sRUFBRVUsTUFBRixFQUFVK0YsS0FBVixLQUFvQixJQUF4QixFQUE4QjtBQUM3QnNRLGVBQVNqUSxHQUFULENBQWE7QUFDWixlQUFRRyxTQUFTTCxTQUFTdVksSUFBVCxHQUFnQlosT0FBTzlYLEtBQVAsRUFBekIsSUFBMkNzUSxTQUFTdFEsS0FBVCxLQUFtQixDQUQxRDtBQUVaLGNBQU9HLFNBQVNDLEdBRko7QUFHWixrQkFBVztBQUhDLE9BQWI7QUFLQTtBQUVEO0FBQ0Q7QUF2QkssR0FBUDtBQXlCQTtBQUNELENBekVEOztBQTJFQTdHLEVBQUUsaUJBQUYsRUFBcUJtSCxFQUFyQixDQUF3QixPQUF4QixFQUFpQyxZQUFXO0FBQzNDL0IsVUFBU3FTLElBQVQsR0FBZ0J6WCxFQUFFLElBQUYsRUFBUUYsSUFBUixDQUFhLEtBQWIsQ0FBaEI7QUFDQSxDQUZEOztBQUlBRSxFQUFFLHVCQUFGLEVBQTJCbUgsRUFBM0IsQ0FBOEIsUUFBOUIsRUFBd0MsWUFBVztBQUNsRCxLQUFJc2IsZUFBZXppQixFQUFFLElBQUYsRUFBUUYsSUFBUixDQUFhLGVBQWIsQ0FBbkI7QUFDQSxLQUFJNGlCLGNBQWMsSUFBSUwsZUFBSixDQUFvQmpkLFNBQVN3SCxNQUE3QixDQUFsQjtBQUNBLEtBQUkwSixRQUFRdFcsRUFBRSxJQUFGLEVBQVErTSxHQUFSLEVBQVo7O0FBRUEsS0FBSXVKLFNBQVNtTSxZQUFiLEVBQTJCO0FBQzFCQyxjQUFZQyxHQUFaLENBQWdCLFVBQWhCLEVBQTRCck0sS0FBNUI7QUFDQSxFQUZELE1BRU87QUFDTm9NLGNBQVl4WCxNQUFaLENBQW1CLFVBQW5CO0FBQ0E7O0FBRUQsS0FBSXlCLFFBQVEsTUFBTStWLFlBQVlFLFFBQVosRUFBbEI7O0FBRUE1aUIsR0FBRSxlQUFGLEVBQW1CRixJQUFuQixDQUF3QixPQUF4QixFQUFpQ3dXLEtBQWpDO0FBQ0FsUixVQUFTcVMsSUFBVCxHQUFnQnpYLEVBQUUsSUFBRixFQUFRRixJQUFSLENBQWEsTUFBYixJQUF3QjZNLEtBQXhDO0FBQ0EsQ0FmRDs7QUFpQkEsU0FBU2tXLFlBQVQsQ0FBc0JqYSxDQUF0QixFQUF5QjtBQUN4QixLQUFJQyxRQUFRN0ksRUFBRSxJQUFGLENBQVo7QUFDQSxLQUFJbVgsUUFBUXRPLE1BQU1zRCxPQUFOLENBQWMsa0JBQWQsQ0FBWjs7QUFFQW9VLGVBQWNwSixLQUFkLEVBQXFCLE9BQXJCO0FBQ0E7O0FBRUQsU0FBUzJMLFdBQVQsQ0FBcUJsYSxDQUFyQixFQUF3QjtBQUN2QixLQUFJQyxRQUFRN0ksRUFBRSxJQUFGLENBQVo7QUFDQSxLQUFJbVgsUUFBUXRPLE1BQU1zRCxPQUFOLENBQWMsa0JBQWQsQ0FBWjs7QUFFQW9VLGVBQWNwSixLQUFkLEVBQXFCLE1BQXJCO0FBQ0E7O0FBRUQsU0FBUzRMLFlBQVQsQ0FBc0JuYSxDQUF0QixFQUF5QjtBQUN4QixLQUFJQyxRQUFRN0ksRUFBRSxJQUFGLENBQVo7QUFDQSxLQUFJbVgsUUFBUXRPLE1BQU1zRCxPQUFOLENBQWMsa0JBQWQsQ0FBWjs7QUFFQW9VLGVBQWNwSixLQUFkOztBQUVBdE8sT0FBTXlXLFFBQU4sQ0FBZSxVQUFVbFksS0FBVixFQUFpQjtBQUMvQixNQUFJMkMsWUFBSjtBQUNBLE1BQUl3VixnQkFBSjs7QUFFQSxNQUFJblksTUFBTW9ZLE9BQVYsRUFBbUI7QUFDbEJ6VixTQUFNM0MsTUFBTW9ZLE9BQVo7QUFDQSxHQUZELE1BRU8sSUFBSXBZLE1BQU1xWSxLQUFWLEVBQWlCO0FBQ3ZCMVYsU0FBTTNDLE1BQU1xWSxLQUFaO0FBQ0E7O0FBRUQsTUFBSTFWLFFBQVEsSUFBUixJQUFnQkEsUUFBUSxDQUF4QixJQUE2QkEsUUFBUSxDQUFyQyxJQUEwQ0EsUUFBUSxFQUFsRCxJQUF3REEsUUFBUSxDQUFoRSxJQUFxRUEsUUFBUSxFQUE3RSxJQUFtRkEsUUFBUSxFQUEzRixJQUFpR0EsUUFBUSxFQUE3RyxFQUFpSDtBQUNoSCxVQUFPLElBQVA7QUFDQTtBQUNEd1YsWUFBVXRMLE9BQU95TCxZQUFQLENBQW9CM1YsR0FBcEIsQ0FBVjs7QUFFQSxNQUFJLENBQUMsV0FBVzVGLElBQVgsQ0FBZ0JvYixPQUFoQixDQUFMLEVBQStCO0FBQzlCLFVBQU8sS0FBUDtBQUNBO0FBQ0QsRUFsQkQ7QUFtQkE7O0FBRUQsU0FBU3lELFlBQVQsQ0FBc0JwYSxDQUF0QixFQUF5QjtBQUN4QixLQUFJQyxRQUFRN0ksRUFBRSxJQUFGLENBQVo7QUFDQSxLQUFJbVgsUUFBUXRPLE1BQU1zRCxPQUFOLENBQWMsa0JBQWQsQ0FBWjtBQUNBLEtBQUl3VSxZQUFZeEosTUFBTW5RLFFBQU4sQ0FBZSxXQUFmLENBQWhCO0FBQ0EsS0FBSWljLE9BQU9wYSxNQUFNc0QsT0FBTixDQUFjLGlCQUFkLENBQVg7QUFDQSxLQUFJb1MsU0FBU3BILE1BQU0zVCxJQUFOLENBQVcsbUJBQVgsQ0FBYjs7QUFFQSxLQUFJcUYsTUFBTTdCLFFBQU4sQ0FBZSxLQUFmLENBQUosRUFBMkI7QUFDMUIsTUFBSXVYLFVBQVMwRSxLQUFLemYsSUFBTCxDQUFVLG1CQUFWLENBQWI7O0FBRUErYSxVQUFPamUsSUFBUCxDQUFZLFVBQVUyTixJQUFWLEVBQWdCO0FBQzNCak8sS0FBRSxJQUFGLEVBQVErTSxHQUFSLENBQVk0VCxZQUFZLENBQVosR0FBZ0IsQ0FBNUI7QUFDQSxHQUZEO0FBR0EsRUFORCxNQU1PO0FBQ05wQyxTQUFPeFIsR0FBUCxDQUFXNFQsWUFBWSxDQUFaLEdBQWdCLENBQTNCO0FBQ0E7QUFDRDs7QUFFRDtBQUNBO0FBQ0E7O0FBRUEsU0FBU3VDLGFBQVQsR0FBeUI7QUFDeEJsakIsR0FBRSxNQUFGLEVBQVVtSCxFQUFWLENBQWEsT0FBYixFQUFzQixrQkFBdEIsRUFBMEMsWUFBWTtBQUNyRCxNQUFJMEIsUUFBUTdJLEVBQUUsSUFBRixDQUFaO0FBQ0EsTUFBSW1YLFFBQVF0TyxNQUFNc0QsT0FBTixDQUFjLGVBQWQsQ0FBWjtBQUNBLE1BQUlnWCxLQUFLaE0sTUFBTXJYLElBQU4sQ0FBVyxJQUFYLENBQVQ7QUFDQSxNQUFJMk0sTUFBTTVELE1BQU0vSSxJQUFOLENBQVcsS0FBWCxDQUFWOztBQUVBRSxJQUFFd00sSUFBRixDQUFPO0FBQ05DLFFBQUtBLEdBREM7QUFFTmhKLFNBQU0sTUFGQTtBQUdOM0QsU0FBTTtBQUNMcWpCLFFBQUlBO0FBREMsSUFIQTtBQU1OakUsYUFBVSxNQU5KO0FBT056SyxZQUFTLGlCQUFVMVAsUUFBVixFQUFvQjtBQUM1QixRQUFJQSxTQUFTMFAsT0FBYixFQUFzQjtBQUNyQnpVLE9BQUUsa0JBQUYsRUFBc0JNLElBQXRCLENBQTJCLFlBQVk7QUFDdEMsVUFBSWtoQixNQUFNeGhCLEVBQUUsSUFBRixDQUFWO0FBQ0EsVUFBSW9qQixNQUFNNUIsSUFBSXJWLE9BQUosQ0FBWSxlQUFaLENBQVY7QUFDQSxVQUFJaVgsSUFBSXRqQixJQUFKLENBQVMsSUFBVCxNQUFtQnFYLE1BQU1yWCxJQUFOLENBQVcsSUFBWCxDQUF2QixFQUF5QztBQUN4QyxXQUFJaUYsU0FBU3BDLEdBQWIsRUFBa0I7QUFDakI2ZSxZQUFJL2dCLFFBQUosQ0FBYSxRQUFiO0FBQ0EsUUFGRCxNQUVPLElBQUlzRSxTQUFTbEUsTUFBYixFQUFxQjtBQUMzQjJnQixZQUFJMWdCLFdBQUosQ0FBZ0IsUUFBaEI7QUFDQTtBQUNEO0FBQ0QsTUFWRDs7QUFZQWQsT0FBRSxvQkFBRixFQUF3QjJGLElBQXhCLENBQTZCWixTQUFTdVIsS0FBdEM7QUFDQTtBQUNEO0FBdkJLLEdBQVA7QUF5QkEsRUEvQkQ7QUFnQ0E7O0FBRUQsU0FBUytNLFlBQVQsR0FBd0I7QUFDdkJyakIsR0FBRSxNQUFGLEVBQVVtSCxFQUFWLENBQWEsT0FBYixFQUFzQixpQkFBdEIsRUFBeUMsWUFBWTtBQUNwRCxNQUFJMEIsUUFBUTdJLEVBQUUsSUFBRixDQUFaO0FBQ0EsTUFBSW1YLFFBQVF0TyxNQUFNc0QsT0FBTixDQUFjLGVBQWQsQ0FBWjtBQUNBLE1BQUlnWCxLQUFLaE0sTUFBTXJYLElBQU4sQ0FBVyxJQUFYLENBQVQ7QUFDQSxNQUFJMk0sTUFBTTVELE1BQU0vSSxJQUFOLENBQVcsS0FBWCxDQUFWOztBQUVBRSxJQUFFd00sSUFBRixDQUFPO0FBQ05DLFFBQUtBLEdBREM7QUFFTmhKLFNBQU0sTUFGQTtBQUdOM0QsU0FBTTtBQUNMcWpCLFFBQUlBO0FBREMsSUFIQTtBQU1OakUsYUFBVSxNQU5KO0FBT056SyxZQUFTLGlCQUFVMVAsUUFBVixFQUFvQjtBQUM1QixRQUFJQSxTQUFTMFAsT0FBYixFQUFzQjtBQUNyQnpVLE9BQUUsaUJBQUYsRUFBcUJNLElBQXJCLENBQTBCLFlBQVk7QUFDckMsVUFBSWtoQixNQUFNeGhCLEVBQUUsSUFBRixDQUFWO0FBQ0EsVUFBSW9qQixNQUFNNUIsSUFBSXJWLE9BQUosQ0FBWSxlQUFaLENBQVY7QUFDQSxVQUFJaVgsSUFBSXRqQixJQUFKLENBQVMsSUFBVCxNQUFtQnFYLE1BQU1yWCxJQUFOLENBQVcsSUFBWCxDQUF2QixFQUF5QztBQUN4QyxXQUFJaUYsU0FBU3BDLEdBQWIsRUFBa0I7QUFDakI2ZSxZQUFJL2dCLFFBQUosQ0FBYSxRQUFiO0FBQ0EsUUFGRCxNQUVPLElBQUlzRSxTQUFTbEUsTUFBYixFQUFxQjtBQUMzQjJnQixZQUFJMWdCLFdBQUosQ0FBZ0IsUUFBaEI7QUFDQTtBQUNEO0FBQ0QsTUFWRDs7QUFZQWQsT0FBRSxtQkFBRixFQUF1QjJGLElBQXZCLENBQTRCWixTQUFTdVIsS0FBckM7QUFDQTtBQUNEO0FBdkJLLEdBQVA7QUF5QkEsRUEvQkQ7QUFnQ0E7O0FBRUQsU0FBU2dOLE9BQVQsR0FBbUI7QUFDbEJ0akIsR0FBRSxNQUFGLEVBQVVtSCxFQUFWLENBQWEsT0FBYixFQUFzQixpQkFBdEIsRUFBeUMsWUFBWTtBQUNwRCxNQUFJMEIsUUFBUTdJLEVBQUUsSUFBRixDQUFaO0FBQ0EsTUFBSW1YLFFBQVF0TyxNQUFNc0QsT0FBTixDQUFjLFlBQWQsQ0FBWjtBQUNBLE1BQUlnWCxLQUFLaE0sTUFBTXJYLElBQU4sQ0FBVyxJQUFYLENBQVQ7QUFDQSxNQUFJMk0sTUFBTTVELE1BQU0vSSxJQUFOLENBQVcsS0FBWCxDQUFWO0FBQ0EsTUFBSXlqQixXQUFXcE0sTUFBTTNULElBQU4sQ0FBVyxvQkFBWCxDQUFmO0FBQ0EsTUFBSXVULFdBQVdsTyxNQUFNckYsSUFBTixDQUFXLGdCQUFYLENBQWY7QUFDQSxNQUFJZ2dCLGtCQUFrQnJNLE1BQU0zVCxJQUFOLENBQVcsbUJBQVgsQ0FBdEI7O0FBRUF4RCxJQUFFd00sSUFBRixDQUFPO0FBQ05DLFFBQUtBLEdBREM7QUFFTmhKLFNBQU0sTUFGQTtBQUdOM0QsU0FBTTtBQUNMcWpCLFFBQUlBO0FBREMsSUFIQTtBQU1OakUsYUFBVSxNQU5KO0FBT056SyxZQUFTLGlCQUFVMVAsUUFBVixFQUFvQjtBQUM1QixRQUFJQSxTQUFTMFAsT0FBYixFQUFzQjtBQUNyQixTQUFJMVAsU0FBU3BDLEdBQWIsRUFBa0I7QUFDakI0Z0IsZUFBU3ppQixXQUFULENBQXFCLFFBQXJCO0FBQ0ErSCxZQUFNcEksUUFBTixDQUFlLFFBQWY7QUFDQSxNQUhELE1BR08sSUFBSXNFLFNBQVNsRSxNQUFiLEVBQXFCO0FBQzNCZ0ksWUFBTS9ILFdBQU4sQ0FBa0IsUUFBbEI7QUFDQTs7QUFFRGlXLGNBQVNwUixJQUFULENBQWNaLFNBQVMwZSxTQUF2QjtBQUNBRCxxQkFBZ0I3ZCxJQUFoQixDQUFxQlosU0FBUzJlLFlBQTlCO0FBQ0E7QUFDRDtBQW5CSyxHQUFQO0FBcUJBLEVBOUJEO0FBK0JBOztBQUVELFNBQVNDLFVBQVQsR0FBc0I7QUFDckIzakIsR0FBRSxNQUFGLEVBQVVtSCxFQUFWLENBQWEsT0FBYixFQUFzQixvQkFBdEIsRUFBNEMsWUFBWTtBQUN2RCxNQUFJMEIsUUFBUTdJLEVBQUUsSUFBRixDQUFaO0FBQ0EsTUFBSW1YLFFBQVF0TyxNQUFNc0QsT0FBTixDQUFjLFlBQWQsQ0FBWjtBQUNBLE1BQUlnWCxLQUFLaE0sTUFBTXJYLElBQU4sQ0FBVyxJQUFYLENBQVQ7QUFDQSxNQUFJMk0sTUFBTTVELE1BQU0vSSxJQUFOLENBQVcsS0FBWCxDQUFWO0FBQ0EsTUFBSThqQixRQUFRek0sTUFBTTNULElBQU4sQ0FBVyxpQkFBWCxDQUFaO0FBQ0EsTUFBSXVULFdBQVdsTyxNQUFNckYsSUFBTixDQUFXLG1CQUFYLENBQWY7QUFDQSxNQUFJcWdCLGVBQWUxTSxNQUFNM1QsSUFBTixDQUFXLGdCQUFYLENBQW5COztBQUVBeEQsSUFBRXdNLElBQUYsQ0FBTztBQUNOQyxRQUFLQSxHQURDO0FBRU5oSixTQUFNLE1BRkE7QUFHTjNELFNBQU07QUFDTHFqQixRQUFJQTtBQURDLElBSEE7QUFNTmpFLGFBQVUsTUFOSjtBQU9OekssWUFBUyxpQkFBVTFQLFFBQVYsRUFBb0I7QUFDNUIsUUFBSUEsU0FBUzBQLE9BQWIsRUFBc0I7QUFDckIsU0FBSTFQLFNBQVNwQyxHQUFiLEVBQWtCO0FBQ2pCaWhCLFlBQU05aUIsV0FBTixDQUFrQixRQUFsQjtBQUNBK0gsWUFBTXBJLFFBQU4sQ0FBZSxRQUFmO0FBQ0EsTUFIRCxNQUdPLElBQUlzRSxTQUFTbEUsTUFBYixFQUFxQjtBQUMzQmdJLFlBQU0vSCxXQUFOLENBQWtCLFFBQWxCO0FBQ0E7O0FBRURpVyxjQUFTcFIsSUFBVCxDQUFjWixTQUFTMmUsWUFBdkI7QUFDQUcsa0JBQWFsZSxJQUFiLENBQWtCWixTQUFTMGUsU0FBM0I7QUFDQTtBQUNEO0FBbkJLLEdBQVA7QUFxQkEsRUE5QkQ7QUErQkE7O0FBRUQsU0FBU0ssY0FBVCxHQUEwQjtBQUN6QjdELGdCQUFlM2YsSUFBZixDQUFvQixVQUFVNEwsS0FBVixFQUFpQjtBQUNwQyxNQUFJLENBQUNsTSxFQUFFLElBQUYsRUFBUTBPLElBQVIsQ0FBYSxTQUFiLENBQUwsRUFBOEI7QUFDN0IsT0FBSTRSLFdBQVd0Z0IsRUFBRSxJQUFGLEVBQVErSSxNQUFSLEdBQWlCb0UsSUFBakIsQ0FBc0IsOEJBQXRCLENBQWY7O0FBRUFtVCxZQUFTRCxPQUFULENBQWlCLE1BQWpCO0FBQ0E7QUFDRCxFQU5EOztBQVFBSixnQkFBZTlZLEVBQWYsQ0FBa0IsUUFBbEIsRUFBNEIsWUFBWTtBQUN2Q2daLFNBQU8sSUFBUDtBQUNBLEVBRkQ7QUFHQTs7QUFFRCxTQUFTNEQsUUFBVCxHQUFvQjtBQUNuQixLQUFJQyxnQkFBZ0Joa0IsRUFBRSxlQUFGLENBQXBCO0FBQ0EsS0FBSWlELFlBQVksd0JBQWMrZ0IsYUFBZCxDQUFoQjs7QUFFQWhrQixHQUFFLGVBQUYsRUFBbUI4YixLQUFuQixDQUF5QixZQUFZOztBQUVwQyxNQUFJalQsUUFBUTdJLEVBQUUsSUFBRixDQUFaO0FBQ0EsTUFBSXlNLE1BQU01RCxNQUFNL0ksSUFBTixDQUFXLEtBQVgsQ0FBVjtBQUNBLE1BQUlta0IsT0FBT3BiLE1BQU0vSSxJQUFOLENBQVcsTUFBWCxDQUFYO0FBQ0EsTUFBSW9rQixRQUFRcmIsTUFBTS9JLElBQU4sQ0FBVyxPQUFYLENBQVo7QUFDQSxNQUFJNE0sU0FBUzdELE1BQU0vSSxJQUFOLENBQVcsUUFBWCxDQUFiOztBQUVBLE1BQUlxa0IsWUFBWW5rQixFQUFFLDBCQUFGLEVBQThCMEYsS0FBOUIsR0FBc0NwQyxJQUF0QyxDQUEyQyxXQUEzQyxDQUFoQjtBQUNBLE1BQUk4Z0IsYUFBYXBrQixFQUFFLDBCQUFGLEVBQThCcWtCLElBQTlCLEdBQXFDL2dCLElBQXJDLENBQTBDLFdBQTFDLENBQWpCOztBQUVBdEQsSUFBRTJLLE1BQUYsQ0FBUyxtQkFBVCxFQUE4QixFQUFFeVosVUFBaEM7O0FBRUEsTUFBSSxDQUFDcGtCLEVBQUUySyxNQUFGLENBQVMsa0JBQVQsQ0FBRCxJQUFpQzNLLEVBQUUySyxNQUFGLENBQVMsa0JBQVQsTUFBaUMsTUFBdEUsRUFBOEU7QUFDN0UzSyxLQUFFMkssTUFBRixDQUFTLGtCQUFULEVBQTZCd1osU0FBN0I7QUFDQTs7QUFHRHRiLFFBQU02RixJQUFOLENBQVcsVUFBWCxFQUF1QixJQUF2Qjs7QUFFQTFPLElBQUV3TSxJQUFGLENBQU87QUFDTi9JLFNBQU0sTUFEQTtBQUVOZ0osUUFBS0EsR0FGQztBQUdOM00sU0FBTTtBQUNMbWtCLFVBQU1BLElBREQ7QUFFTEMsZ0JBRks7QUFHTHhYLFlBQVFBO0FBSEgsSUFIQTtBQVFOd1MsYUFBVSxNQVJKO0FBU056SyxZQUFTLGlCQUFVMVAsUUFBVixFQUFvQjtBQUM1QixRQUFJQSxTQUFTMFAsT0FBYixFQUFzQjtBQUNyQjVMLFdBQU0vSSxJQUFOLENBQVcsTUFBWCxFQUFtQmlGLFNBQVNrZixJQUE1Qjs7QUFFQUQsbUJBQWMzZ0IsTUFBZCxDQUFxQjBCLFNBQVM2UCxJQUE5Qjs7QUFFQSxTQUFJN1AsU0FBU3VmLFVBQWIsRUFBeUI7QUFDeEJ0a0IsUUFBRSwwQkFBRixFQUE4QjRVLElBQTlCLENBQW1DN1AsU0FBU3VmLFVBQTVDO0FBQ0E7O0FBRUQsU0FBSXZmLFNBQVNzZixJQUFiLEVBQW1CO0FBQ2xCeGIsWUFBTTVELElBQU47QUFDQTtBQUVEOztBQUVEdEUsZUFBVyxZQUFZO0FBQ3RCa0ksV0FBTTZGLElBQU4sQ0FBVyxVQUFYLEVBQXVCLEtBQXZCO0FBQ0EsS0FGRCxFQUVHLEdBRkg7O0FBS0EsMkJBQWFKLElBQWIsQ0FBa0J0TyxFQUFFMEssUUFBRixDQUFsQjs7QUFFQTtBQUVBO0FBbENLLEdBQVA7QUFvQ0EsRUF4REQ7QUF5REE7O0FBRUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLFNBQVM2WixjQUFULEdBQTBCO0FBQ3pCLEtBQUk3a0IsYUFBYU0sRUFBRSxNQUFGLENBQWpCO0FBQ0FOLFlBQVd5SCxFQUFYLENBQWMsT0FBZCxFQUF1QixtQkFBdkIsRUFBNEMwYixZQUE1QztBQUNBbmpCLFlBQVd5SCxFQUFYLENBQWMsT0FBZCxFQUF1QixrQkFBdkIsRUFBMkMyYixXQUEzQztBQUNBcGpCLFlBQVd5SCxFQUFYLENBQWMsT0FBZCxFQUF1QixtQkFBdkIsRUFBNEM0YixZQUE1QztBQUNBcmpCLFlBQVd5SCxFQUFYLENBQWMsT0FBZCxFQUF1QixtQkFBdkIsRUFBNEM2YixZQUE1QztBQUNBOztBQUdELFNBQVN4RyxTQUFULEdBQXFCO0FBQ3BCLEtBQU1nSSxVQUFVeGtCLEVBQUUsU0FBRixDQUFoQjtBQUNBLEtBQU15a0IsbUJBQW1CRCxRQUFRNUgsTUFBUixHQUFpQi9WLEdBQWpCLEdBQXVCMmQsUUFBUXZWLE1BQVIsRUFBaEQ7QUFDQSxLQUFNeVYsV0FBVzFrQixFQUFFLFdBQUYsQ0FBakI7QUFDQSxLQUFNMmtCLGNBQWMsUUFBcEI7O0FBRUEza0IsR0FBRVUsTUFBRixFQUFVeUcsRUFBVixDQUFhLFFBQWIsRUFBdUIsWUFBTztBQUM3QixNQUFJeWQsYUFBYTVrQixFQUFFMEssUUFBRixFQUFZOFIsU0FBWixFQUFqQjs7QUFFQSxNQUFJb0ksYUFBYTVrQixFQUFFVSxNQUFGLEVBQVV1TyxNQUFWLEtBQXFCLENBQXRDLEVBQXlDO0FBQ3hDeVYsWUFBU2prQixRQUFULENBQWtCa2tCLFdBQWxCO0FBQ0EsR0FGRCxNQUVPO0FBQ05ELFlBQVM1akIsV0FBVCxDQUFxQjZqQixXQUFyQjtBQUNBO0FBQ0QsTUFBS0MsYUFBYUgsZ0JBQWxCLEVBQXFDO0FBQ3BDQyxZQUFTNWpCLFdBQVQsQ0FBcUI2akIsV0FBckI7QUFDQTtBQUVELEVBWkQ7O0FBY0FELFVBQVN2ZCxFQUFULENBQVksT0FBWixFQUFxQixZQUFNO0FBQzFCbkgsSUFBRSxZQUFGLEVBQWdCNmtCLE9BQWhCLENBQXdCLEVBQUVySSxXQUFXLENBQWIsRUFBeEIsRUFBMEMsR0FBMUMsRUFBK0MsUUFBL0M7QUFDQSxFQUZEO0FBR0E7O0FBRUQsU0FBU3NJLGVBQVQsR0FBMkI7QUFDMUIsS0FBSTFYLFFBQVFwTixFQUFFLHNCQUFGLENBQVo7QUFDQSxLQUFJMGtCLFdBQVcxa0IsRUFBRSxxQkFBRixDQUFmOztBQUVBMGtCLFVBQVN2ZCxFQUFULENBQVksT0FBWixFQUFxQixZQUFXO0FBQy9CaUcsUUFBTTNNLFFBQU4sQ0FBZSxRQUFmO0FBQ0EsRUFGRDs7QUFJQVQsR0FBRTBLLFFBQUYsRUFBWXFhLE9BQVosQ0FBb0IsVUFBVW5jLENBQVYsRUFBWTtBQUMvQixNQUFJLENBQUN3RSxNQUFNNFgsRUFBTixDQUFTcGMsRUFBRXFjLE1BQVgsQ0FBRCxJQUNBN1gsTUFBTWtWLEdBQU4sQ0FBVTFaLEVBQUVxYyxNQUFaLEVBQW9CcmxCLE1BQXBCLEtBQStCLENBRG5DLEVBQ3NDO0FBQ3JDd04sU0FBTXRNLFdBQU4sQ0FBa0IsUUFBbEIsRUFBNEI7QUFDNUI7QUFDRCxFQUxEO0FBTUE7O0FBRUQsU0FBU29rQixrQkFBVCxHQUE4QjtBQUM3QixLQUFJUixXQUFXMWtCLEVBQUUsc0JBQUYsQ0FBZjtBQUNBLEtBQUlvTixRQUFRcE4sRUFBRSxtQkFBRixDQUFaO0FBQ0EsS0FBSW1sQixRQUFRbmxCLEVBQUUsbUJBQUYsQ0FBWjs7QUFFQTBrQixVQUFTdmQsRUFBVCxDQUFZLE9BQVosRUFBcUIsWUFBWTtBQUNoQ25ILElBQUUsSUFBRixFQUFRZ2dCLFdBQVIsQ0FBb0IsUUFBcEI7QUFDQWhnQixJQUFFLElBQUYsRUFBUW1NLE9BQVIsQ0FBZ0JpQixLQUFoQixFQUF1QjVKLElBQXZCLENBQTRCMmhCLEtBQTVCLEVBQW1DcEYsV0FBbkMsQ0FBK0MsR0FBL0M7QUFDQSxFQUhEO0FBSUE7QUFDRDtBQUNBO0FBQ0E7O1FBRVFtRCxhLEdBQUFBLGE7UUFBZUcsWSxHQUFBQSxZO1FBQWNTLGMsR0FBQUEsYztRQUFnQkMsUSxHQUFBQSxRO1FBQVVRLGMsR0FBQUEsYztRQUFnQlosVSxHQUFBQSxVO1FBQVlMLE8sR0FBQUEsTztRQUFTOUcsUyxHQUFBQSxTO1FBQVdzSSxlLEdBQUFBLGU7UUFBaUJJLGtCLEdBQUFBLGtCOzs7Ozs7O0FDN3hCaEk7O0FBRUE7QUFDQTtBQUNBOzs7OztBQUVBLElBQUlFLE9BQU9wbEIsRUFBRSxNQUFGLENBQVg7QUFDQSxJQUFJcWxCLGVBQWVybEIsRUFBRSw2QkFBRixDQUFuQjtBQUNBLElBQUlzbEIsT0FBTyw4Q0FBWDtBQUNBLElBQUlDLFdBQVcsQ0FDZDtBQUNDLGdCQUFlLE9BRGhCO0FBRUMsZ0JBQWUsZUFGaEI7QUFHQyxZQUFXLENBQ1Y7QUFDQyxXQUFTO0FBRFYsRUFEVTtBQUhaLENBRGMsRUFVZDtBQUNDLGdCQUFlLFNBRGhCO0FBRUMsWUFBVyxDQUNWO0FBQ0MsV0FBUztBQURWLEVBRFUsRUFJVjtBQUNDLGdCQUFjO0FBRGYsRUFKVTtBQUZaLENBVmMsRUFxQmQ7QUFDQyxnQkFBZSxjQURoQjtBQUVDLGdCQUFlLGlCQUZoQjtBQUdDLFlBQVcsQ0FDVjtBQUNDLGdCQUFjO0FBRGYsRUFEVSxFQUlWO0FBQ0MsV0FBUztBQURWLEVBSlU7QUFIWixDQXJCYyxFQWlDZDtBQUNDLGdCQUFlLGNBRGhCO0FBRUMsZ0JBQWUsZUFGaEI7QUFHQyxZQUFXLENBQ1Y7QUFDQyxXQUFTO0FBRFYsRUFEVTtBQUhaLENBakNjLEVBMENkO0FBQ0MsZ0JBQWUsWUFEaEI7QUFFQyxnQkFBZSxlQUZoQjtBQUdDLFlBQVcsQ0FDVjtBQUNDLGdCQUFjO0FBRGYsRUFEVSxFQUlWO0FBQ0MsV0FBUztBQURWLEVBSlUsRUFPVjtBQUNDLFlBQVU7QUFEWCxFQVBVO0FBSFosQ0ExQ2MsRUF5RGQ7QUFDQyxnQkFBZSxZQURoQjtBQUVDLGdCQUFlLGlCQUZoQjtBQUdDLFlBQVcsQ0FDVjtBQUNDLFdBQVM7QUFEVixFQURVO0FBSFosQ0F6RGMsRUFrRWQ7QUFDQyxnQkFBZSxLQURoQjtBQUVDLGdCQUFlLGVBRmhCO0FBR0MsWUFBVyxDQUNWO0FBQ0MsZ0JBQWM7QUFEZixFQURVLEVBSVY7QUFDQyxXQUFTO0FBRFYsRUFKVTtBQUhaLENBbEVjLEVBOEVkO0FBQ0MsZ0JBQWUsZ0JBRGhCO0FBRUMsZ0JBQWUsVUFGaEI7QUFHQyxZQUFXLENBQ1Y7QUFDQyxXQUFTO0FBRFYsRUFEVTtBQUhaLENBOUVjLEVBdUZkO0FBQ0MsZ0JBQWUsZUFEaEI7QUFFQyxnQkFBZSxlQUZoQjtBQUdDLFlBQVcsQ0FDVjtBQUNDLFdBQVM7QUFEVixFQURVO0FBSFosQ0F2RmMsRUFnR2Q7QUFDQyxnQkFBZSxlQURoQjtBQUVDLGdCQUFlLGVBRmhCO0FBR0MsWUFBVyxDQUNWO0FBQ0MsV0FBUztBQURWLEVBRFU7QUFIWixDQWhHYyxFQXlHZDtBQUNDLGdCQUFlLFdBRGhCO0FBRUMsZ0JBQWUsZUFGaEI7QUFHQyxZQUFXLENBQ1Y7QUFDQyxnQkFBYztBQURmLEVBRFUsRUFJVjtBQUNDLFdBQVM7QUFEVixFQUpVO0FBSFosQ0F6R2MsRUFxSGQ7QUFDQyxnQkFBZSxNQURoQjtBQUVDLGdCQUFlLGtCQUZoQjtBQUdDLFlBQVcsQ0FDVjtBQUNDLFdBQVM7QUFEVixFQURVO0FBSFosQ0FySGMsRUE4SGQ7QUFDQyxnQkFBZSxnQkFEaEI7QUFFQyxnQkFBZSxrQkFGaEI7QUFHQyxZQUFXLENBQ1Y7QUFDQyxnQkFBYztBQURmLEVBRFUsRUFJVjtBQUNDLFdBQVM7QUFEVixFQUpVO0FBSFosQ0E5SGMsRUEwSWQ7QUFDQyxnQkFBZSxLQURoQjtBQUVDLGdCQUFlLGFBRmhCO0FBR0MsWUFBVyxDQUNWO0FBQ0MsZ0JBQWM7QUFEZixFQURVO0FBSFosQ0ExSWMsRUFtSmQ7QUFDQyxnQkFBZSxLQURoQjtBQUVDLGdCQUFlLFFBRmhCO0FBR0MsWUFBVyxDQUNWO0FBQ0MsZ0JBQWM7QUFEZixFQURVO0FBSFosQ0FuSmMsRUE0SmQ7QUFDQyxnQkFBZSxlQURoQjtBQUVDLGdCQUFlLGlCQUZoQjtBQUdDLFlBQVcsQ0FDVjtBQUNDLFdBQVM7QUFEVixFQURVO0FBSFosQ0E1SmMsRUFxS2Q7QUFDQyxnQkFBZSxNQURoQjtBQUVDLGdCQUFlLGFBRmhCO0FBR0MsWUFBVyxDQUNWO0FBQ0MsZ0JBQWM7QUFEZixFQURVO0FBSFosQ0FyS2MsRUE4S2QsRUE5S2MsRUErS2Q7QUFDQyxnQkFBZSxLQURoQjtBQUVDLGdCQUFlLGVBRmhCO0FBR0MsWUFBVyxDQUNWO0FBQ0MsV0FBUztBQURWLEVBRFU7QUFIWixDQS9LYyxDQUFmOztBQTBMQSxTQUFTQyxRQUFULENBQW1CQyxRQUFuQixFQUE2QkMsU0FBN0IsRUFBd0M7QUFDdkMsS0FBSXZsQixRQUFRLElBQUl3bEIsT0FBT0MsSUFBUCxDQUFZQyxNQUFoQixDQUF1QkosUUFBdkIsRUFBaUNDLFNBQWpDLENBQVosQ0FEdUMsQ0FDa0I7O0FBRXpELFFBQU92bEIsS0FBUDtBQUNBOztBQUVEO0FBQ0E7QUFDQTs7QUFFQSxTQUFTMmxCLE9BQVQsR0FBb0I7QUFDbkIsS0FBSSxDQUFDVixLQUFLeGxCLE1BQVYsRUFBa0I7QUFDakIsU0FBTyxLQUFQO0FBQ0E7O0FBRUQsS0FBSXdGLFdBQVc7QUFDZHFnQixZQUFVLEVBREk7QUFFZEMsYUFBVztBQUZHLEVBQWY7O0FBS0FMLGNBQWFsZSxFQUFiLENBQWdCLE9BQWhCLEVBQXlCLFlBQVk7QUFDcEMsTUFBSTRlLFdBQVcvbEIsRUFBRSxJQUFGLEVBQVFtTSxPQUFSLENBQWdCLDBCQUFoQixFQUE0Q2dCLElBQTVDLENBQWlELHdCQUFqRCxDQUFmO0FBQ0EsTUFBSTZZLFVBQVVobUIsRUFBRSxJQUFGLEVBQVFtTSxPQUFSLENBQWdCLDBCQUFoQixDQUFkO0FBQ0EsTUFBSThaLE9BQU9GLFNBQVN2aUIsSUFBVCxDQUFjLE1BQWQsQ0FBWDtBQUNBLE1BQUkwSSxRQUFROFosUUFBUWxtQixJQUFSLENBQWEsT0FBYixDQUFaOztBQUVBaW1CLFdBQVNoRyxXQUFULENBQXFCLE1BQXJCO0FBQ0FpRyxVQUFRaEcsV0FBUixDQUFvQixNQUFwQjs7QUFFQTVhLGFBQVc7QUFDVnFnQixhQUFVUSxLQUFLbm1CLElBQUwsQ0FBVSxLQUFWLENBREE7QUFFVjRsQixjQUFXTyxLQUFLbm1CLElBQUwsQ0FBVSxLQUFWO0FBRkQsR0FBWDs7QUFLQSxNQUFJb21CLE1BQU0sSUFBSVAsT0FBT0MsSUFBUCxDQUFZTyxHQUFoQixDQUFvQnpiLFNBQVMwYixzQkFBVCxDQUFnQyxLQUFoQyxFQUF1Q2xhLEtBQXZDLENBQXBCLEVBQW1FLEVBQUU7QUFDOUVtYSxTQUFNLEVBRHNFO0FBRTVFQyxXQUFRZCxTQUFTcGdCLFNBQVNxZ0IsUUFBbEIsRUFBNEJyZ0IsU0FBU3NnQixTQUFyQyxDQUZvRTtBQUc1RWEsZ0JBQWEsS0FIK0Q7QUFJNUVDLFdBQVFqQjtBQUpvRSxHQUFuRSxDQUFWOztBQU9BLE1BQUlrQixTQUFTLElBQUlkLE9BQU9DLElBQVAsQ0FBWWMsTUFBaEIsQ0FBdUIsRUFBRTtBQUNyQzlmLGFBQVU0ZSxTQUFTcGdCLFNBQVNxZ0IsUUFBbEIsRUFBNEJyZ0IsU0FBU3NnQixTQUFyQyxDQUR5QjtBQUVuQ1EsUUFBS0EsR0FGOEI7QUFHbkNTLGNBQVcsS0FId0I7QUFJbkNyQixTQUFNQSxJQUo2QjtBQUtuQ25ULFVBQU87QUFMNEIsR0FBdkIsQ0FBYjtBQU9BLEVBNUJEO0FBNkJBOztBQUVEO0FBQ0E7QUFDQTs7UUFFUTJULE8sR0FBQUEsTzs7Ozs7OztBQzFQUjs7QUFFQTtBQUNBO0FBQ0E7Ozs7Ozs7QUFFQTs7OztBQUNBOzs7Ozs7QUFFQTtBQUNBO0FBQ0E7O0FBRUEsSUFBSWMsY0FBYyxlQUFsQjtBQUNBLElBQUlDLGVBQWUsZ0JBQW5CO0FBQ0EsSUFBSUMsV0FBVyxZQUFmO0FBQ0EsSUFBSUMsYUFBYS9tQixFQUFFLGFBQUYsQ0FBakI7QUFDQSxJQUFJa1AsZ0JBQWdCbFAsRUFBRWtQLGFBQUYsQ0FBZ0JxRixRQUFwQztBQUNBLElBQUk5SCxNQUFNLGdDQUFWOztBQUVBLFNBQVMrSCxXQUFULENBQXNCM0MsT0FBdEIsRUFBK0I7QUFDOUIsS0FBSTNDLGNBQWN3RixNQUFsQixFQUEwQjtBQUN6QjFVLElBQUVrUCxhQUFGLENBQWdCZ0csS0FBaEI7QUFDQTs7QUFFRGxWLEdBQUVrUCxhQUFGLENBQWdCeEssSUFBaEIsQ0FBcUI7QUFDcEJtUSxTQUFPO0FBQ05DLFFBQUtySTtBQURDLEdBRGE7QUFJcEJoSixRQUFNLE1BSmM7QUFLcEJvSyxnQkFBYyxHQUxNO0FBTXBCMU8sYUFBVyxTQU5TO0FBT3BCeU8sOEVBUG9CO0FBUXBCRyxhQUFXO0FBQ1ZLLHFCQUFrQiw0QkFBWTtBQUM3QnBPLE1BQUUsb0JBQUYsRUFBd0I0VSxJQUF4QixDQUE2Qi9DLE9BQTdCO0FBQ0E7QUFIUztBQVJTLEVBQXJCO0FBY0E7O0FBRUQsU0FBU3NPLE1BQVQsQ0FBaUJ2RyxLQUFqQixFQUF3QjdZLEtBQXhCLEVBQStCaWMsS0FBL0IsRUFBc0M7QUFDckNoZCxHQUFFNFosS0FBRixFQUFTaUwsT0FBVCxDQUFpQjtBQUNoQm1DLFNBQU9qbUI7QUFEUyxFQUFqQixFQUVHaWMsS0FGSDtBQUdBOztBQUVELFNBQVNpSyxRQUFULEdBQXFCO0FBQ3BCOUcsUUFBTzJHLFFBQVAsRUFBaUIsS0FBakIsRUFBd0IsR0FBeEI7QUFDQW5tQixZQUFXLFlBQVk7QUFDdEJYLElBQUU4bUIsUUFBRixFQUFZcm1CLFFBQVosQ0FBcUIsTUFBckI7QUFDQSxFQUZELEVBRUcsR0FGSDtBQUdBRSxZQUFXLFlBQVk7QUFDdEJYLElBQUUsTUFBRixFQUFVUyxRQUFWLENBQW1CLFVBQW5CO0FBQ0EsRUFGRCxFQUVHLEdBRkg7QUFHQSx3QkFBYTZOLElBQWIsQ0FBa0J0TyxFQUFFOG1CLFFBQUYsQ0FBbEI7QUFDQTs7QUFFRCxTQUFTSSxTQUFULEdBQXNCO0FBQ3JCbG5CLEdBQUU4bUIsUUFBRixFQUFZaG1CLFdBQVosQ0FBd0IsTUFBeEI7QUFDQUgsWUFBVyxZQUFZO0FBQ3RCd2YsU0FBTzJHLFFBQVAsRUFBaUIsT0FBakIsRUFBMEIsR0FBMUI7QUFDQSxFQUZELEVBRUcsR0FGSDtBQUdBOW1CLEdBQUUsTUFBRixFQUFVYyxXQUFWLENBQXNCLFVBQXRCO0FBQ0E7O0FBRUQsU0FBU3FtQixVQUFULENBQXFCdmUsQ0FBckIsRUFBd0I7QUFDdkIsS0FBSXVPLFFBQVFuWCxFQUFFLElBQUYsRUFBUW1NLE9BQVIsQ0FBZ0IsZUFBaEIsQ0FBWjtBQUNBLEtBQUl0TCxTQUFTYixFQUFFLElBQUYsRUFBUUYsSUFBUixDQUFhLEtBQWIsQ0FBYjtBQUNBLEtBQUlzbkIsV0FBV2pRLE1BQU0zVCxJQUFOLENBQVcsb0JBQVgsQ0FBZjtBQUNBLEtBQUk2akIsY0FBY0QsU0FBUzVqQixJQUFULENBQWMsYUFBZCxDQUFsQjtBQUNBLEtBQUk4akIsZ0JBQWdCRixTQUFTNWpCLElBQVQsQ0FBYyxlQUFkLENBQXBCOztBQUVBLEtBQUk0akIsU0FBU3BnQixRQUFULENBQWtCLFNBQWxCLENBQUosRUFBa0M7QUFDakNvZ0IsV0FBU2hLLE9BQVQsQ0FBaUIsTUFBakIsRUFBeUJ0YyxXQUF6QixDQUFxQyxTQUFyQztBQUNBLFNBQU8sS0FBUDtBQUNBLEVBSEQsTUFHTztBQUNOc21CLFdBQVNsSyxNQUFULENBQWdCLE1BQWhCLEVBQXdCemMsUUFBeEIsQ0FBaUMsU0FBakM7QUFDQTs7QUFFRDRtQixhQUFZbGdCLEVBQVosQ0FBZSxPQUFmLEVBQXdCLFlBQVk7QUFDbkMsTUFBSXRHLE1BQUosRUFBWTtBQUNYMG1CLGNBQVdwUSxLQUFYLEVBQWtCLENBQWxCLEVBQXFCdFcsTUFBckI7QUFDQSxHQUZELE1BRU87QUFDTjBtQixjQUFXcFEsS0FBWCxFQUFrQixDQUFsQjtBQUNBO0FBQ0QsRUFORDs7QUFRQW1RLGVBQWNuZ0IsRUFBZCxDQUFpQixPQUFqQixFQUEwQixZQUFZO0FBQ3JDaWdCLFdBQVNoSyxPQUFULENBQWlCLE1BQWpCLEVBQXlCdGMsV0FBekIsQ0FBcUMsU0FBckM7QUFDQSxFQUZEOztBQUlBZCxHQUFFMEssUUFBRixFQUFZb1IsS0FBWixDQUFrQixVQUFVbFQsQ0FBVixFQUFhO0FBQzlCLE1BQUl3ZSxTQUFTcGdCLFFBQVQsQ0FBa0IsU0FBbEIsQ0FBSixFQUFrQztBQUNqQyxPQUFJLENBQUNtUSxNQUFNNk4sRUFBTixDQUFTcGMsRUFBRXFjLE1BQVgsQ0FBRCxJQUF1QjlOLE1BQU1tTCxHQUFOLENBQVUxWixFQUFFcWMsTUFBWixFQUFvQnJsQixNQUFwQixLQUErQixDQUExRCxFQUE2RDtBQUM1RHduQixhQUFTaEssT0FBVCxDQUFpQixNQUFqQixFQUF5QnRjLFdBQXpCLENBQXFDLFNBQXJDO0FBQ0E7QUFDRDtBQUNELEVBTkQ7QUFPQTs7QUFFRCxTQUFTMG1CLFNBQVQsQ0FBb0I1ZSxDQUFwQixFQUF1QjtBQUN0QixLQUFJQyxRQUFRN0ksRUFBRSxJQUFGLENBQVo7QUFDQSxLQUFJbVgsUUFBUXRPLE1BQU1zRCxPQUFOLENBQWMsZUFBZCxDQUFaO0FBQ0EsS0FBSW9TLFNBQVNwSCxNQUFNM1QsSUFBTixDQUFXLGdCQUFYLENBQWI7O0FBRUEsS0FBSW1DLE9BQU80WSxPQUFPeFIsR0FBUCxFQUFYO0FBQ0EsS0FBSTRRLE1BQU1ZLE9BQU96ZSxJQUFQLENBQVksS0FBWixLQUFzQixPQUFoQztBQUNBLEtBQUl3VyxRQUFRLENBQVo7QUFDQSxLQUFJdFcsRUFBRThnQixTQUFGLENBQVluYixJQUFaLENBQUosRUFBdUI7QUFDdEIyUSxVQUFRclAsU0FBU3RCLElBQVQsQ0FBUjtBQUNBMlE7QUFDQUEsVUFBU0EsUUFBUSxDQUFULEdBQWdCQSxRQUFRclAsU0FBUzBXLEdBQVQsQ0FBVCxHQUEwQkEsR0FBMUIsR0FBZ0NySCxLQUEvQyxHQUF3RCxDQUFoRTtBQUNBO0FBQ0RpSSxRQUFPeFIsR0FBUCxDQUFXdUosS0FBWDs7QUFFQWlSLFlBQVdwUSxLQUFYLEVBQWtCYixLQUFsQjtBQUNBOztBQUVELFNBQVNtUixRQUFULENBQW1CN2UsQ0FBbkIsRUFBc0I7QUFDckIsS0FBSUMsUUFBUTdJLEVBQUUsSUFBRixDQUFaO0FBQ0EsS0FBSW1YLFFBQVF0TyxNQUFNc0QsT0FBTixDQUFjLGVBQWQsQ0FBWjtBQUNBLEtBQUlvUyxTQUFTcEgsTUFBTTNULElBQU4sQ0FBVyxnQkFBWCxDQUFiOztBQUVBLEtBQUltQyxPQUFPNFksT0FBT3hSLEdBQVAsRUFBWDtBQUNBLEtBQUk0USxNQUFNWSxPQUFPemUsSUFBUCxDQUFZLEtBQVosS0FBc0IsT0FBaEM7QUFDQSxLQUFJd1csUUFBUSxDQUFaO0FBQ0EsS0FBSXRXLEVBQUU4Z0IsU0FBRixDQUFZbmIsSUFBWixDQUFKLEVBQXVCO0FBQ3RCMlEsVUFBUXJQLFNBQVN0QixJQUFULENBQVI7QUFDQTJRO0FBQ0FBLFVBQVNBLFFBQVEsQ0FBVCxHQUFnQkEsUUFBUXJQLFNBQVMwVyxHQUFULENBQVQsR0FBMEJBLEdBQTFCLEdBQWdDckgsS0FBL0MsR0FBd0QsQ0FBaEU7QUFDQTtBQUNEaUksUUFBT3hSLEdBQVAsQ0FBV3VKLEtBQVg7O0FBRUFpUixZQUFXcFEsS0FBWCxFQUFrQmIsS0FBbEI7QUFDQTs7QUFFRCxTQUFTb1IsU0FBVCxDQUFvQjllLENBQXBCLEVBQXVCO0FBQ3RCLEtBQUlDLFFBQVE3SSxFQUFFLElBQUYsQ0FBWjtBQUNBLEtBQUltWCxRQUFRdE8sTUFBTXNELE9BQU4sQ0FBYyxlQUFkLENBQVo7QUFDQSxLQUFJb1MsU0FBU3BILE1BQU0zVCxJQUFOLENBQVcsZ0JBQVgsQ0FBYjs7QUFFQSxLQUFJbUMsT0FBTzRZLE9BQU94UixHQUFQLEVBQVg7QUFDQSxLQUFJNFEsTUFBTVksT0FBT3plLElBQVAsQ0FBWSxLQUFaLEtBQXNCLE9BQWhDO0FBQ0EsS0FBSSxDQUFDRSxFQUFFOGdCLFNBQUYsQ0FBWW5iLElBQVosQ0FBRCxJQUFzQnNCLFNBQVN0QixJQUFULElBQWlCLENBQTNDLEVBQThDO0FBQzdDNFksU0FBT3hSLEdBQVAsQ0FBVyxDQUFYO0FBQ0E7QUFDRCxLQUFJOUYsU0FBU3RCLElBQVQsSUFBaUJzQixTQUFTMFcsR0FBVCxDQUFyQixFQUFvQztBQUNuQ1ksU0FBT3hSLEdBQVAsQ0FBVzRRLEdBQVg7QUFDQTtBQUNELEtBQUlySCxRQUFRclAsU0FBU3NYLE9BQU94UixHQUFQLEVBQVQsQ0FBWjs7QUFFQXdhLFlBQVdwUSxLQUFYLEVBQWtCYixLQUFsQjtBQUNBOztBQUVELFNBQVNxUixPQUFULENBQWtCL2UsQ0FBbEIsRUFBcUI7QUFDcEIsS0FBSXVPLFFBQVFuWCxFQUFFLElBQUYsRUFBUW1NLE9BQVIsQ0FBZ0IsZUFBaEIsQ0FBWjtBQUNBLEtBQUltSyxRQUFRYSxNQUFNclgsSUFBTixDQUFXLE9BQVgsS0FBdUIsQ0FBbkM7O0FBRUF5bkIsWUFBV3BRLEtBQVgsRUFBa0JiLEtBQWxCO0FBQ0E7O0FBRUQsU0FBU2lSLFVBQVQsQ0FBcUJwUSxLQUFyQixFQUE0QmIsS0FBNUIsRUFBa0Q7QUFBQSxLQUFmelYsTUFBZSx1RUFBTixJQUFNOztBQUNqRCxLQUFJNEwsTUFBTTVMLFVBQVVzVyxNQUFNclgsSUFBTixDQUFXLEtBQVgsQ0FBcEI7QUFDQSxLQUFJOG5CLGFBQWF6USxNQUFNclgsSUFBTixDQUFXLFFBQVgsQ0FBakI7QUFDQSxLQUFJNlcsUUFBUVEsTUFBTXJYLElBQU4sQ0FBVyxPQUFYLENBQVo7QUFDQSxLQUFJOFcsU0FBU08sTUFBTXJYLElBQU4sQ0FBVyxRQUFYLENBQWI7QUFDQSxLQUFJcWpCLEtBQUtoTSxNQUFNclgsSUFBTixDQUFXLElBQVgsQ0FBVDtBQUNBLEtBQUkrbkIscUJBQXNCRCxlQUFlM2MsU0FBaEIsR0FBNkJqTCxFQUFFLGlCQUFGLENBQTdCLEdBQW9EQSxFQUFFLGFBQUYsQ0FBN0U7O0FBRUEsS0FBSXlNLE9BQU8wVyxFQUFYLEVBQWU7QUFDZCxNQUFJbGdCLFlBQVksd0JBQWM0a0Isa0JBQWQsQ0FBaEI7QUFDQTVrQixZQUFVQyxJQUFWOztBQUVBLE1BQUlwRCxPQUFPO0FBQ1ZxakIsT0FBSUE7QUFETSxHQUFYOztBQUlBLE1BQUksQ0FBQ3RpQixNQUFMLEVBQWE7QUFDWmYsUUFBS3dXLEtBQUwsR0FBYUEsS0FBYjtBQUNBOztBQUVELE1BQUlLLFVBQVUxTCxTQUFkLEVBQXlCO0FBQ3hCbkwsUUFBSzZXLEtBQUwsR0FBYUEsS0FBYjtBQUNBOztBQUVELE1BQUlDLFdBQVczTCxTQUFmLEVBQTBCO0FBQ3pCbkwsUUFBSzhXLE1BQUwsR0FBY0EsTUFBZDtBQUNBOztBQUVENVcsSUFBRXdNLElBQUYsQ0FBTztBQUNOQyxRQUFLQSxHQURDO0FBRU5oSixTQUFNLE1BRkE7QUFHTjNELFNBQU1BLElBSEE7QUFJTm9mLGFBQVUsTUFKSjtBQUtOekssWUFBUyxpQkFBVTFQLFFBQVYsRUFBb0I7QUFDNUI5QixjQUFVZ0MsSUFBVjs7QUFFQSxRQUFJRixTQUFTMFAsT0FBYixFQUFzQjtBQUNyQixTQUFJelUsRUFBRSxnQkFBRixFQUFvQkosTUFBcEIsSUFBOEIsT0FBT21GLFNBQVN1UixLQUFoQixLQUEwQixXQUE1RCxFQUF5RTtBQUN4RXRXLFFBQUUsZ0JBQUYsRUFBb0I0VSxJQUFwQixDQUF5QjdQLFNBQVN1UixLQUFsQztBQUNBOztBQUVELFNBQUl0VyxFQUFFLGdCQUFGLEVBQW9CSixNQUFwQixJQUE4QixPQUFPbUYsU0FBU21TLEtBQWhCLEtBQTBCLFdBQTVELEVBQXlFO0FBQ3hFbFgsUUFBRSxnQkFBRixFQUFvQjRVLElBQXBCLENBQXlCN1AsU0FBU21TLEtBQWxDO0FBQ0E7O0FBRUQsU0FBSWxYLEVBQUUsaUJBQUYsRUFBcUJKLE1BQXJCLElBQStCLE9BQU9tRixTQUFTc1IsTUFBaEIsS0FBMkIsV0FBOUQsRUFBMkU7QUFDMUVyVyxRQUFFLGlCQUFGLEVBQXFCNFUsSUFBckIsQ0FBMEI3UCxTQUFTc1IsTUFBbkM7O0FBRUEsNkJBQWEvSCxJQUFiLENBQWtCdE8sRUFBRSxpQkFBRixDQUFsQjs7QUFFQSxVQUFJLENBQUMrRSxTQUFTdVIsS0FBZCxFQUFxQjtBQUNwQnRXLFNBQUUsY0FBRixFQUFrQlMsUUFBbEIsQ0FBMkIsT0FBM0I7QUFDQVQsU0FBRSxvQkFBRixFQUF3QmMsV0FBeEIsQ0FBb0MsT0FBcEM7QUFDQTtBQUNEOztBQUVELFNBQUlkLEVBQUUsVUFBRixFQUFjSixNQUFkLElBQXdCLE9BQU9tRixTQUFTNlAsSUFBaEIsS0FBeUIsV0FBckQsRUFBa0U7QUFDakUsVUFBSTZCLFFBQVF6VyxFQUFFK0UsU0FBUzZQLElBQVgsQ0FBWjtBQUNBNVUsUUFBRSxVQUFGLEVBQWM0VSxJQUFkLENBQW1CNkIsS0FBbkI7O0FBRUEsNkJBQWFuSSxJQUFiLENBQWtCdE8sRUFBRSxVQUFGLENBQWxCOztBQUVBLFVBQUksQ0FBQzRuQixVQUFMLEVBQWlCO0FBQ2hCWDtBQUNBO0FBQ0Q7O0FBRUQsU0FBSWxpQixTQUFTbVMsS0FBYixFQUFvQjtBQUNuQmxYLFFBQUUsMkJBQUYsRUFBK0IyRixJQUEvQixDQUFvQ1osU0FBU21TLEtBQTdDO0FBQ0EsTUFGRCxNQUVPO0FBQ05sWCxRQUFFLDJCQUFGLEVBQStCMkYsSUFBL0IsQ0FBb0MsQ0FBcEM7QUFDQTtBQUNELEtBcENELE1Bb0NPO0FBQ04sU0FBSVosU0FBU0EsUUFBYixFQUF1QjtBQUN0QnlQLGtCQUFZelAsU0FBU0EsUUFBckI7QUFDQTtBQUNEO0FBQ0Q7QUFqREssR0FBUDtBQW1EQTtBQUNEOztBQUVELFNBQVMraUIsU0FBVCxDQUFvQmxmLENBQXBCLEVBQXVCO0FBQ3RCLEtBQUk2RCxNQUFNek0sRUFBRSxJQUFGLEVBQVFGLElBQVIsQ0FBYSxLQUFiLENBQVY7QUFDQSxLQUFJcVgsUUFBUW5YLEVBQUUsSUFBRixFQUFRbU0sT0FBUixDQUFnQixtQkFBaEIsQ0FBWjs7QUFFQSxLQUFJaWIsV0FBV2pRLE1BQU0zVCxJQUFOLENBQVcsb0JBQVgsQ0FBZjtBQUNBLEtBQUk2akIsY0FBY0QsU0FBUzVqQixJQUFULENBQWMsYUFBZCxDQUFsQjtBQUNBLEtBQUk4akIsZ0JBQWdCRixTQUFTNWpCLElBQVQsQ0FBYyxlQUFkLENBQXBCOztBQUVBLEtBQUk0akIsU0FBU3BnQixRQUFULENBQWtCLFNBQWxCLENBQUosRUFBa0M7QUFDakNvZ0IsV0FBU2hLLE9BQVQsQ0FBaUIsTUFBakIsRUFBeUJ0YyxXQUF6QixDQUFxQyxTQUFyQztBQUNBLFNBQU8sS0FBUDtBQUNBLEVBSEQsTUFHTztBQUNOc21CLFdBQVNsSyxNQUFULENBQWdCLE1BQWhCLEVBQXdCemMsUUFBeEIsQ0FBaUMsU0FBakM7QUFDQTs7QUFFRDRtQixhQUFZbGdCLEVBQVosQ0FBZSxPQUFmLEVBQXdCLFlBQVk7QUFDbkMsTUFBSXNGLEdBQUosRUFBUztBQUNSek0sS0FBRXdNLElBQUYsQ0FBTztBQUNOQyxTQUFLQSxHQURDO0FBRU5oSixVQUFNLE1BRkE7QUFHTnliLGNBQVUsTUFISjtBQUlOekssYUFBUyxpQkFBVTFQLFFBQVYsRUFBb0I7QUFDNUIsU0FBSUEsU0FBUzBQLE9BQWIsRUFBc0I7QUFDckIsVUFBSXpVLEVBQUUsZ0JBQUYsRUFBb0JKLE1BQXBCLElBQThCLE9BQU9tRixTQUFTdVIsS0FBaEIsS0FBMEIsV0FBNUQsRUFBeUU7QUFDeEV0VyxTQUFFLGdCQUFGLEVBQW9CNFUsSUFBcEIsQ0FBeUI3UCxTQUFTdVIsS0FBbEM7QUFDQTs7QUFFRCxVQUFJdFcsRUFBRSxnQkFBRixFQUFvQkosTUFBcEIsSUFBOEIsT0FBT21GLFNBQVNtUyxLQUFoQixLQUEwQixXQUE1RCxFQUF5RTtBQUN4RWxYLFNBQUUsZ0JBQUYsRUFBb0I0VSxJQUFwQixDQUF5QjdQLFNBQVNtUyxLQUFsQztBQUNBOztBQUVELFVBQUlsWCxFQUFFLGlCQUFGLEVBQXFCSixNQUFyQixJQUErQixPQUFPbUYsU0FBU3NSLE1BQWhCLEtBQTJCLFdBQTlELEVBQTJFO0FBQzFFclcsU0FBRSxpQkFBRixFQUFxQjRVLElBQXJCLENBQTBCN1AsU0FBU3NSLE1BQW5DOztBQUVBLDhCQUFhL0gsSUFBYixDQUFrQnRPLEVBQUUsaUJBQUYsQ0FBbEI7O0FBRUEsV0FBSSxDQUFDK0UsU0FBU3VSLEtBQWQsRUFBcUI7QUFDcEJ0VyxVQUFFLGNBQUYsRUFBa0JTLFFBQWxCLENBQTJCLE9BQTNCO0FBQ0FULFVBQUUsb0JBQUYsRUFBd0JjLFdBQXhCLENBQW9DLE9BQXBDO0FBQ0E7QUFDRDs7QUFFRCxVQUFJZCxFQUFFLFVBQUYsRUFBY0osTUFBZCxJQUF3QixPQUFPbUYsU0FBUzZQLElBQWhCLEtBQXlCLFdBQXJELEVBQWtFO0FBQ2pFLFdBQUk2QixRQUFRelcsRUFBRStFLFNBQVM2UCxJQUFYLENBQVo7QUFDQTVVLFNBQUUsVUFBRixFQUFjNFUsSUFBZCxDQUFtQjZCLEtBQW5COztBQUVBLDhCQUFhbkksSUFBYixDQUFrQnRPLEVBQUUsVUFBRixDQUFsQjs7QUFFQWluQjtBQUNBOztBQUVELFVBQUlsaUIsU0FBU3dSLFVBQWIsRUFBeUI7QUFDeEJ2VyxTQUFFLGlCQUFGLEVBQXFCMkYsSUFBckIsQ0FBMEJaLFNBQVN3UixVQUFULENBQW9CQyxPQUFwQixDQUE0QixDQUE1QixDQUExQjtBQUNBO0FBQ0QsTUFoQ0QsTUFnQ087QUFDTixVQUFJelIsU0FBU0EsUUFBYixFQUF1QjtBQUN0QnlQLG1CQUFZelAsU0FBU0EsUUFBckI7QUFDQTtBQUNEO0FBQ0Q7QUExQ0ssSUFBUDtBQTRDQTtBQUNELEVBL0NEOztBQWlEQXVpQixlQUFjbmdCLEVBQWQsQ0FBaUIsT0FBakIsRUFBMEIsWUFBWTtBQUNyQ2lnQixXQUFTaEssT0FBVCxDQUFpQixNQUFqQixFQUF5QnRjLFdBQXpCLENBQXFDLFNBQXJDO0FBQ0EsRUFGRDs7QUFJQWQsR0FBRTBLLFFBQUYsRUFBWW9SLEtBQVosQ0FBa0IsVUFBVWxULENBQVYsRUFBYTtBQUM5QixNQUFJd2UsU0FBU3BnQixRQUFULENBQWtCLFNBQWxCLENBQUosRUFBa0M7QUFDakMsT0FBSSxDQUFDbVEsTUFBTTZOLEVBQU4sQ0FBU3BjLEVBQUVxYyxNQUFYLENBQUQsSUFBdUI5TixNQUFNbUwsR0FBTixDQUFVMVosRUFBRXFjLE1BQVosRUFBb0JybEIsTUFBcEIsS0FBK0IsQ0FBMUQsRUFBNkQ7QUFDNUR3bkIsYUFBU2hLLE9BQVQsQ0FBaUIsTUFBakIsRUFBeUJ0YyxXQUF6QixDQUFxQyxTQUFyQztBQUNBO0FBQ0Q7QUFDRCxFQU5EO0FBT0E7O0FBRUQsU0FBU2luQixRQUFULENBQW1COUMsTUFBbkIsRUFBMkI7QUFDMUIsS0FBSXhZLE1BQU16TSxFQUFFLElBQUYsRUFBUUYsSUFBUixDQUFhLEtBQWIsQ0FBVjtBQUNBLEtBQUlBLE9BQU8sRUFBWDtBQUNBLEtBQUlrb0IsV0FBV2hvQixFQUFFLGtCQUFGLEVBQXNCSixNQUFyQzs7QUFFQSxLQUFJNk0sR0FBSixFQUFTO0FBQ1J6TSxJQUFFd00sSUFBRixDQUFPO0FBQ05DLFFBQUtBLEdBREM7QUFFTmhKLFNBQU0sTUFGQTtBQUdOM0QsU0FBTUEsSUFIQTtBQUlOb2YsYUFBVSxNQUpKO0FBS056SyxZQUFTLGlCQUFVMVAsUUFBVixFQUFvQjtBQUM1QixRQUFJQSxTQUFTMFAsT0FBYixFQUFzQjtBQUNyQixTQUFJelUsRUFBRSxnQkFBRixFQUFvQkosTUFBcEIsSUFBOEIsT0FBT21GLFNBQVN1UixLQUFoQixLQUEwQixXQUE1RCxFQUF5RTtBQUN4RXRXLFFBQUUsZ0JBQUYsRUFBb0I0VSxJQUFwQixDQUF5QjdQLFNBQVN1UixLQUFsQztBQUNBOztBQUVELFNBQUl0VyxFQUFFLGdCQUFGLEVBQW9CSixNQUFwQixJQUE4QixPQUFPbUYsU0FBU21TLEtBQWhCLEtBQTBCLFdBQTVELEVBQXlFO0FBQ3hFbFgsUUFBRSxnQkFBRixFQUFvQjRVLElBQXBCLENBQXlCN1AsU0FBU21TLEtBQWxDO0FBQ0E7O0FBRUQsU0FBSWxYLEVBQUUsaUJBQUYsRUFBcUJKLE1BQXJCLElBQStCLE9BQU9tRixTQUFTc1IsTUFBaEIsS0FBMkIsV0FBOUQsRUFBMkU7QUFDMUVyVyxRQUFFLGlCQUFGLEVBQXFCNFUsSUFBckIsQ0FBMEI3UCxTQUFTc1IsTUFBbkM7O0FBRUEsNkJBQWEvSCxJQUFiLENBQWtCdE8sRUFBRSxpQkFBRixDQUFsQjs7QUFFQSxVQUFJLENBQUMrRSxTQUFTdVIsS0FBZCxFQUFxQjtBQUNwQnRXLFNBQUUsY0FBRixFQUFrQlMsUUFBbEIsQ0FBMkIsT0FBM0I7QUFDQVQsU0FBRSxvQkFBRixFQUF3QmMsV0FBeEIsQ0FBb0MsT0FBcEM7QUFDQTtBQUNEOztBQUVELFNBQUlkLEVBQUUsVUFBRixFQUFjSixNQUFkLElBQXdCLE9BQU9tRixTQUFTNlAsSUFBaEIsS0FBeUIsV0FBckQsRUFBa0U7QUFDakUsVUFBSTZCLFFBQVF6VyxFQUFFK0UsU0FBUzZQLElBQVgsQ0FBWjtBQUNBNVUsUUFBRSxVQUFGLEVBQWM0VSxJQUFkLENBQW1CNkIsS0FBbkI7O0FBRUEsNkJBQWFuSSxJQUFiLENBQWtCdE8sRUFBRSxVQUFGLENBQWxCOztBQUVBLFVBQUksQ0FBQ2dvQixRQUFMLEVBQWU7QUFDZGY7QUFDQTtBQUNEOztBQUVjLFNBQUlsaUIsU0FBU21TLEtBQWIsRUFBb0I7QUFDaEJsWCxRQUFFLDJCQUFGLEVBQStCMkYsSUFBL0IsQ0FBb0NaLFNBQVNtUyxLQUE3QztBQUNIO0FBRWhCLEtBbkNELE1BbUNPO0FBQ04sU0FBSW5TLFNBQVNBLFFBQWIsRUFBdUI7QUFDdEJ5UCxrQkFBWXpQLFNBQVNBLFFBQXJCO0FBQ0E7QUFDRDtBQUNEO0FBOUNLLEdBQVA7QUFnREE7QUFDRDs7QUFFRCxTQUFTa2pCLFNBQVQsQ0FBb0JyZixDQUFwQixFQUF1QjtBQUN0QixLQUFJNkQsTUFBTXpNLEVBQUUsSUFBRixFQUFRRixJQUFSLENBQWEsS0FBYixDQUFWO0FBQ0EsS0FBSXFYLFFBQVFuWCxFQUFFLElBQUYsRUFBUW1NLE9BQVIsQ0FBZ0IsZUFBaEIsQ0FBWjtBQUNBLEtBQUlnWCxLQUFLaE0sTUFBTXJYLElBQU4sQ0FBVyxJQUFYLENBQVQ7QUFDQSxLQUFJd1csUUFBU2EsTUFBTXJYLElBQU4sQ0FBVyxPQUFYLE1BQXdCbUwsU0FBekIsR0FBc0NrTSxNQUFNclgsSUFBTixDQUFXLE9BQVgsQ0FBdEMsR0FBNEQsQ0FBeEU7O0FBRUEsS0FBSUEsT0FBTztBQUNWcWpCLE1BQUlBLEVBRE07QUFFVjdNLFNBQU9BO0FBRkcsRUFBWDs7QUFLQXRXLEdBQUV3TSxJQUFGLENBQU87QUFDTkMsT0FBS0EsR0FEQztBQUVOaEosUUFBTSxNQUZBO0FBR04zRCxRQUFNQSxJQUhBO0FBSU5vZixZQUFVLE1BSko7QUFLTnpLLFdBQVMsaUJBQVUxUCxRQUFWLEVBQW9CO0FBQzVCLE9BQUlBLFNBQVMwUCxPQUFiLEVBQXNCO0FBQ3JCLFFBQUl6VSxFQUFFLGdCQUFGLEVBQW9CSixNQUFwQixJQUE4QixPQUFPbUYsU0FBU3VSLEtBQWhCLEtBQTBCLFdBQTVELEVBQXlFO0FBQ3hFdFcsT0FBRSxnQkFBRixFQUFvQjRVLElBQXBCLENBQXlCN1AsU0FBU3VSLEtBQWxDO0FBQ0E7O0FBRUQsUUFBSXRXLEVBQUUsZ0JBQUYsRUFBb0JKLE1BQXBCLElBQThCLE9BQU9tRixTQUFTbVMsS0FBaEIsS0FBMEIsV0FBNUQsRUFBeUU7QUFDeEVsWCxPQUFFLGdCQUFGLEVBQW9CNFUsSUFBcEIsQ0FBeUI3UCxTQUFTbVMsS0FBbEM7QUFDQTs7QUFFRCxRQUFJbFgsRUFBRSxpQkFBRixFQUFxQkosTUFBckIsSUFBK0IsT0FBT21GLFNBQVNzUixNQUFoQixLQUEyQixXQUE5RCxFQUEyRTtBQUMxRXJXLE9BQUUsaUJBQUYsRUFBcUI0VSxJQUFyQixDQUEwQjdQLFNBQVNzUixNQUFuQzs7QUFFQSw0QkFBYS9ILElBQWIsQ0FBa0J0TyxFQUFFLGlCQUFGLENBQWxCOztBQUVBLFNBQUksQ0FBQytFLFNBQVN1UixLQUFkLEVBQXFCO0FBQ3BCdFcsUUFBRSxjQUFGLEVBQWtCUyxRQUFsQixDQUEyQixPQUEzQjtBQUNBVCxRQUFFLG9CQUFGLEVBQXdCYyxXQUF4QixDQUFvQyxPQUFwQztBQUNBO0FBQ0Q7O0FBRUQsUUFBSWQsRUFBRSxVQUFGLEVBQWNKLE1BQWQsSUFBd0IsT0FBT21GLFNBQVM2UCxJQUFoQixLQUF5QixXQUFyRCxFQUFrRTtBQUNqRSxTQUFJNkIsUUFBUXpXLEVBQUUrRSxTQUFTNlAsSUFBWCxDQUFaO0FBQ0E1VSxPQUFFLFVBQUYsRUFBYzRVLElBQWQsQ0FBbUI2QixLQUFuQjs7QUFFQSw0QkFBYW5JLElBQWIsQ0FBa0J0TyxFQUFFLFVBQUYsQ0FBbEI7O0FBRUFpbkI7QUFDQTs7QUFFRCxRQUFJbGlCLFNBQVN3UixVQUFiLEVBQXlCO0FBQ3hCdlcsT0FBRSxpQkFBRixFQUFxQjJGLElBQXJCLENBQTBCWixTQUFTd1IsVUFBVCxDQUFvQkMsT0FBcEIsQ0FBNEIsQ0FBNUIsQ0FBMUI7QUFDQTtBQUNELElBaENELE1BZ0NPO0FBQ04sUUFBSXpSLFNBQVNBLFFBQWIsRUFBdUI7QUFDdEJ5UCxpQkFBWXpQLFNBQVNBLFFBQXJCO0FBQ0E7QUFDRDtBQUNEO0FBM0NLLEVBQVA7QUE2Q0E7O0FBRUQ7QUFDQTtBQUNBOztBQUVBLElBQUltakIsV0FBVyxTQUFYQSxRQUFXLEdBQWtDO0FBQUEsS0FBeEJ4b0IsVUFBd0IsdUVBQVhNLEVBQUUsTUFBRixDQUFXOztBQUNoRE4sWUFBV3lILEVBQVgsQ0FBYyxZQUFkLEVBQTRCLGlCQUE1QixFQUErQ2dnQixVQUEvQztBQUNBem5CLFlBQVd5SCxFQUFYLENBQWMsWUFBZCxFQUE0QixnQkFBNUIsRUFBOENxZ0IsU0FBOUM7QUFDQTluQixZQUFXeUgsRUFBWCxDQUFjLFlBQWQsRUFBNEIsZUFBNUIsRUFBNkNzZ0IsUUFBN0M7QUFDQS9uQixZQUFXeUgsRUFBWCxDQUFjLFlBQWQsRUFBNEIsZ0JBQTVCLEVBQThDdWdCLFNBQTlDO0FBQ0Fob0IsWUFBV3lILEVBQVgsQ0FBYyxZQUFkLEVBQTRCLGNBQTVCLEVBQTRDd2dCLE9BQTVDO0FBQ0Fqb0IsWUFBV3lILEVBQVgsQ0FBYyxZQUFkLEVBQTRCLGdCQUE1QixFQUE4QzJnQixTQUE5QztBQUNBcG9CLFlBQVd5SCxFQUFYLENBQWMsT0FBZCxFQUF1QixpQkFBdkIsRUFBMEM0Z0IsUUFBMUM7QUFDQXJvQixZQUFXeUgsRUFBWCxDQUFjLE9BQWQsRUFBdUIsZ0JBQXZCLEVBQXlDOGdCLFNBQXpDO0FBQ0F2b0IsWUFBV3lILEVBQVgsQ0FBYyxPQUFkLEVBQXVCeWYsV0FBdkIsRUFBb0NtQixRQUFwQztBQUNBcm9CLFlBQVd5SCxFQUFYLENBQWMsT0FBZCxFQUF1QjBmLFlBQXZCLEVBQXFDSyxTQUFyQztBQUNBLENBWEQ7O0FBYUEsU0FBU2lCLGdCQUFULEdBQTZCO0FBQzVCbm9CLEdBQUUwSyxRQUFGLEVBQVlvUixLQUFaLENBQWtCLFVBQVVsVCxDQUFWLEVBQWE7QUFDOUIsTUFBSTVJLEVBQUU4bUIsUUFBRixFQUFZOWYsUUFBWixDQUFxQixNQUFyQixDQUFKLEVBQWtDO0FBQ2pDLE9BQUksQ0FBQytmLFdBQVcvQixFQUFYLENBQWNwYyxFQUFFcWMsTUFBaEIsQ0FBRCxJQUE0QjhCLFdBQVd6RSxHQUFYLENBQWUxWixFQUFFcWMsTUFBakIsRUFBeUJybEIsTUFBekIsS0FBb0MsQ0FBaEUsSUFBcUUsQ0FBQ0ksRUFBRSxnQkFBRixFQUFvQmdsQixFQUFwQixDQUF1QnBjLEVBQUVxYyxNQUF6QixDQUF0RSxJQUEwR2psQixFQUFFLGdCQUFGLEVBQW9Cc2lCLEdBQXBCLENBQXdCMVosRUFBRXFjLE1BQTFCLEVBQWtDcmxCLE1BQWxDLEtBQTZDLENBQTNKLEVBQThKO0FBQzdKc25CO0FBQ0E7QUFDRDtBQUNELEVBTkQ7QUFPQTs7QUFFRDtBQUNBO0FBQ0E7O1FBRVFnQixRLEdBQUFBLFE7UUFBVUMsZ0IsR0FBQUEsZ0I7Ozs7Ozs7QUNuZGxCOztBQUVBO0FBQ0E7QUFDQTs7Ozs7QUFFQSxTQUFTQyxVQUFULENBQXFCQyxTQUFyQixFQUFnQ0MsV0FBaEMsRUFBNkMzVSxJQUE3QyxFQUFtRDtBQUNsRDBVLGFBQVlBLGFBQWFyb0IsRUFBRSxZQUFGLENBQXpCO0FBQ0Fzb0IsZUFBY0EsZUFBZSxXQUE3QjtBQUNBM1UsUUFBT0EsUUFBUTNULEVBQUUsaUJBQUYsRUFBcUJGLElBQXJCLENBQTBCLFFBQTFCLENBQWY7O0FBRUEsS0FBSXVvQixVQUFVem9CLE1BQVYsSUFBb0IrVCxJQUF4QixFQUE4QjtBQUM3QjBVLFlBQVUvbkIsSUFBVixDQUFlLFlBQVk7QUFDMUIsT0FBSXVJLFFBQVE3SSxFQUFFLElBQUYsQ0FBWjtBQUNBLE9BQUk0VSxPQUFPL0wsTUFBTStMLElBQU4sRUFBWDtBQUNBL0wsU0FBTStMLElBQU4sQ0FBV0EsS0FBSzVKLE9BQUwsQ0FBYSxJQUFJRCxNQUFKLENBQVc0SSxJQUFYLEVBQWlCLElBQWpCLENBQWIsRUFBcUMsa0JBQWtCMlUsV0FBbEIsR0FBZ0MsYUFBckUsQ0FBWDtBQUNBLEdBSkQ7QUFLQTtBQUNEOztBQUVEO0FBQ0E7QUFDQTs7UUFFUUYsVSxHQUFBQSxVOzs7Ozs7O0FDeEJSOztBQUVBO0FBQ0E7QUFDQTs7Ozs7QUFFQSxJQUFJRyxVQUFVdm9CLEVBQUUsU0FBRixDQUFkLEMsQ0FBNEI7QUFDNUIsSUFBSXdvQixnQkFBZ0J4b0IsRUFBRSxzQkFBRixDQUFwQjtBQUNBLElBQUl5b0Isc0JBQXNCem9CLEVBQUUseUJBQUYsQ0FBMUI7QUFDQSxJQUFJMG9CLHFCQUFxQjFvQixFQUFFLHVCQUFGLENBQXpCO0FBQ0EsSUFBSTJvQixxQkFBcUIzb0IsRUFBRSx1QkFBRixDQUF6QjtBQUNBLElBQUk0b0IsWUFBWUosY0FBYzFvQixJQUFkLENBQW1CLEtBQW5CLENBQWhCO0FBQ0EsSUFBSStHLE1BQU0waEIsUUFBUU0sV0FBUixFQUFWOztBQUVBO0FBQ0E7QUFDQTs7QUFFQSxTQUFTQyxrQkFBVCxHQUErQjtBQUM5QjlvQixHQUFFLE1BQUYsRUFBVW1ILEVBQVYsQ0FBYSxPQUFiLEVBQXNCLHFCQUF0QixFQUE2QyxZQUFZO0FBQ3hEcWhCLGdCQUFjMWhCLEdBQWQsQ0FBa0I7QUFDakIsVUFBT0QsTUFBTSxJQURJO0FBRWpCLGFBQVUsa0JBQWtCQSxHQUFsQixHQUF3QjtBQUZqQixHQUFsQjtBQUlBN0csSUFBRSxJQUFGLEVBQVFnZ0IsV0FBUixDQUFvQixNQUFwQjs7QUFFQXJmLGFBQVcsWUFBVztBQUNyQlgsS0FBRSx1QkFBRixFQUEyQitvQixLQUEzQjtBQUNBLEdBRkQsRUFFRyxHQUZIOztBQUlBUCxnQkFBY3pJLFdBQWQsQ0FBMEIsTUFBMUI7O0FBRUEsTUFBSXdJLFFBQVF2aEIsUUFBUixDQUFpQixNQUFqQixDQUFKLEVBQThCO0FBQzdCdWhCLFdBQVF2SSxXQUFSLENBQW9CLE1BQXBCO0FBQ0EySSxzQkFBbUI1YixHQUFuQixDQUF1QixFQUF2QjtBQUNBMGIsdUJBQW9CM25CLFdBQXBCLENBQWdDLFNBQWhDO0FBQ0EsR0FKRCxNQUlPO0FBQ05ILGNBQVcsWUFBWTtBQUN0QjRuQixZQUFRdkksV0FBUixDQUFvQixNQUFwQjtBQUNBLElBRkQsRUFFRyxHQUZIO0FBR0E7QUFDRCxFQXRCRDtBQXVCQTs7QUFFRCxTQUFTZ0osa0JBQVQsR0FBK0I7QUFDOUIsS0FBSSxDQUFDTCxtQkFBbUIvb0IsTUFBeEIsRUFBZ0M7QUFDL0IsU0FBTyxLQUFQO0FBQ0E7O0FBRUQrb0Isb0JBQW1CeGhCLEVBQW5CLENBQXNCLE9BQXRCLEVBQStCLFVBQVV5QixDQUFWLEVBQWE7QUFDM0MsTUFBSUMsUUFBUTdJLEVBQUUsSUFBRixDQUFaO0FBQ0EsTUFBSTJGLE9BQU9rRCxNQUFNa0UsR0FBTixFQUFYOztBQUVBLE1BQUlwSCxLQUFLL0YsTUFBTCxJQUFlLENBQW5CLEVBQXNCO0FBQ3JCSSxLQUFFd00sSUFBRixDQUFPO0FBQ05DLFNBQUttYyxTQURDO0FBRU45b0IsVUFBTTZGLElBRkE7QUFHTnVaLGNBQVUsTUFISjtBQUlOekssYUFBUyxpQkFBVTFQLFFBQVYsRUFBb0I7QUFDNUIsU0FBSUEsU0FBUzBQLE9BQWIsRUFBc0I7QUFDckIsVUFBSUcsT0FBTyxFQUFYO0FBQ0EsVUFBSTdQLFNBQVM4UCxLQUFULENBQWVqVixNQUFmLEdBQXdCLENBQTVCLEVBQStCO0FBQzlCLFlBQUssSUFBSVcsSUFBSSxDQUFiLEVBQWdCQSxJQUFJd0UsU0FBUzhQLEtBQVQsQ0FBZWpWLE1BQW5DLEVBQTJDVyxHQUEzQyxFQUFnRDtBQUMvQ3FVLGdCQUNDLHVDQUNDN1AsU0FBUzhQLEtBQVQsQ0FBZXRVLENBQWYsRUFBa0Iwb0IsR0FBbEIsR0FBd0IsMENBQTBDbGtCLFNBQVM4UCxLQUFULENBQWV0VSxDQUFmLEVBQWtCMG9CLEdBQTVELE9BQXhCLEdBQWlHLEVBRGxHLDJDQUdDbGtCLFNBQVM4UCxLQUFULENBQWV0VSxDQUFmLEVBQWtCMm9CLElBQWxCLEdBQXlCLG1IQUFzRW5rQixTQUFTOFAsS0FBVCxDQUFldFUsQ0FBZixFQUFrQjJvQixJQUF4RixvQkFBekIsR0FBNEksRUFIN0ksOENBSTBDbmtCLFNBQVM4UCxLQUFULENBQWV0VSxDQUFmLEVBQWtCa1gsSUFKNUQsVUFJMEUxUyxTQUFTOFAsS0FBVCxDQUFldFUsQ0FBZixFQUFrQnlSLElBSjVGLGFBS0NqTixTQUFTOFAsS0FBVCxDQUFldFUsQ0FBZixFQUFrQjJXLEtBQWxCLEdBQTBCLHdDQUF3Q25TLFNBQVM4UCxLQUFULENBQWV0VSxDQUFmLEVBQWtCMlcsS0FBMUQsWUFBMUIsR0FBd0csRUFMekcsdUJBREQ7QUFTQTtBQUNEbFgsU0FBRSx5Q0FBRixFQUE2QzRVLElBQTdDLENBQWtEQSxJQUFsRDtBQUNBNlQsMkJBQW9CaG9CLFFBQXBCLENBQTZCLFNBQTdCO0FBQ0Fpb0IsMEJBQW1CNW5CLFdBQW5CLENBQStCLFNBQS9CO0FBQ0EsT0FmRCxNQWVPO0FBQ040bkIsMEJBQW1Cam9CLFFBQW5CLENBQTRCLFNBQTVCO0FBQ0Fnb0IsMkJBQW9CM25CLFdBQXBCLENBQWdDLFNBQWhDO0FBQ0E7QUFDRDtBQUNELFNBQUlpRSxTQUFTbkQsS0FBYixFQUFvQjtBQUNuQnhCLGNBQVE0RSxHQUFSLENBQVksT0FBWjtBQUNBO0FBQ0Q7QUE5QkssSUFBUDtBQWdDQSxHQWpDRCxNQWlDTztBQUNOeWpCLHVCQUFvQjNuQixXQUFwQixDQUFnQyxTQUFoQztBQUNBNG5CLHNCQUFtQjVuQixXQUFuQixDQUErQixTQUEvQjtBQUNBO0FBQ0QsRUF6Q0Q7QUEwQ0E7O0FBRUQ7QUFDQTtBQUNBOztRQUVRZ29CLGtCLEdBQUFBLGtCO1FBQW9CRSxrQixHQUFBQSxrQjs7Ozs7OztBQ2pHNUI7O0FBRUE7QUFDQTtBQUNBOzs7Ozs7O0FBRUE7O0FBRUE7QUFDQTtBQUNBOztBQUVBLElBQUlHLGlCQUFpQixLQUFyQjtBQUNBLElBQUlDLGdCQUFnQnBwQixFQUFFLG1CQUFGLENBQXBCO0FBQ0EsSUFBSXFwQixjQUFjcnBCLEVBQUUsdUJBQUYsQ0FBbEI7O0FBRUEsU0FBU3NwQixjQUFULEdBQTJCO0FBQzFCdHBCLEdBQUUsZ0JBQUYsRUFBb0JNLElBQXBCLENBQXlCLFVBQVU0TCxLQUFWLEVBQWlCMUwsRUFBakIsRUFBcUI7QUFDN0MsTUFBSXFJLFFBQVE3SSxFQUFFUSxFQUFGLENBQVo7QUFDQSxNQUFJK29CLGNBQWUxZ0IsTUFBTXZGLElBQU4sQ0FBVyxrQkFBWCxDQUFELEdBQW1DMkQsU0FBUzRCLE1BQU12RixJQUFOLENBQVcsa0JBQVgsQ0FBVCxDQUFuQyxHQUE4RSxHQUFoRztBQUNBLE1BQUlrbUIsWUFBYTNnQixNQUFNdkYsSUFBTixDQUFXLGdCQUFYLENBQUQsR0FBaUN1RixNQUFNdkYsSUFBTixDQUFXLGdCQUFYLENBQWpDLEdBQWdFLGFBQWhGO0FBQ0EsTUFBSW1tQixNQUFPNWdCLE1BQU12RixJQUFOLENBQVcsVUFBWCxDQUFELEdBQTJCdUYsTUFBTXZGLElBQU4sQ0FBVyxVQUFYLENBQTNCLEdBQW9ELE1BQTlEO0FBQ0EsTUFBSW9tQixhQUFjN2dCLE1BQU12RixJQUFOLENBQVcsaUJBQVgsQ0FBRCxHQUFrQ3VGLE1BQU12RixJQUFOLENBQVcsaUJBQVgsQ0FBbEMsR0FBa0UsRUFBbkY7O0FBRUEsTUFBSXFtQixLQUFLOWdCLE1BQU0rZ0IsVUFBTixDQUFpQjtBQUN6QkMsaUJBQWMsQ0FEVztBQUV6QkMsWUFBUyxxQkFGZ0I7QUFHekJDLGFBQVUsc0JBSGU7QUFJekJDLGVBQVksS0FKYTtBQUt6QlQsZ0JBQWFBLFdBTFk7QUFNekI7QUFDQUMsY0FBV0EsU0FQYztBQVF6QkMsUUFBS0EsR0FSb0I7QUFTekJDLGVBQVlBLFVBVGE7QUFVekJPLG9CQUFpQixDQUFDLENBVk87QUFXekI7QUFDQUMsWUFBUyxtQkFBWTtBQUNwQmxxQixNQUFFLHdCQUFGLEVBQTRCUyxRQUE1QixDQUFxQyxTQUFyQzs7QUFFQW9JLFVBQU1yRixJQUFOLENBQVcscUJBQVgsRUFBa0MyRCxFQUFsQyxDQUFxQyxPQUFyQyxFQUE4QyxVQUFVeUIsQ0FBVixFQUFhO0FBQzFEK2dCLFFBQUd4YyxJQUFIO0FBQ0EsS0FGRDtBQUdBdEUsVUFBTXJGLElBQU4sQ0FBVyxxQkFBWCxFQUFrQzJELEVBQWxDLENBQXFDLE9BQXJDLEVBQThDLFVBQVV5QixDQUFWLEVBQWE7QUFDMUQrZ0IsUUFBR1EsUUFBSDtBQUNBLEtBRkQ7QUFHQXRoQixVQUFNckYsSUFBTixDQUFXLHFCQUFYLEVBQWtDMkQsRUFBbEMsQ0FBcUMsT0FBckMsRUFBOEMsVUFBVXlCLENBQVYsRUFBYTtBQUMxRCtnQixRQUFHUyxJQUFIO0FBQ0EsS0FGRDtBQUdBdmhCLFVBQU1yRixJQUFOLENBQVcsc0JBQVgsRUFBbUMyRCxFQUFuQyxDQUFzQyxPQUF0QyxFQUErQyxVQUFVeUIsQ0FBVixFQUFhO0FBQzNEK2dCLFFBQUcxTSxJQUFIO0FBQ0EsS0FGRDtBQUdBO0FBM0J3QixHQUFqQixDQUFUO0FBNkJBLEVBcENEO0FBcUNBOztBQUVEO0FBQ0E7QUFDQTs7QUFFQSxTQUFTb04sWUFBVCxHQUF5QjtBQUN4QixLQUFJQyxhQUFhdHFCLEVBQUUsd0JBQUYsQ0FBakI7O0FBRUFzcUIsWUFBV25qQixFQUFYLENBQWMsT0FBZCxFQUF1QixZQUFZO0FBQ2xDLE1BQUksQ0FBQ2dpQixjQUFMLEVBQXFCO0FBQ3BCRztBQUNBSCxvQkFBaUIsSUFBakI7QUFDQTs7QUFFRCxNQUFJbUIsV0FBV3RqQixRQUFYLENBQW9CLFVBQXBCLENBQUosRUFBcUM7QUFDcENzakIsY0FBV3hwQixXQUFYLENBQXVCLFVBQXZCO0FBQ0Fzb0IsaUJBQWMzb0IsUUFBZCxDQUF1QixTQUF2QjtBQUNBNG9CLGVBQVl2b0IsV0FBWixDQUF3QixTQUF4QjtBQUNBLEdBSkQsTUFJTztBQUNOd3BCLGNBQVc3cEIsUUFBWCxDQUFvQixVQUFwQjtBQUNBMm9CLGlCQUFjdG9CLFdBQWQsQ0FBMEIsU0FBMUI7QUFDQXVvQixlQUFZNW9CLFFBQVosQ0FBcUIsU0FBckI7QUFDQTtBQUNELEVBZkQ7QUFnQkE7O0FBRUQ7QUFDQTtBQUNBOztRQUVRNHBCLFksR0FBQUEsWTs7Ozs7Ozs7QUNyRlI7O0FBRUE7QUFDQTtBQUNBOzs7Ozs7O0FBRUE7Ozs7OztBQUVBO0FBQ0E7O0FBRUEsU0FBU0UsY0FBVCxHQUEyQjtBQUMxQixLQUFJbmQsUUFBUXBOLEVBQUUsbUJBQUYsQ0FBWjtBQUNBLEtBQUlpRCxZQUFZLHdCQUFjbUssS0FBZCxDQUFoQjs7QUFFQXBOLEdBQUUsTUFBRixFQUFVbUgsRUFBVixDQUFhLE9BQWIsRUFBc0Isd0JBQXRCLEVBQWdELFlBQVk7QUFDM0QsTUFBSTBCLFFBQVE3SSxFQUFFLElBQUYsQ0FBWjtBQUNBLE1BQUlpa0IsT0FBT3BiLE1BQU0vSSxJQUFOLENBQVcsTUFBWCxDQUFYO0FBQ0EsTUFBSTBxQixjQUFjM2hCLE1BQU1zRCxPQUFOLENBQWMsZ0JBQWQsQ0FBbEI7QUFDQSxNQUFJTSxNQUFNK2QsWUFBWTFxQixJQUFaLENBQWlCLEtBQWpCLENBQVY7QUFDQSxNQUFJcWpCLEtBQUtxSCxZQUFZMXFCLElBQVosQ0FBaUIsSUFBakIsQ0FBVDtBQUNBbUQsWUFBVUMsSUFBVjs7QUFFQWxELElBQUV3TSxJQUFGLENBQU87QUFDTkMsUUFBS0EsR0FEQztBQUVOaEosU0FBTSxNQUZBO0FBR04zRCxTQUFNO0FBQ0xta0IsVUFBTUEsSUFERDtBQUVMZDtBQUZLLElBSEE7QUFPTmpFLGFBQVUsTUFQSjtBQVFOekssWUFBUyxpQkFBVTFQLFFBQVYsRUFBb0I7QUFDNUIsUUFBSUEsU0FBUzBQLE9BQWIsRUFBc0I7QUFDckJ4UixlQUFVZ0MsSUFBVjtBQUNBakYsT0FBRSx3QkFBRixFQUE0QmMsV0FBNUIsQ0FBd0MsUUFBeEM7QUFDQStILFdBQU1wSSxRQUFOLENBQWUsUUFBZjtBQUNBMk0sV0FBTXdILElBQU4sQ0FBVzdQLFNBQVM2UCxJQUFwQjtBQUNBO0FBQ0Q7QUFmSyxHQUFQO0FBaUJBLEVBekJEO0FBMEJBOztBQUVEO0FBQ0E7QUFDQTs7UUFFUTJWLGMsR0FBQUEsYzs7Ozs7OztBQy9DUjs7QUFFQTtBQUNBOzs7OztBQUVBLFNBQVNFLGFBQVQsR0FBeUI7QUFDeEIsUUFBSUMsY0FBYzFxQixFQUFFLDRCQUFGLENBQWxCO0FBQ0EsUUFBSTJxQixVQUFVRCxZQUFZbG5CLElBQVosQ0FBaUIsVUFBakIsQ0FBZDs7QUFFQW1uQixZQUFReGpCLEVBQVIsQ0FBVyxnQkFBWCxFQUE2QixVQUFVeUIsQ0FBVixFQUFhO0FBQ3pDLFlBQUlDLFFBQVE3SSxFQUFFLElBQUYsQ0FBWjtBQUNBLFlBQUkrTSxNQUFNbEUsTUFBTWtFLEdBQU4sRUFBVjtBQUNBLFlBQUlBLEdBQUosRUFBUztBQUNSck0sbUJBQU8wRSxRQUFQLENBQWdCcVMsSUFBaEIsR0FBdUIxSyxHQUF2QjtBQUNBO0FBQ0QsS0FORDtBQU9BOztBQUVELFNBQVM0UCxXQUFULEdBQXVCO0FBQ25CM2MsTUFBRVUsTUFBRixFQUFVeUcsRUFBVixDQUFhLE1BQWIsRUFBcUIsWUFBWTtBQUM3QixZQUFJeWpCLE9BQU9ua0IsS0FBUCxHQUFlLElBQW5CLEVBQXlCOztBQUU5QixnQkFBSW9rQixlQUFlN3FCLEVBQUUsY0FBRixFQUFrQmlQLE1BQWxCLEVBQW5COztBQUVNalAsY0FBRVUsTUFBRixFQUFVNGIsTUFBVixDQUFpQixZQUFXO0FBQ2pDdU8sK0JBQWU3cUIsRUFBRSxjQUFGLEVBQWtCaVAsTUFBbEIsRUFBZjtBQUNBLGFBRks7O0FBSUcsZ0JBQU15VixXQUFXMWtCLEVBQUUsb0JBQUYsQ0FBakI7QUFDQSxnQkFBTThxQixXQUFXOXFCLEVBQUUsYUFBRixDQUFqQjs7QUFFQTBrQixxQkFBU3ZkLEVBQVQsQ0FBWSxPQUFaLEVBQXFCLFlBQU07QUFDdkIyakIseUJBQVNoa0IsR0FBVCxDQUFhLEVBQUUsZ0NBQThCK2pCLFlBQTlCLFFBQUYsRUFBbUQsT0FBV0EsWUFBWCxPQUFuRCxFQUFiO0FBQ0E3cUIsa0JBQUUsVUFBRixFQUFjcUQsTUFBZCxDQUFxQiw4QkFBckI7QUFDQXJELGtCQUFFLE1BQUYsRUFBVVMsUUFBVixDQUFtQixpQkFBbkI7QUFDQXFxQix5QkFBUzVOLE1BQVQ7QUFDSCxhQUxEOztBQU9BbGQsY0FBRTBLLFFBQUYsRUFBWXFhLE9BQVosQ0FBb0IsVUFBVW5jLENBQVYsRUFBWTtBQUM1QixvQkFBSSxDQUFDa2lCLFNBQVM5RixFQUFULENBQVlwYyxFQUFFcWMsTUFBZCxDQUFELElBQTBCNkYsU0FBU3hJLEdBQVQsQ0FBYTFaLEVBQUVxYyxNQUFmLEVBQXVCcmxCLE1BQXZCLEtBQWtDLENBQWhFLEVBQW1FO0FBQy9ESSxzQkFBRSxXQUFGLEVBQWVhLE1BQWY7QUFDQWlxQiw2QkFBUzFOLE9BQVQ7QUFDZnBkLHNCQUFFLE1BQUYsRUFBVWMsV0FBVixDQUFzQixpQkFBdEI7QUFDWTtBQUNKLGFBTkQ7QUFPSCxTQXpCRCxNQXlCTztBQUNKLGdCQUFJNGpCLFlBQVcxa0IsRUFBRSxjQUFGLENBQWY7QUFDQSxnQkFBSThxQixZQUFXOXFCLEVBQUUsaUJBQUYsQ0FBZjtBQUNBLGdCQUFJK3FCLGFBQWEvcUIsRUFBRSwyQkFBRixDQUFqQjs7QUFFQTBrQixzQkFBUzVJLEtBQVQsQ0FBZSxZQUFZO0FBQ3ZCOWIsa0JBQUUsSUFBRixFQUFRZ2dCLFdBQVIsQ0FBb0IsUUFBcEI7QUFDQStLLDJCQUFXL0ssV0FBWCxDQUF1QixRQUF2QjtBQUNBOEssMEJBQVNFLFVBQVQ7QUFDSCxhQUpEO0FBS0Y7QUFDSixLQXJDRDtBQXNDSDs7QUFFRCxTQUFTQyxlQUFULEdBQTJCO0FBQ3ZCanJCLE1BQUVVLE1BQUYsRUFBVXlHLEVBQVYsQ0FBYSxNQUFiLEVBQXFCLFlBQVk7QUFDN0IsWUFBSXlqQixPQUFPbmtCLEtBQVAsR0FBZSxJQUFuQixFQUF5QjtBQUNyQixnQkFBSW9rQixlQUFlN3FCLEVBQUUsU0FBRixFQUFhaVAsTUFBYixFQUFuQjtBQUNBLGdCQUFNeVYsV0FBVzFrQixFQUFFLHFCQUFGLENBQWpCO0FBQ0EsZ0JBQU04cUIsV0FBVzlxQixFQUFFLHNCQUFGLENBQWpCO0FBQ0EsZ0JBQU1rckIsYUFBYWxyQixFQUFFLFlBQUYsQ0FBbkI7O0FBRUEwa0IscUJBQVM1SSxLQUFULENBQWUsWUFBVztBQUN0QixvQkFBSXFQLGlCQUFpQm5yQixFQUFFLElBQUYsRUFBUUYsSUFBUixDQUFhLGNBQWIsQ0FBckI7O0FBRUFFLGtCQUFFLElBQUYsRUFBUVMsUUFBUixDQUFpQixRQUFqQixFQUEyQnlnQixRQUEzQixHQUFzQ3BnQixXQUF0QyxDQUFrRCxRQUFsRDtBQUNBb3FCLDJCQUFXOU4sT0FBWDtBQUNBcGQsa0JBQUUsVUFBRixFQUFjcUQsTUFBZCxDQUFxQix1Q0FBckI7QUFDQXluQix5QkFBU2hrQixHQUFULENBQWEsRUFBRSxnQ0FBOEIrakIsWUFBOUIsUUFBRixFQUFiLEVBQWlFM04sTUFBakU7QUFDQWxkLGtCQUFFLGlDQUFpQ21yQixjQUFqQyxHQUFrRCxHQUFwRCxFQUF5RGpPLE1BQXpEO0FBQ0EsdUJBQU8sS0FBUDtBQUNILGFBVEQ7O0FBV0FsZCxjQUFFMEssUUFBRixFQUFZcWEsT0FBWixDQUFvQixVQUFVbmMsQ0FBVixFQUFZO0FBQzVCLG9CQUFJLENBQUNraUIsU0FBUzlGLEVBQVQsQ0FBWXBjLEVBQUVxYyxNQUFkLENBQUQsSUFDRzZGLFNBQVN4SSxHQUFULENBQWExWixFQUFFcWMsTUFBZixFQUF1QnJsQixNQUF2QixLQUFrQyxDQUR6QyxFQUM0QztBQUN4Q0ksc0JBQUUsb0JBQUYsRUFBd0JhLE1BQXhCO0FBQ0E2akIsNkJBQVM1akIsV0FBVCxDQUFxQixRQUFyQjtBQUNBZ3FCLDZCQUFTMU4sT0FBVDtBQUNIO0FBQ0osYUFQRDtBQVFILFNBekJELE1BeUJPO0FBQ0gsZ0JBQUlnTyxlQUFlcHJCLEVBQUUscUJBQUYsQ0FBbkI7QUFDQSxnQkFBSXFyQix1QkFBdUJyckIsRUFBRSx5QkFBRixDQUEzQjtBQUNBLGdCQUFJc3JCLFdBQVd0ckIsRUFBRSxxQkFBRixDQUFmOztBQUVBb3JCLHlCQUFhamtCLEVBQWIsQ0FBZ0IsT0FBaEIsRUFBeUIsWUFBWTtBQUNqQ25ILGtCQUFFLElBQUYsRUFBUWtoQixRQUFSLENBQWlCbUssb0JBQWpCLEVBQXVDNXFCLFFBQXZDLENBQWdELFFBQWhEO0FBQ0gsYUFGRDs7QUFJQTZxQixxQkFBU25rQixFQUFULENBQVksT0FBWixFQUFxQixZQUFZO0FBQzdCbkgsa0JBQUUsSUFBRixFQUFRbU0sT0FBUixDQUFnQmtmLG9CQUFoQixFQUFzQ3ZxQixXQUF0QyxDQUFrRCxRQUFsRDtBQUNILGFBRkQ7QUFHSDtBQUNKLEtBdkNEO0FBd0NIOztBQUVELFNBQVN5cUIsWUFBVCxHQUF3QjtBQUNwQixRQUFJN0csV0FBVzFrQixFQUFFLFdBQUYsQ0FBZjtBQUNBLFFBQUk4cUIsV0FBVzlxQixFQUFFLGVBQUYsQ0FBZjs7QUFFQTBrQixhQUFTNUksS0FBVCxDQUFlLFlBQVk7QUFDdkJnUCxpQkFBUzFOLE9BQVQ7QUFDQXBkLFVBQUUsV0FBRixFQUFlYSxNQUFmO0FBQ0FiLFVBQUUsb0JBQUYsRUFBd0JhLE1BQXhCO0FBQ05iLFVBQUUsTUFBRixFQUFVYyxXQUFWLENBQXNCLGlCQUF0QjtBQUVBLEtBTkU7QUFPSDs7QUFFRDtBQUNBO0FBQ0E7O1FBRVEycEIsYSxHQUFBQSxhO1FBQWU5TixXLEdBQUFBLFc7UUFBYXNPLGUsR0FBQUEsZTtRQUFpQk0sWSxHQUFBQSxZOzs7Ozs7O0FDdkhyRDs7QUFFQTtBQUNBO0FBQ0E7Ozs7OztBQUVBOzs7O0FBQ0E7Ozs7QUFFQTtBQUNBO0FBQ0E7O0FBRUEsU0FBU0MsVUFBVCxHQUEyQztBQUFBLEtBQXRCbEwsUUFBc0IsdUVBQVh0Z0IsRUFBRSxNQUFGLENBQVc7O0FBQzFDLEtBQUkrYyxVQUFVdUQsU0FBUzljLElBQVQsQ0FBYyxpQkFBZCxDQUFkOztBQUVBLFVBQVNpb0IsUUFBVCxDQUFtQjVpQixLQUFuQixFQUEwQm5KLFVBQTFCLEVBQXNDO0FBQ3JDLE1BQUkrTSxNQUFNNUQsTUFBTS9JLElBQU4sQ0FBVyxLQUFYLENBQVY7QUFDQSxNQUFJa1MsT0FBT25KLE1BQU12RixJQUFOLENBQVcsTUFBWCxLQUFzQnVGLE1BQU0vSSxJQUFOLENBQVcsTUFBWCxDQUFqQztBQUNBLE1BQUlpQixRQUFROEgsTUFBTWtFLEdBQU4sTUFBZWxFLE1BQU0vSSxJQUFOLENBQVcsS0FBWCxDQUEzQjtBQUNBLE1BQUltRCxZQUFZLHdCQUFjdkQsVUFBZCxDQUFoQjs7QUFFQSxNQUFJK00sT0FBT3VGLElBQVgsRUFBaUI7QUFDaEIvTyxhQUFVQyxJQUFWOztBQUVBbEQsS0FBRXdNLElBQUYsQ0FBTztBQUNOQyxTQUFLQSxHQURDO0FBRU5oSixVQUFNLE1BRkE7QUFHTjNELFVBQU07QUFDTGtTLFdBQU1BLElBREQ7QUFFTGpSLFlBQU9BO0FBRkYsS0FIQTtBQU9ObWUsY0FBVSxNQVBKO0FBUU56SyxhQUFTLGlCQUFVMVAsUUFBVixFQUFvQjtBQUM1QixTQUFJQSxTQUFTMFAsT0FBYixFQUFzQjtBQUNyQixVQUFJMVAsU0FBUzhQLEtBQWIsRUFBb0I7QUFDbkI5UCxnQkFBUzhQLEtBQVQsQ0FBZXlDLE9BQWYsQ0FBdUIsVUFBVXJKLElBQVYsRUFBZ0I7QUFDdEMsWUFBSUEsS0FBSzJHLElBQUwsSUFBYTNHLEtBQUsyTSxRQUF0QixFQUFnQztBQUMvQixhQUFJbkUsUUFBUXpXLEVBQUVpTyxLQUFLMkcsSUFBUCxDQUFaO0FBQ0EsYUFBSXlULFlBQVlyb0IsRUFBRWlPLEtBQUsyTSxRQUFQLENBQWhCOztBQUVBLGFBQUl5TixVQUFVem9CLE1BQWQsRUFBc0I7QUFDckIsY0FBSThyQixXQUFXaHNCLFdBQVc4RCxJQUFYLENBQWdCLGdCQUFoQixDQUFmOztBQUVBa29CLG1CQUFTcHJCLElBQVQsQ0FBYyxZQUFZO0FBQ3pCLGVBQUlraEIsTUFBTXhoQixFQUFFLElBQUYsQ0FBVjtBQUNBLGVBQUl3aEIsSUFBSTFoQixJQUFKLENBQVMsU0FBVCxDQUFKLEVBQXlCO0FBQ3hCMGhCLGdCQUFJcFYsT0FBSixDQUFZLFNBQVo7QUFDQTtBQUNELFdBTEQ7O0FBT0FpYyxvQkFBVXpULElBQVYsQ0FBZTZCLEtBQWY7O0FBRUFpVixtQkFBU3ByQixJQUFULENBQWMsWUFBWTtBQUN6QixlQUFJa2hCLE1BQU14aEIsRUFBRSxJQUFGLEVBQVErSSxNQUFSLEVBQVY7QUFDQSxtQ0FBV3lZLEdBQVg7QUFDQSxXQUhEOztBQUtBZ0sscUJBQVcvVSxLQUFYO0FBQ0E7QUFDRDtBQUNELFFBekJEO0FBMEJBO0FBQ0Q7O0FBRUR4VCxlQUFVZ0MsSUFBVjtBQUNBO0FBekNLLElBQVA7QUEyQ0E7QUFDRDs7QUFFRCxLQUFJOFgsUUFBUW5kLE1BQVosRUFBb0I7QUFDbkJtZCxVQUFRemMsSUFBUixDQUFhLFlBQVk7QUFDeEIsT0FBSXVJLFFBQVE3SSxFQUFFLElBQUYsQ0FBWjtBQUNBLE9BQUlOLGFBQWFtSixNQUFNc0QsT0FBTixDQUFjLDJCQUFkLENBQWpCO0FBQ0EsT0FBSXdmLFVBQVUsS0FBS0EsT0FBTCxDQUFhQyxXQUFiLEVBQWQ7O0FBRUEsT0FBSS9pQixNQUFNL0ksSUFBTixDQUFXLFNBQVgsQ0FBSixFQUEyQjtBQUMxQitJLFVBQU0xQixFQUFOLENBQVMsZ0JBQVQsRUFBMkIsVUFBVXlCLENBQVYsRUFBYTtBQUN2QzZpQixjQUFTNWlCLEtBQVQsRUFBZ0JuSixVQUFoQjtBQUNBLEtBRkQ7QUFHQSxJQUpELE1BSU8sSUFBSWlzQixZQUFZLFFBQVosSUFBeUJBLFlBQVksT0FBWixLQUF3QjlpQixNQUFNdkYsSUFBTixDQUFXLE1BQVgsTUFBdUIsT0FBdkIsSUFBa0N1RixNQUFNdkYsSUFBTixDQUFXLE1BQVgsTUFBdUIsVUFBakYsQ0FBN0IsRUFBNEg7QUFDbEl1RixVQUFNMUIsRUFBTixDQUFTLFFBQVQsRUFBbUIsVUFBVXlCLENBQVYsRUFBYTtBQUMvQjZpQixjQUFTNWlCLEtBQVQsRUFBZ0JuSixVQUFoQjtBQUNBLEtBRkQ7QUFHQSxJQUpNLE1BSUEsSUFBSWlzQixZQUFZLFVBQVosSUFBMkJBLFlBQVksT0FBWixJQUF1QjlpQixNQUFNdkYsSUFBTixDQUFXLE1BQVgsTUFBdUIsT0FBOUMsSUFBeUR1RixNQUFNdkYsSUFBTixDQUFXLE1BQVgsTUFBdUIsVUFBL0csRUFBNEg7QUFDbEl1RixVQUFNMUIsRUFBTixDQUFTLE9BQVQsRUFBa0IsVUFBVXlCLENBQVYsRUFBYTtBQUM5QjZpQixjQUFTNWlCLEtBQVQsRUFBZ0JuSixVQUFoQjtBQUNBLEtBRkQ7QUFHQSxJQUpNLE1BSUE7QUFDTm1KLFVBQU0xQixFQUFOLENBQVMsT0FBVCxFQUFrQixVQUFVeUIsQ0FBVixFQUFhO0FBQzlCNmlCLGNBQVM1aUIsS0FBVCxFQUFnQm5KLFVBQWhCO0FBQ0EsS0FGRDtBQUdBO0FBQ0QsR0F0QkQ7QUF1QkE7QUFDRDs7QUFFRDtBQUNBO0FBQ0E7O2tCQUVlOHJCLFU7Ozs7Ozs7QUN0R2Y7O0FBRUE7QUFDQTtBQUNBOzs7Ozs7QUFFQTs7QUFDQTs7Ozs7O0FBRUE7QUFDQTtBQUNBOztBQUVBLElBQUk3ZixpQkFBaUIsY0FBckI7O0FBRUEsU0FBU2tnQixlQUFULEdBQWlEO0FBQUEsS0FBeEJuc0IsVUFBd0IsdUVBQVhNLEVBQUUsTUFBRixDQUFXOztBQUNoRE4sWUFBVzhELElBQVgsQ0FBZ0JtSSxjQUFoQixFQUFnQ3JMLElBQWhDLENBQXFDLFVBQVU0TCxLQUFWLEVBQWlCO0FBQ3JELE1BQUlyRCxRQUFRN0ksRUFBRSxJQUFGLENBQVo7O0FBRUE2SSxRQUFNdUQsT0FBTixDQUFjO0FBQ2JJLFNBQU07QUFDTEMsU0FBSzVELE1BQU0vSSxJQUFOLENBQVcsS0FBWCxDQURBO0FBRUxBLFVBQU0sY0FBVTRNLE1BQVYsRUFBa0I7QUFDdkIsU0FBSUMsUUFBUTtBQUNYQyxjQUFRRixPQUFPRyxJQURKO0FBRVhDLGVBQVNqRSxNQUFNL0ksSUFBTixDQUFXLE1BQVg7QUFGRSxNQUFaOztBQUtBLFlBQU82TSxLQUFQO0FBQ0E7QUFUSTtBQURPLEdBQWQ7QUFhQSxFQWhCRDtBQWlCQTs7QUFFRDtBQUNBO0FBQ0E7O2tCQUVla2YsZTs7Ozs7OztBQ3ZDZjs7QUFFQTtBQUNBO0FBQ0E7Ozs7OztBQUVBOzs7Ozs7QUFFQTtBQUNBO0FBQ0E7O0FBRUEsU0FBU0MsV0FBVCxHQUF3QjtBQUN2QnByQixRQUFPcXJCLFFBQVAsR0FBa0IsVUFBVWxhLE9BQVYsRUFBbUJwTyxJQUFuQixFQUF5QnVvQixJQUF6QixFQUErQjtBQUNoRCxNQUFNQyxPQUFPLHlCQUFTO0FBQ3JCeG9CLFNBQU1BLFNBQVMsU0FBVCxHQUFxQixNQUFyQixHQUE4QixPQURmO0FBRXJCa0MsU0FBTWtNLE9BRmU7QUFHckJxYSxZQUFTRjtBQUhZLEdBQVQsQ0FBYjtBQUtBQyxPQUFLL29CLElBQUw7QUFDQSxFQVBEO0FBUUE7O0FBRUQ7QUFDQTtBQUNBOztrQkFFZTRvQixXOzs7Ozs7O0FDM0JmOztBQUVBOzs7OztBQUtBO0FBQ0E7QUFDQTs7Ozs7O0FBRUE7Ozs7QUFDQTs7OztBQUVBO0FBQ0E7QUFDQTs7QUFFQSxlQUFLSyxnQkFBTCxDQUFzQjtBQUNyQkMsUUFBTyxNQURjO0FBRXJCQyxTQUFRLGFBRmE7QUFHckJILFVBQVMsSUFIWTtBQUlyQkksY0FBYSxJQUpRO0FBS3JCQyxZQUFXLENBQUMsT0FBRDtBQUxVLENBQXRCOztBQVFBO0FBQ0E7QUFDQTs7Ozs7Ozs7O0FDNUJBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsZUFBZSxZQUFZLG9EQUFvRDtBQUMvRTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBLGdDQUFnQyxVQUFVLEVBQUU7QUFDNUMsQzs7Ozs7O0FDekJBO0FBQ0E7OztBQUdBO0FBQ0EsMFRBQTJULG9CQUFvQixjQUFjLGVBQWUscUJBQXFCLHlDQUF5Qyx3Q0FBd0MsZ0NBQWdDLGlEQUFpRCxvQkFBb0IsNEJBQTRCLG1CQUFtQixFQUFFLHVCQUF1QixXQUFXLGFBQWEsZUFBZSxFQUFFLDJCQUEyQixjQUFjLGVBQWUsaUJBQWlCLEVBQUUsNkJBQTZCLFlBQVksY0FBYyxpQkFBaUIsc0VBQXNFLEVBQUUsNEJBQTRCLGNBQWMsZ0JBQWdCLGlCQUFpQixFQUFFLDBCQUEwQixjQUFjLGFBQWEsZUFBZSxFQUFFLDhCQUE4QixpQkFBaUIsZUFBZSxpQkFBaUIsRUFBRSxnQ0FBZ0MsZUFBZSxjQUFjLGlCQUFpQixzRUFBc0UsRUFBRSwrQkFBK0IsaUJBQWlCLGdCQUFnQixpQkFBaUIsRUFBRSwwQkFBMEIsYUFBYSxjQUFjLGlCQUFpQix5RkFBeUYsRUFBRSw4QkFBOEIsYUFBYSxlQUFlLGlCQUFpQix5RUFBeUUsRUFBRSwrQkFBK0IsYUFBYSxnQkFBZ0IsaUJBQWlCLHlFQUF5RSxFQUFFLHVCQUF1QixrQkFBa0IsRUFBRSw4REFBOEQsbUJBQW1CLHVCQUF1QixZQUFZLGNBQWMsZ0JBQWdCLGdCQUFnQiw4QkFBOEIsaUJBQWlCLDhCQUE4QixFQUFFLGVBQWUsd0NBQXdDLDJDQUEyQyxpREFBaUQscUJBQXFCLEVBQUUsd0JBQXdCLGVBQWUsOEJBQThCLHdFQUF3RSxrQ0FBa0MsRUFBRSx5QkFBeUIseUVBQXlFLGtDQUFrQyxFQUFFLDhCQUE4Qiw4Q0FBOEMsRUFBRSw0QkFBNEIsb0JBQW9CLEVBQUUsd0JBQXdCLHVCQUF1QixhQUFhLGVBQWUsc0JBQXNCLGdCQUFnQixpQkFBaUIsdUJBQXVCLHNCQUFzQiwwQ0FBMEMsdUJBQXVCLG9CQUFvQixpQ0FBaUMsRUFBRSw4QkFBOEIseUNBQXlDLEVBQUUsaUJBQWlCLG9CQUFvQixnQkFBZ0IsaUJBQWlCLDJCQUEyQixtQkFBbUIsZ0JBQWdCLFlBQVksV0FBVyxFQUFFLGlDQUFpQyxlQUFlLDBDQUEwQyxFQUFFLGtDQUFrQywyQ0FBMkMsa0NBQWtDLEVBQUUsOEJBQThCLFVBQVUsa0JBQWtCLEVBQUUsRUFBRSwrQkFBK0IsVUFBVSxpQkFBaUIsRUFBRSxFQUFFLDZCQUE2QixVQUFVLDhCQUE4QixpQkFBaUIsRUFBRSxFQUFFLDhCQUE4QixVQUFVLGdDQUFnQyxpQkFBaUIsRUFBRSxFQUFFLGlDQUFpQyxVQUFVLGdCQUFnQixFQUFFLEVBQUUsZ0NBQWdDLGtCQUFrQixxQkFBcUIsdUJBQXVCLHVCQUF1QixFQUFFLDJDQUEyQyxvQkFBb0Isc0JBQXNCLEVBQUUsOENBQThDLG9CQUFvQixFQUFFLG9GQUFvRiwyQkFBMkIscUNBQXFDLG1CQUFtQixFQUFFLDBDQUEwQyw4QkFBOEIscUNBQXFDLGdCQUFnQixFQUFFLHdDQUF3Qyw4QkFBOEIscUNBQXFDLGdCQUFnQixFQUFFLGtGQUFrRiw4QkFBOEIscUNBQXFDLGdCQUFnQixFQUFFLDBDQUEwQyw4QkFBOEIscUNBQXFDLGdCQUFnQixFQUFFLGlGQUFpRix3QkFBd0IsZ0JBQWdCLHdCQUF3QixFQUFFLCtKQUErSixnQ0FBZ0MsbUJBQW1CLEVBQUUsd0NBQXdDLHdCQUF3QixnQkFBZ0Isd0JBQXdCLEVBQUUsK0VBQStFLGdDQUFnQyxtQkFBbUIsRUFBRTs7QUFFMXhLOzs7Ozs7OztBQ1BBOztBQUVBO0FBQ0E7QUFDQTs7Ozs7O0FBRUE7Ozs7OztBQUVBO0FBQ0E7QUFDQTs7QUFFQSxTQUFTQyxZQUFULEdBQXlCO0FBQ3hCeHNCLEdBQUUsY0FBRixFQUFrQm1ILEVBQWxCLENBQXFCLE9BQXJCLEVBQThCLFlBQVk7QUFDekMsTUFBSXNsQixXQUFXenNCLEVBQUUsSUFBRixFQUFRRixJQUFSLENBQWEsVUFBYixDQUFmO0FBQ0EsdUJBQVc2QyxHQUFYLENBQWUsVUFBZixFQUEyQjhwQixRQUEzQixFQUFxQztBQUNwQ3hpQixZQUFTLE9BQU8sRUFBUCxHQUFZLEVBRGU7QUFFcEN5aUIsU0FBTTtBQUY4QixHQUFyQztBQUlBaHNCLFNBQU8wRSxRQUFQLENBQWdCcVMsSUFBaEIsR0FBdUIvVyxPQUFPMEUsUUFBUCxDQUFnQnFTLElBQXZDO0FBQ0EsRUFQRDtBQVFBOztBQUVEO0FBQ0E7QUFDQTs7a0JBRWUrVSxZOzs7Ozs7O0FDM0JmOztBQUVBO0FBQ0E7QUFDQTs7Ozs7O0FBRUE7Ozs7OztBQUVBO0FBQ0E7QUFDQTs7QUFFQSxTQUFTRyxZQUFULEdBQXlCO0FBQ3hCM3NCLEdBQUUsZUFBRixFQUFtQm1ILEVBQW5CLENBQXNCLE9BQXRCLEVBQStCLFlBQVk7QUFDMUMsTUFBSXlsQixXQUFXNXNCLEVBQUUsSUFBRixFQUFRRixJQUFSLENBQWEsV0FBYixDQUFmO0FBQ0EsdUJBQVc2QyxHQUFYLENBQWUsV0FBZixFQUE0QmlxQixRQUE1QixFQUFzQztBQUNyQzNpQixZQUFTLE9BQU8sRUFBUCxHQUFZLEVBRGdCO0FBRXJDeWlCLFNBQU07QUFGK0IsR0FBdEM7QUFJQWhzQixTQUFPMEUsUUFBUCxDQUFnQnFTLElBQWhCLEdBQXVCL1csT0FBTzBFLFFBQVAsQ0FBZ0JxUyxJQUF2QztBQUNBLEVBUEQ7QUFRQTs7QUFFRDtBQUNBO0FBQ0E7O2tCQUVla1YsWTs7Ozs7OztBQzNCZjs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOzs7OztBQUVBLFNBQVNFLFNBQVQsR0FBc0I7QUFDckIsS0FBSWpoQixPQUFPNUwsRUFBRSxpQkFBRixDQUFYO0FBQ0EsS0FBSThzQixTQUFTbGhCLEtBQUtwSSxJQUFMLENBQVUsT0FBVixDQUFiO0FBQ0EsS0FBSWdMLFFBQVF4TyxFQUFFLGlCQUFGLENBQVo7QUFDQSxLQUFJK3NCLE9BQU8vc0IsRUFBRSx1QkFBRixDQUFYO0FBQ0EsS0FBSWd0QixVQUFVaHRCLEVBQUUsbUJBQUYsQ0FBZDs7QUFFQSxLQUFHNEwsS0FBS2hNLE1BQVIsRUFBZ0I7QUFDZmt0QixTQUFPbmtCLEdBQVAsQ0FBVzZGLEtBQVgsRUFBa0JySCxFQUFsQixDQUFxQixRQUFyQixFQUErQixZQUFNO0FBQ3BDNGxCLFFBQUsxTSxPQUFMLENBQWEsR0FBYjtBQUNBLEdBRkQ7QUFHQTdSLFFBQU1ySCxFQUFOLENBQVMsUUFBVCxFQUFtQixZQUFNO0FBQ3hCLE9BQUdxSCxNQUFNd1csRUFBTixDQUFTLFVBQVQsQ0FBSCxFQUF5QjtBQUN4QitILFNBQUtqTixTQUFMLENBQWUsR0FBZjtBQUNBO0FBQ0QsR0FKRDs7QUFNQWtOLFVBQVE3bEIsRUFBUixDQUFXLE9BQVgsRUFBb0IsWUFBVztBQUM5Qm5ILEtBQUUsOEJBQUYsRUFBa0NjLFdBQWxDLENBQThDLFlBQTlDO0FBQ0FkLEtBQUUsSUFBRixFQUFRUyxRQUFSLENBQWlCLFlBQWpCO0FBQ0EsR0FIRDtBQUlBO0FBQ0Q7O0FBRUQ7QUFDQTtBQUNBOztrQkFFZW9zQixTOzs7Ozs7O0FDdENmOztBQUVBO0FBQ0E7QUFDQTs7Ozs7a0JBZXdCSSxhOztBQWJ4Qjs7QUFDQTs7OztBQUNBOzs7Ozs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7O0FBS2UsU0FBU0EsYUFBVCxHQUF5QjtBQUN2Q2p0QixHQUFFLGFBQUYsRUFBaUJtSCxFQUFqQixDQUFvQixPQUFwQixFQUE2QixlQUE3QixFQUE4QyxZQUFXO0FBQ3hELE1BQUltVixTQUFTdGMsRUFBRSxJQUFGLEVBQVE0YyxNQUFSLEdBQWlCL1YsR0FBOUI7QUFDQTdHLElBQUUySyxNQUFGLENBQVMsYUFBVCxFQUF3QjJSLE1BQXhCOztBQUVBLE1BQUk2SCxZQUFZbmtCLEVBQUUsMEJBQUYsRUFBOEIwRixLQUE5QixHQUFzQ3BDLElBQXRDLENBQTJDLFdBQTNDLENBQWhCO0FBQ0EsTUFBSThnQixhQUFhcGtCLEVBQUUsMEJBQUYsRUFBOEJxa0IsSUFBOUIsR0FBcUMvZ0IsSUFBckMsQ0FBMEMsV0FBMUMsQ0FBakI7O0FBRUF0RCxJQUFFMkssTUFBRixDQUFTLGtCQUFULEVBQTZCd1osU0FBN0I7QUFDQW5rQixJQUFFMkssTUFBRixDQUFTLG1CQUFULEVBQThCLEVBQUV5WixVQUFoQztBQUNBLEVBVEQ7O0FBV0EsS0FBSXBrQixFQUFFMkssTUFBRixDQUFTLG1CQUFULElBQWdDLENBQXBDLEVBQXVDOztBQUV0QyxNQUFJOUIsUUFBUTdJLEVBQUUsZUFBRixDQUFaO0FBQ0EsTUFBSWdrQixnQkFBZ0Joa0IsRUFBRSxlQUFGLENBQXBCO0FBQ0EsTUFBSWlELFlBQVksd0JBQWMrZ0IsYUFBZCxDQUFoQjs7QUFFQSxNQUFJQyxPQUFPamtCLEVBQUUySyxNQUFGLENBQVMsbUJBQVQsQ0FBWDtBQUNBLE1BQUkrQixTQUFTN0QsTUFBTS9JLElBQU4sQ0FBVyxRQUFYLENBQWI7O0FBRUE0TSxTQUFPeVgsU0FBUCxHQUFvQm5rQixFQUFFMkssTUFBRixDQUFTLGtCQUFULENBQXBCLENBQWlEO0FBQ2pEK0IsU0FBT3dnQixPQUFQLEdBQWlCLEVBQUVqSixJQUFuQjs7QUFFQSxNQUFJeFgsTUFBTTVELE1BQU0vSSxJQUFOLENBQVcsa0JBQVgsQ0FBVjs7QUFFQUUsSUFBRXdNLElBQUYsQ0FBTztBQUNOL0ksU0FBTSxNQURBO0FBRU5nSixRQUFLQSxHQUZDO0FBR04zTSxTQUFNO0FBQ0xta0IsVUFBTUEsSUFERDtBQUVMdlgsWUFBUUE7QUFGSCxJQUhBO0FBT053UyxhQUFVLE1BUEo7QUFRTnpLLFlBQVMsaUJBQVUxUCxRQUFWLEVBQW9CO0FBQzVCLFFBQUlBLFNBQVMwUCxPQUFiLEVBQXNCO0FBQ3JCNUwsV0FBTS9JLElBQU4sQ0FBVyxNQUFYLEVBQW1CaUYsU0FBU2tmLElBQTVCOztBQUVBRCxtQkFBYzNnQixNQUFkLENBQXFCMEIsU0FBUzZQLElBQTlCOztBQUVBLFNBQUk3UCxTQUFTdWYsVUFBYixFQUF5QjtBQUN4QnRrQixRQUFFLDBCQUFGLEVBQThCNFUsSUFBOUIsQ0FBbUM3UCxTQUFTdWYsVUFBNUM7QUFDQTs7QUFFRCxTQUFJdmYsU0FBU3NmLElBQWIsRUFBbUI7QUFDbEJ4YixZQUFNNUQsSUFBTjtBQUNBOztBQUVEdEUsZ0JBQVcsWUFBWTtBQUN0QlgsUUFBRSxZQUFGLEVBQWdCNmtCLE9BQWhCLENBQXdCLEVBQUNySSxXQUFXeGMsRUFBRTJLLE1BQUYsQ0FBUyxhQUFULENBQVosRUFBeEIsRUFBNkQsSUFBN0Q7QUFDQXdpQjtBQUNBLE1BSEQsRUFHRyxHQUhIOztBQUtBbHFCLGVBQVVnQyxJQUFWO0FBQ0E7O0FBRUQsMkJBQWFxSixJQUFiLENBQWtCdE8sRUFBRTBLLFFBQUYsQ0FBbEI7QUFDQTtBQS9CSyxHQUFQO0FBaUNBOztBQUlEMUssR0FBRSxNQUFGLEVBQVVtSCxFQUFWLENBQWEsT0FBYixFQUFzQixtQkFBdEIsRUFBMkMsWUFBVztBQUNyRGdtQjtBQUNBLEVBRkQ7O0FBSUEsVUFBU0EsWUFBVCxHQUF5QjtBQUN4Qm50QixJQUFFMkssTUFBRixDQUFTLGtCQUFULEVBQTZCLElBQTdCO0FBQ0EzSyxJQUFFMkssTUFBRixDQUFTLG1CQUFULEVBQThCLElBQTlCO0FBQ0EzSyxJQUFFMkssTUFBRixDQUFTLGFBQVQsRUFBd0IsSUFBeEI7QUFDQTtBQUVELEU7Ozs7Ozs7QUM1RkQ7O0FBRUE7QUFDQTtBQUNBOzs7QUFJQTtBQUNBO0FBQ0E7Ozs7O0FBRUEsU0FBU3lpQixTQUFULEdBQXNCO0FBQ3JCcHRCLEdBQUUsTUFBRixFQUFVbUgsRUFBVixDQUFhLE9BQWIsRUFBc0IsY0FBdEIsRUFBc0MsVUFBVUMsS0FBVixFQUFpQjtBQUN0REEsUUFBTWlOLGNBQU47QUFDQSxNQUFJOE8sS0FBS25qQixFQUFFLElBQUYsRUFBUW1NLE9BQVIsQ0FBZ0IsYUFBaEIsRUFBK0IzSSxJQUEvQixDQUFvQyxxQkFBcEMsRUFBMkQxRCxJQUEzRCxDQUFnRSxTQUFoRSxDQUFUO0FBQ0EsTUFBSXNVLFNBQVVwVSxFQUFFLElBQUYsRUFBUW1NLE9BQVIsQ0FBZ0IsYUFBaEIsRUFBK0JyTSxJQUEvQixDQUFvQyxNQUFwQyxDQUFkOztBQUVBRSxJQUFFd00sSUFBRixDQUFPO0FBQ05DLFFBQUsySCxNQURDO0FBRU4zUSxTQUFNLE1BRkE7QUFHTnliLGFBQVUsTUFISjtBQUlOcGYsU0FBTTtBQUNMcWpCLFFBQUdBO0FBREUsSUFKQTtBQU9OMU8sWUFBUyxpQkFBVTNVLElBQVYsRUFBZ0I7QUFDeEIsUUFBSUEsS0FBSzJVLE9BQVQsRUFBa0I7QUFDakIsU0FBSTNVLEtBQUtpRixRQUFULEVBQW1CO0FBQ2xCZ25CLGVBQVNqc0IsS0FBS2lGLFFBQWQsRUFBd0IsU0FBeEI7QUFDQTtBQUNELFNBQUlqRixLQUFLMFgsTUFBVCxFQUFpQjtBQUNoQjlXLGFBQU8wRSxRQUFQLENBQWdCb1MsTUFBaEI7QUFDQSxNQUZELE1BRU87QUFDTnZVO0FBQ0E7QUFDRCxLQVRELE1BU087QUFDTixTQUFJbkQsS0FBS2lGLFFBQVQsRUFBbUI7QUFDbEJnbkIsZUFBU2pzQixLQUFLaUYsUUFBZCxFQUF3QixTQUF4QjtBQUNBO0FBQ0Q5QjtBQUNBO0FBQ0Q7QUF2QkssR0FBUDtBQXlCQSxFQTlCRDtBQStCQTs7QUFFRDtBQUNBO0FBQ0E7O1FBRVFtcUIsUyxHQUFBQSxTOzs7Ozs7O0FDbERSOztBQUVBO0FBQ0E7Ozs7O0FBRUEsU0FBU0MsaUJBQVQsR0FBOEI7QUFDNUJydEIsR0FBRSxNQUFGLEVBQVVtSCxFQUFWLENBQWEsT0FBYixFQUFzQix5Q0FBdEIsRUFBaUUsWUFBVztBQUMzRSxNQUFJbW1CLFVBQVV0dEIsRUFBRSxJQUFGLEVBQVEyRixJQUFSLEVBQWQ7QUFDQTNGLElBQUUseUNBQUYsRUFBNkNjLFdBQTdDLENBQXlELFdBQXpEOztBQUVBZCxJQUFFLE1BQUYsRUFBVWlGLElBQVY7QUFDQWpGLElBQUUsSUFBRixFQUFRUyxRQUFSLENBQWlCLFdBQWpCOztBQUVBVCxJQUFFLFVBQVFzdEIsT0FBVixFQUFtQnBxQixJQUFuQjtBQUNBLEVBUkQ7QUFTRDs7QUFFRDtBQUNBO0FBQ0E7O1FBRVFtcUIsaUIsR0FBQUEsaUI7Ozs7Ozs7QUNyQlI7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7Ozs7O0FBRUEsU0FBU0UsT0FBVCxHQUFvQjtBQUNuQixLQUFJdnRCLEVBQUUsV0FBRixFQUFlSixNQUFuQixFQUEyQjtBQUMxQmUsYUFBVyxZQUFXO0FBQ3JCWCxLQUFFLFdBQUYsRUFBZTBOLE9BQWYsQ0FBdUIsT0FBdkI7QUFDQSxHQUZELEVBRUcsSUFGSDtBQUdBOztBQUVEMU4sR0FBRSxNQUFGLEVBQVVtSCxFQUFWLENBQWEsT0FBYixFQUFzQixpQkFBdEIsRUFBeUMsWUFBWTtBQUNwRG5ILElBQUUsMEJBQUYsRUFBOEIwTixPQUE5QixDQUFzQyxPQUF0QztBQUNBLEVBRkQ7QUFHQTs7QUFFRDtBQUNBO0FBQ0E7O1FBRVE2ZixPLEdBQUFBLE87Ozs7Ozs7QUM5QlI7O0FBRUE7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7Ozs7O0FBRUEsU0FBU0Msb0JBQVQsR0FBdUQ7QUFBQSxLQUF4Qjl0QixVQUF3Qix1RUFBWE0sRUFBRSxNQUFGLENBQVc7O0FBQ3RETixZQUFXOEQsSUFBWCxDQUFnQixhQUFoQixFQUErQmxELElBQS9CLENBQW9DLFlBQVksQ0FFL0MsQ0FGRDs7QUFJQSxLQUFJbU8sT0FBT3pPLEVBQUUsb0JBQUYsQ0FBWDtBQUFBLEtBQ0MyTyxNQUFNM08sRUFBRSxtQkFBRixDQURQO0FBQUEsS0FFQzRPLE9BQU81TyxFQUFFLG9CQUFGLENBRlI7QUFBQSxLQUdDeXRCLFFBQVF6dEIsRUFBRSwwQkFBRixDQUhUOztBQUtBLFVBQVN1TyxRQUFULENBQWtCQyxLQUFsQixFQUF5QjtBQUN4QkMsT0FBS0MsSUFBTCxDQUFVLFVBQVYsRUFBc0IsS0FBdEI7O0FBRUEsTUFBSSxDQUFDRCxLQUFLQyxJQUFMLENBQVUsU0FBVixDQUFELElBQXlCLENBQUNDLElBQUlELElBQUosQ0FBUyxTQUFULENBQTFCLElBQWlELENBQUNFLEtBQUtGLElBQUwsQ0FBVSxTQUFWLENBQXRELEVBQTJFO0FBQzFFRCxRQUFLQyxJQUFMLENBQVUsVUFBVixFQUFzQixJQUF0QjtBQUNBO0FBQ0Q7O0FBRURELE1BQUt0SCxFQUFMLENBQVEsT0FBUixFQUFpQixZQUFZO0FBQzVCb0g7QUFDQUksTUFBSUQsSUFBSixDQUFTLFNBQVQsRUFBb0IsS0FBcEI7QUFDQUUsT0FBS0YsSUFBTCxDQUFVLFNBQVYsRUFBcUIsS0FBckI7QUFDQSxFQUpEOztBQU1BQyxLQUFJeEgsRUFBSixDQUFPLE9BQVAsRUFBZ0IsWUFBWTtBQUMzQm9IO0FBQ0FFLE9BQUtDLElBQUwsQ0FBVSxTQUFWLEVBQXFCLEtBQXJCO0FBQ0EsRUFIRDs7QUFLQUUsTUFBS3pILEVBQUwsQ0FBUSxPQUFSLEVBQWlCLFlBQVk7QUFDNUJvSDtBQUNBRSxPQUFLQyxJQUFMLENBQVUsU0FBVixFQUFxQixLQUFyQjtBQUNBLEVBSEQ7O0FBS0ErZSxPQUFNdG1CLEVBQU4sQ0FBUyxPQUFULEVBQWtCLFlBQVk7QUFDN0JzSCxPQUFLQyxJQUFMLENBQVUsU0FBVixFQUFxQixLQUFyQjtBQUNBQyxNQUFJRCxJQUFKLENBQVMsU0FBVCxFQUFvQixLQUFwQjtBQUNBRSxPQUFLRixJQUFMLENBQVUsU0FBVixFQUFxQixLQUFyQjs7QUFFQSxNQUFHMU8sRUFBRSxJQUFGLEVBQVEwTyxJQUFSLENBQWEsU0FBYixDQUFILEVBQTRCO0FBQzNCMU8sS0FBRSxrQkFBRixFQUFzQndELElBQXRCLENBQTJCLDRCQUEzQixFQUF5RHlCLElBQXpEO0FBQ0EsR0FGRCxNQUVPO0FBQ05qRixLQUFFLGtCQUFGLEVBQXNCd0QsSUFBdEIsQ0FBMkIsNEJBQTNCLEVBQXlETixJQUF6RDtBQUNBO0FBQ0QsRUFWRDs7QUFZQSxLQUFJdXFCLE1BQU0vZSxJQUFOLENBQVcsU0FBWCxDQUFKLEVBQTJCO0FBQzFCRCxPQUFLQyxJQUFMLENBQVUsU0FBVixFQUFxQixLQUFyQjtBQUNBQyxNQUFJRCxJQUFKLENBQVMsU0FBVCxFQUFvQixLQUFwQjtBQUNBRSxPQUFLRixJQUFMLENBQVUsU0FBVixFQUFxQixLQUFyQjtBQUNBMU8sSUFBRSxrQkFBRixFQUFzQndELElBQXRCLENBQTJCLDRCQUEzQixFQUF5RHlCLElBQXpEO0FBQ0E7QUFDRDs7QUFFRDtBQUNBO0FBQ0E7O1FBRVF1b0Isb0IsR0FBQUEsb0I7Ozs7Ozs7QUNyRVI7O0FBRUE7QUFDQTtBQUNBOzs7Ozs7O0FBRUE7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7OztBQUVBO0FBQ0E7QUFDQTs7QUFFQSxTQUFTRSxXQUFULEdBQXVCO0FBQ3RCLE1BQUkxdEIsRUFBRSxxQkFBRixFQUF5QkosTUFBN0IsRUFBcUM7O0FBRXBDO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNEOztBQUVEO0FBQ0E7QUFDQTs7UUFFUTh0QixXLEdBQUFBLFc7Ozs7Ozs7QUM1RFI7O0FBRUE7Ozs7O0FBS0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7O0FBTUE7QUFDQTtBQUNBOztBQUVBOzs7Ozs7O0FBR0EsU0FBU0MsV0FBVCxHQUF3QjtBQUN2QmxkLFFBQU9ILEVBQVAsQ0FBVTRGLE9BQVYsR0FBb0IsR0FBR0EsT0FBdkI7O0FBRUFsVyxHQUFFLE1BQUYsRUFBVW1ILEVBQVYsQ0FBYSxPQUFiLEVBQXNCLGdCQUF0QixFQUF3QyxZQUFZO0FBQ25ELE1BQUkwQixRQUFRN0ksRUFBRSxJQUFGLENBQVo7QUFBQSxNQUNDNHRCLFlBQVkva0IsTUFBTW1jLEVBQU4sQ0FBUyxVQUFULENBRGI7QUFBQSxNQUVDdlksTUFBTXpNLEVBQUUsOEJBQUYsRUFBa0NGLElBQWxDLENBQXVDLE9BQXZDLENBRlA7QUFBQSxNQUdDK3RCLGVBQWUsRUFIaEI7QUFBQSxNQUlDQyxPQUFPOXRCLEVBQUUsT0FBRixFQUFXK00sR0FBWCxFQUpSO0FBQUEsTUFLQ2doQixlQUFlL3RCLEVBQUUsNkJBQUYsRUFBaUMwTyxJQUFqQyxDQUFzQyxTQUF0QyxDQUxoQjtBQUFBLE1BTUNzZixTQUFTaHVCLEVBQUUsU0FBRixFQUFhK00sR0FBYixFQU5WOztBQVFBbEUsUUFBTUUsTUFBTixHQUFlb0UsSUFBZixHQUFzQjNKLElBQXRCLENBQTJCLGdCQUEzQixFQUE2Q2tMLElBQTdDLENBQWtELFNBQWxELEVBQTZEa2YsU0FBN0Q7O0FBRUEva0IsUUFBTXlWLE9BQU4sQ0FBYyxJQUFkLEVBQW9CMlAsSUFBcEIsQ0FBeUIsR0FBekIsRUFBOEJ6cUIsSUFBOUIsQ0FBbUMsZ0JBQW5DLEVBQXFEMFMsT0FBckQsR0FBK0Q1VixJQUEvRCxDQUFvRSxVQUFVNHRCLENBQVYsRUFBYUMsQ0FBYixFQUFnQjtBQUNuRm51QixLQUFFbXVCLENBQUYsRUFBS3pmLElBQUwsQ0FBVSxTQUFWLEVBQXFCLFlBQVk7QUFDaEMsV0FBTzFPLEVBQUVtdUIsQ0FBRixFQUFLcGxCLE1BQUwsQ0FBWSxHQUFaLEVBQWlCb0UsSUFBakIsQ0FBc0IsSUFBdEIsRUFBNEIzSixJQUE1QixDQUFpQyxVQUFqQyxFQUE2QzVELE1BQXBEO0FBQ0EsSUFGRDtBQUdBLEdBSkQ7O0FBTUE7QUFDQSxNQUFJaUosTUFBTW1jLEVBQU4sQ0FBUyxVQUFULENBQUosRUFBMEI7QUFDekJuYyxTQUFNc0QsT0FBTixDQUFjLElBQWQsRUFBb0IzSSxJQUFwQixDQUF5QixrQkFBekIsRUFBNkNsRCxJQUE3QyxDQUFrRCxZQUFZO0FBQzdEdXRCLGlCQUFhNVgsSUFBYixDQUFrQmpXLEVBQUUsSUFBRixFQUFRRixJQUFSLENBQWEsSUFBYixDQUFsQjtBQUNBLElBRkQ7O0FBSUFFLEtBQUV3TSxJQUFGLENBQU87QUFDTkMsU0FBS0EsR0FEQztBQUVOaEosVUFBTSxNQUZBO0FBR04zRCxVQUFNO0FBQ0wrdEIsbUJBQWNBLFlBRFQ7QUFFTEMsV0FBTUEsSUFGRDtBQUdMQyxtQkFBY0EsWUFIVDtBQUlMQyxhQUFRQSxNQUpIO0FBS0xJLHdCQUFtQjtBQUxkLEtBSEE7QUFVTmxQLGNBQVU7QUFWSixJQUFQO0FBWUEsR0FqQkQsTUFpQk87QUFDTnJXLFNBQU1zRCxPQUFOLENBQWMsSUFBZCxFQUFvQjNJLElBQXBCLENBQXlCLGtCQUF6QixFQUE2Q2xELElBQTdDLENBQWtELFlBQVk7QUFDN0R1dEIsaUJBQWE1WCxJQUFiLENBQWtCalcsRUFBRSxJQUFGLEVBQVFGLElBQVIsQ0FBYSxJQUFiLENBQWxCO0FBQ0EsSUFGRDs7QUFJQUUsS0FBRXdNLElBQUYsQ0FBTztBQUNOQyxTQUFLQSxHQURDO0FBRU5oSixVQUFNLE1BRkE7QUFHTjNELFVBQU07QUFDTCt0QixtQkFBY0EsWUFEVDtBQUVMTyx3QkFBbUI7QUFGZCxLQUhBO0FBT05sUCxjQUFVO0FBUEosSUFBUDtBQVNBO0FBQ0QsRUFsREQ7O0FBb0RBO0FBQ0FsZixHQUFFLE1BQUYsRUFBVW1ILEVBQVYsQ0FBYSxPQUFiLEVBQXNCLHNCQUF0QixFQUE4QyxZQUFZO0FBQ3pELE1BQUkwQixRQUFRN0ksRUFBRSxJQUFGLENBQVo7QUFBQSxNQUNDeU0sTUFBTXpNLEVBQUUsOEJBQUYsRUFBa0NGLElBQWxDLENBQXVDLE9BQXZDLENBRFA7QUFBQSxNQUVDdXVCLFlBQVksRUFGYjtBQUFBLE1BR0NQLE9BQU85dEIsRUFBRSxPQUFGLEVBQVcrTSxHQUFYLEVBSFI7QUFBQSxNQUlDZ2hCLGVBQWUvdEIsRUFBRSw2QkFBRixFQUFpQzBPLElBQWpDLENBQXNDLFNBQXRDLENBSmhCO0FBQUEsTUFLQ3NmLFNBQVNodUIsRUFBRSxTQUFGLEVBQWErTSxHQUFiLEVBTFY7O0FBT0EsTUFBSWxFLE1BQU1tYyxFQUFOLENBQVMsVUFBVCxDQUFKLEVBQTBCO0FBQ3pCcUosYUFBVXBZLElBQVYsQ0FBZXBOLE1BQU0vSSxJQUFOLENBQVcsSUFBWCxDQUFmOztBQUVBRSxLQUFFd00sSUFBRixDQUFPO0FBQ05DLFNBQUtBLEdBREM7QUFFTmhKLFVBQU0sTUFGQTtBQUdOM0QsVUFBTTtBQUNMZ3VCLFdBQU1BLElBREQ7QUFFTEMsbUJBQWNBLFlBRlQ7QUFHTEMsYUFBUUEsTUFISDtBQUlMSyxnQkFBV0EsU0FKTjtBQUtMQyxxQkFBZ0I7QUFMWCxLQUhBO0FBVU5wUCxjQUFVO0FBVkosSUFBUDtBQVlBLEdBZkQsTUFlTztBQUNObVAsYUFBVXBZLElBQVYsQ0FBZXBOLE1BQU0vSSxJQUFOLENBQVcsSUFBWCxDQUFmOztBQUVBRSxLQUFFd00sSUFBRixDQUFPO0FBQ05DLFNBQUtBLEdBREM7QUFFTmhKLFVBQU0sTUFGQTtBQUdOM0QsVUFBTTtBQUNMdXVCLGdCQUFXQSxTQUROO0FBRUxDLHFCQUFnQjtBQUZYLEtBSEE7QUFPTnBQLGNBQVU7QUFQSixJQUFQO0FBU0E7QUFDRCxFQXBDRDs7QUFzQ0FsZixHQUFFLE1BQUYsRUFBVW1ILEVBQVYsQ0FBYSxPQUFiLEVBQXNCLGdCQUF0QixFQUF3QyxVQUFVeUIsQ0FBVixFQUFhO0FBQ3BELE1BQUlDLFFBQVE3SSxFQUFFLElBQUYsQ0FBWjtBQUFBLE1BQ0N1dUIsUUFBUTFsQixNQUFNc0QsT0FBTixDQUFjLGdCQUFkLEVBQWdDck0sSUFBaEMsQ0FBcUMsT0FBckMsQ0FEVDtBQUFBLE1BRUMwdUIsWUFBWTNsQixNQUFNc0QsT0FBTixDQUFjLGdCQUFkLEVBQWdDM0ksSUFBaEMsQ0FBcUMsVUFBckMsRUFBaUQ1RCxNQUY5RDtBQUFBLE1BR0M2dUIsZ0JBQWdCNWxCLE1BQU1zRCxPQUFOLENBQWMsZ0JBQWQsRUFBZ0MzSSxJQUFoQyxDQUFxQyxrQkFBckMsRUFBeUQ1RCxNQUgxRTs7QUFLQSxNQUFJMnVCLFVBQVUsQ0FBZCxFQUFpQjtBQUNoQixPQUFJMWxCLE1BQU1tYyxFQUFOLENBQVMsVUFBVCxDQUFKLEVBQTBCO0FBQ3pCbmMsVUFBTXZGLElBQU4sQ0FBVyxhQUFYLEVBQTBCLElBQTFCLEVBQWdDNkksT0FBaEMsQ0FBd0MsZ0NBQXhDLEVBQTBFM0ksSUFBMUUsQ0FBK0UsT0FBL0UsRUFBd0ZGLElBQXhGLENBQTZGLGFBQTdGLEVBQTRHLElBQTVHO0FBQ0E7QUFDRCxHQUpELE1BSU8sSUFBSWlyQixVQUFVLENBQWQsRUFBaUI7QUFDdkIsT0FBSUMsY0FBY0MsYUFBbEIsRUFBaUM7QUFDaEM1bEIsVUFBTXNELE9BQU4sQ0FBYyxnQ0FBZCxFQUFnRDNJLElBQWhELENBQXFELGdDQUFyRCxFQUF1RjJKLElBQXZGLEdBQThGM0osSUFBOUYsQ0FBbUcsT0FBbkcsRUFBNEdGLElBQTVHLENBQWlILGFBQWpILEVBQWdJLElBQWhJO0FBQ0EsSUFGRCxNQUVPO0FBQ051RixVQUFNc0QsT0FBTixDQUFjLGdDQUFkLEVBQWdEM0ksSUFBaEQsQ0FBcUQsZ0NBQXJELEVBQXVGMkosSUFBdkYsR0FBOEYzSixJQUE5RixDQUFtRyxPQUFuRyxFQUE0R0YsSUFBNUcsQ0FBaUgsYUFBakgsRUFBZ0ksS0FBaEk7QUFDQTtBQUNELEdBTk0sTUFNQSxJQUFJaXJCLFVBQVUsQ0FBZCxFQUFpQjtBQUN2QixPQUFJQyxjQUFjQyxhQUFsQixFQUFpQztBQUNoQzVsQixVQUFNc0QsT0FBTixDQUFjLGdDQUFkLEVBQWdEOGhCLElBQWhELEdBQXVEenFCLElBQXZELENBQTRELE9BQTVELEVBQXFFRixJQUFyRSxDQUEwRSxhQUExRSxFQUF5RixJQUF6RixFQUErRjZJLE9BQS9GLENBQXVHLGdDQUF2RyxFQUF5STNJLElBQXpJLENBQThJLGdDQUE5SSxFQUFnTDJKLElBQWhMLEdBQXVMM0osSUFBdkwsQ0FBNEwsT0FBNUwsRUFBcU1GLElBQXJNLENBQTBNLGFBQTFNLEVBQXlOLElBQXpOO0FBQ0EsSUFGRCxNQUVPO0FBQ051RixVQUFNc0QsT0FBTixDQUFjLGdDQUFkLEVBQWdEOGhCLElBQWhELEdBQXVEenFCLElBQXZELENBQTRELE9BQTVELEVBQXFFRixJQUFyRSxDQUEwRSxhQUExRSxFQUF5RixLQUF6RixFQUFnRzZJLE9BQWhHLENBQXdHLGdDQUF4RyxFQUEwSTNJLElBQTFJLENBQStJLGdDQUEvSSxFQUFpTDJKLElBQWpMLEdBQXdMM0osSUFBeEwsQ0FBNkwsT0FBN0wsRUFBc01GLElBQXRNLENBQTJNLGFBQTNNLEVBQTBOLEtBQTFOO0FBQ0E7QUFDRCxHQU5NLE1BTUE7QUFDTixPQUFJa3JCLGNBQWNDLGFBQWxCLEVBQWlDO0FBQ2hDNWxCLFVBQU1zRCxPQUFOLENBQWMsZ0JBQWQsRUFBZ0M4aEIsSUFBaEMsR0FBdUN6cUIsSUFBdkMsQ0FBNEMsT0FBNUMsRUFBcURGLElBQXJELENBQTBELGFBQTFELEVBQXlFLElBQXpFLEVBQStFNkksT0FBL0UsQ0FBdUYsZ0NBQXZGLEVBQXlIM0ksSUFBekgsQ0FBOEgsZ0NBQTlILEVBQWdLMkosSUFBaEssR0FBdUszSixJQUF2SyxDQUE0SyxPQUE1SyxFQUFxTEYsSUFBckwsQ0FBMEwsYUFBMUwsRUFBeU0sSUFBek07QUFDQSxJQUZELE1BRU87QUFDTnVGLFVBQU1zRCxPQUFOLENBQWMsZ0JBQWQsRUFBZ0M4aEIsSUFBaEMsR0FBdUN6cUIsSUFBdkMsQ0FBNEMsT0FBNUMsRUFBcURGLElBQXJELENBQTBELGFBQTFELEVBQXlFLEtBQXpFLEVBQWdGNkksT0FBaEYsQ0FBd0YsZ0NBQXhGLEVBQTBIM0ksSUFBMUgsQ0FBK0gsZ0NBQS9ILEVBQWlLMkosSUFBakssR0FBd0szSixJQUF4SyxDQUE2SyxPQUE3SyxFQUFzTEYsSUFBdEwsQ0FBMkwsYUFBM0wsRUFBME0sS0FBMU07QUFDQTtBQUNEO0FBQ0QsRUE3QkQ7QUE4QkE7O0FBRUQ7QUFDQTtBQUNBOztrQkFFZXFxQixXOzs7Ozs7O0FDOUpmOztBQUVBOzs7OztBQUtBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7OztBQU1BO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7OztBQUdBLFNBQVNlLGFBQVQsR0FBMEI7QUFDekIxdUIsR0FBRSxrQkFBRixFQUFzQm1ILEVBQXRCLENBQXlCLE9BQXpCLEVBQWtDLFlBQVk7QUFDN0MsTUFBSWlOLFNBQVNwVSxFQUFFLElBQUYsRUFBUUYsSUFBUixDQUFhLGNBQWIsQ0FBYjtBQUFBLE1BQ0MyTSxNQUFNek0sRUFBRSw4QkFBRixFQUFrQ0YsSUFBbEMsQ0FBdUMsT0FBdkMsQ0FEUDs7QUFHQSxXQUFTNnRCLFdBQVQsQ0FBcUI5b0IsTUFBckIsRUFBNkI7QUFDNUI3RSxLQUFFLDBDQUFGLEVBQThDTSxJQUE5QyxDQUFtRCxZQUFZO0FBQzlETixNQUFFLElBQUYsRUFBUTBPLElBQVIsQ0FBYSxTQUFiLEVBQXdCN0osTUFBeEI7QUFDQSxJQUZEO0FBR0E7O0FBRUQsV0FBUzhwQixjQUFULENBQXdCOXBCLE1BQXhCLEVBQWdDO0FBQy9CN0UsS0FBRSxzQ0FBRixFQUEwQ00sSUFBMUMsQ0FBK0MsWUFBWTtBQUMxRE4sTUFBRSxJQUFGLEVBQVFtTSxPQUFSLENBQWdCLElBQWhCLEVBQXNCckYsR0FBdEIsQ0FBMEIsU0FBMUIsRUFBcUNqQyxNQUFyQztBQUNBLElBRkQ7QUFHQTs7QUFFRCxVQUFRdVAsTUFBUjtBQUNDLFFBQUssS0FBTDtBQUNDdVosZ0JBQVksSUFBWjs7QUFFQSxRQUFJaUIsMkJBQTJCLEVBQS9CO0FBQUEsUUFDQ2QsT0FBTzl0QixFQUFFLE9BQUYsRUFBVytNLEdBQVgsRUFEUjtBQUFBLFFBRUNnaEIsZUFBZS90QixFQUFFLDZCQUFGLEVBQWlDME8sSUFBakMsQ0FBc0MsU0FBdEMsQ0FGaEI7QUFBQSxRQUdDc2YsU0FBU2h1QixFQUFFLFNBQUYsRUFBYStNLEdBQWIsRUFIVjs7QUFLQS9NLE1BQUUsa0JBQUYsRUFBc0JNLElBQXRCLENBQTJCLFlBQVk7QUFDdENzdUIsOEJBQXlCM1ksSUFBekIsQ0FBOEJqVyxFQUFFLElBQUYsRUFBUW1OLElBQVIsR0FBZTNKLElBQWYsQ0FBb0IsT0FBcEIsRUFBNkIxRCxJQUE3QixDQUFrQyxJQUFsQyxDQUE5QjtBQUNBLEtBRkQ7O0FBSUFFLE1BQUV3TSxJQUFGLENBQU87QUFDTkMsVUFBS0EsR0FEQztBQUVOaEosV0FBTSxNQUZBO0FBR04zRCxXQUFNO0FBQ0xndUIsWUFBTUEsSUFERDtBQUVMQyxvQkFBY0EsWUFGVDtBQUdMQyxjQUFRQSxNQUhIO0FBSUxZLGdDQUEwQkE7QUFKckIsTUFIQTtBQVNOMVAsZUFBVTtBQVRKLEtBQVA7O0FBWUE7QUFDRCxRQUFLLE1BQUw7QUFDQ3lPLGdCQUFZLEtBQVo7O0FBRUEzdEIsTUFBRXdNLElBQUYsQ0FBTztBQUNOQyxVQUFLQSxHQURDO0FBRU5oSixXQUFNLE1BRkE7QUFHTjNELFdBQU07QUFDTCt1QixlQUFTO0FBREosTUFIQTtBQU1OM1AsZUFBVTtBQU5KLEtBQVA7O0FBU0E7QUFDRCxRQUFLLFdBQUw7QUFDQ3lQLG1CQUFlLE1BQWY7QUFDQTtBQUNEO0FBQ0NBLG1CQUFlLE9BQWY7QUEzQ0Y7QUE2Q0EsRUE3REQ7QUE4REE7O0FBRUQ7QUFDQTtBQUNBOztrQkFFZUQsYTs7Ozs7OztBQ2pHZjs7QUFFQTs7Ozs7QUFLQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBOzs7Ozs7QUFNQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7Ozs7QUFHQSxTQUFTSSxVQUFULEdBQXVCO0FBQ3RCOXVCLEdBQUUsaUJBQUYsRUFBcUJtSCxFQUFyQixDQUF3QixPQUF4QixFQUFpQyxVQUFVeUIsQ0FBVixFQUFhO0FBQzdDLE1BQUkwYyxPQUFPdGxCLEVBQUUsSUFBRixDQUFYOztBQUVBLE1BQUlzbEIsS0FBS3RlLFFBQUwsQ0FBYyxzQ0FBZCxDQUFKLEVBQTJEO0FBQzFEc2UsUUFBS3RGLFdBQUwsQ0FBaUIsUUFBakIsRUFBMkI3VCxPQUEzQixDQUFtQyxJQUFuQyxFQUF5QzNJLElBQXpDLENBQThDLG1CQUE5QyxFQUFtRS9DLFFBQW5FLENBQTRFLFNBQTVFLEVBQXVGSCxJQUF2RixDQUE0RixZQUFZO0FBQ3ZHTixNQUFFLElBQUYsRUFBUXdELElBQVIsQ0FBYSx3QkFBYixFQUF1QzFDLFdBQXZDLENBQW1ELFFBQW5EO0FBQ0EsSUFGRDtBQUdBLEdBSkQsTUFJTztBQUNOd2tCLFFBQUt0RixXQUFMLENBQWlCLFFBQWpCLEVBQTJCalgsTUFBM0IsR0FBb0NnbUIsUUFBcEMsQ0FBNkMsSUFBN0MsRUFBbUQvTyxXQUFuRCxDQUErRCxTQUEvRDtBQUNBO0FBQ0QsRUFWRDtBQVdBOztBQUVEO0FBQ0E7QUFDQTs7a0JBRWU4TyxVOzs7Ozs7O0FDOUNmOztBQUVBOzs7OztBQUtBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7OztBQU1BO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7OztBQUdBLFNBQVNFLGdCQUFULEdBQTZCO0FBQzVCaHZCLEdBQUUsa0JBQUYsRUFBc0JtSCxFQUF0QixDQUF5QixPQUF6QixFQUFrQyxVQUFVeUIsQ0FBVixFQUFhO0FBQzlDLE1BQUlDLFFBQVE3SSxFQUFFLElBQUYsQ0FBWjtBQUFBLE1BQ0NtakIsS0FBS3RhLE1BQU0vSSxJQUFOLENBQVcsSUFBWCxDQUROO0FBQUEsTUFFQzJNLE1BQU16TSxFQUFFLG1CQUFGLEVBQXVCRixJQUF2QixDQUE0QixLQUE1QixDQUZQO0FBQUEsTUFHQ2d1QixPQUFPOXRCLEVBQUUsT0FBRixFQUFXK00sR0FBWCxFQUhSO0FBQUEsTUFJQ2loQixTQUFTaHVCLEVBQUUsU0FBRixFQUFhK00sR0FBYixFQUpWO0FBQUEsTUFLQ2doQixlQUFlL3RCLEVBQUUsNkJBQUYsRUFBaUMwTyxJQUFqQyxDQUFzQyxTQUF0QyxDQUxoQjtBQUFBLE1BTUN1Z0Isb0JBQW9CLEVBTnJCO0FBQUEsTUFPQ0MsU0FBUyxJQVBWO0FBQUEsTUFRQ2xSLFFBQVEsRUFSVDtBQUFBLE1BU0NtUixZQUFZdG1CLE1BQU1zRCxPQUFOLENBQWMsSUFBZCxFQUFvQjNJLElBQXBCLENBQXlCLHlCQUF6QixFQUFvRDVELE1BVGpFO0FBQUEsTUFVQ3d2QixrQkFBa0J2bUIsTUFBTXNELE9BQU4sQ0FBYyxJQUFkLEVBQW9CM0ksSUFBcEIsQ0FBeUIsaUNBQXpCLEVBQTRENUQsTUFWL0U7O0FBWUEsTUFBSWlKLE1BQU1zRSxJQUFOLEdBQWEzSixJQUFiLENBQWtCLE9BQWxCLEVBQTJCd2hCLEVBQTNCLENBQThCLFVBQTlCLENBQUosRUFBK0M7QUFDOUM7QUFDQWtLLFlBQVMsSUFBVDs7QUFFQSxPQUFJQyxhQUFhQyxlQUFqQixFQUFrQztBQUNqQ0YsYUFBUy9MLEVBQVQ7QUFDQTtBQUNELEdBUEQsTUFPTztBQUNOOztBQUVBK0wsWUFBUyxJQUFUO0FBQ0EsT0FBSXJtQixNQUFNc0QsT0FBTixDQUFjLElBQWQsRUFBb0IzSSxJQUFwQixDQUF5QixtQkFBekIsRUFBOEM1RCxNQUFsRCxFQUEwRDtBQUN6RDtBQUNBLFFBQUl1dkIsYUFBYUMsZUFBakIsRUFBa0M7QUFDakM7QUFDQTtBQUNBLEtBSEQsTUFHTztBQUNOO0FBQ0E7QUFDQTtBQUNELElBVEQsTUFTTztBQUNOO0FBQ0FGLGFBQVMsSUFBVDtBQUNBO0FBQ0Q7O0FBRUQ7O0FBRUEsTUFBSWx2QixFQUFFLElBQUYsRUFBUWdILFFBQVIsQ0FBaUIsUUFBakIsQ0FBSixFQUFnQztBQUMvQjZCLFNBQU1FLE1BQU4sR0FBZXZGLElBQWYsQ0FBb0IsaUNBQXBCLEVBQXVEbEQsSUFBdkQsQ0FBNEQsWUFBWTtBQUN2RTJ1QixzQkFBa0JoWixJQUFsQixDQUF1QmpXLEVBQUUsSUFBRixFQUFRRixJQUFSLENBQWEsSUFBYixDQUF2QjtBQUNBLElBRkQ7O0FBSUFFLEtBQUUseUJBQUYsRUFBNkJNLElBQTdCLENBQWtDLFlBQVk7QUFDN0MsUUFBSTBSLE9BQU9oUyxFQUFFLElBQUYsRUFBUXdELElBQVIsQ0FBYSxPQUFiLEVBQXNCRixJQUF0QixDQUEyQixJQUEzQixDQUFYO0FBQUEsUUFDQ3ZDLFFBQVFmLEVBQUUsSUFBRixFQUFRd0QsSUFBUixDQUFhLE9BQWIsRUFBc0J1SixHQUF0QixFQURUOztBQUdBaVIsVUFBTS9ILElBQU4sQ0FBVyxDQUFDakUsSUFBRCxFQUFPalIsS0FBUCxDQUFYO0FBQ0EsSUFMRDs7QUFPQWYsS0FBRXdNLElBQUYsQ0FBTztBQUNOQyxTQUFLQSxHQURDO0FBRU5oSixVQUFNLE1BRkE7QUFHTjNELFVBQU07QUFDTDBlLGtCQUFhMkUsRUFEUjtBQUVMMkssV0FBTUEsSUFGRDtBQUdMQyxtQkFBY0EsWUFIVDtBQUlMQyxhQUFRQSxNQUpIO0FBS0xrQixhQUFRQSxNQUxIO0FBTUxELHdCQUFtQkEsaUJBTmQ7QUFPTGpSLFlBQU9BO0FBUEYsS0FIQTtBQVlOa0IsY0FBVSxNQVpKO0FBYU56SyxhQUFTLGlCQUFVMVAsUUFBVixFQUFvQjtBQUM1QixTQUFJQSxTQUFTMFAsT0FBYixFQUFzQjtBQUNyQixVQUFJNUwsTUFBTXNELE9BQU4sQ0FBYyxJQUFkLEVBQW9CM0ksSUFBcEIsQ0FBeUIsbUJBQXpCLEVBQThDNUQsTUFBbEQsRUFBMEQ7QUFDekRpSixhQUFNc0QsT0FBTixDQUFjLElBQWQsRUFBb0IzSSxJQUFwQixDQUF5QixtQkFBekIsRUFBOEM2ckIsV0FBOUMsQ0FBMER0cUIsU0FBUzZQLElBQW5FO0FBQ0EsT0FGRCxNQUVPO0FBQ04vTCxhQUFNc0QsT0FBTixDQUFjLElBQWQsRUFBb0I5SSxNQUFwQixDQUEyQjBCLFNBQVM2UCxJQUFwQztBQUNBO0FBQ0Q7QUFDRDtBQXJCSyxJQUFQO0FBdUJBO0FBQ0QsRUE3RUQ7QUE4RUE7O0FBRUQ7QUFDQTtBQUNBOztrQkFFZW9hLGdCOzs7Ozs7O0FDakhmOztBQUVBOzs7OztBQUtBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7OztBQU1BO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7OztBQUdBLFNBQVNNLFdBQVQsR0FBd0I7QUFDdkIsS0FBSXR2QixFQUFFLGlCQUFGLEVBQXFCSixNQUF6QixFQUFpQztBQUNoQyxNQUFJMnZCLGVBQWV2dkIsRUFBRSxTQUFGLENBQW5CO0FBQUEsTUFDQ3d2QixjQUFjeHZCLEVBQUUsZUFBRixDQURmO0FBQUEsTUFFQ3lkLE1BQU04UixhQUFhenZCLElBQWIsQ0FBa0IsS0FBbEIsQ0FGUDtBQUFBLE1BR0M2ZCxNQUFNNFIsYUFBYXp2QixJQUFiLENBQWtCLEtBQWxCLENBSFA7QUFBQSxNQUlDMnNCLFdBQVd6c0IsRUFBRSxzQkFBRixFQUEwQkYsSUFBMUIsQ0FBK0IsVUFBL0IsQ0FKWjs7QUFNQTB2QixjQUFZeG5CLE1BQVosQ0FBbUI7QUFDbEJnVyxVQUFPLElBRFc7QUFFbEJQLFFBQUtBLEdBRmE7QUFHbEJFLFFBQUtBLEdBSGE7QUFJbEJNLFdBQVEsQ0FBQ1IsR0FBRCxFQUFNRSxHQUFOLENBSlU7QUFLbEJRLFVBQU8sZUFBVS9XLEtBQVYsRUFBaUI4VyxFQUFqQixFQUFxQjtBQUMzQmxlLE1BQUUsU0FBRixFQUFhK00sR0FBYixDQUFpQm1SLEdBQUdELE1BQUgsQ0FBVSxDQUFWLElBQWUsR0FBZixHQUFxQndPLFFBQXJCLEdBQWdDLEtBQWhDLEdBQXdDdk8sR0FBR0QsTUFBSCxDQUFVLENBQVYsQ0FBeEMsR0FBdUQsR0FBdkQsR0FBNkR3TyxRQUE5RTtBQUNBO0FBUGlCLEdBQW5COztBQVVBOEMsZUFBYXhpQixHQUFiLENBQWlCeWlCLFlBQVl4bkIsTUFBWixDQUFtQixRQUFuQixFQUE2QixDQUE3QixJQUFrQyxHQUFsQyxHQUF3Q3lrQixRQUF4QyxHQUNoQixLQURnQixHQUNSK0MsWUFBWXhuQixNQUFaLENBQW1CLFFBQW5CLEVBQTZCLENBQTdCLENBRFEsR0FDMEIsR0FEMUIsR0FFaEJ5a0IsUUFGRDtBQUdBO0FBQ0Q7O0FBRUQ7QUFDQTtBQUNBOztrQkFFZTZDLFc7Ozs7Ozs7QUN4RGY7O0FBRUE7Ozs7O0FBS0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7O0FBTUE7QUFDQTtBQUNBOztBQUVBOzs7Ozs7O0FBR0EsU0FBU0csZUFBVCxHQUE0QjtBQUMzQixLQUFJenZCLEVBQUUseUJBQUYsRUFBNkJKLE1BQWpDLEVBQXlDO0FBQ3hDSSxJQUFFLHlCQUFGLEVBQTZCTSxJQUE3QixDQUFrQyxZQUFZO0FBQzdDLE9BQUl1SSxRQUFRN0ksRUFBRSxJQUFGLENBQVo7QUFBQSxPQUNDZ1MsT0FBT25KLE1BQU1yRixJQUFOLENBQVcsT0FBWCxFQUFvQkYsSUFBcEIsQ0FBeUIsSUFBekIsQ0FEUjtBQUFBLE9BRUNtYSxNQUFNemQsRUFBRSxNQUFNZ1MsSUFBUixFQUFjbFMsSUFBZCxDQUFtQixLQUFuQixDQUZQO0FBQUEsT0FHQzZkLE1BQU0zZCxFQUFFLE1BQU1nUyxJQUFSLEVBQWNsUyxJQUFkLENBQW1CLEtBQW5CLENBSFA7QUFBQSxPQUlDNHZCLFVBQVU3bUIsTUFBTS9JLElBQU4sQ0FBVyxTQUFYLEtBQXlCLENBSnBDOztBQU1BRSxLQUFFLGtCQUFrQmdTLElBQXBCLEVBQTBCaEssTUFBMUIsQ0FBaUM7QUFDaEN5VixTQUFLQSxHQUQyQjtBQUVoQ0UsU0FBS0EsR0FGMkI7QUFHaENNLFlBQVEsQ0FBQ3lSLE9BQUQsQ0FId0I7QUFJaEN2UixXQUFPLGVBQVUvVyxLQUFWLEVBQWlCOFcsRUFBakIsRUFBcUI7QUFDM0JsZSxPQUFFLE1BQU1nUyxJQUFSLEVBQWNqRixHQUFkLENBQWtCbVIsR0FBR0QsTUFBSCxDQUFVLENBQVYsSUFBZSxHQUFqQztBQUNBO0FBTitCLElBQWpDOztBQVNBamUsS0FBRSxNQUFNZ1MsSUFBUixFQUFjakYsR0FBZCxDQUFrQjJpQixVQUFVLEdBQTVCO0FBQ0EsR0FqQkQ7QUFrQkE7QUFDRDs7QUFFRDtBQUNBO0FBQ0E7O2tCQUVlRCxlOzs7Ozs7O0FDdkRmOztBQUVBOzs7OztBQUtBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7OztBQU1BO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7OztBQUdBLFNBQVNFLFNBQVQsR0FBc0I7QUFDckIzdkIsR0FBRSxvQkFBRixFQUF3Qm1ILEVBQXhCLENBQTJCLE9BQTNCLEVBQW9DLFVBQVV5QixDQUFWLEVBQWE7QUFDaEQsTUFBSUMsUUFBUTdJLEVBQUUsSUFBRixDQUFaOztBQUVBQSxJQUFFLG9CQUFGLEVBQXdCbU4sSUFBeEIsR0FBK0JsSSxJQUEvQjtBQUNBNEQsUUFBTXNFLElBQU4sR0FBYWdULE1BQWI7QUFDQSxFQUxEOztBQU9BbmdCLEdBQUUsTUFBRixFQUFVbUgsRUFBVixDQUFhLE9BQWIsRUFBc0IsVUFBVXlCLENBQVYsRUFBYTtBQUNsQyxNQUFJZ25CLE1BQU01dkIsRUFBRSxvQkFBRixDQUFWOztBQUVBLE1BQUksQ0FBQzR2QixJQUFJNUssRUFBSixDQUFPcGMsRUFBRXFjLE1BQVQsQ0FBRCxJQUFxQjJLLElBQUl0TixHQUFKLENBQVExWixFQUFFcWMsTUFBVixFQUFrQnJsQixNQUFsQixLQUE2QixDQUF0RCxFQUF5RDtBQUN4RGd3QixPQUFJemlCLElBQUosR0FBV2xJLElBQVg7QUFDQTtBQUNELEVBTkQ7QUFPQTs7QUFFRDtBQUNBO0FBQ0E7O2tCQUVlMHFCLFM7Ozs7Ozs7QUNqRGY7O0FBRUE7Ozs7O0FBS0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7O0FBTUE7QUFDQTtBQUNBOztBQUVBOzs7Ozs7O0FBR0EsU0FBU0UsVUFBVCxHQUF1QjtBQUN0Qjd2QixHQUFFLGdCQUFGLEVBQW9CbUgsRUFBcEIsQ0FBdUIsT0FBdkIsRUFBZ0MsWUFBWTtBQUMzQyxNQUFJc0YsTUFBTXpNLEVBQUUsSUFBRixFQUFRRixJQUFSLENBQWEsS0FBYixDQUFWO0FBQUEsTUFDQ2d1QixPQUFPOXRCLEVBQUUsT0FBRixFQUFXK00sR0FBWCxFQURSO0FBQUEsTUFFQ2loQixTQUFTaHVCLEVBQUUsU0FBRixFQUFhK00sR0FBYixFQUZWO0FBQUEsTUFHQ2doQixlQUFlL3RCLEVBQUUsNkJBQUYsRUFBaUMwTyxJQUFqQyxDQUFzQyxTQUF0QyxDQUhoQjtBQUFBLE1BSUNvaEIsbUJBQW1CLEVBSnBCO0FBQUEsTUFLQzlSLFFBQVEsRUFMVDtBQUFBLE1BTUNpUixvQkFBb0IsRUFOckI7O0FBUUFqdkIsSUFBRSx5QkFBRixFQUE2QitJLE1BQTdCLEdBQXNDdkYsSUFBdEMsQ0FBMkMscUNBQTNDLEVBQWtGbEQsSUFBbEYsQ0FBdUYsWUFBWTtBQUNsRzJ1QixxQkFBa0JoWixJQUFsQixDQUF1QmpXLEVBQUUsSUFBRixFQUFRRixJQUFSLENBQWEsSUFBYixDQUF2QjtBQUNBLEdBRkQ7O0FBSUFFLElBQUUseUJBQUYsRUFBNkJNLElBQTdCLENBQWtDLFlBQVk7QUFDN0N3dkIsb0JBQWlCN1osSUFBakIsQ0FBc0JqVyxFQUFFLElBQUYsRUFBUW1OLElBQVIsR0FBZTNKLElBQWYsQ0FBb0IsT0FBcEIsRUFBNkIxRCxJQUE3QixDQUFrQyxJQUFsQyxDQUF0QjtBQUNBLEdBRkQ7O0FBSUFFLElBQUUseUJBQUYsRUFBNkJNLElBQTdCLENBQWtDLFlBQVk7QUFDN0MsT0FBSTBSLE9BQU9oUyxFQUFFLElBQUYsRUFBUXdELElBQVIsQ0FBYSxPQUFiLEVBQXNCRixJQUF0QixDQUEyQixJQUEzQixDQUFYO0FBQUEsT0FDQ3ZDLFFBQVFmLEVBQUUsSUFBRixFQUFRd0QsSUFBUixDQUFhLE9BQWIsRUFBc0J1SixHQUF0QixFQURUOztBQUdBaVIsU0FBTS9ILElBQU4sQ0FBVyxDQUFDakUsSUFBRCxFQUFPalIsS0FBUCxDQUFYO0FBQ0EsR0FMRDs7QUFPQWYsSUFBRXdNLElBQUYsQ0FBTztBQUNOQyxRQUFLQSxHQURDO0FBRU5oSixTQUFNLE1BRkE7QUFHTjNELFNBQU07QUFDTGd1QixVQUFNQSxJQUREO0FBRUxDLGtCQUFjQSxZQUZUO0FBR0xDLFlBQVFBLE1BSEg7QUFJTGlCLHVCQUFtQkEsaUJBSmQ7QUFLTGEsc0JBQWtCQSxnQkFMYjtBQU1MOVIsV0FBT0E7QUFORixJQUhBO0FBV05rQixhQUFVLE1BWEo7O0FBYU56SyxZQUFTLGlCQUFVMVAsUUFBVixFQUFvQjtBQUM1QixRQUFJQSxTQUFTMFAsT0FBYixFQUFzQjtBQUNyQnpVLE9BQUVNLElBQUYsQ0FBT3lFLFNBQVNnckIsUUFBaEIsRUFBMEIsVUFBVTdqQixLQUFWLEVBQWlCbkwsS0FBakIsRUFBd0I7QUFDakQsVUFBSTZZLFFBQVE1WixFQUFFLCtCQUErQmtNLEtBQS9CLEdBQXVDLElBQXpDLEVBQStDbkQsTUFBL0MsRUFBWjtBQUNBNlEsWUFBTXBXLElBQU4sQ0FBVyxtQkFBWCxFQUFnQzNDLE1BQWhDO0FBQ0ErWSxZQUFNdlcsTUFBTixDQUFhdEMsS0FBYjtBQUNBLE1BSkQ7O0FBTUFmLE9BQUUsc0JBQUYsRUFBMEJrRCxJQUExQixDQUErQixNQUEvQjtBQUNBdkMsZ0JBQVcsWUFBWTtBQUN0QlgsUUFBRSxzQkFBRixFQUEwQmlGLElBQTFCLENBQStCLE1BQS9CO0FBQ0EsTUFGRCxFQUVHLElBRkg7O0FBSUFqRixPQUFFLDBDQUFGLEVBQThDMkksR0FBOUMsQ0FBa0QsaURBQWxELEVBQXFHckksSUFBckcsQ0FBMEcsWUFBWTtBQUNySE4sUUFBRSxJQUFGLEVBQVEwTyxJQUFSLENBQWEsU0FBYixFQUF3QixLQUF4QjtBQUNBLE1BRkQ7O0FBSUExTyxPQUFFTSxJQUFGLENBQU95RSxTQUFTcXBCLGlCQUFoQixFQUFtQyxVQUFVakwsRUFBVixFQUFjNk0sTUFBZCxFQUFzQjtBQUN4RGh3QixRQUFFLG9CQUFvQm1qQixFQUFwQixHQUF5QixJQUEzQixFQUFpQ3pVLElBQWpDLENBQXNDLFNBQXRDLEVBQWlELFNBQWpELEVBQTREcEwsSUFBNUQsQ0FBaUUsYUFBakUsRUFBZ0YsQ0FBQzBzQixNQUFqRjtBQUNBLE1BRkQ7O0FBSUFod0IsT0FBRSxrQkFBRixFQUFzQk0sSUFBdEIsQ0FBMkIsWUFBVztBQUNyQyxVQUFJdUksUUFBUTdJLEVBQUUsSUFBRixDQUFaO0FBQUEsVUFDQ3d1QixZQUFZM2xCLE1BQU1yRixJQUFOLENBQVcsNkJBQVgsRUFBMEM1RCxNQUR2RDtBQUFBLFVBRUM2dUIsZ0JBQWdCNWxCLE1BQU1yRixJQUFOLENBQVcscUNBQVgsRUFBa0Q1RCxNQUZuRTs7QUFJQSxVQUFJNHVCLGNBQWNDLGFBQWxCLEVBQWlDO0FBQ2hDNWxCLGFBQU1yRixJQUFOLENBQVcsZ0NBQVgsRUFBNkMySixJQUE3QyxHQUFvRDNKLElBQXBELENBQXlELE9BQXpELEVBQWtFa0wsSUFBbEUsQ0FBdUUsU0FBdkUsRUFBa0YsU0FBbEYsRUFBNkZwTCxJQUE3RixDQUFrRyxhQUFsRyxFQUFpSCxJQUFqSDtBQUNBLE9BRkQsTUFFTyxJQUFJbXJCLGdCQUFnQixDQUFwQixFQUFzQjtBQUM1QjVsQixhQUFNckYsSUFBTixDQUFXLGdDQUFYLEVBQTZDMkosSUFBN0MsR0FBb0QzSixJQUFwRCxDQUF5RCxPQUF6RCxFQUFrRWtMLElBQWxFLENBQXVFLFNBQXZFLEVBQWtGLElBQWxGLEVBQXdGcEwsSUFBeEYsQ0FBNkYsYUFBN0YsRUFBNEcsS0FBNUc7QUFDQSxPQUZNLE1BRUEsSUFBSW1yQixrQkFBa0IsQ0FBdEIsRUFBeUI7QUFDL0I1bEIsYUFBTXJGLElBQU4sQ0FBVyxnQ0FBWCxFQUE2QzJKLElBQTdDLEdBQW9EM0osSUFBcEQsQ0FBeUQsT0FBekQsRUFBa0VrTCxJQUFsRSxDQUF1RSxTQUF2RSxFQUFrRixLQUFsRixFQUF5RnBMLElBQXpGLENBQThGLGFBQTlGLEVBQTZHLElBQTdHO0FBQ0E7QUFDRCxNQVpEOztBQWNBdEQsT0FBRSxrQkFBRixFQUFzQk0sSUFBdEIsQ0FBMkIsWUFBVztBQUNyQyxVQUFJdUksUUFBUTdJLEVBQUUsSUFBRixDQUFaO0FBQUEsVUFDQ3d1QixZQUFZM2xCLE1BQU1yRixJQUFOLENBQVcsWUFBWCxFQUF5QjVELE1BRHRDO0FBQUEsVUFFQzZ1QixnQkFBZ0I1bEIsTUFBTXJGLElBQU4sQ0FBVyxvQkFBWCxFQUFpQzVELE1BRmxEOztBQUlBLFVBQUk0dUIsY0FBY0MsYUFBbEIsRUFBaUM7QUFDaEM1bEIsYUFBTW9sQixJQUFOLEdBQWF6cUIsSUFBYixDQUFrQixPQUFsQixFQUEyQmtMLElBQTNCLENBQWdDLFNBQWhDLEVBQTJDLFNBQTNDLEVBQXNEcEwsSUFBdEQsQ0FBMkQsYUFBM0QsRUFBMEUsSUFBMUU7QUFDQSxPQUZELE1BRU8sSUFBSW1yQixnQkFBZ0IsQ0FBcEIsRUFBc0I7QUFDNUI1bEIsYUFBTW9sQixJQUFOLEdBQWF6cUIsSUFBYixDQUFrQixPQUFsQixFQUEyQmtMLElBQTNCLENBQWdDLFNBQWhDLEVBQTJDLElBQTNDLEVBQWlEcEwsSUFBakQsQ0FBc0QsYUFBdEQsRUFBcUUsS0FBckU7QUFDQSxPQUZNLE1BRUEsSUFBSW1yQixrQkFBa0IsQ0FBdEIsRUFBeUI7QUFDL0I1bEIsYUFBTW9sQixJQUFOLEdBQWF6cUIsSUFBYixDQUFrQixPQUFsQixFQUEyQmtMLElBQTNCLENBQWdDLFNBQWhDLEVBQTJDLEtBQTNDLEVBQWtEcEwsSUFBbEQsQ0FBdUQsYUFBdkQsRUFBc0UsSUFBdEU7QUFDQTtBQUNELE1BWkQ7QUFhQTtBQUNEO0FBOURLLEdBQVA7QUFnRUEsRUF4RkQ7QUF5RkE7O0FBRUQ7QUFDQTtBQUNBOztrQkFFZXVzQixVOzs7Ozs7O0FDNUhmOztBQUVBOzs7OztBQUtBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7OztBQU1BO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7OztBQUdBLFNBQVNJLElBQVQsR0FBaUI7QUFDaEJqd0IsR0FBRSxhQUFGLEVBQWlCbUgsRUFBakIsQ0FBb0IsT0FBcEIsRUFBNkIsVUFBVXlCLENBQVYsRUFBYTtBQUN6QyxNQUFJNkQsTUFBTXpNLEVBQUUsSUFBRixFQUFRRixJQUFSLENBQWEsS0FBYixDQUFWO0FBQUEsTUFDQ293QixTQUFTbHdCLEVBQUUsSUFBRixFQUFRc0QsSUFBUixDQUFhLGFBQWIsQ0FEVjtBQUFBLE1BRUM2c0IsV0FBV253QixFQUFFLHdCQUFGLEVBQTRCK00sR0FBNUIsRUFGWjtBQUFBLE1BR0NxakIsY0FBY3B3QixFQUFFLDJCQUFGLEVBQStCK00sR0FBL0IsRUFIZjtBQUFBLE1BSUNnaEIsZUFBZS90QixFQUFFLDZCQUFGLEVBQWlDME8sSUFBakMsQ0FBc0MsU0FBdEMsQ0FKaEI7QUFBQSxNQUtDa2dCLDJCQUEyQixFQUw1QjtBQUFBLE1BTUNtQixXQUFXLEVBTlo7QUFBQSxNQU9DL1IsUUFBUSxFQVBUOztBQVNBLE1BQUltUyxTQUFTdndCLE1BQVQsR0FBa0IsQ0FBbEIsSUFBdUJ3d0IsWUFBWXh3QixNQUFaLEdBQXFCLENBQWhELEVBQW1EO0FBQ2xEZ0osS0FBRXlMLGNBQUY7O0FBRUFyVSxLQUFFLCtCQUFGLEVBQW1DTSxJQUFuQyxDQUF3QyxZQUFZO0FBQ25ELFFBQUl1SSxRQUFRN0ksRUFBRSxJQUFGLENBQVo7O0FBRUEsUUFBSTZJLE1BQU1FLE1BQU4sR0FBZWdtQixRQUFmLENBQXdCLG1CQUF4QixFQUE2Q252QixNQUE3QyxJQUF1RGlKLE1BQU1zRSxJQUFOLEdBQWEzSixJQUFiLENBQWtCLE9BQWxCLEVBQTJCd2hCLEVBQTNCLENBQThCLFVBQTlCLENBQTNELEVBQXNHO0FBQ3JHNEosOEJBQXlCM1ksSUFBekIsQ0FBOEJwTixNQUFNL0ksSUFBTixDQUFXLElBQVgsQ0FBOUI7QUFDQTtBQUNELElBTkQ7O0FBUUFFLEtBQUUscUJBQUYsRUFBeUJNLElBQXpCLENBQThCLFlBQVk7QUFDekNOLE1BQUUsSUFBRixFQUFRd0QsSUFBUixDQUFhLGVBQWIsRUFBOEJsRCxJQUE5QixDQUFtQyxZQUFZO0FBQzlDLFNBQUk2aUIsS0FBS25qQixFQUFFLElBQUYsRUFBUUYsSUFBUixDQUFhLElBQWIsQ0FBVDtBQUFBLFNBQ0N1d0IsV0FBV3J3QixFQUFFLElBQUYsRUFBUW1NLE9BQVIsQ0FBZ0IscUJBQWhCLEVBQXVDM0ksSUFBdkMsQ0FBNEMsMEJBQTVDLEVBQXdFRixJQUF4RSxDQUE2RSxlQUE3RSxDQURaOztBQUdBeXNCLGNBQVM5WixJQUFULENBQWMsQ0FBQ2tOLEVBQUQsRUFBS2tOLFFBQUwsQ0FBZDtBQUNBLEtBTEQ7QUFNQSxJQVBEOztBQVNBcndCLEtBQUUseUJBQUYsRUFBNkJNLElBQTdCLENBQWtDLFlBQVk7QUFDN0MsUUFBSTBSLE9BQU9oUyxFQUFFLElBQUYsRUFBUXdELElBQVIsQ0FBYSxPQUFiLEVBQXNCRixJQUF0QixDQUEyQixJQUEzQixDQUFYO0FBQUEsUUFDQ3ZDLFFBQVFmLEVBQUUsSUFBRixFQUFRd0QsSUFBUixDQUFhLE9BQWIsRUFBc0J1SixHQUF0QixFQURUOztBQUdBaVIsVUFBTS9ILElBQU4sQ0FBVyxDQUFDakUsSUFBRCxFQUFPalIsS0FBUCxDQUFYO0FBQ0EsSUFMRDs7QUFRQWYsS0FBRXdNLElBQUYsQ0FBTztBQUNOQyxTQUFLQSxHQURDO0FBRU5oSixVQUFNLE1BRkE7QUFHTjNELFVBQU07QUFDTHF3QixlQUFVQSxRQURMO0FBRUxELGFBQVFBLE1BRkg7QUFHTG5DLG1CQUFjQSxZQUhUO0FBSUwvUCxZQUFPQSxLQUpGO0FBS0xvUyxrQkFBYUEsV0FMUjtBQU1MeEIsK0JBQTBCQSx3QkFOckI7QUFPTG1CLGVBQVVBO0FBUEwsS0FIQTtBQVlON1EsY0FBVSxNQVpKO0FBYU5vUixnQkFBWSxzQkFBWTtBQUN2QnR3QixPQUFFLHNCQUFGLEVBQTBCOEcsR0FBMUIsQ0FBOEIsU0FBOUIsRUFBeUMsTUFBekM7QUFDQSxLQWZLO0FBZ0JOMk4sYUFBUyxpQkFBVTFQLFFBQVYsRUFBb0I7QUFDNUIsU0FBSUEsU0FBUzBQLE9BQWIsRUFBc0I7QUFDckJ6VSxRQUFFLHNCQUFGLEVBQTBCaUYsSUFBMUI7O0FBRUFqRixRQUFFLHFCQUFGLEVBQXlCa0QsSUFBekIsQ0FBOEIsTUFBOUI7QUFDQXZDLGlCQUFXLFlBQVk7QUFDdEJYLFNBQUUscUJBQUYsRUFBeUJpRixJQUF6QixDQUE4QixNQUE5QjtBQUNBLE9BRkQsRUFFRyxJQUZIOztBQUlBdEUsaUJBQVcsWUFBWTtBQUN0QnlFLGdCQUFTb1MsTUFBVDtBQUNBLE9BRkQsRUFFRyxJQUZIO0FBR0E7QUFDRDtBQTdCSyxJQUFQO0FBK0JBO0FBQ0QsRUF0RUQ7QUF1RUE7O0FBRUQ7QUFDQTtBQUNBOztrQkFFZXlZLEk7Ozs7Ozs7QUMxR2Y7O0FBRUE7Ozs7O0FBS0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7O0FBTUE7QUFDQTtBQUNBOztBQUVBOzs7Ozs7O0FBR0EsU0FBU00sUUFBVCxHQUFxQjtBQUNwQnZ3QixJQUFFLGNBQUYsRUFBa0JtSCxFQUFsQixDQUFxQixPQUFyQixFQUE4QixZQUFZO0FBQ3pDLFFBQUlxcEIsUUFBUXh3QixFQUFFLFNBQUYsQ0FBWjtBQUNBQSxNQUFFLE1BQUYsRUFBVXFELE1BQVYsQ0FBaUJtdEIsS0FBakI7QUFDQUEsVUFBTXpqQixHQUFOLENBQVUvTSxFQUFFLDRCQUFGLEVBQWdDK00sR0FBaEMsRUFBVixFQUFpRG1pQixNQUFqRDtBQUNBeGtCLGFBQVMrbEIsV0FBVCxDQUFxQixNQUFyQjtBQUNBRCxVQUFNM3ZCLE1BQU47QUFDQSxHQU5EO0FBT0E7O0FBRUQ7QUFDQTtBQUNBOztrQkFFZTB2QixRIiwiZmlsZSI6ImluaXRpYWxpemUuanMiLCJzb3VyY2VzQ29udGVudCI6WyIndXNlIHN0cmljdCc7XHJcblxyXG4vKipcclxuICog0J/RgNC+0YHRgtC+0Lkg0L/RgNC10LvQvtCw0LTQtdGAXHJcbiAqIEBtb2R1bGVcclxuICovXHJcblxyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbi8vIEltcG9ydHNcclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuaW1wb3J0IGNyZWF0ZVRpbWVyIGZyb20gJyMvX21vZHVsZXMvY3JlYXRlLXRpbWVyJztcclxuXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuLy8gUHJpdmF0ZVxyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG4vKipcclxuICog0JTQvtGE0LXQu9GC0L3Ri9C5INC60L7QvdGE0LjQsyDQtNC70Y8g0LLRgdC10YUg0Y3QutC30LXQvNC/0LvRj9GA0L7QsiDQv9GA0LXQu9C+0LDQtNC10YDQsFxyXG4gKiBAdHlwZSB7T2JqZWN0fVxyXG4gKiBAcHJvcCB7c3RyaW5nfSBtYWluQ2xhc3MgLSDQvtGB0L3QvtCy0L3QvtC5IENTUyDQutC70LDRgdGBLCDQutC+0YLQvtGA0YvQuSDQtNC+0LHQsNCy0LvRj9C10YLRgdGPINC6INC60L7QvdGC0LXQudC90LXRgNGDINC/0YDQtdC70L7QtNCw0YDQsFxyXG4gKiBAcHJvcCB7c3RyaW5nfSBzdWJDbGFzcyAtINCU0L7Qv9C+0LvQvdC40YLQtdC70YzQvdGL0LkgQ1NTINC60LvQsNGB0YEgLSDQutC+0YLQvtGA0YvQuSDQsdGD0LTQtdGCINC00L7QsdCw0LLQu9C10L0sINC10YHQu9C4INC40LzQtdC10YIg0LvQvtCz0LjRh9C10YHQutC4INC/0L7Qt9C40YLQuNCy0L3QvtC1INC30L3QsNGH0LXQvdC40LVcclxuICogQHByb3Age3N0cmluZ30gc2hvd0NsYXNzIC0gQ1NTINC60LvQsNGB0YEsINC60L7RgtC+0YDRi9C5INC00L7QsdCw0LLQu9GP0LXRgtGB0Y8g0Log0LrQvtC90YLQtdC50L3QtdGA0YMg0L/RgNC10LvQvtC00LDRgNCwLCDQv9GA0Lgg0LXQs9C+INC/0L7QutCw0LfQtVxyXG4gKiBAcHJvcCB7c3RyaW5nfSBoaWRlQ2xhc3MgLSBDU1Mg0LrQu9Cw0YHRgSwg0LrQvtGC0L7RgNGL0Lkg0LTQvtCx0LDQstC70Y/QtdGC0YHRjyDQuiDQutC+0L3RgtC10LnQvdC10YDRgyDQv9GA0LXQu9C+0LTQsNGA0LAsINC/0LXRgNC10LQg0LXQs9C+INC30LDQutGA0YvRgtC40LXQvFxyXG4gKiBAcHJvcCB7bnVtYmVyfSByZW1vdmVEZWxheSAtINCy0YDQtdC80Y8g0LfQsNC00LXRgNC20LrQuCwg0L/QvtGB0LvQtSDQutC+0YLQvtGA0L7QuSDQsdGD0LTRg9GCINGD0LTQsNC70LXQvdGLINGA0LDQt9C80LXRgtC60LAg0L/RgNC10LvQvtCw0LTRgNC1INC4IENTUyDQutC70LDRgdGB0Ysg0YMg0LrQvtC90YLQtdC50L3QtdGA0LBcclxuICogQHByb3Age3N0cmluZ30gbWFya3VwIC0g0KDQsNC30LzQtdGC0LrQsCDQv9GA0LXQu9C+0LTQtdGA0LBcclxuICogQHByaXZhdGVcclxuICovXHJcbmxldCBkZWZhdWx0Q29uZmlnID0ge1xyXG5cdG1haW5DbGFzczogJ3ByZWxvYWRlcicsXHJcblx0c3ViQ2xhc3M6ICcnLFxyXG5cdHNob3dDbGFzczogJ3ByZWxvYWRlci0tc2hvdycsXHJcblx0aGlkZUNsYXNzOiAncHJlbG9hZGVyLS1oaWRlJyxcclxuXHRyZW1vdmVEZWxheTogMzAwLFxyXG5cdG1hcmt1cDogJzxkaXYgY2xhc3M9XCJwcmVsb2FkZXJfX2Jsb2NrXCI+PC9kaXY+J1xyXG59O1xyXG5cclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4vLyBQdWJsaWNcclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuLyoqXHJcbiAqINCh0L7Qt9C00LDQvdC40LUg0L/RgNC10LvQvtC00LDRgNC+0LJcclxuICogQHBhcmFtIHtKUXVlcnl9ICRjb250YWluZXIgLSDQutC+0L3RgtC10LnQvdC10YAg0LTQu9GPINC/0YDQtdC70L7QsNC00LXRgNCwXHJcbiAqIEBwYXJhbSB7T2JqZWN0fSBbdXNlckNvbmZpZz17fV0gLSDQn9C+0LvRjNC30LLQsNGC0LXQu9GM0YHQutC40Lkg0LrQvtC90YTQuNCzINC00LvRjyDRgdC+0LfQtNCy0LDQtdC80L7Qs9C+INGN0LrQt9C10LzQv9C70Y/RgNCwXHJcbiAqIEBwcm9wIHtKUXVlcnl9IFskY29udGFpbmVyXSAtINC60L7QvdGC0LXQudC90LXRgCDQtNC70Y8g0L/RgNC10LvQvtCw0LTQtdGA0LBcclxuICogQHByb3Age09iamVjdH0gW2NvbmZpZ10gLSDQv9GA0LXQu9C+0LDQtNC10YDQsFxyXG4gKiBAcHJvcCB7T2JqZWN0fG51bWJlcn0gdGltZXIgLSBpZCDRgtCw0LnQvNC10YDQsFxyXG4gKiBAcHJvcCB7Ym9vbGVhbn0gZW1wdHlcclxuICogQGV4YW1wbGVcclxuICogbGV0IHByZWxvYWRlciA9IG5ldyBQcmVsb2FkZXIoJG15RWxlbWVudCk7XHJcbiAqIHByZWxvYWRlci5zaG93KCk7XHJcbiAqIHdpbmRvdy5zZXRUaW1lb3V0KCgpID0+IHByZWxvYWRlci5oaWRlKCksIDEwMDApO1xyXG4gKi9cclxuY2xhc3MgUHJlbG9hZGVyIHtcclxuXHRjb25zdHJ1Y3RvciAoJGNvbnRhaW5lciwgdXNlckNvbmZpZyA9IHt9KSB7XHJcblx0XHRpZiAoISgkY29udGFpbmVyICYmICRjb250YWluZXIubGVuZ3RoKSkge1xyXG5cdFx0XHR0aGlzLmVtcHR5ID0gdHJ1ZTtcclxuXHRcdFx0cmV0dXJuIHRoaXM7XHJcblx0XHR9XHJcblx0XHRpZiAoJGNvbnRhaW5lci5kYXRhKCdwcmVsb2FkZXInKSBpbnN0YW5jZW9mIFByZWxvYWRlcikge1xyXG5cdFx0XHQkY29udGFpbmVyLmRhdGEoJ3ByZWxvYWRlcicsIG51bGwpO1xyXG5cdFx0fVxyXG5cdFx0dGhpcy5lbXB0eSA9IGZhbHNlO1xyXG5cdFx0dGhpcy4kY29udGFpbmVyID0gJGNvbnRhaW5lcjtcclxuXHRcdHRoaXMuY29uZmlnID0gJC5leHRlbmQodHJ1ZSwge30sIFByZWxvYWRlci5jb25maWcsIHVzZXJDb25maWcpO1xyXG5cdFx0dGhpcy50aW1lciA9IGNyZWF0ZVRpbWVyKCk7XHJcblx0XHR0aGlzLiRjb250YWluZXIuZGF0YSgncHJlbG9hZGVyJywgdGhpcyk7XHJcblx0fVxyXG5cclxuXHQvKipcclxuXHQgKiDQn9C+0LrQsNC3INC/0YDQtdC70L7QtNC10YDQsFxyXG5cdCAqIEBwYXJhbSB7c3RyaW5nfSBbcGxhY2U9XCJhcHBlbmRcIl0gLSBhcHBlbmQgLyBwcmVwZW5kIC8gYmVmb3JlIC8gYWZ0ZXJcclxuXHQgKiBAc291cmNlQ29kZVxyXG5cdCAqL1xyXG5cdHNob3cgKHBsYWNlID0gJ2FwcGVuZCcpIHtcclxuXHRcdGlmICh0aGlzLmVtcHR5KSB7XHJcblx0XHRcdGNvbnNvbGUud2FybignRW1wdHkgcHJlbG9hZGVyIGNhbm5vdCBiZSBzaG93biEnKTtcclxuXHRcdFx0cmV0dXJuIGZhbHNlO1xyXG5cdFx0fVxyXG5cdFx0bGV0IHttYWluQ2xhc3MsIHN1YkNsYXNzLCBzaG93Q2xhc3MsIG1hcmt1cH0gPSB0aGlzLmNvbmZpZztcclxuXHJcblx0XHR0aGlzLiRjb250YWluZXIuZWFjaCgoaSwgZWwpID0+IHtcclxuXHRcdFx0bGV0ICRjb250YWluZXIgPSAkKGVsKTtcclxuXHRcdFx0JGNvbnRhaW5lci5hZGRDbGFzcyhtYWluQ2xhc3MpO1xyXG5cdFx0XHRpZiAoc3ViQ2xhc3MpIHtcclxuXHRcdFx0XHQkY29udGFpbmVyLmFkZENsYXNzKHN1YkNsYXNzKTtcclxuXHRcdFx0fVxyXG5cdFx0XHQkY29udGFpbmVyLmRhdGEoJyRwcmVsb2FkZXJNYXJrdXAnLCAkKG1hcmt1cCkpO1xyXG5cdFx0XHQkY29udGFpbmVyW3BsYWNlXSgkY29udGFpbmVyLmRhdGEoJyRwcmVsb2FkZXJNYXJrdXAnKSk7XHJcblx0XHRcdHdpbmRvdy5zZXRUaW1lb3V0KCgpID0+ICRjb250YWluZXIuYWRkQ2xhc3Moc2hvd0NsYXNzKSwgMTApO1xyXG5cdFx0fSk7XHJcblx0fVxyXG5cclxuXHQvKipcclxuXHQgKiDQodC60YDRi9GC0LjQtSDQv9GA0LXQu9C+0LTQtdGA0LBcclxuXHQgKiBAcGFyYW0ge2Jvb2xlYW59IFtyZW1vdmVNYXJrdXA9dHJ1ZV0gLSDRg9C00LDQu9C40YLRjCDRgNCw0LfQvNC10YLQutGDINC/0YDQtdC70L7QtNC10YDQsCDQv9C+0YHQu9C1INGB0LrRgNGL0YLQuNGPXHJcblx0ICogQHNvdXJjZUNvZGVcclxuXHQgKi9cclxuXHRoaWRlIChyZW1vdmVNYXJrdXAgPSB0cnVlKSB7XHJcblx0XHRpZiAodGhpcy5lbXB0eSkge1xyXG5cdFx0XHRjb25zb2xlLndhcm4oJ0VtcHR5IHByZWxvYWRlciBjYW5ub3QgYmUgaGlkZGVuIScpO1xyXG5cdFx0XHRyZXR1cm4gZmFsc2U7XHJcblx0XHR9XHJcblx0XHRsZXQge21haW5DbGFzcywgc3ViQ2xhc3MsIHNob3dDbGFzcywgaGlkZUNsYXNzfSA9IHRoaXMuY29uZmlnO1xyXG5cdFx0dGhpcy4kY29udGFpbmVyLmFkZENsYXNzKGhpZGVDbGFzcyk7XHJcblxyXG5cdFx0dGhpcy50aW1lcigoKSA9PiB7XHJcblx0XHRcdGlmIChyZW1vdmVNYXJrdXApIHtcclxuXHRcdFx0XHR0aGlzLiRjb250YWluZXIuZWFjaCgoaSwgZWwpID0+IHtcclxuXHRcdFx0XHRcdGlmICgkKGVsKS5kYXRhKCckcHJlbG9hZGVyTWFya3VwJykgJiYgJChlbCkuZGF0YSgnJHByZWxvYWRlck1hcmt1cCcpLmxlbmd0aCkge1xyXG5cdFx0XHRcdFx0XHQkKGVsKS5kYXRhKCckcHJlbG9hZGVyTWFya3VwJykucmVtb3ZlKCk7XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0fSk7XHJcblx0XHRcdH1cclxuXHRcdFx0dGhpcy4kY29udGFpbmVyLnJlbW92ZUNsYXNzKFttYWluQ2xhc3MsIHNob3dDbGFzcywgaGlkZUNsYXNzXSk7XHJcblx0XHRcdGlmIChzdWJDbGFzcykge1xyXG5cdFx0XHRcdHRoaXMuJGNvbnRhaW5lci5yZW1vdmVDbGFzcyhzdWJDbGFzcyk7XHJcblx0XHRcdH1cclxuXHRcdH0sIHRoaXMuY29uZmlnLnJlbW92ZURlbGF5KTtcclxuXHR9XHJcblxyXG5cdC8qKlxyXG5cdCAqINCf0L7Qu9GD0YfQtdC90LjQtSDQs9C70L7QsdCw0LvRjNC90L7Qs9C+INC60L7QvdGE0LjQs9CwINC00LvRjyDQstGB0LXRhSDQv9GA0LXQu9C+0LTQsNGA0L7QslxyXG5cdCAqIEByZXR1cm5zIHtPYmplY3R9XHJcblx0ICogQHNvdXJjZUNvZGVcclxuXHQgKi9cclxuXHRzdGF0aWMgZ2V0IGNvbmZpZyAoKSB7XHJcblx0XHRyZXR1cm4gZGVmYXVsdENvbmZpZztcclxuXHR9XHJcblxyXG5cdC8qKlxyXG5cdCAqINCj0YHRgtCw0L3QvtCy0LrQsCDQs9C70L7QsdCw0LvRjNC90L7Qs9C+INC60L7QvdGE0LjQs9CwXHJcblx0ICogQHBhcmFtIHtPYmplY3R9IHZhbHVlXHJcblx0ICogQHNvdXJjZUNvZGVcclxuXHQgKiBAZXhhbXBsZVxyXG5cdCAqIFByZWxvYWRlci5jb25maWcgPSB7bWFpbkNsYXNzOiAnbXktcHJlbG9hZGVyJ31cclxuXHQgKi9cclxuXHRzdGF0aWMgc2V0IGNvbmZpZyAodmFsdWUpIHtcclxuXHRcdCQuZXh0ZW5kKHRydWUsIGRlZmF1bHRDb25maWcsIHZhbHVlKTtcclxuXHR9XHJcbn1cclxuXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuLy8gRXhwb3J0c1xyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG5leHBvcnQgZGVmYXVsdCBQcmVsb2FkZXI7XHJcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL19IVE1ML3NyYy9qcy9fbW9kdWxlcy9QcmVsb2FkZXIuanMiLCIndXNlIHN0cmljdCc7XHJcblxyXG4vKipcclxuICogQG1vZHVsZVxyXG4gKi9cclxuXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuLy8gSW1wb3J0c1xyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG5pbXBvcnQgTW9kdWxlTG9hZGVyIGZyb20gJ3dlem9tLW1vZHVsZS1sb2FkZXInO1xyXG5cclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4vLyBQdWJsaWNcclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuY29uc3QgbW9kdWxlTG9hZGVyID0gbmV3IE1vZHVsZUxvYWRlcih7XHJcblx0ZGVidWc6IElTX0RFVkVMT1AsXHJcblx0aW1wb3J0UHJvbWlzZTogbW9kdWxlTmFtZSA9PiBpbXBvcnQoJy4vX21vZHVsZXMtbG9hZGVycy8nICsgbW9kdWxlTmFtZSksXHJcblx0aW5pdFNlbGVjdG9yOiAnLmpzLWluaXQnLFxyXG5cdGluaXRGdW5jdGlvbk5hbWU6ICdsb2FkZXJJbml0JyxcclxuXHRsb2FkaW5nQ2xhc3M6ICdfbW9kdWxlLWxvYWRpbmcnLFxyXG5cdGxvYWRlZENsYXNzOiAnX21vZHVsZS1sb2FkZWQnLFxyXG5cdGxpc3Q6IHtcclxuXHRcdC8qIGVzbGludC1kaXNhYmxlIGtleS1zcGFjaW5nICovXHJcblx0XHQnanF1ZXJ5LXZhbGlkYXRpb24tLW1vZHVsZS1sb2FkZXInOiAnZm9ybScsXHJcblx0XHQnaW5wdXRtYXNrLS1tb2R1bGUtbG9hZGVyJzogICAgICAgICAnW2RhdGEtcGhvbmVtYXNrXScsXHJcblx0XHQnbWFnbmlmaWMtcG9wdXAtLW1vZHVsZS1sb2FkZXInOiAgICAnW2RhdGEtbWZwXScsXHJcblx0XHQnd3JhcC1tZWRpYS0tbW9kdWxlLWxvYWRlcic6ICAgICAgICAnW2RhdGEtd3JhcC1tZWRpYV0nLFxyXG5cdFx0J3ByaXNtanMtLW1vZHVsZS1sb2FkZXInOiAgICAgICAgICAgJ1tkYXRhLXByaXNtanNdJyxcclxuXHRcdCdsb3phZC0tbW9kdWxlLWxvYWRlcic6ICAgICAgICAgICAgIFsnW2RhdGEtbG96YWRdJywgJ3BpY3R1cmUnXSxcclxuXHRcdCdzY3JvbGwtd2luZG93LS1tb2R1bGUtbG9hZGVyJzogICAgICdbZGF0YS1zY3JvbGwtd2luZG93XScsXHJcblx0XHQnZHJhZ2dhYmxlLXRhYmxlLS1tb2R1bGUtbG9hZGVyJzogICAnW2RhdGEtZHJhZ2dhYmxlLXRhYmxlXScsXHJcblx0XHQnbWFsaWh1LXNjcm9sbGJhci0tbW9kdWxlLWxvYWRlcic6ICAgJ1tkYXRhLXNjcm9sbGJhcl0nXHJcblx0fVxyXG59KTtcclxuXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuLy8gRXhwb3J0c1xyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG5leHBvcnQgZGVmYXVsdCBtb2R1bGVMb2FkZXI7XHJcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL19IVE1ML3NyYy9qcy9tb2R1bGUtbG9hZGVyLmpzIiwiJ3VzZSBzdHJpY3QnO1xyXG5cclxuLyoqXHJcbiAqINCY0L3QuNGCINCy0LDQu9C40LTQsNGG0LjQuCDRhNC+0YDQvFxyXG4gKiBAbW9kdWxlXHJcbiAqL1xyXG5cclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4vLyBJbXBvcnRzXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbmltcG9ydCAnanF1ZXJ5LXZhbGlkYXRpb24nO1xyXG5pbXBvcnQgJ2N1c3RvbS1qcXVlcnktbWV0aG9kcy9mbi9ub2RlLW5hbWUnO1xyXG5pbXBvcnQgJ2N1c3RvbS1qcXVlcnktbWV0aG9kcy9mbi9oYXMtaW5pdGVkLWtleSc7XHJcbmltcG9ydCAnLi9leHRlbmQvdmFsaWRhdGUtZXh0ZW5kJztcclxuaW1wb3J0IHZhbGlkYXRlSGFuZGxlcnMgZnJvbSAnLi92YWxpZGF0ZS1oYW5kbGVycyc7XHJcbmltcG9ydCB2YWxpZGF0ZUdldFJlc3BvbnNlIGZyb20gJy4vdmFsaWRhdGUtZ2V0LXJlc3BvbnNlJztcclxuaW1wb3J0IFByZWxvYWRlciBmcm9tICcjL19tb2R1bGVzL1ByZWxvYWRlcic7XHJcbmltcG9ydCB7bWZwQWpheH0gZnJvbSBcIi4uL21hZ25pZmljLXBvcHVwL21mcC1pbml0XCI7XHJcblxyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbi8vIFByaXZhdGVcclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuLyoqXHJcbiAqIEBjb25zdCB7T2JqZWN0fVxyXG4gKiBAcHJpdmF0ZVxyXG4gKiBAc291cmNlQ29kZVxyXG4gKi9cclxuY29uc3QgY29uZmlnRGVmYXVsdCA9IHtcclxuXHRpZ25vcmU6ICc6aGlkZGVuLCAuanMtaWdub3JlJyxcclxuXHRnZXQgY2xhc3NlcyAoKSB7XHJcblx0XHRyZXR1cm4ge1xyXG5cdFx0XHRlcnJvcjogJ2hhcy1lcnJvcicsXHJcblx0XHRcdHZhbGlkOiAnaXMtdmFsaWQnLFxyXG5cdFx0XHRsYWJlbEVycm9yOiAnbGFiZWwtZXJyb3InLFxyXG5cdFx0XHRmb3JtRXJyb3I6ICdmb3JtLS1lcnJvcicsXHJcblx0XHRcdGZvcm1WYWxpZDogJ2Zvcm0tLXZhbGlkJyxcclxuXHRcdFx0Zm9ybVBlbmRpbmc6ICdmb3JtLS1wZW5kaW5nJ1xyXG5cdFx0fTtcclxuXHR9LFxyXG5cdGdldCBlcnJvckNsYXNzICgpIHtcclxuXHRcdHJldHVybiB0aGlzLmNsYXNzZXMuZXJyb3I7XHJcblx0fSxcclxuXHRnZXQgdmFsaWRDbGFzcyAoKSB7XHJcblx0XHRyZXR1cm4gdGhpcy5jbGFzc2VzLmVycm9yO1xyXG5cdH0sXHJcblxyXG5cdC8qKlxyXG5cdCAqINCS0LDQu9C40LTQuNGA0L7QstCw0YLRjCDRjdC70LXQvNC10L3RgtGLINC/0YDQuCDQv9C+0YLQtdGA0LUg0YTQvtC60YPRgdCwLlxyXG5cdCAqINCY0LvQuCBmYWxzZSDQuNC70Lgg0YTRg9C90LrRhtC40Y9cclxuXHQgKiBAdHlwZSB7Qm9vbGVhbnxGdW5jdGlvbn1cclxuXHQgKiBAcHJvcCB7SFRNTEVsZW1lbnR9IGVsZW1lbnRcclxuXHQgKiBAcHJvcCB7RXZlbnR9IGV2ZW50XHJcblx0ICogQHNlZSB7QGxpbmsgaHR0cHM6Ly9qcXVlcnl2YWxpZGF0aW9uLm9yZy92YWxpZGF0ZS8jb25mb2N1c291dH1cclxuXHQgKi9cclxuXHRvbmZvY3Vzb3V0IChlbGVtZW50KSB7XHJcblx0XHRpZiAoZWxlbWVudC52YWx1ZS5sZW5ndGggfHwgZWxlbWVudC5jbGFzc0xpc3QuY29udGFpbnModGhpcy5zZXR0aW5ncy5jbGFzc2VzLmVycm9yKSkge1xyXG5cdFx0XHR0aGlzLmVsZW1lbnQoZWxlbWVudCk7XHJcblx0XHR9XHJcblx0fSxcclxuXHJcblx0LyoqXHJcblx0ICog0JLQsNC70LjQtNC40YDQvtCy0LDRgtGMINGN0LvQtdC80LXQvdGC0Ysg0L/RgNC4IGtleXVwLlxyXG5cdCAqINCY0LvQuCBmYWxzZSDQuNC70Lgg0YTRg9C90LrRhtC40Y9cclxuXHQgKiBAdHlwZSB7Qm9vbGVhbnxGdW5jdGlvbn1cclxuXHQgKiBAcHJvcCB7SFRNTEVsZW1lbnR9IGVsZW1lbnRcclxuXHQgKiBAcHJvcCB7RXZlbnR9IGV2ZW50XHJcblx0ICogQHNlZSB7QGxpbmsgaHR0cHM6Ly9qcXVlcnl2YWxpZGF0aW9uLm9yZy92YWxpZGF0ZS8jb25rZXl1cH1cclxuXHQgKi9cclxuXHRvbmtleXVwIChlbGVtZW50KSB7XHJcblx0XHRpZiAoZWxlbWVudC5jbGFzc0xpc3QuY29udGFpbnModGhpcy5zZXR0aW5ncy5jbGFzc2VzLmVycm9yKSkge1xyXG5cdFx0XHR0aGlzLmVsZW1lbnQoZWxlbWVudCk7XHJcblx0XHR9XHJcblx0fSxcclxuXHJcblx0LyoqXHJcblx0ICog0J/QvtC00YHQstC10YLQutCwINGN0LvQtdC80LXQvdGC0L7QsiDRgSDQvtGI0LjQsdC60LDQvNC4XHJcblx0ICogQHBhcmFtIHtIVE1MRWxlbWVudH0gZWxlbWVudFxyXG5cdCAqL1xyXG5cdGhpZ2hsaWdodCAoZWxlbWVudCkge1xyXG5cdFx0ZWxlbWVudC5jbGFzc0xpc3QucmVtb3ZlKHRoaXMuc2V0dGluZ3MuY2xhc3Nlcy52YWxpZCk7XHJcblx0XHRlbGVtZW50LmNsYXNzTGlzdC5hZGQodGhpcy5zZXR0aW5ncy5jbGFzc2VzLmVycm9yKTtcclxuXHR9LFxyXG5cclxuXHQvKipcclxuXHQgKiDQo9C00LDQu9C10L3QuNC1INC/0L7QtNGB0LLQtdGC0LrQuCDRjdC70LXQvNC10L3RgtC+0LIg0YEg0L7RiNC40LHQutCw0LzQuFxyXG5cdCAqIEBwYXJhbSB7SFRNTEVsZW1lbnR9IGVsZW1lbnRcclxuXHQgKi9cclxuXHR1bmhpZ2hsaWdodCAoZWxlbWVudCkge1xyXG5cdFx0ZWxlbWVudC5jbGFzc0xpc3QucmVtb3ZlKHRoaXMuc2V0dGluZ3MuY2xhc3Nlcy5lcnJvcik7XHJcblx0XHRlbGVtZW50LmNsYXNzTGlzdC5hZGQodGhpcy5zZXR0aW5ncy5jbGFzc2VzLnZhbGlkKTtcclxuXHR9LFxyXG5cclxuXHQvKipcclxuXHQgKiDQntCx0YDQsNCx0L7RgtGH0LjQuiDRgdCw0LHQvNC40YLQsCDRhNC+0YDQvNGLXHJcblx0ICogQHBhcmFtIHtIVE1MRm9ybUVsZW1lbnR9IGZvcm1cclxuXHQgKiBAcmV0dXJucyB7Ym9vbGVhbn1cclxuXHQgKi9cclxuXHRzdWJtaXRIYW5kbGVyIChmb3JtKSB7XHJcblx0XHRsZXQgJGZvcm0gPSAkKGZvcm0pO1xyXG5cdFx0bGV0IGFjdGlvblVybCA9ICRmb3JtLmRhdGEoJ2FqYXgnKTtcclxuXHJcblx0XHRpZiAoIWFjdGlvblVybCkge1xyXG5cdFx0XHRyZXR1cm4gdHJ1ZTtcclxuXHRcdH1cclxuXHJcblx0XHRsZXQgcHJlbG9hZGVyID0gbmV3IFByZWxvYWRlcigkZm9ybSk7XHJcblx0XHRwcmVsb2FkZXIuc2hvdygpO1xyXG5cclxuXHRcdGxldCBmb3JtRGF0YSA9IG5ldyB3aW5kb3cuRm9ybURhdGEoKTtcclxuXHRcdGZvcm1EYXRhLmFwcGVuZCgneGhyLWxhbmcnLCAkKCdodG1sJykuYXR0cignbGFuZycpIHx8ICdydScpO1xyXG5cclxuXHRcdC8vINC40LPQvdC+0YDQuNGA0YPQtdC80YvQtSDRgtC40L/RiyDQuNC90L/Rg9GC0L7QslxyXG5cdFx0bGV0IGlnbm9yZWRJbnB1dHNUeXBlID0gW1xyXG5cdFx0XHQnc3VibWl0JyxcclxuXHRcdFx0J3Jlc2V0JyxcclxuXHRcdFx0J2J1dHRvbicsXHJcblx0XHRcdCdpbWFnZSdcclxuXHRcdF07XHJcblxyXG5cdFx0JGZvcm0uZmluZCgnaW5wdXQsIHRleHRhcmVhLCBzZWxlY3QnKS5lYWNoKChpLCBlbGVtZW50KSA9PiB7XHJcblx0XHRcdGxldCB7dmFsdWUsIHR5cGV9ID0gZWxlbWVudDtcclxuXHRcdFx0aWYgKH5pZ25vcmVkSW5wdXRzVHlwZS5pbmRleE9mKHR5cGUpKSB7XHJcblx0XHRcdFx0cmV0dXJuIHRydWU7XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdGxldCAkZWxlbWVudCA9ICQoZWxlbWVudCk7XHJcblx0XHRcdGxldCBlbGVtZW50Tm9kZSA9ICRlbGVtZW50Lm5vZGVOYW1lKCk7XHJcblx0XHRcdGxldCBlbGVtZW50TmFtZSA9ICRlbGVtZW50LmRhdGEoJ25hbWUnKSB8fCBudWxsO1xyXG5cclxuXHRcdFx0aWYgKGVsZW1lbnROYW1lID09PSBudWxsKSB7XHJcblx0XHRcdFx0cmV0dXJuIHRydWU7XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdHN3aXRjaCAoZWxlbWVudE5vZGUpIHtcclxuXHRcdFx0XHRjYXNlICdpbnB1dCc6XHJcblx0XHRcdFx0XHRpZiAodHlwZSA9PT0gJ2ZpbGUnICYmIGVsZW1lbnQuZmlsZXMgJiYgZWxlbWVudC5maWxlcy5sZW5ndGgpIHtcclxuXHRcdFx0XHRcdFx0Zm9yIChsZXQgaSA9IDA7IGkgPCBlbGVtZW50LmZpbGVzLmxlbmd0aDsgaSsrKSB7XHJcblx0XHRcdFx0XHRcdFx0bGV0IGZpbGUgPSBlbGVtZW50LmZpbGVzW2ldO1xyXG5cdFx0XHRcdFx0XHRcdGZvcm1EYXRhLmFwcGVuZChlbGVtZW50TmFtZSwgZmlsZSk7XHJcblx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0XHRcdGlmICgodHlwZSA9PT0gJ2NoZWNrYm94JyB8fCB0eXBlID09PSAncmFkaW8nKSAmJiAhZWxlbWVudC5jaGVja2VkKSB7XHJcblx0XHRcdFx0XHRcdFx0YnJlYWs7XHJcblx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdFx0Zm9ybURhdGEuYXBwZW5kKGVsZW1lbnROYW1lLCB2YWx1ZSk7XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRicmVhaztcclxuXHJcblx0XHRcdFx0Y2FzZSAndGV4dGFyZWEnOlxyXG5cdFx0XHRcdFx0Zm9ybURhdGEuYXBwZW5kKGVsZW1lbnROYW1lLCB2YWx1ZSk7XHJcblx0XHRcdFx0XHRicmVhaztcclxuXHJcblx0XHRcdFx0Y2FzZSAnc2VsZWN0JzpcclxuXHRcdFx0XHRcdGxldCBtdWx0aU5hbWUgPSAvXFxbXFxdJC87XHJcblx0XHRcdFx0XHRpZiAoKG11bHRpTmFtZS50ZXN0KGVsZW1lbnROYW1lKSB8fCBlbGVtZW50Lm11bHRpcGxlKSAmJiBlbGVtZW50LnNlbGVjdGVkT3B0aW9ucyAmJiBlbGVtZW50LnNlbGVjdGVkT3B0aW9ucy5sZW5ndGgpIHtcclxuXHRcdFx0XHRcdFx0Zm9yIChsZXQgaSA9IDA7IGkgPCBlbGVtZW50LnNlbGVjdGVkT3B0aW9ucy5sZW5ndGg7IGkrKykge1xyXG5cdFx0XHRcdFx0XHRcdGxldCBvcHRpb24gPSBlbGVtZW50LnNlbGVjdGVkT3B0aW9uc1tpXTtcclxuXHRcdFx0XHRcdFx0XHRpZiAob3B0aW9uLmRpc2FibGVkKSB7XHJcblx0XHRcdFx0XHRcdFx0XHRjb250aW51ZTtcclxuXHRcdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRcdFx0Zm9ybURhdGEuYXBwZW5kKGVsZW1lbnROYW1lLCBvcHRpb24udmFsdWUpO1xyXG5cdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdFx0XHRmb3JtRGF0YS5hcHBlbmQoZWxlbWVudE5hbWUsIHZhbHVlKTtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdGJyZWFrO1xyXG5cdFx0XHR9XHJcblx0XHR9KTtcclxuXHJcblx0XHRsZXQgeGhyID0gbmV3IHdpbmRvdy5YTUxIdHRwUmVxdWVzdCgpO1xyXG5cdFx0eGhyLm9wZW4oJ1BPU1QnLCBhY3Rpb25VcmwpO1xyXG5cdFx0eGhyLm9ucmVhZHlzdGF0ZWNoYW5nZSA9IGZ1bmN0aW9uICgpIHtcclxuXHRcdFx0aWYgKHhoci5yZWFkeVN0YXRlID09PSA0KSB7XHJcblx0XHRcdFx0bGV0IHtzdGF0dXMsIHN0YXR1c1RleHQsIHJlc3BvbnNlfSA9IHhocjtcclxuXHRcdFx0XHQvLyDQstGB0LUg0L/Qu9C+0YXQvlxyXG5cdFx0XHRcdGlmIChzdGF0dXMgIT09IDIwMCkge1xyXG5cdFx0XHRcdFx0Y29uc29sZS5sb2coeGhyKTtcclxuXHRcdFx0XHRcdHByZWxvYWRlci5oaWRlKCk7XHJcblx0XHRcdFx0XHRyZXR1cm4gZmFsc2U7XHJcblx0XHRcdFx0fVxyXG5cclxuXHRcdFx0XHRpZiAodHlwZW9mIHJlc3BvbnNlID09PSAnc3RyaW5nJykge1xyXG5cdFx0XHRcdFx0cmVzcG9uc2UgPSBKU09OLnBhcnNlKHJlc3BvbnNlKTtcclxuXHRcdFx0XHR9XHJcblxyXG5cdFx0XHRcdGlmIChhY3Rpb25VcmwgPT09ICcvLycgKyBsb2NhdGlvbi5ob3N0ICsgJy9hamF4L2NhcnRTaXplcycpIHtcclxuXHRcdFx0XHRcdGxldCAkc2l6ZUNvdW50QmxvY2sgPSAkKCcuanMtcHJvZHVjdC1jb3VudHMnKTtcclxuXHRcdFx0XHRcdGlmKCRzaXplQ291bnRCbG9jay5sZW5ndGgpIHtcclxuXHRcdFx0XHRcdFx0Zm9yIChsZXQgc2l6ZSBpbiByZXNwb25zZS5hY3RpdmVfY291bnQpIHtcclxuXHRcdFx0XHRcdFx0XHRsZXQgJHNpemVCdXR0b24gPSAkc2l6ZUNvdW50QmxvY2suZmluZCgnLmpzLXByb2R1Y3QtY291bnQtaXRlbVtkYXRhLXNpemU9JyArIHNpemUgKyAnXScpO1xyXG5cdFx0XHRcdFx0XHRcdCRzaXplQnV0dG9uLmZpbmQoJ3NwYW4nKS5maXJzdCgpLnJlbW92ZUNsYXNzKCdwcm9kdWN0X19zaXplLXRleHQnKS5hZGRDbGFzcygnc2l6ZV9faXRlbScpO1xyXG5cdFx0XHRcdFx0XHRcdCRzaXplQnV0dG9uLmZpbmQoJy5qcy1wcm9kdWN0LWNvdW50JykucmVtb3ZlQ2xhc3MoJ3Byb2R1Y3RfX3NpemUtY291bnQnKS5hZGRDbGFzcygnc2l6ZV9fY291bnQnKS50ZXh0KHJlc3BvbnNlLmFjdGl2ZV9jb3VudFtzaXplXSk7XHJcblx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHR9XHJcblxyXG5cdFx0XHRcdHZhbGlkYXRlR2V0UmVzcG9uc2UoJGZvcm0sIHJlc3BvbnNlLCBzdGF0dXNUZXh0LCB4aHIpO1xyXG5cdFx0XHR9XHJcblx0XHR9O1xyXG5cclxuXHRcdHhoci5zZW5kKGZvcm1EYXRhKTtcclxuXHRcdHJldHVybiBmYWxzZTtcclxuXHR9XHJcbn07XHJcblxyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbi8vIFB1YmxpY1xyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG4vKipcclxuICogQHBhcmFtIHtKUXVlcnl9ICRlbGVtZW50c1xyXG4gKi9cclxuZnVuY3Rpb24gdmFsaWRhdGUgKCRlbGVtZW50cykge1xyXG5cdCRlbGVtZW50cy5lYWNoKChpLCBlbCkgPT4ge1xyXG5cdFx0Y29uc3QgJGZvcm0gPSAkKGVsKTtcclxuXHRcdGlmICgkZm9ybS5oYXNJbml0ZWRLZXkoJ3ZhbGlkYXRlLWluaXRlZCcpKSB7XHJcblx0XHRcdHJldHVybiB0cnVlO1xyXG5cdFx0fVxyXG5cclxuXHRcdGNvbnN0IGNvbmZpZyA9ICQuZXh0ZW5kKHRydWUsIHt9LCBjb25maWdEZWZhdWx0LCB7XHJcblx0XHRcdC8vIGN1cnJlbnQgZWxlbWVudCBjdXN0b20gY29uZmlnXHJcblx0XHR9KTtcclxuXHJcblx0XHQkZm9ybS52YWxpZGF0ZShjb25maWcpO1xyXG5cdFx0dmFsaWRhdGVIYW5kbGVycygkZm9ybSwgJGZvcm0uZGF0YSgndmFsaWRhdG9yJykpO1xyXG5cdH0pO1xyXG59XHJcblxyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbi8vIEV4cG9ydHNcclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgdmFsaWRhdGU7XHJcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL19IVE1ML3NyYy9qcy9fbW9kdWxlcy9qcXVlcnktdmFsaWRhdGlvbi92YWxpZGF0ZS1pbml0LmpzIiwiJ3VzZSBzdHJpY3QnO1xyXG5cclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4vLyBJbXBvcnRzXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbmltcG9ydCAnc2xpY2stY2Fyb3VzZWwnO1xyXG5cclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4vLyBQcml2YXRlXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbmxldCAkbWFpblNsaWRlciA9ICQoJy5tYWluLXNsaWRlcicpO1xyXG5sZXQgJHByb2R1Y3RTbGlkZXIgPSAkKCcucHJvZHVjdC1zbGlkZXInKTtcclxuLy8gbGV0ICRzaW1pbGFyU2xpZGVyID0gJCgnLnNpbWlsYXItc2xpZGVyJyk7XHJcbmxldCAkY29udGFpbmVyID0gJCgnLmNvbnRhaW5lcicpO1xyXG5sZXQgJGRvdHNDb250cm9scyA9ICQoJy5tYWluLXNsaWRlcl9fY29udHJvbHMnKTtcclxubGV0ICRkb3RzSG9sZGVyID0gJCgnLm1haW4tc2xpZGVyX19jb250cm9scy1ob2xkZXInKTtcclxubGV0ICRwcm9kdWN0UGhvdG9TbGlkZXIgPSAkKCcucHJvZHVjdF9fZ2FsbGVyeS1tYWluJyk7XHJcbmxldCAkcHJvZHVjdE5hdlNsaWRlciA9ICQoJy5nYWxsZXJ5LXByZXZpZXdfX3dyYXAnKTtcclxuXHJcbmZ1bmN0aW9uIGRvdHNQb3NpdGlvbiAodGhhdCkge1xyXG5cdGxldCBwYWRkaW5nID0gKCQod2luZG93KS53aWR0aCgpIC0gJGNvbnRhaW5lci5vdXRlcldpZHRoKCkpIC8gMjtcclxuXHRsZXQgZG90c1Bvc1RvcCA9ICRkb3RzSG9sZGVyLnBvc2l0aW9uKCkudG9wO1xyXG5cclxuXHR0aGF0LmZpbmQoJy5zbGljay1saXN0JykuY3NzKCdwYWRkaW5nLWxlZnQnLCBwYWRkaW5nKTtcclxuXHQkZG90c0NvbnRyb2xzLmNzcygndG9wJywgZG90c1Bvc1RvcCkuY3NzKCdsZWZ0JywgcGFkZGluZyk7XHJcbn1cclxuXHJcbmZ1bmN0aW9uIHNldFBhZGRpbmcgKHRoYXQpIHtcclxuXHRpZiAoIXRoYXQuaGFzQ2xhc3MoJ3NtYWxsJykpIHtcclxuXHRcdGxldCBwYWRkaW5nID0gKCQod2luZG93KS53aWR0aCgpID4gMTAyMykgPyBwYXJzZUludCgoJCh3aW5kb3cpLndpZHRoKCkgLSAkY29udGFpbmVyLm91dGVyV2lkdGgoKSkgLyAyKSA6IDA7XHJcblx0XHR0aGF0LmZpbmQoJy5zbGljay1saXN0JykuY3NzKCdwYWRkaW5nLWxlZnQnLCBwYWRkaW5nKS5jc3MoJ3BhZGRpbmctcmlnaHQnLCBwYWRkaW5nKTtcclxuXHR9XHJcbn1cclxuXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuLy8gUHVibGljXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbmZ1bmN0aW9uIG1haW5TbGlkZXIgKCkge1xyXG5cdGlmICghJG1haW5TbGlkZXIubGVuZ3RoKSB7XHJcblx0XHRyZXR1cm4gZmFsc2U7XHJcblx0fVxyXG5cclxuXHQkbWFpblNsaWRlci5vbignaW5pdCcsIGZ1bmN0aW9uIChldmVudCwgc2xpY2ssIGRpcmVjdGlvbikge1xyXG5cdFx0ZG90c1Bvc2l0aW9uKCQodGhpcykpO1xyXG5cdH0pO1xyXG5cclxuXHQkbWFpblNsaWRlci5zbGljayh7XHJcblx0XHRzbGlkZXNUb1Nob3c6IDEsXHJcblx0XHRzbGlkZXNUb1Njcm9sbDogMSxcclxuXHRcdGluZmluaXRlOiB0cnVlLFxyXG5cdFx0ZG90czogdHJ1ZSxcclxuXHRcdGFycm93czogZmFsc2UsXHJcblx0XHRhdXRvcGxheTogZmFsc2UsXHJcblx0XHRmYWRlOiB0cnVlLFxyXG5cdFx0YXBwZW5kRG90czogJCgnLm1haW4tc2xpZGVyX19jb250cm9scycpLFxyXG5cdFx0Y3VzdG9tUGFnaW5nOiBmdW5jdGlvbiAoc2xpZGVyLCBpKSB7XHJcblx0XHRcdGxldCBudW0gPSAoKGkgKyAxKSA8PSA5KSA/ICcwJy5jb25jYXQoaSArIDEpIDogKGkgKyAxKTtcclxuXHRcdFx0cmV0dXJuIGA8YSBjbGFzcz1cIm1haW4tc2xpZGVyX19kb3RcIj5gICsgbnVtICsgYDwvYT5gO1xyXG5cdFx0fSxcclxuXHRcdHByZXZBcnJvdzogYDxidXR0b24gdHlwZT0nYnV0dG9uJyBjbGFzcz0nc2xpY2stYXJyb3cgc2xpY2stcHJldic+PC9idXR0b24+YCxcclxuXHRcdG5leHRBcnJvdzogYDxidXR0b24gdHlwZT0nYnV0dG9uJyBjbGFzcz0nc2xpY2stYXJyb3cgc2xpY2stbmV4dCc+PC9idXR0b24+YCxcclxuXHRcdHJlc3BvbnNpdmU6IFtcclxuXHRcdFx0e1xyXG5cdFx0XHRcdGJyZWFrcG9pbnQ6IDc2OCxcclxuXHRcdFx0XHRzZXR0aW5nczoge1xyXG5cdFx0XHRcdFx0YXV0b3BsYXk6IHRydWUsXHJcblx0XHRcdFx0XHRwYXVzZU9uSG92ZXI6IGZhbHNlXHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9XHJcblx0XHRdXHJcblx0fSk7XHJcblxyXG5cdCQod2luZG93KS5yZXNpemUoZnVuY3Rpb24gKCkge1xyXG5cdFx0ZG90c1Bvc2l0aW9uKCRtYWluU2xpZGVyKTtcclxuXHR9KTtcclxufVxyXG5cclxuZnVuY3Rpb24gcHJvZHVjdEltZ1NsaWRlciAoKSB7XHJcblx0bGV0ICRwcm9kdWN0SW1nU2xpZGVyID0gJCgnLnByb2R1Y3QtaW1nLXNsaWRlcicpO1xyXG5cclxuXHRpZiAoISRwcm9kdWN0SW1nU2xpZGVyLmxlbmd0aCkge1xyXG5cdFx0cmV0dXJuIGZhbHNlO1xyXG5cdH1cclxuXHQkcHJvZHVjdEltZ1NsaWRlci5ub3QoJy5zbGljay1pbml0aWFsaXplZCcpLmVhY2goZnVuY3Rpb24gKGksZSkge1xyXG5cdFx0bGV0ICR0aGlzID0gJChlKTtcclxuXHJcblx0XHQkdGhpcy5zbGljayh7XHJcblx0XHRcdHNsaWRlc1RvU2hvdzogMSxcclxuXHRcdFx0c2xpZGVzVG9TY3JvbGw6IDEsXHJcblx0XHRcdGluZmluaXRlOiBmYWxzZSxcclxuXHRcdFx0ZG90czogZmFsc2UsXHJcblx0XHRcdGFycm93czogdHJ1ZSxcclxuXHRcdFx0YXV0b3BsYXk6IGZhbHNlLFxyXG5cdFx0XHRmYWRlOiB0cnVlLFxyXG5cdFx0XHRhcHBlbmRBcnJvd3M6ICR0aGlzLnBhcmVudCgpLmZpbmQoJy5wcm9kdWN0LWl0ZW1fX2J1dHRvbnMtYm94JyksXHJcblx0XHRcdHByZXZBcnJvdzogJzxidXR0b24gdHlwZT1cXCdidXR0b25cXCcgY2xhc3M9XFwnc2xpY2stYXJyb3cgc2xpY2stcHJldlxcJz48L2J1dHRvbj4nLFxyXG5cdFx0XHRuZXh0QXJyb3c6ICc8YnV0dG9uIHR5cGU9XFwnYnV0dG9uXFwnIGNsYXNzPVxcJ3NsaWNrLWFycm93IHNsaWNrLW5leHRcXCc+PC9idXR0b24+JyxcclxuXHJcblx0XHRcdC8vIHJlc3BvbnNpdmU6IFt7XHJcblx0XHRcdC8vIFx0YnJlYWtwb2ludDogNzY4LFxyXG5cdFx0XHQvLyBcdHNldHRpbmdzOiB7XHJcblx0XHRcdC8vIFx0XHRhdXRvcGxheTogdHJ1ZSxcclxuXHRcdFx0Ly8gXHRcdHBhdXNlT25Ib3ZlcjogZmFsc2VcclxuXHRcdFx0Ly8gXHR9XHJcblx0XHRcdC8vIH1dXHJcblx0XHR9KTtcclxuXHJcblx0fSk7XHJcblxyXG59XHJcblxyXG5mdW5jdGlvbiBwcm9kdWN0U2xpZGVyICgpIHtcclxuXHRpZiAoISRwcm9kdWN0U2xpZGVyLmxlbmd0aCkge1xyXG5cdFx0cmV0dXJuIGZhbHNlO1xyXG5cdH1cclxuXHJcblx0JHByb2R1Y3RTbGlkZXIuZWFjaChmdW5jdGlvbiAoaSkge1xyXG5cdFx0bGV0ICR0aGlzID0gJCh0aGlzKTtcclxuXHJcblx0XHQkdGhpcy5vbignaW5pdCcsIGZ1bmN0aW9uIChldmVudCwgc2xpY2ssIGRpcmVjdGlvbikge1xyXG5cdFx0XHRzZXRQYWRkaW5nKCR0aGlzKTtcclxuXHRcdH0pO1xyXG5cclxuXHRcdCR0aGlzLnNsaWNrKHtcclxuXHRcdFx0c2xpZGVzVG9TaG93OiA0LFxyXG5cdFx0XHRzbGlkZXNUb1Njcm9sbDogMSxcclxuXHRcdFx0aW5maW5pdGU6IGZhbHNlLFxyXG5cdFx0XHRhcnJvd3M6IHRydWUsXHJcblx0XHRcdGRvdHM6IHRydWUsXHJcblx0XHRcdGFwcGVuZERvdHM6ICR0aGlzLnBhcmVudCgpLmZpbmQoJy5wcm9kdWN0LXNsaWRlcl9fY291bnRlcicpLFxyXG5cdFx0XHRjdXN0b21QYWdpbmc6IGZ1bmN0aW9uIChzbGlkZXIsIGkpIHtcclxuXHRcdFx0XHRsZXQgbnVtID0gaSArIDE7XHJcblx0XHRcdFx0cmV0dXJuICc8c3BhbiBjbGFzcz1cImZpcnN0LW51bVwiPicgKyBudW0gKyAnPC9zcGFuPjxzcGFuIGNsYXNzPVwic2Vjb25kLW51bVwiPiAvICcgKyBzbGlkZXIuc2xpZGVDb3VudCArICc8L3NwYW4+JztcclxuXHRcdFx0fSxcclxuXHRcdFx0YXBwZW5kQXJyb3dzOiAkdGhpcy5wYXJlbnQoKS5maW5kKCcucHJvZHVjdC1zbGlkZXJfX2J1dHRvbnMtYm94JyksXHJcblx0XHRcdHByZXZBcnJvdzogYDxidXR0b24gdHlwZT0nYnV0dG9uJyBjbGFzcz0nc2xpY2stYXJyb3cgc2xpY2stcHJldic+PC9idXR0b24+YCxcclxuXHRcdFx0bmV4dEFycm93OiBgPGJ1dHRvbiB0eXBlPSdidXR0b24nIGNsYXNzPSdzbGljay1hcnJvdyBzbGljay1uZXh0Jz48L2J1dHRvbj5gLFxyXG5cdFx0XHRyZXNwb25zaXZlOiBbXHJcblx0XHRcdFx0e1xyXG5cdFx0XHRcdFx0YnJlYWtwb2ludDogMTAyNSxcclxuXHRcdFx0XHRcdHNldHRpbmdzOiB7XHJcblx0XHRcdFx0XHRcdHNsaWRlc1RvU2hvdzogM1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdH0sXHJcblx0XHRcdFx0e1xyXG5cdFx0XHRcdFx0YnJlYWtwb2ludDogNzY4LFxyXG5cdFx0XHRcdFx0c2V0dGluZ3M6IHtcclxuXHRcdFx0XHRcdFx0c2xpZGVzVG9TaG93OiAyXHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0fSwge1xyXG5cdFx0XHRcdFx0YnJlYWtwb2ludDogNjQwLFxyXG5cdFx0XHRcdFx0c2V0dGluZ3M6IHtcclxuXHRcdFx0XHRcdFx0c2xpZGVzVG9TaG93OiAyXHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0fSwge1xyXG5cdFx0XHRcdFx0YnJlYWtwb2ludDogNDgwLFxyXG5cdFx0XHRcdFx0c2V0dGluZ3M6IHtcclxuXHRcdFx0XHRcdFx0c2xpZGVzVG9TaG93OiAxXHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0fVxyXG5cdFx0XHRdXHJcblx0XHR9KTtcclxuXHJcblx0XHQkKHdpbmRvdykucmVzaXplKGZ1bmN0aW9uICgpIHtcclxuXHRcdFx0c2V0UGFkZGluZygkdGhpcyk7XHJcblx0XHR9KTtcclxuXHR9KTtcclxufVxyXG5cclxuLy8gZnVuY3Rpb24gc2ltaWxhclNsaWRlciAoKSB7XHJcbi8vIFx0aWYgKCEkc2ltaWxhclNsaWRlci5sZW5ndGgpIHtcclxuLy8gXHRcdHJldHVybiBmYWxzZTtcclxuLy8gXHR9XHJcbi8vXHJcbi8vIFx0JHNpbWlsYXJTbGlkZXIuc2xpY2soe1xyXG4vLyBcdFx0c2xpZGVzVG9TaG93OiA0LFxyXG4vLyBcdFx0c2xpZGVzVG9TY3JvbGw6IDEsXHJcbi8vIFx0XHRpbmZpbml0ZTogdHJ1ZSxcclxuLy8gXHRcdGRvdHM6IHRydWUsXHJcbi8vIFx0XHRhcHBlbmREb3RzOiAkc2ltaWxhckNvbnRyb2xzLFxyXG4vLyBcdFx0cHJldkFycm93OiBgPGJ1dHRvbiB0eXBlPSdidXR0b24nIGNsYXNzPSdzbGljay1hcnJvdyBzbGljay1wcmV2Jz48L2J1dHRvbj5gLFxyXG4vLyBcdFx0bmV4dEFycm93OiBgPGJ1dHRvbiB0eXBlPSdidXR0b24nIGNsYXNzPSdzbGljay1hcnJvdyBzbGljay1uZXh0Jz48L2J1dHRvbj5gLFxyXG4vLyBcdFx0cmVzcG9uc2l2ZTogW1xyXG4vLyBcdFx0XHR7XHJcbi8vIFx0XHRcdFx0YnJlYWtwb2ludDogMTAyNSxcclxuLy8gXHRcdFx0XHRzZXR0aW5nczoge1xyXG4vLyBcdFx0XHRcdFx0c2xpZGVzVG9TaG93OiAyXHJcbi8vIFx0XHRcdFx0fVxyXG4vLyBcdFx0XHR9LCB7XHJcbi8vIFx0XHRcdFx0YnJlYWtwb2ludDogNjQxLFxyXG4vLyBcdFx0XHRcdHNldHRpbmdzOiB7XHJcbi8vIFx0XHRcdFx0XHRzbGlkZXNUb1Nob3c6IDFcclxuLy8gXHRcdFx0XHR9XHJcbi8vIFx0XHRcdH1cclxuLy8gXHRcdF1cclxuLy8gXHR9KTtcclxuLy8gfVxyXG5cclxuZnVuY3Rpb24gcHJvZHVjdFBob3RvU2xpZGVyICgpIHtcclxuXHRpZiAoISRwcm9kdWN0UGhvdG9TbGlkZXIubGVuZ3RoKSB7XHJcblx0XHRyZXR1cm4gZmFsc2U7XHJcblx0fVxyXG5cclxuXHRsZXQgcGhvdG9TbGlkZXIgPSAkKCcucHJvZHVjdF9fZ2FsbGVyeS1tYWluJykuc2xpY2soe1xyXG5cdFx0c2xpZGVzVG9TaG93OiAxLFxyXG5cdFx0aW5maW5pdGU6IGZhbHNlLFxyXG5cdFx0ZmFkZTogdHJ1ZSxcclxuXHRcdGRvdHM6IGZhbHNlLFxyXG5cdFx0YXJyb3dzOiBmYWxzZSxcclxuXHRcdGFjY2Vzc2liaWxpdHk6IGZhbHNlLFxyXG5cdFx0YXNOYXZGb3I6ICcuZ2FsbGVyeS1wcmV2aWV3X193cmFwJ1xyXG5cdH0pO1xyXG5cclxuXHQkcHJvZHVjdE5hdlNsaWRlci5zbGljayh7XHJcblx0XHRzbGlkZXNUb1Nob3c6IDQsXHJcblx0XHRzbGlkZXNUb1Njcm9sbDogMSxcclxuXHRcdGFzTmF2Rm9yOiAnLnByb2R1Y3RfX2dhbGxlcnktbWFpbicsXHJcblx0XHRmb2N1c09uU2VsZWN0OiB0cnVlLFxyXG5cdFx0dmVydGljYWw6IHRydWUsXHJcblx0XHRpbmZpbml0ZTogZmFsc2UsXHJcblx0XHRwcmV2QXJyb3c6ICc8YnV0dG9uIHR5cGU9XCJidXR0b25cIiBjbGFzcz1cInNsaWNrLWFycm93IHNsaWNrLXByZXZcIj48L2J1dHRvbj4nLFxyXG5cdFx0bmV4dEFycm93OiAnPGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJzbGljay1hcnJvdyBzbGljay1uZXh0XCI+PC9idXR0b24+JyxcclxuXHJcblx0XHRyZXNwb25zaXZlOiBbXHJcblx0XHRcdHtcclxuXHRcdFx0XHRicmVha3BvaW50OiA3NjgsXHJcblx0XHRcdFx0c2V0dGluZ3M6IHtcclxuXHRcdFx0XHRcdHZlcnRpY2FsOiBmYWxzZSxcclxuXHRcdFx0XHRcdHNsaWRlc1RvU2hvdzogNixcclxuXHRcdFx0XHRcdGluZmluaXRlOiB0cnVlXHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9LCB7XHJcblx0XHRcdFx0YnJlYWtwb2ludDogNDgwLFxyXG5cdFx0XHRcdHNldHRpbmdzOiB7XHJcblx0XHRcdFx0XHR2ZXJ0aWNhbDogZmFsc2UsXHJcblx0XHRcdFx0XHRzbGlkZXNUb1Nob3c6IDMsXHJcblx0XHRcdFx0XHRpbmZpbml0ZTogdHJ1ZVxyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cdFx0XVxyXG5cdH0pO1xyXG5cclxuXHRwaG90b1NsaWRlci5vbignYmVmb3JlQ2hhbmdlJywgZnVuY3Rpb24gKGV2ZW50LCBzbGljaywgY3VycmVudFNsaWRlLCBuZXh0U2xpZGUpIHtcclxuXHJcblx0fSk7XHJcbn1cclxuXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuLy8gRXhwb3J0c1xyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG5leHBvcnQge21haW5TbGlkZXIsIHByb2R1Y3RTbGlkZXIsIHByb2R1Y3RQaG90b1NsaWRlciwgcHJvZHVjdEltZ1NsaWRlcn07XHJcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL19IVE1ML3NyYy9qcy9fbW9kdWxlcy9zbGlkZXJzLmpzIiwiJ3VzZSBzdHJpY3QnO1xyXG5cclxuLyoqXHJcbiAqINCc0L7QtNGD0LvRjCDQtNC70Y8g0YDQsNCx0L7RgtGLINGBINC60YPQutC4XHJcbiAqIEBtb2R1bGVcclxuICovXHJcblxyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbi8vIFByaXZhdGVcclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuY29uc3Qge1xyXG5cdGVuY29kZVVSSUNvbXBvbmVudDogZW5jb2RlcixcclxuXHRkZWNvZGVVUklDb21wb25lbnQ6IGRlY29kZXJcclxufSA9IHdpbmRvdztcclxuXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuLy8gUHVibGljXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbi8qKlxyXG4gKiDQoNCw0LHQvtGC0LAg0YEgYGRvY3VtZW50LmNvb2tpZWBcclxuICogQG5hbWVzcGFjZSBjb29raWVEYXRhXHJcbiAqL1xyXG5jb25zdCBjb29raWVEYXRhID0ge1xyXG5cdC8qKlxyXG5cdCAqINCU0L7QsdCw0LLQu9C10L3QuNC1INC30LDQv9C40YHQuFxyXG5cdCAqIEBwYXJhbSB7c3RyaW5nfSBrZXlcclxuXHQgKiBAcGFyYW0geyp9IHZhbHVlXHJcblx0ICogQHBhcmFtIHtPYmplY3R9IFtvcHRpb25zPXt9XVxyXG5cdCAqIEBwYXJhbSB7RGF0ZXxudW1iZXJ8c3RyaW5nfSBbb3B0aW9ucy5leHBpcmVzXVxyXG5cdCAqIEBwYXJhbSB7c3RyaW5nfSBbb3B0aW9ucy5wYXRoXVxyXG5cdCAqIEBwYXJhbSB7c3RyaW5nfSBbb3B0aW9ucy5kb21haW5dXHJcblx0ICogQHBhcmFtIHtib29sZWFufSBbb3B0aW9ucy5zZWN1cmVdXHJcblx0ICogQHNlZSB7QGxpbmsgaHR0cHM6Ly9sZWFybi5qYXZhc2NyaXB0LnJ1L2Nvb2tpZSPRhNGD0L3QutGG0LjRjy1zZXRjb29raWUtbmFtZS12YWx1ZS1vcHRpb25zfVxyXG5cdCAqIEBzb3VyY2VDb2RlXHJcblx0ICovXHJcblx0YWRkIChrZXksIHZhbHVlLCBvcHRpb25zID0ge30pIHtcclxuXHRcdC8vIGlmICghdGhpcy5hbGxvd1VzYWdlICYmIChrZXkgIT09IHRoaXMuYWxsb3dVc2FnZUtleSkpIHtcclxuXHRcdC8vIFx0Y29uc29sZS53YXJuKCdDb29raWUgdXNhZ2UgaXMgZGlzYWxsb3dlZCEnKTtcclxuXHRcdC8vIFx0cmV0dXJuIGZhbHNlO1xyXG5cdFx0Ly8gfVxyXG5cdFx0bGV0IGV4cGlyZXMgPSBvcHRpb25zLmV4cGlyZXM7XHJcblxyXG5cdFx0aWYgKHR5cGVvZiBleHBpcmVzID09PSAnbnVtYmVyJyAmJiBleHBpcmVzKSB7XHJcblx0XHRcdGxldCBkID0gbmV3IERhdGUoKTtcclxuXHRcdFx0ZC5zZXRUaW1lKGQuZ2V0VGltZSgpICsgZXhwaXJlcyAqIDEwMDApO1xyXG5cdFx0XHRleHBpcmVzID0gb3B0aW9ucy5leHBpcmVzID0gZDtcclxuXHRcdH1cclxuXHJcblx0XHRpZiAoZXhwaXJlcyAmJiBleHBpcmVzLnRvVVRDU3RyaW5nKSB7XHJcblx0XHRcdG9wdGlvbnMuZXhwaXJlcyA9IGV4cGlyZXMudG9VVENTdHJpbmcoKTtcclxuXHRcdH1cclxuXHJcblx0XHR2YWx1ZSA9IGVuY29kZXIodmFsdWUpO1xyXG5cdFx0bGV0IHVwZGF0ZWRDb29raWUgPSBgJHtrZXl9PSR7dmFsdWV9YDtcclxuXHJcblx0XHRmb3IgKGxldCBwcm9wTmFtZSBpbiBvcHRpb25zKSB7XHJcblx0XHRcdHVwZGF0ZWRDb29raWUgKz0gYDsgJHtwcm9wTmFtZX1gO1xyXG5cdFx0XHRsZXQgcHJvcFZhbHVlID0gb3B0aW9uc1twcm9wTmFtZV07XHJcblx0XHRcdGlmIChwcm9wVmFsdWUgIT09IHRydWUpIHtcclxuXHRcdFx0XHR1cGRhdGVkQ29va2llICs9IGA9JHtwcm9wVmFsdWV9YDtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cclxuXHRcdGRvY3VtZW50LmNvb2tpZSA9IHVwZGF0ZWRDb29raWU7XHJcblx0fSxcclxuXHJcblx0LyoqXHJcblx0ICog0J/QvtC70YPRh9C10L3QuNC1INC60YPQutC4INC/0L4g0LrQu9GO0YfRg1xyXG5cdCAqIEBwYXJhbSBrZXlcclxuXHQgKiBAcmV0dXJuIHtzdHJpbmd8dW5kZWZpbmVkfVxyXG5cdCAqIEBzZWUge0BsaW5rIGh0dHBzOi8vbGVhcm4uamF2YXNjcmlwdC5ydS9jb29raWUj0YTRg9C90LrRhtC40Y8tZ2V0Y29va2llLW5hbWV9XHJcblx0ICogQHNvdXJjZUNvZGVcclxuXHQgKi9cclxuXHRyZWNlaXZlIChrZXkpIHtcclxuXHRcdGxldCBtYXRjaGVzID0gZG9jdW1lbnQuY29va2llLm1hdGNoKG5ldyBSZWdFeHAoXHJcblx0XHRcdGAoPzpefDsgKSR7a2V5LnJlcGxhY2UoLyhbXFwuJD8qfHt9XFwoXFwpXFxbXFxdXFxcXFxcL1xcK15dKS9nLCAnXFxcXCQxJyl9PShbXjtdKilgIC8vIGVzbGludC1kaXNhYmxlLWxpbmUgbm8tdXNlbGVzcy1lc2NhcGVcclxuXHRcdCkpO1xyXG5cdFx0cmV0dXJuIG1hdGNoZXMgPyBkZWNvZGVyKG1hdGNoZXNbMV0pIDogdW5kZWZpbmVkO1xyXG5cdH0sXHJcblxyXG5cdC8qKlxyXG5cdCAqINCj0LTQsNC70LXQvdC40LUg0LrRg9C60Lgg0L/QviDQutC70Y7Rh9GDXHJcblx0ICogQHBhcmFtIHtzdHJpbmd9IGtleVxyXG5cdCAqIEBzZWUge0BsaW5rIGh0dHBzOi8vbGVhcm4uamF2YXNjcmlwdC5ydS9jb29raWUj0YTRg9C90LrRhtC40Y8tZGVsZXRlY29va2llLW5hbWV9XHJcblx0ICogQHNvdXJjZUNvZGVcclxuXHQgKi9cclxuXHRkZWxldGUgKGtleSkge1xyXG5cdFx0dGhpcy5hZGQoa2V5LCAnJywge2V4cGlyZXM6IC0xfSk7XHJcblx0fSxcclxuXHJcblx0LyoqXHJcblx0ICog0JfQsNC00LDRgtGMINCy0L7Qv9GA0L7RgSDQviDQv9C+0LvRjNC30L7QstCw0YLQtdC70YzRgdC60L7QvCDRgdC+0LPQu9Cw0YHQuNC4INC40YHQv9C+0LvRjNC30LDQstC90LjRjyDQutGD0LrQuFxyXG5cdCAqIEByZXR1cm4ge2Jvb2xlYW59XHJcblx0ICogQHNvdXJjZUNvZGVcclxuXHQgKi9cclxuXHRhc2tVc2FnZSAoKSB7XHJcblx0XHR0aGlzLiRpbmZvQmxvY2sgPSB0aGlzLiRpbmZvQmxvY2sgfHwgJCgnW2RhdGEtY29va2llLWluZm9dJyk7XHJcblx0XHR0aGlzLiRpbmZvQnV0dG9uID0gdGhpcy4kaW5mb0J1dHRvbiB8fCAkKCdbZGF0YS1jb29raWUtc3VibWl0XScpO1xyXG5cclxuXHRcdGlmICh0aGlzLiRpbmZvQmxvY2subGVuZ3RoICYmIHRoaXMuJGluZm9CdXR0b24ubGVuZ3RoKSB7XHJcblx0XHRcdGlmICh0aGlzLmFsbG93VXNhZ2UpIHtcclxuXHRcdFx0XHR0aGlzLiRpbmZvQmxvY2sucmVtb3ZlKCk7XHJcblx0XHRcdFx0cmV0dXJuIGZhbHNlO1xyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHRjb25zdCBoaWRkZW5DbGFzcyA9ICdpcy1oaWRkZW4nO1xyXG5cdFx0XHR0aGlzLiRpbmZvQnV0dG9uLm9uKCdjbGljaycsICgpID0+IHtcclxuXHRcdFx0XHR0aGlzLiRpbmZvQmxvY2suYWRkQ2xhc3MoaGlkZGVuQ2xhc3MpO1xyXG5cdFx0XHRcdHRoaXMuYWRkKHRoaXMuYWxsb3dVc2FnZUtleSwgdGhpcy5hbGxvd1VzYWdlVmFsdWUpO1xyXG5cdFx0XHRcdHdpbmRvdy5zZXRUaW1lb3V0KCgpID0+IHtcclxuXHRcdFx0XHRcdHRoaXMuJGluZm9CbG9jay5yZW1vdmUoKTtcclxuXHRcdFx0XHR9LCB0aGlzLmFsbG93VXNhZ2VEZWxheSk7XHJcblx0XHRcdH0pO1xyXG5cclxuXHRcdFx0d2luZG93LnNldFRpbWVvdXQoKCkgPT4ge1xyXG5cdFx0XHRcdHRoaXMuJGluZm9CbG9jay5yZW1vdmVDbGFzcyhoaWRkZW5DbGFzcyk7XHJcblx0XHRcdH0sIHRoaXMuYWxsb3dVc2FnZURlbGF5KTtcclxuXHRcdFx0cmV0dXJuIHRydWU7XHJcblx0XHR9XHJcblx0fSxcclxuXHJcblx0LyoqXHJcblx0ICog0J/RgNC+0LLQtdGA0LrQsCDQv9C+0LvRjNC30L7QstCw0YLQtdC70YzRgdC60L7Qs9C+INGB0L7Qs9C70LDRgdC40Y9cclxuXHQgKiDQvdCwINC40YHQv9C+0LvRjNC30L7QstCw0L3QuNGPINGE0LDQudC70L7QsiBjb29raWVcclxuXHQgKiBAcmV0dXJuIHtib29sZWFufVxyXG5cdCAqIEBwcm90ZWN0ZWRcclxuXHQgKiBAc291cmNlQ29kZVxyXG5cdCAqL1xyXG5cdGdldCBhbGxvd1VzYWdlICgpIHtcclxuXHRcdHJldHVybiB0aGlzLnJlY2VpdmUodGhpcy5hbGxvd1VzYWdlS2V5KSA9PT0gdGhpcy5hbGxvd1VzYWdlVmFsdWU7XHJcblx0fSxcclxuXHJcblx0LyoqXHJcblx0ICog0JrQu9GO0Ycg0L/RgNC+0LLQtdGA0LrQuCDQv9C+0LvRjNC30L7QstCw0YLQtdC70YzRgdC60L7Qs9C+INGB0L7Qs9C70LDRgdC40Y9cclxuXHQgKiDQvdCwINC40YHQv9C+0LvRjNC30L7QstCw0L3QuNGPINGE0LDQudC70L7QsiBjb29raWVcclxuXHQgKiBAdHlwZSB7c3RyaW5nfVxyXG5cdCAqIEBkZWZhdWx0ICdhbGxvdy1jb29raWUtdXNhZ2UnXHJcblx0ICogQHByb3RlY3RlZFxyXG5cdCAqIEBzb3VyY2VDb2RlXHJcblx0ICovXHJcblx0Z2V0IGFsbG93VXNhZ2VLZXkgKCkge1xyXG5cdFx0cmV0dXJuICdhbGxvdy1jb29raWUtdXNhZ2UnO1xyXG5cdH0sXHJcblxyXG5cdC8qKlxyXG5cdCAqINCX0L3QsNGH0LXQvdC40LUg0LrQu9GO0YfQsCDQv9GA0L7QstC10YDQutC4INC/0L7Qu9GM0LfQvtCy0LDRgtC10LvRjNGB0LrQvtCz0L4g0YHQvtCz0LvQsNGB0LjRj1xyXG5cdCAqINC90LAg0LjRgdC/0L7Qu9GM0LfQvtCy0LDQvdC40Y8g0YTQsNC50LvQvtCyIGNvb2tpZVxyXG5cdCAqIEB0eXBlIHtzdHJpbmd9XHJcblx0ICogQGRlZmF1bHQgJ3RydWUnXHJcblx0ICogQHByb3RlY3RlZFxyXG5cdCAqIEBzb3VyY2VDb2RlXHJcblx0ICovXHJcblx0Z2V0IGFsbG93VXNhZ2VWYWx1ZSAoKSB7XHJcblx0XHRyZXR1cm4gJ3RydWUnO1xyXG5cdH0sXHJcblxyXG5cdC8qKlxyXG5cdCAqINCX0LDQtNC10YDQttC60LAgKG1zKSDQtNC70Y8g0L/RgNC+0LLQtdGA0LrQuCDQv9C+0LvRjNC30L7QstCw0YLQtdC70YzRgdC60L7Qs9C+INGB0L7Qs9C70LDRgdC40Y9cclxuXHQgKiDQvdCwINC40YHQv9C+0LvRjNC30L7QstCw0L3QuNGPINGE0LDQudC70L7QsiBjb29raWVcclxuXHQgKiBAdHlwZSB7bnVtYmVyfVxyXG5cdCAqIEBkZWZhdWx0IDIwMDBcclxuXHQgKiBAcHJvdGVjdGVkXHJcblx0ICogQHNvdXJjZUNvZGVcclxuXHQgKi9cclxuXHRnZXQgYWxsb3dVc2FnZURlbGF5ICgpIHtcclxuXHRcdHJldHVybiAyMDAwO1xyXG5cdH1cclxufTtcclxuXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuLy8gRXhwb3J0c1xyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG5leHBvcnQgZGVmYXVsdCBjb29raWVEYXRhO1xyXG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9fSFRNTC9zcmMvanMvX21vZHVsZXMvY29va2llLWRhdGEuanMiLCIndXNlIHN0cmljdCc7XHJcblxyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbi8vIEltcG9ydHNcclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuaW1wb3J0ICdzZWxlY3QyJztcclxuaW1wb3J0IHZhbGlkYXRlIGZyb20gJyMvX21vZHVsZXMvanF1ZXJ5LXZhbGlkYXRpb24vdmFsaWRhdGUtaW5pdCc7XHJcblxyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbi8vIFByaXZhdGVcclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxubGV0IHNlbGVjdFNlbGVjdG9yID0gJy5zZWxlY3QyJztcclxubGV0IHdyYXA7XHJcbmxldCBwbGFjZWhvbGRlcjtcclxubGV0ICRjbG9zZUJ0biA9ICQoJy5maWx0ZXItY2xvc2UtYnRuJyk7XHJcbmxldCBpc0xhbmcgPSBmYWxzZTtcclxuXHJcbmZ1bmN0aW9uIHNlbGVjdDJJbml0IChsYW5nLCAkY29udGFpbmVyID0gJCgnYm9keScpKSB7XHJcblx0JGNvbnRhaW5lci5maW5kKHNlbGVjdFNlbGVjdG9yKS5lYWNoKGZ1bmN0aW9uIChpbmRleCkge1xyXG5cdFx0bGV0ICR0aGlzID0gJCh0aGlzKTtcclxuXHRcdHdyYXAgPSAkdGhpcy5jbG9zZXN0KCcuc2VsZWN0Mi13cmFwJyk7XHJcblx0XHRwbGFjZWhvbGRlciA9ICR0aGlzLmRhdGEoJ3BsYWNlaG9sZGVyJyk7XHJcblx0XHRsZXQgJGZvcm0gPSAkdGhpcy5jbG9zZXN0KCcuanMtZm9ybScpO1xyXG5cclxuXHRcdGlmICgkdGhpcy5oYXNDbGFzcygnYWpheFNlbGVjdDInKSkge1xyXG4gICAgICAgICAgICAkdGhpcy5zZWxlY3QyKHtcclxuICAgICAgICAgICAgICAgIG1pbmltdW1SZXN1bHRzRm9yU2VhcmNoOiAgMSxcclxuICAgICAgICAgICAgICAgIGRyb3Bkb3duUGFyZW50OiB3cmFwLFxyXG4gICAgICAgICAgICAgICAgcGxhY2Vob2xkZXI6IHBsYWNlaG9sZGVyLFxyXG4gICAgICAgICAgICAgICAgd2lkdGg6ICdyZXNvbHZlJyxcclxuICAgICAgICAgICAgICAgIGxhbmd1YWdlOiBsYW5nLFxyXG4gICAgICAgICAgICAgICAgYWpheDoge1xyXG4gICAgICAgICAgICAgICAgICAgIHVybDogJHRoaXMuZGF0YSgndXJsJyksXHJcbiAgICAgICAgICAgICAgICAgICAgdHlwZTogJ1BPU1QnLFxyXG4gICAgICAgICAgICAgICAgICAgIGRhdGE6IGZ1bmN0aW9uIChwYXJhbXMpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgbGV0IHF1ZXJ5ID0ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc2VhcmNoOiBwYXJhbXMudGVybSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNpdHlSZWY6ICQoJy5qcy1jb3VyaWVyLWNpdHknKS52YWwoKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gcXVlcnk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICBwcm9jZXNzUmVzdWx0czogZnVuY3Rpb24gKGRhdGEpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJlc3VsdHM6IGRhdGEucmVzdWx0LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSBlbHNlIGlmICgkdGhpcy5oYXNDbGFzcygnYWpheFNlbGVjdDMnKSkge1xyXG4gICAgICAgICAgICAkdGhpcy5zZWxlY3QyKHtcclxuICAgICAgICAgICAgICAgIG1pbmltdW1SZXN1bHRzRm9yU2VhcmNoOiAgMSxcclxuICAgICAgICAgICAgICAgIGRyb3Bkb3duUGFyZW50OiB3cmFwLFxyXG4gICAgICAgICAgICAgICAgcGxhY2Vob2xkZXI6IHBsYWNlaG9sZGVyLFxyXG4gICAgICAgICAgICAgICAgd2lkdGg6ICdyZXNvbHZlJyxcclxuICAgICAgICAgICAgICAgIGxhbmd1YWdlOiBsYW5nLFxyXG4gICAgICAgICAgICAgICAgYWpheDoge1xyXG4gICAgICAgICAgICAgICAgICAgIHVybDogJHRoaXMuZGF0YSgndXJsJyksXHJcbiAgICAgICAgICAgICAgICAgICAgdHlwZTogJ1BPU1QnLFxyXG4gICAgICAgICAgICAgICAgICAgIGRhdGE6IGZ1bmN0aW9uIChwYXJhbXMpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgbGV0IHF1ZXJ5ID0ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc2VhcmNoOiBwYXJhbXMudGVybSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNpdHlSZWY6ICQoJy5qcy1jb3VyaWVyLWNpdHktbG9nZ2VkJykudmFsKClcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHF1ZXJ5O1xyXG4gICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgcHJvY2Vzc1Jlc3VsdHM6IGZ1bmN0aW9uIChkYXRhKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXN1bHRzOiBkYXRhLnJlc3VsdCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICR0aGlzLnNlbGVjdDIoe1xyXG4gICAgICAgICAgICAgICAgbWluaW11bVJlc3VsdHNGb3JTZWFyY2g6IDEsXHJcbiAgICAgICAgICAgICAgICBkcm9wZG93blBhcmVudDogd3JhcCxcclxuICAgICAgICAgICAgICAgIHBsYWNlaG9sZGVyOiBwbGFjZWhvbGRlcixcclxuICAgICAgICAgICAgICAgIHdpZHRoOiAncmVzb2x2ZScsXHJcbiAgICAgICAgICAgICAgICBsYW5ndWFnZTogbGFuZ1xyXG4gICAgICAgICAgICB9KTtcclxuXHRcdFx0JCgnLmpzLWRlbGl2ZXJ5LW5wLWhvbWUnKS52YWwoJycpO1xyXG5cdFx0XHQkKCcuanMtZGVsaXZlcnktbnAtc3RyZWV0JykudmFsKCcnKTtcclxuICAgICAgICB9XHJcblxyXG5cdFx0JHRoaXMub24oJ3NlbGVjdDI6c2VsZWN0JywgZnVuY3Rpb24gKCkge1xyXG5cdFx0XHRsZXQgJGNvbnRhaW5lciA9ICR0aGlzLm5leHQoJy5zZWxlY3QyLWNvbnRhaW5lcicpO1xyXG5cdFx0XHRsZXQgJHdyYXAgPSAkY29udGFpbmVyLmNsb3Nlc3QoJy5zZWxlY3QyLXdyYXAnKTtcclxuXHRcdFx0JGNvbnRhaW5lci5hZGRDbGFzcygnc2VsZWN0ZWQnKTtcclxuXHRcdFx0JHdyYXAuYWRkQ2xhc3MoJ3NlbGVjdGVkJyk7XHJcblxyXG5cdFx0XHRjb25zdCAkZm9ybSA9ICQodGhpcy5mb3JtKTtcclxuXHRcdFx0Y29uc3QgVmFsaWRhdG9yID0gJGZvcm0uZGF0YSgndmFsaWRhdG9yJyk7XHJcblx0XHRcdGlmIChWYWxpZGF0b3IpIHtcclxuXHRcdFx0XHRWYWxpZGF0b3IuZWxlbWVudCgkdGhpcyk7XHJcblx0XHRcdH1cclxuXHRcdH0pO1xyXG5cclxuXHRcdCR0aGlzLm9uKCdzZWxlY3QyOnVuc2VsZWN0JywgZnVuY3Rpb24gKCkge1xyXG5cdFx0XHRsZXQgJGNvbnRhaW5lciA9ICR0aGlzLm5leHQoJy5zZWxlY3QyLWNvbnRhaW5lcicpO1xyXG5cdFx0XHRsZXQgJHdyYXAgPSAkY29udGFpbmVyLmNsb3Nlc3QoJy5zZWxlY3QyLXdyYXAnKTtcclxuXHRcdFx0JGNvbnRhaW5lci5yZW1vdmVDbGFzcygnc2VsZWN0ZWQnKTtcclxuXHRcdFx0JHdyYXAucmVtb3ZlQ2xhc3MoJ3NlbGVjdGVkJyk7XHJcblxyXG5cdFx0XHRjb25zdCAkZm9ybSA9ICQodGhpcy5mb3JtKTtcclxuXHRcdFx0Y29uc3QgVmFsaWRhdG9yID0gJGZvcm0uZGF0YSgndmFsaWRhdG9yJyk7XHJcblx0XHRcdGlmIChWYWxpZGF0b3IpIHtcclxuXHRcdFx0XHRWYWxpZGF0b3IuZWxlbWVudCgkdGhpcyk7XHJcblx0XHRcdH1cclxuXHRcdH0pO1xyXG5cdH0pO1xyXG59XHJcblxyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbi8vIFB1YmxpY1xyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG5mdW5jdGlvbiBpbml0U2VsZWN0ICgkY29udGFpbmVyID0gJCgnYm9keScpKSB7XHJcblx0bGV0IGxhbmcgPSAkKCdodG1sJykuYXR0cignbGFuZycpO1xyXG5cdGlmIChsYW5nICYmICFpc0xhbmcpIHtcclxuXHRcdGlzTGFuZyA9IHRydWU7XHJcblx0XHQkLmdldFNjcmlwdCgnL01lZGlhL2Fzc2V0cy9qcy9zdGF0aWMvc2VsZWN0Mi8nICsgbGFuZyArICcuanMnLCBmdW5jdGlvbiAoKSB7XHJcblx0XHRcdHNlbGVjdDJJbml0KGxhbmcsICRjb250YWluZXIpO1xyXG5cdFx0fSk7XHJcblx0fSBlbHNlIHtcclxuXHRcdHNlbGVjdDJJbml0KGxhbmcsICRjb250YWluZXIpO1xyXG5cdH1cclxuXHJcblx0JGNsb3NlQnRuLm9uKCdjbGljaycsIGZ1bmN0aW9uICgpIHtcclxuXHRcdGxldCAkcGFyZW50ID0gJCh0aGlzKS5jbG9zZXN0KCcuc2VsZWN0Mi13cmFwJyk7XHJcblx0XHRsZXQgJHNlbGVjdGVkID0gJHBhcmVudC5maW5kKHNlbGVjdFNlbGVjdG9yKTtcclxuXHRcdCRzZWxlY3RlZC52YWwoJycpLnRyaWdnZXIoJ2NoYW5nZScpO1xyXG5cdFx0JHNlbGVjdGVkLnRyaWdnZXIoJ3NlbGVjdDI6dW5zZWxlY3QnKTtcclxuXHR9KTtcclxufVxyXG5cclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4vLyBFeHBvcnRzXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbmV4cG9ydCB7aW5pdFNlbGVjdH07XHJcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL19IVE1ML3NyYy9qcy9fbW9kdWxlcy9zZWxlY3QuanMiLCIndXNlIHN0cmljdCc7XHJcblxyXG4vKipcclxuICpcclxuICogQG1vZHVsZVxyXG4gKi9cclxuXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuLy8gSW1wb3J0c1xyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG5pbXBvcnQgJ2N1c3RvbS1qcXVlcnktbWV0aG9kcy9mbi9oYXMtaW5pdGVkLWtleSc7XHJcbmltcG9ydCAnbWFnbmlmaWMtcG9wdXAnO1xyXG5pbXBvcnQgJy4vbWZwLWV4dGVuZCc7XHJcbmltcG9ydCAnLi9tZnAtZXh0ZW5kLnNjc3MnO1xyXG5pbXBvcnQgbW9kdWxlTG9hZGVyIGZyb20gJyMvbW9kdWxlLWxvYWRlcic7XHJcbmltcG9ydCB7bWFpblNsaWRlciwgcHJvZHVjdFBob3RvU2xpZGVyLCBwcm9kdWN0U2xpZGVyfSBmcm9tIFwiIy9fbW9kdWxlcy9zbGlkZXJzXCI7XHJcbmltcG9ydCB6b29taXQgZnJvbSBcIiMvX21vZHVsZXMvem9vbWl0XCI7XHJcbmltcG9ydCB7YWlyRGF0ZXBpY2tlcn0gZnJvbSBcIiMvX21vZHVsZXMvYWlyRGF0ZXBpY2tlclwiO1xyXG5cclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4vLyBQdWJsaWNcclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuLyoqXHJcbiAqIEBwYXJhbSB7SlF1ZXJ5fSAkZWxlbWVudHNcclxuICogQHNvdXJjZUNvZGVcclxuICovXHJcbmZ1bmN0aW9uIG1mcEFqYXggKCRlbGVtZW50cykge1xyXG5cdCRlbGVtZW50cy5lYWNoKChpLCBlbCkgPT4ge1xyXG5cdFx0Y29uc3QgJGVsZW1lbnQgPSAkKGVsKTtcclxuXHRcdGlmICgkZWxlbWVudC5oYXNJbml0ZWRLZXkoJ21mcEFqYXgtaW5pdGVkJykpIHtcclxuXHRcdFx0cmV0dXJuIHRydWU7XHJcblx0XHR9XHJcblxyXG5cdFx0Y29uc3QgY29uZmlnID0gJC5leHRlbmQodHJ1ZSwge1xyXG5cdFx0XHR0eXBlOiAnYWpheCcsXHJcblx0XHRcdG1haW5DbGFzczogJ21mcC1hbmltYXRlLXpvb20taW4nLFxyXG5cdFx0XHRjbG9zZU1hcmt1cDogYDxidXR0b24gY2xhc3M9J21mcC1jbG9zZScgaWQ9J2N1c3RvbS1jbG9zZS1idG4nPjwvYnV0dG9uPmAsXHJcblx0XHRcdHJlbW92YWxEZWxheTogMzAwLFxyXG5cdFx0XHRhdXRvRm9jdXNMYXN0OiBmYWxzZSxcclxuXHRcdFx0Y2FsbGJhY2tzOiB7XHJcblx0XHRcdFx0ZWxlbWVudFBhcnNlIChpdGVtKSB7XHJcblx0XHRcdFx0XHRjb25zdCB7XHJcblx0XHRcdFx0XHRcdHVybCxcclxuXHRcdFx0XHRcdFx0dHlwZSA9ICdQT1NUJyxcclxuXHRcdFx0XHRcdFx0cGFyYW06IGRhdGEgPSB7fVxyXG5cdFx0XHRcdFx0fSA9IGl0ZW0uZWwuZGF0YSgpO1xyXG5cdFx0XHRcdFx0dGhpcy5zdC5hamF4LnNldHRpbmdzID0ge3VybCwgdHlwZSwgZGF0YX07XHJcblx0XHRcdFx0fSxcclxuXHRcdFx0XHRhamF4Q29udGVudEFkZGVkOiBmdW5jdGlvbiAoKSB7XHJcblx0XHRcdFx0XHRsZXQgJGNvbnRhaW5lciA9IHRoaXMuY29udGVudENvbnRhaW5lciB8fCBbXTtcclxuXHRcdFx0XHRcdGlmICgkY29udGFpbmVyKSB7XHJcblx0XHRcdFx0XHRcdG1vZHVsZUxvYWRlci5pbml0KCRjb250YWluZXIpO1xyXG5cclxuXHRcdFx0XHRcdFx0aWYgKCQoJy5sb2dpbi1wb3B1cCcpLmxlbmd0aCA+IDApIHtcclxuXHRcdFx0XHRcdFx0XHRhaXJEYXRlcGlja2VyKCk7XHJcblxyXG5cdFx0XHRcdFx0XHRcdGxldCByb3puID0gJCgnaW5wdXRbbmFtZT1cInJvem5cIl0nKSxcclxuXHRcdFx0XHRcdFx0XHRcdG9wdCA9ICQoJ2lucHV0W25hbWU9XCJvcHRcIl0nKSxcclxuXHRcdFx0XHRcdFx0XHRcdGRyb3AgPSAkKCdpbnB1dFtuYW1lPVwiZHJvcFwiXScpLFxyXG5cdFx0XHRcdFx0XHRcdFx0cm96bkJpcnRoID0gJCgnLnJvem4tYmlydGhkYXknKVxyXG5cclxuXHRcdFx0XHRcdFx0XHRmdW5jdGlvbiBjaGVja1JlcShpbnB1dCkge1xyXG5cdFx0XHRcdFx0XHRcdFx0cm96bi5wcm9wKCdyZXF1aXJlZCcsIGZhbHNlKTtcclxuXHJcblx0XHRcdFx0XHRcdFx0XHRpZiAoIXJvem4ucHJvcCgnY2hlY2tlZCcpICYmICFvcHQucHJvcCgnY2hlY2tlZCcpICYmICFkcm9wLnByb3AoJ2NoZWNrZWQnKSl7XHJcblx0XHRcdFx0XHRcdFx0XHRcdHJvem4ucHJvcCgncmVxdWlyZWQnLCB0cnVlKTtcclxuXHRcdFx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdFx0XHR9XHJcblxyXG5cdFx0XHRcdFx0XHRcdHJvem4ub24oJ2NsaWNrJywgZnVuY3Rpb24gKCkge1xyXG5cdFx0XHRcdFx0XHRcdFx0Y2hlY2tSZXEoKTtcclxuXHRcdFx0XHRcdFx0XHRcdG9wdC5wcm9wKCdjaGVja2VkJywgZmFsc2UpO1xyXG5cdFx0XHRcdFx0XHRcdFx0ZHJvcC5wcm9wKCdjaGVja2VkJywgZmFsc2UpO1xyXG5cclxuXHJcblx0XHRcdFx0XHRcdFx0XHRpZiAocm96bi5wcm9wKCdjaGVja2VkJykpe1xyXG5cdFx0XHRcdFx0XHRcdFx0XHRyb3puQmlydGguc2hvdygpO1xyXG5cdFx0XHRcdFx0XHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHRcdFx0XHRcdFx0cm96bkJpcnRoLmhpZGUoKS5maW5kKCdpbnB1dCcpLnZhbCgnJyk7XHJcblx0XHRcdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRcdFx0fSk7XHJcblxyXG5cdFx0XHRcdFx0XHRcdG9wdC5vbignY2xpY2snLCBmdW5jdGlvbiAoKSB7XHJcblx0XHRcdFx0XHRcdFx0XHRjaGVja1JlcSgpO1xyXG5cdFx0XHRcdFx0XHRcdFx0cm96bi5wcm9wKCdjaGVja2VkJywgZmFsc2UpO1xyXG5cdFx0XHRcdFx0XHRcdFx0cm96bkJpcnRoLmhpZGUoKS5maW5kKCdpbnB1dCcpLnZhbCgnJyk7XHJcblx0XHRcdFx0XHRcdFx0fSk7XHJcblxyXG5cdFx0XHRcdFx0XHRcdGRyb3Aub24oJ2NsaWNrJywgZnVuY3Rpb24gKCkge1xyXG5cdFx0XHRcdFx0XHRcdFx0Y2hlY2tSZXEoKTtcclxuXHRcdFx0XHRcdFx0XHRcdHJvem4ucHJvcCgnY2hlY2tlZCcsIGZhbHNlKTtcclxuXHRcdFx0XHRcdFx0XHRcdHJvem5CaXJ0aC5oaWRlKCkuZmluZCgnaW5wdXQnKS52YWwoJycpO1xyXG5cdFx0XHRcdFx0XHRcdH0pO1xyXG5cclxuXHRcdFx0XHRcdFx0XHQkKCcubG9naW4tcG9wdXBfX3N1Ym1pdCcpLm9uKCdjbGljaycsIGZ1bmN0aW9uICgpe1xyXG5cdFx0XHRcdFx0XHRcdFx0aWYgKCQoXCIjYmlydGhkYXlcIikudmFsKCkubGVuZ3RoIHx8ICQoXCIjYmFieU5hbWVcIikudmFsKCkubGVuZ3RoKXtcclxuXHRcdFx0XHRcdFx0XHRcdFx0JChcIiNiaXJ0aGRheSwgI2JhYnlOYW1lXCIpLnByb3AoJ3JlcXVpcmVkJywgdHJ1ZSlcclxuXHRcdFx0XHRcdFx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0XHRcdFx0XHRcdCQoXCIjYmlydGhkYXksICNiYWJ5TmFtZVwiKS5yZW1vdmVBdHRyKCdyZXF1aXJlZCcpLnJlbW92ZUNsYXNzKCdoYXMtZXJyb3InKTtcclxuXHRcdFx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdFx0XHR9KVxyXG5cdFx0XHRcdFx0XHR9XHJcblxyXG5cdFx0XHRcdFx0XHRpZiAoJCgnLnNpemVzLWdhbGxlcnktcG9wdXAnKS5sZW5ndGggPiAwKSB7XHJcblxyXG5cdFx0XHRcdFx0XHRcdGxldCAkcHJvZHVjdE5hdlNsaWRlciA9ICQoJy5zaXplcy1nYWxsZXJ5LXBvcHVwJykuZmluZCgnLmdhbGxlcnktcHJldmlld19fd3JhcCcpO1xyXG5cclxuXHRcdFx0XHRcdFx0XHRmdW5jdGlvbiBwcm9kdWN0UGhvdG9TbGlkZXIoKSB7XHJcblxyXG5cdFx0XHRcdFx0XHRcdFx0bGV0IHBob3RvU2xpZGVyID0gJCgnLnNpemVzLWdhbGxlcnktcG9wdXAgLnByb2R1Y3RfX2dhbGxlcnktbWFpbicpLnNsaWNrKHtcclxuXHRcdFx0XHRcdFx0XHRcdFx0c2xpZGVzVG9TaG93OiAxLFxyXG5cdFx0XHRcdFx0XHRcdFx0XHRpbmZpbml0ZTogZmFsc2UsXHJcblx0XHRcdFx0XHRcdFx0XHRcdGZhZGU6IHRydWUsXHJcblx0XHRcdFx0XHRcdFx0XHRcdGRvdHM6IGZhbHNlLFxyXG5cdFx0XHRcdFx0XHRcdFx0XHRhcnJvd3M6IGZhbHNlLFxyXG5cdFx0XHRcdFx0XHRcdFx0XHRhY2Nlc3NpYmlsaXR5OiBmYWxzZSxcclxuXHRcdFx0XHRcdFx0XHRcdFx0YXNOYXZGb3I6ICcuc2l6ZXMtZ2FsbGVyeS1wb3B1cCAuZ2FsbGVyeS1wcmV2aWV3X193cmFwJ1xyXG5cdFx0XHRcdFx0XHRcdFx0fSk7XHJcblxyXG5cdFx0XHRcdFx0XHRcdFx0JHByb2R1Y3ROYXZTbGlkZXIuc2xpY2soe1xyXG5cdFx0XHRcdFx0XHRcdFx0XHRzbGlkZXNUb1Nob3c6IDQsXHJcblx0XHRcdFx0XHRcdFx0XHRcdHNsaWRlc1RvU2Nyb2xsOiAxLFxyXG5cdFx0XHRcdFx0XHRcdFx0XHRhc05hdkZvcjogJy5zaXplcy1nYWxsZXJ5LXBvcHVwIC5wcm9kdWN0X19nYWxsZXJ5LW1haW4nLFxyXG5cdFx0XHRcdFx0XHRcdFx0XHRmb2N1c09uU2VsZWN0OiB0cnVlLFxyXG5cdFx0XHRcdFx0XHRcdFx0XHR2ZXJ0aWNhbDogdHJ1ZSxcclxuXHRcdFx0XHRcdFx0XHRcdFx0aW5maW5pdGU6IGZhbHNlLFxyXG5cdFx0XHRcdFx0XHRcdFx0XHRwcmV2QXJyb3c6ICc8YnV0dG9uIHR5cGU9XCJidXR0b25cIiBjbGFzcz1cInNsaWNrLWFycm93IHNsaWNrLXByZXZcIj48L2J1dHRvbj4nLFxyXG5cdFx0XHRcdFx0XHRcdFx0XHRuZXh0QXJyb3c6ICc8YnV0dG9uIHR5cGU9XCJidXR0b25cIiBjbGFzcz1cInNsaWNrLWFycm93IHNsaWNrLW5leHRcIj48L2J1dHRvbj4nLFxyXG5cclxuXHRcdFx0XHRcdFx0XHRcdFx0cmVzcG9uc2l2ZTogW1xyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdHtcclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdGJyZWFrcG9pbnQ6IDc2OCxcclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdHNldHRpbmdzOiB7XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdHZlcnRpY2FsOiBmYWxzZSxcclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0c2xpZGVzVG9TaG93OiA2LFxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRpbmZpbml0ZTogdHJ1ZVxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdH0sIHtcclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdGJyZWFrcG9pbnQ6IDQ4MCxcclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdHNldHRpbmdzOiB7XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdHZlcnRpY2FsOiBmYWxzZSxcclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0c2xpZGVzVG9TaG93OiAzLFxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRpbmZpbml0ZTogdHJ1ZVxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdFx0XHRcdFx0XVxyXG5cdFx0XHRcdFx0XHRcdFx0fSk7XHJcblxyXG5cdFx0XHRcdFx0XHRcdFx0cGhvdG9TbGlkZXIub24oJ2JlZm9yZUNoYW5nZScsIGZ1bmN0aW9uIChldmVudCwgc2xpY2ssIGN1cnJlbnRTbGlkZSwgbmV4dFNsaWRlKSB7XHJcblxyXG5cdFx0XHRcdFx0XHRcdFx0fSk7XHJcblxyXG5cdFx0XHRcdFx0XHRcdFx0em9vbWl0KCk7XHJcblx0XHRcdFx0XHRcdFx0fVxyXG5cclxuXHRcdFx0XHRcdFx0XHRwcm9kdWN0UGhvdG9TbGlkZXIoKTtcclxuXHJcblx0XHRcdFx0XHRcdFx0JChcIi5zaXplcy1nYWxsZXJ5LXBvcHVwIFwiKS5vbmUoJ2NsaWNrJywgZnVuY3Rpb24gKCl7XHJcblx0XHRcdFx0XHRcdFx0XHQkKCcucHJvZHVjdF9fZ2FsbGVyeS1pdGVtIGltZycpLmVhY2goZnVuY3Rpb24gKCl7XHJcblx0XHRcdFx0XHRcdFx0XHRcdGxldCBhc3BlY3RSYXRpbyA9ICQodGhpcykud2lkdGgoKSAvICQodGhpcykuaGVpZ2h0KCk7XHJcblx0XHRcdFx0XHRcdFx0XHRcdCQodGhpcykuZGF0YShcImFzcGVjdC1yYXRpb1wiLCBhc3BlY3RSYXRpbyk7XHJcblxyXG5cdFx0XHRcdFx0XHRcdFx0XHRpZiAoYXNwZWN0UmF0aW8gPiAxKSB7XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0JCh0aGlzKS5jbG9zZXN0KCcucHJvZHVjdF9fZ2FsbGVyeS1pdGVtJykuYWRkQ2xhc3MoJ2xhbmRzY2FwZScpO1xyXG5cdFx0XHRcdFx0XHRcdFx0XHR9IGVsc2UgaWYgKGFzcGVjdFJhdGlvIDwgMSkge1xyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdCQodGhpcykuY2xvc2VzdCgnLnByb2R1Y3RfX2dhbGxlcnktaXRlbScpLmFkZENsYXNzKCdwb3J0cmFpdCcpO1xyXG5cdFx0XHRcdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRcdFx0XHR9KTtcclxuXHRcdFx0XHRcdFx0XHR9KVxyXG5cclxuXHJcblx0XHRcdFx0XHRcdH1cclxuXHJcblx0XHRcdFx0XHRcdGlmICgkKCcuc2l6ZXMtYmlnLXBvcHVwJykubGVuZ3RoID4gMCkge1xyXG5cdFx0XHRcdFx0XHRcdCQoJy5zaXplcy1iaWctcG9wdXAgLm1mcC1jbG9zZScpLm9uKCdjbGljaycsIGZ1bmN0aW9uICgpe1xyXG5cdFx0XHRcdFx0XHRcdFx0c2V0VGltZW91dChmdW5jdGlvbiAoKXtcclxuXHRcdFx0XHRcdFx0XHRcdFx0JCgnLnByb2R1Y3RfX2dhbGxlcnktaXRlbScpLnRyaWdnZXIoJ2NsaWNrJyk7XHJcblx0XHRcdFx0XHRcdFx0XHR9LCA1MDApXHJcblx0XHRcdFx0XHRcdFx0fSlcclxuXHRcdFx0XHRcdFx0fVxyXG5cclxuXHRcdFx0XHRcdFx0aWYgKCQoJy5yZXZpZXctcG9wdXAnKS5sZW5ndGggPiAwKSB7XHJcblx0XHRcdFx0XHRcdFx0YWlyRGF0ZXBpY2tlcigpO1xyXG5cdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHR9XHJcblxyXG5cdFx0XHRcdFx0JCgnYm9keScpLm9uKCdjbGljaycsICcucG9wdXAtaW5mb3JtYXRpb24tYmxvY2sgLmNhcnQtcmVnaXN0cmF0aW9uX19jYWxsYmFjaycsIGZ1bmN0aW9uICgpe1xyXG5cdFx0XHRcdFx0XHQkKCcuaGVhZGVyLXBob25lc19fY2FsbGJhY2snKS50cmlnZ2VyKCdjbGljaycpXHJcblx0XHRcdFx0XHR9KVxyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cdFx0fSwge1xyXG5cdFx0XHQvLyBjdXJyZW50IGVsZW1lbnQgY3VzdG9tIGNvbmZpZ1xyXG5cdFx0fSk7XHJcblxyXG5cdFx0JGVsZW1lbnQubWFnbmlmaWNQb3B1cChjb25maWcpO1xyXG5cdH0pO1xyXG59XHJcblxyXG4vKipcclxuICogQHBhcmFtIHtKUXVlcnl9ICRlbGVtZW50c1xyXG4gKiBAc291cmNlQ29kZVxyXG4gKi9cclxuZnVuY3Rpb24gbWZwSWZyYW1lICgkZWxlbWVudHMpIHtcclxuXHQkZWxlbWVudHMuZWFjaCgoaSwgZWwpID0+IHtcclxuXHRcdGNvbnN0ICRlbGVtZW50ID0gJChlbCk7XHJcblx0XHRpZiAoJGVsZW1lbnQuaGFzSW5pdGVkS2V5KCdtZnBJZnJhbWUtaW5pdGVkJykpIHtcclxuXHRcdFx0cmV0dXJuIHRydWU7XHJcblx0XHR9XHJcblxyXG5cdFx0Y29uc3QgY29uZmlnID0gJC5leHRlbmQodHJ1ZSwge1xyXG5cdFx0XHR0eXBlOiAnaWZyYW1lJyxcclxuXHRcdFx0cmVtb3ZhbERlbGF5OiAzMDAsXHJcblx0XHRcdG1haW5DbGFzczogJ21mcC1hbmltYXRlLXpvb20taW4nLFxyXG5cdFx0XHRjbG9zZUJ0bkluc2lkZTogdHJ1ZVxyXG5cdFx0fSwge1xyXG5cdFx0XHQvLyBjdXJyZW50IGVsZW1lbnQgY3VzdG9tIGNvbmZpZ1xyXG5cdFx0fSk7XHJcblxyXG5cdFx0JGVsZW1lbnQubWFnbmlmaWNQb3B1cChjb25maWcpO1xyXG5cdH0pO1xyXG59XHJcblxyXG4vKipcclxuICogQHBhcmFtIHtKUXVlcnl9ICRlbGVtZW50c1xyXG4gKiBAc291cmNlQ29kZVxyXG4gKi9cclxuZnVuY3Rpb24gbWZwSW5saW5lICgkZWxlbWVudHMpIHtcclxuXHQkZWxlbWVudHMuZWFjaCgoaSwgZWwpID0+IHtcclxuXHRcdGNvbnN0ICRlbGVtZW50ID0gJChlbCk7XHJcblx0XHRpZiAoJGVsZW1lbnQuaGFzSW5pdGVkS2V5KCdtZnBJbmxpbmUtaW5pdGVkJykpIHtcclxuXHRcdFx0cmV0dXJuIHRydWU7XHJcblx0XHR9XHJcblxyXG5cdFx0Y29uc3QgY29uZmlnID0gJC5leHRlbmQodHJ1ZSwge1xyXG5cdFx0XHR0eXBlOiAnaW5saW5lJyxcclxuXHRcdFx0cmVtb3ZhbERlbGF5OiAzMDAsXHJcblx0XHRcdG1haW5DbGFzczogJ21mcC1hbmltYXRlLXpvb20taW4nLFxyXG5cdFx0XHRjbG9zZUJ0bkluc2lkZTogdHJ1ZVxyXG5cdFx0fSwge1xyXG5cdFx0XHQvLyBjdXJyZW50IGVsZW1lbnQgY3VzdG9tIGNvbmZpZ1xyXG5cdFx0fSk7XHJcblxyXG5cdFx0JGVsZW1lbnQubWFnbmlmaWNQb3B1cChjb25maWcpO1xyXG5cdH0pO1xyXG59XHJcblxyXG4vKipcclxuICogQHBhcmFtIHtKUXVlcnl9ICRlbGVtZW50c1xyXG4gKiBAc291cmNlQ29kZVxyXG4gKi9cclxuZnVuY3Rpb24gbWZwR2FsbGVyeSAoJGVsZW1lbnRzKSB7XHJcblx0JGVsZW1lbnRzLmVhY2goKGksIGVsKSA9PiB7XHJcblx0XHRsZXQgJGVsZW1lbnQgPSAkKGVsKTtcclxuXHRcdGlmICgkZWxlbWVudC5oYXNJbml0ZWRLZXkoJ21mcEdhbGxlcnktaW5pdGVkJykpIHtcclxuXHRcdFx0cmV0dXJuIHRydWU7XHJcblx0XHR9XHJcblxyXG5cdFx0Y29uc3QgY29uZmlnID0gJC5leHRlbmQodHJ1ZSwge1xyXG5cdFx0XHR0eXBlOiAnaW1hZ2UnLFxyXG5cdFx0XHRyZW1vdmFsRGVsYXk6IDMwMCxcclxuXHRcdFx0bWFpbkNsYXNzOiAnbWZwLWFuaW1hdGUtem9vbS1pbicsXHJcblx0XHRcdGRlbGVnYXRlOiAnW2RhdGEtbWZwLXNyY10nLFxyXG5cdFx0XHRjbG9zZU1hcmt1cDogYDxidXR0b24gY2xhc3M9J21mcC1jbG9zZScgaWQ9J2N1c3RvbS1jbG9zZS1idG4nPjwvYnV0dG9uPmAsXHJcblx0XHRcdGNsb3NlQnRuSW5zaWRlOiB0cnVlLFxyXG5cdFx0XHRnYWxsZXJ5OiB7XHJcblx0XHRcdFx0ZW5hYmxlZDogdHJ1ZSxcclxuXHRcdFx0XHRwcmVsb2FkOiBbMCwgMl0sXHJcblx0XHRcdFx0bmF2aWdhdGVCeUltZ0NsaWNrOiB0cnVlLFxyXG5cdFx0XHRcdGFycm93TWFya3VwOiAnPGJ1dHRvbiB0aXRsZT1cIiV0aXRsZSVcIiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJtZnAtYXJyb3cgbWZwLWFycm93LSVkaXIlXCI+PC9idXR0b24+J1xyXG5cdFx0XHR9XHJcblx0XHR9LCB7XHJcblx0XHRcdC8vIGN1cnJlbnQgZWxlbWVudCBjdXN0b20gY29uZmlnXHJcblx0XHR9KTtcclxuXHJcblx0XHQkZWxlbWVudC5tYWduaWZpY1BvcHVwKGNvbmZpZyk7XHJcblx0fSk7XHJcbn1cclxuXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuLy8gRXhwb3J0c1xyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG5leHBvcnQge21mcEFqYXgsIG1mcElmcmFtZSwgbWZwSW5saW5lLCBtZnBHYWxsZXJ5fTtcclxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vX0hUTUwvc3JjL2pzL19tb2R1bGVzL21hZ25pZmljLXBvcHVwL21mcC1pbml0LmpzIiwiJ3VzZSBzdHJpY3QnO1xyXG5cclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4vLyBJbXBvcnRzXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbmltcG9ydCAnIy9fdmVuZG9ycy96b29taXQubWluJ1xyXG5cclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4vLyBQcml2YXRlXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbi8qKlxyXG4gKiBAdHlwZSB7T2JqZWN0fVxyXG4gKiBAcHJpdmF0ZVxyXG4gKi9cclxuXHJcbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIHpvb21pdCgpIHtcclxuXHQkKCcuem9vbS1pbWcnKS5lYWNoKGZ1bmN0aW9uICgpe1xyXG5cdFx0JCh0aGlzKS56b29tSXQoKVxyXG5cdH0pO1xyXG59O1xyXG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9fSFRNTC9zcmMvanMvX21vZHVsZXMvem9vbWl0LmpzIiwiJ3VzZSBzdHJpY3QnO1xyXG5cclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4vLyBJbXBvcnRzXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbmltcG9ydCAnLi4vX3ZlbmRvcnMvYWlyZGF0ZXBpY2tlcic7XHJcblxyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbi8vIFB1YmxpY1xyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG5mdW5jdGlvbiBhaXJEYXRlcGlja2VyICgpIHtcclxuXHQkKFwiLmpzLWJpcnRoZGF5XCIpLmRhdGVwaWNrZXIoe1xyXG5cdFx0bWF4RGF0ZTogbmV3IERhdGUoKVxyXG5cdH0pXHJcblxyXG5cdCQoJy5qcy1iaXJ0aGRheScpLm9uKCdpbnB1dCcsIGZ1bmN0aW9uKCkge1xyXG5cdFx0JCh0aGlzKS52YWwoJCh0aGlzKS52YWwoKS5yZXBsYWNlKC9bQS1aYS160JAt0K/QsC3Rj9CB0ZEsMC05XS8sICcnKSlcclxuXHR9KTtcclxufVxyXG5cclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4vLyBFeHBvcnRzXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbmV4cG9ydCB7YWlyRGF0ZXBpY2tlcn07XHJcblxyXG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9fSFRNTC9zcmMvanMvX21vZHVsZXMvYWlyRGF0ZXBpY2tlci5qcyIsIid1c2Ugc3RyaWN0JztcclxuXHJcbi8qKlxyXG4gKiDQodC+0LfQtNCw0L3QuNC1INGC0LDQudC80LXRgNC+0LIg0YEg0LLQvdGD0YLRgNC10L3QuNC8INC30LDQvNGL0LrQsNC90LjQtdC8XHJcbiAqIEBtb2R1bGVcclxuICogQGV4YW1wbGVcclxuaW1wb3J0IGNyZWF0ZVRpbWVyIGZyb20gJy4vY3JlYXRlLXRpbWVyJztcclxuXHJcbmxldCB0aW1lciA9IGNyZWF0ZVRpbWVyKDUwMCk7XHJcbiQod2luZG93KS5vbigncmVzaXplJywgZnVuY3Rpb24gKCkge1xyXG5cdHRpbWVyKCgpID0+IHtcclxuXHRcdGNvbnNvbGUubG9nKCdkb25lJyk7XHJcblx0fSk7XHJcbn0pO1xyXG5cclxuLy8gb25seSBjbGVhciB0aW1lb3V0XHJcbnRpbWVyLmNsZWFyKCk7XHJcbiAqL1xyXG5cclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4vLyBQdWJsaWNcclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuLyoqXHJcbiAqIEBwYXJhbSB7bnVtYmVyfSBbZGVmYXVsdERlbGF5PTMwMF1cclxuICogQHJldHVybiB7RnVuY3Rpb259INC30LDQv9GD0YHQuiDRgtCw0LnQvNC10YDQvtCyXHJcbiAqL1xyXG5mdW5jdGlvbiBjcmVhdGVUaW1lciAoZGVmYXVsdERlbGF5ID0gMzAwKSB7XHJcblx0bGV0IHRpbWVySWQgPSBudWxsO1xyXG5cdGxldCB7Y2xlYXJUaW1lb3V0LCBzZXRUaW1lb3V0fSA9IHdpbmRvdztcclxuXHJcblx0LyoqXHJcblx0ICogQHBhcmFtIHtGdW5jdGlvbn0gZm5cclxuXHQgKiBAcGFyYW0ge251bWJlcn0gW2RlbGF5PWRlZmF1bHREZWxheV1cclxuXHQgKiBAcHJvcCB7RnVuY3Rpb259IGNsZWFyXHJcblx0ICovXHJcblx0ZnVuY3Rpb24gdGltZXIgKGZuLCBkZWxheSA9IGRlZmF1bHREZWxheSkge1xyXG5cdFx0Y2xlYXJUaW1lb3V0KHRpbWVySWQpO1xyXG5cdFx0dGltZXJJZCA9IHNldFRpbWVvdXQoZm4sIGRlbGF5KTtcclxuXHR9XHJcblxyXG5cdHRpbWVyLmNsZWFyID0gKCkgPT4gY2xlYXJUaW1lb3V0KHRpbWVySWQpO1xyXG5cdHJldHVybiB0aW1lcjtcclxufVxyXG5cclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4vLyBFeHBvcnRzXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNyZWF0ZVRpbWVyO1xyXG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9fSFRNTC9zcmMvanMvX21vZHVsZXMvY3JlYXRlLXRpbWVyLmpzIiwiJ3VzZSBzdHJpY3QnO1xyXG5cclxuLyoqXHJcbiAqIEBmaWxlT3ZlcnZpZXcg0J7RgdC90L7QstC90L7QuSDRhNCw0LnQuyDQuNC90LjRhtC40LDQu9C40LfQsNGG0LjQuCDQvNC+0LTRg9C70LXQuVxyXG4gKi9cclxuXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuLy8gSW1wb3J0c1xyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG5pbXBvcnQgJyMvX3ZlbmRvcnMvcHJvbWlzZS1wb2x5ZmlsbCc7XHJcbmltcG9ydCAnIy9fdmVuZG9ycy9qcXVlcnknO1xyXG5pbXBvcnQgJyMvX3ZlbmRvcnMvZ3NhcC9nc2FwJztcclxuXHJcbmltcG9ydCBtb2R1bGVMb2FkZXIgZnJvbSAnIy9tb2R1bGUtbG9hZGVyJztcclxuaW1wb3J0IHdzVGFicyBmcm9tICcjL19tb2R1bGVzL3dzdGFicyc7XHJcbmltcG9ydCBjb29raWVEYXRhIGZyb20gJyMvX21vZHVsZXMvY29va2llLWRhdGEnO1xyXG5pbXBvcnQgdmFsaWRhdGUgZnJvbSAnIy9fbW9kdWxlcy9qcXVlcnktdmFsaWRhdGlvbi92YWxpZGF0ZS1pbml0JztcclxuaW1wb3J0IHttYWluTWVudSwgbW9iaWxlTWVudSwgZmlsdGVyc01lbnV9IGZyb20gJyMvX21vZHVsZXMvbWVudSc7XHJcbmltcG9ydCB7bWFpblNsaWRlciwgcHJvZHVjdFNsaWRlciwgcHJvZHVjdFBob3RvU2xpZGVyLCBwcm9kdWN0SW1nU2xpZGVyfSBmcm9tICcjL19tb2R1bGVzL3NsaWRlcnMnO1xyXG5pbXBvcnQge3N1Yk1lbnVTaG93fSBmcm9tICcjL19tb2R1bGVzL2ZpeGVkLWhlYWRlcic7XHJcbmltcG9ydCB7aW5pdFJhbmdlU2xpZGVyfSBmcm9tICcjL19tb2R1bGVzL3JhbmdlLXNsaWRlcic7XHJcbmltcG9ydCB7aW5pdFNlbGVjdH0gZnJvbSAnIy9fbW9kdWxlcy9zZWxlY3QnO1xyXG5pbXBvcnQge3N1Yk1lbnVUb2dnbGV9IGZyb20gJyMvX21vZHVsZXMvc3ViLW1lbnUnO1xyXG5pbXBvcnQge1xyXG5cdGFkZFRvRmF2b3JpdGUsXHJcblx0dG9nZ2xlQ2FydEluZm8sXHJcblx0bG9hZE1vcmUsXHJcblx0cHJvZHVjdENvdW50ZXIsXHJcblx0bGlrZUFkZCxcclxuXHRkaXNsaWtlQWRkLFxyXG5cdHNjcm9sbFRvcCxcclxuXHRoZWFkZXJQaG9uZXNNb2IsXHJcblx0dG9nZ2xlRGVsaXZlcnlJbmZvXHJcbn0gZnJvbSAnIy9fbW9kdWxlcy9jb21tb24nO1xyXG5pbXBvcnQge2luaXRNYXB9IGZyb20gJyMvX21vZHVsZXMvbWFwJztcclxuaW1wb3J0IHtjYXJ0SW5pdCwgY2FydERlZmF1bHRDbG9zZX0gZnJvbSAnIy9fbW9kdWxlcy9jYXJ0JztcclxuaW1wb3J0IHtzZWxlY3RXb3JkfSBmcm9tICcjL19tb2R1bGVzL3NlbGVjdC13b3JkJztcclxuaW1wb3J0IHtoZWFkZXJTZWFyY2hUb2dnbGUsIHNlYXJjaEF1dG9jb21wbGV0ZX0gZnJvbSAnIy9fbW9kdWxlcy9oZWFkZXItc2VhcmNoJztcclxuaW1wb3J0IHtwcm9kdWN0TWVkaWF9IGZyb20gJyMvX21vZHVsZXMvcHJvZHVjdC0zNjAnO1xyXG5pbXBvcnQge3BhZ2luYXRpb25Jbml0fSBmcm9tICcjL19tb2R1bGVzL3BhZ2luYXRpb24nO1xyXG5pbXBvcnQge2NhdGFsb2dTZWxlY3QsIGNhdGFsb2dNZW51LCBjYXRhbG9nTWVudUl0ZW0sIGNhdGFsb2dDbG9zZX0gZnJvbSAnIy9fbW9kdWxlcy9jYXRhbG9nLXNlbGVjdCc7XHJcbmltcG9ydCBjaG9vc2VBamF4IGZyb20gJyMvX21vZHVsZXMvY2hvb3NlLWFqYXgnO1xyXG5pbXBvcnQgaW5pdEFqYXhTZWxlY3QyIGZyb20gJyMvX21vZHVsZXMvYWpheFNlbGVjdDInO1xyXG5pbXBvcnQgc2hvd05vdHkgZnJvbSAnIy9fbW9kdWxlcy9zaG93LW5vdHknO1xyXG5pbXBvcnQgY3VycmVuY3lJbml0IGZyb20gJyMvX21vZHVsZXMvY3VycmVuY3knO1xyXG5pbXBvcnQgdXNlclR5cGVJbml0IGZyb20gJyMvX21vZHVsZXMvdXNlci10eXBlJztcclxuaW1wb3J0IHBheVRvZ2dsZSBmcm9tICcjL19tb2R1bGVzL3BheS1pbmZvLXRvZ2dsZSc7XHJcbmltcG9ydCB6b29taXQgZnJvbSAnIy9fbW9kdWxlcy96b29taXQnO1xyXG5pbXBvcnQgYmFja1RvQ2F0YWxvZyBmcm9tIFwiIy9fbW9kdWxlcy9iYWNrVG9DYXRhbG9nXCI7XHJcbmltcG9ydCB7YWlyRGF0ZXBpY2tlcn0gZnJvbSBcIiMvX21vZHVsZXMvYWlyRGF0ZXBpY2tlclwiO1xyXG5pbXBvcnQge2RlbGxDaGlsZH0gZnJvbSBcIiMvX21vZHVsZXMvZGVsbENoaWxkXCI7XHJcbmltcG9ydCB7Y2hhbmdlU2l6ZUluVGFibGV9IGZyb20gXCIjL19tb2R1bGVzL2NoYW5nZVNpemVJblRhYmxlXCI7XHJcbmltcG9ydCB7Y2xpY2tNZX0gZnJvbSBcIiMvX21vZHVsZXMvY2xpY2tNZVwiO1xyXG5pbXBvcnQge2RlbGl2ZXJ5UmVnaXN0cmF0aW9ufSBmcm9tIFwiIy9fbW9kdWxlcy9kZWxpdmVyeVJlZ2lzdHJhdGlvblwiO1xyXG5pbXBvcnQge2V4cG9ydFRhYmxlfSBmcm9tIFwiIy9fbW9kdWxlcy9leHBvcnQtdGFibGVcIjtcclxuaW1wb3J0IHtmaXhlZEhlYWRlcn0gZnJvbSBcIiMvX21vZHVsZXMvZml4ZWQtaGVhZGVyXCI7XHJcblxyXG5cclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4vLyBQdWJsaWNcclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxud2luZG93LmpRdWVyeShkb2N1bWVudCkucmVhZHkoJCA9PiB7XHJcblx0bW9kdWxlTG9hZGVyLmluaXQoJChkb2N1bWVudCkpO1xyXG5cdGNvb2tpZURhdGEuYXNrVXNhZ2UoKTsgLy8g0LXRgdC70Lgg0L3Rg9C20L3QviDQuNGB0L/QvtC70YzQt9C+0LLQsNGC0Ywg0LrRg9C60LhcclxuXHR3c1RhYnMuaW5pdCgpO1xyXG5cdC8vIHdzVGFicy5zZXRBY3RpdmUoKTsgLy8g0LXRgdC70Lgg0L3Rg9C20L3QviDQv9GA0LjQvdGD0LTQuNGC0LXQu9GM0L3QviDQsNC60YLQuNCy0LjRgNC+0LLQsNGC0Ywg0YLQsNCx0YtcclxuXHR2YWxpZGF0ZSgkKCcuanMtZm9ybScpKTtcclxuXHRtYWluTWVudSgpO1xyXG5cdG1vYmlsZU1lbnUoKTtcclxuXHRhaXJEYXRlcGlja2VyKCk7XHJcblx0bWFpblNsaWRlcigpO1xyXG5cdHByb2R1Y3RJbWdTbGlkZXIoKTtcclxuXHRwcm9kdWN0U2xpZGVyKCk7XHJcblx0aW5pdFJhbmdlU2xpZGVyKCk7XHJcblx0aW5pdFNlbGVjdCgpO1xyXG5cdHByb2R1Y3RQaG90b1NsaWRlcigpO1xyXG5cdHN1Yk1lbnVUb2dnbGUoJy5maWx0ZXJzX19pdGVtLXRvcCcsICcuZmlsdGVyc19faXRlbScsICcuZmlsdGVyc19fY2hpbGQtd3JhcCcpO1xyXG5cdGZpeGVkSGVhZGVyKCk7XHJcblx0ZXhwb3J0VGFibGUoKTtcclxuXHRzdWJNZW51U2hvdygpO1xyXG5cdHN1Yk1lbnVUb2dnbGUoJy5mYXFfX2l0ZW0tdG9wJywgJy5mYXFfX2l0ZW0nLCAnLmZhcV9faXRlbS1ib3R0b20nKTtcclxuXHRzdWJNZW51VG9nZ2xlKCcuaGlzdG9yeV9faXRlbS10b3AnLCAnLmhpc3RvcnlfX2l0ZW0nLCAnLmhpc3RvcnlfX2l0ZW0tYm90dG9tJyk7XHJcblx0YWRkVG9GYXZvcml0ZSgpO1xyXG5cdHRvZ2dsZUNhcnRJbmZvKCk7XHJcblx0aW5pdE1hcCgpO1xyXG5cdGZpbHRlcnNNZW51KCk7XHJcblx0Y2FydEluaXQoKTtcclxuXHRzZWxlY3RXb3JkKCk7XHJcblx0bG9hZE1vcmUoKTtcclxuXHRoZWFkZXJTZWFyY2hUb2dnbGUoKTtcclxuXHRzZWFyY2hBdXRvY29tcGxldGUoKTtcclxuXHRwcm9kdWN0TWVkaWEoKTtcclxuXHRwcm9kdWN0Q291bnRlcigpO1xyXG5cdHNjcm9sbFRvcCgpO1xyXG5cdGhlYWRlclBob25lc01vYigpO1xyXG5cdHRvZ2dsZURlbGl2ZXJ5SW5mbygpO1xyXG5cdGNhcnREZWZhdWx0Q2xvc2UoKTtcclxuXHRsaWtlQWRkKCk7XHJcblx0ZGlzbGlrZUFkZCgpO1xyXG5cdHBhZ2luYXRpb25Jbml0KCk7XHJcblx0Y2F0YWxvZ1NlbGVjdCgpO1xyXG4gICAgY2F0YWxvZ01lbnUoKTtcclxuICAgIGNhdGFsb2dNZW51SXRlbSgpO1xyXG4gICAgY2F0YWxvZ0Nsb3NlKCk7XHJcblx0Y2hvb3NlQWpheCgpO1xyXG5cdGluaXRBamF4U2VsZWN0MigpO1xyXG5cdHNob3dOb3R5KCk7XHJcblx0Y3VycmVuY3lJbml0KCk7XHJcblx0dXNlclR5cGVJbml0KCk7XHJcblx0cGF5VG9nZ2xlKCk7XHJcblx0em9vbWl0KCk7XHJcblx0YmFja1RvQ2F0YWxvZygpO1xyXG5cdGRlbGxDaGlsZCgpO1xyXG5cdGNoYW5nZVNpemVJblRhYmxlKCk7XHJcblx0Y2xpY2tNZSgpO1xyXG5cdGRlbGl2ZXJ5UmVnaXN0cmF0aW9uKCk7XHJcbn0pO1xyXG5cclxuaWYgKElTX1BST0RVQ1RJT04pIHtcclxuXHRpbXBvcnQoJyMvX21vZHVsZXMvd2V6b20tbG9nJyk7XHJcbn1cclxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vX0hUTUwvc3JjL2pzL2VudHJ5LmpzIiwidmFyIG1hcCA9IHtcblx0XCIuL2RyYWdnYWJsZS10YWJsZS0tbW9kdWxlLWxvYWRlclwiOiBbXG5cdFx0MTcsXG5cdFx0OFxuXHRdLFxuXHRcIi4vZHJhZ2dhYmxlLXRhYmxlLS1tb2R1bGUtbG9hZGVyLmpzXCI6IFtcblx0XHQxNyxcblx0XHQ4XG5cdF0sXG5cdFwiLi9pbnB1dG1hc2stLW1vZHVsZS1sb2FkZXJcIjogW1xuXHRcdDE5LFxuXHRcdDNcblx0XSxcblx0XCIuL2lucHV0bWFzay0tbW9kdWxlLWxvYWRlci5qc1wiOiBbXG5cdFx0MTksXG5cdFx0M1xuXHRdLFxuXHRcIi4vanF1ZXJ5LXZhbGlkYXRpb24tLW1vZHVsZS1sb2FkZXJcIjogW1xuXHRcdDIwLFxuXHRcdDdcblx0XSxcblx0XCIuL2pxdWVyeS12YWxpZGF0aW9uLS1tb2R1bGUtbG9hZGVyLmpzXCI6IFtcblx0XHQyMCxcblx0XHQ3XG5cdF0sXG5cdFwiLi9sb3phZC0tbW9kdWxlLWxvYWRlclwiOiBbXG5cdFx0MjIsXG5cdFx0MlxuXHRdLFxuXHRcIi4vbG96YWQtLW1vZHVsZS1sb2FkZXIuanNcIjogW1xuXHRcdDIyLFxuXHRcdDJcblx0XSxcblx0XCIuL21hZ25pZmljLXBvcHVwLS1tb2R1bGUtbG9hZGVyXCI6IFtcblx0XHQyMyxcblx0XHQ2XG5cdF0sXG5cdFwiLi9tYWduaWZpYy1wb3B1cC0tbW9kdWxlLWxvYWRlci5qc1wiOiBbXG5cdFx0MjMsXG5cdFx0NlxuXHRdLFxuXHRcIi4vbWFsaWh1LXNjcm9sbGJhci0tbW9kdWxlLWxvYWRlclwiOiBbXG5cdFx0MjQsXG5cdFx0MVxuXHRdLFxuXHRcIi4vbWFsaWh1LXNjcm9sbGJhci0tbW9kdWxlLWxvYWRlci5qc1wiOiBbXG5cdFx0MjQsXG5cdFx0MVxuXHRdLFxuXHRcIi4vcHJpc21qcy0tbW9kdWxlLWxvYWRlclwiOiBbXG5cdFx0MjUsXG5cdFx0NVxuXHRdLFxuXHRcIi4vcHJpc21qcy0tbW9kdWxlLWxvYWRlci5qc1wiOiBbXG5cdFx0MjUsXG5cdFx0NVxuXHRdLFxuXHRcIi4vc2Nyb2xsLXdpbmRvdy0tbW9kdWxlLWxvYWRlclwiOiBbXG5cdFx0MjYsXG5cdFx0MFxuXHRdLFxuXHRcIi4vc2Nyb2xsLXdpbmRvdy0tbW9kdWxlLWxvYWRlci5qc1wiOiBbXG5cdFx0MjYsXG5cdFx0MFxuXHRdLFxuXHRcIi4vd3JhcC1tZWRpYS0tbW9kdWxlLWxvYWRlclwiOiBbXG5cdFx0MjcsXG5cdFx0NFxuXHRdLFxuXHRcIi4vd3JhcC1tZWRpYS0tbW9kdWxlLWxvYWRlci5qc1wiOiBbXG5cdFx0MjcsXG5cdFx0NFxuXHRdXG59O1xuZnVuY3Rpb24gd2VicGFja0FzeW5jQ29udGV4dChyZXEpIHtcblx0dmFyIGlkcyA9IG1hcFtyZXFdO1xuXHRpZighaWRzKVxuXHRcdHJldHVybiBQcm9taXNlLnJlamVjdChuZXcgRXJyb3IoXCJDYW5ub3QgZmluZCBtb2R1bGUgJ1wiICsgcmVxICsgXCInLlwiKSk7XG5cdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fLmUoaWRzWzFdKS50aGVuKGZ1bmN0aW9uKCkge1xuXHRcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKGlkc1swXSk7XG5cdH0pO1xufTtcbndlYnBhY2tBc3luY0NvbnRleHQua2V5cyA9IGZ1bmN0aW9uIHdlYnBhY2tBc3luY0NvbnRleHRLZXlzKCkge1xuXHRyZXR1cm4gT2JqZWN0LmtleXMobWFwKTtcbn07XG53ZWJwYWNrQXN5bmNDb250ZXh0LmlkID0gMzY7XG5tb2R1bGUuZXhwb3J0cyA9IHdlYnBhY2tBc3luY0NvbnRleHQ7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9fSFRNTC9zcmMvanMvX21vZHVsZXMtbG9hZGVycyBsYXp5IF5cXC5cXC8uKiRcbi8vIG1vZHVsZSBpZCA9IDM2XG4vLyBtb2R1bGUgY2h1bmtzID0gOSIsIid1c2Ugc3RyaWN0JztcclxuXHJcbi8qKlxyXG4gKiDQoNCw0YHRiNC40YDQtdC90LjQtSDQsdC40LHQu9C40L7RgtC10LrQuCDQstCw0LvQuNC00LDRhtC40LhcclxuICogQG1vZHVsZVxyXG4gKi9cclxuXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuLy8gSW1wb3J0c1xyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG5pbXBvcnQgY2xhc3NSdWxlcyBmcm9tICcuL3ZhbGlkYXRlLWNsYXNzLXJ1bGVzJztcclxuaW1wb3J0IHdlem9tTWV0aG9kcyBmcm9tICcuL3ZhbGlkYXRlLXdlem9tLW1ldGhvZHMnO1xyXG5cclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4vLyBQcml2YXRlXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbi8qKlxyXG4gKiDQn9C+0LvRg9GH0LXQvdC40LUg0LjQvNC10L3QuCDRgtC40L/QsFxyXG4gKiBAcGFyYW0ge3N0cmluZ30gdHlwZVxyXG4gKiBAcGFyYW0ge2Jvb2xlYW59IFttdWx0aXBsZUZpbGVPclNlbGVjdF1cclxuICogQHJldHVybiB7c3RyaW5nfVxyXG4gKiBAcHJpdmF0ZVxyXG4gKi9cclxuZnVuY3Rpb24gZm9ybUdldFR5cGVOYW1lICh0eXBlLCBtdWx0aXBsZUZpbGVPclNlbGVjdCkge1xyXG5cdGxldCB0eXBlTmFtZTtcclxuXHRzd2l0Y2ggKHR5cGUpIHtcclxuXHRcdGNhc2UgJ3NlbGVjdC1vbmUnOlxyXG5cdFx0Y2FzZSAnc2VsZWN0LW11bHRpcGxlJzpcclxuXHRcdFx0dHlwZU5hbWUgPSAnX3NlbGVjdCc7XHJcblx0XHRcdGJyZWFrO1xyXG5cdFx0Y2FzZSAncmFkaW8nOlxyXG5cdFx0Y2FzZSAnY2hlY2tib3gnOlxyXG5cdFx0XHR0eXBlTmFtZSA9ICdfY2hlY2tlcic7XHJcblx0XHRcdGJyZWFrO1xyXG5cdFx0Y2FzZSAnZmlsZSc6XHJcblx0XHRcdHR5cGVOYW1lID0gbXVsdGlwbGVGaWxlT3JTZWxlY3QgPyAnX2ZpbGVzJyA6ICdfZmlsZSc7XHJcblx0XHRcdGJyZWFrO1xyXG5cdFx0ZGVmYXVsdDpcclxuXHRcdFx0dHlwZU5hbWUgPSAnJztcclxuXHR9XHJcblx0cmV0dXJuIHR5cGVOYW1lO1xyXG59XHJcblxyXG4vKipcclxuICog0J/QvtC70YPRh9C10L3QuNC1INGB0L7QvtCx0YnQtdC90LjRjyDQsiDQt9Cw0LLQuNGB0LjQvNC+0YHRgtC4INC+0YIg0Y3Qu9C10LzQtdC90YLQsFxyXG4gKiBAcGFyYW0ge0hUTUxFbGVtZW50fSBlbGVtZW50XHJcbiAqIEBwYXJhbSB7c3RyaW5nfSBtZXRob2RcclxuICogQHJldHVybiB7c3RyaW5nfVxyXG4gKiBAcHJpdmF0ZVxyXG4gKi9cclxuZnVuY3Rpb24gZm9ybUdldE1ldGhvZE1zZ05hbWUgKGVsZW1lbnQsIG1ldGhvZCkge1xyXG5cdGxldCB2YWx1ZSA9IGVsZW1lbnQudmFsdWU7XHJcblx0bGV0IG1ldGhvZE5hbWU7XHJcblx0c3dpdGNoIChtZXRob2QpIHtcclxuXHRcdGNhc2UgJ3JlcXVpcmVkJzpcclxuXHRcdGNhc2UgJ21heGxlbmd0aCc6XHJcblx0XHRjYXNlICdtaW5sZW5ndGgnOlxyXG5cdFx0Y2FzZSAncmFuZ2VsZW5ndGgnOlxyXG5cdFx0XHRtZXRob2ROYW1lID0gbWV0aG9kICsgZm9ybUdldFR5cGVOYW1lKGVsZW1lbnQudHlwZSwgZWxlbWVudC5tdWx0aXBsZSk7XHJcblx0XHRcdGJyZWFrO1xyXG5cdFx0Y2FzZSAncGFzc3dvcmQnOlxyXG5cdFx0XHRpZiAoL14oXFxzfFxcdCkqLy50ZXN0KHZhbHVlKSkge1xyXG5cdFx0XHRcdG1ldGhvZE5hbWUgPSBtZXRob2QgKyAnX3NwYWNlJztcclxuXHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHRtZXRob2ROYW1lID0gbWV0aG9kO1xyXG5cdFx0XHR9XHJcblx0XHRcdGJyZWFrO1xyXG5cdFx0Y2FzZSAndXJsJzpcclxuXHRcdFx0aWYgKCEvXihodHRwKHMpPzopP1xcL1xcLy9pLnRlc3QodmFsdWUpKSB7XHJcblx0XHRcdFx0bWV0aG9kTmFtZSA9IG1ldGhvZCArICdfcHJvdG9jb2wnO1xyXG5cdFx0XHR9IGVsc2UgaWYgKCEvKCg/IVxcLykuKVxcLi4qJC8udGVzdCh2YWx1ZSkpIHtcclxuXHRcdFx0XHRtZXRob2ROYW1lID0gbWV0aG9kICsgJ19kb21lbic7XHJcblx0XHRcdH0gZWxzZSBpZiAoIS9cXC4uezIsfSQvLnRlc3QodmFsdWUpKSB7XHJcblx0XHRcdFx0bWV0aG9kTmFtZSA9IG1ldGhvZCArICdfZG9tZW4tbGVuZ3RoJztcclxuXHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHRtZXRob2ROYW1lID0gbWV0aG9kO1xyXG5cdFx0XHR9XHJcblx0XHRcdGJyZWFrO1xyXG5cdFx0Y2FzZSAnZW1haWwnOlxyXG5cdFx0XHRpZiAoLyhAXFwufFxcLiQpLy50ZXN0KHZhbHVlKSkge1xyXG5cdFx0XHRcdG1ldGhvZE5hbWUgPSBtZXRob2QgKyAnX2RvdCc7XHJcblx0XHRcdH0gZWxzZSBpZiAoL0AuKihcXFxcfFxcLykvLnRlc3QodmFsdWUpKSB7XHJcblx0XHRcdFx0bWV0aG9kTmFtZSA9IG1ldGhvZCArICdfc2xhc2gnO1xyXG5cdFx0XHR9IGVsc2UgaWYgKCEvQC8udGVzdCh2YWx1ZSkpIHtcclxuXHRcdFx0XHRtZXRob2ROYW1lID0gbWV0aG9kICsgJ19hdCc7XHJcblx0XHRcdH0gZWxzZSBpZiAoL15ALy50ZXN0KHZhbHVlKSkge1xyXG5cdFx0XHRcdG1ldGhvZE5hbWUgPSBtZXRob2QgKyAnX2F0LXN0YXJ0JztcclxuXHRcdFx0fSBlbHNlIGlmICh2YWx1ZS5zcGxpdCgnQCcpLmxlbmd0aCA+IDIpIHtcclxuXHRcdFx0XHRtZXRob2ROYW1lID0gbWV0aG9kICsgJ19hdC1tdWx0aXBsZSc7XHJcblx0XHRcdH0gZWxzZSBpZiAoL0AkLy50ZXN0KHZhbHVlKSkge1xyXG5cdFx0XHRcdG1ldGhvZE5hbWUgPSBtZXRob2QgKyAnX2F0LWVuZCc7XHJcblx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0bWV0aG9kTmFtZSA9IG1ldGhvZDtcclxuXHRcdFx0fVxyXG5cdFx0XHRicmVhaztcclxuXHRcdGRlZmF1bHQ6XHJcblx0XHRcdG1ldGhvZE5hbWUgPSBtZXRob2Q7XHJcblx0fVxyXG5cdHJldHVybiBtZXRob2ROYW1lO1xyXG59XHJcblxyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbi8vIFB1YmxpY1xyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG4vKipcclxuICog0J7QsdGK0LXQutGCINGA0LDRgdGI0LjRgNC10L3QuNC5XHJcbiAqIEBjb25zdCB7T2JqZWN0fVxyXG4gKi9cclxuY29uc3QgZXh0ZW5kQ29uZmlnID0ge1xyXG5cdC8qKlxyXG5cdCAqINCf0YDQvtGC0L7RgtC40L/RiyDQv9C70LDQs9C40L3QsFxyXG5cdCAqIEB2YXIge09iamVjdH0gbWVzc2FnZXNcclxuXHQgKiBAc291cmNlQ29kZSB8KzMwXHJcblx0ICovXHJcblx0Z2V0IG1lc3NhZ2VzICgpIHtcclxuXHRcdGxldCB0cmFuc2xhdGVzID0gKHdpbmRvdy5qc1RyYW5zbGF0aW9ucyAmJiB3aW5kb3cuanNUcmFuc2xhdGlvbnNbJ2pxdWVyeS12YWxpZGF0aW9uJ10pIHx8IHt9O1xyXG5cdFx0aWYgKE9iamVjdC5rZXlzKHRyYW5zbGF0ZXMpLmxlbmd0aCA9PT0gMCkge1xyXG5cdFx0XHRyZXR1cm4gY29uc29sZS53YXJuKCfQn9C10YDQtdCy0L7QtNGLINC00LvRjyBqcXVlcnktdmFsaWRhdGlvbiAtINC+0YLRgdGD0YLRgdGC0LLRg9GO0YIhJyk7XHJcblx0XHR9XHJcblxyXG5cdFx0Zm9yIChsZXQga2V5IGluIHRyYW5zbGF0ZXMpIHtcclxuXHRcdFx0bGV0IHZhbHVlID0gdHJhbnNsYXRlc1trZXldO1xyXG5cclxuXHRcdFx0c3dpdGNoIChrZXkpIHtcclxuXHRcdFx0XHRjYXNlICdtYXhsZW5ndGgnOlxyXG5cdFx0XHRcdGNhc2UgJ21heGxlbmd0aF9jaGVja2VyJzpcclxuXHRcdFx0XHRjYXNlICdtYXhsZW5ndGhfc2VsZWN0JzpcclxuXHRcdFx0XHRjYXNlICdtaW5sZW5ndGgnOlxyXG5cdFx0XHRcdGNhc2UgJ21pbmxlbmd0aF9jaGVja2VyJzpcclxuXHRcdFx0XHRjYXNlICdtaW5sZW5ndGhfc2VsZWN0JzpcclxuXHRcdFx0XHRjYXNlICdyYW5nZWxlbmd0aCc6XHJcblx0XHRcdFx0Y2FzZSAncmFuZ2VsZW5ndGhfY2hlY2tlcic6XHJcblx0XHRcdFx0Y2FzZSAncmFuZ2VsZW5ndGhfc2VsZWN0JzpcclxuXHRcdFx0XHRjYXNlICdyYW5nZSc6XHJcblx0XHRcdFx0Y2FzZSAnbWluJzpcclxuXHRcdFx0XHRjYXNlICdtYXgnOlxyXG5cdFx0XHRcdGNhc2UgJ2ZpbGV0eXBlJzpcclxuXHRcdFx0XHRjYXNlICdmaWxlc2l6ZSc6XHJcblx0XHRcdFx0Y2FzZSAnZmlsZXNpemVFYWNoJzpcclxuXHRcdFx0XHRjYXNlICdwYXR0ZXJuJzpcclxuXHRcdFx0XHRcdHRyYW5zbGF0ZXNba2V5XSA9ICQudmFsaWRhdG9yLmZvcm1hdCh2YWx1ZSk7XHJcblx0XHRcdFx0XHRicmVhaztcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cclxuXHRcdHJldHVybiB0cmFuc2xhdGVzO1xyXG5cdH0sXHJcblxyXG5cdC8qKlxyXG5cdCAqINCf0YDQvtGC0L7RgtC40L/RiyDQv9C70LDQs9C40L3QsFxyXG5cdCAqIEB2YXIge09iamVjdH0gcHJvdG90eXBlXHJcblx0ICovXHJcblx0cHJvdG90eXBlOiB7XHJcblx0XHQvKipcclxuXHRcdCAqINCS0YvQstC+0LQg0LTQtdGE0L7Qu9GC0L3Ri9GFINGB0L7QvtCx0YnQtdC90LjQuVxyXG5cdFx0ICogQHBhcmFtIHtFbGVtZW50fSBlbGVtZW50XHJcblx0XHQgKiBAcGFyYW0ge09iamVjdHxzdHJpbmd9IHJ1bGVcclxuXHRcdCAqIEByZXR1cm4ge3N0cmluZ31cclxuXHRcdCAqIEBtZXRob2QgcHJvdG90eXBlOjpkZWZhdWx0TWVzc2FnZVxyXG5cdFx0ICovXHJcblx0XHRkZWZhdWx0TWVzc2FnZSAoZWxlbWVudCwgcnVsZSkge1xyXG5cdFx0XHRpZiAodHlwZW9mIHJ1bGUgPT09ICdzdHJpbmcnKSB7XHJcblx0XHRcdFx0cnVsZSA9IHttZXRob2Q6IHJ1bGV9O1xyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHRsZXQgbWVzc2FnZSA9IHRoaXMuZmluZERlZmluZWQoXHJcblx0XHRcdFx0dGhpcy5jdXN0b21NZXNzYWdlKGVsZW1lbnQubmFtZSwgcnVsZS5tZXRob2QpLFxyXG5cdFx0XHRcdHRoaXMuY3VzdG9tRGF0YU1lc3NhZ2UoZWxlbWVudCwgcnVsZS5tZXRob2QpLFxyXG5cdFx0XHRcdCF0aGlzLnNldHRpbmdzLmlnbm9yZVRpdGxlICYmIGVsZW1lbnQudGl0bGUgfHwgdW5kZWZpbmVkLCAvLyBlc2xpbnQtZGlzYWJsZS1saW5lIG5vLW1peGVkLW9wZXJhdG9yc1xyXG5cdFx0XHRcdCQudmFsaWRhdG9yLm1lc3NhZ2VzW2Zvcm1HZXRNZXRob2RNc2dOYW1lKGVsZW1lbnQsIHJ1bGUubWV0aG9kKV0sXHJcblx0XHRcdFx0JzxzdHJvbmc+V2FybmluZzogTm8gbWVzc2FnZSBkZWZpbmVkIGZvciAnICsgZWxlbWVudC5uYW1lICsgJzwvc3Ryb25nPidcclxuXHRcdFx0KTtcclxuXHJcblx0XHRcdGxldCBwYXR0ZXJuID0gL1xcJD9cXHsoXFxkKylcXH0vZztcclxuXHJcblx0XHRcdGlmICh0eXBlb2YgbWVzc2FnZSA9PT0gJ2Z1bmN0aW9uJykge1xyXG5cdFx0XHRcdG1lc3NhZ2UgPSBtZXNzYWdlLmNhbGwodGhpcywgcnVsZS5wYXJhbWV0ZXJzLCBlbGVtZW50KTtcclxuXHRcdFx0fSBlbHNlIGlmIChwYXR0ZXJuLnRlc3QobWVzc2FnZSkpIHtcclxuXHRcdFx0XHRtZXNzYWdlID0gJC52YWxpZGF0b3IuZm9ybWF0KG1lc3NhZ2UucmVwbGFjZShwYXR0ZXJuLCAneyQxfScpLCBydWxlLnBhcmFtZXRlcnMpO1xyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHRyZXR1cm4gbWVzc2FnZTtcclxuXHRcdH1cclxuXHR9LFxyXG5cclxuXHQvKipcclxuXHQgKiDQnNC10YLQvtC00Ysg0LLQsNC70LjQtNCw0YbQuNC4XHJcblx0ICogQHZhciB7T2JqZWN0fSBtZXRob2RzXHJcblx0ICovXHJcblx0bWV0aG9kczogd2V6b21NZXRob2RzXHJcbn07XHJcblxyXG5mb3IgKGxldCBrZXkgaW4gZXh0ZW5kQ29uZmlnKSB7XHJcblx0JC5leHRlbmQoJC52YWxpZGF0b3Jba2V5XSwgZXh0ZW5kQ29uZmlnW2tleV0pO1xyXG59XHJcblxyXG4kLnZhbGlkYXRvci5hZGRDbGFzc1J1bGVzKGNsYXNzUnVsZXMpO1xyXG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9fSFRNTC9zcmMvanMvX21vZHVsZXMvanF1ZXJ5LXZhbGlkYXRpb24vZXh0ZW5kL3ZhbGlkYXRlLWV4dGVuZC5qcyIsIid1c2Ugc3RyaWN0JztcclxuXHJcbi8qKlxyXG4gKiDQoNCw0YHRiNC40YDQtdC90LjQtSDQsNCy0YLQvtC80LDRgtC40YfQtdGB0LrQvtC5INCy0LDQu9C40LTQsNGG0LjQuCDQv9C+INC40LzQtdC90LggQ1NTINC60LvQsNGB0YHQvtCyXHJcbiAqIEBtb2R1bGVcclxuICovXHJcblxyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbi8vIFB1YmxpY1xyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG4vKipcclxuICog0KHQv9C40YHQvtC6INC/0YDQsNCy0LjQuyDQtNC70Y8g0LLQsNC70LjQtNCw0YbQuNC4INGN0LvQtdC80LXQvdGC0L7QsiDQv9C+INC+0LTQvdC+0LzRgyDQutC70LDRgdGB0YNcclxuICog0JrQsNC20LTQvtC1INGB0LLQvtC50YHRgtCy0L4gLSBjc3Mg0LrQu9Cw0YHRgVxyXG4gKiBAdHlwZSB7T2JqZWN0fVxyXG4gKiBAc291cmNlQ29kZVxyXG4gKi9cclxuY29uc3QgY2xhc3NSdWxlcyA9IHtcclxuXHQndmFsaWRhdGUtYXMtbG9naW4nOiB7XHJcblx0XHRtaW5sZW5ndGg6IDIsXHJcblx0XHRsb2dpbjogdHJ1ZSxcclxuXHRcdG1heGxlbmd0aDogNixcclxuXHRcdHJlcXVpcmVkOiB0cnVlXHJcblx0fSxcclxuXHQndmFsaWRhdGUtYXMtZmlsZS1pbWFnZXMnOiB7XHJcblx0XHRmaWxldHlwZTogJ3BuZ3xqcGU/Z3xnaWZ8c3ZnJyxcclxuXHRcdC8vIGZpbGVzaXplZWFjaDogNTAwLFxyXG5cdFx0ZmlsZXNpemU6IDUwMCxcclxuXHRcdG1heHVwbG9hZDogMixcclxuXHRcdHJlcXVpcmVkOiB0cnVlXHJcblx0fVxyXG59O1xyXG5cclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4vLyBFeHBvcnRzXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzUnVsZXM7XHJcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL19IVE1ML3NyYy9qcy9fbW9kdWxlcy9qcXVlcnktdmFsaWRhdGlvbi9leHRlbmQvdmFsaWRhdGUtY2xhc3MtcnVsZXMuanMiLCIndXNlIHN0cmljdCc7XHJcblxyXG4vKipcclxuICog0JTQvtC/0L7Qu9C90LjRgtC10LvRjNC90YvQtSDQvNC10YLQvtC00Ysg0LLQsNC70LjQtNCw0YbQuNC4XHJcbiAqIEBtb2R1bGVcclxuICovXHJcblxyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbi8vIFB1YmxpY1xyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG4vKipcclxuICog0JzQtdGC0L7QtNGLINCy0LDQu9C40LTQsNGG0LjQuFxyXG4gKiBAdmFyIHtPYmplY3R9IG1ldGhvZHNcclxuICovXHJcbmNvbnN0IG1ldGhvZHMgPSB7XHJcblx0LyoqXHJcblx0ICog0JLQsNC70LjQtNCw0YbQuNGPIGVtYWlsXHJcblx0ICogQHBhcmFtIHtzdHJpbmd9IHZhbHVlXHJcblx0ICogQHBhcmFtIHtIVE1MSW5wdXRFbGVtZW50fEhUTUxUZXh0QXJlYUVsZW1lbnR9IGVsZW1lbnRcclxuXHQgKiBAcGFyYW0ge09iamVjdH0gcGFyYW1cclxuXHQgKiBAcmV0dXJuIHtib29sZWFufVxyXG5cdCAqIEBtZXRob2QgbWV0aG9kczo6ZW1haWxcclxuXHQgKiBAc291cmNlQ29kZSB8KzRcclxuXHQgKi9cclxuXHRlbWFpbCAodmFsdWUsIGVsZW1lbnQpIHtcclxuXHRcdGxldCBwYXR0ZXJuID0gL14oKFthLXpBLVowLTkmKy09X10pKygoXFwuKFthLXpBLVowLTkmKy09X10pezEsfSkrKT8pezEsNjR9QChbYS16QS1aMC05XShbYS16QS1aMC05LV17MCw2MX1bYS16QS1aMC05XSk/XFwuKStbYS16QS1aXXsyLDZ9JC87XHJcblx0XHRyZXR1cm4gdGhpcy5vcHRpb25hbChlbGVtZW50KSB8fCBwYXR0ZXJuLnRlc3QodmFsdWUpO1xyXG5cdH0sXHJcblxyXG5cdC8qKlxyXG5cdCAqINCS0LDQu9C40LTQsNGG0LjRjyDQv9C+INC/0LDRgtGC0LXRgNC90YNcclxuXHQgKiBAcGFyYW0ge3N0cmluZ30gdmFsdWVcclxuXHQgKiBAcGFyYW0ge0hUTUxJbnB1dEVsZW1lbnR8SFRNTFRleHRBcmVhRWxlbWVudH0gZWxlbWVudFxyXG5cdCAqIEBwYXJhbSB7c3RyaW5nfFJlZ0V4cH0gcGFyYW1cclxuXHQgKiBAcmV0dXJuIHtib29sZWFufVxyXG5cdCAqIEBtZXRob2QgbWV0aG9kczo6cGF0dGVyblxyXG5cdCAqIEBzb3VyY2VDb2RlIHwrOVxyXG5cdCAqL1xyXG5cdHBhdHRlcm4gKHZhbHVlLCBlbGVtZW50LCBwYXJhbSkge1xyXG5cdFx0aWYgKHRoaXMub3B0aW9uYWwoZWxlbWVudCkpIHtcclxuXHRcdFx0cmV0dXJuIHRydWU7XHJcblx0XHR9XHJcblx0XHRpZiAodHlwZW9mIHBhcmFtID09PSAnc3RyaW5nJykge1xyXG5cdFx0XHRwYXJhbSA9IG5ldyBSZWdFeHAoYF4oPzoke3BhcmFtfSkkYCk7XHJcblx0XHR9XHJcblx0XHRyZXR1cm4gcGFyYW0udGVzdCh2YWx1ZSk7XHJcblx0fSxcclxuXHJcblx0LyoqXHJcblx0ICog0JLQsNC70LjQtNCw0YbQuNGPINC/0LDRgNC+0LvQtdC5XHJcblx0ICogQHBhcmFtIHtzdHJpbmd9IHZhbHVlXHJcblx0ICogQHBhcmFtIHtIVE1MSW5wdXRFbGVtZW50fSBlbGVtZW50XHJcblx0ICogQHBhcmFtIHtPYmplY3R9IHBhcmFtXHJcblx0ICogQHJldHVybiB7Ym9vbGVhbn1cclxuXHQgKiBAbWV0aG9kIG1ldGhvZHM6OnBhc3N3b3JkXHJcblx0ICogQHNvdXJjZUNvZGUgfCszXHJcblx0ICovXHJcblx0cGFzc3dvcmQgKHZhbHVlLCBlbGVtZW50KSB7XHJcblx0XHRyZXR1cm4gdGhpcy5vcHRpb25hbChlbGVtZW50KSB8fCAvXlxcUy4qJC8udGVzdCh2YWx1ZSk7XHJcblx0fSxcclxuXHJcblx0LyoqXHJcblx0ICog0J/RgNC+0LLQtdGA0LrQsCDQvtCx0YnQtdCz0L4g0L7QsdGK0LXQvNCwINCy0YHQtdGFINGE0LDQudC70LAg0LIgS0JcclxuXHQgKiBAcGFyYW0ge3N0cmluZ30gdmFsdWVcclxuXHQgKiBAcGFyYW0ge0hUTUxJbnB1dEVsZW1lbnR9IGVsZW1lbnRcclxuXHQgKiBAcGFyYW0ge09iamVjdH0gcGFyYW1cclxuXHQgKiBAcmV0dXJuIHtib29sZWFufVxyXG5cdCAqIEBtZXRob2QgbWV0aG9kczo6ZmlsZXNpemVcclxuXHQgKiBAc291cmNlQ29kZSB8KzdcclxuXHQgKi9cclxuXHRmaWxlc2l6ZSAodmFsdWUsIGVsZW1lbnQsIHBhcmFtKSB7XHJcblx0XHRsZXQga2IgPSAwO1xyXG5cdFx0Zm9yIChsZXQgaSA9IDA7IGkgPCBlbGVtZW50LmZpbGVzLmxlbmd0aDsgaSsrKSB7XHJcblx0XHRcdGtiICs9IGVsZW1lbnQuZmlsZXNbaV0uc2l6ZTtcclxuXHRcdH1cclxuXHRcdHJldHVybiB0aGlzLm9wdGlvbmFsKGVsZW1lbnQpIHx8IChrYiAvIDEwMjQgPD0gcGFyYW0pO1xyXG5cdH0sXHJcblxyXG5cdC8qKlxyXG5cdCAqINCc0LDQutGB0LjQvNCw0LvRjNC90L7QtSDQutC+0LvQuNGH0LXRgdGC0LLQviDRhNCw0LnQu9C+0LIg0LTQu9GPINC30LDQs9GA0YPQt9C60LhcclxuXHQgKiBAcGFyYW0ge3N0cmluZ30gdmFsdWVcclxuXHQgKiBAcGFyYW0ge0hUTUxJbnB1dEVsZW1lbnR9IGVsZW1lbnRcclxuXHQgKiBAcGFyYW0ge09iamVjdH0gcGFyYW1cclxuXHQgKiBAcmV0dXJuIHtib29sZWFufVxyXG5cdCAqIEBtZXRob2QgbWV0aG9kczo6bWF4dXBsb2FkXHJcblx0ICogQHNvdXJjZUNvZGUgfCszXHJcblx0ICovXHJcblx0bWF4dXBsb2FkICh2YWx1ZSwgZWxlbWVudCwgcGFyYW0pIHtcclxuXHRcdHJldHVybiBlbGVtZW50LmZpbGVzLmxlbmd0aCA8PSBwYXJhbTtcclxuXHR9LFxyXG5cclxuXHQvKipcclxuXHQgKiDQn9GA0L7QstC10YDQutCwINC+0LHRitC10LzQsCDQutCw0LbQtNC+0LPQviDRhNCw0LnQu9CwINCyIEtCXHJcblx0ICogQHBhcmFtIHtzdHJpbmd9IHZhbHVlXHJcblx0ICogQHBhcmFtIHtIVE1MSW5wdXRFbGVtZW50fSBlbGVtZW50XHJcblx0ICogQHBhcmFtIHtPYmplY3R9IHBhcmFtXHJcblx0ICogQHJldHVybiB7Ym9vbGVhbn1cclxuXHQgKiBAbWV0aG9kIG1ldGhvZHM6OmZpbGVzaXplZWFjaFxyXG5cdCAqIEBzb3VyY2VDb2RlIHwrMTBcclxuXHQgKi9cclxuXHRmaWxlc2l6ZWVhY2ggKHZhbHVlLCBlbGVtZW50LCBwYXJhbSkge1xyXG5cdFx0bGV0IGZsYWcgPSB0cnVlO1xyXG5cdFx0Zm9yIChsZXQgaSA9IDA7IGkgPCBlbGVtZW50LmZpbGVzLmxlbmd0aDsgaSsrKSB7XHJcblx0XHRcdGlmIChlbGVtZW50LmZpbGVzW2ldLnNpemUgLyAxMDI0ID4gcGFyYW0pIHtcclxuXHRcdFx0XHRmbGFnID0gZmFsc2U7XHJcblx0XHRcdFx0YnJlYWs7XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHRcdHJldHVybiB0aGlzLm9wdGlvbmFsKGVsZW1lbnQpIHx8IGZsYWc7XHJcblx0fSxcclxuXHJcblx0LyoqXHJcblx0ICog0J/RgNC+0LLQtdGA0LrQsCDRgtC40L/QsCDRhNCw0LnQu9CwINC/0L4g0YDQsNGB0YjQuNGA0LXQvdC40Y5cclxuXHQgKiBAcGFyYW0ge3N0cmluZ30gdmFsdWVcclxuXHQgKiBAcGFyYW0ge0hUTUxJbnB1dEVsZW1lbnR9IGVsZW1lbnRcclxuXHQgKiBAcGFyYW0ge09iamVjdH0gcGFyYW1cclxuXHQgKiBAcmV0dXJuIHtib29sZWFufVxyXG5cdCAqIEBtZXRob2QgbWV0aG9kczo6ZmlsZXR5cGVcclxuXHQgKiBAc291cmNlQ29kZSB8KzI1XHJcblx0ICovXHJcblx0ZmlsZXR5cGUgKHZhbHVlLCBlbGVtZW50LCBwYXJhbSkge1xyXG5cdFx0bGV0IHJlc3VsdDtcclxuXHRcdGxldCBleHRlbnNpb25zID0gJ3BuZ3xqcGU/Z3xnaWZ8c3ZnfGRvY3xwZGZ8emlwfHJhcnx0YXJ8aHRtbHxzd2Z8dHh0fHhsc3xkb2N4fHhsc3h8b2R0JztcclxuXHRcdGlmIChlbGVtZW50LmZpbGVzLmxlbmd0aCA8IDEpIHtcclxuXHRcdFx0cmV0dXJuIHRydWU7XHJcblx0XHR9XHJcblxyXG5cdFx0cGFyYW0gPSB0eXBlb2YgcGFyYW0gPT09ICdzdHJpbmcnID8gcGFyYW0ucmVwbGFjZSgvLC9nLCAnfCcpIDogZXh0ZW5zaW9ucztcclxuXHRcdGlmIChlbGVtZW50Lm11bHRpcGxlKSB7XHJcblx0XHRcdGxldCBmaWxlcyA9IGVsZW1lbnQuZmlsZXM7XHJcblx0XHRcdGZvciAobGV0IGkgPSAwOyBpIDwgZmlsZXMubGVuZ3RoOyBpKyspIHtcclxuXHRcdFx0XHRsZXQgdmFsdWUgPSBmaWxlc1tpXS5uYW1lO1xyXG5cdFx0XHRcdGxldCB2YWx1ZU1hdGNoID0gdmFsdWUubWF0Y2gobmV3IFJlZ0V4cCgnLignICsgcGFyYW0gKyAnKSQnLCAnaScpKTtcclxuXHJcblx0XHRcdFx0cmVzdWx0ID0gdGhpcy5vcHRpb25hbChlbGVtZW50KSB8fCB2YWx1ZU1hdGNoO1xyXG5cdFx0XHRcdGlmIChyZXN1bHQgPT09IG51bGwpIHtcclxuXHRcdFx0XHRcdGJyZWFrO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cdFx0fSBlbHNlIHtcclxuXHRcdFx0bGV0IHZhbHVlTWF0Y2ggPSB2YWx1ZS5tYXRjaChuZXcgUmVnRXhwKCdcXFxcLignICsgcGFyYW0gKyAnKSQnLCAnaScpKTtcclxuXHRcdFx0cmVzdWx0ID0gdGhpcy5vcHRpb25hbChlbGVtZW50KSB8fCB2YWx1ZU1hdGNoO1xyXG5cdFx0fVxyXG5cdFx0cmV0dXJuIHJlc3VsdDtcclxuXHR9LFxyXG5cclxuXHQvKipcclxuXHQgKiDQn9GA0L7QstC10YDQutCwINCy0LDQu9C40LTQvdC+0YHRgtC4INC+0LTQvdC+0LPQviDQuNC3INGD0LrQsNC30LDQvdC90YvRhSDRjdC70LXQvNC10L3RgtC+0LJcclxuXHQgKiBf0Y3RgtC+0YIg0LjQu9C4INC00YDRg9Cz0L7QuSAtINC00L7Qu9C20LXQvSDQsdGL0YLRjCDQstCw0LvQuNC00L3Ri9C8X1xyXG5cdCAqIEBwYXJhbSB7c3RyaW5nfSB2YWx1ZVxyXG5cdCAqIEBwYXJhbSB7SFRNTElucHV0RWxlbWVudHxIVE1MVGV4dEFyZWFFbGVtZW50fEhUTUxTZWxlY3RFbGVtZW50fSBlbGVtZW50XHJcblx0ICogQHBhcmFtIHtPYmplY3R9IHBhcmFtXHJcblx0ICogQHJldHVybiB7Ym9vbGVhbn1cclxuXHQgKiBAbWV0aG9kIG1ldGhvZHM6Om9yXHJcblx0ICogQHNvdXJjZUNvZGUgfCs0XHJcblx0ICovXHJcblx0b3IgKHZhbHVlLCBlbGVtZW50LCBwYXJhbSkge1xyXG5cdFx0bGV0ICRtb2R1bGUgPSAkKGVsZW1lbnQuZm9ybSk7XHJcblx0XHRyZXR1cm4gJG1vZHVsZS5maW5kKHBhcmFtICsgJzpmaWxsZWQnKS5sZW5ndGg7XHJcblx0fSxcclxuXHJcblx0LyoqXHJcblx0ICog0J/RgNC+0LLQtdGA0LrQsCDQstCw0LvQuNC00L3QvtGB0YLQuCDRgdC70L7QsiAo0YfQsNGJ0LUg0LLRgdC10LPQviDQuNGB0L/QvtC70YzQt9GD0LXRgtGB0Y8g0LTQu9GPINC40LzQtdC90Lgg0LjQu9C4INGE0LDQvNC40LvQuNC4KVxyXG5cdCAqIEBwYXJhbSB7c3RyaW5nfSB2YWx1ZVxyXG5cdCAqIEBwYXJhbSB7SFRNTElucHV0RWxlbWVudHxIVE1MVGV4dEFyZWFFbGVtZW50fSBlbGVtZW50XHJcblx0ICogQHJldHVybiB7Ym9vbGVhbn1cclxuXHQgKiBAbWV0aG9kIG1ldGhvZHM6OndvcmRcclxuXHQgKiBAc291cmNlQ29kZSB8KzRcclxuXHQgKi9cclxuXHR3b3JkICh2YWx1ZSwgZWxlbWVudCkge1xyXG5cdFx0bGV0IHRlc3RWYWx1ZSA9IC9eW2EtekEtWtCwLdGP0JAt0K/RltCG0ZfQh9GU0ZHQgdCE0pHSkMSExIXEhsSHxJjEmcWBxYLFg8WEw5PDs8WaxZvFucW6xbvFvFxcJ1xcYFxcLSBdKiQvLnRlc3QodmFsdWUpOyAvLyBlc2xpbnQtZGlzYWJsZS1saW5lIG5vLXVzZWxlc3MtZXNjYXBlXHJcblx0XHRyZXR1cm4gdGhpcy5vcHRpb25hbChlbGVtZW50KSB8fCB0ZXN0VmFsdWU7XHJcblx0fSxcclxuXHJcblx0LyoqXHJcblx0ICog0J/RgNC+0LLQtdGA0LrQsCDQstCw0LvQuNC00L3QvtGB0YLQuCDQu9C+0LPQuNC90L7QslxyXG5cdCAqIEBwYXJhbSB7c3RyaW5nfSB2YWx1ZVxyXG5cdCAqIEBwYXJhbSB7SFRNTElucHV0RWxlbWVudHxIVE1MVGV4dEFyZWFFbGVtZW50fSBlbGVtZW50XHJcblx0ICogQHBhcmFtIHtPYmplY3R9IHBhcmFtXHJcblx0ICogQHJldHVybiB7Ym9vbGVhbn1cclxuXHQgKiBAbWV0aG9kIG1ldGhvZHM6OmxvZ2luXHJcblx0ICogQHNvdXJjZUNvZGUgfCs0XHJcblx0ICovXHJcblx0bG9naW4gKHZhbHVlLCBlbGVtZW50KSB7XHJcblx0XHRsZXQgdGVzdFZhbHVlID0gL15bMC05YS16QS1a0LAt0Y/QkC3Qr9GW0IbRl9CH0ZTQhNGR0IHSkdKQXVswLTlhLXpBLVrQsC3Rj9CQLdCv0ZbQhtGX0IfRlNCE0pHSkMSExIXEhsSHxJjEmcWBxYLFg8WEw5PDs8WaxZvFucW6xbvFvFxcLVxcLl9dKyQvLnRlc3QodmFsdWUpOyAvLyBlc2xpbnQtZGlzYWJsZS1saW5lIG5vLXVzZWxlc3MtZXNjYXBlXHJcblx0XHRyZXR1cm4gdGhpcy5vcHRpb25hbChlbGVtZW50KSB8fCB0ZXN0VmFsdWU7XHJcblx0fSxcclxuXHJcblx0LyoqXHJcblx0ICog0JLQsNC70LjQtNCw0YbQuNGPINC90L7QvNC10YDQsCDRgtC10LvQtdGE0L7QvdCwICjRg9C60YApXHJcblx0ICogQHBhcmFtIHtzdHJpbmd9IHZhbHVlXHJcblx0ICogQHBhcmFtIHtIVE1MSW5wdXRFbGVtZW50fEhUTUxUZXh0QXJlYUVsZW1lbnR9IGVsZW1lbnRcclxuXHQgKiBAcmV0dXJuIHtib29sZWFufVxyXG5cdCAqIEBtZXRob2QgbWV0aG9kczo6cGhvbmV1YVxyXG5cdCAqIEBzb3VyY2VDb2RlIHwrM1xyXG5cdCAqL1xyXG5cdHBob25ldWEgKHZhbHVlLCBlbGVtZW50KSB7XHJcblx0XHRmdW5jdGlvbiB0ZXN0VmFsdWUgKHZhbHVlKSB7XHJcblx0XHRcdGlmICgvXihcXCspPzM4Ly50ZXN0KHZhbHVlKSkge1xyXG5cdFx0XHRcdHJldHVybiAvXigoKFxcKz8pKDM4KSlcXHM/KSgoWzAtOV17M30pfChcXChbMC05XXszfVxcKSkpKFxcLXxcXHMpPygoWzAtOV17M30pKFxcLXxcXHMpPyhbMC05XXsyfSkoXFwtfFxccyk/KFswLTldezJ9KXwoWzAtOV17Mn0pKFxcLXxcXHMpPyhbMC05XXsyfSkoXFwtfFxccyk/KFswLTldezN9KXwoWzAtOV17Mn0pKFxcLXxcXHMpPyhbMC05XXszfSkoXFwtfFxccyk/KFswLTldezJ9KSkkLy50ZXN0KHZhbHVlKTsgLy8gZXNsaW50LWRpc2FibGUtbGluZSBuby11c2VsZXNzLWVzY2FwZVxyXG5cdFx0XHR9XHJcblx0XHRcdHJldHVybiAvXigoWzAtOV17M30pfChcXChbMC05XXszfVxcKSkpKFxcLXxcXHMpPygoWzAtOV17M30pKFxcLXxcXHMpPyhbMC05XXsyfSkoXFwtfFxccyk/KFswLTldezJ9KXwoWzAtOV17Mn0pKFxcLXxcXHMpPyhbMC05XXsyfSkoXFwtfFxccyk/KFswLTldezN9KXwoWzAtOV17Mn0pKFxcLXxcXHMpPyhbMC05XXszfSkoXFwtfFxccyk/KFswLTldezJ9KSkkLy50ZXN0KHZhbHVlKTsgLy8gZXNsaW50LWRpc2FibGUtbGluZSBuby11c2VsZXNzLWVzY2FwZVxyXG5cdFx0fVxyXG5cclxuXHRcdHJldHVybiB0aGlzLm9wdGlvbmFsKGVsZW1lbnQpIHx8IHRlc3RWYWx1ZSh2YWx1ZSk7XHJcblx0fSxcclxuXHJcblx0LyoqXHJcblx0ICog0JLQsNC70LjQtNCw0YbQuNGPINC90L7QvNC10YDQsCDRgtC10LvQtdGE0L7QvdCwICjQvtCx0YnQsNGPKVxyXG5cdCAqIEBwYXJhbSB7c3RyaW5nfSB2YWx1ZVxyXG5cdCAqIEBwYXJhbSB7SFRNTElucHV0RWxlbWVudHxIVE1MVGV4dEFyZWFFbGVtZW50fSBlbGVtZW50XHJcblx0ICogQHBhcmFtIHtPYmplY3R9IHBhcmFtXHJcblx0ICogQHJldHVybiB7Ym9vbGVhbn1cclxuXHQgKiBAbWV0aG9kIG1ldGhvZHM6OnBob25lXHJcblx0ICogQHNvdXJjZUNvZGUgfCszXHJcblx0ICovXHJcblx0cGhvbmUgKHZhbHVlLCBlbGVtZW50KSB7XHJcblx0XHRyZXR1cm4gdGhpcy5vcHRpb25hbChlbGVtZW50KSB8fCAvXigoKFxcKz8pKFxcZHsxLDN9KSlcXHM/KT8oKFswLTldezAsNH0pfChcXChbMC05XXszfVxcKSkpKFxcLXxcXHMpPygoWzAtOV17M30pKFxcLXxcXHMpPyhbMC05XXsyfSkoXFwtfFxccyk/KFswLTldezJ9KXwoWzAtOV17Mn0pKFxcLXxcXHMpPyhbMC05XXsyfSkoXFwtfFxccyk/KFswLTldezN9KXwoWzAtOV17Mn0pKFxcLXxcXHMpPyhbMC05XXszfSkoXFwtfFxccyk/KFswLTldezJ9KSkkLy50ZXN0KHZhbHVlKTsgLy8gZXNsaW50LWRpc2FibGUtbGluZSBuby11c2VsZXNzLWVzY2FwZVxyXG5cdH0sXHJcblxyXG5cdC8qKlxyXG5cdCAqINCf0YDQvtCy0LXRgNC60Lgg0L/RgNC+0LHQtdC70L7QsiDQsiDQt9C90LDRh9C10L3QuNC4XHJcblx0ICogQHBhcmFtIHtzdHJpbmd9IHZhbHVlXHJcblx0ICogQHJldHVybiB7Ym9vbGVhbn1cclxuXHQgKiBAbWV0aG9kIG1ldGhvZHM6Om5vc3BhY2VcclxuXHQgKiBAc291cmNlQ29kZSB8KzRcclxuXHQgKi9cclxuXHRub3NwYWNlICh2YWx1ZSkge1xyXG5cdFx0bGV0IHN0ciA9IFN0cmluZyh2YWx1ZSkucmVwbGFjZSgvXFxzfFxcdHxcXHJ8XFxuL2csICcnKTtcclxuXHRcdHJldHVybiBzdHIubGVuZ3RoID4gMDtcclxuXHR9LFxyXG5cclxuXHQvKipcclxuXHQgKiDQn9GA0L7QstC10YDQutC4INCy0LDQu9C40LTQvdC+0YHRgtC4INGN0LvQtdC80LXQvdGC0LBcclxuXHQgKiBAcGFyYW0ge3N0cmluZ30gdmFsdWVcclxuXHQgKiBAcGFyYW0ge0hUTUxJbnB1dEVsZW1lbnR8SFRNTFRleHRBcmVhRWxlbWVudHxIVE1MU2VsZWN0RWxlbWVudH0gZWxlbWVudFxyXG5cdCAqIEByZXR1cm4ge2Jvb2xlYW59XHJcblx0ICogQG1ldGhvZCBtZXRob2RzOjp2YWxpZFRydWVcclxuXHQgKiBAc291cmNlQ29kZSB8KzNcclxuXHQgKi9cclxuXHR2YWxpZGRhdGEgKHZhbHVlLCBlbGVtZW50KSB7XHJcblx0XHRyZXR1cm4gJChlbGVtZW50KS5kYXRhKCd2YWxpZCcpID09PSB0cnVlO1xyXG5cdH1cclxufTtcclxuXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuLy8gRXhwb3J0c1xyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG5leHBvcnQgZGVmYXVsdCBtZXRob2RzO1xyXG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9fSFRNTC9zcmMvanMvX21vZHVsZXMvanF1ZXJ5LXZhbGlkYXRpb24vZXh0ZW5kL3ZhbGlkYXRlLXdlem9tLW1ldGhvZHMuanMiLCIndXNlIHN0cmljdCc7XHJcblxyXG4vKipcclxuICog0J/QvtC70YzQt9C+0LLQsNGC0LXQu9GM0YHQutC40LUg0L7QsdGA0LDQsdC+0YLRh9C40LrQuCDRhNC+0YDQvNGLXHJcbiAqIEBtb2R1bGVcclxuICovXHJcblxyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbi8vIFB1YmxpY1xyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG4vKipcclxuICogQHBhcmFtIHtKUXVlcnl9ICRmb3JtXHJcbiAqIEBwYXJhbSB7T2JqZWN0fSBWYWxpZGF0b3Ige0BsaW5rIGh0dHBzOi8vanF1ZXJ5dmFsaWRhdGlvbi5vcmcvP3M9dmFsaWRhdG9yfVxyXG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSBWYWxpZGF0b3IucmVzZXRGb3JtIHtAbGluayBodHRwczovL2pxdWVyeXZhbGlkYXRpb24ub3JnL1ZhbGlkYXRvci5yZXNldEZvcm0vfVxyXG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSBWYWxpZGF0b3IuZWxlbWVudCB7QGxpbmsgaHR0cHM6Ly9qcXVlcnl2YWxpZGF0aW9uLm9yZy9WYWxpZGF0b3IuZWxlbWVudC99XHJcbiAqL1xyXG5mdW5jdGlvbiB2YWxpZGF0ZUhhbmRsZXJzICgkZm9ybSwgVmFsaWRhdG9yKSB7XHJcblx0Ly8g0YHQsNCx0LzQuNGCINGE0L7RgNC80YssINCx0LvQvtC6INC+0YLQv9GA0LDQstC60Lgg0L/RgNC4INC+0YjQuNCx0LrQtSDRgdC60YDQuNC/0YLQvtCyXHJcblx0Y29uc3QgbWV0aG9kID0gJGZvcm0uYXR0cignbWV0aG9kJykgfHwgJ3Bvc3QnO1xyXG5cdCRmb3JtLnByb3AoJ21ldGhvZCcsIG1ldGhvZCk7XHJcblx0JGZvcm0ub24oJ3N1Ym1pdCcsIGV2ZW50ID0+IHtcclxuXHRcdGNvbnN0IGFjdGlvbiA9ICRmb3JtLmF0dHIoJ2FjdGlvbicpO1xyXG5cdFx0aWYgKCFhY3Rpb24pIHtcclxuXHRcdFx0ZXZlbnQucHJldmVudERlZmF1bHQoKTtcclxuXHRcdH1cclxuXHR9KTtcclxuXHJcblx0Ly8g0YHQsdGA0L7RgSDRhNC+0YDQvNGLXHJcblx0JGZvcm0ub24oJ3Jlc2V0JywgKCkgPT4gVmFsaWRhdG9yLnJlc2V0Rm9ybSgpKTtcclxuXHJcblx0Ly8g0L/RgNC+0LLQtdGA0LrQsCDRhNCw0LnQu9C+0LIsINC/0YDQuCDRgdC80LXQvdC1XHJcblx0JGZvcm0ub24oJ2NoYW5nZScsICdpbnB1dFt0eXBlPVwiZmlsZVwiXScsIGZ1bmN0aW9uICgpIHtcclxuXHRcdFZhbGlkYXRvci5lbGVtZW50KHRoaXMpO1xyXG5cdH0pO1xyXG5cclxuXHQvLyDQtNCw0LvRjNGI0LUg0LLQsNGIINC60L7QtFxyXG59XHJcblxyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbi8vIEV4cG9ydHNcclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgdmFsaWRhdGVIYW5kbGVycztcclxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vX0hUTUwvc3JjL2pzL19tb2R1bGVzL2pxdWVyeS12YWxpZGF0aW9uL3ZhbGlkYXRlLWhhbmRsZXJzLmpzIiwiJ3VzZSBzdHJpY3QnO1xyXG5cclxuLyoqXHJcbiAqINCe0LHRgNCw0LHQvtGC0LrQsCDQvtGC0LLQtdGC0LAg0L7RgiDRgdC10YDQstC10YDQsFxyXG4gKiBAbW9kdWxlXHJcbiAqL1xyXG5cclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4vLyBJbXBvcnRzXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbmltcG9ydCBQcmVsb2FkZXIgZnJvbSAnIy9fbW9kdWxlcy9QcmVsb2FkZXInO1xyXG5pbXBvcnQgbW9kdWxlTG9hZGVyIGZyb20gJyMvbW9kdWxlLWxvYWRlcic7XHJcbmltcG9ydCAnIy9fbW9kdWxlcy9tYWduaWZpYy1wb3B1cC9tZnAtaW5pdCc7XHJcblxyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbi8vIFByaXZhdGVcclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuLyoqXHJcbiAqINCS0YvQstC+0LQg0YPQstC10LTQvtC80LvQtdC90LjQuVxyXG4gKiBAcGFyYW0ge0pRdWVyeX0gJGZvcm1cclxuICogQHBhcmFtIHtzdHJpbmd9IG1lc3NhZ2VcclxuICogQHBhcmFtIHtib29sZWFufSBbc3VjY2Vzc11cclxuICogQHByaXZhdGVcclxuICovXHJcblxyXG5sZXQgbWFnbmlmaWNQb3B1cCA9ICQubWFnbmlmaWNQb3B1cC5pbnN0YW5jZTtcclxubGV0IHVybCA9ICcvVmlld3MvV2lkZ2V0cy9Qb3B1cC9UaGFuay5waHAnO1xyXG5cclxuZnVuY3Rpb24gc2hvd01lc3NhZ2UgKCRmb3JtLCBtZXNzYWdlLCBzdWNjZXNzKSB7XHJcblx0aWYgKG1hZ25pZmljUG9wdXAuaXNPcGVuKSB7XHJcblx0XHRsZXQgJHBvcHVwID0gJGZvcm0uY2xvc2VzdCgnLnBvcHVwJyk7XHJcblxyXG5cdFx0aWYgKCQoJy5sb2dpbi1wb3B1cCcpLmxlbmd0aCkge1xyXG5cdFx0XHQkcG9wdXAuZmluZCgnLmdyaWQnKS5oaWRlKCk7XHJcblx0XHRcdCRwb3B1cC5hcHBlbmQoYDxkaXYgY2xhc3M9XCJncmlkIHBvcHVwLW5vdHlcIj48ZGl2IGNsYXNzPVwiZ2NlbGwtLTI0XCI+PHAgY2xhc3M9J3RoYW5rLXBvcHVwX190ZXh0IHRoYW5rLXBvcHVwX190ZXh0LS1pbm5lcic+YCArIG1lc3NhZ2UgKyBgPC9wPjwvZGl2PjxidXR0b24gY2xhc3M9XCJ0aGlzLWNsb3NlXCIgaWQ9XCJjdXN0b20tY2xvc2UtYnRuXCIgc3R5bGU9XCJjb2xvcjpyZWQ7IGZvbnQtc2l6ZTogMTVweFwiPtCS0LXRgNC90YPRgtGM0YHRjyDQvdCw0LfQsNC0PC9idXR0b24+PC9kaXY+YCk7XHJcblxyXG5cdFx0XHQkKCcudGhpcy1jbG9zZScpLm9uKCdjbGljaycsIGZ1bmN0aW9uICgpe1xyXG5cdFx0XHRcdCRwb3B1cC5maW5kKCcuZ3JpZCcpLnNob3coKTtcclxuXHRcdFx0XHQkKCcucG9wdXAtbm90eScpLnJlbW92ZSgpO1xyXG5cdFx0XHR9KVxyXG5cdFx0fSBlbHNlIHtcclxuXHRcdFx0JHBvcHVwLmh0bWwoYDxwIGNsYXNzPSd0aGFuay1wb3B1cF9fdGV4dCB0aGFuay1wb3B1cF9fdGV4dC0taW5uZXInPmAgKyBtZXNzYWdlICsgYDwvcD48YnV0dG9uIGNsYXNzPSdtZnAtY2xvc2UnIGlkPSdjdXN0b20tY2xvc2UtYnRuJz48L2J1dHRvbj5gKTtcclxuXHRcdH1cclxuXHJcblx0XHRyZXR1cm4gZmFsc2U7XHJcblx0fVxyXG5cclxuXHQkLm1hZ25pZmljUG9wdXAub3Blbih7XHJcblx0XHRpdGVtczoge1xyXG5cdFx0XHRzcmM6IHVybFxyXG5cdFx0fSxcclxuXHRcdHR5cGU6ICdhamF4JyxcclxuXHRcdHJlbW92YWxEZWxheTogMzAwLFxyXG5cdFx0bWFpbkNsYXNzOiAnem9vbS1pbicsXHJcblx0XHRjbG9zZU1hcmt1cDogYDxidXR0b24gY2xhc3M9J21mcC1jbG9zZScgaWQ9J2N1c3RvbS1jbG9zZS1idG4nPjwvYnV0dG9uPmAsXHJcblx0XHRjYWxsYmFja3M6IHtcclxuXHRcdFx0YWpheENvbnRlbnRBZGRlZDogZnVuY3Rpb24gKCkge1xyXG5cdFx0XHRcdCQoJy50aGFuay1wb3B1cF9fdGV4dCcpLmh0bWwobWVzc2FnZSk7XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHR9KTtcclxufVxyXG5cclxuZnVuY3Rpb24gc2hvd1dpZGdldE1lc3NhZ2UgKCRmb3JtLCBtZXNzYWdlLCB3aWRnZXQpIHtcclxuXHRpZiAobWFnbmlmaWNQb3B1cC5pc09wZW4pIHtcclxuXHRcdGxldCAkcG9wdXAgPSAkZm9ybS5jbG9zZXN0KCcucG9wdXAnKTtcclxuXHJcblx0XHQkcG9wdXAuaHRtbChgPHAgY2xhc3M9J3RoYW5rLXBvcHVwX190ZXh0IHRoYW5rLXBvcHVwX190ZXh0LS1pbm5lcic+YCArIG1lc3NhZ2UgKyBgPC9wPjxidXR0b24gY2xhc3M9J21mcC1jbG9zZScgaWQ9J2N1c3RvbS1jbG9zZS1idG4nPjwvYnV0dG9uPmArd2lkZ2V0KTtcclxuXHJcblx0XHRyZXR1cm4gZmFsc2U7XHJcblx0fVxyXG5cclxuXHQkLm1hZ25pZmljUG9wdXAub3Blbih7XHJcblx0XHRpdGVtczoge1xyXG5cdFx0XHRzcmM6IHVybFxyXG5cdFx0fSxcclxuXHRcdHR5cGU6ICdhamF4JyxcclxuXHRcdHJlbW92YWxEZWxheTogMzAwLFxyXG5cdFx0bWFpbkNsYXNzOiAnem9vbS1pbicsXHJcblx0XHRjbG9zZU1hcmt1cDogYDxidXR0b24gY2xhc3M9J21mcC1jbG9zZScgaWQ9J2N1c3RvbS1jbG9zZS1idG4nPjwvYnV0dG9uPmAsXHJcblx0XHRjYWxsYmFja3M6IHtcclxuXHRcdFx0YWpheENvbnRlbnRBZGRlZDogZnVuY3Rpb24gKCkge1xyXG5cdFx0XHRcdCQoJy50aGFuay1wb3B1cF9fdGV4dCcpLmh0bWwobWVzc2FnZSk7XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHR9KTtcclxufVxyXG5cclxuZnVuY3Rpb24gY2xvc2VQb3B1cCAoKSB7XHJcblx0aWYgKG1hZ25pZmljUG9wdXAuaXNPcGVuKSB7XHJcblx0XHQkLm1hZ25pZmljUG9wdXAuY2xvc2UoKTtcclxuXHR9XHJcbn1cclxuXHJcbmZ1bmN0aW9uIHN1bSAoYXJyKSB7XHJcblx0Y29uc3QgcmVkdWNlciA9IChhY2N1bXVsYXRvciwgY3VycmVudFZhbHVlKSA9PiBwYXJzZUludChhY2N1bXVsYXRvcikgKyBwYXJzZUludChjdXJyZW50VmFsdWUpO1xyXG5cclxuXHRyZXR1cm4gYXJyLnJlZHVjZShyZWR1Y2VyKTtcclxufVxyXG5cclxuLy8gVGhvdXNhbmQgc2VwYXJhdG9yXHJcblxyXG5mdW5jdGlvbiBzZXRUaG91c2FuZHMgKG51bWJlclRleHQsIG5ld1NlcGFyYXRvciwgc2VwYXJhdG9yKSB7XHJcblx0bmV3U2VwYXJhdG9yID0gbmV3U2VwYXJhdG9yIHx8ICcuJztcclxuXHRzZXBhcmF0b3IgPSBzZXBhcmF0b3IgfHwgJy4nO1xyXG5cdG51bWJlclRleHQgPSAnJyArIG51bWJlclRleHQ7XHJcblx0bnVtYmVyVGV4dCA9IG51bWJlclRleHQuc3BsaXQoc2VwYXJhdG9yKTtcclxuXHJcblx0bGV0IG51bWJlclBlbm55ID0gbnVtYmVyVGV4dFsxXSB8fCAnJztcclxuXHRsZXQgbnVtYmVyVmFsdWUgPSBudW1iZXJUZXh0WzBdO1xyXG5cclxuXHRsZXQgdGhvdXNhbmRzVmFsdWUgPSBbXTtcclxuXHRsZXQgY291bnRlciA9IDA7XHJcblxyXG5cdGZvciAobGV0IGkgPSBudW1iZXJWYWx1ZS5sZW5ndGggLSAxOyBpID49IDA7IGktLSkge1xyXG5cdFx0bGV0IG51bSA9IG51bWJlclZhbHVlW2ldO1xyXG5cdFx0dGhvdXNhbmRzVmFsdWUucHVzaChudW0pO1xyXG5cclxuXHRcdGlmICgrK2NvdW50ZXIgPT09IDMgJiYgaSkge1xyXG5cdFx0XHR0aG91c2FuZHNWYWx1ZS5wdXNoKCcgJyk7XHJcblx0XHRcdGNvdW50ZXIgPSAwO1xyXG5cdFx0fVxyXG5cdH1cclxuXHJcblx0dGhvdXNhbmRzVmFsdWUgPSB0aG91c2FuZHNWYWx1ZS5yZXZlcnNlKCkuam9pbignJyk7XHJcblx0aWYgKG51bWJlclBlbm55Lmxlbmd0aCkge1xyXG5cdFx0cmV0dXJuIFt0aG91c2FuZHNWYWx1ZSwgbnVtYmVyUGVubnldLmpvaW4obmV3U2VwYXJhdG9yKTtcclxuXHR9XHJcblx0cmV0dXJuIHRob3VzYW5kc1ZhbHVlO1xyXG59XHJcblxyXG5mdW5jdGlvbiB1cGRhdGVTdGF0aWNDYXJ0IChyZXNwb25zZSkge1xyXG5cdGlmICgkKCcuanMtY2FydC1zdGF0aWMnKS5sZW5ndGggJiYgdHlwZW9mIHJlc3BvbnNlLnN0YXRpYyAhPT0gJ3VuZGVmaW5lZCcpIHtcclxuXHRcdCQoJy5qcy1jYXJ0LXN0YXRpYycpLmh0bWwocmVzcG9uc2Uuc3RhdGljKTtcclxuXHJcblx0XHRpZiAoIXJlc3BvbnNlLmNvdW50KSB7XHJcblx0XHRcdCQoJ1tkYXRhLXRvdGFsXScpLmFkZENsYXNzKCdfaGlkZScpO1xyXG5cdFx0XHQkKCdbZGF0YS10b3RhbC1lbXB0eV0nKS5yZW1vdmVDbGFzcygnX2hpZGUnKTtcclxuXHRcdH1cclxuXHJcblx0XHRpZiAoJCgnLmpzLXRvdGFsLXByaWNlJykubGVuZ3RoICYmIHR5cGVvZiByZXNwb25zZS50b3RhbFByaWNlICE9PSAndW5kZWZpbmVkJykge1xyXG5cdFx0XHQkKCcuanMtdG90YWwtcHJpY2UnKS50ZXh0KHJlc3BvbnNlLnRvdGFsUHJpY2UudG9GaXhlZCgyKSk7XHJcblx0XHR9XHJcblxyXG5cdFx0bW9kdWxlTG9hZGVyLmluaXQoJCgnLmpzLWNhcnQtc3RhdGljJykpO1xyXG5cdH1cclxuXHJcblx0aWYgKCQoJy5qcy1jYXJ0JykubGVuZ3RoICYmIHR5cGVvZiByZXNwb25zZS5odG1sICE9PSAndW5kZWZpbmVkJykge1xyXG5cdFx0bGV0ICRodG1sID0gJChyZXNwb25zZS5odG1sKTtcclxuXHRcdCQoJy5qcy1jYXJ0JykuaHRtbCgkaHRtbCk7XHJcblxyXG5cdFx0bW9kdWxlTG9hZGVyLmluaXQoJCgnLmpzLWNhcnQnKSk7XHJcblx0fVxyXG5cclxuXHRpZiAoJCgnLmpzLWNhcnQtY291bnQnKS5sZW5ndGggJiYgdHlwZW9mIHJlc3BvbnNlLmNvdW50ICE9PSAndW5kZWZpbmVkJykge1xyXG5cdFx0JCgnLmpzLWNhcnQtY291bnQnKS5odG1sKHJlc3BvbnNlLmNvdW50KTtcclxuXHR9XHJcbn1cclxuXHJcbmZ1bmN0aW9uIGNoYW5nZVNpemVzICgkZm9ybSwgc2l6ZXMsIGNvdW50cykge1xyXG5cdGxldCAkd3JhcCA9ICQoJy5qcy1wcm9kdWN0LWNvdW50cycpO1xyXG5cdGxldCAkaXRlbXMgPSAkd3JhcC5maW5kKCcuanMtcHJvZHVjdC1jb3VudC1pdGVtJyk7XHJcblx0bGV0ICRjYXJ0SXRlbSA9ICR3cmFwLmNsb3Nlc3QoJy5qcy1jYXJ0LWl0ZW0nKTtcclxuXHJcblx0bGV0ICRjb3VudGVyID0gJGNhcnRJdGVtLmZpbmQoJy5qcy1wcm9kdWN0LWNvdW50LWFsbCcpO1xyXG5cdGxldCAkY291bnRlcklucHV0ID0gJGNhcnRJdGVtLmZpbmQoJy5qcy1wcm9kdWN0LWlucHV0Jyk7XHJcblx0bGV0ICRwcmljZSA9ICRjYXJ0SXRlbS5maW5kKCcuanMtcHJvZHVjdC1wcmljZScpO1xyXG5cclxuXHRsZXQgY291bnQgPSBzdW0oY291bnRzKTtcclxuXHRsZXQgcHJpY2UgPSAkcHJpY2UuZGF0YSgncHJpY2UnKSAqIGNvdW50O1xyXG5cclxuXHQkY2FydEl0ZW0uZGF0YSgnc2l6ZXMnLCBzaXplcyk7XHJcblx0JGNhcnRJdGVtLmRhdGEoJ2NvdW50cycsIGNvdW50cyk7XHJcblx0JGNhcnRJdGVtLmRhdGEoJ2NvdW50JywgY291bnQpO1xyXG5cdCRjb3VudGVyLnRleHQoY291bnQpO1xyXG5cdCRjb3VudGVySW5wdXQudmFsKGNvdW50KTtcclxuXHQkcHJpY2UudGV4dChzZXRUaG91c2FuZHMocHJpY2UudG9GaXhlZCgyKSkpO1xyXG5cclxuXHQkaXRlbXMucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpO1xyXG5cclxuXHQkaXRlbXMuZWFjaChmdW5jdGlvbiAoaSwgZWwpIHtcclxuXHRcdGxldCAkaXRlbSA9ICQodGhpcyk7XHJcblx0XHRsZXQgJGNvdW50ID0gJCh0aGlzKS5maW5kKCcuanMtcHJvZHVjdC1jb3VudCcpO1xyXG5cclxuXHRcdGxldCBwYXJhbSA9ICRpdGVtLmRhdGEoJ3BhcmFtJyk7XHJcblx0XHRwYXJhbS5wYXJhbS5jb3VudHMgPSBjb3VudHM7XHJcblxyXG5cdFx0JGl0ZW0uYXR0cignZGF0YS1wYXJhbScsIEpTT04uc3RyaW5naWZ5KHBhcmFtKSk7XHJcblx0XHQkY291bnQudGV4dCgnJyk7XHJcblxyXG5cdFx0c2l6ZXMuZm9yRWFjaCgoaXRlbSwgaSkgPT4ge1xyXG5cdFx0XHRpZiAoJGl0ZW0uZGF0YSgnc2l6ZScpID09PSBzaXplc1tpXSkge1xyXG5cdFx0XHRcdGlmIChjb3VudHNbaV0gPiAwKSB7XHJcblx0XHRcdFx0XHQkY291bnQuaHRtbChjb3VudHNbaV0pO1xyXG5cdFx0XHRcdFx0JGl0ZW0uYWRkQ2xhc3MoJ2FjdGl2ZScpO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cdFx0fSk7XHJcblx0fSk7XHJcbn1cclxuXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuLy8gUHVibGljXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbi8qKlxyXG4gKiBAcGFyYW0ge0pRdWVyeX0gJGZvcm1cclxuICogQHBhcmFtIHtPYmplY3R9IHJlc3BvbnNlXHJcbiAqIEBwYXJhbSB7c3RyaW5nfSBzdGF0dXNUZXh0XHJcbiAqIEBwYXJhbSB7T2JqZWN0fSB4aHJcclxuICogQHBhcmFtIHtzdHJpbmd9IHJlc3BvbnNlLnJlc3BvbnNlIC0g0YLQtdC60YHRgtC+0LLQvtC1INGB0L7QvtCx0YnQtdC90LjQtVxyXG4gKiBAcGFyYW0ge2Jvb2xlYW59IHJlc3BvbnNlLnN1Y2Nlc3MgLSDRg9GB0L/QtdGI0L3Ri9C5INC30LDQv9GA0L7RgVxyXG4gKiBAcGFyYW0ge3N0cmluZ30gW3Jlc3BvbnNlLnJlZGlyZWN0XSAtINGD0YDQuyDQtNC70Y8g0YDQtdC00LjRgNC10LrRgtCwLCDQtdGB0LvQuCDRgNCw0LLQtdC9INGC0LXQutGD0YnQtdC80YMg0YPRgNC70YMgLSDQv9C10YDQtdCz0YDQsNC20LDQtdC8INGB0YLRgNCw0L3QuNGG0YNcclxuICogQHBhcmFtIHtib29sZWFufSBbcmVzcG9uc2UucmVsb2FkXSAtINC/0LXRgNC10LPRgNGD0LfQuNGC0Ywg0YHRgtGA0LDQvdC40YbRg1xyXG4gKiBAcGFyYW0ge2Jvb2xlYW59IFtyZXNwb25zZS5yZXNldF0gLSDRgdCx0YDQvtGB0LjRgtGMINGE0L7RgNC80YNcclxuICogQHBhcmFtIHtBcnJheX0gW3Jlc3BvbnNlLmNsZWFyXSAtINGB0LHRgNC+0YHQuNGC0Ywg0YTQvtGA0LzRg1xyXG4gKiBAcHJpdmF0ZVxyXG4gKi9cclxuZnVuY3Rpb24gdmFsaWRhdGVHZXRSZXNwb25zZSAoJGZvcm0sIHJlc3BvbnNlLCBzdGF0dXNUZXh0LCB4aHIpIHtcclxuXHRjb25zdCBwcmVsb2FkZXIgPSAkZm9ybS5kYXRhKCdwcmVsb2FkZXInKTtcclxuXHRpZiAocHJlbG9hZGVyIGluc3RhbmNlb2YgUHJlbG9hZGVyKSB7XHJcblx0XHRwcmVsb2FkZXIuaGlkZSgpO1xyXG5cdH1cclxuXHJcblx0Ly8g0L7QsdGA0LDQsdCw0YLRi9Cy0LDQtdC8INC+0YLQstC10YJcclxuXHQvLyAtLS0tLS0tLS0tLS0tLS0tLS1cclxuXHRpZiAodHlwZW9mIHJlc3BvbnNlID09PSAnc3RyaW5nJykge1xyXG5cdFx0cmVzcG9uc2UgPSBKU09OLnBhcnNlKHJlc3BvbnNlKTtcclxuXHR9XHJcblxyXG5cdGlmIChyZXNwb25zZS5yZWxvYWQgfHwgd2luZG93LmxvY2F0aW9uLmhyZWYgPT09IHJlc3BvbnNlLnJlZGlyZWN0KSB7XHJcblx0XHRyZXR1cm4gd2luZG93LmxvY2F0aW9uLnJlbG9hZCgpO1xyXG5cdH1cclxuXHJcblx0aWYgKHJlc3BvbnNlLnJlZGlyZWN0KSB7XHJcblx0XHRyZXR1cm4gKHdpbmRvdy5sb2NhdGlvbi5ocmVmID0gcmVzcG9uc2UucmVkaXJlY3QpO1xyXG5cdH1cclxuXHJcblx0aWYgKHJlc3BvbnNlLnN1Y2Nlc3MpIHtcclxuXHRcdGlmIChyZXNwb25zZS5jbGVhcikge1xyXG5cdFx0XHRpZiAoQXJyYXkuaXNBcnJheShyZXNwb25zZS5jbGVhcikpIHtcclxuXHRcdFx0XHRyZXNwb25zZS5jbGVhci5mb3JFYWNoKGNsZWFyU2VsZWN0b3IgPT4ge1xyXG5cdFx0XHRcdFx0JGZvcm0uZmluZChjbGVhclNlbGVjdG9yKS52YWwoJycpO1xyXG5cdFx0XHRcdH0pO1xyXG5cdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdC8vINC40LPQvdC+0YDQuNGA0YPQtdC80YvQtSDRgtC40L/RiyDQuNC90L/Rg9GC0L7QslxyXG5cdFx0XHRcdGxldCBpZ25vcmVkSW5wdXRzVHlwZSA9IFtcclxuXHRcdFx0XHRcdCdzdWJtaXQnLFxyXG5cdFx0XHRcdFx0J3Jlc2V0JyxcclxuXHRcdFx0XHRcdCdidXR0b24nLFxyXG5cdFx0XHRcdFx0J2ltYWdlJ1xyXG5cdFx0XHRcdF07XHJcblx0XHRcdFx0JGZvcm0uZmluZCgnaW5wdXQsIHRleHRhcmVhLCBzZWxlY3QnKS5lYWNoKChpLCBlbGVtZW50KSA9PiB7XHJcblx0XHRcdFx0XHRpZiAofmlnbm9yZWRJbnB1dHNUeXBlLmluZGV4T2YoZWxlbWVudC50eXBlKSkge1xyXG5cdFx0XHRcdFx0XHRyZXR1cm4gdHJ1ZTtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdGVsZW1lbnQudmFsdWUgPSAnJztcclxuXHRcdFx0XHR9KTtcclxuXHRcdFx0fVxyXG5cdFx0fSBlbHNlIGlmIChyZXNwb25zZS5yZXNldCkge1xyXG5cdFx0XHQkZm9ybS50cmlnZ2VyKCdyZXNldCcpO1xyXG5cdFx0fVxyXG5cdH1cclxuXHRpZiAocmVzcG9uc2Uuc2l6ZXMpIHtcclxuXHRcdGNoYW5nZVNpemVzKCRmb3JtLCByZXNwb25zZS5zaXplcywgcmVzcG9uc2UuY291bnRzKTtcclxuXHR9XHJcblx0aWYgKHJlc3BvbnNlLmNhcnQpIHtcclxuXHRcdHVwZGF0ZVN0YXRpY0NhcnQocmVzcG9uc2UpO1xyXG5cdH1cclxuXHRpZiAocmVzcG9uc2UucmVzcG9uc2UpIHtcclxuXHRcdGlmIChyZXNwb25zZS53aWRnZXQpIHtcclxuXHRcdFx0c2hvd1dpZGdldE1lc3NhZ2UoJGZvcm0sIHJlc3BvbnNlLm1lc3NhZ2UscmVzcG9uc2UucmVzcG9uc2UpO1xyXG5cdFx0fWVsc2V7XHJcblx0XHRcdHNob3dNZXNzYWdlKCRmb3JtLCByZXNwb25zZS5yZXNwb25zZSwgcmVzcG9uc2Uuc3VjY2Vzcyk7XHJcblx0XHR9XHJcblx0fVxyXG5cdGlmIChyZXNwb25zZS5jbG9zZSkge1xyXG5cdFx0Y2xvc2VQb3B1cCgpO1xyXG5cdH1cclxuXHRpZiAocmVzcG9uc2UuZm9ybSkge1xyXG5cdFx0bGV0ICRodG1sID0gJCgnPGRpdiBjbGFzcz1cIl9oaWRlXCI+JyArIHJlc3BvbnNlLmZvcm0gICsgJzwvZGl2PicpO1xyXG5cdFx0JGZvcm0uYXBwZW5kKCRodG1sKTtcclxuXHRcdCRodG1sLmZpbmQoJ2Zvcm0nKS5nZXQoMCkuc3VibWl0KCk7XHJcblx0fVxyXG59XHJcblxyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbi8vIEV4cG9ydHNcclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgdmFsaWRhdGVHZXRSZXNwb25zZTtcclxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vX0hUTUwvc3JjL2pzL19tb2R1bGVzL2pxdWVyeS12YWxpZGF0aW9uL3ZhbGlkYXRlLWdldC1yZXNwb25zZS5qcyIsIid1c2Ugc3RyaWN0JztcclxuXHJcbi8qKlxyXG4gKiDQoNCw0YHRiNC40YDQtdC90LjQtSDQtNC10YTQvtC70YLQvdGL0YUg0L/QsNGA0LDQvNC10YLRgNC+0LIg0L/Qu9Cw0LPQuNC90LBcclxuICogQG1vZHVsZVxyXG4gKi9cclxuXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuLy8gUHVibGljXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbihmdW5jdGlvbiAoJCwganNUcmFuc2xhdGlvbnMgPSB7fSkge1xyXG5cdGxldCBtZnAgPSBqc1RyYW5zbGF0aW9uc1snbWFnbmlmaWMtcG9wdXAnXSB8fCB7fTtcclxuXHRpZiAoT2JqZWN0LmtleXMobWZwKS5sZW5ndGggPT09IDApIHtcclxuXHRcdHJldHVybiBjb25zb2xlLndhcm4oJ9Cf0LXRgNC10LLQvtC00Ysg0LTQu9GPIG1hZ25pZmljUG9wdXAgLSDQvtGC0YHRg9GC0YHRgtCy0YPRjtGCIScpO1xyXG5cdH1cclxuXHJcblx0JC5leHRlbmQodHJ1ZSwgJC5tYWduaWZpY1BvcHVwLmRlZmF1bHRzLCB7XHJcblx0XHR0Q2xvc2U6IG1mcC50Q2xvc2UsXHJcblx0XHR0TG9hZGluZzogbWZwLnRMb2FkaW5nLFxyXG5cdFx0Z2FsbGVyeToge1xyXG5cdFx0XHR0UHJldjogbWZwLnRQcmV2LFxyXG5cdFx0XHR0TmV4dDogbWZwLnROZXh0LFxyXG5cdFx0XHR0Q291bnRlcjogbWZwLnRDb3VudGVyXHJcblx0XHR9LFxyXG5cdFx0aW1hZ2U6IHtcclxuXHRcdFx0dEVycm9yOiBtZnAudEVycm9ySW1hZ2VcclxuXHRcdH0sXHJcblx0XHRhamF4OiB7XHJcblx0XHRcdHRFcnJvcjogbWZwLnRFcnJvclxyXG5cdFx0fSxcclxuXHRcdGlubGluZToge1xyXG5cdFx0XHR0Tm90Rm91bmQ6IG1mcC50Tm90Rm91bmRcclxuXHRcdH1cclxuXHR9KTtcclxufSkod2luZG93LmpRdWVyeSwgd2luZG93LmpzVHJhbnNsYXRpb25zKTtcclxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vX0hUTUwvc3JjL2pzL19tb2R1bGVzL21hZ25pZmljLXBvcHVwL21mcC1leHRlbmQuanMiLCIvLyBzdHlsZS1sb2FkZXI6IEFkZHMgc29tZSBjc3MgdG8gdGhlIERPTSBieSBhZGRpbmcgYSA8c3R5bGU+IHRhZ1xuXG4vLyBsb2FkIHRoZSBzdHlsZXNcbnZhciBjb250ZW50ID0gcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tMi0xIS4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tMi0yIS4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9zYXNzLWxvYWRlci9saWIvbG9hZGVyLmpzPz9yZWYtLTItMyEuL21mcC1leHRlbmQuc2Nzc1wiKTtcbmlmKHR5cGVvZiBjb250ZW50ID09PSAnc3RyaW5nJykgY29udGVudCA9IFtbbW9kdWxlLmlkLCBjb250ZW50LCAnJ11dO1xuLy8gUHJlcGFyZSBjc3NUcmFuc2Zvcm1hdGlvblxudmFyIHRyYW5zZm9ybTtcblxudmFyIG9wdGlvbnMgPSB7XCJpbnNlcnRBdFwiOntcImJlZm9yZVwiOlwiI3dlYnBhY2stc3R5bGUtbG9hZGVyLWluc2VydC1iZWZvcmUtdGhpc1wifSxcImhtclwiOnRydWV9XG5vcHRpb25zLnRyYW5zZm9ybSA9IHRyYW5zZm9ybVxuLy8gYWRkIHRoZSBzdHlsZXMgdG8gdGhlIERPTVxudmFyIHVwZGF0ZSA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3N0eWxlLWxvYWRlci9saWIvYWRkU3R5bGVzLmpzXCIpKGNvbnRlbnQsIG9wdGlvbnMpO1xuaWYoY29udGVudC5sb2NhbHMpIG1vZHVsZS5leHBvcnRzID0gY29udGVudC5sb2NhbHM7XG4vLyBIb3QgTW9kdWxlIFJlcGxhY2VtZW50XG5pZihtb2R1bGUuaG90KSB7XG5cdC8vIFdoZW4gdGhlIHN0eWxlcyBjaGFuZ2UsIHVwZGF0ZSB0aGUgPHN0eWxlPiB0YWdzXG5cdGlmKCFjb250ZW50LmxvY2Fscykge1xuXHRcdG1vZHVsZS5ob3QuYWNjZXB0KFwiISEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS0yLTEhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS0yLTIhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Nhc3MtbG9hZGVyL2xpYi9sb2FkZXIuanM/P3JlZi0tMi0zIS4vbWZwLWV4dGVuZC5zY3NzXCIsIGZ1bmN0aW9uKCkge1xuXHRcdFx0dmFyIG5ld0NvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS0yLTEhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS0yLTIhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Nhc3MtbG9hZGVyL2xpYi9sb2FkZXIuanM/P3JlZi0tMi0zIS4vbWZwLWV4dGVuZC5zY3NzXCIpO1xuXHRcdFx0aWYodHlwZW9mIG5ld0NvbnRlbnQgPT09ICdzdHJpbmcnKSBuZXdDb250ZW50ID0gW1ttb2R1bGUuaWQsIG5ld0NvbnRlbnQsICcnXV07XG5cdFx0XHR1cGRhdGUobmV3Q29udGVudCk7XG5cdFx0fSk7XG5cdH1cblx0Ly8gV2hlbiB0aGUgbW9kdWxlIGlzIGRpc3Bvc2VkLCByZW1vdmUgdGhlIDxzdHlsZT4gdGFnc1xuXHRtb2R1bGUuaG90LmRpc3Bvc2UoZnVuY3Rpb24oKSB7IHVwZGF0ZSgpOyB9KTtcbn1cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL19IVE1ML3NyYy9qcy9fbW9kdWxlcy9tYWduaWZpYy1wb3B1cC9tZnAtZXh0ZW5kLnNjc3Ncbi8vIG1vZHVsZSBpZCA9IDQ1XG4vLyBtb2R1bGUgY2h1bmtzID0gOSIsImV4cG9ydHMgPSBtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCIuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9saWIvY3NzLWJhc2UuanNcIikoZmFsc2UpO1xuLy8gaW1wb3J0c1xuXG5cbi8vIG1vZHVsZVxuZXhwb3J0cy5wdXNoKFttb2R1bGUuaWQsIFwiLyogTWFnbmlmaWMgUG9wdXAgQ1NTICovXFxuLm1mcC1iZyB7XFxuICB0b3A6IDA7XFxuICBsZWZ0OiAwO1xcbiAgd2lkdGg6IDEwMCU7XFxuICBoZWlnaHQ6IDEwMCU7XFxuICB6LWluZGV4OiAxMDQyO1xcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcXG4gIHBvc2l0aW9uOiBmaXhlZDtcXG4gIGJhY2tncm91bmQ6ICMwYjBiMGI7XFxuICBvcGFjaXR5OiAwLjg7IH1cXG5cXG4ubWZwLXdyYXAge1xcbiAgdG9wOiAwO1xcbiAgbGVmdDogMDtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgaGVpZ2h0OiAxMDAlO1xcbiAgei1pbmRleDogMTA0MztcXG4gIHBvc2l0aW9uOiBmaXhlZDtcXG4gIG91dGxpbmU6IG5vbmUgIWltcG9ydGFudDtcXG4gIC13ZWJraXQtYmFja2ZhY2UtdmlzaWJpbGl0eTogaGlkZGVuOyB9XFxuXFxuLm1mcC1jb250YWluZXIge1xcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgd2lkdGg6IDEwMCU7XFxuICBoZWlnaHQ6IDEwMCU7XFxuICBsZWZ0OiAwO1xcbiAgdG9wOiAwO1xcbiAgcGFkZGluZzogMCA4cHg7XFxuICBib3gtc2l6aW5nOiBib3JkZXItYm94OyB9XFxuXFxuLm1mcC1jb250YWluZXI6YmVmb3JlIHtcXG4gIGNvbnRlbnQ6ICcnO1xcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xcbiAgaGVpZ2h0OiAxMDAlO1xcbiAgdmVydGljYWwtYWxpZ246IG1pZGRsZTsgfVxcblxcbi5tZnAtYWxpZ24tdG9wIC5tZnAtY29udGFpbmVyOmJlZm9yZSB7XFxuICBkaXNwbGF5OiBub25lOyB9XFxuXFxuLm1mcC1jb250ZW50IHtcXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcXG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcXG4gIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XFxuICBtYXJnaW46IDAgYXV0bztcXG4gIHRleHQtYWxpZ246IGxlZnQ7XFxuICB6LWluZGV4OiAxMDQ1OyB9XFxuXFxuLm1mcC1pbmxpbmUtaG9sZGVyIC5tZnAtY29udGVudCxcXG4ubWZwLWFqYXgtaG9sZGVyIC5tZnAtY29udGVudCB7XFxuICB3aWR0aDogMTAwJTtcXG4gIGN1cnNvcjogYXV0bzsgfVxcblxcbi5tZnAtYWpheC1jdXIge1xcbiAgY3Vyc29yOiBwcm9ncmVzczsgfVxcblxcbi5tZnAtem9vbS1vdXQtY3VyLCAubWZwLXpvb20tb3V0LWN1ciAubWZwLWltYWdlLWhvbGRlciAubWZwLWNsb3NlIHtcXG4gIGN1cnNvcjogem9vbS1vdXQ7IH1cXG5cXG4ubWZwLXpvb20ge1xcbiAgY3Vyc29yOiBwb2ludGVyO1xcbiAgY3Vyc29yOiB6b29tLWluOyB9XFxuXFxuLm1mcC1hdXRvLWN1cnNvciAubWZwLWNvbnRlbnQge1xcbiAgY3Vyc29yOiBhdXRvOyB9XFxuXFxuLm1mcC1jbG9zZSxcXG4ubWZwLWFycm93LFxcbi5tZnAtcHJlbG9hZGVyLFxcbi5tZnAtY291bnRlciB7XFxuICAtd2Via2l0LXVzZXItc2VsZWN0OiBub25lO1xcbiAgLW1vei11c2VyLXNlbGVjdDogbm9uZTtcXG4gIC1tcy11c2VyLXNlbGVjdDogbm9uZTtcXG4gIHVzZXItc2VsZWN0OiBub25lOyB9XFxuXFxuLm1mcC1sb2FkaW5nLm1mcC1maWd1cmUge1xcbiAgZGlzcGxheTogbm9uZTsgfVxcblxcbi5tZnAtaGlkZSB7XFxuICBkaXNwbGF5OiBub25lICFpbXBvcnRhbnQ7IH1cXG5cXG4ubWZwLXByZWxvYWRlciB7XFxuICBjb2xvcjogI0NDQztcXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gIHRvcDogNTAlO1xcbiAgd2lkdGg6IGF1dG87XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICBtYXJnaW4tdG9wOiAtMC44ZW07XFxuICBsZWZ0OiA4cHg7XFxuICByaWdodDogOHB4O1xcbiAgei1pbmRleDogMTA0NDsgfVxcbiAgLm1mcC1wcmVsb2FkZXIgYSB7XFxuICAgIGNvbG9yOiAjQ0NDOyB9XFxuICAgIC5tZnAtcHJlbG9hZGVyIGE6aG92ZXIge1xcbiAgICAgIGNvbG9yOiAjRkZGOyB9XFxuXFxuLm1mcC1zLXJlYWR5IC5tZnAtcHJlbG9hZGVyIHtcXG4gIGRpc3BsYXk6IG5vbmU7IH1cXG5cXG4ubWZwLXMtZXJyb3IgLm1mcC1jb250ZW50IHtcXG4gIGRpc3BsYXk6IG5vbmU7IH1cXG5cXG5idXR0b24ubWZwLWNsb3NlLCBidXR0b24ubWZwLWFycm93IHtcXG4gIG92ZXJmbG93OiB2aXNpYmxlO1xcbiAgY3Vyc29yOiBwb2ludGVyO1xcbiAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XFxuICBib3JkZXI6IDA7XFxuICAtd2Via2l0LWFwcGVhcmFuY2U6IG5vbmU7XFxuICBkaXNwbGF5OiBibG9jaztcXG4gIG91dGxpbmU6IG5vbmU7XFxuICBwYWRkaW5nOiAwO1xcbiAgei1pbmRleDogMTA0NjtcXG4gIGJveC1zaGFkb3c6IG5vbmU7XFxuICB0b3VjaC1hY3Rpb246IG1hbmlwdWxhdGlvbjsgfVxcblxcbmJ1dHRvbjo6LW1vei1mb2N1cy1pbm5lciB7XFxuICBwYWRkaW5nOiAwO1xcbiAgYm9yZGVyOiAwOyB9XFxuXFxuLm1mcC1jbG9zZSB7XFxuICB3aWR0aDogNDRweDtcXG4gIGhlaWdodDogNDRweDtcXG4gIGxpbmUtaGVpZ2h0OiA0NHB4O1xcbiAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgcmlnaHQ6IDA7XFxuICB0b3A6IDA7XFxuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICBvcGFjaXR5OiAwLjY1O1xcbiAgcGFkZGluZzogMCAwIDE4cHggMTBweDtcXG4gIGNvbG9yOiAjRkZGO1xcbiAgZm9udC1zdHlsZTogbm9ybWFsO1xcbiAgZm9udC1zaXplOiAyOHB4O1xcbiAgZm9udC1mYW1pbHk6IEFyaWFsLCBCYXNrZXJ2aWxsZSwgbW9ub3NwYWNlOyB9XFxuICAubWZwLWNsb3NlOmhvdmVyLCAubWZwLWNsb3NlOmZvY3VzIHtcXG4gICAgb3BhY2l0eTogMTsgfVxcbiAgLm1mcC1jbG9zZTphY3RpdmUge1xcbiAgICB0b3A6IDFweDsgfVxcblxcbi5tZnAtY2xvc2UtYnRuLWluIC5tZnAtY2xvc2Uge1xcbiAgY29sb3I6ICMzMzM7IH1cXG5cXG4ubWZwLWltYWdlLWhvbGRlciAubWZwLWNsb3NlLFxcbi5tZnAtaWZyYW1lLWhvbGRlciAubWZwLWNsb3NlIHtcXG4gIGNvbG9yOiAjRkZGO1xcbiAgcmlnaHQ6IC02cHg7XFxuICB0ZXh0LWFsaWduOiByaWdodDtcXG4gIHBhZGRpbmctcmlnaHQ6IDZweDtcXG4gIHdpZHRoOiAxMDAlOyB9XFxuXFxuLm1mcC1jb3VudGVyIHtcXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gIHRvcDogMDtcXG4gIHJpZ2h0OiAwO1xcbiAgY29sb3I6ICNDQ0M7XFxuICBmb250LXNpemU6IDEycHg7XFxuICBsaW5lLWhlaWdodDogMThweDtcXG4gIHdoaXRlLXNwYWNlOiBub3dyYXA7IH1cXG5cXG4ubWZwLWFycm93IHtcXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gIG9wYWNpdHk6IDAuNjU7XFxuICBtYXJnaW46IDA7XFxuICB0b3A6IDUwJTtcXG4gIG1hcmdpbi10b3A6IC01NXB4O1xcbiAgcGFkZGluZzogMDtcXG4gIHdpZHRoOiA5MHB4O1xcbiAgaGVpZ2h0OiAxMTBweDtcXG4gIC13ZWJraXQtdGFwLWhpZ2hsaWdodC1jb2xvcjogdHJhbnNwYXJlbnQ7IH1cXG4gIC5tZnAtYXJyb3c6YWN0aXZlIHtcXG4gICAgbWFyZ2luLXRvcDogLTU0cHg7IH1cXG4gIC5tZnAtYXJyb3c6aG92ZXIsIC5tZnAtYXJyb3c6Zm9jdXMge1xcbiAgICBvcGFjaXR5OiAxOyB9XFxuICAubWZwLWFycm93OmJlZm9yZSwgLm1mcC1hcnJvdzphZnRlciB7XFxuICAgIGNvbnRlbnQ6ICcnO1xcbiAgICBkaXNwbGF5OiBibG9jaztcXG4gICAgd2lkdGg6IDA7XFxuICAgIGhlaWdodDogMDtcXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgICBsZWZ0OiAwO1xcbiAgICB0b3A6IDA7XFxuICAgIG1hcmdpbi10b3A6IDM1cHg7XFxuICAgIG1hcmdpbi1sZWZ0OiAzNXB4O1xcbiAgICBib3JkZXI6IG1lZGl1bSBpbnNldCB0cmFuc3BhcmVudDsgfVxcbiAgLm1mcC1hcnJvdzphZnRlciB7XFxuICAgIGJvcmRlci10b3Atd2lkdGg6IDEzcHg7XFxuICAgIGJvcmRlci1ib3R0b20td2lkdGg6IDEzcHg7XFxuICAgIHRvcDogOHB4OyB9XFxuICAubWZwLWFycm93OmJlZm9yZSB7XFxuICAgIGJvcmRlci10b3Atd2lkdGg6IDIxcHg7XFxuICAgIGJvcmRlci1ib3R0b20td2lkdGg6IDIxcHg7XFxuICAgIG9wYWNpdHk6IDAuNzsgfVxcblxcbi5tZnAtYXJyb3ctbGVmdCB7XFxuICBsZWZ0OiAwOyB9XFxuICAubWZwLWFycm93LWxlZnQ6YWZ0ZXIge1xcbiAgICBib3JkZXItcmlnaHQ6IDE3cHggc29saWQgI0ZGRjtcXG4gICAgbWFyZ2luLWxlZnQ6IDMxcHg7IH1cXG4gIC5tZnAtYXJyb3ctbGVmdDpiZWZvcmUge1xcbiAgICBtYXJnaW4tbGVmdDogMjVweDtcXG4gICAgYm9yZGVyLXJpZ2h0OiAyN3B4IHNvbGlkICMzRjNGM0Y7IH1cXG5cXG4ubWZwLWFycm93LXJpZ2h0IHtcXG4gIHJpZ2h0OiAwOyB9XFxuICAubWZwLWFycm93LXJpZ2h0OmFmdGVyIHtcXG4gICAgYm9yZGVyLWxlZnQ6IDE3cHggc29saWQgI0ZGRjtcXG4gICAgbWFyZ2luLWxlZnQ6IDM5cHg7IH1cXG4gIC5tZnAtYXJyb3ctcmlnaHQ6YmVmb3JlIHtcXG4gICAgYm9yZGVyLWxlZnQ6IDI3cHggc29saWQgIzNGM0YzRjsgfVxcblxcbi5tZnAtaWZyYW1lLWhvbGRlciB7XFxuICBwYWRkaW5nLXRvcDogNDBweDtcXG4gIHBhZGRpbmctYm90dG9tOiA0MHB4OyB9XFxuICAubWZwLWlmcmFtZS1ob2xkZXIgLm1mcC1jb250ZW50IHtcXG4gICAgbGluZS1oZWlnaHQ6IDA7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgICBtYXgtd2lkdGg6IDkwMHB4OyB9XFxuICAubWZwLWlmcmFtZS1ob2xkZXIgLm1mcC1jbG9zZSB7XFxuICAgIHRvcDogLTQwcHg7IH1cXG5cXG4ubWZwLWlmcmFtZS1zY2FsZXIge1xcbiAgd2lkdGg6IDEwMCU7XFxuICBoZWlnaHQ6IDA7XFxuICBvdmVyZmxvdzogaGlkZGVuO1xcbiAgcGFkZGluZy10b3A6IDU2LjI1JTsgfVxcbiAgLm1mcC1pZnJhbWUtc2NhbGVyIGlmcmFtZSB7XFxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gICAgZGlzcGxheTogYmxvY2s7XFxuICAgIHRvcDogMDtcXG4gICAgbGVmdDogMDtcXG4gICAgd2lkdGg6IDEwMCU7XFxuICAgIGhlaWdodDogMTAwJTtcXG4gICAgYm94LXNoYWRvdzogMCAwIDhweCByZ2JhKDAsIDAsIDAsIDAuNik7XFxuICAgIGJhY2tncm91bmQ6ICMwMDA7IH1cXG5cXG4vKiBNYWluIGltYWdlIGluIHBvcHVwICovXFxuaW1nLm1mcC1pbWcge1xcbiAgd2lkdGg6IGF1dG87XFxuICBtYXgtd2lkdGg6IDEwMCU7XFxuICBoZWlnaHQ6IGF1dG87XFxuICBkaXNwbGF5OiBibG9jaztcXG4gIGxpbmUtaGVpZ2h0OiAwO1xcbiAgYm94LXNpemluZzogYm9yZGVyLWJveDtcXG4gIHBhZGRpbmc6IDQwcHggMCA0MHB4O1xcbiAgbWFyZ2luOiAwIGF1dG87IH1cXG5cXG4vKiBUaGUgc2hhZG93IGJlaGluZCB0aGUgaW1hZ2UgKi9cXG4ubWZwLWZpZ3VyZSB7XFxuICBsaW5lLWhlaWdodDogMDsgfVxcbiAgLm1mcC1maWd1cmU6YWZ0ZXIge1xcbiAgICBjb250ZW50OiAnJztcXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgICBsZWZ0OiAwO1xcbiAgICB0b3A6IDQwcHg7XFxuICAgIGJvdHRvbTogNDBweDtcXG4gICAgZGlzcGxheTogYmxvY2s7XFxuICAgIHJpZ2h0OiAwO1xcbiAgICB3aWR0aDogYXV0bztcXG4gICAgaGVpZ2h0OiBhdXRvO1xcbiAgICB6LWluZGV4OiAtMTtcXG4gICAgYm94LXNoYWRvdzogMCAwIDhweCByZ2JhKDAsIDAsIDAsIDAuNik7XFxuICAgIGJhY2tncm91bmQ6ICM0NDQ7IH1cXG4gIC5tZnAtZmlndXJlIHNtYWxsIHtcXG4gICAgY29sb3I6ICNCREJEQkQ7XFxuICAgIGRpc3BsYXk6IGJsb2NrO1xcbiAgICBmb250LXNpemU6IDEycHg7XFxuICAgIGxpbmUtaGVpZ2h0OiAxNHB4OyB9XFxuICAubWZwLWZpZ3VyZSBmaWd1cmUge1xcbiAgICBtYXJnaW46IDA7IH1cXG5cXG4ubWZwLWJvdHRvbS1iYXIge1xcbiAgbWFyZ2luLXRvcDogLTM2cHg7XFxuICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICB0b3A6IDEwMCU7XFxuICBsZWZ0OiAwO1xcbiAgd2lkdGg6IDEwMCU7XFxuICBjdXJzb3I6IGF1dG87IH1cXG5cXG4ubWZwLXRpdGxlIHtcXG4gIHRleHQtYWxpZ246IGxlZnQ7XFxuICBsaW5lLWhlaWdodDogMThweDtcXG4gIGNvbG9yOiAjRjNGM0YzO1xcbiAgd29yZC13cmFwOiBicmVhay13b3JkO1xcbiAgcGFkZGluZy1yaWdodDogMzZweDsgfVxcblxcbi5tZnAtaW1hZ2UtaG9sZGVyIC5tZnAtY29udGVudCB7XFxuICBtYXgtd2lkdGg6IDEwMCU7IH1cXG5cXG4ubWZwLWdhbGxlcnkgLm1mcC1pbWFnZS1ob2xkZXIgLm1mcC1maWd1cmUge1xcbiAgY3Vyc29yOiBwb2ludGVyOyB9XFxuXFxuLm1mcC1hbmltYXRlLXpvb20taW4gLm1mcC1pZnJhbWUtc2NhbGVyIHtcXG4gIG92ZXJmbG93OiBpbml0aWFsOyB9XFxuICAubWZwLWFuaW1hdGUtem9vbS1pbiAubWZwLWlmcmFtZS1zY2FsZXIgLm1mcC1jbG9zZSB7XFxuICAgIHdpZHRoOiA0NHB4OyB9XFxuXFxuLm1mcC1hbmltYXRlLXpvb20taW4gLm1mcC1pZnJhbWUtc2NhbGVyIGlmcmFtZSxcXG4ubWZwLWFuaW1hdGUtem9vbS1pbiAubWZwLWZpZ3VyZSxcXG4ubWZwLWFuaW1hdGUtem9vbS1pbiAubWZwLWFycm93LFxcbi5tZnAtYW5pbWF0ZS16b29tLWluIC5tZnAtY29udGVudCA+ICoge1xcbiAgb3BhY2l0eTogMDtcXG4gIHRyYW5zZm9ybTogc2NhbGUoMC44KTtcXG4gIHRyYW5zaXRpb246IHRyYW5zZm9ybSAwLjJzIGVhc2UtaW4tb3V0LCBvcGFjaXR5IDAuMnMgZWFzZS1pbi1vdXQ7IH1cXG5cXG4ubWZwLWFuaW1hdGUtem9vbS1pbi5tZnAtcmVhZHkgLm1mcC1pZnJhbWUtc2NhbGVyIGlmcmFtZSxcXG4ubWZwLWFuaW1hdGUtem9vbS1pbi5tZnAtcmVhZHkgLm1mcC1maWd1cmUsXFxuLm1mcC1hbmltYXRlLXpvb20taW4ubWZwLXJlYWR5IC5tZnAtYXJyb3csXFxuLm1mcC1hbmltYXRlLXpvb20taW4ubWZwLXJlYWR5IC5tZnAtY29udGVudCA+ICoge1xcbiAgb3BhY2l0eTogMTtcXG4gIHRyYW5zZm9ybTogc2NhbGUoMSk7IH1cXG5cXG4ubWZwLWFuaW1hdGUtem9vbS1pbi5tZnAtcmVtb3ZpbmcgLm1mcC1pZnJhbWUtc2NhbGVyIGlmcmFtZSxcXG4ubWZwLWFuaW1hdGUtem9vbS1pbi5tZnAtcmVtb3ZpbmcgLm1mcC1maWd1cmUsXFxuLm1mcC1hbmltYXRlLXpvb20taW4ubWZwLXJlbW92aW5nIC5tZnAtYXJyb3csXFxuLm1mcC1hbmltYXRlLXpvb20taW4ubWZwLXJlbW92aW5nIC5tZnAtY29udGVudCA+ICoge1xcbiAgb3BhY2l0eTogMDtcXG4gIHRyYW5zZm9ybTogc2NhbGUoMC44KTsgfVxcblxcbi5tZnAtYW5pbWF0ZS16b29tLWluLm1mcC1iZyB7XFxuICBvcGFjaXR5OiAwO1xcbiAgdHJhbnNpdGlvbjogb3BhY2l0eSAwLjNzIGVhc2UtaW4tb3V0OyB9XFxuICAubWZwLWFuaW1hdGUtem9vbS1pbi5tZnAtYmcubWZwLXJlYWR5IHtcXG4gICAgb3BhY2l0eTogLjg7IH1cXG4gIC5tZnAtYW5pbWF0ZS16b29tLWluLm1mcC1iZy5tZnAtcmVtb3Zpbmcge1xcbiAgICBvcGFjaXR5OiAwOyB9XFxuICBAbWVkaWEgYWxsIGFuZCAobWF4LXdpZHRoOiA5MDBweCkge1xcbiAgLm1mcC1hcnJvdyB7XFxuICAgIHRyYW5zZm9ybTogc2NhbGUoMC43NSk7IH1cXG4gIC5tZnAtYXJyb3ctbGVmdCB7XFxuICAgIHRyYW5zZm9ybS1vcmlnaW46IDA7IH1cXG4gIC5tZnAtYXJyb3ctcmlnaHQge1xcbiAgICB0cmFuc2Zvcm0tb3JpZ2luOiAxMDAlOyB9XFxuICAubWZwLWNvbnRhaW5lciB7XFxuICAgIHBhZGRpbmctbGVmdDogNnB4O1xcbiAgICBwYWRkaW5nLXJpZ2h0OiA2cHg7IH0gfVxcbiAgQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogODAwcHgpIGFuZCAob3JpZW50YXRpb246IGxhbmRzY2FwZSksIHNjcmVlbiBhbmQgKG1heC1oZWlnaHQ6IDMwMHB4KSB7XFxuICAvKipcXG4gICAgICAgKiBSZW1vdmUgYWxsIHBhZGRpbmdzIGFyb3VuZCB0aGUgaW1hZ2Ugb24gc21hbGwgc2NyZWVuXFxuICAgICAgICovXFxuICAubWZwLWltZy1tb2JpbGUgLm1mcC1pbWFnZS1ob2xkZXIge1xcbiAgICBwYWRkaW5nLWxlZnQ6IDA7XFxuICAgIHBhZGRpbmctcmlnaHQ6IDA7IH1cXG4gIC5tZnAtaW1nLW1vYmlsZSBpbWcubWZwLWltZyB7XFxuICAgIHBhZGRpbmc6IDA7IH1cXG4gIC5tZnAtaW1nLW1vYmlsZSAubWZwLWZpZ3VyZTphZnRlciB7XFxuICAgIHRvcDogMDtcXG4gICAgYm90dG9tOiAwOyB9XFxuICAubWZwLWltZy1tb2JpbGUgLm1mcC1maWd1cmUgc21hbGwge1xcbiAgICBkaXNwbGF5OiBpbmxpbmU7XFxuICAgIG1hcmdpbi1sZWZ0OiA1cHg7IH1cXG4gIC5tZnAtaW1nLW1vYmlsZSAubWZwLWJvdHRvbS1iYXIge1xcbiAgICBiYWNrZ3JvdW5kOiByZ2JhKDAsIDAsIDAsIDAuNik7XFxuICAgIGJvdHRvbTogMDtcXG4gICAgbWFyZ2luOiAwO1xcbiAgICB0b3A6IGF1dG87XFxuICAgIHBhZGRpbmc6IDNweCA1cHg7XFxuICAgIHBvc2l0aW9uOiBmaXhlZDtcXG4gICAgYm94LXNpemluZzogYm9yZGVyLWJveDsgfVxcbiAgICAubWZwLWltZy1tb2JpbGUgLm1mcC1ib3R0b20tYmFyOmVtcHR5IHtcXG4gICAgICBwYWRkaW5nOiAwOyB9XFxuICAubWZwLWltZy1tb2JpbGUgLm1mcC1jb3VudGVyIHtcXG4gICAgcmlnaHQ6IDVweDtcXG4gICAgdG9wOiAzcHg7IH1cXG4gIC5tZnAtaW1nLW1vYmlsZSAubWZwLWNsb3NlIHtcXG4gICAgdG9wOiAwO1xcbiAgICByaWdodDogMDtcXG4gICAgd2lkdGg6IDM1cHg7XFxuICAgIGhlaWdodDogMzVweDtcXG4gICAgbGluZS1oZWlnaHQ6IDM1cHg7XFxuICAgIGJhY2tncm91bmQ6IHJnYmEoMCwgMCwgMCwgMC42KTtcXG4gICAgcG9zaXRpb246IGZpeGVkO1xcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICAgIHBhZGRpbmc6IDA7IH0gfVxcblwiLCBcIlwiXSk7XG5cbi8vIGV4cG9ydHNcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXI/P3JlZi0tMi0xIS4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYj8/cmVmLS0yLTIhLi9ub2RlX21vZHVsZXMvc2Fzcy1sb2FkZXIvbGliL2xvYWRlci5qcz8/cmVmLS0yLTMhLi9fSFRNTC9zcmMvanMvX21vZHVsZXMvbWFnbmlmaWMtcG9wdXAvbWZwLWV4dGVuZC5zY3NzXG4vLyBtb2R1bGUgaWQgPSA0NlxuLy8gbW9kdWxlIGNodW5rcyA9IDkiLCIndXNlIHN0cmljdCc7XHJcblxyXG4vKipcclxuICogV2V6b20gU3RhbmRhcmQgVGFic1xyXG4gKiBAbW9kdWxlXHJcbiAqL1xyXG5cclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4vLyBJbXBvcnRzXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbmltcG9ydCAnY3VzdG9tLWpxdWVyeS1tZXRob2RzL2ZuL2dldC1teS1lbGVtZW50cyc7XHJcblxyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbi8vIFByaXZhdGVcclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuLyoqXHJcbiAqINCd0LUg0YDQtdCw0LPQuNGA0L7QstCw0YLRjCDQvdCwINC60LvQuNC6XHJcbiAqIEBwYXJhbSB7SlF1ZXJ5fSAkYnV0dG9uXHJcbiAqIEByZXR1cm4ge2Jvb2xlYW58dW5kZWZpbmVkfVxyXG4gKiBAcHJpdmF0ZVxyXG4gKi9cclxuZnVuY3Rpb24gbm9SZWFjdCAoJGJ1dHRvbikge1xyXG5cdHJldHVybiAkYnV0dG9uLmhhc0NsYXNzKHdzVGFicy5jc3NDbGFzcy5hY3RpdmUpIHx8ICRidXR0b24uaGFzQ2xhc3Mod3NUYWJzLmNzc0NsYXNzLmRpc2FibGVkKSB8fCAkYnV0dG9uLnByb3AoJ2Rpc2FibGVkJyk7XHJcbn1cclxuXHJcbi8qKlxyXG4gKiBAcGFyYW0ge0pRdWVyeX0gJGJ1dHRvblxyXG4gKiBAcGFyYW0ge0pRdWVyeX0gJGNvbnRleHRcclxuICogQHByaXZhdGVcclxuICovXHJcbmZ1bmN0aW9uIGNoYW5nZVRhYiAoJGJ1dHRvbiwgJGNvbnRleHQpIHtcclxuXHRsZXQgbXlOcyA9ICRidXR0b24uZGF0YSh3c1RhYnMua2V5cy5ucyk7XHJcblx0bGV0IG15TmFtZSA9ICRidXR0b24uZGF0YSh3c1RhYnMua2V5cy5idXR0b24pO1xyXG5cclxuXHRsZXQgYnV0dG9uc1NlbGVjdG9yID0gYFtkYXRhLSR7d3NUYWJzLmtleXMubnN9PVwiJHtteU5zfVwiXVtkYXRhLSR7d3NUYWJzLmtleXMuYnV0dG9ufV1gO1xyXG5cdGxldCBidXR0b25TeW5jU2VsZWN0b3IgPSBgW2RhdGEtJHt3c1RhYnMua2V5cy5uc309XCIke215TnN9XCJdW2RhdGEtJHt3c1RhYnMua2V5cy5idXR0b259PVwiJHtteU5hbWV9XCJdYDtcclxuXHRsZXQgYmxvY2tzU2VsZWN0b3IgPSBgW2RhdGEtJHt3c1RhYnMua2V5cy5uc309XCIke215TnN9XCJdW2RhdGEtJHt3c1RhYnMua2V5cy5ibG9ja31dYDtcclxuXHRsZXQgYmxvY2tTZWxlY3RvciA9IGBbZGF0YS0ke3dzVGFicy5rZXlzLm5zfT1cIiR7bXlOc31cIl1bZGF0YS0ke3dzVGFicy5rZXlzLmJsb2NrfT1cIiR7bXlOYW1lfVwiXWA7XHJcblxyXG5cdC8qKlxyXG5cdCAqIEB0eXBlIHtKUXVlcnl9XHJcblx0ICovXHJcblx0bGV0ICRibG9jayA9ICRidXR0b24uZ2V0TXlFbGVtZW50cyh3c1RhYnMua2V5cy5teUJsb2NrLCBibG9ja1NlbGVjdG9yKTtcclxuXHRpZiAobm9SZWFjdCgkYnV0dG9uKSkge1xyXG5cdFx0JGJ1dHRvbi5hZGQoJGJsb2NrKS50cmlnZ2VyKHdzVGFicy5ldmVudHMuYWdhaW4pO1xyXG5cdFx0cmV0dXJuIGZhbHNlO1xyXG5cdH1cclxuXHJcblx0LyoqXHJcblx0ICogQHR5cGUge0pRdWVyeX1cclxuXHQgKi9cclxuXHRsZXQgJHNpYmxpbmdCbG9ja3MgPSAkYmxvY2suZ2V0TXlFbGVtZW50cyh3c1RhYnMua2V5cy5teUJsb2NrcywgYmxvY2tzU2VsZWN0b3IsICRjb250ZXh0LCB0cnVlKTtcclxuXHJcblx0LyoqXHJcblx0ICogQHR5cGUge0pRdWVyeX1cclxuXHQgKi9cclxuXHRsZXQgJHNpYmxpbmdCdXR0b25zID0gJGJ1dHRvbi5nZXRNeUVsZW1lbnRzKHdzVGFicy5rZXlzLm15QnV0dG9ucywgYnV0dG9uc1NlbGVjdG9yLCAkY29udGV4dCwgdHJ1ZSk7XHJcblx0bGV0ICRzeW5jQnV0dG9ucyA9ICRzaWJsaW5nQnV0dG9ucy5maWx0ZXIoYnV0dG9uU3luY1NlbGVjdG9yKTtcclxuXHRpZiAoJHN5bmNCdXR0b25zLmxlbmd0aCkge1xyXG5cdFx0JHNpYmxpbmdCdXR0b25zID0gJHNpYmxpbmdCdXR0b25zLm5vdCgkc3luY0J1dHRvbnMpO1xyXG5cdH1cclxuXHJcblx0JHNpYmxpbmdCdXR0b25zLmFkZCgkc2libGluZ0Jsb2NrcykucmVtb3ZlQ2xhc3Mod3NUYWJzLmNzc0NsYXNzLmFjdGl2ZSkudHJpZ2dlcih3c1RhYnMuZXZlbnRzLm9mZik7XHJcblx0JGJ1dHRvbi5hZGQoJHN5bmNCdXR0b25zKS5hZGQoJGJsb2NrKS5hZGRDbGFzcyh3c1RhYnMuY3NzQ2xhc3MuYWN0aXZlKS50cmlnZ2VyKHdzVGFicy5ldmVudHMub24pO1xyXG59XHJcblxyXG4vKipcclxuICog0JDQutGC0LjQstCw0YbQuNGPINGC0LDQsdC+0LIsINC10YHQu9C4INC90LXRgtGDINCw0LrRgtC40LLQvdGL0YVcclxuICogQHBhcmFtIHtKUXVlcnl9ICRidXR0b25zXHJcbiAqIEBwYXJhbSB7SlF1ZXJ5fSAkY29udGV4dFxyXG4gKiBAcHJpdmF0ZVxyXG4gKi9cclxuZnVuY3Rpb24gc2V0QWN0aXZlSWZOb3RIYXZlICgkYnV0dG9ucywgJGNvbnRleHQpIHtcclxuXHRsZXQgbnMgPSAkYnV0dG9ucy5kYXRhKHdzVGFicy5rZXlzLm5zKTtcclxuXHRsZXQgc2VsZWN0b3IgPSBgW2RhdGEtJHt3c1RhYnMua2V5cy5uc309XCIke25zfVwiXWA7XHJcblx0bGV0ICRncm91cCA9ICRidXR0b25zLmZpbHRlcihzZWxlY3Rvcik7XHJcblxyXG5cdGlmICgkZ3JvdXAubGVuZ3RoKSB7XHJcblx0XHRsZXQgJGFjdGl2ZSA9ICRncm91cC5maWx0ZXIoYC4ke3dzVGFicy5jc3NDbGFzcy5hY3RpdmV9YCk7XHJcblx0XHRpZiAoISRhY3RpdmUubGVuZ3RoKSB7XHJcblx0XHRcdGNoYW5nZVRhYigkZ3JvdXAuZXEoMCksICRjb250ZXh0KTtcclxuXHRcdH1cclxuXHJcblx0XHRpZiAoJGdyb3VwLmxlbmd0aCA8ICRidXR0b25zLmxlbmd0aCkge1xyXG5cdFx0XHRzZXRBY3RpdmVJZk5vdEhhdmUoJGJ1dHRvbnMubm90KHNlbGVjdG9yKSwgJGNvbnRleHQpO1xyXG5cdFx0fVxyXG5cdH1cclxufVxyXG5cclxuLyoqXHJcbiAqINCh0LHRgNC+0YEg0LfQsNCy0LjRgdC40LzQvtC70YHRgtC10LlcclxuICogQHBhcmFtIHtKUXVlcnl9ICRsaXN0XHJcbiAqIEBwYXJhbSB7QXJyYXkuPHN0cmluZz59IGtleXNcclxuICogQHByaXZhdGVcclxuICovXHJcbmZ1bmN0aW9uIGRyb3BEZXBlbmRlbmNpZXMgKCRsaXN0LCBrZXlzKSB7XHJcblx0JGxpc3QuZWFjaCgoaSwgZWwpID0+IHtcclxuXHRcdGxldCAkaXRlbSA9ICQoZWwpO1xyXG5cdFx0d3NUYWJzLmtleXMuZm9yRWFjaChrZXkgPT4ge1xyXG5cdFx0XHQkaXRlbS5kYXRhKGtleSwgbnVsbCk7XHJcblx0XHR9KTtcclxuXHR9KTtcclxufVxyXG5cclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4vLyBQdWJsaWNcclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuLyoqXHJcbiAqIEBuYW1lc3BhY2VcclxuICovXHJcbmNvbnN0IHdzVGFicyA9IHtcclxuXHQvKipcclxuXHQgKiDQodC+0LHRi9GC0LjRj1xyXG5cdCAqIEBlbnVtIHtzdHJpbmd9XHJcblx0ICogQHNvdXJjZUNvZGVcclxuXHQgKi9cclxuXHRldmVudHM6IHtcclxuXHRcdG9uOiAnd3N0YWJzLW9uJyxcclxuXHRcdG9mZjogJ3dzdGFicy1vZmYnLFxyXG5cdFx0YWdhaW46ICd3c3RhYnMtYWdhaW4nXHJcblx0fSxcclxuXHJcblx0LyoqXHJcblx0ICogQ1NTINC60LvQsNGB0YHRi1xyXG5cdCAqIEBlbnVtIHtzdHJpbmd9XHJcblx0ICogQHNvdXJjZUNvZGVcclxuXHQgKi9cclxuXHRjc3NDbGFzczoge1xyXG5cdFx0YWN0aXZlOiAnaXMtYWN0aXZlJyxcclxuXHRcdGRpc2FibGVkOiAnaXMtZGlzYWJsZWQnXHJcblx0fSxcclxuXHJcblx0LyoqXHJcblx0ICog0JrQu9GO0YfQuFxyXG5cdCAqIEBlbnVtIHtzdHJpbmd9XHJcblx0ICogQHNvdXJjZUNvZGVcclxuXHQgKi9cclxuXHRrZXlzOiB7XHJcblx0XHRuczogJ3dzdGFicy1ucycsXHJcblx0XHRidXR0b246ICd3c3RhYnMtYnV0dG9uJyxcclxuXHRcdGJsb2NrOiAnd3N0YWJzLWJsb2NrJyxcclxuXHRcdG15QmxvY2s6ICckbXlXc1RhYnNCbG9jaycsXHJcblx0XHRteUJsb2NrczogJyRteVdzVGFic0Jsb2NrcycsXHJcblx0XHRteUJ1dHRvbnM6ICckbXlXc1RhYnNCdXR0b25zJ1xyXG5cdH0sXHJcblxyXG5cdC8qKlxyXG5cdCAqINCY0L3QuNGG0LjQsNC70LjQt9Cw0YbQuNGPXHJcblx0ICogQHBhcmFtIHtKUXVlcnl9IFskY29udGV4dD0kKGRvY3VtZW50KV1cclxuXHQgKiBAc291cmNlQ29kZVxyXG5cdCAqL1xyXG5cdGluaXQgKCRjb250ZXh0ID0gJChkb2N1bWVudCkpIHtcclxuXHRcdCRjb250ZXh0Lm9uKCdjbGljaycsIGBbZGF0YS0ke3dzVGFicy5rZXlzLmJ1dHRvbn1dYCwgeyRjb250ZXh0fSwgZnVuY3Rpb24gKGV2ZW50KSB7XHJcblx0XHRcdGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XHJcblx0XHRcdGNoYW5nZVRhYigkKHRoaXMpLCAkY29udGV4dCk7XHJcblx0XHR9KTtcclxuXHR9LFxyXG5cclxuXHQvKipcclxuXHQgKiDQn9GA0LjQvdGD0LTQuNGC0LXQu9GM0L3QsNGPINCw0LrRgtC40LLQsNGG0LjRjyDRgtCw0LHQvtCyLCDQtdGB0LvQuCDQvdC10YLRgyDQsNC60YLQuNCy0L3Ri9GFXHJcblx0ICogQHBhcmFtIHtKUXVlcnl9IFskY29udGV4dD0kKGRvY3VtZW50KV1cclxuXHQgKiBAc291cmNlQ29kZVxyXG5cdCAqL1xyXG5cdHNldEFjdGl2ZSAoJGNvbnRleHQgPSAkKGRvY3VtZW50KSkge1xyXG5cdFx0bGV0ICRidXR0b25zID0gJGNvbnRleHQuZmluZChgW2RhdGEtJHt3c1RhYnMua2V5cy5idXR0b259XWApO1xyXG5cdFx0c2V0QWN0aXZlSWZOb3RIYXZlKCRidXR0b25zLCAkY29udGV4dCk7XHJcblx0fSxcclxuXHJcblx0LyoqXHJcblx0ICog0KHQsdGA0L7RgSDQstGB0LXRhSDRgdCy0Y/Qt9C10LkuXHJcblx0ICog0JDQutGC0YPQsNC70YzQvdC+INC/0YDQuCDQtNC40L3QsNC80LjRh9C10YHQutC+0Lwg0LTQvtCx0LDQstC70LXQvdC40LUg0L3QvtCy0YvQuSDQutC90L7Qv9C+0Log0Lgg0LHQu9C+0LrQvtCyINCyINGD0LbQtSDRgdGD0YnQtdGB0YLQstGD0Y7RidC40LUg0LPRgNGD0L/Qv9GLINGC0LDQsdC+0LJcclxuXHQgKiBAcGFyYW0ge0pRdWVyeX0gWyRjb250ZXh0PSQoZG9jdW1lbnQpXVxyXG5cdCAqIEBzb3VyY2VDb2RlXHJcblx0ICovXHJcblx0ZHJvcERlcGVuZGVuY2llcyAoJGNvbnRleHQgPSAkKGRvY3VtZW50KSkge1xyXG5cdFx0bGV0ICRidXR0b25zID0gJGNvbnRleHQuZmluZChgW2RhdGEtJHt3c1RhYnMua2V5cy5idXR0b259XWApO1xyXG5cdFx0bGV0ICRibG9ja3MgPSAkY29udGV4dC5maW5kKGBbZGF0YS0ke3dzVGFicy5rZXlzLmJsb2NrfV1gKTtcclxuXHRcdGRyb3BEZXBlbmRlbmNpZXMoJGJ1dHRvbnMsIFt3c1RhYnMua2V5cy5teUJsb2NrLCB3c1RhYnMua2V5cy5teUJ1dHRvbnNdKTtcclxuXHRcdGRyb3BEZXBlbmRlbmNpZXMoJGJsb2NrcywgW3dzVGFicy5rZXlzLm15QmxvY2tzXSk7XHJcblx0fVxyXG59O1xyXG5cclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4vLyBFeHBvcnRzXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IHdzVGFicztcclxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vX0hUTUwvc3JjL2pzL19tb2R1bGVzL3dzdGFicy5qcyIsIid1c2Ugc3RyaWN0JztcclxuXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuLy8gSW1wb3J0c1xyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG5pbXBvcnQgJ2pxdWVyeS5tbWVudSc7XHJcblxyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbi8vIFByaXZhdGVcclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxubGV0ICRmaWx0ZXJzTWVudSA9ICQoJyNmaWx0ZXJzLW1lbnUnKTtcclxubGV0ICRvcGVuQm50ID0gJCgnLm1lbnUtb3BlbicpO1xyXG5sZXQgJGNsb3NlQnRuID0gJCgnLnNpZGViYXItY2xvc2UnKTtcclxubGV0ICRtb2JpbGVPcGVuQnRuID0gJCgnLm1vYmlsZS1tZW51LW9wZW4nKTtcclxubGV0ICRtb2JpbGVDbG9zZUJ0biA9ICQoJy5tb2JpbGUtbWVudS1jbG9zZScpO1xyXG5sZXQgJGZpbHRlcnNPcGVuQnRuID0gJCgnLmZpbHRlcnMtbWVudS1vcGVuJyk7XHJcbmxldCAkZmlsdGVyc0Nsb3NlQnRuID0gJCgnLmZpbHRlcnMtbWVudS1jbG9zZScpO1xyXG5cclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4vLyBQdWJsaWNcclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuZnVuY3Rpb24gbWFpbk1lbnUgKCkge1xyXG5cdGxldCAkbWVudSA9ICQoJyNtZW51JykubW1lbnUoe1xyXG5cdFx0J3NsaWRpbmdTdWJtZW51cyc6IGZhbHNlLFxyXG5cdFx0J2V4dGVuc2lvbnMnOiBbXHJcblx0XHRcdCdmeC1tZW51LXNsaWRlJyxcclxuXHRcdFx0J2JvcmRlci1ub25lJyxcclxuXHRcdFx0J3BhZ2VkaW0tYmxhY2snXHJcblx0XHRdLFxyXG5cdFx0J25hdmJhcic6IHtcclxuXHRcdFx0YWRkOiBmYWxzZVxyXG5cdFx0fVxyXG5cdH0pO1xyXG5cclxuXHRsZXQgQVBJID0gJG1lbnUuZGF0YSgnbW1lbnUnKTtcclxuXHJcblx0JG9wZW5CbnQub24oJ2NsaWNrJywgZnVuY3Rpb24gKCkge1xyXG5cdFx0QVBJLm9wZW4oKTtcclxuXHR9KTtcclxuXHJcblx0JGNsb3NlQnRuLmNsaWNrKGZ1bmN0aW9uICgpIHtcclxuXHRcdEFQSS5jbG9zZSgpO1xyXG5cdH0pO1xyXG59XHJcblxyXG5mdW5jdGlvbiBtb2JpbGVNZW51ICgpIHtcclxuXHQvLyBpZiAoJCh3aW5kb3cpLndpZHRoKCkgPiAxMDI0KSB7XHJcblx0Ly8gXHRyZXR1cm4gZmFsc2U7XHJcblx0Ly8gfVxyXG5cclxuXHRsZXQgJG1lbnUgPSAkKCcjbW9iaWxlLW1lbnUnKS5tbWVudSh7XHJcblx0XHQnc2xpZGluZ1N1Ym1lbnVzJzogZmFsc2UsXHJcblx0XHQnZXh0ZW5zaW9ucyc6IFtcclxuXHRcdFx0J2JvcmRlci1ub25lJyxcclxuXHRcdFx0J3BhZ2VkaW0tYmxhY2snLFxyXG5cdFx0XHQnZngtbWVudS1zbGlkZSdcclxuXHRcdF0sXHJcblx0XHQnbmF2YmFyJzoge1xyXG5cdFx0XHRhZGQ6IGZhbHNlXHJcblx0XHR9XHJcblx0fSk7XHJcblxyXG5cdGxldCBBUEkgPSAkbWVudS5kYXRhKCdtbWVudScpO1xyXG5cclxuXHQkbW9iaWxlT3BlbkJ0bi5vbignY2xpY2snLCBmdW5jdGlvbiAoKSB7XHJcblx0XHRBUEkub3BlbigpO1xyXG5cdH0pO1xyXG5cclxuXHQkbW9iaWxlQ2xvc2VCdG4uY2xpY2soZnVuY3Rpb24gKCkge1xyXG5cdFx0QVBJLmNsb3NlKCk7XHJcblx0fSk7XHJcbn1cclxuXHJcbmZ1bmN0aW9uIGZpbHRlcnNNZW51ICgpIHtcclxuXHRpZiAoISRmaWx0ZXJzTWVudS5sZW5ndGgpIHtcclxuXHRcdHJldHVybiBmYWxzZTtcclxuXHR9XHJcblxyXG5cdGlmICgkKHdpbmRvdykud2lkdGgoKSA+IDEwMjQpIHtcclxuXHRcdHJldHVybiBmYWxzZTtcclxuXHR9XHJcblxyXG5cdGxldCAkbWVudSA9ICRmaWx0ZXJzTWVudS5tbWVudSh7XHJcblx0XHQnc2xpZGluZ1N1Ym1lbnVzJzogZmFsc2UsXHJcblx0XHQnZXh0ZW5zaW9ucyc6IFtcclxuXHRcdFx0J2JvcmRlci1ub25lJyxcclxuXHRcdFx0J3BhZ2VkaW0tYmxhY2snLFxyXG5cdFx0XHQncG9zaXRpb24tZnJvbnQnXHJcblx0XHRdLFxyXG5cdFx0J25hdmJhcic6IHtcclxuXHRcdFx0YWRkOiBmYWxzZVxyXG5cdFx0fVxyXG5cdH0pO1xyXG5cdCRtZW51LmFwcGVuZCgkKCcubW9iaWxlX19zdW1tYXJ5JykpO1xyXG5cclxuXHRsZXQgQVBJID0gJG1lbnUuZGF0YSgnbW1lbnUnKTtcclxuXHJcblx0JGZpbHRlcnNPcGVuQnRuLm9uKCdjbGljaycsIGZ1bmN0aW9uICgpIHtcclxuXHRcdEFQSS5vcGVuKCk7XHJcblx0fSk7XHJcblxyXG5cdCRmaWx0ZXJzQ2xvc2VCdG4uY2xpY2soZnVuY3Rpb24gKCkge1xyXG5cdFx0QVBJLmNsb3NlKCk7XHJcblx0fSk7XHJcbn1cclxuXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuLy8gRXhwb3J0c1xyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG5leHBvcnQge21haW5NZW51LCBtb2JpbGVNZW51LCBmaWx0ZXJzTWVudX07XHJcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL19IVE1ML3NyYy9qcy9fbW9kdWxlcy9tZW51LmpzIiwiJ3VzZSBzdHJpY3QnO1xyXG5cclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4vLyBQcml2YXRlXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbmxldCBoZWFkZXIgPSAkKCcuaGVhZGVyX190b3AnKTsgLy8g0JzQtdC90Y5cclxubGV0IHNjcm9sbFByZXYgPSAwOyAvLyDQn9GA0LXQtNGL0LTRg9GJ0LXQtSDQt9C90LDRh9C10L3QuNC1INGB0LrRgNC+0LvQu9CwXHJcbmxldCAkbWVudUNoaWxkSXRlbSA9ICQoJy5oZWFkZXItbWVudV9faXRlbS5jaGlsZCcpO1xyXG5sZXQgYm9keSA9ICQoJ2JvZHknKTtcclxuXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuLy8gUHVibGljXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbmZ1bmN0aW9uIGZpeGVkSGVhZGVyICgpIHtcclxuXHRib2R5LmF0dHIoJ2RhdGEtcGFyYW0nLCAkKCcuaGVhZGVyJykuaGVpZ2h0KCkpO1xyXG5cclxuXHRpZiAoJCh3aW5kb3cpLndpZHRoKCkgPiAxMDIzICAmJiBib2R5Lmhhc0NsYXNzKCdmeCcpKSB7XHJcblx0XHQkKHdpbmRvdykuc2Nyb2xsKGZ1bmN0aW9uICgpIHtcclxuXHRcdFx0bGV0IHNjcm9sbGVkID0gJCh3aW5kb3cpLnNjcm9sbFRvcCgpOyAvLyDQktGL0YHQvtGC0LAg0YHQutGA0L7Qu9C70LAg0LIgcHhcclxuXHRcdFx0bGV0IGZpcnN0U2Nyb2xsVXAgPSBmYWxzZTsgLy8g0J/QsNGA0LDQvNC10YLRgCDQvdCw0YfQsNC70LAg0YHQutC+0LvQu9CwINCy0LLQtdGA0YVcclxuXHRcdFx0bGV0IGZpcnN0U2Nyb2xsRG93biA9IGZhbHNlOyAvLyDQn9Cw0YDQsNC80LXRgtGAINC90LDRh9Cw0LvQsCDRgdC60L7Qu9C70LAg0LLQvdC40LdcclxuXHRcdFx0bGV0IGNhdGFsb2dNZW51ID0gJCgnLmpzLWNhdGFsb2cnKTtcclxuXHRcdFx0bGV0IGhlaWdodCA9IGJvZHkuYXR0cignZGF0YS1wYXJhbScpO1xyXG5cclxuXHRcdFx0Ly8g0JXRgdC70Lgg0YHQutGA0L7Qu9C70LjQvFxyXG5cdFx0XHRpZiAoc2Nyb2xsZWQgPiAwKSB7XHJcblx0XHRcdFx0Ly8g0JXRgdC70Lgg0YLQtdC60YPRidC10LUg0LfQvdCw0YfQtdC90LjQtSDRgdC60YDQvtC70LvQsCA+INC/0YDQtdC00YvQtNGD0YnQtdCz0L4sINGCLtC1LiDRgdC60YDQvtC70LvQuNC8INCy0L3QuNC3XHJcblx0XHRcdFx0aWYgKHNjcm9sbGVkID4gc2Nyb2xsUHJldikge1xyXG5cdFx0XHRcdFx0Zmlyc3RTY3JvbGxVcCA9IGZhbHNlOyAvLyDQntCx0L3Rg9C70Y/QtdC8INC/0LDRgNCw0LzQtdGC0YAg0L3QsNGH0LDQu9CwINGB0LrRgNC+0LvQu9CwINCy0LLQtdGA0YVcclxuXHRcdFx0XHRcdC8vINCV0YHQu9C4INC80LXQvdGOINCy0LjQtNC90L5cclxuXHRcdFx0XHRcdGlmIChzY3JvbGxlZCA8IGhlYWRlci5oZWlnaHQoKSArIGhlYWRlci5vZmZzZXQoKS50b3ApIHtcclxuXHRcdFx0XHRcdFx0Ly8g0JXRgdC70Lgg0YLQvtC70YzQutC+INC90LDRh9Cw0LvQuCDRgdC60YDQvtC70LvQuNGC0Ywg0LLQvdC40LdcclxuXHRcdFx0XHRcdFx0aWYgKGZpcnN0U2Nyb2xsRG93biA9PT0gZmFsc2UpIHtcclxuXHRcdFx0XHRcdFx0XHRsZXQgdG9wUG9zaXRpb24gPSBoZWFkZXIub2Zmc2V0KCkudG9wOyAvLyDQpNC40LrRgdC40YDRg9C10Lwg0YLQtdC60YPRidGD0Y4g0L/QvtC30LjRhtC40Y4g0LzQtdC90Y5cclxuXHRcdFx0XHRcdFx0XHRoZWFkZXIucmVtb3ZlQ2xhc3MoJ19fZml4ZWQnKTtcclxuXHRcdFx0XHRcdFx0XHRoZWFkZXIuY3NzKHtcclxuXHRcdFx0XHRcdFx0XHRcdCd0b3AnOiAnMHB4J1xyXG5cdFx0XHRcdFx0XHRcdH0pO1xyXG5cdFx0XHRcdFx0XHRcdGZpcnN0U2Nyb2xsRG93biA9IHRydWU7XHJcblx0XHRcdFx0XHRcdH1cclxuXHJcblx0XHRcdFx0XHRcdC8vINCV0YHQu9C4INC80LXQvdGOINCd0JUg0LLQuNC00L3QvlxyXG5cdFx0XHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHRcdFx0Ly8g0J/QvtC30LjRhtC40L7QvdC40YDRg9C10Lwg0LzQtdC90Y4g0YTQuNC60YHQuNGA0L7QstCw0L3QvdC+INCy0L3QtSDRjdC60YDQsNC90LBcclxuXHRcdFx0XHRcdFx0aGVhZGVyLmNzcyh7XHJcblx0XHRcdFx0XHRcdFx0J3RvcCc6ICctJyArIGhlYWRlci5oZWlnaHQoKSArICdweCdcclxuXHRcdFx0XHRcdFx0fSk7XHJcblx0XHRcdFx0XHR9XHJcblxyXG5cdFx0XHRcdFx0Ly8g0JXRgdC70Lgg0YLQtdC60YPRidC10LUg0LfQvdCw0YfQtdC90LjQtSDRgdC60YDQvtC70LvQsCA8INC/0YDQtdC00YvQtNGD0YnQtdCz0L4sINGCLtC1LiDRgdC60YDQvtC70LvQuNC8INCy0LLQtdGA0YVcclxuXHRcdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdFx0Zmlyc3RTY3JvbGxEb3duID0gZmFsc2U7IC8vINCe0LHQvdGD0LvRj9C10Lwg0L/QsNGA0LDQvNC10YLRgCDQvdCw0YfQsNC70LAg0YHQutGA0L7Qu9C70LAg0LLQvdC40LdcclxuXHRcdFx0XHRcdC8vINCV0YHQu9C4INC80LXQvdGOINC90LUg0LLQuNC00L3QvlxyXG5cdFx0XHRcdFx0aWYgKHNjcm9sbGVkID4gaGVhZGVyLm9mZnNldCgpLnRvcCkge1xyXG5cdFx0XHRcdFx0XHQvLyDQldGB0LvQuCDRgtC+0LvRjNC60L4g0L3QsNGH0LDQu9C4INGB0LrRgNC+0LvQu9C40YLRjCDQstCy0LXRgNGFXHJcblx0XHRcdFx0XHRcdGlmIChmaXJzdFNjcm9sbFVwID09PSBmYWxzZSkge1xyXG5cdFx0XHRcdFx0XHRcdGxldCB0b3BQb3NpdGlvbiA9IGhlYWRlci5vZmZzZXQoKS50b3A7IC8vINCk0LjQutGB0LjRgNGD0LXQvCDRgtC10LrRg9GJ0YPRjiDQv9C+0LfQuNGG0LjRjiDQvNC10L3RjlxyXG5cdFx0XHRcdFx0XHRcdGhlYWRlci5hZGRDbGFzcygnX19maXhlZCcpO1xyXG5cdFx0XHRcdFx0XHRcdGhlYWRlci5jc3Moe1xyXG5cdFx0XHRcdFx0XHRcdFx0J3RvcCc6ICcwcHgnXHJcblx0XHRcdFx0XHRcdFx0fSk7XHJcblx0XHRcdFx0XHRcdFx0Zmlyc3RTY3JvbGxVcCA9IHRydWU7XHJcblx0XHRcdFx0XHRcdH1cclxuXHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0fVxyXG5cdFx0XHRcdC8vINCf0YDQuNGB0LLQsNC10LLQsNC10Lwg0YLQtdC60YPRidC10LUg0LfQvdCw0YfQtdC90LjQtSDRgdC60YDQvtC70LvQsCDQv9GA0LXQtNGL0LTRg9GJ0LXQvNGDXHJcblx0XHRcdFx0c2Nyb2xsUHJldiA9IHNjcm9sbGVkO1xyXG5cdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdGhlYWRlci5yZW1vdmVDbGFzcygnX19maXhlZCcpO1xyXG5cdFx0XHRcdGhlYWRlci5jc3Moe1xyXG5cdFx0XHRcdFx0J3RvcCc6ICcwcHgnXHJcblx0XHRcdFx0fSk7XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdGNhdGFsb2dNZW51LmNzcygndG9wJywgaGVhZGVyLmhlaWdodCgpKTtcclxuXHJcblx0XHRcdGlmIChoZWFkZXIuaGFzQ2xhc3MoJ19fZml4ZWQnKSkge1xyXG5cdFx0XHRcdGJvZHkuY3NzKCdwYWRkaW5nLXRvcCcsIGhlaWdodCArICdweCcpO1xyXG5cdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdGJvZHkuY3NzKCdwYWRkaW5nLXRvcCcsIDApO1xyXG5cdFx0XHR9XHJcblx0XHR9KTtcclxuXHR9XHJcbn1cclxuXHJcbmZ1bmN0aW9uIHN1Yk1lbnVTaG93ICgpIHtcclxuXHQkbWVudUNoaWxkSXRlbS5vbignbW91c2VlbnRlcicsIGZ1bmN0aW9uIChlKSB7XHJcblx0XHRsZXQgJHRhcmdldCA9ICQodGhpcyk7XHJcblx0XHRsZXQgc3BlZWQgPSAkdGFyZ2V0Lmhhc0NsYXNzKCdjaGlsZCcpID8gMCA6IDQwMDtcclxuXHRcdCQoJy5oZWFkZXItbWVudV9fY2hpbGQtd3JhcCcpLmVhY2goZnVuY3Rpb24gKCkge1xyXG5cdFx0XHQkKHRoaXMpLnN0b3AoKS5jc3MoJ2Rpc3BsYXknLCAnbm9uZScpO1xyXG5cdFx0fSk7XHJcblx0XHQkdGFyZ2V0LmZpbmQoJy5oZWFkZXItbWVudV9fY2hpbGQtd3JhcCcpLmZhZGVJbihzcGVlZCk7XHJcblx0fSkub24oJ21vdXNlbGVhdmUnLCBmdW5jdGlvbiAoKSB7XHJcblx0XHRsZXQgJHN1Yk1lbnUgPSAkKHRoaXMpLmZpbmQoJy5oZWFkZXItbWVudV9fY2hpbGQtd3JhcCcpO1xyXG5cdFx0JHN1Yk1lbnUuc3RvcCgpLmZhZGVPdXQoMTUwKTtcclxuXHR9KTtcclxufVxyXG5cclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4vLyBFeHBvcnRzXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbmV4cG9ydCB7Zml4ZWRIZWFkZXIsIHN1Yk1lbnVTaG93fTtcclxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vX0hUTUwvc3JjL2pzL19tb2R1bGVzL2ZpeGVkLWhlYWRlci5qcyIsIid1c2Ugc3RyaWN0JztcclxuXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuLy8gSW1wb3J0c1xyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG5pbXBvcnQgJy4uL192ZW5kb3JzL2pxdWVyeS11aS5taW4nO1xyXG5pbXBvcnQgJy4uL192ZW5kb3JzL2pxdWVyeS51aS50b3VjaC1wdW5jaC5taW4nO1xyXG5cclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4vLyBQcml2YXRlXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbmxldCAkcmFuZ2VTbGlkZXIgPSAkKCcucHJpY2Utc2xpZGVyJyk7XHJcblxyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbi8vIFB1YmxpY1xyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG5mdW5jdGlvbiBpbml0UmFuZ2VTbGlkZXIgKCkge1xyXG5cdGlmICghJHJhbmdlU2xpZGVyLmxlbmd0aCkge1xyXG5cdFx0cmV0dXJuIGZhbHNlO1xyXG5cdH1cclxuXHJcblx0JHJhbmdlU2xpZGVyLmVhY2goZnVuY3Rpb24gKGl0ZW0pIHtcclxuXHRcdGxldCAkdGhpcyA9ICQodGhpcyk7XHJcblx0XHRsZXQgJHdyYXAgPSAkdGhpcy5jbG9zZXN0KCcuZmlsdGVyc19fcHJpY2UtZ3JvdXAnKTtcclxuXHRcdGxldCAkbWluQ29zdCA9ICR3cmFwLmZpbmQoJy5taW4tY29zdCcpO1xyXG5cdFx0bGV0ICRtYXhDb3N0ID0gJHdyYXAuZmluZCgnLm1heC1jb3N0Jyk7XHJcblx0XHRsZXQgbWluID0gcGFyc2VGbG9hdCgkdGhpcy5kYXRhKCdtaW4nKSk7XHJcblx0XHRsZXQgbWF4ID0gcGFyc2VGbG9hdCgkdGhpcy5kYXRhKCdtYXgnKSk7XHJcblx0XHRsZXQgc3RhcnQgPSBwYXJzZUZsb2F0KCR0aGlzLmRhdGEoJ3N0YXJ0JykpO1xyXG5cdFx0bGV0IGVuZCA9IHBhcnNlRmxvYXQoJHRoaXMuZGF0YSgnZW5kJykpO1xyXG5cclxuXHRcdGxldCBzdGFydFZhbHVlID0gKHN0YXJ0ICE9PSB1bmRlZmluZWQpID8gc3RhcnQgOiBtaW47XHJcblx0XHRsZXQgZW5kVmFsdWUgPSAoZW5kICE9PSB1bmRlZmluZWQpID8gZW5kIDogbWF4O1xyXG5cclxuXHRcdCQodGhpcykuc2xpZGVyKHtcclxuXHRcdFx0bWluOiBtaW4sXHJcblx0XHRcdG1heDogbWF4LFxyXG5cdFx0XHRyYW5nZTogdHJ1ZSxcclxuXHRcdFx0dmFsdWVzOiBbc3RhcnRWYWx1ZSwgZW5kVmFsdWVdLFxyXG5cclxuXHRcdFx0c3RvcDogZnVuY3Rpb24gKGV2ZW50LCB1aSkge1xyXG5cdFx0XHRcdCRtaW5Db3N0LnZhbCh1aS52YWx1ZXNbMF0pO1xyXG5cdFx0XHRcdCRtYXhDb3N0LnZhbCh1aS52YWx1ZXNbMV0pO1xyXG5cdFx0XHR9LFxyXG5cclxuXHRcdFx0c2xpZGU6IGZ1bmN0aW9uIChldmVudCwgdWkpIHtcclxuXHRcdFx0XHQkbWluQ29zdC52YWwodWkudmFsdWVzWzBdKTtcclxuXHRcdFx0XHQkbWF4Q29zdC52YWwodWkudmFsdWVzWzFdKTtcclxuXHRcdFx0fSxcclxuXHJcblx0XHRcdGNoYW5nZTogZnVuY3Rpb24gKGV2ZW50LCB1aSkge1xyXG5cdFx0XHRcdGxldCAkZmlsdGVyV3JhcHBlciA9ICQodGhpcykucGFyZW50cygnLm1haW4tZmlsdGVyLXdyYXBwZXInKTtcclxuXHRcdFx0XHRsZXQgJGNvdW50ZXIgPSAkKCcuZmlsdGVyLXN1bW1hcnktd3JhcHBlcicpO1xyXG5cdFx0XHRcdGxldCAkaW5wdXQgPSAkKHRoaXMpLnBhcmVudHMoJy5maWx0ZXJzX19pdGVtJyk7XHJcblx0XHRcdFx0bGV0IHBvc2l0aW9uID0gJGlucHV0Lm9mZnNldCgpO1xyXG5cclxuXHRcdFx0XHRsZXQgY2F0ZWdvcnlfaWQgPSAkZmlsdGVyV3JhcHBlci5kYXRhKCdjYXRlZ29yeScpO1xyXG5cdFx0XHRcdGxldCBmaWx0ZXJzID0ge307XHJcblx0XHRcdFx0JCgnLmZpbHRlci1pbnB1dDpjaGVja2VkJykuZWFjaChmdW5jdGlvbigpIHtcclxuXHRcdFx0XHRcdGxldCBuYW1lID0gJCh0aGlzKS5hdHRyKCduYW1lJyk7XHJcblx0XHRcdFx0XHRpZiAoIWZpbHRlcnMuaGFzT3duUHJvcGVydHkobmFtZSkpIGZpbHRlcnNbbmFtZV0gPSBbXTtcclxuXHJcblx0XHRcdFx0XHRsZXQgdmFsdWUgPSAkKHRoaXMpLnZhbCgpO1xyXG5cclxuXHRcdFx0XHRcdGlmICghZmlsdGVyc1tuYW1lXS5pbmNsdWRlcyh2YWx1ZSkpIGZpbHRlcnNbbmFtZV0ucHVzaCh2YWx1ZSk7XHJcblx0XHRcdFx0fSk7XHJcblxyXG5cdFx0XHRcdGxldCAkbWluUmFuZ2VJbnB1dCA9ICRmaWx0ZXJXcmFwcGVyLmZpbmQoJy5taW4tY29zdCcpO1xyXG5cdFx0XHRcdGxldCAkbWF4UmFuZ2VJbnB1dCA9ICRmaWx0ZXJXcmFwcGVyLmZpbmQoJy5tYXgtY29zdCcpO1xyXG5cdFx0XHRcdGxldCBtaW5SYW5nZUNvc3QgPSAkbWluUmFuZ2VJbnB1dC52YWwoKTtcclxuXHRcdFx0XHRsZXQgbWF4UmFuZ2VDb3N0ID0gJG1heFJhbmdlSW5wdXQudmFsKCk7XHJcblx0XHRcdFx0bGV0IG1pblByaWNlID0gJG1pblJhbmdlSW5wdXQuZGF0YSgnbWluJyk7XHJcblx0XHRcdFx0bGV0IG1heFByaWNlID0gJG1heFJhbmdlSW5wdXQuZGF0YSgnbWF4Jyk7XHJcblx0XHRcdFx0aWYgKG1pblByaWNlIDwgbWluUmFuZ2VDb3N0IHx8IG1heFByaWNlID4gbWF4UmFuZ2VDb3N0KSB7XHJcblx0XHRcdFx0XHRmaWx0ZXJzWydtaW5jb3N0J10gPSBtaW5SYW5nZUNvc3Q7XHJcblx0XHRcdFx0XHRmaWx0ZXJzWydtYXhjb3N0J10gPSBtYXhSYW5nZUNvc3Q7XHJcblx0XHRcdFx0fVxyXG5cclxuXHRcdFx0XHQkLmFqYXgoe1xyXG5cdFx0XHRcdFx0dHlwZTogJ3Bvc3QnLFxyXG5cdFx0XHRcdFx0dXJsOiAnL2FqYXgvcHJlcGFyZUZpbHRlcicsXHJcblx0XHRcdFx0XHRkYXRhOiB7XHJcblx0XHRcdFx0XHRcdGNhdGVnb3J5X2lkLFxyXG5cdFx0XHRcdFx0XHRmaWx0ZXJzXHJcblx0XHRcdFx0XHR9LFxyXG5cdFx0XHRcdFx0ZGF0YVR5cGU6ICdqc29uJyxcclxuXHRcdFx0XHRcdHN1Y2Nlc3M6IGZ1bmN0aW9uKHJlc3BvbnNlKSB7XHJcblx0XHRcdFx0XHRcdGlmIChyZXNwb25zZS5zdWNjZXNzKSB7XHJcblx0XHRcdFx0XHRcdFx0JGNvdW50ZXIuZmluZCgnLmNvdW50JykudGV4dChyZXNwb25zZS5jb3VudCk7XHJcblx0XHRcdFx0XHRcdFx0JGNvdW50ZXIuZmluZCgnLnN1bW1hcnktc3VibWl0JykuZGF0YSgndXJsJywgcmVzcG9uc2UudXJsKTtcclxuXHJcblx0XHRcdFx0XHRcdFx0aWYoJCh3aW5kb3cpLndpZHRoKCkgID4gMTAyMykge1xyXG5cdFx0XHRcdFx0XHRcdFx0JGNvdW50ZXIuY3NzKHtcclxuXHRcdFx0XHRcdFx0XHRcdFx0J2xlZnQnOiBwYXJzZUludChwb3NpdGlvbi5sZWZ0ICsgJGlucHV0LndpZHRoKCkpIC0gJGNvdW50ZXIud2lkdGgoKSAvIDIsXHJcblx0XHRcdFx0XHRcdFx0XHRcdCd0b3AnOiBwb3NpdGlvbi50b3AsXHJcblx0XHRcdFx0XHRcdFx0XHRcdCdkaXNwbGF5JzogJ2Jsb2NrJ1xyXG5cdFx0XHRcdFx0XHRcdFx0fSk7XHJcblx0XHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0fSk7XHJcblx0XHRcdH1cclxuXHRcdH0pO1xyXG5cclxuXHRcdCRtaW5Db3N0LmNoYW5nZShmdW5jdGlvbiAoKSB7XHJcblx0XHRcdGxldCBtaW5WYWx1ZSA9ICQodGhpcykudmFsKCk7XHJcblx0XHRcdGxldCBtYXhWYWx1ZSA9ICRtYXhDb3N0LnZhbCgpO1xyXG5cclxuXHRcdFx0aWYgKHBhcnNlSW50KG1pblZhbHVlKSA+IHBhcnNlSW50KG1heFZhbHVlKSkge1xyXG5cdFx0XHRcdG1pblZhbHVlID0gbWF4VmFsdWU7XHJcblx0XHRcdFx0JG1pbkNvc3QudmFsKG1pblZhbHVlKTtcclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0JHJhbmdlU2xpZGVyLnNsaWRlcigndmFsdWVzJywgMCwgbWluVmFsdWUpO1xyXG5cdFx0fSk7XHJcblxyXG5cdFx0JG1heENvc3QuY2hhbmdlKGZ1bmN0aW9uICgpIHtcclxuXHRcdFx0bGV0IG1pblZhbHVlID0gJG1pbkNvc3QudmFsKCk7XHJcblx0XHRcdGxldCBtYXhWYWx1ZSA9ICQodGhpcykudmFsKCk7XHJcblxyXG5cdFx0XHRpZiAobWF4VmFsdWUgPiBtYXgpIHtcclxuXHRcdFx0XHRtYXhWYWx1ZSA9IG1heDtcclxuXHRcdFx0XHQkbWF4Q29zdC52YWwobWF4KTtcclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0aWYgKHBhcnNlSW50KG1pblZhbHVlKSA+IHBhcnNlSW50KG1heFZhbHVlKSkge1xyXG5cdFx0XHRcdG1heFZhbHVlID0gbWluVmFsdWU7XHJcblx0XHRcdFx0JG1heENvc3QudmFsKG1heFZhbHVlKTtcclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0JHJhbmdlU2xpZGVyLnNsaWRlcigndmFsdWVzJywgMSwgbWF4VmFsdWUpO1xyXG5cdFx0fSk7XHJcblxyXG5cdFx0JG1pbkNvc3Qua2V5cHJlc3MoZnVuY3Rpb24gKGV2ZW50KSB7XHJcblx0XHRcdGxldCBrZXk7XHJcblx0XHRcdGxldCBrZXlDaGFyO1xyXG5cclxuXHRcdFx0aWYgKGV2ZW50LmtleUNvZGUpIHtcclxuXHRcdFx0XHRrZXkgPSBldmVudC5rZXlDb2RlO1xyXG5cdFx0XHR9IGVsc2UgaWYgKGV2ZW50LndoaWNoKSB7XHJcblx0XHRcdFx0a2V5ID0gZXZlbnQud2hpY2g7XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdGlmIChrZXkgPT09IG51bGwgfHwga2V5ID09PSAwIHx8IGtleSA9PT0gOCB8fCBrZXkgPT09IDEzIHx8IGtleSA9PT0gOSB8fCBrZXkgPT09IDQ2IHx8IGtleSA9PT0gMzcgfHwga2V5ID09PSAzOSkge1xyXG5cdFx0XHRcdHJldHVybiB0cnVlO1xyXG5cdFx0XHR9XHJcblx0XHRcdGtleUNoYXIgPSBTdHJpbmcuZnJvbUNoYXJDb2RlKGtleSk7XHJcblxyXG5cdFx0XHRpZiAoIS9eWzAtOV0rJC8udGVzdChrZXlDaGFyKSkge1xyXG5cdFx0XHRcdHJldHVybiBmYWxzZTtcclxuXHRcdFx0fVxyXG5cdFx0fSk7XHJcblxyXG5cdFx0JG1heENvc3Qua2V5cHJlc3MoZnVuY3Rpb24gKGV2ZW50KSB7XHJcblx0XHRcdGxldCBrZXk7XHJcblx0XHRcdGxldCBrZXlDaGFyO1xyXG5cclxuXHRcdFx0aWYgKGV2ZW50LmtleUNvZGUpIHtcclxuXHRcdFx0XHRrZXkgPSBldmVudC5rZXlDb2RlO1xyXG5cdFx0XHR9IGVsc2UgaWYgKGV2ZW50LndoaWNoKSB7XHJcblx0XHRcdFx0a2V5ID0gZXZlbnQud2hpY2g7XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdGlmIChrZXkgPT09IG51bGwgfHwga2V5ID09PSAwIHx8IGtleSA9PT0gOCB8fCBrZXkgPT09IDEzIHx8IGtleSA9PT0gOSB8fCBrZXkgPT09IDQ2IHx8IGtleSA9PT0gMzcgfHwga2V5ID09PSAzOSkge1xyXG5cdFx0XHRcdHJldHVybiB0cnVlO1xyXG5cdFx0XHR9XHJcblx0XHRcdGtleUNoYXIgPSBTdHJpbmcuZnJvbUNoYXJDb2RlKGtleSk7XHJcblxyXG5cdFx0XHRpZiAoIS9eWzAtOV0rJC8udGVzdChrZXlDaGFyKSkge1xyXG5cdFx0XHRcdHJldHVybiBmYWxzZTtcclxuXHRcdFx0fVxyXG5cdFx0fSk7XHJcblx0fSk7XHJcbn1cclxuXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuLy8gRXhwb3J0c1xyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG5leHBvcnQge2luaXRSYW5nZVNsaWRlcn07XHJcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL19IVE1ML3NyYy9qcy9fbW9kdWxlcy9yYW5nZS1zbGlkZXIuanMiLCIndXNlIHN0cmljdCc7XG5cbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbi8vIFB1YmxpY1xuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXG5mdW5jdGlvbiBzdWJNZW51VG9nZ2xlIChpdGVtLCB3cmFwLCBzbGlkZSwgYnRuKSB7XG5cdGxldCAkd3JhcDtcblx0bGV0ICRvcGVuV3JhcCA9ICQod3JhcCArICcub3BlbicpO1xuXHRsZXQgJHRoaXM7XG5cblx0aWYgKCRvcGVuV3JhcCkge1xuXHRcdCRvcGVuV3JhcC5maW5kKHNsaWRlKS5zbGlkZURvd24oJ2Zhc3QnKTtcblx0fVxuXG5cdGlmIChidG4pIHtcblx0XHQkKGl0ZW0pLmZpbmQoYnRuKS5jbGljayhmdW5jdGlvbiAoZSkge1xuXHRcdFx0JHRoaXMgPSAkKHRoaXMpO1xuXHRcdFx0JHdyYXAgPSAkdGhpcy5jbG9zZXN0KCQod3JhcCkpO1xuXG5cdFx0XHRpZiAoJHdyYXAuZmluZChzbGlkZSkubGVuZ3RoKSB7XG5cdFx0XHRcdGUucHJldmVudERlZmF1bHQoKTtcblxuXHRcdFx0XHQkd3JhcC5maW5kKHNsaWRlKS5zbGlkZVRvZ2dsZSgnZmFzdCcpO1xuXHRcdFx0XHQkd3JhcC50b2dnbGVDbGFzcygnb3BlbicpO1xuXHRcdFx0XHQkdGhpcy5jbG9zZXN0KGl0ZW0pLnRvZ2dsZUNsYXNzKCdvcGVuJyk7XG5cdFx0XHR9XG5cdFx0fSk7XG5cdH0gZWxzZSB7XG5cdFx0JChpdGVtKS5jbGljayhmdW5jdGlvbiAoZSkge1xuXHRcdFx0JHRoaXMgPSAkKHRoaXMpO1xuXHRcdFx0JHdyYXAgPSAkdGhpcy5jbG9zZXN0KCQod3JhcCkpO1xuXG5cdFx0XHRpZiAoJHdyYXAuZmluZChzbGlkZSkubGVuZ3RoKSB7XG5cdFx0XHRcdGUucHJldmVudERlZmF1bHQoKTtcblxuXHRcdFx0XHQkd3JhcC5maW5kKHNsaWRlKS5zbGlkZVRvZ2dsZSgnZmFzdCcpO1xuXHRcdFx0XHQkd3JhcC50b2dnbGVDbGFzcygnb3BlbicpO1xuXHRcdFx0XHQkdGhpcy50b2dnbGVDbGFzcygnb3BlbicpO1xuXHRcdFx0fVxuXHRcdH0pO1xuXHR9XG59XG5cbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbi8vIEV4cG9ydHNcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblxuZXhwb3J0IHtzdWJNZW51VG9nZ2xlfTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL19IVE1ML3NyYy9qcy9fbW9kdWxlcy9zdWItbWVudS5qcyIsIid1c2Ugc3RyaWN0JztcclxuXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuLy8gSW1wb3J0c1xyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG5pbXBvcnQgUHJlbG9hZGVyIGZyb20gJyMvX21vZHVsZXMvUHJlbG9hZGVyJztcclxuaW1wb3J0IG1vZHVsZUxvYWRlciBmcm9tIFwiIy9tb2R1bGUtbG9hZGVyXCI7XHJcbmltcG9ydCAnIy9fdmVuZG9ycy9qcXVlcnkuY29va2llJ1xyXG5pbXBvcnQge3Byb2R1Y3RJbWdTbGlkZXJ9IGZyb20gJyMvX21vZHVsZXMvc2xpZGVycyc7XHJcbmltcG9ydCB7aW5pdFNlbGVjdH0gZnJvbSBcIi4vc2VsZWN0XCI7XHJcblxyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbi8vIFByaXZhdGVcclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxubGV0ICRjYXJ0SW5mb1JhZGlvID0gJCgnLmNhcnQtaW5mb19fcmFkaW8nKTtcclxubGV0ICRjYXJ0RGVsaXZlcnlDb250ZW50ID0gJCgnLmNhcnQtaW5mb19fZGVsaXZlcnktY29udGVudCcpO1xyXG4vLyBsZXQgJHByb2R1Y3RNaW51cyA9ICQoJy5qcy1wcm9kdWN0LW1pbnVzJyk7XHJcbi8vIGxldCAkcHJvZHVjdFBsdXMgPSAkKCcuanMtcHJvZHVjdC1wbHVzJyk7XHJcblxyXG4vLyBUaG91c2FuZCBzZXBhcmF0b3JcclxuXHJcbmZ1bmN0aW9uIHNldFRob3VzYW5kcyhudW1iZXJUZXh0LCBuZXdTZXBhcmF0b3IsIHNlcGFyYXRvcikge1xyXG5cdG5ld1NlcGFyYXRvciA9IG5ld1NlcGFyYXRvciB8fCAnLic7XHJcblx0c2VwYXJhdG9yID0gc2VwYXJhdG9yIHx8ICcuJztcclxuXHRudW1iZXJUZXh0ID0gJycgKyBudW1iZXJUZXh0O1xyXG5cdG51bWJlclRleHQgPSBudW1iZXJUZXh0LnNwbGl0KHNlcGFyYXRvcik7XHJcblxyXG5cdGxldCBudW1iZXJQZW5ueSA9IG51bWJlclRleHRbMV0gfHwgJyc7XHJcblx0bGV0IG51bWJlclZhbHVlID0gbnVtYmVyVGV4dFswXTtcclxuXHJcblx0bGV0IHRob3VzYW5kc1ZhbHVlID0gW107XHJcblx0bGV0IGNvdW50ZXIgPSAwO1xyXG5cclxuXHRmb3IgKGxldCBpID0gbnVtYmVyVmFsdWUubGVuZ3RoIC0gMTsgaSA+PSAwOyBpLS0pIHtcclxuXHRcdGxldCBudW0gPSBudW1iZXJWYWx1ZVtpXTtcclxuXHRcdHRob3VzYW5kc1ZhbHVlLnB1c2gobnVtKTtcclxuXHJcblx0XHRpZiAoKytjb3VudGVyID09PSAzICYmIGkpIHtcclxuXHRcdFx0dGhvdXNhbmRzVmFsdWUucHVzaCgnICcpO1xyXG5cdFx0XHRjb3VudGVyID0gMDtcclxuXHRcdH1cclxuXHR9XHJcblxyXG5cdHRob3VzYW5kc1ZhbHVlID0gdGhvdXNhbmRzVmFsdWUucmV2ZXJzZSgpLmpvaW4oJycpO1xyXG5cdGlmIChudW1iZXJQZW5ueS5sZW5ndGgpIHtcclxuXHRcdHJldHVybiBbdGhvdXNhbmRzVmFsdWUsIG51bWJlclBlbm55XS5qb2luKG5ld1NlcGFyYXRvcik7XHJcblx0fVxyXG5cdHJldHVybiB0aG91c2FuZHNWYWx1ZTtcclxufVxyXG5cclxuZnVuY3Rpb24gdG9nZ2xlKHRoYXQpIHtcclxuXHRsZXQgX3RoaXMgPSB0aGF0O1xyXG5cclxuXHQkY2FydERlbGl2ZXJ5Q29udGVudC5zbGlkZVVwKCdmYXN0Jyk7XHJcblxyXG5cdGlmICgkKF90aGlzKS5wcm9wKCdjaGVja2VkJykpIHtcclxuXHRcdGxldCAkY29udGVudCA9ICQoX3RoaXMpLnBhcmVudCgpLm5leHQoJy5jYXJ0LWluZm9fX2RlbGl2ZXJ5LWNvbnRlbnQnKTtcclxuXHJcblx0XHQkY29udGVudC5zbGlkZURvd24oJ2Zhc3QnKTtcclxuXHR9XHJcbn1cclxuXHJcbmZ1bmN0aW9uIGNvdW50ZXJVcGRhdGUoJGl0ZW0sIHZhbCkge1xyXG5cdGxldCAkaW5wdXQgPSAkaXRlbS5maW5kKCcuanMtcHJvZHVjdC1pbnB1dCcpO1xyXG5cdGxldCAkY291bnQgPSAkaXRlbS5maW5kKCcuanMtcHJvZHVjdC1jb3VudCcpO1xyXG5cdGxldCAkY291bnRJdGVtID0gJGl0ZW0uZmluZCgnLmpzLXByb2R1Y3QtY291bnQtaXRlbScpO1xyXG5cdGxldCAkcHJpY2VBbGwgPSAkaXRlbS5maW5kKCcuanMtcHJvZHVjdC1wcmljZScpO1xyXG5cdGxldCAkY291bnRBbGwgPSAkaXRlbS5maW5kKCcuanMtcHJvZHVjdC1jb3VudC1hbGwnKTtcclxuXHJcblx0bGV0IHdob2xlc2FsZSA9ICRpdGVtLmhhc0NsYXNzKCd3aG9sZXNhbGUnKTtcclxuXHRsZXQgbWF4Q291bnRBbGwgPSBwYXJzZUludCgkaW5wdXQuZGF0YSgnbWF4JykpO1xyXG5cdGxldCBjb3VudE9sZEFsbCA9ICQuaXNOdW1lcmljKCRpbnB1dC52YWwoKSkgPyBwYXJzZUludCgkaW5wdXQudmFsKCkpIDogMTtcclxuXHRsZXQgY29zdCA9IHBhcnNlRmxvYXQoJHByaWNlQWxsLmRhdGEoJ3ByaWNlJykpO1xyXG5cdGxldCBjb3VudCA9IDA7XHJcblx0bGV0IGNvdW50TmV3QWxsID0gY291bnRPbGRBbGw7XHJcblxyXG5cdCQoJy5qcy1wcm9kdWN0LXBsdXMnKS5vbignY2xpY2snLCBmdW5jdGlvbiAoKSB7XHJcblx0XHRsZXQgdGhpc1BsdXMgPSAkKHRoaXMpLnNpYmxpbmdzKCcuanMtcHJvZHVjdC1pbnB1dCcpO1xyXG5cclxuXHRcdGlmICh0aGlzUGx1cy52YWwoKSA9PSB0aGlzUGx1cy5kYXRhKCdtYXgnKSkge1xyXG5cdFx0XHRsZXQgbWF4Tm90eSA9ICQoJy5tYXhpbXVtLW5vdHknKTtcclxuXHRcdFx0bWF4Tm90eS5zaG93KCk7XHJcblxyXG5cdFx0XHRzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcclxuXHRcdFx0XHRtYXhOb3R5LmhpZGUoKTtcclxuXHRcdFx0fSwgMjAwMClcclxuXHRcdH1cclxuXHR9KVxyXG5cclxuXHRpZiAoJGNvdW50Lmxlbmd0aCkge1xyXG5cdFx0bGV0IG1pbkNvdW50QWxsID0gKHdob2xlc2FsZSAmJiAkY291bnQubGVuZ3RoKSA/ICRjb3VudC5sZW5ndGggOiAxO1xyXG5cdFx0bGV0IGNvdW50cyA9IFtdO1xyXG5cclxuXHRcdCRjb3VudC5lYWNoKGZ1bmN0aW9uICgpIHtcclxuXHRcdFx0bGV0ICRjb3VudEl0ZW0gPSAkKHRoaXMpO1xyXG5cdFx0XHRsZXQgY291bnRPbGQgPSBwYXJzZUludCgkY291bnRJdGVtLnRleHQoKSk7XHJcblx0XHRcdGxldCBtYXhDb3VudCA9IHBhcnNlSW50KCRjb3VudEl0ZW0uZGF0YSgnbWF4JykpO1xyXG5cdFx0XHRsZXQgY291bnROZXcgPSBjb3VudE9sZDtcclxuXHJcblx0XHRcdGlmICh2YWwgPT09ICdwbHVzJykge1xyXG5cdFx0XHRcdGNvdW50TmV3ID0gY291bnRPbGQgPCBtYXhDb3VudCA/IChjb3VudE9sZCArIDEpIDogbWF4Q291bnQ7XHJcblx0XHRcdH0gZWxzZSBpZiAodmFsID09PSAnbWludXMnKSB7XHJcblx0XHRcdFx0Y291bnROZXcgPSBjb3VudE9sZCA+IDEgPyAoY291bnRPbGQgLSAxKSA6IChtYXhDb3VudCA/IDEgOiAwKTtcclxuXHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHRjb3VudE5ldyA9IChjb3VudE9sZEFsbCA+IG1heENvdW50KSA/IG1heENvdW50IDogKChjb3VudE9sZEFsbCA8IDEpID8gMSA6IGNvdW50T2xkQWxsKTtcclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0Y291bnQgKz0gY291bnROZXc7XHJcblxyXG5cdFx0XHRjb3VudHMucHVzaChjb3VudE5ldyArICcnKTtcclxuXHJcblx0XHRcdCRjb3VudEl0ZW0uaHRtbChjb3VudE5ldyA/IGNvdW50TmV3IDogJycpOyAvLyBlc2xpbnQtZGlzYWJsZS1saW5lIG5vLXVubmVlZGVkLXRlcm5hcnlcclxuXHRcdH0pO1xyXG5cclxuXHRcdCRjb3VudEl0ZW0uZWFjaChmdW5jdGlvbiAoKSB7XHJcblx0XHRcdGxldCAkY291bnRJdGVtID0gJCh0aGlzKTtcclxuXHRcdFx0bGV0IHBhcmFtID0gJGNvdW50SXRlbS5kYXRhKCdwYXJhbScpO1xyXG5cdFx0XHRwYXJhbS5wYXJhbS5jb3VudHMgPSBjb3VudHM7XHJcblx0XHRcdCRjb3VudEl0ZW0uZGF0YSgncGFyYW0nLCBwYXJhbSk7XHJcblx0XHR9KTtcclxuXHJcblx0XHRpZiAodmFsID09PSAncGx1cycpIHtcclxuXHRcdFx0Y291bnROZXdBbGwgPSBjb3VudE9sZEFsbCA8IG1heENvdW50QWxsID8gKGNvdW50T2xkQWxsICsgMSkgOiBtYXhDb3VudEFsbDtcclxuXHRcdH0gZWxzZSBpZiAodmFsID09PSAnbWludXMnKSB7XHJcblx0XHRcdGNvdW50TmV3QWxsID0gY291bnRPbGRBbGwgPiAxID8gKGNvdW50T2xkQWxsIC0gMSkgOiBtaW5Db3VudEFsbDtcclxuXHRcdH0gZWxzZSB7XHJcblx0XHRcdGNvdW50TmV3QWxsID0gKGNvdW50T2xkQWxsID4gbWF4Q291bnRBbGwpID8gbWF4Q291bnRBbGwgOiAoKGNvdW50T2xkQWxsIDwgMSkgPyBtaW5Db3VudEFsbCA6IGNvdW50T2xkQWxsKTtcclxuXHRcdH1cclxuXHJcblx0XHQkaW5wdXQudmFsKGNvdW50KTtcclxuXHJcblx0XHQkaXRlbS5kYXRhKCdjb3VudHMnLCBjb3VudHMpO1xyXG5cdFx0JGl0ZW0uZGF0YSgnY291bnQnLCBjb3VudE5ld0FsbCk7XHJcblxyXG5cdFx0JHByaWNlQWxsLmh0bWwoc2V0VGhvdXNhbmRzKChjb3VudCAqIGNvc3QpLnRvRml4ZWQoMikpKTtcclxuXHRcdCRjb3VudEFsbC5odG1sKGNvdW50KTtcclxuXHR9IGVsc2Uge1xyXG5cdFx0aWYgKHdob2xlc2FsZSkge1xyXG5cdFx0XHQkaXRlbS5jbG9zZXN0KCdmb3JtJykuZmluZCgnLmpzLXByb2R1Y3QtaW5wdXQnKS5lYWNoKGZ1bmN0aW9uICgpIHtcclxuXHRcdFx0XHRsZXQgJHRoID0gJCh0aGlzKTtcclxuXHJcblx0XHRcdFx0bGV0IG1heENvdW50SXRlbSA9IHBhcnNlSW50KCR0aC5kYXRhKCdtYXgnKSk7XHJcblx0XHRcdFx0bGV0IGNvdW50TmV3SXRlbSA9IGNvdW50T2xkQWxsO1xyXG5cdFx0XHRcdGxldCBjb3VudE5vdyA9ICR0aC52YWwoKTtcclxuXHJcblx0XHRcdFx0aWYgKHZhbCA9PT0gJ3BsdXMnKSB7XHJcblx0XHRcdFx0XHRjb3VudE5ld0l0ZW0gPSArK2NvdW50Tm93IDwgbWF4Q291bnRJdGVtID8gY291bnROb3cgOiBtYXhDb3VudEl0ZW07XHJcblx0XHRcdFx0fSBlbHNlIGlmICh2YWwgPT09ICdtaW51cycpIHtcclxuXHRcdFx0XHRcdGNvdW50TmV3SXRlbSA9IC0tY291bnROb3cgPiAwID8gY291bnROb3cgOiAwO1xyXG5cdFx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0XHRjb3VudE5ld0l0ZW0gPSAoY291bnRPbGRBbGwgPiBtYXhDb3VudEl0ZW0pID8gbWF4Q291bnRJdGVtIDogKChjb3VudE9sZEFsbCA8IDApID8gMCA6IGNvdW50T2xkQWxsKTtcclxuXHRcdFx0XHR9XHJcblxyXG5cdFx0XHRcdCR0aC52YWwoY291bnROZXdJdGVtKTtcclxuXHRcdFx0fSk7XHJcblx0XHR9XHJcblxyXG5cdFx0aWYgKHZhbCA9PT0gJ3BsdXMnKSB7XHJcblx0XHRcdGNvdW50TmV3QWxsID0gY291bnRPbGRBbGwgPCBtYXhDb3VudEFsbCA/IChjb3VudE9sZEFsbCArIDEpIDogbWF4Q291bnRBbGw7XHJcblx0XHR9IGVsc2UgaWYgKHZhbCA9PT0gJ21pbnVzJykge1xyXG5cdFx0XHRjb3VudE5ld0FsbCA9IGNvdW50T2xkQWxsID4gMCA/IChjb3VudE9sZEFsbCAtIDEpIDogMDtcclxuXHRcdH0gZWxzZSB7XHJcblx0XHRcdGNvdW50TmV3QWxsID0gKGNvdW50T2xkQWxsID4gbWF4Q291bnRBbGwpID8gbWF4Q291bnRBbGwgOiAoKGNvdW50T2xkQWxsIDwgMCkgPyAwIDogY291bnRPbGRBbGwpO1xyXG5cdFx0fVxyXG5cclxuXHRcdCRpbnB1dC52YWwoY291bnROZXdBbGwpO1xyXG5cclxuXHRcdCRpdGVtLmRhdGEoJ2NvdW50cycsIGNvdW50TmV3QWxsKTtcclxuXHRcdCRpdGVtLmRhdGEoJ2NvdW50JywgY291bnROZXdBbGwpO1xyXG5cclxuXHRcdCRwcmljZUFsbC5odG1sKHNldFRob3VzYW5kcygoY291bnROZXdBbGwgKiBjb3N0KS50b0ZpeGVkKDIpKSk7XHJcblx0XHQkY291bnRBbGwuaHRtbChjb3VudE5ld0FsbCk7XHJcblx0fVxyXG5cclxuXHRpZiAoJGlucHV0LnZhbCgpID4gMCkge1xyXG5cdFx0JCgnYm9keScpLm9uKCdjbGljaycsICcuanMtYWNjZXB0JywgZnVuY3Rpb24gKCkge1xyXG5cdFx0XHRzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcclxuXHRcdFx0XHQkKCcuanMtYWNjZXB0JykuY2xvc2VzdCgnLnNpemVzLXBvcHVwJykuZmluZCgnLm1mcC1jbG9zZScpLnRyaWdnZXIoJ2NsaWNrJyk7XHJcblx0XHRcdFx0JCgnLmhlYWRlci1jYXJ0X19idG4uanMtY2FydC1vcGVuJykudHJpZ2dlcignY2xpY2snKTtcclxuXHRcdFx0fSwgMTAwMClcclxuXHRcdH0pXHJcblx0fVxyXG59XHJcblxyXG4kKCdib2R5Jykub24oJ2NsaWNrIGNoYW5nZScsICcuanMtZm9ybS10cmlnZ2VyJywgZnVuY3Rpb24gKCkge1xyXG5cdGxldCAkdGhpcyA9ICQodGhpcyk7XHJcblx0bGV0ICRpdGVtID0gJHRoaXMuY2xvc2VzdCgnLmpzLXByb2R1Y3QtaXRlbScpO1xyXG5cdGxldCAkaXRlbUlucHV0ID0gJy5qcy1wcm9kdWN0LWlucHV0JztcclxuXHJcblx0aWYgKCR0aGlzLmhhc0NsYXNzKCdqcy1wcm9kdWN0LW1pbnVzJykpIHtcclxuXHRcdGNvdW50ZXJVcGRhdGUoJGl0ZW0sICdtaW51cycpO1xyXG5cdFx0JHRoaXMuY2xvc2VzdCgnZm9ybScpLnN1Ym1pdCgpO1xyXG5cdH1cclxuXHJcblx0aWYgKCR0aGlzLmhhc0NsYXNzKCdqcy1wcm9kdWN0LXBsdXMnKSkge1xyXG5cclxuXHRcdGlmICgkdGhpcy5zaWJsaW5ncygkaXRlbUlucHV0KS52YWwoKSA9PSAkdGhpcy5zaWJsaW5ncygkaXRlbUlucHV0KS5kYXRhKCdtYXgnKSkge1xyXG5cclxuXHRcdFx0bGV0IG1heE5vdHkgPSAkKCcubWF4aW11bS1ub3R5Jyk7XHJcblx0XHRcdG1heE5vdHkuc2hvdygpO1xyXG5cclxuXHRcdFx0Y291bnRlclVwZGF0ZSgkaXRlbSwgZmFsc2UpO1xyXG5cclxuXHRcdFx0c2V0VGltZW91dChmdW5jdGlvbiAoKSB7XHJcblx0XHRcdFx0bWF4Tm90eS5oaWRlKCk7XHJcblx0XHRcdH0sIDIwMDApXHJcblxyXG5cdFx0fSBlbHNlIHtcclxuXHRcdFx0Y291bnRlclVwZGF0ZSgkaXRlbSwgJ3BsdXMnKTtcclxuXHRcdFx0JHRoaXMuY2xvc2VzdCgnZm9ybScpLnN1Ym1pdCgpO1xyXG5cdFx0fVxyXG5cclxuXHR9XHJcblxyXG59KTtcclxuXHJcbiQoJy5jaGlsZC1jcmVhdGlvbl9fYnV0dG9uJykub24oJ2NsaWNrJywgZnVuY3Rpb24gKCl7XHJcblx0bGV0IHdyYXBwZXIgPSAkKCcuY2hpbGQtc2VsZWN0aW9uX193cmFwJyk7XHJcblx0bGV0IGJ0biA9ICQoJy5jaGlsZC1jcmVhdGlvbl9fYnV0dG9uJyk7XHJcblxyXG5cdGlmICh3cmFwcGVyLmhhc0NsYXNzKCdhY3RpdmUnKSkge1xyXG5cdFx0d3JhcHBlci5yZW1vdmVDbGFzcygnYWN0aXZlJyk7XHJcblx0XHR3cmFwcGVyLnNsaWRlVXAoNTAwKTtcclxuXHRcdGJ0bi5jc3MoJ2JvcmRlci1jb2xvcicsICcjYTFhMWExJyk7XHJcblx0fSBlbHNlIHtcclxuXHRcdHdyYXBwZXIuYWRkQ2xhc3MoJ2FjdGl2ZScpO1xyXG5cdFx0d3JhcHBlci5zbGlkZURvd24oNTAwKTtcclxuXHRcdGJ0bi5jc3MoJ2JvcmRlci1jb2xvcicsICcjZjhkNGNkJyk7XHJcblxyXG5cclxuXHRcdGluaXRTZWxlY3QoKTtcclxuXHJcblx0XHQkKCcuc2VsZWN0aW9uLXNob3cnKS5maW5kKCdzZWxlY3QnKS5lYWNoKGZ1bmN0aW9uIChpLGUpe1xyXG5cdFx0XHQkKGUpLnNlbGVjdDIoe1xyXG5cdFx0XHRcdG1pbmltdW1SZXN1bHRzRm9yU2VhcmNoOiBJbmZpbml0eSxcclxuXHRcdFx0XHRkcm9wZG93blBhcmVudDogJChlKS5jbG9zZXN0KCcubWZwLWNvbnRhaW5lcicpLFxyXG5cdFx0XHRcdHdpZHRoOiAncmVzb2x2ZScsXHJcblx0XHRcdFx0bGFuZ3VhZ2U6ICQoJ2h0bWwnKS5hdHRyKCdsYW5nJyksXHJcblx0XHRcdFx0cG9zaXRpb25Ecm9wZG93bjogZmFsc2VcclxuXHRcdFx0fSk7XHJcblx0XHR9KVxyXG5cdH1cclxufSk7XHJcblxyXG4kLm1hZ25pZmljUG9wdXAuaW5zdGFuY2UuY2xvc2UgPSBmdW5jdGlvbiAoKSB7XHJcblx0JCgnLmNoaWxkLXNlbGVjdGlvbl9fd3JhcCcpLnJlbW92ZUNsYXNzKCdhY3RpdmUnKTtcclxuXHQkKCcuY2hpbGQtY3JlYXRpb25fX2J1dHRvbicpLmNzcygnYm9yZGVyLWNvbG9yJywgJyNhMWExYTEnKTtcclxuXHQkLm1hZ25pZmljUG9wdXAucHJvdG8uY2xvc2UuY2FsbCh0aGlzKTtcclxufTtcclxuXHJcbiQoJ2JvZHknKS5vbignY2xpY2snLCAnLmNoaWxkLXNlbGVjdGlvbl9fdGFiJywgZnVuY3Rpb24gKCl7XHJcblx0bGV0IGNvbnRlbnRJZCA9ICQodGhpcykuYXR0cignZGF0YS1pZCcpO1xyXG5cdGxldCBjb250ZW50ID0gJCh0aGlzKS5wYXJlbnRzKCcuY2hpbGQtc2VsZWN0aW9uX193cmFwJykuZmluZChcIltkYXRhLWNvbnRlbnQtaWQ9J1wiICsgY29udGVudElkICsgXCInXVwiKTtcclxuXHJcblx0JCh0aGlzKS5wYXJlbnRzKCcuY2hpbGQtc2VsZWN0aW9uX19nZW5kZXInKS5maW5kKCcuYWN0aXZlJykucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpO1xyXG5cdCQodGhpcykuYWRkQ2xhc3MoJ2FjdGl2ZScpO1xyXG5cdCQoJy5jaGlsZC1hZ2UnKS5yZW1vdmVDbGFzcygnaXMtYWN0aXZlJyk7XHJcblx0Y29udGVudC5hZGRDbGFzcygnaXMtYWN0aXZlJyk7XHJcblxyXG5cdGluaXRTZWxlY3QoKTtcclxuXHJcblx0JCgnLnNlbGVjdGlvbi1zaG93JykuZmluZCgnc2VsZWN0JykuZWFjaChmdW5jdGlvbiAoaSxlKXtcclxuXHRcdCQoZSkuc2VsZWN0Mih7XHJcblx0XHRcdG1pbmltdW1SZXN1bHRzRm9yU2VhcmNoOiBJbmZpbml0eSxcclxuXHRcdFx0ZHJvcGRvd25QYXJlbnQ6ICQoZSkuY2xvc2VzdCgnLm1mcC1jb250YWluZXInKSxcclxuXHRcdFx0d2lkdGg6ICdyZXNvbHZlJyxcclxuXHRcdFx0bGFuZ3VhZ2U6ICQoJ2h0bWwnKS5hdHRyKCdsYW5nJyksXHJcblx0XHRcdHBvc2l0aW9uRHJvcGRvd246IGZhbHNlXHJcblx0XHR9KTtcclxuXHR9KVxyXG59KTtcclxuXHJcbiQoJ2JvZHknKS5vbignY2hhbmdlJywgJy5jaGlsZC1zZWxlY3Rpb25fX2NvbnRlbnQnLCBmdW5jdGlvbiAoZSl7XHJcblx0ZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG5cclxuXHRsZXQgJGZpbHRlcldyYXBwZXIgPSAkKHRoaXMpLnBhcmVudHMoJy5zZWN0aW9uJykuZmluZCgnLm1haW4tZmlsdGVyLXdyYXBwZXInKTtcclxuXHJcblx0aWYgKCQod2luZG93KS53aWR0aCgpIDwgMTAyNCkge1xyXG5cdFx0JGZpbHRlcldyYXBwZXIgPSAkKHRoaXMpLnBhcmVudHMoJ2JvZHknKS5maW5kKCcuY2F0YWxvZ19fZmlsdGVyLS1tb2JpbGUnKTtcclxuXHR9XHJcblxyXG5cdGxldCBjYXRlZ29yeV9pZCA9ICRmaWx0ZXJXcmFwcGVyLmRhdGEoJ2NhdGVnb3J5Jyk7XHJcblx0bGV0IGZpbHRlcnMgPSB7fTtcclxuXHJcblx0JGZpbHRlcldyYXBwZXIuZmluZCgnLmZpbHRlci1pbnB1dDpjaGVja2VkJykuZWFjaChmdW5jdGlvbigpIHtcclxuXHRcdGxldCBuYW1lID0gJCh0aGlzKS5hdHRyKCduYW1lJyk7XHJcblx0XHRpZiAoIWZpbHRlcnMuaGFzT3duUHJvcGVydHkobmFtZSkpIGZpbHRlcnNbbmFtZV0gPSBbXTtcclxuXHJcblx0XHRsZXQgdmFsdWUgPSAkKHRoaXMpLnZhbCgpO1xyXG5cclxuXHRcdGlmICghZmlsdGVyc1tuYW1lXS5pbmNsdWRlcyh2YWx1ZSkpIGZpbHRlcnNbbmFtZV0ucHVzaCh2YWx1ZSk7XHJcblx0fSk7XHJcblxyXG5cdGxldCAkbWluUmFuZ2VJbnB1dCA9ICRmaWx0ZXJXcmFwcGVyLmZpbmQoJy5taW4tY29zdCcpO1xyXG5cdGxldCAkbWF4UmFuZ2VJbnB1dCA9ICRmaWx0ZXJXcmFwcGVyLmZpbmQoJy5tYXgtY29zdCcpO1xyXG5cdGxldCBtaW5SYW5nZUNvc3QgPSAkbWluUmFuZ2VJbnB1dC52YWwoKTtcclxuXHRsZXQgbWF4UmFuZ2VDb3N0ID0gJG1heFJhbmdlSW5wdXQudmFsKCk7XHJcblx0bGV0IG1pblByaWNlID0gJG1pblJhbmdlSW5wdXQuZGF0YSgnbWluJyk7XHJcblx0bGV0IG1heFByaWNlID0gJG1heFJhbmdlSW5wdXQuZGF0YSgnbWF4Jyk7XHJcblx0aWYgKG1pblByaWNlIDwgbWluUmFuZ2VDb3N0IHx8IG1heFByaWNlID4gbWF4UmFuZ2VDb3N0KSB7XHJcblx0XHRmaWx0ZXJzWydtaW5jb3N0J10gPSBtaW5SYW5nZUNvc3Q7XHJcblx0XHRmaWx0ZXJzWydtYXhjb3N0J10gPSBtYXhSYW5nZUNvc3Q7XHJcblx0fVxyXG5cclxuXHRsZXQgc2V4ID0gJCgnLmNoaWxkLXNlbGVjdGlvbl9fdGFiLmFjdGl2ZScpLmF0dHIoJ2RhdGEtdmFsdWUnKTtcclxuXHRsZXQgYWdlID0gJCgnLmNoaWxkLWFnZS5pcy1hY3RpdmUnKS5maW5kKCcuYWdlLXNlbGVjdGlvbicpLnZhbCgpO1xyXG5cclxuXHRmaWx0ZXJzWydwb2xyZWJlbmthJ10gPSBbc2V4XTtcclxuXHRmaWx0ZXJzWydhZ2UnXSA9IFthZ2VdO1xyXG5cclxuXHRsZXQgc2VhcmNoID0gbnVsbDtcclxuXHRjb25zdCBwYXJhbXMgPSBuZXcgVVJMU2VhcmNoUGFyYW1zKHdpbmRvdy5sb2NhdGlvbi5zZWFyY2gpO1xyXG5cdGlmIChwYXJhbXMuaGFzKCdzZWFyY2gnKSkgc2VhcmNoID0gcGFyYW1zLmdldCgnc2VhcmNoJyk7XHJcblxyXG5cdCQuYWpheCh7XHJcblx0XHR0eXBlOiAncG9zdCcsXHJcblx0XHR1cmw6ICcvYWpheC9wcmVwYXJlRmlsdGVyJyxcclxuXHRcdGRhdGE6IHtcclxuXHRcdFx0Y2F0ZWdvcnlfaWQsXHJcblx0XHRcdGZpbHRlcnMsXHJcblx0XHRcdHNlYXJjaFxyXG5cdFx0fSxcclxuXHRcdGRhdGFUeXBlOiAnanNvbicsXHJcblx0XHRzdWNjZXNzOiBmdW5jdGlvbihyZXNwb25zZSkge1xyXG5cdFx0XHRpZiAocmVzcG9uc2Uuc3VjY2VzcyAmJiByZXNwb25zZS51cmwpIHtcclxuXHRcdFx0XHR3aW5kb3cubG9jYXRpb24uaHJlZiA9IHJlc3BvbnNlLnVybDtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdH0pO1xyXG59KTtcclxuXHJcbiQoJy5zZWNvbmRfX2Jsb2NrJykub24oJ2NsaWNrJywgZnVuY3Rpb24gKCl7XHJcblx0bGV0IHBhcmVudCA9ICQodGhpcykucGFyZW50cygnLmRlbGl2ZXJ5LWluZm9fX2l0ZW0nKTtcclxuXHRsZXQgY29udGVudCA9IHBhcmVudC5maW5kKCcuZGVsaXZlcnktaW5mb19fY29udGVudCcpO1xyXG5cdGxldCBzcGFuID0gcGFyZW50LmZpbmQoJy5vcGVuX2RlbGl2ZXJ5IHNwYW4nKTtcclxuXHJcblx0aWYgKGNvbnRlbnQuaGFzQ2xhc3MoJ25vbmVfX2Jsb2NrJykpe1xyXG5cdFx0Y29udGVudC5yZW1vdmVDbGFzcygnbm9uZV9fYmxvY2snKVxyXG5cdFx0Y29udGVudC5zbGlkZURvd24oNTAwKTtcclxuXHRcdCQodGhpcykuYWRkQ2xhc3MoJ29wZW5lZCcpO1xyXG5cdFx0c3Bhbi5odG1sKCctJyk7XHJcblx0fSBlbHNlIHtcclxuXHRcdGNvbnRlbnQuYWRkQ2xhc3MoJ25vbmVfX2Jsb2NrJyk7XHJcblx0XHRjb250ZW50LnNsaWRlVXAoNTAwKTtcclxuXHRcdCQodGhpcykucmVtb3ZlQ2xhc3MoJ29wZW5lZCcpO1xyXG5cdFx0c3Bhbi5odG1sKCcrJyk7XHJcblx0fVxyXG5cclxufSk7XHJcblxyXG4kKCcuZmlsdGVyc19faXRlbScpLm9uKCdjbGljaycsICdhJywgZnVuY3Rpb24oZXZlbnQpIHtcclxuXHJcblx0aWYgKCEkKHRoaXMpLmhhc0NsYXNzKCdmaWx0ZXJzX19jaGlsZC1uYW1lJykpIHtcclxuXHRcdGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XHJcblx0XHRpZiAoJCh0aGlzKS5oYXNDbGFzcygnYWN0aXZlJykpIHtcclxuXHRcdFx0JCh0aGlzKS5yZW1vdmVDbGFzcygnYWN0aXZlJylcclxuXHRcdH0gZWxzZSB7XHJcblx0XHRcdCQodGhpcykuYWRkQ2xhc3MoJ2FjdGl2ZScpO1xyXG5cdFx0fVxyXG5cclxuXHRcdGxldCAkZmlsdGVyV3JhcHBlciA9ICQodGhpcykucGFyZW50cygnLm1haW4tZmlsdGVyLXdyYXBwZXInKTtcclxuXHRcdGxldCAkY291bnRlciA9ICQoJy5maWx0ZXItc3VtbWFyeS13cmFwcGVyJyk7XHJcblx0XHRsZXQgJGlucHV0ID0gJCh0aGlzKS5wYXJlbnRzKCcuZmlsdGVyc19faXRlbScpO1xyXG5cdFx0bGV0IHBvc2l0aW9uID0gJGlucHV0Lm9mZnNldCgpO1xyXG5cclxuXHRcdGxldCBjYXRlZ29yeV9pZCA9ICRmaWx0ZXJXcmFwcGVyLmRhdGEoJ2NhdGVnb3J5Jyk7XHJcblx0XHRsZXQgZmlsdGVycyA9IHt9O1xyXG5cclxuXHRcdCRmaWx0ZXJXcmFwcGVyLmZpbmQoJy5hY3RpdmUnKS5lYWNoKGZ1bmN0aW9uICgpIHtcclxuXHRcdFx0bGV0IG5hbWUgPSAkKHRoaXMpLmF0dHIoJ2RhdGEtdHlwZScpO1xyXG5cdFx0XHRpZiAoIWZpbHRlcnMuaGFzT3duUHJvcGVydHkobmFtZSkpIGZpbHRlcnNbbmFtZV0gPSBbXTtcclxuXHJcblx0XHRcdGxldCB2YWx1ZSA9ICQodGhpcykuYXR0cignZGF0YS12YWx1ZScpO1xyXG5cclxuXHRcdFx0aWYgKCFmaWx0ZXJzW25hbWVdLmluY2x1ZGVzKHZhbHVlKSkgZmlsdGVyc1tuYW1lXS5wdXNoKHZhbHVlKTtcclxuXHRcdH0pO1xyXG5cclxuXHRcdGxldCAkbWluUmFuZ2VJbnB1dCA9ICRmaWx0ZXJXcmFwcGVyLmZpbmQoJy5taW4tY29zdCcpO1xyXG5cdFx0bGV0ICRtYXhSYW5nZUlucHV0ID0gJGZpbHRlcldyYXBwZXIuZmluZCgnLm1heC1jb3N0Jyk7XHJcblx0XHRsZXQgbWluUmFuZ2VDb3N0ID0gJG1pblJhbmdlSW5wdXQudmFsKCk7XHJcblx0XHRsZXQgbWF4UmFuZ2VDb3N0ID0gJG1heFJhbmdlSW5wdXQudmFsKCk7XHJcblx0XHRsZXQgbWluUHJpY2UgPSAkbWluUmFuZ2VJbnB1dC5kYXRhKCdtaW4nKTtcclxuXHRcdGxldCBtYXhQcmljZSA9ICRtYXhSYW5nZUlucHV0LmRhdGEoJ21heCcpO1xyXG5cdFx0aWYgKG1pblByaWNlIDwgbWluUmFuZ2VDb3N0IHx8IG1heFByaWNlID4gbWF4UmFuZ2VDb3N0KSB7XHJcblx0XHRcdGZpbHRlcnNbJ21pbmNvc3QnXSA9IG1pblJhbmdlQ29zdDtcclxuXHRcdFx0ZmlsdGVyc1snbWF4Y29zdCddID0gbWF4UmFuZ2VDb3N0O1xyXG5cdFx0fVxyXG5cclxuXHRcdGxldCBzZWFyY2ggPSBudWxsO1xyXG5cdFx0Y29uc3QgcGFyYW1zID0gbmV3IFVSTFNlYXJjaFBhcmFtcyh3aW5kb3cubG9jYXRpb24uc2VhcmNoKTtcclxuXHRcdGlmIChwYXJhbXMuaGFzKCdzZWFyY2gnKSkgc2VhcmNoID0gcGFyYW1zLmdldCgnc2VhcmNoJyk7XHJcblxyXG5cdFx0aWYgKCQuaXNFbXB0eU9iamVjdChmaWx0ZXJzKSAmJiAhJCgnLmNhdGFsb2dfX2Nob3Nlbi1pdGVtJykubGVuZ3RoKSB7XHJcblx0XHRcdCRjb3VudGVyLmNzcygnZGlzcGxheScsICdub25lJyk7XHJcblx0XHRcdHJldHVybjtcclxuXHRcdH1cclxuXHJcblx0XHQkLmFqYXgoe1xyXG5cdFx0XHR0eXBlOiAncG9zdCcsXHJcblx0XHRcdHVybDogJy9hamF4L3ByZXBhcmVGaWx0ZXInLFxyXG5cdFx0XHRkYXRhOiB7XHJcblx0XHRcdFx0Y2F0ZWdvcnlfaWQsXHJcblx0XHRcdFx0ZmlsdGVycyxcclxuXHRcdFx0XHRzZWFyY2hcclxuXHRcdFx0fSxcclxuXHRcdFx0ZGF0YVR5cGU6ICdqc29uJyxcclxuXHRcdFx0c3VjY2VzczogZnVuY3Rpb24gKHJlc3BvbnNlKSB7XHJcblx0XHRcdFx0aWYgKHJlc3BvbnNlLnN1Y2Nlc3MpIHtcclxuXHRcdFx0XHRcdCRjb3VudGVyLmZpbmQoJy5jb3VudCcpLnRleHQocmVzcG9uc2UuY291bnQpO1xyXG5cdFx0XHRcdFx0JGNvdW50ZXIuZmluZCgnLnN1bW1hcnktc3VibWl0JykuYXR0cignZGF0YS11cmwnLCByZXNwb25zZS51cmwpO1xyXG5cclxuXHRcdFx0XHRcdGlmICgkKHdpbmRvdykud2lkdGgoKSA+IDEwMjMpIHtcclxuXHRcdFx0XHRcdFx0JGNvdW50ZXIuY3NzKHtcclxuXHRcdFx0XHRcdFx0XHQnbGVmdCc6IHBhcnNlSW50KHBvc2l0aW9uLmxlZnQgKyAkaW5wdXQud2lkdGgoKSkgLSAkY291bnRlci53aWR0aCgpIC8gMixcclxuXHRcdFx0XHRcdFx0XHQndG9wJzogcG9zaXRpb24udG9wLFxyXG5cdFx0XHRcdFx0XHRcdCdkaXNwbGF5JzogJ2Jsb2NrJ1xyXG5cdFx0XHRcdFx0XHR9KTtcclxuXHRcdFx0XHRcdH1cclxuXHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9XHJcblx0XHR9KTtcclxuXHR9XHJcbn0pO1xyXG5cclxuJCgnLnN1bW1hcnktc3VibWl0Jykub24oJ2NsaWNrJywgZnVuY3Rpb24oKSB7XHJcblx0bG9jYXRpb24uaHJlZiA9ICQodGhpcykuZGF0YSgndXJsJyk7XHJcbn0pO1xyXG5cclxuJCgnc2VsZWN0W25hbWU9cGVyLXBhZ2VdJykub24oJ2NoYW5nZScsIGZ1bmN0aW9uKCkge1xyXG5cdGxldCBkZWZhdWx0TGltaXQgPSAkKHRoaXMpLmRhdGEoJ2RlZmF1bHRfbGltaXQnKTtcclxuXHRsZXQgcXVlcnlQYXJhbXMgPSBuZXcgVVJMU2VhcmNoUGFyYW1zKGxvY2F0aW9uLnNlYXJjaCk7XHJcblx0bGV0IGNvdW50ID0gJCh0aGlzKS52YWwoKTtcclxuXHJcblx0aWYgKGNvdW50ICE9IGRlZmF1bHRMaW1pdCkge1xyXG5cdFx0cXVlcnlQYXJhbXMuc2V0KCdwZXJfcGFnZScsIGNvdW50KTtcclxuXHR9IGVsc2Uge1xyXG5cdFx0cXVlcnlQYXJhbXMuZGVsZXRlKCdwZXJfcGFnZScpO1xyXG5cdH1cclxuXHJcblx0bGV0IHF1ZXJ5ID0gJz8nICsgcXVlcnlQYXJhbXMudG9TdHJpbmcoKTtcclxuXHJcblx0JCgnLmpzLWxvYWQtbW9yZScpLmRhdGEoJ2xpbWl0JywgY291bnQpO1xyXG5cdGxvY2F0aW9uLmhyZWYgPSAkKHRoaXMpLmRhdGEoJ2Jhc2UnKSArICBxdWVyeTtcclxufSk7XHJcblxyXG5mdW5jdGlvbiBwcm9kdWN0TWludXMoZSkge1xyXG5cdGxldCAkdGhpcyA9ICQodGhpcyk7XHJcblx0bGV0ICRpdGVtID0gJHRoaXMuY2xvc2VzdCgnLmpzLXByb2R1Y3QtaXRlbScpO1xyXG5cclxuXHRjb3VudGVyVXBkYXRlKCRpdGVtLCAnbWludXMnKTtcclxufVxyXG5cclxuZnVuY3Rpb24gcHJvZHVjdFBsdXMoZSkge1xyXG5cdGxldCAkdGhpcyA9ICQodGhpcyk7XHJcblx0bGV0ICRpdGVtID0gJHRoaXMuY2xvc2VzdCgnLmpzLXByb2R1Y3QtaXRlbScpO1xyXG5cclxuXHRjb3VudGVyVXBkYXRlKCRpdGVtLCAncGx1cycpO1xyXG59XHJcblxyXG5mdW5jdGlvbiBwcm9kdWN0SW5wdXQoZSkge1xyXG5cdGxldCAkdGhpcyA9ICQodGhpcyk7XHJcblx0bGV0ICRpdGVtID0gJHRoaXMuY2xvc2VzdCgnLmpzLXByb2R1Y3QtaXRlbScpO1xyXG5cclxuXHRjb3VudGVyVXBkYXRlKCRpdGVtKTtcclxuXHJcblx0JHRoaXMua2V5cHJlc3MoZnVuY3Rpb24gKGV2ZW50KSB7XHJcblx0XHRsZXQga2V5O1xyXG5cdFx0bGV0IGtleUNoYXI7XHJcblxyXG5cdFx0aWYgKGV2ZW50LmtleUNvZGUpIHtcclxuXHRcdFx0a2V5ID0gZXZlbnQua2V5Q29kZTtcclxuXHRcdH0gZWxzZSBpZiAoZXZlbnQud2hpY2gpIHtcclxuXHRcdFx0a2V5ID0gZXZlbnQud2hpY2g7XHJcblx0XHR9XHJcblxyXG5cdFx0aWYgKGtleSA9PT0gbnVsbCB8fCBrZXkgPT09IDAgfHwga2V5ID09PSA4IHx8IGtleSA9PT0gMTMgfHwga2V5ID09PSA5IHx8IGtleSA9PT0gNDYgfHwga2V5ID09PSAzNyB8fCBrZXkgPT09IDM5KSB7XHJcblx0XHRcdHJldHVybiB0cnVlO1xyXG5cdFx0fVxyXG5cdFx0a2V5Q2hhciA9IFN0cmluZy5mcm9tQ2hhckNvZGUoa2V5KTtcclxuXHJcblx0XHRpZiAoIS9eWzAtOV0rJC8udGVzdChrZXlDaGFyKSkge1xyXG5cdFx0XHRyZXR1cm4gZmFsc2U7XHJcblx0XHR9XHJcblx0fSk7XHJcbn1cclxuXHJcbmZ1bmN0aW9uIHByb2R1Y3RDbGVhbihlKSB7XHJcblx0bGV0ICR0aGlzID0gJCh0aGlzKTtcclxuXHRsZXQgJGl0ZW0gPSAkdGhpcy5jbG9zZXN0KCcuanMtcHJvZHVjdC1pdGVtJyk7XHJcblx0bGV0IHdob2xlc2FsZSA9ICRpdGVtLmhhc0NsYXNzKCd3aG9sZXNhbGUnKTtcclxuXHRsZXQgJGFsbCA9ICR0aGlzLmNsb3Nlc3QoJy5qcy1wcm9kdWN0LWFsbCcpO1xyXG5cdGxldCAkaW5wdXQgPSAkaXRlbS5maW5kKCcuanMtcHJvZHVjdC1pbnB1dCcpO1xyXG5cclxuXHRpZiAoJHRoaXMuaGFzQ2xhc3MoJ2FsbCcpKSB7XHJcblx0XHRsZXQgJGlucHV0ID0gJGFsbC5maW5kKCcuanMtcHJvZHVjdC1pbnB1dCcpO1xyXG5cclxuXHRcdCRpbnB1dC5lYWNoKGZ1bmN0aW9uIChpdGVtKSB7XHJcblx0XHRcdCQodGhpcykudmFsKHdob2xlc2FsZSA/IDEgOiAwKTtcclxuXHRcdH0pO1xyXG5cdH0gZWxzZSB7XHJcblx0XHQkaW5wdXQudmFsKHdob2xlc2FsZSA/IDEgOiAwKTtcclxuXHR9XHJcbn1cclxuXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuLy8gUHVibGljXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbmZ1bmN0aW9uIGFkZFRvRmF2b3JpdGUoKSB7XHJcblx0JCgnYm9keScpLm9uKCdjbGljaycsICcuanMtZmF2b3JpdGUtYnRuJywgZnVuY3Rpb24gKCkge1xyXG5cdFx0bGV0ICR0aGlzID0gJCh0aGlzKTtcclxuXHRcdGxldCAkaXRlbSA9ICR0aGlzLmNsb3Nlc3QoJy5qcy1jYXJ0LWl0ZW0nKTtcclxuXHRcdGxldCBpZCA9ICRpdGVtLmRhdGEoJ2lkJyk7XHJcblx0XHRsZXQgdXJsID0gJHRoaXMuZGF0YSgndXJsJyk7XHJcblxyXG5cdFx0JC5hamF4KHtcclxuXHRcdFx0dXJsOiB1cmwsXHJcblx0XHRcdHR5cGU6ICdwb3N0JyxcclxuXHRcdFx0ZGF0YToge1xyXG5cdFx0XHRcdGlkOiBpZFxyXG5cdFx0XHR9LFxyXG5cdFx0XHRkYXRhVHlwZTogJ2pzb24nLFxyXG5cdFx0XHRzdWNjZXNzOiBmdW5jdGlvbiAocmVzcG9uc2UpIHtcclxuXHRcdFx0XHRpZiAocmVzcG9uc2Uuc3VjY2Vzcykge1xyXG5cdFx0XHRcdFx0JCgnLmpzLWZhdm9yaXRlLWJ0bicpLmVhY2goZnVuY3Rpb24gKCkge1xyXG5cdFx0XHRcdFx0XHRsZXQgJHRoID0gJCh0aGlzKTtcclxuXHRcdFx0XHRcdFx0bGV0ICRpdCA9ICR0aC5jbG9zZXN0KCcuanMtY2FydC1pdGVtJyk7XHJcblx0XHRcdFx0XHRcdGlmICgkaXQuZGF0YSgnaWQnKSA9PT0gJGl0ZW0uZGF0YSgnaWQnKSkge1xyXG5cdFx0XHRcdFx0XHRcdGlmIChyZXNwb25zZS5hZGQpIHtcclxuXHRcdFx0XHRcdFx0XHRcdCR0aC5hZGRDbGFzcygnYWN0aXZlJyk7XHJcblx0XHRcdFx0XHRcdFx0fSBlbHNlIGlmIChyZXNwb25zZS5yZW1vdmUpIHtcclxuXHRcdFx0XHRcdFx0XHRcdCR0aC5yZW1vdmVDbGFzcygnYWN0aXZlJyk7XHJcblx0XHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHR9KTtcclxuXHJcblx0XHRcdFx0XHQkKCcuanMtZmF2b3JpdGUtY291bnQnKS50ZXh0KHJlc3BvbnNlLmNvdW50KTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH1cclxuXHRcdH0pO1xyXG5cdH0pO1xyXG59XHJcblxyXG5mdW5jdGlvbiBhZGRUb0NvbXBhcmUoKSB7XHJcblx0JCgnYm9keScpLm9uKCdjbGljaycsICcuanMtY29tcGFyZS1idG4nLCBmdW5jdGlvbiAoKSB7XHJcblx0XHRsZXQgJHRoaXMgPSAkKHRoaXMpO1xyXG5cdFx0bGV0ICRpdGVtID0gJHRoaXMuY2xvc2VzdCgnLmpzLWNhcnQtaXRlbScpO1xyXG5cdFx0bGV0IGlkID0gJGl0ZW0uZGF0YSgnaWQnKTtcclxuXHRcdGxldCB1cmwgPSAkdGhpcy5kYXRhKCd1cmwnKTtcclxuXHJcblx0XHQkLmFqYXgoe1xyXG5cdFx0XHR1cmw6IHVybCxcclxuXHRcdFx0dHlwZTogJ3Bvc3QnLFxyXG5cdFx0XHRkYXRhOiB7XHJcblx0XHRcdFx0aWQ6IGlkXHJcblx0XHRcdH0sXHJcblx0XHRcdGRhdGFUeXBlOiAnanNvbicsXHJcblx0XHRcdHN1Y2Nlc3M6IGZ1bmN0aW9uIChyZXNwb25zZSkge1xyXG5cdFx0XHRcdGlmIChyZXNwb25zZS5zdWNjZXNzKSB7XHJcblx0XHRcdFx0XHQkKCcuanMtY29tcGFyZS1idG4nKS5lYWNoKGZ1bmN0aW9uICgpIHtcclxuXHRcdFx0XHRcdFx0bGV0ICR0aCA9ICQodGhpcyk7XHJcblx0XHRcdFx0XHRcdGxldCAkaXQgPSAkdGguY2xvc2VzdCgnLmpzLWNhcnQtaXRlbScpO1xyXG5cdFx0XHRcdFx0XHRpZiAoJGl0LmRhdGEoJ2lkJykgPT09ICRpdGVtLmRhdGEoJ2lkJykpIHtcclxuXHRcdFx0XHRcdFx0XHRpZiAocmVzcG9uc2UuYWRkKSB7XHJcblx0XHRcdFx0XHRcdFx0XHQkdGguYWRkQ2xhc3MoJ2FjdGl2ZScpO1xyXG5cdFx0XHRcdFx0XHRcdH0gZWxzZSBpZiAocmVzcG9uc2UucmVtb3ZlKSB7XHJcblx0XHRcdFx0XHRcdFx0XHQkdGgucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpO1xyXG5cdFx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0fSk7XHJcblxyXG5cdFx0XHRcdFx0JCgnLmpzLWNvbXBhcmUtY291bnQnKS50ZXh0KHJlc3BvbnNlLmNvdW50KTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH1cclxuXHRcdH0pO1xyXG5cdH0pO1xyXG59XHJcblxyXG5mdW5jdGlvbiBsaWtlQWRkKCkge1xyXG5cdCQoJ2JvZHknKS5vbignY2xpY2snLCAnLmpzLXJldmlldy1saWtlJywgZnVuY3Rpb24gKCkge1xyXG5cdFx0bGV0ICR0aGlzID0gJCh0aGlzKTtcclxuXHRcdGxldCAkaXRlbSA9ICR0aGlzLmNsb3Nlc3QoJy5qcy1yZXZpZXcnKTtcclxuXHRcdGxldCBpZCA9ICRpdGVtLmRhdGEoJ2lkJyk7XHJcblx0XHRsZXQgdXJsID0gJHRoaXMuZGF0YSgndXJsJyk7XHJcblx0XHRsZXQgJGRpc2xpa2UgPSAkaXRlbS5maW5kKCcuanMtcmV2aWV3LWRpc2xpa2UnKTtcclxuXHRcdGxldCAkY291bnRlciA9ICR0aGlzLmZpbmQoJy5qcy1saWtlLWNvdW50Jyk7XHJcblx0XHRsZXQgJGRpc2xpa2VDb3VudGVyID0gJGl0ZW0uZmluZCgnLmpzLWRpc2xpa2UtY291bnQnKTtcclxuXHJcblx0XHQkLmFqYXgoe1xyXG5cdFx0XHR1cmw6IHVybCxcclxuXHRcdFx0dHlwZTogJ3Bvc3QnLFxyXG5cdFx0XHRkYXRhOiB7XHJcblx0XHRcdFx0aWQ6IGlkXHJcblx0XHRcdH0sXHJcblx0XHRcdGRhdGFUeXBlOiAnanNvbicsXHJcblx0XHRcdHN1Y2Nlc3M6IGZ1bmN0aW9uIChyZXNwb25zZSkge1xyXG5cdFx0XHRcdGlmIChyZXNwb25zZS5zdWNjZXNzKSB7XHJcblx0XHRcdFx0XHRpZiAocmVzcG9uc2UuYWRkKSB7XHJcblx0XHRcdFx0XHRcdCRkaXNsaWtlLnJlbW92ZUNsYXNzKCdhY3RpdmUnKTtcclxuXHRcdFx0XHRcdFx0JHRoaXMuYWRkQ2xhc3MoJ2FjdGl2ZScpO1xyXG5cdFx0XHRcdFx0fSBlbHNlIGlmIChyZXNwb25zZS5yZW1vdmUpIHtcclxuXHRcdFx0XHRcdFx0JHRoaXMucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpO1xyXG5cdFx0XHRcdFx0fVxyXG5cclxuXHRcdFx0XHRcdCRjb3VudGVyLnRleHQocmVzcG9uc2UubGlrZUNvdW50KTtcclxuXHRcdFx0XHRcdCRkaXNsaWtlQ291bnRlci50ZXh0KHJlc3BvbnNlLmRpc2xpa2VDb3VudCk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9XHJcblx0XHR9KTtcclxuXHR9KTtcclxufVxyXG5cclxuZnVuY3Rpb24gZGlzbGlrZUFkZCgpIHtcclxuXHQkKCdib2R5Jykub24oJ2NsaWNrJywgJy5qcy1yZXZpZXctZGlzbGlrZScsIGZ1bmN0aW9uICgpIHtcclxuXHRcdGxldCAkdGhpcyA9ICQodGhpcyk7XHJcblx0XHRsZXQgJGl0ZW0gPSAkdGhpcy5jbG9zZXN0KCcuanMtcmV2aWV3Jyk7XHJcblx0XHRsZXQgaWQgPSAkaXRlbS5kYXRhKCdpZCcpO1xyXG5cdFx0bGV0IHVybCA9ICR0aGlzLmRhdGEoJ3VybCcpO1xyXG5cdFx0bGV0ICRsaWtlID0gJGl0ZW0uZmluZCgnLmpzLXJldmlldy1saWtlJyk7XHJcblx0XHRsZXQgJGNvdW50ZXIgPSAkdGhpcy5maW5kKCcuanMtZGlzbGlrZS1jb3VudCcpO1xyXG5cdFx0bGV0ICRsaWtlQ291bnRlciA9ICRpdGVtLmZpbmQoJy5qcy1saWtlLWNvdW50Jyk7XHJcblxyXG5cdFx0JC5hamF4KHtcclxuXHRcdFx0dXJsOiB1cmwsXHJcblx0XHRcdHR5cGU6ICdwb3N0JyxcclxuXHRcdFx0ZGF0YToge1xyXG5cdFx0XHRcdGlkOiBpZFxyXG5cdFx0XHR9LFxyXG5cdFx0XHRkYXRhVHlwZTogJ2pzb24nLFxyXG5cdFx0XHRzdWNjZXNzOiBmdW5jdGlvbiAocmVzcG9uc2UpIHtcclxuXHRcdFx0XHRpZiAocmVzcG9uc2Uuc3VjY2Vzcykge1xyXG5cdFx0XHRcdFx0aWYgKHJlc3BvbnNlLmFkZCkge1xyXG5cdFx0XHRcdFx0XHQkbGlrZS5yZW1vdmVDbGFzcygnYWN0aXZlJyk7XHJcblx0XHRcdFx0XHRcdCR0aGlzLmFkZENsYXNzKCdhY3RpdmUnKTtcclxuXHRcdFx0XHRcdH0gZWxzZSBpZiAocmVzcG9uc2UucmVtb3ZlKSB7XHJcblx0XHRcdFx0XHRcdCR0aGlzLnJlbW92ZUNsYXNzKCdhY3RpdmUnKTtcclxuXHRcdFx0XHRcdH1cclxuXHJcblx0XHRcdFx0XHQkY291bnRlci50ZXh0KHJlc3BvbnNlLmRpc2xpa2VDb3VudCk7XHJcblx0XHRcdFx0XHQkbGlrZUNvdW50ZXIudGV4dChyZXNwb25zZS5saWtlQ291bnQpO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cdFx0fSk7XHJcblx0fSk7XHJcbn1cclxuXHJcbmZ1bmN0aW9uIHRvZ2dsZUNhcnRJbmZvKCkge1xyXG5cdCRjYXJ0SW5mb1JhZGlvLmVhY2goZnVuY3Rpb24gKGluZGV4KSB7XHJcblx0XHRpZiAoISQodGhpcykucHJvcCgnY2hlY2tlZCcpKSB7XHJcblx0XHRcdGxldCAkY29udGVudCA9ICQodGhpcykucGFyZW50KCkubmV4dCgnLmNhcnQtaW5mb19fZGVsaXZlcnktY29udGVudCcpO1xyXG5cclxuXHRcdFx0JGNvbnRlbnQuc2xpZGVVcCgnZmFzdCcpO1xyXG5cdFx0fVxyXG5cdH0pO1xyXG5cclxuXHQkY2FydEluZm9SYWRpby5vbignY2hhbmdlJywgZnVuY3Rpb24gKCkge1xyXG5cdFx0dG9nZ2xlKHRoaXMpO1xyXG5cdH0pO1xyXG59XHJcblxyXG5mdW5jdGlvbiBsb2FkTW9yZSgpIHtcclxuXHRsZXQgJGxvYWRNb3JlV3JhcCA9ICQoJy5qcy1sb2FkLXdyYXAnKTtcclxuXHRsZXQgcHJlbG9hZGVyID0gbmV3IFByZWxvYWRlcigkbG9hZE1vcmVXcmFwKTtcclxuXHJcblx0JCgnLmpzLWxvYWQtbW9yZScpLmNsaWNrKGZ1bmN0aW9uICgpIHtcclxuXHJcblx0XHRsZXQgJHRoaXMgPSAkKHRoaXMpO1xyXG5cdFx0bGV0IHVybCA9ICR0aGlzLmRhdGEoJ3VybCcpO1xyXG5cdFx0bGV0IHBhZ2UgPSAkdGhpcy5kYXRhKCdwYWdlJyk7XHJcblx0XHRsZXQgbGltaXQgPSAkdGhpcy5kYXRhKCdsaW1pdCcpO1xyXG5cdFx0bGV0IHBhcmFtcyA9ICR0aGlzLmRhdGEoJ3BhcmFtcycpO1xyXG5cclxuXHRcdGxldCBzdGFydFBhZ2UgPSAkKFwiLnBhZ2luYXRpb25fX2l0ZW0uYWN0aXZlXCIpLmZpcnN0KCkuYXR0cignZGF0YS1wYWdlJyk7XHJcblx0XHRsZXQgZmluaXNoUGFnZSA9ICQoXCIucGFnaW5hdGlvbl9faXRlbS5hY3RpdmVcIikubGFzdCgpLmF0dHIoJ2RhdGEtcGFnZScpO1xyXG5cclxuXHRcdCQuY29va2llKCdwYWdlX251bWJlckZpbmlzaCcsICsrZmluaXNoUGFnZSk7XHJcblxyXG5cdFx0aWYgKCEkLmNvb2tpZSgncGFnZV9udW1iZXJTdGFydCcpIHx8ICQuY29va2llKCdwYWdlX251bWJlclN0YXJ0JykgPT09ICdudWxsJykge1xyXG5cdFx0XHQkLmNvb2tpZSgncGFnZV9udW1iZXJTdGFydCcsIHN0YXJ0UGFnZSk7XHJcblx0XHR9XHJcblxyXG5cclxuXHRcdCR0aGlzLnByb3AoJ2Rpc2FibGVkJywgdHJ1ZSlcclxuXHJcblx0XHQkLmFqYXgoe1xyXG5cdFx0XHR0eXBlOiAncG9zdCcsXHJcblx0XHRcdHVybDogdXJsLFxyXG5cdFx0XHRkYXRhOiB7XHJcblx0XHRcdFx0cGFnZTogcGFnZSxcclxuXHRcdFx0XHRsaW1pdCxcclxuXHRcdFx0XHRwYXJhbXM6IHBhcmFtc1xyXG5cdFx0XHR9LFxyXG5cdFx0XHRkYXRhVHlwZTogJ2pzb24nLFxyXG5cdFx0XHRzdWNjZXNzOiBmdW5jdGlvbiAocmVzcG9uc2UpIHtcclxuXHRcdFx0XHRpZiAocmVzcG9uc2Uuc3VjY2Vzcykge1xyXG5cdFx0XHRcdFx0JHRoaXMuZGF0YSgncGFnZScsIHJlc3BvbnNlLnBhZ2UpO1xyXG5cclxuXHRcdFx0XHRcdCRsb2FkTW9yZVdyYXAuYXBwZW5kKHJlc3BvbnNlLmh0bWwpO1xyXG5cclxuXHRcdFx0XHRcdGlmIChyZXNwb25zZS5wYWdpbmF0aW9uKSB7XHJcblx0XHRcdFx0XHRcdCQoJy5qcy1wYWdpbmF0aW9uLWNvbnRhaW5lcicpLmh0bWwocmVzcG9uc2UucGFnaW5hdGlvbik7XHJcblx0XHRcdFx0XHR9XHJcblxyXG5cdFx0XHRcdFx0aWYgKHJlc3BvbnNlLmxhc3QpIHtcclxuXHRcdFx0XHRcdFx0JHRoaXMuaGlkZSgpO1xyXG5cdFx0XHRcdFx0fVxyXG5cclxuXHRcdFx0XHR9XHJcblxyXG5cdFx0XHRcdHNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xyXG5cdFx0XHRcdFx0JHRoaXMucHJvcCgnZGlzYWJsZWQnLCBmYWxzZSk7XHJcblx0XHRcdFx0fSwgNTAwKVxyXG5cclxuXHJcblx0XHRcdFx0bW9kdWxlTG9hZGVyLmluaXQoJChkb2N1bWVudCkpO1xyXG5cclxuXHRcdFx0XHRwcm9kdWN0SW1nU2xpZGVyKCk7XHJcblxyXG5cdFx0XHR9XHJcblx0XHR9KTtcclxuXHR9KTtcclxufVxyXG5cclxuLy8gZnVuY3Rpb24gY2FydEhvbGRlclNpemUgKCkge1xyXG4vLyBcdGxldCBib3R0b20gPSAkKCcuY2FydC13aW5kb3dfX2JvdHRvbScpLm91dGVySGVpZ2h0KCk7XHJcbi8vIFx0bGV0IGNvbnRlbnQgPSAkKCcuY2FydC13aW5kb3dfX2NvbnRlbnQnKS5vdXRlckhlaWdodCgpO1xyXG4vLyBcdGxldCB0b3AgPSAkKCcuY2FydC13aW5kb3dfX3RvcCcpLm91dGVySGVpZ2h0KCk7XHJcbi8vIFx0bGV0IGhvbGRlciA9ICQoJy5jYXJ0LWhvbGRlcicpLm91dGVySGVpZ2h0KCk7XHJcbi8vIH1cclxuXHJcbmZ1bmN0aW9uIHByb2R1Y3RDb3VudGVyKCkge1xyXG5cdGxldCAkY29udGFpbmVyID0gJCgnYm9keScpO1xyXG5cdCRjb250YWluZXIub24oJ2NsaWNrJywgJy5qcy1wcm9kdWN0LW1pbnVzJywgcHJvZHVjdE1pbnVzKTtcclxuXHQkY29udGFpbmVyLm9uKCdjbGljaycsICcuanMtcHJvZHVjdC1wbHVzJywgcHJvZHVjdFBsdXMpO1xyXG5cdCRjb250YWluZXIub24oJ2tleXVwJywgJy5qcy1wcm9kdWN0LWlucHV0JywgcHJvZHVjdElucHV0KTtcclxuXHQkY29udGFpbmVyLm9uKCdjbGljaycsICcuanMtcHJvZHVjdC1jbGVhbicsIHByb2R1Y3RDbGVhbik7XHJcbn1cclxuXHJcblxyXG5mdW5jdGlvbiBzY3JvbGxUb3AoKSB7XHJcblx0Y29uc3QgJGZvb3RlciA9ICQoJy5mb290ZXInKTtcclxuXHRjb25zdCAkZm9vdGVyT2Zmc2V0VG9wID0gJGZvb3Rlci5vZmZzZXQoKS50b3AgLSAkZm9vdGVyLmhlaWdodCgpO1xyXG5cdGNvbnN0ICR0cmlnZ2VyID0gJCgnLnRvVG9wLWpzJyk7XHJcblx0Y29uc3QgYWN0aXZlQ2xhc3MgPSAnYWN0aXZlJztcclxuXHJcblx0JCh3aW5kb3cpLm9uKCdzY3JvbGwnLCAoKSA9PiAge1xyXG5cdFx0bGV0ICRvZmZzZXRUb3AgPSAkKGRvY3VtZW50KS5zY3JvbGxUb3AoKTtcclxuXHJcblx0XHRpZiAoJG9mZnNldFRvcCA+ICQod2luZG93KS5oZWlnaHQoKSAvIDMpIHtcclxuXHRcdFx0JHRyaWdnZXIuYWRkQ2xhc3MoYWN0aXZlQ2xhc3MpO1xyXG5cdFx0fSBlbHNlIHtcclxuXHRcdFx0JHRyaWdnZXIucmVtb3ZlQ2xhc3MoYWN0aXZlQ2xhc3MpO1xyXG5cdFx0fVxyXG5cdFx0aWYgKCAkb2Zmc2V0VG9wID4gJGZvb3Rlck9mZnNldFRvcCApIHtcclxuXHRcdFx0JHRyaWdnZXIucmVtb3ZlQ2xhc3MoYWN0aXZlQ2xhc3MpO1xyXG5cdFx0fVxyXG5cclxuXHR9KTtcclxuXHJcblx0JHRyaWdnZXIub24oXCJjbGlja1wiLCAoKSA9PiB7XHJcblx0XHQkKCdodG1sLCBib2R5JykuYW5pbWF0ZSh7IHNjcm9sbFRvcDogMCB9LCA2MDAsICdsaW5lYXInKTtcclxuXHR9KTtcclxufVxyXG5cclxuZnVuY3Rpb24gaGVhZGVyUGhvbmVzTW9iKCkge1xyXG5cdGxldCAkd3JhcCA9ICQoJy5oZWFkZXItcGhvbmVzX193cmFwJyk7XHJcblx0bGV0ICR0cmlnZ2VyID0gJCgnLmhlYWRlci1waG9uZXNfX3N2ZycpO1xyXG5cclxuXHQkdHJpZ2dlci5vbignY2xpY2snLCBmdW5jdGlvbigpIHtcclxuXHRcdCR3cmFwLmFkZENsYXNzKCdhY3RpdmUnKTtcclxuXHR9KVxyXG5cclxuXHQkKGRvY3VtZW50KS5tb3VzZXVwKGZ1bmN0aW9uIChlKXtcclxuXHRcdGlmICghJHdyYXAuaXMoZS50YXJnZXQpXHJcblx0XHRcdCYmICR3cmFwLmhhcyhlLnRhcmdldCkubGVuZ3RoID09PSAwKSB7XHJcblx0XHRcdCR3cmFwLnJlbW92ZUNsYXNzKCdhY3RpdmUnKTs7XHJcblx0XHR9XHJcblx0fSk7XHJcbn1cclxuXHJcbmZ1bmN0aW9uIHRvZ2dsZURlbGl2ZXJ5SW5mbygpIHtcclxuXHRsZXQgJHRyaWdnZXIgPSAkKCcuanMtZGVsaXZlcnktdHJpZ2dlcicpO1xyXG5cdGxldCAkd3JhcCA9ICQoJy5qcy1kZWxpdmVyeS1pdGVtJyk7XHJcblx0bGV0ICR0ZXh0ID0gJCgnLmpzLWRlbGl2ZXJ5LXRleHQnKTtcclxuXHJcblx0JHRyaWdnZXIub24oJ2NsaWNrJywgZnVuY3Rpb24gKCkge1xyXG5cdFx0JCh0aGlzKS50b2dnbGVDbGFzcygncm90YXRlJyk7XHJcblx0XHQkKHRoaXMpLmNsb3Nlc3QoJHdyYXApLmZpbmQoJHRleHQpLnNsaWRlVG9nZ2xlKDMwMCk7XHJcblx0fSlcclxufVxyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbi8vIEV4cG9ydHNcclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuZXhwb3J0IHthZGRUb0Zhdm9yaXRlLCBhZGRUb0NvbXBhcmUsIHRvZ2dsZUNhcnRJbmZvLCBsb2FkTW9yZSwgcHJvZHVjdENvdW50ZXIsIGRpc2xpa2VBZGQsIGxpa2VBZGQsIHNjcm9sbFRvcCwgaGVhZGVyUGhvbmVzTW9iLCB0b2dnbGVEZWxpdmVyeUluZm99O1xyXG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9fSFRNTC9zcmMvanMvX21vZHVsZXMvY29tbW9uLmpzIiwiJ3VzZSBzdHJpY3QnO1xyXG5cclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4vLyBQcml2YXRlXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbmxldCAkbWFwID0gJCgnLm1hcCcpO1xyXG5sZXQgJGNsaWNrVGFyZ2V0ID0gJCgnLmNvbnRhY3RzLWl0ZW1fX2FkZHJlc3MtYm94Jyk7XHJcbmxldCBpY29uID0gJy9NZWRpYS9hc3NldHMvY3NzL3N0YXRpYy9waWMvbWFya2VyLWljb24ucG5nJztcclxubGV0IG1hcFN0eWxlID0gW1xyXG5cdHtcclxuXHRcdCdmZWF0dXJlVHlwZSc6ICd3YXRlcicsXHJcblx0XHQnZWxlbWVudFR5cGUnOiAnZ2VvbWV0cnkuZmlsbCcsXHJcblx0XHQnc3R5bGVycyc6IFtcclxuXHRcdFx0e1xyXG5cdFx0XHRcdCdjb2xvcic6ICcjZDNkM2QzJ1xyXG5cdFx0XHR9XHJcblx0XHRdXHJcblx0fSxcclxuXHR7XHJcblx0XHQnZmVhdHVyZVR5cGUnOiAndHJhbnNpdCcsXHJcblx0XHQnc3R5bGVycyc6IFtcclxuXHRcdFx0e1xyXG5cdFx0XHRcdCdjb2xvcic6ICcjODA4MDgwJ1xyXG5cdFx0XHR9LFxyXG5cdFx0XHR7XHJcblx0XHRcdFx0J3Zpc2liaWxpdHknOiAnb2ZmJ1xyXG5cdFx0XHR9XHJcblx0XHRdXHJcblx0fSxcclxuXHR7XHJcblx0XHQnZmVhdHVyZVR5cGUnOiAncm9hZC5oaWdod2F5JyxcclxuXHRcdCdlbGVtZW50VHlwZSc6ICdnZW9tZXRyeS5zdHJva2UnLFxyXG5cdFx0J3N0eWxlcnMnOiBbXHJcblx0XHRcdHtcclxuXHRcdFx0XHQndmlzaWJpbGl0eSc6ICdvbidcclxuXHRcdFx0fSxcclxuXHRcdFx0e1xyXG5cdFx0XHRcdCdjb2xvcic6ICcjYjNiM2IzJ1xyXG5cdFx0XHR9XHJcblx0XHRdXHJcblx0fSxcclxuXHR7XHJcblx0XHQnZmVhdHVyZVR5cGUnOiAncm9hZC5oaWdod2F5JyxcclxuXHRcdCdlbGVtZW50VHlwZSc6ICdnZW9tZXRyeS5maWxsJyxcclxuXHRcdCdzdHlsZXJzJzogW1xyXG5cdFx0XHR7XHJcblx0XHRcdFx0J2NvbG9yJzogJyNmZmZmZmYnXHJcblx0XHRcdH1cclxuXHRcdF1cclxuXHR9LFxyXG5cdHtcclxuXHRcdCdmZWF0dXJlVHlwZSc6ICdyb2FkLmxvY2FsJyxcclxuXHRcdCdlbGVtZW50VHlwZSc6ICdnZW9tZXRyeS5maWxsJyxcclxuXHRcdCdzdHlsZXJzJzogW1xyXG5cdFx0XHR7XHJcblx0XHRcdFx0J3Zpc2liaWxpdHknOiAnb24nXHJcblx0XHRcdH0sXHJcblx0XHRcdHtcclxuXHRcdFx0XHQnY29sb3InOiAnI2ZmZmZmZidcclxuXHRcdFx0fSxcclxuXHRcdFx0e1xyXG5cdFx0XHRcdCd3ZWlnaHQnOiAxLjhcclxuXHRcdFx0fVxyXG5cdFx0XVxyXG5cdH0sXHJcblx0e1xyXG5cdFx0J2ZlYXR1cmVUeXBlJzogJ3JvYWQubG9jYWwnLFxyXG5cdFx0J2VsZW1lbnRUeXBlJzogJ2dlb21ldHJ5LnN0cm9rZScsXHJcblx0XHQnc3R5bGVycyc6IFtcclxuXHRcdFx0e1xyXG5cdFx0XHRcdCdjb2xvcic6ICcjZDdkN2Q3J1xyXG5cdFx0XHR9XHJcblx0XHRdXHJcblx0fSxcclxuXHR7XHJcblx0XHQnZmVhdHVyZVR5cGUnOiAncG9pJyxcclxuXHRcdCdlbGVtZW50VHlwZSc6ICdnZW9tZXRyeS5maWxsJyxcclxuXHRcdCdzdHlsZXJzJzogW1xyXG5cdFx0XHR7XHJcblx0XHRcdFx0J3Zpc2liaWxpdHknOiAnb24nXHJcblx0XHRcdH0sXHJcblx0XHRcdHtcclxuXHRcdFx0XHQnY29sb3InOiAnI2ViZWJlYidcclxuXHRcdFx0fVxyXG5cdFx0XVxyXG5cdH0sXHJcblx0e1xyXG5cdFx0J2ZlYXR1cmVUeXBlJzogJ2FkbWluaXN0cmF0aXZlJyxcclxuXHRcdCdlbGVtZW50VHlwZSc6ICdnZW9tZXRyeScsXHJcblx0XHQnc3R5bGVycyc6IFtcclxuXHRcdFx0e1xyXG5cdFx0XHRcdCdjb2xvcic6ICcjYTdhN2E3J1xyXG5cdFx0XHR9XHJcblx0XHRdXHJcblx0fSxcclxuXHR7XHJcblx0XHQnZmVhdHVyZVR5cGUnOiAncm9hZC5hcnRlcmlhbCcsXHJcblx0XHQnZWxlbWVudFR5cGUnOiAnZ2VvbWV0cnkuZmlsbCcsXHJcblx0XHQnc3R5bGVycyc6IFtcclxuXHRcdFx0e1xyXG5cdFx0XHRcdCdjb2xvcic6ICcjZmZmZmZmJ1xyXG5cdFx0XHR9XHJcblx0XHRdXHJcblx0fSxcclxuXHR7XHJcblx0XHQnZmVhdHVyZVR5cGUnOiAncm9hZC5hcnRlcmlhbCcsXHJcblx0XHQnZWxlbWVudFR5cGUnOiAnZ2VvbWV0cnkuZmlsbCcsXHJcblx0XHQnc3R5bGVycyc6IFtcclxuXHRcdFx0e1xyXG5cdFx0XHRcdCdjb2xvcic6ICcjZmZmZmZmJ1xyXG5cdFx0XHR9XHJcblx0XHRdXHJcblx0fSxcclxuXHR7XHJcblx0XHQnZmVhdHVyZVR5cGUnOiAnbGFuZHNjYXBlJyxcclxuXHRcdCdlbGVtZW50VHlwZSc6ICdnZW9tZXRyeS5maWxsJyxcclxuXHRcdCdzdHlsZXJzJzogW1xyXG5cdFx0XHR7XHJcblx0XHRcdFx0J3Zpc2liaWxpdHknOiAnb24nXHJcblx0XHRcdH0sXHJcblx0XHRcdHtcclxuXHRcdFx0XHQnY29sb3InOiAnI2VmZWZlZidcclxuXHRcdFx0fVxyXG5cdFx0XVxyXG5cdH0sXHJcblx0e1xyXG5cdFx0J2ZlYXR1cmVUeXBlJzogJ3JvYWQnLFxyXG5cdFx0J2VsZW1lbnRUeXBlJzogJ2xhYmVscy50ZXh0LmZpbGwnLFxyXG5cdFx0J3N0eWxlcnMnOiBbXHJcblx0XHRcdHtcclxuXHRcdFx0XHQnY29sb3InOiAnIzY5Njk2OSdcclxuXHRcdFx0fVxyXG5cdFx0XVxyXG5cdH0sXHJcblx0e1xyXG5cdFx0J2ZlYXR1cmVUeXBlJzogJ2FkbWluaXN0cmF0aXZlJyxcclxuXHRcdCdlbGVtZW50VHlwZSc6ICdsYWJlbHMudGV4dC5maWxsJyxcclxuXHRcdCdzdHlsZXJzJzogW1xyXG5cdFx0XHR7XHJcblx0XHRcdFx0J3Zpc2liaWxpdHknOiAnb24nXHJcblx0XHRcdH0sXHJcblx0XHRcdHtcclxuXHRcdFx0XHQnY29sb3InOiAnIzczNzM3MydcclxuXHRcdFx0fVxyXG5cdFx0XVxyXG5cdH0sXHJcblx0e1xyXG5cdFx0J2ZlYXR1cmVUeXBlJzogJ3BvaScsXHJcblx0XHQnZWxlbWVudFR5cGUnOiAnbGFiZWxzLmljb24nLFxyXG5cdFx0J3N0eWxlcnMnOiBbXHJcblx0XHRcdHtcclxuXHRcdFx0XHQndmlzaWJpbGl0eSc6ICdvZmYnXHJcblx0XHRcdH1cclxuXHRcdF1cclxuXHR9LFxyXG5cdHtcclxuXHRcdCdmZWF0dXJlVHlwZSc6ICdwb2knLFxyXG5cdFx0J2VsZW1lbnRUeXBlJzogJ2xhYmVscycsXHJcblx0XHQnc3R5bGVycyc6IFtcclxuXHRcdFx0e1xyXG5cdFx0XHRcdCd2aXNpYmlsaXR5JzogJ29mZidcclxuXHRcdFx0fVxyXG5cdFx0XVxyXG5cdH0sXHJcblx0e1xyXG5cdFx0J2ZlYXR1cmVUeXBlJzogJ3JvYWQuYXJ0ZXJpYWwnLFxyXG5cdFx0J2VsZW1lbnRUeXBlJzogJ2dlb21ldHJ5LnN0cm9rZScsXHJcblx0XHQnc3R5bGVycyc6IFtcclxuXHRcdFx0e1xyXG5cdFx0XHRcdCdjb2xvcic6ICcjZDZkNmQ2J1xyXG5cdFx0XHR9XHJcblx0XHRdXHJcblx0fSxcclxuXHR7XHJcblx0XHQnZmVhdHVyZVR5cGUnOiAncm9hZCcsXHJcblx0XHQnZWxlbWVudFR5cGUnOiAnbGFiZWxzLmljb24nLFxyXG5cdFx0J3N0eWxlcnMnOiBbXHJcblx0XHRcdHtcclxuXHRcdFx0XHQndmlzaWJpbGl0eSc6ICdvZmYnXHJcblx0XHRcdH1cclxuXHRcdF1cclxuXHR9LFxyXG5cdHt9LFxyXG5cdHtcclxuXHRcdCdmZWF0dXJlVHlwZSc6ICdwb2knLFxyXG5cdFx0J2VsZW1lbnRUeXBlJzogJ2dlb21ldHJ5LmZpbGwnLFxyXG5cdFx0J3N0eWxlcnMnOiBbXHJcblx0XHRcdHtcclxuXHRcdFx0XHQnY29sb3InOiAnI2RhZGFkYSdcclxuXHRcdFx0fVxyXG5cdFx0XVxyXG5cdH1cclxuXTtcclxuXHJcbmZ1bmN0aW9uIHNldFBsYWNlIChsYXRpdHVkZSwgbG9uZ2l0dWRlKSB7XHJcblx0bGV0IHBsYWNlID0gbmV3IGdvb2dsZS5tYXBzLkxhdExuZyhsYXRpdHVkZSwgbG9uZ2l0dWRlKTsgLy8gZXNsaW50LWRpc2FibGUtbGluZSBuby11bmRlZlxyXG5cclxuXHRyZXR1cm4gcGxhY2U7XHJcbn1cclxuXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuLy8gUHVibGljXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbmZ1bmN0aW9uIGluaXRNYXAgKCkge1xyXG5cdGlmICghJG1hcC5sZW5ndGgpIHtcclxuXHRcdHJldHVybiBmYWxzZTtcclxuXHR9XHJcblxyXG5cdGxldCBsb2NhdGlvbiA9IHtcclxuXHRcdGxhdGl0dWRlOiAnJyxcclxuXHRcdGxvbmdpdHVkZTogJydcclxuXHR9O1xyXG5cclxuXHQkY2xpY2tUYXJnZXQub24oJ2NsaWNrJywgZnVuY3Rpb24gKCkge1xyXG5cdFx0bGV0ICRtYXBXcmFwID0gJCh0aGlzKS5jbG9zZXN0KCcuY29udGFjdHMtaXRlbV9fdG9wLXdyYXAnKS5uZXh0KCcuY29udGFjdHMtaXRlbV9fYm90dG9tJyk7XHJcblx0XHRsZXQgJG1hcFRvcCA9ICQodGhpcykuY2xvc2VzdCgnLmNvbnRhY3RzLWl0ZW1fX3RvcC13cmFwJyk7XHJcblx0XHRsZXQgX21hcCA9ICRtYXBXcmFwLmZpbmQoJy5tYXAnKTtcclxuXHRcdGxldCBpbmRleCA9ICRtYXBUb3AuZGF0YSgnaW5kZXgnKTtcclxuXHJcblx0XHQkbWFwV3JhcC5zbGlkZVRvZ2dsZSgnZmFzdCcpO1xyXG5cdFx0JG1hcFRvcC50b2dnbGVDbGFzcygnb3BlbicpO1xyXG5cclxuXHRcdGxvY2F0aW9uID0ge1xyXG5cdFx0XHRsYXRpdHVkZTogX21hcC5kYXRhKCdsYXQnKSxcclxuXHRcdFx0bG9uZ2l0dWRlOiBfbWFwLmRhdGEoJ2xuZycpXHJcblx0XHR9O1xyXG5cclxuXHRcdGxldCBtYXAgPSBuZXcgZ29vZ2xlLm1hcHMuTWFwKGRvY3VtZW50LmdldEVsZW1lbnRzQnlDbGFzc05hbWUoJ21hcCcpW2luZGV4XSwgeyAvLyBlc2xpbnQtZGlzYWJsZS1saW5lIG5vLXVuZGVmXHJcblx0XHRcdHpvb206IDE1LFxyXG5cdFx0XHRjZW50ZXI6IHNldFBsYWNlKGxvY2F0aW9uLmxhdGl0dWRlLCBsb2NhdGlvbi5sb25naXR1ZGUpLFxyXG5cdFx0XHRzY3JvbGx3aGVlbDogZmFsc2UsXHJcblx0XHRcdHN0eWxlczogbWFwU3R5bGVcclxuXHRcdH0pO1xyXG5cclxuXHRcdGxldCBtYXJrZXIgPSBuZXcgZ29vZ2xlLm1hcHMuTWFya2VyKHsgLy8gZXNsaW50LWRpc2FibGUtbGluZSBuby11bmRlZiwgbm8tdW51c2VkLXZhcnNcclxuXHRcdFx0cG9zaXRpb246IHNldFBsYWNlKGxvY2F0aW9uLmxhdGl0dWRlLCBsb2NhdGlvbi5sb25naXR1ZGUpLFxyXG5cdFx0XHRtYXA6IG1hcCxcclxuXHRcdFx0b3B0aW1pemVkOiBmYWxzZSxcclxuXHRcdFx0aWNvbjogaWNvbixcclxuXHRcdFx0dGl0bGU6ICcnXHJcblx0XHR9KTtcclxuXHR9KTtcclxufVxyXG5cclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4vLyBFeHBvcnRzXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbmV4cG9ydCB7aW5pdE1hcH07XHJcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL19IVE1ML3NyYy9qcy9fbW9kdWxlcy9tYXAuanMiLCIndXNlIHN0cmljdCc7XHJcblxyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbi8vIEltcG9ydHNcclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuaW1wb3J0IFByZWxvYWRlciBmcm9tICcjL19tb2R1bGVzL1ByZWxvYWRlcic7XHJcbmltcG9ydCBtb2R1bGVMb2FkZXIgZnJvbSAnIy9tb2R1bGUtbG9hZGVyJztcclxuXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuLy8gUHJpdmF0ZVxyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG5sZXQgb3BlbkNhcnRCdG4gPSAnLmpzLWNhcnQtb3Blbic7XHJcbmxldCBjbG9zZUNhcnRCdG4gPSAnLmpzLWNhcnQtY2xvc2UnO1xyXG5sZXQgY2FydFdyYXAgPSAnLmNhcnQtd3JhcCc7XHJcbmxldCAkY2FydElubmVyID0gJCgnLmNhcnQtaW5uZXInKTtcclxubGV0IG1hZ25pZmljUG9wdXAgPSAkLm1hZ25pZmljUG9wdXAuaW5zdGFuY2U7XHJcbmxldCB1cmwgPSAnL1ZpZXdzL1dpZGdldHMvUG9wdXAvVGhhbmsucGhwJztcclxuXHJcbmZ1bmN0aW9uIHNob3dNZXNzYWdlIChtZXNzYWdlKSB7XHJcblx0aWYgKG1hZ25pZmljUG9wdXAuaXNPcGVuKSB7XHJcblx0XHQkLm1hZ25pZmljUG9wdXAuY2xvc2UoKTtcclxuXHR9XHJcblxyXG5cdCQubWFnbmlmaWNQb3B1cC5vcGVuKHtcclxuXHRcdGl0ZW1zOiB7XHJcblx0XHRcdHNyYzogdXJsXHJcblx0XHR9LFxyXG5cdFx0dHlwZTogJ2FqYXgnLFxyXG5cdFx0cmVtb3ZhbERlbGF5OiAzMDAsXHJcblx0XHRtYWluQ2xhc3M6ICd6b29tLWluJyxcclxuXHRcdGNsb3NlTWFya3VwOiBgPGJ1dHRvbiBjbGFzcz0nbWZwLWNsb3NlJyBpZD0nY3VzdG9tLWNsb3NlLWJ0bic+PC9idXR0b24+YCxcclxuXHRcdGNhbGxiYWNrczoge1xyXG5cdFx0XHRhamF4Q29udGVudEFkZGVkOiBmdW5jdGlvbiAoKSB7XHJcblx0XHRcdFx0JCgnLnRoYW5rLXBvcHVwX190ZXh0JykuaHRtbChtZXNzYWdlKTtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdH0pO1xyXG59XHJcblxyXG5mdW5jdGlvbiB0b2dnbGUgKGJsb2NrLCB2YWx1ZSwgc3BlZWQpIHtcclxuXHQkKGJsb2NrKS5hbmltYXRlKHtcclxuXHRcdHJpZ2h0OiB2YWx1ZVxyXG5cdH0sIHNwZWVkKTtcclxufVxyXG5cclxuZnVuY3Rpb24gb3BlbkNhcnQgKCkge1xyXG5cdHRvZ2dsZShjYXJ0V3JhcCwgJzBweCcsIDQ1MCk7XHJcblx0c2V0VGltZW91dChmdW5jdGlvbiAoKSB7XHJcblx0XHQkKGNhcnRXcmFwKS5hZGRDbGFzcygnb3BlbicpO1xyXG5cdH0sIDUwMCk7XHJcblx0c2V0VGltZW91dChmdW5jdGlvbiAoKSB7XHJcblx0XHQkKCdib2R5JykuYWRkQ2xhc3MoJ292ZXJmbG93Jyk7XHJcblx0fSwgNTAwKTtcclxuXHRtb2R1bGVMb2FkZXIuaW5pdCgkKGNhcnRXcmFwKSk7XHJcbn1cclxuXHJcbmZ1bmN0aW9uIGNsb3NlQ2FydCAoKSB7XHJcblx0JChjYXJ0V3JhcCkucmVtb3ZlQ2xhc3MoJ29wZW4nKTtcclxuXHRzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcclxuXHRcdHRvZ2dsZShjYXJ0V3JhcCwgJy0xMDAlJywgMzAwKTtcclxuXHR9LCA1MDApO1xyXG5cdCQoJ2JvZHknKS5yZW1vdmVDbGFzcygnb3ZlcmZsb3cnKTtcclxufVxyXG5cclxuZnVuY3Rpb24gY2FydFJlbW92ZSAoZSkge1xyXG5cdGxldCAkaXRlbSA9ICQodGhpcykuY2xvc2VzdCgnLmpzLWNhcnQtaXRlbScpO1xyXG5cdGxldCByZW1vdmUgPSAkKHRoaXMpLmRhdGEoJ3VybCcpO1xyXG5cdGxldCAkY29uZmlybSA9ICRpdGVtLmZpbmQoJy5qcy1yZW1vdmUtY29uZmlybScpO1xyXG5cdGxldCAkY29uZmlybUJ0biA9ICRjb25maXJtLmZpbmQoJy5qcy1jb25maXJtJyk7XHJcblx0bGV0ICR1bkNvbmZpcm1CdG4gPSAkY29uZmlybS5maW5kKCcuanMtdW5jb25maXJtJyk7XHJcblxyXG5cdGlmICgkY29uZmlybS5oYXNDbGFzcygnaXMtc2hvdycpKSB7XHJcblx0XHQkY29uZmlybS5mYWRlT3V0KCdmYXN0JykucmVtb3ZlQ2xhc3MoJ2lzLXNob3cnKTtcclxuXHRcdHJldHVybiBmYWxzZTtcclxuXHR9IGVsc2Uge1xyXG5cdFx0JGNvbmZpcm0uZmFkZUluKCdmYXN0JykuYWRkQ2xhc3MoJ2lzLXNob3cnKTtcclxuXHR9XHJcblxyXG5cdCRjb25maXJtQnRuLm9uKCdjbGljaycsIGZ1bmN0aW9uICgpIHtcclxuXHRcdGlmIChyZW1vdmUpIHtcclxuXHRcdFx0Y2FydFVwZGF0ZSgkaXRlbSwgMCwgcmVtb3ZlKTtcclxuXHRcdH0gZWxzZSB7XHJcblx0XHRcdGNhcnRVcGRhdGUoJGl0ZW0sIDApO1xyXG5cdFx0fVxyXG5cdH0pO1xyXG5cclxuXHQkdW5Db25maXJtQnRuLm9uKCdjbGljaycsIGZ1bmN0aW9uICgpIHtcclxuXHRcdCRjb25maXJtLmZhZGVPdXQoJ2Zhc3QnKS5yZW1vdmVDbGFzcygnaXMtc2hvdycpO1xyXG5cdH0pO1xyXG5cclxuXHQkKGRvY3VtZW50KS5jbGljayhmdW5jdGlvbiAoZSkge1xyXG5cdFx0aWYgKCRjb25maXJtLmhhc0NsYXNzKCdpcy1zaG93JykpIHtcclxuXHRcdFx0aWYgKCEkaXRlbS5pcyhlLnRhcmdldCkgJiYgJGl0ZW0uaGFzKGUudGFyZ2V0KS5sZW5ndGggPT09IDApIHtcclxuXHRcdFx0XHQkY29uZmlybS5mYWRlT3V0KCdmYXN0JykucmVtb3ZlQ2xhc3MoJ2lzLXNob3cnKTtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdH0pO1xyXG59XHJcblxyXG5mdW5jdGlvbiBjYXJ0TWludXMgKGUpIHtcclxuXHRsZXQgJHRoaXMgPSAkKHRoaXMpO1xyXG5cdGxldCAkaXRlbSA9ICR0aGlzLmNsb3Nlc3QoJy5qcy1jYXJ0LWl0ZW0nKTtcclxuXHRsZXQgJGlucHV0ID0gJGl0ZW0uZmluZCgnLmpzLWNhcnQtaW5wdXQnKTtcclxuXHJcblx0bGV0IHRleHQgPSAkaW5wdXQudmFsKCk7XHJcblx0bGV0IG1heCA9ICRpbnB1dC5kYXRhKCdtYXgnKSB8fCAxMDAwMDAwO1xyXG5cdGxldCBjb3VudCA9IDE7XHJcblx0aWYgKCQuaXNOdW1lcmljKHRleHQpKSB7XHJcblx0XHRjb3VudCA9IHBhcnNlSW50KHRleHQpO1xyXG5cdFx0Y291bnQtLTtcclxuXHRcdGNvdW50ID0gKGNvdW50ID4gMSkgPyAoKGNvdW50ID4gcGFyc2VJbnQobWF4KSkgPyBtYXggOiBjb3VudCkgOiAxO1xyXG5cdH1cclxuXHQkaW5wdXQudmFsKGNvdW50KTtcclxuXHJcblx0Y2FydFVwZGF0ZSgkaXRlbSwgY291bnQpO1xyXG59XHJcblxyXG5mdW5jdGlvbiBjYXJ0UGx1cyAoZSkge1xyXG5cdGxldCAkdGhpcyA9ICQodGhpcyk7XHJcblx0bGV0ICRpdGVtID0gJHRoaXMuY2xvc2VzdCgnLmpzLWNhcnQtaXRlbScpO1xyXG5cdGxldCAkaW5wdXQgPSAkaXRlbS5maW5kKCcuanMtY2FydC1pbnB1dCcpO1xyXG5cclxuXHRsZXQgdGV4dCA9ICRpbnB1dC52YWwoKTtcclxuXHRsZXQgbWF4ID0gJGlucHV0LmRhdGEoJ21heCcpIHx8IDEwMDAwMDA7XHJcblx0bGV0IGNvdW50ID0gMTtcclxuXHRpZiAoJC5pc051bWVyaWModGV4dCkpIHtcclxuXHRcdGNvdW50ID0gcGFyc2VJbnQodGV4dCk7XHJcblx0XHRjb3VudCsrO1xyXG5cdFx0Y291bnQgPSAoY291bnQgPiAxKSA/ICgoY291bnQgPiBwYXJzZUludChtYXgpKSA/IG1heCA6IGNvdW50KSA6IDE7XHJcblx0fVxyXG5cdCRpbnB1dC52YWwoY291bnQpO1xyXG5cclxuXHRjYXJ0VXBkYXRlKCRpdGVtLCBjb3VudCk7XHJcbn1cclxuXHJcbmZ1bmN0aW9uIGNhcnRJbnB1dCAoZSkge1xyXG5cdGxldCAkdGhpcyA9ICQodGhpcyk7XHJcblx0bGV0ICRpdGVtID0gJHRoaXMuY2xvc2VzdCgnLmpzLWNhcnQtaXRlbScpO1xyXG5cdGxldCAkaW5wdXQgPSAkaXRlbS5maW5kKCcuanMtY2FydC1pbnB1dCcpO1xyXG5cclxuXHRsZXQgdGV4dCA9ICRpbnB1dC52YWwoKTtcclxuXHRsZXQgbWF4ID0gJGlucHV0LmRhdGEoJ21heCcpIHx8IDEwMDAwMDA7XHJcblx0aWYgKCEkLmlzTnVtZXJpYyh0ZXh0KSB8fCBwYXJzZUludCh0ZXh0KSA8IDEpIHtcclxuXHRcdCRpbnB1dC52YWwoMSk7XHJcblx0fVxyXG5cdGlmIChwYXJzZUludCh0ZXh0KSA+IHBhcnNlSW50KG1heCkpIHtcclxuXHRcdCRpbnB1dC52YWwobWF4KTtcclxuXHR9XHJcblx0bGV0IGNvdW50ID0gcGFyc2VJbnQoJGlucHV0LnZhbCgpKTtcclxuXHJcblx0Y2FydFVwZGF0ZSgkaXRlbSwgY291bnQpO1xyXG59XHJcblxyXG5mdW5jdGlvbiBjYXJ0QWRkIChlKSB7XHJcblx0bGV0ICRpdGVtID0gJCh0aGlzKS5jbG9zZXN0KCcuanMtY2FydC1pdGVtJyk7XHJcblx0bGV0IGNvdW50ID0gJGl0ZW0uZGF0YSgnY291bnQnKSB8fCAxO1xyXG5cclxuXHRjYXJ0VXBkYXRlKCRpdGVtLCBjb3VudCk7XHJcbn1cclxuXHJcbmZ1bmN0aW9uIGNhcnRVcGRhdGUgKCRpdGVtLCBjb3VudCwgcmVtb3ZlID0gbnVsbCkge1xyXG5cdGxldCB1cmwgPSByZW1vdmUgfHwgJGl0ZW0uZGF0YSgndXJsJyk7XHJcblx0bGV0IHN0YXRpY0l0ZW0gPSAkaXRlbS5kYXRhKCdzdGF0aWMnKTtcclxuXHRsZXQgc2l6ZXMgPSAkaXRlbS5kYXRhKCdzaXplcycpO1xyXG5cdGxldCBjb3VudHMgPSAkaXRlbS5kYXRhKCdjb3VudHMnKTtcclxuXHRsZXQgaWQgPSAkaXRlbS5kYXRhKCdpZCcpO1xyXG5cdGxldCBwcmVsb2FkZXJDb250YWluZXIgPSAoc3RhdGljSXRlbSAhPT0gdW5kZWZpbmVkKSA/ICQoJy5qcy1jYXJ0LXN0YXRpYycpIDogJCgnLmNhcnQtaW5uZXInKTtcclxuXHJcblx0aWYgKHVybCAmJiBpZCkge1xyXG5cdFx0bGV0IHByZWxvYWRlciA9IG5ldyBQcmVsb2FkZXIocHJlbG9hZGVyQ29udGFpbmVyKTtcclxuXHRcdHByZWxvYWRlci5zaG93KCk7XHJcblxyXG5cdFx0bGV0IGRhdGEgPSB7XHJcblx0XHRcdGlkOiBpZFxyXG5cdFx0fTtcclxuXHJcblx0XHRpZiAoIXJlbW92ZSkge1xyXG5cdFx0XHRkYXRhLmNvdW50ID0gY291bnQ7XHJcblx0XHR9XHJcblxyXG5cdFx0aWYgKHNpemVzICE9PSB1bmRlZmluZWQpIHtcclxuXHRcdFx0ZGF0YS5zaXplcyA9IHNpemVzO1xyXG5cdFx0fVxyXG5cclxuXHRcdGlmIChjb3VudHMgIT09IHVuZGVmaW5lZCkge1xyXG5cdFx0XHRkYXRhLmNvdW50cyA9IGNvdW50cztcclxuXHRcdH1cclxuXHJcblx0XHQkLmFqYXgoe1xyXG5cdFx0XHR1cmw6IHVybCxcclxuXHRcdFx0dHlwZTogJ3Bvc3QnLFxyXG5cdFx0XHRkYXRhOiBkYXRhLFxyXG5cdFx0XHRkYXRhVHlwZTogJ2pzb24nLFxyXG5cdFx0XHRzdWNjZXNzOiBmdW5jdGlvbiAocmVzcG9uc2UpIHtcclxuXHRcdFx0XHRwcmVsb2FkZXIuaGlkZSgpO1xyXG5cclxuXHRcdFx0XHRpZiAocmVzcG9uc2Uuc3VjY2Vzcykge1xyXG5cdFx0XHRcdFx0aWYgKCQoJy5qcy1jYXJ0LWNvdW50JykubGVuZ3RoICYmIHR5cGVvZiByZXNwb25zZS5jb3VudCAhPT0gJ3VuZGVmaW5lZCcpIHtcclxuXHRcdFx0XHRcdFx0JCgnLmpzLWNhcnQtY291bnQnKS5odG1sKHJlc3BvbnNlLmNvdW50KTtcclxuXHRcdFx0XHRcdH1cclxuXHJcblx0XHRcdFx0XHRpZiAoJCgnLmpzLWNhcnQtcHJpY2UnKS5sZW5ndGggJiYgdHlwZW9mIHJlc3BvbnNlLnByaWNlICE9PSAndW5kZWZpbmVkJykge1xyXG5cdFx0XHRcdFx0XHQkKCcuanMtY2FydC1wcmljZScpLmh0bWwocmVzcG9uc2UucHJpY2UpO1xyXG5cdFx0XHRcdFx0fVxyXG5cclxuXHRcdFx0XHRcdGlmICgkKCcuanMtY2FydC1zdGF0aWMnKS5sZW5ndGggJiYgdHlwZW9mIHJlc3BvbnNlLnN0YXRpYyAhPT0gJ3VuZGVmaW5lZCcpIHtcclxuXHRcdFx0XHRcdFx0JCgnLmpzLWNhcnQtc3RhdGljJykuaHRtbChyZXNwb25zZS5zdGF0aWMpO1xyXG5cclxuXHRcdFx0XHRcdFx0bW9kdWxlTG9hZGVyLmluaXQoJCgnLmpzLWNhcnQtc3RhdGljJykpO1xyXG5cclxuXHRcdFx0XHRcdFx0aWYgKCFyZXNwb25zZS5jb3VudCkge1xyXG5cdFx0XHRcdFx0XHRcdCQoJ1tkYXRhLXRvdGFsXScpLmFkZENsYXNzKCdfaGlkZScpO1xyXG5cdFx0XHRcdFx0XHRcdCQoJ1tkYXRhLXRvdGFsLWVtcHR5XScpLnJlbW92ZUNsYXNzKCdfaGlkZScpO1xyXG5cdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHR9XHJcblxyXG5cdFx0XHRcdFx0aWYgKCQoJy5qcy1jYXJ0JykubGVuZ3RoICYmIHR5cGVvZiByZXNwb25zZS5odG1sICE9PSAndW5kZWZpbmVkJykge1xyXG5cdFx0XHRcdFx0XHRsZXQgJGh0bWwgPSAkKHJlc3BvbnNlLmh0bWwpO1xyXG5cdFx0XHRcdFx0XHQkKCcuanMtY2FydCcpLmh0bWwoJGh0bWwpO1xyXG5cclxuXHRcdFx0XHRcdFx0bW9kdWxlTG9hZGVyLmluaXQoJCgnLmpzLWNhcnQnKSk7XHJcblxyXG5cdFx0XHRcdFx0XHRpZiAoIXN0YXRpY0l0ZW0pIHtcclxuXHRcdFx0XHRcdFx0XHRvcGVuQ2FydCgpO1xyXG5cdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHR9XHJcblxyXG5cdFx0XHRcdFx0aWYgKHJlc3BvbnNlLnByaWNlKSB7XHJcblx0XHRcdFx0XHRcdCQoJy5oZWFkZXItY2FydF9fc3VtbSBzcGFuIGInKS50ZXh0KHJlc3BvbnNlLnByaWNlKTtcclxuXHRcdFx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0XHRcdCQoJy5oZWFkZXItY2FydF9fc3VtbSBzcGFuIGInKS50ZXh0KDApO1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0XHRpZiAocmVzcG9uc2UucmVzcG9uc2UpIHtcclxuXHRcdFx0XHRcdFx0c2hvd01lc3NhZ2UocmVzcG9uc2UucmVzcG9uc2UpO1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cdFx0fSk7XHJcblx0fVxyXG59XHJcblxyXG5mdW5jdGlvbiBjYXJ0Q2xlYW4gKGUpIHtcclxuXHRsZXQgdXJsID0gJCh0aGlzKS5kYXRhKCd1cmwnKTtcclxuXHRsZXQgJGl0ZW0gPSAkKHRoaXMpLmNsb3Nlc3QoJy5jYXJ0LXdpbmRvd19fdG9wJyk7XHJcblxyXG5cdGxldCAkY29uZmlybSA9ICRpdGVtLmZpbmQoJy5qcy1yZW1vdmUtY29uZmlybScpO1xyXG5cdGxldCAkY29uZmlybUJ0biA9ICRjb25maXJtLmZpbmQoJy5qcy1jb25maXJtJyk7XHJcblx0bGV0ICR1bkNvbmZpcm1CdG4gPSAkY29uZmlybS5maW5kKCcuanMtdW5jb25maXJtJyk7XHJcblxyXG5cdGlmICgkY29uZmlybS5oYXNDbGFzcygnaXMtc2hvdycpKSB7XHJcblx0XHQkY29uZmlybS5mYWRlT3V0KCdmYXN0JykucmVtb3ZlQ2xhc3MoJ2lzLXNob3cnKTtcclxuXHRcdHJldHVybiBmYWxzZTtcclxuXHR9IGVsc2Uge1xyXG5cdFx0JGNvbmZpcm0uZmFkZUluKCdmYXN0JykuYWRkQ2xhc3MoJ2lzLXNob3cnKTtcclxuXHR9XHJcblxyXG5cdCRjb25maXJtQnRuLm9uKCdjbGljaycsIGZ1bmN0aW9uICgpIHtcclxuXHRcdGlmICh1cmwpIHtcclxuXHRcdFx0JC5hamF4KHtcclxuXHRcdFx0XHR1cmw6IHVybCxcclxuXHRcdFx0XHR0eXBlOiAncG9zdCcsXHJcblx0XHRcdFx0ZGF0YVR5cGU6ICdqc29uJyxcclxuXHRcdFx0XHRzdWNjZXNzOiBmdW5jdGlvbiAocmVzcG9uc2UpIHtcclxuXHRcdFx0XHRcdGlmIChyZXNwb25zZS5zdWNjZXNzKSB7XHJcblx0XHRcdFx0XHRcdGlmICgkKCcuanMtY2FydC1jb3VudCcpLmxlbmd0aCAmJiB0eXBlb2YgcmVzcG9uc2UuY291bnQgIT09ICd1bmRlZmluZWQnKSB7XHJcblx0XHRcdFx0XHRcdFx0JCgnLmpzLWNhcnQtY291bnQnKS5odG1sKHJlc3BvbnNlLmNvdW50KTtcclxuXHRcdFx0XHRcdFx0fVxyXG5cclxuXHRcdFx0XHRcdFx0aWYgKCQoJy5qcy1jYXJ0LXByaWNlJykubGVuZ3RoICYmIHR5cGVvZiByZXNwb25zZS5wcmljZSAhPT0gJ3VuZGVmaW5lZCcpIHtcclxuXHRcdFx0XHRcdFx0XHQkKCcuanMtY2FydC1wcmljZScpLmh0bWwocmVzcG9uc2UucHJpY2UpO1xyXG5cdFx0XHRcdFx0XHR9XHJcblxyXG5cdFx0XHRcdFx0XHRpZiAoJCgnLmpzLWNhcnQtc3RhdGljJykubGVuZ3RoICYmIHR5cGVvZiByZXNwb25zZS5zdGF0aWMgIT09ICd1bmRlZmluZWQnKSB7XHJcblx0XHRcdFx0XHRcdFx0JCgnLmpzLWNhcnQtc3RhdGljJykuaHRtbChyZXNwb25zZS5zdGF0aWMpO1xyXG5cclxuXHRcdFx0XHRcdFx0XHRtb2R1bGVMb2FkZXIuaW5pdCgkKCcuanMtY2FydC1zdGF0aWMnKSk7XHJcblxyXG5cdFx0XHRcdFx0XHRcdGlmICghcmVzcG9uc2UuY291bnQpIHtcclxuXHRcdFx0XHRcdFx0XHRcdCQoJ1tkYXRhLXRvdGFsXScpLmFkZENsYXNzKCdfaGlkZScpO1xyXG5cdFx0XHRcdFx0XHRcdFx0JCgnW2RhdGEtdG90YWwtZW1wdHldJykucmVtb3ZlQ2xhc3MoJ19oaWRlJyk7XHJcblx0XHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0XHR9XHJcblxyXG5cdFx0XHRcdFx0XHRpZiAoJCgnLmpzLWNhcnQnKS5sZW5ndGggJiYgdHlwZW9mIHJlc3BvbnNlLmh0bWwgIT09ICd1bmRlZmluZWQnKSB7XHJcblx0XHRcdFx0XHRcdFx0bGV0ICRodG1sID0gJChyZXNwb25zZS5odG1sKTtcclxuXHRcdFx0XHRcdFx0XHQkKCcuanMtY2FydCcpLmh0bWwoJGh0bWwpO1xyXG5cclxuXHRcdFx0XHRcdFx0XHRtb2R1bGVMb2FkZXIuaW5pdCgkKCcuanMtY2FydCcpKTtcclxuXHJcblx0XHRcdFx0XHRcdFx0b3BlbkNhcnQoKTtcclxuXHRcdFx0XHRcdFx0fVxyXG5cclxuXHRcdFx0XHRcdFx0aWYgKHJlc3BvbnNlLnRvdGFsUHJpY2UpIHtcclxuXHRcdFx0XHRcdFx0XHQkKCcuanMtdG90YWwtcHJpY2UnKS50ZXh0KHJlc3BvbnNlLnRvdGFsUHJpY2UudG9GaXhlZCgyKSk7XHJcblx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0XHRcdGlmIChyZXNwb25zZS5yZXNwb25zZSkge1xyXG5cdFx0XHRcdFx0XHRcdHNob3dNZXNzYWdlKHJlc3BvbnNlLnJlc3BvbnNlKTtcclxuXHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdH1cclxuXHRcdFx0fSk7XHJcblx0XHR9XHJcblx0fSk7XHJcblxyXG5cdCR1bkNvbmZpcm1CdG4ub24oJ2NsaWNrJywgZnVuY3Rpb24gKCkge1xyXG5cdFx0JGNvbmZpcm0uZmFkZU91dCgnZmFzdCcpLnJlbW92ZUNsYXNzKCdpcy1zaG93Jyk7XHJcblx0fSk7XHJcblxyXG5cdCQoZG9jdW1lbnQpLmNsaWNrKGZ1bmN0aW9uIChlKSB7XHJcblx0XHRpZiAoJGNvbmZpcm0uaGFzQ2xhc3MoJ2lzLXNob3cnKSkge1xyXG5cdFx0XHRpZiAoISRpdGVtLmlzKGUudGFyZ2V0KSAmJiAkaXRlbS5oYXMoZS50YXJnZXQpLmxlbmd0aCA9PT0gMCkge1xyXG5cdFx0XHRcdCRjb25maXJtLmZhZGVPdXQoJ2Zhc3QnKS5yZW1vdmVDbGFzcygnaXMtc2hvdycpO1xyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0fSk7XHJcbn1cclxuXHJcbmZ1bmN0aW9uIGNhcnRTaG93ICh0YXJnZXQpIHtcclxuXHRsZXQgdXJsID0gJCh0aGlzKS5kYXRhKCd1cmwnKTtcclxuXHRsZXQgZGF0YSA9IHt9O1xyXG5cdGxldCBiaWdQb3B1cCA9ICQoJy5zaXplcy1iaWctcG9wdXAnKS5sZW5ndGg7XHJcblxyXG5cdGlmICh1cmwpIHtcclxuXHRcdCQuYWpheCh7XHJcblx0XHRcdHVybDogdXJsLFxyXG5cdFx0XHR0eXBlOiAncG9zdCcsXHJcblx0XHRcdGRhdGE6IGRhdGEsXHJcblx0XHRcdGRhdGFUeXBlOiAnanNvbicsXHJcblx0XHRcdHN1Y2Nlc3M6IGZ1bmN0aW9uIChyZXNwb25zZSkge1xyXG5cdFx0XHRcdGlmIChyZXNwb25zZS5zdWNjZXNzKSB7XHJcblx0XHRcdFx0XHRpZiAoJCgnLmpzLWNhcnQtY291bnQnKS5sZW5ndGggJiYgdHlwZW9mIHJlc3BvbnNlLmNvdW50ICE9PSAndW5kZWZpbmVkJykge1xyXG5cdFx0XHRcdFx0XHQkKCcuanMtY2FydC1jb3VudCcpLmh0bWwocmVzcG9uc2UuY291bnQpO1xyXG5cdFx0XHRcdFx0fVxyXG5cclxuXHRcdFx0XHRcdGlmICgkKCcuanMtY2FydC1wcmljZScpLmxlbmd0aCAmJiB0eXBlb2YgcmVzcG9uc2UucHJpY2UgIT09ICd1bmRlZmluZWQnKSB7XHJcblx0XHRcdFx0XHRcdCQoJy5qcy1jYXJ0LXByaWNlJykuaHRtbChyZXNwb25zZS5wcmljZSk7XHJcblx0XHRcdFx0XHR9XHJcblxyXG5cdFx0XHRcdFx0aWYgKCQoJy5qcy1jYXJ0LXN0YXRpYycpLmxlbmd0aCAmJiB0eXBlb2YgcmVzcG9uc2Uuc3RhdGljICE9PSAndW5kZWZpbmVkJykge1xyXG5cdFx0XHRcdFx0XHQkKCcuanMtY2FydC1zdGF0aWMnKS5odG1sKHJlc3BvbnNlLnN0YXRpYyk7XHJcblxyXG5cdFx0XHRcdFx0XHRtb2R1bGVMb2FkZXIuaW5pdCgkKCcuanMtY2FydC1zdGF0aWMnKSk7XHJcblxyXG5cdFx0XHRcdFx0XHRpZiAoIXJlc3BvbnNlLmNvdW50KSB7XHJcblx0XHRcdFx0XHRcdFx0JCgnW2RhdGEtdG90YWxdJykuYWRkQ2xhc3MoJ19oaWRlJyk7XHJcblx0XHRcdFx0XHRcdFx0JCgnW2RhdGEtdG90YWwtZW1wdHldJykucmVtb3ZlQ2xhc3MoJ19oaWRlJyk7XHJcblx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdH1cclxuXHJcblx0XHRcdFx0XHRpZiAoJCgnLmpzLWNhcnQnKS5sZW5ndGggJiYgdHlwZW9mIHJlc3BvbnNlLmh0bWwgIT09ICd1bmRlZmluZWQnKSB7XHJcblx0XHRcdFx0XHRcdGxldCAkaHRtbCA9ICQocmVzcG9uc2UuaHRtbCk7XHJcblx0XHRcdFx0XHRcdCQoJy5qcy1jYXJ0JykuaHRtbCgkaHRtbCk7XHJcblxyXG5cdFx0XHRcdFx0XHRtb2R1bGVMb2FkZXIuaW5pdCgkKCcuanMtY2FydCcpKTtcclxuXHJcblx0XHRcdFx0XHRcdGlmICghYmlnUG9wdXApIHtcclxuXHRcdFx0XHRcdFx0XHRvcGVuQ2FydCgpO1xyXG5cdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHR9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZS5wcmljZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAkKCcuaGVhZGVyLWNhcnRfX3N1bW0gc3BhbiBiJykudGV4dChyZXNwb25zZS5wcmljZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuXHRcdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdFx0aWYgKHJlc3BvbnNlLnJlc3BvbnNlKSB7XHJcblx0XHRcdFx0XHRcdHNob3dNZXNzYWdlKHJlc3BvbnNlLnJlc3BvbnNlKTtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHR9XHJcblx0XHRcdH1cclxuXHRcdH0pO1xyXG5cdH1cclxufVxyXG5cclxuZnVuY3Rpb24gY2FydE9yZGVyIChlKSB7XHJcblx0bGV0IHVybCA9ICQodGhpcykuZGF0YSgndXJsJyk7XHJcblx0bGV0ICRpdGVtID0gJCh0aGlzKS5jbG9zZXN0KCcuanMtY2FydC1pdGVtJyk7XHJcblx0bGV0IGlkID0gJGl0ZW0uZGF0YSgnaWQnKTtcclxuXHRsZXQgY291bnQgPSAoJGl0ZW0uZGF0YSgnY291bnQnKSAhPT0gdW5kZWZpbmVkKSA/ICRpdGVtLmRhdGEoJ2NvdW50JykgOiAxO1xyXG5cclxuXHRsZXQgZGF0YSA9IHtcclxuXHRcdGlkOiBpZCxcclxuXHRcdGNvdW50OiBjb3VudFxyXG5cdH07XHJcblxyXG5cdCQuYWpheCh7XHJcblx0XHR1cmw6IHVybCxcclxuXHRcdHR5cGU6ICdwb3N0JyxcclxuXHRcdGRhdGE6IGRhdGEsXHJcblx0XHRkYXRhVHlwZTogJ2pzb24nLFxyXG5cdFx0c3VjY2VzczogZnVuY3Rpb24gKHJlc3BvbnNlKSB7XHJcblx0XHRcdGlmIChyZXNwb25zZS5zdWNjZXNzKSB7XHJcblx0XHRcdFx0aWYgKCQoJy5qcy1jYXJ0LWNvdW50JykubGVuZ3RoICYmIHR5cGVvZiByZXNwb25zZS5jb3VudCAhPT0gJ3VuZGVmaW5lZCcpIHtcclxuXHRcdFx0XHRcdCQoJy5qcy1jYXJ0LWNvdW50JykuaHRtbChyZXNwb25zZS5jb3VudCk7XHJcblx0XHRcdFx0fVxyXG5cclxuXHRcdFx0XHRpZiAoJCgnLmpzLWNhcnQtcHJpY2UnKS5sZW5ndGggJiYgdHlwZW9mIHJlc3BvbnNlLnByaWNlICE9PSAndW5kZWZpbmVkJykge1xyXG5cdFx0XHRcdFx0JCgnLmpzLWNhcnQtcHJpY2UnKS5odG1sKHJlc3BvbnNlLnByaWNlKTtcclxuXHRcdFx0XHR9XHJcblxyXG5cdFx0XHRcdGlmICgkKCcuanMtY2FydC1zdGF0aWMnKS5sZW5ndGggJiYgdHlwZW9mIHJlc3BvbnNlLnN0YXRpYyAhPT0gJ3VuZGVmaW5lZCcpIHtcclxuXHRcdFx0XHRcdCQoJy5qcy1jYXJ0LXN0YXRpYycpLmh0bWwocmVzcG9uc2Uuc3RhdGljKTtcclxuXHJcblx0XHRcdFx0XHRtb2R1bGVMb2FkZXIuaW5pdCgkKCcuanMtY2FydC1zdGF0aWMnKSk7XHJcblxyXG5cdFx0XHRcdFx0aWYgKCFyZXNwb25zZS5jb3VudCkge1xyXG5cdFx0XHRcdFx0XHQkKCdbZGF0YS10b3RhbF0nKS5hZGRDbGFzcygnX2hpZGUnKTtcclxuXHRcdFx0XHRcdFx0JCgnW2RhdGEtdG90YWwtZW1wdHldJykucmVtb3ZlQ2xhc3MoJ19oaWRlJyk7XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0fVxyXG5cclxuXHRcdFx0XHRpZiAoJCgnLmpzLWNhcnQnKS5sZW5ndGggJiYgdHlwZW9mIHJlc3BvbnNlLmh0bWwgIT09ICd1bmRlZmluZWQnKSB7XHJcblx0XHRcdFx0XHRsZXQgJGh0bWwgPSAkKHJlc3BvbnNlLmh0bWwpO1xyXG5cdFx0XHRcdFx0JCgnLmpzLWNhcnQnKS5odG1sKCRodG1sKTtcclxuXHJcblx0XHRcdFx0XHRtb2R1bGVMb2FkZXIuaW5pdCgkKCcuanMtY2FydCcpKTtcclxuXHJcblx0XHRcdFx0XHRvcGVuQ2FydCgpO1xyXG5cdFx0XHRcdH1cclxuXHJcblx0XHRcdFx0aWYgKHJlc3BvbnNlLnRvdGFsUHJpY2UpIHtcclxuXHRcdFx0XHRcdCQoJy5qcy10b3RhbC1wcmljZScpLnRleHQocmVzcG9uc2UudG90YWxQcmljZS50b0ZpeGVkKDIpKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0aWYgKHJlc3BvbnNlLnJlc3BvbnNlKSB7XHJcblx0XHRcdFx0XHRzaG93TWVzc2FnZShyZXNwb25zZS5yZXNwb25zZSk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0fSk7XHJcbn1cclxuXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuLy8gUHVibGljXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbmxldCBjYXJ0SW5pdCA9IGZ1bmN0aW9uICgkY29udGFpbmVyID0gJCgnYm9keScpKSB7XHJcblx0JGNvbnRhaW5lci5vbignY2xpY2suY2FydCcsICcuanMtY2FydC1yZW1vdmUnLCBjYXJ0UmVtb3ZlKTtcclxuXHQkY29udGFpbmVyLm9uKCdjbGljay5jYXJ0JywgJy5qcy1jYXJ0LW1pbnVzJywgY2FydE1pbnVzKTtcclxuXHQkY29udGFpbmVyLm9uKCdjbGljay5jYXJ0JywgJy5qcy1jYXJ0LXBsdXMnLCBjYXJ0UGx1cyk7XHJcblx0JGNvbnRhaW5lci5vbigna2V5dXAuY2FydCcsICcuanMtY2FydC1pbnB1dCcsIGNhcnRJbnB1dCk7XHJcblx0JGNvbnRhaW5lci5vbignY2xpY2suY2FydCcsICcuanMtY2FydC1hZGQnLCBjYXJ0QWRkKTtcclxuXHQkY29udGFpbmVyLm9uKCdjbGljay5jYXJ0JywgJy5qcy1jYXJ0LWNsZWFuJywgY2FydENsZWFuKTtcclxuXHQkY29udGFpbmVyLm9uKCdjbGljaycsICcuanMtY2FydC1yZWRhY3QnLCBjYXJ0U2hvdyk7XHJcblx0JGNvbnRhaW5lci5vbignY2xpY2snLCAnLmpzLWNhcnQtb3JkZXInLCBjYXJ0T3JkZXIpO1xyXG5cdCRjb250YWluZXIub24oJ2NsaWNrJywgb3BlbkNhcnRCdG4sIGNhcnRTaG93KTtcclxuXHQkY29udGFpbmVyLm9uKCdjbGljaycsIGNsb3NlQ2FydEJ0biwgY2xvc2VDYXJ0KTtcclxufTtcclxuXHJcbmZ1bmN0aW9uIGNhcnREZWZhdWx0Q2xvc2UgKCkge1xyXG5cdCQoZG9jdW1lbnQpLmNsaWNrKGZ1bmN0aW9uIChlKSB7XHJcblx0XHRpZiAoJChjYXJ0V3JhcCkuaGFzQ2xhc3MoJ29wZW4nKSkge1xyXG5cdFx0XHRpZiAoISRjYXJ0SW5uZXIuaXMoZS50YXJnZXQpICYmICRjYXJ0SW5uZXIuaGFzKGUudGFyZ2V0KS5sZW5ndGggPT09IDAgJiYgISQoJy5tZnAtY29udGFpbmVyJykuaXMoZS50YXJnZXQpICYmICQoJy5tZnAtY29udGFpbmVyJykuaGFzKGUudGFyZ2V0KS5sZW5ndGggPT09IDApIHtcclxuXHRcdFx0XHRjbG9zZUNhcnQoKTtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdH0pO1xyXG59XHJcblxyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbi8vIEV4cG9ydHNcclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuZXhwb3J0IHtjYXJ0SW5pdCwgY2FydERlZmF1bHRDbG9zZX07XHJcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL19IVE1ML3NyYy9qcy9fbW9kdWxlcy9jYXJ0LmpzIiwiJ3VzZSBzdHJpY3QnO1xyXG5cclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4vLyBQdWJsaWNcclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuZnVuY3Rpb24gc2VsZWN0V29yZCAoJHNlbGVjdG9yLCBzZWxlY3RDbGFzcywgd29yZCkge1xyXG5cdCRzZWxlY3RvciA9ICRzZWxlY3RvciB8fCAkKCcuanMtc2VsZWN0Jyk7XHJcblx0c2VsZWN0Q2xhc3MgPSBzZWxlY3RDbGFzcyB8fCAnaXMtc2VsZWN0JztcclxuXHR3b3JkID0gd29yZCB8fCAkKCcuanMtc2VsZWN0LXdvcmQnKS5kYXRhKCdzZWxlY3QnKTtcclxuXHJcblx0aWYgKCRzZWxlY3Rvci5sZW5ndGggJiYgd29yZCkge1xyXG5cdFx0JHNlbGVjdG9yLmVhY2goZnVuY3Rpb24gKCkge1xyXG5cdFx0XHRsZXQgJHRoaXMgPSAkKHRoaXMpO1xyXG5cdFx0XHRsZXQgaHRtbCA9ICR0aGlzLmh0bWwoKTtcclxuXHRcdFx0JHRoaXMuaHRtbChodG1sLnJlcGxhY2UobmV3IFJlZ0V4cCh3b3JkLCAnaWcnKSwgJzxzcGFuIGNsYXNzPVwiJyArIHNlbGVjdENsYXNzICsgJ1wiPiQmPC9zcGFuPicpKTtcclxuXHRcdH0pO1xyXG5cdH1cclxufVxyXG5cclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4vLyBFeHBvcnRzXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbmV4cG9ydCB7c2VsZWN0V29yZH07XHJcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL19IVE1ML3NyYy9qcy9fbW9kdWxlcy9zZWxlY3Qtd29yZC5qcyIsIid1c2Ugc3RyaWN0JztcclxuXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuLy8gUHJpdmF0ZVxyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG5sZXQgJGhlYWRlciA9ICQoJy5oZWFkZXInKTsgLy8g0JzQtdC90Y5cclxubGV0ICRoZWFkZXJTZWFyY2ggPSAkKCcuaGVhZGVyX19zZWFyY2gtd3JhcCcpO1xyXG5sZXQgJGhlYWRlclNlYXJjaFJlc3VsdCA9ICQoJy5oZWFkZXJfX3NlYXJjaC1yZXN1bHRzJyk7XHJcbmxldCAkaGVhZGVyU2VhcmNoRW1wdHkgPSAkKCcuaGVhZGVyX19zZWFyY2gtZW1wdHknKTtcclxubGV0ICRoZWFkZXJTZWFyY2hJbnB1dCA9ICQoJy5oZWFkZXJfX3NlYXJjaC1pbnB1dCcpO1xyXG5sZXQgc2VhcmNoVXJsID0gJGhlYWRlclNlYXJjaC5kYXRhKCd1cmwnKTtcclxubGV0IHRvcCA9ICRoZWFkZXIub3V0ZXJIZWlnaHQoKTtcclxuXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuLy8gUHVibGljXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbmZ1bmN0aW9uIGhlYWRlclNlYXJjaFRvZ2dsZSAoKSB7XHJcblx0JCgnYm9keScpLm9uKCdjbGljaycsICcuaGVhZGVyLXNlYXJjaF9fYnRuJywgZnVuY3Rpb24gKCkge1xyXG5cdFx0JGhlYWRlclNlYXJjaC5jc3Moe1xyXG5cdFx0XHQndG9wJzogdG9wICsgJ3B4JyxcclxuXHRcdFx0J2hlaWdodCc6ICdjYWxjKDEwMHZoIC0gJyArIHRvcCArICdweCknXHJcblx0XHR9KTtcclxuXHRcdCQodGhpcykudG9nZ2xlQ2xhc3MoJ29wZW4nKTtcclxuXHJcblx0XHRzZXRUaW1lb3V0KGZ1bmN0aW9uICgpe1xyXG5cdFx0XHQkKCcuaGVhZGVyX19zZWFyY2gtaW5wdXQnKS5mb2N1cygpO1xyXG5cdFx0fSwgNTAwKVxyXG5cclxuXHRcdCRoZWFkZXJTZWFyY2guc2xpZGVUb2dnbGUoJ2Zhc3QnKTtcclxuXHJcblx0XHRpZiAoJGhlYWRlci5oYXNDbGFzcygnb3BlbicpKSB7XHJcblx0XHRcdCRoZWFkZXIudG9nZ2xlQ2xhc3MoJ29wZW4nKTtcclxuXHRcdFx0JGhlYWRlclNlYXJjaElucHV0LnZhbCgnJyk7XHJcblx0XHRcdCRoZWFkZXJTZWFyY2hSZXN1bHQucmVtb3ZlQ2xhc3MoJ2lzLXNob3cnKTtcclxuXHRcdH0gZWxzZSB7XHJcblx0XHRcdHNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xyXG5cdFx0XHRcdCRoZWFkZXIudG9nZ2xlQ2xhc3MoJ29wZW4nKTtcclxuXHRcdFx0fSwgNDAwKTtcclxuXHRcdH1cclxuXHR9KTtcclxufVxyXG5cclxuZnVuY3Rpb24gc2VhcmNoQXV0b2NvbXBsZXRlICgpIHtcclxuXHRpZiAoISRoZWFkZXJTZWFyY2hJbnB1dC5sZW5ndGgpIHtcclxuXHRcdHJldHVybiBmYWxzZTtcclxuXHR9XHJcblxyXG5cdCRoZWFkZXJTZWFyY2hJbnB1dC5vbigna2V5dXAnLCBmdW5jdGlvbiAoZSkge1xyXG5cdFx0bGV0ICR0aGlzID0gJCh0aGlzKTtcclxuXHRcdGxldCB0ZXh0ID0gJHRoaXMudmFsKCk7XHJcblxyXG5cdFx0aWYgKHRleHQubGVuZ3RoID49IDIpIHtcclxuXHRcdFx0JC5hamF4KHtcclxuXHRcdFx0XHR1cmw6IHNlYXJjaFVybCxcclxuXHRcdFx0XHRkYXRhOiB0ZXh0LFxyXG5cdFx0XHRcdGRhdGFUeXBlOiAnanNvbicsXHJcblx0XHRcdFx0c3VjY2VzczogZnVuY3Rpb24gKHJlc3BvbnNlKSB7XHJcblx0XHRcdFx0XHRpZiAocmVzcG9uc2Uuc3VjY2Vzcykge1xyXG5cdFx0XHRcdFx0XHRsZXQgaHRtbCA9ICcnO1xyXG5cdFx0XHRcdFx0XHRpZiAocmVzcG9uc2UuaXRlbXMubGVuZ3RoID4gMCkge1xyXG5cdFx0XHRcdFx0XHRcdGZvciAobGV0IGkgPSAwOyBpIDwgcmVzcG9uc2UuaXRlbXMubGVuZ3RoOyBpKyspIHtcclxuXHRcdFx0XHRcdFx0XHRcdGh0bWwgKz1cclxuXHRcdFx0XHRcdFx0XHRcdFx0YDxkaXYgY2xhc3M9XCJzZWFyY2gtcmVzdWx0X19pdGVtXCI+YCArXHJcblx0XHRcdFx0XHRcdFx0XHRcdChyZXNwb25zZS5pdGVtc1tpXS5pbWcgPyBgPGltZyBjbGFzcz1cInNlYXJjaC1yZXN1bHRfX2ltZ1wiIHNyYz1cImAgKyByZXNwb25zZS5pdGVtc1tpXS5pbWcgKyBgXCI+YCA6ICcnKSArXHJcblx0XHRcdFx0XHRcdFx0XHRcdGA8ZGl2IGNsYXNzPVwic2VhcmNoLXJlc3VsdF9faW5mb1wiPmAgK1xyXG5cdFx0XHRcdFx0XHRcdFx0XHQocmVzcG9uc2UuaXRlbXNbaV0uY29kZSA/IGA8c3BhbiBjbGFzcz1cInNlYXJjaC1yZXN1bHRfX2NvZGVcIj7QmtC+0LQg0YLQvtCy0LDRgNCwOiA8c3BhbiBjbGFzcz1cImRhcmtcIj5gICsgcmVzcG9uc2UuaXRlbXNbaV0uY29kZSArIGA8L3NwYW4+IDwvc3Bhbj5gIDogJycpICtcclxuXHRcdFx0XHRcdFx0XHRcdFx0YDxhIGNsYXNzPVwic2VhcmNoLXJlc3VsdF9fbmFtZVwiIGhyZWY9XCJgICsgcmVzcG9uc2UuaXRlbXNbaV0uaHJlZiArIGBcIj5gICsgcmVzcG9uc2UuaXRlbXNbaV0ubmFtZSArIGA8L2E+YCArXHJcblx0XHRcdFx0XHRcdFx0XHRcdChyZXNwb25zZS5pdGVtc1tpXS5wcmljZSA/IGA8c3BhbiBjbGFzcz1cInNlYXJjaC1yZXN1bHRfX3ByaWNlXCI+YCArIHJlc3BvbnNlLml0ZW1zW2ldLnByaWNlICsgYDwvc3Bhbj5gIDogJycpICtcclxuXHRcdFx0XHRcdFx0XHRcdFx0YDwvZGl2PmAgK1xyXG5cdFx0XHRcdFx0XHRcdFx0XHRgPC9kaXY+YDtcclxuXHRcdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRcdFx0JCgnLmhlYWRlcl9fc2VhcmNoLXJlc3VsdHMgLm1DU0JfY29udGFpbmVyJykuaHRtbChodG1sKTtcclxuXHRcdFx0XHRcdFx0XHQkaGVhZGVyU2VhcmNoUmVzdWx0LmFkZENsYXNzKCdpcy1zaG93Jyk7XHJcblx0XHRcdFx0XHRcdFx0JGhlYWRlclNlYXJjaEVtcHR5LnJlbW92ZUNsYXNzKCdpcy1zaG93Jyk7XHJcblx0XHRcdFx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0XHRcdFx0JGhlYWRlclNlYXJjaEVtcHR5LmFkZENsYXNzKCdpcy1zaG93Jyk7XHJcblx0XHRcdFx0XHRcdFx0JGhlYWRlclNlYXJjaFJlc3VsdC5yZW1vdmVDbGFzcygnaXMtc2hvdycpO1xyXG5cdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRpZiAocmVzcG9uc2UuZXJyb3IpIHtcclxuXHRcdFx0XHRcdFx0Y29uc29sZS5sb2coJ2Vycm9yJyk7XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9KTtcclxuXHRcdH0gZWxzZSB7XHJcblx0XHRcdCRoZWFkZXJTZWFyY2hSZXN1bHQucmVtb3ZlQ2xhc3MoJ2lzLXNob3cnKTtcclxuXHRcdFx0JGhlYWRlclNlYXJjaEVtcHR5LnJlbW92ZUNsYXNzKCdpcy1zaG93Jyk7XHJcblx0XHR9XHJcblx0fSk7XHJcbn1cclxuXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuLy8gRXhwb3J0c1xyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG5leHBvcnQge2hlYWRlclNlYXJjaFRvZ2dsZSwgc2VhcmNoQXV0b2NvbXBsZXRlfTtcclxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vX0hUTUwvc3JjL2pzL19tb2R1bGVzL2hlYWRlci1zZWFyY2guanMiLCIndXNlIHN0cmljdCc7XHJcblxyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbi8vIEltcG9ydHNcclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuaW1wb3J0ICcuLi9fdmVuZG9ycy90aHJlZXNpeHR5JztcclxuXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuLy8gUHJpdmF0ZVxyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG5sZXQgdGhyZWVzaXh0eUZsYWcgPSBmYWxzZTtcclxubGV0ICRwcm9kdWN0SW1hZ2UgPSAkKCcucHJvZHVjdF9fZ2FsbGVyeScpO1xyXG5sZXQgJHByb2R1Y3QzNjAgPSAkKCcucHJvZHVjdF9fZ2FsbGVyeS0zNjAnKTtcclxuXHJcbmZ1bmN0aW9uIHRocmVlc2l4dHlJbml0ICgpIHtcclxuXHQkKCcuanMtdGhyZWVzaXh0eScpLmVhY2goZnVuY3Rpb24gKGluZGV4LCBlbCkge1xyXG5cdFx0bGV0ICR0aGlzID0gJChlbCk7XHJcblx0XHRsZXQgdG90YWxGcmFtZXMgPSAoJHRoaXMuYXR0cignZGF0YS10b3RhbEZyYW1lcycpKSA/IHBhcnNlSW50KCR0aGlzLmF0dHIoJ2RhdGEtdG90YWxGcmFtZXMnKSkgOiAxODA7XHJcblx0XHRsZXQgaW1hZ2VQYXRoID0gKCR0aGlzLmF0dHIoJ2RhdGEtaW1hZ2VQYXRoJykpID8gJHRoaXMuYXR0cignZGF0YS1pbWFnZVBhdGgnKSA6ICdpbWFnZXMvMzYwLyc7XHJcblx0XHRsZXQgZXh0ID0gKCR0aGlzLmF0dHIoJ2RhdGEtZXh0JykpID8gJHRoaXMuYXR0cignZGF0YS1leHQnKSA6ICcucG5nJztcclxuXHRcdGxldCBmaWxlUHJlZml4ID0gKCR0aGlzLmF0dHIoJ2RhdGEtZmlsZVByZWZpeCcpKSA/ICR0aGlzLmF0dHIoJ2RhdGEtZmlsZVByZWZpeCcpIDogJyc7XHJcblxyXG5cdFx0bGV0IHRzID0gJHRoaXMuVGhyZWVTaXh0eSh7XHJcblx0XHRcdGN1cnJlbnRGcmFtZTogMSxcclxuXHRcdFx0aW1nTGlzdDogJy50aHJlZXNpeHR5X19pbWFnZXMnLFxyXG5cdFx0XHRwcm9ncmVzczogJy50aHJlZXNpeHR5X19zcGlubmVyJyxcclxuXHRcdFx0bmF2aWdhdGlvbjogZmFsc2UsXHJcblx0XHRcdHRvdGFsRnJhbWVzOiB0b3RhbEZyYW1lcyxcclxuXHRcdFx0Ly8gZW5kRnJhbWU6IGVuZEZyYW1lLFxyXG5cdFx0XHRpbWFnZVBhdGg6IGltYWdlUGF0aCxcclxuXHRcdFx0ZXh0OiBleHQsXHJcblx0XHRcdGZpbGVQcmVmaXg6IGZpbGVQcmVmaXgsXHJcblx0XHRcdHNwZWVkTXVsdGlwbGllcjogLTcsXHJcblx0XHRcdC8vIGF1dG9wbGF5RGlyZWN0aW9uOiAtMSxcclxuXHRcdFx0b25SZWFkeTogZnVuY3Rpb24gKCkge1xyXG5cdFx0XHRcdCQoJy5qcy10aHJlZXNpeHR5LWJ1dHRvbnMnKS5hZGRDbGFzcygnaXMtc2hvdycpO1xyXG5cclxuXHRcdFx0XHQkdGhpcy5maW5kKCcuanMtdGhyZWVzaXh0eS1uZXh0Jykub24oJ2NsaWNrJywgZnVuY3Rpb24gKGUpIHtcclxuXHRcdFx0XHRcdHRzLm5leHQoKTtcclxuXHRcdFx0XHR9KTtcclxuXHRcdFx0XHQkdGhpcy5maW5kKCcuanMtdGhyZWVzaXh0eS1wcmV2Jykub24oJ2NsaWNrJywgZnVuY3Rpb24gKGUpIHtcclxuXHRcdFx0XHRcdHRzLnByZXZpb3VzKCk7XHJcblx0XHRcdFx0fSk7XHJcblx0XHRcdFx0JHRoaXMuZmluZCgnLmpzLXRocmVlc2l4dHktcGxheScpLm9uKCdjbGljaycsIGZ1bmN0aW9uIChlKSB7XHJcblx0XHRcdFx0XHR0cy5wbGF5KCk7XHJcblx0XHRcdFx0fSk7XHJcblx0XHRcdFx0JHRoaXMuZmluZCgnLmpzLXRocmVlc2l4dHktcGF1c2UnKS5vbignY2xpY2snLCBmdW5jdGlvbiAoZSkge1xyXG5cdFx0XHRcdFx0dHMuc3RvcCgpO1xyXG5cdFx0XHRcdH0pO1xyXG5cdFx0XHR9XHJcblx0XHR9KTtcclxuXHR9KTtcclxufVxyXG5cclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4vLyBQdWJsaWNcclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuZnVuY3Rpb24gcHJvZHVjdE1lZGlhICgpIHtcclxuXHRsZXQgJG1lZGlhTGluayA9ICQoJy5qcy1wcm9kdWN0LW1lZGlhLWxpbmsnKTtcclxuXHJcblx0JG1lZGlhTGluay5vbignY2xpY2snLCBmdW5jdGlvbiAoKSB7XHJcblx0XHRpZiAoIXRocmVlc2l4dHlGbGFnKSB7XHJcblx0XHRcdHRocmVlc2l4dHlJbml0KCk7XHJcblx0XHRcdHRocmVlc2l4dHlGbGFnID0gdHJ1ZTtcclxuXHRcdH1cclxuXHJcblx0XHRpZiAoJG1lZGlhTGluay5oYXNDbGFzcygnaXMtaW1hZ2UnKSkge1xyXG5cdFx0XHQkbWVkaWFMaW5rLnJlbW92ZUNsYXNzKCdpcy1pbWFnZScpO1xyXG5cdFx0XHQkcHJvZHVjdEltYWdlLmFkZENsYXNzKCdpcy1oaWRlJyk7XHJcblx0XHRcdCRwcm9kdWN0MzYwLnJlbW92ZUNsYXNzKCdpcy1oaWRlJyk7XHJcblx0XHR9IGVsc2Uge1xyXG5cdFx0XHQkbWVkaWFMaW5rLmFkZENsYXNzKCdpcy1pbWFnZScpO1xyXG5cdFx0XHQkcHJvZHVjdEltYWdlLnJlbW92ZUNsYXNzKCdpcy1oaWRlJyk7XHJcblx0XHRcdCRwcm9kdWN0MzYwLmFkZENsYXNzKCdpcy1oaWRlJyk7XHJcblx0XHR9XHJcblx0fSk7XHJcbn1cclxuXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuLy8gRXhwb3J0c1xyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG5leHBvcnQge3Byb2R1Y3RNZWRpYX07XHJcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL19IVE1ML3NyYy9qcy9fbW9kdWxlcy9wcm9kdWN0LTM2MC5qcyIsIid1c2Ugc3RyaWN0JztcclxuXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuLy8gSW1wb3J0c1xyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG5pbXBvcnQgUHJlbG9hZGVyIGZyb20gJyMvX21vZHVsZXMvUHJlbG9hZGVyJztcclxuXHJcbi8vIFB1YmxpY1xyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG5mdW5jdGlvbiBwYWdpbmF0aW9uSW5pdCAoKSB7XHJcblx0bGV0ICR3cmFwID0gJCgnLnByb2R1Y3RfX3Jldmlld3MnKTtcclxuXHRsZXQgcHJlbG9hZGVyID0gbmV3IFByZWxvYWRlcigkd3JhcCk7XHJcblxyXG5cdCQoJ2JvZHknKS5vbignY2xpY2snLCAnW2RhdGEtcGFnaW5hdGlvbi1pdGVtXScsIGZ1bmN0aW9uICgpIHtcclxuXHRcdGxldCAkdGhpcyA9ICQodGhpcyk7XHJcblx0XHRsZXQgcGFnZSA9ICR0aGlzLmRhdGEoJ3BhZ2UnKTtcclxuXHRcdGxldCAkcGFnaW5hdGlvbiA9ICR0aGlzLmNsb3Nlc3QoJy5qcy1wYWdpbmF0aW9uJyk7XHJcblx0XHRsZXQgdXJsID0gJHBhZ2luYXRpb24uZGF0YSgndXJsJyk7XHJcblx0XHRsZXQgaWQgPSAkcGFnaW5hdGlvbi5kYXRhKCdpZCcpO1xyXG5cdFx0cHJlbG9hZGVyLnNob3coKTtcclxuXHJcblx0XHQkLmFqYXgoe1xyXG5cdFx0XHR1cmw6IHVybCxcclxuXHRcdFx0dHlwZTogJ3Bvc3QnLFxyXG5cdFx0XHRkYXRhOiB7XHJcblx0XHRcdFx0cGFnZTogcGFnZSxcclxuXHRcdFx0XHRpZFxyXG5cdFx0XHR9LFxyXG5cdFx0XHRkYXRhVHlwZTogJ2pzb24nLFxyXG5cdFx0XHRzdWNjZXNzOiBmdW5jdGlvbiAocmVzcG9uc2UpIHtcclxuXHRcdFx0XHRpZiAocmVzcG9uc2Uuc3VjY2Vzcykge1xyXG5cdFx0XHRcdFx0cHJlbG9hZGVyLmhpZGUoKTtcclxuXHRcdFx0XHRcdCQoJ1tkYXRhLXBhZ2luYXRpb24taXRlbV0nKS5yZW1vdmVDbGFzcygnYWN0aXZlJyk7XHJcblx0XHRcdFx0XHQkdGhpcy5hZGRDbGFzcygnYWN0aXZlJyk7XHJcblx0XHRcdFx0XHQkd3JhcC5odG1sKHJlc3BvbnNlLmh0bWwpO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cdFx0fSk7XHJcblx0fSk7XHJcbn1cclxuXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuLy8gRXhwb3J0c1xyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG5leHBvcnQge3BhZ2luYXRpb25Jbml0fTtcclxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vX0hUTUwvc3JjL2pzL19tb2R1bGVzL3BhZ2luYXRpb24uanMiLCIndXNlIHN0cmljdCc7XHJcblxyXG4vLyBQdWJsaWNcclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuZnVuY3Rpb24gY2F0YWxvZ1NlbGVjdCgpIHtcclxuXHRsZXQgJHNlbGVjdFdyYXAgPSAkKCcuY2F0YWxvZ19faXRlbXMtc2VsZWN0LWJveCcpO1xyXG5cdGxldCAkc2VsZWN0ID0gJHNlbGVjdFdyYXAuZmluZCgnLnNlbGVjdDInKTtcclxuXHJcblx0JHNlbGVjdC5vbignc2VsZWN0MjpzZWxlY3QnLCBmdW5jdGlvbiAoZSkge1xyXG5cdFx0bGV0ICR0aGlzID0gJCh0aGlzKTtcclxuXHRcdGxldCB2YWwgPSAkdGhpcy52YWwoKTtcclxuXHRcdGlmICh2YWwpIHtcclxuXHRcdFx0d2luZG93LmxvY2F0aW9uLmhyZWYgPSB2YWw7XHJcblx0XHR9XHJcblx0fSk7XHJcbn1cclxuXHJcbmZ1bmN0aW9uIGNhdGFsb2dNZW51KCkge1xyXG4gICAgJCh3aW5kb3cpLm9uKCdsb2FkJywgZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIGlmIChzY3JlZW4ud2lkdGggPiAxMDI0KSB7XHJcblxyXG5cdFx0XHRsZXQgaGVhZGVySGVpZ2h0ID0gJCgnLmhlYWRlcl9fdG9wJykuaGVpZ2h0KCk7XHJcblxyXG4gICAgICAgIFx0JCh3aW5kb3cpLnNjcm9sbChmdW5jdGlvbiAoKXtcclxuXHRcdFx0XHRoZWFkZXJIZWlnaHQgPSAkKCcuaGVhZGVyX190b3AnKS5oZWlnaHQoKTtcclxuXHRcdFx0fSk7XHJcblxyXG4gICAgICAgICAgICBjb25zdCAkdHJpZ2dlciA9ICQoJy5qcy1jYXRhbG9nLWJ1dHRvbicpO1xyXG4gICAgICAgICAgICBjb25zdCAkY2F0YWxvZyA9ICQoJy5qcy1jYXRhbG9nJyk7XHJcblxyXG4gICAgICAgICAgICAkdHJpZ2dlci5vbignY2xpY2snLCAoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAkY2F0YWxvZy5jc3MoeyAnbWluLWhlaWdodCc6IGBjYWxjKDEwMHZoIC0gJHtoZWFkZXJIZWlnaHR9cHgpYCwgJ3RvcCcgOiBgJHtoZWFkZXJIZWlnaHR9cHhgfSk7XHJcbiAgICAgICAgICAgICAgICAkKCcuY2F0YWxvZycpLmFwcGVuZCgnPGRpdiBjbGFzcz1cImxpZ2h0Ym94XCI+PC9kaXY+Jyk7XHJcbiAgICAgICAgICAgICAgICAkKCdib2R5JykuYWRkQ2xhc3MoJ292ZXJmbG93LWhpZGRlbicpO1xyXG4gICAgICAgICAgICAgICAgJGNhdGFsb2cuZmFkZUluKCk7XHJcbiAgICAgICAgICAgIH0pXHJcblxyXG4gICAgICAgICAgICAkKGRvY3VtZW50KS5tb3VzZXVwKGZ1bmN0aW9uIChlKXtcclxuICAgICAgICAgICAgICAgIGlmICghJGNhdGFsb2cuaXMoZS50YXJnZXQpICYmICRjYXRhbG9nLmhhcyhlLnRhcmdldCkubGVuZ3RoID09PSAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgJCgnLmxpZ2h0Ym94JykucmVtb3ZlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgJGNhdGFsb2cuZmFkZU91dCgpO1xyXG5cdFx0XHRcdFx0JCgnYm9keScpLnJlbW92ZUNsYXNzKCdvdmVyZmxvdy1oaWRkZW4nKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICBsZXQgJHRyaWdnZXIgPSAkKCcuanMtbW9iLW1lbnUnKTtcclxuICAgICAgICAgICBsZXQgJGNhdGFsb2cgPSAkKCcuanMtbW9iLWNhdGFsb2cnKTtcclxuICAgICAgICAgICBsZXQgJGhlYWRlclRvcCA9ICQoJy5oZWFkZXJfX3RvcC1jb250ZW50LS1tb2InKTtcclxuXHJcbiAgICAgICAgICAgJHRyaWdnZXIuY2xpY2soZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgICAkKHRoaXMpLnRvZ2dsZUNsYXNzKCdhY3RpdmUnKTtcclxuICAgICAgICAgICAgICAgJGhlYWRlclRvcC50b2dnbGVDbGFzcygnYWN0aXZlJyk7XHJcbiAgICAgICAgICAgICAgICRjYXRhbG9nLmZhZGVUb2dnbGUoKTtcclxuICAgICAgICAgICB9KVxyXG4gICAgICAgIH1cclxuICAgIH0pXHJcbn1cclxuXHJcbmZ1bmN0aW9uIGNhdGFsb2dNZW51SXRlbSgpIHtcclxuICAgICQod2luZG93KS5vbignbG9hZCcsIGZ1bmN0aW9uICgpIHtcclxuICAgICAgICBpZiAoc2NyZWVuLndpZHRoID4gMTAyNCkge1xyXG4gICAgICAgICAgICBsZXQgaGVhZGVySGVpZ2h0ID0gJCgnLmhlYWRlcicpLmhlaWdodCgpO1xyXG4gICAgICAgICAgICBjb25zdCAkdHJpZ2dlciA9ICQoJ1tkYXRhLWNhdGFsb2ctaXRlbV0nKTtcclxuICAgICAgICAgICAgY29uc3QgJGNhdGFsb2cgPSAkKCcuanMtY2F0YWxvZy1jYXRlZ29yeScpO1xyXG4gICAgICAgICAgICBjb25zdCBjYXRhbG9nU3ViID0gJCgnW2RhdGEtc3ViXScpO1xyXG5cclxuICAgICAgICAgICAgJHRyaWdnZXIuY2xpY2soZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgICAgICBsZXQgdGFyZ2V0Q2F0ZWdvcnkgPSAkKHRoaXMpLmRhdGEoJ2NhdGFsb2ctaXRlbScpO1xyXG5cclxuICAgICAgICAgICAgICAgICQodGhpcykuYWRkQ2xhc3MoJ2FjdGl2ZScpLnNpYmxpbmdzKCkucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpO1xyXG4gICAgICAgICAgICAgICAgY2F0YWxvZ1N1Yi5mYWRlT3V0KCk7XHJcbiAgICAgICAgICAgICAgICAkKCcuY2F0YWxvZycpLmFwcGVuZCgnPGRpdiBjbGFzcz1cImxpZ2h0Ym94LWNhdGVnb3J5XCI+PC9kaXY+Jyk7XHJcbiAgICAgICAgICAgICAgICAkY2F0YWxvZy5jc3MoeyAnbWluLWhlaWdodCc6IGBjYWxjKDEwMHZoIC0gJHtoZWFkZXJIZWlnaHR9cHgpYH0pLmZhZGVJbigpO1xyXG4gICAgICAgICAgICAgICAgJCgnLmNhdGFsb2ctbWVudV9fc3ViW2RhdGEtc3ViPScgKyB0YXJnZXRDYXRlZ29yeSArICddJykuZmFkZUluKCk7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgJChkb2N1bWVudCkubW91c2V1cChmdW5jdGlvbiAoZSl7XHJcbiAgICAgICAgICAgICAgICBpZiAoISRjYXRhbG9nLmlzKGUudGFyZ2V0KVxyXG4gICAgICAgICAgICAgICAgICAgICYmICRjYXRhbG9nLmhhcyhlLnRhcmdldCkubGVuZ3RoID09PSAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgJCgnLmxpZ2h0Ym94LWNhdGVnb3J5JykucmVtb3ZlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgJHRyaWdnZXIucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpO1xyXG4gICAgICAgICAgICAgICAgICAgICRjYXRhbG9nLmZhZGVPdXQoKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgbGV0ICRjYXRhbG9nSXRlbSA9ICQoJ1tkYXRhLWNhdGFsb2ctaXRlbV0nKTtcclxuICAgICAgICAgICAgbGV0ICRjYXRhbG9nSXRlbUNoaWxkcmVuID0gJCgnLmNhdGFsb2dfX2l0ZW0tY2hpbGRyZW4nKTtcclxuICAgICAgICAgICAgbGV0ICRiYWNrQnRuID0gJCgnLmNhdGFsb2dfX2l0ZW0tYmFjaycpO1xyXG5cclxuICAgICAgICAgICAgJGNhdGFsb2dJdGVtLm9uKCdjbGljaycsIGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgICQodGhpcykuc2libGluZ3MoJGNhdGFsb2dJdGVtQ2hpbGRyZW4pLmFkZENsYXNzKCdhY3RpdmUnKTtcclxuICAgICAgICAgICAgfSlcclxuXHJcbiAgICAgICAgICAgICRiYWNrQnRuLm9uKCdjbGljaycsIGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgICQodGhpcykuY2xvc2VzdCgkY2F0YWxvZ0l0ZW1DaGlsZHJlbikucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpO1xyXG4gICAgICAgICAgICB9KVxyXG4gICAgICAgIH1cclxuICAgIH0pXHJcbn1cclxuXHJcbmZ1bmN0aW9uIGNhdGFsb2dDbG9zZSgpIHtcclxuICAgIGxldCAkdHJpZ2dlciA9ICQoJy5qcy1jbG9zZScpO1xyXG4gICAgbGV0ICRjYXRhbG9nID0gJCgnLmNhdGFsb2ctbWVudScpO1xyXG5cclxuICAgICR0cmlnZ2VyLmNsaWNrKGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAkY2F0YWxvZy5mYWRlT3V0KCk7XHJcbiAgICAgICAgJCgnLmxpZ2h0Ym94JykucmVtb3ZlKCk7XHJcbiAgICAgICAgJCgnLmxpZ2h0Ym94LWNhdGVnb3J5JykucmVtb3ZlKCk7XHJcblx0XHQkKCdib2R5JykucmVtb3ZlQ2xhc3MoJ292ZXJmbG93LWhpZGRlbicpO1xyXG5cclxuXHR9KVxyXG59XHJcblxyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbi8vIEV4cG9ydHNcclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuZXhwb3J0IHtjYXRhbG9nU2VsZWN0LCBjYXRhbG9nTWVudSwgY2F0YWxvZ01lbnVJdGVtLCBjYXRhbG9nQ2xvc2V9O1xyXG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9fSFRNTC9zcmMvanMvX21vZHVsZXMvY2F0YWxvZy1zZWxlY3QuanMiLCIndXNlIHN0cmljdCc7XHJcblxyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbi8vIEltcG9ydHNcclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuaW1wb3J0IFByZWxvYWRlciBmcm9tICcuL1ByZWxvYWRlcic7XHJcbmltcG9ydCB7aW5pdFNlbGVjdH0gZnJvbSAnIy9fbW9kdWxlcy9zZWxlY3QnO1xyXG5cclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4vLyBQdWJsaWNcclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuZnVuY3Rpb24gY2hvb3NlQWpheCAoJGNvbnRlbnQgPSAkKCdib2R5JykpIHtcclxuXHRsZXQgJHRhcmdldCA9ICRjb250ZW50LmZpbmQoJy5qcy1jaG9vc2UtYWpheCcpO1xyXG5cclxuXHRmdW5jdGlvbiBzZW5kQWpheCAoJHRoaXMsICRjb250YWluZXIpIHtcclxuXHRcdGxldCB1cmwgPSAkdGhpcy5kYXRhKCd1cmwnKTtcclxuXHRcdGxldCBuYW1lID0gJHRoaXMuYXR0cignbmFtZScpIHx8ICR0aGlzLmRhdGEoJ25hbWUnKTtcclxuXHRcdGxldCB2YWx1ZSA9ICR0aGlzLnZhbCgpIHx8ICR0aGlzLmRhdGEoJ3ZhbCcpO1xyXG5cdFx0bGV0IHByZWxvYWRlciA9IG5ldyBQcmVsb2FkZXIoJGNvbnRhaW5lcik7XHJcblxyXG5cdFx0aWYgKHVybCAmJiBuYW1lKSB7XHJcblx0XHRcdHByZWxvYWRlci5zaG93KCk7XHJcblxyXG5cdFx0XHQkLmFqYXgoe1xyXG5cdFx0XHRcdHVybDogdXJsLFxyXG5cdFx0XHRcdHR5cGU6ICdwb3N0JyxcclxuXHRcdFx0XHRkYXRhOiB7XHJcblx0XHRcdFx0XHRuYW1lOiBuYW1lLFxyXG5cdFx0XHRcdFx0dmFsdWU6IHZhbHVlXHJcblx0XHRcdFx0fSxcclxuXHRcdFx0XHRkYXRhVHlwZTogJ2pzb24nLFxyXG5cdFx0XHRcdHN1Y2Nlc3M6IGZ1bmN0aW9uIChyZXNwb25zZSkge1xyXG5cdFx0XHRcdFx0aWYgKHJlc3BvbnNlLnN1Y2Nlc3MpIHtcclxuXHRcdFx0XHRcdFx0aWYgKHJlc3BvbnNlLml0ZW1zKSB7XHJcblx0XHRcdFx0XHRcdFx0cmVzcG9uc2UuaXRlbXMuZm9yRWFjaChmdW5jdGlvbiAoaXRlbSkge1xyXG5cdFx0XHRcdFx0XHRcdFx0aWYgKGl0ZW0uaHRtbCAmJiBpdGVtLnNlbGVjdG9yKSB7XHJcblx0XHRcdFx0XHRcdFx0XHRcdGxldCAkaHRtbCA9ICQoaXRlbS5odG1sKTtcclxuXHRcdFx0XHRcdFx0XHRcdFx0bGV0ICRzZWxlY3RvciA9ICQoaXRlbS5zZWxlY3Rvcik7XHJcblxyXG5cdFx0XHRcdFx0XHRcdFx0XHRpZiAoJHNlbGVjdG9yLmxlbmd0aCkge1xyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdGxldCAkc2VsZWN0MiA9ICRjb250YWluZXIuZmluZCgnc2VsZWN0LnNlbGVjdDInKTtcclxuXHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0JHNlbGVjdDIuZWFjaChmdW5jdGlvbiAoKSB7XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRsZXQgJHRoID0gJCh0aGlzKTtcclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdGlmICgkdGguZGF0YSgnc2VsZWN0MicpKSB7XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdCR0aC5zZWxlY3QyKCdkZXN0cm95Jyk7XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0fSk7XHJcblxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdCRzZWxlY3Rvci5odG1sKCRodG1sKTtcclxuXHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0JHNlbGVjdDIuZWFjaChmdW5jdGlvbiAoKSB7XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRsZXQgJHRoID0gJCh0aGlzKS5wYXJlbnQoKTtcclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdGluaXRTZWxlY3QoJHRoKTtcclxuXHRcdFx0XHRcdFx0XHRcdFx0XHR9KTtcclxuXHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0Y2hvb3NlQWpheCgkaHRtbCk7XHJcblx0XHRcdFx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdFx0XHR9KTtcclxuXHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0fVxyXG5cclxuXHRcdFx0XHRcdHByZWxvYWRlci5oaWRlKCk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9KTtcclxuXHRcdH1cclxuXHR9XHJcblxyXG5cdGlmICgkdGFyZ2V0Lmxlbmd0aCkge1xyXG5cdFx0JHRhcmdldC5lYWNoKGZ1bmN0aW9uICgpIHtcclxuXHRcdFx0bGV0ICR0aGlzID0gJCh0aGlzKTtcclxuXHRcdFx0bGV0ICRjb250YWluZXIgPSAkdGhpcy5jbG9zZXN0KCcuanMtY2hvb3NlLWFqYXgtY29udGFpbmVyJyk7XHJcblx0XHRcdGxldCB0YWdOYW1lID0gdGhpcy50YWdOYW1lLnRvTG93ZXJDYXNlKCk7XHJcblxyXG5cdFx0XHRpZiAoJHRoaXMuZGF0YSgnc2VsZWN0MicpKSB7XHJcblx0XHRcdFx0JHRoaXMub24oJ3NlbGVjdDI6c2VsZWN0JywgZnVuY3Rpb24gKGUpIHtcclxuXHRcdFx0XHRcdHNlbmRBamF4KCR0aGlzLCAkY29udGFpbmVyKTtcclxuXHRcdFx0XHR9KTtcclxuXHRcdFx0fSBlbHNlIGlmICh0YWdOYW1lID09PSAnc2VsZWN0JyB8fCAodGFnTmFtZSA9PT0gJ2lucHV0JyAmJiAoJHRoaXMuYXR0cigndHlwZScpID09PSAncmFkaW8nIHx8ICR0aGlzLmF0dHIoJ3R5cGUnKSA9PT0gJ2NoZWNrYm94JykpKSB7XHJcblx0XHRcdFx0JHRoaXMub24oJ2NoYW5nZScsIGZ1bmN0aW9uIChlKSB7XHJcblx0XHRcdFx0XHRzZW5kQWpheCgkdGhpcywgJGNvbnRhaW5lcik7XHJcblx0XHRcdFx0fSk7XHJcblx0XHRcdH0gZWxzZSBpZiAodGFnTmFtZSA9PT0gJ3RleHRhcmVhJyB8fCAodGFnTmFtZSA9PT0gJ2lucHV0JyAmJiAkdGhpcy5hdHRyKCd0eXBlJykgIT09ICdyYWRpbycgJiYgJHRoaXMuYXR0cigndHlwZScpICE9PSAnY2hlY2tib3gnKSkge1xyXG5cdFx0XHRcdCR0aGlzLm9uKCdrZXl1cCcsIGZ1bmN0aW9uIChlKSB7XHJcblx0XHRcdFx0XHRzZW5kQWpheCgkdGhpcywgJGNvbnRhaW5lcik7XHJcblx0XHRcdFx0fSk7XHJcblx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0JHRoaXMub24oJ2NsaWNrJywgZnVuY3Rpb24gKGUpIHtcclxuXHRcdFx0XHRcdHNlbmRBamF4KCR0aGlzLCAkY29udGFpbmVyKTtcclxuXHRcdFx0XHR9KTtcclxuXHRcdFx0fVxyXG5cdFx0fSk7XHJcblx0fVxyXG59XHJcblxyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbi8vIEV4cG9ydHNcclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2hvb3NlQWpheDtcclxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vX0hUTUwvc3JjL2pzL19tb2R1bGVzL2Nob29zZS1hamF4LmpzIiwiJ3VzZSBzdHJpY3QnO1xyXG5cclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4vLyBJbXBvcnRzXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbmltcG9ydCAnc2VsZWN0Mic7XHJcbmltcG9ydCB2YWxpZGF0ZSBmcm9tICcjL19tb2R1bGVzL2pxdWVyeS12YWxpZGF0aW9uL3ZhbGlkYXRlLWluaXQnO1xyXG5cclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4vLyBQcml2YXRlXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbmxldCBzZWxlY3RTZWxlY3RvciA9ICcuYWpheFNlbGVjdDInO1xyXG5cclxuZnVuY3Rpb24gaW5pdEFqYXhTZWxlY3QyKCRjb250YWluZXIgPSAkKCdib2R5JykpIHtcclxuXHQkY29udGFpbmVyLmZpbmQoc2VsZWN0U2VsZWN0b3IpLmVhY2goZnVuY3Rpb24gKGluZGV4KSB7XHJcblx0XHRsZXQgJHRoaXMgPSAkKHRoaXMpO1xyXG5cclxuXHRcdCR0aGlzLnNlbGVjdDIoe1xyXG5cdFx0XHRhamF4OiB7XHJcblx0XHRcdFx0dXJsOiAkdGhpcy5kYXRhKCd1cmwnKSxcclxuXHRcdFx0XHRkYXRhOiBmdW5jdGlvbiAocGFyYW1zKSB7XHJcblx0XHRcdFx0XHR2YXIgcXVlcnkgPSB7XHJcblx0XHRcdFx0XHRcdHNlYXJjaDogcGFyYW1zLnRlcm0sXHJcblx0XHRcdFx0XHRcdGNpdHlSZWY6ICR0aGlzLmRhdGFbJ2NpdHknXVxyXG5cdFx0XHRcdFx0fVxyXG5cclxuXHRcdFx0XHRcdHJldHVybiBxdWVyeTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH1cclxuXHRcdH0pO1xyXG5cdH0pO1xyXG59XHJcblxyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbi8vIEV4cG9ydHNcclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgaW5pdEFqYXhTZWxlY3QyO1xuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL19IVE1ML3NyYy9qcy9fbW9kdWxlcy9hamF4U2VsZWN0Mi5qcyIsIid1c2Ugc3RyaWN0JztcclxuXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuLy8gSW1wb3J0c1xyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG5pbXBvcnQgTm90eSBmcm9tICcjL19tb2R1bGVzL25vdHkvbm90eS1leHRlbmQnO1xyXG5cclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4vLyBQdWJsaWNcclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuZnVuY3Rpb24gbWVzc2FnZUluaXQgKCkge1xyXG5cdHdpbmRvdy5nZW5lcmF0ZSA9IGZ1bmN0aW9uIChtZXNzYWdlLCB0eXBlLCB0aW1lKSB7XHJcblx0XHRjb25zdCBub3R5ID0gbmV3IE5vdHkoe1xyXG5cdFx0XHR0eXBlOiB0eXBlID09PSAnc3VjY2VzcycgPyAnaW5mbycgOiAnZXJyb3InLFxyXG5cdFx0XHR0ZXh0OiBtZXNzYWdlLFxyXG5cdFx0XHR0aW1lb3V0OiB0aW1lXHJcblx0XHR9KTtcclxuXHRcdG5vdHkuc2hvdygpO1xyXG5cdH07XHJcbn1cclxuXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuLy8gRXhwb3J0c1xyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG5leHBvcnQgZGVmYXVsdCBtZXNzYWdlSW5pdDtcclxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vX0hUTUwvc3JjL2pzL19tb2R1bGVzL3Nob3ctbm90eS5qcyIsIid1c2Ugc3RyaWN0JztcclxuXHJcbi8qKlxyXG4gKlxyXG4gKiBAbW9kdWxlXHJcbiAqL1xyXG5cclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4vLyBJbXBvcnRzXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbmltcG9ydCBOb3R5IGZyb20gJ25vdHknO1xyXG5pbXBvcnQgJy4vbm90eS1leHRlbmQuc2Nzcyc7XHJcblxyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbi8vIFByaXZhdGVcclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuTm90eS5vdmVycmlkZURlZmF1bHRzKHtcclxuXHR0aGVtZTogJ21pbnQnLFxyXG5cdGxheW91dDogJ2JvdHRvbVJpZ2h0JyxcclxuXHR0aW1lb3V0OiA1MDAwLFxyXG5cdHByb2dyZXNzQmFyOiB0cnVlLFxyXG5cdGNsb3NlV2l0aDogWydjbGljayddXHJcbn0pO1xyXG5cclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4vLyBFeHBvcnRzXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IE5vdHk7XHJcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL19IVE1ML3NyYy9qcy9fbW9kdWxlcy9ub3R5L25vdHktZXh0ZW5kLmpzIiwiLy8gc3R5bGUtbG9hZGVyOiBBZGRzIHNvbWUgY3NzIHRvIHRoZSBET00gYnkgYWRkaW5nIGEgPHN0eWxlPiB0YWdcblxuLy8gbG9hZCB0aGUgc3R5bGVzXG52YXIgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTItMSEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTItMiEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvc2Fzcy1sb2FkZXIvbGliL2xvYWRlci5qcz8/cmVmLS0yLTMhLi9ub3R5LWV4dGVuZC5zY3NzXCIpO1xuaWYodHlwZW9mIGNvbnRlbnQgPT09ICdzdHJpbmcnKSBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG4vLyBQcmVwYXJlIGNzc1RyYW5zZm9ybWF0aW9uXG52YXIgdHJhbnNmb3JtO1xuXG52YXIgb3B0aW9ucyA9IHtcImluc2VydEF0XCI6e1wiYmVmb3JlXCI6XCIjd2VicGFjay1zdHlsZS1sb2FkZXItaW5zZXJ0LWJlZm9yZS10aGlzXCJ9LFwiaG1yXCI6dHJ1ZX1cbm9wdGlvbnMudHJhbnNmb3JtID0gdHJhbnNmb3JtXG4vLyBhZGQgdGhlIHN0eWxlcyB0byB0aGUgRE9NXG52YXIgdXBkYXRlID0gcmVxdWlyZShcIiEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvc3R5bGUtbG9hZGVyL2xpYi9hZGRTdHlsZXMuanNcIikoY29udGVudCwgb3B0aW9ucyk7XG5pZihjb250ZW50LmxvY2FscykgbW9kdWxlLmV4cG9ydHMgPSBjb250ZW50LmxvY2Fscztcbi8vIEhvdCBNb2R1bGUgUmVwbGFjZW1lbnRcbmlmKG1vZHVsZS5ob3QpIHtcblx0Ly8gV2hlbiB0aGUgc3R5bGVzIGNoYW5nZSwgdXBkYXRlIHRoZSA8c3R5bGU+IHRhZ3Ncblx0aWYoIWNvbnRlbnQubG9jYWxzKSB7XG5cdFx0bW9kdWxlLmhvdC5hY2NlcHQoXCIhIS4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTItMSEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTItMiEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvc2Fzcy1sb2FkZXIvbGliL2xvYWRlci5qcz8/cmVmLS0yLTMhLi9ub3R5LWV4dGVuZC5zY3NzXCIsIGZ1bmN0aW9uKCkge1xuXHRcdFx0dmFyIG5ld0NvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS0yLTEhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS0yLTIhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Nhc3MtbG9hZGVyL2xpYi9sb2FkZXIuanM/P3JlZi0tMi0zIS4vbm90eS1leHRlbmQuc2Nzc1wiKTtcblx0XHRcdGlmKHR5cGVvZiBuZXdDb250ZW50ID09PSAnc3RyaW5nJykgbmV3Q29udGVudCA9IFtbbW9kdWxlLmlkLCBuZXdDb250ZW50LCAnJ11dO1xuXHRcdFx0dXBkYXRlKG5ld0NvbnRlbnQpO1xuXHRcdH0pO1xuXHR9XG5cdC8vIFdoZW4gdGhlIG1vZHVsZSBpcyBkaXNwb3NlZCwgcmVtb3ZlIHRoZSA8c3R5bGU+IHRhZ3Ncblx0bW9kdWxlLmhvdC5kaXNwb3NlKGZ1bmN0aW9uKCkgeyB1cGRhdGUoKTsgfSk7XG59XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9fSFRNTC9zcmMvanMvX21vZHVsZXMvbm90eS9ub3R5LWV4dGVuZC5zY3NzXG4vLyBtb2R1bGUgaWQgPSA3NFxuLy8gbW9kdWxlIGNodW5rcyA9IDkiLCJleHBvcnRzID0gbW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvbGliL2Nzcy1iYXNlLmpzXCIpKGZhbHNlKTtcbi8vIGltcG9ydHNcblxuXG4vLyBtb2R1bGVcbmV4cG9ydHMucHVzaChbbW9kdWxlLmlkLCBcIi5ub3R5X2xheW91dF9taXhpbiwgI25vdHlfbGF5b3V0X190b3AsICNub3R5X2xheW91dF9fdG9wTGVmdCwgI25vdHlfbGF5b3V0X190b3BDZW50ZXIsICNub3R5X2xheW91dF9fdG9wUmlnaHQsICNub3R5X2xheW91dF9fYm90dG9tLCAjbm90eV9sYXlvdXRfX2JvdHRvbUxlZnQsICNub3R5X2xheW91dF9fYm90dG9tQ2VudGVyLCAjbm90eV9sYXlvdXRfX2JvdHRvbVJpZ2h0LCAjbm90eV9sYXlvdXRfX2NlbnRlciwgI25vdHlfbGF5b3V0X19jZW50ZXJMZWZ0LCAjbm90eV9sYXlvdXRfX2NlbnRlclJpZ2h0IHtcXG4gIHBvc2l0aW9uOiBmaXhlZDtcXG4gIG1hcmdpbjogMDtcXG4gIHBhZGRpbmc6IDA7XFxuICB6LWluZGV4OiA5OTk5OTk5O1xcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGVaKDApIHNjYWxlKDEsIDEpO1xcbiAgLXdlYmtpdC1iYWNrZmFjZS12aXNpYmlsaXR5OiBoaWRkZW47XFxuICBiYWNrZmFjZS12aXNpYmlsaXR5OiBoaWRkZW47XFxuICAtd2Via2l0LWZvbnQtc21vb3RoaW5nOiBzdWJwaXhlbC1hbnRpYWxpYXNlZDtcXG4gIGZpbHRlcjogYmx1cigwKTtcXG4gIC13ZWJraXQtZmlsdGVyOiBibHVyKDApO1xcbiAgbWF4LXdpZHRoOiA5MCU7IH1cXG5cXG4jbm90eV9sYXlvdXRfX3RvcCB7XFxuICB0b3A6IDA7XFxuICBsZWZ0OiA1JTtcXG4gIHdpZHRoOiA5MCU7IH1cXG5cXG4jbm90eV9sYXlvdXRfX3RvcExlZnQge1xcbiAgdG9wOiAyMHB4O1xcbiAgbGVmdDogMjBweDtcXG4gIHdpZHRoOiAzMjVweDsgfVxcblxcbiNub3R5X2xheW91dF9fdG9wQ2VudGVyIHtcXG4gIHRvcDogNSU7XFxuICBsZWZ0OiA1MCU7XFxuICB3aWR0aDogMzI1cHg7XFxuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZShjYWxjKC01MCUgLSAuNXB4KSkgdHJhbnNsYXRlWigwKSBzY2FsZSgxLCAxKTsgfVxcblxcbiNub3R5X2xheW91dF9fdG9wUmlnaHQge1xcbiAgdG9wOiAyMHB4O1xcbiAgcmlnaHQ6IDIwcHg7XFxuICB3aWR0aDogMzI1cHg7IH1cXG5cXG4jbm90eV9sYXlvdXRfX2JvdHRvbSB7XFxuICBib3R0b206IDA7XFxuICBsZWZ0OiA1JTtcXG4gIHdpZHRoOiA5MCU7IH1cXG5cXG4jbm90eV9sYXlvdXRfX2JvdHRvbUxlZnQge1xcbiAgYm90dG9tOiAyMHB4O1xcbiAgbGVmdDogMjBweDtcXG4gIHdpZHRoOiAzMjVweDsgfVxcblxcbiNub3R5X2xheW91dF9fYm90dG9tQ2VudGVyIHtcXG4gIGJvdHRvbTogNSU7XFxuICBsZWZ0OiA1MCU7XFxuICB3aWR0aDogMzI1cHg7XFxuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZShjYWxjKC01MCUgLSAuNXB4KSkgdHJhbnNsYXRlWigwKSBzY2FsZSgxLCAxKTsgfVxcblxcbiNub3R5X2xheW91dF9fYm90dG9tUmlnaHQge1xcbiAgYm90dG9tOiAyMHB4O1xcbiAgcmlnaHQ6IDIwcHg7XFxuICB3aWR0aDogMzI1cHg7IH1cXG5cXG4jbm90eV9sYXlvdXRfX2NlbnRlciB7XFxuICB0b3A6IDUwJTtcXG4gIGxlZnQ6IDUwJTtcXG4gIHdpZHRoOiAzMjVweDtcXG4gIHRyYW5zZm9ybTogdHJhbnNsYXRlKGNhbGMoLTUwJSAtIC41cHgpLCBjYWxjKC01MCUgLSAuNXB4KSkgdHJhbnNsYXRlWigwKSBzY2FsZSgxLCAxKTsgfVxcblxcbiNub3R5X2xheW91dF9fY2VudGVyTGVmdCB7XFxuICB0b3A6IDUwJTtcXG4gIGxlZnQ6IDIwcHg7XFxuICB3aWR0aDogMzI1cHg7XFxuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgwLCBjYWxjKC01MCUgLSAuNXB4KSkgdHJhbnNsYXRlWigwKSBzY2FsZSgxLCAxKTsgfVxcblxcbiNub3R5X2xheW91dF9fY2VudGVyUmlnaHQge1xcbiAgdG9wOiA1MCU7XFxuICByaWdodDogMjBweDtcXG4gIHdpZHRoOiAzMjVweDtcXG4gIHRyYW5zZm9ybTogdHJhbnNsYXRlKDAsIGNhbGMoLTUwJSAtIC41cHgpKSB0cmFuc2xhdGVaKDApIHNjYWxlKDEsIDEpOyB9XFxuXFxuLm5vdHlfcHJvZ3Jlc3NiYXIge1xcbiAgZGlzcGxheTogbm9uZTsgfVxcblxcbi5ub3R5X2hhc190aW1lb3V0Lm5vdHlfaGFzX3Byb2dyZXNzYmFyIC5ub3R5X3Byb2dyZXNzYmFyIHtcXG4gIGRpc3BsYXk6IGJsb2NrO1xcbiAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgbGVmdDogMDtcXG4gIGJvdHRvbTogMDtcXG4gIGhlaWdodDogM3B4O1xcbiAgd2lkdGg6IDEwMCU7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjNjQ2NDY0O1xcbiAgb3BhY2l0eTogMC4yO1xcbiAgZmlsdGVyOiBhbHBoYShvcGFjaXR5PTEwKTsgfVxcblxcbi5ub3R5X2JhciB7XFxuICAtd2Via2l0LWJhY2tmYWNlLXZpc2liaWxpdHk6IGhpZGRlbjtcXG4gIHRyYW5zZm9ybTogdHJhbnNsYXRlKDAsIDApIHNjYWxlKDEsIDEpO1xcbiAgLXdlYmtpdC1mb250LXNtb290aGluZzogc3VicGl4ZWwtYW50aWFsaWFzZWQ7XFxuICBvdmVyZmxvdzogaGlkZGVuOyB9XFxuXFxuLm5vdHlfZWZmZWN0c19vcGVuIHtcXG4gIG9wYWNpdHk6IDA7XFxuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSg1MCUpO1xcbiAgYW5pbWF0aW9uOiBub3R5X2FuaW1faW4gMC41cyBjdWJpYy1iZXppZXIoMC42OCwgLTAuNTUsIDAuMjY1LCAxLjU1KTtcXG4gIGFuaW1hdGlvbi1maWxsLW1vZGU6IGZvcndhcmRzOyB9XFxuXFxuLm5vdHlfZWZmZWN0c19jbG9zZSB7XFxuICBhbmltYXRpb246IG5vdHlfYW5pbV9vdXQgMC41cyBjdWJpYy1iZXppZXIoMC42OCwgLTAuNTUsIDAuMjY1LCAxLjU1KTtcXG4gIGFuaW1hdGlvbi1maWxsLW1vZGU6IGZvcndhcmRzOyB9XFxuXFxuLm5vdHlfZml4X2VmZmVjdHNfaGVpZ2h0IHtcXG4gIGFuaW1hdGlvbjogbm90eV9hbmltX2hlaWdodCA3NW1zIGVhc2Utb3V0OyB9XFxuXFxuLm5vdHlfY2xvc2Vfd2l0aF9jbGljayB7XFxuICBjdXJzb3I6IHBvaW50ZXI7IH1cXG5cXG4ubm90eV9jbG9zZV9idXR0b24ge1xcbiAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgdG9wOiAycHg7XFxuICByaWdodDogMnB4O1xcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XFxuICB3aWR0aDogMjBweDtcXG4gIGhlaWdodDogMjBweDtcXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gIGxpbmUtaGVpZ2h0OiAyMHB4O1xcbiAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgwLCAwLCAwLCAwLjA1KTtcXG4gIGJvcmRlci1yYWRpdXM6IDJweDtcXG4gIGN1cnNvcjogcG9pbnRlcjtcXG4gIHRyYW5zaXRpb246IGFsbCAuMnMgZWFzZS1vdXQ7IH1cXG5cXG4ubm90eV9jbG9zZV9idXR0b246aG92ZXIge1xcbiAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgwLCAwLCAwLCAwLjEpOyB9XFxuXFxuLm5vdHlfbW9kYWwge1xcbiAgcG9zaXRpb246IGZpeGVkO1xcbiAgd2lkdGg6IDEwMCU7XFxuICBoZWlnaHQ6IDEwMCU7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDAwO1xcbiAgei1pbmRleDogMTAwMDA7XFxuICBvcGFjaXR5OiAuMztcXG4gIGxlZnQ6IDA7XFxuICB0b3A6IDA7IH1cXG5cXG4ubm90eV9tb2RhbC5ub3R5X21vZGFsX29wZW4ge1xcbiAgb3BhY2l0eTogMDtcXG4gIGFuaW1hdGlvbjogbm90eV9tb2RhbF9pbiAuM3MgZWFzZS1vdXQ7IH1cXG5cXG4ubm90eV9tb2RhbC5ub3R5X21vZGFsX2Nsb3NlIHtcXG4gIGFuaW1hdGlvbjogbm90eV9tb2RhbF9vdXQgLjNzIGVhc2Utb3V0O1xcbiAgYW5pbWF0aW9uLWZpbGwtbW9kZTogZm9yd2FyZHM7IH1cXG5cXG5Aa2V5ZnJhbWVzIG5vdHlfbW9kYWxfaW4ge1xcbiAgMTAwJSB7XFxuICAgIG9wYWNpdHk6IC4zOyB9IH1cXG5cXG5Aa2V5ZnJhbWVzIG5vdHlfbW9kYWxfb3V0IHtcXG4gIDEwMCUge1xcbiAgICBvcGFjaXR5OiAwOyB9IH1cXG5cXG5Aa2V5ZnJhbWVzIG5vdHlfYW5pbV9pbiB7XFxuICAxMDAlIHtcXG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoMCk7XFxuICAgIG9wYWNpdHk6IDE7IH0gfVxcblxcbkBrZXlmcmFtZXMgbm90eV9hbmltX291dCB7XFxuICAxMDAlIHtcXG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoNTAlKTtcXG4gICAgb3BhY2l0eTogMDsgfSB9XFxuXFxuQGtleWZyYW1lcyBub3R5X2FuaW1faGVpZ2h0IHtcXG4gIDEwMCUge1xcbiAgICBoZWlnaHQ6IDA7IH0gfVxcblxcbi5ub3R5X3RoZW1lX19taW50Lm5vdHlfYmFyIHtcXG4gIG1hcmdpbjogNHB4IDA7XFxuICBvdmVyZmxvdzogaGlkZGVuO1xcbiAgYm9yZGVyLXJhZGl1czogMnB4O1xcbiAgcG9zaXRpb246IHJlbGF0aXZlOyB9XFxuICAubm90eV90aGVtZV9fbWludC5ub3R5X2JhciAubm90eV9ib2R5IHtcXG4gICAgcGFkZGluZzogMTBweDtcXG4gICAgZm9udC1zaXplOiAxNHB4OyB9XFxuICAubm90eV90aGVtZV9fbWludC5ub3R5X2JhciAubm90eV9idXR0b25zIHtcXG4gICAgcGFkZGluZzogMTBweDsgfVxcblxcbi5ub3R5X3RoZW1lX19taW50Lm5vdHlfdHlwZV9fYWxlcnQsXFxuLm5vdHlfdGhlbWVfX21pbnQubm90eV90eXBlX19ub3RpZmljYXRpb24ge1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcXG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjRDFEMUQxO1xcbiAgY29sb3I6ICMyRjJGMkY7IH1cXG5cXG4ubm90eV90aGVtZV9fbWludC5ub3R5X3R5cGVfX3dhcm5pbmcge1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI0ZGQUU0MjtcXG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjRTg5RjNDO1xcbiAgY29sb3I6ICNmZmY7IH1cXG5cXG4ubm90eV90aGVtZV9fbWludC5ub3R5X3R5cGVfX2Vycm9yIHtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNERTYzNkY7XFxuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI0NBNUE2NTtcXG4gIGNvbG9yOiAjZmZmOyB9XFxuXFxuLm5vdHlfdGhlbWVfX21pbnQubm90eV90eXBlX19pbmZvLFxcbi5ub3R5X3RoZW1lX19taW50Lm5vdHlfdHlwZV9faW5mb3JtYXRpb24ge1xcbiAgYmFja2dyb3VuZC1jb2xvcjogIzdGN0VGRjtcXG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjNzQ3M0U4O1xcbiAgY29sb3I6ICNmZmY7IH1cXG5cXG4ubm90eV90aGVtZV9fbWludC5ub3R5X3R5cGVfX3N1Y2Nlc3Mge1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI0FGQzc2NTtcXG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjQTBCNTVDO1xcbiAgY29sb3I6ICNmZmY7IH1cXG5cXG4ubm90eV90aGVtZV9fbWludC5ub3R5X3R5cGVfX2luZm8sIC5ub3R5X3RoZW1lX19taW50Lm5vdHlfdHlwZV9faW5mb3JtYXRpb24ge1xcbiAgYmFja2dyb3VuZDogIzI4YTc0NTtcXG4gIGNvbG9yOiAjZmZmO1xcbiAgYm9yZGVyLWJvdHRvbTogbm9uZTsgfVxcbiAgLm5vdHlfdGhlbWVfX21pbnQubm90eV90eXBlX19pbmZvLm5vdHlfaGFzX3Byb2dyZXNzYmFyIC5ub3R5X3Byb2dyZXNzYmFyLCAubm90eV90aGVtZV9fbWludC5ub3R5X3R5cGVfX2luZm9ybWF0aW9uLm5vdHlfaGFzX3Byb2dyZXNzYmFyIC5ub3R5X3Byb2dyZXNzYmFyIHtcXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZkZTUyZDtcXG4gICAgb3BhY2l0eTogMC42OyB9XFxuXFxuLm5vdHlfdGhlbWVfX21pbnQubm90eV90eXBlX19lcnJvciB7XFxuICBiYWNrZ3JvdW5kOiAjZWQ0OTU2O1xcbiAgY29sb3I6ICNmZmY7XFxuICBib3JkZXItYm90dG9tOiBub25lOyB9XFxuICAubm90eV90aGVtZV9fbWludC5ub3R5X3R5cGVfX2Vycm9yLm5vdHlfaGFzX3Byb2dyZXNzYmFyIC5ub3R5X3Byb2dyZXNzYmFyIHtcXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZkZTUyZDtcXG4gICAgb3BhY2l0eTogMC40OyB9XFxuXCIsIFwiXCJdKTtcblxuLy8gZXhwb3J0c1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlcj8/cmVmLS0yLTEhLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliPz9yZWYtLTItMiEuL25vZGVfbW9kdWxlcy9zYXNzLWxvYWRlci9saWIvbG9hZGVyLmpzPz9yZWYtLTItMyEuL19IVE1ML3NyYy9qcy9fbW9kdWxlcy9ub3R5L25vdHktZXh0ZW5kLnNjc3Ncbi8vIG1vZHVsZSBpZCA9IDc1XG4vLyBtb2R1bGUgY2h1bmtzID0gOSIsIid1c2Ugc3RyaWN0JztcclxuXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuLy8gSW1wb3J0c1xyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG5pbXBvcnQgY29va2llRGF0YSBmcm9tICcjL19tb2R1bGVzL2Nvb2tpZS1kYXRhJztcclxuXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuLy8gUHVibGljXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbmZ1bmN0aW9uIGN1cnJlbmN5SW5pdCAoKSB7XHJcblx0JCgnLmpzLWN1cnJlbmN5Jykub24oJ2NsaWNrJywgZnVuY3Rpb24gKCkge1xyXG5cdFx0bGV0IGN1cnJlbmN5ID0gJCh0aGlzKS5kYXRhKCdjdXJyZW5jeScpO1xyXG5cdFx0Y29va2llRGF0YS5hZGQoJ2N1cnJlbmN5JywgY3VycmVuY3ksIHtcclxuXHRcdFx0ZXhwaXJlczogMzYwMCAqIDI0ICogMzAsXHJcblx0XHRcdHBhdGg6ICcvJ1xyXG5cdFx0fSk7XHJcblx0XHR3aW5kb3cubG9jYXRpb24uaHJlZiA9IHdpbmRvdy5sb2NhdGlvbi5ocmVmO1xyXG5cdH0pO1xyXG59XHJcblxyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbi8vIEV4cG9ydHNcclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgY3VycmVuY3lJbml0O1xyXG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9fSFRNTC9zcmMvanMvX21vZHVsZXMvY3VycmVuY3kuanMiLCIndXNlIHN0cmljdCc7XHJcblxyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbi8vIEltcG9ydHNcclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuaW1wb3J0IGNvb2tpZURhdGEgZnJvbSAnIy9fbW9kdWxlcy9jb29raWUtZGF0YSc7XHJcblxyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbi8vIFB1YmxpY1xyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG5mdW5jdGlvbiB1c2VyVHlwZUluaXQgKCkge1xyXG5cdCQoJy5qcy11c2VyLXR5cGUnKS5vbignY2xpY2snLCBmdW5jdGlvbiAoKSB7XHJcblx0XHRsZXQgdXNlclR5cGUgPSAkKHRoaXMpLmRhdGEoJ3VzZXItdHlwZScpO1xyXG5cdFx0Y29va2llRGF0YS5hZGQoJ3VzZXItdHlwZScsIHVzZXJUeXBlLCB7XHJcblx0XHRcdGV4cGlyZXM6IDM2MDAgKiAyNCAqIDMwLFxyXG5cdFx0XHRwYXRoOiAnLydcclxuXHRcdH0pO1xyXG5cdFx0d2luZG93LmxvY2F0aW9uLmhyZWYgPSB3aW5kb3cubG9jYXRpb24uaHJlZjtcclxuXHR9KTtcclxufVxyXG5cclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4vLyBFeHBvcnRzXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IHVzZXJUeXBlSW5pdDtcclxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vX0hUTUwvc3JjL2pzL19tb2R1bGVzL3VzZXItdHlwZS5qcyIsIid1c2Ugc3RyaWN0JztcclxuXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuLy8gSW1wb3J0c1xyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbi8vIFB1YmxpY1xyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG5mdW5jdGlvbiBwYXlUb2dnbGUgKCkge1xyXG5cdGxldCB3cmFwID0gJCgnW2RhdGEtcGF5LXdyYXBdJyk7XHJcblx0bGV0IGlucHV0cyA9IHdyYXAuZmluZCgnaW5wdXQnKTtcclxuXHRsZXQgaW5wdXQgPSAkKCdbZGF0YS1wYXktY2FydF0nKTtcclxuXHRsZXQgaW5mbyA9ICQoJ1tkYXRhLXBheS1jYXJ0LWJsb2NrXScpO1xyXG5cdGxldCBwYXlUeXBlID0gJCgnLmNhcnQtaW5mb19fbGFiZWwnKTtcclxuXHJcblx0aWYod3JhcC5sZW5ndGgpIHtcclxuXHRcdGlucHV0cy5ub3QoaW5wdXQpLm9uKCdjaGFuZ2UnLCAoKSA9PiB7XHJcblx0XHRcdGluZm8uc2xpZGVVcCgxNTApO1xyXG5cdFx0fSk7XHJcblx0XHRpbnB1dC5vbignY2hhbmdlJywgKCkgPT4ge1xyXG5cdFx0XHRpZihpbnB1dC5pcygnOmNoZWNrZWQnKSkge1xyXG5cdFx0XHRcdGluZm8uc2xpZGVEb3duKDE1MCk7XHJcblx0XHRcdH1cclxuXHRcdH0pO1xyXG5cclxuXHRcdHBheVR5cGUub24oJ2NsaWNrJywgZnVuY3Rpb24gKCl7XHJcblx0XHRcdCQoJy5jYXJ0LWluZm9fX2xhYmVsLmlzLWNoZWNrZWQnKS5yZW1vdmVDbGFzcygnaXMtY2hlY2tlZCcpO1xyXG5cdFx0XHQkKHRoaXMpLmFkZENsYXNzKCdpcy1jaGVja2VkJyk7XHJcblx0XHR9KVxyXG5cdH1cclxufVxyXG5cclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4vLyBFeHBvcnRzXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IHBheVRvZ2dsZTtcclxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vX0hUTUwvc3JjL2pzL19tb2R1bGVzL3BheS1pbmZvLXRvZ2dsZS5qcyIsIid1c2Ugc3RyaWN0JztcclxuXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuLy8gSW1wb3J0c1xyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG5pbXBvcnQgJyMvX3ZlbmRvcnMvanF1ZXJ5LmNvb2tpZSdcclxuaW1wb3J0IG1vZHVsZUxvYWRlciBmcm9tIFwiIy9tb2R1bGUtbG9hZGVyXCI7XHJcbmltcG9ydCBQcmVsb2FkZXIgZnJvbSBcIiMvX21vZHVsZXMvUHJlbG9hZGVyXCI7XHJcblxyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbi8vIFByaXZhdGVcclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuLyoqXHJcbiAqIEB0eXBlIHtPYmplY3R9XHJcbiAqIEBwcml2YXRlXHJcbiAqL1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gYmFja1RvQ2F0YWxvZygpIHtcclxuXHQkKCcuZm9yLWNvb2tpZScpLm9uKCdjbGljaycsICcucHJvZHVjdC1pdGVtJywgZnVuY3Rpb24gKCl7XHJcblx0XHRsZXQgc2Nyb2xsID0gJCh0aGlzKS5vZmZzZXQoKS50b3A7XHJcblx0XHQkLmNvb2tpZSgncGFnZV9zY3JvbGwnLCBzY3JvbGwpO1xyXG5cclxuXHRcdGxldCBzdGFydFBhZ2UgPSAkKFwiLnBhZ2luYXRpb25fX2l0ZW0uYWN0aXZlXCIpLmZpcnN0KCkuYXR0cignZGF0YS1wYWdlJyk7XHJcblx0XHRsZXQgZmluaXNoUGFnZSA9ICQoXCIucGFnaW5hdGlvbl9faXRlbS5hY3RpdmVcIikubGFzdCgpLmF0dHIoJ2RhdGEtcGFnZScpO1xyXG5cclxuXHRcdCQuY29va2llKCdwYWdlX251bWJlclN0YXJ0Jywgc3RhcnRQYWdlKTtcclxuXHRcdCQuY29va2llKCdwYWdlX251bWJlckZpbmlzaCcsICsrZmluaXNoUGFnZSk7XHJcblx0fSk7XHJcblxyXG5cdGlmICgkLmNvb2tpZSgncGFnZV9udW1iZXJGaW5pc2gnKSA+IDApIHtcclxuXHJcblx0XHRsZXQgJHRoaXMgPSAkKCcuanMtbG9hZC1tb3JlJyk7XHJcblx0XHRsZXQgJGxvYWRNb3JlV3JhcCA9ICQoJy5qcy1sb2FkLXdyYXAnKTtcclxuXHRcdGxldCBwcmVsb2FkZXIgPSBuZXcgUHJlbG9hZGVyKCRsb2FkTW9yZVdyYXApO1xyXG5cclxuXHRcdGxldCBwYWdlID0gJC5jb29raWUoJ3BhZ2VfbnVtYmVyRmluaXNoJyk7XHJcblx0XHRsZXQgcGFyYW1zID0gJHRoaXMuZGF0YSgncGFyYW1zJyk7XHJcblxyXG5cdFx0cGFyYW1zLnN0YXJ0UGFnZSA9ICAkLmNvb2tpZSgncGFnZV9udW1iZXJTdGFydCcpOztcclxuXHRcdHBhcmFtcy5lbmRQYWdlID0gLS1wYWdlO1xyXG5cclxuXHRcdGxldCB1cmwgPSAkdGhpcy5kYXRhKCdiYWNrdG9jYXRhbG9ndXJsJylcclxuXHJcblx0XHQkLmFqYXgoe1xyXG5cdFx0XHR0eXBlOiAncG9zdCcsXHJcblx0XHRcdHVybDogdXJsLFxyXG5cdFx0XHRkYXRhOiB7XHJcblx0XHRcdFx0cGFnZTogcGFnZSxcclxuXHRcdFx0XHRwYXJhbXM6IHBhcmFtc1xyXG5cdFx0XHR9LFxyXG5cdFx0XHRkYXRhVHlwZTogJ2pzb24nLFxyXG5cdFx0XHRzdWNjZXNzOiBmdW5jdGlvbiAocmVzcG9uc2UpIHtcclxuXHRcdFx0XHRpZiAocmVzcG9uc2Uuc3VjY2Vzcykge1xyXG5cdFx0XHRcdFx0JHRoaXMuZGF0YSgncGFnZScsIHJlc3BvbnNlLnBhZ2UpO1xyXG5cclxuXHRcdFx0XHRcdCRsb2FkTW9yZVdyYXAuYXBwZW5kKHJlc3BvbnNlLmh0bWwpO1xyXG5cclxuXHRcdFx0XHRcdGlmIChyZXNwb25zZS5wYWdpbmF0aW9uKSB7XHJcblx0XHRcdFx0XHRcdCQoJy5qcy1wYWdpbmF0aW9uLWNvbnRhaW5lcicpLmh0bWwocmVzcG9uc2UucGFnaW5hdGlvbik7XHJcblx0XHRcdFx0XHR9XHJcblxyXG5cdFx0XHRcdFx0aWYgKHJlc3BvbnNlLmxhc3QpIHtcclxuXHRcdFx0XHRcdFx0JHRoaXMuaGlkZSgpO1xyXG5cdFx0XHRcdFx0fVxyXG5cclxuXHRcdFx0XHRcdHNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xyXG5cdFx0XHRcdFx0XHQkKCdodG1sLCBib2R5JykuYW5pbWF0ZSh7c2Nyb2xsVG9wOiAkLmNvb2tpZSgncGFnZV9zY3JvbGwnKX0sMTAwMCk7XHJcblx0XHRcdFx0XHRcdGNsZWFyQ29va2llcygpO1xyXG5cdFx0XHRcdFx0fSwgNTAwKTtcclxuXHJcblx0XHRcdFx0XHRwcmVsb2FkZXIuaGlkZSgpO1xyXG5cdFx0XHRcdH1cclxuXHJcblx0XHRcdFx0bW9kdWxlTG9hZGVyLmluaXQoJChkb2N1bWVudCkpO1xyXG5cdFx0XHR9XHJcblx0XHR9KTtcclxuXHR9XHJcblxyXG5cclxuXHJcblx0JCgnYm9keScpLm9uKCdjbGljaycsICcucGFnaW5hdGlvbl9faXRlbScsIGZ1bmN0aW9uICgpe1xyXG5cdFx0Y2xlYXJDb29raWVzKCk7XHJcblx0fSk7XHJcblxyXG5cdGZ1bmN0aW9uIGNsZWFyQ29va2llcyAoKSB7XHJcblx0XHQkLmNvb2tpZSgncGFnZV9udW1iZXJTdGFydCcsIG51bGwpO1xyXG5cdFx0JC5jb29raWUoJ3BhZ2VfbnVtYmVyRmluaXNoJywgbnVsbCk7XHJcblx0XHQkLmNvb2tpZSgncGFnZV9zY3JvbGwnLCBudWxsKTtcclxuXHR9XHJcblxyXG59O1xyXG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9fSFRNTC9zcmMvanMvX21vZHVsZXMvYmFja1RvQ2F0YWxvZy5qcyIsIid1c2Ugc3RyaWN0JztcclxuXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuLy8gSW1wb3J0c1xyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG5cclxuXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuLy8gUHVibGljXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbmZ1bmN0aW9uIGRlbGxDaGlsZCAoKSB7XHJcblx0JChcImJvZHlcIikub24oJ2NsaWNrJywgJy5qcy1kZWxsSW5mbycsIGZ1bmN0aW9uIChldmVudCkge1xyXG5cdFx0ZXZlbnQucHJldmVudERlZmF1bHQoKTtcclxuXHRcdGxldCBpZCA9ICQodGhpcykuY2xvc2VzdCgnLmZvcm0tZ3JvdXAnKS5maW5kKCdpbnB1dFtkYXRhLWRlbGwtaWRdJykuZGF0YSgnZGVsbC1pZCcpO1xyXG5cdFx0bGV0IGFjdGlvbiA9ICAkKHRoaXMpLmNsb3Nlc3QoJy5mb3JtLWdyb3VwJykuZGF0YSgnYWpheCcpO1xyXG5cclxuXHRcdCQuYWpheCh7XHJcblx0XHRcdHVybDogYWN0aW9uLFxyXG5cdFx0XHR0eXBlOiAnUE9TVCcsXHJcblx0XHRcdGRhdGFUeXBlOiAnSlNPTicsXHJcblx0XHRcdGRhdGE6IHtcclxuXHRcdFx0XHRpZDppZFxyXG5cdFx0XHR9LFxyXG5cdFx0XHRzdWNjZXNzOiBmdW5jdGlvbiAoZGF0YSkge1xyXG5cdFx0XHRcdGlmIChkYXRhLnN1Y2Nlc3MpIHtcclxuXHRcdFx0XHRcdGlmIChkYXRhLnJlc3BvbnNlKSB7XHJcblx0XHRcdFx0XHRcdGdlbmVyYXRlKGRhdGEucmVzcG9uc2UsICdzdWNjZXNzJyk7XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRpZiAoZGF0YS5yZWxvYWQpIHtcclxuXHRcdFx0XHRcdFx0d2luZG93LmxvY2F0aW9uLnJlbG9hZCgpO1xyXG5cdFx0XHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHRcdFx0cHJlbG9hZGVyKCk7XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHRcdGlmIChkYXRhLnJlc3BvbnNlKSB7XHJcblx0XHRcdFx0XHRcdGdlbmVyYXRlKGRhdGEucmVzcG9uc2UsICd3YXJuaW5nJyk7XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRwcmVsb2FkZXIoKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH1cclxuXHRcdH0pO1xyXG5cdH0pO1xyXG59XHJcblxyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbi8vIEV4cG9ydHNcclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuZXhwb3J0IHtkZWxsQ2hpbGR9O1xyXG5cclxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vX0hUTUwvc3JjL2pzL19tb2R1bGVzL2RlbGxDaGlsZC5qcyIsIid1c2Ugc3RyaWN0JztcclxuXHJcbi8vIFB1YmxpY1xyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG5mdW5jdGlvbiBjaGFuZ2VTaXplSW5UYWJsZSAoKSB7XHJcblx0ICQoJ2JvZHknKS5vbignY2xpY2snLCAnLnRhYmxlLXNpemVfX3BvcHVwIC5wcm9kdWN0X19zaXplcy1pdGVtJywgZnVuY3Rpb24gKCl7XHJcblx0XHQgbGV0IHRhYk5hbWUgPSAkKHRoaXMpLnRleHQoKTtcclxuXHRcdCAkKCcudGFibGUtc2l6ZV9fcG9wdXAgLnByb2R1Y3RfX3NpemVzLWl0ZW0nKS5yZW1vdmVDbGFzcygnaXMtYWN0aXZlJylcclxuXHJcblx0XHQgJCgnLnRhYicpLmhpZGUoKTtcclxuXHRcdCAkKHRoaXMpLmFkZENsYXNzKCdpcy1hY3RpdmUnKTtcclxuXHJcblx0XHQgJCgnLnRhYi0nK3RhYk5hbWUpLnNob3coKVxyXG5cdCB9KVxyXG59XHJcblxyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbi8vIEV4cG9ydHNcclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuZXhwb3J0IHtjaGFuZ2VTaXplSW5UYWJsZX07XHJcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL19IVE1ML3NyYy9qcy9fbW9kdWxlcy9jaGFuZ2VTaXplSW5UYWJsZS5qcyIsIid1c2Ugc3RyaWN0JztcclxuXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuLy8gSW1wb3J0c1xyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbi8vIFByaXZhdGVcclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4vLyBQdWJsaWNcclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuZnVuY3Rpb24gY2xpY2tNZSAoKSB7XHJcblx0aWYgKCQoJy5jbGlja19tZScpLmxlbmd0aCkge1xyXG5cdFx0c2V0VGltZW91dChmdW5jdGlvbiAoKXtcclxuXHRcdFx0JCgnLmNsaWNrX21lJykudHJpZ2dlcignY2xpY2snKTtcclxuXHRcdH0sIDEwMDApXHJcblx0fVxyXG5cclxuXHQkKCdib2R5Jykub24oJ2NsaWNrJywgJy5vcGVuX19jYWxsYmFjaycsIGZ1bmN0aW9uICgpIHtcclxuXHRcdCQoJy5oZWFkZXItcGhvbmVzX19jYWxsYmFjaycpLnRyaWdnZXIoJ2NsaWNrJylcclxuXHR9KVxyXG59XHJcblxyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbi8vIEV4cG9ydHNcclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuZXhwb3J0IHtjbGlja01lfTtcclxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vX0hUTUwvc3JjL2pzL19tb2R1bGVzL2NsaWNrTWUuanMiLCIndXNlIHN0cmljdCc7XHJcblxyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbi8vIEltcG9ydHNcclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuLy8gUHVibGljXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbmZ1bmN0aW9uIGRlbGl2ZXJ5UmVnaXN0cmF0aW9uICgkY29udGFpbmVyID0gJCgnYm9keScpKSB7XHJcblx0JGNvbnRhaW5lci5maW5kKCcuc2Nyb2xsLWJveCcpLmVhY2goZnVuY3Rpb24gKCkge1xyXG5cclxuXHR9KTtcclxuXHJcblx0bGV0IHJvem4gPSAkKCdpbnB1dFtuYW1lPVwicm96blwiXScpLFxyXG5cdFx0b3B0ID0gJCgnaW5wdXRbbmFtZT1cIm9wdFwiXScpLFxyXG5cdFx0ZHJvcCA9ICQoJ2lucHV0W25hbWU9XCJkcm9wXCJdJyksXHJcblx0XHRub1JlZyA9ICQoJ2lucHV0W25hbWU9XCJuby1wcm9maWxlXCJdJylcclxuXHJcblx0ZnVuY3Rpb24gY2hlY2tSZXEoaW5wdXQpIHtcclxuXHRcdHJvem4ucHJvcCgncmVxdWlyZWQnLCBmYWxzZSk7XHJcblxyXG5cdFx0aWYgKCFyb3puLnByb3AoJ2NoZWNrZWQnKSAmJiAhb3B0LnByb3AoJ2NoZWNrZWQnKSAmJiAhZHJvcC5wcm9wKCdjaGVja2VkJykpe1xyXG5cdFx0XHRyb3puLnByb3AoJ3JlcXVpcmVkJywgdHJ1ZSk7XHJcblx0XHR9XHJcblx0fVxyXG5cclxuXHRyb3puLm9uKCdjbGljaycsIGZ1bmN0aW9uICgpIHtcclxuXHRcdGNoZWNrUmVxKCk7XHJcblx0XHRvcHQucHJvcCgnY2hlY2tlZCcsIGZhbHNlKTtcclxuXHRcdGRyb3AucHJvcCgnY2hlY2tlZCcsIGZhbHNlKTtcclxuXHR9KTtcclxuXHJcblx0b3B0Lm9uKCdjbGljaycsIGZ1bmN0aW9uICgpIHtcclxuXHRcdGNoZWNrUmVxKCk7XHJcblx0XHRyb3puLnByb3AoJ2NoZWNrZWQnLCBmYWxzZSk7XHJcblx0fSk7XHJcblxyXG5cdGRyb3Aub24oJ2NsaWNrJywgZnVuY3Rpb24gKCkge1xyXG5cdFx0Y2hlY2tSZXEoKTtcclxuXHRcdHJvem4ucHJvcCgnY2hlY2tlZCcsIGZhbHNlKTtcclxuXHR9KTtcclxuXHJcblx0bm9SZWcub24oJ2NsaWNrJywgZnVuY3Rpb24gKCkge1xyXG5cdFx0cm96bi5wcm9wKCdjaGVja2VkJywgZmFsc2UpO1xyXG5cdFx0b3B0LnByb3AoJ2NoZWNrZWQnLCBmYWxzZSk7XHJcblx0XHRkcm9wLnByb3AoJ2NoZWNrZWQnLCBmYWxzZSk7XHJcblxyXG5cdFx0aWYoJCh0aGlzKS5wcm9wKCdjaGVja2VkJykpIHtcclxuXHRcdFx0JCgnLmNhcnQtaW5mb19faXRlbScpLmZpbmQoJy5jYXJ0LXJlZ2lzdHJhdGlvbl9fc3RhdHVzJykuaGlkZSgpO1xyXG5cdFx0fSBlbHNlIHtcclxuXHRcdFx0JCgnLmNhcnQtaW5mb19faXRlbScpLmZpbmQoJy5jYXJ0LXJlZ2lzdHJhdGlvbl9fc3RhdHVzJykuc2hvdygpO1xyXG5cdFx0fVxyXG5cdH0pO1xyXG5cclxuXHRpZiAobm9SZWcucHJvcCgnY2hlY2tlZCcpKSB7XHJcblx0XHRyb3puLnByb3AoJ2NoZWNrZWQnLCBmYWxzZSk7XHJcblx0XHRvcHQucHJvcCgnY2hlY2tlZCcsIGZhbHNlKTtcclxuXHRcdGRyb3AucHJvcCgnY2hlY2tlZCcsIGZhbHNlKTtcclxuXHRcdCQoJy5jYXJ0LWluZm9fX2l0ZW0nKS5maW5kKCcuY2FydC1yZWdpc3RyYXRpb25fX3N0YXR1cycpLmhpZGUoKTtcclxuXHR9XHJcbn1cclxuXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuLy8gRXhwb3J0c1xyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG5leHBvcnQge2RlbGl2ZXJ5UmVnaXN0cmF0aW9ufTtcclxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vX0hUTUwvc3JjL2pzL19tb2R1bGVzL2RlbGl2ZXJ5UmVnaXN0cmF0aW9uLmpzIiwiJ3VzZSBzdHJpY3QnO1xyXG5cclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4vLyBJbXBvcnRzXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbmltcG9ydCBzZXRDaGVja2VycyBmcm9tIFwiLi9leHBvcnQtbW9kdWxlL3NldC1jaGVja2Vyc1wiO1xyXG5pbXBvcnQgcGFuZWxXaXRoQnRucyBmcm9tIFwiLi9leHBvcnQtbW9kdWxlL3BhbmVsLXdpdGgtYnRuc1wiXHJcbmltcG9ydCBsaXN0VG9nZ2xlIGZyb20gXCIuL2V4cG9ydC1tb2R1bGUvbGlzdC10b2dnbGVcIlxyXG5pbXBvcnQgcHJvZHVjdHNEb3dubG9hZCBmcm9tIFwiLi9leHBvcnQtbW9kdWxlL3Byb2R1Y3RzLWRvd25sb2FkXCI7XHJcbmltcG9ydCBwcmljZUZyb21UbyBmcm9tIFwiLi9leHBvcnQtbW9kdWxlL3ByaWNlLWZyb20tdG9cIjtcclxuaW1wb3J0IHByaWNlRnJvbUNsaWVudCBmcm9tIFwiLi9leHBvcnQtbW9kdWxlL3ByaWNlLWZyb20tY2xpZW50XCI7XHJcbmltcG9ydCBpbmZvQmxvY2sgZnJvbSBcIi4vZXhwb3J0LW1vZHVsZS9pbmZvLWJsb2NrXCI7XHJcbmltcG9ydCBhamF4RmlsdGVyIGZyb20gXCIuL2V4cG9ydC1tb2R1bGUvYWpheC1maWx0ZXJcIjtcclxuaW1wb3J0IHNhdmUgZnJvbSBcIi4vZXhwb3J0LW1vZHVsZS9zYXZlXCI7XHJcbmltcG9ydCBjb3B5TGluayBmcm9tIFwiLi9leHBvcnQtbW9kdWxlL2NvcHktbGlua1wiO1xyXG5cclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4vLyBQdWJsaWNcclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuZnVuY3Rpb24gZXhwb3J0VGFibGUoKSB7XHJcblx0aWYgKCQoJ1tkYXRhLWV4cG9ydC10YWJsZV0nKS5sZW5ndGgpIHtcclxuXHJcblx0XHQvLyDQn9GA0L7RgdGC0LDQvdC+0LLQutCwINGH0LXQutC10YDQvtCyICsg0L7RgtC/0YDQsNCy0LrQsCBpZCDQutCw0YLQtdCz0L7RgNC40LhcclxuXHRcdHNldENoZWNrZXJzKCk7XHJcblxyXG5cdFx0Ly8g0J/QsNC90LXQu9GMINC60L3QvtC/0L7QuiDQtNC70Y8g0YfQtdC60LXRgNC+0LJcclxuXHRcdHBhbmVsV2l0aEJ0bnMoKTtcclxuXHJcblx0XHQvLyDQoNCw0LfQstC+0YDQsNGH0LjQstCw0L3QuNC1INGB0L/QuNGB0LrQvtCyXHJcblx0XHRsaXN0VG9nZ2xlKCk7XHJcblxyXG5cdFx0Ly8g0J/QvtC00LPRgNGD0LfQutCwINGC0L7QstCw0YDQvtCyINC/0YDQuCDRgNCw0YHRgdC60YDRi9GC0LjQuCDRgdC/0LjRgdC60LAg0YEg0L/RgNC40LzQtdC90LXQvdC40LXQvCDRhNC40LvRjNGC0YDQsFxyXG5cdFx0cHJvZHVjdHNEb3dubG9hZCgpO1xyXG5cclxuXHRcdC8vINCh0LvQsNC50LTQtdGAINGG0LXQvdGLINC+0YIg0Lgg0LTQvlxyXG5cdFx0cHJpY2VGcm9tVG8oKTtcclxuXHJcblx0XHQvLyDQodC70LDQudC00LXRgCDRhtC10L0g0LrQu9C40LXQvdGC0LBcclxuXHRcdHByaWNlRnJvbUNsaWVudCgpO1xyXG5cclxuXHRcdC8vIEluZm8g0LHQu9C+0LpcclxuXHRcdGluZm9CbG9jaygpO1xyXG5cclxuXHRcdC8vIEFqYXgg0L3QsCDRhNC40LvRjNGC0YBcclxuXHRcdGFqYXhGaWx0ZXIoKTtcclxuXHJcblx0XHQvLyDQodC+0YXRgNCw0L3QtdC90LjQtVxyXG5cdFx0c2F2ZSgpO1xyXG5cclxuXHRcdC8vINCa0L7Qv9C40YDQvtCy0LDQvdC40LUgVVJMXHJcblx0XHRjb3B5TGluaygpO1xyXG5cdH1cclxufVxyXG5cclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4vLyBFeHBvcnRzXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbmV4cG9ydCB7ZXhwb3J0VGFibGV9O1xyXG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9fSFRNTC9zcmMvanMvX21vZHVsZXMvZXhwb3J0LXRhYmxlLmpzIiwiJ3VzZSBzdHJpY3QnO1xyXG5cclxuLyoqXHJcbiAqXHJcbiAqIEBtb2R1bGVcclxuICovXHJcblxyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbi8vIEltcG9ydHNcclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4vLyBQcml2YXRlXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbi8qKlxyXG4gKiBAcGFyYW0ge0pRdWVyeX0gJGVsZW1lbnRcclxuICogQHJldHVybiB7c3RyaW5nfVxyXG4gKiBAcHJpdmF0ZVxyXG4gKi9cclxuXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuLy8gUHVibGljXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbi8qKlxyXG4gKiBAcGFyYW0ge0pRdWVyeX0gJGVsZW1lbnRzXHJcbiAqL1xyXG5mdW5jdGlvbiBzZXRDaGVja2VycyAoKSB7XHJcblx0alF1ZXJ5LmZuLnJldmVyc2UgPSBbXS5yZXZlcnNlO1xyXG5cclxuXHQkKFwiYm9keVwiKS5vbignY2xpY2snLCAnaW5wdXQ6Y2hlY2tib3gnLCBmdW5jdGlvbiAoKSB7XHJcblx0XHRsZXQgJHRoaXMgPSAkKHRoaXMpLFxyXG5cdFx0XHRpc0NoZWNrZWQgPSAkdGhpcy5pcyhcIjpjaGVja2VkXCIpLFxyXG5cdFx0XHR1cmwgPSAkKCcuanMtY2xpY2stcHJvZHVjdC1jYXRlZ29yaWVzJykuZGF0YSgnY2xpY2snKSxcclxuXHRcdFx0Y2F0ZWdvcmllc0lkID0gW10sXHJcblx0XHRcdHNvcnQgPSAkKCcjc29ydCcpLnZhbCgpLFxyXG5cdFx0XHRhdmFpbGFiaWxpdHkgPSAkKCcuZXhwb3J0X19hdmFpbGFiaWxpdHkgaW5wdXQnKS5wcm9wKCdjaGVja2VkJyksXHJcblx0XHRcdGFtb3VudCA9ICQoJyNhbW91bnQnKS52YWwoKTtcclxuXHJcblx0XHQkdGhpcy5wYXJlbnQoKS5uZXh0KCkuZmluZChcImlucHV0OmNoZWNrYm94XCIpLnByb3AoXCJjaGVja2VkXCIsIGlzQ2hlY2tlZCk7XHJcblxyXG5cdFx0JHRoaXMucGFyZW50cyhcInVsXCIpLnByZXYoXCJhXCIpLmZpbmQoXCJpbnB1dDpjaGVja2JveFwiKS5yZXZlcnNlKCkuZWFjaChmdW5jdGlvbiAoYSwgYikge1xyXG5cdFx0XHQkKGIpLnByb3AoXCJjaGVja2VkXCIsIGZ1bmN0aW9uICgpIHtcclxuXHRcdFx0XHRyZXR1cm4gJChiKS5wYXJlbnQoXCJhXCIpLm5leHQoXCJ1bFwiKS5maW5kKFwiOmNoZWNrZWRcIikubGVuZ3RoO1xyXG5cdFx0XHR9KTtcclxuXHRcdH0pO1xyXG5cclxuXHRcdC8vINCe0YLQv9GA0LDQstC60LAgaWQg0LrQsNGC0LXQs9C+0YDQuNC4INC/0YDQuCBjaGVja2V0ZWRcclxuXHRcdGlmICgkdGhpcy5pcygnOmNoZWNrZWQnKSkge1xyXG5cdFx0XHQkdGhpcy5jbG9zZXN0KCdsaScpLmZpbmQoJy5qcy1wcm9kdWN0LXNob3cnKS5lYWNoKGZ1bmN0aW9uICgpIHtcclxuXHRcdFx0XHRjYXRlZ29yaWVzSWQucHVzaCgkKHRoaXMpLmRhdGEoJ2lkJykpXHJcblx0XHRcdH0pXHJcblxyXG5cdFx0XHQkLmFqYXgoe1xyXG5cdFx0XHRcdHVybDogdXJsLFxyXG5cdFx0XHRcdHR5cGU6ICdwb3N0JyxcclxuXHRcdFx0XHRkYXRhOiB7XHJcblx0XHRcdFx0XHRjYXRlZ29yaWVzSWQ6IGNhdGVnb3JpZXNJZCxcclxuXHRcdFx0XHRcdHNvcnQ6IHNvcnQsXHJcblx0XHRcdFx0XHRhdmFpbGFiaWxpdHk6IGF2YWlsYWJpbGl0eSxcclxuXHRcdFx0XHRcdGFtb3VudDogYW1vdW50LFxyXG5cdFx0XHRcdFx0Y2hlY2tlZENhdGVnb3JpZXM6IHRydWVcclxuXHRcdFx0XHR9LFxyXG5cdFx0XHRcdGRhdGFUeXBlOiAnanNvbidcclxuXHRcdFx0fSk7XHJcblx0XHR9IGVsc2Uge1xyXG5cdFx0XHQkdGhpcy5jbG9zZXN0KCdsaScpLmZpbmQoJy5qcy1wcm9kdWN0LXNob3cnKS5lYWNoKGZ1bmN0aW9uICgpIHtcclxuXHRcdFx0XHRjYXRlZ29yaWVzSWQucHVzaCgkKHRoaXMpLmRhdGEoJ2lkJykpXHJcblx0XHRcdH0pXHJcblxyXG5cdFx0XHQkLmFqYXgoe1xyXG5cdFx0XHRcdHVybDogdXJsLFxyXG5cdFx0XHRcdHR5cGU6ICdwb3N0JyxcclxuXHRcdFx0XHRkYXRhOiB7XHJcblx0XHRcdFx0XHRjYXRlZ29yaWVzSWQ6IGNhdGVnb3JpZXNJZCxcclxuXHRcdFx0XHRcdGNoZWNrZWRDYXRlZ29yaWVzOiBmYWxzZVxyXG5cdFx0XHRcdH0sXHJcblx0XHRcdFx0ZGF0YVR5cGU6ICdqc29uJ1xyXG5cdFx0XHR9KTtcclxuXHRcdH1cclxuXHR9KTtcclxuXHJcblx0Ly8g0J7RgtC/0YDQsNCy0LrQsCBpZCDRgtC+0LLQsNGA0LBcclxuXHQkKCdib2R5Jykub24oJ2NsaWNrJywgJy5qcy1wcm9kdWN0LWluLXRhYmxlJywgZnVuY3Rpb24gKCkge1xyXG5cdFx0bGV0ICR0aGlzID0gJCh0aGlzKSxcclxuXHRcdFx0dXJsID0gJCgnLmpzLWNsaWNrLXByb2R1Y3QtY2F0ZWdvcmllcycpLmRhdGEoJ2NsaWNrJyksXHJcblx0XHRcdHByb2R1Y3RJZCA9IFtdLFxyXG5cdFx0XHRzb3J0ID0gJCgnI3NvcnQnKS52YWwoKSxcclxuXHRcdFx0YXZhaWxhYmlsaXR5ID0gJCgnLmV4cG9ydF9fYXZhaWxhYmlsaXR5IGlucHV0JykucHJvcCgnY2hlY2tlZCcpLFxyXG5cdFx0XHRhbW91bnQgPSAkKCcjYW1vdW50JykudmFsKCk7XHJcblxyXG5cdFx0aWYgKCR0aGlzLmlzKCc6Y2hlY2tlZCcpKSB7XHJcblx0XHRcdHByb2R1Y3RJZC5wdXNoKCR0aGlzLmRhdGEoJ2lkJykpXHJcblxyXG5cdFx0XHQkLmFqYXgoe1xyXG5cdFx0XHRcdHVybDogdXJsLFxyXG5cdFx0XHRcdHR5cGU6ICdwb3N0JyxcclxuXHRcdFx0XHRkYXRhOiB7XHJcblx0XHRcdFx0XHRzb3J0OiBzb3J0LFxyXG5cdFx0XHRcdFx0YXZhaWxhYmlsaXR5OiBhdmFpbGFiaWxpdHksXHJcblx0XHRcdFx0XHRhbW91bnQ6IGFtb3VudCxcclxuXHRcdFx0XHRcdHByb2R1Y3RJZDogcHJvZHVjdElkLFxyXG5cdFx0XHRcdFx0Y2hlY2tlZFByb2R1Y3Q6IHRydWVcclxuXHRcdFx0XHR9LFxyXG5cdFx0XHRcdGRhdGFUeXBlOiAnanNvbidcclxuXHRcdFx0fSk7XHJcblx0XHR9IGVsc2Uge1xyXG5cdFx0XHRwcm9kdWN0SWQucHVzaCgkdGhpcy5kYXRhKCdpZCcpKVxyXG5cclxuXHRcdFx0JC5hamF4KHtcclxuXHRcdFx0XHR1cmw6IHVybCxcclxuXHRcdFx0XHR0eXBlOiAncG9zdCcsXHJcblx0XHRcdFx0ZGF0YToge1xyXG5cdFx0XHRcdFx0cHJvZHVjdElkOiBwcm9kdWN0SWQsXHJcblx0XHRcdFx0XHRjaGVja2VkUHJvZHVjdDogZmFsc2VcclxuXHRcdFx0XHR9LFxyXG5cdFx0XHRcdGRhdGFUeXBlOiAnanNvbidcclxuXHRcdFx0fSk7XHJcblx0XHR9XHJcblx0fSlcclxuXHJcblx0JChcImJvZHlcIikub24oJ2NsaWNrJywgJ2lucHV0OmNoZWNrYm94JywgZnVuY3Rpb24gKGUpIHtcclxuXHRcdGxldCAkdGhpcyA9ICQodGhpcyksXHJcblx0XHRcdGxldmVsID0gJHRoaXMuY2xvc2VzdCgnLm5lc3RpbmctbGV2ZWwnKS5kYXRhKCdsZXZlbCcpLFxyXG5cdFx0XHRhbGxJbnB1dHMgPSAkdGhpcy5jbG9zZXN0KCcubmVzdGluZy1sZXZlbCcpLmZpbmQoJ2xpIGlucHV0JykubGVuZ3RoLFxyXG5cdFx0XHRjaGVja2VkSW5wdXRzID0gJHRoaXMuY2xvc2VzdCgnLm5lc3RpbmctbGV2ZWwnKS5maW5kKCdsaSBpbnB1dDpjaGVja2VkJykubGVuZ3RoO1xyXG5cclxuXHRcdGlmIChsZXZlbCA9PT0gMSkge1xyXG5cdFx0XHRpZiAoJHRoaXMuaXMoJzpjaGVja2VkJykpIHtcclxuXHRcdFx0XHQkdGhpcy5hdHRyKCdkYXRhLW5vdEFsbCcsIHRydWUpLmNsb3Nlc3QoJy5uZXN0aW5nLWxldmVsW2RhdGEtbGV2ZWw9XCIxXCJdJykuZmluZCgnaW5wdXQnKS5hdHRyKCdkYXRhLW5vdEFsbCcsIHRydWUpXHJcblx0XHRcdH1cclxuXHRcdH0gZWxzZSBpZiAobGV2ZWwgPT09IDIpIHtcclxuXHRcdFx0aWYgKGFsbElucHV0cyA9PT0gY2hlY2tlZElucHV0cykge1xyXG5cdFx0XHRcdCR0aGlzLmNsb3Nlc3QoJy5uZXN0aW5nLWxldmVsW2RhdGEtbGV2ZWw9XCIxXCJdJykuZmluZCgnLmNoZWNrYm94X19pY29uLS1tYWluLWNhdGVnb3J5JykubmV4dCgpLmZpbmQoJ2lucHV0JykuYXR0cignZGF0YS1ub3RBbGwnLCB0cnVlKVxyXG5cdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdCR0aGlzLmNsb3Nlc3QoJy5uZXN0aW5nLWxldmVsW2RhdGEtbGV2ZWw9XCIxXCJdJykuZmluZCgnLmNoZWNrYm94X19pY29uLS1tYWluLWNhdGVnb3J5JykubmV4dCgpLmZpbmQoJ2lucHV0JykuYXR0cignZGF0YS1ub3RBbGwnLCBmYWxzZSlcclxuXHRcdFx0fVxyXG5cdFx0fSBlbHNlIGlmIChsZXZlbCA9PT0gMykge1xyXG5cdFx0XHRpZiAoYWxsSW5wdXRzID09PSBjaGVja2VkSW5wdXRzKSB7XHJcblx0XHRcdFx0JHRoaXMuY2xvc2VzdCgnLm5lc3RpbmctbGV2ZWxbZGF0YS1sZXZlbD1cIjNcIl0nKS5wcmV2KCkuZmluZCgnaW5wdXQnKS5hdHRyKCdkYXRhLW5vdEFsbCcsIHRydWUpLmNsb3Nlc3QoJy5uZXN0aW5nLWxldmVsW2RhdGEtbGV2ZWw9XCIxXCJdJykuZmluZCgnLmNoZWNrYm94X19pY29uLS1tYWluLWNhdGVnb3J5JykubmV4dCgpLmZpbmQoJ2lucHV0JykuYXR0cignZGF0YS1ub3RBbGwnLCB0cnVlKVxyXG5cdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdCR0aGlzLmNsb3Nlc3QoJy5uZXN0aW5nLWxldmVsW2RhdGEtbGV2ZWw9XCIzXCJdJykucHJldigpLmZpbmQoJ2lucHV0JykuYXR0cignZGF0YS1ub3RBbGwnLCBmYWxzZSkuY2xvc2VzdCgnLm5lc3RpbmctbGV2ZWxbZGF0YS1sZXZlbD1cIjFcIl0nKS5maW5kKCcuY2hlY2tib3hfX2ljb24tLW1haW4tY2F0ZWdvcnknKS5uZXh0KCkuZmluZCgnaW5wdXQnKS5hdHRyKCdkYXRhLW5vdEFsbCcsIGZhbHNlKVxyXG5cdFx0XHR9XHJcblx0XHR9IGVsc2Uge1xyXG5cdFx0XHRpZiAoYWxsSW5wdXRzID09PSBjaGVja2VkSW5wdXRzKSB7XHJcblx0XHRcdFx0JHRoaXMuY2xvc2VzdCgnLm5lc3RpbmctbGV2ZWwnKS5wcmV2KCkuZmluZCgnaW5wdXQnKS5hdHRyKCdkYXRhLW5vdEFsbCcsIHRydWUpLmNsb3Nlc3QoJy5uZXN0aW5nLWxldmVsW2RhdGEtbGV2ZWw9XCIxXCJdJykuZmluZCgnLmNoZWNrYm94X19pY29uLS1tYWluLWNhdGVnb3J5JykubmV4dCgpLmZpbmQoJ2lucHV0JykuYXR0cignZGF0YS1ub3RBbGwnLCB0cnVlKVxyXG5cdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdCR0aGlzLmNsb3Nlc3QoJy5uZXN0aW5nLWxldmVsJykucHJldigpLmZpbmQoJ2lucHV0JykuYXR0cignZGF0YS1ub3RBbGwnLCBmYWxzZSkuY2xvc2VzdCgnLm5lc3RpbmctbGV2ZWxbZGF0YS1sZXZlbD1cIjFcIl0nKS5maW5kKCcuY2hlY2tib3hfX2ljb24tLW1haW4tY2F0ZWdvcnknKS5uZXh0KCkuZmluZCgnaW5wdXQnKS5hdHRyKCdkYXRhLW5vdEFsbCcsIGZhbHNlKVxyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0fSlcclxufVxyXG5cclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4vLyBFeHBvcnRzXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IHNldENoZWNrZXJzO1xyXG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9fSFRNTC9zcmMvanMvX21vZHVsZXMvZXhwb3J0LW1vZHVsZS9zZXQtY2hlY2tlcnMuanMiLCIndXNlIHN0cmljdCc7XHJcblxyXG4vKipcclxuICpcclxuICogQG1vZHVsZVxyXG4gKi9cclxuXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuLy8gSW1wb3J0c1xyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbi8vIFByaXZhdGVcclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuLyoqXHJcbiAqIEBwYXJhbSB7SlF1ZXJ5fSAkZWxlbWVudFxyXG4gKiBAcmV0dXJuIHtzdHJpbmd9XHJcbiAqIEBwcml2YXRlXHJcbiAqL1xyXG5cclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4vLyBQdWJsaWNcclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuLyoqXHJcbiAqIEBwYXJhbSB7SlF1ZXJ5fSAkZWxlbWVudHNcclxuICovXHJcbmZ1bmN0aW9uIHBhbmVsV2l0aEJ0bnMgKCkge1xyXG5cdCQoJy5qcy1zZXQtY2hlY2tlcnMnKS5vbignY2xpY2snLCBmdW5jdGlvbiAoKSB7XHJcblx0XHRsZXQgYWN0aW9uID0gJCh0aGlzKS5kYXRhKCdzZXQtY2hlY2tlcnMnKSxcclxuXHRcdFx0dXJsID0gJCgnLmpzLWNsaWNrLXByb2R1Y3QtY2F0ZWdvcmllcycpLmRhdGEoJ2NsaWNrJyk7XHJcblxyXG5cdFx0ZnVuY3Rpb24gc2V0Q2hlY2tlcnMoc3RhdHVzKSB7XHJcblx0XHRcdCQoXCIuZXhwb3J0X19jaGVja2VycyBpbnB1dFt0eXBlPSdjaGVja2JveCddXCIpLmVhY2goZnVuY3Rpb24gKCkge1xyXG5cdFx0XHRcdCQodGhpcykucHJvcCgnY2hlY2tlZCcsIHN0YXR1cylcclxuXHRcdFx0fSlcclxuXHRcdH1cclxuXHJcblx0XHRmdW5jdGlvbiB0b2dnbGVDaGVja2VycyhzdGF0dXMpIHtcclxuXHRcdFx0JChcImlucHV0W3R5cGU9J2NoZWNrYm94J106bm90KDpjaGVja2VkKVwiKS5lYWNoKGZ1bmN0aW9uICgpIHtcclxuXHRcdFx0XHQkKHRoaXMpLmNsb3Nlc3QoJ2xpJykuY3NzKCdkaXNwbGF5Jywgc3RhdHVzKTtcclxuXHRcdFx0fSlcclxuXHRcdH1cclxuXHJcblx0XHRzd2l0Y2ggKGFjdGlvbikge1xyXG5cdFx0XHRjYXNlICdhbGwnOlxyXG5cdFx0XHRcdHNldENoZWNrZXJzKHRydWUpXHJcblxyXG5cdFx0XHRcdGxldCBjaGVja2VyZWRFbXB0eUNhdGVnb3JpZXMgPSBbXSxcclxuXHRcdFx0XHRcdHNvcnQgPSAkKCcjc29ydCcpLnZhbCgpLFxyXG5cdFx0XHRcdFx0YXZhaWxhYmlsaXR5ID0gJCgnLmV4cG9ydF9fYXZhaWxhYmlsaXR5IGlucHV0JykucHJvcCgnY2hlY2tlZCcpLFxyXG5cdFx0XHRcdFx0YW1vdW50ID0gJCgnI2Ftb3VudCcpLnZhbCgpO1xyXG5cclxuXHRcdFx0XHQkKCcuanMtcHJvZHVjdC1zaG93JykuZWFjaChmdW5jdGlvbiAoKSB7XHJcblx0XHRcdFx0XHRjaGVja2VyZWRFbXB0eUNhdGVnb3JpZXMucHVzaCgkKHRoaXMpLm5leHQoKS5maW5kKCdpbnB1dCcpLmRhdGEoJ2lkJykpO1xyXG5cdFx0XHRcdH0pXHJcblxyXG5cdFx0XHRcdCQuYWpheCh7XHJcblx0XHRcdFx0XHR1cmw6IHVybCxcclxuXHRcdFx0XHRcdHR5cGU6ICdwb3N0JyxcclxuXHRcdFx0XHRcdGRhdGE6IHtcclxuXHRcdFx0XHRcdFx0c29ydDogc29ydCxcclxuXHRcdFx0XHRcdFx0YXZhaWxhYmlsaXR5OiBhdmFpbGFiaWxpdHksXHJcblx0XHRcdFx0XHRcdGFtb3VudDogYW1vdW50LFxyXG5cdFx0XHRcdFx0XHRjaGVja2VyZWRFbXB0eUNhdGVnb3JpZXM6IGNoZWNrZXJlZEVtcHR5Q2F0ZWdvcmllc1xyXG5cdFx0XHRcdFx0fSxcclxuXHRcdFx0XHRcdGRhdGFUeXBlOiAnanNvbidcclxuXHRcdFx0XHR9KTtcclxuXHJcblx0XHRcdFx0YnJlYWs7XHJcblx0XHRcdGNhc2UgJ25vbmUnOlxyXG5cdFx0XHRcdHNldENoZWNrZXJzKGZhbHNlKVxyXG5cclxuXHRcdFx0XHQkLmFqYXgoe1xyXG5cdFx0XHRcdFx0dXJsOiB1cmwsXHJcblx0XHRcdFx0XHR0eXBlOiAncG9zdCcsXHJcblx0XHRcdFx0XHRkYXRhOiB7XHJcblx0XHRcdFx0XHRcdGRlbGxBbGw6IHRydWUsXHJcblx0XHRcdFx0XHR9LFxyXG5cdFx0XHRcdFx0ZGF0YVR5cGU6ICdqc29uJ1xyXG5cdFx0XHRcdH0pO1xyXG5cclxuXHRcdFx0XHRicmVhaztcclxuXHRcdFx0Y2FzZSAnb25seUNoZWNrJzpcclxuXHRcdFx0XHR0b2dnbGVDaGVja2Vycygnbm9uZScpO1xyXG5cdFx0XHRcdGJyZWFrO1xyXG5cdFx0XHRkZWZhdWx0OlxyXG5cdFx0XHRcdHRvZ2dsZUNoZWNrZXJzKCdibG9jaycpO1xyXG5cdFx0fVxyXG5cdH0pXHJcbn1cclxuXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuLy8gRXhwb3J0c1xyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG5leHBvcnQgZGVmYXVsdCBwYW5lbFdpdGhCdG5zO1xyXG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9fSFRNTC9zcmMvanMvX21vZHVsZXMvZXhwb3J0LW1vZHVsZS9wYW5lbC13aXRoLWJ0bnMuanMiLCIndXNlIHN0cmljdCc7XHJcblxyXG4vKipcclxuICpcclxuICogQG1vZHVsZVxyXG4gKi9cclxuXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuLy8gSW1wb3J0c1xyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbi8vIFByaXZhdGVcclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuLyoqXHJcbiAqIEBwYXJhbSB7SlF1ZXJ5fSAkZWxlbWVudFxyXG4gKiBAcmV0dXJuIHtzdHJpbmd9XHJcbiAqIEBwcml2YXRlXHJcbiAqL1xyXG5cclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4vLyBQdWJsaWNcclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuLyoqXHJcbiAqIEBwYXJhbSB7SlF1ZXJ5fSAkZWxlbWVudHNcclxuICovXHJcbmZ1bmN0aW9uIGxpc3RUb2dnbGUgKCkge1xyXG5cdCQoJy5jaGVja2JveF9faWNvbicpLm9uKCdjbGljaycsIGZ1bmN0aW9uIChlKSB7XHJcblx0XHRsZXQgaWNvbiA9ICQodGhpcyk7XHJcblxyXG5cdFx0aWYgKGljb24uaGFzQ2xhc3MoJ2NoZWNrYm94X19pY29uLS1tYWluLWNhdGVnb3J5IG9wZW5lZCcpKSB7XHJcblx0XHRcdGljb24udG9nZ2xlQ2xhc3MoJ29wZW5lZCcpLmNsb3Nlc3QoJ2xpJykuZmluZCgnLmV4cG9ydF9fY2hlY2tlcnMnKS5hZGRDbGFzcygnaXMtaGlkZScpLmVhY2goZnVuY3Rpb24gKCkge1xyXG5cdFx0XHRcdCQodGhpcykuZmluZCgnLmNoZWNrYm94X19pY29uLm9wZW5lZCcpLnJlbW92ZUNsYXNzKCdvcGVuZWQnKVxyXG5cdFx0XHR9KVxyXG5cdFx0fSBlbHNlIHtcclxuXHRcdFx0aWNvbi50b2dnbGVDbGFzcygnb3BlbmVkJykucGFyZW50KCkuY2hpbGRyZW4oJ3VsJykudG9nZ2xlQ2xhc3MoJ2lzLWhpZGUnKTtcclxuXHRcdH1cclxuXHR9KVxyXG59XHJcblxyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbi8vIEV4cG9ydHNcclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgbGlzdFRvZ2dsZTtcclxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vX0hUTUwvc3JjL2pzL19tb2R1bGVzL2V4cG9ydC1tb2R1bGUvbGlzdC10b2dnbGUuanMiLCIndXNlIHN0cmljdCc7XHJcblxyXG4vKipcclxuICpcclxuICogQG1vZHVsZVxyXG4gKi9cclxuXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuLy8gSW1wb3J0c1xyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbi8vIFByaXZhdGVcclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuLyoqXHJcbiAqIEBwYXJhbSB7SlF1ZXJ5fSAkZWxlbWVudFxyXG4gKiBAcmV0dXJuIHtzdHJpbmd9XHJcbiAqIEBwcml2YXRlXHJcbiAqL1xyXG5cclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4vLyBQdWJsaWNcclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuLyoqXHJcbiAqIEBwYXJhbSB7SlF1ZXJ5fSAkZWxlbWVudHNcclxuICovXHJcbmZ1bmN0aW9uIHByb2R1Y3RzRG93bmxvYWQgKCkge1xyXG5cdCQoJy5qcy1wcm9kdWN0LXNob3cnKS5vbignY2xpY2snLCBmdW5jdGlvbiAoZSkge1xyXG5cdFx0bGV0ICR0aGlzID0gJCh0aGlzKSxcclxuXHRcdFx0aWQgPSAkdGhpcy5kYXRhKCdpZCcpLFxyXG5cdFx0XHR1cmwgPSAkKCcuZXhwb3J0X19jaGVja2VycycpLmRhdGEoJ3VybCcpLFxyXG5cdFx0XHRzb3J0ID0gJCgnI3NvcnQnKS52YWwoKSxcclxuXHRcdFx0YW1vdW50ID0gJCgnI2Ftb3VudCcpLnZhbCgpLFxyXG5cdFx0XHRhdmFpbGFiaWxpdHkgPSAkKCcuZXhwb3J0X19hdmFpbGFiaWxpdHkgaW5wdXQnKS5wcm9wKCdjaGVja2VkJyksXHJcblx0XHRcdGNoZWNrZXJlZFByb2R1Y3RzID0gW10sXHJcblx0XHRcdHNlbGVjdCA9IG51bGwsXHJcblx0XHRcdHJhbmdlID0gW10sXHJcblx0XHRcdGxlbmd0aEFsbCA9ICR0aGlzLmNsb3Nlc3QoJ2xpJykuZmluZCgnLmV4cG9ydF9fY2hlY2tlcnMgaW5wdXQnKS5sZW5ndGgsXHJcblx0XHRcdGxlbmd0aENoZWNrZXRlZCA9ICR0aGlzLmNsb3Nlc3QoJ2xpJykuZmluZCgnLmV4cG9ydF9fY2hlY2tlcnMgaW5wdXQ6Y2hlY2tlZCcpLmxlbmd0aDtcclxuXHJcblx0XHRpZiAoJHRoaXMubmV4dCgpLmZpbmQoJ2lucHV0JykuaXMoJzpjaGVja2VkJykpIHtcclxuXHRcdFx0Ly8gY29uc29sZS5sb2coJ9Cn0LXQutC90YPRgtCw0Y8g0LrQsNGC0LXQs9C+0YDQuNGPJyk7XHJcblx0XHRcdHNlbGVjdCA9IG51bGw7XHJcblxyXG5cdFx0XHRpZiAobGVuZ3RoQWxsID09IGxlbmd0aENoZWNrZXRlZCkge1xyXG5cdFx0XHRcdHNlbGVjdCA9IGlkO1xyXG5cdFx0XHR9XHJcblx0XHR9IGVsc2Uge1xyXG5cdFx0XHQvLyBjb25zb2xlLmxvZygn0J3QtSDRh9C10LrQvdGD0YLQsNGPINC60LDRgtC10LPQvtGA0LjRjycpO1xyXG5cclxuXHRcdFx0c2VsZWN0ID0gbnVsbDtcclxuXHRcdFx0aWYgKCR0aGlzLmNsb3Nlc3QoJ2xpJykuZmluZCgnLmV4cG9ydF9fY2hlY2tlcnMnKS5sZW5ndGgpIHtcclxuXHRcdFx0XHQvLyBjb25zb2xlLmxvZygn0LXRgdGC0Ywg0YLQvtCy0LDRgCcpO1xyXG5cdFx0XHRcdGlmIChsZW5ndGhBbGwgPT0gbGVuZ3RoQ2hlY2tldGVkKSB7XHJcblx0XHRcdFx0XHQvLyBjb25zb2xlLmxvZygn0LLRgdC1INCy0YvQsdGA0LDQvdC+Jyk7XHJcblx0XHRcdFx0XHQvLyBzZWxlY3QgPSBpZDtcclxuXHRcdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdFx0Ly8gY29uc29sZS5sb2coJ9C90LjRh9C10LPQviDQvdC1INCy0YvQsdGA0LDQvdC+Jyk7XHJcblx0XHRcdFx0XHQvLyBzZWxlY3QgPSBudWxsXHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdC8vIGNvbnNvbGUubG9nKCfQvdC10YIg0YLQvtCy0YDQvtCyJyk7XHJcblx0XHRcdFx0c2VsZWN0ID0gbnVsbDtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cclxuXHRcdC8vIGNvbnNvbGUubG9nKHNlbGVjdCk7XHJcblxyXG5cdFx0aWYgKCQodGhpcykuaGFzQ2xhc3MoJ29wZW5lZCcpKSB7XHJcblx0XHRcdCR0aGlzLnBhcmVudCgpLmZpbmQoJy5leHBvcnRfX2NoZWNrZXJzIGlucHV0OmNoZWNrZWQnKS5lYWNoKGZ1bmN0aW9uICgpIHtcclxuXHRcdFx0XHRjaGVja2VyZWRQcm9kdWN0cy5wdXNoKCQodGhpcykuZGF0YSgnaWQnKSk7XHJcblx0XHRcdH0pO1xyXG5cclxuXHRcdFx0JCgnLmV4cG9ydF9fc2xpZGVyLS1jbGllbnQnKS5lYWNoKGZ1bmN0aW9uICgpIHtcclxuXHRcdFx0XHRsZXQgbmFtZSA9ICQodGhpcykuZmluZCgnaW5wdXQnKS5hdHRyKCdpZCcpLFxyXG5cdFx0XHRcdFx0dmFsdWUgPSAkKHRoaXMpLmZpbmQoJ2lucHV0JykudmFsKCk7XHJcblxyXG5cdFx0XHRcdHJhbmdlLnB1c2goW25hbWUsIHZhbHVlXSlcclxuXHRcdFx0fSlcclxuXHJcblx0XHRcdCQuYWpheCh7XHJcblx0XHRcdFx0dXJsOiB1cmwsXHJcblx0XHRcdFx0dHlwZTogJ3Bvc3QnLFxyXG5cdFx0XHRcdGRhdGE6IHtcclxuXHRcdFx0XHRcdGNhdGVnb3J5X2lkOiBpZCxcclxuXHRcdFx0XHRcdHNvcnQ6IHNvcnQsXHJcblx0XHRcdFx0XHRhdmFpbGFiaWxpdHk6IGF2YWlsYWJpbGl0eSxcclxuXHRcdFx0XHRcdGFtb3VudDogYW1vdW50LFxyXG5cdFx0XHRcdFx0c2VsZWN0OiBzZWxlY3QsXHJcblx0XHRcdFx0XHRjaGVja2VyZWRQcm9kdWN0czogY2hlY2tlcmVkUHJvZHVjdHMsXHJcblx0XHRcdFx0XHRyYW5nZTogcmFuZ2VcclxuXHRcdFx0XHR9LFxyXG5cdFx0XHRcdGRhdGFUeXBlOiAnanNvbicsXHJcblx0XHRcdFx0c3VjY2VzczogZnVuY3Rpb24gKHJlc3BvbnNlKSB7XHJcblx0XHRcdFx0XHRpZiAocmVzcG9uc2Uuc3VjY2Vzcykge1xyXG5cdFx0XHRcdFx0XHRpZiAoJHRoaXMuY2xvc2VzdCgnbGknKS5maW5kKCcuZXhwb3J0X19jaGVja2VycycpLmxlbmd0aCkge1xyXG5cdFx0XHRcdFx0XHRcdCR0aGlzLmNsb3Nlc3QoJ2xpJykuZmluZCgnLmV4cG9ydF9fY2hlY2tlcnMnKS5yZXBsYWNlV2l0aChyZXNwb25zZS5odG1sKVxyXG5cdFx0XHRcdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdFx0XHRcdCR0aGlzLmNsb3Nlc3QoJ2xpJykuYXBwZW5kKHJlc3BvbnNlLmh0bWwpXHJcblx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHR9XHJcblx0XHRcdH0pO1xyXG5cdFx0fVxyXG5cdH0pXHJcbn1cclxuXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuLy8gRXhwb3J0c1xyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG5leHBvcnQgZGVmYXVsdCBwcm9kdWN0c0Rvd25sb2FkO1xyXG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9fSFRNTC9zcmMvanMvX21vZHVsZXMvZXhwb3J0LW1vZHVsZS9wcm9kdWN0cy1kb3dubG9hZC5qcyIsIid1c2Ugc3RyaWN0JztcclxuXHJcbi8qKlxyXG4gKlxyXG4gKiBAbW9kdWxlXHJcbiAqL1xyXG5cclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4vLyBJbXBvcnRzXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuLy8gUHJpdmF0ZVxyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG4vKipcclxuICogQHBhcmFtIHtKUXVlcnl9ICRlbGVtZW50XHJcbiAqIEByZXR1cm4ge3N0cmluZ31cclxuICogQHByaXZhdGVcclxuICovXHJcblxyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbi8vIFB1YmxpY1xyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG4vKipcclxuICogQHBhcmFtIHtKUXVlcnl9ICRlbGVtZW50c1xyXG4gKi9cclxuZnVuY3Rpb24gcHJpY2VGcm9tVG8gKCkge1xyXG5cdGlmICgkKCcuZXhwb3J0X19zbGlkZXInKS5sZW5ndGgpIHtcclxuXHRcdGxldCBhbW91bnRTbGlkZXIgPSAkKCcjYW1vdW50JyksXHJcblx0XHRcdHNsaWRlclJhbmdlID0gJChcIiNzbGlkZXItcmFuZ2VcIiksXHJcblx0XHRcdG1pbiA9IGFtb3VudFNsaWRlci5kYXRhKCdtaW4nKSxcclxuXHRcdFx0bWF4ID0gYW1vdW50U2xpZGVyLmRhdGEoJ21heCcpLFxyXG5cdFx0XHRjdXJyZW5jeSA9ICQoJ2lucHV0W2RhdGEtY3VycmVuY3ldJykuZGF0YSgnY3VycmVuY3knKTtcclxuXHJcblx0XHRzbGlkZXJSYW5nZS5zbGlkZXIoe1xyXG5cdFx0XHRyYW5nZTogdHJ1ZSxcclxuXHRcdFx0bWluOiBtaW4sXHJcblx0XHRcdG1heDogbWF4LFxyXG5cdFx0XHR2YWx1ZXM6IFttaW4sIG1heF0sXHJcblx0XHRcdHNsaWRlOiBmdW5jdGlvbiAoZXZlbnQsIHVpKSB7XHJcblx0XHRcdFx0JChcIiNhbW91bnRcIikudmFsKHVpLnZhbHVlc1swXSArICcgJyArIGN1cnJlbmN5ICsgXCIgLSBcIiArIHVpLnZhbHVlc1sxXSArICcgJyArIGN1cnJlbmN5KTtcclxuXHRcdFx0fVxyXG5cdFx0fSk7XHJcblxyXG5cdFx0YW1vdW50U2xpZGVyLnZhbChzbGlkZXJSYW5nZS5zbGlkZXIoXCJ2YWx1ZXNcIiwgMCkgKyAnICcgKyBjdXJyZW5jeSArXHJcblx0XHRcdFwiIC0gXCIgKyBzbGlkZXJSYW5nZS5zbGlkZXIoXCJ2YWx1ZXNcIiwgMSkgKyAnICcgK1xyXG5cdFx0XHRjdXJyZW5jeSk7XHJcblx0fVxyXG59XHJcblxyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbi8vIEV4cG9ydHNcclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgcHJpY2VGcm9tVG87XHJcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL19IVE1ML3NyYy9qcy9fbW9kdWxlcy9leHBvcnQtbW9kdWxlL3ByaWNlLWZyb20tdG8uanMiLCIndXNlIHN0cmljdCc7XHJcblxyXG4vKipcclxuICpcclxuICogQG1vZHVsZVxyXG4gKi9cclxuXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuLy8gSW1wb3J0c1xyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbi8vIFByaXZhdGVcclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuLyoqXHJcbiAqIEBwYXJhbSB7SlF1ZXJ5fSAkZWxlbWVudFxyXG4gKiBAcmV0dXJuIHtzdHJpbmd9XHJcbiAqIEBwcml2YXRlXHJcbiAqL1xyXG5cclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4vLyBQdWJsaWNcclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuLyoqXHJcbiAqIEBwYXJhbSB7SlF1ZXJ5fSAkZWxlbWVudHNcclxuICovXHJcbmZ1bmN0aW9uIHByaWNlRnJvbUNsaWVudCAoKSB7XHJcblx0aWYgKCQoJy5leHBvcnRfX3NsaWRlci0tY2xpZW50JykubGVuZ3RoKSB7XHJcblx0XHQkKCcuZXhwb3J0X19zbGlkZXItLWNsaWVudCcpLmVhY2goZnVuY3Rpb24gKCkge1xyXG5cdFx0XHRsZXQgJHRoaXMgPSAkKHRoaXMpLFxyXG5cdFx0XHRcdG5hbWUgPSAkdGhpcy5maW5kKCdpbnB1dCcpLmF0dHIoJ2lkJyksXHJcblx0XHRcdFx0bWluID0gJCgnIycgKyBuYW1lKS5kYXRhKCdtaW4nKSxcclxuXHRcdFx0XHRtYXggPSAkKCcjJyArIG5hbWUpLmRhdGEoJ21heCcpLFxyXG5cdFx0XHRcdHBlcmNlbnQgPSAkdGhpcy5kYXRhKCdwZXJjZW50JykgfHwgMDtcclxuXHJcblx0XHRcdCQoXCIjc2xpZGVyLXJhbmdlXCIgKyBuYW1lKS5zbGlkZXIoe1xyXG5cdFx0XHRcdG1pbjogbWluLFxyXG5cdFx0XHRcdG1heDogbWF4LFxyXG5cdFx0XHRcdHZhbHVlczogW3BlcmNlbnRdLFxyXG5cdFx0XHRcdHNsaWRlOiBmdW5jdGlvbiAoZXZlbnQsIHVpKSB7XHJcblx0XHRcdFx0XHQkKFwiI1wiICsgbmFtZSkudmFsKHVpLnZhbHVlc1swXSArIFwiJVwiKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH0pO1xyXG5cclxuXHRcdFx0JChcIiNcIiArIG5hbWUpLnZhbChwZXJjZW50ICsgJyUnKVxyXG5cdFx0fSlcclxuXHR9XHJcbn1cclxuXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuLy8gRXhwb3J0c1xyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG5leHBvcnQgZGVmYXVsdCBwcmljZUZyb21DbGllbnQ7XHJcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL19IVE1ML3NyYy9qcy9fbW9kdWxlcy9leHBvcnQtbW9kdWxlL3ByaWNlLWZyb20tY2xpZW50LmpzIiwiJ3VzZSBzdHJpY3QnO1xyXG5cclxuLyoqXHJcbiAqXHJcbiAqIEBtb2R1bGVcclxuICovXHJcblxyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbi8vIEltcG9ydHNcclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4vLyBQcml2YXRlXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbi8qKlxyXG4gKiBAcGFyYW0ge0pRdWVyeX0gJGVsZW1lbnRcclxuICogQHJldHVybiB7c3RyaW5nfVxyXG4gKiBAcHJpdmF0ZVxyXG4gKi9cclxuXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuLy8gUHVibGljXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbi8qKlxyXG4gKiBAcGFyYW0ge0pRdWVyeX0gJGVsZW1lbnRzXHJcbiAqL1xyXG5mdW5jdGlvbiBpbmZvQmxvY2sgKCkge1xyXG5cdCQoJy5leHBvcnRfX2luZm8taWNvbicpLm9uKCdjbGljaycsIGZ1bmN0aW9uIChlKSB7XHJcblx0XHRsZXQgJHRoaXMgPSAkKHRoaXMpO1xyXG5cclxuXHRcdCQoJy5leHBvcnRfX2luZm8taWNvbicpLm5leHQoKS5oaWRlKCk7XHJcblx0XHQkdGhpcy5uZXh0KCkudG9nZ2xlKCk7XHJcblx0fSlcclxuXHJcblx0JCgnYm9keScpLm9uKCdjbGljaycsIGZ1bmN0aW9uIChlKSB7XHJcblx0XHRsZXQgZGl2ID0gJCgnLmV4cG9ydF9faW5mby1pY29uJylcclxuXHJcblx0XHRpZiAoIWRpdi5pcyhlLnRhcmdldCkgJiYgZGl2LmhhcyhlLnRhcmdldCkubGVuZ3RoID09PSAwKSB7XHJcblx0XHRcdGRpdi5uZXh0KCkuaGlkZSgpO1xyXG5cdFx0fVxyXG5cdH0pXHJcbn1cclxuXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuLy8gRXhwb3J0c1xyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG5leHBvcnQgZGVmYXVsdCBpbmZvQmxvY2s7XHJcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL19IVE1ML3NyYy9qcy9fbW9kdWxlcy9leHBvcnQtbW9kdWxlL2luZm8tYmxvY2suanMiLCIndXNlIHN0cmljdCc7XHJcblxyXG4vKipcclxuICpcclxuICogQG1vZHVsZVxyXG4gKi9cclxuXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuLy8gSW1wb3J0c1xyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbi8vIFByaXZhdGVcclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuLyoqXHJcbiAqIEBwYXJhbSB7SlF1ZXJ5fSAkZWxlbWVudFxyXG4gKiBAcmV0dXJuIHtzdHJpbmd9XHJcbiAqIEBwcml2YXRlXHJcbiAqL1xyXG5cclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4vLyBQdWJsaWNcclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuLyoqXHJcbiAqIEBwYXJhbSB7SlF1ZXJ5fSAkZWxlbWVudHNcclxuICovXHJcbmZ1bmN0aW9uIGFqYXhGaWx0ZXIgKCkge1xyXG5cdCQoJy5qcy1zZXQtZmlsdGVyJykub24oJ2NsaWNrJywgZnVuY3Rpb24gKCkge1xyXG5cdFx0bGV0IHVybCA9ICQodGhpcykuZGF0YSgndXJsJyksXHJcblx0XHRcdHNvcnQgPSAkKCcjc29ydCcpLnZhbCgpLFxyXG5cdFx0XHRhbW91bnQgPSAkKCcjYW1vdW50JykudmFsKCksXHJcblx0XHRcdGF2YWlsYWJpbGl0eSA9ICQoJy5leHBvcnRfX2F2YWlsYWJpbGl0eSBpbnB1dCcpLnByb3AoJ2NoZWNrZWQnKSxcclxuXHRcdFx0b3BlbmVkQ2F0ZWdvcmllcyA9IFtdLFxyXG5cdFx0XHRyYW5nZSA9IFtdLFxyXG5cdFx0XHRjaGVja2VyZWRQcm9kdWN0cyA9IFtdO1xyXG5cclxuXHRcdCQoJy5qcy1wcm9kdWN0LXNob3cub3BlbmVkJykucGFyZW50KCkuZmluZCgnLmV4cG9ydF9fcHJvZHVjdC1uYW1lIGlucHV0OmNoZWNrZWQnKS5lYWNoKGZ1bmN0aW9uICgpIHtcclxuXHRcdFx0Y2hlY2tlcmVkUHJvZHVjdHMucHVzaCgkKHRoaXMpLmRhdGEoJ2lkJykpO1xyXG5cdFx0fSk7XHJcblxyXG5cdFx0JCgnLmpzLXByb2R1Y3Qtc2hvdy5vcGVuZWQnKS5lYWNoKGZ1bmN0aW9uICgpIHtcclxuXHRcdFx0b3BlbmVkQ2F0ZWdvcmllcy5wdXNoKCQodGhpcykubmV4dCgpLmZpbmQoJ2lucHV0JykuZGF0YSgnaWQnKSk7XHJcblx0XHR9KTtcclxuXHJcblx0XHQkKCcuZXhwb3J0X19zbGlkZXItLWNsaWVudCcpLmVhY2goZnVuY3Rpb24gKCkge1xyXG5cdFx0XHRsZXQgbmFtZSA9ICQodGhpcykuZmluZCgnaW5wdXQnKS5hdHRyKCdpZCcpLFxyXG5cdFx0XHRcdHZhbHVlID0gJCh0aGlzKS5maW5kKCdpbnB1dCcpLnZhbCgpO1xyXG5cclxuXHRcdFx0cmFuZ2UucHVzaChbbmFtZSwgdmFsdWVdKVxyXG5cdFx0fSlcclxuXHJcblx0XHQkLmFqYXgoe1xyXG5cdFx0XHR1cmw6IHVybCxcclxuXHRcdFx0dHlwZTogJ3Bvc3QnLFxyXG5cdFx0XHRkYXRhOiB7XHJcblx0XHRcdFx0c29ydDogc29ydCxcclxuXHRcdFx0XHRhdmFpbGFiaWxpdHk6IGF2YWlsYWJpbGl0eSxcclxuXHRcdFx0XHRhbW91bnQ6IGFtb3VudCxcclxuXHRcdFx0XHRjaGVja2VyZWRQcm9kdWN0czogY2hlY2tlcmVkUHJvZHVjdHMsXHJcblx0XHRcdFx0b3BlbmVkQ2F0ZWdvcmllczogb3BlbmVkQ2F0ZWdvcmllcyxcclxuXHRcdFx0XHRyYW5nZTogcmFuZ2VcclxuXHRcdFx0fSxcclxuXHRcdFx0ZGF0YVR5cGU6ICdqc29uJyxcclxuXHJcblx0XHRcdHN1Y2Nlc3M6IGZ1bmN0aW9uIChyZXNwb25zZSkge1xyXG5cdFx0XHRcdGlmIChyZXNwb25zZS5zdWNjZXNzKSB7XHJcblx0XHRcdFx0XHQkLmVhY2gocmVzcG9uc2UucHJvZHVjdHMsIGZ1bmN0aW9uIChpbmRleCwgdmFsdWUpIHtcclxuXHRcdFx0XHRcdFx0bGV0IGJsb2NrID0gJCgnLmpzLXByb2R1Y3Qtc2hvd1tkYXRhLWlkPVwiJyArIGluZGV4ICsgJ1wiXScpLnBhcmVudCgpO1xyXG5cdFx0XHRcdFx0XHRibG9jay5maW5kKCcuZXhwb3J0X19jaGVja2VycycpLnJlbW92ZSgpO1xyXG5cdFx0XHRcdFx0XHRibG9jay5hcHBlbmQodmFsdWUpXHJcblx0XHRcdFx0XHR9KVxyXG5cclxuXHRcdFx0XHRcdCQoJy5leHBvcnRfX2ZpbHRlci1ub3R5Jykuc2hvdyhcInNsb3dcIilcclxuXHRcdFx0XHRcdHNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xyXG5cdFx0XHRcdFx0XHQkKCcuZXhwb3J0X19maWx0ZXItbm90eScpLmhpZGUoXCJzbG93XCIpXHJcblx0XHRcdFx0XHR9LCAzMDAwKVxyXG5cclxuXHRcdFx0XHRcdCQoXCIuZXhwb3J0X19jaGVja2VycyBpbnB1dFt0eXBlPSdjaGVja2JveCddXCIpLm5vdChcIi5uZXN0aW5nLWxldmVsLS1wcm9kdWN0cyBpbnB1dFt0eXBlPSdjaGVja2JveCddXCIpLmVhY2goZnVuY3Rpb24gKCkge1xyXG5cdFx0XHRcdFx0XHQkKHRoaXMpLnByb3AoJ2NoZWNrZWQnLCBmYWxzZSlcclxuXHRcdFx0XHRcdH0pXHJcblxyXG5cdFx0XHRcdFx0JC5lYWNoKHJlc3BvbnNlLmNoZWNrZWRDYXRlZ29yaWVzLCBmdW5jdGlvbiAoaWQsIG5vdEFsbCkge1xyXG5cdFx0XHRcdFx0XHQkKCdpbnB1dFtkYXRhLWlkPVwiJyArIGlkICsgJ1wiXScpLnByb3AoJ2NoZWNrZWQnLCAnY2hlY2tlZCcpLmF0dHIoJ2RhdGEtbm90QWxsJywgIW5vdEFsbClcclxuXHRcdFx0XHRcdH0pXHJcblxyXG5cdFx0XHRcdFx0JCgnW2RhdGEtbGV2ZWw9XCIxXCJdJykuZWFjaChmdW5jdGlvbiAoKXtcclxuXHRcdFx0XHRcdFx0bGV0ICR0aGlzID0gJCh0aGlzKSxcclxuXHRcdFx0XHRcdFx0XHRhbGxJbnB1dHMgPSAkdGhpcy5maW5kKCdbZGF0YS1sZXZlbD1cIjJcIl0gbGkgYSBpbnB1dCcpLmxlbmd0aCxcclxuXHRcdFx0XHRcdFx0XHRjaGVja2VkSW5wdXRzID0gJHRoaXMuZmluZCgnW2RhdGEtbGV2ZWw9XCIyXCJdIGxpIGEgaW5wdXQ6Y2hlY2tlZCcpLmxlbmd0aDtcclxuXHJcblx0XHRcdFx0XHRcdGlmIChhbGxJbnB1dHMgPT09IGNoZWNrZWRJbnB1dHMpIHtcclxuXHRcdFx0XHRcdFx0XHQkdGhpcy5maW5kKCcuY2hlY2tib3hfX2ljb24tLW1haW4tY2F0ZWdvcnknKS5uZXh0KCkuZmluZCgnaW5wdXQnKS5wcm9wKCdjaGVja2VkJywgJ2NoZWNrZWQnKS5hdHRyKCdkYXRhLW5vdEFsbCcsIHRydWUpXHJcblx0XHRcdFx0XHRcdH0gZWxzZSBpZiAoY2hlY2tlZElucHV0cyA+IDApe1xyXG5cdFx0XHRcdFx0XHRcdCR0aGlzLmZpbmQoJy5jaGVja2JveF9faWNvbi0tbWFpbi1jYXRlZ29yeScpLm5leHQoKS5maW5kKCdpbnB1dCcpLnByb3AoJ2NoZWNrZWQnLCB0cnVlKS5hdHRyKCdkYXRhLW5vdEFsbCcsIGZhbHNlKVxyXG5cdFx0XHRcdFx0XHR9IGVsc2UgaWYgKGNoZWNrZWRJbnB1dHMgPT09IDApIHtcclxuXHRcdFx0XHRcdFx0XHQkdGhpcy5maW5kKCcuY2hlY2tib3hfX2ljb24tLW1haW4tY2F0ZWdvcnknKS5uZXh0KCkuZmluZCgnaW5wdXQnKS5wcm9wKCdjaGVja2VkJywgZmFsc2UpLmF0dHIoJ2RhdGEtbm90QWxsJywgdHJ1ZSlcclxuXHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0fSlcclxuXHJcblx0XHRcdFx0XHQkKCdbZGF0YS1sZXZlbD1cIjNcIl0nKS5lYWNoKGZ1bmN0aW9uICgpe1xyXG5cdFx0XHRcdFx0XHRsZXQgJHRoaXMgPSAkKHRoaXMpLFxyXG5cdFx0XHRcdFx0XHRcdGFsbElucHV0cyA9ICR0aGlzLmZpbmQoJ2xpIGEgaW5wdXQnKS5sZW5ndGgsXHJcblx0XHRcdFx0XHRcdFx0Y2hlY2tlZElucHV0cyA9ICR0aGlzLmZpbmQoJ2xpIGEgaW5wdXQ6Y2hlY2tlZCcpLmxlbmd0aDtcclxuXHJcblx0XHRcdFx0XHRcdGlmIChhbGxJbnB1dHMgPT09IGNoZWNrZWRJbnB1dHMpIHtcclxuXHRcdFx0XHRcdFx0XHQkdGhpcy5wcmV2KCkuZmluZCgnaW5wdXQnKS5wcm9wKCdjaGVja2VkJywgJ2NoZWNrZWQnKS5hdHRyKCdkYXRhLW5vdEFsbCcsIHRydWUpXHJcblx0XHRcdFx0XHRcdH0gZWxzZSBpZiAoY2hlY2tlZElucHV0cyA+IDApe1xyXG5cdFx0XHRcdFx0XHRcdCR0aGlzLnByZXYoKS5maW5kKCdpbnB1dCcpLnByb3AoJ2NoZWNrZWQnLCB0cnVlKS5hdHRyKCdkYXRhLW5vdEFsbCcsIGZhbHNlKVxyXG5cdFx0XHRcdFx0XHR9IGVsc2UgaWYgKGNoZWNrZWRJbnB1dHMgPT09IDApIHtcclxuXHRcdFx0XHRcdFx0XHQkdGhpcy5wcmV2KCkuZmluZCgnaW5wdXQnKS5wcm9wKCdjaGVja2VkJywgZmFsc2UpLmF0dHIoJ2RhdGEtbm90QWxsJywgdHJ1ZSlcclxuXHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0fSlcclxuXHRcdFx0XHR9XHJcblx0XHRcdH1cclxuXHRcdH0pO1xyXG5cdH0pO1xyXG59XHJcblxyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbi8vIEV4cG9ydHNcclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgYWpheEZpbHRlcjtcclxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vX0hUTUwvc3JjL2pzL19tb2R1bGVzL2V4cG9ydC1tb2R1bGUvYWpheC1maWx0ZXIuanMiLCIndXNlIHN0cmljdCc7XHJcblxyXG4vKipcclxuICpcclxuICogQG1vZHVsZVxyXG4gKi9cclxuXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuLy8gSW1wb3J0c1xyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbi8vIFByaXZhdGVcclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuLyoqXHJcbiAqIEBwYXJhbSB7SlF1ZXJ5fSAkZWxlbWVudFxyXG4gKiBAcmV0dXJuIHtzdHJpbmd9XHJcbiAqIEBwcml2YXRlXHJcbiAqL1xyXG5cclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4vLyBQdWJsaWNcclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuLyoqXHJcbiAqIEBwYXJhbSB7SlF1ZXJ5fSAkZWxlbWVudHNcclxuICovXHJcbmZ1bmN0aW9uIHNhdmUgKCkge1xyXG5cdCQoJy5qcy1zYXZlWE1MJykub24oJ2NsaWNrJywgZnVuY3Rpb24gKGUpIHtcclxuXHRcdGxldCB1cmwgPSAkKHRoaXMpLmRhdGEoJ3VybCcpLFxyXG5cdFx0XHRwcm9tSWQgPSAkKHRoaXMpLmF0dHIoJ2RhdGEtcHJvbWlkJyksXHJcblx0XHRcdHNob3BOYW1lID0gJCgnaW5wdXRbbmFtZT1cInNob3BOYW1lXCJdJykudmFsKCksXHJcblx0XHRcdGNvbXBhbnlOYW1lID0gJCgnaW5wdXRbbmFtZT1cImNvbXBhbnlOYW1lXCJdJykudmFsKCksXHJcblx0XHRcdGF2YWlsYWJpbGl0eSA9ICQoJy5leHBvcnRfX2F2YWlsYWJpbGl0eSBpbnB1dCcpLnByb3AoJ2NoZWNrZWQnKSxcclxuXHRcdFx0Y2hlY2tlcmVkRW1wdHlDYXRlZ29yaWVzID0gW10sXHJcblx0XHRcdHByb2R1Y3RzID0gW10sXHJcblx0XHRcdHJhbmdlID0gW107XHJcblxyXG5cdFx0aWYgKHNob3BOYW1lLmxlbmd0aCA+IDAgJiYgY29tcGFueU5hbWUubGVuZ3RoID4gMCkge1xyXG5cdFx0XHRlLnByZXZlbnREZWZhdWx0KCk7XHJcblxyXG5cdFx0XHQkKCcuanMtcHJvZHVjdC1zaG93Om5vdCgub3BlbmVkKScpLmVhY2goZnVuY3Rpb24gKCkge1xyXG5cdFx0XHRcdGxldCAkdGhpcyA9ICQodGhpcyk7XHJcblxyXG5cdFx0XHRcdGlmICgkdGhpcy5wYXJlbnQoKS5jaGlsZHJlbignLmV4cG9ydF9fY2hlY2tlcnMnKS5sZW5ndGggJiYgJHRoaXMubmV4dCgpLmZpbmQoJ2lucHV0JykuaXMoXCI6Y2hlY2tlZFwiKSkge1xyXG5cdFx0XHRcdFx0Y2hlY2tlcmVkRW1wdHlDYXRlZ29yaWVzLnB1c2goJHRoaXMuZGF0YSgnaWQnKSk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9KVxyXG5cclxuXHRcdFx0JCgnLmV4cG9ydF9fdGFibGUtYm9keScpLmVhY2goZnVuY3Rpb24gKCkge1xyXG5cdFx0XHRcdCQodGhpcykuZmluZCgnaW5wdXQ6Y2hlY2tlZCcpLmVhY2goZnVuY3Rpb24gKCkge1xyXG5cdFx0XHRcdFx0bGV0IGlkID0gJCh0aGlzKS5kYXRhKCdpZCcpLFxyXG5cdFx0XHRcdFx0XHRuZXdQcmljZSA9ICQodGhpcykuY2xvc2VzdCgnLmV4cG9ydF9fdGFibGUtYm9keScpLmZpbmQoJy5nY2VsbC0tM1tkYXRhLW5ld3ByaWNlXScpLmF0dHIoJ2RhdGEtbmV3cHJpY2UnKTtcclxuXHJcblx0XHRcdFx0XHRwcm9kdWN0cy5wdXNoKFtpZCwgbmV3UHJpY2VdKTtcclxuXHRcdFx0XHR9KVxyXG5cdFx0XHR9KTtcclxuXHJcblx0XHRcdCQoJy5leHBvcnRfX3NsaWRlci0tY2xpZW50JykuZWFjaChmdW5jdGlvbiAoKSB7XHJcblx0XHRcdFx0bGV0IG5hbWUgPSAkKHRoaXMpLmZpbmQoJ2lucHV0JykuYXR0cignaWQnKSxcclxuXHRcdFx0XHRcdHZhbHVlID0gJCh0aGlzKS5maW5kKCdpbnB1dCcpLnZhbCgpO1xyXG5cclxuXHRcdFx0XHRyYW5nZS5wdXNoKFtuYW1lLCB2YWx1ZV0pXHJcblx0XHRcdH0pXHJcblxyXG5cclxuXHRcdFx0JC5hamF4KHtcclxuXHRcdFx0XHR1cmw6IHVybCxcclxuXHRcdFx0XHR0eXBlOiAncG9zdCcsXHJcblx0XHRcdFx0ZGF0YToge1xyXG5cdFx0XHRcdFx0c2hvcE5hbWU6IHNob3BOYW1lLFxyXG5cdFx0XHRcdFx0cHJvbUlkOiBwcm9tSWQsXHJcblx0XHRcdFx0XHRhdmFpbGFiaWxpdHk6IGF2YWlsYWJpbGl0eSxcclxuXHRcdFx0XHRcdHJhbmdlOiByYW5nZSxcclxuXHRcdFx0XHRcdGNvbXBhbnlOYW1lOiBjb21wYW55TmFtZSxcclxuXHRcdFx0XHRcdGNoZWNrZXJlZEVtcHR5Q2F0ZWdvcmllczogY2hlY2tlcmVkRW1wdHlDYXRlZ29yaWVzLFxyXG5cdFx0XHRcdFx0cHJvZHVjdHM6IHByb2R1Y3RzLFxyXG5cdFx0XHRcdH0sXHJcblx0XHRcdFx0ZGF0YVR5cGU6ICdqc29uJyxcclxuXHRcdFx0XHRiZWZvcmVTZW5kOiBmdW5jdGlvbiAoKSB7XHJcblx0XHRcdFx0XHQkKCcucHJlbG9hZGVyLmwtd3JhcHBlcicpLmNzcygnZGlzcGxheScsICdmbGV4Jyk7XHJcblx0XHRcdFx0fSxcclxuXHRcdFx0XHRzdWNjZXNzOiBmdW5jdGlvbiAocmVzcG9uc2UpIHtcclxuXHRcdFx0XHRcdGlmIChyZXNwb25zZS5zdWNjZXNzKSB7XHJcblx0XHRcdFx0XHRcdCQoJy5wcmVsb2FkZXIubC13cmFwcGVyJykuaGlkZSgpO1xyXG5cclxuXHRcdFx0XHRcdFx0JCgnLmV4cG9ydF9fc2F2ZWQtbm90eScpLnNob3coXCJzbG93XCIpXHJcblx0XHRcdFx0XHRcdHNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xyXG5cdFx0XHRcdFx0XHRcdCQoJy5leHBvcnRfX3NhdmVkLW5vdHknKS5oaWRlKFwic2xvd1wiKVxyXG5cdFx0XHRcdFx0XHR9LCAzMDAwKVxyXG5cclxuXHRcdFx0XHRcdFx0c2V0VGltZW91dChmdW5jdGlvbiAoKSB7XHJcblx0XHRcdFx0XHRcdFx0bG9jYXRpb24ucmVsb2FkKCk7XHJcblx0XHRcdFx0XHRcdH0sIDI1MDApXHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9KTtcclxuXHRcdH1cclxuXHR9KTtcclxufVxyXG5cclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4vLyBFeHBvcnRzXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IHNhdmU7XHJcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL19IVE1ML3NyYy9qcy9fbW9kdWxlcy9leHBvcnQtbW9kdWxlL3NhdmUuanMiLCIndXNlIHN0cmljdCc7XHJcblxyXG4vKipcclxuICpcclxuICogQG1vZHVsZVxyXG4gKi9cclxuXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuLy8gSW1wb3J0c1xyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbi8vIFByaXZhdGVcclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuLyoqXHJcbiAqIEBwYXJhbSB7SlF1ZXJ5fSAkZWxlbWVudFxyXG4gKiBAcmV0dXJuIHtzdHJpbmd9XHJcbiAqIEBwcml2YXRlXHJcbiAqL1xyXG5cclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4vLyBQdWJsaWNcclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuLyoqXHJcbiAqIEBwYXJhbSB7SlF1ZXJ5fSAkZWxlbWVudHNcclxuICovXHJcbmZ1bmN0aW9uIGNvcHlMaW5rICgpIHtcclxuXHQkKCcuanMtY29weS11cmwnKS5vbignY2xpY2snLCBmdW5jdGlvbiAoKSB7XHJcblx0XHR2YXIgJHRlbXAgPSAkKFwiPGlucHV0PlwiKTtcclxuXHRcdCQoXCJib2R5XCIpLmFwcGVuZCgkdGVtcCk7XHJcblx0XHQkdGVtcC52YWwoJCgnaW5wdXRbbmFtZT1cInVybC1mb3ItZmlsZVwiXScpLnZhbCgpKS5zZWxlY3QoKTtcclxuXHRcdGRvY3VtZW50LmV4ZWNDb21tYW5kKFwiY29weVwiKTtcclxuXHRcdCR0ZW1wLnJlbW92ZSgpO1xyXG5cdH0pXHJcbn1cclxuXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuLy8gRXhwb3J0c1xyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG5leHBvcnQgZGVmYXVsdCBjb3B5TGluaztcclxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vX0hUTUwvc3JjL2pzL19tb2R1bGVzL2V4cG9ydC1tb2R1bGUvY29weS1saW5rLmpzIl0sInNvdXJjZVJvb3QiOiIifQ==