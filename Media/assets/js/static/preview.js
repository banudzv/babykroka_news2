(function(window,$){if($===undefined){return false;}
	var $iPreviewer=$('#previewer'),$iPreviewerOverlay=$('#previewer__overlay'),$iPreviewerOpen=$('#previewer__open'),$iPreviewerPrev=$('#previewer__prev'),$iPreviewerNext=$('#previewer__next'),$iPreviewerNav=$('#previewer__nav'),$iPreviewerCloseMenu=$('#previewer__close-menu');function closeMenu(){$iPreviewerNav.css('display','none');}
	function close(){$iPreviewer.removeClass('is-active');}
	function open(){$iPreviewer.addClass('is-active');}
	function show(){$iPreviewer.addClass('is-builded');}
	function sibling(goTo){var $iPreviewerActivePage=$('.previewer__item.is-active');var $iPreviewerNewPage=$iPreviewerActivePage[goTo]();if(!$iPreviewerNewPage.length){var orderIndex='first';if(goTo==='prev'){orderIndex='last';}
		$iPreviewerNewPage=$('.previewer__item')[orderIndex]();}
		$iPreviewerNewPage[0].click();}
	$iPreviewer.on('click','.previewer__close',function(event){close();});$iPreviewerOverlay.on('click',function(event){close();});$iPreviewerCloseMenu.on('click',function(event){closeMenu();});$iPreviewerOpen.on('click',function(event){open();});setTimeout(function(){show();$iPreviewerPrev.on('click',function(event){sibling('prev');});$iPreviewerNext.on('click',function(event){sibling('next');});},500);})(window,window.jQuery);
