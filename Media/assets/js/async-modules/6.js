webpackJsonp([6],{

/***/ 23:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * Прослойка для загрузки модулей `magnific-popup`
 * @module
 */

// ----------------------------------------
// Imports
// ----------------------------------------

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.loaderInit = undefined;

var _mfpInit = __webpack_require__(11);

// ----------------------------------------
// Public
// ----------------------------------------

/**
 * @param {JQuery} $elements
 * @param {ModuleLoader} moduleLoader
 */
function loaderInit($elements, moduleLoader) {
	var $ajax = $elements.filter('[data-mfp="ajax"]');
	var $iframe = $elements.filter('[data-mfp="iframe"]');
	var $inline = $elements.filter('[data-mfp="inline"]');
	var $gallery = $elements.filter('[data-mfp="gallery"]');

	if ($ajax.length) {
		(0, _mfpInit.mfpAjax)($ajax);
	}

	if ($iframe.length) {
		(0, _mfpInit.mfpIframe)($iframe);
	}

	if ($inline.length) {
		(0, _mfpInit.mfpInline)($inline);
	}

	if ($gallery.length) {
		(0, _mfpInit.mfpGallery)($gallery);
	}
}

// ----------------------------------------
// Exports
// ----------------------------------------

exports.loaderInit = loaderInit;

/***/ })

});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9fSFRNTC9zcmMvanMvX21vZHVsZXMtbG9hZGVycy9tYWduaWZpYy1wb3B1cC0tbW9kdWxlLWxvYWRlci5qcyJdLCJuYW1lcyI6WyJsb2FkZXJJbml0IiwiJGVsZW1lbnRzIiwibW9kdWxlTG9hZGVyIiwiJGFqYXgiLCJmaWx0ZXIiLCIkaWZyYW1lIiwiJGlubGluZSIsIiRnYWxsZXJ5IiwibGVuZ3RoIl0sIm1hcHBpbmdzIjoiOzs7Ozs7QUFBQTs7QUFFQTs7Ozs7QUFLQTtBQUNBO0FBQ0E7Ozs7Ozs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7QUFJQSxTQUFTQSxVQUFULENBQXFCQyxTQUFyQixFQUFnQ0MsWUFBaEMsRUFBOEM7QUFDN0MsS0FBTUMsUUFBUUYsVUFBVUcsTUFBVixDQUFpQixtQkFBakIsQ0FBZDtBQUNBLEtBQU1DLFVBQVVKLFVBQVVHLE1BQVYsQ0FBaUIscUJBQWpCLENBQWhCO0FBQ0EsS0FBTUUsVUFBVUwsVUFBVUcsTUFBVixDQUFpQixxQkFBakIsQ0FBaEI7QUFDQSxLQUFNRyxXQUFXTixVQUFVRyxNQUFWLENBQWlCLHNCQUFqQixDQUFqQjs7QUFFQSxLQUFJRCxNQUFNSyxNQUFWLEVBQWtCO0FBQ2pCLHdCQUFRTCxLQUFSO0FBQ0E7O0FBRUQsS0FBSUUsUUFBUUcsTUFBWixFQUFvQjtBQUNuQiwwQkFBVUgsT0FBVjtBQUNBOztBQUVELEtBQUlDLFFBQVFFLE1BQVosRUFBb0I7QUFDbkIsMEJBQVVGLE9BQVY7QUFDQTs7QUFFRCxLQUFJQyxTQUFTQyxNQUFiLEVBQXFCO0FBQ3BCLDJCQUFXRCxRQUFYO0FBQ0E7QUFDRDs7QUFFRDtBQUNBO0FBQ0E7O1FBRVFQLFUsR0FBQUEsVSIsImZpbGUiOiJhc3luYy1tb2R1bGVzLzYuanM/dj0yNmUyYjJhZTZjYTliOGFiMzNkYSIsInNvdXJjZXNDb250ZW50IjpbIid1c2Ugc3RyaWN0JztcclxuXHJcbi8qKlxyXG4gKiDQn9GA0L7RgdC70L7QudC60LAg0LTQu9GPINC30LDQs9GA0YPQt9C60Lgg0LzQvtC00YPQu9C10LkgYG1hZ25pZmljLXBvcHVwYFxyXG4gKiBAbW9kdWxlXHJcbiAqL1xyXG5cclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4vLyBJbXBvcnRzXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbmltcG9ydCB7bWZwQWpheCwgbWZwSWZyYW1lLCBtZnBJbmxpbmUsIG1mcEdhbGxlcnl9IGZyb20gJyMvX21vZHVsZXMvbWFnbmlmaWMtcG9wdXAvbWZwLWluaXQnO1xyXG5cclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4vLyBQdWJsaWNcclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuLyoqXHJcbiAqIEBwYXJhbSB7SlF1ZXJ5fSAkZWxlbWVudHNcclxuICogQHBhcmFtIHtNb2R1bGVMb2FkZXJ9IG1vZHVsZUxvYWRlclxyXG4gKi9cclxuZnVuY3Rpb24gbG9hZGVySW5pdCAoJGVsZW1lbnRzLCBtb2R1bGVMb2FkZXIpIHtcclxuXHRjb25zdCAkYWpheCA9ICRlbGVtZW50cy5maWx0ZXIoJ1tkYXRhLW1mcD1cImFqYXhcIl0nKTtcclxuXHRjb25zdCAkaWZyYW1lID0gJGVsZW1lbnRzLmZpbHRlcignW2RhdGEtbWZwPVwiaWZyYW1lXCJdJyk7XHJcblx0Y29uc3QgJGlubGluZSA9ICRlbGVtZW50cy5maWx0ZXIoJ1tkYXRhLW1mcD1cImlubGluZVwiXScpO1xyXG5cdGNvbnN0ICRnYWxsZXJ5ID0gJGVsZW1lbnRzLmZpbHRlcignW2RhdGEtbWZwPVwiZ2FsbGVyeVwiXScpO1xyXG5cclxuXHRpZiAoJGFqYXgubGVuZ3RoKSB7XHJcblx0XHRtZnBBamF4KCRhamF4KTtcclxuXHR9XHJcblxyXG5cdGlmICgkaWZyYW1lLmxlbmd0aCkge1xyXG5cdFx0bWZwSWZyYW1lKCRpZnJhbWUpO1xyXG5cdH1cclxuXHJcblx0aWYgKCRpbmxpbmUubGVuZ3RoKSB7XHJcblx0XHRtZnBJbmxpbmUoJGlubGluZSk7XHJcblx0fVxyXG5cclxuXHRpZiAoJGdhbGxlcnkubGVuZ3RoKSB7XHJcblx0XHRtZnBHYWxsZXJ5KCRnYWxsZXJ5KTtcclxuXHR9XHJcbn1cclxuXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuLy8gRXhwb3J0c1xyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG5leHBvcnQge2xvYWRlckluaXR9O1xyXG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9fSFRNTC9zcmMvanMvX21vZHVsZXMtbG9hZGVycy9tYWduaWZpYy1wb3B1cC0tbW9kdWxlLWxvYWRlci5qcyJdLCJzb3VyY2VSb290IjoiIn0=