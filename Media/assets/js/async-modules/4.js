webpackJsonp([4],{

/***/ 27:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * Прослойка для загрузки модуля `wrap-media`
 * @module
 */

// ----------------------------------------
// Public
// ----------------------------------------

/**
 * @param {JQuery} $elements
 * @param {ModuleLoader} moduleLoader
 */

Object.defineProperty(exports, "__esModule", {
  value: true
});
function loaderInit($elements, moduleLoader) {
  var $media = $elements.find('iframe, video');
  if ($media.length) {
    __webpack_require__.e/* import() */(13).then(__webpack_require__.bind(null, 142)).then(function (module) {
      return module.default($media);
    });
  }
}

// ----------------------------------------
// Exports
// ----------------------------------------

exports.loaderInit = loaderInit;

/***/ })

});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9fSFRNTC9zcmMvanMvX21vZHVsZXMtbG9hZGVycy93cmFwLW1lZGlhLS1tb2R1bGUtbG9hZGVyLmpzIl0sIm5hbWVzIjpbImxvYWRlckluaXQiLCIkZWxlbWVudHMiLCJtb2R1bGVMb2FkZXIiLCIkbWVkaWEiLCJmaW5kIiwibGVuZ3RoIiwidGhlbiIsIm1vZHVsZSIsImRlZmF1bHQiXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBOztBQUVBOzs7OztBQUtBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7Ozs7QUFJQSxTQUFTQSxVQUFULENBQXFCQyxTQUFyQixFQUFnQ0MsWUFBaEMsRUFBOEM7QUFDN0MsTUFBTUMsU0FBU0YsVUFBVUcsSUFBVixDQUFlLGVBQWYsQ0FBZjtBQUNBLE1BQUlELE9BQU9FLE1BQVgsRUFBbUI7QUFDbEIsc0ZBQW9FQyxJQUFwRSxDQUF5RTtBQUFBLGFBQVVDLE9BQU9DLE9BQVAsQ0FBZUwsTUFBZixDQUFWO0FBQUEsS0FBekU7QUFDQTtBQUNEOztBQUVEO0FBQ0E7QUFDQTs7UUFFUUgsVSxHQUFBQSxVIiwiZmlsZSI6ImFzeW5jLW1vZHVsZXMvNC5qcz92PTNiNDY1NmUxYjNkMDg0MmMzZGI3Iiwic291cmNlc0NvbnRlbnQiOlsiJ3VzZSBzdHJpY3QnO1xyXG5cclxuLyoqXHJcbiAqINCf0YDQvtGB0LvQvtC50LrQsCDQtNC70Y8g0LfQsNCz0YDRg9C30LrQuCDQvNC+0LTRg9C70Y8gYHdyYXAtbWVkaWFgXHJcbiAqIEBtb2R1bGVcclxuICovXHJcblxyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbi8vIFB1YmxpY1xyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG4vKipcclxuICogQHBhcmFtIHtKUXVlcnl9ICRlbGVtZW50c1xyXG4gKiBAcGFyYW0ge01vZHVsZUxvYWRlcn0gbW9kdWxlTG9hZGVyXHJcbiAqL1xyXG5mdW5jdGlvbiBsb2FkZXJJbml0ICgkZWxlbWVudHMsIG1vZHVsZUxvYWRlcikge1xyXG5cdGNvbnN0ICRtZWRpYSA9ICRlbGVtZW50cy5maW5kKCdpZnJhbWUsIHZpZGVvJyk7XHJcblx0aWYgKCRtZWRpYS5sZW5ndGgpIHtcclxuXHRcdGltcG9ydCgvKiB3ZWJwYWNrQ2h1bmtOYW1lOiBcIndyYXAtbWVkaWFcIiAqLycjL19tb2R1bGVzL3dyYXAtbWVkaWEnKS50aGVuKG1vZHVsZSA9PiBtb2R1bGUuZGVmYXVsdCgkbWVkaWEpKTtcclxuXHR9XHJcbn1cclxuXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuLy8gRXhwb3J0c1xyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG5leHBvcnQge2xvYWRlckluaXR9O1xyXG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9fSFRNTC9zcmMvanMvX21vZHVsZXMtbG9hZGVycy93cmFwLW1lZGlhLS1tb2R1bGUtbG9hZGVyLmpzIl0sInNvdXJjZVJvb3QiOiIifQ==