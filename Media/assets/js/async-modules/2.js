webpackJsonp([2],{

/***/ 101:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * Lazy load изображений на оснве модуля `lozad`
 * @see {@link https://github.com/ApoorvSaxena/lozad.js}
 * @module
 */

// ----------------------------------------
// Imports
// ----------------------------------------

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _lozad = __webpack_require__(102);

var _lozad2 = _interopRequireDefault(_lozad);

__webpack_require__(21);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// ----------------------------------------
// Private
// ----------------------------------------

/**
 * Коллбек вставки источника
 * @param {JQuery} $el
 * @private
 * @sourceCode
 */
function onLoad($el) {
	var $parent = $el.parent('.lozad-parent');
	var delay = $el.data('lozad-delay') || 50;
	setTimeout(function () {
		if ($parent.length) {
			$parent.addClass('lozad-parent--is-ready');
		}
		$el.addClass('lozad--is-ready');
	}, delay);
}

/**
 * @param {Element} element
 * @private
 * @sourceCode
 */
function load(element) {
	var $element = $(element);
	var nodeName = $element.nodeName();
	var img = document.createElement('img');

	if (nodeName === 'picture') {
		var $img = $(img);
		$img.on('load', function () {
			return onLoad($element);
		});
		$element.append($img);
	} else {
		/** @type {string} */
		var src = $element.data('lozad');
		img.onload = function () {
			if ($element.nodeName() === 'img') {
				$element.attr('src', src);
			} else {
				$element.css('background-image', 'url(' + src + ')');
			}
			onLoad($element);
		};
		img.src = src;
	}
}

/**
 * @param {JQuery} $elements
 * @private
 * @sourceCode
 */
function init($elements) {
	$elements.each(function (i, el) {
		var $el = $(el);
		var lozadObserver = $el.data('lozadObserver') || (0, _lozad2.default)(el, {
			load: load,
			rootMargin: '0px',
			threshold: 0
		});

		$el.data('lozadObserver', lozadObserver);
		lozadObserver.observe();
	});
}

// ----------------------------------------
// Public
// ----------------------------------------

/**
 * @param {JQuery} $elements
 * @sourceCode
 */
function lozadLoad($elements) {
	if (window.Modernizr && window.Modernizr.intersectionobserver) {
		init($elements);
	} else {
		__webpack_require__.e/* import() */(12).then(__webpack_require__.bind(null, 103)).then(function () {
			init($elements);
		});
	}
}

// ----------------------------------------
// Exports
// ----------------------------------------

exports.default = lozadLoad;

/***/ }),

/***/ 102:
/***/ (function(module, exports, __webpack_require__) {

/*! lozad.js - v1.3.0 - 2018-02-16
* https://github.com/ApoorvSaxena/lozad.js
* Copyright (c) 2018 Apoorv Saxena; Licensed MIT */


(function (global, factory) {
	 true ? module.exports = factory() :
	typeof define === 'function' && define.amd ? define(factory) :
	(global.lozad = factory());
}(this, (function () { 'use strict';

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

/**
 * Detect IE browser
 * @const {boolean}
 * @private
 */
var isIE = document.documentMode;

var defaultConfig = {
  rootMargin: '0px',
  threshold: 0,
  load: function load(element) {
    if (element.nodeName.toLowerCase() === 'picture') {
      var img = document.createElement('img');
      if (isIE && element.getAttribute('data-iesrc')) {
        img.src = element.getAttribute('data-iesrc');
      }
      element.appendChild(img);
    }
    if (element.getAttribute('data-src')) {
      element.src = element.getAttribute('data-src');
    }
    if (element.getAttribute('data-srcset')) {
      element.srcset = element.getAttribute('data-srcset');
    }
    if (element.getAttribute('data-background-image')) {
      element.style.backgroundImage = 'url(' + element.getAttribute('data-background-image') + ')';
    }
  },
  loaded: function loaded() {}
};

function markAsLoaded(element) {
  element.setAttribute('data-loaded', true);
}

var isLoaded = function isLoaded(element) {
  return element.getAttribute('data-loaded') === 'true';
};

var onIntersection = function onIntersection(load, loaded) {
  return function (entries, observer) {
    entries.forEach(function (entry) {
      if (entry.intersectionRatio > 0) {
        observer.unobserve(entry.target);

        if (!isLoaded(entry.target)) {
          load(entry.target);
          markAsLoaded(entry.target);
          loaded(entry.target);
        }
      }
    });
  };
};

var getElements = function getElements(selector) {
  if (selector instanceof Element) {
    return [selector];
  }
  if (selector instanceof NodeList) {
    return selector;
  }
  return document.querySelectorAll(selector);
};

var lozad = function () {
  var selector = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '.lozad';
  var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

  var _defaultConfig$option = _extends({}, defaultConfig, options),
      rootMargin = _defaultConfig$option.rootMargin,
      threshold = _defaultConfig$option.threshold,
      load = _defaultConfig$option.load,
      loaded = _defaultConfig$option.loaded;

  var observer = void 0;

  if (window.IntersectionObserver) {
    observer = new IntersectionObserver(onIntersection(load, loaded), {
      rootMargin: rootMargin,
      threshold: threshold
    });
  }

  return {
    observe: function observe() {
      var elements = getElements(selector);

      for (var i = 0; i < elements.length; i++) {
        if (isLoaded(elements[i])) {
          continue;
        }
        if (observer) {
          observer.observe(elements[i]);
          continue;
        }
        load(elements[i]);
        markAsLoaded(elements[i]);
        loaded(elements[i]);
      }
    },
    triggerLoad: function triggerLoad(element) {
      if (isLoaded(element)) {
        return;
      }

      load(element);
      markAsLoaded(element);
      loaded(element);
    }
  };
};

return lozad;

})));


/***/ }),

/***/ 22:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * Прослойка для загрузки модуля `lozad`
 * @module
 */

// ----------------------------------------
// Imports
// ----------------------------------------

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.loaderInit = undefined;

var _lozadLoad = __webpack_require__(101);

var _lozadLoad2 = _interopRequireDefault(_lozadLoad);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// ----------------------------------------
// Public
// ----------------------------------------

/**
 * @param {JQuery} $elements
 * @param {ModuleLoader} moduleLoader
 */
function loaderInit($elements, moduleLoader) {
  (0, _lozadLoad2.default)($elements);
}

// ----------------------------------------
// Exports
// ----------------------------------------

exports.loaderInit = loaderInit;

/***/ })

});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9fSFRNTC9zcmMvanMvX21vZHVsZXMvbG96YWQtbG9hZC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbG96YWQvZGlzdC9sb3phZC5qcyIsIndlYnBhY2s6Ly8vLi9fSFRNTC9zcmMvanMvX21vZHVsZXMtbG9hZGVycy9sb3phZC0tbW9kdWxlLWxvYWRlci5qcyJdLCJuYW1lcyI6WyJvbkxvYWQiLCIkZWwiLCIkcGFyZW50IiwicGFyZW50IiwiZGVsYXkiLCJkYXRhIiwic2V0VGltZW91dCIsImxlbmd0aCIsImFkZENsYXNzIiwibG9hZCIsImVsZW1lbnQiLCIkZWxlbWVudCIsIiQiLCJub2RlTmFtZSIsImltZyIsImRvY3VtZW50IiwiY3JlYXRlRWxlbWVudCIsIiRpbWciLCJvbiIsImFwcGVuZCIsInNyYyIsIm9ubG9hZCIsImF0dHIiLCJjc3MiLCJpbml0IiwiJGVsZW1lbnRzIiwiZWFjaCIsImkiLCJlbCIsImxvemFkT2JzZXJ2ZXIiLCJyb290TWFyZ2luIiwidGhyZXNob2xkIiwib2JzZXJ2ZSIsImxvemFkTG9hZCIsIndpbmRvdyIsIk1vZGVybml6ciIsImludGVyc2VjdGlvbm9ic2VydmVyIiwidGhlbiIsImxvYWRlckluaXQiLCJtb2R1bGVMb2FkZXIiXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBOztBQUVBOzs7Ozs7QUFNQTtBQUNBO0FBQ0E7Ozs7OztBQUVBOzs7O0FBQ0E7Ozs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7OztBQU1BLFNBQVNBLE1BQVQsQ0FBaUJDLEdBQWpCLEVBQXNCO0FBQ3JCLEtBQUlDLFVBQVVELElBQUlFLE1BQUosQ0FBVyxlQUFYLENBQWQ7QUFDQSxLQUFJQyxRQUFRSCxJQUFJSSxJQUFKLENBQVMsYUFBVCxLQUEyQixFQUF2QztBQUNBQyxZQUFXLFlBQU07QUFDaEIsTUFBSUosUUFBUUssTUFBWixFQUFvQjtBQUNuQkwsV0FBUU0sUUFBUixDQUFpQix3QkFBakI7QUFDQTtBQUNEUCxNQUFJTyxRQUFKLENBQWEsaUJBQWI7QUFDQSxFQUxELEVBS0dKLEtBTEg7QUFNQTs7QUFFRDs7Ozs7QUFLQSxTQUFTSyxJQUFULENBQWVDLE9BQWYsRUFBd0I7QUFDdkIsS0FBTUMsV0FBV0MsRUFBRUYsT0FBRixDQUFqQjtBQUNBLEtBQU1HLFdBQVdGLFNBQVNFLFFBQVQsRUFBakI7QUFDQSxLQUFNQyxNQUFNQyxTQUFTQyxhQUFULENBQXVCLEtBQXZCLENBQVo7O0FBRUEsS0FBSUgsYUFBYSxTQUFqQixFQUE0QjtBQUMzQixNQUFJSSxPQUFPTCxFQUFFRSxHQUFGLENBQVg7QUFDQUcsT0FBS0MsRUFBTCxDQUFRLE1BQVIsRUFBZ0I7QUFBQSxVQUFNbEIsT0FBT1csUUFBUCxDQUFOO0FBQUEsR0FBaEI7QUFDQUEsV0FBU1EsTUFBVCxDQUFnQkYsSUFBaEI7QUFDQSxFQUpELE1BSU87QUFDTjtBQUNBLE1BQU1HLE1BQU1ULFNBQVNOLElBQVQsQ0FBYyxPQUFkLENBQVo7QUFDQVMsTUFBSU8sTUFBSixHQUFhLFlBQU07QUFDbEIsT0FBSVYsU0FBU0UsUUFBVCxPQUF3QixLQUE1QixFQUFtQztBQUNsQ0YsYUFBU1csSUFBVCxDQUFjLEtBQWQsRUFBcUJGLEdBQXJCO0FBQ0EsSUFGRCxNQUVPO0FBQ05ULGFBQVNZLEdBQVQsQ0FBYSxrQkFBYixXQUF3Q0gsR0FBeEM7QUFDQTtBQUNEcEIsVUFBT1csUUFBUDtBQUNBLEdBUEQ7QUFRQUcsTUFBSU0sR0FBSixHQUFVQSxHQUFWO0FBQ0E7QUFDRDs7QUFFRDs7Ozs7QUFLQSxTQUFTSSxJQUFULENBQWVDLFNBQWYsRUFBMEI7QUFDekJBLFdBQVVDLElBQVYsQ0FBZSxVQUFDQyxDQUFELEVBQUlDLEVBQUosRUFBVztBQUN6QixNQUFNM0IsTUFBTVcsRUFBRWdCLEVBQUYsQ0FBWjtBQUNBLE1BQU1DLGdCQUFnQjVCLElBQUlJLElBQUosQ0FBUyxlQUFULEtBQTZCLHFCQUFNdUIsRUFBTixFQUFVO0FBQzVEbkIsYUFENEQ7QUFFNURxQixlQUFZLEtBRmdEO0FBRzVEQyxjQUFXO0FBSGlELEdBQVYsQ0FBbkQ7O0FBTUE5QixNQUFJSSxJQUFKLENBQVMsZUFBVCxFQUEwQndCLGFBQTFCO0FBQ0FBLGdCQUFjRyxPQUFkO0FBQ0EsRUFWRDtBQVdBOztBQUVEO0FBQ0E7QUFDQTs7QUFFQTs7OztBQUlBLFNBQVNDLFNBQVQsQ0FBb0JSLFNBQXBCLEVBQStCO0FBQzlCLEtBQUlTLE9BQU9DLFNBQVAsSUFBb0JELE9BQU9DLFNBQVAsQ0FBaUJDLG9CQUF6QyxFQUErRDtBQUM5RFosT0FBS0MsU0FBTDtBQUNBLEVBRkQsTUFFTztBQUNOLG9GQUErRVksSUFBL0UsQ0FBb0YsWUFBTTtBQUN6RmIsUUFBS0MsU0FBTDtBQUNBLEdBRkQ7QUFHQTtBQUNEOztBQUVEO0FBQ0E7QUFDQTs7a0JBRWVRLFM7Ozs7Ozs7QUMxR2Y7QUFDQTtBQUNBLG1DQUFtQzs7O0FBR25DO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQyxxQkFBcUI7O0FBRXRCLG1EQUFtRCxnQkFBZ0Isc0JBQXNCLE9BQU8sMkJBQTJCLDBCQUEwQix5REFBeUQsMkJBQTJCLEVBQUUsRUFBRSxFQUFFLGVBQWU7O0FBRTlQO0FBQ0E7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQSx5Q0FBeUM7QUFDekM7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7O0FBRUE7QUFDQTtBQUNBOztBQUVBLHFCQUFxQixxQkFBcUI7QUFDMUM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUEsQ0FBQzs7Ozs7Ozs7O0FDaElEOztBQUVBOzs7OztBQUtBO0FBQ0E7QUFDQTs7Ozs7OztBQUVBOzs7Ozs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7QUFJQSxTQUFTSyxVQUFULENBQXFCYixTQUFyQixFQUFnQ2MsWUFBaEMsRUFBOEM7QUFDN0MsMkJBQVVkLFNBQVY7QUFDQTs7QUFFRDtBQUNBO0FBQ0E7O1FBRVFhLFUsR0FBQUEsVSIsImZpbGUiOiJhc3luYy1tb2R1bGVzLzIuanM/dj01MGZhOTE1OGZiZTU1NDM5YTQ5YSIsInNvdXJjZXNDb250ZW50IjpbIid1c2Ugc3RyaWN0JztcclxuXHJcbi8qKlxyXG4gKiBMYXp5IGxvYWQg0LjQt9C+0LHRgNCw0LbQtdC90LjQuSDQvdCwINC+0YHQvdCy0LUg0LzQvtC00YPQu9GPIGBsb3phZGBcclxuICogQHNlZSB7QGxpbmsgaHR0cHM6Ly9naXRodWIuY29tL0Fwb29ydlNheGVuYS9sb3phZC5qc31cclxuICogQG1vZHVsZVxyXG4gKi9cclxuXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuLy8gSW1wb3J0c1xyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG5pbXBvcnQgbG96YWQgZnJvbSAnbG96YWQnO1xyXG5pbXBvcnQgJ2N1c3RvbS1qcXVlcnktbWV0aG9kcy9mbi9ub2RlLW5hbWUnO1xyXG5cclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4vLyBQcml2YXRlXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbi8qKlxyXG4gKiDQmtC+0LvQu9Cx0LXQuiDQstGB0YLQsNCy0LrQuCDQuNGB0YLQvtGH0L3QuNC60LBcclxuICogQHBhcmFtIHtKUXVlcnl9ICRlbFxyXG4gKiBAcHJpdmF0ZVxyXG4gKiBAc291cmNlQ29kZVxyXG4gKi9cclxuZnVuY3Rpb24gb25Mb2FkICgkZWwpIHtcclxuXHRsZXQgJHBhcmVudCA9ICRlbC5wYXJlbnQoJy5sb3phZC1wYXJlbnQnKTtcclxuXHRsZXQgZGVsYXkgPSAkZWwuZGF0YSgnbG96YWQtZGVsYXknKSB8fCA1MDtcclxuXHRzZXRUaW1lb3V0KCgpID0+IHtcclxuXHRcdGlmICgkcGFyZW50Lmxlbmd0aCkge1xyXG5cdFx0XHQkcGFyZW50LmFkZENsYXNzKCdsb3phZC1wYXJlbnQtLWlzLXJlYWR5Jyk7XHJcblx0XHR9XHJcblx0XHQkZWwuYWRkQ2xhc3MoJ2xvemFkLS1pcy1yZWFkeScpO1xyXG5cdH0sIGRlbGF5KTtcclxufVxyXG5cclxuLyoqXHJcbiAqIEBwYXJhbSB7RWxlbWVudH0gZWxlbWVudFxyXG4gKiBAcHJpdmF0ZVxyXG4gKiBAc291cmNlQ29kZVxyXG4gKi9cclxuZnVuY3Rpb24gbG9hZCAoZWxlbWVudCkge1xyXG5cdGNvbnN0ICRlbGVtZW50ID0gJChlbGVtZW50KTtcclxuXHRjb25zdCBub2RlTmFtZSA9ICRlbGVtZW50Lm5vZGVOYW1lKCk7XHJcblx0Y29uc3QgaW1nID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnaW1nJyk7XHJcblxyXG5cdGlmIChub2RlTmFtZSA9PT0gJ3BpY3R1cmUnKSB7XHJcblx0XHRsZXQgJGltZyA9ICQoaW1nKTtcclxuXHRcdCRpbWcub24oJ2xvYWQnLCAoKSA9PiBvbkxvYWQoJGVsZW1lbnQpKTtcclxuXHRcdCRlbGVtZW50LmFwcGVuZCgkaW1nKTtcclxuXHR9IGVsc2Uge1xyXG5cdFx0LyoqIEB0eXBlIHtzdHJpbmd9ICovXHJcblx0XHRjb25zdCBzcmMgPSAkZWxlbWVudC5kYXRhKCdsb3phZCcpO1xyXG5cdFx0aW1nLm9ubG9hZCA9ICgpID0+IHtcclxuXHRcdFx0aWYgKCRlbGVtZW50Lm5vZGVOYW1lKCkgPT09ICdpbWcnKSB7XHJcblx0XHRcdFx0JGVsZW1lbnQuYXR0cignc3JjJywgc3JjKTtcclxuXHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHQkZWxlbWVudC5jc3MoJ2JhY2tncm91bmQtaW1hZ2UnLCBgdXJsKCR7c3JjfSlgKTtcclxuXHRcdFx0fVxyXG5cdFx0XHRvbkxvYWQoJGVsZW1lbnQpO1xyXG5cdFx0fTtcclxuXHRcdGltZy5zcmMgPSBzcmM7XHJcblx0fVxyXG59XHJcblxyXG4vKipcclxuICogQHBhcmFtIHtKUXVlcnl9ICRlbGVtZW50c1xyXG4gKiBAcHJpdmF0ZVxyXG4gKiBAc291cmNlQ29kZVxyXG4gKi9cclxuZnVuY3Rpb24gaW5pdCAoJGVsZW1lbnRzKSB7XHJcblx0JGVsZW1lbnRzLmVhY2goKGksIGVsKSA9PiB7XHJcblx0XHRjb25zdCAkZWwgPSAkKGVsKTtcclxuXHRcdGNvbnN0IGxvemFkT2JzZXJ2ZXIgPSAkZWwuZGF0YSgnbG96YWRPYnNlcnZlcicpIHx8IGxvemFkKGVsLCB7XHJcblx0XHRcdGxvYWQsXHJcblx0XHRcdHJvb3RNYXJnaW46ICcwcHgnLFxyXG5cdFx0XHR0aHJlc2hvbGQ6IDBcclxuXHRcdH0pO1xyXG5cclxuXHRcdCRlbC5kYXRhKCdsb3phZE9ic2VydmVyJywgbG96YWRPYnNlcnZlcik7XHJcblx0XHRsb3phZE9ic2VydmVyLm9ic2VydmUoKTtcclxuXHR9KTtcclxufVxyXG5cclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4vLyBQdWJsaWNcclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuLyoqXHJcbiAqIEBwYXJhbSB7SlF1ZXJ5fSAkZWxlbWVudHNcclxuICogQHNvdXJjZUNvZGVcclxuICovXHJcbmZ1bmN0aW9uIGxvemFkTG9hZCAoJGVsZW1lbnRzKSB7XHJcblx0aWYgKHdpbmRvdy5Nb2Rlcm5penIgJiYgd2luZG93Lk1vZGVybml6ci5pbnRlcnNlY3Rpb25vYnNlcnZlcikge1xyXG5cdFx0aW5pdCgkZWxlbWVudHMpO1xyXG5cdH0gZWxzZSB7XHJcblx0XHRpbXBvcnQoLyogd2VicGFja0NodW5rTmFtZTogXCJpbnRlcnNlY3Rpb24tb2JzZXJ2ZXJcIiAqLydpbnRlcnNlY3Rpb24tb2JzZXJ2ZXInKS50aGVuKCgpID0+IHtcclxuXHRcdFx0aW5pdCgkZWxlbWVudHMpO1xyXG5cdFx0fSk7XHJcblx0fVxyXG59XHJcblxyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbi8vIEV4cG9ydHNcclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgbG96YWRMb2FkO1xyXG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9fSFRNTC9zcmMvanMvX21vZHVsZXMvbG96YWQtbG9hZC5qcyIsIi8qISBsb3phZC5qcyAtIHYxLjMuMCAtIDIwMTgtMDItMTZcbiogaHR0cHM6Ly9naXRodWIuY29tL0Fwb29ydlNheGVuYS9sb3phZC5qc1xuKiBDb3B5cmlnaHQgKGMpIDIwMTggQXBvb3J2IFNheGVuYTsgTGljZW5zZWQgTUlUICovXG5cblxuKGZ1bmN0aW9uIChnbG9iYWwsIGZhY3RvcnkpIHtcblx0dHlwZW9mIGV4cG9ydHMgPT09ICdvYmplY3QnICYmIHR5cGVvZiBtb2R1bGUgIT09ICd1bmRlZmluZWQnID8gbW9kdWxlLmV4cG9ydHMgPSBmYWN0b3J5KCkgOlxuXHR0eXBlb2YgZGVmaW5lID09PSAnZnVuY3Rpb24nICYmIGRlZmluZS5hbWQgPyBkZWZpbmUoZmFjdG9yeSkgOlxuXHQoZ2xvYmFsLmxvemFkID0gZmFjdG9yeSgpKTtcbn0odGhpcywgKGZ1bmN0aW9uICgpIHsgJ3VzZSBzdHJpY3QnO1xuXG52YXIgX2V4dGVuZHMgPSBPYmplY3QuYXNzaWduIHx8IGZ1bmN0aW9uICh0YXJnZXQpIHsgZm9yICh2YXIgaSA9IDE7IGkgPCBhcmd1bWVudHMubGVuZ3RoOyBpKyspIHsgdmFyIHNvdXJjZSA9IGFyZ3VtZW50c1tpXTsgZm9yICh2YXIga2V5IGluIHNvdXJjZSkgeyBpZiAoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKHNvdXJjZSwga2V5KSkgeyB0YXJnZXRba2V5XSA9IHNvdXJjZVtrZXldOyB9IH0gfSByZXR1cm4gdGFyZ2V0OyB9O1xuXG4vKipcbiAqIERldGVjdCBJRSBicm93c2VyXG4gKiBAY29uc3Qge2Jvb2xlYW59XG4gKiBAcHJpdmF0ZVxuICovXG52YXIgaXNJRSA9IGRvY3VtZW50LmRvY3VtZW50TW9kZTtcblxudmFyIGRlZmF1bHRDb25maWcgPSB7XG4gIHJvb3RNYXJnaW46ICcwcHgnLFxuICB0aHJlc2hvbGQ6IDAsXG4gIGxvYWQ6IGZ1bmN0aW9uIGxvYWQoZWxlbWVudCkge1xuICAgIGlmIChlbGVtZW50Lm5vZGVOYW1lLnRvTG93ZXJDYXNlKCkgPT09ICdwaWN0dXJlJykge1xuICAgICAgdmFyIGltZyA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2ltZycpO1xuICAgICAgaWYgKGlzSUUgJiYgZWxlbWVudC5nZXRBdHRyaWJ1dGUoJ2RhdGEtaWVzcmMnKSkge1xuICAgICAgICBpbWcuc3JjID0gZWxlbWVudC5nZXRBdHRyaWJ1dGUoJ2RhdGEtaWVzcmMnKTtcbiAgICAgIH1cbiAgICAgIGVsZW1lbnQuYXBwZW5kQ2hpbGQoaW1nKTtcbiAgICB9XG4gICAgaWYgKGVsZW1lbnQuZ2V0QXR0cmlidXRlKCdkYXRhLXNyYycpKSB7XG4gICAgICBlbGVtZW50LnNyYyA9IGVsZW1lbnQuZ2V0QXR0cmlidXRlKCdkYXRhLXNyYycpO1xuICAgIH1cbiAgICBpZiAoZWxlbWVudC5nZXRBdHRyaWJ1dGUoJ2RhdGEtc3Jjc2V0JykpIHtcbiAgICAgIGVsZW1lbnQuc3Jjc2V0ID0gZWxlbWVudC5nZXRBdHRyaWJ1dGUoJ2RhdGEtc3Jjc2V0Jyk7XG4gICAgfVxuICAgIGlmIChlbGVtZW50LmdldEF0dHJpYnV0ZSgnZGF0YS1iYWNrZ3JvdW5kLWltYWdlJykpIHtcbiAgICAgIGVsZW1lbnQuc3R5bGUuYmFja2dyb3VuZEltYWdlID0gJ3VybCgnICsgZWxlbWVudC5nZXRBdHRyaWJ1dGUoJ2RhdGEtYmFja2dyb3VuZC1pbWFnZScpICsgJyknO1xuICAgIH1cbiAgfSxcbiAgbG9hZGVkOiBmdW5jdGlvbiBsb2FkZWQoKSB7fVxufTtcblxuZnVuY3Rpb24gbWFya0FzTG9hZGVkKGVsZW1lbnQpIHtcbiAgZWxlbWVudC5zZXRBdHRyaWJ1dGUoJ2RhdGEtbG9hZGVkJywgdHJ1ZSk7XG59XG5cbnZhciBpc0xvYWRlZCA9IGZ1bmN0aW9uIGlzTG9hZGVkKGVsZW1lbnQpIHtcbiAgcmV0dXJuIGVsZW1lbnQuZ2V0QXR0cmlidXRlKCdkYXRhLWxvYWRlZCcpID09PSAndHJ1ZSc7XG59O1xuXG52YXIgb25JbnRlcnNlY3Rpb24gPSBmdW5jdGlvbiBvbkludGVyc2VjdGlvbihsb2FkLCBsb2FkZWQpIHtcbiAgcmV0dXJuIGZ1bmN0aW9uIChlbnRyaWVzLCBvYnNlcnZlcikge1xuICAgIGVudHJpZXMuZm9yRWFjaChmdW5jdGlvbiAoZW50cnkpIHtcbiAgICAgIGlmIChlbnRyeS5pbnRlcnNlY3Rpb25SYXRpbyA+IDApIHtcbiAgICAgICAgb2JzZXJ2ZXIudW5vYnNlcnZlKGVudHJ5LnRhcmdldCk7XG5cbiAgICAgICAgaWYgKCFpc0xvYWRlZChlbnRyeS50YXJnZXQpKSB7XG4gICAgICAgICAgbG9hZChlbnRyeS50YXJnZXQpO1xuICAgICAgICAgIG1hcmtBc0xvYWRlZChlbnRyeS50YXJnZXQpO1xuICAgICAgICAgIGxvYWRlZChlbnRyeS50YXJnZXQpO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfSk7XG4gIH07XG59O1xuXG52YXIgZ2V0RWxlbWVudHMgPSBmdW5jdGlvbiBnZXRFbGVtZW50cyhzZWxlY3Rvcikge1xuICBpZiAoc2VsZWN0b3IgaW5zdGFuY2VvZiBFbGVtZW50KSB7XG4gICAgcmV0dXJuIFtzZWxlY3Rvcl07XG4gIH1cbiAgaWYgKHNlbGVjdG9yIGluc3RhbmNlb2YgTm9kZUxpc3QpIHtcbiAgICByZXR1cm4gc2VsZWN0b3I7XG4gIH1cbiAgcmV0dXJuIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoc2VsZWN0b3IpO1xufTtcblxudmFyIGxvemFkID0gZnVuY3Rpb24gKCkge1xuICB2YXIgc2VsZWN0b3IgPSBhcmd1bWVudHMubGVuZ3RoID4gMCAmJiBhcmd1bWVudHNbMF0gIT09IHVuZGVmaW5lZCA/IGFyZ3VtZW50c1swXSA6ICcubG96YWQnO1xuICB2YXIgb3B0aW9ucyA9IGFyZ3VtZW50cy5sZW5ndGggPiAxICYmIGFyZ3VtZW50c1sxXSAhPT0gdW5kZWZpbmVkID8gYXJndW1lbnRzWzFdIDoge307XG5cbiAgdmFyIF9kZWZhdWx0Q29uZmlnJG9wdGlvbiA9IF9leHRlbmRzKHt9LCBkZWZhdWx0Q29uZmlnLCBvcHRpb25zKSxcbiAgICAgIHJvb3RNYXJnaW4gPSBfZGVmYXVsdENvbmZpZyRvcHRpb24ucm9vdE1hcmdpbixcbiAgICAgIHRocmVzaG9sZCA9IF9kZWZhdWx0Q29uZmlnJG9wdGlvbi50aHJlc2hvbGQsXG4gICAgICBsb2FkID0gX2RlZmF1bHRDb25maWckb3B0aW9uLmxvYWQsXG4gICAgICBsb2FkZWQgPSBfZGVmYXVsdENvbmZpZyRvcHRpb24ubG9hZGVkO1xuXG4gIHZhciBvYnNlcnZlciA9IHZvaWQgMDtcblxuICBpZiAod2luZG93LkludGVyc2VjdGlvbk9ic2VydmVyKSB7XG4gICAgb2JzZXJ2ZXIgPSBuZXcgSW50ZXJzZWN0aW9uT2JzZXJ2ZXIob25JbnRlcnNlY3Rpb24obG9hZCwgbG9hZGVkKSwge1xuICAgICAgcm9vdE1hcmdpbjogcm9vdE1hcmdpbixcbiAgICAgIHRocmVzaG9sZDogdGhyZXNob2xkXG4gICAgfSk7XG4gIH1cblxuICByZXR1cm4ge1xuICAgIG9ic2VydmU6IGZ1bmN0aW9uIG9ic2VydmUoKSB7XG4gICAgICB2YXIgZWxlbWVudHMgPSBnZXRFbGVtZW50cyhzZWxlY3Rvcik7XG5cbiAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgZWxlbWVudHMubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgaWYgKGlzTG9hZGVkKGVsZW1lbnRzW2ldKSkge1xuICAgICAgICAgIGNvbnRpbnVlO1xuICAgICAgICB9XG4gICAgICAgIGlmIChvYnNlcnZlcikge1xuICAgICAgICAgIG9ic2VydmVyLm9ic2VydmUoZWxlbWVudHNbaV0pO1xuICAgICAgICAgIGNvbnRpbnVlO1xuICAgICAgICB9XG4gICAgICAgIGxvYWQoZWxlbWVudHNbaV0pO1xuICAgICAgICBtYXJrQXNMb2FkZWQoZWxlbWVudHNbaV0pO1xuICAgICAgICBsb2FkZWQoZWxlbWVudHNbaV0pO1xuICAgICAgfVxuICAgIH0sXG4gICAgdHJpZ2dlckxvYWQ6IGZ1bmN0aW9uIHRyaWdnZXJMb2FkKGVsZW1lbnQpIHtcbiAgICAgIGlmIChpc0xvYWRlZChlbGVtZW50KSkge1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG5cbiAgICAgIGxvYWQoZWxlbWVudCk7XG4gICAgICBtYXJrQXNMb2FkZWQoZWxlbWVudCk7XG4gICAgICBsb2FkZWQoZWxlbWVudCk7XG4gICAgfVxuICB9O1xufTtcblxucmV0dXJuIGxvemFkO1xuXG59KSkpO1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbG96YWQvZGlzdC9sb3phZC5qc1xuLy8gbW9kdWxlIGlkID0gMTAyXG4vLyBtb2R1bGUgY2h1bmtzID0gMiIsIid1c2Ugc3RyaWN0JztcclxuXHJcbi8qKlxyXG4gKiDQn9GA0L7RgdC70L7QudC60LAg0LTQu9GPINC30LDQs9GA0YPQt9C60Lgg0LzQvtC00YPQu9GPIGBsb3phZGBcclxuICogQG1vZHVsZVxyXG4gKi9cclxuXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuLy8gSW1wb3J0c1xyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG5pbXBvcnQgbG96YWRMb2FkIGZyb20gJyMvX21vZHVsZXMvbG96YWQtbG9hZCc7XHJcblxyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbi8vIFB1YmxpY1xyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG4vKipcclxuICogQHBhcmFtIHtKUXVlcnl9ICRlbGVtZW50c1xyXG4gKiBAcGFyYW0ge01vZHVsZUxvYWRlcn0gbW9kdWxlTG9hZGVyXHJcbiAqL1xyXG5mdW5jdGlvbiBsb2FkZXJJbml0ICgkZWxlbWVudHMsIG1vZHVsZUxvYWRlcikge1xyXG5cdGxvemFkTG9hZCgkZWxlbWVudHMpO1xyXG59XHJcblxyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbi8vIEV4cG9ydHNcclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuZXhwb3J0IHtsb2FkZXJJbml0fTtcclxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vX0hUTUwvc3JjL2pzL19tb2R1bGVzLWxvYWRlcnMvbG96YWQtLW1vZHVsZS1sb2FkZXIuanMiXSwic291cmNlUm9vdCI6IiJ9