webpackJsonp([7],{

/***/ 20:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * Прослойка для загрузки модуля `jquery-validation`
 * @module
 */

// ----------------------------------------
// Imports
// ----------------------------------------

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.loaderInit = undefined;

var _validateInit = __webpack_require__(4);

var _validateInit2 = _interopRequireDefault(_validateInit);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// ----------------------------------------
// Public
// ----------------------------------------

/**
 * @param {JQuery} $elements
 * @param {ModuleLoader} moduleLoader
 */
function loaderInit($elements, moduleLoader) {
  (0, _validateInit2.default)($elements);
}

// ----------------------------------------
// Exports
// ----------------------------------------

exports.loaderInit = loaderInit;

/***/ })

});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9fSFRNTC9zcmMvanMvX21vZHVsZXMtbG9hZGVycy9qcXVlcnktdmFsaWRhdGlvbi0tbW9kdWxlLWxvYWRlci5qcyJdLCJuYW1lcyI6WyJsb2FkZXJJbml0IiwiJGVsZW1lbnRzIiwibW9kdWxlTG9hZGVyIl0sIm1hcHBpbmdzIjoiOzs7Ozs7QUFBQTs7QUFFQTs7Ozs7QUFLQTtBQUNBO0FBQ0E7Ozs7Ozs7QUFFQTs7Ozs7O0FBRUE7QUFDQTtBQUNBOztBQUVBOzs7O0FBSUEsU0FBU0EsVUFBVCxDQUFxQkMsU0FBckIsRUFBZ0NDLFlBQWhDLEVBQThDO0FBQzdDLDhCQUFTRCxTQUFUO0FBQ0E7O0FBRUQ7QUFDQTtBQUNBOztRQUVRRCxVLEdBQUFBLFUiLCJmaWxlIjoiYXN5bmMtbW9kdWxlcy83LmpzP3Y9MWI4ZDc5N2VkNDliNWM5MTkxNDQiLCJzb3VyY2VzQ29udGVudCI6WyIndXNlIHN0cmljdCc7XHJcblxyXG4vKipcclxuICog0J/RgNC+0YHQu9C+0LnQutCwINC00LvRjyDQt9Cw0LPRgNGD0LfQutC4INC80L7QtNGD0LvRjyBganF1ZXJ5LXZhbGlkYXRpb25gXHJcbiAqIEBtb2R1bGVcclxuICovXHJcblxyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbi8vIEltcG9ydHNcclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuaW1wb3J0IHZhbGlkYXRlIGZyb20gJyMvX21vZHVsZXMvanF1ZXJ5LXZhbGlkYXRpb24vdmFsaWRhdGUtaW5pdCc7XHJcblxyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbi8vIFB1YmxpY1xyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG4vKipcclxuICogQHBhcmFtIHtKUXVlcnl9ICRlbGVtZW50c1xyXG4gKiBAcGFyYW0ge01vZHVsZUxvYWRlcn0gbW9kdWxlTG9hZGVyXHJcbiAqL1xyXG5mdW5jdGlvbiBsb2FkZXJJbml0ICgkZWxlbWVudHMsIG1vZHVsZUxvYWRlcikge1xyXG5cdHZhbGlkYXRlKCRlbGVtZW50cyk7XHJcbn1cclxuXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuLy8gRXhwb3J0c1xyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG5leHBvcnQge2xvYWRlckluaXR9O1xyXG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9fSFRNTC9zcmMvanMvX21vZHVsZXMtbG9hZGVycy9qcXVlcnktdmFsaWRhdGlvbi0tbW9kdWxlLWxvYWRlci5qcyJdLCJzb3VyY2VSb290IjoiIn0=