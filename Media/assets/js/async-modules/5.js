webpackJsonp([5],{

/***/ 25:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * Прослойка для загрузки модуля `prism`
 * @module
 */

// ----------------------------------------
// Public
// ----------------------------------------

/**
 * @param {JQuery} $elements
 * @param {ModuleLoader} moduleLoader
 */

Object.defineProperty(exports, "__esModule", {
  value: true
});
function loaderInit($elements, moduleLoader) {
  var $code = $elements.find('code[class*="lang"]');
  if ($code.length) {
    __webpack_require__.e/* import() */(10).then(__webpack_require__.bind(null, 107)).then(function (module) {
      return module.default($code);
    });
  }
}

// ----------------------------------------
// Exports
// ----------------------------------------

exports.loaderInit = loaderInit;

/***/ })

});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9fSFRNTC9zcmMvanMvX21vZHVsZXMtbG9hZGVycy9wcmlzbWpzLS1tb2R1bGUtbG9hZGVyLmpzIl0sIm5hbWVzIjpbImxvYWRlckluaXQiLCIkZWxlbWVudHMiLCJtb2R1bGVMb2FkZXIiLCIkY29kZSIsImZpbmQiLCJsZW5ndGgiLCJ0aGVuIiwibW9kdWxlIiwiZGVmYXVsdCJdLCJtYXBwaW5ncyI6Ijs7Ozs7O0FBQUE7O0FBRUE7Ozs7O0FBS0E7QUFDQTtBQUNBOztBQUVBOzs7Ozs7OztBQUlBLFNBQVNBLFVBQVQsQ0FBcUJDLFNBQXJCLEVBQWdDQyxZQUFoQyxFQUE4QztBQUM3QyxNQUFNQyxRQUFRRixVQUFVRyxJQUFWLENBQWUscUJBQWYsQ0FBZDtBQUNBLE1BQUlELE1BQU1FLE1BQVYsRUFBa0I7QUFDakIsc0ZBQW9FQyxJQUFwRSxDQUF5RTtBQUFBLGFBQVVDLE9BQU9DLE9BQVAsQ0FBZUwsS0FBZixDQUFWO0FBQUEsS0FBekU7QUFDQTtBQUNEOztBQUVEO0FBQ0E7QUFDQTs7UUFFUUgsVSxHQUFBQSxVIiwiZmlsZSI6ImFzeW5jLW1vZHVsZXMvNS5qcz92PWExMzg4YTAzZGExNWZkMjBiMmNmIiwic291cmNlc0NvbnRlbnQiOlsiJ3VzZSBzdHJpY3QnO1xyXG5cclxuLyoqXHJcbiAqINCf0YDQvtGB0LvQvtC50LrQsCDQtNC70Y8g0LfQsNCz0YDRg9C30LrQuCDQvNC+0LTRg9C70Y8gYHByaXNtYFxyXG4gKiBAbW9kdWxlXHJcbiAqL1xyXG5cclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4vLyBQdWJsaWNcclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuLyoqXHJcbiAqIEBwYXJhbSB7SlF1ZXJ5fSAkZWxlbWVudHNcclxuICogQHBhcmFtIHtNb2R1bGVMb2FkZXJ9IG1vZHVsZUxvYWRlclxyXG4gKi9cclxuZnVuY3Rpb24gbG9hZGVySW5pdCAoJGVsZW1lbnRzLCBtb2R1bGVMb2FkZXIpIHtcclxuXHRjb25zdCAkY29kZSA9ICRlbGVtZW50cy5maW5kKCdjb2RlW2NsYXNzKj1cImxhbmdcIl0nKTtcclxuXHRpZiAoJGNvZGUubGVuZ3RoKSB7XHJcblx0XHRpbXBvcnQoLyogd2VicGFja0NodW5rTmFtZTogXCJwcmlzbWpzXCIgKi8nIy9fbW9kdWxlcy9wcmlzbWpzL3ByaXNtJykudGhlbihtb2R1bGUgPT4gbW9kdWxlLmRlZmF1bHQoJGNvZGUpKTtcclxuXHR9XHJcbn1cclxuXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuLy8gRXhwb3J0c1xyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG5leHBvcnQge2xvYWRlckluaXR9O1xyXG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9fSFRNTC9zcmMvanMvX21vZHVsZXMtbG9hZGVycy9wcmlzbWpzLS1tb2R1bGUtbG9hZGVyLmpzIl0sInNvdXJjZVJvb3QiOiIifQ==