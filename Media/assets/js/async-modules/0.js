webpackJsonp([0],{

/***/ 140:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * Скролл window
 * @see {@link https://greensock.com/docs/Plugins/ScrollToPlugin}
 * @module
 */

// ----------------------------------------
// Imports
// ----------------------------------------

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _gsap = __webpack_require__(16);

var _gsap2 = _interopRequireDefault(_gsap);

var _TweenLite = __webpack_require__(95);

var _TweenLite2 = _interopRequireDefault(_TweenLite);

__webpack_require__(141);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// ----------------------------------------
// Private
// ----------------------------------------

/**
 * Длительность скролла, в секундах
 * @type {number}
 * @private
 */
var defaultDuration = 1;

/**
 * @type {JQuery}
 * @private
 */
var $window = $(window);

/**
 * @type {number}
 * @private
 */
var showHeight = 500;

/**
 * @type {string}
 * @private
 */
var showClass = 'scroll-up--show';

// ----------------------------------------
// Public
// ----------------------------------------

function scrollWindow($elements) {
	$elements.on('click', function () {
		var $this = $(this);
		var scroll = $this.data('scroll-window') || 0;

		if (scroll === 'up') {
			scroll = 0;
		} else if (scroll === 'down') {
			scroll = 'max';
		}

		_TweenLite2.default.to(window, defaultDuration, {
			scrollTo: scroll,
			ease: _gsap2.default.Power2.easeOut,
			onComplete: function onComplete() {
				// console.log('complete');
			}
		});
	});

	var $scrollUp = $elements.filter('[data-scroll-window="up"]');
	if ($scrollUp.length) {
		var onScroll = function onScroll() {
			var scroll = $window.scrollTop();
			var doClass = scroll > showHeight ? 'addClass' : 'removeClass';
			$scrollUp[doClass](showClass);
		};

		$window.on('scroll', onScroll);
		onScroll();
	}
}

// ----------------------------------------
// Exports
// ----------------------------------------

exports.default = scrollWindow;

/***/ }),

/***/ 141:
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(global) {var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

/*!
 * VERSION: 1.9.0
 * DATE: 2017-06-19
 * UPDATES AND DOCS AT: http://greensock.com
 *
 * @license Copyright (c) 2008-2017, GreenSock. All rights reserved.
 * This work is subject to the terms at http://greensock.com/standard-license or for
 * Club GreenSock members, the software agreement that was issued with your membership.
 *
 * @author: Jack Doyle, jack@greensock.com
 **/
var _gsScope = typeof module !== "undefined" && module.exports && typeof global !== "undefined" ? global : undefined || window; //helps ensure compatibility with AMD/RequireJS and CommonJS/Node
(_gsScope._gsQueue || (_gsScope._gsQueue = [])).push(function () {

	"use strict";

	var _doc = (_gsScope.document || {}).documentElement,
	    _window = _gsScope,
	    _max = function _max(element, axis) {
		var dim = axis === "x" ? "Width" : "Height",
		    scroll = "scroll" + dim,
		    client = "client" + dim,
		    body = document.body;
		return element === _window || element === _doc || element === body ? Math.max(_doc[scroll], body[scroll]) - (_window["inner" + dim] || _doc[client] || body[client]) : element[scroll] - element["offset" + dim];
	},
	    _unwrapElement = function _unwrapElement(value) {
		if (typeof value === "string") {
			value = _window.GreenSockGlobals.TweenLite.selector(value);
		}
		if (value.length && value !== _window && value[0] && value[0].style && !value.nodeType) {
			value = value[0];
		}
		return value === _window || value.nodeType && value.style ? value : null;
	},
	    _buildGetter = function _buildGetter(e, axis) {
		//pass in an element and an axis ("x" or "y") and it'll return a getter function for the scroll position of that element (like scrollTop or scrollLeft, although if the element is the window, it'll use the pageXOffset/pageYOffset or the documentElement's scrollTop/scrollLeft or document.body's. Basically this streamlines things and makes a very fast getter across browsers.
		var p = "scroll" + (axis === "x" ? "Left" : "Top");
		if (e === _window) {
			if (e.pageXOffset != null) {
				p = "page" + axis.toUpperCase() + "Offset";
			} else if (_doc[p] != null) {
				e = _doc;
			} else {
				e = document.body;
			}
		}
		return function () {
			return e[p];
		};
	},
	    _getOffset = function _getOffset(element, container) {
		var rect = _unwrapElement(element).getBoundingClientRect(),
		    isRoot = !container || container === _window || container === document.body,
		    cRect = (isRoot ? _doc : container).getBoundingClientRect(),
		    offsets = { x: rect.left - cRect.left, y: rect.top - cRect.top };
		if (!isRoot && container) {
			//only add the current scroll position if it's not the window/body.
			offsets.x += _buildGetter(container, "x")();
			offsets.y += _buildGetter(container, "y")();
		}
		return offsets;
	},
	    _parseVal = function _parseVal(value, target, axis) {
		var type = typeof value === "undefined" ? "undefined" : _typeof(value);
		return !isNaN(value) ? parseFloat(value) : type === "number" || type === "string" && value.charAt(1) === "=" ? value : value === "max" ? _max(target, axis) : Math.min(_max(target, axis), _getOffset(value, target)[axis]);
	},
	    ScrollToPlugin = _gsScope._gsDefine.plugin({
		propName: "scrollTo",
		API: 2,
		global: true,
		version: "1.9.0",

		//called when the tween renders for the first time. This is where initial values should be recorded and any setup routines should run.
		init: function init(target, value, tween) {
			this._wdw = target === _window;
			this._target = target;
			this._tween = tween;
			if ((typeof value === "undefined" ? "undefined" : _typeof(value)) !== "object") {
				value = { y: value }; //if we don't receive an object as the parameter, assume the user intends "y".
				if (typeof value.y === "string" && value.y !== "max" && value.y.charAt(1) !== "=") {
					value.x = value.y;
				}
			} else if (value.nodeType) {
				value = { y: value, x: value };
			}
			this.vars = value;
			this._autoKill = value.autoKill !== false;
			this.getX = _buildGetter(target, "x");
			this.getY = _buildGetter(target, "y");
			this.x = this.xPrev = this.getX();
			this.y = this.yPrev = this.getY();
			if (value.x != null) {
				this._addTween(this, "x", this.x, _parseVal(value.x, target, "x") - (value.offsetX || 0), "scrollTo_x", true);
				this._overwriteProps.push("scrollTo_x");
			} else {
				this.skipX = true;
			}
			if (value.y != null) {
				this._addTween(this, "y", this.y, _parseVal(value.y, target, "y") - (value.offsetY || 0), "scrollTo_y", true);
				this._overwriteProps.push("scrollTo_y");
			} else {
				this.skipY = true;
			}
			return true;
		},

		//called each time the values should be updated, and the ratio gets passed as the only parameter (typically it's a value between 0 and 1, but it can exceed those when using an ease like Elastic.easeOut or Back.easeOut, etc.)
		set: function set(v) {
			this._super.setRatio.call(this, v);

			var x = this._wdw || !this.skipX ? this.getX() : this.xPrev,
			    y = this._wdw || !this.skipY ? this.getY() : this.yPrev,
			    yDif = y - this.yPrev,
			    xDif = x - this.xPrev,
			    threshold = ScrollToPlugin.autoKillThreshold;

			if (this.x < 0) {
				//can't scroll to a position less than 0! Might happen if someone uses a Back.easeOut or Elastic.easeOut when scrolling back to the top of the page (for example)
				this.x = 0;
			}
			if (this.y < 0) {
				this.y = 0;
			}
			if (this._autoKill) {
				//note: iOS has a bug that throws off the scroll by several pixels, so we need to check if it's within 7 pixels of the previous one that we set instead of just looking for an exact match.
				if (!this.skipX && (xDif > threshold || xDif < -threshold) && x < _max(this._target, "x")) {
					this.skipX = true; //if the user scrolls separately, we should stop tweening!
				}
				if (!this.skipY && (yDif > threshold || yDif < -threshold) && y < _max(this._target, "y")) {
					this.skipY = true; //if the user scrolls separately, we should stop tweening!
				}
				if (this.skipX && this.skipY) {
					this._tween.kill();
					if (this.vars.onAutoKill) {
						this.vars.onAutoKill.apply(this.vars.onAutoKillScope || this._tween, this.vars.onAutoKillParams || []);
					}
				}
			}
			if (this._wdw) {
				_window.scrollTo(!this.skipX ? this.x : x, !this.skipY ? this.y : y);
			} else {
				if (!this.skipY) {
					this._target.scrollTop = this.y;
				}
				if (!this.skipX) {
					this._target.scrollLeft = this.x;
				}
			}
			this.xPrev = this.x;
			this.yPrev = this.y;
		}

	}),
	    p = ScrollToPlugin.prototype;

	ScrollToPlugin.max = _max;
	ScrollToPlugin.getOffset = _getOffset;
	ScrollToPlugin.buildGetter = _buildGetter;
	ScrollToPlugin.autoKillThreshold = 7;

	p._kill = function (lookup) {
		if (lookup.scrollTo_x) {
			this.skipX = true;
		}
		if (lookup.scrollTo_y) {
			this.skipY = true;
		}
		return this._super._kill.call(this, lookup);
	};
});if (_gsScope._gsDefine) {
	_gsScope._gsQueue.pop()();
}

//export to AMD/RequireJS and CommonJS/Node (precursor to full modular build system coming at a later date)
(function (name) {
	"use strict";

	var getGlobal = function getGlobal() {
		return (_gsScope.GreenSockGlobals || _gsScope)[name];
	};
	if (typeof module !== "undefined" && module.exports) {
		//node
		__webpack_require__(95);
		module.exports = getGlobal();
	} else if (true) {
		//AMD
		!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(95)], __WEBPACK_AMD_DEFINE_FACTORY__ = (getGlobal),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	}
})("ScrollToPlugin");
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(3)))

/***/ }),

/***/ 26:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * Прослойка для загрузки модуля `scroll-window`
 * @module
 */

// ----------------------------------------
// Imports
// ----------------------------------------

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.loaderInit = undefined;

var _scrollWindow = __webpack_require__(140);

var _scrollWindow2 = _interopRequireDefault(_scrollWindow);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// ----------------------------------------
// Public
// ----------------------------------------

/**
 * @param {JQuery} $elements
 * @param {ModuleLoader} moduleLoader
 */
function loaderInit($elements, moduleLoader) {
  (0, _scrollWindow2.default)($elements);
}

// ----------------------------------------
// Exports
// ----------------------------------------

exports.loaderInit = loaderInit;

/***/ }),

/***/ 95:
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(global) {var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

/*!
 * VERSION: 1.20.3
 * DATE: 2017-10-02
 * UPDATES AND DOCS AT: http://greensock.com
 *
 * @license Copyright (c) 2008-2017, GreenSock. All rights reserved.
 * This work is subject to the terms at http://greensock.com/standard-license or for
 * Club GreenSock members, the software agreement that was issued with your membership.
 * 
 * @author: Jack Doyle, jack@greensock.com
 */
(function (window, moduleName) {

	"use strict";

	var _exports = {},
	    _doc = window.document,
	    _globals = window.GreenSockGlobals = window.GreenSockGlobals || window;
	if (_globals.TweenLite) {
		return; //in case the core set of classes is already loaded, don't instantiate twice.
	}
	var _namespace = function _namespace(ns) {
		var a = ns.split("."),
		    p = _globals,
		    i;
		for (i = 0; i < a.length; i++) {
			p[a[i]] = p = p[a[i]] || {};
		}
		return p;
	},
	    gs = _namespace("com.greensock"),
	    _tinyNum = 0.0000000001,
	    _slice = function _slice(a) {
		//don't use Array.prototype.slice.call(target, 0) because that doesn't work in IE8 with a NodeList that's returned by querySelectorAll()
		var b = [],
		    l = a.length,
		    i;
		for (i = 0; i !== l; b.push(a[i++])) {}
		return b;
	},
	    _emptyFunc = function _emptyFunc() {},
	    _isArray = function () {
		//works around issues in iframe environments where the Array global isn't shared, thus if the object originates in a different window/iframe, "(obj instanceof Array)" will evaluate false. We added some speed optimizations to avoid Object.prototype.toString.call() unless it's absolutely necessary because it's VERY slow (like 20x slower)
		var toString = Object.prototype.toString,
		    array = toString.call([]);
		return function (obj) {
			return obj != null && (obj instanceof Array || (typeof obj === "undefined" ? "undefined" : _typeof(obj)) === "object" && !!obj.push && toString.call(obj) === array);
		};
	}(),
	    a,
	    i,
	    p,
	    _ticker,
	    _tickerActive,
	    _defLookup = {},


	/**
  * @constructor
  * Defines a GreenSock class, optionally with an array of dependencies that must be instantiated first and passed into the definition.
  * This allows users to load GreenSock JS files in any order even if they have interdependencies (like CSSPlugin extends TweenPlugin which is
  * inside TweenLite.js, but if CSSPlugin is loaded first, it should wait to run its code until TweenLite.js loads and instantiates TweenPlugin
  * and then pass TweenPlugin to CSSPlugin's definition). This is all done automatically and internally.
  *
  * Every definition will be added to a "com.greensock" global object (typically window, but if a window.GreenSockGlobals object is found,
  * it will go there as of v1.7). For example, TweenLite will be found at window.com.greensock.TweenLite and since it's a global class that should be available anywhere,
  * it is ALSO referenced at window.TweenLite. However some classes aren't considered global, like the base com.greensock.core.Animation class, so
  * those will only be at the package like window.com.greensock.core.Animation. Again, if you define a GreenSockGlobals object on the window, everything
  * gets tucked neatly inside there instead of on the window directly. This allows you to do advanced things like load multiple versions of GreenSock
  * files and put them into distinct objects (imagine a banner ad uses a newer version but the main site uses an older one). In that case, you could
  * sandbox the banner one like:
  *
  * <script>
  *     var gs = window.GreenSockGlobals = {}; //the newer version we're about to load could now be referenced in a "gs" object, like gs.TweenLite.to(...). Use whatever alias you want as long as it's unique, "gs" or "banner" or whatever.
  * </script>
  * <script src="js/greensock/v1.7/TweenMax.js"></script>
  * <script>
  *     window.GreenSockGlobals = window._gsQueue = window._gsDefine = null; //reset it back to null (along with the special _gsQueue variable) so that the next load of TweenMax affects the window and we can reference things directly like TweenLite.to(...)
  * </script>
  * <script src="js/greensock/v1.6/TweenMax.js"></script>
  * <script>
  *     gs.TweenLite.to(...); //would use v1.7
  *     TweenLite.to(...); //would use v1.6
  * </script>
  *
  * @param {!string} ns The namespace of the class definition, leaving off "com.greensock." as that's assumed. For example, "TweenLite" or "plugins.CSSPlugin" or "easing.Back".
  * @param {!Array.<string>} dependencies An array of dependencies (described as their namespaces minus "com.greensock." prefix). For example ["TweenLite","plugins.TweenPlugin","core.Animation"]
  * @param {!function():Object} func The function that should be called and passed the resolved dependencies which will return the actual class for this definition.
  * @param {boolean=} global If true, the class will be added to the global scope (typically window unless you define a window.GreenSockGlobals object)
  */
	Definition = function Definition(ns, dependencies, func, global) {
		this.sc = _defLookup[ns] ? _defLookup[ns].sc : []; //subclasses
		_defLookup[ns] = this;
		this.gsClass = null;
		this.func = func;
		var _classes = [];
		this.check = function (init) {
			var i = dependencies.length,
			    missing = i,
			    cur,
			    a,
			    n,
			    cl;
			while (--i > -1) {
				if ((cur = _defLookup[dependencies[i]] || new Definition(dependencies[i], [])).gsClass) {
					_classes[i] = cur.gsClass;
					missing--;
				} else if (init) {
					cur.sc.push(this);
				}
			}
			if (missing === 0 && func) {
				a = ("com.greensock." + ns).split(".");
				n = a.pop();
				cl = _namespace(a.join("."))[n] = this.gsClass = func.apply(func, _classes);

				//exports to multiple environments
				if (global) {
					_globals[n] = _exports[n] = cl; //provides a way to avoid global namespace pollution. By default, the main classes like TweenLite, Power1, Strong, etc. are added to window unless a GreenSockGlobals is defined. So if you want to have things added to a custom object instead, just do something like window.GreenSockGlobals = {} before loading any GreenSock files. You can even set up an alias like window.GreenSockGlobals = windows.gs = {} so that you can access everything like gs.TweenLite. Also remember that ALL classes are added to the window.com.greensock object (in their respective packages, like com.greensock.easing.Power1, com.greensock.TweenLite, etc.)
					if (typeof module !== "undefined" && module.exports) {
						//node
						if (ns === moduleName) {
							module.exports = _exports[moduleName] = cl;
							for (i in _exports) {
								cl[i] = _exports[i];
							}
						} else if (_exports[moduleName]) {
							_exports[moduleName][n] = cl;
						}
					} else if (true) {
						//AMD
						!(__WEBPACK_AMD_DEFINE_ARRAY__ = [], __WEBPACK_AMD_DEFINE_RESULT__ = (function () {
							return cl;
						}).apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
					}
				}
				for (i = 0; i < this.sc.length; i++) {
					this.sc[i].check();
				}
			}
		};
		this.check(true);
	},


	//used to create Definition instances (which basically registers a class that has dependencies).
	_gsDefine = window._gsDefine = function (ns, dependencies, func, global) {
		return new Definition(ns, dependencies, func, global);
	},


	//a quick way to create a class that doesn't have any dependencies. Returns the class, but first registers it in the GreenSock namespace so that other classes can grab it (other classes might be dependent on the class).
	_class = gs._class = function (ns, func, global) {
		func = func || function () {};
		_gsDefine(ns, [], function () {
			return func;
		}, global);
		return func;
	};

	_gsDefine.globals = _globals;

	/*
  * ----------------------------------------------------------------
  * Ease
  * ----------------------------------------------------------------
  */
	var _baseParams = [0, 0, 1, 1],
	    Ease = _class("easing.Ease", function (func, extraParams, type, power) {
		this._func = func;
		this._type = type || 0;
		this._power = power || 0;
		this._params = extraParams ? _baseParams.concat(extraParams) : _baseParams;
	}, true),
	    _easeMap = Ease.map = {},
	    _easeReg = Ease.register = function (ease, names, types, create) {
		var na = names.split(","),
		    i = na.length,
		    ta = (types || "easeIn,easeOut,easeInOut").split(","),
		    e,
		    name,
		    j,
		    type;
		while (--i > -1) {
			name = na[i];
			e = create ? _class("easing." + name, null, true) : gs.easing[name] || {};
			j = ta.length;
			while (--j > -1) {
				type = ta[j];
				_easeMap[name + "." + type] = _easeMap[type + name] = e[type] = ease.getRatio ? ease : ease[type] || new ease();
			}
		}
	};

	p = Ease.prototype;
	p._calcEnd = false;
	p.getRatio = function (p) {
		if (this._func) {
			this._params[0] = p;
			return this._func.apply(null, this._params);
		}
		var t = this._type,
		    pw = this._power,
		    r = t === 1 ? 1 - p : t === 2 ? p : p < 0.5 ? p * 2 : (1 - p) * 2;
		if (pw === 1) {
			r *= r;
		} else if (pw === 2) {
			r *= r * r;
		} else if (pw === 3) {
			r *= r * r * r;
		} else if (pw === 4) {
			r *= r * r * r * r;
		}
		return t === 1 ? 1 - r : t === 2 ? r : p < 0.5 ? r / 2 : 1 - r / 2;
	};

	//create all the standard eases like Linear, Quad, Cubic, Quart, Quint, Strong, Power0, Power1, Power2, Power3, and Power4 (each with easeIn, easeOut, and easeInOut)
	a = ["Linear", "Quad", "Cubic", "Quart", "Quint,Strong"];
	i = a.length;
	while (--i > -1) {
		p = a[i] + ",Power" + i;
		_easeReg(new Ease(null, null, 1, i), p, "easeOut", true);
		_easeReg(new Ease(null, null, 2, i), p, "easeIn" + (i === 0 ? ",easeNone" : ""));
		_easeReg(new Ease(null, null, 3, i), p, "easeInOut");
	}
	_easeMap.linear = gs.easing.Linear.easeIn;
	_easeMap.swing = gs.easing.Quad.easeInOut; //for jQuery folks


	/*
  * ----------------------------------------------------------------
  * EventDispatcher
  * ----------------------------------------------------------------
  */
	var EventDispatcher = _class("events.EventDispatcher", function (target) {
		this._listeners = {};
		this._eventTarget = target || this;
	});
	p = EventDispatcher.prototype;

	p.addEventListener = function (type, callback, scope, useParam, priority) {
		priority = priority || 0;
		var list = this._listeners[type],
		    index = 0,
		    listener,
		    i;
		if (this === _ticker && !_tickerActive) {
			_ticker.wake();
		}
		if (list == null) {
			this._listeners[type] = list = [];
		}
		i = list.length;
		while (--i > -1) {
			listener = list[i];
			if (listener.c === callback && listener.s === scope) {
				list.splice(i, 1);
			} else if (index === 0 && listener.pr < priority) {
				index = i + 1;
			}
		}
		list.splice(index, 0, { c: callback, s: scope, up: useParam, pr: priority });
	};

	p.removeEventListener = function (type, callback) {
		var list = this._listeners[type],
		    i;
		if (list) {
			i = list.length;
			while (--i > -1) {
				if (list[i].c === callback) {
					list.splice(i, 1);
					return;
				}
			}
		}
	};

	p.dispatchEvent = function (type) {
		var list = this._listeners[type],
		    i,
		    t,
		    listener;
		if (list) {
			i = list.length;
			if (i > 1) {
				list = list.slice(0); //in case addEventListener() is called from within a listener/callback (otherwise the index could change, resulting in a skip)
			}
			t = this._eventTarget;
			while (--i > -1) {
				listener = list[i];
				if (listener) {
					if (listener.up) {
						listener.c.call(listener.s || t, { type: type, target: t });
					} else {
						listener.c.call(listener.s || t);
					}
				}
			}
		}
	};

	/*
  * ----------------------------------------------------------------
  * Ticker
  * ----------------------------------------------------------------
  */
	var _reqAnimFrame = window.requestAnimationFrame,
	    _cancelAnimFrame = window.cancelAnimationFrame,
	    _getTime = Date.now || function () {
		return new Date().getTime();
	},
	    _lastUpdate = _getTime();

	//now try to determine the requestAnimationFrame and cancelAnimationFrame functions and if none are found, we'll use a setTimeout()/clearTimeout() polyfill.
	a = ["ms", "moz", "webkit", "o"];
	i = a.length;
	while (--i > -1 && !_reqAnimFrame) {
		_reqAnimFrame = window[a[i] + "RequestAnimationFrame"];
		_cancelAnimFrame = window[a[i] + "CancelAnimationFrame"] || window[a[i] + "CancelRequestAnimationFrame"];
	}

	_class("Ticker", function (fps, useRAF) {
		var _self = this,
		    _startTime = _getTime(),
		    _useRAF = useRAF !== false && _reqAnimFrame ? "auto" : false,
		    _lagThreshold = 500,
		    _adjustedLag = 33,
		    _tickWord = "tick",
		    //helps reduce gc burden
		_fps,
		    _req,
		    _id,
		    _gap,
		    _nextTime,
		    _tick = function _tick(manual) {
			var elapsed = _getTime() - _lastUpdate,
			    overlap,
			    dispatch;
			if (elapsed > _lagThreshold) {
				_startTime += elapsed - _adjustedLag;
			}
			_lastUpdate += elapsed;
			_self.time = (_lastUpdate - _startTime) / 1000;
			overlap = _self.time - _nextTime;
			if (!_fps || overlap > 0 || manual === true) {
				_self.frame++;
				_nextTime += overlap + (overlap >= _gap ? 0.004 : _gap - overlap);
				dispatch = true;
			}
			if (manual !== true) {
				//make sure the request is made before we dispatch the "tick" event so that timing is maintained. Otherwise, if processing the "tick" requires a bunch of time (like 15ms) and we're using a setTimeout() that's based on 16.7ms, it'd technically take 31.7ms between frames otherwise.
				_id = _req(_tick);
			}
			if (dispatch) {
				_self.dispatchEvent(_tickWord);
			}
		};

		EventDispatcher.call(_self);
		_self.time = _self.frame = 0;
		_self.tick = function () {
			_tick(true);
		};

		_self.lagSmoothing = function (threshold, adjustedLag) {
			if (!arguments.length) {
				//if lagSmoothing() is called with no arguments, treat it like a getter that returns a boolean indicating if it's enabled or not. This is purposely undocumented and is for internal use.
				return _lagThreshold < 1 / _tinyNum;
			}
			_lagThreshold = threshold || 1 / _tinyNum; //zero should be interpreted as basically unlimited
			_adjustedLag = Math.min(adjustedLag, _lagThreshold, 0);
		};

		_self.sleep = function () {
			if (_id == null) {
				return;
			}
			if (!_useRAF || !_cancelAnimFrame) {
				clearTimeout(_id);
			} else {
				_cancelAnimFrame(_id);
			}
			_req = _emptyFunc;
			_id = null;
			if (_self === _ticker) {
				_tickerActive = false;
			}
		};

		_self.wake = function (seamless) {
			if (_id !== null) {
				_self.sleep();
			} else if (seamless) {
				_startTime += -_lastUpdate + (_lastUpdate = _getTime());
			} else if (_self.frame > 10) {
				//don't trigger lagSmoothing if we're just waking up, and make sure that at least 10 frames have elapsed because of the iOS bug that we work around below with the 1.5-second setTimout().
				_lastUpdate = _getTime() - _lagThreshold + 5;
			}
			_req = _fps === 0 ? _emptyFunc : !_useRAF || !_reqAnimFrame ? function (f) {
				return setTimeout(f, (_nextTime - _self.time) * 1000 + 1 | 0);
			} : _reqAnimFrame;
			if (_self === _ticker) {
				_tickerActive = true;
			}
			_tick(2);
		};

		_self.fps = function (value) {
			if (!arguments.length) {
				return _fps;
			}
			_fps = value;
			_gap = 1 / (_fps || 60);
			_nextTime = this.time + _gap;
			_self.wake();
		};

		_self.useRAF = function (value) {
			if (!arguments.length) {
				return _useRAF;
			}
			_self.sleep();
			_useRAF = value;
			_self.fps(_fps);
		};
		_self.fps(fps);

		//a bug in iOS 6 Safari occasionally prevents the requestAnimationFrame from working initially, so we use a 1.5-second timeout that automatically falls back to setTimeout() if it senses this condition.
		setTimeout(function () {
			if (_useRAF === "auto" && _self.frame < 5 && _doc.visibilityState !== "hidden") {
				_self.useRAF(false);
			}
		}, 1500);
	});

	p = gs.Ticker.prototype = new gs.events.EventDispatcher();
	p.constructor = gs.Ticker;

	/*
  * ----------------------------------------------------------------
  * Animation
  * ----------------------------------------------------------------
  */
	var Animation = _class("core.Animation", function (duration, vars) {
		this.vars = vars = vars || {};
		this._duration = this._totalDuration = duration || 0;
		this._delay = Number(vars.delay) || 0;
		this._timeScale = 1;
		this._active = vars.immediateRender === true;
		this.data = vars.data;
		this._reversed = vars.reversed === true;

		if (!_rootTimeline) {
			return;
		}
		if (!_tickerActive) {
			//some browsers (like iOS 6 Safari) shut down JavaScript execution when the tab is disabled and they [occasionally] neglect to start up requestAnimationFrame again when returning - this code ensures that the engine starts up again properly.
			_ticker.wake();
		}

		var tl = this.vars.useFrames ? _rootFramesTimeline : _rootTimeline;
		tl.add(this, tl._time);

		if (this.vars.paused) {
			this.paused(true);
		}
	});

	_ticker = Animation.ticker = new gs.Ticker();
	p = Animation.prototype;
	p._dirty = p._gc = p._initted = p._paused = false;
	p._totalTime = p._time = 0;
	p._rawPrevTime = -1;
	p._next = p._last = p._onUpdate = p._timeline = p.timeline = null;
	p._paused = false;

	//some browsers (like iOS) occasionally drop the requestAnimationFrame event when the user switches to a different tab and then comes back again, so we use a 2-second setTimeout() to sense if/when that condition occurs and then wake() the ticker.
	var _checkTimeout = function _checkTimeout() {
		if (_tickerActive && _getTime() - _lastUpdate > 2000 && (_doc.visibilityState !== "hidden" || !_ticker.lagSmoothing())) {
			//note: if the tab is hidden, we should still wake if lagSmoothing has been disabled.
			_ticker.wake();
		}
		var t = setTimeout(_checkTimeout, 2000);
		if (t.unref) {
			// allows a node process to exit even if the timeout’s callback hasn't been invoked. Without it, the node process could hang as this function is called every two seconds.
			t.unref();
		}
	};
	_checkTimeout();

	p.play = function (from, suppressEvents) {
		if (from != null) {
			this.seek(from, suppressEvents);
		}
		return this.reversed(false).paused(false);
	};

	p.pause = function (atTime, suppressEvents) {
		if (atTime != null) {
			this.seek(atTime, suppressEvents);
		}
		return this.paused(true);
	};

	p.resume = function (from, suppressEvents) {
		if (from != null) {
			this.seek(from, suppressEvents);
		}
		return this.paused(false);
	};

	p.seek = function (time, suppressEvents) {
		return this.totalTime(Number(time), suppressEvents !== false);
	};

	p.restart = function (includeDelay, suppressEvents) {
		return this.reversed(false).paused(false).totalTime(includeDelay ? -this._delay : 0, suppressEvents !== false, true);
	};

	p.reverse = function (from, suppressEvents) {
		if (from != null) {
			this.seek(from || this.totalDuration(), suppressEvents);
		}
		return this.reversed(true).paused(false);
	};

	p.render = function (time, suppressEvents, force) {
		//stub - we override this method in subclasses.
	};

	p.invalidate = function () {
		this._time = this._totalTime = 0;
		this._initted = this._gc = false;
		this._rawPrevTime = -1;
		if (this._gc || !this.timeline) {
			this._enabled(true);
		}
		return this;
	};

	p.isActive = function () {
		var tl = this._timeline,
		    //the 2 root timelines won't have a _timeline; they're always active.
		startTime = this._startTime,
		    rawTime;
		return !tl || !this._gc && !this._paused && tl.isActive() && (rawTime = tl.rawTime(true)) >= startTime && rawTime < startTime + this.totalDuration() / this._timeScale - 0.0000001;
	};

	p._enabled = function (enabled, ignoreTimeline) {
		if (!_tickerActive) {
			_ticker.wake();
		}
		this._gc = !enabled;
		this._active = this.isActive();
		if (ignoreTimeline !== true) {
			if (enabled && !this.timeline) {
				this._timeline.add(this, this._startTime - this._delay);
			} else if (!enabled && this.timeline) {
				this._timeline._remove(this, true);
			}
		}
		return false;
	};

	p._kill = function (vars, target) {
		return this._enabled(false, false);
	};

	p.kill = function (vars, target) {
		this._kill(vars, target);
		return this;
	};

	p._uncache = function (includeSelf) {
		var tween = includeSelf ? this : this.timeline;
		while (tween) {
			tween._dirty = true;
			tween = tween.timeline;
		}
		return this;
	};

	p._swapSelfInParams = function (params) {
		var i = params.length,
		    copy = params.concat();
		while (--i > -1) {
			if (params[i] === "{self}") {
				copy[i] = this;
			}
		}
		return copy;
	};

	p._callback = function (type) {
		var v = this.vars,
		    callback = v[type],
		    params = v[type + "Params"],
		    scope = v[type + "Scope"] || v.callbackScope || this,
		    l = params ? params.length : 0;
		switch (l) {//speed optimization; call() is faster than apply() so use it when there are only a few parameters (which is by far most common). Previously we simply did var v = this.vars; v[type].apply(v[type + "Scope"] || v.callbackScope || this, v[type + "Params"] || _blankArray);
			case 0:
				callback.call(scope);break;
			case 1:
				callback.call(scope, params[0]);break;
			case 2:
				callback.call(scope, params[0], params[1]);break;
			default:
				callback.apply(scope, params);
		}
	};

	//----Animation getters/setters --------------------------------------------------------

	p.eventCallback = function (type, callback, params, scope) {
		if ((type || "").substr(0, 2) === "on") {
			var v = this.vars;
			if (arguments.length === 1) {
				return v[type];
			}
			if (callback == null) {
				delete v[type];
			} else {
				v[type] = callback;
				v[type + "Params"] = _isArray(params) && params.join("").indexOf("{self}") !== -1 ? this._swapSelfInParams(params) : params;
				v[type + "Scope"] = scope;
			}
			if (type === "onUpdate") {
				this._onUpdate = callback;
			}
		}
		return this;
	};

	p.delay = function (value) {
		if (!arguments.length) {
			return this._delay;
		}
		if (this._timeline.smoothChildTiming) {
			this.startTime(this._startTime + value - this._delay);
		}
		this._delay = value;
		return this;
	};

	p.duration = function (value) {
		if (!arguments.length) {
			this._dirty = false;
			return this._duration;
		}
		this._duration = this._totalDuration = value;
		this._uncache(true); //true in case it's a TweenMax or TimelineMax that has a repeat - we'll need to refresh the totalDuration.
		if (this._timeline.smoothChildTiming) if (this._time > 0) if (this._time < this._duration) if (value !== 0) {
			this.totalTime(this._totalTime * (value / this._duration), true);
		}
		return this;
	};

	p.totalDuration = function (value) {
		this._dirty = false;
		return !arguments.length ? this._totalDuration : this.duration(value);
	};

	p.time = function (value, suppressEvents) {
		if (!arguments.length) {
			return this._time;
		}
		if (this._dirty) {
			this.totalDuration();
		}
		return this.totalTime(value > this._duration ? this._duration : value, suppressEvents);
	};

	p.totalTime = function (time, suppressEvents, uncapped) {
		if (!_tickerActive) {
			_ticker.wake();
		}
		if (!arguments.length) {
			return this._totalTime;
		}
		if (this._timeline) {
			if (time < 0 && !uncapped) {
				time += this.totalDuration();
			}
			if (this._timeline.smoothChildTiming) {
				if (this._dirty) {
					this.totalDuration();
				}
				var totalDuration = this._totalDuration,
				    tl = this._timeline;
				if (time > totalDuration && !uncapped) {
					time = totalDuration;
				}
				this._startTime = (this._paused ? this._pauseTime : tl._time) - (!this._reversed ? time : totalDuration - time) / this._timeScale;
				if (!tl._dirty) {
					//for performance improvement. If the parent's cache is already dirty, it already took care of marking the ancestors as dirty too, so skip the function call here.
					this._uncache(false);
				}
				//in case any of the ancestor timelines had completed but should now be enabled, we should reset their totalTime() which will also ensure that they're lined up properly and enabled. Skip for animations that are on the root (wasteful). Example: a TimelineLite.exportRoot() is performed when there's a paused tween on the root, the export will not complete until that tween is unpaused, but imagine a child gets restarted later, after all [unpaused] tweens have completed. The startTime of that child would get pushed out, but one of the ancestors may have completed.
				if (tl._timeline) {
					while (tl._timeline) {
						if (tl._timeline._time !== (tl._startTime + tl._totalTime) / tl._timeScale) {
							tl.totalTime(tl._totalTime, true);
						}
						tl = tl._timeline;
					}
				}
			}
			if (this._gc) {
				this._enabled(true, false);
			}
			if (this._totalTime !== time || this._duration === 0) {
				if (_lazyTweens.length) {
					_lazyRender();
				}
				this.render(time, suppressEvents, false);
				if (_lazyTweens.length) {
					//in case rendering caused any tweens to lazy-init, we should render them because typically when someone calls seek() or time() or progress(), they expect an immediate render.
					_lazyRender();
				}
			}
		}
		return this;
	};

	p.progress = p.totalProgress = function (value, suppressEvents) {
		var duration = this.duration();
		return !arguments.length ? duration ? this._time / duration : this.ratio : this.totalTime(duration * value, suppressEvents);
	};

	p.startTime = function (value) {
		if (!arguments.length) {
			return this._startTime;
		}
		if (value !== this._startTime) {
			this._startTime = value;
			if (this.timeline) if (this.timeline._sortChildren) {
				this.timeline.add(this, value - this._delay); //ensures that any necessary re-sequencing of Animations in the timeline occurs to make sure the rendering order is correct.
			}
		}
		return this;
	};

	p.endTime = function (includeRepeats) {
		return this._startTime + (includeRepeats != false ? this.totalDuration() : this.duration()) / this._timeScale;
	};

	p.timeScale = function (value) {
		if (!arguments.length) {
			return this._timeScale;
		}
		var pauseTime, t;
		value = value || _tinyNum; //can't allow zero because it'll throw the math off
		if (this._timeline && this._timeline.smoothChildTiming) {
			pauseTime = this._pauseTime;
			t = pauseTime || pauseTime === 0 ? pauseTime : this._timeline.totalTime();
			this._startTime = t - (t - this._startTime) * this._timeScale / value;
		}
		this._timeScale = value;
		t = this.timeline;
		while (t && t.timeline) {
			//must update the duration/totalDuration of all ancestor timelines immediately in case in the middle of a render loop, one tween alters another tween's timeScale which shoves its startTime before 0, forcing the parent timeline to shift around and shiftChildren() which could affect that next tween's render (startTime). Doesn't matter for the root timeline though.
			t._dirty = true;
			t.totalDuration();
			t = t.timeline;
		}
		return this;
	};

	p.reversed = function (value) {
		if (!arguments.length) {
			return this._reversed;
		}
		if (value != this._reversed) {
			this._reversed = value;
			this.totalTime(this._timeline && !this._timeline.smoothChildTiming ? this.totalDuration() - this._totalTime : this._totalTime, true);
		}
		return this;
	};

	p.paused = function (value) {
		if (!arguments.length) {
			return this._paused;
		}
		var tl = this._timeline,
		    raw,
		    elapsed;
		if (value != this._paused) if (tl) {
			if (!_tickerActive && !value) {
				_ticker.wake();
			}
			raw = tl.rawTime();
			elapsed = raw - this._pauseTime;
			if (!value && tl.smoothChildTiming) {
				this._startTime += elapsed;
				this._uncache(false);
			}
			this._pauseTime = value ? raw : null;
			this._paused = value;
			this._active = this.isActive();
			if (!value && elapsed !== 0 && this._initted && this.duration()) {
				raw = tl.smoothChildTiming ? this._totalTime : (raw - this._startTime) / this._timeScale;
				this.render(raw, raw === this._totalTime, true); //in case the target's properties changed via some other tween or manual update by the user, we should force a render.
			}
		}
		if (this._gc && !value) {
			this._enabled(true, false);
		}
		return this;
	};

	/*
  * ----------------------------------------------------------------
  * SimpleTimeline
  * ----------------------------------------------------------------
  */
	var SimpleTimeline = _class("core.SimpleTimeline", function (vars) {
		Animation.call(this, 0, vars);
		this.autoRemoveChildren = this.smoothChildTiming = true;
	});

	p = SimpleTimeline.prototype = new Animation();
	p.constructor = SimpleTimeline;
	p.kill()._gc = false;
	p._first = p._last = p._recent = null;
	p._sortChildren = false;

	p.add = p.insert = function (child, position, align, stagger) {
		var prevTween, st;
		child._startTime = Number(position || 0) + child._delay;
		if (child._paused) if (this !== child._timeline) {
			//we only adjust the _pauseTime if it wasn't in this timeline already. Remember, sometimes a tween will be inserted again into the same timeline when its startTime is changed so that the tweens in the TimelineLite/Max are re-ordered properly in the linked list (so everything renders in the proper order).
			child._pauseTime = child._startTime + (this.rawTime() - child._startTime) / child._timeScale;
		}
		if (child.timeline) {
			child.timeline._remove(child, true); //removes from existing timeline so that it can be properly added to this one.
		}
		child.timeline = child._timeline = this;
		if (child._gc) {
			child._enabled(true, true);
		}
		prevTween = this._last;
		if (this._sortChildren) {
			st = child._startTime;
			while (prevTween && prevTween._startTime > st) {
				prevTween = prevTween._prev;
			}
		}
		if (prevTween) {
			child._next = prevTween._next;
			prevTween._next = child;
		} else {
			child._next = this._first;
			this._first = child;
		}
		if (child._next) {
			child._next._prev = child;
		} else {
			this._last = child;
		}
		child._prev = prevTween;
		this._recent = child;
		if (this._timeline) {
			this._uncache(true);
		}
		return this;
	};

	p._remove = function (tween, skipDisable) {
		if (tween.timeline === this) {
			if (!skipDisable) {
				tween._enabled(false, true);
			}

			if (tween._prev) {
				tween._prev._next = tween._next;
			} else if (this._first === tween) {
				this._first = tween._next;
			}
			if (tween._next) {
				tween._next._prev = tween._prev;
			} else if (this._last === tween) {
				this._last = tween._prev;
			}
			tween._next = tween._prev = tween.timeline = null;
			if (tween === this._recent) {
				this._recent = this._last;
			}

			if (this._timeline) {
				this._uncache(true);
			}
		}
		return this;
	};

	p.render = function (time, suppressEvents, force) {
		var tween = this._first,
		    next;
		this._totalTime = this._time = this._rawPrevTime = time;
		while (tween) {
			next = tween._next; //record it here because the value could change after rendering...
			if (tween._active || time >= tween._startTime && !tween._paused && !tween._gc) {
				if (!tween._reversed) {
					tween.render((time - tween._startTime) * tween._timeScale, suppressEvents, force);
				} else {
					tween.render((!tween._dirty ? tween._totalDuration : tween.totalDuration()) - (time - tween._startTime) * tween._timeScale, suppressEvents, force);
				}
			}
			tween = next;
		}
	};

	p.rawTime = function () {
		if (!_tickerActive) {
			_ticker.wake();
		}
		return this._totalTime;
	};

	/*
  * ----------------------------------------------------------------
  * TweenLite
  * ----------------------------------------------------------------
  */
	var TweenLite = _class("TweenLite", function (target, duration, vars) {
		Animation.call(this, duration, vars);
		this.render = TweenLite.prototype.render; //speed optimization (avoid prototype lookup on this "hot" method)

		if (target == null) {
			throw "Cannot tween a null target.";
		}

		this.target = target = typeof target !== "string" ? target : TweenLite.selector(target) || target;

		var isSelector = target.jquery || target.length && target !== window && target[0] && (target[0] === window || target[0].nodeType && target[0].style && !target.nodeType),
		    overwrite = this.vars.overwrite,
		    i,
		    targ,
		    targets;

		this._overwrite = overwrite = overwrite == null ? _overwriteLookup[TweenLite.defaultOverwrite] : typeof overwrite === "number" ? overwrite >> 0 : _overwriteLookup[overwrite];

		if ((isSelector || target instanceof Array || target.push && _isArray(target)) && typeof target[0] !== "number") {
			this._targets = targets = _slice(target); //don't use Array.prototype.slice.call(target, 0) because that doesn't work in IE8 with a NodeList that's returned by querySelectorAll()
			this._propLookup = [];
			this._siblings = [];
			for (i = 0; i < targets.length; i++) {
				targ = targets[i];
				if (!targ) {
					targets.splice(i--, 1);
					continue;
				} else if (typeof targ === "string") {
					targ = targets[i--] = TweenLite.selector(targ); //in case it's an array of strings
					if (typeof targ === "string") {
						targets.splice(i + 1, 1); //to avoid an endless loop (can't imagine why the selector would return a string, but just in case)
					}
					continue;
				} else if (targ.length && targ !== window && targ[0] && (targ[0] === window || targ[0].nodeType && targ[0].style && !targ.nodeType)) {
					//in case the user is passing in an array of selector objects (like jQuery objects), we need to check one more level and pull things out if necessary. Also note that <select> elements pass all the criteria regarding length and the first child having style, so we must also check to ensure the target isn't an HTML node itself.
					targets.splice(i--, 1);
					this._targets = targets = targets.concat(_slice(targ));
					continue;
				}
				this._siblings[i] = _register(targ, this, false);
				if (overwrite === 1) if (this._siblings[i].length > 1) {
					_applyOverwrite(targ, this, null, 1, this._siblings[i]);
				}
			}
		} else {
			this._propLookup = {};
			this._siblings = _register(target, this, false);
			if (overwrite === 1) if (this._siblings.length > 1) {
				_applyOverwrite(target, this, null, 1, this._siblings);
			}
		}
		if (this.vars.immediateRender || duration === 0 && this._delay === 0 && this.vars.immediateRender !== false) {
			this._time = -_tinyNum; //forces a render without having to set the render() "force" parameter to true because we want to allow lazying by default (using the "force" parameter always forces an immediate full render)
			this.render(Math.min(0, -this._delay)); //in case delay is negative
		}
	}, true),
	    _isSelector = function _isSelector(v) {
		return v && v.length && v !== window && v[0] && (v[0] === window || v[0].nodeType && v[0].style && !v.nodeType); //we cannot check "nodeType" if the target is window from within an iframe, otherwise it will trigger a security error in some browsers like Firefox.
	},
	    _autoCSS = function _autoCSS(vars, target) {
		var css = {},
		    p;
		for (p in vars) {
			if (!_reservedProps[p] && (!(p in target) || p === "transform" || p === "x" || p === "y" || p === "width" || p === "height" || p === "className" || p === "border") && (!_plugins[p] || _plugins[p] && _plugins[p]._autoCSS)) {
				//note: <img> elements contain read-only "x" and "y" properties. We should also prioritize editing css width/height rather than the element's properties.
				css[p] = vars[p];
				delete vars[p];
			}
		}
		vars.css = css;
	};

	p = TweenLite.prototype = new Animation();
	p.constructor = TweenLite;
	p.kill()._gc = false;

	//----TweenLite defaults, overwrite management, and root updates ----------------------------------------------------

	p.ratio = 0;
	p._firstPT = p._targets = p._overwrittenProps = p._startAt = null;
	p._notifyPluginsOfEnabled = p._lazy = false;

	TweenLite.version = "1.20.3";
	TweenLite.defaultEase = p._ease = new Ease(null, null, 1, 1);
	TweenLite.defaultOverwrite = "auto";
	TweenLite.ticker = _ticker;
	TweenLite.autoSleep = 120;
	TweenLite.lagSmoothing = function (threshold, adjustedLag) {
		_ticker.lagSmoothing(threshold, adjustedLag);
	};

	TweenLite.selector = window.$ || window.jQuery || function (e) {
		var selector = window.$ || window.jQuery;
		if (selector) {
			TweenLite.selector = selector;
			return selector(e);
		}
		return typeof _doc === "undefined" ? e : _doc.querySelectorAll ? _doc.querySelectorAll(e) : _doc.getElementById(e.charAt(0) === "#" ? e.substr(1) : e);
	};

	var _lazyTweens = [],
	    _lazyLookup = {},
	    _numbersExp = /(?:(-|-=|\+=)?\d*\.?\d*(?:e[\-+]?\d+)?)[0-9]/ig,
	    _relExp = /[\+-]=-?[\.\d]/,

	//_nonNumbersExp = /(?:([\-+](?!(\d|=)))|[^\d\-+=e]|(e(?![\-+][\d])))+/ig,
	_setRatio = function _setRatio(v) {
		var pt = this._firstPT,
		    min = 0.000001,
		    val;
		while (pt) {
			val = !pt.blob ? pt.c * v + pt.s : v === 1 && this.end != null ? this.end : v ? this.join("") : this.start;
			if (pt.m) {
				val = pt.m(val, this._target || pt.t);
			} else if (val < min) if (val > -min && !pt.blob) {
				//prevents issues with converting very small numbers to strings in the browser
				val = 0;
			}
			if (!pt.f) {
				pt.t[pt.p] = val;
			} else if (pt.fp) {
				pt.t[pt.p](pt.fp, val);
			} else {
				pt.t[pt.p](val);
			}
			pt = pt._next;
		}
	},

	//compares two strings (start/end), finds the numbers that are different and spits back an array representing the whole value but with the changing values isolated as elements. For example, "rgb(0,0,0)" and "rgb(100,50,0)" would become ["rgb(", 0, ",", 50, ",0)"]. Notice it merges the parts that are identical (performance optimization). The array also has a linked list of PropTweens attached starting with _firstPT that contain the tweening data (t, p, s, c, f, etc.). It also stores the starting value as a "start" property so that we can revert to it if/when necessary, like when a tween rewinds fully. If the quantity of numbers differs between the start and end, it will always prioritize the end value(s). The pt parameter is optional - it's for a PropTween that will be appended to the end of the linked list and is typically for actually setting the value after all of the elements have been updated (with array.join("")).
	_blobDif = function _blobDif(start, end, filter, pt) {
		var a = [],
		    charIndex = 0,
		    s = "",
		    color = 0,
		    startNums,
		    endNums,
		    num,
		    i,
		    l,
		    nonNumbers,
		    currentNum;
		a.start = start;
		a.end = end;
		start = a[0] = start + ""; //ensure values are strings
		end = a[1] = end + "";
		if (filter) {
			filter(a); //pass an array with the starting and ending values and let the filter do whatever it needs to the values.
			start = a[0];
			end = a[1];
		}
		a.length = 0;
		startNums = start.match(_numbersExp) || [];
		endNums = end.match(_numbersExp) || [];
		if (pt) {
			pt._next = null;
			pt.blob = 1;
			a._firstPT = a._applyPT = pt; //apply last in the linked list (which means inserting it first)
		}
		l = endNums.length;
		for (i = 0; i < l; i++) {
			currentNum = endNums[i];
			nonNumbers = end.substr(charIndex, end.indexOf(currentNum, charIndex) - charIndex);
			s += nonNumbers || !i ? nonNumbers : ","; //note: SVG spec allows omission of comma/space when a negative sign is wedged between two numbers, like 2.5-5.3 instead of 2.5,-5.3 but when tweening, the negative value may switch to positive, so we insert the comma just in case.
			charIndex += nonNumbers.length;
			if (color) {
				//sense rgba() values and round them.
				color = (color + 1) % 5;
			} else if (nonNumbers.substr(-5) === "rgba(") {
				color = 1;
			}
			if (currentNum === startNums[i] || startNums.length <= i) {
				s += currentNum;
			} else {
				if (s) {
					a.push(s);
					s = "";
				}
				num = parseFloat(startNums[i]);
				a.push(num);
				a._firstPT = { _next: a._firstPT, t: a, p: a.length - 1, s: num, c: (currentNum.charAt(1) === "=" ? parseInt(currentNum.charAt(0) + "1", 10) * parseFloat(currentNum.substr(2)) : parseFloat(currentNum) - num) || 0, f: 0, m: color && color < 4 ? Math.round : 0 };
				//note: we don't set _prev because we'll never need to remove individual PropTweens from this list.
			}
			charIndex += currentNum.length;
		}
		s += end.substr(charIndex);
		if (s) {
			a.push(s);
		}
		a.setRatio = _setRatio;
		if (_relExp.test(end)) {
			//if the end string contains relative values, delete it so that on the final render (in _setRatio()), we don't actually set it to the string with += or -= characters (forces it to use the calculated value).
			a.end = null;
		}
		return a;
	},

	//note: "funcParam" is only necessary for function-based getters/setters that require an extra parameter like getAttribute("width") and setAttribute("width", value). In this example, funcParam would be "width". Used by AttrPlugin for example.
	_addPropTween = function _addPropTween(target, prop, start, end, overwriteProp, mod, funcParam, stringFilter, index) {
		if (typeof end === "function") {
			end = end(index || 0, target);
		}
		var type = _typeof(target[prop]),
		    getterName = type !== "function" ? "" : prop.indexOf("set") || typeof target["get" + prop.substr(3)] !== "function" ? prop : "get" + prop.substr(3),
		    s = start !== "get" ? start : !getterName ? target[prop] : funcParam ? target[getterName](funcParam) : target[getterName](),
		    isRelative = typeof end === "string" && end.charAt(1) === "=",
		    pt = { t: target, p: prop, s: s, f: type === "function", pg: 0, n: overwriteProp || prop, m: !mod ? 0 : typeof mod === "function" ? mod : Math.round, pr: 0, c: isRelative ? parseInt(end.charAt(0) + "1", 10) * parseFloat(end.substr(2)) : parseFloat(end) - s || 0 },
		    blob;

		if (typeof s !== "number" || typeof end !== "number" && !isRelative) {
			if (funcParam || isNaN(s) || !isRelative && isNaN(end) || typeof s === "boolean" || typeof end === "boolean") {
				//a blob (string that has multiple numbers in it)
				pt.fp = funcParam;
				blob = _blobDif(s, isRelative ? parseFloat(pt.s) + pt.c : end, stringFilter || TweenLite.defaultStringFilter, pt);
				pt = { t: blob, p: "setRatio", s: 0, c: 1, f: 2, pg: 0, n: overwriteProp || prop, pr: 0, m: 0 }; //"2" indicates it's a Blob property tween. Needed for RoundPropsPlugin for example.
			} else {
				pt.s = parseFloat(s);
				if (!isRelative) {
					pt.c = parseFloat(end) - pt.s || 0;
				}
			}
		}
		if (pt.c) {
			//only add it to the linked list if there's a change.
			if (pt._next = this._firstPT) {
				pt._next._prev = pt;
			}
			this._firstPT = pt;
			return pt;
		}
	},
	    _internals = TweenLite._internals = { isArray: _isArray, isSelector: _isSelector, lazyTweens: _lazyTweens, blobDif: _blobDif },
	    //gives us a way to expose certain private values to other GreenSock classes without contaminating tha main TweenLite object.
	_plugins = TweenLite._plugins = {},
	    _tweenLookup = _internals.tweenLookup = {},
	    _tweenLookupNum = 0,
	    _reservedProps = _internals.reservedProps = { ease: 1, delay: 1, overwrite: 1, onComplete: 1, onCompleteParams: 1, onCompleteScope: 1, useFrames: 1, runBackwards: 1, startAt: 1, onUpdate: 1, onUpdateParams: 1, onUpdateScope: 1, onStart: 1, onStartParams: 1, onStartScope: 1, onReverseComplete: 1, onReverseCompleteParams: 1, onReverseCompleteScope: 1, onRepeat: 1, onRepeatParams: 1, onRepeatScope: 1, easeParams: 1, yoyo: 1, immediateRender: 1, repeat: 1, repeatDelay: 1, data: 1, paused: 1, reversed: 1, autoCSS: 1, lazy: 1, onOverwrite: 1, callbackScope: 1, stringFilter: 1, id: 1, yoyoEase: 1 },
	    _overwriteLookup = { none: 0, all: 1, auto: 2, concurrent: 3, allOnStart: 4, preexisting: 5, "true": 1, "false": 0 },
	    _rootFramesTimeline = Animation._rootFramesTimeline = new SimpleTimeline(),
	    _rootTimeline = Animation._rootTimeline = new SimpleTimeline(),
	    _nextGCFrame = 30,
	    _lazyRender = _internals.lazyRender = function () {
		var i = _lazyTweens.length,
		    tween;
		_lazyLookup = {};
		while (--i > -1) {
			tween = _lazyTweens[i];
			if (tween && tween._lazy !== false) {
				tween.render(tween._lazy[0], tween._lazy[1], true);
				tween._lazy = false;
			}
		}
		_lazyTweens.length = 0;
	};

	_rootTimeline._startTime = _ticker.time;
	_rootFramesTimeline._startTime = _ticker.frame;
	_rootTimeline._active = _rootFramesTimeline._active = true;
	setTimeout(_lazyRender, 1); //on some mobile devices, there isn't a "tick" before code runs which means any lazy renders wouldn't run before the next official "tick".

	Animation._updateRoot = TweenLite.render = function () {
		var i, a, p;
		if (_lazyTweens.length) {
			//if code is run outside of the requestAnimationFrame loop, there may be tweens queued AFTER the engine refreshed, so we need to ensure any pending renders occur before we refresh again.
			_lazyRender();
		}
		_rootTimeline.render((_ticker.time - _rootTimeline._startTime) * _rootTimeline._timeScale, false, false);
		_rootFramesTimeline.render((_ticker.frame - _rootFramesTimeline._startTime) * _rootFramesTimeline._timeScale, false, false);
		if (_lazyTweens.length) {
			_lazyRender();
		}
		if (_ticker.frame >= _nextGCFrame) {
			//dump garbage every 120 frames or whatever the user sets TweenLite.autoSleep to
			_nextGCFrame = _ticker.frame + (parseInt(TweenLite.autoSleep, 10) || 120);
			for (p in _tweenLookup) {
				a = _tweenLookup[p].tweens;
				i = a.length;
				while (--i > -1) {
					if (a[i]._gc) {
						a.splice(i, 1);
					}
				}
				if (a.length === 0) {
					delete _tweenLookup[p];
				}
			}
			//if there are no more tweens in the root timelines, or if they're all paused, make the _timer sleep to reduce load on the CPU slightly
			p = _rootTimeline._first;
			if (!p || p._paused) if (TweenLite.autoSleep && !_rootFramesTimeline._first && _ticker._listeners.tick.length === 1) {
				while (p && p._paused) {
					p = p._next;
				}
				if (!p) {
					_ticker.sleep();
				}
			}
		}
	};

	_ticker.addEventListener("tick", Animation._updateRoot);

	var _register = function _register(target, tween, scrub) {
		var id = target._gsTweenID,
		    a,
		    i;
		if (!_tweenLookup[id || (target._gsTweenID = id = "t" + _tweenLookupNum++)]) {
			_tweenLookup[id] = { target: target, tweens: [] };
		}
		if (tween) {
			a = _tweenLookup[id].tweens;
			a[i = a.length] = tween;
			if (scrub) {
				while (--i > -1) {
					if (a[i] === tween) {
						a.splice(i, 1);
					}
				}
			}
		}
		return _tweenLookup[id].tweens;
	},
	    _onOverwrite = function _onOverwrite(overwrittenTween, overwritingTween, target, killedProps) {
		var func = overwrittenTween.vars.onOverwrite,
		    r1,
		    r2;
		if (func) {
			r1 = func(overwrittenTween, overwritingTween, target, killedProps);
		}
		func = TweenLite.onOverwrite;
		if (func) {
			r2 = func(overwrittenTween, overwritingTween, target, killedProps);
		}
		return r1 !== false && r2 !== false;
	},
	    _applyOverwrite = function _applyOverwrite(target, tween, props, mode, siblings) {
		var i, changed, curTween, l;
		if (mode === 1 || mode >= 4) {
			l = siblings.length;
			for (i = 0; i < l; i++) {
				if ((curTween = siblings[i]) !== tween) {
					if (!curTween._gc) {
						if (curTween._kill(null, target, tween)) {
							changed = true;
						}
					}
				} else if (mode === 5) {
					break;
				}
			}
			return changed;
		}
		//NOTE: Add 0.0000000001 to overcome floating point errors that can cause the startTime to be VERY slightly off (when a tween's time() is set for example)
		var startTime = tween._startTime + _tinyNum,
		    overlaps = [],
		    oCount = 0,
		    zeroDur = tween._duration === 0,
		    globalStart;
		i = siblings.length;
		while (--i > -1) {
			if ((curTween = siblings[i]) === tween || curTween._gc || curTween._paused) {
				//ignore
			} else if (curTween._timeline !== tween._timeline) {
				globalStart = globalStart || _checkOverlap(tween, 0, zeroDur);
				if (_checkOverlap(curTween, globalStart, zeroDur) === 0) {
					overlaps[oCount++] = curTween;
				}
			} else if (curTween._startTime <= startTime) if (curTween._startTime + curTween.totalDuration() / curTween._timeScale > startTime) if (!((zeroDur || !curTween._initted) && startTime - curTween._startTime <= 0.0000000002)) {
				overlaps[oCount++] = curTween;
			}
		}

		i = oCount;
		while (--i > -1) {
			curTween = overlaps[i];
			if (mode === 2) if (curTween._kill(props, target, tween)) {
				changed = true;
			}
			if (mode !== 2 || !curTween._firstPT && curTween._initted) {
				if (mode !== 2 && !_onOverwrite(curTween, tween)) {
					continue;
				}
				if (curTween._enabled(false, false)) {
					//if all property tweens have been overwritten, kill the tween.
					changed = true;
				}
			}
		}
		return changed;
	},
	    _checkOverlap = function _checkOverlap(tween, reference, zeroDur) {
		var tl = tween._timeline,
		    ts = tl._timeScale,
		    t = tween._startTime;
		while (tl._timeline) {
			t += tl._startTime;
			ts *= tl._timeScale;
			if (tl._paused) {
				return -100;
			}
			tl = tl._timeline;
		}
		t /= ts;
		return t > reference ? t - reference : zeroDur && t === reference || !tween._initted && t - reference < 2 * _tinyNum ? _tinyNum : (t += tween.totalDuration() / tween._timeScale / ts) > reference + _tinyNum ? 0 : t - reference - _tinyNum;
	};

	//---- TweenLite instance methods -----------------------------------------------------------------------------

	p._init = function () {
		var v = this.vars,
		    op = this._overwrittenProps,
		    dur = this._duration,
		    immediate = !!v.immediateRender,
		    ease = v.ease,
		    i,
		    initPlugins,
		    pt,
		    p,
		    startVars,
		    l;
		if (v.startAt) {
			if (this._startAt) {
				this._startAt.render(-1, true); //if we've run a startAt previously (when the tween instantiated), we should revert it so that the values re-instantiate correctly particularly for relative tweens. Without this, a TweenLite.fromTo(obj, 1, {x:"+=100"}, {x:"-=100"}), for example, would actually jump to +=200 because the startAt would run twice, doubling the relative change.
				this._startAt.kill();
			}
			startVars = {};
			for (p in v.startAt) {
				//copy the properties/values into a new object to avoid collisions, like var to = {x:0}, from = {x:500}; timeline.fromTo(e, 1, from, to).fromTo(e, 1, to, from);
				startVars[p] = v.startAt[p];
			}
			startVars.data = "isStart";
			startVars.overwrite = false;
			startVars.immediateRender = true;
			startVars.lazy = immediate && v.lazy !== false;
			startVars.startAt = startVars.delay = null; //no nesting of startAt objects allowed (otherwise it could cause an infinite loop).
			startVars.onUpdate = v.onUpdate;
			startVars.onUpdateParams = v.onUpdateParams;
			startVars.onUpdateScope = v.onUpdateScope || v.callbackScope || this;
			this._startAt = TweenLite.to(this.target, 0, startVars);
			if (immediate) {
				if (this._time > 0) {
					this._startAt = null; //tweens that render immediately (like most from() and fromTo() tweens) shouldn't revert when their parent timeline's playhead goes backward past the startTime because the initial render could have happened anytime and it shouldn't be directly correlated to this tween's startTime. Imagine setting up a complex animation where the beginning states of various objects are rendered immediately but the tween doesn't happen for quite some time - if we revert to the starting values as soon as the playhead goes backward past the tween's startTime, it will throw things off visually. Reversion should only happen in TimelineLite/Max instances where immediateRender was false (which is the default in the convenience methods like from()).
				} else if (dur !== 0) {
					return; //we skip initialization here so that overwriting doesn't occur until the tween actually begins. Otherwise, if you create several immediateRender:true tweens of the same target/properties to drop into a TimelineLite or TimelineMax, the last one created would overwrite the first ones because they didn't get placed into the timeline yet before the first render occurs and kicks in overwriting.
				}
			}
		} else if (v.runBackwards && dur !== 0) {
			//from() tweens must be handled uniquely: their beginning values must be rendered but we don't want overwriting to occur yet (when time is still 0). Wait until the tween actually begins before doing all the routines like overwriting. At that time, we should render at the END of the tween to ensure that things initialize correctly (remember, from() tweens go backwards)
			if (this._startAt) {
				this._startAt.render(-1, true);
				this._startAt.kill();
				this._startAt = null;
			} else {
				if (this._time !== 0) {
					//in rare cases (like if a from() tween runs and then is invalidate()-ed), immediateRender could be true but the initial forced-render gets skipped, so there's no need to force the render in this context when the _time is greater than 0
					immediate = false;
				}
				pt = {};
				for (p in v) {
					//copy props into a new object and skip any reserved props, otherwise onComplete or onUpdate or onStart could fire. We should, however, permit autoCSS to go through.
					if (!_reservedProps[p] || p === "autoCSS") {
						pt[p] = v[p];
					}
				}
				pt.overwrite = 0;
				pt.data = "isFromStart"; //we tag the tween with as "isFromStart" so that if [inside a plugin] we need to only do something at the very END of a tween, we have a way of identifying this tween as merely the one that's setting the beginning values for a "from()" tween. For example, clearProps in CSSPlugin should only get applied at the very END of a tween and without this tag, from(...{height:100, clearProps:"height", delay:1}) would wipe the height at the beginning of the tween and after 1 second, it'd kick back in.
				pt.lazy = immediate && v.lazy !== false;
				pt.immediateRender = immediate; //zero-duration tweens render immediately by default, but if we're not specifically instructed to render this tween immediately, we should skip this and merely _init() to record the starting values (rendering them immediately would push them to completion which is wasteful in that case - we'd have to render(-1) immediately after)
				this._startAt = TweenLite.to(this.target, 0, pt);
				if (!immediate) {
					this._startAt._init(); //ensures that the initial values are recorded
					this._startAt._enabled(false); //no need to have the tween render on the next cycle. Disable it because we'll always manually control the renders of the _startAt tween.
					if (this.vars.immediateRender) {
						this._startAt = null;
					}
				} else if (this._time === 0) {
					return;
				}
			}
		}
		this._ease = ease = !ease ? TweenLite.defaultEase : ease instanceof Ease ? ease : typeof ease === "function" ? new Ease(ease, v.easeParams) : _easeMap[ease] || TweenLite.defaultEase;
		if (v.easeParams instanceof Array && ease.config) {
			this._ease = ease.config.apply(ease, v.easeParams);
		}
		this._easeType = this._ease._type;
		this._easePower = this._ease._power;
		this._firstPT = null;

		if (this._targets) {
			l = this._targets.length;
			for (i = 0; i < l; i++) {
				if (this._initProps(this._targets[i], this._propLookup[i] = {}, this._siblings[i], op ? op[i] : null, i)) {
					initPlugins = true;
				}
			}
		} else {
			initPlugins = this._initProps(this.target, this._propLookup, this._siblings, op, 0);
		}

		if (initPlugins) {
			TweenLite._onPluginEvent("_onInitAllProps", this); //reorders the array in order of priority. Uses a static TweenPlugin method in order to minimize file size in TweenLite
		}
		if (op) if (!this._firstPT) if (typeof this.target !== "function") {
			//if all tweening properties have been overwritten, kill the tween. If the target is a function, it's probably a delayedCall so let it live.
			this._enabled(false, false);
		}
		if (v.runBackwards) {
			pt = this._firstPT;
			while (pt) {
				pt.s += pt.c;
				pt.c = -pt.c;
				pt = pt._next;
			}
		}
		this._onUpdate = v.onUpdate;
		this._initted = true;
	};

	p._initProps = function (target, propLookup, siblings, overwrittenProps, index) {
		var p, i, initPlugins, plugin, pt, v;
		if (target == null) {
			return false;
		}

		if (_lazyLookup[target._gsTweenID]) {
			_lazyRender(); //if other tweens of the same target have recently initted but haven't rendered yet, we've got to force the render so that the starting values are correct (imagine populating a timeline with a bunch of sequential tweens and then jumping to the end)
		}

		if (!this.vars.css) if (target.style) if (target !== window && target.nodeType) if (_plugins.css) if (this.vars.autoCSS !== false) {
			//it's so common to use TweenLite/Max to animate the css of DOM elements, we assume that if the target is a DOM element, that's what is intended (a convenience so that users don't have to wrap things in css:{}, although we still recommend it for a slight performance boost and better specificity). Note: we cannot check "nodeType" on the window inside an iframe.
			_autoCSS(this.vars, target);
		}
		for (p in this.vars) {
			v = this.vars[p];
			if (_reservedProps[p]) {
				if (v) if (v instanceof Array || v.push && _isArray(v)) if (v.join("").indexOf("{self}") !== -1) {
					this.vars[p] = v = this._swapSelfInParams(v, this);
				}
			} else if (_plugins[p] && (plugin = new _plugins[p]())._onInitTween(target, this.vars[p], this, index)) {

				//t - target 		[object]
				//p - property 		[string]
				//s - start			[number]
				//c - change		[number]
				//f - isFunction	[boolean]
				//n - name			[string]
				//pg - isPlugin 	[boolean]
				//pr - priority		[number]
				//m - mod           [function | 0]
				this._firstPT = pt = { _next: this._firstPT, t: plugin, p: "setRatio", s: 0, c: 1, f: 1, n: p, pg: 1, pr: plugin._priority, m: 0 };
				i = plugin._overwriteProps.length;
				while (--i > -1) {
					propLookup[plugin._overwriteProps[i]] = this._firstPT;
				}
				if (plugin._priority || plugin._onInitAllProps) {
					initPlugins = true;
				}
				if (plugin._onDisable || plugin._onEnable) {
					this._notifyPluginsOfEnabled = true;
				}
				if (pt._next) {
					pt._next._prev = pt;
				}
			} else {
				propLookup[p] = _addPropTween.call(this, target, p, "get", v, p, 0, null, this.vars.stringFilter, index);
			}
		}

		if (overwrittenProps) if (this._kill(overwrittenProps, target)) {
			//another tween may have tried to overwrite properties of this tween before init() was called (like if two tweens start at the same time, the one created second will run first)
			return this._initProps(target, propLookup, siblings, overwrittenProps, index);
		}
		if (this._overwrite > 1) if (this._firstPT) if (siblings.length > 1) if (_applyOverwrite(target, this, propLookup, this._overwrite, siblings)) {
			this._kill(propLookup, target);
			return this._initProps(target, propLookup, siblings, overwrittenProps, index);
		}
		if (this._firstPT) if (this.vars.lazy !== false && this._duration || this.vars.lazy && !this._duration) {
			//zero duration tweens don't lazy render by default; everything else does.
			_lazyLookup[target._gsTweenID] = true;
		}
		return initPlugins;
	};

	p.render = function (time, suppressEvents, force) {
		var prevTime = this._time,
		    duration = this._duration,
		    prevRawPrevTime = this._rawPrevTime,
		    isComplete,
		    callback,
		    pt,
		    rawPrevTime;
		if (time >= duration - 0.0000001 && time >= 0) {
			//to work around occasional floating point math artifacts.
			this._totalTime = this._time = duration;
			this.ratio = this._ease._calcEnd ? this._ease.getRatio(1) : 1;
			if (!this._reversed) {
				isComplete = true;
				callback = "onComplete";
				force = force || this._timeline.autoRemoveChildren; //otherwise, if the animation is unpaused/activated after it's already finished, it doesn't get removed from the parent timeline.
			}
			if (duration === 0) if (this._initted || !this.vars.lazy || force) {
				//zero-duration tweens are tricky because we must discern the momentum/direction of time in order to determine whether the starting values should be rendered or the ending values. If the "playhead" of its timeline goes past the zero-duration tween in the forward direction or lands directly on it, the end values should be rendered, but if the timeline's "playhead" moves past it in the backward direction (from a postitive time to a negative time), the starting values must be rendered.
				if (this._startTime === this._timeline._duration) {
					//if a zero-duration tween is at the VERY end of a timeline and that timeline renders at its end, it will typically add a tiny bit of cushion to the render time to prevent rounding errors from getting in the way of tweens rendering their VERY end. If we then reverse() that timeline, the zero-duration tween will trigger its onReverseComplete even though technically the playhead didn't pass over it again. It's a very specific edge case we must accommodate.
					time = 0;
				}
				if (prevRawPrevTime < 0 || time <= 0 && time >= -0.0000001 || prevRawPrevTime === _tinyNum && this.data !== "isPause") if (prevRawPrevTime !== time) {
					//note: when this.data is "isPause", it's a callback added by addPause() on a timeline that we should not be triggered when LEAVING its exact start time. In other words, tl.addPause(1).play(1) shouldn't pause.
					force = true;
					if (prevRawPrevTime > _tinyNum) {
						callback = "onReverseComplete";
					}
				}
				this._rawPrevTime = rawPrevTime = !suppressEvents || time || prevRawPrevTime === time ? time : _tinyNum; //when the playhead arrives at EXACTLY time 0 (right on top) of a zero-duration tween, we need to discern if events are suppressed so that when the playhead moves again (next time), it'll trigger the callback. If events are NOT suppressed, obviously the callback would be triggered in this render. Basically, the callback should fire either when the playhead ARRIVES or LEAVES this exact spot, not both. Imagine doing a timeline.seek(0) and there's a callback that sits at 0. Since events are suppressed on that seek() by default, nothing will fire, but when the playhead moves off of that position, the callback should fire. This behavior is what people intuitively expect. We set the _rawPrevTime to be a precise tiny number to indicate this scenario rather than using another property/variable which would increase memory usage. This technique is less readable, but more efficient.
			}
		} else if (time < 0.0000001) {
			//to work around occasional floating point math artifacts, round super small values to 0.
			this._totalTime = this._time = 0;
			this.ratio = this._ease._calcEnd ? this._ease.getRatio(0) : 0;
			if (prevTime !== 0 || duration === 0 && prevRawPrevTime > 0) {
				callback = "onReverseComplete";
				isComplete = this._reversed;
			}
			if (time < 0) {
				this._active = false;
				if (duration === 0) if (this._initted || !this.vars.lazy || force) {
					//zero-duration tweens are tricky because we must discern the momentum/direction of time in order to determine whether the starting values should be rendered or the ending values. If the "playhead" of its timeline goes past the zero-duration tween in the forward direction or lands directly on it, the end values should be rendered, but if the timeline's "playhead" moves past it in the backward direction (from a postitive time to a negative time), the starting values must be rendered.
					if (prevRawPrevTime >= 0 && !(prevRawPrevTime === _tinyNum && this.data === "isPause")) {
						force = true;
					}
					this._rawPrevTime = rawPrevTime = !suppressEvents || time || prevRawPrevTime === time ? time : _tinyNum; //when the playhead arrives at EXACTLY time 0 (right on top) of a zero-duration tween, we need to discern if events are suppressed so that when the playhead moves again (next time), it'll trigger the callback. If events are NOT suppressed, obviously the callback would be triggered in this render. Basically, the callback should fire either when the playhead ARRIVES or LEAVES this exact spot, not both. Imagine doing a timeline.seek(0) and there's a callback that sits at 0. Since events are suppressed on that seek() by default, nothing will fire, but when the playhead moves off of that position, the callback should fire. This behavior is what people intuitively expect. We set the _rawPrevTime to be a precise tiny number to indicate this scenario rather than using another property/variable which would increase memory usage. This technique is less readable, but more efficient.
				}
			}
			if (!this._initted || this._startAt && this._startAt.progress()) {
				//if we render the very beginning (time == 0) of a fromTo(), we must force the render (normal tweens wouldn't need to render at a time of 0 when the prevTime was also 0). This is also mandatory to make sure overwriting kicks in immediately. Also, we check progress() because if startAt has already rendered at its end, we should force a render at its beginning. Otherwise, if you put the playhead directly on top of where a fromTo({immediateRender:false}) starts, and then move it backwards, the from() won't revert its values.
				force = true;
			}
		} else {
			this._totalTime = this._time = time;

			if (this._easeType) {
				var r = time / duration,
				    type = this._easeType,
				    pow = this._easePower;
				if (type === 1 || type === 3 && r >= 0.5) {
					r = 1 - r;
				}
				if (type === 3) {
					r *= 2;
				}
				if (pow === 1) {
					r *= r;
				} else if (pow === 2) {
					r *= r * r;
				} else if (pow === 3) {
					r *= r * r * r;
				} else if (pow === 4) {
					r *= r * r * r * r;
				}

				if (type === 1) {
					this.ratio = 1 - r;
				} else if (type === 2) {
					this.ratio = r;
				} else if (time / duration < 0.5) {
					this.ratio = r / 2;
				} else {
					this.ratio = 1 - r / 2;
				}
			} else {
				this.ratio = this._ease.getRatio(time / duration);
			}
		}

		if (this._time === prevTime && !force) {
			return;
		} else if (!this._initted) {
			this._init();
			if (!this._initted || this._gc) {
				//immediateRender tweens typically won't initialize until the playhead advances (_time is greater than 0) in order to ensure that overwriting occurs properly. Also, if all of the tweening properties have been overwritten (which would cause _gc to be true, as set in _init()), we shouldn't continue otherwise an onStart callback could be called for example.
				return;
			} else if (!force && this._firstPT && (this.vars.lazy !== false && this._duration || this.vars.lazy && !this._duration)) {
				this._time = this._totalTime = prevTime;
				this._rawPrevTime = prevRawPrevTime;
				_lazyTweens.push(this);
				this._lazy = [time, suppressEvents];
				return;
			}
			//_ease is initially set to defaultEase, so now that init() has run, _ease is set properly and we need to recalculate the ratio. Overall this is faster than using conditional logic earlier in the method to avoid having to set ratio twice because we only init() once but renderTime() gets called VERY frequently.
			if (this._time && !isComplete) {
				this.ratio = this._ease.getRatio(this._time / duration);
			} else if (isComplete && this._ease._calcEnd) {
				this.ratio = this._ease.getRatio(this._time === 0 ? 0 : 1);
			}
		}
		if (this._lazy !== false) {
			//in case a lazy render is pending, we should flush it because the new render is occurring now (imagine a lazy tween instantiating and then immediately the user calls tween.seek(tween.duration()), skipping to the end - the end render would be forced, and then if we didn't flush the lazy render, it'd fire AFTER the seek(), rendering it at the wrong time.
			this._lazy = false;
		}
		if (!this._active) if (!this._paused && this._time !== prevTime && time >= 0) {
			this._active = true; //so that if the user renders a tween (as opposed to the timeline rendering it), the timeline is forced to re-render and align it with the proper time/frame on the next rendering cycle. Maybe the tween already finished but the user manually re-renders it as halfway done.
		}
		if (prevTime === 0) {
			if (this._startAt) {
				if (time >= 0) {
					this._startAt.render(time, true, force);
				} else if (!callback) {
					callback = "_dummyGS"; //if no callback is defined, use a dummy value just so that the condition at the end evaluates as true because _startAt should render AFTER the normal render loop when the time is negative. We could handle this in a more intuitive way, of course, but the render loop is the MOST important thing to optimize, so this technique allows us to avoid adding extra conditional logic in a high-frequency area.
				}
			}
			if (this.vars.onStart) if (this._time !== 0 || duration === 0) if (!suppressEvents) {
				this._callback("onStart");
			}
		}
		pt = this._firstPT;
		while (pt) {
			if (pt.f) {
				pt.t[pt.p](pt.c * this.ratio + pt.s);
			} else {
				pt.t[pt.p] = pt.c * this.ratio + pt.s;
			}
			pt = pt._next;
		}

		if (this._onUpdate) {
			if (time < 0) if (this._startAt && time !== -0.0001) {
				//if the tween is positioned at the VERY beginning (_startTime 0) of its parent timeline, it's illegal for the playhead to go back further, so we should not render the recorded startAt values.
				this._startAt.render(time, true, force); //note: for performance reasons, we tuck this conditional logic inside less traveled areas (most tweens don't have an onUpdate). We'd just have it at the end before the onComplete, but the values should be updated before any onUpdate is called, so we ALSO put it here and then if it's not called, we do so later near the onComplete.
			}
			if (!suppressEvents) if (this._time !== prevTime || isComplete || force) {
				this._callback("onUpdate");
			}
		}
		if (callback) if (!this._gc || force) {
			//check _gc because there's a chance that kill() could be called in an onUpdate
			if (time < 0 && this._startAt && !this._onUpdate && time !== -0.0001) {
				//-0.0001 is a special value that we use when looping back to the beginning of a repeated TimelineMax, in which case we shouldn't render the _startAt values.
				this._startAt.render(time, true, force);
			}
			if (isComplete) {
				if (this._timeline.autoRemoveChildren) {
					this._enabled(false, false);
				}
				this._active = false;
			}
			if (!suppressEvents && this.vars[callback]) {
				this._callback(callback);
			}
			if (duration === 0 && this._rawPrevTime === _tinyNum && rawPrevTime !== _tinyNum) {
				//the onComplete or onReverseComplete could trigger movement of the playhead and for zero-duration tweens (which must discern direction) that land directly back on their start time, we don't want to fire again on the next render. Think of several addPause()'s in a timeline that forces the playhead to a certain spot, but what if it's already paused and another tween is tweening the "time" of the timeline? Each time it moves [forward] past that spot, it would move back, and since suppressEvents is true, it'd reset _rawPrevTime to _tinyNum so that when it begins again, the callback would fire (so ultimately it could bounce back and forth during that tween). Again, this is a very uncommon scenario, but possible nonetheless.
				this._rawPrevTime = 0;
			}
		}
	};

	p._kill = function (vars, target, overwritingTween) {
		if (vars === "all") {
			vars = null;
		}
		if (vars == null) if (target == null || target === this.target) {
			this._lazy = false;
			return this._enabled(false, false);
		}
		target = typeof target !== "string" ? target || this._targets || this.target : TweenLite.selector(target) || target;
		var simultaneousOverwrite = overwritingTween && this._time && overwritingTween._startTime === this._startTime && this._timeline === overwritingTween._timeline,
		    i,
		    overwrittenProps,
		    p,
		    pt,
		    propLookup,
		    changed,
		    killProps,
		    record,
		    killed;
		if ((_isArray(target) || _isSelector(target)) && typeof target[0] !== "number") {
			i = target.length;
			while (--i > -1) {
				if (this._kill(vars, target[i], overwritingTween)) {
					changed = true;
				}
			}
		} else {
			if (this._targets) {
				i = this._targets.length;
				while (--i > -1) {
					if (target === this._targets[i]) {
						propLookup = this._propLookup[i] || {};
						this._overwrittenProps = this._overwrittenProps || [];
						overwrittenProps = this._overwrittenProps[i] = vars ? this._overwrittenProps[i] || {} : "all";
						break;
					}
				}
			} else if (target !== this.target) {
				return false;
			} else {
				propLookup = this._propLookup;
				overwrittenProps = this._overwrittenProps = vars ? this._overwrittenProps || {} : "all";
			}

			if (propLookup) {
				killProps = vars || propLookup;
				record = vars !== overwrittenProps && overwrittenProps !== "all" && vars !== propLookup && ((typeof vars === "undefined" ? "undefined" : _typeof(vars)) !== "object" || !vars._tempKill); //_tempKill is a super-secret way to delete a particular tweening property but NOT have it remembered as an official overwritten property (like in BezierPlugin)
				if (overwritingTween && (TweenLite.onOverwrite || this.vars.onOverwrite)) {
					for (p in killProps) {
						if (propLookup[p]) {
							if (!killed) {
								killed = [];
							}
							killed.push(p);
						}
					}
					if ((killed || !vars) && !_onOverwrite(this, overwritingTween, target, killed)) {
						//if the onOverwrite returned false, that means the user wants to override the overwriting (cancel it).
						return false;
					}
				}

				for (p in killProps) {
					if (pt = propLookup[p]) {
						if (simultaneousOverwrite) {
							//if another tween overwrites this one and they both start at exactly the same time, yet this tween has already rendered once (for example, at 0.001) because it's first in the queue, we should revert the values to where they were at 0 so that the starting values aren't contaminated on the overwriting tween.
							if (pt.f) {
								pt.t[pt.p](pt.s);
							} else {
								pt.t[pt.p] = pt.s;
							}
							changed = true;
						}
						if (pt.pg && pt.t._kill(killProps)) {
							changed = true; //some plugins need to be notified so they can perform cleanup tasks first
						}
						if (!pt.pg || pt.t._overwriteProps.length === 0) {
							if (pt._prev) {
								pt._prev._next = pt._next;
							} else if (pt === this._firstPT) {
								this._firstPT = pt._next;
							}
							if (pt._next) {
								pt._next._prev = pt._prev;
							}
							pt._next = pt._prev = null;
						}
						delete propLookup[p];
					}
					if (record) {
						overwrittenProps[p] = 1;
					}
				}
				if (!this._firstPT && this._initted) {
					//if all tweening properties are killed, kill the tween. Without this line, if there's a tween with multiple targets and then you killTweensOf() each target individually, the tween would technically still remain active and fire its onComplete even though there aren't any more properties tweening.
					this._enabled(false, false);
				}
			}
		}
		return changed;
	};

	p.invalidate = function () {
		if (this._notifyPluginsOfEnabled) {
			TweenLite._onPluginEvent("_onDisable", this);
		}
		this._firstPT = this._overwrittenProps = this._startAt = this._onUpdate = null;
		this._notifyPluginsOfEnabled = this._active = this._lazy = false;
		this._propLookup = this._targets ? {} : [];
		Animation.prototype.invalidate.call(this);
		if (this.vars.immediateRender) {
			this._time = -_tinyNum; //forces a render without having to set the render() "force" parameter to true because we want to allow lazying by default (using the "force" parameter always forces an immediate full render)
			this.render(Math.min(0, -this._delay)); //in case delay is negative.
		}
		return this;
	};

	p._enabled = function (enabled, ignoreTimeline) {
		if (!_tickerActive) {
			_ticker.wake();
		}
		if (enabled && this._gc) {
			var targets = this._targets,
			    i;
			if (targets) {
				i = targets.length;
				while (--i > -1) {
					this._siblings[i] = _register(targets[i], this, true);
				}
			} else {
				this._siblings = _register(this.target, this, true);
			}
		}
		Animation.prototype._enabled.call(this, enabled, ignoreTimeline);
		if (this._notifyPluginsOfEnabled) if (this._firstPT) {
			return TweenLite._onPluginEvent(enabled ? "_onEnable" : "_onDisable", this);
		}
		return false;
	};

	//----TweenLite static methods -----------------------------------------------------

	TweenLite.to = function (target, duration, vars) {
		return new TweenLite(target, duration, vars);
	};

	TweenLite.from = function (target, duration, vars) {
		vars.runBackwards = true;
		vars.immediateRender = vars.immediateRender != false;
		return new TweenLite(target, duration, vars);
	};

	TweenLite.fromTo = function (target, duration, fromVars, toVars) {
		toVars.startAt = fromVars;
		toVars.immediateRender = toVars.immediateRender != false && fromVars.immediateRender != false;
		return new TweenLite(target, duration, toVars);
	};

	TweenLite.delayedCall = function (delay, callback, params, scope, useFrames) {
		return new TweenLite(callback, 0, { delay: delay, onComplete: callback, onCompleteParams: params, callbackScope: scope, onReverseComplete: callback, onReverseCompleteParams: params, immediateRender: false, lazy: false, useFrames: useFrames, overwrite: 0 });
	};

	TweenLite.set = function (target, vars) {
		return new TweenLite(target, 0, vars);
	};

	TweenLite.getTweensOf = function (target, onlyActive) {
		if (target == null) {
			return [];
		}
		target = typeof target !== "string" ? target : TweenLite.selector(target) || target;
		var i, a, j, t;
		if ((_isArray(target) || _isSelector(target)) && typeof target[0] !== "number") {
			i = target.length;
			a = [];
			while (--i > -1) {
				a = a.concat(TweenLite.getTweensOf(target[i], onlyActive));
			}
			i = a.length;
			//now get rid of any duplicates (tweens of arrays of objects could cause duplicates)
			while (--i > -1) {
				t = a[i];
				j = i;
				while (--j > -1) {
					if (t === a[j]) {
						a.splice(i, 1);
					}
				}
			}
		} else if (target._gsTweenID) {
			a = _register(target).concat();
			i = a.length;
			while (--i > -1) {
				if (a[i]._gc || onlyActive && !a[i].isActive()) {
					a.splice(i, 1);
				}
			}
		}
		return a || [];
	};

	TweenLite.killTweensOf = TweenLite.killDelayedCallsTo = function (target, onlyActive, vars) {
		if ((typeof onlyActive === "undefined" ? "undefined" : _typeof(onlyActive)) === "object") {
			vars = onlyActive; //for backwards compatibility (before "onlyActive" parameter was inserted)
			onlyActive = false;
		}
		var a = TweenLite.getTweensOf(target, onlyActive),
		    i = a.length;
		while (--i > -1) {
			a[i]._kill(vars, target);
		}
	};

	/*
  * ----------------------------------------------------------------
  * TweenPlugin   (could easily be split out as a separate file/class, but included for ease of use (so that people don't need to include another script call before loading plugins which is easy to forget)
  * ----------------------------------------------------------------
  */
	var TweenPlugin = _class("plugins.TweenPlugin", function (props, priority) {
		this._overwriteProps = (props || "").split(",");
		this._propName = this._overwriteProps[0];
		this._priority = priority || 0;
		this._super = TweenPlugin.prototype;
	}, true);

	p = TweenPlugin.prototype;
	TweenPlugin.version = "1.19.0";
	TweenPlugin.API = 2;
	p._firstPT = null;
	p._addTween = _addPropTween;
	p.setRatio = _setRatio;

	p._kill = function (lookup) {
		var a = this._overwriteProps,
		    pt = this._firstPT,
		    i;
		if (lookup[this._propName] != null) {
			this._overwriteProps = [];
		} else {
			i = a.length;
			while (--i > -1) {
				if (lookup[a[i]] != null) {
					a.splice(i, 1);
				}
			}
		}
		while (pt) {
			if (lookup[pt.n] != null) {
				if (pt._next) {
					pt._next._prev = pt._prev;
				}
				if (pt._prev) {
					pt._prev._next = pt._next;
					pt._prev = null;
				} else if (this._firstPT === pt) {
					this._firstPT = pt._next;
				}
			}
			pt = pt._next;
		}
		return false;
	};

	p._mod = p._roundProps = function (lookup) {
		var pt = this._firstPT,
		    val;
		while (pt) {
			val = lookup[this._propName] || pt.n != null && lookup[pt.n.split(this._propName + "_").join("")];
			if (val && typeof val === "function") {
				//some properties that are very plugin-specific add a prefix named after the _propName plus an underscore, so we need to ignore that extra stuff here.
				if (pt.f === 2) {
					pt.t._applyPT.m = val;
				} else {
					pt.m = val;
				}
			}
			pt = pt._next;
		}
	};

	TweenLite._onPluginEvent = function (type, tween) {
		var pt = tween._firstPT,
		    changed,
		    pt2,
		    first,
		    last,
		    next;
		if (type === "_onInitAllProps") {
			//sorts the PropTween linked list in order of priority because some plugins need to render earlier/later than others, like MotionBlurPlugin applies its effects after all x/y/alpha tweens have rendered on each frame.
			while (pt) {
				next = pt._next;
				pt2 = first;
				while (pt2 && pt2.pr > pt.pr) {
					pt2 = pt2._next;
				}
				if (pt._prev = pt2 ? pt2._prev : last) {
					pt._prev._next = pt;
				} else {
					first = pt;
				}
				if (pt._next = pt2) {
					pt2._prev = pt;
				} else {
					last = pt;
				}
				pt = next;
			}
			pt = tween._firstPT = first;
		}
		while (pt) {
			if (pt.pg) if (typeof pt.t[type] === "function") if (pt.t[type]()) {
				changed = true;
			}
			pt = pt._next;
		}
		return changed;
	};

	TweenPlugin.activate = function (plugins) {
		var i = plugins.length;
		while (--i > -1) {
			if (plugins[i].API === TweenPlugin.API) {
				_plugins[new plugins[i]()._propName] = plugins[i];
			}
		}
		return true;
	};

	//provides a more concise way to define plugins that have no dependencies besides TweenPlugin and TweenLite, wrapping common boilerplate stuff into one function (added in 1.9.0). You don't NEED to use this to define a plugin - the old way still works and can be useful in certain (rare) situations.
	_gsDefine.plugin = function (config) {
		if (!config || !config.propName || !config.init || !config.API) {
			throw "illegal plugin definition.";
		}
		var propName = config.propName,
		    priority = config.priority || 0,
		    overwriteProps = config.overwriteProps,
		    map = { init: "_onInitTween", set: "setRatio", kill: "_kill", round: "_mod", mod: "_mod", initAll: "_onInitAllProps" },
		    Plugin = _class("plugins." + propName.charAt(0).toUpperCase() + propName.substr(1) + "Plugin", function () {
			TweenPlugin.call(this, propName, priority);
			this._overwriteProps = overwriteProps || [];
		}, config.global === true),
		    p = Plugin.prototype = new TweenPlugin(propName),
		    prop;
		p.constructor = Plugin;
		Plugin.API = config.API;
		for (prop in map) {
			if (typeof config[prop] === "function") {
				p[map[prop]] = config[prop];
			}
		}
		Plugin.version = config.version;
		TweenPlugin.activate([Plugin]);
		return Plugin;
	};

	//now run through all the dependencies discovered and if any are missing, log that to the console as a warning. This is why it's best to have TweenLite load last - it can check all the dependencies for you.
	a = window._gsQueue;
	if (a) {
		for (i = 0; i < a.length; i++) {
			a[i]();
		}
		for (p in _defLookup) {
			if (!_defLookup[p].func) {
				window.console.log("GSAP encountered missing dependency: " + p);
			}
		}
	}

	_tickerActive = false; //ensures that the first official animation forces a ticker.tick() to update the time when it is instantiated
})(typeof module !== "undefined" && module.exports && typeof global !== "undefined" ? global : undefined || window, "TweenLite");
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(3)))

/***/ })

});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9fSFRNTC9zcmMvanMvX21vZHVsZXMvc2Nyb2xsLXdpbmRvdy5qcyIsIndlYnBhY2s6Ly8vLi9fSFRNTC9zcmMvanMvX3ZlbmRvcnMvZ3NhcC9wbHVnaW5zL1Njcm9sbFRvUGx1Z2luLmpzIiwid2VicGFjazovLy8uL19IVE1ML3NyYy9qcy9fbW9kdWxlcy1sb2FkZXJzL3Njcm9sbC13aW5kb3ctLW1vZHVsZS1sb2FkZXIuanMiLCJ3ZWJwYWNrOi8vLy4vX0hUTUwvc3JjL2pzL192ZW5kb3JzL2dzYXAvVHdlZW5MaXRlLmpzIl0sIm5hbWVzIjpbImRlZmF1bHREdXJhdGlvbiIsIiR3aW5kb3ciLCIkIiwid2luZG93Iiwic2hvd0hlaWdodCIsInNob3dDbGFzcyIsInNjcm9sbFdpbmRvdyIsIiRlbGVtZW50cyIsIm9uIiwiJHRoaXMiLCJzY3JvbGwiLCJkYXRhIiwidG8iLCJzY3JvbGxUbyIsImVhc2UiLCJQb3dlcjIiLCJlYXNlT3V0Iiwib25Db21wbGV0ZSIsIiRzY3JvbGxVcCIsImZpbHRlciIsImxlbmd0aCIsIm9uU2Nyb2xsIiwic2Nyb2xsVG9wIiwiZG9DbGFzcyIsIl9nc1Njb3BlIiwibW9kdWxlIiwiZXhwb3J0cyIsImdsb2JhbCIsIl9nc1F1ZXVlIiwicHVzaCIsIl9kb2MiLCJkb2N1bWVudCIsImRvY3VtZW50RWxlbWVudCIsIl93aW5kb3ciLCJfbWF4IiwiZWxlbWVudCIsImF4aXMiLCJkaW0iLCJjbGllbnQiLCJib2R5IiwiTWF0aCIsIm1heCIsIl91bndyYXBFbGVtZW50IiwidmFsdWUiLCJHcmVlblNvY2tHbG9iYWxzIiwiVHdlZW5MaXRlIiwic2VsZWN0b3IiLCJzdHlsZSIsIm5vZGVUeXBlIiwiX2J1aWxkR2V0dGVyIiwiZSIsInAiLCJwYWdlWE9mZnNldCIsInRvVXBwZXJDYXNlIiwiX2dldE9mZnNldCIsImNvbnRhaW5lciIsInJlY3QiLCJnZXRCb3VuZGluZ0NsaWVudFJlY3QiLCJpc1Jvb3QiLCJjUmVjdCIsIm9mZnNldHMiLCJ4IiwibGVmdCIsInkiLCJ0b3AiLCJfcGFyc2VWYWwiLCJ0YXJnZXQiLCJ0eXBlIiwiaXNOYU4iLCJwYXJzZUZsb2F0IiwiY2hhckF0IiwibWluIiwiU2Nyb2xsVG9QbHVnaW4iLCJfZ3NEZWZpbmUiLCJwbHVnaW4iLCJwcm9wTmFtZSIsIkFQSSIsInZlcnNpb24iLCJpbml0IiwidHdlZW4iLCJfd2R3IiwiX3RhcmdldCIsIl90d2VlbiIsInZhcnMiLCJfYXV0b0tpbGwiLCJhdXRvS2lsbCIsImdldFgiLCJnZXRZIiwieFByZXYiLCJ5UHJldiIsIl9hZGRUd2VlbiIsIm9mZnNldFgiLCJfb3ZlcndyaXRlUHJvcHMiLCJza2lwWCIsIm9mZnNldFkiLCJza2lwWSIsInNldCIsInYiLCJfc3VwZXIiLCJzZXRSYXRpbyIsImNhbGwiLCJ5RGlmIiwieERpZiIsInRocmVzaG9sZCIsImF1dG9LaWxsVGhyZXNob2xkIiwia2lsbCIsIm9uQXV0b0tpbGwiLCJhcHBseSIsIm9uQXV0b0tpbGxTY29wZSIsIm9uQXV0b0tpbGxQYXJhbXMiLCJzY3JvbGxMZWZ0IiwicHJvdG90eXBlIiwiZ2V0T2Zmc2V0IiwiYnVpbGRHZXR0ZXIiLCJfa2lsbCIsImxvb2t1cCIsInNjcm9sbFRvX3giLCJzY3JvbGxUb195IiwicG9wIiwibmFtZSIsImdldEdsb2JhbCIsInJlcXVpcmUiLCJkZWZpbmUiLCJsb2FkZXJJbml0IiwibW9kdWxlTG9hZGVyIiwibW9kdWxlTmFtZSIsIl9leHBvcnRzIiwiX2dsb2JhbHMiLCJfbmFtZXNwYWNlIiwibnMiLCJhIiwic3BsaXQiLCJpIiwiZ3MiLCJfdGlueU51bSIsIl9zbGljZSIsImIiLCJsIiwiX2VtcHR5RnVuYyIsIl9pc0FycmF5IiwidG9TdHJpbmciLCJPYmplY3QiLCJhcnJheSIsIm9iaiIsIkFycmF5IiwiX3RpY2tlciIsIl90aWNrZXJBY3RpdmUiLCJfZGVmTG9va3VwIiwiRGVmaW5pdGlvbiIsImRlcGVuZGVuY2llcyIsImZ1bmMiLCJzYyIsImdzQ2xhc3MiLCJfY2xhc3NlcyIsImNoZWNrIiwibWlzc2luZyIsImN1ciIsIm4iLCJjbCIsImpvaW4iLCJfY2xhc3MiLCJnbG9iYWxzIiwiX2Jhc2VQYXJhbXMiLCJFYXNlIiwiZXh0cmFQYXJhbXMiLCJwb3dlciIsIl9mdW5jIiwiX3R5cGUiLCJfcG93ZXIiLCJfcGFyYW1zIiwiY29uY2F0IiwiX2Vhc2VNYXAiLCJtYXAiLCJfZWFzZVJlZyIsInJlZ2lzdGVyIiwibmFtZXMiLCJ0eXBlcyIsImNyZWF0ZSIsIm5hIiwidGEiLCJqIiwiZWFzaW5nIiwiZ2V0UmF0aW8iLCJfY2FsY0VuZCIsInQiLCJwdyIsInIiLCJsaW5lYXIiLCJMaW5lYXIiLCJlYXNlSW4iLCJzd2luZyIsIlF1YWQiLCJlYXNlSW5PdXQiLCJFdmVudERpc3BhdGNoZXIiLCJfbGlzdGVuZXJzIiwiX2V2ZW50VGFyZ2V0IiwiYWRkRXZlbnRMaXN0ZW5lciIsImNhbGxiYWNrIiwic2NvcGUiLCJ1c2VQYXJhbSIsInByaW9yaXR5IiwibGlzdCIsImluZGV4IiwibGlzdGVuZXIiLCJ3YWtlIiwiYyIsInMiLCJzcGxpY2UiLCJwciIsInVwIiwicmVtb3ZlRXZlbnRMaXN0ZW5lciIsImRpc3BhdGNoRXZlbnQiLCJzbGljZSIsIl9yZXFBbmltRnJhbWUiLCJyZXF1ZXN0QW5pbWF0aW9uRnJhbWUiLCJfY2FuY2VsQW5pbUZyYW1lIiwiY2FuY2VsQW5pbWF0aW9uRnJhbWUiLCJfZ2V0VGltZSIsIkRhdGUiLCJub3ciLCJnZXRUaW1lIiwiX2xhc3RVcGRhdGUiLCJmcHMiLCJ1c2VSQUYiLCJfc2VsZiIsIl9zdGFydFRpbWUiLCJfdXNlUkFGIiwiX2xhZ1RocmVzaG9sZCIsIl9hZGp1c3RlZExhZyIsIl90aWNrV29yZCIsIl9mcHMiLCJfcmVxIiwiX2lkIiwiX2dhcCIsIl9uZXh0VGltZSIsIl90aWNrIiwibWFudWFsIiwiZWxhcHNlZCIsIm92ZXJsYXAiLCJkaXNwYXRjaCIsInRpbWUiLCJmcmFtZSIsInRpY2siLCJsYWdTbW9vdGhpbmciLCJhZGp1c3RlZExhZyIsImFyZ3VtZW50cyIsInNsZWVwIiwiY2xlYXJUaW1lb3V0Iiwic2VhbWxlc3MiLCJmIiwic2V0VGltZW91dCIsInZpc2liaWxpdHlTdGF0ZSIsIlRpY2tlciIsImV2ZW50cyIsImNvbnN0cnVjdG9yIiwiQW5pbWF0aW9uIiwiZHVyYXRpb24iLCJfZHVyYXRpb24iLCJfdG90YWxEdXJhdGlvbiIsIl9kZWxheSIsIk51bWJlciIsImRlbGF5IiwiX3RpbWVTY2FsZSIsIl9hY3RpdmUiLCJpbW1lZGlhdGVSZW5kZXIiLCJfcmV2ZXJzZWQiLCJyZXZlcnNlZCIsIl9yb290VGltZWxpbmUiLCJ0bCIsInVzZUZyYW1lcyIsIl9yb290RnJhbWVzVGltZWxpbmUiLCJhZGQiLCJfdGltZSIsInBhdXNlZCIsInRpY2tlciIsIl9kaXJ0eSIsIl9nYyIsIl9pbml0dGVkIiwiX3BhdXNlZCIsIl90b3RhbFRpbWUiLCJfcmF3UHJldlRpbWUiLCJfbmV4dCIsIl9sYXN0IiwiX29uVXBkYXRlIiwiX3RpbWVsaW5lIiwidGltZWxpbmUiLCJfY2hlY2tUaW1lb3V0IiwidW5yZWYiLCJwbGF5IiwiZnJvbSIsInN1cHByZXNzRXZlbnRzIiwic2VlayIsInBhdXNlIiwiYXRUaW1lIiwicmVzdW1lIiwidG90YWxUaW1lIiwicmVzdGFydCIsImluY2x1ZGVEZWxheSIsInJldmVyc2UiLCJ0b3RhbER1cmF0aW9uIiwicmVuZGVyIiwiZm9yY2UiLCJpbnZhbGlkYXRlIiwiX2VuYWJsZWQiLCJpc0FjdGl2ZSIsInN0YXJ0VGltZSIsInJhd1RpbWUiLCJlbmFibGVkIiwiaWdub3JlVGltZWxpbmUiLCJfcmVtb3ZlIiwiX3VuY2FjaGUiLCJpbmNsdWRlU2VsZiIsIl9zd2FwU2VsZkluUGFyYW1zIiwicGFyYW1zIiwiY29weSIsIl9jYWxsYmFjayIsImNhbGxiYWNrU2NvcGUiLCJldmVudENhbGxiYWNrIiwic3Vic3RyIiwiaW5kZXhPZiIsInNtb290aENoaWxkVGltaW5nIiwidW5jYXBwZWQiLCJfcGF1c2VUaW1lIiwiX2xhenlUd2VlbnMiLCJfbGF6eVJlbmRlciIsInByb2dyZXNzIiwidG90YWxQcm9ncmVzcyIsInJhdGlvIiwiX3NvcnRDaGlsZHJlbiIsImVuZFRpbWUiLCJpbmNsdWRlUmVwZWF0cyIsInRpbWVTY2FsZSIsInBhdXNlVGltZSIsInJhdyIsIlNpbXBsZVRpbWVsaW5lIiwiYXV0b1JlbW92ZUNoaWxkcmVuIiwiX2ZpcnN0IiwiX3JlY2VudCIsImluc2VydCIsImNoaWxkIiwicG9zaXRpb24iLCJhbGlnbiIsInN0YWdnZXIiLCJwcmV2VHdlZW4iLCJzdCIsIl9wcmV2Iiwic2tpcERpc2FibGUiLCJuZXh0IiwiaXNTZWxlY3RvciIsImpxdWVyeSIsIm92ZXJ3cml0ZSIsInRhcmciLCJ0YXJnZXRzIiwiX292ZXJ3cml0ZSIsIl9vdmVyd3JpdGVMb29rdXAiLCJkZWZhdWx0T3ZlcndyaXRlIiwiX3RhcmdldHMiLCJfcHJvcExvb2t1cCIsIl9zaWJsaW5ncyIsIl9yZWdpc3RlciIsIl9hcHBseU92ZXJ3cml0ZSIsIl9pc1NlbGVjdG9yIiwiX2F1dG9DU1MiLCJjc3MiLCJfcmVzZXJ2ZWRQcm9wcyIsIl9wbHVnaW5zIiwiX2ZpcnN0UFQiLCJfb3ZlcndyaXR0ZW5Qcm9wcyIsIl9zdGFydEF0IiwiX25vdGlmeVBsdWdpbnNPZkVuYWJsZWQiLCJfbGF6eSIsImRlZmF1bHRFYXNlIiwiX2Vhc2UiLCJhdXRvU2xlZXAiLCJqUXVlcnkiLCJxdWVyeVNlbGVjdG9yQWxsIiwiZ2V0RWxlbWVudEJ5SWQiLCJfbGF6eUxvb2t1cCIsIl9udW1iZXJzRXhwIiwiX3JlbEV4cCIsIl9zZXRSYXRpbyIsInB0IiwidmFsIiwiYmxvYiIsImVuZCIsInN0YXJ0IiwibSIsImZwIiwiX2Jsb2JEaWYiLCJjaGFySW5kZXgiLCJjb2xvciIsInN0YXJ0TnVtcyIsImVuZE51bXMiLCJudW0iLCJub25OdW1iZXJzIiwiY3VycmVudE51bSIsIm1hdGNoIiwiX2FwcGx5UFQiLCJwYXJzZUludCIsInJvdW5kIiwidGVzdCIsIl9hZGRQcm9wVHdlZW4iLCJwcm9wIiwib3ZlcndyaXRlUHJvcCIsIm1vZCIsImZ1bmNQYXJhbSIsInN0cmluZ0ZpbHRlciIsImdldHRlck5hbWUiLCJpc1JlbGF0aXZlIiwicGciLCJkZWZhdWx0U3RyaW5nRmlsdGVyIiwiX2ludGVybmFscyIsImlzQXJyYXkiLCJsYXp5VHdlZW5zIiwiYmxvYkRpZiIsIl90d2Vlbkxvb2t1cCIsInR3ZWVuTG9va3VwIiwiX3R3ZWVuTG9va3VwTnVtIiwicmVzZXJ2ZWRQcm9wcyIsIm9uQ29tcGxldGVQYXJhbXMiLCJvbkNvbXBsZXRlU2NvcGUiLCJydW5CYWNrd2FyZHMiLCJzdGFydEF0Iiwib25VcGRhdGUiLCJvblVwZGF0ZVBhcmFtcyIsIm9uVXBkYXRlU2NvcGUiLCJvblN0YXJ0Iiwib25TdGFydFBhcmFtcyIsIm9uU3RhcnRTY29wZSIsIm9uUmV2ZXJzZUNvbXBsZXRlIiwib25SZXZlcnNlQ29tcGxldGVQYXJhbXMiLCJvblJldmVyc2VDb21wbGV0ZVNjb3BlIiwib25SZXBlYXQiLCJvblJlcGVhdFBhcmFtcyIsIm9uUmVwZWF0U2NvcGUiLCJlYXNlUGFyYW1zIiwieW95byIsInJlcGVhdCIsInJlcGVhdERlbGF5IiwiYXV0b0NTUyIsImxhenkiLCJvbk92ZXJ3cml0ZSIsImlkIiwieW95b0Vhc2UiLCJub25lIiwiYWxsIiwiYXV0byIsImNvbmN1cnJlbnQiLCJhbGxPblN0YXJ0IiwicHJlZXhpc3RpbmciLCJfbmV4dEdDRnJhbWUiLCJsYXp5UmVuZGVyIiwiX3VwZGF0ZVJvb3QiLCJ0d2VlbnMiLCJzY3J1YiIsIl9nc1R3ZWVuSUQiLCJfb25PdmVyd3JpdGUiLCJvdmVyd3JpdHRlblR3ZWVuIiwib3ZlcndyaXRpbmdUd2VlbiIsImtpbGxlZFByb3BzIiwicjEiLCJyMiIsInByb3BzIiwibW9kZSIsInNpYmxpbmdzIiwiY2hhbmdlZCIsImN1clR3ZWVuIiwib3ZlcmxhcHMiLCJvQ291bnQiLCJ6ZXJvRHVyIiwiZ2xvYmFsU3RhcnQiLCJfY2hlY2tPdmVybGFwIiwicmVmZXJlbmNlIiwidHMiLCJfaW5pdCIsIm9wIiwiZHVyIiwiaW1tZWRpYXRlIiwiaW5pdFBsdWdpbnMiLCJzdGFydFZhcnMiLCJjb25maWciLCJfZWFzZVR5cGUiLCJfZWFzZVBvd2VyIiwiX2luaXRQcm9wcyIsIl9vblBsdWdpbkV2ZW50IiwicHJvcExvb2t1cCIsIm92ZXJ3cml0dGVuUHJvcHMiLCJfb25Jbml0VHdlZW4iLCJfcHJpb3JpdHkiLCJfb25Jbml0QWxsUHJvcHMiLCJfb25EaXNhYmxlIiwiX29uRW5hYmxlIiwicHJldlRpbWUiLCJwcmV2UmF3UHJldlRpbWUiLCJpc0NvbXBsZXRlIiwicmF3UHJldlRpbWUiLCJwb3ciLCJzaW11bHRhbmVvdXNPdmVyd3JpdGUiLCJraWxsUHJvcHMiLCJyZWNvcmQiLCJraWxsZWQiLCJfdGVtcEtpbGwiLCJmcm9tVG8iLCJmcm9tVmFycyIsInRvVmFycyIsImRlbGF5ZWRDYWxsIiwiZ2V0VHdlZW5zT2YiLCJvbmx5QWN0aXZlIiwia2lsbFR3ZWVuc09mIiwia2lsbERlbGF5ZWRDYWxsc1RvIiwiVHdlZW5QbHVnaW4iLCJfcHJvcE5hbWUiLCJfbW9kIiwiX3JvdW5kUHJvcHMiLCJwdDIiLCJmaXJzdCIsImxhc3QiLCJhY3RpdmF0ZSIsInBsdWdpbnMiLCJvdmVyd3JpdGVQcm9wcyIsImluaXRBbGwiLCJQbHVnaW4iLCJjb25zb2xlIiwibG9nIl0sIm1hcHBpbmdzIjoiOzs7Ozs7QUFBQTs7QUFFQTs7Ozs7O0FBTUE7QUFDQTtBQUNBOzs7Ozs7QUFFQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7O0FBS0EsSUFBTUEsa0JBQWtCLENBQXhCOztBQUVBOzs7O0FBSUEsSUFBTUMsVUFBVUMsRUFBRUMsTUFBRixDQUFoQjs7QUFFQTs7OztBQUlBLElBQU1DLGFBQWEsR0FBbkI7O0FBRUE7Ozs7QUFJQSxJQUFNQyxZQUFZLGlCQUFsQjs7QUFFQTtBQUNBO0FBQ0E7O0FBRUEsU0FBU0MsWUFBVCxDQUF1QkMsU0FBdkIsRUFBa0M7QUFDakNBLFdBQVVDLEVBQVYsQ0FBYSxPQUFiLEVBQXNCLFlBQVk7QUFDakMsTUFBSUMsUUFBUVAsRUFBRSxJQUFGLENBQVo7QUFDQSxNQUFJUSxTQUFTRCxNQUFNRSxJQUFOLENBQVcsZUFBWCxLQUErQixDQUE1Qzs7QUFFQSxNQUFJRCxXQUFXLElBQWYsRUFBcUI7QUFDcEJBLFlBQVMsQ0FBVDtBQUNBLEdBRkQsTUFFTyxJQUFJQSxXQUFXLE1BQWYsRUFBdUI7QUFDN0JBLFlBQVMsS0FBVDtBQUNBOztBQUVELHNCQUFVRSxFQUFWLENBQWFULE1BQWIsRUFBcUJILGVBQXJCLEVBQXNDO0FBQ3JDYSxhQUFVSCxNQUQyQjtBQUVyQ0ksU0FBTSxlQUFLQyxNQUFMLENBQVlDLE9BRm1CO0FBR3JDQyxhQUhxQyx3QkFHdkI7QUFDYjtBQUNBO0FBTG9DLEdBQXRDO0FBT0EsRUFqQkQ7O0FBbUJBLEtBQU1DLFlBQVlYLFVBQVVZLE1BQVYsQ0FBaUIsMkJBQWpCLENBQWxCO0FBQ0EsS0FBSUQsVUFBVUUsTUFBZCxFQUFzQjtBQUNyQixNQUFNQyxXQUFXLFNBQVhBLFFBQVcsR0FBTTtBQUN0QixPQUFJWCxTQUFTVCxRQUFRcUIsU0FBUixFQUFiO0FBQ0EsT0FBSUMsVUFBVWIsU0FBU04sVUFBVCxHQUFzQixVQUF0QixHQUFtQyxhQUFqRDtBQUNBYyxhQUFVSyxPQUFWLEVBQW1CbEIsU0FBbkI7QUFDQSxHQUpEOztBQU1BSixVQUFRTyxFQUFSLENBQVcsUUFBWCxFQUFxQmEsUUFBckI7QUFDQUE7QUFDQTtBQUNEOztBQUVEO0FBQ0E7QUFDQTs7a0JBRWVmLFk7Ozs7Ozs7Ozs7OztBQ3RGZjs7Ozs7Ozs7Ozs7QUFXQSxJQUFJa0IsV0FBWSxPQUFPQyxNQUFQLEtBQW1CLFdBQW5CLElBQWtDQSxPQUFPQyxPQUF6QyxJQUFvRCxPQUFPQyxNQUFQLEtBQW1CLFdBQXhFLEdBQXVGQSxNQUF2RixHQUFnRyxhQUFReEIsTUFBdkgsQyxDQUErSDtBQUMvSCxDQUFDcUIsU0FBU0ksUUFBVCxLQUFzQkosU0FBU0ksUUFBVCxHQUFvQixFQUExQyxDQUFELEVBQWdEQyxJQUFoRCxDQUFzRCxZQUFXOztBQUVoRTs7QUFFQSxLQUFJQyxPQUFPLENBQUNOLFNBQVNPLFFBQVQsSUFBcUIsRUFBdEIsRUFBMEJDLGVBQXJDO0FBQUEsS0FDQ0MsVUFBVVQsUUFEWDtBQUFBLEtBRUNVLE9BQU8sU0FBUEEsSUFBTyxDQUFTQyxPQUFULEVBQWtCQyxJQUFsQixFQUF3QjtBQUM5QixNQUFJQyxNQUFPRCxTQUFTLEdBQVYsR0FBaUIsT0FBakIsR0FBMkIsUUFBckM7QUFBQSxNQUNDMUIsU0FBUyxXQUFXMkIsR0FEckI7QUFBQSxNQUVDQyxTQUFTLFdBQVdELEdBRnJCO0FBQUEsTUFHQ0UsT0FBT1IsU0FBU1EsSUFIakI7QUFJQSxTQUFRSixZQUFZRixPQUFaLElBQXVCRSxZQUFZTCxJQUFuQyxJQUEyQ0ssWUFBWUksSUFBeEQsR0FBZ0VDLEtBQUtDLEdBQUwsQ0FBU1gsS0FBS3BCLE1BQUwsQ0FBVCxFQUF1QjZCLEtBQUs3QixNQUFMLENBQXZCLEtBQXdDdUIsUUFBUSxVQUFVSSxHQUFsQixLQUEwQlAsS0FBS1EsTUFBTCxDQUExQixJQUEwQ0MsS0FBS0QsTUFBTCxDQUFsRixDQUFoRSxHQUFrS0gsUUFBUXpCLE1BQVIsSUFBa0J5QixRQUFRLFdBQVdFLEdBQW5CLENBQTNMO0FBQ0EsRUFSRjtBQUFBLEtBU0NLLGlCQUFpQixTQUFqQkEsY0FBaUIsQ0FBU0MsS0FBVCxFQUFnQjtBQUNoQyxNQUFJLE9BQU9BLEtBQVAsS0FBa0IsUUFBdEIsRUFBZ0M7QUFDL0JBLFdBQVFWLFFBQVFXLGdCQUFSLENBQXlCQyxTQUF6QixDQUFtQ0MsUUFBbkMsQ0FBNENILEtBQTVDLENBQVI7QUFDQTtBQUNELE1BQUlBLE1BQU12QixNQUFOLElBQWdCdUIsVUFBVVYsT0FBMUIsSUFBcUNVLE1BQU0sQ0FBTixDQUFyQyxJQUFpREEsTUFBTSxDQUFOLEVBQVNJLEtBQTFELElBQW1FLENBQUNKLE1BQU1LLFFBQTlFLEVBQXdGO0FBQ3ZGTCxXQUFRQSxNQUFNLENBQU4sQ0FBUjtBQUNBO0FBQ0QsU0FBUUEsVUFBVVYsT0FBVixJQUFzQlUsTUFBTUssUUFBTixJQUFrQkwsTUFBTUksS0FBL0MsR0FBeURKLEtBQXpELEdBQWlFLElBQXhFO0FBQ0EsRUFqQkY7QUFBQSxLQWtCQ00sZUFBZSxTQUFmQSxZQUFlLENBQVNDLENBQVQsRUFBWWQsSUFBWixFQUFrQjtBQUFFO0FBQ2xDLE1BQUllLElBQUksWUFBYWYsU0FBUyxHQUFWLEdBQWlCLE1BQWpCLEdBQTBCLEtBQXRDLENBQVI7QUFDQSxNQUFJYyxNQUFNakIsT0FBVixFQUFtQjtBQUNsQixPQUFJaUIsRUFBRUUsV0FBRixJQUFpQixJQUFyQixFQUEyQjtBQUMxQkQsUUFBSSxTQUFTZixLQUFLaUIsV0FBTCxFQUFULEdBQThCLFFBQWxDO0FBQ0EsSUFGRCxNQUVPLElBQUl2QixLQUFLcUIsQ0FBTCxLQUFXLElBQWYsRUFBcUI7QUFDM0JELFFBQUlwQixJQUFKO0FBQ0EsSUFGTSxNQUVBO0FBQ05vQixRQUFJbkIsU0FBU1EsSUFBYjtBQUNBO0FBQ0Q7QUFDRCxTQUFPLFlBQVc7QUFDakIsVUFBT1csRUFBRUMsQ0FBRixDQUFQO0FBQ0EsR0FGRDtBQUdBLEVBaENGO0FBQUEsS0FpQ0NHLGFBQWEsU0FBYkEsVUFBYSxDQUFTbkIsT0FBVCxFQUFrQm9CLFNBQWxCLEVBQTZCO0FBQ3pDLE1BQUlDLE9BQU9kLGVBQWVQLE9BQWYsRUFBd0JzQixxQkFBeEIsRUFBWDtBQUFBLE1BQ0NDLFNBQVUsQ0FBQ0gsU0FBRCxJQUFjQSxjQUFjdEIsT0FBNUIsSUFBdUNzQixjQUFjeEIsU0FBU1EsSUFEekU7QUFBQSxNQUVDb0IsUUFBUSxDQUFDRCxTQUFTNUIsSUFBVCxHQUFnQnlCLFNBQWpCLEVBQTRCRSxxQkFBNUIsRUFGVDtBQUFBLE1BR0NHLFVBQVUsRUFBQ0MsR0FBR0wsS0FBS00sSUFBTCxHQUFZSCxNQUFNRyxJQUF0QixFQUE0QkMsR0FBR1AsS0FBS1EsR0FBTCxHQUFXTCxNQUFNSyxHQUFoRCxFQUhYO0FBSUEsTUFBSSxDQUFDTixNQUFELElBQVdILFNBQWYsRUFBMEI7QUFBRTtBQUMzQkssV0FBUUMsQ0FBUixJQUFhWixhQUFhTSxTQUFiLEVBQXdCLEdBQXhCLEdBQWI7QUFDQUssV0FBUUcsQ0FBUixJQUFhZCxhQUFhTSxTQUFiLEVBQXdCLEdBQXhCLEdBQWI7QUFDQTtBQUNELFNBQU9LLE9BQVA7QUFDQSxFQTNDRjtBQUFBLEtBNENDSyxZQUFZLFNBQVpBLFNBQVksQ0FBU3RCLEtBQVQsRUFBZ0J1QixNQUFoQixFQUF3QjlCLElBQXhCLEVBQThCO0FBQ3pDLE1BQUkrQixjQUFjeEIsS0FBZCx5Q0FBY0EsS0FBZCxDQUFKO0FBQ0EsU0FBTyxDQUFDeUIsTUFBTXpCLEtBQU4sQ0FBRCxHQUFnQjBCLFdBQVcxQixLQUFYLENBQWhCLEdBQXFDd0IsU0FBUyxRQUFULElBQXNCQSxTQUFTLFFBQVQsSUFBcUJ4QixNQUFNMkIsTUFBTixDQUFhLENBQWIsTUFBb0IsR0FBaEUsR0FBd0UzQixLQUF4RSxHQUFpRkEsVUFBVSxLQUFYLEdBQW9CVCxLQUFLZ0MsTUFBTCxFQUFhOUIsSUFBYixDQUFwQixHQUF5Q0ksS0FBSytCLEdBQUwsQ0FBU3JDLEtBQUtnQyxNQUFMLEVBQWE5QixJQUFiLENBQVQsRUFBNkJrQixXQUFXWCxLQUFYLEVBQWtCdUIsTUFBbEIsRUFBMEI5QixJQUExQixDQUE3QixDQUFwSztBQUNBLEVBL0NGO0FBQUEsS0FpRENvQyxpQkFBaUJoRCxTQUFTaUQsU0FBVCxDQUFtQkMsTUFBbkIsQ0FBMEI7QUFDMUNDLFlBQVUsVUFEZ0M7QUFFMUNDLE9BQUssQ0FGcUM7QUFHMUNqRCxVQUFRLElBSGtDO0FBSTFDa0QsV0FBUSxPQUprQzs7QUFNMUM7QUFDQUMsUUFBTSxjQUFTWixNQUFULEVBQWlCdkIsS0FBakIsRUFBd0JvQyxLQUF4QixFQUErQjtBQUNwQyxRQUFLQyxJQUFMLEdBQWFkLFdBQVdqQyxPQUF4QjtBQUNBLFFBQUtnRCxPQUFMLEdBQWVmLE1BQWY7QUFDQSxRQUFLZ0IsTUFBTCxHQUFjSCxLQUFkO0FBQ0EsT0FBSSxRQUFPcEMsS0FBUCx5Q0FBT0EsS0FBUCxPQUFrQixRQUF0QixFQUFnQztBQUMvQkEsWUFBUSxFQUFDb0IsR0FBRXBCLEtBQUgsRUFBUixDQUQrQixDQUNaO0FBQ25CLFFBQUksT0FBT0EsTUFBTW9CLENBQWIsS0FBb0IsUUFBcEIsSUFBZ0NwQixNQUFNb0IsQ0FBTixLQUFZLEtBQTVDLElBQXFEcEIsTUFBTW9CLENBQU4sQ0FBUU8sTUFBUixDQUFlLENBQWYsTUFBc0IsR0FBL0UsRUFBb0Y7QUFDbkYzQixXQUFNa0IsQ0FBTixHQUFVbEIsTUFBTW9CLENBQWhCO0FBQ0E7QUFDRCxJQUxELE1BS08sSUFBSXBCLE1BQU1LLFFBQVYsRUFBb0I7QUFDMUJMLFlBQVEsRUFBQ29CLEdBQUVwQixLQUFILEVBQVVrQixHQUFFbEIsS0FBWixFQUFSO0FBQ0E7QUFDRCxRQUFLd0MsSUFBTCxHQUFZeEMsS0FBWjtBQUNBLFFBQUt5QyxTQUFMLEdBQWtCekMsTUFBTTBDLFFBQU4sS0FBbUIsS0FBckM7QUFDQSxRQUFLQyxJQUFMLEdBQVlyQyxhQUFhaUIsTUFBYixFQUFxQixHQUFyQixDQUFaO0FBQ0EsUUFBS3FCLElBQUwsR0FBWXRDLGFBQWFpQixNQUFiLEVBQXFCLEdBQXJCLENBQVo7QUFDQSxRQUFLTCxDQUFMLEdBQVMsS0FBSzJCLEtBQUwsR0FBYSxLQUFLRixJQUFMLEVBQXRCO0FBQ0EsUUFBS3ZCLENBQUwsR0FBUyxLQUFLMEIsS0FBTCxHQUFhLEtBQUtGLElBQUwsRUFBdEI7QUFDQSxPQUFJNUMsTUFBTWtCLENBQU4sSUFBVyxJQUFmLEVBQXFCO0FBQ3BCLFNBQUs2QixTQUFMLENBQWUsSUFBZixFQUFxQixHQUFyQixFQUEwQixLQUFLN0IsQ0FBL0IsRUFBa0NJLFVBQVV0QixNQUFNa0IsQ0FBaEIsRUFBbUJLLE1BQW5CLEVBQTJCLEdBQTNCLEtBQW1DdkIsTUFBTWdELE9BQU4sSUFBaUIsQ0FBcEQsQ0FBbEMsRUFBMEYsWUFBMUYsRUFBd0csSUFBeEc7QUFDQSxTQUFLQyxlQUFMLENBQXFCL0QsSUFBckIsQ0FBMEIsWUFBMUI7QUFDQSxJQUhELE1BR087QUFDTixTQUFLZ0UsS0FBTCxHQUFhLElBQWI7QUFDQTtBQUNELE9BQUlsRCxNQUFNb0IsQ0FBTixJQUFXLElBQWYsRUFBcUI7QUFDcEIsU0FBSzJCLFNBQUwsQ0FBZSxJQUFmLEVBQXFCLEdBQXJCLEVBQTBCLEtBQUszQixDQUEvQixFQUFrQ0UsVUFBVXRCLE1BQU1vQixDQUFoQixFQUFtQkcsTUFBbkIsRUFBMkIsR0FBM0IsS0FBbUN2QixNQUFNbUQsT0FBTixJQUFpQixDQUFwRCxDQUFsQyxFQUEwRixZQUExRixFQUF3RyxJQUF4RztBQUNBLFNBQUtGLGVBQUwsQ0FBcUIvRCxJQUFyQixDQUEwQixZQUExQjtBQUNBLElBSEQsTUFHTztBQUNOLFNBQUtrRSxLQUFMLEdBQWEsSUFBYjtBQUNBO0FBQ0QsVUFBTyxJQUFQO0FBQ0EsR0F0Q3lDOztBQXdDMUM7QUFDQUMsT0FBSyxhQUFTQyxDQUFULEVBQVk7QUFDaEIsUUFBS0MsTUFBTCxDQUFZQyxRQUFaLENBQXFCQyxJQUFyQixDQUEwQixJQUExQixFQUFnQ0gsQ0FBaEM7O0FBRUEsT0FBSXBDLElBQUssS0FBS21CLElBQUwsSUFBYSxDQUFDLEtBQUthLEtBQXBCLEdBQTZCLEtBQUtQLElBQUwsRUFBN0IsR0FBMkMsS0FBS0UsS0FBeEQ7QUFBQSxPQUNDekIsSUFBSyxLQUFLaUIsSUFBTCxJQUFhLENBQUMsS0FBS2UsS0FBcEIsR0FBNkIsS0FBS1IsSUFBTCxFQUE3QixHQUEyQyxLQUFLRSxLQURyRDtBQUFBLE9BRUNZLE9BQU90QyxJQUFJLEtBQUswQixLQUZqQjtBQUFBLE9BR0NhLE9BQU96QyxJQUFJLEtBQUsyQixLQUhqQjtBQUFBLE9BSUNlLFlBQVkvQixlQUFlZ0MsaUJBSjVCOztBQU1BLE9BQUksS0FBSzNDLENBQUwsR0FBUyxDQUFiLEVBQWdCO0FBQUU7QUFDakIsU0FBS0EsQ0FBTCxHQUFTLENBQVQ7QUFDQTtBQUNELE9BQUksS0FBS0UsQ0FBTCxHQUFTLENBQWIsRUFBZ0I7QUFDZixTQUFLQSxDQUFMLEdBQVMsQ0FBVDtBQUNBO0FBQ0QsT0FBSSxLQUFLcUIsU0FBVCxFQUFvQjtBQUNuQjtBQUNBLFFBQUksQ0FBQyxLQUFLUyxLQUFOLEtBQWdCUyxPQUFPQyxTQUFQLElBQW9CRCxPQUFPLENBQUNDLFNBQTVDLEtBQTBEMUMsSUFBSTNCLEtBQUssS0FBSytDLE9BQVYsRUFBbUIsR0FBbkIsQ0FBbEUsRUFBMkY7QUFDMUYsVUFBS1ksS0FBTCxHQUFhLElBQWIsQ0FEMEYsQ0FDdkU7QUFDbkI7QUFDRCxRQUFJLENBQUMsS0FBS0UsS0FBTixLQUFnQk0sT0FBT0UsU0FBUCxJQUFvQkYsT0FBTyxDQUFDRSxTQUE1QyxLQUEwRHhDLElBQUk3QixLQUFLLEtBQUsrQyxPQUFWLEVBQW1CLEdBQW5CLENBQWxFLEVBQTJGO0FBQzFGLFVBQUtjLEtBQUwsR0FBYSxJQUFiLENBRDBGLENBQ3ZFO0FBQ25CO0FBQ0QsUUFBSSxLQUFLRixLQUFMLElBQWMsS0FBS0UsS0FBdkIsRUFBOEI7QUFDN0IsVUFBS2IsTUFBTCxDQUFZdUIsSUFBWjtBQUNBLFNBQUksS0FBS3RCLElBQUwsQ0FBVXVCLFVBQWQsRUFBMEI7QUFDekIsV0FBS3ZCLElBQUwsQ0FBVXVCLFVBQVYsQ0FBcUJDLEtBQXJCLENBQTJCLEtBQUt4QixJQUFMLENBQVV5QixlQUFWLElBQTZCLEtBQUsxQixNQUE3RCxFQUFxRSxLQUFLQyxJQUFMLENBQVUwQixnQkFBVixJQUE4QixFQUFuRztBQUNBO0FBQ0Q7QUFDRDtBQUNELE9BQUksS0FBSzdCLElBQVQsRUFBZTtBQUNkL0MsWUFBUXBCLFFBQVIsQ0FBa0IsQ0FBQyxLQUFLZ0YsS0FBUCxHQUFnQixLQUFLaEMsQ0FBckIsR0FBeUJBLENBQTFDLEVBQThDLENBQUMsS0FBS2tDLEtBQVAsR0FBZ0IsS0FBS2hDLENBQXJCLEdBQXlCQSxDQUF0RTtBQUNBLElBRkQsTUFFTztBQUNOLFFBQUksQ0FBQyxLQUFLZ0MsS0FBVixFQUFpQjtBQUNoQixVQUFLZCxPQUFMLENBQWEzRCxTQUFiLEdBQXlCLEtBQUt5QyxDQUE5QjtBQUNBO0FBQ0QsUUFBSSxDQUFDLEtBQUs4QixLQUFWLEVBQWlCO0FBQ2hCLFVBQUtaLE9BQUwsQ0FBYTZCLFVBQWIsR0FBMEIsS0FBS2pELENBQS9CO0FBQ0E7QUFDRDtBQUNELFFBQUsyQixLQUFMLEdBQWEsS0FBSzNCLENBQWxCO0FBQ0EsUUFBSzRCLEtBQUwsR0FBYSxLQUFLMUIsQ0FBbEI7QUFDQTs7QUFuRnlDLEVBQTFCLENBakRsQjtBQUFBLEtBdUlDWixJQUFJcUIsZUFBZXVDLFNBdklwQjs7QUF5SUF2QyxnQkFBZS9CLEdBQWYsR0FBcUJQLElBQXJCO0FBQ0FzQyxnQkFBZXdDLFNBQWYsR0FBMkIxRCxVQUEzQjtBQUNBa0IsZ0JBQWV5QyxXQUFmLEdBQTZCaEUsWUFBN0I7QUFDQXVCLGdCQUFlZ0MsaUJBQWYsR0FBbUMsQ0FBbkM7O0FBRUFyRCxHQUFFK0QsS0FBRixHQUFVLFVBQVNDLE1BQVQsRUFBaUI7QUFDMUIsTUFBSUEsT0FBT0MsVUFBWCxFQUF1QjtBQUN0QixRQUFLdkIsS0FBTCxHQUFhLElBQWI7QUFDQTtBQUNELE1BQUlzQixPQUFPRSxVQUFYLEVBQXVCO0FBQ3RCLFFBQUt0QixLQUFMLEdBQWEsSUFBYjtBQUNBO0FBQ0QsU0FBTyxLQUFLRyxNQUFMLENBQVlnQixLQUFaLENBQWtCZCxJQUFsQixDQUF1QixJQUF2QixFQUE2QmUsTUFBN0IsQ0FBUDtBQUNBLEVBUkQ7QUFVQSxDQTVKRCxFQTRKSSxJQUFJM0YsU0FBU2lELFNBQWIsRUFBd0I7QUFBRWpELFVBQVNJLFFBQVQsQ0FBa0IwRixHQUFsQjtBQUE0Qjs7QUFFMUQ7QUFDQyxXQUFTQyxJQUFULEVBQWU7QUFDZjs7QUFDQSxLQUFJQyxZQUFZLFNBQVpBLFNBQVksR0FBVztBQUMxQixTQUFPLENBQUNoRyxTQUFTb0IsZ0JBQVQsSUFBNkJwQixRQUE5QixFQUF3QytGLElBQXhDLENBQVA7QUFDQSxFQUZEO0FBR0EsS0FBSSxPQUFPOUYsTUFBUCxLQUFtQixXQUFuQixJQUFrQ0EsT0FBT0MsT0FBN0MsRUFBc0Q7QUFBRTtBQUN2RCtGLEVBQUEsbUJBQUFBLENBQVEsRUFBUjtBQUNBaEcsU0FBT0MsT0FBUCxHQUFpQjhGLFdBQWpCO0FBQ0EsRUFIRCxNQUdPLElBQUksSUFBSixFQUFpRDtBQUFFO0FBQ3pERSxFQUFBLGlDQUFPLENBQUMsdUJBQUQsQ0FBUCxvQ0FBc0JGLFNBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDRCxDQVhBLEVBV0MsZ0JBWEQsQ0FBRCxDOzs7Ozs7Ozs7QUMzS0E7O0FBRUE7Ozs7O0FBS0E7QUFDQTtBQUNBOzs7Ozs7O0FBRUE7Ozs7OztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7OztBQUlBLFNBQVNHLFVBQVQsQ0FBcUJwSCxTQUFyQixFQUFnQ3FILFlBQWhDLEVBQThDO0FBQzdDLDhCQUFhckgsU0FBYjtBQUNBOztBQUVEO0FBQ0E7QUFDQTs7UUFFUW9ILFUsR0FBQUEsVTs7Ozs7Ozs7Ozs7O0FDN0JSOzs7Ozs7Ozs7OztBQVdBLENBQUMsVUFBU3hILE1BQVQsRUFBaUIwSCxVQUFqQixFQUE2Qjs7QUFFNUI7O0FBQ0EsS0FBSUMsV0FBVyxFQUFmO0FBQUEsS0FDQ2hHLE9BQU8zQixPQUFPNEIsUUFEZjtBQUFBLEtBRUNnRyxXQUFXNUgsT0FBT3lDLGdCQUFQLEdBQTBCekMsT0FBT3lDLGdCQUFQLElBQTJCekMsTUFGakU7QUFHQSxLQUFJNEgsU0FBU2xGLFNBQWIsRUFBd0I7QUFDdkIsU0FEdUIsQ0FDZjtBQUNSO0FBQ0QsS0FBSW1GLGFBQWEsU0FBYkEsVUFBYSxDQUFTQyxFQUFULEVBQWE7QUFDNUIsTUFBSUMsSUFBSUQsR0FBR0UsS0FBSCxDQUFTLEdBQVQsQ0FBUjtBQUFBLE1BQ0NoRixJQUFJNEUsUUFETDtBQUFBLE1BQ2VLLENBRGY7QUFFQSxPQUFLQSxJQUFJLENBQVQsRUFBWUEsSUFBSUYsRUFBRTlHLE1BQWxCLEVBQTBCZ0gsR0FBMUIsRUFBK0I7QUFDOUJqRixLQUFFK0UsRUFBRUUsQ0FBRixDQUFGLElBQVVqRixJQUFJQSxFQUFFK0UsRUFBRUUsQ0FBRixDQUFGLEtBQVcsRUFBekI7QUFDQTtBQUNELFNBQU9qRixDQUFQO0FBQ0EsRUFQRjtBQUFBLEtBUUNrRixLQUFLTCxXQUFXLGVBQVgsQ0FSTjtBQUFBLEtBU0NNLFdBQVcsWUFUWjtBQUFBLEtBVUNDLFNBQVMsU0FBVEEsTUFBUyxDQUFTTCxDQUFULEVBQVk7QUFBRTtBQUN0QixNQUFJTSxJQUFJLEVBQVI7QUFBQSxNQUNDQyxJQUFJUCxFQUFFOUcsTUFEUDtBQUFBLE1BRUNnSCxDQUZEO0FBR0EsT0FBS0EsSUFBSSxDQUFULEVBQVlBLE1BQU1LLENBQWxCLEVBQXFCRCxFQUFFM0csSUFBRixDQUFPcUcsRUFBRUUsR0FBRixDQUFQLENBQXJCLEVBQXFDLENBQUU7QUFDdkMsU0FBT0ksQ0FBUDtBQUNBLEVBaEJGO0FBQUEsS0FpQkNFLGFBQWEsU0FBYkEsVUFBYSxHQUFXLENBQUUsQ0FqQjNCO0FBQUEsS0FrQkNDLFdBQVksWUFBVztBQUFFO0FBQ3hCLE1BQUlDLFdBQVdDLE9BQU85QixTQUFQLENBQWlCNkIsUUFBaEM7QUFBQSxNQUNDRSxRQUFRRixTQUFTeEMsSUFBVCxDQUFjLEVBQWQsQ0FEVDtBQUVBLFNBQU8sVUFBUzJDLEdBQVQsRUFBYztBQUNwQixVQUFPQSxPQUFPLElBQVAsS0FBZ0JBLGVBQWVDLEtBQWYsSUFBeUIsUUFBT0QsR0FBUCx5Q0FBT0EsR0FBUCxPQUFnQixRQUFoQixJQUE0QixDQUFDLENBQUNBLElBQUlsSCxJQUFsQyxJQUEwQytHLFNBQVN4QyxJQUFULENBQWMyQyxHQUFkLE1BQXVCRCxLQUExRyxDQUFQO0FBQ0EsR0FGRDtBQUdBLEVBTlcsRUFsQmI7QUFBQSxLQXlCQ1osQ0F6QkQ7QUFBQSxLQXlCSUUsQ0F6Qko7QUFBQSxLQXlCT2pGLENBekJQO0FBQUEsS0F5QlU4RixPQXpCVjtBQUFBLEtBeUJtQkMsYUF6Qm5CO0FBQUEsS0EwQkNDLGFBQWEsRUExQmQ7OztBQTRCQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUNBQyxjQUFhLFNBQWJBLFVBQWEsQ0FBU25CLEVBQVQsRUFBYW9CLFlBQWIsRUFBMkJDLElBQTNCLEVBQWlDM0gsTUFBakMsRUFBeUM7QUFDckQsT0FBSzRILEVBQUwsR0FBV0osV0FBV2xCLEVBQVgsQ0FBRCxHQUFtQmtCLFdBQVdsQixFQUFYLEVBQWVzQixFQUFsQyxHQUF1QyxFQUFqRCxDQURxRCxDQUNBO0FBQ3JESixhQUFXbEIsRUFBWCxJQUFpQixJQUFqQjtBQUNBLE9BQUt1QixPQUFMLEdBQWUsSUFBZjtBQUNBLE9BQUtGLElBQUwsR0FBWUEsSUFBWjtBQUNBLE1BQUlHLFdBQVcsRUFBZjtBQUNBLE9BQUtDLEtBQUwsR0FBYSxVQUFTNUUsSUFBVCxFQUFlO0FBQzNCLE9BQUlzRCxJQUFJaUIsYUFBYWpJLE1BQXJCO0FBQUEsT0FDQ3VJLFVBQVV2QixDQURYO0FBQUEsT0FFQ3dCLEdBRkQ7QUFBQSxPQUVNMUIsQ0FGTjtBQUFBLE9BRVMyQixDQUZUO0FBQUEsT0FFWUMsRUFGWjtBQUdBLFVBQU8sRUFBRTFCLENBQUYsR0FBTSxDQUFDLENBQWQsRUFBaUI7QUFDaEIsUUFBSSxDQUFDd0IsTUFBTVQsV0FBV0UsYUFBYWpCLENBQWIsQ0FBWCxLQUErQixJQUFJZ0IsVUFBSixDQUFlQyxhQUFhakIsQ0FBYixDQUFmLEVBQWdDLEVBQWhDLENBQXRDLEVBQTJFb0IsT0FBL0UsRUFBd0Y7QUFDdkZDLGNBQVNyQixDQUFULElBQWN3QixJQUFJSixPQUFsQjtBQUNBRztBQUNBLEtBSEQsTUFHTyxJQUFJN0UsSUFBSixFQUFVO0FBQ2hCOEUsU0FBSUwsRUFBSixDQUFPMUgsSUFBUCxDQUFZLElBQVo7QUFDQTtBQUNEO0FBQ0QsT0FBSThILFlBQVksQ0FBWixJQUFpQkwsSUFBckIsRUFBMkI7QUFDMUJwQixRQUFJLENBQUMsbUJBQW1CRCxFQUFwQixFQUF3QkUsS0FBeEIsQ0FBOEIsR0FBOUIsQ0FBSjtBQUNBMEIsUUFBSTNCLEVBQUVaLEdBQUYsRUFBSjtBQUNBd0MsU0FBSzlCLFdBQVdFLEVBQUU2QixJQUFGLENBQU8sR0FBUCxDQUFYLEVBQXdCRixDQUF4QixJQUE2QixLQUFLTCxPQUFMLEdBQWVGLEtBQUszQyxLQUFMLENBQVcyQyxJQUFYLEVBQWlCRyxRQUFqQixDQUFqRDs7QUFFQTtBQUNBLFFBQUk5SCxNQUFKLEVBQVk7QUFDWG9HLGNBQVM4QixDQUFULElBQWMvQixTQUFTK0IsQ0FBVCxJQUFjQyxFQUE1QixDQURXLENBQ3FCO0FBQ2hDLFNBQUksT0FBT3JJLE1BQVAsS0FBbUIsV0FBbkIsSUFBa0NBLE9BQU9DLE9BQTdDLEVBQXNEO0FBQUU7QUFDdkQsVUFBSXVHLE9BQU9KLFVBQVgsRUFBdUI7QUFDdEJwRyxjQUFPQyxPQUFQLEdBQWlCb0csU0FBU0QsVUFBVCxJQUF1QmlDLEVBQXhDO0FBQ0EsWUFBSzFCLENBQUwsSUFBVU4sUUFBVixFQUFvQjtBQUNuQmdDLFdBQUcxQixDQUFILElBQVFOLFNBQVNNLENBQVQsQ0FBUjtBQUNBO0FBQ0QsT0FMRCxNQUtPLElBQUlOLFNBQVNELFVBQVQsQ0FBSixFQUEwQjtBQUNoQ0MsZ0JBQVNELFVBQVQsRUFBcUJnQyxDQUFyQixJQUEwQkMsRUFBMUI7QUFDQTtBQUNELE1BVEQsTUFTTyxJQUFJLElBQUosRUFBZ0Q7QUFBRTtBQUN4RHBDLE1BQUEsaUNBQTZGLEVBQTdGLG1DQUFpRyxZQUFXO0FBQUUsY0FBT29DLEVBQVA7QUFBWSxPQUExSDtBQUFBO0FBQ0E7QUFDRDtBQUNELFNBQUsxQixJQUFJLENBQVQsRUFBWUEsSUFBSSxLQUFLbUIsRUFBTCxDQUFRbkksTUFBeEIsRUFBZ0NnSCxHQUFoQyxFQUFxQztBQUNwQyxVQUFLbUIsRUFBTCxDQUFRbkIsQ0FBUixFQUFXc0IsS0FBWDtBQUNBO0FBQ0Q7QUFDRCxHQXJDRDtBQXNDQSxPQUFLQSxLQUFMLENBQVcsSUFBWDtBQUNBLEVBMUdGOzs7QUE0R0M7QUFDQWpGLGFBQVl0RSxPQUFPc0UsU0FBUCxHQUFtQixVQUFTd0QsRUFBVCxFQUFhb0IsWUFBYixFQUEyQkMsSUFBM0IsRUFBaUMzSCxNQUFqQyxFQUF5QztBQUN2RSxTQUFPLElBQUl5SCxVQUFKLENBQWVuQixFQUFmLEVBQW1Cb0IsWUFBbkIsRUFBaUNDLElBQWpDLEVBQXVDM0gsTUFBdkMsQ0FBUDtBQUNBLEVBL0dGOzs7QUFpSEM7QUFDQXFJLFVBQVMzQixHQUFHMkIsTUFBSCxHQUFZLFVBQVMvQixFQUFULEVBQWFxQixJQUFiLEVBQW1CM0gsTUFBbkIsRUFBMkI7QUFDL0MySCxTQUFPQSxRQUFRLFlBQVcsQ0FBRSxDQUE1QjtBQUNBN0UsWUFBVXdELEVBQVYsRUFBYyxFQUFkLEVBQWtCLFlBQVU7QUFBRSxVQUFPcUIsSUFBUDtBQUFjLEdBQTVDLEVBQThDM0gsTUFBOUM7QUFDQSxTQUFPMkgsSUFBUDtBQUNBLEVBdEhGOztBQXdIQTdFLFdBQVV3RixPQUFWLEdBQW9CbEMsUUFBcEI7O0FBSUY7Ozs7O0FBS0UsS0FBSW1DLGNBQWMsQ0FBQyxDQUFELEVBQUksQ0FBSixFQUFPLENBQVAsRUFBVSxDQUFWLENBQWxCO0FBQUEsS0FDQ0MsT0FBT0gsT0FBTyxhQUFQLEVBQXNCLFVBQVNWLElBQVQsRUFBZWMsV0FBZixFQUE0QmpHLElBQTVCLEVBQWtDa0csS0FBbEMsRUFBeUM7QUFDckUsT0FBS0MsS0FBTCxHQUFhaEIsSUFBYjtBQUNBLE9BQUtpQixLQUFMLEdBQWFwRyxRQUFRLENBQXJCO0FBQ0EsT0FBS3FHLE1BQUwsR0FBY0gsU0FBUyxDQUF2QjtBQUNBLE9BQUtJLE9BQUwsR0FBZUwsY0FBY0YsWUFBWVEsTUFBWixDQUFtQk4sV0FBbkIsQ0FBZCxHQUFnREYsV0FBL0Q7QUFDQSxFQUxNLEVBS0osSUFMSSxDQURSO0FBQUEsS0FPQ1MsV0FBV1IsS0FBS1MsR0FBTCxHQUFXLEVBUHZCO0FBQUEsS0FRQ0MsV0FBV1YsS0FBS1csUUFBTCxHQUFnQixVQUFTaEssSUFBVCxFQUFlaUssS0FBZixFQUFzQkMsS0FBdEIsRUFBNkJDLE1BQTdCLEVBQXFDO0FBQy9ELE1BQUlDLEtBQUtILE1BQU01QyxLQUFOLENBQVksR0FBWixDQUFUO0FBQUEsTUFDQ0MsSUFBSThDLEdBQUc5SixNQURSO0FBQUEsTUFFQytKLEtBQUssQ0FBQ0gsU0FBUywwQkFBVixFQUFzQzdDLEtBQXRDLENBQTRDLEdBQTVDLENBRk47QUFBQSxNQUdDakYsQ0FIRDtBQUFBLE1BR0lxRSxJQUhKO0FBQUEsTUFHVTZELENBSFY7QUFBQSxNQUdhakgsSUFIYjtBQUlBLFNBQU8sRUFBRWlFLENBQUYsR0FBTSxDQUFDLENBQWQsRUFBaUI7QUFDaEJiLFVBQU8yRCxHQUFHOUMsQ0FBSCxDQUFQO0FBQ0FsRixPQUFJK0gsU0FBU2pCLE9BQU8sWUFBVXpDLElBQWpCLEVBQXVCLElBQXZCLEVBQTZCLElBQTdCLENBQVQsR0FBOENjLEdBQUdnRCxNQUFILENBQVU5RCxJQUFWLEtBQW1CLEVBQXJFO0FBQ0E2RCxPQUFJRCxHQUFHL0osTUFBUDtBQUNBLFVBQU8sRUFBRWdLLENBQUYsR0FBTSxDQUFDLENBQWQsRUFBaUI7QUFDaEJqSCxXQUFPZ0gsR0FBR0MsQ0FBSCxDQUFQO0FBQ0FULGFBQVNwRCxPQUFPLEdBQVAsR0FBYXBELElBQXRCLElBQThCd0csU0FBU3hHLE9BQU9vRCxJQUFoQixJQUF3QnJFLEVBQUVpQixJQUFGLElBQVVyRCxLQUFLd0ssUUFBTCxHQUFnQnhLLElBQWhCLEdBQXVCQSxLQUFLcUQsSUFBTCxLQUFjLElBQUlyRCxJQUFKLEVBQXJHO0FBQ0E7QUFDRDtBQUNELEVBdEJGOztBQXdCQXFDLEtBQUlnSCxLQUFLcEQsU0FBVDtBQUNBNUQsR0FBRW9JLFFBQUYsR0FBYSxLQUFiO0FBQ0FwSSxHQUFFbUksUUFBRixHQUFhLFVBQVNuSSxDQUFULEVBQVk7QUFDeEIsTUFBSSxLQUFLbUgsS0FBVCxFQUFnQjtBQUNmLFFBQUtHLE9BQUwsQ0FBYSxDQUFiLElBQWtCdEgsQ0FBbEI7QUFDQSxVQUFPLEtBQUttSCxLQUFMLENBQVczRCxLQUFYLENBQWlCLElBQWpCLEVBQXVCLEtBQUs4RCxPQUE1QixDQUFQO0FBQ0E7QUFDRCxNQUFJZSxJQUFJLEtBQUtqQixLQUFiO0FBQUEsTUFDQ2tCLEtBQUssS0FBS2pCLE1BRFg7QUFBQSxNQUVDa0IsSUFBS0YsTUFBTSxDQUFQLEdBQVksSUFBSXJJLENBQWhCLEdBQXFCcUksTUFBTSxDQUFQLEdBQVlySSxDQUFaLEdBQWlCQSxJQUFJLEdBQUwsR0FBWUEsSUFBSSxDQUFoQixHQUFvQixDQUFDLElBQUlBLENBQUwsSUFBVSxDQUZ2RTtBQUdBLE1BQUlzSSxPQUFPLENBQVgsRUFBYztBQUNiQyxRQUFLQSxDQUFMO0FBQ0EsR0FGRCxNQUVPLElBQUlELE9BQU8sQ0FBWCxFQUFjO0FBQ3BCQyxRQUFLQSxJQUFJQSxDQUFUO0FBQ0EsR0FGTSxNQUVBLElBQUlELE9BQU8sQ0FBWCxFQUFjO0FBQ3BCQyxRQUFLQSxJQUFJQSxDQUFKLEdBQVFBLENBQWI7QUFDQSxHQUZNLE1BRUEsSUFBSUQsT0FBTyxDQUFYLEVBQWM7QUFDcEJDLFFBQUtBLElBQUlBLENBQUosR0FBUUEsQ0FBUixHQUFZQSxDQUFqQjtBQUNBO0FBQ0QsU0FBUUYsTUFBTSxDQUFQLEdBQVksSUFBSUUsQ0FBaEIsR0FBcUJGLE1BQU0sQ0FBUCxHQUFZRSxDQUFaLEdBQWlCdkksSUFBSSxHQUFMLEdBQVl1SSxJQUFJLENBQWhCLEdBQW9CLElBQUtBLElBQUksQ0FBeEU7QUFDQSxFQWxCRDs7QUFvQkE7QUFDQXhELEtBQUksQ0FBQyxRQUFELEVBQVUsTUFBVixFQUFpQixPQUFqQixFQUF5QixPQUF6QixFQUFpQyxjQUFqQyxDQUFKO0FBQ0FFLEtBQUlGLEVBQUU5RyxNQUFOO0FBQ0EsUUFBTyxFQUFFZ0gsQ0FBRixHQUFNLENBQUMsQ0FBZCxFQUFpQjtBQUNoQmpGLE1BQUkrRSxFQUFFRSxDQUFGLElBQUssUUFBTCxHQUFjQSxDQUFsQjtBQUNBeUMsV0FBUyxJQUFJVixJQUFKLENBQVMsSUFBVCxFQUFjLElBQWQsRUFBbUIsQ0FBbkIsRUFBcUIvQixDQUFyQixDQUFULEVBQWtDakYsQ0FBbEMsRUFBcUMsU0FBckMsRUFBZ0QsSUFBaEQ7QUFDQTBILFdBQVMsSUFBSVYsSUFBSixDQUFTLElBQVQsRUFBYyxJQUFkLEVBQW1CLENBQW5CLEVBQXFCL0IsQ0FBckIsQ0FBVCxFQUFrQ2pGLENBQWxDLEVBQXFDLFlBQWFpRixNQUFNLENBQVAsR0FBWSxXQUFaLEdBQTBCLEVBQXRDLENBQXJDO0FBQ0F5QyxXQUFTLElBQUlWLElBQUosQ0FBUyxJQUFULEVBQWMsSUFBZCxFQUFtQixDQUFuQixFQUFxQi9CLENBQXJCLENBQVQsRUFBa0NqRixDQUFsQyxFQUFxQyxXQUFyQztBQUNBO0FBQ0R3SCxVQUFTZ0IsTUFBVCxHQUFrQnRELEdBQUdnRCxNQUFILENBQVVPLE1BQVYsQ0FBaUJDLE1BQW5DO0FBQ0FsQixVQUFTbUIsS0FBVCxHQUFpQnpELEdBQUdnRCxNQUFILENBQVVVLElBQVYsQ0FBZUMsU0FBaEMsQ0FsTTRCLENBa01lOzs7QUFHN0M7Ozs7O0FBS0UsS0FBSUMsa0JBQWtCakMsT0FBTyx3QkFBUCxFQUFpQyxVQUFTOUYsTUFBVCxFQUFpQjtBQUN2RSxPQUFLZ0ksVUFBTCxHQUFrQixFQUFsQjtBQUNBLE9BQUtDLFlBQUwsR0FBb0JqSSxVQUFVLElBQTlCO0FBQ0EsRUFIcUIsQ0FBdEI7QUFJQWYsS0FBSThJLGdCQUFnQmxGLFNBQXBCOztBQUVBNUQsR0FBRWlKLGdCQUFGLEdBQXFCLFVBQVNqSSxJQUFULEVBQWVrSSxRQUFmLEVBQXlCQyxLQUF6QixFQUFnQ0MsUUFBaEMsRUFBMENDLFFBQTFDLEVBQW9EO0FBQ3hFQSxhQUFXQSxZQUFZLENBQXZCO0FBQ0EsTUFBSUMsT0FBTyxLQUFLUCxVQUFMLENBQWdCL0gsSUFBaEIsQ0FBWDtBQUFBLE1BQ0N1SSxRQUFRLENBRFQ7QUFBQSxNQUVDQyxRQUZEO0FBQUEsTUFFV3ZFLENBRlg7QUFHQSxNQUFJLFNBQVNhLE9BQVQsSUFBb0IsQ0FBQ0MsYUFBekIsRUFBd0M7QUFDdkNELFdBQVEyRCxJQUFSO0FBQ0E7QUFDRCxNQUFJSCxRQUFRLElBQVosRUFBa0I7QUFDakIsUUFBS1AsVUFBTCxDQUFnQi9ILElBQWhCLElBQXdCc0ksT0FBTyxFQUEvQjtBQUNBO0FBQ0RyRSxNQUFJcUUsS0FBS3JMLE1BQVQ7QUFDQSxTQUFPLEVBQUVnSCxDQUFGLEdBQU0sQ0FBQyxDQUFkLEVBQWlCO0FBQ2hCdUUsY0FBV0YsS0FBS3JFLENBQUwsQ0FBWDtBQUNBLE9BQUl1RSxTQUFTRSxDQUFULEtBQWVSLFFBQWYsSUFBMkJNLFNBQVNHLENBQVQsS0FBZVIsS0FBOUMsRUFBcUQ7QUFDcERHLFNBQUtNLE1BQUwsQ0FBWTNFLENBQVosRUFBZSxDQUFmO0FBQ0EsSUFGRCxNQUVPLElBQUlzRSxVQUFVLENBQVYsSUFBZUMsU0FBU0ssRUFBVCxHQUFjUixRQUFqQyxFQUEyQztBQUNqREUsWUFBUXRFLElBQUksQ0FBWjtBQUNBO0FBQ0Q7QUFDRHFFLE9BQUtNLE1BQUwsQ0FBWUwsS0FBWixFQUFtQixDQUFuQixFQUFzQixFQUFDRyxHQUFFUixRQUFILEVBQWFTLEdBQUVSLEtBQWYsRUFBc0JXLElBQUdWLFFBQXpCLEVBQW1DUyxJQUFHUixRQUF0QyxFQUF0QjtBQUNBLEVBckJEOztBQXVCQXJKLEdBQUUrSixtQkFBRixHQUF3QixVQUFTL0ksSUFBVCxFQUFla0ksUUFBZixFQUF5QjtBQUNoRCxNQUFJSSxPQUFPLEtBQUtQLFVBQUwsQ0FBZ0IvSCxJQUFoQixDQUFYO0FBQUEsTUFBa0NpRSxDQUFsQztBQUNBLE1BQUlxRSxJQUFKLEVBQVU7QUFDVHJFLE9BQUlxRSxLQUFLckwsTUFBVDtBQUNBLFVBQU8sRUFBRWdILENBQUYsR0FBTSxDQUFDLENBQWQsRUFBaUI7QUFDaEIsUUFBSXFFLEtBQUtyRSxDQUFMLEVBQVF5RSxDQUFSLEtBQWNSLFFBQWxCLEVBQTRCO0FBQzNCSSxVQUFLTSxNQUFMLENBQVkzRSxDQUFaLEVBQWUsQ0FBZjtBQUNBO0FBQ0E7QUFDRDtBQUNEO0FBQ0QsRUFYRDs7QUFhQWpGLEdBQUVnSyxhQUFGLEdBQWtCLFVBQVNoSixJQUFULEVBQWU7QUFDaEMsTUFBSXNJLE9BQU8sS0FBS1AsVUFBTCxDQUFnQi9ILElBQWhCLENBQVg7QUFBQSxNQUNDaUUsQ0FERDtBQUFBLE1BQ0lvRCxDQURKO0FBQUEsTUFDT21CLFFBRFA7QUFFQSxNQUFJRixJQUFKLEVBQVU7QUFDVHJFLE9BQUlxRSxLQUFLckwsTUFBVDtBQUNBLE9BQUlnSCxJQUFJLENBQVIsRUFBVztBQUNWcUUsV0FBT0EsS0FBS1csS0FBTCxDQUFXLENBQVgsQ0FBUCxDQURVLENBQ1k7QUFDdEI7QUFDRDVCLE9BQUksS0FBS1csWUFBVDtBQUNBLFVBQU8sRUFBRS9ELENBQUYsR0FBTSxDQUFDLENBQWQsRUFBaUI7QUFDaEJ1RSxlQUFXRixLQUFLckUsQ0FBTCxDQUFYO0FBQ0EsUUFBSXVFLFFBQUosRUFBYztBQUNiLFNBQUlBLFNBQVNNLEVBQWIsRUFBaUI7QUFDaEJOLGVBQVNFLENBQVQsQ0FBV3pHLElBQVgsQ0FBZ0J1RyxTQUFTRyxDQUFULElBQWN0QixDQUE5QixFQUFpQyxFQUFDckgsTUFBS0EsSUFBTixFQUFZRCxRQUFPc0gsQ0FBbkIsRUFBakM7QUFDQSxNQUZELE1BRU87QUFDTm1CLGVBQVNFLENBQVQsQ0FBV3pHLElBQVgsQ0FBZ0J1RyxTQUFTRyxDQUFULElBQWN0QixDQUE5QjtBQUNBO0FBQ0Q7QUFDRDtBQUNEO0FBQ0QsRUFwQkQ7O0FBdUJGOzs7OztBQUtHLEtBQUk2QixnQkFBZ0JsTixPQUFPbU4scUJBQTNCO0FBQUEsS0FDQUMsbUJBQW1CcE4sT0FBT3FOLG9CQUQxQjtBQUFBLEtBRUFDLFdBQVdDLEtBQUtDLEdBQUwsSUFBWSxZQUFXO0FBQUMsU0FBTyxJQUFJRCxJQUFKLEdBQVdFLE9BQVgsRUFBUDtBQUE2QixFQUZoRTtBQUFBLEtBR0FDLGNBQWNKLFVBSGQ7O0FBS0Q7QUFDQXZGLEtBQUksQ0FBQyxJQUFELEVBQU0sS0FBTixFQUFZLFFBQVosRUFBcUIsR0FBckIsQ0FBSjtBQUNBRSxLQUFJRixFQUFFOUcsTUFBTjtBQUNBLFFBQU8sRUFBRWdILENBQUYsR0FBTSxDQUFDLENBQVAsSUFBWSxDQUFDaUYsYUFBcEIsRUFBbUM7QUFDbENBLGtCQUFnQmxOLE9BQU8rSCxFQUFFRSxDQUFGLElBQU8sdUJBQWQsQ0FBaEI7QUFDQW1GLHFCQUFtQnBOLE9BQU8rSCxFQUFFRSxDQUFGLElBQU8sc0JBQWQsS0FBeUNqSSxPQUFPK0gsRUFBRUUsQ0FBRixJQUFPLDZCQUFkLENBQTVEO0FBQ0E7O0FBRUQ0QixRQUFPLFFBQVAsRUFBaUIsVUFBUzhELEdBQVQsRUFBY0MsTUFBZCxFQUFzQjtBQUN0QyxNQUFJQyxRQUFRLElBQVo7QUFBQSxNQUNDQyxhQUFhUixVQURkO0FBQUEsTUFFQ1MsVUFBV0gsV0FBVyxLQUFYLElBQW9CVixhQUFyQixHQUFzQyxNQUF0QyxHQUErQyxLQUYxRDtBQUFBLE1BR0NjLGdCQUFnQixHQUhqQjtBQUFBLE1BSUNDLGVBQWUsRUFKaEI7QUFBQSxNQUtDQyxZQUFZLE1BTGI7QUFBQSxNQUtxQjtBQUNwQkMsTUFORDtBQUFBLE1BTU9DLElBTlA7QUFBQSxNQU1hQyxHQU5iO0FBQUEsTUFNa0JDLElBTmxCO0FBQUEsTUFNd0JDLFNBTnhCO0FBQUEsTUFPQ0MsUUFBUSxTQUFSQSxLQUFRLENBQVNDLE1BQVQsRUFBaUI7QUFDeEIsT0FBSUMsVUFBVXBCLGFBQWFJLFdBQTNCO0FBQUEsT0FDQ2lCLE9BREQ7QUFBQSxPQUNVQyxRQURWO0FBRUEsT0FBSUYsVUFBVVYsYUFBZCxFQUE2QjtBQUM1QkYsa0JBQWNZLFVBQVVULFlBQXhCO0FBQ0E7QUFDRFAsa0JBQWVnQixPQUFmO0FBQ0FiLFNBQU1nQixJQUFOLEdBQWEsQ0FBQ25CLGNBQWNJLFVBQWYsSUFBNkIsSUFBMUM7QUFDQWEsYUFBVWQsTUFBTWdCLElBQU4sR0FBYU4sU0FBdkI7QUFDQSxPQUFJLENBQUNKLElBQUQsSUFBU1EsVUFBVSxDQUFuQixJQUF3QkYsV0FBVyxJQUF2QyxFQUE2QztBQUM1Q1osVUFBTWlCLEtBQU47QUFDQVAsaUJBQWFJLFdBQVdBLFdBQVdMLElBQVgsR0FBa0IsS0FBbEIsR0FBMEJBLE9BQU9LLE9BQTVDLENBQWI7QUFDQUMsZUFBVyxJQUFYO0FBQ0E7QUFDRCxPQUFJSCxXQUFXLElBQWYsRUFBcUI7QUFBRTtBQUN0QkosVUFBTUQsS0FBS0ksS0FBTCxDQUFOO0FBQ0E7QUFDRCxPQUFJSSxRQUFKLEVBQWM7QUFDYmYsVUFBTWIsYUFBTixDQUFvQmtCLFNBQXBCO0FBQ0E7QUFDRCxHQTNCRjs7QUE2QkFwQyxrQkFBZ0I3RixJQUFoQixDQUFxQjRILEtBQXJCO0FBQ0FBLFFBQU1nQixJQUFOLEdBQWFoQixNQUFNaUIsS0FBTixHQUFjLENBQTNCO0FBQ0FqQixRQUFNa0IsSUFBTixHQUFhLFlBQVc7QUFDdkJQLFNBQU0sSUFBTjtBQUNBLEdBRkQ7O0FBSUFYLFFBQU1tQixZQUFOLEdBQXFCLFVBQVM1SSxTQUFULEVBQW9CNkksV0FBcEIsRUFBaUM7QUFDckQsT0FBSSxDQUFDQyxVQUFVak8sTUFBZixFQUF1QjtBQUFFO0FBQ3hCLFdBQVErTSxnQkFBZ0IsSUFBSTdGLFFBQTVCO0FBQ0E7QUFDRDZGLG1CQUFnQjVILGFBQWMsSUFBSStCLFFBQWxDLENBSnFELENBSVI7QUFDN0M4RixrQkFBZTVMLEtBQUsrQixHQUFMLENBQVM2SyxXQUFULEVBQXNCakIsYUFBdEIsRUFBcUMsQ0FBckMsQ0FBZjtBQUNBLEdBTkQ7O0FBUUFILFFBQU1zQixLQUFOLEdBQWMsWUFBVztBQUN4QixPQUFJZCxPQUFPLElBQVgsRUFBaUI7QUFDaEI7QUFDQTtBQUNELE9BQUksQ0FBQ04sT0FBRCxJQUFZLENBQUNYLGdCQUFqQixFQUFtQztBQUNsQ2dDLGlCQUFhZixHQUFiO0FBQ0EsSUFGRCxNQUVPO0FBQ05qQixxQkFBaUJpQixHQUFqQjtBQUNBO0FBQ0RELFVBQU83RixVQUFQO0FBQ0E4RixTQUFNLElBQU47QUFDQSxPQUFJUixVQUFVL0UsT0FBZCxFQUF1QjtBQUN0QkMsb0JBQWdCLEtBQWhCO0FBQ0E7QUFDRCxHQWREOztBQWdCQThFLFFBQU1wQixJQUFOLEdBQWEsVUFBUzRDLFFBQVQsRUFBbUI7QUFDL0IsT0FBSWhCLFFBQVEsSUFBWixFQUFrQjtBQUNqQlIsVUFBTXNCLEtBQU47QUFDQSxJQUZELE1BRU8sSUFBSUUsUUFBSixFQUFjO0FBQ3BCdkIsa0JBQWMsQ0FBQ0osV0FBRCxJQUFnQkEsY0FBY0osVUFBOUIsQ0FBZDtBQUNBLElBRk0sTUFFQSxJQUFJTyxNQUFNaUIsS0FBTixHQUFjLEVBQWxCLEVBQXNCO0FBQUU7QUFDOUJwQixrQkFBY0osYUFBYVUsYUFBYixHQUE2QixDQUEzQztBQUNBO0FBQ0RJLFVBQVFELFNBQVMsQ0FBVixHQUFlNUYsVUFBZixHQUE2QixDQUFDd0YsT0FBRCxJQUFZLENBQUNiLGFBQWQsR0FBK0IsVUFBU29DLENBQVQsRUFBWTtBQUFFLFdBQU9DLFdBQVdELENBQVgsRUFBZSxDQUFDZixZQUFZVixNQUFNZ0IsSUFBbkIsSUFBMkIsSUFBM0IsR0FBa0MsQ0FBbkMsR0FBd0MsQ0FBdEQsQ0FBUDtBQUFrRSxJQUEvRyxHQUFrSDNCLGFBQXJKO0FBQ0EsT0FBSVcsVUFBVS9FLE9BQWQsRUFBdUI7QUFDdEJDLG9CQUFnQixJQUFoQjtBQUNBO0FBQ0R5RixTQUFNLENBQU47QUFDQSxHQWJEOztBQWVBWCxRQUFNRixHQUFOLEdBQVksVUFBU25MLEtBQVQsRUFBZ0I7QUFDM0IsT0FBSSxDQUFDME0sVUFBVWpPLE1BQWYsRUFBdUI7QUFDdEIsV0FBT2tOLElBQVA7QUFDQTtBQUNEQSxVQUFPM0wsS0FBUDtBQUNBOEwsVUFBTyxLQUFLSCxRQUFRLEVBQWIsQ0FBUDtBQUNBSSxlQUFZLEtBQUtNLElBQUwsR0FBWVAsSUFBeEI7QUFDQVQsU0FBTXBCLElBQU47QUFDQSxHQVJEOztBQVVBb0IsUUFBTUQsTUFBTixHQUFlLFVBQVNwTCxLQUFULEVBQWdCO0FBQzlCLE9BQUksQ0FBQzBNLFVBQVVqTyxNQUFmLEVBQXVCO0FBQ3RCLFdBQU84TSxPQUFQO0FBQ0E7QUFDREYsU0FBTXNCLEtBQU47QUFDQXBCLGFBQVV2TCxLQUFWO0FBQ0FxTCxTQUFNRixHQUFOLENBQVVRLElBQVY7QUFDQSxHQVBEO0FBUUFOLFFBQU1GLEdBQU4sQ0FBVUEsR0FBVjs7QUFFQTtBQUNBNEIsYUFBVyxZQUFXO0FBQ3JCLE9BQUl4QixZQUFZLE1BQVosSUFBc0JGLE1BQU1pQixLQUFOLEdBQWMsQ0FBcEMsSUFBeUNuTixLQUFLNk4sZUFBTCxLQUF5QixRQUF0RSxFQUFnRjtBQUMvRTNCLFVBQU1ELE1BQU4sQ0FBYSxLQUFiO0FBQ0E7QUFDRCxHQUpELEVBSUcsSUFKSDtBQUtBLEVBckdEOztBQXVHQTVLLEtBQUlrRixHQUFHdUgsTUFBSCxDQUFVN0ksU0FBVixHQUFzQixJQUFJc0IsR0FBR3dILE1BQUgsQ0FBVTVELGVBQWQsRUFBMUI7QUFDQTlJLEdBQUUyTSxXQUFGLEdBQWdCekgsR0FBR3VILE1BQW5COztBQUdGOzs7OztBQUtFLEtBQUlHLFlBQVkvRixPQUFPLGdCQUFQLEVBQXlCLFVBQVNnRyxRQUFULEVBQW1CN0ssSUFBbkIsRUFBeUI7QUFDaEUsT0FBS0EsSUFBTCxHQUFZQSxPQUFPQSxRQUFRLEVBQTNCO0FBQ0EsT0FBSzhLLFNBQUwsR0FBaUIsS0FBS0MsY0FBTCxHQUFzQkYsWUFBWSxDQUFuRDtBQUNBLE9BQUtHLE1BQUwsR0FBY0MsT0FBT2pMLEtBQUtrTCxLQUFaLEtBQXNCLENBQXBDO0FBQ0EsT0FBS0MsVUFBTCxHQUFrQixDQUFsQjtBQUNBLE9BQUtDLE9BQUwsR0FBZ0JwTCxLQUFLcUwsZUFBTCxLQUF5QixJQUF6QztBQUNBLE9BQUs3UCxJQUFMLEdBQVl3RSxLQUFLeEUsSUFBakI7QUFDQSxPQUFLOFAsU0FBTCxHQUFrQnRMLEtBQUt1TCxRQUFMLEtBQWtCLElBQXBDOztBQUVBLE1BQUksQ0FBQ0MsYUFBTCxFQUFvQjtBQUNuQjtBQUNBO0FBQ0QsTUFBSSxDQUFDekgsYUFBTCxFQUFvQjtBQUFFO0FBQ3JCRCxXQUFRMkQsSUFBUjtBQUNBOztBQUVELE1BQUlnRSxLQUFLLEtBQUt6TCxJQUFMLENBQVUwTCxTQUFWLEdBQXNCQyxtQkFBdEIsR0FBNENILGFBQXJEO0FBQ0FDLEtBQUdHLEdBQUgsQ0FBTyxJQUFQLEVBQWFILEdBQUdJLEtBQWhCOztBQUVBLE1BQUksS0FBSzdMLElBQUwsQ0FBVThMLE1BQWQsRUFBc0I7QUFDckIsUUFBS0EsTUFBTCxDQUFZLElBQVo7QUFDQTtBQUNELEVBdEJjLENBQWhCOztBQXdCQWhJLFdBQVU4RyxVQUFVbUIsTUFBVixHQUFtQixJQUFJN0ksR0FBR3VILE1BQVAsRUFBN0I7QUFDQXpNLEtBQUk0TSxVQUFVaEosU0FBZDtBQUNBNUQsR0FBRWdPLE1BQUYsR0FBV2hPLEVBQUVpTyxHQUFGLEdBQVFqTyxFQUFFa08sUUFBRixHQUFhbE8sRUFBRW1PLE9BQUYsR0FBWSxLQUE1QztBQUNBbk8sR0FBRW9PLFVBQUYsR0FBZXBPLEVBQUU2TixLQUFGLEdBQVUsQ0FBekI7QUFDQTdOLEdBQUVxTyxZQUFGLEdBQWlCLENBQUMsQ0FBbEI7QUFDQXJPLEdBQUVzTyxLQUFGLEdBQVV0TyxFQUFFdU8sS0FBRixHQUFVdk8sRUFBRXdPLFNBQUYsR0FBY3hPLEVBQUV5TyxTQUFGLEdBQWN6TyxFQUFFME8sUUFBRixHQUFhLElBQTdEO0FBQ0ExTyxHQUFFbU8sT0FBRixHQUFZLEtBQVo7O0FBR0E7QUFDQSxLQUFJUSxnQkFBZ0IsU0FBaEJBLGFBQWdCLEdBQVc7QUFDN0IsTUFBSTVJLGlCQUFpQnVFLGFBQWFJLFdBQWIsR0FBMkIsSUFBNUMsS0FBcUQvTCxLQUFLNk4sZUFBTCxLQUF5QixRQUF6QixJQUFxQyxDQUFDMUcsUUFBUWtHLFlBQVIsRUFBM0YsQ0FBSixFQUF3SDtBQUFFO0FBQ3pIbEcsV0FBUTJELElBQVI7QUFDQTtBQUNELE1BQUlwQixJQUFJa0UsV0FBV29DLGFBQVgsRUFBMEIsSUFBMUIsQ0FBUjtBQUNBLE1BQUl0RyxFQUFFdUcsS0FBTixFQUFhO0FBQ1o7QUFDQXZHLEtBQUV1RyxLQUFGO0FBQ0E7QUFDRCxFQVRGO0FBVUFEOztBQUdBM08sR0FBRTZPLElBQUYsR0FBUyxVQUFTQyxJQUFULEVBQWVDLGNBQWYsRUFBK0I7QUFDdkMsTUFBSUQsUUFBUSxJQUFaLEVBQWtCO0FBQ2pCLFFBQUtFLElBQUwsQ0FBVUYsSUFBVixFQUFnQkMsY0FBaEI7QUFDQTtBQUNELFNBQU8sS0FBS3hCLFFBQUwsQ0FBYyxLQUFkLEVBQXFCTyxNQUFyQixDQUE0QixLQUE1QixDQUFQO0FBQ0EsRUFMRDs7QUFPQTlOLEdBQUVpUCxLQUFGLEdBQVUsVUFBU0MsTUFBVCxFQUFpQkgsY0FBakIsRUFBaUM7QUFDMUMsTUFBSUcsVUFBVSxJQUFkLEVBQW9CO0FBQ25CLFFBQUtGLElBQUwsQ0FBVUUsTUFBVixFQUFrQkgsY0FBbEI7QUFDQTtBQUNELFNBQU8sS0FBS2pCLE1BQUwsQ0FBWSxJQUFaLENBQVA7QUFDQSxFQUxEOztBQU9BOU4sR0FBRW1QLE1BQUYsR0FBVyxVQUFTTCxJQUFULEVBQWVDLGNBQWYsRUFBK0I7QUFDekMsTUFBSUQsUUFBUSxJQUFaLEVBQWtCO0FBQ2pCLFFBQUtFLElBQUwsQ0FBVUYsSUFBVixFQUFnQkMsY0FBaEI7QUFDQTtBQUNELFNBQU8sS0FBS2pCLE1BQUwsQ0FBWSxLQUFaLENBQVA7QUFDQSxFQUxEOztBQU9BOU4sR0FBRWdQLElBQUYsR0FBUyxVQUFTbkQsSUFBVCxFQUFla0QsY0FBZixFQUErQjtBQUN2QyxTQUFPLEtBQUtLLFNBQUwsQ0FBZW5DLE9BQU9wQixJQUFQLENBQWYsRUFBNkJrRCxtQkFBbUIsS0FBaEQsQ0FBUDtBQUNBLEVBRkQ7O0FBSUEvTyxHQUFFcVAsT0FBRixHQUFZLFVBQVNDLFlBQVQsRUFBdUJQLGNBQXZCLEVBQXVDO0FBQ2xELFNBQU8sS0FBS3hCLFFBQUwsQ0FBYyxLQUFkLEVBQXFCTyxNQUFyQixDQUE0QixLQUE1QixFQUFtQ3NCLFNBQW5DLENBQTZDRSxlQUFlLENBQUMsS0FBS3RDLE1BQXJCLEdBQThCLENBQTNFLEVBQStFK0IsbUJBQW1CLEtBQWxHLEVBQTBHLElBQTFHLENBQVA7QUFDQSxFQUZEOztBQUlBL08sR0FBRXVQLE9BQUYsR0FBWSxVQUFTVCxJQUFULEVBQWVDLGNBQWYsRUFBK0I7QUFDMUMsTUFBSUQsUUFBUSxJQUFaLEVBQWtCO0FBQ2pCLFFBQUtFLElBQUwsQ0FBV0YsUUFBUSxLQUFLVSxhQUFMLEVBQW5CLEVBQTBDVCxjQUExQztBQUNBO0FBQ0QsU0FBTyxLQUFLeEIsUUFBTCxDQUFjLElBQWQsRUFBb0JPLE1BQXBCLENBQTJCLEtBQTNCLENBQVA7QUFDQSxFQUxEOztBQU9BOU4sR0FBRXlQLE1BQUYsR0FBVyxVQUFTNUQsSUFBVCxFQUFla0QsY0FBZixFQUErQlcsS0FBL0IsRUFBc0M7QUFDaEQ7QUFDQSxFQUZEOztBQUlBMVAsR0FBRTJQLFVBQUYsR0FBZSxZQUFXO0FBQ3pCLE9BQUs5QixLQUFMLEdBQWEsS0FBS08sVUFBTCxHQUFrQixDQUEvQjtBQUNBLE9BQUtGLFFBQUwsR0FBZ0IsS0FBS0QsR0FBTCxHQUFXLEtBQTNCO0FBQ0EsT0FBS0ksWUFBTCxHQUFvQixDQUFDLENBQXJCO0FBQ0EsTUFBSSxLQUFLSixHQUFMLElBQVksQ0FBQyxLQUFLUyxRQUF0QixFQUFnQztBQUMvQixRQUFLa0IsUUFBTCxDQUFjLElBQWQ7QUFDQTtBQUNELFNBQU8sSUFBUDtBQUNBLEVBUkQ7O0FBVUE1UCxHQUFFNlAsUUFBRixHQUFhLFlBQVc7QUFDdkIsTUFBSXBDLEtBQUssS0FBS2dCLFNBQWQ7QUFBQSxNQUF5QjtBQUN4QnFCLGNBQVksS0FBS2hGLFVBRGxCO0FBQUEsTUFFQ2lGLE9BRkQ7QUFHQSxTQUFRLENBQUN0QyxFQUFELElBQVEsQ0FBQyxLQUFLUSxHQUFOLElBQWEsQ0FBQyxLQUFLRSxPQUFuQixJQUE4QlYsR0FBR29DLFFBQUgsRUFBOUIsSUFBK0MsQ0FBQ0UsVUFBVXRDLEdBQUdzQyxPQUFILENBQVcsSUFBWCxDQUFYLEtBQWdDRCxTQUEvRSxJQUE0RkMsVUFBVUQsWUFBWSxLQUFLTixhQUFMLEtBQXVCLEtBQUtyQyxVQUF4QyxHQUFxRCxTQUEzSztBQUNBLEVBTEQ7O0FBT0FuTixHQUFFNFAsUUFBRixHQUFhLFVBQVVJLE9BQVYsRUFBbUJDLGNBQW5CLEVBQW1DO0FBQy9DLE1BQUksQ0FBQ2xLLGFBQUwsRUFBb0I7QUFDbkJELFdBQVEyRCxJQUFSO0FBQ0E7QUFDRCxPQUFLd0UsR0FBTCxHQUFXLENBQUMrQixPQUFaO0FBQ0EsT0FBSzVDLE9BQUwsR0FBZSxLQUFLeUMsUUFBTCxFQUFmO0FBQ0EsTUFBSUksbUJBQW1CLElBQXZCLEVBQTZCO0FBQzVCLE9BQUlELFdBQVcsQ0FBQyxLQUFLdEIsUUFBckIsRUFBK0I7QUFDOUIsU0FBS0QsU0FBTCxDQUFlYixHQUFmLENBQW1CLElBQW5CLEVBQXlCLEtBQUs5QyxVQUFMLEdBQWtCLEtBQUtrQyxNQUFoRDtBQUNBLElBRkQsTUFFTyxJQUFJLENBQUNnRCxPQUFELElBQVksS0FBS3RCLFFBQXJCLEVBQStCO0FBQ3JDLFNBQUtELFNBQUwsQ0FBZXlCLE9BQWYsQ0FBdUIsSUFBdkIsRUFBNkIsSUFBN0I7QUFDQTtBQUNEO0FBQ0QsU0FBTyxLQUFQO0FBQ0EsRUFkRDs7QUFpQkFsUSxHQUFFK0QsS0FBRixHQUFVLFVBQVMvQixJQUFULEVBQWVqQixNQUFmLEVBQXVCO0FBQ2hDLFNBQU8sS0FBSzZPLFFBQUwsQ0FBYyxLQUFkLEVBQXFCLEtBQXJCLENBQVA7QUFDQSxFQUZEOztBQUlBNVAsR0FBRXNELElBQUYsR0FBUyxVQUFTdEIsSUFBVCxFQUFlakIsTUFBZixFQUF1QjtBQUMvQixPQUFLZ0QsS0FBTCxDQUFXL0IsSUFBWCxFQUFpQmpCLE1BQWpCO0FBQ0EsU0FBTyxJQUFQO0FBQ0EsRUFIRDs7QUFLQWYsR0FBRW1RLFFBQUYsR0FBYSxVQUFTQyxXQUFULEVBQXNCO0FBQ2xDLE1BQUl4TyxRQUFRd08sY0FBYyxJQUFkLEdBQXFCLEtBQUsxQixRQUF0QztBQUNBLFNBQU85TSxLQUFQLEVBQWM7QUFDYkEsU0FBTW9NLE1BQU4sR0FBZSxJQUFmO0FBQ0FwTSxXQUFRQSxNQUFNOE0sUUFBZDtBQUNBO0FBQ0QsU0FBTyxJQUFQO0FBQ0EsRUFQRDs7QUFTQTFPLEdBQUVxUSxpQkFBRixHQUFzQixVQUFTQyxNQUFULEVBQWlCO0FBQ3RDLE1BQUlyTCxJQUFJcUwsT0FBT3JTLE1BQWY7QUFBQSxNQUNDc1MsT0FBT0QsT0FBTy9JLE1BQVAsRUFEUjtBQUVBLFNBQU8sRUFBRXRDLENBQUYsR0FBTSxDQUFDLENBQWQsRUFBaUI7QUFDaEIsT0FBSXFMLE9BQU9yTCxDQUFQLE1BQWMsUUFBbEIsRUFBNEI7QUFDM0JzTCxTQUFLdEwsQ0FBTCxJQUFVLElBQVY7QUFDQTtBQUNEO0FBQ0QsU0FBT3NMLElBQVA7QUFDQSxFQVREOztBQVdBdlEsR0FBRXdRLFNBQUYsR0FBYyxVQUFTeFAsSUFBVCxFQUFlO0FBQzVCLE1BQUk4QixJQUFJLEtBQUtkLElBQWI7QUFBQSxNQUNDa0gsV0FBV3BHLEVBQUU5QixJQUFGLENBRFo7QUFBQSxNQUVDc1AsU0FBU3hOLEVBQUU5QixPQUFPLFFBQVQsQ0FGVjtBQUFBLE1BR0NtSSxRQUFRckcsRUFBRTlCLE9BQU8sT0FBVCxLQUFxQjhCLEVBQUUyTixhQUF2QixJQUF3QyxJQUhqRDtBQUFBLE1BSUNuTCxJQUFJZ0wsU0FBU0EsT0FBT3JTLE1BQWhCLEdBQXlCLENBSjlCO0FBS0EsVUFBUXFILENBQVIsR0FBYTtBQUNaLFFBQUssQ0FBTDtBQUFRNEQsYUFBU2pHLElBQVQsQ0FBY2tHLEtBQWQsRUFBc0I7QUFDOUIsUUFBSyxDQUFMO0FBQVFELGFBQVNqRyxJQUFULENBQWNrRyxLQUFkLEVBQXFCbUgsT0FBTyxDQUFQLENBQXJCLEVBQWlDO0FBQ3pDLFFBQUssQ0FBTDtBQUFRcEgsYUFBU2pHLElBQVQsQ0FBY2tHLEtBQWQsRUFBcUJtSCxPQUFPLENBQVAsQ0FBckIsRUFBZ0NBLE9BQU8sQ0FBUCxDQUFoQyxFQUE0QztBQUNwRDtBQUFTcEgsYUFBUzFGLEtBQVQsQ0FBZTJGLEtBQWYsRUFBc0JtSCxNQUF0QjtBQUpWO0FBTUEsRUFaRDs7QUFjRjs7QUFFRXRRLEdBQUUwUSxhQUFGLEdBQWtCLFVBQVMxUCxJQUFULEVBQWVrSSxRQUFmLEVBQXlCb0gsTUFBekIsRUFBaUNuSCxLQUFqQyxFQUF3QztBQUN6RCxNQUFJLENBQUNuSSxRQUFRLEVBQVQsRUFBYTJQLE1BQWIsQ0FBb0IsQ0FBcEIsRUFBc0IsQ0FBdEIsTUFBNkIsSUFBakMsRUFBdUM7QUFDdEMsT0FBSTdOLElBQUksS0FBS2QsSUFBYjtBQUNBLE9BQUlrSyxVQUFVak8sTUFBVixLQUFxQixDQUF6QixFQUE0QjtBQUMzQixXQUFPNkUsRUFBRTlCLElBQUYsQ0FBUDtBQUNBO0FBQ0QsT0FBSWtJLFlBQVksSUFBaEIsRUFBc0I7QUFDckIsV0FBT3BHLEVBQUU5QixJQUFGLENBQVA7QUFDQSxJQUZELE1BRU87QUFDTjhCLE1BQUU5QixJQUFGLElBQVVrSSxRQUFWO0FBQ0FwRyxNQUFFOUIsT0FBTyxRQUFULElBQXNCd0UsU0FBUzhLLE1BQVQsS0FBb0JBLE9BQU8xSixJQUFQLENBQVksRUFBWixFQUFnQmdLLE9BQWhCLENBQXdCLFFBQXhCLE1BQXNDLENBQUMsQ0FBNUQsR0FBaUUsS0FBS1AsaUJBQUwsQ0FBdUJDLE1BQXZCLENBQWpFLEdBQWtHQSxNQUF2SDtBQUNBeE4sTUFBRTlCLE9BQU8sT0FBVCxJQUFvQm1JLEtBQXBCO0FBQ0E7QUFDRCxPQUFJbkksU0FBUyxVQUFiLEVBQXlCO0FBQ3hCLFNBQUt3TixTQUFMLEdBQWlCdEYsUUFBakI7QUFDQTtBQUNEO0FBQ0QsU0FBTyxJQUFQO0FBQ0EsRUFsQkQ7O0FBb0JBbEosR0FBRWtOLEtBQUYsR0FBVSxVQUFTMU4sS0FBVCxFQUFnQjtBQUN6QixNQUFJLENBQUMwTSxVQUFVak8sTUFBZixFQUF1QjtBQUN0QixVQUFPLEtBQUsrTyxNQUFaO0FBQ0E7QUFDRCxNQUFJLEtBQUt5QixTQUFMLENBQWVvQyxpQkFBbkIsRUFBc0M7QUFDckMsUUFBS2YsU0FBTCxDQUFnQixLQUFLaEYsVUFBTCxHQUFrQnRMLEtBQWxCLEdBQTBCLEtBQUt3TixNQUEvQztBQUNBO0FBQ0QsT0FBS0EsTUFBTCxHQUFjeE4sS0FBZDtBQUNBLFNBQU8sSUFBUDtBQUNBLEVBVEQ7O0FBV0FRLEdBQUU2TSxRQUFGLEdBQWEsVUFBU3JOLEtBQVQsRUFBZ0I7QUFDNUIsTUFBSSxDQUFDME0sVUFBVWpPLE1BQWYsRUFBdUI7QUFDdEIsUUFBSytQLE1BQUwsR0FBYyxLQUFkO0FBQ0EsVUFBTyxLQUFLbEIsU0FBWjtBQUNBO0FBQ0QsT0FBS0EsU0FBTCxHQUFpQixLQUFLQyxjQUFMLEdBQXNCdk4sS0FBdkM7QUFDQSxPQUFLMlEsUUFBTCxDQUFjLElBQWQsRUFONEIsQ0FNUDtBQUNyQixNQUFJLEtBQUsxQixTQUFMLENBQWVvQyxpQkFBbkIsRUFBc0MsSUFBSSxLQUFLaEQsS0FBTCxHQUFhLENBQWpCLEVBQW9CLElBQUksS0FBS0EsS0FBTCxHQUFhLEtBQUtmLFNBQXRCLEVBQWlDLElBQUl0TixVQUFVLENBQWQsRUFBaUI7QUFDM0csUUFBSzRQLFNBQUwsQ0FBZSxLQUFLaEIsVUFBTCxJQUFtQjVPLFFBQVEsS0FBS3NOLFNBQWhDLENBQWYsRUFBMkQsSUFBM0Q7QUFDQTtBQUNELFNBQU8sSUFBUDtBQUNBLEVBWEQ7O0FBYUE5TSxHQUFFd1AsYUFBRixHQUFrQixVQUFTaFEsS0FBVCxFQUFnQjtBQUNqQyxPQUFLd08sTUFBTCxHQUFjLEtBQWQ7QUFDQSxTQUFRLENBQUM5QixVQUFVak8sTUFBWixHQUFzQixLQUFLOE8sY0FBM0IsR0FBNEMsS0FBS0YsUUFBTCxDQUFjck4sS0FBZCxDQUFuRDtBQUNBLEVBSEQ7O0FBS0FRLEdBQUU2TCxJQUFGLEdBQVMsVUFBU3JNLEtBQVQsRUFBZ0J1UCxjQUFoQixFQUFnQztBQUN4QyxNQUFJLENBQUM3QyxVQUFVak8sTUFBZixFQUF1QjtBQUN0QixVQUFPLEtBQUs0UCxLQUFaO0FBQ0E7QUFDRCxNQUFJLEtBQUtHLE1BQVQsRUFBaUI7QUFDaEIsUUFBS3dCLGFBQUw7QUFDQTtBQUNELFNBQU8sS0FBS0osU0FBTCxDQUFnQjVQLFFBQVEsS0FBS3NOLFNBQWQsR0FBMkIsS0FBS0EsU0FBaEMsR0FBNEN0TixLQUEzRCxFQUFrRXVQLGNBQWxFLENBQVA7QUFDQSxFQVJEOztBQVVBL08sR0FBRW9QLFNBQUYsR0FBYyxVQUFTdkQsSUFBVCxFQUFla0QsY0FBZixFQUErQitCLFFBQS9CLEVBQXlDO0FBQ3RELE1BQUksQ0FBQy9LLGFBQUwsRUFBb0I7QUFDbkJELFdBQVEyRCxJQUFSO0FBQ0E7QUFDRCxNQUFJLENBQUN5QyxVQUFVak8sTUFBZixFQUF1QjtBQUN0QixVQUFPLEtBQUttUSxVQUFaO0FBQ0E7QUFDRCxNQUFJLEtBQUtLLFNBQVQsRUFBb0I7QUFDbkIsT0FBSTVDLE9BQU8sQ0FBUCxJQUFZLENBQUNpRixRQUFqQixFQUEyQjtBQUMxQmpGLFlBQVEsS0FBSzJELGFBQUwsRUFBUjtBQUNBO0FBQ0QsT0FBSSxLQUFLZixTQUFMLENBQWVvQyxpQkFBbkIsRUFBc0M7QUFDckMsUUFBSSxLQUFLN0MsTUFBVCxFQUFpQjtBQUNoQixVQUFLd0IsYUFBTDtBQUNBO0FBQ0QsUUFBSUEsZ0JBQWdCLEtBQUt6QyxjQUF6QjtBQUFBLFFBQ0NVLEtBQUssS0FBS2dCLFNBRFg7QUFFQSxRQUFJNUMsT0FBTzJELGFBQVAsSUFBd0IsQ0FBQ3NCLFFBQTdCLEVBQXVDO0FBQ3RDakYsWUFBTzJELGFBQVA7QUFDQTtBQUNELFNBQUsxRSxVQUFMLEdBQWtCLENBQUMsS0FBS3FELE9BQUwsR0FBZSxLQUFLNEMsVUFBcEIsR0FBaUN0RCxHQUFHSSxLQUFyQyxJQUErQyxDQUFDLENBQUMsS0FBS1AsU0FBTixHQUFrQnpCLElBQWxCLEdBQXlCMkQsZ0JBQWdCM0QsSUFBMUMsSUFBa0QsS0FBS3NCLFVBQXhIO0FBQ0EsUUFBSSxDQUFDTSxHQUFHTyxNQUFSLEVBQWdCO0FBQUU7QUFDakIsVUFBS21DLFFBQUwsQ0FBYyxLQUFkO0FBQ0E7QUFDRDtBQUNBLFFBQUkxQyxHQUFHZ0IsU0FBUCxFQUFrQjtBQUNqQixZQUFPaEIsR0FBR2dCLFNBQVYsRUFBcUI7QUFDcEIsVUFBSWhCLEdBQUdnQixTQUFILENBQWFaLEtBQWIsS0FBdUIsQ0FBQ0osR0FBRzNDLFVBQUgsR0FBZ0IyQyxHQUFHVyxVQUFwQixJQUFrQ1gsR0FBR04sVUFBaEUsRUFBNEU7QUFDM0VNLFVBQUcyQixTQUFILENBQWEzQixHQUFHVyxVQUFoQixFQUE0QixJQUE1QjtBQUNBO0FBQ0RYLFdBQUtBLEdBQUdnQixTQUFSO0FBQ0E7QUFDRDtBQUNEO0FBQ0QsT0FBSSxLQUFLUixHQUFULEVBQWM7QUFDYixTQUFLMkIsUUFBTCxDQUFjLElBQWQsRUFBb0IsS0FBcEI7QUFDQTtBQUNELE9BQUksS0FBS3hCLFVBQUwsS0FBb0J2QyxJQUFwQixJQUE0QixLQUFLaUIsU0FBTCxLQUFtQixDQUFuRCxFQUFzRDtBQUNyRCxRQUFJa0UsWUFBWS9TLE1BQWhCLEVBQXdCO0FBQ3ZCZ1Q7QUFDQTtBQUNELFNBQUt4QixNQUFMLENBQVk1RCxJQUFaLEVBQWtCa0QsY0FBbEIsRUFBa0MsS0FBbEM7QUFDQSxRQUFJaUMsWUFBWS9TLE1BQWhCLEVBQXdCO0FBQUU7QUFDekJnVDtBQUNBO0FBQ0Q7QUFDRDtBQUNELFNBQU8sSUFBUDtBQUNBLEVBaEREOztBQWtEQWpSLEdBQUVrUixRQUFGLEdBQWFsUixFQUFFbVIsYUFBRixHQUFrQixVQUFTM1IsS0FBVCxFQUFnQnVQLGNBQWhCLEVBQWdDO0FBQzlELE1BQUlsQyxXQUFXLEtBQUtBLFFBQUwsRUFBZjtBQUNBLFNBQVEsQ0FBQ1gsVUFBVWpPLE1BQVosR0FBdUI0TyxXQUFXLEtBQUtnQixLQUFMLEdBQWFoQixRQUF4QixHQUFtQyxLQUFLdUUsS0FBL0QsR0FBd0UsS0FBS2hDLFNBQUwsQ0FBZXZDLFdBQVdyTixLQUExQixFQUFpQ3VQLGNBQWpDLENBQS9FO0FBQ0EsRUFIRDs7QUFLQS9PLEdBQUU4UCxTQUFGLEdBQWMsVUFBU3RRLEtBQVQsRUFBZ0I7QUFDN0IsTUFBSSxDQUFDME0sVUFBVWpPLE1BQWYsRUFBdUI7QUFDdEIsVUFBTyxLQUFLNk0sVUFBWjtBQUNBO0FBQ0QsTUFBSXRMLFVBQVUsS0FBS3NMLFVBQW5CLEVBQStCO0FBQzlCLFFBQUtBLFVBQUwsR0FBa0J0TCxLQUFsQjtBQUNBLE9BQUksS0FBS2tQLFFBQVQsRUFBbUIsSUFBSSxLQUFLQSxRQUFMLENBQWMyQyxhQUFsQixFQUFpQztBQUNuRCxTQUFLM0MsUUFBTCxDQUFjZCxHQUFkLENBQWtCLElBQWxCLEVBQXdCcE8sUUFBUSxLQUFLd04sTUFBckMsRUFEbUQsQ0FDTDtBQUM5QztBQUNEO0FBQ0QsU0FBTyxJQUFQO0FBQ0EsRUFYRDs7QUFhQWhOLEdBQUVzUixPQUFGLEdBQVksVUFBU0MsY0FBVCxFQUF5QjtBQUNwQyxTQUFPLEtBQUt6RyxVQUFMLEdBQWtCLENBQUV5RyxrQkFBa0IsS0FBbkIsR0FBNEIsS0FBSy9CLGFBQUwsRUFBNUIsR0FBbUQsS0FBSzNDLFFBQUwsRUFBcEQsSUFBdUUsS0FBS00sVUFBckc7QUFDQSxFQUZEOztBQUlBbk4sR0FBRXdSLFNBQUYsR0FBYyxVQUFTaFMsS0FBVCxFQUFnQjtBQUM3QixNQUFJLENBQUMwTSxVQUFVak8sTUFBZixFQUF1QjtBQUN0QixVQUFPLEtBQUtrUCxVQUFaO0FBQ0E7QUFDRCxNQUFJc0UsU0FBSixFQUFlcEosQ0FBZjtBQUNBN0ksVUFBUUEsU0FBUzJGLFFBQWpCLENBTDZCLENBS0Y7QUFDM0IsTUFBSSxLQUFLc0osU0FBTCxJQUFrQixLQUFLQSxTQUFMLENBQWVvQyxpQkFBckMsRUFBd0Q7QUFDdkRZLGVBQVksS0FBS1YsVUFBakI7QUFDQTFJLE9BQUtvSixhQUFhQSxjQUFjLENBQTVCLEdBQWlDQSxTQUFqQyxHQUE2QyxLQUFLaEQsU0FBTCxDQUFlVyxTQUFmLEVBQWpEO0FBQ0EsUUFBS3RFLFVBQUwsR0FBa0J6QyxJQUFLLENBQUNBLElBQUksS0FBS3lDLFVBQVYsSUFBd0IsS0FBS3FDLFVBQTdCLEdBQTBDM04sS0FBakU7QUFDQTtBQUNELE9BQUsyTixVQUFMLEdBQWtCM04sS0FBbEI7QUFDQTZJLE1BQUksS0FBS3FHLFFBQVQ7QUFDQSxTQUFPckcsS0FBS0EsRUFBRXFHLFFBQWQsRUFBd0I7QUFBRTtBQUN6QnJHLEtBQUUyRixNQUFGLEdBQVcsSUFBWDtBQUNBM0YsS0FBRW1ILGFBQUY7QUFDQW5ILE9BQUlBLEVBQUVxRyxRQUFOO0FBQ0E7QUFDRCxTQUFPLElBQVA7QUFDQSxFQW5CRDs7QUFxQkExTyxHQUFFdU4sUUFBRixHQUFhLFVBQVMvTixLQUFULEVBQWdCO0FBQzVCLE1BQUksQ0FBQzBNLFVBQVVqTyxNQUFmLEVBQXVCO0FBQ3RCLFVBQU8sS0FBS3FQLFNBQVo7QUFDQTtBQUNELE1BQUk5TixTQUFTLEtBQUs4TixTQUFsQixFQUE2QjtBQUM1QixRQUFLQSxTQUFMLEdBQWlCOU4sS0FBakI7QUFDQSxRQUFLNFAsU0FBTCxDQUFpQixLQUFLWCxTQUFMLElBQWtCLENBQUMsS0FBS0EsU0FBTCxDQUFlb0MsaUJBQW5DLEdBQXdELEtBQUtyQixhQUFMLEtBQXVCLEtBQUtwQixVQUFwRixHQUFpRyxLQUFLQSxVQUF0SCxFQUFtSSxJQUFuSTtBQUNBO0FBQ0QsU0FBTyxJQUFQO0FBQ0EsRUFURDs7QUFXQXBPLEdBQUU4TixNQUFGLEdBQVcsVUFBU3RPLEtBQVQsRUFBZ0I7QUFDMUIsTUFBSSxDQUFDME0sVUFBVWpPLE1BQWYsRUFBdUI7QUFDdEIsVUFBTyxLQUFLa1EsT0FBWjtBQUNBO0FBQ0QsTUFBSVYsS0FBSyxLQUFLZ0IsU0FBZDtBQUFBLE1BQ0NpRCxHQUREO0FBQUEsTUFDTWhHLE9BRE47QUFFQSxNQUFJbE0sU0FBUyxLQUFLMk8sT0FBbEIsRUFBMkIsSUFBSVYsRUFBSixFQUFRO0FBQ2xDLE9BQUksQ0FBQzFILGFBQUQsSUFBa0IsQ0FBQ3ZHLEtBQXZCLEVBQThCO0FBQzdCc0csWUFBUTJELElBQVI7QUFDQTtBQUNEaUksU0FBTWpFLEdBQUdzQyxPQUFILEVBQU47QUFDQXJFLGFBQVVnRyxNQUFNLEtBQUtYLFVBQXJCO0FBQ0EsT0FBSSxDQUFDdlIsS0FBRCxJQUFVaU8sR0FBR29ELGlCQUFqQixFQUFvQztBQUNuQyxTQUFLL0YsVUFBTCxJQUFtQlksT0FBbkI7QUFDQSxTQUFLeUUsUUFBTCxDQUFjLEtBQWQ7QUFDQTtBQUNELFFBQUtZLFVBQUwsR0FBa0J2UixRQUFRa1MsR0FBUixHQUFjLElBQWhDO0FBQ0EsUUFBS3ZELE9BQUwsR0FBZTNPLEtBQWY7QUFDQSxRQUFLNE4sT0FBTCxHQUFlLEtBQUt5QyxRQUFMLEVBQWY7QUFDQSxPQUFJLENBQUNyUSxLQUFELElBQVVrTSxZQUFZLENBQXRCLElBQTJCLEtBQUt3QyxRQUFoQyxJQUE0QyxLQUFLckIsUUFBTCxFQUFoRCxFQUFpRTtBQUNoRTZFLFVBQU1qRSxHQUFHb0QsaUJBQUgsR0FBdUIsS0FBS3pDLFVBQTVCLEdBQXlDLENBQUNzRCxNQUFNLEtBQUs1RyxVQUFaLElBQTBCLEtBQUtxQyxVQUE5RTtBQUNBLFNBQUtzQyxNQUFMLENBQVlpQyxHQUFaLEVBQWtCQSxRQUFRLEtBQUt0RCxVQUEvQixFQUE0QyxJQUE1QyxFQUZnRSxDQUViO0FBQ25EO0FBQ0Q7QUFDRCxNQUFJLEtBQUtILEdBQUwsSUFBWSxDQUFDek8sS0FBakIsRUFBd0I7QUFDdkIsUUFBS29RLFFBQUwsQ0FBYyxJQUFkLEVBQW9CLEtBQXBCO0FBQ0E7QUFDRCxTQUFPLElBQVA7QUFDQSxFQTVCRDs7QUErQkY7Ozs7O0FBS0UsS0FBSStCLGlCQUFpQjlLLE9BQU8scUJBQVAsRUFBOEIsVUFBUzdFLElBQVQsRUFBZTtBQUNqRTRLLFlBQVUzSixJQUFWLENBQWUsSUFBZixFQUFxQixDQUFyQixFQUF3QmpCLElBQXhCO0FBQ0EsT0FBSzRQLGtCQUFMLEdBQTBCLEtBQUtmLGlCQUFMLEdBQXlCLElBQW5EO0FBQ0EsRUFIb0IsQ0FBckI7O0FBS0E3USxLQUFJMlIsZUFBZS9OLFNBQWYsR0FBMkIsSUFBSWdKLFNBQUosRUFBL0I7QUFDQTVNLEdBQUUyTSxXQUFGLEdBQWdCZ0YsY0FBaEI7QUFDQTNSLEdBQUVzRCxJQUFGLEdBQVMySyxHQUFULEdBQWUsS0FBZjtBQUNBak8sR0FBRTZSLE1BQUYsR0FBVzdSLEVBQUV1TyxLQUFGLEdBQVV2TyxFQUFFOFIsT0FBRixHQUFZLElBQWpDO0FBQ0E5UixHQUFFcVIsYUFBRixHQUFrQixLQUFsQjs7QUFFQXJSLEdBQUU0TixHQUFGLEdBQVE1TixFQUFFK1IsTUFBRixHQUFXLFVBQVNDLEtBQVQsRUFBZ0JDLFFBQWhCLEVBQTBCQyxLQUExQixFQUFpQ0MsT0FBakMsRUFBMEM7QUFDNUQsTUFBSUMsU0FBSixFQUFlQyxFQUFmO0FBQ0FMLFFBQU1sSCxVQUFOLEdBQW1CbUMsT0FBT2dGLFlBQVksQ0FBbkIsSUFBd0JELE1BQU1oRixNQUFqRDtBQUNBLE1BQUlnRixNQUFNN0QsT0FBVixFQUFtQixJQUFJLFNBQVM2RCxNQUFNdkQsU0FBbkIsRUFBOEI7QUFBRTtBQUNsRHVELFNBQU1qQixVQUFOLEdBQW1CaUIsTUFBTWxILFVBQU4sR0FBb0IsQ0FBQyxLQUFLaUYsT0FBTCxLQUFpQmlDLE1BQU1sSCxVQUF4QixJQUFzQ2tILE1BQU03RSxVQUFuRjtBQUNBO0FBQ0QsTUFBSTZFLE1BQU10RCxRQUFWLEVBQW9CO0FBQ25Cc0QsU0FBTXRELFFBQU4sQ0FBZXdCLE9BQWYsQ0FBdUI4QixLQUF2QixFQUE4QixJQUE5QixFQURtQixDQUNrQjtBQUNyQztBQUNEQSxRQUFNdEQsUUFBTixHQUFpQnNELE1BQU12RCxTQUFOLEdBQWtCLElBQW5DO0FBQ0EsTUFBSXVELE1BQU0vRCxHQUFWLEVBQWU7QUFDZCtELFNBQU1wQyxRQUFOLENBQWUsSUFBZixFQUFxQixJQUFyQjtBQUNBO0FBQ0R3QyxjQUFZLEtBQUs3RCxLQUFqQjtBQUNBLE1BQUksS0FBSzhDLGFBQVQsRUFBd0I7QUFDdkJnQixRQUFLTCxNQUFNbEgsVUFBWDtBQUNBLFVBQU9zSCxhQUFhQSxVQUFVdEgsVUFBVixHQUF1QnVILEVBQTNDLEVBQStDO0FBQzlDRCxnQkFBWUEsVUFBVUUsS0FBdEI7QUFDQTtBQUNEO0FBQ0QsTUFBSUYsU0FBSixFQUFlO0FBQ2RKLFNBQU0xRCxLQUFOLEdBQWM4RCxVQUFVOUQsS0FBeEI7QUFDQThELGFBQVU5RCxLQUFWLEdBQWtCMEQsS0FBbEI7QUFDQSxHQUhELE1BR087QUFDTkEsU0FBTTFELEtBQU4sR0FBYyxLQUFLdUQsTUFBbkI7QUFDQSxRQUFLQSxNQUFMLEdBQWNHLEtBQWQ7QUFDQTtBQUNELE1BQUlBLE1BQU0xRCxLQUFWLEVBQWlCO0FBQ2hCMEQsU0FBTTFELEtBQU4sQ0FBWWdFLEtBQVosR0FBb0JOLEtBQXBCO0FBQ0EsR0FGRCxNQUVPO0FBQ04sUUFBS3pELEtBQUwsR0FBYXlELEtBQWI7QUFDQTtBQUNEQSxRQUFNTSxLQUFOLEdBQWNGLFNBQWQ7QUFDQSxPQUFLTixPQUFMLEdBQWVFLEtBQWY7QUFDQSxNQUFJLEtBQUt2RCxTQUFULEVBQW9CO0FBQ25CLFFBQUswQixRQUFMLENBQWMsSUFBZDtBQUNBO0FBQ0QsU0FBTyxJQUFQO0FBQ0EsRUF0Q0Q7O0FBd0NBblEsR0FBRWtRLE9BQUYsR0FBWSxVQUFTdE8sS0FBVCxFQUFnQjJRLFdBQWhCLEVBQTZCO0FBQ3hDLE1BQUkzUSxNQUFNOE0sUUFBTixLQUFtQixJQUF2QixFQUE2QjtBQUM1QixPQUFJLENBQUM2RCxXQUFMLEVBQWtCO0FBQ2pCM1EsVUFBTWdPLFFBQU4sQ0FBZSxLQUFmLEVBQXNCLElBQXRCO0FBQ0E7O0FBRUQsT0FBSWhPLE1BQU0wUSxLQUFWLEVBQWlCO0FBQ2hCMVEsVUFBTTBRLEtBQU4sQ0FBWWhFLEtBQVosR0FBb0IxTSxNQUFNME0sS0FBMUI7QUFDQSxJQUZELE1BRU8sSUFBSSxLQUFLdUQsTUFBTCxLQUFnQmpRLEtBQXBCLEVBQTJCO0FBQ2pDLFNBQUtpUSxNQUFMLEdBQWNqUSxNQUFNME0sS0FBcEI7QUFDQTtBQUNELE9BQUkxTSxNQUFNME0sS0FBVixFQUFpQjtBQUNoQjFNLFVBQU0wTSxLQUFOLENBQVlnRSxLQUFaLEdBQW9CMVEsTUFBTTBRLEtBQTFCO0FBQ0EsSUFGRCxNQUVPLElBQUksS0FBSy9ELEtBQUwsS0FBZTNNLEtBQW5CLEVBQTBCO0FBQ2hDLFNBQUsyTSxLQUFMLEdBQWEzTSxNQUFNMFEsS0FBbkI7QUFDQTtBQUNEMVEsU0FBTTBNLEtBQU4sR0FBYzFNLE1BQU0wUSxLQUFOLEdBQWMxUSxNQUFNOE0sUUFBTixHQUFpQixJQUE3QztBQUNBLE9BQUk5TSxVQUFVLEtBQUtrUSxPQUFuQixFQUE0QjtBQUMzQixTQUFLQSxPQUFMLEdBQWUsS0FBS3ZELEtBQXBCO0FBQ0E7O0FBRUQsT0FBSSxLQUFLRSxTQUFULEVBQW9CO0FBQ25CLFNBQUswQixRQUFMLENBQWMsSUFBZDtBQUNBO0FBQ0Q7QUFDRCxTQUFPLElBQVA7QUFDQSxFQTFCRDs7QUE0QkFuUSxHQUFFeVAsTUFBRixHQUFXLFVBQVM1RCxJQUFULEVBQWVrRCxjQUFmLEVBQStCVyxLQUEvQixFQUFzQztBQUNoRCxNQUFJOU4sUUFBUSxLQUFLaVEsTUFBakI7QUFBQSxNQUNDVyxJQUREO0FBRUEsT0FBS3BFLFVBQUwsR0FBa0IsS0FBS1AsS0FBTCxHQUFhLEtBQUtRLFlBQUwsR0FBb0J4QyxJQUFuRDtBQUNBLFNBQU9qSyxLQUFQLEVBQWM7QUFDYjRRLFVBQU81USxNQUFNME0sS0FBYixDQURhLENBQ087QUFDcEIsT0FBSTFNLE1BQU13TCxPQUFOLElBQWtCdkIsUUFBUWpLLE1BQU1rSixVQUFkLElBQTRCLENBQUNsSixNQUFNdU0sT0FBbkMsSUFBOEMsQ0FBQ3ZNLE1BQU1xTSxHQUEzRSxFQUFpRjtBQUNoRixRQUFJLENBQUNyTSxNQUFNMEwsU0FBWCxFQUFzQjtBQUNyQjFMLFdBQU02TixNQUFOLENBQWEsQ0FBQzVELE9BQU9qSyxNQUFNa0osVUFBZCxJQUE0QmxKLE1BQU11TCxVQUEvQyxFQUEyRDRCLGNBQTNELEVBQTJFVyxLQUEzRTtBQUNBLEtBRkQsTUFFTztBQUNOOU4sV0FBTTZOLE1BQU4sQ0FBYSxDQUFFLENBQUM3TixNQUFNb00sTUFBUixHQUFrQnBNLE1BQU1tTCxjQUF4QixHQUF5Q25MLE1BQU00TixhQUFOLEVBQTFDLElBQW9FLENBQUMzRCxPQUFPakssTUFBTWtKLFVBQWQsSUFBNEJsSixNQUFNdUwsVUFBbkgsRUFBZ0k0QixjQUFoSSxFQUFnSlcsS0FBaEo7QUFDQTtBQUNEO0FBQ0Q5TixXQUFRNFEsSUFBUjtBQUNBO0FBQ0QsRUFmRDs7QUFpQkF4UyxHQUFFK1AsT0FBRixHQUFZLFlBQVc7QUFDdEIsTUFBSSxDQUFDaEssYUFBTCxFQUFvQjtBQUNuQkQsV0FBUTJELElBQVI7QUFDQTtBQUNELFNBQU8sS0FBSzJFLFVBQVo7QUFDQSxFQUxEOztBQU9GOzs7OztBQUtFLEtBQUkxTyxZQUFZbUgsT0FBTyxXQUFQLEVBQW9CLFVBQVM5RixNQUFULEVBQWlCOEwsUUFBakIsRUFBMkI3SyxJQUEzQixFQUFpQztBQUNuRTRLLFlBQVUzSixJQUFWLENBQWUsSUFBZixFQUFxQjRKLFFBQXJCLEVBQStCN0ssSUFBL0I7QUFDQSxPQUFLeU4sTUFBTCxHQUFjL1AsVUFBVWtFLFNBQVYsQ0FBb0I2TCxNQUFsQyxDQUZtRSxDQUV6Qjs7QUFFMUMsTUFBSTFPLFVBQVUsSUFBZCxFQUFvQjtBQUNuQixTQUFNLDZCQUFOO0FBQ0E7O0FBRUQsT0FBS0EsTUFBTCxHQUFjQSxTQUFVLE9BQU9BLE1BQVAsS0FBbUIsUUFBcEIsR0FBZ0NBLE1BQWhDLEdBQXlDckIsVUFBVUMsUUFBVixDQUFtQm9CLE1BQW5CLEtBQThCQSxNQUE5Rjs7QUFFQSxNQUFJMFIsYUFBYzFSLE9BQU8yUixNQUFQLElBQWtCM1IsT0FBTzlDLE1BQVAsSUFBaUI4QyxXQUFXL0QsTUFBNUIsSUFBc0MrRCxPQUFPLENBQVAsQ0FBdEMsS0FBb0RBLE9BQU8sQ0FBUCxNQUFjL0QsTUFBZCxJQUF5QitELE9BQU8sQ0FBUCxFQUFVbEIsUUFBVixJQUFzQmtCLE9BQU8sQ0FBUCxFQUFVbkIsS0FBaEMsSUFBeUMsQ0FBQ21CLE9BQU9sQixRQUE5SCxDQUFwQztBQUFBLE1BQ0M4UyxZQUFZLEtBQUszUSxJQUFMLENBQVUyUSxTQUR2QjtBQUFBLE1BRUMxTixDQUZEO0FBQUEsTUFFSTJOLElBRko7QUFBQSxNQUVVQyxPQUZWOztBQUlBLE9BQUtDLFVBQUwsR0FBa0JILFlBQWFBLGFBQWEsSUFBZCxHQUFzQkksaUJBQWlCclQsVUFBVXNULGdCQUEzQixDQUF0QixHQUFzRSxPQUFPTCxTQUFQLEtBQXNCLFFBQXZCLEdBQW1DQSxhQUFhLENBQWhELEdBQW9ESSxpQkFBaUJKLFNBQWpCLENBQXZKOztBQUVBLE1BQUksQ0FBQ0YsY0FBYzFSLGtCQUFrQjhFLEtBQWhDLElBQTBDOUUsT0FBT3JDLElBQVAsSUFBZThHLFNBQVN6RSxNQUFULENBQTFELEtBQWdGLE9BQU9BLE9BQU8sQ0FBUCxDQUFQLEtBQXNCLFFBQTFHLEVBQW9IO0FBQ25ILFFBQUtrUyxRQUFMLEdBQWdCSixVQUFVek4sT0FBT3JFLE1BQVAsQ0FBMUIsQ0FEbUgsQ0FDeEU7QUFDM0MsUUFBS21TLFdBQUwsR0FBbUIsRUFBbkI7QUFDQSxRQUFLQyxTQUFMLEdBQWlCLEVBQWpCO0FBQ0EsUUFBS2xPLElBQUksQ0FBVCxFQUFZQSxJQUFJNE4sUUFBUTVVLE1BQXhCLEVBQWdDZ0gsR0FBaEMsRUFBcUM7QUFDcEMyTixXQUFPQyxRQUFRNU4sQ0FBUixDQUFQO0FBQ0EsUUFBSSxDQUFDMk4sSUFBTCxFQUFXO0FBQ1ZDLGFBQVFqSixNQUFSLENBQWUzRSxHQUFmLEVBQW9CLENBQXBCO0FBQ0E7QUFDQSxLQUhELE1BR08sSUFBSSxPQUFPMk4sSUFBUCxLQUFpQixRQUFyQixFQUErQjtBQUNyQ0EsWUFBT0MsUUFBUTVOLEdBQVIsSUFBZXZGLFVBQVVDLFFBQVYsQ0FBbUJpVCxJQUFuQixDQUF0QixDQURxQyxDQUNXO0FBQ2hELFNBQUksT0FBT0EsSUFBUCxLQUFpQixRQUFyQixFQUErQjtBQUM5QkMsY0FBUWpKLE1BQVIsQ0FBZTNFLElBQUUsQ0FBakIsRUFBb0IsQ0FBcEIsRUFEOEIsQ0FDTjtBQUN4QjtBQUNEO0FBQ0EsS0FOTSxNQU1BLElBQUkyTixLQUFLM1UsTUFBTCxJQUFlMlUsU0FBUzVWLE1BQXhCLElBQWtDNFYsS0FBSyxDQUFMLENBQWxDLEtBQThDQSxLQUFLLENBQUwsTUFBWTVWLE1BQVosSUFBdUI0VixLQUFLLENBQUwsRUFBUS9TLFFBQVIsSUFBb0IrUyxLQUFLLENBQUwsRUFBUWhULEtBQTVCLElBQXFDLENBQUNnVCxLQUFLL1MsUUFBaEgsQ0FBSixFQUFnSTtBQUFFO0FBQ3hJZ1QsYUFBUWpKLE1BQVIsQ0FBZTNFLEdBQWYsRUFBb0IsQ0FBcEI7QUFDQSxVQUFLZ08sUUFBTCxHQUFnQkosVUFBVUEsUUFBUXRMLE1BQVIsQ0FBZW5DLE9BQU93TixJQUFQLENBQWYsQ0FBMUI7QUFDQTtBQUNBO0FBQ0QsU0FBS08sU0FBTCxDQUFlbE8sQ0FBZixJQUFvQm1PLFVBQVVSLElBQVYsRUFBZ0IsSUFBaEIsRUFBc0IsS0FBdEIsQ0FBcEI7QUFDQSxRQUFJRCxjQUFjLENBQWxCLEVBQXFCLElBQUksS0FBS1EsU0FBTCxDQUFlbE8sQ0FBZixFQUFrQmhILE1BQWxCLEdBQTJCLENBQS9CLEVBQWtDO0FBQ3REb1YscUJBQWdCVCxJQUFoQixFQUFzQixJQUF0QixFQUE0QixJQUE1QixFQUFrQyxDQUFsQyxFQUFxQyxLQUFLTyxTQUFMLENBQWVsTyxDQUFmLENBQXJDO0FBQ0E7QUFDRDtBQUVELEdBMUJELE1BMEJPO0FBQ04sUUFBS2lPLFdBQUwsR0FBbUIsRUFBbkI7QUFDQSxRQUFLQyxTQUFMLEdBQWlCQyxVQUFVclMsTUFBVixFQUFrQixJQUFsQixFQUF3QixLQUF4QixDQUFqQjtBQUNBLE9BQUk0UixjQUFjLENBQWxCLEVBQXFCLElBQUksS0FBS1EsU0FBTCxDQUFlbFYsTUFBZixHQUF3QixDQUE1QixFQUErQjtBQUNuRG9WLG9CQUFnQnRTLE1BQWhCLEVBQXdCLElBQXhCLEVBQThCLElBQTlCLEVBQW9DLENBQXBDLEVBQXVDLEtBQUtvUyxTQUE1QztBQUNBO0FBQ0Q7QUFDRCxNQUFJLEtBQUtuUixJQUFMLENBQVVxTCxlQUFWLElBQThCUixhQUFhLENBQWIsSUFBa0IsS0FBS0csTUFBTCxLQUFnQixDQUFsQyxJQUF1QyxLQUFLaEwsSUFBTCxDQUFVcUwsZUFBVixLQUE4QixLQUF2RyxFQUErRztBQUM5RyxRQUFLUSxLQUFMLEdBQWEsQ0FBQzFJLFFBQWQsQ0FEOEcsQ0FDdEY7QUFDeEIsUUFBS3NLLE1BQUwsQ0FBWXBRLEtBQUsrQixHQUFMLENBQVMsQ0FBVCxFQUFZLENBQUMsS0FBSzRMLE1BQWxCLENBQVosRUFGOEcsQ0FFdEU7QUFDeEM7QUFDRCxFQXJEYyxFQXFEWixJQXJEWSxDQUFoQjtBQUFBLEtBc0RDc0csY0FBYyxTQUFkQSxXQUFjLENBQVN4USxDQUFULEVBQVk7QUFDekIsU0FBUUEsS0FBS0EsRUFBRTdFLE1BQVAsSUFBaUI2RSxNQUFNOUYsTUFBdkIsSUFBaUM4RixFQUFFLENBQUYsQ0FBakMsS0FBMENBLEVBQUUsQ0FBRixNQUFTOUYsTUFBVCxJQUFvQjhGLEVBQUUsQ0FBRixFQUFLakQsUUFBTCxJQUFpQmlELEVBQUUsQ0FBRixFQUFLbEQsS0FBdEIsSUFBK0IsQ0FBQ2tELEVBQUVqRCxRQUFoRyxDQUFSLENBRHlCLENBQzRGO0FBQ3JILEVBeERGO0FBQUEsS0F5REMwVCxXQUFXLFNBQVhBLFFBQVcsQ0FBU3ZSLElBQVQsRUFBZWpCLE1BQWYsRUFBdUI7QUFDakMsTUFBSXlTLE1BQU0sRUFBVjtBQUFBLE1BQ0N4VCxDQUREO0FBRUEsT0FBS0EsQ0FBTCxJQUFVZ0MsSUFBVixFQUFnQjtBQUNmLE9BQUksQ0FBQ3lSLGVBQWV6VCxDQUFmLENBQUQsS0FBdUIsRUFBRUEsS0FBS2UsTUFBUCxLQUFrQmYsTUFBTSxXQUF4QixJQUF1Q0EsTUFBTSxHQUE3QyxJQUFvREEsTUFBTSxHQUExRCxJQUFpRUEsTUFBTSxPQUF2RSxJQUFrRkEsTUFBTSxRQUF4RixJQUFvR0EsTUFBTSxXQUExRyxJQUF5SEEsTUFBTSxRQUF0SixNQUFvSyxDQUFDMFQsU0FBUzFULENBQVQsQ0FBRCxJQUFpQjBULFNBQVMxVCxDQUFULEtBQWUwVCxTQUFTMVQsQ0FBVCxFQUFZdVQsUUFBaE4sQ0FBSixFQUFnTztBQUFFO0FBQ2pPQyxRQUFJeFQsQ0FBSixJQUFTZ0MsS0FBS2hDLENBQUwsQ0FBVDtBQUNBLFdBQU9nQyxLQUFLaEMsQ0FBTCxDQUFQO0FBQ0E7QUFDRDtBQUNEZ0MsT0FBS3dSLEdBQUwsR0FBV0EsR0FBWDtBQUNBLEVBbkVGOztBQXFFQXhULEtBQUlOLFVBQVVrRSxTQUFWLEdBQXNCLElBQUlnSixTQUFKLEVBQTFCO0FBQ0E1TSxHQUFFMk0sV0FBRixHQUFnQmpOLFNBQWhCO0FBQ0FNLEdBQUVzRCxJQUFGLEdBQVMySyxHQUFULEdBQWUsS0FBZjs7QUFFRjs7QUFFRWpPLEdBQUVvUixLQUFGLEdBQVUsQ0FBVjtBQUNBcFIsR0FBRTJULFFBQUYsR0FBYTNULEVBQUVpVCxRQUFGLEdBQWFqVCxFQUFFNFQsaUJBQUYsR0FBc0I1VCxFQUFFNlQsUUFBRixHQUFhLElBQTdEO0FBQ0E3VCxHQUFFOFQsdUJBQUYsR0FBNEI5VCxFQUFFK1QsS0FBRixHQUFVLEtBQXRDOztBQUVBclUsV0FBVWdDLE9BQVYsR0FBb0IsUUFBcEI7QUFDQWhDLFdBQVVzVSxXQUFWLEdBQXdCaFUsRUFBRWlVLEtBQUYsR0FBVSxJQUFJak4sSUFBSixDQUFTLElBQVQsRUFBZSxJQUFmLEVBQXFCLENBQXJCLEVBQXdCLENBQXhCLENBQWxDO0FBQ0F0SCxXQUFVc1QsZ0JBQVYsR0FBNkIsTUFBN0I7QUFDQXRULFdBQVVxTyxNQUFWLEdBQW1CakksT0FBbkI7QUFDQXBHLFdBQVV3VSxTQUFWLEdBQXNCLEdBQXRCO0FBQ0F4VSxXQUFVc00sWUFBVixHQUF5QixVQUFTNUksU0FBVCxFQUFvQjZJLFdBQXBCLEVBQWlDO0FBQ3pEbkcsVUFBUWtHLFlBQVIsQ0FBcUI1SSxTQUFyQixFQUFnQzZJLFdBQWhDO0FBQ0EsRUFGRDs7QUFJQXZNLFdBQVVDLFFBQVYsR0FBcUIzQyxPQUFPRCxDQUFQLElBQVlDLE9BQU9tWCxNQUFuQixJQUE2QixVQUFTcFUsQ0FBVCxFQUFZO0FBQzdELE1BQUlKLFdBQVczQyxPQUFPRCxDQUFQLElBQVlDLE9BQU9tWCxNQUFsQztBQUNBLE1BQUl4VSxRQUFKLEVBQWM7QUFDYkQsYUFBVUMsUUFBVixHQUFxQkEsUUFBckI7QUFDQSxVQUFPQSxTQUFTSSxDQUFULENBQVA7QUFDQTtBQUNELFNBQVEsT0FBT3BCLElBQVAsS0FBaUIsV0FBbEIsR0FBaUNvQixDQUFqQyxHQUFzQ3BCLEtBQUt5VixnQkFBTCxHQUF3QnpWLEtBQUt5VixnQkFBTCxDQUFzQnJVLENBQXRCLENBQXhCLEdBQW1EcEIsS0FBSzBWLGNBQUwsQ0FBcUJ0VSxFQUFFb0IsTUFBRixDQUFTLENBQVQsTUFBZ0IsR0FBakIsR0FBd0JwQixFQUFFNFEsTUFBRixDQUFTLENBQVQsQ0FBeEIsR0FBc0M1USxDQUExRCxDQUFoRztBQUNBLEVBUEQ7O0FBU0EsS0FBSWlSLGNBQWMsRUFBbEI7QUFBQSxLQUNDc0QsY0FBYyxFQURmO0FBQUEsS0FFQ0MsY0FBYyxnREFGZjtBQUFBLEtBR0NDLFVBQVUsZ0JBSFg7O0FBSUM7QUFDQUMsYUFBWSxTQUFaQSxTQUFZLENBQVMzUixDQUFULEVBQVk7QUFDdkIsTUFBSTRSLEtBQUssS0FBS2YsUUFBZDtBQUFBLE1BQ0N2UyxNQUFNLFFBRFA7QUFBQSxNQUVDdVQsR0FGRDtBQUdBLFNBQU9ELEVBQVAsRUFBVztBQUNWQyxTQUFNLENBQUNELEdBQUdFLElBQUosR0FBV0YsR0FBR2hMLENBQUgsR0FBTzVHLENBQVAsR0FBVzRSLEdBQUcvSyxDQUF6QixHQUE4QjdHLE1BQU0sQ0FBTixJQUFXLEtBQUsrUixHQUFMLElBQVksSUFBeEIsR0FBZ0MsS0FBS0EsR0FBckMsR0FBMkMvUixJQUFJLEtBQUs4RCxJQUFMLENBQVUsRUFBVixDQUFKLEdBQW9CLEtBQUtrTyxLQUF2RztBQUNBLE9BQUlKLEdBQUdLLENBQVAsRUFBVTtBQUNUSixVQUFNRCxHQUFHSyxDQUFILENBQUtKLEdBQUwsRUFBVSxLQUFLN1MsT0FBTCxJQUFnQjRTLEdBQUdyTSxDQUE3QixDQUFOO0FBQ0EsSUFGRCxNQUVPLElBQUlzTSxNQUFNdlQsR0FBVixFQUFlLElBQUl1VCxNQUFNLENBQUN2VCxHQUFQLElBQWMsQ0FBQ3NULEdBQUdFLElBQXRCLEVBQTRCO0FBQUU7QUFDbkRELFVBQU0sQ0FBTjtBQUNBO0FBQ0QsT0FBSSxDQUFDRCxHQUFHcEksQ0FBUixFQUFXO0FBQ1ZvSSxPQUFHck0sQ0FBSCxDQUFLcU0sR0FBRzFVLENBQVIsSUFBYTJVLEdBQWI7QUFDQSxJQUZELE1BRU8sSUFBSUQsR0FBR00sRUFBUCxFQUFXO0FBQ2pCTixPQUFHck0sQ0FBSCxDQUFLcU0sR0FBRzFVLENBQVIsRUFBVzBVLEdBQUdNLEVBQWQsRUFBa0JMLEdBQWxCO0FBQ0EsSUFGTSxNQUVBO0FBQ05ELE9BQUdyTSxDQUFILENBQUtxTSxHQUFHMVUsQ0FBUixFQUFXMlUsR0FBWDtBQUNBO0FBQ0RELFFBQUtBLEdBQUdwRyxLQUFSO0FBQ0E7QUFDRCxFQXpCRjs7QUEwQkM7QUFDQTJHLFlBQVcsU0FBWEEsUUFBVyxDQUFTSCxLQUFULEVBQWdCRCxHQUFoQixFQUFxQjdXLE1BQXJCLEVBQTZCMFcsRUFBN0IsRUFBaUM7QUFDM0MsTUFBSTNQLElBQUksRUFBUjtBQUFBLE1BQ0NtUSxZQUFZLENBRGI7QUFBQSxNQUVDdkwsSUFBSSxFQUZMO0FBQUEsTUFHQ3dMLFFBQVEsQ0FIVDtBQUFBLE1BSUNDLFNBSkQ7QUFBQSxNQUlZQyxPQUpaO0FBQUEsTUFJcUJDLEdBSnJCO0FBQUEsTUFJMEJyUSxDQUoxQjtBQUFBLE1BSTZCSyxDQUo3QjtBQUFBLE1BSWdDaVEsVUFKaEM7QUFBQSxNQUk0Q0MsVUFKNUM7QUFLQXpRLElBQUUrUCxLQUFGLEdBQVVBLEtBQVY7QUFDQS9QLElBQUU4UCxHQUFGLEdBQVFBLEdBQVI7QUFDQUMsVUFBUS9QLEVBQUUsQ0FBRixJQUFPK1AsUUFBUSxFQUF2QixDQVIyQyxDQVFoQjtBQUMzQkQsUUFBTTlQLEVBQUUsQ0FBRixJQUFPOFAsTUFBTSxFQUFuQjtBQUNBLE1BQUk3VyxNQUFKLEVBQVk7QUFDWEEsVUFBTytHLENBQVAsRUFEVyxDQUNBO0FBQ1grUCxXQUFRL1AsRUFBRSxDQUFGLENBQVI7QUFDQThQLFNBQU05UCxFQUFFLENBQUYsQ0FBTjtBQUNBO0FBQ0RBLElBQUU5RyxNQUFGLEdBQVcsQ0FBWDtBQUNBbVgsY0FBWU4sTUFBTVcsS0FBTixDQUFZbEIsV0FBWixLQUE0QixFQUF4QztBQUNBYyxZQUFVUixJQUFJWSxLQUFKLENBQVVsQixXQUFWLEtBQTBCLEVBQXBDO0FBQ0EsTUFBSUcsRUFBSixFQUFRO0FBQ1BBLE1BQUdwRyxLQUFILEdBQVcsSUFBWDtBQUNBb0csTUFBR0UsSUFBSCxHQUFVLENBQVY7QUFDQTdQLEtBQUU0TyxRQUFGLEdBQWE1TyxFQUFFMlEsUUFBRixHQUFhaEIsRUFBMUIsQ0FITyxDQUd1QjtBQUM5QjtBQUNEcFAsTUFBSStQLFFBQVFwWCxNQUFaO0FBQ0EsT0FBS2dILElBQUksQ0FBVCxFQUFZQSxJQUFJSyxDQUFoQixFQUFtQkwsR0FBbkIsRUFBd0I7QUFDdkJ1USxnQkFBYUgsUUFBUXBRLENBQVIsQ0FBYjtBQUNBc1EsZ0JBQWFWLElBQUlsRSxNQUFKLENBQVd1RSxTQUFYLEVBQXNCTCxJQUFJakUsT0FBSixDQUFZNEUsVUFBWixFQUF3Qk4sU0FBeEIsSUFBbUNBLFNBQXpELENBQWI7QUFDQXZMLFFBQU00TCxjQUFjLENBQUN0USxDQUFoQixHQUFxQnNRLFVBQXJCLEdBQWtDLEdBQXZDLENBSHVCLENBR3FCO0FBQzVDTCxnQkFBYUssV0FBV3RYLE1BQXhCO0FBQ0EsT0FBSWtYLEtBQUosRUFBVztBQUFFO0FBQ1pBLFlBQVEsQ0FBQ0EsUUFBUSxDQUFULElBQWMsQ0FBdEI7QUFDQSxJQUZELE1BRU8sSUFBSUksV0FBVzVFLE1BQVgsQ0FBa0IsQ0FBQyxDQUFuQixNQUEwQixPQUE5QixFQUF1QztBQUM3Q3dFLFlBQVEsQ0FBUjtBQUNBO0FBQ0QsT0FBSUssZUFBZUosVUFBVW5RLENBQVYsQ0FBZixJQUErQm1RLFVBQVVuWCxNQUFWLElBQW9CZ0gsQ0FBdkQsRUFBMEQ7QUFDekQwRSxTQUFLNkwsVUFBTDtBQUNBLElBRkQsTUFFTztBQUNOLFFBQUk3TCxDQUFKLEVBQU87QUFDTjVFLE9BQUVyRyxJQUFGLENBQU9pTCxDQUFQO0FBQ0FBLFNBQUksRUFBSjtBQUNBO0FBQ0QyTCxVQUFNcFUsV0FBV2tVLFVBQVVuUSxDQUFWLENBQVgsQ0FBTjtBQUNBRixNQUFFckcsSUFBRixDQUFPNFcsR0FBUDtBQUNBdlEsTUFBRTRPLFFBQUYsR0FBYSxFQUFDckYsT0FBT3ZKLEVBQUU0TyxRQUFWLEVBQW9CdEwsR0FBRXRELENBQXRCLEVBQXlCL0UsR0FBRytFLEVBQUU5RyxNQUFGLEdBQVMsQ0FBckMsRUFBd0MwTCxHQUFFMkwsR0FBMUMsRUFBK0M1TCxHQUFFLENBQUU4TCxXQUFXclUsTUFBWCxDQUFrQixDQUFsQixNQUF5QixHQUExQixHQUFpQ3dVLFNBQVNILFdBQVdyVSxNQUFYLENBQWtCLENBQWxCLElBQXVCLEdBQWhDLEVBQXFDLEVBQXJDLElBQTJDRCxXQUFXc1UsV0FBVzdFLE1BQVgsQ0FBa0IsQ0FBbEIsQ0FBWCxDQUE1RSxHQUFnSHpQLFdBQVdzVSxVQUFYLElBQXlCRixHQUExSSxLQUFtSixDQUFwTSxFQUF1TWhKLEdBQUUsQ0FBek0sRUFBNE15SSxHQUFHSSxTQUFTQSxRQUFRLENBQWxCLEdBQXVCOVYsS0FBS3VXLEtBQTVCLEdBQW9DLENBQWxQLEVBQWI7QUFDQTtBQUNBO0FBQ0RWLGdCQUFhTSxXQUFXdlgsTUFBeEI7QUFDQTtBQUNEMEwsT0FBS2tMLElBQUlsRSxNQUFKLENBQVd1RSxTQUFYLENBQUw7QUFDQSxNQUFJdkwsQ0FBSixFQUFPO0FBQ041RSxLQUFFckcsSUFBRixDQUFPaUwsQ0FBUDtBQUNBO0FBQ0Q1RSxJQUFFL0IsUUFBRixHQUFheVIsU0FBYjtBQUNBLE1BQUlELFFBQVFxQixJQUFSLENBQWFoQixHQUFiLENBQUosRUFBdUI7QUFBRTtBQUN4QjlQLEtBQUU4UCxHQUFGLEdBQVEsSUFBUjtBQUNBO0FBQ0QsU0FBTzlQLENBQVA7QUFDQSxFQXBGRjs7QUFxRkM7QUFDQStRLGlCQUFnQixTQUFoQkEsYUFBZ0IsQ0FBUy9VLE1BQVQsRUFBaUJnVixJQUFqQixFQUF1QmpCLEtBQXZCLEVBQThCRCxHQUE5QixFQUFtQ21CLGFBQW5DLEVBQWtEQyxHQUFsRCxFQUF1REMsU0FBdkQsRUFBa0VDLFlBQWxFLEVBQWdGNU0sS0FBaEYsRUFBdUY7QUFDdEcsTUFBSSxPQUFPc0wsR0FBUCxLQUFnQixVQUFwQixFQUFnQztBQUMvQkEsU0FBTUEsSUFBSXRMLFNBQVMsQ0FBYixFQUFnQnhJLE1BQWhCLENBQU47QUFDQTtBQUNELE1BQUlDLGVBQWNELE9BQU9nVixJQUFQLENBQWQsQ0FBSjtBQUFBLE1BQ0NLLGFBQWNwVixTQUFTLFVBQVYsR0FBd0IsRUFBeEIsR0FBK0IrVSxLQUFLbkYsT0FBTCxDQUFhLEtBQWIsS0FBdUIsT0FBTzdQLE9BQU8sUUFBUWdWLEtBQUtwRixNQUFMLENBQVksQ0FBWixDQUFmLENBQVAsS0FBMkMsVUFBbkUsR0FBaUZvRixJQUFqRixHQUF3RixRQUFRQSxLQUFLcEYsTUFBTCxDQUFZLENBQVosQ0FENUk7QUFBQSxNQUVDaEgsSUFBS21MLFVBQVUsS0FBWCxHQUFvQkEsS0FBcEIsR0FBNEIsQ0FBQ3NCLFVBQUQsR0FBY3JWLE9BQU9nVixJQUFQLENBQWQsR0FBNkJHLFlBQVluVixPQUFPcVYsVUFBUCxFQUFtQkYsU0FBbkIsQ0FBWixHQUE0Q25WLE9BQU9xVixVQUFQLEdBRjFHO0FBQUEsTUFHQ0MsYUFBYyxPQUFPeEIsR0FBUCxLQUFnQixRQUFoQixJQUE0QkEsSUFBSTFULE1BQUosQ0FBVyxDQUFYLE1BQWtCLEdBSDdEO0FBQUEsTUFJQ3VULEtBQUssRUFBQ3JNLEdBQUV0SCxNQUFILEVBQVdmLEdBQUUrVixJQUFiLEVBQW1CcE0sR0FBRUEsQ0FBckIsRUFBd0IyQyxHQUFHdEwsU0FBUyxVQUFwQyxFQUFpRHNWLElBQUcsQ0FBcEQsRUFBdUQ1UCxHQUFFc1AsaUJBQWlCRCxJQUExRSxFQUFnRmhCLEdBQUcsQ0FBQ2tCLEdBQUQsR0FBTyxDQUFQLEdBQVksT0FBT0EsR0FBUCxLQUFnQixVQUFqQixHQUErQkEsR0FBL0IsR0FBcUM1VyxLQUFLdVcsS0FBeEksRUFBZ0ovTCxJQUFHLENBQW5KLEVBQXNKSCxHQUFFMk0sYUFBYVYsU0FBU2QsSUFBSTFULE1BQUosQ0FBVyxDQUFYLElBQWdCLEdBQXpCLEVBQThCLEVBQTlCLElBQW9DRCxXQUFXMlQsSUFBSWxFLE1BQUosQ0FBVyxDQUFYLENBQVgsQ0FBakQsR0FBOEV6UCxXQUFXMlQsR0FBWCxJQUFrQmxMLENBQW5CLElBQXlCLENBQTlQLEVBSk47QUFBQSxNQUtDaUwsSUFMRDs7QUFPQSxNQUFJLE9BQU9qTCxDQUFQLEtBQWMsUUFBZCxJQUEyQixPQUFPa0wsR0FBUCxLQUFnQixRQUFoQixJQUE0QixDQUFDd0IsVUFBNUQsRUFBeUU7QUFDeEUsT0FBSUgsYUFBYWpWLE1BQU0wSSxDQUFOLENBQWIsSUFBMEIsQ0FBQzBNLFVBQUQsSUFBZXBWLE1BQU00VCxHQUFOLENBQXpDLElBQXdELE9BQU9sTCxDQUFQLEtBQWMsU0FBdEUsSUFBbUYsT0FBT2tMLEdBQVAsS0FBZ0IsU0FBdkcsRUFBa0g7QUFDakg7QUFDQUgsT0FBR00sRUFBSCxHQUFRa0IsU0FBUjtBQUNBdEIsV0FBT0ssU0FBU3RMLENBQVQsRUFBYTBNLGFBQWFuVixXQUFXd1QsR0FBRy9LLENBQWQsSUFBbUIrSyxHQUFHaEwsQ0FBbkMsR0FBdUNtTCxHQUFwRCxFQUEwRHNCLGdCQUFnQnpXLFVBQVU2VyxtQkFBcEYsRUFBeUc3QixFQUF6RyxDQUFQO0FBQ0FBLFNBQUssRUFBQ3JNLEdBQUd1TSxJQUFKLEVBQVU1VSxHQUFHLFVBQWIsRUFBeUIySixHQUFHLENBQTVCLEVBQStCRCxHQUFHLENBQWxDLEVBQXFDNEMsR0FBRyxDQUF4QyxFQUEyQ2dLLElBQUksQ0FBL0MsRUFBa0Q1UCxHQUFHc1AsaUJBQWlCRCxJQUF0RSxFQUE0RWxNLElBQUksQ0FBaEYsRUFBbUZrTCxHQUFHLENBQXRGLEVBQUwsQ0FKaUgsQ0FJbEI7QUFDL0YsSUFMRCxNQUtPO0FBQ05MLE9BQUcvSyxDQUFILEdBQU96SSxXQUFXeUksQ0FBWCxDQUFQO0FBQ0EsUUFBSSxDQUFDME0sVUFBTCxFQUFpQjtBQUNoQjNCLFFBQUdoTCxDQUFILEdBQVF4SSxXQUFXMlQsR0FBWCxJQUFrQkgsR0FBRy9LLENBQXRCLElBQTRCLENBQW5DO0FBQ0E7QUFDRDtBQUNEO0FBQ0QsTUFBSStLLEdBQUdoTCxDQUFQLEVBQVU7QUFBRTtBQUNYLE9BQUtnTCxHQUFHcEcsS0FBSCxHQUFXLEtBQUtxRixRQUFyQixFQUFnQztBQUMvQmUsT0FBR3BHLEtBQUgsQ0FBU2dFLEtBQVQsR0FBaUJvQyxFQUFqQjtBQUNBO0FBQ0QsUUFBS2YsUUFBTCxHQUFnQmUsRUFBaEI7QUFDQSxVQUFPQSxFQUFQO0FBQ0E7QUFDRCxFQXJIRjtBQUFBLEtBc0hDOEIsYUFBYTlXLFVBQVU4VyxVQUFWLEdBQXVCLEVBQUNDLFNBQVFqUixRQUFULEVBQW1CaU4sWUFBV2EsV0FBOUIsRUFBMkNvRCxZQUFXMUYsV0FBdEQsRUFBbUUyRixTQUFRMUIsUUFBM0UsRUF0SHJDO0FBQUEsS0FzSDJIO0FBQzFIdkIsWUFBV2hVLFVBQVVnVSxRQUFWLEdBQXFCLEVBdkhqQztBQUFBLEtBd0hDa0QsZUFBZUosV0FBV0ssV0FBWCxHQUF5QixFQXhIekM7QUFBQSxLQXlIQ0Msa0JBQWtCLENBekhuQjtBQUFBLEtBMEhDckQsaUJBQWlCK0MsV0FBV08sYUFBWCxHQUEyQixFQUFDcFosTUFBSyxDQUFOLEVBQVN1UCxPQUFNLENBQWYsRUFBa0J5RixXQUFVLENBQTVCLEVBQStCN1UsWUFBVyxDQUExQyxFQUE2Q2taLGtCQUFpQixDQUE5RCxFQUFpRUMsaUJBQWdCLENBQWpGLEVBQW9GdkosV0FBVSxDQUE5RixFQUFpR3dKLGNBQWEsQ0FBOUcsRUFBaUhDLFNBQVEsQ0FBekgsRUFBNEhDLFVBQVMsQ0FBckksRUFBd0lDLGdCQUFlLENBQXZKLEVBQTBKQyxlQUFjLENBQXhLLEVBQTJLQyxTQUFRLENBQW5MLEVBQXNMQyxlQUFjLENBQXBNLEVBQXVNQyxjQUFhLENBQXBOLEVBQXVOQyxtQkFBa0IsQ0FBek8sRUFBNE9DLHlCQUF3QixDQUFwUSxFQUF1UUMsd0JBQXVCLENBQTlSLEVBQWlTQyxVQUFTLENBQTFTLEVBQTZTQyxnQkFBZSxDQUE1VCxFQUErVEMsZUFBYyxDQUE3VSxFQUFnVkMsWUFBVyxDQUEzVixFQUE4VkMsTUFBSyxDQUFuVyxFQUFzVzVLLGlCQUFnQixDQUF0WCxFQUF5WDZLLFFBQU8sQ0FBaFksRUFBbVlDLGFBQVksQ0FBL1ksRUFBa1ozYSxNQUFLLENBQXZaLEVBQTBac1EsUUFBTyxDQUFqYSxFQUFvYVAsVUFBUyxDQUE3YSxFQUFnYjZLLFNBQVEsQ0FBeGIsRUFBMmJDLE1BQUssQ0FBaGMsRUFBbWNDLGFBQVksQ0FBL2MsRUFBa2Q3SCxlQUFjLENBQWhlLEVBQW1lMEYsY0FBYSxDQUFoZixFQUFtZm9DLElBQUcsQ0FBdGYsRUFBeWZDLFVBQVMsQ0FBbGdCLEVBMUg3QztBQUFBLEtBMkhDekYsbUJBQW1CLEVBQUMwRixNQUFLLENBQU4sRUFBU0MsS0FBSSxDQUFiLEVBQWdCQyxNQUFLLENBQXJCLEVBQXdCQyxZQUFXLENBQW5DLEVBQXNDQyxZQUFXLENBQWpELEVBQW9EQyxhQUFZLENBQWhFLEVBQW1FLFFBQU8sQ0FBMUUsRUFBNkUsU0FBUSxDQUFyRixFQTNIcEI7QUFBQSxLQTRIQ25MLHNCQUFzQmYsVUFBVWUsbUJBQVYsR0FBZ0MsSUFBSWdFLGNBQUosRUE1SHZEO0FBQUEsS0E2SENuRSxnQkFBZ0JaLFVBQVVZLGFBQVYsR0FBMEIsSUFBSW1FLGNBQUosRUE3SDNDO0FBQUEsS0E4SENvSCxlQUFlLEVBOUhoQjtBQUFBLEtBK0hDOUgsY0FBY3VGLFdBQVd3QyxVQUFYLEdBQXdCLFlBQVc7QUFDaEQsTUFBSS9ULElBQUkrTCxZQUFZL1MsTUFBcEI7QUFBQSxNQUNDMkQsS0FERDtBQUVBMFMsZ0JBQWMsRUFBZDtBQUNBLFNBQU8sRUFBRXJQLENBQUYsR0FBTSxDQUFDLENBQWQsRUFBaUI7QUFDaEJyRCxXQUFRb1AsWUFBWS9MLENBQVosQ0FBUjtBQUNBLE9BQUlyRCxTQUFTQSxNQUFNbVMsS0FBTixLQUFnQixLQUE3QixFQUFvQztBQUNuQ25TLFVBQU02TixNQUFOLENBQWE3TixNQUFNbVMsS0FBTixDQUFZLENBQVosQ0FBYixFQUE2Qm5TLE1BQU1tUyxLQUFOLENBQVksQ0FBWixDQUE3QixFQUE2QyxJQUE3QztBQUNBblMsVUFBTW1TLEtBQU4sR0FBYyxLQUFkO0FBQ0E7QUFDRDtBQUNEL0MsY0FBWS9TLE1BQVosR0FBcUIsQ0FBckI7QUFDQSxFQTNJRjs7QUE2SUF1UCxlQUFjMUMsVUFBZCxHQUEyQmhGLFFBQVErRixJQUFuQztBQUNBOEIscUJBQW9CN0MsVUFBcEIsR0FBaUNoRixRQUFRZ0csS0FBekM7QUFDQTBCLGVBQWNKLE9BQWQsR0FBd0JPLG9CQUFvQlAsT0FBcEIsR0FBOEIsSUFBdEQ7QUFDQWIsWUFBVzBFLFdBQVgsRUFBd0IsQ0FBeEIsRUF2bEM0QixDQXVsQ0E7O0FBRTVCckUsV0FBVXFNLFdBQVYsR0FBd0J2WixVQUFVK1AsTUFBVixHQUFtQixZQUFXO0FBQ3BELE1BQUl4SyxDQUFKLEVBQU9GLENBQVAsRUFBVS9FLENBQVY7QUFDQSxNQUFJZ1IsWUFBWS9TLE1BQWhCLEVBQXdCO0FBQUU7QUFDekJnVDtBQUNBO0FBQ0R6RCxnQkFBY2lDLE1BQWQsQ0FBcUIsQ0FBQzNKLFFBQVErRixJQUFSLEdBQWUyQixjQUFjMUMsVUFBOUIsSUFBNEMwQyxjQUFjTCxVQUEvRSxFQUEyRixLQUEzRixFQUFrRyxLQUFsRztBQUNBUSxzQkFBb0I4QixNQUFwQixDQUEyQixDQUFDM0osUUFBUWdHLEtBQVIsR0FBZ0I2QixvQkFBb0I3QyxVQUFyQyxJQUFtRDZDLG9CQUFvQlIsVUFBbEcsRUFBOEcsS0FBOUcsRUFBcUgsS0FBckg7QUFDQSxNQUFJNkQsWUFBWS9TLE1BQWhCLEVBQXdCO0FBQ3ZCZ1Q7QUFDQTtBQUNELE1BQUluTCxRQUFRZ0csS0FBUixJQUFpQmlOLFlBQXJCLEVBQW1DO0FBQUU7QUFDcENBLGtCQUFlalQsUUFBUWdHLEtBQVIsSUFBaUI2SixTQUFTalcsVUFBVXdVLFNBQW5CLEVBQThCLEVBQTlCLEtBQXFDLEdBQXRELENBQWY7QUFDQSxRQUFLbFUsQ0FBTCxJQUFVNFcsWUFBVixFQUF3QjtBQUN2QjdSLFFBQUk2UixhQUFhNVcsQ0FBYixFQUFnQmtaLE1BQXBCO0FBQ0FqVSxRQUFJRixFQUFFOUcsTUFBTjtBQUNBLFdBQU8sRUFBRWdILENBQUYsR0FBTSxDQUFDLENBQWQsRUFBaUI7QUFDaEIsU0FBSUYsRUFBRUUsQ0FBRixFQUFLZ0osR0FBVCxFQUFjO0FBQ2JsSixRQUFFNkUsTUFBRixDQUFTM0UsQ0FBVCxFQUFZLENBQVo7QUFDQTtBQUNEO0FBQ0QsUUFBSUYsRUFBRTlHLE1BQUYsS0FBYSxDQUFqQixFQUFvQjtBQUNuQixZQUFPMlksYUFBYTVXLENBQWIsQ0FBUDtBQUNBO0FBQ0Q7QUFDRDtBQUNBQSxPQUFJd04sY0FBY3FFLE1BQWxCO0FBQ0EsT0FBSSxDQUFDN1IsQ0FBRCxJQUFNQSxFQUFFbU8sT0FBWixFQUFxQixJQUFJek8sVUFBVXdVLFNBQVYsSUFBdUIsQ0FBQ3ZHLG9CQUFvQmtFLE1BQTVDLElBQXNEL0wsUUFBUWlELFVBQVIsQ0FBbUJnRCxJQUFuQixDQUF3QjlOLE1BQXhCLEtBQW1DLENBQTdGLEVBQWdHO0FBQ3BILFdBQU8rQixLQUFLQSxFQUFFbU8sT0FBZCxFQUF1QjtBQUN0Qm5PLFNBQUlBLEVBQUVzTyxLQUFOO0FBQ0E7QUFDRCxRQUFJLENBQUN0TyxDQUFMLEVBQVE7QUFDUDhGLGFBQVFxRyxLQUFSO0FBQ0E7QUFDRDtBQUNEO0FBQ0QsRUFuQ0Y7O0FBcUNBckcsU0FBUW1ELGdCQUFSLENBQXlCLE1BQXpCLEVBQWlDMkQsVUFBVXFNLFdBQTNDOztBQUVBLEtBQUk3RixZQUFZLFNBQVpBLFNBQVksQ0FBU3JTLE1BQVQsRUFBaUJhLEtBQWpCLEVBQXdCdVgsS0FBeEIsRUFBK0I7QUFDN0MsTUFBSVosS0FBS3hYLE9BQU9xWSxVQUFoQjtBQUFBLE1BQTRCclUsQ0FBNUI7QUFBQSxNQUErQkUsQ0FBL0I7QUFDQSxNQUFJLENBQUMyUixhQUFhMkIsT0FBT3hYLE9BQU9xWSxVQUFQLEdBQW9CYixLQUFLLE1BQU96QixpQkFBdkMsQ0FBYixDQUFMLEVBQStFO0FBQzlFRixnQkFBYTJCLEVBQWIsSUFBbUIsRUFBQ3hYLFFBQU9BLE1BQVIsRUFBZ0JtWSxRQUFPLEVBQXZCLEVBQW5CO0FBQ0E7QUFDRCxNQUFJdFgsS0FBSixFQUFXO0FBQ1ZtRCxPQUFJNlIsYUFBYTJCLEVBQWIsRUFBaUJXLE1BQXJCO0FBQ0FuVSxLQUFHRSxJQUFJRixFQUFFOUcsTUFBVCxJQUFvQjJELEtBQXBCO0FBQ0EsT0FBSXVYLEtBQUosRUFBVztBQUNWLFdBQU8sRUFBRWxVLENBQUYsR0FBTSxDQUFDLENBQWQsRUFBaUI7QUFDaEIsU0FBSUYsRUFBRUUsQ0FBRixNQUFTckQsS0FBYixFQUFvQjtBQUNuQm1ELFFBQUU2RSxNQUFGLENBQVMzRSxDQUFULEVBQVksQ0FBWjtBQUNBO0FBQ0Q7QUFDRDtBQUNEO0FBQ0QsU0FBTzJSLGFBQWEyQixFQUFiLEVBQWlCVyxNQUF4QjtBQUNBLEVBakJGO0FBQUEsS0FrQkNHLGVBQWUsU0FBZkEsWUFBZSxDQUFTQyxnQkFBVCxFQUEyQkMsZ0JBQTNCLEVBQTZDeFksTUFBN0MsRUFBcUR5WSxXQUFyRCxFQUFrRTtBQUNoRixNQUFJclQsT0FBT21ULGlCQUFpQnRYLElBQWpCLENBQXNCc1csV0FBakM7QUFBQSxNQUE4Q21CLEVBQTlDO0FBQUEsTUFBa0RDLEVBQWxEO0FBQ0EsTUFBSXZULElBQUosRUFBVTtBQUNUc1QsUUFBS3RULEtBQUttVCxnQkFBTCxFQUF1QkMsZ0JBQXZCLEVBQXlDeFksTUFBekMsRUFBaUR5WSxXQUFqRCxDQUFMO0FBQ0E7QUFDRHJULFNBQU96RyxVQUFVNFksV0FBakI7QUFDQSxNQUFJblMsSUFBSixFQUFVO0FBQ1R1VCxRQUFLdlQsS0FBS21ULGdCQUFMLEVBQXVCQyxnQkFBdkIsRUFBeUN4WSxNQUF6QyxFQUFpRHlZLFdBQWpELENBQUw7QUFDQTtBQUNELFNBQVFDLE9BQU8sS0FBUCxJQUFnQkMsT0FBTyxLQUEvQjtBQUNBLEVBNUJGO0FBQUEsS0E2QkNyRyxrQkFBa0IsU0FBbEJBLGVBQWtCLENBQVN0UyxNQUFULEVBQWlCYSxLQUFqQixFQUF3QitYLEtBQXhCLEVBQStCQyxJQUEvQixFQUFxQ0MsUUFBckMsRUFBK0M7QUFDaEUsTUFBSTVVLENBQUosRUFBTzZVLE9BQVAsRUFBZ0JDLFFBQWhCLEVBQTBCelUsQ0FBMUI7QUFDQSxNQUFJc1UsU0FBUyxDQUFULElBQWNBLFFBQVEsQ0FBMUIsRUFBNkI7QUFDNUJ0VSxPQUFJdVUsU0FBUzViLE1BQWI7QUFDQSxRQUFLZ0gsSUFBSSxDQUFULEVBQVlBLElBQUlLLENBQWhCLEVBQW1CTCxHQUFuQixFQUF3QjtBQUN2QixRQUFJLENBQUM4VSxXQUFXRixTQUFTNVUsQ0FBVCxDQUFaLE1BQTZCckQsS0FBakMsRUFBd0M7QUFDdkMsU0FBSSxDQUFDbVksU0FBUzlMLEdBQWQsRUFBbUI7QUFDbEIsVUFBSThMLFNBQVNoVyxLQUFULENBQWUsSUFBZixFQUFxQmhELE1BQXJCLEVBQTZCYSxLQUE3QixDQUFKLEVBQXlDO0FBQ3hDa1ksaUJBQVUsSUFBVjtBQUNBO0FBQ0Q7QUFDRCxLQU5ELE1BTU8sSUFBSUYsU0FBUyxDQUFiLEVBQWdCO0FBQ3RCO0FBQ0E7QUFDRDtBQUNELFVBQU9FLE9BQVA7QUFDQTtBQUNEO0FBQ0EsTUFBSWhLLFlBQVlsTyxNQUFNa0osVUFBTixHQUFtQjNGLFFBQW5DO0FBQUEsTUFDQzZVLFdBQVcsRUFEWjtBQUFBLE1BRUNDLFNBQVMsQ0FGVjtBQUFBLE1BR0NDLFVBQVd0WSxNQUFNa0wsU0FBTixLQUFvQixDQUhoQztBQUFBLE1BSUNxTixXQUpEO0FBS0FsVixNQUFJNFUsU0FBUzViLE1BQWI7QUFDQSxTQUFPLEVBQUVnSCxDQUFGLEdBQU0sQ0FBQyxDQUFkLEVBQWlCO0FBQ2hCLE9BQUksQ0FBQzhVLFdBQVdGLFNBQVM1VSxDQUFULENBQVosTUFBNkJyRCxLQUE3QixJQUFzQ21ZLFNBQVM5TCxHQUEvQyxJQUFzRDhMLFNBQVM1TCxPQUFuRSxFQUE0RTtBQUMzRTtBQUNBLElBRkQsTUFFTyxJQUFJNEwsU0FBU3RMLFNBQVQsS0FBdUI3TSxNQUFNNk0sU0FBakMsRUFBNEM7QUFDbEQwTCxrQkFBY0EsZUFBZUMsY0FBY3hZLEtBQWQsRUFBcUIsQ0FBckIsRUFBd0JzWSxPQUF4QixDQUE3QjtBQUNBLFFBQUlFLGNBQWNMLFFBQWQsRUFBd0JJLFdBQXhCLEVBQXFDRCxPQUFyQyxNQUFrRCxDQUF0RCxFQUF5RDtBQUN4REYsY0FBU0MsUUFBVCxJQUFxQkYsUUFBckI7QUFDQTtBQUNELElBTE0sTUFLQSxJQUFJQSxTQUFTalAsVUFBVCxJQUF1QmdGLFNBQTNCLEVBQXNDLElBQUlpSyxTQUFTalAsVUFBVCxHQUFzQmlQLFNBQVN2SyxhQUFULEtBQTJCdUssU0FBUzVNLFVBQTFELEdBQXVFMkMsU0FBM0UsRUFBc0YsSUFBSSxFQUFFLENBQUNvSyxXQUFXLENBQUNILFNBQVM3TCxRQUF0QixLQUFtQzRCLFlBQVlpSyxTQUFTalAsVUFBckIsSUFBbUMsWUFBeEUsQ0FBSixFQUEyRjtBQUM3TmtQLGFBQVNDLFFBQVQsSUFBcUJGLFFBQXJCO0FBQ0E7QUFDRDs7QUFFRDlVLE1BQUlnVixNQUFKO0FBQ0EsU0FBTyxFQUFFaFYsQ0FBRixHQUFNLENBQUMsQ0FBZCxFQUFpQjtBQUNoQjhVLGNBQVdDLFNBQVMvVSxDQUFULENBQVg7QUFDQSxPQUFJMlUsU0FBUyxDQUFiLEVBQWdCLElBQUlHLFNBQVNoVyxLQUFULENBQWU0VixLQUFmLEVBQXNCNVksTUFBdEIsRUFBOEJhLEtBQTlCLENBQUosRUFBMEM7QUFDekRrWSxjQUFVLElBQVY7QUFDQTtBQUNELE9BQUlGLFNBQVMsQ0FBVCxJQUFlLENBQUNHLFNBQVNwRyxRQUFWLElBQXNCb0csU0FBUzdMLFFBQWxELEVBQTZEO0FBQzVELFFBQUkwTCxTQUFTLENBQVQsSUFBYyxDQUFDUCxhQUFhVSxRQUFiLEVBQXVCblksS0FBdkIsQ0FBbkIsRUFBa0Q7QUFDakQ7QUFDQTtBQUNELFFBQUltWSxTQUFTbkssUUFBVCxDQUFrQixLQUFsQixFQUF5QixLQUF6QixDQUFKLEVBQXFDO0FBQUU7QUFDdENrSyxlQUFVLElBQVY7QUFDQTtBQUNEO0FBQ0Q7QUFDRCxTQUFPQSxPQUFQO0FBQ0EsRUFsRkY7QUFBQSxLQW1GQ00sZ0JBQWdCLFNBQWhCQSxhQUFnQixDQUFTeFksS0FBVCxFQUFnQnlZLFNBQWhCLEVBQTJCSCxPQUEzQixFQUFvQztBQUNuRCxNQUFJek0sS0FBSzdMLE1BQU02TSxTQUFmO0FBQUEsTUFDQzZMLEtBQUs3TSxHQUFHTixVQURUO0FBQUEsTUFFQzlFLElBQUl6RyxNQUFNa0osVUFGWDtBQUdBLFNBQU8yQyxHQUFHZ0IsU0FBVixFQUFxQjtBQUNwQnBHLFFBQUtvRixHQUFHM0MsVUFBUjtBQUNBd1AsU0FBTTdNLEdBQUdOLFVBQVQ7QUFDQSxPQUFJTSxHQUFHVSxPQUFQLEVBQWdCO0FBQ2YsV0FBTyxDQUFDLEdBQVI7QUFDQTtBQUNEVixRQUFLQSxHQUFHZ0IsU0FBUjtBQUNBO0FBQ0RwRyxPQUFLaVMsRUFBTDtBQUNBLFNBQVFqUyxJQUFJZ1MsU0FBTCxHQUFrQmhTLElBQUlnUyxTQUF0QixHQUFvQ0gsV0FBVzdSLE1BQU1nUyxTQUFsQixJQUFpQyxDQUFDelksTUFBTXNNLFFBQVAsSUFBbUI3RixJQUFJZ1MsU0FBSixHQUFnQixJQUFJbFYsUUFBekUsR0FBc0ZBLFFBQXRGLEdBQWtHLENBQUNrRCxLQUFLekcsTUFBTTROLGFBQU4sS0FBd0I1TixNQUFNdUwsVUFBOUIsR0FBMkNtTixFQUFqRCxJQUF1REQsWUFBWWxWLFFBQXBFLEdBQWdGLENBQWhGLEdBQW9Ga0QsSUFBSWdTLFNBQUosR0FBZ0JsVixRQUE5TztBQUNBLEVBakdGOztBQW9HRjs7QUFFRW5GLEdBQUV1YSxLQUFGLEdBQVUsWUFBVztBQUNwQixNQUFJelgsSUFBSSxLQUFLZCxJQUFiO0FBQUEsTUFDQ3dZLEtBQUssS0FBSzVHLGlCQURYO0FBQUEsTUFFQzZHLE1BQU0sS0FBSzNOLFNBRlo7QUFBQSxNQUdDNE4sWUFBWSxDQUFDLENBQUM1WCxFQUFFdUssZUFIakI7QUFBQSxNQUlDMVAsT0FBT21GLEVBQUVuRixJQUpWO0FBQUEsTUFLQ3NILENBTEQ7QUFBQSxNQUtJMFYsV0FMSjtBQUFBLE1BS2lCakcsRUFMakI7QUFBQSxNQUtxQjFVLENBTHJCO0FBQUEsTUFLd0I0YSxTQUx4QjtBQUFBLE1BS21DdFYsQ0FMbkM7QUFNQSxNQUFJeEMsRUFBRXFVLE9BQU4sRUFBZTtBQUNkLE9BQUksS0FBS3RELFFBQVQsRUFBbUI7QUFDbEIsU0FBS0EsUUFBTCxDQUFjcEUsTUFBZCxDQUFxQixDQUFDLENBQXRCLEVBQXlCLElBQXpCLEVBRGtCLENBQ2M7QUFDaEMsU0FBS29FLFFBQUwsQ0FBY3ZRLElBQWQ7QUFDQTtBQUNEc1gsZUFBWSxFQUFaO0FBQ0EsUUFBSzVhLENBQUwsSUFBVThDLEVBQUVxVSxPQUFaLEVBQXFCO0FBQUU7QUFDdEJ5RCxjQUFVNWEsQ0FBVixJQUFlOEMsRUFBRXFVLE9BQUYsQ0FBVW5YLENBQVYsQ0FBZjtBQUNBO0FBQ0Q0YSxhQUFVcGQsSUFBVixHQUFpQixTQUFqQjtBQUNBb2QsYUFBVWpJLFNBQVYsR0FBc0IsS0FBdEI7QUFDQWlJLGFBQVV2TixlQUFWLEdBQTRCLElBQTVCO0FBQ0F1TixhQUFVdkMsSUFBVixHQUFrQnFDLGFBQWE1WCxFQUFFdVYsSUFBRixLQUFXLEtBQTFDO0FBQ0F1QyxhQUFVekQsT0FBVixHQUFvQnlELFVBQVUxTixLQUFWLEdBQWtCLElBQXRDLENBYmMsQ0FhOEI7QUFDNUMwTixhQUFVeEQsUUFBVixHQUFxQnRVLEVBQUVzVSxRQUF2QjtBQUNBd0QsYUFBVXZELGNBQVYsR0FBMkJ2VSxFQUFFdVUsY0FBN0I7QUFDQXVELGFBQVV0RCxhQUFWLEdBQTBCeFUsRUFBRXdVLGFBQUYsSUFBbUJ4VSxFQUFFMk4sYUFBckIsSUFBc0MsSUFBaEU7QUFDQSxRQUFLb0QsUUFBTCxHQUFnQm5VLFVBQVVqQyxFQUFWLENBQWEsS0FBS3NELE1BQWxCLEVBQTBCLENBQTFCLEVBQTZCNlosU0FBN0IsQ0FBaEI7QUFDQSxPQUFJRixTQUFKLEVBQWU7QUFDZCxRQUFJLEtBQUs3TSxLQUFMLEdBQWEsQ0FBakIsRUFBb0I7QUFDbkIsVUFBS2dHLFFBQUwsR0FBZ0IsSUFBaEIsQ0FEbUIsQ0FDRztBQUN0QixLQUZELE1BRU8sSUFBSTRHLFFBQVEsQ0FBWixFQUFlO0FBQ3JCLFlBRHFCLENBQ2I7QUFDUjtBQUNEO0FBQ0QsR0F6QkQsTUF5Qk8sSUFBSTNYLEVBQUVvVSxZQUFGLElBQWtCdUQsUUFBUSxDQUE5QixFQUFpQztBQUN2QztBQUNBLE9BQUksS0FBSzVHLFFBQVQsRUFBbUI7QUFDbEIsU0FBS0EsUUFBTCxDQUFjcEUsTUFBZCxDQUFxQixDQUFDLENBQXRCLEVBQXlCLElBQXpCO0FBQ0EsU0FBS29FLFFBQUwsQ0FBY3ZRLElBQWQ7QUFDQSxTQUFLdVEsUUFBTCxHQUFnQixJQUFoQjtBQUNBLElBSkQsTUFJTztBQUNOLFFBQUksS0FBS2hHLEtBQUwsS0FBZSxDQUFuQixFQUFzQjtBQUFFO0FBQ3ZCNk0saUJBQVksS0FBWjtBQUNBO0FBQ0RoRyxTQUFLLEVBQUw7QUFDQSxTQUFLMVUsQ0FBTCxJQUFVOEMsQ0FBVixFQUFhO0FBQUU7QUFDZCxTQUFJLENBQUMyUSxlQUFlelQsQ0FBZixDQUFELElBQXNCQSxNQUFNLFNBQWhDLEVBQTJDO0FBQzFDMFUsU0FBRzFVLENBQUgsSUFBUThDLEVBQUU5QyxDQUFGLENBQVI7QUFDQTtBQUNEO0FBQ0QwVSxPQUFHL0IsU0FBSCxHQUFlLENBQWY7QUFDQStCLE9BQUdsWCxJQUFILEdBQVUsYUFBVixDQVhNLENBV21CO0FBQ3pCa1gsT0FBRzJELElBQUgsR0FBV3FDLGFBQWE1WCxFQUFFdVYsSUFBRixLQUFXLEtBQW5DO0FBQ0EzRCxPQUFHckgsZUFBSCxHQUFxQnFOLFNBQXJCLENBYk0sQ0FhMEI7QUFDaEMsU0FBSzdHLFFBQUwsR0FBZ0JuVSxVQUFVakMsRUFBVixDQUFhLEtBQUtzRCxNQUFsQixFQUEwQixDQUExQixFQUE2QjJULEVBQTdCLENBQWhCO0FBQ0EsUUFBSSxDQUFDZ0csU0FBTCxFQUFnQjtBQUNmLFVBQUs3RyxRQUFMLENBQWMwRyxLQUFkLEdBRGUsQ0FDUTtBQUN2QixVQUFLMUcsUUFBTCxDQUFjakUsUUFBZCxDQUF1QixLQUF2QixFQUZlLENBRWdCO0FBQy9CLFNBQUksS0FBSzVOLElBQUwsQ0FBVXFMLGVBQWQsRUFBK0I7QUFDOUIsV0FBS3dHLFFBQUwsR0FBZ0IsSUFBaEI7QUFDQTtBQUNELEtBTkQsTUFNTyxJQUFJLEtBQUtoRyxLQUFMLEtBQWUsQ0FBbkIsRUFBc0I7QUFDNUI7QUFDQTtBQUNEO0FBQ0Q7QUFDRCxPQUFLb0csS0FBTCxHQUFhdFcsT0FBUSxDQUFDQSxJQUFGLEdBQVUrQixVQUFVc1UsV0FBcEIsR0FBbUNyVyxnQkFBZ0JxSixJQUFqQixHQUF5QnJKLElBQXpCLEdBQWlDLE9BQU9BLElBQVAsS0FBaUIsVUFBbEIsR0FBZ0MsSUFBSXFKLElBQUosQ0FBU3JKLElBQVQsRUFBZW1GLEVBQUVrVixVQUFqQixDQUFoQyxHQUErRHhRLFNBQVM3SixJQUFULEtBQWtCK0IsVUFBVXNVLFdBQWpMO0FBQ0EsTUFBSWxSLEVBQUVrVixVQUFGLFlBQXdCblMsS0FBeEIsSUFBaUNsSSxLQUFLa2QsTUFBMUMsRUFBa0Q7QUFDakQsUUFBSzVHLEtBQUwsR0FBYXRXLEtBQUtrZCxNQUFMLENBQVlyWCxLQUFaLENBQWtCN0YsSUFBbEIsRUFBd0JtRixFQUFFa1YsVUFBMUIsQ0FBYjtBQUNBO0FBQ0QsT0FBSzhDLFNBQUwsR0FBaUIsS0FBSzdHLEtBQUwsQ0FBVzdNLEtBQTVCO0FBQ0EsT0FBSzJULFVBQUwsR0FBa0IsS0FBSzlHLEtBQUwsQ0FBVzVNLE1BQTdCO0FBQ0EsT0FBS3NNLFFBQUwsR0FBZ0IsSUFBaEI7O0FBRUEsTUFBSSxLQUFLVixRQUFULEVBQW1CO0FBQ2xCM04sT0FBSSxLQUFLMk4sUUFBTCxDQUFjaFYsTUFBbEI7QUFDQSxRQUFLZ0gsSUFBSSxDQUFULEVBQVlBLElBQUlLLENBQWhCLEVBQW1CTCxHQUFuQixFQUF3QjtBQUN2QixRQUFLLEtBQUsrVixVQUFMLENBQWlCLEtBQUsvSCxRQUFMLENBQWNoTyxDQUFkLENBQWpCLEVBQW9DLEtBQUtpTyxXQUFMLENBQWlCak8sQ0FBakIsSUFBc0IsRUFBMUQsRUFBK0QsS0FBS2tPLFNBQUwsQ0FBZWxPLENBQWYsQ0FBL0QsRUFBbUZ1VixLQUFLQSxHQUFHdlYsQ0FBSCxDQUFMLEdBQWEsSUFBaEcsRUFBdUdBLENBQXZHLENBQUwsRUFBaUg7QUFDaEgwVixtQkFBYyxJQUFkO0FBQ0E7QUFDRDtBQUNELEdBUEQsTUFPTztBQUNOQSxpQkFBYyxLQUFLSyxVQUFMLENBQWdCLEtBQUtqYSxNQUFyQixFQUE2QixLQUFLbVMsV0FBbEMsRUFBK0MsS0FBS0MsU0FBcEQsRUFBK0RxSCxFQUEvRCxFQUFtRSxDQUFuRSxDQUFkO0FBQ0E7O0FBRUQsTUFBSUcsV0FBSixFQUFpQjtBQUNoQmpiLGFBQVV1YixjQUFWLENBQXlCLGlCQUF6QixFQUE0QyxJQUE1QyxFQURnQixDQUNtQztBQUNuRDtBQUNELE1BQUlULEVBQUosRUFBUSxJQUFJLENBQUMsS0FBSzdHLFFBQVYsRUFBb0IsSUFBSSxPQUFPLEtBQUs1UyxNQUFaLEtBQXdCLFVBQTVCLEVBQXdDO0FBQUU7QUFDckUsUUFBSzZPLFFBQUwsQ0FBYyxLQUFkLEVBQXFCLEtBQXJCO0FBQ0E7QUFDRCxNQUFJOU0sRUFBRW9VLFlBQU4sRUFBb0I7QUFDbkJ4QyxRQUFLLEtBQUtmLFFBQVY7QUFDQSxVQUFPZSxFQUFQLEVBQVc7QUFDVkEsT0FBRy9LLENBQUgsSUFBUStLLEdBQUdoTCxDQUFYO0FBQ0FnTCxPQUFHaEwsQ0FBSCxHQUFPLENBQUNnTCxHQUFHaEwsQ0FBWDtBQUNBZ0wsU0FBS0EsR0FBR3BHLEtBQVI7QUFDQTtBQUNEO0FBQ0QsT0FBS0UsU0FBTCxHQUFpQjFMLEVBQUVzVSxRQUFuQjtBQUNBLE9BQUtsSixRQUFMLEdBQWdCLElBQWhCO0FBQ0EsRUFuR0Q7O0FBcUdBbE8sR0FBRWdiLFVBQUYsR0FBZSxVQUFTamEsTUFBVCxFQUFpQm1hLFVBQWpCLEVBQTZCckIsUUFBN0IsRUFBdUNzQixnQkFBdkMsRUFBeUQ1UixLQUF6RCxFQUFnRTtBQUM5RSxNQUFJdkosQ0FBSixFQUFPaUYsQ0FBUCxFQUFVMFYsV0FBVixFQUF1QnBaLE1BQXZCLEVBQStCbVQsRUFBL0IsRUFBbUM1UixDQUFuQztBQUNBLE1BQUkvQixVQUFVLElBQWQsRUFBb0I7QUFDbkIsVUFBTyxLQUFQO0FBQ0E7O0FBRUQsTUFBSXVULFlBQVl2VCxPQUFPcVksVUFBbkIsQ0FBSixFQUFvQztBQUNuQ25JLGlCQURtQyxDQUNwQjtBQUNmOztBQUVELE1BQUksQ0FBQyxLQUFLalAsSUFBTCxDQUFVd1IsR0FBZixFQUFvQixJQUFJelMsT0FBT25CLEtBQVgsRUFBa0IsSUFBSW1CLFdBQVcvRCxNQUFYLElBQXFCK0QsT0FBT2xCLFFBQWhDLEVBQTBDLElBQUk2VCxTQUFTRixHQUFiLEVBQWtCLElBQUksS0FBS3hSLElBQUwsQ0FBVW9XLE9BQVYsS0FBc0IsS0FBMUIsRUFBaUM7QUFBRTtBQUNwSTdFLFlBQVMsS0FBS3ZSLElBQWQsRUFBb0JqQixNQUFwQjtBQUNBO0FBQ0QsT0FBS2YsQ0FBTCxJQUFVLEtBQUtnQyxJQUFmLEVBQXFCO0FBQ3BCYyxPQUFJLEtBQUtkLElBQUwsQ0FBVWhDLENBQVYsQ0FBSjtBQUNBLE9BQUl5VCxlQUFlelQsQ0FBZixDQUFKLEVBQXVCO0FBQ3RCLFFBQUk4QyxDQUFKLEVBQU8sSUFBS0EsYUFBYStDLEtBQWQsSUFBeUIvQyxFQUFFcEUsSUFBRixJQUFVOEcsU0FBUzFDLENBQVQsQ0FBdkMsRUFBcUQsSUFBSUEsRUFBRThELElBQUYsQ0FBTyxFQUFQLEVBQVdnSyxPQUFYLENBQW1CLFFBQW5CLE1BQWlDLENBQUMsQ0FBdEMsRUFBeUM7QUFDcEcsVUFBSzVPLElBQUwsQ0FBVWhDLENBQVYsSUFBZThDLElBQUksS0FBS3VOLGlCQUFMLENBQXVCdk4sQ0FBdkIsRUFBMEIsSUFBMUIsQ0FBbkI7QUFDQTtBQUVELElBTEQsTUFLTyxJQUFJNFEsU0FBUzFULENBQVQsS0FBZSxDQUFDdUIsU0FBUyxJQUFJbVMsU0FBUzFULENBQVQsQ0FBSixFQUFWLEVBQTZCb2IsWUFBN0IsQ0FBMENyYSxNQUExQyxFQUFrRCxLQUFLaUIsSUFBTCxDQUFVaEMsQ0FBVixDQUFsRCxFQUFnRSxJQUFoRSxFQUFzRXVKLEtBQXRFLENBQW5CLEVBQWlHOztBQUV2RztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFLb0ssUUFBTCxHQUFnQmUsS0FBSyxFQUFDcEcsT0FBTSxLQUFLcUYsUUFBWixFQUFzQnRMLEdBQUU5RyxNQUF4QixFQUFnQ3ZCLEdBQUUsVUFBbEMsRUFBOEMySixHQUFFLENBQWhELEVBQW1ERCxHQUFFLENBQXJELEVBQXdENEMsR0FBRSxDQUExRCxFQUE2RDVGLEdBQUUxRyxDQUEvRCxFQUFrRXNXLElBQUcsQ0FBckUsRUFBd0V6TSxJQUFHdEksT0FBTzhaLFNBQWxGLEVBQTZGdEcsR0FBRSxDQUEvRixFQUFyQjtBQUNBOVAsUUFBSTFELE9BQU9rQixlQUFQLENBQXVCeEUsTUFBM0I7QUFDQSxXQUFPLEVBQUVnSCxDQUFGLEdBQU0sQ0FBQyxDQUFkLEVBQWlCO0FBQ2hCaVcsZ0JBQVczWixPQUFPa0IsZUFBUCxDQUF1QndDLENBQXZCLENBQVgsSUFBd0MsS0FBSzBPLFFBQTdDO0FBQ0E7QUFDRCxRQUFJcFMsT0FBTzhaLFNBQVAsSUFBb0I5WixPQUFPK1osZUFBL0IsRUFBZ0Q7QUFDL0NYLG1CQUFjLElBQWQ7QUFDQTtBQUNELFFBQUlwWixPQUFPZ2EsVUFBUCxJQUFxQmhhLE9BQU9pYSxTQUFoQyxFQUEyQztBQUMxQyxVQUFLMUgsdUJBQUwsR0FBK0IsSUFBL0I7QUFDQTtBQUNELFFBQUlZLEdBQUdwRyxLQUFQLEVBQWM7QUFDYm9HLFFBQUdwRyxLQUFILENBQVNnRSxLQUFULEdBQWlCb0MsRUFBakI7QUFDQTtBQUVELElBMUJNLE1BMEJBO0FBQ053RyxlQUFXbGIsQ0FBWCxJQUFnQjhWLGNBQWM3UyxJQUFkLENBQW1CLElBQW5CLEVBQXlCbEMsTUFBekIsRUFBaUNmLENBQWpDLEVBQW9DLEtBQXBDLEVBQTJDOEMsQ0FBM0MsRUFBOEM5QyxDQUE5QyxFQUFpRCxDQUFqRCxFQUFvRCxJQUFwRCxFQUEwRCxLQUFLZ0MsSUFBTCxDQUFVbVUsWUFBcEUsRUFBa0Y1TSxLQUFsRixDQUFoQjtBQUNBO0FBQ0Q7O0FBRUQsTUFBSTRSLGdCQUFKLEVBQXNCLElBQUksS0FBS3BYLEtBQUwsQ0FBV29YLGdCQUFYLEVBQTZCcGEsTUFBN0IsQ0FBSixFQUEwQztBQUFFO0FBQ2pFLFVBQU8sS0FBS2lhLFVBQUwsQ0FBZ0JqYSxNQUFoQixFQUF3Qm1hLFVBQXhCLEVBQW9DckIsUUFBcEMsRUFBOENzQixnQkFBOUMsRUFBZ0U1UixLQUFoRSxDQUFQO0FBQ0E7QUFDRCxNQUFJLEtBQUt1SixVQUFMLEdBQWtCLENBQXRCLEVBQXlCLElBQUksS0FBS2EsUUFBVCxFQUFtQixJQUFJa0csU0FBUzViLE1BQVQsR0FBa0IsQ0FBdEIsRUFBeUIsSUFBSW9WLGdCQUFnQnRTLE1BQWhCLEVBQXdCLElBQXhCLEVBQThCbWEsVUFBOUIsRUFBMEMsS0FBS3BJLFVBQS9DLEVBQTJEK0csUUFBM0QsQ0FBSixFQUEwRTtBQUM5SSxRQUFLOVYsS0FBTCxDQUFXbVgsVUFBWCxFQUF1Qm5hLE1BQXZCO0FBQ0EsVUFBTyxLQUFLaWEsVUFBTCxDQUFnQmphLE1BQWhCLEVBQXdCbWEsVUFBeEIsRUFBb0NyQixRQUFwQyxFQUE4Q3NCLGdCQUE5QyxFQUFnRTVSLEtBQWhFLENBQVA7QUFDQTtBQUNELE1BQUksS0FBS29LLFFBQVQsRUFBbUIsSUFBSyxLQUFLM1IsSUFBTCxDQUFVcVcsSUFBVixLQUFtQixLQUFuQixJQUE0QixLQUFLdkwsU0FBbEMsSUFBaUQsS0FBSzlLLElBQUwsQ0FBVXFXLElBQVYsSUFBa0IsQ0FBQyxLQUFLdkwsU0FBN0UsRUFBeUY7QUFBRTtBQUM3R3dILGVBQVl2VCxPQUFPcVksVUFBbkIsSUFBaUMsSUFBakM7QUFDQTtBQUNELFNBQU91QixXQUFQO0FBQ0EsRUE5REQ7O0FBZ0VBM2EsR0FBRXlQLE1BQUYsR0FBVyxVQUFTNUQsSUFBVCxFQUFla0QsY0FBZixFQUErQlcsS0FBL0IsRUFBc0M7QUFDaEQsTUFBSStMLFdBQVcsS0FBSzVOLEtBQXBCO0FBQUEsTUFDQ2hCLFdBQVcsS0FBS0MsU0FEakI7QUFBQSxNQUVDNE8sa0JBQWtCLEtBQUtyTixZQUZ4QjtBQUFBLE1BR0NzTixVQUhEO0FBQUEsTUFHYXpTLFFBSGI7QUFBQSxNQUd1QndMLEVBSHZCO0FBQUEsTUFHMkJrSCxXQUgzQjtBQUlBLE1BQUkvUCxRQUFRZ0IsV0FBVyxTQUFuQixJQUFnQ2hCLFFBQVEsQ0FBNUMsRUFBK0M7QUFBRTtBQUNoRCxRQUFLdUMsVUFBTCxHQUFrQixLQUFLUCxLQUFMLEdBQWFoQixRQUEvQjtBQUNBLFFBQUt1RSxLQUFMLEdBQWEsS0FBSzZDLEtBQUwsQ0FBVzdMLFFBQVgsR0FBc0IsS0FBSzZMLEtBQUwsQ0FBVzlMLFFBQVgsQ0FBb0IsQ0FBcEIsQ0FBdEIsR0FBK0MsQ0FBNUQ7QUFDQSxPQUFJLENBQUMsS0FBS21GLFNBQVYsRUFBc0I7QUFDckJxTyxpQkFBYSxJQUFiO0FBQ0F6UyxlQUFXLFlBQVg7QUFDQXdHLFlBQVNBLFNBQVMsS0FBS2pCLFNBQUwsQ0FBZW1ELGtCQUFqQyxDQUhxQixDQUdpQztBQUN0RDtBQUNELE9BQUkvRSxhQUFhLENBQWpCLEVBQW9CLElBQUksS0FBS3FCLFFBQUwsSUFBaUIsQ0FBQyxLQUFLbE0sSUFBTCxDQUFVcVcsSUFBNUIsSUFBb0MzSSxLQUF4QyxFQUErQztBQUFFO0FBQ3BFLFFBQUksS0FBSzVFLFVBQUwsS0FBb0IsS0FBSzJELFNBQUwsQ0FBZTNCLFNBQXZDLEVBQWtEO0FBQUU7QUFDbkRqQixZQUFPLENBQVA7QUFDQTtBQUNELFFBQUk2UCxrQkFBa0IsQ0FBbEIsSUFBd0I3UCxRQUFRLENBQVIsSUFBYUEsUUFBUSxDQUFDLFNBQTlDLElBQTZENlAsb0JBQW9CdlcsUUFBcEIsSUFBZ0MsS0FBSzNILElBQUwsS0FBYyxTQUEvRyxFQUEySCxJQUFJa2Usb0JBQW9CN1AsSUFBeEIsRUFBOEI7QUFBRTtBQUMxSjZELGFBQVEsSUFBUjtBQUNBLFNBQUlnTSxrQkFBa0J2VyxRQUF0QixFQUFnQztBQUMvQitELGlCQUFXLG1CQUFYO0FBQ0E7QUFDRDtBQUNELFNBQUttRixZQUFMLEdBQW9CdU4sY0FBZSxDQUFDN00sY0FBRCxJQUFtQmxELElBQW5CLElBQTJCNlAsb0JBQW9CN1AsSUFBaEQsR0FBd0RBLElBQXhELEdBQStEMUcsUUFBakcsQ0FWa0UsQ0FVeUM7QUFDM0c7QUFFRCxHQXJCRCxNQXFCTyxJQUFJMEcsT0FBTyxTQUFYLEVBQXNCO0FBQUU7QUFDOUIsUUFBS3VDLFVBQUwsR0FBa0IsS0FBS1AsS0FBTCxHQUFhLENBQS9CO0FBQ0EsUUFBS3VELEtBQUwsR0FBYSxLQUFLNkMsS0FBTCxDQUFXN0wsUUFBWCxHQUFzQixLQUFLNkwsS0FBTCxDQUFXOUwsUUFBWCxDQUFvQixDQUFwQixDQUF0QixHQUErQyxDQUE1RDtBQUNBLE9BQUlzVCxhQUFhLENBQWIsSUFBbUI1TyxhQUFhLENBQWIsSUFBa0I2TyxrQkFBa0IsQ0FBM0QsRUFBK0Q7QUFDOUR4UyxlQUFXLG1CQUFYO0FBQ0F5UyxpQkFBYSxLQUFLck8sU0FBbEI7QUFDQTtBQUNELE9BQUl6QixPQUFPLENBQVgsRUFBYztBQUNiLFNBQUt1QixPQUFMLEdBQWUsS0FBZjtBQUNBLFFBQUlQLGFBQWEsQ0FBakIsRUFBb0IsSUFBSSxLQUFLcUIsUUFBTCxJQUFpQixDQUFDLEtBQUtsTSxJQUFMLENBQVVxVyxJQUE1QixJQUFvQzNJLEtBQXhDLEVBQStDO0FBQUU7QUFDcEUsU0FBSWdNLG1CQUFtQixDQUFuQixJQUF3QixFQUFFQSxvQkFBb0J2VyxRQUFwQixJQUFnQyxLQUFLM0gsSUFBTCxLQUFjLFNBQWhELENBQTVCLEVBQXdGO0FBQ3ZGa1MsY0FBUSxJQUFSO0FBQ0E7QUFDRCxVQUFLckIsWUFBTCxHQUFvQnVOLGNBQWUsQ0FBQzdNLGNBQUQsSUFBbUJsRCxJQUFuQixJQUEyQjZQLG9CQUFvQjdQLElBQWhELEdBQXdEQSxJQUF4RCxHQUErRDFHLFFBQWpHLENBSmtFLENBSXlDO0FBQzNHO0FBQ0Q7QUFDRCxPQUFJLENBQUMsS0FBSytJLFFBQU4sSUFBbUIsS0FBSzJGLFFBQUwsSUFBaUIsS0FBS0EsUUFBTCxDQUFjM0MsUUFBZCxFQUF4QyxFQUFtRTtBQUFFO0FBQ3BFeEIsWUFBUSxJQUFSO0FBQ0E7QUFDRCxHQW5CTSxNQW1CQTtBQUNOLFFBQUt0QixVQUFMLEdBQWtCLEtBQUtQLEtBQUwsR0FBYWhDLElBQS9COztBQUVBLE9BQUksS0FBS2lQLFNBQVQsRUFBb0I7QUFDbkIsUUFBSXZTLElBQUlzRCxPQUFPZ0IsUUFBZjtBQUFBLFFBQXlCN0wsT0FBTyxLQUFLOFosU0FBckM7QUFBQSxRQUFnRGUsTUFBTSxLQUFLZCxVQUEzRDtBQUNBLFFBQUkvWixTQUFTLENBQVQsSUFBZUEsU0FBUyxDQUFULElBQWN1SCxLQUFLLEdBQXRDLEVBQTRDO0FBQzNDQSxTQUFJLElBQUlBLENBQVI7QUFDQTtBQUNELFFBQUl2SCxTQUFTLENBQWIsRUFBZ0I7QUFDZnVILFVBQUssQ0FBTDtBQUNBO0FBQ0QsUUFBSXNULFFBQVEsQ0FBWixFQUFlO0FBQ2R0VCxVQUFLQSxDQUFMO0FBQ0EsS0FGRCxNQUVPLElBQUlzVCxRQUFRLENBQVosRUFBZTtBQUNyQnRULFVBQUtBLElBQUlBLENBQVQ7QUFDQSxLQUZNLE1BRUEsSUFBSXNULFFBQVEsQ0FBWixFQUFlO0FBQ3JCdFQsVUFBS0EsSUFBSUEsQ0FBSixHQUFRQSxDQUFiO0FBQ0EsS0FGTSxNQUVBLElBQUlzVCxRQUFRLENBQVosRUFBZTtBQUNyQnRULFVBQUtBLElBQUlBLENBQUosR0FBUUEsQ0FBUixHQUFZQSxDQUFqQjtBQUNBOztBQUVELFFBQUl2SCxTQUFTLENBQWIsRUFBZ0I7QUFDZixVQUFLb1EsS0FBTCxHQUFhLElBQUk3SSxDQUFqQjtBQUNBLEtBRkQsTUFFTyxJQUFJdkgsU0FBUyxDQUFiLEVBQWdCO0FBQ3RCLFVBQUtvUSxLQUFMLEdBQWE3SSxDQUFiO0FBQ0EsS0FGTSxNQUVBLElBQUlzRCxPQUFPZ0IsUUFBUCxHQUFrQixHQUF0QixFQUEyQjtBQUNqQyxVQUFLdUUsS0FBTCxHQUFhN0ksSUFBSSxDQUFqQjtBQUNBLEtBRk0sTUFFQTtBQUNOLFVBQUs2SSxLQUFMLEdBQWEsSUFBSzdJLElBQUksQ0FBdEI7QUFDQTtBQUVELElBNUJELE1BNEJPO0FBQ04sU0FBSzZJLEtBQUwsR0FBYSxLQUFLNkMsS0FBTCxDQUFXOUwsUUFBWCxDQUFvQjBELE9BQU9nQixRQUEzQixDQUFiO0FBQ0E7QUFDRDs7QUFFRCxNQUFJLEtBQUtnQixLQUFMLEtBQWU0TixRQUFmLElBQTJCLENBQUMvTCxLQUFoQyxFQUF1QztBQUN0QztBQUNBLEdBRkQsTUFFTyxJQUFJLENBQUMsS0FBS3hCLFFBQVYsRUFBb0I7QUFDMUIsUUFBS3FNLEtBQUw7QUFDQSxPQUFJLENBQUMsS0FBS3JNLFFBQU4sSUFBa0IsS0FBS0QsR0FBM0IsRUFBZ0M7QUFBRTtBQUNqQztBQUNBLElBRkQsTUFFTyxJQUFJLENBQUN5QixLQUFELElBQVUsS0FBS2lFLFFBQWYsS0FBNkIsS0FBSzNSLElBQUwsQ0FBVXFXLElBQVYsS0FBbUIsS0FBbkIsSUFBNEIsS0FBS3ZMLFNBQWxDLElBQWlELEtBQUs5SyxJQUFMLENBQVVxVyxJQUFWLElBQWtCLENBQUMsS0FBS3ZMLFNBQXJHLENBQUosRUFBc0g7QUFDNUgsU0FBS2UsS0FBTCxHQUFhLEtBQUtPLFVBQUwsR0FBa0JxTixRQUEvQjtBQUNBLFNBQUtwTixZQUFMLEdBQW9CcU4sZUFBcEI7QUFDQTFLLGdCQUFZdFMsSUFBWixDQUFpQixJQUFqQjtBQUNBLFNBQUtxVixLQUFMLEdBQWEsQ0FBQ2xJLElBQUQsRUFBT2tELGNBQVAsQ0FBYjtBQUNBO0FBQ0E7QUFDRDtBQUNBLE9BQUksS0FBS2xCLEtBQUwsSUFBYyxDQUFDOE4sVUFBbkIsRUFBK0I7QUFDOUIsU0FBS3ZLLEtBQUwsR0FBYSxLQUFLNkMsS0FBTCxDQUFXOUwsUUFBWCxDQUFvQixLQUFLMEYsS0FBTCxHQUFhaEIsUUFBakMsQ0FBYjtBQUNBLElBRkQsTUFFTyxJQUFJOE8sY0FBYyxLQUFLMUgsS0FBTCxDQUFXN0wsUUFBN0IsRUFBdUM7QUFDN0MsU0FBS2dKLEtBQUwsR0FBYSxLQUFLNkMsS0FBTCxDQUFXOUwsUUFBWCxDQUFxQixLQUFLMEYsS0FBTCxLQUFlLENBQWhCLEdBQXFCLENBQXJCLEdBQXlCLENBQTdDLENBQWI7QUFDQTtBQUNEO0FBQ0QsTUFBSSxLQUFLa0csS0FBTCxLQUFlLEtBQW5CLEVBQTBCO0FBQUU7QUFDM0IsUUFBS0EsS0FBTCxHQUFhLEtBQWI7QUFDQTtBQUNELE1BQUksQ0FBQyxLQUFLM0csT0FBVixFQUFtQixJQUFJLENBQUMsS0FBS2UsT0FBTixJQUFpQixLQUFLTixLQUFMLEtBQWU0TixRQUFoQyxJQUE0QzVQLFFBQVEsQ0FBeEQsRUFBMkQ7QUFDN0UsUUFBS3VCLE9BQUwsR0FBZSxJQUFmLENBRDZFLENBQ3ZEO0FBQ3RCO0FBQ0QsTUFBSXFPLGFBQWEsQ0FBakIsRUFBb0I7QUFDbkIsT0FBSSxLQUFLNUgsUUFBVCxFQUFtQjtBQUNsQixRQUFJaEksUUFBUSxDQUFaLEVBQWU7QUFDZCxVQUFLZ0ksUUFBTCxDQUFjcEUsTUFBZCxDQUFxQjVELElBQXJCLEVBQTJCLElBQTNCLEVBQWlDNkQsS0FBakM7QUFDQSxLQUZELE1BRU8sSUFBSSxDQUFDeEcsUUFBTCxFQUFlO0FBQ3JCQSxnQkFBVyxVQUFYLENBRHFCLENBQ0U7QUFDdkI7QUFDRDtBQUNELE9BQUksS0FBS2xILElBQUwsQ0FBVXVWLE9BQWQsRUFBdUIsSUFBSSxLQUFLMUosS0FBTCxLQUFlLENBQWYsSUFBb0JoQixhQUFhLENBQXJDLEVBQXdDLElBQUksQ0FBQ2tDLGNBQUwsRUFBcUI7QUFDbkYsU0FBS3lCLFNBQUwsQ0FBZSxTQUFmO0FBQ0E7QUFDRDtBQUNEa0UsT0FBSyxLQUFLZixRQUFWO0FBQ0EsU0FBT2UsRUFBUCxFQUFXO0FBQ1YsT0FBSUEsR0FBR3BJLENBQVAsRUFBVTtBQUNUb0ksT0FBR3JNLENBQUgsQ0FBS3FNLEdBQUcxVSxDQUFSLEVBQVcwVSxHQUFHaEwsQ0FBSCxHQUFPLEtBQUswSCxLQUFaLEdBQW9Cc0QsR0FBRy9LLENBQWxDO0FBQ0EsSUFGRCxNQUVPO0FBQ04rSyxPQUFHck0sQ0FBSCxDQUFLcU0sR0FBRzFVLENBQVIsSUFBYTBVLEdBQUdoTCxDQUFILEdBQU8sS0FBSzBILEtBQVosR0FBb0JzRCxHQUFHL0ssQ0FBcEM7QUFDQTtBQUNEK0ssUUFBS0EsR0FBR3BHLEtBQVI7QUFDQTs7QUFFRCxNQUFJLEtBQUtFLFNBQVQsRUFBb0I7QUFDbkIsT0FBSTNDLE9BQU8sQ0FBWCxFQUFjLElBQUksS0FBS2dJLFFBQUwsSUFBaUJoSSxTQUFTLENBQUMsTUFBL0IsRUFBdUM7QUFBRTtBQUN0RCxTQUFLZ0ksUUFBTCxDQUFjcEUsTUFBZCxDQUFxQjVELElBQXJCLEVBQTJCLElBQTNCLEVBQWlDNkQsS0FBakMsRUFEb0QsQ0FDWDtBQUN6QztBQUNELE9BQUksQ0FBQ1gsY0FBTCxFQUFxQixJQUFJLEtBQUtsQixLQUFMLEtBQWU0TixRQUFmLElBQTJCRSxVQUEzQixJQUF5Q2pNLEtBQTdDLEVBQW9EO0FBQ3hFLFNBQUtjLFNBQUwsQ0FBZSxVQUFmO0FBQ0E7QUFDRDtBQUNELE1BQUl0SCxRQUFKLEVBQWMsSUFBSSxDQUFDLEtBQUsrRSxHQUFOLElBQWF5QixLQUFqQixFQUF3QjtBQUFFO0FBQ3ZDLE9BQUk3RCxPQUFPLENBQVAsSUFBWSxLQUFLZ0ksUUFBakIsSUFBNkIsQ0FBQyxLQUFLckYsU0FBbkMsSUFBZ0QzQyxTQUFTLENBQUMsTUFBOUQsRUFBc0U7QUFBRTtBQUN2RSxTQUFLZ0ksUUFBTCxDQUFjcEUsTUFBZCxDQUFxQjVELElBQXJCLEVBQTJCLElBQTNCLEVBQWlDNkQsS0FBakM7QUFDQTtBQUNELE9BQUlpTSxVQUFKLEVBQWdCO0FBQ2YsUUFBSSxLQUFLbE4sU0FBTCxDQUFlbUQsa0JBQW5CLEVBQXVDO0FBQ3RDLFVBQUtoQyxRQUFMLENBQWMsS0FBZCxFQUFxQixLQUFyQjtBQUNBO0FBQ0QsU0FBS3hDLE9BQUwsR0FBZSxLQUFmO0FBQ0E7QUFDRCxPQUFJLENBQUMyQixjQUFELElBQW1CLEtBQUsvTSxJQUFMLENBQVVrSCxRQUFWLENBQXZCLEVBQTRDO0FBQzNDLFNBQUtzSCxTQUFMLENBQWV0SCxRQUFmO0FBQ0E7QUFDRCxPQUFJMkQsYUFBYSxDQUFiLElBQWtCLEtBQUt3QixZQUFMLEtBQXNCbEosUUFBeEMsSUFBb0R5VyxnQkFBZ0J6VyxRQUF4RSxFQUFrRjtBQUFFO0FBQ25GLFNBQUtrSixZQUFMLEdBQW9CLENBQXBCO0FBQ0E7QUFDRDtBQUNELEVBMUpEOztBQTRKQXJPLEdBQUUrRCxLQUFGLEdBQVUsVUFBUy9CLElBQVQsRUFBZWpCLE1BQWYsRUFBdUJ3WSxnQkFBdkIsRUFBeUM7QUFDbEQsTUFBSXZYLFNBQVMsS0FBYixFQUFvQjtBQUNuQkEsVUFBTyxJQUFQO0FBQ0E7QUFDRCxNQUFJQSxRQUFRLElBQVosRUFBa0IsSUFBSWpCLFVBQVUsSUFBVixJQUFrQkEsV0FBVyxLQUFLQSxNQUF0QyxFQUE4QztBQUMvRCxRQUFLZ1QsS0FBTCxHQUFhLEtBQWI7QUFDQSxVQUFPLEtBQUtuRSxRQUFMLENBQWMsS0FBZCxFQUFxQixLQUFyQixDQUFQO0FBQ0E7QUFDRDdPLFdBQVUsT0FBT0EsTUFBUCxLQUFtQixRQUFwQixHQUFpQ0EsVUFBVSxLQUFLa1MsUUFBZixJQUEyQixLQUFLbFMsTUFBakUsR0FBMkVyQixVQUFVQyxRQUFWLENBQW1Cb0IsTUFBbkIsS0FBOEJBLE1BQWxIO0FBQ0EsTUFBSSthLHdCQUF5QnZDLG9CQUFvQixLQUFLMUwsS0FBekIsSUFBa0MwTCxpQkFBaUJ6TyxVQUFqQixLQUFnQyxLQUFLQSxVQUF2RSxJQUFxRixLQUFLMkQsU0FBTCxLQUFtQjhLLGlCQUFpQjlLLFNBQXRKO0FBQUEsTUFDQ3hKLENBREQ7QUFBQSxNQUNJa1csZ0JBREo7QUFBQSxNQUNzQm5iLENBRHRCO0FBQUEsTUFDeUIwVSxFQUR6QjtBQUFBLE1BQzZCd0csVUFEN0I7QUFBQSxNQUN5Q3BCLE9BRHpDO0FBQUEsTUFDa0RpQyxTQURsRDtBQUFBLE1BQzZEQyxNQUQ3RDtBQUFBLE1BQ3FFQyxNQURyRTtBQUVBLE1BQUksQ0FBQ3pXLFNBQVN6RSxNQUFULEtBQW9CdVMsWUFBWXZTLE1BQVosQ0FBckIsS0FBNkMsT0FBT0EsT0FBTyxDQUFQLENBQVAsS0FBc0IsUUFBdkUsRUFBaUY7QUFDaEZrRSxPQUFJbEUsT0FBTzlDLE1BQVg7QUFDQSxVQUFPLEVBQUVnSCxDQUFGLEdBQU0sQ0FBQyxDQUFkLEVBQWlCO0FBQ2hCLFFBQUksS0FBS2xCLEtBQUwsQ0FBVy9CLElBQVgsRUFBaUJqQixPQUFPa0UsQ0FBUCxDQUFqQixFQUE0QnNVLGdCQUE1QixDQUFKLEVBQW1EO0FBQ2xETyxlQUFVLElBQVY7QUFDQTtBQUNEO0FBQ0QsR0FQRCxNQU9PO0FBQ04sT0FBSSxLQUFLN0csUUFBVCxFQUFtQjtBQUNsQmhPLFFBQUksS0FBS2dPLFFBQUwsQ0FBY2hWLE1BQWxCO0FBQ0EsV0FBTyxFQUFFZ0gsQ0FBRixHQUFNLENBQUMsQ0FBZCxFQUFpQjtBQUNoQixTQUFJbEUsV0FBVyxLQUFLa1MsUUFBTCxDQUFjaE8sQ0FBZCxDQUFmLEVBQWlDO0FBQ2hDaVcsbUJBQWEsS0FBS2hJLFdBQUwsQ0FBaUJqTyxDQUFqQixLQUF1QixFQUFwQztBQUNBLFdBQUsyTyxpQkFBTCxHQUF5QixLQUFLQSxpQkFBTCxJQUEwQixFQUFuRDtBQUNBdUgseUJBQW1CLEtBQUt2SCxpQkFBTCxDQUF1QjNPLENBQXZCLElBQTRCakQsT0FBTyxLQUFLNFIsaUJBQUwsQ0FBdUIzTyxDQUF2QixLQUE2QixFQUFwQyxHQUF5QyxLQUF4RjtBQUNBO0FBQ0E7QUFDRDtBQUNELElBVkQsTUFVTyxJQUFJbEUsV0FBVyxLQUFLQSxNQUFwQixFQUE0QjtBQUNsQyxXQUFPLEtBQVA7QUFDQSxJQUZNLE1BRUE7QUFDTm1hLGlCQUFhLEtBQUtoSSxXQUFsQjtBQUNBaUksdUJBQW1CLEtBQUt2SCxpQkFBTCxHQUF5QjVSLE9BQU8sS0FBSzRSLGlCQUFMLElBQTBCLEVBQWpDLEdBQXNDLEtBQWxGO0FBQ0E7O0FBRUQsT0FBSXNILFVBQUosRUFBZ0I7QUFDZmEsZ0JBQVkvWixRQUFRa1osVUFBcEI7QUFDQWMsYUFBVWhhLFNBQVNtWixnQkFBVCxJQUE2QkEscUJBQXFCLEtBQWxELElBQTJEblosU0FBU2taLFVBQXBFLEtBQW1GLFFBQU9sWixJQUFQLHlDQUFPQSxJQUFQLE9BQWlCLFFBQWpCLElBQTZCLENBQUNBLEtBQUtrYSxTQUF0SCxDQUFWLENBRmUsQ0FFOEg7QUFDN0ksUUFBSTNDLHFCQUFxQjdaLFVBQVU0WSxXQUFWLElBQXlCLEtBQUt0VyxJQUFMLENBQVVzVyxXQUF4RCxDQUFKLEVBQTBFO0FBQ3pFLFVBQUt0WSxDQUFMLElBQVUrYixTQUFWLEVBQXFCO0FBQ3BCLFVBQUliLFdBQVdsYixDQUFYLENBQUosRUFBbUI7QUFDbEIsV0FBSSxDQUFDaWMsTUFBTCxFQUFhO0FBQ1pBLGlCQUFTLEVBQVQ7QUFDQTtBQUNEQSxjQUFPdmQsSUFBUCxDQUFZc0IsQ0FBWjtBQUNBO0FBQ0Q7QUFDRCxTQUFJLENBQUNpYyxVQUFVLENBQUNqYSxJQUFaLEtBQXFCLENBQUNxWCxhQUFhLElBQWIsRUFBbUJFLGdCQUFuQixFQUFxQ3hZLE1BQXJDLEVBQTZDa2IsTUFBN0MsQ0FBMUIsRUFBZ0Y7QUFBRTtBQUNqRixhQUFPLEtBQVA7QUFDQTtBQUNEOztBQUVELFNBQUtqYyxDQUFMLElBQVUrYixTQUFWLEVBQXFCO0FBQ3BCLFNBQUtySCxLQUFLd0csV0FBV2xiLENBQVgsQ0FBVixFQUEwQjtBQUN6QixVQUFJOGIscUJBQUosRUFBMkI7QUFBRTtBQUM1QixXQUFJcEgsR0FBR3BJLENBQVAsRUFBVTtBQUNUb0ksV0FBR3JNLENBQUgsQ0FBS3FNLEdBQUcxVSxDQUFSLEVBQVcwVSxHQUFHL0ssQ0FBZDtBQUNBLFFBRkQsTUFFTztBQUNOK0ssV0FBR3JNLENBQUgsQ0FBS3FNLEdBQUcxVSxDQUFSLElBQWEwVSxHQUFHL0ssQ0FBaEI7QUFDQTtBQUNEbVEsaUJBQVUsSUFBVjtBQUNBO0FBQ0QsVUFBSXBGLEdBQUc0QixFQUFILElBQVM1QixHQUFHck0sQ0FBSCxDQUFLdEUsS0FBTCxDQUFXZ1ksU0FBWCxDQUFiLEVBQW9DO0FBQ25DakMsaUJBQVUsSUFBVixDQURtQyxDQUNuQjtBQUNoQjtBQUNELFVBQUksQ0FBQ3BGLEdBQUc0QixFQUFKLElBQVU1QixHQUFHck0sQ0FBSCxDQUFLNUYsZUFBTCxDQUFxQnhFLE1BQXJCLEtBQWdDLENBQTlDLEVBQWlEO0FBQ2hELFdBQUl5VyxHQUFHcEMsS0FBUCxFQUFjO0FBQ2JvQyxXQUFHcEMsS0FBSCxDQUFTaEUsS0FBVCxHQUFpQm9HLEdBQUdwRyxLQUFwQjtBQUNBLFFBRkQsTUFFTyxJQUFJb0csT0FBTyxLQUFLZixRQUFoQixFQUEwQjtBQUNoQyxhQUFLQSxRQUFMLEdBQWdCZSxHQUFHcEcsS0FBbkI7QUFDQTtBQUNELFdBQUlvRyxHQUFHcEcsS0FBUCxFQUFjO0FBQ2JvRyxXQUFHcEcsS0FBSCxDQUFTZ0UsS0FBVCxHQUFpQm9DLEdBQUdwQyxLQUFwQjtBQUNBO0FBQ0RvQyxVQUFHcEcsS0FBSCxHQUFXb0csR0FBR3BDLEtBQUgsR0FBVyxJQUF0QjtBQUNBO0FBQ0QsYUFBTzRJLFdBQVdsYixDQUFYLENBQVA7QUFDQTtBQUNELFNBQUlnYyxNQUFKLEVBQVk7QUFDWGIsdUJBQWlCbmIsQ0FBakIsSUFBc0IsQ0FBdEI7QUFDQTtBQUNEO0FBQ0QsUUFBSSxDQUFDLEtBQUsyVCxRQUFOLElBQWtCLEtBQUt6RixRQUEzQixFQUFxQztBQUFFO0FBQ3RDLFVBQUswQixRQUFMLENBQWMsS0FBZCxFQUFxQixLQUFyQjtBQUNBO0FBQ0Q7QUFDRDtBQUNELFNBQU9rSyxPQUFQO0FBQ0EsRUF6RkQ7O0FBMkZBOVosR0FBRTJQLFVBQUYsR0FBZSxZQUFXO0FBQ3pCLE1BQUksS0FBS21FLHVCQUFULEVBQWtDO0FBQ2pDcFUsYUFBVXViLGNBQVYsQ0FBeUIsWUFBekIsRUFBdUMsSUFBdkM7QUFDQTtBQUNELE9BQUt0SCxRQUFMLEdBQWdCLEtBQUtDLGlCQUFMLEdBQXlCLEtBQUtDLFFBQUwsR0FBZ0IsS0FBS3JGLFNBQUwsR0FBaUIsSUFBMUU7QUFDQSxPQUFLc0YsdUJBQUwsR0FBK0IsS0FBSzFHLE9BQUwsR0FBZSxLQUFLMkcsS0FBTCxHQUFhLEtBQTNEO0FBQ0EsT0FBS2IsV0FBTCxHQUFvQixLQUFLRCxRQUFOLEdBQWtCLEVBQWxCLEdBQXVCLEVBQTFDO0FBQ0FyRyxZQUFVaEosU0FBVixDQUFvQitMLFVBQXBCLENBQStCMU0sSUFBL0IsQ0FBb0MsSUFBcEM7QUFDQSxNQUFJLEtBQUtqQixJQUFMLENBQVVxTCxlQUFkLEVBQStCO0FBQzlCLFFBQUtRLEtBQUwsR0FBYSxDQUFDMUksUUFBZCxDQUQ4QixDQUNOO0FBQ3hCLFFBQUtzSyxNQUFMLENBQVlwUSxLQUFLK0IsR0FBTCxDQUFTLENBQVQsRUFBWSxDQUFDLEtBQUs0TCxNQUFsQixDQUFaLEVBRjhCLENBRVU7QUFDeEM7QUFDRCxTQUFPLElBQVA7QUFDQSxFQWJEOztBQWVBaE4sR0FBRTRQLFFBQUYsR0FBYSxVQUFTSSxPQUFULEVBQWtCQyxjQUFsQixFQUFrQztBQUM5QyxNQUFJLENBQUNsSyxhQUFMLEVBQW9CO0FBQ25CRCxXQUFRMkQsSUFBUjtBQUNBO0FBQ0QsTUFBSXVHLFdBQVcsS0FBSy9CLEdBQXBCLEVBQXlCO0FBQ3hCLE9BQUk0RSxVQUFVLEtBQUtJLFFBQW5CO0FBQUEsT0FDQ2hPLENBREQ7QUFFQSxPQUFJNE4sT0FBSixFQUFhO0FBQ1o1TixRQUFJNE4sUUFBUTVVLE1BQVo7QUFDQSxXQUFPLEVBQUVnSCxDQUFGLEdBQU0sQ0FBQyxDQUFkLEVBQWlCO0FBQ2hCLFVBQUtrTyxTQUFMLENBQWVsTyxDQUFmLElBQW9CbU8sVUFBVVAsUUFBUTVOLENBQVIsQ0FBVixFQUFzQixJQUF0QixFQUE0QixJQUE1QixDQUFwQjtBQUNBO0FBQ0QsSUFMRCxNQUtPO0FBQ04sU0FBS2tPLFNBQUwsR0FBaUJDLFVBQVUsS0FBS3JTLE1BQWYsRUFBdUIsSUFBdkIsRUFBNkIsSUFBN0IsQ0FBakI7QUFDQTtBQUNEO0FBQ0Q2TCxZQUFVaEosU0FBVixDQUFvQmdNLFFBQXBCLENBQTZCM00sSUFBN0IsQ0FBa0MsSUFBbEMsRUFBd0MrTSxPQUF4QyxFQUFpREMsY0FBakQ7QUFDQSxNQUFJLEtBQUs2RCx1QkFBVCxFQUFrQyxJQUFJLEtBQUtILFFBQVQsRUFBbUI7QUFDcEQsVUFBT2pVLFVBQVV1YixjQUFWLENBQTBCakwsVUFBVSxXQUFWLEdBQXdCLFlBQWxELEVBQWlFLElBQWpFLENBQVA7QUFDQTtBQUNELFNBQU8sS0FBUDtBQUNBLEVBckJEOztBQXdCRjs7QUFFRXRRLFdBQVVqQyxFQUFWLEdBQWUsVUFBU3NELE1BQVQsRUFBaUI4TCxRQUFqQixFQUEyQjdLLElBQTNCLEVBQWlDO0FBQy9DLFNBQU8sSUFBSXRDLFNBQUosQ0FBY3FCLE1BQWQsRUFBc0I4TCxRQUF0QixFQUFnQzdLLElBQWhDLENBQVA7QUFDQSxFQUZEOztBQUlBdEMsV0FBVW9QLElBQVYsR0FBaUIsVUFBUy9OLE1BQVQsRUFBaUI4TCxRQUFqQixFQUEyQjdLLElBQTNCLEVBQWlDO0FBQ2pEQSxPQUFLa1YsWUFBTCxHQUFvQixJQUFwQjtBQUNBbFYsT0FBS3FMLGVBQUwsR0FBd0JyTCxLQUFLcUwsZUFBTCxJQUF3QixLQUFoRDtBQUNBLFNBQU8sSUFBSTNOLFNBQUosQ0FBY3FCLE1BQWQsRUFBc0I4TCxRQUF0QixFQUFnQzdLLElBQWhDLENBQVA7QUFDQSxFQUpEOztBQU1BdEMsV0FBVXljLE1BQVYsR0FBbUIsVUFBU3BiLE1BQVQsRUFBaUI4TCxRQUFqQixFQUEyQnVQLFFBQTNCLEVBQXFDQyxNQUFyQyxFQUE2QztBQUMvREEsU0FBT2xGLE9BQVAsR0FBaUJpRixRQUFqQjtBQUNBQyxTQUFPaFAsZUFBUCxHQUEwQmdQLE9BQU9oUCxlQUFQLElBQTBCLEtBQTFCLElBQW1DK08sU0FBUy9PLGVBQVQsSUFBNEIsS0FBekY7QUFDQSxTQUFPLElBQUkzTixTQUFKLENBQWNxQixNQUFkLEVBQXNCOEwsUUFBdEIsRUFBZ0N3UCxNQUFoQyxDQUFQO0FBQ0EsRUFKRDs7QUFNQTNjLFdBQVU0YyxXQUFWLEdBQXdCLFVBQVNwUCxLQUFULEVBQWdCaEUsUUFBaEIsRUFBMEJvSCxNQUExQixFQUFrQ25ILEtBQWxDLEVBQXlDdUUsU0FBekMsRUFBb0Q7QUFDM0UsU0FBTyxJQUFJaE8sU0FBSixDQUFjd0osUUFBZCxFQUF3QixDQUF4QixFQUEyQixFQUFDZ0UsT0FBTUEsS0FBUCxFQUFjcFAsWUFBV29MLFFBQXpCLEVBQW1DOE4sa0JBQWlCMUcsTUFBcEQsRUFBNERHLGVBQWN0SCxLQUExRSxFQUFpRnVPLG1CQUFrQnhPLFFBQW5HLEVBQTZHeU8seUJBQXdCckgsTUFBckksRUFBNklqRCxpQkFBZ0IsS0FBN0osRUFBb0tnTCxNQUFLLEtBQXpLLEVBQWdMM0ssV0FBVUEsU0FBMUwsRUFBcU1pRixXQUFVLENBQS9NLEVBQTNCLENBQVA7QUFDQSxFQUZEOztBQUlBalQsV0FBVW1ELEdBQVYsR0FBZ0IsVUFBUzlCLE1BQVQsRUFBaUJpQixJQUFqQixFQUF1QjtBQUN0QyxTQUFPLElBQUl0QyxTQUFKLENBQWNxQixNQUFkLEVBQXNCLENBQXRCLEVBQXlCaUIsSUFBekIsQ0FBUDtBQUNBLEVBRkQ7O0FBSUF0QyxXQUFVNmMsV0FBVixHQUF3QixVQUFTeGIsTUFBVCxFQUFpQnliLFVBQWpCLEVBQTZCO0FBQ3BELE1BQUl6YixVQUFVLElBQWQsRUFBb0I7QUFBRSxVQUFPLEVBQVA7QUFBWTtBQUNsQ0EsV0FBVSxPQUFPQSxNQUFQLEtBQW1CLFFBQXBCLEdBQWdDQSxNQUFoQyxHQUF5Q3JCLFVBQVVDLFFBQVYsQ0FBbUJvQixNQUFuQixLQUE4QkEsTUFBaEY7QUFDQSxNQUFJa0UsQ0FBSixFQUFPRixDQUFQLEVBQVVrRCxDQUFWLEVBQWFJLENBQWI7QUFDQSxNQUFJLENBQUM3QyxTQUFTekUsTUFBVCxLQUFvQnVTLFlBQVl2UyxNQUFaLENBQXJCLEtBQTZDLE9BQU9BLE9BQU8sQ0FBUCxDQUFQLEtBQXNCLFFBQXZFLEVBQWlGO0FBQ2hGa0UsT0FBSWxFLE9BQU85QyxNQUFYO0FBQ0E4RyxPQUFJLEVBQUo7QUFDQSxVQUFPLEVBQUVFLENBQUYsR0FBTSxDQUFDLENBQWQsRUFBaUI7QUFDaEJGLFFBQUlBLEVBQUV3QyxNQUFGLENBQVM3SCxVQUFVNmMsV0FBVixDQUFzQnhiLE9BQU9rRSxDQUFQLENBQXRCLEVBQWlDdVgsVUFBakMsQ0FBVCxDQUFKO0FBQ0E7QUFDRHZYLE9BQUlGLEVBQUU5RyxNQUFOO0FBQ0E7QUFDQSxVQUFPLEVBQUVnSCxDQUFGLEdBQU0sQ0FBQyxDQUFkLEVBQWlCO0FBQ2hCb0QsUUFBSXRELEVBQUVFLENBQUYsQ0FBSjtBQUNBZ0QsUUFBSWhELENBQUo7QUFDQSxXQUFPLEVBQUVnRCxDQUFGLEdBQU0sQ0FBQyxDQUFkLEVBQWlCO0FBQ2hCLFNBQUlJLE1BQU10RCxFQUFFa0QsQ0FBRixDQUFWLEVBQWdCO0FBQ2ZsRCxRQUFFNkUsTUFBRixDQUFTM0UsQ0FBVCxFQUFZLENBQVo7QUFDQTtBQUNEO0FBQ0Q7QUFDRCxHQWpCRCxNQWlCTyxJQUFJbEUsT0FBT3FZLFVBQVgsRUFBdUI7QUFDN0JyVSxPQUFJcU8sVUFBVXJTLE1BQVYsRUFBa0J3RyxNQUFsQixFQUFKO0FBQ0F0QyxPQUFJRixFQUFFOUcsTUFBTjtBQUNBLFVBQU8sRUFBRWdILENBQUYsR0FBTSxDQUFDLENBQWQsRUFBaUI7QUFDaEIsUUFBSUYsRUFBRUUsQ0FBRixFQUFLZ0osR0FBTCxJQUFhdU8sY0FBYyxDQUFDelgsRUFBRUUsQ0FBRixFQUFLNEssUUFBTCxFQUFoQyxFQUFrRDtBQUNqRDlLLE9BQUU2RSxNQUFGLENBQVMzRSxDQUFULEVBQVksQ0FBWjtBQUNBO0FBQ0Q7QUFDRDtBQUNELFNBQU9GLEtBQUssRUFBWjtBQUNBLEVBL0JEOztBQWlDQXJGLFdBQVUrYyxZQUFWLEdBQXlCL2MsVUFBVWdkLGtCQUFWLEdBQStCLFVBQVMzYixNQUFULEVBQWlCeWIsVUFBakIsRUFBNkJ4YSxJQUE3QixFQUFtQztBQUMxRixNQUFJLFFBQU93YSxVQUFQLHlDQUFPQSxVQUFQLE9BQXVCLFFBQTNCLEVBQXFDO0FBQ3BDeGEsVUFBT3dhLFVBQVAsQ0FEb0MsQ0FDakI7QUFDbkJBLGdCQUFhLEtBQWI7QUFDQTtBQUNELE1BQUl6WCxJQUFJckYsVUFBVTZjLFdBQVYsQ0FBc0J4YixNQUF0QixFQUE4QnliLFVBQTlCLENBQVI7QUFBQSxNQUNDdlgsSUFBSUYsRUFBRTlHLE1BRFA7QUFFQSxTQUFPLEVBQUVnSCxDQUFGLEdBQU0sQ0FBQyxDQUFkLEVBQWlCO0FBQ2hCRixLQUFFRSxDQUFGLEVBQUtsQixLQUFMLENBQVcvQixJQUFYLEVBQWlCakIsTUFBakI7QUFDQTtBQUNELEVBVkQ7O0FBY0Y7Ozs7O0FBS0UsS0FBSTRiLGNBQWM5VixPQUFPLHFCQUFQLEVBQThCLFVBQVM4UyxLQUFULEVBQWdCdFEsUUFBaEIsRUFBMEI7QUFDdkUsT0FBSzVHLGVBQUwsR0FBdUIsQ0FBQ2tYLFNBQVMsRUFBVixFQUFjM1UsS0FBZCxDQUFvQixHQUFwQixDQUF2QjtBQUNBLE9BQUs0WCxTQUFMLEdBQWlCLEtBQUtuYSxlQUFMLENBQXFCLENBQXJCLENBQWpCO0FBQ0EsT0FBSzRZLFNBQUwsR0FBaUJoUyxZQUFZLENBQTdCO0FBQ0EsT0FBS3RHLE1BQUwsR0FBYzRaLFlBQVkvWSxTQUExQjtBQUNBLEVBTGUsRUFLYixJQUxhLENBQWxCOztBQU9BNUQsS0FBSTJjLFlBQVkvWSxTQUFoQjtBQUNBK1ksYUFBWWpiLE9BQVosR0FBc0IsUUFBdEI7QUFDQWliLGFBQVlsYixHQUFaLEdBQWtCLENBQWxCO0FBQ0F6QixHQUFFMlQsUUFBRixHQUFhLElBQWI7QUFDQTNULEdBQUV1QyxTQUFGLEdBQWN1VCxhQUFkO0FBQ0E5VixHQUFFZ0QsUUFBRixHQUFheVIsU0FBYjs7QUFFQXpVLEdBQUUrRCxLQUFGLEdBQVUsVUFBU0MsTUFBVCxFQUFpQjtBQUMxQixNQUFJZSxJQUFJLEtBQUt0QyxlQUFiO0FBQUEsTUFDQ2lTLEtBQUssS0FBS2YsUUFEWDtBQUFBLE1BRUMxTyxDQUZEO0FBR0EsTUFBSWpCLE9BQU8sS0FBSzRZLFNBQVosS0FBMEIsSUFBOUIsRUFBb0M7QUFDbkMsUUFBS25hLGVBQUwsR0FBdUIsRUFBdkI7QUFDQSxHQUZELE1BRU87QUFDTndDLE9BQUlGLEVBQUU5RyxNQUFOO0FBQ0EsVUFBTyxFQUFFZ0gsQ0FBRixHQUFNLENBQUMsQ0FBZCxFQUFpQjtBQUNoQixRQUFJakIsT0FBT2UsRUFBRUUsQ0FBRixDQUFQLEtBQWdCLElBQXBCLEVBQTBCO0FBQ3pCRixPQUFFNkUsTUFBRixDQUFTM0UsQ0FBVCxFQUFZLENBQVo7QUFDQTtBQUNEO0FBQ0Q7QUFDRCxTQUFPeVAsRUFBUCxFQUFXO0FBQ1YsT0FBSTFRLE9BQU8wUSxHQUFHaE8sQ0FBVixLQUFnQixJQUFwQixFQUEwQjtBQUN6QixRQUFJZ08sR0FBR3BHLEtBQVAsRUFBYztBQUNib0csUUFBR3BHLEtBQUgsQ0FBU2dFLEtBQVQsR0FBaUJvQyxHQUFHcEMsS0FBcEI7QUFDQTtBQUNELFFBQUlvQyxHQUFHcEMsS0FBUCxFQUFjO0FBQ2JvQyxRQUFHcEMsS0FBSCxDQUFTaEUsS0FBVCxHQUFpQm9HLEdBQUdwRyxLQUFwQjtBQUNBb0csUUFBR3BDLEtBQUgsR0FBVyxJQUFYO0FBQ0EsS0FIRCxNQUdPLElBQUksS0FBS3FCLFFBQUwsS0FBa0JlLEVBQXRCLEVBQTBCO0FBQ2hDLFVBQUtmLFFBQUwsR0FBZ0JlLEdBQUdwRyxLQUFuQjtBQUNBO0FBQ0Q7QUFDRG9HLFFBQUtBLEdBQUdwRyxLQUFSO0FBQ0E7QUFDRCxTQUFPLEtBQVA7QUFDQSxFQTdCRDs7QUErQkF0TyxHQUFFNmMsSUFBRixHQUFTN2MsRUFBRThjLFdBQUYsR0FBZ0IsVUFBUzlZLE1BQVQsRUFBaUI7QUFDekMsTUFBSTBRLEtBQUssS0FBS2YsUUFBZDtBQUFBLE1BQ0NnQixHQUREO0FBRUEsU0FBT0QsRUFBUCxFQUFXO0FBQ1ZDLFNBQU0zUSxPQUFPLEtBQUs0WSxTQUFaLEtBQTJCbEksR0FBR2hPLENBQUgsSUFBUSxJQUFSLElBQWdCMUMsT0FBUTBRLEdBQUdoTyxDQUFILENBQUsxQixLQUFMLENBQVcsS0FBSzRYLFNBQUwsR0FBaUIsR0FBNUIsRUFBaUNoVyxJQUFqQyxDQUFzQyxFQUF0QyxDQUFSLENBQWpEO0FBQ0EsT0FBSStOLE9BQU8sT0FBT0EsR0FBUCxLQUFnQixVQUEzQixFQUF1QztBQUFFO0FBQ3hDLFFBQUlELEdBQUdwSSxDQUFILEtBQVMsQ0FBYixFQUFnQjtBQUNmb0ksUUFBR3JNLENBQUgsQ0FBS3FOLFFBQUwsQ0FBY1gsQ0FBZCxHQUFrQkosR0FBbEI7QUFDQSxLQUZELE1BRU87QUFDTkQsUUFBR0ssQ0FBSCxHQUFPSixHQUFQO0FBQ0E7QUFDRDtBQUNERCxRQUFLQSxHQUFHcEcsS0FBUjtBQUNBO0FBQ0QsRUFkRDs7QUFnQkE1TyxXQUFVdWIsY0FBVixHQUEyQixVQUFTamEsSUFBVCxFQUFlWSxLQUFmLEVBQXNCO0FBQ2hELE1BQUk4UyxLQUFLOVMsTUFBTStSLFFBQWY7QUFBQSxNQUNDbUcsT0FERDtBQUFBLE1BQ1VpRCxHQURWO0FBQUEsTUFDZUMsS0FEZjtBQUFBLE1BQ3NCQyxJQUR0QjtBQUFBLE1BQzRCekssSUFENUI7QUFFQSxNQUFJeFIsU0FBUyxpQkFBYixFQUFnQztBQUMvQjtBQUNBLFVBQU8wVCxFQUFQLEVBQVc7QUFDVmxDLFdBQU9rQyxHQUFHcEcsS0FBVjtBQUNBeU8sVUFBTUMsS0FBTjtBQUNBLFdBQU9ELE9BQU9BLElBQUlsVCxFQUFKLEdBQVM2SyxHQUFHN0ssRUFBMUIsRUFBOEI7QUFDN0JrVCxXQUFNQSxJQUFJek8sS0FBVjtBQUNBO0FBQ0QsUUFBS29HLEdBQUdwQyxLQUFILEdBQVd5SyxNQUFNQSxJQUFJekssS0FBVixHQUFrQjJLLElBQWxDLEVBQXlDO0FBQ3hDdkksUUFBR3BDLEtBQUgsQ0FBU2hFLEtBQVQsR0FBaUJvRyxFQUFqQjtBQUNBLEtBRkQsTUFFTztBQUNOc0ksYUFBUXRJLEVBQVI7QUFDQTtBQUNELFFBQUtBLEdBQUdwRyxLQUFILEdBQVd5TyxHQUFoQixFQUFzQjtBQUNyQkEsU0FBSXpLLEtBQUosR0FBWW9DLEVBQVo7QUFDQSxLQUZELE1BRU87QUFDTnVJLFlBQU92SSxFQUFQO0FBQ0E7QUFDREEsU0FBS2xDLElBQUw7QUFDQTtBQUNEa0MsUUFBSzlTLE1BQU0rUixRQUFOLEdBQWlCcUosS0FBdEI7QUFDQTtBQUNELFNBQU90SSxFQUFQLEVBQVc7QUFDVixPQUFJQSxHQUFHNEIsRUFBUCxFQUFXLElBQUksT0FBTzVCLEdBQUdyTSxDQUFILENBQUtySCxJQUFMLENBQVAsS0FBdUIsVUFBM0IsRUFBdUMsSUFBSTBULEdBQUdyTSxDQUFILENBQUtySCxJQUFMLEdBQUosRUFBa0I7QUFDbkU4WSxjQUFVLElBQVY7QUFDQTtBQUNEcEYsUUFBS0EsR0FBR3BHLEtBQVI7QUFDQTtBQUNELFNBQU93TCxPQUFQO0FBQ0EsRUFoQ0Q7O0FBa0NBNkMsYUFBWU8sUUFBWixHQUF1QixVQUFTQyxPQUFULEVBQWtCO0FBQ3hDLE1BQUlsWSxJQUFJa1ksUUFBUWxmLE1BQWhCO0FBQ0EsU0FBTyxFQUFFZ0gsQ0FBRixHQUFNLENBQUMsQ0FBZCxFQUFpQjtBQUNoQixPQUFJa1ksUUFBUWxZLENBQVIsRUFBV3hELEdBQVgsS0FBbUJrYixZQUFZbGIsR0FBbkMsRUFBd0M7QUFDdkNpUyxhQUFVLElBQUl5SixRQUFRbFksQ0FBUixDQUFKLEVBQUQsQ0FBbUIyWCxTQUE1QixJQUF5Q08sUUFBUWxZLENBQVIsQ0FBekM7QUFDQTtBQUNEO0FBQ0QsU0FBTyxJQUFQO0FBQ0EsRUFSRDs7QUFVQTtBQUNBM0QsV0FBVUMsTUFBVixHQUFtQixVQUFTc1osTUFBVCxFQUFpQjtBQUNuQyxNQUFJLENBQUNBLE1BQUQsSUFBVyxDQUFDQSxPQUFPclosUUFBbkIsSUFBK0IsQ0FBQ3FaLE9BQU9sWixJQUF2QyxJQUErQyxDQUFDa1osT0FBT3BaLEdBQTNELEVBQWdFO0FBQUUsU0FBTSw0QkFBTjtBQUFxQztBQUN2RyxNQUFJRCxXQUFXcVosT0FBT3JaLFFBQXRCO0FBQUEsTUFDQzZILFdBQVd3UixPQUFPeFIsUUFBUCxJQUFtQixDQUQvQjtBQUFBLE1BRUMrVCxpQkFBaUJ2QyxPQUFPdUMsY0FGekI7QUFBQSxNQUdDM1YsTUFBTSxFQUFDOUYsTUFBSyxjQUFOLEVBQXNCa0IsS0FBSSxVQUExQixFQUFzQ1MsTUFBSyxPQUEzQyxFQUFvRHNTLE9BQU0sTUFBMUQsRUFBa0VLLEtBQUksTUFBdEUsRUFBOEVvSCxTQUFRLGlCQUF0RixFQUhQO0FBQUEsTUFJQ0MsU0FBU3pXLE9BQU8sYUFBYXJGLFNBQVNMLE1BQVQsQ0FBZ0IsQ0FBaEIsRUFBbUJqQixXQUFuQixFQUFiLEdBQWdEc0IsU0FBU21QLE1BQVQsQ0FBZ0IsQ0FBaEIsQ0FBaEQsR0FBcUUsUUFBNUUsRUFDUixZQUFXO0FBQ1ZnTSxlQUFZMVosSUFBWixDQUFpQixJQUFqQixFQUF1QnpCLFFBQXZCLEVBQWlDNkgsUUFBakM7QUFDQSxRQUFLNUcsZUFBTCxHQUF1QjJhLGtCQUFrQixFQUF6QztBQUNBLEdBSk8sRUFJSnZDLE9BQU9yYyxNQUFQLEtBQWtCLElBSmQsQ0FKVjtBQUFBLE1BU0N3QixJQUFJc2QsT0FBTzFaLFNBQVAsR0FBbUIsSUFBSStZLFdBQUosQ0FBZ0JuYixRQUFoQixDQVR4QjtBQUFBLE1BVUN1VSxJQVZEO0FBV0EvVixJQUFFMk0sV0FBRixHQUFnQjJRLE1BQWhCO0FBQ0FBLFNBQU83YixHQUFQLEdBQWFvWixPQUFPcFosR0FBcEI7QUFDQSxPQUFLc1UsSUFBTCxJQUFhdE8sR0FBYixFQUFrQjtBQUNqQixPQUFJLE9BQU9vVCxPQUFPOUUsSUFBUCxDQUFQLEtBQXlCLFVBQTdCLEVBQXlDO0FBQ3hDL1YsTUFBRXlILElBQUlzTyxJQUFKLENBQUYsSUFBZThFLE9BQU85RSxJQUFQLENBQWY7QUFDQTtBQUNEO0FBQ0R1SCxTQUFPNWIsT0FBUCxHQUFpQm1aLE9BQU9uWixPQUF4QjtBQUNBaWIsY0FBWU8sUUFBWixDQUFxQixDQUFDSSxNQUFELENBQXJCO0FBQ0EsU0FBT0EsTUFBUDtBQUNBLEVBdkJEOztBQTBCQTtBQUNBdlksS0FBSS9ILE9BQU95QixRQUFYO0FBQ0EsS0FBSXNHLENBQUosRUFBTztBQUNOLE9BQUtFLElBQUksQ0FBVCxFQUFZQSxJQUFJRixFQUFFOUcsTUFBbEIsRUFBMEJnSCxHQUExQixFQUErQjtBQUM5QkYsS0FBRUUsQ0FBRjtBQUNBO0FBQ0QsT0FBS2pGLENBQUwsSUFBVWdHLFVBQVYsRUFBc0I7QUFDckIsT0FBSSxDQUFDQSxXQUFXaEcsQ0FBWCxFQUFjbUcsSUFBbkIsRUFBeUI7QUFDeEJuSixXQUFPdWdCLE9BQVAsQ0FBZUMsR0FBZixDQUFtQiwwQ0FBMEN4ZCxDQUE3RDtBQUNBO0FBQ0Q7QUFDRDs7QUFFRCtGLGlCQUFnQixLQUFoQixDQXg0RDRCLENBdzRETDtBQUV4QixDQTE0REQsRUEwNERJLE9BQU96SCxNQUFQLEtBQW1CLFdBQW5CLElBQWtDQSxPQUFPQyxPQUF6QyxJQUFvRCxPQUFPQyxNQUFQLEtBQW1CLFdBQXhFLEdBQXVGQSxNQUF2RixHQUFnRyxhQUFReEIsTUExNEQzRyxFQTA0RG1ILFdBMTREbkgsRSIsImZpbGUiOiJhc3luYy1tb2R1bGVzLzAuanM/dj0yNTU1YjM0MGQyZTZhMjRiMWYxOCIsInNvdXJjZXNDb250ZW50IjpbIid1c2Ugc3RyaWN0JztcclxuXHJcbi8qKlxyXG4gKiDQodC60YDQvtC70Lsgd2luZG93XHJcbiAqIEBzZWUge0BsaW5rIGh0dHBzOi8vZ3JlZW5zb2NrLmNvbS9kb2NzL1BsdWdpbnMvU2Nyb2xsVG9QbHVnaW59XHJcbiAqIEBtb2R1bGVcclxuICovXHJcblxyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbi8vIEltcG9ydHNcclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuaW1wb3J0IEdTQVAgZnJvbSAnIy9fdmVuZG9ycy9nc2FwL2dzYXAnO1xyXG5pbXBvcnQgVHdlZW5MaXRlIGZyb20gJyMvX3ZlbmRvcnMvZ3NhcC9Ud2VlbkxpdGUnO1xyXG5pbXBvcnQgJyMvX3ZlbmRvcnMvZ3NhcC9wbHVnaW5zL1Njcm9sbFRvUGx1Z2luJztcclxuXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuLy8gUHJpdmF0ZVxyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG4vKipcclxuICog0JTQu9C40YLQtdC70YzQvdC+0YHRgtGMINGB0LrRgNC+0LvQu9CwLCDQsiDRgdC10LrRg9C90LTQsNGFXHJcbiAqIEB0eXBlIHtudW1iZXJ9XHJcbiAqIEBwcml2YXRlXHJcbiAqL1xyXG5jb25zdCBkZWZhdWx0RHVyYXRpb24gPSAxO1xyXG5cclxuLyoqXHJcbiAqIEB0eXBlIHtKUXVlcnl9XHJcbiAqIEBwcml2YXRlXHJcbiAqL1xyXG5jb25zdCAkd2luZG93ID0gJCh3aW5kb3cpO1xyXG5cclxuLyoqXHJcbiAqIEB0eXBlIHtudW1iZXJ9XHJcbiAqIEBwcml2YXRlXHJcbiAqL1xyXG5jb25zdCBzaG93SGVpZ2h0ID0gNTAwO1xyXG5cclxuLyoqXHJcbiAqIEB0eXBlIHtzdHJpbmd9XHJcbiAqIEBwcml2YXRlXHJcbiAqL1xyXG5jb25zdCBzaG93Q2xhc3MgPSAnc2Nyb2xsLXVwLS1zaG93JztcclxuXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuLy8gUHVibGljXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbmZ1bmN0aW9uIHNjcm9sbFdpbmRvdyAoJGVsZW1lbnRzKSB7XHJcblx0JGVsZW1lbnRzLm9uKCdjbGljaycsIGZ1bmN0aW9uICgpIHtcclxuXHRcdGxldCAkdGhpcyA9ICQodGhpcyk7XHJcblx0XHRsZXQgc2Nyb2xsID0gJHRoaXMuZGF0YSgnc2Nyb2xsLXdpbmRvdycpIHx8IDA7XHJcblxyXG5cdFx0aWYgKHNjcm9sbCA9PT0gJ3VwJykge1xyXG5cdFx0XHRzY3JvbGwgPSAwO1xyXG5cdFx0fSBlbHNlIGlmIChzY3JvbGwgPT09ICdkb3duJykge1xyXG5cdFx0XHRzY3JvbGwgPSAnbWF4JztcclxuXHRcdH1cclxuXHJcblx0XHRUd2VlbkxpdGUudG8od2luZG93LCBkZWZhdWx0RHVyYXRpb24sIHtcclxuXHRcdFx0c2Nyb2xsVG86IHNjcm9sbCxcclxuXHRcdFx0ZWFzZTogR1NBUC5Qb3dlcjIuZWFzZU91dCxcclxuXHRcdFx0b25Db21wbGV0ZSAoKSB7XHJcblx0XHRcdFx0Ly8gY29uc29sZS5sb2coJ2NvbXBsZXRlJyk7XHJcblx0XHRcdH1cclxuXHRcdH0pO1xyXG5cdH0pO1xyXG5cclxuXHRjb25zdCAkc2Nyb2xsVXAgPSAkZWxlbWVudHMuZmlsdGVyKCdbZGF0YS1zY3JvbGwtd2luZG93PVwidXBcIl0nKTtcclxuXHRpZiAoJHNjcm9sbFVwLmxlbmd0aCkge1xyXG5cdFx0Y29uc3Qgb25TY3JvbGwgPSAoKSA9PiB7XHJcblx0XHRcdGxldCBzY3JvbGwgPSAkd2luZG93LnNjcm9sbFRvcCgpO1xyXG5cdFx0XHRsZXQgZG9DbGFzcyA9IHNjcm9sbCA+IHNob3dIZWlnaHQgPyAnYWRkQ2xhc3MnIDogJ3JlbW92ZUNsYXNzJztcclxuXHRcdFx0JHNjcm9sbFVwW2RvQ2xhc3NdKHNob3dDbGFzcyk7XHJcblx0XHR9O1xyXG5cclxuXHRcdCR3aW5kb3cub24oJ3Njcm9sbCcsIG9uU2Nyb2xsKTtcclxuXHRcdG9uU2Nyb2xsKCk7XHJcblx0fVxyXG59XHJcblxyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbi8vIEV4cG9ydHNcclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgc2Nyb2xsV2luZG93O1xyXG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9fSFRNTC9zcmMvanMvX21vZHVsZXMvc2Nyb2xsLXdpbmRvdy5qcyIsIi8qIVxyXG4gKiBWRVJTSU9OOiAxLjkuMFxyXG4gKiBEQVRFOiAyMDE3LTA2LTE5XHJcbiAqIFVQREFURVMgQU5EIERPQ1MgQVQ6IGh0dHA6Ly9ncmVlbnNvY2suY29tXHJcbiAqXHJcbiAqIEBsaWNlbnNlIENvcHlyaWdodCAoYykgMjAwOC0yMDE3LCBHcmVlblNvY2suIEFsbCByaWdodHMgcmVzZXJ2ZWQuXHJcbiAqIFRoaXMgd29yayBpcyBzdWJqZWN0IHRvIHRoZSB0ZXJtcyBhdCBodHRwOi8vZ3JlZW5zb2NrLmNvbS9zdGFuZGFyZC1saWNlbnNlIG9yIGZvclxyXG4gKiBDbHViIEdyZWVuU29jayBtZW1iZXJzLCB0aGUgc29mdHdhcmUgYWdyZWVtZW50IHRoYXQgd2FzIGlzc3VlZCB3aXRoIHlvdXIgbWVtYmVyc2hpcC5cclxuICpcclxuICogQGF1dGhvcjogSmFjayBEb3lsZSwgamFja0BncmVlbnNvY2suY29tXHJcbiAqKi9cclxudmFyIF9nc1Njb3BlID0gKHR5cGVvZihtb2R1bGUpICE9PSBcInVuZGVmaW5lZFwiICYmIG1vZHVsZS5leHBvcnRzICYmIHR5cGVvZihnbG9iYWwpICE9PSBcInVuZGVmaW5lZFwiKSA/IGdsb2JhbCA6IHRoaXMgfHwgd2luZG93OyAvL2hlbHBzIGVuc3VyZSBjb21wYXRpYmlsaXR5IHdpdGggQU1EL1JlcXVpcmVKUyBhbmQgQ29tbW9uSlMvTm9kZVxyXG4oX2dzU2NvcGUuX2dzUXVldWUgfHwgKF9nc1Njb3BlLl9nc1F1ZXVlID0gW10pKS5wdXNoKCBmdW5jdGlvbigpIHtcclxuXHJcblx0XCJ1c2Ugc3RyaWN0XCI7XHJcblxyXG5cdHZhciBfZG9jID0gKF9nc1Njb3BlLmRvY3VtZW50IHx8IHt9KS5kb2N1bWVudEVsZW1lbnQsXHJcblx0XHRfd2luZG93ID0gX2dzU2NvcGUsXHJcblx0XHRfbWF4ID0gZnVuY3Rpb24oZWxlbWVudCwgYXhpcykge1xyXG5cdFx0XHR2YXIgZGltID0gKGF4aXMgPT09IFwieFwiKSA/IFwiV2lkdGhcIiA6IFwiSGVpZ2h0XCIsXHJcblx0XHRcdFx0c2Nyb2xsID0gXCJzY3JvbGxcIiArIGRpbSxcclxuXHRcdFx0XHRjbGllbnQgPSBcImNsaWVudFwiICsgZGltLFxyXG5cdFx0XHRcdGJvZHkgPSBkb2N1bWVudC5ib2R5O1xyXG5cdFx0XHRyZXR1cm4gKGVsZW1lbnQgPT09IF93aW5kb3cgfHwgZWxlbWVudCA9PT0gX2RvYyB8fCBlbGVtZW50ID09PSBib2R5KSA/IE1hdGgubWF4KF9kb2Nbc2Nyb2xsXSwgYm9keVtzY3JvbGxdKSAtIChfd2luZG93W1wiaW5uZXJcIiArIGRpbV0gfHwgX2RvY1tjbGllbnRdIHx8IGJvZHlbY2xpZW50XSkgOiBlbGVtZW50W3Njcm9sbF0gLSBlbGVtZW50W1wib2Zmc2V0XCIgKyBkaW1dO1xyXG5cdFx0fSxcclxuXHRcdF91bndyYXBFbGVtZW50ID0gZnVuY3Rpb24odmFsdWUpIHtcclxuXHRcdFx0aWYgKHR5cGVvZih2YWx1ZSkgPT09IFwic3RyaW5nXCIpIHtcclxuXHRcdFx0XHR2YWx1ZSA9IF93aW5kb3cuR3JlZW5Tb2NrR2xvYmFscy5Ud2VlbkxpdGUuc2VsZWN0b3IodmFsdWUpO1xyXG5cdFx0XHR9XHJcblx0XHRcdGlmICh2YWx1ZS5sZW5ndGggJiYgdmFsdWUgIT09IF93aW5kb3cgJiYgdmFsdWVbMF0gJiYgdmFsdWVbMF0uc3R5bGUgJiYgIXZhbHVlLm5vZGVUeXBlKSB7XHJcblx0XHRcdFx0dmFsdWUgPSB2YWx1ZVswXTtcclxuXHRcdFx0fVxyXG5cdFx0XHRyZXR1cm4gKHZhbHVlID09PSBfd2luZG93IHx8ICh2YWx1ZS5ub2RlVHlwZSAmJiB2YWx1ZS5zdHlsZSkpID8gdmFsdWUgOiBudWxsO1xyXG5cdFx0fSxcclxuXHRcdF9idWlsZEdldHRlciA9IGZ1bmN0aW9uKGUsIGF4aXMpIHsgLy9wYXNzIGluIGFuIGVsZW1lbnQgYW5kIGFuIGF4aXMgKFwieFwiIG9yIFwieVwiKSBhbmQgaXQnbGwgcmV0dXJuIGEgZ2V0dGVyIGZ1bmN0aW9uIGZvciB0aGUgc2Nyb2xsIHBvc2l0aW9uIG9mIHRoYXQgZWxlbWVudCAobGlrZSBzY3JvbGxUb3Agb3Igc2Nyb2xsTGVmdCwgYWx0aG91Z2ggaWYgdGhlIGVsZW1lbnQgaXMgdGhlIHdpbmRvdywgaXQnbGwgdXNlIHRoZSBwYWdlWE9mZnNldC9wYWdlWU9mZnNldCBvciB0aGUgZG9jdW1lbnRFbGVtZW50J3Mgc2Nyb2xsVG9wL3Njcm9sbExlZnQgb3IgZG9jdW1lbnQuYm9keSdzLiBCYXNpY2FsbHkgdGhpcyBzdHJlYW1saW5lcyB0aGluZ3MgYW5kIG1ha2VzIGEgdmVyeSBmYXN0IGdldHRlciBhY3Jvc3MgYnJvd3NlcnMuXHJcblx0XHRcdHZhciBwID0gXCJzY3JvbGxcIiArICgoYXhpcyA9PT0gXCJ4XCIpID8gXCJMZWZ0XCIgOiBcIlRvcFwiKTtcclxuXHRcdFx0aWYgKGUgPT09IF93aW5kb3cpIHtcclxuXHRcdFx0XHRpZiAoZS5wYWdlWE9mZnNldCAhPSBudWxsKSB7XHJcblx0XHRcdFx0XHRwID0gXCJwYWdlXCIgKyBheGlzLnRvVXBwZXJDYXNlKCkgKyBcIk9mZnNldFwiO1xyXG5cdFx0XHRcdH0gZWxzZSBpZiAoX2RvY1twXSAhPSBudWxsKSB7XHJcblx0XHRcdFx0XHRlID0gX2RvYztcclxuXHRcdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdFx0ZSA9IGRvY3VtZW50LmJvZHk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9XHJcblx0XHRcdHJldHVybiBmdW5jdGlvbigpIHtcclxuXHRcdFx0XHRyZXR1cm4gZVtwXTtcclxuXHRcdFx0fTtcclxuXHRcdH0sXHJcblx0XHRfZ2V0T2Zmc2V0ID0gZnVuY3Rpb24oZWxlbWVudCwgY29udGFpbmVyKSB7XHJcblx0XHRcdHZhciByZWN0ID0gX3Vud3JhcEVsZW1lbnQoZWxlbWVudCkuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCksXHJcblx0XHRcdFx0aXNSb290ID0gKCFjb250YWluZXIgfHwgY29udGFpbmVyID09PSBfd2luZG93IHx8IGNvbnRhaW5lciA9PT0gZG9jdW1lbnQuYm9keSksXHJcblx0XHRcdFx0Y1JlY3QgPSAoaXNSb290ID8gX2RvYyA6IGNvbnRhaW5lcikuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCksXHJcblx0XHRcdFx0b2Zmc2V0cyA9IHt4OiByZWN0LmxlZnQgLSBjUmVjdC5sZWZ0LCB5OiByZWN0LnRvcCAtIGNSZWN0LnRvcH07XHJcblx0XHRcdGlmICghaXNSb290ICYmIGNvbnRhaW5lcikgeyAvL29ubHkgYWRkIHRoZSBjdXJyZW50IHNjcm9sbCBwb3NpdGlvbiBpZiBpdCdzIG5vdCB0aGUgd2luZG93L2JvZHkuXHJcblx0XHRcdFx0b2Zmc2V0cy54ICs9IF9idWlsZEdldHRlcihjb250YWluZXIsIFwieFwiKSgpO1xyXG5cdFx0XHRcdG9mZnNldHMueSArPSBfYnVpbGRHZXR0ZXIoY29udGFpbmVyLCBcInlcIikoKTtcclxuXHRcdFx0fVxyXG5cdFx0XHRyZXR1cm4gb2Zmc2V0cztcclxuXHRcdH0sXHJcblx0XHRfcGFyc2VWYWwgPSBmdW5jdGlvbih2YWx1ZSwgdGFyZ2V0LCBheGlzKSB7XHJcblx0XHRcdHZhciB0eXBlID0gdHlwZW9mKHZhbHVlKTtcclxuXHRcdFx0cmV0dXJuICFpc05hTih2YWx1ZSkgPyBwYXJzZUZsb2F0KHZhbHVlKSA6ICh0eXBlID09PSBcIm51bWJlclwiIHx8ICh0eXBlID09PSBcInN0cmluZ1wiICYmIHZhbHVlLmNoYXJBdCgxKSA9PT0gXCI9XCIpKSA/IHZhbHVlIDogKHZhbHVlID09PSBcIm1heFwiKSA/IF9tYXgodGFyZ2V0LCBheGlzKSA6IE1hdGgubWluKF9tYXgodGFyZ2V0LCBheGlzKSwgX2dldE9mZnNldCh2YWx1ZSwgdGFyZ2V0KVtheGlzXSk7XHJcblx0XHR9LFxyXG5cclxuXHRcdFNjcm9sbFRvUGx1Z2luID0gX2dzU2NvcGUuX2dzRGVmaW5lLnBsdWdpbih7XHJcblx0XHRcdHByb3BOYW1lOiBcInNjcm9sbFRvXCIsXHJcblx0XHRcdEFQSTogMixcclxuXHRcdFx0Z2xvYmFsOiB0cnVlLFxyXG5cdFx0XHR2ZXJzaW9uOlwiMS45LjBcIixcclxuXHJcblx0XHRcdC8vY2FsbGVkIHdoZW4gdGhlIHR3ZWVuIHJlbmRlcnMgZm9yIHRoZSBmaXJzdCB0aW1lLiBUaGlzIGlzIHdoZXJlIGluaXRpYWwgdmFsdWVzIHNob3VsZCBiZSByZWNvcmRlZCBhbmQgYW55IHNldHVwIHJvdXRpbmVzIHNob3VsZCBydW4uXHJcblx0XHRcdGluaXQ6IGZ1bmN0aW9uKHRhcmdldCwgdmFsdWUsIHR3ZWVuKSB7XHJcblx0XHRcdFx0dGhpcy5fd2R3ID0gKHRhcmdldCA9PT0gX3dpbmRvdyk7XHJcblx0XHRcdFx0dGhpcy5fdGFyZ2V0ID0gdGFyZ2V0O1xyXG5cdFx0XHRcdHRoaXMuX3R3ZWVuID0gdHdlZW47XHJcblx0XHRcdFx0aWYgKHR5cGVvZih2YWx1ZSkgIT09IFwib2JqZWN0XCIpIHtcclxuXHRcdFx0XHRcdHZhbHVlID0ge3k6dmFsdWV9OyAvL2lmIHdlIGRvbid0IHJlY2VpdmUgYW4gb2JqZWN0IGFzIHRoZSBwYXJhbWV0ZXIsIGFzc3VtZSB0aGUgdXNlciBpbnRlbmRzIFwieVwiLlxyXG5cdFx0XHRcdFx0aWYgKHR5cGVvZih2YWx1ZS55KSA9PT0gXCJzdHJpbmdcIiAmJiB2YWx1ZS55ICE9PSBcIm1heFwiICYmIHZhbHVlLnkuY2hhckF0KDEpICE9PSBcIj1cIikge1xyXG5cdFx0XHRcdFx0XHR2YWx1ZS54ID0gdmFsdWUueTtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHR9IGVsc2UgaWYgKHZhbHVlLm5vZGVUeXBlKSB7XHJcblx0XHRcdFx0XHR2YWx1ZSA9IHt5OnZhbHVlLCB4OnZhbHVlfTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0dGhpcy52YXJzID0gdmFsdWU7XHJcblx0XHRcdFx0dGhpcy5fYXV0b0tpbGwgPSAodmFsdWUuYXV0b0tpbGwgIT09IGZhbHNlKTtcclxuXHRcdFx0XHR0aGlzLmdldFggPSBfYnVpbGRHZXR0ZXIodGFyZ2V0LCBcInhcIik7XHJcblx0XHRcdFx0dGhpcy5nZXRZID0gX2J1aWxkR2V0dGVyKHRhcmdldCwgXCJ5XCIpO1xyXG5cdFx0XHRcdHRoaXMueCA9IHRoaXMueFByZXYgPSB0aGlzLmdldFgoKTtcclxuXHRcdFx0XHR0aGlzLnkgPSB0aGlzLnlQcmV2ID0gdGhpcy5nZXRZKCk7XHJcblx0XHRcdFx0aWYgKHZhbHVlLnggIT0gbnVsbCkge1xyXG5cdFx0XHRcdFx0dGhpcy5fYWRkVHdlZW4odGhpcywgXCJ4XCIsIHRoaXMueCwgX3BhcnNlVmFsKHZhbHVlLngsIHRhcmdldCwgXCJ4XCIpIC0gKHZhbHVlLm9mZnNldFggfHwgMCksIFwic2Nyb2xsVG9feFwiLCB0cnVlKTtcclxuXHRcdFx0XHRcdHRoaXMuX292ZXJ3cml0ZVByb3BzLnB1c2goXCJzY3JvbGxUb194XCIpO1xyXG5cdFx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0XHR0aGlzLnNraXBYID0gdHJ1ZTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0aWYgKHZhbHVlLnkgIT0gbnVsbCkge1xyXG5cdFx0XHRcdFx0dGhpcy5fYWRkVHdlZW4odGhpcywgXCJ5XCIsIHRoaXMueSwgX3BhcnNlVmFsKHZhbHVlLnksIHRhcmdldCwgXCJ5XCIpIC0gKHZhbHVlLm9mZnNldFkgfHwgMCksIFwic2Nyb2xsVG9feVwiLCB0cnVlKTtcclxuXHRcdFx0XHRcdHRoaXMuX292ZXJ3cml0ZVByb3BzLnB1c2goXCJzY3JvbGxUb195XCIpO1xyXG5cdFx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0XHR0aGlzLnNraXBZID0gdHJ1ZTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0cmV0dXJuIHRydWU7XHJcblx0XHRcdH0sXHJcblxyXG5cdFx0XHQvL2NhbGxlZCBlYWNoIHRpbWUgdGhlIHZhbHVlcyBzaG91bGQgYmUgdXBkYXRlZCwgYW5kIHRoZSByYXRpbyBnZXRzIHBhc3NlZCBhcyB0aGUgb25seSBwYXJhbWV0ZXIgKHR5cGljYWxseSBpdCdzIGEgdmFsdWUgYmV0d2VlbiAwIGFuZCAxLCBidXQgaXQgY2FuIGV4Y2VlZCB0aG9zZSB3aGVuIHVzaW5nIGFuIGVhc2UgbGlrZSBFbGFzdGljLmVhc2VPdXQgb3IgQmFjay5lYXNlT3V0LCBldGMuKVxyXG5cdFx0XHRzZXQ6IGZ1bmN0aW9uKHYpIHtcclxuXHRcdFx0XHR0aGlzLl9zdXBlci5zZXRSYXRpby5jYWxsKHRoaXMsIHYpO1xyXG5cclxuXHRcdFx0XHR2YXIgeCA9ICh0aGlzLl93ZHcgfHwgIXRoaXMuc2tpcFgpID8gdGhpcy5nZXRYKCkgOiB0aGlzLnhQcmV2LFxyXG5cdFx0XHRcdFx0eSA9ICh0aGlzLl93ZHcgfHwgIXRoaXMuc2tpcFkpID8gdGhpcy5nZXRZKCkgOiB0aGlzLnlQcmV2LFxyXG5cdFx0XHRcdFx0eURpZiA9IHkgLSB0aGlzLnlQcmV2LFxyXG5cdFx0XHRcdFx0eERpZiA9IHggLSB0aGlzLnhQcmV2LFxyXG5cdFx0XHRcdFx0dGhyZXNob2xkID0gU2Nyb2xsVG9QbHVnaW4uYXV0b0tpbGxUaHJlc2hvbGQ7XHJcblxyXG5cdFx0XHRcdGlmICh0aGlzLnggPCAwKSB7IC8vY2FuJ3Qgc2Nyb2xsIHRvIGEgcG9zaXRpb24gbGVzcyB0aGFuIDAhIE1pZ2h0IGhhcHBlbiBpZiBzb21lb25lIHVzZXMgYSBCYWNrLmVhc2VPdXQgb3IgRWxhc3RpYy5lYXNlT3V0IHdoZW4gc2Nyb2xsaW5nIGJhY2sgdG8gdGhlIHRvcCBvZiB0aGUgcGFnZSAoZm9yIGV4YW1wbGUpXHJcblx0XHRcdFx0XHR0aGlzLnggPSAwO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0XHRpZiAodGhpcy55IDwgMCkge1xyXG5cdFx0XHRcdFx0dGhpcy55ID0gMDtcclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0aWYgKHRoaXMuX2F1dG9LaWxsKSB7XHJcblx0XHRcdFx0XHQvL25vdGU6IGlPUyBoYXMgYSBidWcgdGhhdCB0aHJvd3Mgb2ZmIHRoZSBzY3JvbGwgYnkgc2V2ZXJhbCBwaXhlbHMsIHNvIHdlIG5lZWQgdG8gY2hlY2sgaWYgaXQncyB3aXRoaW4gNyBwaXhlbHMgb2YgdGhlIHByZXZpb3VzIG9uZSB0aGF0IHdlIHNldCBpbnN0ZWFkIG9mIGp1c3QgbG9va2luZyBmb3IgYW4gZXhhY3QgbWF0Y2guXHJcblx0XHRcdFx0XHRpZiAoIXRoaXMuc2tpcFggJiYgKHhEaWYgPiB0aHJlc2hvbGQgfHwgeERpZiA8IC10aHJlc2hvbGQpICYmIHggPCBfbWF4KHRoaXMuX3RhcmdldCwgXCJ4XCIpKSB7XHJcblx0XHRcdFx0XHRcdHRoaXMuc2tpcFggPSB0cnVlOyAvL2lmIHRoZSB1c2VyIHNjcm9sbHMgc2VwYXJhdGVseSwgd2Ugc2hvdWxkIHN0b3AgdHdlZW5pbmchXHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRpZiAoIXRoaXMuc2tpcFkgJiYgKHlEaWYgPiB0aHJlc2hvbGQgfHwgeURpZiA8IC10aHJlc2hvbGQpICYmIHkgPCBfbWF4KHRoaXMuX3RhcmdldCwgXCJ5XCIpKSB7XHJcblx0XHRcdFx0XHRcdHRoaXMuc2tpcFkgPSB0cnVlOyAvL2lmIHRoZSB1c2VyIHNjcm9sbHMgc2VwYXJhdGVseSwgd2Ugc2hvdWxkIHN0b3AgdHdlZW5pbmchXHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRpZiAodGhpcy5za2lwWCAmJiB0aGlzLnNraXBZKSB7XHJcblx0XHRcdFx0XHRcdHRoaXMuX3R3ZWVuLmtpbGwoKTtcclxuXHRcdFx0XHRcdFx0aWYgKHRoaXMudmFycy5vbkF1dG9LaWxsKSB7XHJcblx0XHRcdFx0XHRcdFx0dGhpcy52YXJzLm9uQXV0b0tpbGwuYXBwbHkodGhpcy52YXJzLm9uQXV0b0tpbGxTY29wZSB8fCB0aGlzLl90d2VlbiwgdGhpcy52YXJzLm9uQXV0b0tpbGxQYXJhbXMgfHwgW10pO1xyXG5cdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0fVxyXG5cdFx0XHRcdGlmICh0aGlzLl93ZHcpIHtcclxuXHRcdFx0XHRcdF93aW5kb3cuc2Nyb2xsVG8oKCF0aGlzLnNraXBYKSA/IHRoaXMueCA6IHgsICghdGhpcy5za2lwWSkgPyB0aGlzLnkgOiB5KTtcclxuXHRcdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdFx0aWYgKCF0aGlzLnNraXBZKSB7XHJcblx0XHRcdFx0XHRcdHRoaXMuX3RhcmdldC5zY3JvbGxUb3AgPSB0aGlzLnk7XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRpZiAoIXRoaXMuc2tpcFgpIHtcclxuXHRcdFx0XHRcdFx0dGhpcy5fdGFyZ2V0LnNjcm9sbExlZnQgPSB0aGlzLng7XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0fVxyXG5cdFx0XHRcdHRoaXMueFByZXYgPSB0aGlzLng7XHJcblx0XHRcdFx0dGhpcy55UHJldiA9IHRoaXMueTtcclxuXHRcdFx0fVxyXG5cclxuXHRcdH0pLFxyXG5cdFx0cCA9IFNjcm9sbFRvUGx1Z2luLnByb3RvdHlwZTtcclxuXHJcblx0U2Nyb2xsVG9QbHVnaW4ubWF4ID0gX21heDtcclxuXHRTY3JvbGxUb1BsdWdpbi5nZXRPZmZzZXQgPSBfZ2V0T2Zmc2V0O1xyXG5cdFNjcm9sbFRvUGx1Z2luLmJ1aWxkR2V0dGVyID0gX2J1aWxkR2V0dGVyO1xyXG5cdFNjcm9sbFRvUGx1Z2luLmF1dG9LaWxsVGhyZXNob2xkID0gNztcclxuXHJcblx0cC5fa2lsbCA9IGZ1bmN0aW9uKGxvb2t1cCkge1xyXG5cdFx0aWYgKGxvb2t1cC5zY3JvbGxUb194KSB7XHJcblx0XHRcdHRoaXMuc2tpcFggPSB0cnVlO1xyXG5cdFx0fVxyXG5cdFx0aWYgKGxvb2t1cC5zY3JvbGxUb195KSB7XHJcblx0XHRcdHRoaXMuc2tpcFkgPSB0cnVlO1xyXG5cdFx0fVxyXG5cdFx0cmV0dXJuIHRoaXMuX3N1cGVyLl9raWxsLmNhbGwodGhpcywgbG9va3VwKTtcclxuXHR9O1xyXG5cclxufSk7IGlmIChfZ3NTY29wZS5fZ3NEZWZpbmUpIHsgX2dzU2NvcGUuX2dzUXVldWUucG9wKCkoKTsgfVxyXG5cclxuLy9leHBvcnQgdG8gQU1EL1JlcXVpcmVKUyBhbmQgQ29tbW9uSlMvTm9kZSAocHJlY3Vyc29yIHRvIGZ1bGwgbW9kdWxhciBidWlsZCBzeXN0ZW0gY29taW5nIGF0IGEgbGF0ZXIgZGF0ZSlcclxuKGZ1bmN0aW9uKG5hbWUpIHtcclxuXHRcInVzZSBzdHJpY3RcIjtcclxuXHR2YXIgZ2V0R2xvYmFsID0gZnVuY3Rpb24oKSB7XHJcblx0XHRyZXR1cm4gKF9nc1Njb3BlLkdyZWVuU29ja0dsb2JhbHMgfHwgX2dzU2NvcGUpW25hbWVdO1xyXG5cdH07XHJcblx0aWYgKHR5cGVvZihtb2R1bGUpICE9PSBcInVuZGVmaW5lZFwiICYmIG1vZHVsZS5leHBvcnRzKSB7IC8vbm9kZVxyXG5cdFx0cmVxdWlyZShcIi4uL1R3ZWVuTGl0ZS5qc1wiKTtcclxuXHRcdG1vZHVsZS5leHBvcnRzID0gZ2V0R2xvYmFsKCk7XHJcblx0fSBlbHNlIGlmICh0eXBlb2YoZGVmaW5lKSA9PT0gXCJmdW5jdGlvblwiICYmIGRlZmluZS5hbWQpIHsgLy9BTURcclxuXHRcdGRlZmluZShbXCJUd2VlbkxpdGVcIl0sIGdldEdsb2JhbCk7XHJcblx0fVxyXG59KFwiU2Nyb2xsVG9QbHVnaW5cIikpO1xyXG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9fSFRNTC9zcmMvanMvX3ZlbmRvcnMvZ3NhcC9wbHVnaW5zL1Njcm9sbFRvUGx1Z2luLmpzIiwiJ3VzZSBzdHJpY3QnO1xyXG5cclxuLyoqXHJcbiAqINCf0YDQvtGB0LvQvtC50LrQsCDQtNC70Y8g0LfQsNCz0YDRg9C30LrQuCDQvNC+0LTRg9C70Y8gYHNjcm9sbC13aW5kb3dgXHJcbiAqIEBtb2R1bGVcclxuICovXHJcblxyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbi8vIEltcG9ydHNcclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuaW1wb3J0IHNjcm9sbFdpbmRvdyBmcm9tICcjL19tb2R1bGVzL3Njcm9sbC13aW5kb3cnO1xyXG5cclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4vLyBQdWJsaWNcclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuLyoqXHJcbiAqIEBwYXJhbSB7SlF1ZXJ5fSAkZWxlbWVudHNcclxuICogQHBhcmFtIHtNb2R1bGVMb2FkZXJ9IG1vZHVsZUxvYWRlclxyXG4gKi9cclxuZnVuY3Rpb24gbG9hZGVySW5pdCAoJGVsZW1lbnRzLCBtb2R1bGVMb2FkZXIpIHtcclxuXHRzY3JvbGxXaW5kb3coJGVsZW1lbnRzKTtcclxufVxyXG5cclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4vLyBFeHBvcnRzXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbmV4cG9ydCB7bG9hZGVySW5pdH07XHJcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL19IVE1ML3NyYy9qcy9fbW9kdWxlcy1sb2FkZXJzL3Njcm9sbC13aW5kb3ctLW1vZHVsZS1sb2FkZXIuanMiLCIvKiFcclxuICogVkVSU0lPTjogMS4yMC4zXHJcbiAqIERBVEU6IDIwMTctMTAtMDJcclxuICogVVBEQVRFUyBBTkQgRE9DUyBBVDogaHR0cDovL2dyZWVuc29jay5jb21cclxuICpcclxuICogQGxpY2Vuc2UgQ29weXJpZ2h0IChjKSAyMDA4LTIwMTcsIEdyZWVuU29jay4gQWxsIHJpZ2h0cyByZXNlcnZlZC5cclxuICogVGhpcyB3b3JrIGlzIHN1YmplY3QgdG8gdGhlIHRlcm1zIGF0IGh0dHA6Ly9ncmVlbnNvY2suY29tL3N0YW5kYXJkLWxpY2Vuc2Ugb3IgZm9yXHJcbiAqIENsdWIgR3JlZW5Tb2NrIG1lbWJlcnMsIHRoZSBzb2Z0d2FyZSBhZ3JlZW1lbnQgdGhhdCB3YXMgaXNzdWVkIHdpdGggeW91ciBtZW1iZXJzaGlwLlxyXG4gKiBcclxuICogQGF1dGhvcjogSmFjayBEb3lsZSwgamFja0BncmVlbnNvY2suY29tXHJcbiAqL1xyXG4oZnVuY3Rpb24od2luZG93LCBtb2R1bGVOYW1lKSB7XHJcblxyXG5cdFx0XCJ1c2Ugc3RyaWN0XCI7XHJcblx0XHR2YXIgX2V4cG9ydHMgPSB7fSxcclxuXHRcdFx0X2RvYyA9IHdpbmRvdy5kb2N1bWVudCxcclxuXHRcdFx0X2dsb2JhbHMgPSB3aW5kb3cuR3JlZW5Tb2NrR2xvYmFscyA9IHdpbmRvdy5HcmVlblNvY2tHbG9iYWxzIHx8IHdpbmRvdztcclxuXHRcdGlmIChfZ2xvYmFscy5Ud2VlbkxpdGUpIHtcclxuXHRcdFx0cmV0dXJuOyAvL2luIGNhc2UgdGhlIGNvcmUgc2V0IG9mIGNsYXNzZXMgaXMgYWxyZWFkeSBsb2FkZWQsIGRvbid0IGluc3RhbnRpYXRlIHR3aWNlLlxyXG5cdFx0fVxyXG5cdFx0dmFyIF9uYW1lc3BhY2UgPSBmdW5jdGlvbihucykge1xyXG5cdFx0XHRcdHZhciBhID0gbnMuc3BsaXQoXCIuXCIpLFxyXG5cdFx0XHRcdFx0cCA9IF9nbG9iYWxzLCBpO1xyXG5cdFx0XHRcdGZvciAoaSA9IDA7IGkgPCBhLmxlbmd0aDsgaSsrKSB7XHJcblx0XHRcdFx0XHRwW2FbaV1dID0gcCA9IHBbYVtpXV0gfHwge307XHJcblx0XHRcdFx0fVxyXG5cdFx0XHRcdHJldHVybiBwO1xyXG5cdFx0XHR9LFxyXG5cdFx0XHRncyA9IF9uYW1lc3BhY2UoXCJjb20uZ3JlZW5zb2NrXCIpLFxyXG5cdFx0XHRfdGlueU51bSA9IDAuMDAwMDAwMDAwMSxcclxuXHRcdFx0X3NsaWNlID0gZnVuY3Rpb24oYSkgeyAvL2Rvbid0IHVzZSBBcnJheS5wcm90b3R5cGUuc2xpY2UuY2FsbCh0YXJnZXQsIDApIGJlY2F1c2UgdGhhdCBkb2Vzbid0IHdvcmsgaW4gSUU4IHdpdGggYSBOb2RlTGlzdCB0aGF0J3MgcmV0dXJuZWQgYnkgcXVlcnlTZWxlY3RvckFsbCgpXHJcblx0XHRcdFx0dmFyIGIgPSBbXSxcclxuXHRcdFx0XHRcdGwgPSBhLmxlbmd0aCxcclxuXHRcdFx0XHRcdGk7XHJcblx0XHRcdFx0Zm9yIChpID0gMDsgaSAhPT0gbDsgYi5wdXNoKGFbaSsrXSkpIHt9XHJcblx0XHRcdFx0cmV0dXJuIGI7XHJcblx0XHRcdH0sXHJcblx0XHRcdF9lbXB0eUZ1bmMgPSBmdW5jdGlvbigpIHt9LFxyXG5cdFx0XHRfaXNBcnJheSA9IChmdW5jdGlvbigpIHsgLy93b3JrcyBhcm91bmQgaXNzdWVzIGluIGlmcmFtZSBlbnZpcm9ubWVudHMgd2hlcmUgdGhlIEFycmF5IGdsb2JhbCBpc24ndCBzaGFyZWQsIHRodXMgaWYgdGhlIG9iamVjdCBvcmlnaW5hdGVzIGluIGEgZGlmZmVyZW50IHdpbmRvdy9pZnJhbWUsIFwiKG9iaiBpbnN0YW5jZW9mIEFycmF5KVwiIHdpbGwgZXZhbHVhdGUgZmFsc2UuIFdlIGFkZGVkIHNvbWUgc3BlZWQgb3B0aW1pemF0aW9ucyB0byBhdm9pZCBPYmplY3QucHJvdG90eXBlLnRvU3RyaW5nLmNhbGwoKSB1bmxlc3MgaXQncyBhYnNvbHV0ZWx5IG5lY2Vzc2FyeSBiZWNhdXNlIGl0J3MgVkVSWSBzbG93IChsaWtlIDIweCBzbG93ZXIpXHJcblx0XHRcdFx0dmFyIHRvU3RyaW5nID0gT2JqZWN0LnByb3RvdHlwZS50b1N0cmluZyxcclxuXHRcdFx0XHRcdGFycmF5ID0gdG9TdHJpbmcuY2FsbChbXSk7XHJcblx0XHRcdFx0cmV0dXJuIGZ1bmN0aW9uKG9iaikge1xyXG5cdFx0XHRcdFx0cmV0dXJuIG9iaiAhPSBudWxsICYmIChvYmogaW5zdGFuY2VvZiBBcnJheSB8fCAodHlwZW9mKG9iaikgPT09IFwib2JqZWN0XCIgJiYgISFvYmoucHVzaCAmJiB0b1N0cmluZy5jYWxsKG9iaikgPT09IGFycmF5KSk7XHJcblx0XHRcdFx0fTtcclxuXHRcdFx0fSgpKSxcclxuXHRcdFx0YSwgaSwgcCwgX3RpY2tlciwgX3RpY2tlckFjdGl2ZSxcclxuXHRcdFx0X2RlZkxvb2t1cCA9IHt9LFxyXG5cclxuXHRcdFx0LyoqXHJcblx0XHRcdCAqIEBjb25zdHJ1Y3RvclxyXG5cdFx0XHQgKiBEZWZpbmVzIGEgR3JlZW5Tb2NrIGNsYXNzLCBvcHRpb25hbGx5IHdpdGggYW4gYXJyYXkgb2YgZGVwZW5kZW5jaWVzIHRoYXQgbXVzdCBiZSBpbnN0YW50aWF0ZWQgZmlyc3QgYW5kIHBhc3NlZCBpbnRvIHRoZSBkZWZpbml0aW9uLlxyXG5cdFx0XHQgKiBUaGlzIGFsbG93cyB1c2VycyB0byBsb2FkIEdyZWVuU29jayBKUyBmaWxlcyBpbiBhbnkgb3JkZXIgZXZlbiBpZiB0aGV5IGhhdmUgaW50ZXJkZXBlbmRlbmNpZXMgKGxpa2UgQ1NTUGx1Z2luIGV4dGVuZHMgVHdlZW5QbHVnaW4gd2hpY2ggaXNcclxuXHRcdFx0ICogaW5zaWRlIFR3ZWVuTGl0ZS5qcywgYnV0IGlmIENTU1BsdWdpbiBpcyBsb2FkZWQgZmlyc3QsIGl0IHNob3VsZCB3YWl0IHRvIHJ1biBpdHMgY29kZSB1bnRpbCBUd2VlbkxpdGUuanMgbG9hZHMgYW5kIGluc3RhbnRpYXRlcyBUd2VlblBsdWdpblxyXG5cdFx0XHQgKiBhbmQgdGhlbiBwYXNzIFR3ZWVuUGx1Z2luIHRvIENTU1BsdWdpbidzIGRlZmluaXRpb24pLiBUaGlzIGlzIGFsbCBkb25lIGF1dG9tYXRpY2FsbHkgYW5kIGludGVybmFsbHkuXHJcblx0XHRcdCAqXHJcblx0XHRcdCAqIEV2ZXJ5IGRlZmluaXRpb24gd2lsbCBiZSBhZGRlZCB0byBhIFwiY29tLmdyZWVuc29ja1wiIGdsb2JhbCBvYmplY3QgKHR5cGljYWxseSB3aW5kb3csIGJ1dCBpZiBhIHdpbmRvdy5HcmVlblNvY2tHbG9iYWxzIG9iamVjdCBpcyBmb3VuZCxcclxuXHRcdFx0ICogaXQgd2lsbCBnbyB0aGVyZSBhcyBvZiB2MS43KS4gRm9yIGV4YW1wbGUsIFR3ZWVuTGl0ZSB3aWxsIGJlIGZvdW5kIGF0IHdpbmRvdy5jb20uZ3JlZW5zb2NrLlR3ZWVuTGl0ZSBhbmQgc2luY2UgaXQncyBhIGdsb2JhbCBjbGFzcyB0aGF0IHNob3VsZCBiZSBhdmFpbGFibGUgYW55d2hlcmUsXHJcblx0XHRcdCAqIGl0IGlzIEFMU08gcmVmZXJlbmNlZCBhdCB3aW5kb3cuVHdlZW5MaXRlLiBIb3dldmVyIHNvbWUgY2xhc3NlcyBhcmVuJ3QgY29uc2lkZXJlZCBnbG9iYWwsIGxpa2UgdGhlIGJhc2UgY29tLmdyZWVuc29jay5jb3JlLkFuaW1hdGlvbiBjbGFzcywgc29cclxuXHRcdFx0ICogdGhvc2Ugd2lsbCBvbmx5IGJlIGF0IHRoZSBwYWNrYWdlIGxpa2Ugd2luZG93LmNvbS5ncmVlbnNvY2suY29yZS5BbmltYXRpb24uIEFnYWluLCBpZiB5b3UgZGVmaW5lIGEgR3JlZW5Tb2NrR2xvYmFscyBvYmplY3Qgb24gdGhlIHdpbmRvdywgZXZlcnl0aGluZ1xyXG5cdFx0XHQgKiBnZXRzIHR1Y2tlZCBuZWF0bHkgaW5zaWRlIHRoZXJlIGluc3RlYWQgb2Ygb24gdGhlIHdpbmRvdyBkaXJlY3RseS4gVGhpcyBhbGxvd3MgeW91IHRvIGRvIGFkdmFuY2VkIHRoaW5ncyBsaWtlIGxvYWQgbXVsdGlwbGUgdmVyc2lvbnMgb2YgR3JlZW5Tb2NrXHJcblx0XHRcdCAqIGZpbGVzIGFuZCBwdXQgdGhlbSBpbnRvIGRpc3RpbmN0IG9iamVjdHMgKGltYWdpbmUgYSBiYW5uZXIgYWQgdXNlcyBhIG5ld2VyIHZlcnNpb24gYnV0IHRoZSBtYWluIHNpdGUgdXNlcyBhbiBvbGRlciBvbmUpLiBJbiB0aGF0IGNhc2UsIHlvdSBjb3VsZFxyXG5cdFx0XHQgKiBzYW5kYm94IHRoZSBiYW5uZXIgb25lIGxpa2U6XHJcblx0XHRcdCAqXHJcblx0XHRcdCAqIDxzY3JpcHQ+XHJcblx0XHRcdCAqICAgICB2YXIgZ3MgPSB3aW5kb3cuR3JlZW5Tb2NrR2xvYmFscyA9IHt9OyAvL3RoZSBuZXdlciB2ZXJzaW9uIHdlJ3JlIGFib3V0IHRvIGxvYWQgY291bGQgbm93IGJlIHJlZmVyZW5jZWQgaW4gYSBcImdzXCIgb2JqZWN0LCBsaWtlIGdzLlR3ZWVuTGl0ZS50byguLi4pLiBVc2Ugd2hhdGV2ZXIgYWxpYXMgeW91IHdhbnQgYXMgbG9uZyBhcyBpdCdzIHVuaXF1ZSwgXCJnc1wiIG9yIFwiYmFubmVyXCIgb3Igd2hhdGV2ZXIuXHJcblx0XHRcdCAqIDwvc2NyaXB0PlxyXG5cdFx0XHQgKiA8c2NyaXB0IHNyYz1cImpzL2dyZWVuc29jay92MS43L1R3ZWVuTWF4LmpzXCI+PC9zY3JpcHQ+XHJcblx0XHRcdCAqIDxzY3JpcHQ+XHJcblx0XHRcdCAqICAgICB3aW5kb3cuR3JlZW5Tb2NrR2xvYmFscyA9IHdpbmRvdy5fZ3NRdWV1ZSA9IHdpbmRvdy5fZ3NEZWZpbmUgPSBudWxsOyAvL3Jlc2V0IGl0IGJhY2sgdG8gbnVsbCAoYWxvbmcgd2l0aCB0aGUgc3BlY2lhbCBfZ3NRdWV1ZSB2YXJpYWJsZSkgc28gdGhhdCB0aGUgbmV4dCBsb2FkIG9mIFR3ZWVuTWF4IGFmZmVjdHMgdGhlIHdpbmRvdyBhbmQgd2UgY2FuIHJlZmVyZW5jZSB0aGluZ3MgZGlyZWN0bHkgbGlrZSBUd2VlbkxpdGUudG8oLi4uKVxyXG5cdFx0XHQgKiA8L3NjcmlwdD5cclxuXHRcdFx0ICogPHNjcmlwdCBzcmM9XCJqcy9ncmVlbnNvY2svdjEuNi9Ud2Vlbk1heC5qc1wiPjwvc2NyaXB0PlxyXG5cdFx0XHQgKiA8c2NyaXB0PlxyXG5cdFx0XHQgKiAgICAgZ3MuVHdlZW5MaXRlLnRvKC4uLik7IC8vd291bGQgdXNlIHYxLjdcclxuXHRcdFx0ICogICAgIFR3ZWVuTGl0ZS50byguLi4pOyAvL3dvdWxkIHVzZSB2MS42XHJcblx0XHRcdCAqIDwvc2NyaXB0PlxyXG5cdFx0XHQgKlxyXG5cdFx0XHQgKiBAcGFyYW0geyFzdHJpbmd9IG5zIFRoZSBuYW1lc3BhY2Ugb2YgdGhlIGNsYXNzIGRlZmluaXRpb24sIGxlYXZpbmcgb2ZmIFwiY29tLmdyZWVuc29jay5cIiBhcyB0aGF0J3MgYXNzdW1lZC4gRm9yIGV4YW1wbGUsIFwiVHdlZW5MaXRlXCIgb3IgXCJwbHVnaW5zLkNTU1BsdWdpblwiIG9yIFwiZWFzaW5nLkJhY2tcIi5cclxuXHRcdFx0ICogQHBhcmFtIHshQXJyYXkuPHN0cmluZz59IGRlcGVuZGVuY2llcyBBbiBhcnJheSBvZiBkZXBlbmRlbmNpZXMgKGRlc2NyaWJlZCBhcyB0aGVpciBuYW1lc3BhY2VzIG1pbnVzIFwiY29tLmdyZWVuc29jay5cIiBwcmVmaXgpLiBGb3IgZXhhbXBsZSBbXCJUd2VlbkxpdGVcIixcInBsdWdpbnMuVHdlZW5QbHVnaW5cIixcImNvcmUuQW5pbWF0aW9uXCJdXHJcblx0XHRcdCAqIEBwYXJhbSB7IWZ1bmN0aW9uKCk6T2JqZWN0fSBmdW5jIFRoZSBmdW5jdGlvbiB0aGF0IHNob3VsZCBiZSBjYWxsZWQgYW5kIHBhc3NlZCB0aGUgcmVzb2x2ZWQgZGVwZW5kZW5jaWVzIHdoaWNoIHdpbGwgcmV0dXJuIHRoZSBhY3R1YWwgY2xhc3MgZm9yIHRoaXMgZGVmaW5pdGlvbi5cclxuXHRcdFx0ICogQHBhcmFtIHtib29sZWFuPX0gZ2xvYmFsIElmIHRydWUsIHRoZSBjbGFzcyB3aWxsIGJlIGFkZGVkIHRvIHRoZSBnbG9iYWwgc2NvcGUgKHR5cGljYWxseSB3aW5kb3cgdW5sZXNzIHlvdSBkZWZpbmUgYSB3aW5kb3cuR3JlZW5Tb2NrR2xvYmFscyBvYmplY3QpXHJcblx0XHRcdCAqL1xyXG5cdFx0XHREZWZpbml0aW9uID0gZnVuY3Rpb24obnMsIGRlcGVuZGVuY2llcywgZnVuYywgZ2xvYmFsKSB7XHJcblx0XHRcdFx0dGhpcy5zYyA9IChfZGVmTG9va3VwW25zXSkgPyBfZGVmTG9va3VwW25zXS5zYyA6IFtdOyAvL3N1YmNsYXNzZXNcclxuXHRcdFx0XHRfZGVmTG9va3VwW25zXSA9IHRoaXM7XHJcblx0XHRcdFx0dGhpcy5nc0NsYXNzID0gbnVsbDtcclxuXHRcdFx0XHR0aGlzLmZ1bmMgPSBmdW5jO1xyXG5cdFx0XHRcdHZhciBfY2xhc3NlcyA9IFtdO1xyXG5cdFx0XHRcdHRoaXMuY2hlY2sgPSBmdW5jdGlvbihpbml0KSB7XHJcblx0XHRcdFx0XHR2YXIgaSA9IGRlcGVuZGVuY2llcy5sZW5ndGgsXHJcblx0XHRcdFx0XHRcdG1pc3NpbmcgPSBpLFxyXG5cdFx0XHRcdFx0XHRjdXIsIGEsIG4sIGNsO1xyXG5cdFx0XHRcdFx0d2hpbGUgKC0taSA+IC0xKSB7XHJcblx0XHRcdFx0XHRcdGlmICgoY3VyID0gX2RlZkxvb2t1cFtkZXBlbmRlbmNpZXNbaV1dIHx8IG5ldyBEZWZpbml0aW9uKGRlcGVuZGVuY2llc1tpXSwgW10pKS5nc0NsYXNzKSB7XHJcblx0XHRcdFx0XHRcdFx0X2NsYXNzZXNbaV0gPSBjdXIuZ3NDbGFzcztcclxuXHRcdFx0XHRcdFx0XHRtaXNzaW5nLS07XHJcblx0XHRcdFx0XHRcdH0gZWxzZSBpZiAoaW5pdCkge1xyXG5cdFx0XHRcdFx0XHRcdGN1ci5zYy5wdXNoKHRoaXMpO1xyXG5cdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRpZiAobWlzc2luZyA9PT0gMCAmJiBmdW5jKSB7XHJcblx0XHRcdFx0XHRcdGEgPSAoXCJjb20uZ3JlZW5zb2NrLlwiICsgbnMpLnNwbGl0KFwiLlwiKTtcclxuXHRcdFx0XHRcdFx0biA9IGEucG9wKCk7XHJcblx0XHRcdFx0XHRcdGNsID0gX25hbWVzcGFjZShhLmpvaW4oXCIuXCIpKVtuXSA9IHRoaXMuZ3NDbGFzcyA9IGZ1bmMuYXBwbHkoZnVuYywgX2NsYXNzZXMpO1xyXG5cclxuXHRcdFx0XHRcdFx0Ly9leHBvcnRzIHRvIG11bHRpcGxlIGVudmlyb25tZW50c1xyXG5cdFx0XHRcdFx0XHRpZiAoZ2xvYmFsKSB7XHJcblx0XHRcdFx0XHRcdFx0X2dsb2JhbHNbbl0gPSBfZXhwb3J0c1tuXSA9IGNsOyAvL3Byb3ZpZGVzIGEgd2F5IHRvIGF2b2lkIGdsb2JhbCBuYW1lc3BhY2UgcG9sbHV0aW9uLiBCeSBkZWZhdWx0LCB0aGUgbWFpbiBjbGFzc2VzIGxpa2UgVHdlZW5MaXRlLCBQb3dlcjEsIFN0cm9uZywgZXRjLiBhcmUgYWRkZWQgdG8gd2luZG93IHVubGVzcyBhIEdyZWVuU29ja0dsb2JhbHMgaXMgZGVmaW5lZC4gU28gaWYgeW91IHdhbnQgdG8gaGF2ZSB0aGluZ3MgYWRkZWQgdG8gYSBjdXN0b20gb2JqZWN0IGluc3RlYWQsIGp1c3QgZG8gc29tZXRoaW5nIGxpa2Ugd2luZG93LkdyZWVuU29ja0dsb2JhbHMgPSB7fSBiZWZvcmUgbG9hZGluZyBhbnkgR3JlZW5Tb2NrIGZpbGVzLiBZb3UgY2FuIGV2ZW4gc2V0IHVwIGFuIGFsaWFzIGxpa2Ugd2luZG93LkdyZWVuU29ja0dsb2JhbHMgPSB3aW5kb3dzLmdzID0ge30gc28gdGhhdCB5b3UgY2FuIGFjY2VzcyBldmVyeXRoaW5nIGxpa2UgZ3MuVHdlZW5MaXRlLiBBbHNvIHJlbWVtYmVyIHRoYXQgQUxMIGNsYXNzZXMgYXJlIGFkZGVkIHRvIHRoZSB3aW5kb3cuY29tLmdyZWVuc29jayBvYmplY3QgKGluIHRoZWlyIHJlc3BlY3RpdmUgcGFja2FnZXMsIGxpa2UgY29tLmdyZWVuc29jay5lYXNpbmcuUG93ZXIxLCBjb20uZ3JlZW5zb2NrLlR3ZWVuTGl0ZSwgZXRjLilcclxuXHRcdFx0XHRcdFx0XHRpZiAodHlwZW9mKG1vZHVsZSkgIT09IFwidW5kZWZpbmVkXCIgJiYgbW9kdWxlLmV4cG9ydHMpIHsgLy9ub2RlXHJcblx0XHRcdFx0XHRcdFx0XHRpZiAobnMgPT09IG1vZHVsZU5hbWUpIHtcclxuXHRcdFx0XHRcdFx0XHRcdFx0bW9kdWxlLmV4cG9ydHMgPSBfZXhwb3J0c1ttb2R1bGVOYW1lXSA9IGNsO1xyXG5cdFx0XHRcdFx0XHRcdFx0XHRmb3IgKGkgaW4gX2V4cG9ydHMpIHtcclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRjbFtpXSA9IF9leHBvcnRzW2ldO1xyXG5cdFx0XHRcdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRcdFx0XHR9IGVsc2UgaWYgKF9leHBvcnRzW21vZHVsZU5hbWVdKSB7XHJcblx0XHRcdFx0XHRcdFx0XHRcdF9leHBvcnRzW21vZHVsZU5hbWVdW25dID0gY2w7XHJcblx0XHRcdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRcdFx0fSBlbHNlIGlmICh0eXBlb2YoZGVmaW5lKSA9PT0gXCJmdW5jdGlvblwiICYmIGRlZmluZS5hbWQpeyAvL0FNRFxyXG5cdFx0XHRcdFx0XHRcdFx0ZGVmaW5lKCh3aW5kb3cuR3JlZW5Tb2NrQU1EUGF0aCA/IHdpbmRvdy5HcmVlblNvY2tBTURQYXRoICsgXCIvXCIgOiBcIlwiKSArIG5zLnNwbGl0KFwiLlwiKS5wb3AoKSwgW10sIGZ1bmN0aW9uKCkgeyByZXR1cm4gY2w7IH0pO1xyXG5cdFx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0XHRmb3IgKGkgPSAwOyBpIDwgdGhpcy5zYy5sZW5ndGg7IGkrKykge1xyXG5cdFx0XHRcdFx0XHRcdHRoaXMuc2NbaV0uY2hlY2soKTtcclxuXHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdH07XHJcblx0XHRcdFx0dGhpcy5jaGVjayh0cnVlKTtcclxuXHRcdFx0fSxcclxuXHJcblx0XHRcdC8vdXNlZCB0byBjcmVhdGUgRGVmaW5pdGlvbiBpbnN0YW5jZXMgKHdoaWNoIGJhc2ljYWxseSByZWdpc3RlcnMgYSBjbGFzcyB0aGF0IGhhcyBkZXBlbmRlbmNpZXMpLlxyXG5cdFx0XHRfZ3NEZWZpbmUgPSB3aW5kb3cuX2dzRGVmaW5lID0gZnVuY3Rpb24obnMsIGRlcGVuZGVuY2llcywgZnVuYywgZ2xvYmFsKSB7XHJcblx0XHRcdFx0cmV0dXJuIG5ldyBEZWZpbml0aW9uKG5zLCBkZXBlbmRlbmNpZXMsIGZ1bmMsIGdsb2JhbCk7XHJcblx0XHRcdH0sXHJcblxyXG5cdFx0XHQvL2EgcXVpY2sgd2F5IHRvIGNyZWF0ZSBhIGNsYXNzIHRoYXQgZG9lc24ndCBoYXZlIGFueSBkZXBlbmRlbmNpZXMuIFJldHVybnMgdGhlIGNsYXNzLCBidXQgZmlyc3QgcmVnaXN0ZXJzIGl0IGluIHRoZSBHcmVlblNvY2sgbmFtZXNwYWNlIHNvIHRoYXQgb3RoZXIgY2xhc3NlcyBjYW4gZ3JhYiBpdCAob3RoZXIgY2xhc3NlcyBtaWdodCBiZSBkZXBlbmRlbnQgb24gdGhlIGNsYXNzKS5cclxuXHRcdFx0X2NsYXNzID0gZ3MuX2NsYXNzID0gZnVuY3Rpb24obnMsIGZ1bmMsIGdsb2JhbCkge1xyXG5cdFx0XHRcdGZ1bmMgPSBmdW5jIHx8IGZ1bmN0aW9uKCkge307XHJcblx0XHRcdFx0X2dzRGVmaW5lKG5zLCBbXSwgZnVuY3Rpb24oKXsgcmV0dXJuIGZ1bmM7IH0sIGdsb2JhbCk7XHJcblx0XHRcdFx0cmV0dXJuIGZ1bmM7XHJcblx0XHRcdH07XHJcblxyXG5cdFx0X2dzRGVmaW5lLmdsb2JhbHMgPSBfZ2xvYmFscztcclxuXHJcblxyXG5cclxuLypcclxuICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gKiBFYXNlXHJcbiAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICovXHJcblx0XHR2YXIgX2Jhc2VQYXJhbXMgPSBbMCwgMCwgMSwgMV0sXHJcblx0XHRcdEVhc2UgPSBfY2xhc3MoXCJlYXNpbmcuRWFzZVwiLCBmdW5jdGlvbihmdW5jLCBleHRyYVBhcmFtcywgdHlwZSwgcG93ZXIpIHtcclxuXHRcdFx0XHR0aGlzLl9mdW5jID0gZnVuYztcclxuXHRcdFx0XHR0aGlzLl90eXBlID0gdHlwZSB8fCAwO1xyXG5cdFx0XHRcdHRoaXMuX3Bvd2VyID0gcG93ZXIgfHwgMDtcclxuXHRcdFx0XHR0aGlzLl9wYXJhbXMgPSBleHRyYVBhcmFtcyA/IF9iYXNlUGFyYW1zLmNvbmNhdChleHRyYVBhcmFtcykgOiBfYmFzZVBhcmFtcztcclxuXHRcdFx0fSwgdHJ1ZSksXHJcblx0XHRcdF9lYXNlTWFwID0gRWFzZS5tYXAgPSB7fSxcclxuXHRcdFx0X2Vhc2VSZWcgPSBFYXNlLnJlZ2lzdGVyID0gZnVuY3Rpb24oZWFzZSwgbmFtZXMsIHR5cGVzLCBjcmVhdGUpIHtcclxuXHRcdFx0XHR2YXIgbmEgPSBuYW1lcy5zcGxpdChcIixcIiksXHJcblx0XHRcdFx0XHRpID0gbmEubGVuZ3RoLFxyXG5cdFx0XHRcdFx0dGEgPSAodHlwZXMgfHwgXCJlYXNlSW4sZWFzZU91dCxlYXNlSW5PdXRcIikuc3BsaXQoXCIsXCIpLFxyXG5cdFx0XHRcdFx0ZSwgbmFtZSwgaiwgdHlwZTtcclxuXHRcdFx0XHR3aGlsZSAoLS1pID4gLTEpIHtcclxuXHRcdFx0XHRcdG5hbWUgPSBuYVtpXTtcclxuXHRcdFx0XHRcdGUgPSBjcmVhdGUgPyBfY2xhc3MoXCJlYXNpbmcuXCIrbmFtZSwgbnVsbCwgdHJ1ZSkgOiBncy5lYXNpbmdbbmFtZV0gfHwge307XHJcblx0XHRcdFx0XHRqID0gdGEubGVuZ3RoO1xyXG5cdFx0XHRcdFx0d2hpbGUgKC0taiA+IC0xKSB7XHJcblx0XHRcdFx0XHRcdHR5cGUgPSB0YVtqXTtcclxuXHRcdFx0XHRcdFx0X2Vhc2VNYXBbbmFtZSArIFwiLlwiICsgdHlwZV0gPSBfZWFzZU1hcFt0eXBlICsgbmFtZV0gPSBlW3R5cGVdID0gZWFzZS5nZXRSYXRpbyA/IGVhc2UgOiBlYXNlW3R5cGVdIHx8IG5ldyBlYXNlKCk7XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9O1xyXG5cclxuXHRcdHAgPSBFYXNlLnByb3RvdHlwZTtcclxuXHRcdHAuX2NhbGNFbmQgPSBmYWxzZTtcclxuXHRcdHAuZ2V0UmF0aW8gPSBmdW5jdGlvbihwKSB7XHJcblx0XHRcdGlmICh0aGlzLl9mdW5jKSB7XHJcblx0XHRcdFx0dGhpcy5fcGFyYW1zWzBdID0gcDtcclxuXHRcdFx0XHRyZXR1cm4gdGhpcy5fZnVuYy5hcHBseShudWxsLCB0aGlzLl9wYXJhbXMpO1xyXG5cdFx0XHR9XHJcblx0XHRcdHZhciB0ID0gdGhpcy5fdHlwZSxcclxuXHRcdFx0XHRwdyA9IHRoaXMuX3Bvd2VyLFxyXG5cdFx0XHRcdHIgPSAodCA9PT0gMSkgPyAxIC0gcCA6ICh0ID09PSAyKSA/IHAgOiAocCA8IDAuNSkgPyBwICogMiA6ICgxIC0gcCkgKiAyO1xyXG5cdFx0XHRpZiAocHcgPT09IDEpIHtcclxuXHRcdFx0XHRyICo9IHI7XHJcblx0XHRcdH0gZWxzZSBpZiAocHcgPT09IDIpIHtcclxuXHRcdFx0XHRyICo9IHIgKiByO1xyXG5cdFx0XHR9IGVsc2UgaWYgKHB3ID09PSAzKSB7XHJcblx0XHRcdFx0ciAqPSByICogciAqIHI7XHJcblx0XHRcdH0gZWxzZSBpZiAocHcgPT09IDQpIHtcclxuXHRcdFx0XHRyICo9IHIgKiByICogciAqIHI7XHJcblx0XHRcdH1cclxuXHRcdFx0cmV0dXJuICh0ID09PSAxKSA/IDEgLSByIDogKHQgPT09IDIpID8gciA6IChwIDwgMC41KSA/IHIgLyAyIDogMSAtIChyIC8gMik7XHJcblx0XHR9O1xyXG5cclxuXHRcdC8vY3JlYXRlIGFsbCB0aGUgc3RhbmRhcmQgZWFzZXMgbGlrZSBMaW5lYXIsIFF1YWQsIEN1YmljLCBRdWFydCwgUXVpbnQsIFN0cm9uZywgUG93ZXIwLCBQb3dlcjEsIFBvd2VyMiwgUG93ZXIzLCBhbmQgUG93ZXI0IChlYWNoIHdpdGggZWFzZUluLCBlYXNlT3V0LCBhbmQgZWFzZUluT3V0KVxyXG5cdFx0YSA9IFtcIkxpbmVhclwiLFwiUXVhZFwiLFwiQ3ViaWNcIixcIlF1YXJ0XCIsXCJRdWludCxTdHJvbmdcIl07XHJcblx0XHRpID0gYS5sZW5ndGg7XHJcblx0XHR3aGlsZSAoLS1pID4gLTEpIHtcclxuXHRcdFx0cCA9IGFbaV0rXCIsUG93ZXJcIitpO1xyXG5cdFx0XHRfZWFzZVJlZyhuZXcgRWFzZShudWxsLG51bGwsMSxpKSwgcCwgXCJlYXNlT3V0XCIsIHRydWUpO1xyXG5cdFx0XHRfZWFzZVJlZyhuZXcgRWFzZShudWxsLG51bGwsMixpKSwgcCwgXCJlYXNlSW5cIiArICgoaSA9PT0gMCkgPyBcIixlYXNlTm9uZVwiIDogXCJcIikpO1xyXG5cdFx0XHRfZWFzZVJlZyhuZXcgRWFzZShudWxsLG51bGwsMyxpKSwgcCwgXCJlYXNlSW5PdXRcIik7XHJcblx0XHR9XHJcblx0XHRfZWFzZU1hcC5saW5lYXIgPSBncy5lYXNpbmcuTGluZWFyLmVhc2VJbjtcclxuXHRcdF9lYXNlTWFwLnN3aW5nID0gZ3MuZWFzaW5nLlF1YWQuZWFzZUluT3V0OyAvL2ZvciBqUXVlcnkgZm9sa3NcclxuXHJcblxyXG4vKlxyXG4gKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAqIEV2ZW50RGlzcGF0Y2hlclxyXG4gKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAqL1xyXG5cdFx0dmFyIEV2ZW50RGlzcGF0Y2hlciA9IF9jbGFzcyhcImV2ZW50cy5FdmVudERpc3BhdGNoZXJcIiwgZnVuY3Rpb24odGFyZ2V0KSB7XHJcblx0XHRcdHRoaXMuX2xpc3RlbmVycyA9IHt9O1xyXG5cdFx0XHR0aGlzLl9ldmVudFRhcmdldCA9IHRhcmdldCB8fCB0aGlzO1xyXG5cdFx0fSk7XHJcblx0XHRwID0gRXZlbnREaXNwYXRjaGVyLnByb3RvdHlwZTtcclxuXHJcblx0XHRwLmFkZEV2ZW50TGlzdGVuZXIgPSBmdW5jdGlvbih0eXBlLCBjYWxsYmFjaywgc2NvcGUsIHVzZVBhcmFtLCBwcmlvcml0eSkge1xyXG5cdFx0XHRwcmlvcml0eSA9IHByaW9yaXR5IHx8IDA7XHJcblx0XHRcdHZhciBsaXN0ID0gdGhpcy5fbGlzdGVuZXJzW3R5cGVdLFxyXG5cdFx0XHRcdGluZGV4ID0gMCxcclxuXHRcdFx0XHRsaXN0ZW5lciwgaTtcclxuXHRcdFx0aWYgKHRoaXMgPT09IF90aWNrZXIgJiYgIV90aWNrZXJBY3RpdmUpIHtcclxuXHRcdFx0XHRfdGlja2VyLndha2UoKTtcclxuXHRcdFx0fVxyXG5cdFx0XHRpZiAobGlzdCA9PSBudWxsKSB7XHJcblx0XHRcdFx0dGhpcy5fbGlzdGVuZXJzW3R5cGVdID0gbGlzdCA9IFtdO1xyXG5cdFx0XHR9XHJcblx0XHRcdGkgPSBsaXN0Lmxlbmd0aDtcclxuXHRcdFx0d2hpbGUgKC0taSA+IC0xKSB7XHJcblx0XHRcdFx0bGlzdGVuZXIgPSBsaXN0W2ldO1xyXG5cdFx0XHRcdGlmIChsaXN0ZW5lci5jID09PSBjYWxsYmFjayAmJiBsaXN0ZW5lci5zID09PSBzY29wZSkge1xyXG5cdFx0XHRcdFx0bGlzdC5zcGxpY2UoaSwgMSk7XHJcblx0XHRcdFx0fSBlbHNlIGlmIChpbmRleCA9PT0gMCAmJiBsaXN0ZW5lci5wciA8IHByaW9yaXR5KSB7XHJcblx0XHRcdFx0XHRpbmRleCA9IGkgKyAxO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cdFx0XHRsaXN0LnNwbGljZShpbmRleCwgMCwge2M6Y2FsbGJhY2ssIHM6c2NvcGUsIHVwOnVzZVBhcmFtLCBwcjpwcmlvcml0eX0pO1xyXG5cdFx0fTtcclxuXHJcblx0XHRwLnJlbW92ZUV2ZW50TGlzdGVuZXIgPSBmdW5jdGlvbih0eXBlLCBjYWxsYmFjaykge1xyXG5cdFx0XHR2YXIgbGlzdCA9IHRoaXMuX2xpc3RlbmVyc1t0eXBlXSwgaTtcclxuXHRcdFx0aWYgKGxpc3QpIHtcclxuXHRcdFx0XHRpID0gbGlzdC5sZW5ndGg7XHJcblx0XHRcdFx0d2hpbGUgKC0taSA+IC0xKSB7XHJcblx0XHRcdFx0XHRpZiAobGlzdFtpXS5jID09PSBjYWxsYmFjaykge1xyXG5cdFx0XHRcdFx0XHRsaXN0LnNwbGljZShpLCAxKTtcclxuXHRcdFx0XHRcdFx0cmV0dXJuO1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cdFx0fTtcclxuXHJcblx0XHRwLmRpc3BhdGNoRXZlbnQgPSBmdW5jdGlvbih0eXBlKSB7XHJcblx0XHRcdHZhciBsaXN0ID0gdGhpcy5fbGlzdGVuZXJzW3R5cGVdLFxyXG5cdFx0XHRcdGksIHQsIGxpc3RlbmVyO1xyXG5cdFx0XHRpZiAobGlzdCkge1xyXG5cdFx0XHRcdGkgPSBsaXN0Lmxlbmd0aDtcclxuXHRcdFx0XHRpZiAoaSA+IDEpIHsgXHJcblx0XHRcdFx0XHRsaXN0ID0gbGlzdC5zbGljZSgwKTsgLy9pbiBjYXNlIGFkZEV2ZW50TGlzdGVuZXIoKSBpcyBjYWxsZWQgZnJvbSB3aXRoaW4gYSBsaXN0ZW5lci9jYWxsYmFjayAob3RoZXJ3aXNlIHRoZSBpbmRleCBjb3VsZCBjaGFuZ2UsIHJlc3VsdGluZyBpbiBhIHNraXApXHJcblx0XHRcdFx0fVxyXG5cdFx0XHRcdHQgPSB0aGlzLl9ldmVudFRhcmdldDtcclxuXHRcdFx0XHR3aGlsZSAoLS1pID4gLTEpIHtcclxuXHRcdFx0XHRcdGxpc3RlbmVyID0gbGlzdFtpXTtcclxuXHRcdFx0XHRcdGlmIChsaXN0ZW5lcikge1xyXG5cdFx0XHRcdFx0XHRpZiAobGlzdGVuZXIudXApIHtcclxuXHRcdFx0XHRcdFx0XHRsaXN0ZW5lci5jLmNhbGwobGlzdGVuZXIucyB8fCB0LCB7dHlwZTp0eXBlLCB0YXJnZXQ6dH0pO1xyXG5cdFx0XHRcdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdFx0XHRcdGxpc3RlbmVyLmMuY2FsbChsaXN0ZW5lci5zIHx8IHQpO1xyXG5cdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9XHJcblx0XHR9O1xyXG5cclxuXHJcbi8qXHJcbiAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICogVGlja2VyXHJcbiAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICovXHJcbiBcdFx0dmFyIF9yZXFBbmltRnJhbWUgPSB3aW5kb3cucmVxdWVzdEFuaW1hdGlvbkZyYW1lLFxyXG5cdFx0XHRfY2FuY2VsQW5pbUZyYW1lID0gd2luZG93LmNhbmNlbEFuaW1hdGlvbkZyYW1lLFxyXG5cdFx0XHRfZ2V0VGltZSA9IERhdGUubm93IHx8IGZ1bmN0aW9uKCkge3JldHVybiBuZXcgRGF0ZSgpLmdldFRpbWUoKTt9LFxyXG5cdFx0XHRfbGFzdFVwZGF0ZSA9IF9nZXRUaW1lKCk7XHJcblxyXG5cdFx0Ly9ub3cgdHJ5IHRvIGRldGVybWluZSB0aGUgcmVxdWVzdEFuaW1hdGlvbkZyYW1lIGFuZCBjYW5jZWxBbmltYXRpb25GcmFtZSBmdW5jdGlvbnMgYW5kIGlmIG5vbmUgYXJlIGZvdW5kLCB3ZSdsbCB1c2UgYSBzZXRUaW1lb3V0KCkvY2xlYXJUaW1lb3V0KCkgcG9seWZpbGwuXHJcblx0XHRhID0gW1wibXNcIixcIm1velwiLFwid2Via2l0XCIsXCJvXCJdO1xyXG5cdFx0aSA9IGEubGVuZ3RoO1xyXG5cdFx0d2hpbGUgKC0taSA+IC0xICYmICFfcmVxQW5pbUZyYW1lKSB7XHJcblx0XHRcdF9yZXFBbmltRnJhbWUgPSB3aW5kb3dbYVtpXSArIFwiUmVxdWVzdEFuaW1hdGlvbkZyYW1lXCJdO1xyXG5cdFx0XHRfY2FuY2VsQW5pbUZyYW1lID0gd2luZG93W2FbaV0gKyBcIkNhbmNlbEFuaW1hdGlvbkZyYW1lXCJdIHx8IHdpbmRvd1thW2ldICsgXCJDYW5jZWxSZXF1ZXN0QW5pbWF0aW9uRnJhbWVcIl07XHJcblx0XHR9XHJcblxyXG5cdFx0X2NsYXNzKFwiVGlja2VyXCIsIGZ1bmN0aW9uKGZwcywgdXNlUkFGKSB7XHJcblx0XHRcdHZhciBfc2VsZiA9IHRoaXMsXHJcblx0XHRcdFx0X3N0YXJ0VGltZSA9IF9nZXRUaW1lKCksXHJcblx0XHRcdFx0X3VzZVJBRiA9ICh1c2VSQUYgIT09IGZhbHNlICYmIF9yZXFBbmltRnJhbWUpID8gXCJhdXRvXCIgOiBmYWxzZSxcclxuXHRcdFx0XHRfbGFnVGhyZXNob2xkID0gNTAwLFxyXG5cdFx0XHRcdF9hZGp1c3RlZExhZyA9IDMzLFxyXG5cdFx0XHRcdF90aWNrV29yZCA9IFwidGlja1wiLCAvL2hlbHBzIHJlZHVjZSBnYyBidXJkZW5cclxuXHRcdFx0XHRfZnBzLCBfcmVxLCBfaWQsIF9nYXAsIF9uZXh0VGltZSxcclxuXHRcdFx0XHRfdGljayA9IGZ1bmN0aW9uKG1hbnVhbCkge1xyXG5cdFx0XHRcdFx0dmFyIGVsYXBzZWQgPSBfZ2V0VGltZSgpIC0gX2xhc3RVcGRhdGUsXHJcblx0XHRcdFx0XHRcdG92ZXJsYXAsIGRpc3BhdGNoO1xyXG5cdFx0XHRcdFx0aWYgKGVsYXBzZWQgPiBfbGFnVGhyZXNob2xkKSB7XHJcblx0XHRcdFx0XHRcdF9zdGFydFRpbWUgKz0gZWxhcHNlZCAtIF9hZGp1c3RlZExhZztcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdF9sYXN0VXBkYXRlICs9IGVsYXBzZWQ7XHJcblx0XHRcdFx0XHRfc2VsZi50aW1lID0gKF9sYXN0VXBkYXRlIC0gX3N0YXJ0VGltZSkgLyAxMDAwO1xyXG5cdFx0XHRcdFx0b3ZlcmxhcCA9IF9zZWxmLnRpbWUgLSBfbmV4dFRpbWU7XHJcblx0XHRcdFx0XHRpZiAoIV9mcHMgfHwgb3ZlcmxhcCA+IDAgfHwgbWFudWFsID09PSB0cnVlKSB7XHJcblx0XHRcdFx0XHRcdF9zZWxmLmZyYW1lKys7XHJcblx0XHRcdFx0XHRcdF9uZXh0VGltZSArPSBvdmVybGFwICsgKG92ZXJsYXAgPj0gX2dhcCA/IDAuMDA0IDogX2dhcCAtIG92ZXJsYXApO1xyXG5cdFx0XHRcdFx0XHRkaXNwYXRjaCA9IHRydWU7XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRpZiAobWFudWFsICE9PSB0cnVlKSB7IC8vbWFrZSBzdXJlIHRoZSByZXF1ZXN0IGlzIG1hZGUgYmVmb3JlIHdlIGRpc3BhdGNoIHRoZSBcInRpY2tcIiBldmVudCBzbyB0aGF0IHRpbWluZyBpcyBtYWludGFpbmVkLiBPdGhlcndpc2UsIGlmIHByb2Nlc3NpbmcgdGhlIFwidGlja1wiIHJlcXVpcmVzIGEgYnVuY2ggb2YgdGltZSAobGlrZSAxNW1zKSBhbmQgd2UncmUgdXNpbmcgYSBzZXRUaW1lb3V0KCkgdGhhdCdzIGJhc2VkIG9uIDE2LjdtcywgaXQnZCB0ZWNobmljYWxseSB0YWtlIDMxLjdtcyBiZXR3ZWVuIGZyYW1lcyBvdGhlcndpc2UuXHJcblx0XHRcdFx0XHRcdF9pZCA9IF9yZXEoX3RpY2spO1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0aWYgKGRpc3BhdGNoKSB7XHJcblx0XHRcdFx0XHRcdF9zZWxmLmRpc3BhdGNoRXZlbnQoX3RpY2tXb3JkKTtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHR9O1xyXG5cclxuXHRcdFx0RXZlbnREaXNwYXRjaGVyLmNhbGwoX3NlbGYpO1xyXG5cdFx0XHRfc2VsZi50aW1lID0gX3NlbGYuZnJhbWUgPSAwO1xyXG5cdFx0XHRfc2VsZi50aWNrID0gZnVuY3Rpb24oKSB7XHJcblx0XHRcdFx0X3RpY2sodHJ1ZSk7XHJcblx0XHRcdH07XHJcblxyXG5cdFx0XHRfc2VsZi5sYWdTbW9vdGhpbmcgPSBmdW5jdGlvbih0aHJlc2hvbGQsIGFkanVzdGVkTGFnKSB7XHJcblx0XHRcdFx0aWYgKCFhcmd1bWVudHMubGVuZ3RoKSB7IC8vaWYgbGFnU21vb3RoaW5nKCkgaXMgY2FsbGVkIHdpdGggbm8gYXJndW1lbnRzLCB0cmVhdCBpdCBsaWtlIGEgZ2V0dGVyIHRoYXQgcmV0dXJucyBhIGJvb2xlYW4gaW5kaWNhdGluZyBpZiBpdCdzIGVuYWJsZWQgb3Igbm90LiBUaGlzIGlzIHB1cnBvc2VseSB1bmRvY3VtZW50ZWQgYW5kIGlzIGZvciBpbnRlcm5hbCB1c2UuXHJcblx0XHRcdFx0XHRyZXR1cm4gKF9sYWdUaHJlc2hvbGQgPCAxIC8gX3RpbnlOdW0pO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0XHRfbGFnVGhyZXNob2xkID0gdGhyZXNob2xkIHx8ICgxIC8gX3RpbnlOdW0pOyAvL3plcm8gc2hvdWxkIGJlIGludGVycHJldGVkIGFzIGJhc2ljYWxseSB1bmxpbWl0ZWRcclxuXHRcdFx0XHRfYWRqdXN0ZWRMYWcgPSBNYXRoLm1pbihhZGp1c3RlZExhZywgX2xhZ1RocmVzaG9sZCwgMCk7XHJcblx0XHRcdH07XHJcblxyXG5cdFx0XHRfc2VsZi5zbGVlcCA9IGZ1bmN0aW9uKCkge1xyXG5cdFx0XHRcdGlmIChfaWQgPT0gbnVsbCkge1xyXG5cdFx0XHRcdFx0cmV0dXJuO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0XHRpZiAoIV91c2VSQUYgfHwgIV9jYW5jZWxBbmltRnJhbWUpIHtcclxuXHRcdFx0XHRcdGNsZWFyVGltZW91dChfaWQpO1xyXG5cdFx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0XHRfY2FuY2VsQW5pbUZyYW1lKF9pZCk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHRcdF9yZXEgPSBfZW1wdHlGdW5jO1xyXG5cdFx0XHRcdF9pZCA9IG51bGw7XHJcblx0XHRcdFx0aWYgKF9zZWxmID09PSBfdGlja2VyKSB7XHJcblx0XHRcdFx0XHRfdGlja2VyQWN0aXZlID0gZmFsc2U7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9O1xyXG5cclxuXHRcdFx0X3NlbGYud2FrZSA9IGZ1bmN0aW9uKHNlYW1sZXNzKSB7XHJcblx0XHRcdFx0aWYgKF9pZCAhPT0gbnVsbCkge1xyXG5cdFx0XHRcdFx0X3NlbGYuc2xlZXAoKTtcclxuXHRcdFx0XHR9IGVsc2UgaWYgKHNlYW1sZXNzKSB7XHJcblx0XHRcdFx0XHRfc3RhcnRUaW1lICs9IC1fbGFzdFVwZGF0ZSArIChfbGFzdFVwZGF0ZSA9IF9nZXRUaW1lKCkpO1xyXG5cdFx0XHRcdH0gZWxzZSBpZiAoX3NlbGYuZnJhbWUgPiAxMCkgeyAvL2Rvbid0IHRyaWdnZXIgbGFnU21vb3RoaW5nIGlmIHdlJ3JlIGp1c3Qgd2FraW5nIHVwLCBhbmQgbWFrZSBzdXJlIHRoYXQgYXQgbGVhc3QgMTAgZnJhbWVzIGhhdmUgZWxhcHNlZCBiZWNhdXNlIG9mIHRoZSBpT1MgYnVnIHRoYXQgd2Ugd29yayBhcm91bmQgYmVsb3cgd2l0aCB0aGUgMS41LXNlY29uZCBzZXRUaW1vdXQoKS5cclxuXHRcdFx0XHRcdF9sYXN0VXBkYXRlID0gX2dldFRpbWUoKSAtIF9sYWdUaHJlc2hvbGQgKyA1O1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0XHRfcmVxID0gKF9mcHMgPT09IDApID8gX2VtcHR5RnVuYyA6ICghX3VzZVJBRiB8fCAhX3JlcUFuaW1GcmFtZSkgPyBmdW5jdGlvbihmKSB7IHJldHVybiBzZXRUaW1lb3V0KGYsICgoX25leHRUaW1lIC0gX3NlbGYudGltZSkgKiAxMDAwICsgMSkgfCAwKTsgfSA6IF9yZXFBbmltRnJhbWU7XHJcblx0XHRcdFx0aWYgKF9zZWxmID09PSBfdGlja2VyKSB7XHJcblx0XHRcdFx0XHRfdGlja2VyQWN0aXZlID0gdHJ1ZTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0X3RpY2soMik7XHJcblx0XHRcdH07XHJcblxyXG5cdFx0XHRfc2VsZi5mcHMgPSBmdW5jdGlvbih2YWx1ZSkge1xyXG5cdFx0XHRcdGlmICghYXJndW1lbnRzLmxlbmd0aCkge1xyXG5cdFx0XHRcdFx0cmV0dXJuIF9mcHM7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHRcdF9mcHMgPSB2YWx1ZTtcclxuXHRcdFx0XHRfZ2FwID0gMSAvIChfZnBzIHx8IDYwKTtcclxuXHRcdFx0XHRfbmV4dFRpbWUgPSB0aGlzLnRpbWUgKyBfZ2FwO1xyXG5cdFx0XHRcdF9zZWxmLndha2UoKTtcclxuXHRcdFx0fTtcclxuXHJcblx0XHRcdF9zZWxmLnVzZVJBRiA9IGZ1bmN0aW9uKHZhbHVlKSB7XHJcblx0XHRcdFx0aWYgKCFhcmd1bWVudHMubGVuZ3RoKSB7XHJcblx0XHRcdFx0XHRyZXR1cm4gX3VzZVJBRjtcclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0X3NlbGYuc2xlZXAoKTtcclxuXHRcdFx0XHRfdXNlUkFGID0gdmFsdWU7XHJcblx0XHRcdFx0X3NlbGYuZnBzKF9mcHMpO1xyXG5cdFx0XHR9O1xyXG5cdFx0XHRfc2VsZi5mcHMoZnBzKTtcclxuXHJcblx0XHRcdC8vYSBidWcgaW4gaU9TIDYgU2FmYXJpIG9jY2FzaW9uYWxseSBwcmV2ZW50cyB0aGUgcmVxdWVzdEFuaW1hdGlvbkZyYW1lIGZyb20gd29ya2luZyBpbml0aWFsbHksIHNvIHdlIHVzZSBhIDEuNS1zZWNvbmQgdGltZW91dCB0aGF0IGF1dG9tYXRpY2FsbHkgZmFsbHMgYmFjayB0byBzZXRUaW1lb3V0KCkgaWYgaXQgc2Vuc2VzIHRoaXMgY29uZGl0aW9uLlxyXG5cdFx0XHRzZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xyXG5cdFx0XHRcdGlmIChfdXNlUkFGID09PSBcImF1dG9cIiAmJiBfc2VsZi5mcmFtZSA8IDUgJiYgX2RvYy52aXNpYmlsaXR5U3RhdGUgIT09IFwiaGlkZGVuXCIpIHtcclxuXHRcdFx0XHRcdF9zZWxmLnVzZVJBRihmYWxzZSk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9LCAxNTAwKTtcclxuXHRcdH0pO1xyXG5cclxuXHRcdHAgPSBncy5UaWNrZXIucHJvdG90eXBlID0gbmV3IGdzLmV2ZW50cy5FdmVudERpc3BhdGNoZXIoKTtcclxuXHRcdHAuY29uc3RydWN0b3IgPSBncy5UaWNrZXI7XHJcblxyXG5cclxuLypcclxuICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gKiBBbmltYXRpb25cclxuICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gKi9cclxuXHRcdHZhciBBbmltYXRpb24gPSBfY2xhc3MoXCJjb3JlLkFuaW1hdGlvblwiLCBmdW5jdGlvbihkdXJhdGlvbiwgdmFycykge1xyXG5cdFx0XHRcdHRoaXMudmFycyA9IHZhcnMgPSB2YXJzIHx8IHt9O1xyXG5cdFx0XHRcdHRoaXMuX2R1cmF0aW9uID0gdGhpcy5fdG90YWxEdXJhdGlvbiA9IGR1cmF0aW9uIHx8IDA7XHJcblx0XHRcdFx0dGhpcy5fZGVsYXkgPSBOdW1iZXIodmFycy5kZWxheSkgfHwgMDtcclxuXHRcdFx0XHR0aGlzLl90aW1lU2NhbGUgPSAxO1xyXG5cdFx0XHRcdHRoaXMuX2FjdGl2ZSA9ICh2YXJzLmltbWVkaWF0ZVJlbmRlciA9PT0gdHJ1ZSk7XHJcblx0XHRcdFx0dGhpcy5kYXRhID0gdmFycy5kYXRhO1xyXG5cdFx0XHRcdHRoaXMuX3JldmVyc2VkID0gKHZhcnMucmV2ZXJzZWQgPT09IHRydWUpO1xyXG5cclxuXHRcdFx0XHRpZiAoIV9yb290VGltZWxpbmUpIHtcclxuXHRcdFx0XHRcdHJldHVybjtcclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0aWYgKCFfdGlja2VyQWN0aXZlKSB7IC8vc29tZSBicm93c2VycyAobGlrZSBpT1MgNiBTYWZhcmkpIHNodXQgZG93biBKYXZhU2NyaXB0IGV4ZWN1dGlvbiB3aGVuIHRoZSB0YWIgaXMgZGlzYWJsZWQgYW5kIHRoZXkgW29jY2FzaW9uYWxseV0gbmVnbGVjdCB0byBzdGFydCB1cCByZXF1ZXN0QW5pbWF0aW9uRnJhbWUgYWdhaW4gd2hlbiByZXR1cm5pbmcgLSB0aGlzIGNvZGUgZW5zdXJlcyB0aGF0IHRoZSBlbmdpbmUgc3RhcnRzIHVwIGFnYWluIHByb3Blcmx5LlxyXG5cdFx0XHRcdFx0X3RpY2tlci53YWtlKCk7XHJcblx0XHRcdFx0fVxyXG5cclxuXHRcdFx0XHR2YXIgdGwgPSB0aGlzLnZhcnMudXNlRnJhbWVzID8gX3Jvb3RGcmFtZXNUaW1lbGluZSA6IF9yb290VGltZWxpbmU7XHJcblx0XHRcdFx0dGwuYWRkKHRoaXMsIHRsLl90aW1lKTtcclxuXHJcblx0XHRcdFx0aWYgKHRoaXMudmFycy5wYXVzZWQpIHtcclxuXHRcdFx0XHRcdHRoaXMucGF1c2VkKHRydWUpO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fSk7XHJcblxyXG5cdFx0X3RpY2tlciA9IEFuaW1hdGlvbi50aWNrZXIgPSBuZXcgZ3MuVGlja2VyKCk7XHJcblx0XHRwID0gQW5pbWF0aW9uLnByb3RvdHlwZTtcclxuXHRcdHAuX2RpcnR5ID0gcC5fZ2MgPSBwLl9pbml0dGVkID0gcC5fcGF1c2VkID0gZmFsc2U7XHJcblx0XHRwLl90b3RhbFRpbWUgPSBwLl90aW1lID0gMDtcclxuXHRcdHAuX3Jhd1ByZXZUaW1lID0gLTE7XHJcblx0XHRwLl9uZXh0ID0gcC5fbGFzdCA9IHAuX29uVXBkYXRlID0gcC5fdGltZWxpbmUgPSBwLnRpbWVsaW5lID0gbnVsbDtcclxuXHRcdHAuX3BhdXNlZCA9IGZhbHNlO1xyXG5cclxuXHJcblx0XHQvL3NvbWUgYnJvd3NlcnMgKGxpa2UgaU9TKSBvY2Nhc2lvbmFsbHkgZHJvcCB0aGUgcmVxdWVzdEFuaW1hdGlvbkZyYW1lIGV2ZW50IHdoZW4gdGhlIHVzZXIgc3dpdGNoZXMgdG8gYSBkaWZmZXJlbnQgdGFiIGFuZCB0aGVuIGNvbWVzIGJhY2sgYWdhaW4sIHNvIHdlIHVzZSBhIDItc2Vjb25kIHNldFRpbWVvdXQoKSB0byBzZW5zZSBpZi93aGVuIHRoYXQgY29uZGl0aW9uIG9jY3VycyBhbmQgdGhlbiB3YWtlKCkgdGhlIHRpY2tlci5cclxuXHRcdHZhciBfY2hlY2tUaW1lb3V0ID0gZnVuY3Rpb24oKSB7XHJcblx0XHRcdFx0aWYgKF90aWNrZXJBY3RpdmUgJiYgX2dldFRpbWUoKSAtIF9sYXN0VXBkYXRlID4gMjAwMCAmJiAoX2RvYy52aXNpYmlsaXR5U3RhdGUgIT09IFwiaGlkZGVuXCIgfHwgIV90aWNrZXIubGFnU21vb3RoaW5nKCkpKSB7IC8vbm90ZTogaWYgdGhlIHRhYiBpcyBoaWRkZW4sIHdlIHNob3VsZCBzdGlsbCB3YWtlIGlmIGxhZ1Ntb290aGluZyBoYXMgYmVlbiBkaXNhYmxlZC5cclxuXHRcdFx0XHRcdF90aWNrZXIud2FrZSgpO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0XHR2YXIgdCA9IHNldFRpbWVvdXQoX2NoZWNrVGltZW91dCwgMjAwMCk7XHJcblx0XHRcdFx0aWYgKHQudW5yZWYpIHtcclxuXHRcdFx0XHRcdC8vIGFsbG93cyBhIG5vZGUgcHJvY2VzcyB0byBleGl0IGV2ZW4gaWYgdGhlIHRpbWVvdXTigJlzIGNhbGxiYWNrIGhhc24ndCBiZWVuIGludm9rZWQuIFdpdGhvdXQgaXQsIHRoZSBub2RlIHByb2Nlc3MgY291bGQgaGFuZyBhcyB0aGlzIGZ1bmN0aW9uIGlzIGNhbGxlZCBldmVyeSB0d28gc2Vjb25kcy5cclxuXHRcdFx0XHRcdHQudW5yZWYoKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH07XHJcblx0XHRfY2hlY2tUaW1lb3V0KCk7XHJcblxyXG5cclxuXHRcdHAucGxheSA9IGZ1bmN0aW9uKGZyb20sIHN1cHByZXNzRXZlbnRzKSB7XHJcblx0XHRcdGlmIChmcm9tICE9IG51bGwpIHtcclxuXHRcdFx0XHR0aGlzLnNlZWsoZnJvbSwgc3VwcHJlc3NFdmVudHMpO1xyXG5cdFx0XHR9XHJcblx0XHRcdHJldHVybiB0aGlzLnJldmVyc2VkKGZhbHNlKS5wYXVzZWQoZmFsc2UpO1xyXG5cdFx0fTtcclxuXHJcblx0XHRwLnBhdXNlID0gZnVuY3Rpb24oYXRUaW1lLCBzdXBwcmVzc0V2ZW50cykge1xyXG5cdFx0XHRpZiAoYXRUaW1lICE9IG51bGwpIHtcclxuXHRcdFx0XHR0aGlzLnNlZWsoYXRUaW1lLCBzdXBwcmVzc0V2ZW50cyk7XHJcblx0XHRcdH1cclxuXHRcdFx0cmV0dXJuIHRoaXMucGF1c2VkKHRydWUpO1xyXG5cdFx0fTtcclxuXHJcblx0XHRwLnJlc3VtZSA9IGZ1bmN0aW9uKGZyb20sIHN1cHByZXNzRXZlbnRzKSB7XHJcblx0XHRcdGlmIChmcm9tICE9IG51bGwpIHtcclxuXHRcdFx0XHR0aGlzLnNlZWsoZnJvbSwgc3VwcHJlc3NFdmVudHMpO1xyXG5cdFx0XHR9XHJcblx0XHRcdHJldHVybiB0aGlzLnBhdXNlZChmYWxzZSk7XHJcblx0XHR9O1xyXG5cclxuXHRcdHAuc2VlayA9IGZ1bmN0aW9uKHRpbWUsIHN1cHByZXNzRXZlbnRzKSB7XHJcblx0XHRcdHJldHVybiB0aGlzLnRvdGFsVGltZShOdW1iZXIodGltZSksIHN1cHByZXNzRXZlbnRzICE9PSBmYWxzZSk7XHJcblx0XHR9O1xyXG5cclxuXHRcdHAucmVzdGFydCA9IGZ1bmN0aW9uKGluY2x1ZGVEZWxheSwgc3VwcHJlc3NFdmVudHMpIHtcclxuXHRcdFx0cmV0dXJuIHRoaXMucmV2ZXJzZWQoZmFsc2UpLnBhdXNlZChmYWxzZSkudG90YWxUaW1lKGluY2x1ZGVEZWxheSA/IC10aGlzLl9kZWxheSA6IDAsIChzdXBwcmVzc0V2ZW50cyAhPT0gZmFsc2UpLCB0cnVlKTtcclxuXHRcdH07XHJcblxyXG5cdFx0cC5yZXZlcnNlID0gZnVuY3Rpb24oZnJvbSwgc3VwcHJlc3NFdmVudHMpIHtcclxuXHRcdFx0aWYgKGZyb20gIT0gbnVsbCkge1xyXG5cdFx0XHRcdHRoaXMuc2VlaygoZnJvbSB8fCB0aGlzLnRvdGFsRHVyYXRpb24oKSksIHN1cHByZXNzRXZlbnRzKTtcclxuXHRcdFx0fVxyXG5cdFx0XHRyZXR1cm4gdGhpcy5yZXZlcnNlZCh0cnVlKS5wYXVzZWQoZmFsc2UpO1xyXG5cdFx0fTtcclxuXHJcblx0XHRwLnJlbmRlciA9IGZ1bmN0aW9uKHRpbWUsIHN1cHByZXNzRXZlbnRzLCBmb3JjZSkge1xyXG5cdFx0XHQvL3N0dWIgLSB3ZSBvdmVycmlkZSB0aGlzIG1ldGhvZCBpbiBzdWJjbGFzc2VzLlxyXG5cdFx0fTtcclxuXHJcblx0XHRwLmludmFsaWRhdGUgPSBmdW5jdGlvbigpIHtcclxuXHRcdFx0dGhpcy5fdGltZSA9IHRoaXMuX3RvdGFsVGltZSA9IDA7XHJcblx0XHRcdHRoaXMuX2luaXR0ZWQgPSB0aGlzLl9nYyA9IGZhbHNlO1xyXG5cdFx0XHR0aGlzLl9yYXdQcmV2VGltZSA9IC0xO1xyXG5cdFx0XHRpZiAodGhpcy5fZ2MgfHwgIXRoaXMudGltZWxpbmUpIHtcclxuXHRcdFx0XHR0aGlzLl9lbmFibGVkKHRydWUpO1xyXG5cdFx0XHR9XHJcblx0XHRcdHJldHVybiB0aGlzO1xyXG5cdFx0fTtcclxuXHJcblx0XHRwLmlzQWN0aXZlID0gZnVuY3Rpb24oKSB7XHJcblx0XHRcdHZhciB0bCA9IHRoaXMuX3RpbWVsaW5lLCAvL3RoZSAyIHJvb3QgdGltZWxpbmVzIHdvbid0IGhhdmUgYSBfdGltZWxpbmU7IHRoZXkncmUgYWx3YXlzIGFjdGl2ZS5cclxuXHRcdFx0XHRzdGFydFRpbWUgPSB0aGlzLl9zdGFydFRpbWUsXHJcblx0XHRcdFx0cmF3VGltZTtcclxuXHRcdFx0cmV0dXJuICghdGwgfHwgKCF0aGlzLl9nYyAmJiAhdGhpcy5fcGF1c2VkICYmIHRsLmlzQWN0aXZlKCkgJiYgKHJhd1RpbWUgPSB0bC5yYXdUaW1lKHRydWUpKSA+PSBzdGFydFRpbWUgJiYgcmF3VGltZSA8IHN0YXJ0VGltZSArIHRoaXMudG90YWxEdXJhdGlvbigpIC8gdGhpcy5fdGltZVNjYWxlIC0gMC4wMDAwMDAxKSk7XHJcblx0XHR9O1xyXG5cclxuXHRcdHAuX2VuYWJsZWQgPSBmdW5jdGlvbiAoZW5hYmxlZCwgaWdub3JlVGltZWxpbmUpIHtcclxuXHRcdFx0aWYgKCFfdGlja2VyQWN0aXZlKSB7XHJcblx0XHRcdFx0X3RpY2tlci53YWtlKCk7XHJcblx0XHRcdH1cclxuXHRcdFx0dGhpcy5fZ2MgPSAhZW5hYmxlZDtcclxuXHRcdFx0dGhpcy5fYWN0aXZlID0gdGhpcy5pc0FjdGl2ZSgpO1xyXG5cdFx0XHRpZiAoaWdub3JlVGltZWxpbmUgIT09IHRydWUpIHtcclxuXHRcdFx0XHRpZiAoZW5hYmxlZCAmJiAhdGhpcy50aW1lbGluZSkge1xyXG5cdFx0XHRcdFx0dGhpcy5fdGltZWxpbmUuYWRkKHRoaXMsIHRoaXMuX3N0YXJ0VGltZSAtIHRoaXMuX2RlbGF5KTtcclxuXHRcdFx0XHR9IGVsc2UgaWYgKCFlbmFibGVkICYmIHRoaXMudGltZWxpbmUpIHtcclxuXHRcdFx0XHRcdHRoaXMuX3RpbWVsaW5lLl9yZW1vdmUodGhpcywgdHJ1ZSk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9XHJcblx0XHRcdHJldHVybiBmYWxzZTtcclxuXHRcdH07XHJcblxyXG5cclxuXHRcdHAuX2tpbGwgPSBmdW5jdGlvbih2YXJzLCB0YXJnZXQpIHtcclxuXHRcdFx0cmV0dXJuIHRoaXMuX2VuYWJsZWQoZmFsc2UsIGZhbHNlKTtcclxuXHRcdH07XHJcblxyXG5cdFx0cC5raWxsID0gZnVuY3Rpb24odmFycywgdGFyZ2V0KSB7XHJcblx0XHRcdHRoaXMuX2tpbGwodmFycywgdGFyZ2V0KTtcclxuXHRcdFx0cmV0dXJuIHRoaXM7XHJcblx0XHR9O1xyXG5cclxuXHRcdHAuX3VuY2FjaGUgPSBmdW5jdGlvbihpbmNsdWRlU2VsZikge1xyXG5cdFx0XHR2YXIgdHdlZW4gPSBpbmNsdWRlU2VsZiA/IHRoaXMgOiB0aGlzLnRpbWVsaW5lO1xyXG5cdFx0XHR3aGlsZSAodHdlZW4pIHtcclxuXHRcdFx0XHR0d2Vlbi5fZGlydHkgPSB0cnVlO1xyXG5cdFx0XHRcdHR3ZWVuID0gdHdlZW4udGltZWxpbmU7XHJcblx0XHRcdH1cclxuXHRcdFx0cmV0dXJuIHRoaXM7XHJcblx0XHR9O1xyXG5cclxuXHRcdHAuX3N3YXBTZWxmSW5QYXJhbXMgPSBmdW5jdGlvbihwYXJhbXMpIHtcclxuXHRcdFx0dmFyIGkgPSBwYXJhbXMubGVuZ3RoLFxyXG5cdFx0XHRcdGNvcHkgPSBwYXJhbXMuY29uY2F0KCk7XHJcblx0XHRcdHdoaWxlICgtLWkgPiAtMSkge1xyXG5cdFx0XHRcdGlmIChwYXJhbXNbaV0gPT09IFwie3NlbGZ9XCIpIHtcclxuXHRcdFx0XHRcdGNvcHlbaV0gPSB0aGlzO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cdFx0XHRyZXR1cm4gY29weTtcclxuXHRcdH07XHJcblxyXG5cdFx0cC5fY2FsbGJhY2sgPSBmdW5jdGlvbih0eXBlKSB7XHJcblx0XHRcdHZhciB2ID0gdGhpcy52YXJzLFxyXG5cdFx0XHRcdGNhbGxiYWNrID0gdlt0eXBlXSxcclxuXHRcdFx0XHRwYXJhbXMgPSB2W3R5cGUgKyBcIlBhcmFtc1wiXSxcclxuXHRcdFx0XHRzY29wZSA9IHZbdHlwZSArIFwiU2NvcGVcIl0gfHwgdi5jYWxsYmFja1Njb3BlIHx8IHRoaXMsXHJcblx0XHRcdFx0bCA9IHBhcmFtcyA/IHBhcmFtcy5sZW5ndGggOiAwO1xyXG5cdFx0XHRzd2l0Y2ggKGwpIHsgLy9zcGVlZCBvcHRpbWl6YXRpb247IGNhbGwoKSBpcyBmYXN0ZXIgdGhhbiBhcHBseSgpIHNvIHVzZSBpdCB3aGVuIHRoZXJlIGFyZSBvbmx5IGEgZmV3IHBhcmFtZXRlcnMgKHdoaWNoIGlzIGJ5IGZhciBtb3N0IGNvbW1vbikuIFByZXZpb3VzbHkgd2Ugc2ltcGx5IGRpZCB2YXIgdiA9IHRoaXMudmFyczsgdlt0eXBlXS5hcHBseSh2W3R5cGUgKyBcIlNjb3BlXCJdIHx8IHYuY2FsbGJhY2tTY29wZSB8fCB0aGlzLCB2W3R5cGUgKyBcIlBhcmFtc1wiXSB8fCBfYmxhbmtBcnJheSk7XHJcblx0XHRcdFx0Y2FzZSAwOiBjYWxsYmFjay5jYWxsKHNjb3BlKTsgYnJlYWs7XHJcblx0XHRcdFx0Y2FzZSAxOiBjYWxsYmFjay5jYWxsKHNjb3BlLCBwYXJhbXNbMF0pOyBicmVhaztcclxuXHRcdFx0XHRjYXNlIDI6IGNhbGxiYWNrLmNhbGwoc2NvcGUsIHBhcmFtc1swXSwgcGFyYW1zWzFdKTsgYnJlYWs7XHJcblx0XHRcdFx0ZGVmYXVsdDogY2FsbGJhY2suYXBwbHkoc2NvcGUsIHBhcmFtcyk7XHJcblx0XHRcdH1cclxuXHRcdH07XHJcblxyXG4vLy0tLS1BbmltYXRpb24gZ2V0dGVycy9zZXR0ZXJzIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG5cdFx0cC5ldmVudENhbGxiYWNrID0gZnVuY3Rpb24odHlwZSwgY2FsbGJhY2ssIHBhcmFtcywgc2NvcGUpIHtcclxuXHRcdFx0aWYgKCh0eXBlIHx8IFwiXCIpLnN1YnN0cigwLDIpID09PSBcIm9uXCIpIHtcclxuXHRcdFx0XHR2YXIgdiA9IHRoaXMudmFycztcclxuXHRcdFx0XHRpZiAoYXJndW1lbnRzLmxlbmd0aCA9PT0gMSkge1xyXG5cdFx0XHRcdFx0cmV0dXJuIHZbdHlwZV07XHJcblx0XHRcdFx0fVxyXG5cdFx0XHRcdGlmIChjYWxsYmFjayA9PSBudWxsKSB7XHJcblx0XHRcdFx0XHRkZWxldGUgdlt0eXBlXTtcclxuXHRcdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdFx0dlt0eXBlXSA9IGNhbGxiYWNrO1xyXG5cdFx0XHRcdFx0dlt0eXBlICsgXCJQYXJhbXNcIl0gPSAoX2lzQXJyYXkocGFyYW1zKSAmJiBwYXJhbXMuam9pbihcIlwiKS5pbmRleE9mKFwie3NlbGZ9XCIpICE9PSAtMSkgPyB0aGlzLl9zd2FwU2VsZkluUGFyYW1zKHBhcmFtcykgOiBwYXJhbXM7XHJcblx0XHRcdFx0XHR2W3R5cGUgKyBcIlNjb3BlXCJdID0gc2NvcGU7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHRcdGlmICh0eXBlID09PSBcIm9uVXBkYXRlXCIpIHtcclxuXHRcdFx0XHRcdHRoaXMuX29uVXBkYXRlID0gY2FsbGJhY2s7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9XHJcblx0XHRcdHJldHVybiB0aGlzO1xyXG5cdFx0fTtcclxuXHJcblx0XHRwLmRlbGF5ID0gZnVuY3Rpb24odmFsdWUpIHtcclxuXHRcdFx0aWYgKCFhcmd1bWVudHMubGVuZ3RoKSB7XHJcblx0XHRcdFx0cmV0dXJuIHRoaXMuX2RlbGF5O1xyXG5cdFx0XHR9XHJcblx0XHRcdGlmICh0aGlzLl90aW1lbGluZS5zbW9vdGhDaGlsZFRpbWluZykge1xyXG5cdFx0XHRcdHRoaXMuc3RhcnRUaW1lKCB0aGlzLl9zdGFydFRpbWUgKyB2YWx1ZSAtIHRoaXMuX2RlbGF5ICk7XHJcblx0XHRcdH1cclxuXHRcdFx0dGhpcy5fZGVsYXkgPSB2YWx1ZTtcclxuXHRcdFx0cmV0dXJuIHRoaXM7XHJcblx0XHR9O1xyXG5cclxuXHRcdHAuZHVyYXRpb24gPSBmdW5jdGlvbih2YWx1ZSkge1xyXG5cdFx0XHRpZiAoIWFyZ3VtZW50cy5sZW5ndGgpIHtcclxuXHRcdFx0XHR0aGlzLl9kaXJ0eSA9IGZhbHNlO1xyXG5cdFx0XHRcdHJldHVybiB0aGlzLl9kdXJhdGlvbjtcclxuXHRcdFx0fVxyXG5cdFx0XHR0aGlzLl9kdXJhdGlvbiA9IHRoaXMuX3RvdGFsRHVyYXRpb24gPSB2YWx1ZTtcclxuXHRcdFx0dGhpcy5fdW5jYWNoZSh0cnVlKTsgLy90cnVlIGluIGNhc2UgaXQncyBhIFR3ZWVuTWF4IG9yIFRpbWVsaW5lTWF4IHRoYXQgaGFzIGEgcmVwZWF0IC0gd2UnbGwgbmVlZCB0byByZWZyZXNoIHRoZSB0b3RhbER1cmF0aW9uLlxyXG5cdFx0XHRpZiAodGhpcy5fdGltZWxpbmUuc21vb3RoQ2hpbGRUaW1pbmcpIGlmICh0aGlzLl90aW1lID4gMCkgaWYgKHRoaXMuX3RpbWUgPCB0aGlzLl9kdXJhdGlvbikgaWYgKHZhbHVlICE9PSAwKSB7XHJcblx0XHRcdFx0dGhpcy50b3RhbFRpbWUodGhpcy5fdG90YWxUaW1lICogKHZhbHVlIC8gdGhpcy5fZHVyYXRpb24pLCB0cnVlKTtcclxuXHRcdFx0fVxyXG5cdFx0XHRyZXR1cm4gdGhpcztcclxuXHRcdH07XHJcblxyXG5cdFx0cC50b3RhbER1cmF0aW9uID0gZnVuY3Rpb24odmFsdWUpIHtcclxuXHRcdFx0dGhpcy5fZGlydHkgPSBmYWxzZTtcclxuXHRcdFx0cmV0dXJuICghYXJndW1lbnRzLmxlbmd0aCkgPyB0aGlzLl90b3RhbER1cmF0aW9uIDogdGhpcy5kdXJhdGlvbih2YWx1ZSk7XHJcblx0XHR9O1xyXG5cclxuXHRcdHAudGltZSA9IGZ1bmN0aW9uKHZhbHVlLCBzdXBwcmVzc0V2ZW50cykge1xyXG5cdFx0XHRpZiAoIWFyZ3VtZW50cy5sZW5ndGgpIHtcclxuXHRcdFx0XHRyZXR1cm4gdGhpcy5fdGltZTtcclxuXHRcdFx0fVxyXG5cdFx0XHRpZiAodGhpcy5fZGlydHkpIHtcclxuXHRcdFx0XHR0aGlzLnRvdGFsRHVyYXRpb24oKTtcclxuXHRcdFx0fVxyXG5cdFx0XHRyZXR1cm4gdGhpcy50b3RhbFRpbWUoKHZhbHVlID4gdGhpcy5fZHVyYXRpb24pID8gdGhpcy5fZHVyYXRpb24gOiB2YWx1ZSwgc3VwcHJlc3NFdmVudHMpO1xyXG5cdFx0fTtcclxuXHJcblx0XHRwLnRvdGFsVGltZSA9IGZ1bmN0aW9uKHRpbWUsIHN1cHByZXNzRXZlbnRzLCB1bmNhcHBlZCkge1xyXG5cdFx0XHRpZiAoIV90aWNrZXJBY3RpdmUpIHtcclxuXHRcdFx0XHRfdGlja2VyLndha2UoKTtcclxuXHRcdFx0fVxyXG5cdFx0XHRpZiAoIWFyZ3VtZW50cy5sZW5ndGgpIHtcclxuXHRcdFx0XHRyZXR1cm4gdGhpcy5fdG90YWxUaW1lO1xyXG5cdFx0XHR9XHJcblx0XHRcdGlmICh0aGlzLl90aW1lbGluZSkge1xyXG5cdFx0XHRcdGlmICh0aW1lIDwgMCAmJiAhdW5jYXBwZWQpIHtcclxuXHRcdFx0XHRcdHRpbWUgKz0gdGhpcy50b3RhbER1cmF0aW9uKCk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHRcdGlmICh0aGlzLl90aW1lbGluZS5zbW9vdGhDaGlsZFRpbWluZykge1xyXG5cdFx0XHRcdFx0aWYgKHRoaXMuX2RpcnR5KSB7XHJcblx0XHRcdFx0XHRcdHRoaXMudG90YWxEdXJhdGlvbigpO1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0dmFyIHRvdGFsRHVyYXRpb24gPSB0aGlzLl90b3RhbER1cmF0aW9uLFxyXG5cdFx0XHRcdFx0XHR0bCA9IHRoaXMuX3RpbWVsaW5lO1xyXG5cdFx0XHRcdFx0aWYgKHRpbWUgPiB0b3RhbER1cmF0aW9uICYmICF1bmNhcHBlZCkge1xyXG5cdFx0XHRcdFx0XHR0aW1lID0gdG90YWxEdXJhdGlvbjtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdHRoaXMuX3N0YXJ0VGltZSA9ICh0aGlzLl9wYXVzZWQgPyB0aGlzLl9wYXVzZVRpbWUgOiB0bC5fdGltZSkgLSAoKCF0aGlzLl9yZXZlcnNlZCA/IHRpbWUgOiB0b3RhbER1cmF0aW9uIC0gdGltZSkgLyB0aGlzLl90aW1lU2NhbGUpO1xyXG5cdFx0XHRcdFx0aWYgKCF0bC5fZGlydHkpIHsgLy9mb3IgcGVyZm9ybWFuY2UgaW1wcm92ZW1lbnQuIElmIHRoZSBwYXJlbnQncyBjYWNoZSBpcyBhbHJlYWR5IGRpcnR5LCBpdCBhbHJlYWR5IHRvb2sgY2FyZSBvZiBtYXJraW5nIHRoZSBhbmNlc3RvcnMgYXMgZGlydHkgdG9vLCBzbyBza2lwIHRoZSBmdW5jdGlvbiBjYWxsIGhlcmUuXHJcblx0XHRcdFx0XHRcdHRoaXMuX3VuY2FjaGUoZmFsc2UpO1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0Ly9pbiBjYXNlIGFueSBvZiB0aGUgYW5jZXN0b3IgdGltZWxpbmVzIGhhZCBjb21wbGV0ZWQgYnV0IHNob3VsZCBub3cgYmUgZW5hYmxlZCwgd2Ugc2hvdWxkIHJlc2V0IHRoZWlyIHRvdGFsVGltZSgpIHdoaWNoIHdpbGwgYWxzbyBlbnN1cmUgdGhhdCB0aGV5J3JlIGxpbmVkIHVwIHByb3Blcmx5IGFuZCBlbmFibGVkLiBTa2lwIGZvciBhbmltYXRpb25zIHRoYXQgYXJlIG9uIHRoZSByb290ICh3YXN0ZWZ1bCkuIEV4YW1wbGU6IGEgVGltZWxpbmVMaXRlLmV4cG9ydFJvb3QoKSBpcyBwZXJmb3JtZWQgd2hlbiB0aGVyZSdzIGEgcGF1c2VkIHR3ZWVuIG9uIHRoZSByb290LCB0aGUgZXhwb3J0IHdpbGwgbm90IGNvbXBsZXRlIHVudGlsIHRoYXQgdHdlZW4gaXMgdW5wYXVzZWQsIGJ1dCBpbWFnaW5lIGEgY2hpbGQgZ2V0cyByZXN0YXJ0ZWQgbGF0ZXIsIGFmdGVyIGFsbCBbdW5wYXVzZWRdIHR3ZWVucyBoYXZlIGNvbXBsZXRlZC4gVGhlIHN0YXJ0VGltZSBvZiB0aGF0IGNoaWxkIHdvdWxkIGdldCBwdXNoZWQgb3V0LCBidXQgb25lIG9mIHRoZSBhbmNlc3RvcnMgbWF5IGhhdmUgY29tcGxldGVkLlxyXG5cdFx0XHRcdFx0aWYgKHRsLl90aW1lbGluZSkge1xyXG5cdFx0XHRcdFx0XHR3aGlsZSAodGwuX3RpbWVsaW5lKSB7XHJcblx0XHRcdFx0XHRcdFx0aWYgKHRsLl90aW1lbGluZS5fdGltZSAhPT0gKHRsLl9zdGFydFRpbWUgKyB0bC5fdG90YWxUaW1lKSAvIHRsLl90aW1lU2NhbGUpIHtcclxuXHRcdFx0XHRcdFx0XHRcdHRsLnRvdGFsVGltZSh0bC5fdG90YWxUaW1lLCB0cnVlKTtcclxuXHRcdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRcdFx0dGwgPSB0bC5fdGltZWxpbmU7XHJcblx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0aWYgKHRoaXMuX2djKSB7XHJcblx0XHRcdFx0XHR0aGlzLl9lbmFibGVkKHRydWUsIGZhbHNlKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0aWYgKHRoaXMuX3RvdGFsVGltZSAhPT0gdGltZSB8fCB0aGlzLl9kdXJhdGlvbiA9PT0gMCkge1xyXG5cdFx0XHRcdFx0aWYgKF9sYXp5VHdlZW5zLmxlbmd0aCkge1xyXG5cdFx0XHRcdFx0XHRfbGF6eVJlbmRlcigpO1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0dGhpcy5yZW5kZXIodGltZSwgc3VwcHJlc3NFdmVudHMsIGZhbHNlKTtcclxuXHRcdFx0XHRcdGlmIChfbGF6eVR3ZWVucy5sZW5ndGgpIHsgLy9pbiBjYXNlIHJlbmRlcmluZyBjYXVzZWQgYW55IHR3ZWVucyB0byBsYXp5LWluaXQsIHdlIHNob3VsZCByZW5kZXIgdGhlbSBiZWNhdXNlIHR5cGljYWxseSB3aGVuIHNvbWVvbmUgY2FsbHMgc2VlaygpIG9yIHRpbWUoKSBvciBwcm9ncmVzcygpLCB0aGV5IGV4cGVjdCBhbiBpbW1lZGlhdGUgcmVuZGVyLlxyXG5cdFx0XHRcdFx0XHRfbGF6eVJlbmRlcigpO1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cdFx0XHRyZXR1cm4gdGhpcztcclxuXHRcdH07XHJcblxyXG5cdFx0cC5wcm9ncmVzcyA9IHAudG90YWxQcm9ncmVzcyA9IGZ1bmN0aW9uKHZhbHVlLCBzdXBwcmVzc0V2ZW50cykge1xyXG5cdFx0XHR2YXIgZHVyYXRpb24gPSB0aGlzLmR1cmF0aW9uKCk7XHJcblx0XHRcdHJldHVybiAoIWFyZ3VtZW50cy5sZW5ndGgpID8gKGR1cmF0aW9uID8gdGhpcy5fdGltZSAvIGR1cmF0aW9uIDogdGhpcy5yYXRpbykgOiB0aGlzLnRvdGFsVGltZShkdXJhdGlvbiAqIHZhbHVlLCBzdXBwcmVzc0V2ZW50cyk7XHJcblx0XHR9O1xyXG5cclxuXHRcdHAuc3RhcnRUaW1lID0gZnVuY3Rpb24odmFsdWUpIHtcclxuXHRcdFx0aWYgKCFhcmd1bWVudHMubGVuZ3RoKSB7XHJcblx0XHRcdFx0cmV0dXJuIHRoaXMuX3N0YXJ0VGltZTtcclxuXHRcdFx0fVxyXG5cdFx0XHRpZiAodmFsdWUgIT09IHRoaXMuX3N0YXJ0VGltZSkge1xyXG5cdFx0XHRcdHRoaXMuX3N0YXJ0VGltZSA9IHZhbHVlO1xyXG5cdFx0XHRcdGlmICh0aGlzLnRpbWVsaW5lKSBpZiAodGhpcy50aW1lbGluZS5fc29ydENoaWxkcmVuKSB7XHJcblx0XHRcdFx0XHR0aGlzLnRpbWVsaW5lLmFkZCh0aGlzLCB2YWx1ZSAtIHRoaXMuX2RlbGF5KTsgLy9lbnN1cmVzIHRoYXQgYW55IG5lY2Vzc2FyeSByZS1zZXF1ZW5jaW5nIG9mIEFuaW1hdGlvbnMgaW4gdGhlIHRpbWVsaW5lIG9jY3VycyB0byBtYWtlIHN1cmUgdGhlIHJlbmRlcmluZyBvcmRlciBpcyBjb3JyZWN0LlxyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cdFx0XHRyZXR1cm4gdGhpcztcclxuXHRcdH07XHJcblxyXG5cdFx0cC5lbmRUaW1lID0gZnVuY3Rpb24oaW5jbHVkZVJlcGVhdHMpIHtcclxuXHRcdFx0cmV0dXJuIHRoaXMuX3N0YXJ0VGltZSArICgoaW5jbHVkZVJlcGVhdHMgIT0gZmFsc2UpID8gdGhpcy50b3RhbER1cmF0aW9uKCkgOiB0aGlzLmR1cmF0aW9uKCkpIC8gdGhpcy5fdGltZVNjYWxlO1xyXG5cdFx0fTtcclxuXHJcblx0XHRwLnRpbWVTY2FsZSA9IGZ1bmN0aW9uKHZhbHVlKSB7XHJcblx0XHRcdGlmICghYXJndW1lbnRzLmxlbmd0aCkge1xyXG5cdFx0XHRcdHJldHVybiB0aGlzLl90aW1lU2NhbGU7XHJcblx0XHRcdH1cclxuXHRcdFx0dmFyIHBhdXNlVGltZSwgdDtcclxuXHRcdFx0dmFsdWUgPSB2YWx1ZSB8fCBfdGlueU51bTsgLy9jYW4ndCBhbGxvdyB6ZXJvIGJlY2F1c2UgaXQnbGwgdGhyb3cgdGhlIG1hdGggb2ZmXHJcblx0XHRcdGlmICh0aGlzLl90aW1lbGluZSAmJiB0aGlzLl90aW1lbGluZS5zbW9vdGhDaGlsZFRpbWluZykge1xyXG5cdFx0XHRcdHBhdXNlVGltZSA9IHRoaXMuX3BhdXNlVGltZTtcclxuXHRcdFx0XHR0ID0gKHBhdXNlVGltZSB8fCBwYXVzZVRpbWUgPT09IDApID8gcGF1c2VUaW1lIDogdGhpcy5fdGltZWxpbmUudG90YWxUaW1lKCk7XHJcblx0XHRcdFx0dGhpcy5fc3RhcnRUaW1lID0gdCAtICgodCAtIHRoaXMuX3N0YXJ0VGltZSkgKiB0aGlzLl90aW1lU2NhbGUgLyB2YWx1ZSk7XHJcblx0XHRcdH1cclxuXHRcdFx0dGhpcy5fdGltZVNjYWxlID0gdmFsdWU7XHJcblx0XHRcdHQgPSB0aGlzLnRpbWVsaW5lO1xyXG5cdFx0XHR3aGlsZSAodCAmJiB0LnRpbWVsaW5lKSB7IC8vbXVzdCB1cGRhdGUgdGhlIGR1cmF0aW9uL3RvdGFsRHVyYXRpb24gb2YgYWxsIGFuY2VzdG9yIHRpbWVsaW5lcyBpbW1lZGlhdGVseSBpbiBjYXNlIGluIHRoZSBtaWRkbGUgb2YgYSByZW5kZXIgbG9vcCwgb25lIHR3ZWVuIGFsdGVycyBhbm90aGVyIHR3ZWVuJ3MgdGltZVNjYWxlIHdoaWNoIHNob3ZlcyBpdHMgc3RhcnRUaW1lIGJlZm9yZSAwLCBmb3JjaW5nIHRoZSBwYXJlbnQgdGltZWxpbmUgdG8gc2hpZnQgYXJvdW5kIGFuZCBzaGlmdENoaWxkcmVuKCkgd2hpY2ggY291bGQgYWZmZWN0IHRoYXQgbmV4dCB0d2VlbidzIHJlbmRlciAoc3RhcnRUaW1lKS4gRG9lc24ndCBtYXR0ZXIgZm9yIHRoZSByb290IHRpbWVsaW5lIHRob3VnaC5cclxuXHRcdFx0XHR0Ll9kaXJ0eSA9IHRydWU7XHJcblx0XHRcdFx0dC50b3RhbER1cmF0aW9uKCk7XHJcblx0XHRcdFx0dCA9IHQudGltZWxpbmU7XHJcblx0XHRcdH1cclxuXHRcdFx0cmV0dXJuIHRoaXM7XHJcblx0XHR9O1xyXG5cclxuXHRcdHAucmV2ZXJzZWQgPSBmdW5jdGlvbih2YWx1ZSkge1xyXG5cdFx0XHRpZiAoIWFyZ3VtZW50cy5sZW5ndGgpIHtcclxuXHRcdFx0XHRyZXR1cm4gdGhpcy5fcmV2ZXJzZWQ7XHJcblx0XHRcdH1cclxuXHRcdFx0aWYgKHZhbHVlICE9IHRoaXMuX3JldmVyc2VkKSB7XHJcblx0XHRcdFx0dGhpcy5fcmV2ZXJzZWQgPSB2YWx1ZTtcclxuXHRcdFx0XHR0aGlzLnRvdGFsVGltZSgoKHRoaXMuX3RpbWVsaW5lICYmICF0aGlzLl90aW1lbGluZS5zbW9vdGhDaGlsZFRpbWluZykgPyB0aGlzLnRvdGFsRHVyYXRpb24oKSAtIHRoaXMuX3RvdGFsVGltZSA6IHRoaXMuX3RvdGFsVGltZSksIHRydWUpO1xyXG5cdFx0XHR9XHJcblx0XHRcdHJldHVybiB0aGlzO1xyXG5cdFx0fTtcclxuXHJcblx0XHRwLnBhdXNlZCA9IGZ1bmN0aW9uKHZhbHVlKSB7XHJcblx0XHRcdGlmICghYXJndW1lbnRzLmxlbmd0aCkge1xyXG5cdFx0XHRcdHJldHVybiB0aGlzLl9wYXVzZWQ7XHJcblx0XHRcdH1cclxuXHRcdFx0dmFyIHRsID0gdGhpcy5fdGltZWxpbmUsXHJcblx0XHRcdFx0cmF3LCBlbGFwc2VkO1xyXG5cdFx0XHRpZiAodmFsdWUgIT0gdGhpcy5fcGF1c2VkKSBpZiAodGwpIHtcclxuXHRcdFx0XHRpZiAoIV90aWNrZXJBY3RpdmUgJiYgIXZhbHVlKSB7XHJcblx0XHRcdFx0XHRfdGlja2VyLndha2UoKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0cmF3ID0gdGwucmF3VGltZSgpO1xyXG5cdFx0XHRcdGVsYXBzZWQgPSByYXcgLSB0aGlzLl9wYXVzZVRpbWU7XHJcblx0XHRcdFx0aWYgKCF2YWx1ZSAmJiB0bC5zbW9vdGhDaGlsZFRpbWluZykge1xyXG5cdFx0XHRcdFx0dGhpcy5fc3RhcnRUaW1lICs9IGVsYXBzZWQ7XHJcblx0XHRcdFx0XHR0aGlzLl91bmNhY2hlKGZhbHNlKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0dGhpcy5fcGF1c2VUaW1lID0gdmFsdWUgPyByYXcgOiBudWxsO1xyXG5cdFx0XHRcdHRoaXMuX3BhdXNlZCA9IHZhbHVlO1xyXG5cdFx0XHRcdHRoaXMuX2FjdGl2ZSA9IHRoaXMuaXNBY3RpdmUoKTtcclxuXHRcdFx0XHRpZiAoIXZhbHVlICYmIGVsYXBzZWQgIT09IDAgJiYgdGhpcy5faW5pdHRlZCAmJiB0aGlzLmR1cmF0aW9uKCkpIHtcclxuXHRcdFx0XHRcdHJhdyA9IHRsLnNtb290aENoaWxkVGltaW5nID8gdGhpcy5fdG90YWxUaW1lIDogKHJhdyAtIHRoaXMuX3N0YXJ0VGltZSkgLyB0aGlzLl90aW1lU2NhbGU7XHJcblx0XHRcdFx0XHR0aGlzLnJlbmRlcihyYXcsIChyYXcgPT09IHRoaXMuX3RvdGFsVGltZSksIHRydWUpOyAvL2luIGNhc2UgdGhlIHRhcmdldCdzIHByb3BlcnRpZXMgY2hhbmdlZCB2aWEgc29tZSBvdGhlciB0d2VlbiBvciBtYW51YWwgdXBkYXRlIGJ5IHRoZSB1c2VyLCB3ZSBzaG91bGQgZm9yY2UgYSByZW5kZXIuXHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9XHJcblx0XHRcdGlmICh0aGlzLl9nYyAmJiAhdmFsdWUpIHtcclxuXHRcdFx0XHR0aGlzLl9lbmFibGVkKHRydWUsIGZhbHNlKTtcclxuXHRcdFx0fVxyXG5cdFx0XHRyZXR1cm4gdGhpcztcclxuXHRcdH07XHJcblxyXG5cclxuLypcclxuICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gKiBTaW1wbGVUaW1lbGluZVxyXG4gKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAqL1xyXG5cdFx0dmFyIFNpbXBsZVRpbWVsaW5lID0gX2NsYXNzKFwiY29yZS5TaW1wbGVUaW1lbGluZVwiLCBmdW5jdGlvbih2YXJzKSB7XHJcblx0XHRcdEFuaW1hdGlvbi5jYWxsKHRoaXMsIDAsIHZhcnMpO1xyXG5cdFx0XHR0aGlzLmF1dG9SZW1vdmVDaGlsZHJlbiA9IHRoaXMuc21vb3RoQ2hpbGRUaW1pbmcgPSB0cnVlO1xyXG5cdFx0fSk7XHJcblxyXG5cdFx0cCA9IFNpbXBsZVRpbWVsaW5lLnByb3RvdHlwZSA9IG5ldyBBbmltYXRpb24oKTtcclxuXHRcdHAuY29uc3RydWN0b3IgPSBTaW1wbGVUaW1lbGluZTtcclxuXHRcdHAua2lsbCgpLl9nYyA9IGZhbHNlO1xyXG5cdFx0cC5fZmlyc3QgPSBwLl9sYXN0ID0gcC5fcmVjZW50ID0gbnVsbDtcclxuXHRcdHAuX3NvcnRDaGlsZHJlbiA9IGZhbHNlO1xyXG5cclxuXHRcdHAuYWRkID0gcC5pbnNlcnQgPSBmdW5jdGlvbihjaGlsZCwgcG9zaXRpb24sIGFsaWduLCBzdGFnZ2VyKSB7XHJcblx0XHRcdHZhciBwcmV2VHdlZW4sIHN0O1xyXG5cdFx0XHRjaGlsZC5fc3RhcnRUaW1lID0gTnVtYmVyKHBvc2l0aW9uIHx8IDApICsgY2hpbGQuX2RlbGF5O1xyXG5cdFx0XHRpZiAoY2hpbGQuX3BhdXNlZCkgaWYgKHRoaXMgIT09IGNoaWxkLl90aW1lbGluZSkgeyAvL3dlIG9ubHkgYWRqdXN0IHRoZSBfcGF1c2VUaW1lIGlmIGl0IHdhc24ndCBpbiB0aGlzIHRpbWVsaW5lIGFscmVhZHkuIFJlbWVtYmVyLCBzb21ldGltZXMgYSB0d2VlbiB3aWxsIGJlIGluc2VydGVkIGFnYWluIGludG8gdGhlIHNhbWUgdGltZWxpbmUgd2hlbiBpdHMgc3RhcnRUaW1lIGlzIGNoYW5nZWQgc28gdGhhdCB0aGUgdHdlZW5zIGluIHRoZSBUaW1lbGluZUxpdGUvTWF4IGFyZSByZS1vcmRlcmVkIHByb3Blcmx5IGluIHRoZSBsaW5rZWQgbGlzdCAoc28gZXZlcnl0aGluZyByZW5kZXJzIGluIHRoZSBwcm9wZXIgb3JkZXIpLlxyXG5cdFx0XHRcdGNoaWxkLl9wYXVzZVRpbWUgPSBjaGlsZC5fc3RhcnRUaW1lICsgKCh0aGlzLnJhd1RpbWUoKSAtIGNoaWxkLl9zdGFydFRpbWUpIC8gY2hpbGQuX3RpbWVTY2FsZSk7XHJcblx0XHRcdH1cclxuXHRcdFx0aWYgKGNoaWxkLnRpbWVsaW5lKSB7XHJcblx0XHRcdFx0Y2hpbGQudGltZWxpbmUuX3JlbW92ZShjaGlsZCwgdHJ1ZSk7IC8vcmVtb3ZlcyBmcm9tIGV4aXN0aW5nIHRpbWVsaW5lIHNvIHRoYXQgaXQgY2FuIGJlIHByb3Blcmx5IGFkZGVkIHRvIHRoaXMgb25lLlxyXG5cdFx0XHR9XHJcblx0XHRcdGNoaWxkLnRpbWVsaW5lID0gY2hpbGQuX3RpbWVsaW5lID0gdGhpcztcclxuXHRcdFx0aWYgKGNoaWxkLl9nYykge1xyXG5cdFx0XHRcdGNoaWxkLl9lbmFibGVkKHRydWUsIHRydWUpO1xyXG5cdFx0XHR9XHJcblx0XHRcdHByZXZUd2VlbiA9IHRoaXMuX2xhc3Q7XHJcblx0XHRcdGlmICh0aGlzLl9zb3J0Q2hpbGRyZW4pIHtcclxuXHRcdFx0XHRzdCA9IGNoaWxkLl9zdGFydFRpbWU7XHJcblx0XHRcdFx0d2hpbGUgKHByZXZUd2VlbiAmJiBwcmV2VHdlZW4uX3N0YXJ0VGltZSA+IHN0KSB7XHJcblx0XHRcdFx0XHRwcmV2VHdlZW4gPSBwcmV2VHdlZW4uX3ByZXY7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9XHJcblx0XHRcdGlmIChwcmV2VHdlZW4pIHtcclxuXHRcdFx0XHRjaGlsZC5fbmV4dCA9IHByZXZUd2Vlbi5fbmV4dDtcclxuXHRcdFx0XHRwcmV2VHdlZW4uX25leHQgPSBjaGlsZDtcclxuXHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHRjaGlsZC5fbmV4dCA9IHRoaXMuX2ZpcnN0O1xyXG5cdFx0XHRcdHRoaXMuX2ZpcnN0ID0gY2hpbGQ7XHJcblx0XHRcdH1cclxuXHRcdFx0aWYgKGNoaWxkLl9uZXh0KSB7XHJcblx0XHRcdFx0Y2hpbGQuX25leHQuX3ByZXYgPSBjaGlsZDtcclxuXHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHR0aGlzLl9sYXN0ID0gY2hpbGQ7XHJcblx0XHRcdH1cclxuXHRcdFx0Y2hpbGQuX3ByZXYgPSBwcmV2VHdlZW47XHJcblx0XHRcdHRoaXMuX3JlY2VudCA9IGNoaWxkO1xyXG5cdFx0XHRpZiAodGhpcy5fdGltZWxpbmUpIHtcclxuXHRcdFx0XHR0aGlzLl91bmNhY2hlKHRydWUpO1xyXG5cdFx0XHR9XHJcblx0XHRcdHJldHVybiB0aGlzO1xyXG5cdFx0fTtcclxuXHJcblx0XHRwLl9yZW1vdmUgPSBmdW5jdGlvbih0d2Vlbiwgc2tpcERpc2FibGUpIHtcclxuXHRcdFx0aWYgKHR3ZWVuLnRpbWVsaW5lID09PSB0aGlzKSB7XHJcblx0XHRcdFx0aWYgKCFza2lwRGlzYWJsZSkge1xyXG5cdFx0XHRcdFx0dHdlZW4uX2VuYWJsZWQoZmFsc2UsIHRydWUpO1xyXG5cdFx0XHRcdH1cclxuXHJcblx0XHRcdFx0aWYgKHR3ZWVuLl9wcmV2KSB7XHJcblx0XHRcdFx0XHR0d2Vlbi5fcHJldi5fbmV4dCA9IHR3ZWVuLl9uZXh0O1xyXG5cdFx0XHRcdH0gZWxzZSBpZiAodGhpcy5fZmlyc3QgPT09IHR3ZWVuKSB7XHJcblx0XHRcdFx0XHR0aGlzLl9maXJzdCA9IHR3ZWVuLl9uZXh0O1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0XHRpZiAodHdlZW4uX25leHQpIHtcclxuXHRcdFx0XHRcdHR3ZWVuLl9uZXh0Ll9wcmV2ID0gdHdlZW4uX3ByZXY7XHJcblx0XHRcdFx0fSBlbHNlIGlmICh0aGlzLl9sYXN0ID09PSB0d2Vlbikge1xyXG5cdFx0XHRcdFx0dGhpcy5fbGFzdCA9IHR3ZWVuLl9wcmV2O1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0XHR0d2Vlbi5fbmV4dCA9IHR3ZWVuLl9wcmV2ID0gdHdlZW4udGltZWxpbmUgPSBudWxsO1xyXG5cdFx0XHRcdGlmICh0d2VlbiA9PT0gdGhpcy5fcmVjZW50KSB7XHJcblx0XHRcdFx0XHR0aGlzLl9yZWNlbnQgPSB0aGlzLl9sYXN0O1xyXG5cdFx0XHRcdH1cclxuXHJcblx0XHRcdFx0aWYgKHRoaXMuX3RpbWVsaW5lKSB7XHJcblx0XHRcdFx0XHR0aGlzLl91bmNhY2hlKHRydWUpO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cdFx0XHRyZXR1cm4gdGhpcztcclxuXHRcdH07XHJcblxyXG5cdFx0cC5yZW5kZXIgPSBmdW5jdGlvbih0aW1lLCBzdXBwcmVzc0V2ZW50cywgZm9yY2UpIHtcclxuXHRcdFx0dmFyIHR3ZWVuID0gdGhpcy5fZmlyc3QsXHJcblx0XHRcdFx0bmV4dDtcclxuXHRcdFx0dGhpcy5fdG90YWxUaW1lID0gdGhpcy5fdGltZSA9IHRoaXMuX3Jhd1ByZXZUaW1lID0gdGltZTtcclxuXHRcdFx0d2hpbGUgKHR3ZWVuKSB7XHJcblx0XHRcdFx0bmV4dCA9IHR3ZWVuLl9uZXh0OyAvL3JlY29yZCBpdCBoZXJlIGJlY2F1c2UgdGhlIHZhbHVlIGNvdWxkIGNoYW5nZSBhZnRlciByZW5kZXJpbmcuLi5cclxuXHRcdFx0XHRpZiAodHdlZW4uX2FjdGl2ZSB8fCAodGltZSA+PSB0d2Vlbi5fc3RhcnRUaW1lICYmICF0d2Vlbi5fcGF1c2VkICYmICF0d2Vlbi5fZ2MpKSB7XHJcblx0XHRcdFx0XHRpZiAoIXR3ZWVuLl9yZXZlcnNlZCkge1xyXG5cdFx0XHRcdFx0XHR0d2Vlbi5yZW5kZXIoKHRpbWUgLSB0d2Vlbi5fc3RhcnRUaW1lKSAqIHR3ZWVuLl90aW1lU2NhbGUsIHN1cHByZXNzRXZlbnRzLCBmb3JjZSk7XHJcblx0XHRcdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdFx0XHR0d2Vlbi5yZW5kZXIoKCghdHdlZW4uX2RpcnR5KSA/IHR3ZWVuLl90b3RhbER1cmF0aW9uIDogdHdlZW4udG90YWxEdXJhdGlvbigpKSAtICgodGltZSAtIHR3ZWVuLl9zdGFydFRpbWUpICogdHdlZW4uX3RpbWVTY2FsZSksIHN1cHByZXNzRXZlbnRzLCBmb3JjZSk7XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0fVxyXG5cdFx0XHRcdHR3ZWVuID0gbmV4dDtcclxuXHRcdFx0fVxyXG5cdFx0fTtcclxuXHJcblx0XHRwLnJhd1RpbWUgPSBmdW5jdGlvbigpIHtcclxuXHRcdFx0aWYgKCFfdGlja2VyQWN0aXZlKSB7XHJcblx0XHRcdFx0X3RpY2tlci53YWtlKCk7XHJcblx0XHRcdH1cclxuXHRcdFx0cmV0dXJuIHRoaXMuX3RvdGFsVGltZTtcclxuXHRcdH07XHJcblxyXG4vKlxyXG4gKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAqIFR3ZWVuTGl0ZVxyXG4gKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAqL1xyXG5cdFx0dmFyIFR3ZWVuTGl0ZSA9IF9jbGFzcyhcIlR3ZWVuTGl0ZVwiLCBmdW5jdGlvbih0YXJnZXQsIGR1cmF0aW9uLCB2YXJzKSB7XHJcblx0XHRcdFx0QW5pbWF0aW9uLmNhbGwodGhpcywgZHVyYXRpb24sIHZhcnMpO1xyXG5cdFx0XHRcdHRoaXMucmVuZGVyID0gVHdlZW5MaXRlLnByb3RvdHlwZS5yZW5kZXI7IC8vc3BlZWQgb3B0aW1pemF0aW9uIChhdm9pZCBwcm90b3R5cGUgbG9va3VwIG9uIHRoaXMgXCJob3RcIiBtZXRob2QpXHJcblxyXG5cdFx0XHRcdGlmICh0YXJnZXQgPT0gbnVsbCkge1xyXG5cdFx0XHRcdFx0dGhyb3cgXCJDYW5ub3QgdHdlZW4gYSBudWxsIHRhcmdldC5cIjtcclxuXHRcdFx0XHR9XHJcblxyXG5cdFx0XHRcdHRoaXMudGFyZ2V0ID0gdGFyZ2V0ID0gKHR5cGVvZih0YXJnZXQpICE9PSBcInN0cmluZ1wiKSA/IHRhcmdldCA6IFR3ZWVuTGl0ZS5zZWxlY3Rvcih0YXJnZXQpIHx8IHRhcmdldDtcclxuXHJcblx0XHRcdFx0dmFyIGlzU2VsZWN0b3IgPSAodGFyZ2V0LmpxdWVyeSB8fCAodGFyZ2V0Lmxlbmd0aCAmJiB0YXJnZXQgIT09IHdpbmRvdyAmJiB0YXJnZXRbMF0gJiYgKHRhcmdldFswXSA9PT0gd2luZG93IHx8ICh0YXJnZXRbMF0ubm9kZVR5cGUgJiYgdGFyZ2V0WzBdLnN0eWxlICYmICF0YXJnZXQubm9kZVR5cGUpKSkpLFxyXG5cdFx0XHRcdFx0b3ZlcndyaXRlID0gdGhpcy52YXJzLm92ZXJ3cml0ZSxcclxuXHRcdFx0XHRcdGksIHRhcmcsIHRhcmdldHM7XHJcblxyXG5cdFx0XHRcdHRoaXMuX292ZXJ3cml0ZSA9IG92ZXJ3cml0ZSA9IChvdmVyd3JpdGUgPT0gbnVsbCkgPyBfb3ZlcndyaXRlTG9va3VwW1R3ZWVuTGl0ZS5kZWZhdWx0T3ZlcndyaXRlXSA6ICh0eXBlb2Yob3ZlcndyaXRlKSA9PT0gXCJudW1iZXJcIikgPyBvdmVyd3JpdGUgPj4gMCA6IF9vdmVyd3JpdGVMb29rdXBbb3ZlcndyaXRlXTtcclxuXHJcblx0XHRcdFx0aWYgKChpc1NlbGVjdG9yIHx8IHRhcmdldCBpbnN0YW5jZW9mIEFycmF5IHx8ICh0YXJnZXQucHVzaCAmJiBfaXNBcnJheSh0YXJnZXQpKSkgJiYgdHlwZW9mKHRhcmdldFswXSkgIT09IFwibnVtYmVyXCIpIHtcclxuXHRcdFx0XHRcdHRoaXMuX3RhcmdldHMgPSB0YXJnZXRzID0gX3NsaWNlKHRhcmdldCk7ICAvL2Rvbid0IHVzZSBBcnJheS5wcm90b3R5cGUuc2xpY2UuY2FsbCh0YXJnZXQsIDApIGJlY2F1c2UgdGhhdCBkb2Vzbid0IHdvcmsgaW4gSUU4IHdpdGggYSBOb2RlTGlzdCB0aGF0J3MgcmV0dXJuZWQgYnkgcXVlcnlTZWxlY3RvckFsbCgpXHJcblx0XHRcdFx0XHR0aGlzLl9wcm9wTG9va3VwID0gW107XHJcblx0XHRcdFx0XHR0aGlzLl9zaWJsaW5ncyA9IFtdO1xyXG5cdFx0XHRcdFx0Zm9yIChpID0gMDsgaSA8IHRhcmdldHMubGVuZ3RoOyBpKyspIHtcclxuXHRcdFx0XHRcdFx0dGFyZyA9IHRhcmdldHNbaV07XHJcblx0XHRcdFx0XHRcdGlmICghdGFyZykge1xyXG5cdFx0XHRcdFx0XHRcdHRhcmdldHMuc3BsaWNlKGktLSwgMSk7XHJcblx0XHRcdFx0XHRcdFx0Y29udGludWU7XHJcblx0XHRcdFx0XHRcdH0gZWxzZSBpZiAodHlwZW9mKHRhcmcpID09PSBcInN0cmluZ1wiKSB7XHJcblx0XHRcdFx0XHRcdFx0dGFyZyA9IHRhcmdldHNbaS0tXSA9IFR3ZWVuTGl0ZS5zZWxlY3Rvcih0YXJnKTsgLy9pbiBjYXNlIGl0J3MgYW4gYXJyYXkgb2Ygc3RyaW5nc1xyXG5cdFx0XHRcdFx0XHRcdGlmICh0eXBlb2YodGFyZykgPT09IFwic3RyaW5nXCIpIHtcclxuXHRcdFx0XHRcdFx0XHRcdHRhcmdldHMuc3BsaWNlKGkrMSwgMSk7IC8vdG8gYXZvaWQgYW4gZW5kbGVzcyBsb29wIChjYW4ndCBpbWFnaW5lIHdoeSB0aGUgc2VsZWN0b3Igd291bGQgcmV0dXJuIGEgc3RyaW5nLCBidXQganVzdCBpbiBjYXNlKVxyXG5cdFx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdFx0XHRjb250aW51ZTtcclxuXHRcdFx0XHRcdFx0fSBlbHNlIGlmICh0YXJnLmxlbmd0aCAmJiB0YXJnICE9PSB3aW5kb3cgJiYgdGFyZ1swXSAmJiAodGFyZ1swXSA9PT0gd2luZG93IHx8ICh0YXJnWzBdLm5vZGVUeXBlICYmIHRhcmdbMF0uc3R5bGUgJiYgIXRhcmcubm9kZVR5cGUpKSkgeyAvL2luIGNhc2UgdGhlIHVzZXIgaXMgcGFzc2luZyBpbiBhbiBhcnJheSBvZiBzZWxlY3RvciBvYmplY3RzIChsaWtlIGpRdWVyeSBvYmplY3RzKSwgd2UgbmVlZCB0byBjaGVjayBvbmUgbW9yZSBsZXZlbCBhbmQgcHVsbCB0aGluZ3Mgb3V0IGlmIG5lY2Vzc2FyeS4gQWxzbyBub3RlIHRoYXQgPHNlbGVjdD4gZWxlbWVudHMgcGFzcyBhbGwgdGhlIGNyaXRlcmlhIHJlZ2FyZGluZyBsZW5ndGggYW5kIHRoZSBmaXJzdCBjaGlsZCBoYXZpbmcgc3R5bGUsIHNvIHdlIG11c3QgYWxzbyBjaGVjayB0byBlbnN1cmUgdGhlIHRhcmdldCBpc24ndCBhbiBIVE1MIG5vZGUgaXRzZWxmLlxyXG5cdFx0XHRcdFx0XHRcdHRhcmdldHMuc3BsaWNlKGktLSwgMSk7XHJcblx0XHRcdFx0XHRcdFx0dGhpcy5fdGFyZ2V0cyA9IHRhcmdldHMgPSB0YXJnZXRzLmNvbmNhdChfc2xpY2UodGFyZykpO1xyXG5cdFx0XHRcdFx0XHRcdGNvbnRpbnVlO1xyXG5cdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRcdHRoaXMuX3NpYmxpbmdzW2ldID0gX3JlZ2lzdGVyKHRhcmcsIHRoaXMsIGZhbHNlKTtcclxuXHRcdFx0XHRcdFx0aWYgKG92ZXJ3cml0ZSA9PT0gMSkgaWYgKHRoaXMuX3NpYmxpbmdzW2ldLmxlbmd0aCA+IDEpIHtcclxuXHRcdFx0XHRcdFx0XHRfYXBwbHlPdmVyd3JpdGUodGFyZywgdGhpcywgbnVsbCwgMSwgdGhpcy5fc2libGluZ3NbaV0pO1xyXG5cdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHR9XHJcblxyXG5cdFx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0XHR0aGlzLl9wcm9wTG9va3VwID0ge307XHJcblx0XHRcdFx0XHR0aGlzLl9zaWJsaW5ncyA9IF9yZWdpc3Rlcih0YXJnZXQsIHRoaXMsIGZhbHNlKTtcclxuXHRcdFx0XHRcdGlmIChvdmVyd3JpdGUgPT09IDEpIGlmICh0aGlzLl9zaWJsaW5ncy5sZW5ndGggPiAxKSB7XHJcblx0XHRcdFx0XHRcdF9hcHBseU92ZXJ3cml0ZSh0YXJnZXQsIHRoaXMsIG51bGwsIDEsIHRoaXMuX3NpYmxpbmdzKTtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0aWYgKHRoaXMudmFycy5pbW1lZGlhdGVSZW5kZXIgfHwgKGR1cmF0aW9uID09PSAwICYmIHRoaXMuX2RlbGF5ID09PSAwICYmIHRoaXMudmFycy5pbW1lZGlhdGVSZW5kZXIgIT09IGZhbHNlKSkge1xyXG5cdFx0XHRcdFx0dGhpcy5fdGltZSA9IC1fdGlueU51bTsgLy9mb3JjZXMgYSByZW5kZXIgd2l0aG91dCBoYXZpbmcgdG8gc2V0IHRoZSByZW5kZXIoKSBcImZvcmNlXCIgcGFyYW1ldGVyIHRvIHRydWUgYmVjYXVzZSB3ZSB3YW50IHRvIGFsbG93IGxhenlpbmcgYnkgZGVmYXVsdCAodXNpbmcgdGhlIFwiZm9yY2VcIiBwYXJhbWV0ZXIgYWx3YXlzIGZvcmNlcyBhbiBpbW1lZGlhdGUgZnVsbCByZW5kZXIpXHJcblx0XHRcdFx0XHR0aGlzLnJlbmRlcihNYXRoLm1pbigwLCAtdGhpcy5fZGVsYXkpKTsgLy9pbiBjYXNlIGRlbGF5IGlzIG5lZ2F0aXZlXHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9LCB0cnVlKSxcclxuXHRcdFx0X2lzU2VsZWN0b3IgPSBmdW5jdGlvbih2KSB7XHJcblx0XHRcdFx0cmV0dXJuICh2ICYmIHYubGVuZ3RoICYmIHYgIT09IHdpbmRvdyAmJiB2WzBdICYmICh2WzBdID09PSB3aW5kb3cgfHwgKHZbMF0ubm9kZVR5cGUgJiYgdlswXS5zdHlsZSAmJiAhdi5ub2RlVHlwZSkpKTsgLy93ZSBjYW5ub3QgY2hlY2sgXCJub2RlVHlwZVwiIGlmIHRoZSB0YXJnZXQgaXMgd2luZG93IGZyb20gd2l0aGluIGFuIGlmcmFtZSwgb3RoZXJ3aXNlIGl0IHdpbGwgdHJpZ2dlciBhIHNlY3VyaXR5IGVycm9yIGluIHNvbWUgYnJvd3NlcnMgbGlrZSBGaXJlZm94LlxyXG5cdFx0XHR9LFxyXG5cdFx0XHRfYXV0b0NTUyA9IGZ1bmN0aW9uKHZhcnMsIHRhcmdldCkge1xyXG5cdFx0XHRcdHZhciBjc3MgPSB7fSxcclxuXHRcdFx0XHRcdHA7XHJcblx0XHRcdFx0Zm9yIChwIGluIHZhcnMpIHtcclxuXHRcdFx0XHRcdGlmICghX3Jlc2VydmVkUHJvcHNbcF0gJiYgKCEocCBpbiB0YXJnZXQpIHx8IHAgPT09IFwidHJhbnNmb3JtXCIgfHwgcCA9PT0gXCJ4XCIgfHwgcCA9PT0gXCJ5XCIgfHwgcCA9PT0gXCJ3aWR0aFwiIHx8IHAgPT09IFwiaGVpZ2h0XCIgfHwgcCA9PT0gXCJjbGFzc05hbWVcIiB8fCBwID09PSBcImJvcmRlclwiKSAmJiAoIV9wbHVnaW5zW3BdIHx8IChfcGx1Z2luc1twXSAmJiBfcGx1Z2luc1twXS5fYXV0b0NTUykpKSB7IC8vbm90ZTogPGltZz4gZWxlbWVudHMgY29udGFpbiByZWFkLW9ubHkgXCJ4XCIgYW5kIFwieVwiIHByb3BlcnRpZXMuIFdlIHNob3VsZCBhbHNvIHByaW9yaXRpemUgZWRpdGluZyBjc3Mgd2lkdGgvaGVpZ2h0IHJhdGhlciB0aGFuIHRoZSBlbGVtZW50J3MgcHJvcGVydGllcy5cclxuXHRcdFx0XHRcdFx0Y3NzW3BdID0gdmFyc1twXTtcclxuXHRcdFx0XHRcdFx0ZGVsZXRlIHZhcnNbcF07XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0fVxyXG5cdFx0XHRcdHZhcnMuY3NzID0gY3NzO1xyXG5cdFx0XHR9O1xyXG5cclxuXHRcdHAgPSBUd2VlbkxpdGUucHJvdG90eXBlID0gbmV3IEFuaW1hdGlvbigpO1xyXG5cdFx0cC5jb25zdHJ1Y3RvciA9IFR3ZWVuTGl0ZTtcclxuXHRcdHAua2lsbCgpLl9nYyA9IGZhbHNlO1xyXG5cclxuLy8tLS0tVHdlZW5MaXRlIGRlZmF1bHRzLCBvdmVyd3JpdGUgbWFuYWdlbWVudCwgYW5kIHJvb3QgdXBkYXRlcyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG5cdFx0cC5yYXRpbyA9IDA7XHJcblx0XHRwLl9maXJzdFBUID0gcC5fdGFyZ2V0cyA9IHAuX292ZXJ3cml0dGVuUHJvcHMgPSBwLl9zdGFydEF0ID0gbnVsbDtcclxuXHRcdHAuX25vdGlmeVBsdWdpbnNPZkVuYWJsZWQgPSBwLl9sYXp5ID0gZmFsc2U7XHJcblxyXG5cdFx0VHdlZW5MaXRlLnZlcnNpb24gPSBcIjEuMjAuM1wiO1xyXG5cdFx0VHdlZW5MaXRlLmRlZmF1bHRFYXNlID0gcC5fZWFzZSA9IG5ldyBFYXNlKG51bGwsIG51bGwsIDEsIDEpO1xyXG5cdFx0VHdlZW5MaXRlLmRlZmF1bHRPdmVyd3JpdGUgPSBcImF1dG9cIjtcclxuXHRcdFR3ZWVuTGl0ZS50aWNrZXIgPSBfdGlja2VyO1xyXG5cdFx0VHdlZW5MaXRlLmF1dG9TbGVlcCA9IDEyMDtcclxuXHRcdFR3ZWVuTGl0ZS5sYWdTbW9vdGhpbmcgPSBmdW5jdGlvbih0aHJlc2hvbGQsIGFkanVzdGVkTGFnKSB7XHJcblx0XHRcdF90aWNrZXIubGFnU21vb3RoaW5nKHRocmVzaG9sZCwgYWRqdXN0ZWRMYWcpO1xyXG5cdFx0fTtcclxuXHJcblx0XHRUd2VlbkxpdGUuc2VsZWN0b3IgPSB3aW5kb3cuJCB8fCB3aW5kb3cualF1ZXJ5IHx8IGZ1bmN0aW9uKGUpIHtcclxuXHRcdFx0dmFyIHNlbGVjdG9yID0gd2luZG93LiQgfHwgd2luZG93LmpRdWVyeTtcclxuXHRcdFx0aWYgKHNlbGVjdG9yKSB7XHJcblx0XHRcdFx0VHdlZW5MaXRlLnNlbGVjdG9yID0gc2VsZWN0b3I7XHJcblx0XHRcdFx0cmV0dXJuIHNlbGVjdG9yKGUpO1xyXG5cdFx0XHR9XHJcblx0XHRcdHJldHVybiAodHlwZW9mKF9kb2MpID09PSBcInVuZGVmaW5lZFwiKSA/IGUgOiAoX2RvYy5xdWVyeVNlbGVjdG9yQWxsID8gX2RvYy5xdWVyeVNlbGVjdG9yQWxsKGUpIDogX2RvYy5nZXRFbGVtZW50QnlJZCgoZS5jaGFyQXQoMCkgPT09IFwiI1wiKSA/IGUuc3Vic3RyKDEpIDogZSkpO1xyXG5cdFx0fTtcclxuXHJcblx0XHR2YXIgX2xhenlUd2VlbnMgPSBbXSxcclxuXHRcdFx0X2xhenlMb29rdXAgPSB7fSxcclxuXHRcdFx0X251bWJlcnNFeHAgPSAvKD86KC18LT18XFwrPSk/XFxkKlxcLj9cXGQqKD86ZVtcXC0rXT9cXGQrKT8pWzAtOV0vaWcsXHJcblx0XHRcdF9yZWxFeHAgPSAvW1xcKy1dPS0/W1xcLlxcZF0vLFxyXG5cdFx0XHQvL19ub25OdW1iZXJzRXhwID0gLyg/OihbXFwtK10oPyEoXFxkfD0pKSl8W15cXGRcXC0rPWVdfChlKD8hW1xcLStdW1xcZF0pKSkrL2lnLFxyXG5cdFx0XHRfc2V0UmF0aW8gPSBmdW5jdGlvbih2KSB7XHJcblx0XHRcdFx0dmFyIHB0ID0gdGhpcy5fZmlyc3RQVCxcclxuXHRcdFx0XHRcdG1pbiA9IDAuMDAwMDAxLFxyXG5cdFx0XHRcdFx0dmFsO1xyXG5cdFx0XHRcdHdoaWxlIChwdCkge1xyXG5cdFx0XHRcdFx0dmFsID0gIXB0LmJsb2IgPyBwdC5jICogdiArIHB0LnMgOiAodiA9PT0gMSAmJiB0aGlzLmVuZCAhPSBudWxsKSA/IHRoaXMuZW5kIDogdiA/IHRoaXMuam9pbihcIlwiKSA6IHRoaXMuc3RhcnQ7XHJcblx0XHRcdFx0XHRpZiAocHQubSkge1xyXG5cdFx0XHRcdFx0XHR2YWwgPSBwdC5tKHZhbCwgdGhpcy5fdGFyZ2V0IHx8IHB0LnQpO1xyXG5cdFx0XHRcdFx0fSBlbHNlIGlmICh2YWwgPCBtaW4pIGlmICh2YWwgPiAtbWluICYmICFwdC5ibG9iKSB7IC8vcHJldmVudHMgaXNzdWVzIHdpdGggY29udmVydGluZyB2ZXJ5IHNtYWxsIG51bWJlcnMgdG8gc3RyaW5ncyBpbiB0aGUgYnJvd3NlclxyXG5cdFx0XHRcdFx0XHR2YWwgPSAwO1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0aWYgKCFwdC5mKSB7XHJcblx0XHRcdFx0XHRcdHB0LnRbcHQucF0gPSB2YWw7XHJcblx0XHRcdFx0XHR9IGVsc2UgaWYgKHB0LmZwKSB7XHJcblx0XHRcdFx0XHRcdHB0LnRbcHQucF0ocHQuZnAsIHZhbCk7XHJcblx0XHRcdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdFx0XHRwdC50W3B0LnBdKHZhbCk7XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRwdCA9IHB0Ll9uZXh0O1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fSxcclxuXHRcdFx0Ly9jb21wYXJlcyB0d28gc3RyaW5ncyAoc3RhcnQvZW5kKSwgZmluZHMgdGhlIG51bWJlcnMgdGhhdCBhcmUgZGlmZmVyZW50IGFuZCBzcGl0cyBiYWNrIGFuIGFycmF5IHJlcHJlc2VudGluZyB0aGUgd2hvbGUgdmFsdWUgYnV0IHdpdGggdGhlIGNoYW5naW5nIHZhbHVlcyBpc29sYXRlZCBhcyBlbGVtZW50cy4gRm9yIGV4YW1wbGUsIFwicmdiKDAsMCwwKVwiIGFuZCBcInJnYigxMDAsNTAsMClcIiB3b3VsZCBiZWNvbWUgW1wicmdiKFwiLCAwLCBcIixcIiwgNTAsIFwiLDApXCJdLiBOb3RpY2UgaXQgbWVyZ2VzIHRoZSBwYXJ0cyB0aGF0IGFyZSBpZGVudGljYWwgKHBlcmZvcm1hbmNlIG9wdGltaXphdGlvbikuIFRoZSBhcnJheSBhbHNvIGhhcyBhIGxpbmtlZCBsaXN0IG9mIFByb3BUd2VlbnMgYXR0YWNoZWQgc3RhcnRpbmcgd2l0aCBfZmlyc3RQVCB0aGF0IGNvbnRhaW4gdGhlIHR3ZWVuaW5nIGRhdGEgKHQsIHAsIHMsIGMsIGYsIGV0Yy4pLiBJdCBhbHNvIHN0b3JlcyB0aGUgc3RhcnRpbmcgdmFsdWUgYXMgYSBcInN0YXJ0XCIgcHJvcGVydHkgc28gdGhhdCB3ZSBjYW4gcmV2ZXJ0IHRvIGl0IGlmL3doZW4gbmVjZXNzYXJ5LCBsaWtlIHdoZW4gYSB0d2VlbiByZXdpbmRzIGZ1bGx5LiBJZiB0aGUgcXVhbnRpdHkgb2YgbnVtYmVycyBkaWZmZXJzIGJldHdlZW4gdGhlIHN0YXJ0IGFuZCBlbmQsIGl0IHdpbGwgYWx3YXlzIHByaW9yaXRpemUgdGhlIGVuZCB2YWx1ZShzKS4gVGhlIHB0IHBhcmFtZXRlciBpcyBvcHRpb25hbCAtIGl0J3MgZm9yIGEgUHJvcFR3ZWVuIHRoYXQgd2lsbCBiZSBhcHBlbmRlZCB0byB0aGUgZW5kIG9mIHRoZSBsaW5rZWQgbGlzdCBhbmQgaXMgdHlwaWNhbGx5IGZvciBhY3R1YWxseSBzZXR0aW5nIHRoZSB2YWx1ZSBhZnRlciBhbGwgb2YgdGhlIGVsZW1lbnRzIGhhdmUgYmVlbiB1cGRhdGVkICh3aXRoIGFycmF5LmpvaW4oXCJcIikpLlxyXG5cdFx0XHRfYmxvYkRpZiA9IGZ1bmN0aW9uKHN0YXJ0LCBlbmQsIGZpbHRlciwgcHQpIHtcclxuXHRcdFx0XHR2YXIgYSA9IFtdLFxyXG5cdFx0XHRcdFx0Y2hhckluZGV4ID0gMCxcclxuXHRcdFx0XHRcdHMgPSBcIlwiLFxyXG5cdFx0XHRcdFx0Y29sb3IgPSAwLFxyXG5cdFx0XHRcdFx0c3RhcnROdW1zLCBlbmROdW1zLCBudW0sIGksIGwsIG5vbk51bWJlcnMsIGN1cnJlbnROdW07XHJcblx0XHRcdFx0YS5zdGFydCA9IHN0YXJ0O1xyXG5cdFx0XHRcdGEuZW5kID0gZW5kO1xyXG5cdFx0XHRcdHN0YXJ0ID0gYVswXSA9IHN0YXJ0ICsgXCJcIjsgLy9lbnN1cmUgdmFsdWVzIGFyZSBzdHJpbmdzXHJcblx0XHRcdFx0ZW5kID0gYVsxXSA9IGVuZCArIFwiXCI7XHJcblx0XHRcdFx0aWYgKGZpbHRlcikge1xyXG5cdFx0XHRcdFx0ZmlsdGVyKGEpOyAvL3Bhc3MgYW4gYXJyYXkgd2l0aCB0aGUgc3RhcnRpbmcgYW5kIGVuZGluZyB2YWx1ZXMgYW5kIGxldCB0aGUgZmlsdGVyIGRvIHdoYXRldmVyIGl0IG5lZWRzIHRvIHRoZSB2YWx1ZXMuXHJcblx0XHRcdFx0XHRzdGFydCA9IGFbMF07XHJcblx0XHRcdFx0XHRlbmQgPSBhWzFdO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0XHRhLmxlbmd0aCA9IDA7XHJcblx0XHRcdFx0c3RhcnROdW1zID0gc3RhcnQubWF0Y2goX251bWJlcnNFeHApIHx8IFtdO1xyXG5cdFx0XHRcdGVuZE51bXMgPSBlbmQubWF0Y2goX251bWJlcnNFeHApIHx8IFtdO1xyXG5cdFx0XHRcdGlmIChwdCkge1xyXG5cdFx0XHRcdFx0cHQuX25leHQgPSBudWxsO1xyXG5cdFx0XHRcdFx0cHQuYmxvYiA9IDE7XHJcblx0XHRcdFx0XHRhLl9maXJzdFBUID0gYS5fYXBwbHlQVCA9IHB0OyAvL2FwcGx5IGxhc3QgaW4gdGhlIGxpbmtlZCBsaXN0ICh3aGljaCBtZWFucyBpbnNlcnRpbmcgaXQgZmlyc3QpXHJcblx0XHRcdFx0fVxyXG5cdFx0XHRcdGwgPSBlbmROdW1zLmxlbmd0aDtcclxuXHRcdFx0XHRmb3IgKGkgPSAwOyBpIDwgbDsgaSsrKSB7XHJcblx0XHRcdFx0XHRjdXJyZW50TnVtID0gZW5kTnVtc1tpXTtcclxuXHRcdFx0XHRcdG5vbk51bWJlcnMgPSBlbmQuc3Vic3RyKGNoYXJJbmRleCwgZW5kLmluZGV4T2YoY3VycmVudE51bSwgY2hhckluZGV4KS1jaGFySW5kZXgpO1xyXG5cdFx0XHRcdFx0cyArPSAobm9uTnVtYmVycyB8fCAhaSkgPyBub25OdW1iZXJzIDogXCIsXCI7IC8vbm90ZTogU1ZHIHNwZWMgYWxsb3dzIG9taXNzaW9uIG9mIGNvbW1hL3NwYWNlIHdoZW4gYSBuZWdhdGl2ZSBzaWduIGlzIHdlZGdlZCBiZXR3ZWVuIHR3byBudW1iZXJzLCBsaWtlIDIuNS01LjMgaW5zdGVhZCBvZiAyLjUsLTUuMyBidXQgd2hlbiB0d2VlbmluZywgdGhlIG5lZ2F0aXZlIHZhbHVlIG1heSBzd2l0Y2ggdG8gcG9zaXRpdmUsIHNvIHdlIGluc2VydCB0aGUgY29tbWEganVzdCBpbiBjYXNlLlxyXG5cdFx0XHRcdFx0Y2hhckluZGV4ICs9IG5vbk51bWJlcnMubGVuZ3RoO1xyXG5cdFx0XHRcdFx0aWYgKGNvbG9yKSB7IC8vc2Vuc2UgcmdiYSgpIHZhbHVlcyBhbmQgcm91bmQgdGhlbS5cclxuXHRcdFx0XHRcdFx0Y29sb3IgPSAoY29sb3IgKyAxKSAlIDU7XHJcblx0XHRcdFx0XHR9IGVsc2UgaWYgKG5vbk51bWJlcnMuc3Vic3RyKC01KSA9PT0gXCJyZ2JhKFwiKSB7XHJcblx0XHRcdFx0XHRcdGNvbG9yID0gMTtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdGlmIChjdXJyZW50TnVtID09PSBzdGFydE51bXNbaV0gfHwgc3RhcnROdW1zLmxlbmd0aCA8PSBpKSB7XHJcblx0XHRcdFx0XHRcdHMgKz0gY3VycmVudE51bTtcclxuXHRcdFx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0XHRcdGlmIChzKSB7XHJcblx0XHRcdFx0XHRcdFx0YS5wdXNoKHMpO1xyXG5cdFx0XHRcdFx0XHRcdHMgPSBcIlwiO1xyXG5cdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRcdG51bSA9IHBhcnNlRmxvYXQoc3RhcnROdW1zW2ldKTtcclxuXHRcdFx0XHRcdFx0YS5wdXNoKG51bSk7XHJcblx0XHRcdFx0XHRcdGEuX2ZpcnN0UFQgPSB7X25leHQ6IGEuX2ZpcnN0UFQsIHQ6YSwgcDogYS5sZW5ndGgtMSwgczpudW0sIGM6KChjdXJyZW50TnVtLmNoYXJBdCgxKSA9PT0gXCI9XCIpID8gcGFyc2VJbnQoY3VycmVudE51bS5jaGFyQXQoMCkgKyBcIjFcIiwgMTApICogcGFyc2VGbG9hdChjdXJyZW50TnVtLnN1YnN0cigyKSkgOiAocGFyc2VGbG9hdChjdXJyZW50TnVtKSAtIG51bSkpIHx8IDAsIGY6MCwgbTooY29sb3IgJiYgY29sb3IgPCA0KSA/IE1hdGgucm91bmQgOiAwfTtcclxuXHRcdFx0XHRcdFx0Ly9ub3RlOiB3ZSBkb24ndCBzZXQgX3ByZXYgYmVjYXVzZSB3ZSdsbCBuZXZlciBuZWVkIHRvIHJlbW92ZSBpbmRpdmlkdWFsIFByb3BUd2VlbnMgZnJvbSB0aGlzIGxpc3QuXHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRjaGFySW5kZXggKz0gY3VycmVudE51bS5sZW5ndGg7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHRcdHMgKz0gZW5kLnN1YnN0cihjaGFySW5kZXgpO1xyXG5cdFx0XHRcdGlmIChzKSB7XHJcblx0XHRcdFx0XHRhLnB1c2gocyk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHRcdGEuc2V0UmF0aW8gPSBfc2V0UmF0aW87XHJcblx0XHRcdFx0aWYgKF9yZWxFeHAudGVzdChlbmQpKSB7IC8vaWYgdGhlIGVuZCBzdHJpbmcgY29udGFpbnMgcmVsYXRpdmUgdmFsdWVzLCBkZWxldGUgaXQgc28gdGhhdCBvbiB0aGUgZmluYWwgcmVuZGVyIChpbiBfc2V0UmF0aW8oKSksIHdlIGRvbid0IGFjdHVhbGx5IHNldCBpdCB0byB0aGUgc3RyaW5nIHdpdGggKz0gb3IgLT0gY2hhcmFjdGVycyAoZm9yY2VzIGl0IHRvIHVzZSB0aGUgY2FsY3VsYXRlZCB2YWx1ZSkuXHJcblx0XHRcdFx0XHRhLmVuZCA9IG51bGw7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHRcdHJldHVybiBhO1xyXG5cdFx0XHR9LFxyXG5cdFx0XHQvL25vdGU6IFwiZnVuY1BhcmFtXCIgaXMgb25seSBuZWNlc3NhcnkgZm9yIGZ1bmN0aW9uLWJhc2VkIGdldHRlcnMvc2V0dGVycyB0aGF0IHJlcXVpcmUgYW4gZXh0cmEgcGFyYW1ldGVyIGxpa2UgZ2V0QXR0cmlidXRlKFwid2lkdGhcIikgYW5kIHNldEF0dHJpYnV0ZShcIndpZHRoXCIsIHZhbHVlKS4gSW4gdGhpcyBleGFtcGxlLCBmdW5jUGFyYW0gd291bGQgYmUgXCJ3aWR0aFwiLiBVc2VkIGJ5IEF0dHJQbHVnaW4gZm9yIGV4YW1wbGUuXHJcblx0XHRcdF9hZGRQcm9wVHdlZW4gPSBmdW5jdGlvbih0YXJnZXQsIHByb3AsIHN0YXJ0LCBlbmQsIG92ZXJ3cml0ZVByb3AsIG1vZCwgZnVuY1BhcmFtLCBzdHJpbmdGaWx0ZXIsIGluZGV4KSB7XHJcblx0XHRcdFx0aWYgKHR5cGVvZihlbmQpID09PSBcImZ1bmN0aW9uXCIpIHtcclxuXHRcdFx0XHRcdGVuZCA9IGVuZChpbmRleCB8fCAwLCB0YXJnZXQpO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0XHR2YXIgdHlwZSA9IHR5cGVvZih0YXJnZXRbcHJvcF0pLFxyXG5cdFx0XHRcdFx0Z2V0dGVyTmFtZSA9ICh0eXBlICE9PSBcImZ1bmN0aW9uXCIpID8gXCJcIiA6ICgocHJvcC5pbmRleE9mKFwic2V0XCIpIHx8IHR5cGVvZih0YXJnZXRbXCJnZXRcIiArIHByb3Auc3Vic3RyKDMpXSkgIT09IFwiZnVuY3Rpb25cIikgPyBwcm9wIDogXCJnZXRcIiArIHByb3Auc3Vic3RyKDMpKSxcclxuXHRcdFx0XHRcdHMgPSAoc3RhcnQgIT09IFwiZ2V0XCIpID8gc3RhcnQgOiAhZ2V0dGVyTmFtZSA/IHRhcmdldFtwcm9wXSA6IGZ1bmNQYXJhbSA/IHRhcmdldFtnZXR0ZXJOYW1lXShmdW5jUGFyYW0pIDogdGFyZ2V0W2dldHRlck5hbWVdKCksXHJcblx0XHRcdFx0XHRpc1JlbGF0aXZlID0gKHR5cGVvZihlbmQpID09PSBcInN0cmluZ1wiICYmIGVuZC5jaGFyQXQoMSkgPT09IFwiPVwiKSxcclxuXHRcdFx0XHRcdHB0ID0ge3Q6dGFyZ2V0LCBwOnByb3AsIHM6cywgZjoodHlwZSA9PT0gXCJmdW5jdGlvblwiKSwgcGc6MCwgbjpvdmVyd3JpdGVQcm9wIHx8IHByb3AsIG06KCFtb2QgPyAwIDogKHR5cGVvZihtb2QpID09PSBcImZ1bmN0aW9uXCIpID8gbW9kIDogTWF0aC5yb3VuZCksIHByOjAsIGM6aXNSZWxhdGl2ZSA/IHBhcnNlSW50KGVuZC5jaGFyQXQoMCkgKyBcIjFcIiwgMTApICogcGFyc2VGbG9hdChlbmQuc3Vic3RyKDIpKSA6IChwYXJzZUZsb2F0KGVuZCkgLSBzKSB8fCAwfSxcclxuXHRcdFx0XHRcdGJsb2I7XHJcblxyXG5cdFx0XHRcdGlmICh0eXBlb2YocykgIT09IFwibnVtYmVyXCIgfHwgKHR5cGVvZihlbmQpICE9PSBcIm51bWJlclwiICYmICFpc1JlbGF0aXZlKSkge1xyXG5cdFx0XHRcdFx0aWYgKGZ1bmNQYXJhbSB8fCBpc05hTihzKSB8fCAoIWlzUmVsYXRpdmUgJiYgaXNOYU4oZW5kKSkgfHwgdHlwZW9mKHMpID09PSBcImJvb2xlYW5cIiB8fCB0eXBlb2YoZW5kKSA9PT0gXCJib29sZWFuXCIpIHtcclxuXHRcdFx0XHRcdFx0Ly9hIGJsb2IgKHN0cmluZyB0aGF0IGhhcyBtdWx0aXBsZSBudW1iZXJzIGluIGl0KVxyXG5cdFx0XHRcdFx0XHRwdC5mcCA9IGZ1bmNQYXJhbTtcclxuXHRcdFx0XHRcdFx0YmxvYiA9IF9ibG9iRGlmKHMsIChpc1JlbGF0aXZlID8gcGFyc2VGbG9hdChwdC5zKSArIHB0LmMgOiBlbmQpLCBzdHJpbmdGaWx0ZXIgfHwgVHdlZW5MaXRlLmRlZmF1bHRTdHJpbmdGaWx0ZXIsIHB0KTtcclxuXHRcdFx0XHRcdFx0cHQgPSB7dDogYmxvYiwgcDogXCJzZXRSYXRpb1wiLCBzOiAwLCBjOiAxLCBmOiAyLCBwZzogMCwgbjogb3ZlcndyaXRlUHJvcCB8fCBwcm9wLCBwcjogMCwgbTogMH07IC8vXCIyXCIgaW5kaWNhdGVzIGl0J3MgYSBCbG9iIHByb3BlcnR5IHR3ZWVuLiBOZWVkZWQgZm9yIFJvdW5kUHJvcHNQbHVnaW4gZm9yIGV4YW1wbGUuXHJcblx0XHRcdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdFx0XHRwdC5zID0gcGFyc2VGbG9hdChzKTtcclxuXHRcdFx0XHRcdFx0aWYgKCFpc1JlbGF0aXZlKSB7XHJcblx0XHRcdFx0XHRcdFx0cHQuYyA9IChwYXJzZUZsb2F0KGVuZCkgLSBwdC5zKSB8fCAwO1xyXG5cdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0fVxyXG5cdFx0XHRcdGlmIChwdC5jKSB7IC8vb25seSBhZGQgaXQgdG8gdGhlIGxpbmtlZCBsaXN0IGlmIHRoZXJlJ3MgYSBjaGFuZ2UuXHJcblx0XHRcdFx0XHRpZiAoKHB0Ll9uZXh0ID0gdGhpcy5fZmlyc3RQVCkpIHtcclxuXHRcdFx0XHRcdFx0cHQuX25leHQuX3ByZXYgPSBwdDtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdHRoaXMuX2ZpcnN0UFQgPSBwdDtcclxuXHRcdFx0XHRcdHJldHVybiBwdDtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH0sXHJcblx0XHRcdF9pbnRlcm5hbHMgPSBUd2VlbkxpdGUuX2ludGVybmFscyA9IHtpc0FycmF5Ol9pc0FycmF5LCBpc1NlbGVjdG9yOl9pc1NlbGVjdG9yLCBsYXp5VHdlZW5zOl9sYXp5VHdlZW5zLCBibG9iRGlmOl9ibG9iRGlmfSwgLy9naXZlcyB1cyBhIHdheSB0byBleHBvc2UgY2VydGFpbiBwcml2YXRlIHZhbHVlcyB0byBvdGhlciBHcmVlblNvY2sgY2xhc3NlcyB3aXRob3V0IGNvbnRhbWluYXRpbmcgdGhhIG1haW4gVHdlZW5MaXRlIG9iamVjdC5cclxuXHRcdFx0X3BsdWdpbnMgPSBUd2VlbkxpdGUuX3BsdWdpbnMgPSB7fSxcclxuXHRcdFx0X3R3ZWVuTG9va3VwID0gX2ludGVybmFscy50d2Vlbkxvb2t1cCA9IHt9LFxyXG5cdFx0XHRfdHdlZW5Mb29rdXBOdW0gPSAwLFxyXG5cdFx0XHRfcmVzZXJ2ZWRQcm9wcyA9IF9pbnRlcm5hbHMucmVzZXJ2ZWRQcm9wcyA9IHtlYXNlOjEsIGRlbGF5OjEsIG92ZXJ3cml0ZToxLCBvbkNvbXBsZXRlOjEsIG9uQ29tcGxldGVQYXJhbXM6MSwgb25Db21wbGV0ZVNjb3BlOjEsIHVzZUZyYW1lczoxLCBydW5CYWNrd2FyZHM6MSwgc3RhcnRBdDoxLCBvblVwZGF0ZToxLCBvblVwZGF0ZVBhcmFtczoxLCBvblVwZGF0ZVNjb3BlOjEsIG9uU3RhcnQ6MSwgb25TdGFydFBhcmFtczoxLCBvblN0YXJ0U2NvcGU6MSwgb25SZXZlcnNlQ29tcGxldGU6MSwgb25SZXZlcnNlQ29tcGxldGVQYXJhbXM6MSwgb25SZXZlcnNlQ29tcGxldGVTY29wZToxLCBvblJlcGVhdDoxLCBvblJlcGVhdFBhcmFtczoxLCBvblJlcGVhdFNjb3BlOjEsIGVhc2VQYXJhbXM6MSwgeW95bzoxLCBpbW1lZGlhdGVSZW5kZXI6MSwgcmVwZWF0OjEsIHJlcGVhdERlbGF5OjEsIGRhdGE6MSwgcGF1c2VkOjEsIHJldmVyc2VkOjEsIGF1dG9DU1M6MSwgbGF6eToxLCBvbk92ZXJ3cml0ZToxLCBjYWxsYmFja1Njb3BlOjEsIHN0cmluZ0ZpbHRlcjoxLCBpZDoxLCB5b3lvRWFzZToxfSxcclxuXHRcdFx0X292ZXJ3cml0ZUxvb2t1cCA9IHtub25lOjAsIGFsbDoxLCBhdXRvOjIsIGNvbmN1cnJlbnQ6MywgYWxsT25TdGFydDo0LCBwcmVleGlzdGluZzo1LCBcInRydWVcIjoxLCBcImZhbHNlXCI6MH0sXHJcblx0XHRcdF9yb290RnJhbWVzVGltZWxpbmUgPSBBbmltYXRpb24uX3Jvb3RGcmFtZXNUaW1lbGluZSA9IG5ldyBTaW1wbGVUaW1lbGluZSgpLFxyXG5cdFx0XHRfcm9vdFRpbWVsaW5lID0gQW5pbWF0aW9uLl9yb290VGltZWxpbmUgPSBuZXcgU2ltcGxlVGltZWxpbmUoKSxcclxuXHRcdFx0X25leHRHQ0ZyYW1lID0gMzAsXHJcblx0XHRcdF9sYXp5UmVuZGVyID0gX2ludGVybmFscy5sYXp5UmVuZGVyID0gZnVuY3Rpb24oKSB7XHJcblx0XHRcdFx0dmFyIGkgPSBfbGF6eVR3ZWVucy5sZW5ndGgsXHJcblx0XHRcdFx0XHR0d2VlbjtcclxuXHRcdFx0XHRfbGF6eUxvb2t1cCA9IHt9O1xyXG5cdFx0XHRcdHdoaWxlICgtLWkgPiAtMSkge1xyXG5cdFx0XHRcdFx0dHdlZW4gPSBfbGF6eVR3ZWVuc1tpXTtcclxuXHRcdFx0XHRcdGlmICh0d2VlbiAmJiB0d2Vlbi5fbGF6eSAhPT0gZmFsc2UpIHtcclxuXHRcdFx0XHRcdFx0dHdlZW4ucmVuZGVyKHR3ZWVuLl9sYXp5WzBdLCB0d2Vlbi5fbGF6eVsxXSwgdHJ1ZSk7XHJcblx0XHRcdFx0XHRcdHR3ZWVuLl9sYXp5ID0gZmFsc2U7XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0fVxyXG5cdFx0XHRcdF9sYXp5VHdlZW5zLmxlbmd0aCA9IDA7XHJcblx0XHRcdH07XHJcblxyXG5cdFx0X3Jvb3RUaW1lbGluZS5fc3RhcnRUaW1lID0gX3RpY2tlci50aW1lO1xyXG5cdFx0X3Jvb3RGcmFtZXNUaW1lbGluZS5fc3RhcnRUaW1lID0gX3RpY2tlci5mcmFtZTtcclxuXHRcdF9yb290VGltZWxpbmUuX2FjdGl2ZSA9IF9yb290RnJhbWVzVGltZWxpbmUuX2FjdGl2ZSA9IHRydWU7XHJcblx0XHRzZXRUaW1lb3V0KF9sYXp5UmVuZGVyLCAxKTsgLy9vbiBzb21lIG1vYmlsZSBkZXZpY2VzLCB0aGVyZSBpc24ndCBhIFwidGlja1wiIGJlZm9yZSBjb2RlIHJ1bnMgd2hpY2ggbWVhbnMgYW55IGxhenkgcmVuZGVycyB3b3VsZG4ndCBydW4gYmVmb3JlIHRoZSBuZXh0IG9mZmljaWFsIFwidGlja1wiLlxyXG5cclxuXHRcdEFuaW1hdGlvbi5fdXBkYXRlUm9vdCA9IFR3ZWVuTGl0ZS5yZW5kZXIgPSBmdW5jdGlvbigpIHtcclxuXHRcdFx0XHR2YXIgaSwgYSwgcDtcclxuXHRcdFx0XHRpZiAoX2xhenlUd2VlbnMubGVuZ3RoKSB7IC8vaWYgY29kZSBpcyBydW4gb3V0c2lkZSBvZiB0aGUgcmVxdWVzdEFuaW1hdGlvbkZyYW1lIGxvb3AsIHRoZXJlIG1heSBiZSB0d2VlbnMgcXVldWVkIEFGVEVSIHRoZSBlbmdpbmUgcmVmcmVzaGVkLCBzbyB3ZSBuZWVkIHRvIGVuc3VyZSBhbnkgcGVuZGluZyByZW5kZXJzIG9jY3VyIGJlZm9yZSB3ZSByZWZyZXNoIGFnYWluLlxyXG5cdFx0XHRcdFx0X2xhenlSZW5kZXIoKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0X3Jvb3RUaW1lbGluZS5yZW5kZXIoKF90aWNrZXIudGltZSAtIF9yb290VGltZWxpbmUuX3N0YXJ0VGltZSkgKiBfcm9vdFRpbWVsaW5lLl90aW1lU2NhbGUsIGZhbHNlLCBmYWxzZSk7XHJcblx0XHRcdFx0X3Jvb3RGcmFtZXNUaW1lbGluZS5yZW5kZXIoKF90aWNrZXIuZnJhbWUgLSBfcm9vdEZyYW1lc1RpbWVsaW5lLl9zdGFydFRpbWUpICogX3Jvb3RGcmFtZXNUaW1lbGluZS5fdGltZVNjYWxlLCBmYWxzZSwgZmFsc2UpO1xyXG5cdFx0XHRcdGlmIChfbGF6eVR3ZWVucy5sZW5ndGgpIHtcclxuXHRcdFx0XHRcdF9sYXp5UmVuZGVyKCk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHRcdGlmIChfdGlja2VyLmZyYW1lID49IF9uZXh0R0NGcmFtZSkgeyAvL2R1bXAgZ2FyYmFnZSBldmVyeSAxMjAgZnJhbWVzIG9yIHdoYXRldmVyIHRoZSB1c2VyIHNldHMgVHdlZW5MaXRlLmF1dG9TbGVlcCB0b1xyXG5cdFx0XHRcdFx0X25leHRHQ0ZyYW1lID0gX3RpY2tlci5mcmFtZSArIChwYXJzZUludChUd2VlbkxpdGUuYXV0b1NsZWVwLCAxMCkgfHwgMTIwKTtcclxuXHRcdFx0XHRcdGZvciAocCBpbiBfdHdlZW5Mb29rdXApIHtcclxuXHRcdFx0XHRcdFx0YSA9IF90d2Vlbkxvb2t1cFtwXS50d2VlbnM7XHJcblx0XHRcdFx0XHRcdGkgPSBhLmxlbmd0aDtcclxuXHRcdFx0XHRcdFx0d2hpbGUgKC0taSA+IC0xKSB7XHJcblx0XHRcdFx0XHRcdFx0aWYgKGFbaV0uX2djKSB7XHJcblx0XHRcdFx0XHRcdFx0XHRhLnNwbGljZShpLCAxKTtcclxuXHRcdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdFx0aWYgKGEubGVuZ3RoID09PSAwKSB7XHJcblx0XHRcdFx0XHRcdFx0ZGVsZXRlIF90d2Vlbkxvb2t1cFtwXTtcclxuXHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0Ly9pZiB0aGVyZSBhcmUgbm8gbW9yZSB0d2VlbnMgaW4gdGhlIHJvb3QgdGltZWxpbmVzLCBvciBpZiB0aGV5J3JlIGFsbCBwYXVzZWQsIG1ha2UgdGhlIF90aW1lciBzbGVlcCB0byByZWR1Y2UgbG9hZCBvbiB0aGUgQ1BVIHNsaWdodGx5XHJcblx0XHRcdFx0XHRwID0gX3Jvb3RUaW1lbGluZS5fZmlyc3Q7XHJcblx0XHRcdFx0XHRpZiAoIXAgfHwgcC5fcGF1c2VkKSBpZiAoVHdlZW5MaXRlLmF1dG9TbGVlcCAmJiAhX3Jvb3RGcmFtZXNUaW1lbGluZS5fZmlyc3QgJiYgX3RpY2tlci5fbGlzdGVuZXJzLnRpY2subGVuZ3RoID09PSAxKSB7XHJcblx0XHRcdFx0XHRcdHdoaWxlIChwICYmIHAuX3BhdXNlZCkge1xyXG5cdFx0XHRcdFx0XHRcdHAgPSBwLl9uZXh0O1xyXG5cdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRcdGlmICghcCkge1xyXG5cdFx0XHRcdFx0XHRcdF90aWNrZXIuc2xlZXAoKTtcclxuXHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdH1cclxuXHRcdFx0fTtcclxuXHJcblx0XHRfdGlja2VyLmFkZEV2ZW50TGlzdGVuZXIoXCJ0aWNrXCIsIEFuaW1hdGlvbi5fdXBkYXRlUm9vdCk7XHJcblxyXG5cdFx0dmFyIF9yZWdpc3RlciA9IGZ1bmN0aW9uKHRhcmdldCwgdHdlZW4sIHNjcnViKSB7XHJcblx0XHRcdFx0dmFyIGlkID0gdGFyZ2V0Ll9nc1R3ZWVuSUQsIGEsIGk7XHJcblx0XHRcdFx0aWYgKCFfdHdlZW5Mb29rdXBbaWQgfHwgKHRhcmdldC5fZ3NUd2VlbklEID0gaWQgPSBcInRcIiArIChfdHdlZW5Mb29rdXBOdW0rKykpXSkge1xyXG5cdFx0XHRcdFx0X3R3ZWVuTG9va3VwW2lkXSA9IHt0YXJnZXQ6dGFyZ2V0LCB0d2VlbnM6W119O1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0XHRpZiAodHdlZW4pIHtcclxuXHRcdFx0XHRcdGEgPSBfdHdlZW5Mb29rdXBbaWRdLnR3ZWVucztcclxuXHRcdFx0XHRcdGFbKGkgPSBhLmxlbmd0aCldID0gdHdlZW47XHJcblx0XHRcdFx0XHRpZiAoc2NydWIpIHtcclxuXHRcdFx0XHRcdFx0d2hpbGUgKC0taSA+IC0xKSB7XHJcblx0XHRcdFx0XHRcdFx0aWYgKGFbaV0gPT09IHR3ZWVuKSB7XHJcblx0XHRcdFx0XHRcdFx0XHRhLnNwbGljZShpLCAxKTtcclxuXHRcdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0cmV0dXJuIF90d2Vlbkxvb2t1cFtpZF0udHdlZW5zO1xyXG5cdFx0XHR9LFxyXG5cdFx0XHRfb25PdmVyd3JpdGUgPSBmdW5jdGlvbihvdmVyd3JpdHRlblR3ZWVuLCBvdmVyd3JpdGluZ1R3ZWVuLCB0YXJnZXQsIGtpbGxlZFByb3BzKSB7XHJcblx0XHRcdFx0dmFyIGZ1bmMgPSBvdmVyd3JpdHRlblR3ZWVuLnZhcnMub25PdmVyd3JpdGUsIHIxLCByMjtcclxuXHRcdFx0XHRpZiAoZnVuYykge1xyXG5cdFx0XHRcdFx0cjEgPSBmdW5jKG92ZXJ3cml0dGVuVHdlZW4sIG92ZXJ3cml0aW5nVHdlZW4sIHRhcmdldCwga2lsbGVkUHJvcHMpO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0XHRmdW5jID0gVHdlZW5MaXRlLm9uT3ZlcndyaXRlO1xyXG5cdFx0XHRcdGlmIChmdW5jKSB7XHJcblx0XHRcdFx0XHRyMiA9IGZ1bmMob3ZlcndyaXR0ZW5Ud2Vlbiwgb3ZlcndyaXRpbmdUd2VlbiwgdGFyZ2V0LCBraWxsZWRQcm9wcyk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHRcdHJldHVybiAocjEgIT09IGZhbHNlICYmIHIyICE9PSBmYWxzZSk7XHJcblx0XHRcdH0sXHJcblx0XHRcdF9hcHBseU92ZXJ3cml0ZSA9IGZ1bmN0aW9uKHRhcmdldCwgdHdlZW4sIHByb3BzLCBtb2RlLCBzaWJsaW5ncykge1xyXG5cdFx0XHRcdHZhciBpLCBjaGFuZ2VkLCBjdXJUd2VlbiwgbDtcclxuXHRcdFx0XHRpZiAobW9kZSA9PT0gMSB8fCBtb2RlID49IDQpIHtcclxuXHRcdFx0XHRcdGwgPSBzaWJsaW5ncy5sZW5ndGg7XHJcblx0XHRcdFx0XHRmb3IgKGkgPSAwOyBpIDwgbDsgaSsrKSB7XHJcblx0XHRcdFx0XHRcdGlmICgoY3VyVHdlZW4gPSBzaWJsaW5nc1tpXSkgIT09IHR3ZWVuKSB7XHJcblx0XHRcdFx0XHRcdFx0aWYgKCFjdXJUd2Vlbi5fZ2MpIHtcclxuXHRcdFx0XHRcdFx0XHRcdGlmIChjdXJUd2Vlbi5fa2lsbChudWxsLCB0YXJnZXQsIHR3ZWVuKSkge1xyXG5cdFx0XHRcdFx0XHRcdFx0XHRjaGFuZ2VkID0gdHJ1ZTtcclxuXHRcdFx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRcdH0gZWxzZSBpZiAobW9kZSA9PT0gNSkge1xyXG5cdFx0XHRcdFx0XHRcdGJyZWFrO1xyXG5cdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRyZXR1cm4gY2hhbmdlZDtcclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0Ly9OT1RFOiBBZGQgMC4wMDAwMDAwMDAxIHRvIG92ZXJjb21lIGZsb2F0aW5nIHBvaW50IGVycm9ycyB0aGF0IGNhbiBjYXVzZSB0aGUgc3RhcnRUaW1lIHRvIGJlIFZFUlkgc2xpZ2h0bHkgb2ZmICh3aGVuIGEgdHdlZW4ncyB0aW1lKCkgaXMgc2V0IGZvciBleGFtcGxlKVxyXG5cdFx0XHRcdHZhciBzdGFydFRpbWUgPSB0d2Vlbi5fc3RhcnRUaW1lICsgX3RpbnlOdW0sXHJcblx0XHRcdFx0XHRvdmVybGFwcyA9IFtdLFxyXG5cdFx0XHRcdFx0b0NvdW50ID0gMCxcclxuXHRcdFx0XHRcdHplcm9EdXIgPSAodHdlZW4uX2R1cmF0aW9uID09PSAwKSxcclxuXHRcdFx0XHRcdGdsb2JhbFN0YXJ0O1xyXG5cdFx0XHRcdGkgPSBzaWJsaW5ncy5sZW5ndGg7XHJcblx0XHRcdFx0d2hpbGUgKC0taSA+IC0xKSB7XHJcblx0XHRcdFx0XHRpZiAoKGN1clR3ZWVuID0gc2libGluZ3NbaV0pID09PSB0d2VlbiB8fCBjdXJUd2Vlbi5fZ2MgfHwgY3VyVHdlZW4uX3BhdXNlZCkge1xyXG5cdFx0XHRcdFx0XHQvL2lnbm9yZVxyXG5cdFx0XHRcdFx0fSBlbHNlIGlmIChjdXJUd2Vlbi5fdGltZWxpbmUgIT09IHR3ZWVuLl90aW1lbGluZSkge1xyXG5cdFx0XHRcdFx0XHRnbG9iYWxTdGFydCA9IGdsb2JhbFN0YXJ0IHx8IF9jaGVja092ZXJsYXAodHdlZW4sIDAsIHplcm9EdXIpO1xyXG5cdFx0XHRcdFx0XHRpZiAoX2NoZWNrT3ZlcmxhcChjdXJUd2VlbiwgZ2xvYmFsU3RhcnQsIHplcm9EdXIpID09PSAwKSB7XHJcblx0XHRcdFx0XHRcdFx0b3ZlcmxhcHNbb0NvdW50KytdID0gY3VyVHdlZW47XHJcblx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdH0gZWxzZSBpZiAoY3VyVHdlZW4uX3N0YXJ0VGltZSA8PSBzdGFydFRpbWUpIGlmIChjdXJUd2Vlbi5fc3RhcnRUaW1lICsgY3VyVHdlZW4udG90YWxEdXJhdGlvbigpIC8gY3VyVHdlZW4uX3RpbWVTY2FsZSA+IHN0YXJ0VGltZSkgaWYgKCEoKHplcm9EdXIgfHwgIWN1clR3ZWVuLl9pbml0dGVkKSAmJiBzdGFydFRpbWUgLSBjdXJUd2Vlbi5fc3RhcnRUaW1lIDw9IDAuMDAwMDAwMDAwMikpIHtcclxuXHRcdFx0XHRcdFx0b3ZlcmxhcHNbb0NvdW50KytdID0gY3VyVHdlZW47XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0fVxyXG5cclxuXHRcdFx0XHRpID0gb0NvdW50O1xyXG5cdFx0XHRcdHdoaWxlICgtLWkgPiAtMSkge1xyXG5cdFx0XHRcdFx0Y3VyVHdlZW4gPSBvdmVybGFwc1tpXTtcclxuXHRcdFx0XHRcdGlmIChtb2RlID09PSAyKSBpZiAoY3VyVHdlZW4uX2tpbGwocHJvcHMsIHRhcmdldCwgdHdlZW4pKSB7XHJcblx0XHRcdFx0XHRcdGNoYW5nZWQgPSB0cnVlO1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0aWYgKG1vZGUgIT09IDIgfHwgKCFjdXJUd2Vlbi5fZmlyc3RQVCAmJiBjdXJUd2Vlbi5faW5pdHRlZCkpIHtcclxuXHRcdFx0XHRcdFx0aWYgKG1vZGUgIT09IDIgJiYgIV9vbk92ZXJ3cml0ZShjdXJUd2VlbiwgdHdlZW4pKSB7XHJcblx0XHRcdFx0XHRcdFx0Y29udGludWU7XHJcblx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdFx0aWYgKGN1clR3ZWVuLl9lbmFibGVkKGZhbHNlLCBmYWxzZSkpIHsgLy9pZiBhbGwgcHJvcGVydHkgdHdlZW5zIGhhdmUgYmVlbiBvdmVyd3JpdHRlbiwga2lsbCB0aGUgdHdlZW4uXHJcblx0XHRcdFx0XHRcdFx0Y2hhbmdlZCA9IHRydWU7XHJcblx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0cmV0dXJuIGNoYW5nZWQ7XHJcblx0XHRcdH0sXHJcblx0XHRcdF9jaGVja092ZXJsYXAgPSBmdW5jdGlvbih0d2VlbiwgcmVmZXJlbmNlLCB6ZXJvRHVyKSB7XHJcblx0XHRcdFx0dmFyIHRsID0gdHdlZW4uX3RpbWVsaW5lLFxyXG5cdFx0XHRcdFx0dHMgPSB0bC5fdGltZVNjYWxlLFxyXG5cdFx0XHRcdFx0dCA9IHR3ZWVuLl9zdGFydFRpbWU7XHJcblx0XHRcdFx0d2hpbGUgKHRsLl90aW1lbGluZSkge1xyXG5cdFx0XHRcdFx0dCArPSB0bC5fc3RhcnRUaW1lO1xyXG5cdFx0XHRcdFx0dHMgKj0gdGwuX3RpbWVTY2FsZTtcclxuXHRcdFx0XHRcdGlmICh0bC5fcGF1c2VkKSB7XHJcblx0XHRcdFx0XHRcdHJldHVybiAtMTAwO1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0dGwgPSB0bC5fdGltZWxpbmU7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHRcdHQgLz0gdHM7XHJcblx0XHRcdFx0cmV0dXJuICh0ID4gcmVmZXJlbmNlKSA/IHQgLSByZWZlcmVuY2UgOiAoKHplcm9EdXIgJiYgdCA9PT0gcmVmZXJlbmNlKSB8fCAoIXR3ZWVuLl9pbml0dGVkICYmIHQgLSByZWZlcmVuY2UgPCAyICogX3RpbnlOdW0pKSA/IF90aW55TnVtIDogKCh0ICs9IHR3ZWVuLnRvdGFsRHVyYXRpb24oKSAvIHR3ZWVuLl90aW1lU2NhbGUgLyB0cykgPiByZWZlcmVuY2UgKyBfdGlueU51bSkgPyAwIDogdCAtIHJlZmVyZW5jZSAtIF90aW55TnVtO1xyXG5cdFx0XHR9O1xyXG5cclxuXHJcbi8vLS0tLSBUd2VlbkxpdGUgaW5zdGFuY2UgbWV0aG9kcyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuXHRcdHAuX2luaXQgPSBmdW5jdGlvbigpIHtcclxuXHRcdFx0dmFyIHYgPSB0aGlzLnZhcnMsXHJcblx0XHRcdFx0b3AgPSB0aGlzLl9vdmVyd3JpdHRlblByb3BzLFxyXG5cdFx0XHRcdGR1ciA9IHRoaXMuX2R1cmF0aW9uLFxyXG5cdFx0XHRcdGltbWVkaWF0ZSA9ICEhdi5pbW1lZGlhdGVSZW5kZXIsXHJcblx0XHRcdFx0ZWFzZSA9IHYuZWFzZSxcclxuXHRcdFx0XHRpLCBpbml0UGx1Z2lucywgcHQsIHAsIHN0YXJ0VmFycywgbDtcclxuXHRcdFx0aWYgKHYuc3RhcnRBdCkge1xyXG5cdFx0XHRcdGlmICh0aGlzLl9zdGFydEF0KSB7XHJcblx0XHRcdFx0XHR0aGlzLl9zdGFydEF0LnJlbmRlcigtMSwgdHJ1ZSk7IC8vaWYgd2UndmUgcnVuIGEgc3RhcnRBdCBwcmV2aW91c2x5ICh3aGVuIHRoZSB0d2VlbiBpbnN0YW50aWF0ZWQpLCB3ZSBzaG91bGQgcmV2ZXJ0IGl0IHNvIHRoYXQgdGhlIHZhbHVlcyByZS1pbnN0YW50aWF0ZSBjb3JyZWN0bHkgcGFydGljdWxhcmx5IGZvciByZWxhdGl2ZSB0d2VlbnMuIFdpdGhvdXQgdGhpcywgYSBUd2VlbkxpdGUuZnJvbVRvKG9iaiwgMSwge3g6XCIrPTEwMFwifSwge3g6XCItPTEwMFwifSksIGZvciBleGFtcGxlLCB3b3VsZCBhY3R1YWxseSBqdW1wIHRvICs9MjAwIGJlY2F1c2UgdGhlIHN0YXJ0QXQgd291bGQgcnVuIHR3aWNlLCBkb3VibGluZyB0aGUgcmVsYXRpdmUgY2hhbmdlLlxyXG5cdFx0XHRcdFx0dGhpcy5fc3RhcnRBdC5raWxsKCk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHRcdHN0YXJ0VmFycyA9IHt9O1xyXG5cdFx0XHRcdGZvciAocCBpbiB2LnN0YXJ0QXQpIHsgLy9jb3B5IHRoZSBwcm9wZXJ0aWVzL3ZhbHVlcyBpbnRvIGEgbmV3IG9iamVjdCB0byBhdm9pZCBjb2xsaXNpb25zLCBsaWtlIHZhciB0byA9IHt4OjB9LCBmcm9tID0ge3g6NTAwfTsgdGltZWxpbmUuZnJvbVRvKGUsIDEsIGZyb20sIHRvKS5mcm9tVG8oZSwgMSwgdG8sIGZyb20pO1xyXG5cdFx0XHRcdFx0c3RhcnRWYXJzW3BdID0gdi5zdGFydEF0W3BdO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0XHRzdGFydFZhcnMuZGF0YSA9IFwiaXNTdGFydFwiO1xyXG5cdFx0XHRcdHN0YXJ0VmFycy5vdmVyd3JpdGUgPSBmYWxzZTtcclxuXHRcdFx0XHRzdGFydFZhcnMuaW1tZWRpYXRlUmVuZGVyID0gdHJ1ZTtcclxuXHRcdFx0XHRzdGFydFZhcnMubGF6eSA9IChpbW1lZGlhdGUgJiYgdi5sYXp5ICE9PSBmYWxzZSk7XHJcblx0XHRcdFx0c3RhcnRWYXJzLnN0YXJ0QXQgPSBzdGFydFZhcnMuZGVsYXkgPSBudWxsOyAvL25vIG5lc3Rpbmcgb2Ygc3RhcnRBdCBvYmplY3RzIGFsbG93ZWQgKG90aGVyd2lzZSBpdCBjb3VsZCBjYXVzZSBhbiBpbmZpbml0ZSBsb29wKS5cclxuXHRcdFx0XHRzdGFydFZhcnMub25VcGRhdGUgPSB2Lm9uVXBkYXRlO1xyXG5cdFx0XHRcdHN0YXJ0VmFycy5vblVwZGF0ZVBhcmFtcyA9IHYub25VcGRhdGVQYXJhbXM7XHJcblx0XHRcdFx0c3RhcnRWYXJzLm9uVXBkYXRlU2NvcGUgPSB2Lm9uVXBkYXRlU2NvcGUgfHwgdi5jYWxsYmFja1Njb3BlIHx8IHRoaXM7XHJcblx0XHRcdFx0dGhpcy5fc3RhcnRBdCA9IFR3ZWVuTGl0ZS50byh0aGlzLnRhcmdldCwgMCwgc3RhcnRWYXJzKTtcclxuXHRcdFx0XHRpZiAoaW1tZWRpYXRlKSB7XHJcblx0XHRcdFx0XHRpZiAodGhpcy5fdGltZSA+IDApIHtcclxuXHRcdFx0XHRcdFx0dGhpcy5fc3RhcnRBdCA9IG51bGw7IC8vdHdlZW5zIHRoYXQgcmVuZGVyIGltbWVkaWF0ZWx5IChsaWtlIG1vc3QgZnJvbSgpIGFuZCBmcm9tVG8oKSB0d2VlbnMpIHNob3VsZG4ndCByZXZlcnQgd2hlbiB0aGVpciBwYXJlbnQgdGltZWxpbmUncyBwbGF5aGVhZCBnb2VzIGJhY2t3YXJkIHBhc3QgdGhlIHN0YXJ0VGltZSBiZWNhdXNlIHRoZSBpbml0aWFsIHJlbmRlciBjb3VsZCBoYXZlIGhhcHBlbmVkIGFueXRpbWUgYW5kIGl0IHNob3VsZG4ndCBiZSBkaXJlY3RseSBjb3JyZWxhdGVkIHRvIHRoaXMgdHdlZW4ncyBzdGFydFRpbWUuIEltYWdpbmUgc2V0dGluZyB1cCBhIGNvbXBsZXggYW5pbWF0aW9uIHdoZXJlIHRoZSBiZWdpbm5pbmcgc3RhdGVzIG9mIHZhcmlvdXMgb2JqZWN0cyBhcmUgcmVuZGVyZWQgaW1tZWRpYXRlbHkgYnV0IHRoZSB0d2VlbiBkb2Vzbid0IGhhcHBlbiBmb3IgcXVpdGUgc29tZSB0aW1lIC0gaWYgd2UgcmV2ZXJ0IHRvIHRoZSBzdGFydGluZyB2YWx1ZXMgYXMgc29vbiBhcyB0aGUgcGxheWhlYWQgZ29lcyBiYWNrd2FyZCBwYXN0IHRoZSB0d2VlbidzIHN0YXJ0VGltZSwgaXQgd2lsbCB0aHJvdyB0aGluZ3Mgb2ZmIHZpc3VhbGx5LiBSZXZlcnNpb24gc2hvdWxkIG9ubHkgaGFwcGVuIGluIFRpbWVsaW5lTGl0ZS9NYXggaW5zdGFuY2VzIHdoZXJlIGltbWVkaWF0ZVJlbmRlciB3YXMgZmFsc2UgKHdoaWNoIGlzIHRoZSBkZWZhdWx0IGluIHRoZSBjb252ZW5pZW5jZSBtZXRob2RzIGxpa2UgZnJvbSgpKS5cclxuXHRcdFx0XHRcdH0gZWxzZSBpZiAoZHVyICE9PSAwKSB7XHJcblx0XHRcdFx0XHRcdHJldHVybjsgLy93ZSBza2lwIGluaXRpYWxpemF0aW9uIGhlcmUgc28gdGhhdCBvdmVyd3JpdGluZyBkb2Vzbid0IG9jY3VyIHVudGlsIHRoZSB0d2VlbiBhY3R1YWxseSBiZWdpbnMuIE90aGVyd2lzZSwgaWYgeW91IGNyZWF0ZSBzZXZlcmFsIGltbWVkaWF0ZVJlbmRlcjp0cnVlIHR3ZWVucyBvZiB0aGUgc2FtZSB0YXJnZXQvcHJvcGVydGllcyB0byBkcm9wIGludG8gYSBUaW1lbGluZUxpdGUgb3IgVGltZWxpbmVNYXgsIHRoZSBsYXN0IG9uZSBjcmVhdGVkIHdvdWxkIG92ZXJ3cml0ZSB0aGUgZmlyc3Qgb25lcyBiZWNhdXNlIHRoZXkgZGlkbid0IGdldCBwbGFjZWQgaW50byB0aGUgdGltZWxpbmUgeWV0IGJlZm9yZSB0aGUgZmlyc3QgcmVuZGVyIG9jY3VycyBhbmQga2lja3MgaW4gb3ZlcndyaXRpbmcuXHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9IGVsc2UgaWYgKHYucnVuQmFja3dhcmRzICYmIGR1ciAhPT0gMCkge1xyXG5cdFx0XHRcdC8vZnJvbSgpIHR3ZWVucyBtdXN0IGJlIGhhbmRsZWQgdW5pcXVlbHk6IHRoZWlyIGJlZ2lubmluZyB2YWx1ZXMgbXVzdCBiZSByZW5kZXJlZCBidXQgd2UgZG9uJ3Qgd2FudCBvdmVyd3JpdGluZyB0byBvY2N1ciB5ZXQgKHdoZW4gdGltZSBpcyBzdGlsbCAwKS4gV2FpdCB1bnRpbCB0aGUgdHdlZW4gYWN0dWFsbHkgYmVnaW5zIGJlZm9yZSBkb2luZyBhbGwgdGhlIHJvdXRpbmVzIGxpa2Ugb3ZlcndyaXRpbmcuIEF0IHRoYXQgdGltZSwgd2Ugc2hvdWxkIHJlbmRlciBhdCB0aGUgRU5EIG9mIHRoZSB0d2VlbiB0byBlbnN1cmUgdGhhdCB0aGluZ3MgaW5pdGlhbGl6ZSBjb3JyZWN0bHkgKHJlbWVtYmVyLCBmcm9tKCkgdHdlZW5zIGdvIGJhY2t3YXJkcylcclxuXHRcdFx0XHRpZiAodGhpcy5fc3RhcnRBdCkge1xyXG5cdFx0XHRcdFx0dGhpcy5fc3RhcnRBdC5yZW5kZXIoLTEsIHRydWUpO1xyXG5cdFx0XHRcdFx0dGhpcy5fc3RhcnRBdC5raWxsKCk7XHJcblx0XHRcdFx0XHR0aGlzLl9zdGFydEF0ID0gbnVsbDtcclxuXHRcdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdFx0aWYgKHRoaXMuX3RpbWUgIT09IDApIHsgLy9pbiByYXJlIGNhc2VzIChsaWtlIGlmIGEgZnJvbSgpIHR3ZWVuIHJ1bnMgYW5kIHRoZW4gaXMgaW52YWxpZGF0ZSgpLWVkKSwgaW1tZWRpYXRlUmVuZGVyIGNvdWxkIGJlIHRydWUgYnV0IHRoZSBpbml0aWFsIGZvcmNlZC1yZW5kZXIgZ2V0cyBza2lwcGVkLCBzbyB0aGVyZSdzIG5vIG5lZWQgdG8gZm9yY2UgdGhlIHJlbmRlciBpbiB0aGlzIGNvbnRleHQgd2hlbiB0aGUgX3RpbWUgaXMgZ3JlYXRlciB0aGFuIDBcclxuXHRcdFx0XHRcdFx0aW1tZWRpYXRlID0gZmFsc2U7XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRwdCA9IHt9O1xyXG5cdFx0XHRcdFx0Zm9yIChwIGluIHYpIHsgLy9jb3B5IHByb3BzIGludG8gYSBuZXcgb2JqZWN0IGFuZCBza2lwIGFueSByZXNlcnZlZCBwcm9wcywgb3RoZXJ3aXNlIG9uQ29tcGxldGUgb3Igb25VcGRhdGUgb3Igb25TdGFydCBjb3VsZCBmaXJlLiBXZSBzaG91bGQsIGhvd2V2ZXIsIHBlcm1pdCBhdXRvQ1NTIHRvIGdvIHRocm91Z2guXHJcblx0XHRcdFx0XHRcdGlmICghX3Jlc2VydmVkUHJvcHNbcF0gfHwgcCA9PT0gXCJhdXRvQ1NTXCIpIHtcclxuXHRcdFx0XHRcdFx0XHRwdFtwXSA9IHZbcF07XHJcblx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdHB0Lm92ZXJ3cml0ZSA9IDA7XHJcblx0XHRcdFx0XHRwdC5kYXRhID0gXCJpc0Zyb21TdGFydFwiOyAvL3dlIHRhZyB0aGUgdHdlZW4gd2l0aCBhcyBcImlzRnJvbVN0YXJ0XCIgc28gdGhhdCBpZiBbaW5zaWRlIGEgcGx1Z2luXSB3ZSBuZWVkIHRvIG9ubHkgZG8gc29tZXRoaW5nIGF0IHRoZSB2ZXJ5IEVORCBvZiBhIHR3ZWVuLCB3ZSBoYXZlIGEgd2F5IG9mIGlkZW50aWZ5aW5nIHRoaXMgdHdlZW4gYXMgbWVyZWx5IHRoZSBvbmUgdGhhdCdzIHNldHRpbmcgdGhlIGJlZ2lubmluZyB2YWx1ZXMgZm9yIGEgXCJmcm9tKClcIiB0d2Vlbi4gRm9yIGV4YW1wbGUsIGNsZWFyUHJvcHMgaW4gQ1NTUGx1Z2luIHNob3VsZCBvbmx5IGdldCBhcHBsaWVkIGF0IHRoZSB2ZXJ5IEVORCBvZiBhIHR3ZWVuIGFuZCB3aXRob3V0IHRoaXMgdGFnLCBmcm9tKC4uLntoZWlnaHQ6MTAwLCBjbGVhclByb3BzOlwiaGVpZ2h0XCIsIGRlbGF5OjF9KSB3b3VsZCB3aXBlIHRoZSBoZWlnaHQgYXQgdGhlIGJlZ2lubmluZyBvZiB0aGUgdHdlZW4gYW5kIGFmdGVyIDEgc2Vjb25kLCBpdCdkIGtpY2sgYmFjayBpbi5cclxuXHRcdFx0XHRcdHB0LmxhenkgPSAoaW1tZWRpYXRlICYmIHYubGF6eSAhPT0gZmFsc2UpO1xyXG5cdFx0XHRcdFx0cHQuaW1tZWRpYXRlUmVuZGVyID0gaW1tZWRpYXRlOyAvL3plcm8tZHVyYXRpb24gdHdlZW5zIHJlbmRlciBpbW1lZGlhdGVseSBieSBkZWZhdWx0LCBidXQgaWYgd2UncmUgbm90IHNwZWNpZmljYWxseSBpbnN0cnVjdGVkIHRvIHJlbmRlciB0aGlzIHR3ZWVuIGltbWVkaWF0ZWx5LCB3ZSBzaG91bGQgc2tpcCB0aGlzIGFuZCBtZXJlbHkgX2luaXQoKSB0byByZWNvcmQgdGhlIHN0YXJ0aW5nIHZhbHVlcyAocmVuZGVyaW5nIHRoZW0gaW1tZWRpYXRlbHkgd291bGQgcHVzaCB0aGVtIHRvIGNvbXBsZXRpb24gd2hpY2ggaXMgd2FzdGVmdWwgaW4gdGhhdCBjYXNlIC0gd2UnZCBoYXZlIHRvIHJlbmRlcigtMSkgaW1tZWRpYXRlbHkgYWZ0ZXIpXHJcblx0XHRcdFx0XHR0aGlzLl9zdGFydEF0ID0gVHdlZW5MaXRlLnRvKHRoaXMudGFyZ2V0LCAwLCBwdCk7XHJcblx0XHRcdFx0XHRpZiAoIWltbWVkaWF0ZSkge1xyXG5cdFx0XHRcdFx0XHR0aGlzLl9zdGFydEF0Ll9pbml0KCk7IC8vZW5zdXJlcyB0aGF0IHRoZSBpbml0aWFsIHZhbHVlcyBhcmUgcmVjb3JkZWRcclxuXHRcdFx0XHRcdFx0dGhpcy5fc3RhcnRBdC5fZW5hYmxlZChmYWxzZSk7IC8vbm8gbmVlZCB0byBoYXZlIHRoZSB0d2VlbiByZW5kZXIgb24gdGhlIG5leHQgY3ljbGUuIERpc2FibGUgaXQgYmVjYXVzZSB3ZSdsbCBhbHdheXMgbWFudWFsbHkgY29udHJvbCB0aGUgcmVuZGVycyBvZiB0aGUgX3N0YXJ0QXQgdHdlZW4uXHJcblx0XHRcdFx0XHRcdGlmICh0aGlzLnZhcnMuaW1tZWRpYXRlUmVuZGVyKSB7XHJcblx0XHRcdFx0XHRcdFx0dGhpcy5fc3RhcnRBdCA9IG51bGw7XHJcblx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdH0gZWxzZSBpZiAodGhpcy5fdGltZSA9PT0gMCkge1xyXG5cdFx0XHRcdFx0XHRyZXR1cm47XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9XHJcblx0XHRcdHRoaXMuX2Vhc2UgPSBlYXNlID0gKCFlYXNlKSA/IFR3ZWVuTGl0ZS5kZWZhdWx0RWFzZSA6IChlYXNlIGluc3RhbmNlb2YgRWFzZSkgPyBlYXNlIDogKHR5cGVvZihlYXNlKSA9PT0gXCJmdW5jdGlvblwiKSA/IG5ldyBFYXNlKGVhc2UsIHYuZWFzZVBhcmFtcykgOiBfZWFzZU1hcFtlYXNlXSB8fCBUd2VlbkxpdGUuZGVmYXVsdEVhc2U7XHJcblx0XHRcdGlmICh2LmVhc2VQYXJhbXMgaW5zdGFuY2VvZiBBcnJheSAmJiBlYXNlLmNvbmZpZykge1xyXG5cdFx0XHRcdHRoaXMuX2Vhc2UgPSBlYXNlLmNvbmZpZy5hcHBseShlYXNlLCB2LmVhc2VQYXJhbXMpO1xyXG5cdFx0XHR9XHJcblx0XHRcdHRoaXMuX2Vhc2VUeXBlID0gdGhpcy5fZWFzZS5fdHlwZTtcclxuXHRcdFx0dGhpcy5fZWFzZVBvd2VyID0gdGhpcy5fZWFzZS5fcG93ZXI7XHJcblx0XHRcdHRoaXMuX2ZpcnN0UFQgPSBudWxsO1xyXG5cclxuXHRcdFx0aWYgKHRoaXMuX3RhcmdldHMpIHtcclxuXHRcdFx0XHRsID0gdGhpcy5fdGFyZ2V0cy5sZW5ndGg7XHJcblx0XHRcdFx0Zm9yIChpID0gMDsgaSA8IGw7IGkrKykge1xyXG5cdFx0XHRcdFx0aWYgKCB0aGlzLl9pbml0UHJvcHMoIHRoaXMuX3RhcmdldHNbaV0sICh0aGlzLl9wcm9wTG9va3VwW2ldID0ge30pLCB0aGlzLl9zaWJsaW5nc1tpXSwgKG9wID8gb3BbaV0gOiBudWxsKSwgaSkgKSB7XHJcblx0XHRcdFx0XHRcdGluaXRQbHVnaW5zID0gdHJ1ZTtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHR9XHJcblx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0aW5pdFBsdWdpbnMgPSB0aGlzLl9pbml0UHJvcHModGhpcy50YXJnZXQsIHRoaXMuX3Byb3BMb29rdXAsIHRoaXMuX3NpYmxpbmdzLCBvcCwgMCk7XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdGlmIChpbml0UGx1Z2lucykge1xyXG5cdFx0XHRcdFR3ZWVuTGl0ZS5fb25QbHVnaW5FdmVudChcIl9vbkluaXRBbGxQcm9wc1wiLCB0aGlzKTsgLy9yZW9yZGVycyB0aGUgYXJyYXkgaW4gb3JkZXIgb2YgcHJpb3JpdHkuIFVzZXMgYSBzdGF0aWMgVHdlZW5QbHVnaW4gbWV0aG9kIGluIG9yZGVyIHRvIG1pbmltaXplIGZpbGUgc2l6ZSBpbiBUd2VlbkxpdGVcclxuXHRcdFx0fVxyXG5cdFx0XHRpZiAob3ApIGlmICghdGhpcy5fZmlyc3RQVCkgaWYgKHR5cGVvZih0aGlzLnRhcmdldCkgIT09IFwiZnVuY3Rpb25cIikgeyAvL2lmIGFsbCB0d2VlbmluZyBwcm9wZXJ0aWVzIGhhdmUgYmVlbiBvdmVyd3JpdHRlbiwga2lsbCB0aGUgdHdlZW4uIElmIHRoZSB0YXJnZXQgaXMgYSBmdW5jdGlvbiwgaXQncyBwcm9iYWJseSBhIGRlbGF5ZWRDYWxsIHNvIGxldCBpdCBsaXZlLlxyXG5cdFx0XHRcdHRoaXMuX2VuYWJsZWQoZmFsc2UsIGZhbHNlKTtcclxuXHRcdFx0fVxyXG5cdFx0XHRpZiAodi5ydW5CYWNrd2FyZHMpIHtcclxuXHRcdFx0XHRwdCA9IHRoaXMuX2ZpcnN0UFQ7XHJcblx0XHRcdFx0d2hpbGUgKHB0KSB7XHJcblx0XHRcdFx0XHRwdC5zICs9IHB0LmM7XHJcblx0XHRcdFx0XHRwdC5jID0gLXB0LmM7XHJcblx0XHRcdFx0XHRwdCA9IHB0Ll9uZXh0O1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cdFx0XHR0aGlzLl9vblVwZGF0ZSA9IHYub25VcGRhdGU7XHJcblx0XHRcdHRoaXMuX2luaXR0ZWQgPSB0cnVlO1xyXG5cdFx0fTtcclxuXHJcblx0XHRwLl9pbml0UHJvcHMgPSBmdW5jdGlvbih0YXJnZXQsIHByb3BMb29rdXAsIHNpYmxpbmdzLCBvdmVyd3JpdHRlblByb3BzLCBpbmRleCkge1xyXG5cdFx0XHR2YXIgcCwgaSwgaW5pdFBsdWdpbnMsIHBsdWdpbiwgcHQsIHY7XHJcblx0XHRcdGlmICh0YXJnZXQgPT0gbnVsbCkge1xyXG5cdFx0XHRcdHJldHVybiBmYWxzZTtcclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0aWYgKF9sYXp5TG9va3VwW3RhcmdldC5fZ3NUd2VlbklEXSkge1xyXG5cdFx0XHRcdF9sYXp5UmVuZGVyKCk7IC8vaWYgb3RoZXIgdHdlZW5zIG9mIHRoZSBzYW1lIHRhcmdldCBoYXZlIHJlY2VudGx5IGluaXR0ZWQgYnV0IGhhdmVuJ3QgcmVuZGVyZWQgeWV0LCB3ZSd2ZSBnb3QgdG8gZm9yY2UgdGhlIHJlbmRlciBzbyB0aGF0IHRoZSBzdGFydGluZyB2YWx1ZXMgYXJlIGNvcnJlY3QgKGltYWdpbmUgcG9wdWxhdGluZyBhIHRpbWVsaW5lIHdpdGggYSBidW5jaCBvZiBzZXF1ZW50aWFsIHR3ZWVucyBhbmQgdGhlbiBqdW1waW5nIHRvIHRoZSBlbmQpXHJcblx0XHRcdH1cclxuXHJcblx0XHRcdGlmICghdGhpcy52YXJzLmNzcykgaWYgKHRhcmdldC5zdHlsZSkgaWYgKHRhcmdldCAhPT0gd2luZG93ICYmIHRhcmdldC5ub2RlVHlwZSkgaWYgKF9wbHVnaW5zLmNzcykgaWYgKHRoaXMudmFycy5hdXRvQ1NTICE9PSBmYWxzZSkgeyAvL2l0J3Mgc28gY29tbW9uIHRvIHVzZSBUd2VlbkxpdGUvTWF4IHRvIGFuaW1hdGUgdGhlIGNzcyBvZiBET00gZWxlbWVudHMsIHdlIGFzc3VtZSB0aGF0IGlmIHRoZSB0YXJnZXQgaXMgYSBET00gZWxlbWVudCwgdGhhdCdzIHdoYXQgaXMgaW50ZW5kZWQgKGEgY29udmVuaWVuY2Ugc28gdGhhdCB1c2VycyBkb24ndCBoYXZlIHRvIHdyYXAgdGhpbmdzIGluIGNzczp7fSwgYWx0aG91Z2ggd2Ugc3RpbGwgcmVjb21tZW5kIGl0IGZvciBhIHNsaWdodCBwZXJmb3JtYW5jZSBib29zdCBhbmQgYmV0dGVyIHNwZWNpZmljaXR5KS4gTm90ZTogd2UgY2Fubm90IGNoZWNrIFwibm9kZVR5cGVcIiBvbiB0aGUgd2luZG93IGluc2lkZSBhbiBpZnJhbWUuXHJcblx0XHRcdFx0X2F1dG9DU1ModGhpcy52YXJzLCB0YXJnZXQpO1xyXG5cdFx0XHR9XHJcblx0XHRcdGZvciAocCBpbiB0aGlzLnZhcnMpIHtcclxuXHRcdFx0XHR2ID0gdGhpcy52YXJzW3BdO1xyXG5cdFx0XHRcdGlmIChfcmVzZXJ2ZWRQcm9wc1twXSkge1xyXG5cdFx0XHRcdFx0aWYgKHYpIGlmICgodiBpbnN0YW5jZW9mIEFycmF5KSB8fCAodi5wdXNoICYmIF9pc0FycmF5KHYpKSkgaWYgKHYuam9pbihcIlwiKS5pbmRleE9mKFwie3NlbGZ9XCIpICE9PSAtMSkge1xyXG5cdFx0XHRcdFx0XHR0aGlzLnZhcnNbcF0gPSB2ID0gdGhpcy5fc3dhcFNlbGZJblBhcmFtcyh2LCB0aGlzKTtcclxuXHRcdFx0XHRcdH1cclxuXHJcblx0XHRcdFx0fSBlbHNlIGlmIChfcGx1Z2luc1twXSAmJiAocGx1Z2luID0gbmV3IF9wbHVnaW5zW3BdKCkpLl9vbkluaXRUd2Vlbih0YXJnZXQsIHRoaXMudmFyc1twXSwgdGhpcywgaW5kZXgpKSB7XHJcblxyXG5cdFx0XHRcdFx0Ly90IC0gdGFyZ2V0IFx0XHRbb2JqZWN0XVxyXG5cdFx0XHRcdFx0Ly9wIC0gcHJvcGVydHkgXHRcdFtzdHJpbmddXHJcblx0XHRcdFx0XHQvL3MgLSBzdGFydFx0XHRcdFtudW1iZXJdXHJcblx0XHRcdFx0XHQvL2MgLSBjaGFuZ2VcdFx0W251bWJlcl1cclxuXHRcdFx0XHRcdC8vZiAtIGlzRnVuY3Rpb25cdFtib29sZWFuXVxyXG5cdFx0XHRcdFx0Ly9uIC0gbmFtZVx0XHRcdFtzdHJpbmddXHJcblx0XHRcdFx0XHQvL3BnIC0gaXNQbHVnaW4gXHRbYm9vbGVhbl1cclxuXHRcdFx0XHRcdC8vcHIgLSBwcmlvcml0eVx0XHRbbnVtYmVyXVxyXG5cdFx0XHRcdFx0Ly9tIC0gbW9kICAgICAgICAgICBbZnVuY3Rpb24gfCAwXVxyXG5cdFx0XHRcdFx0dGhpcy5fZmlyc3RQVCA9IHB0ID0ge19uZXh0OnRoaXMuX2ZpcnN0UFQsIHQ6cGx1Z2luLCBwOlwic2V0UmF0aW9cIiwgczowLCBjOjEsIGY6MSwgbjpwLCBwZzoxLCBwcjpwbHVnaW4uX3ByaW9yaXR5LCBtOjB9O1xyXG5cdFx0XHRcdFx0aSA9IHBsdWdpbi5fb3ZlcndyaXRlUHJvcHMubGVuZ3RoO1xyXG5cdFx0XHRcdFx0d2hpbGUgKC0taSA+IC0xKSB7XHJcblx0XHRcdFx0XHRcdHByb3BMb29rdXBbcGx1Z2luLl9vdmVyd3JpdGVQcm9wc1tpXV0gPSB0aGlzLl9maXJzdFBUO1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0aWYgKHBsdWdpbi5fcHJpb3JpdHkgfHwgcGx1Z2luLl9vbkluaXRBbGxQcm9wcykge1xyXG5cdFx0XHRcdFx0XHRpbml0UGx1Z2lucyA9IHRydWU7XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRpZiAocGx1Z2luLl9vbkRpc2FibGUgfHwgcGx1Z2luLl9vbkVuYWJsZSkge1xyXG5cdFx0XHRcdFx0XHR0aGlzLl9ub3RpZnlQbHVnaW5zT2ZFbmFibGVkID0gdHJ1ZTtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdGlmIChwdC5fbmV4dCkge1xyXG5cdFx0XHRcdFx0XHRwdC5fbmV4dC5fcHJldiA9IHB0O1xyXG5cdFx0XHRcdFx0fVxyXG5cclxuXHRcdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdFx0cHJvcExvb2t1cFtwXSA9IF9hZGRQcm9wVHdlZW4uY2FsbCh0aGlzLCB0YXJnZXQsIHAsIFwiZ2V0XCIsIHYsIHAsIDAsIG51bGwsIHRoaXMudmFycy5zdHJpbmdGaWx0ZXIsIGluZGV4KTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdGlmIChvdmVyd3JpdHRlblByb3BzKSBpZiAodGhpcy5fa2lsbChvdmVyd3JpdHRlblByb3BzLCB0YXJnZXQpKSB7IC8vYW5vdGhlciB0d2VlbiBtYXkgaGF2ZSB0cmllZCB0byBvdmVyd3JpdGUgcHJvcGVydGllcyBvZiB0aGlzIHR3ZWVuIGJlZm9yZSBpbml0KCkgd2FzIGNhbGxlZCAobGlrZSBpZiB0d28gdHdlZW5zIHN0YXJ0IGF0IHRoZSBzYW1lIHRpbWUsIHRoZSBvbmUgY3JlYXRlZCBzZWNvbmQgd2lsbCBydW4gZmlyc3QpXHJcblx0XHRcdFx0cmV0dXJuIHRoaXMuX2luaXRQcm9wcyh0YXJnZXQsIHByb3BMb29rdXAsIHNpYmxpbmdzLCBvdmVyd3JpdHRlblByb3BzLCBpbmRleCk7XHJcblx0XHRcdH1cclxuXHRcdFx0aWYgKHRoaXMuX292ZXJ3cml0ZSA+IDEpIGlmICh0aGlzLl9maXJzdFBUKSBpZiAoc2libGluZ3MubGVuZ3RoID4gMSkgaWYgKF9hcHBseU92ZXJ3cml0ZSh0YXJnZXQsIHRoaXMsIHByb3BMb29rdXAsIHRoaXMuX292ZXJ3cml0ZSwgc2libGluZ3MpKSB7XHJcblx0XHRcdFx0dGhpcy5fa2lsbChwcm9wTG9va3VwLCB0YXJnZXQpO1xyXG5cdFx0XHRcdHJldHVybiB0aGlzLl9pbml0UHJvcHModGFyZ2V0LCBwcm9wTG9va3VwLCBzaWJsaW5ncywgb3ZlcndyaXR0ZW5Qcm9wcywgaW5kZXgpO1xyXG5cdFx0XHR9XHJcblx0XHRcdGlmICh0aGlzLl9maXJzdFBUKSBpZiAoKHRoaXMudmFycy5sYXp5ICE9PSBmYWxzZSAmJiB0aGlzLl9kdXJhdGlvbikgfHwgKHRoaXMudmFycy5sYXp5ICYmICF0aGlzLl9kdXJhdGlvbikpIHsgLy96ZXJvIGR1cmF0aW9uIHR3ZWVucyBkb24ndCBsYXp5IHJlbmRlciBieSBkZWZhdWx0OyBldmVyeXRoaW5nIGVsc2UgZG9lcy5cclxuXHRcdFx0XHRfbGF6eUxvb2t1cFt0YXJnZXQuX2dzVHdlZW5JRF0gPSB0cnVlO1xyXG5cdFx0XHR9XHJcblx0XHRcdHJldHVybiBpbml0UGx1Z2lucztcclxuXHRcdH07XHJcblxyXG5cdFx0cC5yZW5kZXIgPSBmdW5jdGlvbih0aW1lLCBzdXBwcmVzc0V2ZW50cywgZm9yY2UpIHtcclxuXHRcdFx0dmFyIHByZXZUaW1lID0gdGhpcy5fdGltZSxcclxuXHRcdFx0XHRkdXJhdGlvbiA9IHRoaXMuX2R1cmF0aW9uLFxyXG5cdFx0XHRcdHByZXZSYXdQcmV2VGltZSA9IHRoaXMuX3Jhd1ByZXZUaW1lLFxyXG5cdFx0XHRcdGlzQ29tcGxldGUsIGNhbGxiYWNrLCBwdCwgcmF3UHJldlRpbWU7XHJcblx0XHRcdGlmICh0aW1lID49IGR1cmF0aW9uIC0gMC4wMDAwMDAxICYmIHRpbWUgPj0gMCkgeyAvL3RvIHdvcmsgYXJvdW5kIG9jY2FzaW9uYWwgZmxvYXRpbmcgcG9pbnQgbWF0aCBhcnRpZmFjdHMuXHJcblx0XHRcdFx0dGhpcy5fdG90YWxUaW1lID0gdGhpcy5fdGltZSA9IGR1cmF0aW9uO1xyXG5cdFx0XHRcdHRoaXMucmF0aW8gPSB0aGlzLl9lYXNlLl9jYWxjRW5kID8gdGhpcy5fZWFzZS5nZXRSYXRpbygxKSA6IDE7XHJcblx0XHRcdFx0aWYgKCF0aGlzLl9yZXZlcnNlZCApIHtcclxuXHRcdFx0XHRcdGlzQ29tcGxldGUgPSB0cnVlO1xyXG5cdFx0XHRcdFx0Y2FsbGJhY2sgPSBcIm9uQ29tcGxldGVcIjtcclxuXHRcdFx0XHRcdGZvcmNlID0gKGZvcmNlIHx8IHRoaXMuX3RpbWVsaW5lLmF1dG9SZW1vdmVDaGlsZHJlbik7IC8vb3RoZXJ3aXNlLCBpZiB0aGUgYW5pbWF0aW9uIGlzIHVucGF1c2VkL2FjdGl2YXRlZCBhZnRlciBpdCdzIGFscmVhZHkgZmluaXNoZWQsIGl0IGRvZXNuJ3QgZ2V0IHJlbW92ZWQgZnJvbSB0aGUgcGFyZW50IHRpbWVsaW5lLlxyXG5cdFx0XHRcdH1cclxuXHRcdFx0XHRpZiAoZHVyYXRpb24gPT09IDApIGlmICh0aGlzLl9pbml0dGVkIHx8ICF0aGlzLnZhcnMubGF6eSB8fCBmb3JjZSkgeyAvL3plcm8tZHVyYXRpb24gdHdlZW5zIGFyZSB0cmlja3kgYmVjYXVzZSB3ZSBtdXN0IGRpc2Nlcm4gdGhlIG1vbWVudHVtL2RpcmVjdGlvbiBvZiB0aW1lIGluIG9yZGVyIHRvIGRldGVybWluZSB3aGV0aGVyIHRoZSBzdGFydGluZyB2YWx1ZXMgc2hvdWxkIGJlIHJlbmRlcmVkIG9yIHRoZSBlbmRpbmcgdmFsdWVzLiBJZiB0aGUgXCJwbGF5aGVhZFwiIG9mIGl0cyB0aW1lbGluZSBnb2VzIHBhc3QgdGhlIHplcm8tZHVyYXRpb24gdHdlZW4gaW4gdGhlIGZvcndhcmQgZGlyZWN0aW9uIG9yIGxhbmRzIGRpcmVjdGx5IG9uIGl0LCB0aGUgZW5kIHZhbHVlcyBzaG91bGQgYmUgcmVuZGVyZWQsIGJ1dCBpZiB0aGUgdGltZWxpbmUncyBcInBsYXloZWFkXCIgbW92ZXMgcGFzdCBpdCBpbiB0aGUgYmFja3dhcmQgZGlyZWN0aW9uIChmcm9tIGEgcG9zdGl0aXZlIHRpbWUgdG8gYSBuZWdhdGl2ZSB0aW1lKSwgdGhlIHN0YXJ0aW5nIHZhbHVlcyBtdXN0IGJlIHJlbmRlcmVkLlxyXG5cdFx0XHRcdFx0aWYgKHRoaXMuX3N0YXJ0VGltZSA9PT0gdGhpcy5fdGltZWxpbmUuX2R1cmF0aW9uKSB7IC8vaWYgYSB6ZXJvLWR1cmF0aW9uIHR3ZWVuIGlzIGF0IHRoZSBWRVJZIGVuZCBvZiBhIHRpbWVsaW5lIGFuZCB0aGF0IHRpbWVsaW5lIHJlbmRlcnMgYXQgaXRzIGVuZCwgaXQgd2lsbCB0eXBpY2FsbHkgYWRkIGEgdGlueSBiaXQgb2YgY3VzaGlvbiB0byB0aGUgcmVuZGVyIHRpbWUgdG8gcHJldmVudCByb3VuZGluZyBlcnJvcnMgZnJvbSBnZXR0aW5nIGluIHRoZSB3YXkgb2YgdHdlZW5zIHJlbmRlcmluZyB0aGVpciBWRVJZIGVuZC4gSWYgd2UgdGhlbiByZXZlcnNlKCkgdGhhdCB0aW1lbGluZSwgdGhlIHplcm8tZHVyYXRpb24gdHdlZW4gd2lsbCB0cmlnZ2VyIGl0cyBvblJldmVyc2VDb21wbGV0ZSBldmVuIHRob3VnaCB0ZWNobmljYWxseSB0aGUgcGxheWhlYWQgZGlkbid0IHBhc3Mgb3ZlciBpdCBhZ2Fpbi4gSXQncyBhIHZlcnkgc3BlY2lmaWMgZWRnZSBjYXNlIHdlIG11c3QgYWNjb21tb2RhdGUuXHJcblx0XHRcdFx0XHRcdHRpbWUgPSAwO1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0aWYgKHByZXZSYXdQcmV2VGltZSA8IDAgfHwgKHRpbWUgPD0gMCAmJiB0aW1lID49IC0wLjAwMDAwMDEpIHx8IChwcmV2UmF3UHJldlRpbWUgPT09IF90aW55TnVtICYmIHRoaXMuZGF0YSAhPT0gXCJpc1BhdXNlXCIpKSBpZiAocHJldlJhd1ByZXZUaW1lICE9PSB0aW1lKSB7IC8vbm90ZTogd2hlbiB0aGlzLmRhdGEgaXMgXCJpc1BhdXNlXCIsIGl0J3MgYSBjYWxsYmFjayBhZGRlZCBieSBhZGRQYXVzZSgpIG9uIGEgdGltZWxpbmUgdGhhdCB3ZSBzaG91bGQgbm90IGJlIHRyaWdnZXJlZCB3aGVuIExFQVZJTkcgaXRzIGV4YWN0IHN0YXJ0IHRpbWUuIEluIG90aGVyIHdvcmRzLCB0bC5hZGRQYXVzZSgxKS5wbGF5KDEpIHNob3VsZG4ndCBwYXVzZS5cclxuXHRcdFx0XHRcdFx0Zm9yY2UgPSB0cnVlO1xyXG5cdFx0XHRcdFx0XHRpZiAocHJldlJhd1ByZXZUaW1lID4gX3RpbnlOdW0pIHtcclxuXHRcdFx0XHRcdFx0XHRjYWxsYmFjayA9IFwib25SZXZlcnNlQ29tcGxldGVcIjtcclxuXHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0dGhpcy5fcmF3UHJldlRpbWUgPSByYXdQcmV2VGltZSA9ICghc3VwcHJlc3NFdmVudHMgfHwgdGltZSB8fCBwcmV2UmF3UHJldlRpbWUgPT09IHRpbWUpID8gdGltZSA6IF90aW55TnVtOyAvL3doZW4gdGhlIHBsYXloZWFkIGFycml2ZXMgYXQgRVhBQ1RMWSB0aW1lIDAgKHJpZ2h0IG9uIHRvcCkgb2YgYSB6ZXJvLWR1cmF0aW9uIHR3ZWVuLCB3ZSBuZWVkIHRvIGRpc2Nlcm4gaWYgZXZlbnRzIGFyZSBzdXBwcmVzc2VkIHNvIHRoYXQgd2hlbiB0aGUgcGxheWhlYWQgbW92ZXMgYWdhaW4gKG5leHQgdGltZSksIGl0J2xsIHRyaWdnZXIgdGhlIGNhbGxiYWNrLiBJZiBldmVudHMgYXJlIE5PVCBzdXBwcmVzc2VkLCBvYnZpb3VzbHkgdGhlIGNhbGxiYWNrIHdvdWxkIGJlIHRyaWdnZXJlZCBpbiB0aGlzIHJlbmRlci4gQmFzaWNhbGx5LCB0aGUgY2FsbGJhY2sgc2hvdWxkIGZpcmUgZWl0aGVyIHdoZW4gdGhlIHBsYXloZWFkIEFSUklWRVMgb3IgTEVBVkVTIHRoaXMgZXhhY3Qgc3BvdCwgbm90IGJvdGguIEltYWdpbmUgZG9pbmcgYSB0aW1lbGluZS5zZWVrKDApIGFuZCB0aGVyZSdzIGEgY2FsbGJhY2sgdGhhdCBzaXRzIGF0IDAuIFNpbmNlIGV2ZW50cyBhcmUgc3VwcHJlc3NlZCBvbiB0aGF0IHNlZWsoKSBieSBkZWZhdWx0LCBub3RoaW5nIHdpbGwgZmlyZSwgYnV0IHdoZW4gdGhlIHBsYXloZWFkIG1vdmVzIG9mZiBvZiB0aGF0IHBvc2l0aW9uLCB0aGUgY2FsbGJhY2sgc2hvdWxkIGZpcmUuIFRoaXMgYmVoYXZpb3IgaXMgd2hhdCBwZW9wbGUgaW50dWl0aXZlbHkgZXhwZWN0LiBXZSBzZXQgdGhlIF9yYXdQcmV2VGltZSB0byBiZSBhIHByZWNpc2UgdGlueSBudW1iZXIgdG8gaW5kaWNhdGUgdGhpcyBzY2VuYXJpbyByYXRoZXIgdGhhbiB1c2luZyBhbm90aGVyIHByb3BlcnR5L3ZhcmlhYmxlIHdoaWNoIHdvdWxkIGluY3JlYXNlIG1lbW9yeSB1c2FnZS4gVGhpcyB0ZWNobmlxdWUgaXMgbGVzcyByZWFkYWJsZSwgYnV0IG1vcmUgZWZmaWNpZW50LlxyXG5cdFx0XHRcdH1cclxuXHJcblx0XHRcdH0gZWxzZSBpZiAodGltZSA8IDAuMDAwMDAwMSkgeyAvL3RvIHdvcmsgYXJvdW5kIG9jY2FzaW9uYWwgZmxvYXRpbmcgcG9pbnQgbWF0aCBhcnRpZmFjdHMsIHJvdW5kIHN1cGVyIHNtYWxsIHZhbHVlcyB0byAwLlxyXG5cdFx0XHRcdHRoaXMuX3RvdGFsVGltZSA9IHRoaXMuX3RpbWUgPSAwO1xyXG5cdFx0XHRcdHRoaXMucmF0aW8gPSB0aGlzLl9lYXNlLl9jYWxjRW5kID8gdGhpcy5fZWFzZS5nZXRSYXRpbygwKSA6IDA7XHJcblx0XHRcdFx0aWYgKHByZXZUaW1lICE9PSAwIHx8IChkdXJhdGlvbiA9PT0gMCAmJiBwcmV2UmF3UHJldlRpbWUgPiAwKSkge1xyXG5cdFx0XHRcdFx0Y2FsbGJhY2sgPSBcIm9uUmV2ZXJzZUNvbXBsZXRlXCI7XHJcblx0XHRcdFx0XHRpc0NvbXBsZXRlID0gdGhpcy5fcmV2ZXJzZWQ7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHRcdGlmICh0aW1lIDwgMCkge1xyXG5cdFx0XHRcdFx0dGhpcy5fYWN0aXZlID0gZmFsc2U7XHJcblx0XHRcdFx0XHRpZiAoZHVyYXRpb24gPT09IDApIGlmICh0aGlzLl9pbml0dGVkIHx8ICF0aGlzLnZhcnMubGF6eSB8fCBmb3JjZSkgeyAvL3plcm8tZHVyYXRpb24gdHdlZW5zIGFyZSB0cmlja3kgYmVjYXVzZSB3ZSBtdXN0IGRpc2Nlcm4gdGhlIG1vbWVudHVtL2RpcmVjdGlvbiBvZiB0aW1lIGluIG9yZGVyIHRvIGRldGVybWluZSB3aGV0aGVyIHRoZSBzdGFydGluZyB2YWx1ZXMgc2hvdWxkIGJlIHJlbmRlcmVkIG9yIHRoZSBlbmRpbmcgdmFsdWVzLiBJZiB0aGUgXCJwbGF5aGVhZFwiIG9mIGl0cyB0aW1lbGluZSBnb2VzIHBhc3QgdGhlIHplcm8tZHVyYXRpb24gdHdlZW4gaW4gdGhlIGZvcndhcmQgZGlyZWN0aW9uIG9yIGxhbmRzIGRpcmVjdGx5IG9uIGl0LCB0aGUgZW5kIHZhbHVlcyBzaG91bGQgYmUgcmVuZGVyZWQsIGJ1dCBpZiB0aGUgdGltZWxpbmUncyBcInBsYXloZWFkXCIgbW92ZXMgcGFzdCBpdCBpbiB0aGUgYmFja3dhcmQgZGlyZWN0aW9uIChmcm9tIGEgcG9zdGl0aXZlIHRpbWUgdG8gYSBuZWdhdGl2ZSB0aW1lKSwgdGhlIHN0YXJ0aW5nIHZhbHVlcyBtdXN0IGJlIHJlbmRlcmVkLlxyXG5cdFx0XHRcdFx0XHRpZiAocHJldlJhd1ByZXZUaW1lID49IDAgJiYgIShwcmV2UmF3UHJldlRpbWUgPT09IF90aW55TnVtICYmIHRoaXMuZGF0YSA9PT0gXCJpc1BhdXNlXCIpKSB7XHJcblx0XHRcdFx0XHRcdFx0Zm9yY2UgPSB0cnVlO1xyXG5cdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRcdHRoaXMuX3Jhd1ByZXZUaW1lID0gcmF3UHJldlRpbWUgPSAoIXN1cHByZXNzRXZlbnRzIHx8IHRpbWUgfHwgcHJldlJhd1ByZXZUaW1lID09PSB0aW1lKSA/IHRpbWUgOiBfdGlueU51bTsgLy93aGVuIHRoZSBwbGF5aGVhZCBhcnJpdmVzIGF0IEVYQUNUTFkgdGltZSAwIChyaWdodCBvbiB0b3ApIG9mIGEgemVyby1kdXJhdGlvbiB0d2Vlbiwgd2UgbmVlZCB0byBkaXNjZXJuIGlmIGV2ZW50cyBhcmUgc3VwcHJlc3NlZCBzbyB0aGF0IHdoZW4gdGhlIHBsYXloZWFkIG1vdmVzIGFnYWluIChuZXh0IHRpbWUpLCBpdCdsbCB0cmlnZ2VyIHRoZSBjYWxsYmFjay4gSWYgZXZlbnRzIGFyZSBOT1Qgc3VwcHJlc3NlZCwgb2J2aW91c2x5IHRoZSBjYWxsYmFjayB3b3VsZCBiZSB0cmlnZ2VyZWQgaW4gdGhpcyByZW5kZXIuIEJhc2ljYWxseSwgdGhlIGNhbGxiYWNrIHNob3VsZCBmaXJlIGVpdGhlciB3aGVuIHRoZSBwbGF5aGVhZCBBUlJJVkVTIG9yIExFQVZFUyB0aGlzIGV4YWN0IHNwb3QsIG5vdCBib3RoLiBJbWFnaW5lIGRvaW5nIGEgdGltZWxpbmUuc2VlaygwKSBhbmQgdGhlcmUncyBhIGNhbGxiYWNrIHRoYXQgc2l0cyBhdCAwLiBTaW5jZSBldmVudHMgYXJlIHN1cHByZXNzZWQgb24gdGhhdCBzZWVrKCkgYnkgZGVmYXVsdCwgbm90aGluZyB3aWxsIGZpcmUsIGJ1dCB3aGVuIHRoZSBwbGF5aGVhZCBtb3ZlcyBvZmYgb2YgdGhhdCBwb3NpdGlvbiwgdGhlIGNhbGxiYWNrIHNob3VsZCBmaXJlLiBUaGlzIGJlaGF2aW9yIGlzIHdoYXQgcGVvcGxlIGludHVpdGl2ZWx5IGV4cGVjdC4gV2Ugc2V0IHRoZSBfcmF3UHJldlRpbWUgdG8gYmUgYSBwcmVjaXNlIHRpbnkgbnVtYmVyIHRvIGluZGljYXRlIHRoaXMgc2NlbmFyaW8gcmF0aGVyIHRoYW4gdXNpbmcgYW5vdGhlciBwcm9wZXJ0eS92YXJpYWJsZSB3aGljaCB3b3VsZCBpbmNyZWFzZSBtZW1vcnkgdXNhZ2UuIFRoaXMgdGVjaG5pcXVlIGlzIGxlc3MgcmVhZGFibGUsIGJ1dCBtb3JlIGVmZmljaWVudC5cclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0aWYgKCF0aGlzLl9pbml0dGVkIHx8ICh0aGlzLl9zdGFydEF0ICYmIHRoaXMuX3N0YXJ0QXQucHJvZ3Jlc3MoKSkpIHsgLy9pZiB3ZSByZW5kZXIgdGhlIHZlcnkgYmVnaW5uaW5nICh0aW1lID09IDApIG9mIGEgZnJvbVRvKCksIHdlIG11c3QgZm9yY2UgdGhlIHJlbmRlciAobm9ybWFsIHR3ZWVucyB3b3VsZG4ndCBuZWVkIHRvIHJlbmRlciBhdCBhIHRpbWUgb2YgMCB3aGVuIHRoZSBwcmV2VGltZSB3YXMgYWxzbyAwKS4gVGhpcyBpcyBhbHNvIG1hbmRhdG9yeSB0byBtYWtlIHN1cmUgb3ZlcndyaXRpbmcga2lja3MgaW4gaW1tZWRpYXRlbHkuIEFsc28sIHdlIGNoZWNrIHByb2dyZXNzKCkgYmVjYXVzZSBpZiBzdGFydEF0IGhhcyBhbHJlYWR5IHJlbmRlcmVkIGF0IGl0cyBlbmQsIHdlIHNob3VsZCBmb3JjZSBhIHJlbmRlciBhdCBpdHMgYmVnaW5uaW5nLiBPdGhlcndpc2UsIGlmIHlvdSBwdXQgdGhlIHBsYXloZWFkIGRpcmVjdGx5IG9uIHRvcCBvZiB3aGVyZSBhIGZyb21Ubyh7aW1tZWRpYXRlUmVuZGVyOmZhbHNlfSkgc3RhcnRzLCBhbmQgdGhlbiBtb3ZlIGl0IGJhY2t3YXJkcywgdGhlIGZyb20oKSB3b24ndCByZXZlcnQgaXRzIHZhbHVlcy5cclxuXHRcdFx0XHRcdGZvcmNlID0gdHJ1ZTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0dGhpcy5fdG90YWxUaW1lID0gdGhpcy5fdGltZSA9IHRpbWU7XHJcblxyXG5cdFx0XHRcdGlmICh0aGlzLl9lYXNlVHlwZSkge1xyXG5cdFx0XHRcdFx0dmFyIHIgPSB0aW1lIC8gZHVyYXRpb24sIHR5cGUgPSB0aGlzLl9lYXNlVHlwZSwgcG93ID0gdGhpcy5fZWFzZVBvd2VyO1xyXG5cdFx0XHRcdFx0aWYgKHR5cGUgPT09IDEgfHwgKHR5cGUgPT09IDMgJiYgciA+PSAwLjUpKSB7XHJcblx0XHRcdFx0XHRcdHIgPSAxIC0gcjtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdGlmICh0eXBlID09PSAzKSB7XHJcblx0XHRcdFx0XHRcdHIgKj0gMjtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdGlmIChwb3cgPT09IDEpIHtcclxuXHRcdFx0XHRcdFx0ciAqPSByO1xyXG5cdFx0XHRcdFx0fSBlbHNlIGlmIChwb3cgPT09IDIpIHtcclxuXHRcdFx0XHRcdFx0ciAqPSByICogcjtcclxuXHRcdFx0XHRcdH0gZWxzZSBpZiAocG93ID09PSAzKSB7XHJcblx0XHRcdFx0XHRcdHIgKj0gciAqIHIgKiByO1xyXG5cdFx0XHRcdFx0fSBlbHNlIGlmIChwb3cgPT09IDQpIHtcclxuXHRcdFx0XHRcdFx0ciAqPSByICogciAqIHIgKiByO1xyXG5cdFx0XHRcdFx0fVxyXG5cclxuXHRcdFx0XHRcdGlmICh0eXBlID09PSAxKSB7XHJcblx0XHRcdFx0XHRcdHRoaXMucmF0aW8gPSAxIC0gcjtcclxuXHRcdFx0XHRcdH0gZWxzZSBpZiAodHlwZSA9PT0gMikge1xyXG5cdFx0XHRcdFx0XHR0aGlzLnJhdGlvID0gcjtcclxuXHRcdFx0XHRcdH0gZWxzZSBpZiAodGltZSAvIGR1cmF0aW9uIDwgMC41KSB7XHJcblx0XHRcdFx0XHRcdHRoaXMucmF0aW8gPSByIC8gMjtcclxuXHRcdFx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0XHRcdHRoaXMucmF0aW8gPSAxIC0gKHIgLyAyKTtcclxuXHRcdFx0XHRcdH1cclxuXHJcblx0XHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHRcdHRoaXMucmF0aW8gPSB0aGlzLl9lYXNlLmdldFJhdGlvKHRpbWUgLyBkdXJhdGlvbik7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHRpZiAodGhpcy5fdGltZSA9PT0gcHJldlRpbWUgJiYgIWZvcmNlKSB7XHJcblx0XHRcdFx0cmV0dXJuO1xyXG5cdFx0XHR9IGVsc2UgaWYgKCF0aGlzLl9pbml0dGVkKSB7XHJcblx0XHRcdFx0dGhpcy5faW5pdCgpO1xyXG5cdFx0XHRcdGlmICghdGhpcy5faW5pdHRlZCB8fCB0aGlzLl9nYykgeyAvL2ltbWVkaWF0ZVJlbmRlciB0d2VlbnMgdHlwaWNhbGx5IHdvbid0IGluaXRpYWxpemUgdW50aWwgdGhlIHBsYXloZWFkIGFkdmFuY2VzIChfdGltZSBpcyBncmVhdGVyIHRoYW4gMCkgaW4gb3JkZXIgdG8gZW5zdXJlIHRoYXQgb3ZlcndyaXRpbmcgb2NjdXJzIHByb3Blcmx5LiBBbHNvLCBpZiBhbGwgb2YgdGhlIHR3ZWVuaW5nIHByb3BlcnRpZXMgaGF2ZSBiZWVuIG92ZXJ3cml0dGVuICh3aGljaCB3b3VsZCBjYXVzZSBfZ2MgdG8gYmUgdHJ1ZSwgYXMgc2V0IGluIF9pbml0KCkpLCB3ZSBzaG91bGRuJ3QgY29udGludWUgb3RoZXJ3aXNlIGFuIG9uU3RhcnQgY2FsbGJhY2sgY291bGQgYmUgY2FsbGVkIGZvciBleGFtcGxlLlxyXG5cdFx0XHRcdFx0cmV0dXJuO1xyXG5cdFx0XHRcdH0gZWxzZSBpZiAoIWZvcmNlICYmIHRoaXMuX2ZpcnN0UFQgJiYgKCh0aGlzLnZhcnMubGF6eSAhPT0gZmFsc2UgJiYgdGhpcy5fZHVyYXRpb24pIHx8ICh0aGlzLnZhcnMubGF6eSAmJiAhdGhpcy5fZHVyYXRpb24pKSkge1xyXG5cdFx0XHRcdFx0dGhpcy5fdGltZSA9IHRoaXMuX3RvdGFsVGltZSA9IHByZXZUaW1lO1xyXG5cdFx0XHRcdFx0dGhpcy5fcmF3UHJldlRpbWUgPSBwcmV2UmF3UHJldlRpbWU7XHJcblx0XHRcdFx0XHRfbGF6eVR3ZWVucy5wdXNoKHRoaXMpO1xyXG5cdFx0XHRcdFx0dGhpcy5fbGF6eSA9IFt0aW1lLCBzdXBwcmVzc0V2ZW50c107XHJcblx0XHRcdFx0XHRyZXR1cm47XHJcblx0XHRcdFx0fVxyXG5cdFx0XHRcdC8vX2Vhc2UgaXMgaW5pdGlhbGx5IHNldCB0byBkZWZhdWx0RWFzZSwgc28gbm93IHRoYXQgaW5pdCgpIGhhcyBydW4sIF9lYXNlIGlzIHNldCBwcm9wZXJseSBhbmQgd2UgbmVlZCB0byByZWNhbGN1bGF0ZSB0aGUgcmF0aW8uIE92ZXJhbGwgdGhpcyBpcyBmYXN0ZXIgdGhhbiB1c2luZyBjb25kaXRpb25hbCBsb2dpYyBlYXJsaWVyIGluIHRoZSBtZXRob2QgdG8gYXZvaWQgaGF2aW5nIHRvIHNldCByYXRpbyB0d2ljZSBiZWNhdXNlIHdlIG9ubHkgaW5pdCgpIG9uY2UgYnV0IHJlbmRlclRpbWUoKSBnZXRzIGNhbGxlZCBWRVJZIGZyZXF1ZW50bHkuXHJcblx0XHRcdFx0aWYgKHRoaXMuX3RpbWUgJiYgIWlzQ29tcGxldGUpIHtcclxuXHRcdFx0XHRcdHRoaXMucmF0aW8gPSB0aGlzLl9lYXNlLmdldFJhdGlvKHRoaXMuX3RpbWUgLyBkdXJhdGlvbik7XHJcblx0XHRcdFx0fSBlbHNlIGlmIChpc0NvbXBsZXRlICYmIHRoaXMuX2Vhc2UuX2NhbGNFbmQpIHtcclxuXHRcdFx0XHRcdHRoaXMucmF0aW8gPSB0aGlzLl9lYXNlLmdldFJhdGlvKCh0aGlzLl90aW1lID09PSAwKSA/IDAgOiAxKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH1cclxuXHRcdFx0aWYgKHRoaXMuX2xhenkgIT09IGZhbHNlKSB7IC8vaW4gY2FzZSBhIGxhenkgcmVuZGVyIGlzIHBlbmRpbmcsIHdlIHNob3VsZCBmbHVzaCBpdCBiZWNhdXNlIHRoZSBuZXcgcmVuZGVyIGlzIG9jY3VycmluZyBub3cgKGltYWdpbmUgYSBsYXp5IHR3ZWVuIGluc3RhbnRpYXRpbmcgYW5kIHRoZW4gaW1tZWRpYXRlbHkgdGhlIHVzZXIgY2FsbHMgdHdlZW4uc2Vlayh0d2Vlbi5kdXJhdGlvbigpKSwgc2tpcHBpbmcgdG8gdGhlIGVuZCAtIHRoZSBlbmQgcmVuZGVyIHdvdWxkIGJlIGZvcmNlZCwgYW5kIHRoZW4gaWYgd2UgZGlkbid0IGZsdXNoIHRoZSBsYXp5IHJlbmRlciwgaXQnZCBmaXJlIEFGVEVSIHRoZSBzZWVrKCksIHJlbmRlcmluZyBpdCBhdCB0aGUgd3JvbmcgdGltZS5cclxuXHRcdFx0XHR0aGlzLl9sYXp5ID0gZmFsc2U7XHJcblx0XHRcdH1cclxuXHRcdFx0aWYgKCF0aGlzLl9hY3RpdmUpIGlmICghdGhpcy5fcGF1c2VkICYmIHRoaXMuX3RpbWUgIT09IHByZXZUaW1lICYmIHRpbWUgPj0gMCkge1xyXG5cdFx0XHRcdHRoaXMuX2FjdGl2ZSA9IHRydWU7ICAvL3NvIHRoYXQgaWYgdGhlIHVzZXIgcmVuZGVycyBhIHR3ZWVuIChhcyBvcHBvc2VkIHRvIHRoZSB0aW1lbGluZSByZW5kZXJpbmcgaXQpLCB0aGUgdGltZWxpbmUgaXMgZm9yY2VkIHRvIHJlLXJlbmRlciBhbmQgYWxpZ24gaXQgd2l0aCB0aGUgcHJvcGVyIHRpbWUvZnJhbWUgb24gdGhlIG5leHQgcmVuZGVyaW5nIGN5Y2xlLiBNYXliZSB0aGUgdHdlZW4gYWxyZWFkeSBmaW5pc2hlZCBidXQgdGhlIHVzZXIgbWFudWFsbHkgcmUtcmVuZGVycyBpdCBhcyBoYWxmd2F5IGRvbmUuXHJcblx0XHRcdH1cclxuXHRcdFx0aWYgKHByZXZUaW1lID09PSAwKSB7XHJcblx0XHRcdFx0aWYgKHRoaXMuX3N0YXJ0QXQpIHtcclxuXHRcdFx0XHRcdGlmICh0aW1lID49IDApIHtcclxuXHRcdFx0XHRcdFx0dGhpcy5fc3RhcnRBdC5yZW5kZXIodGltZSwgdHJ1ZSwgZm9yY2UpO1xyXG5cdFx0XHRcdFx0fSBlbHNlIGlmICghY2FsbGJhY2spIHtcclxuXHRcdFx0XHRcdFx0Y2FsbGJhY2sgPSBcIl9kdW1teUdTXCI7IC8vaWYgbm8gY2FsbGJhY2sgaXMgZGVmaW5lZCwgdXNlIGEgZHVtbXkgdmFsdWUganVzdCBzbyB0aGF0IHRoZSBjb25kaXRpb24gYXQgdGhlIGVuZCBldmFsdWF0ZXMgYXMgdHJ1ZSBiZWNhdXNlIF9zdGFydEF0IHNob3VsZCByZW5kZXIgQUZURVIgdGhlIG5vcm1hbCByZW5kZXIgbG9vcCB3aGVuIHRoZSB0aW1lIGlzIG5lZ2F0aXZlLiBXZSBjb3VsZCBoYW5kbGUgdGhpcyBpbiBhIG1vcmUgaW50dWl0aXZlIHdheSwgb2YgY291cnNlLCBidXQgdGhlIHJlbmRlciBsb29wIGlzIHRoZSBNT1NUIGltcG9ydGFudCB0aGluZyB0byBvcHRpbWl6ZSwgc28gdGhpcyB0ZWNobmlxdWUgYWxsb3dzIHVzIHRvIGF2b2lkIGFkZGluZyBleHRyYSBjb25kaXRpb25hbCBsb2dpYyBpbiBhIGhpZ2gtZnJlcXVlbmN5IGFyZWEuXHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0fVxyXG5cdFx0XHRcdGlmICh0aGlzLnZhcnMub25TdGFydCkgaWYgKHRoaXMuX3RpbWUgIT09IDAgfHwgZHVyYXRpb24gPT09IDApIGlmICghc3VwcHJlc3NFdmVudHMpIHtcclxuXHRcdFx0XHRcdHRoaXMuX2NhbGxiYWNrKFwib25TdGFydFwiKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH1cclxuXHRcdFx0cHQgPSB0aGlzLl9maXJzdFBUO1xyXG5cdFx0XHR3aGlsZSAocHQpIHtcclxuXHRcdFx0XHRpZiAocHQuZikge1xyXG5cdFx0XHRcdFx0cHQudFtwdC5wXShwdC5jICogdGhpcy5yYXRpbyArIHB0LnMpO1xyXG5cdFx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0XHRwdC50W3B0LnBdID0gcHQuYyAqIHRoaXMucmF0aW8gKyBwdC5zO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0XHRwdCA9IHB0Ll9uZXh0O1xyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHRpZiAodGhpcy5fb25VcGRhdGUpIHtcclxuXHRcdFx0XHRpZiAodGltZSA8IDApIGlmICh0aGlzLl9zdGFydEF0ICYmIHRpbWUgIT09IC0wLjAwMDEpIHsgLy9pZiB0aGUgdHdlZW4gaXMgcG9zaXRpb25lZCBhdCB0aGUgVkVSWSBiZWdpbm5pbmcgKF9zdGFydFRpbWUgMCkgb2YgaXRzIHBhcmVudCB0aW1lbGluZSwgaXQncyBpbGxlZ2FsIGZvciB0aGUgcGxheWhlYWQgdG8gZ28gYmFjayBmdXJ0aGVyLCBzbyB3ZSBzaG91bGQgbm90IHJlbmRlciB0aGUgcmVjb3JkZWQgc3RhcnRBdCB2YWx1ZXMuXHJcblx0XHRcdFx0XHR0aGlzLl9zdGFydEF0LnJlbmRlcih0aW1lLCB0cnVlLCBmb3JjZSk7IC8vbm90ZTogZm9yIHBlcmZvcm1hbmNlIHJlYXNvbnMsIHdlIHR1Y2sgdGhpcyBjb25kaXRpb25hbCBsb2dpYyBpbnNpZGUgbGVzcyB0cmF2ZWxlZCBhcmVhcyAobW9zdCB0d2VlbnMgZG9uJ3QgaGF2ZSBhbiBvblVwZGF0ZSkuIFdlJ2QganVzdCBoYXZlIGl0IGF0IHRoZSBlbmQgYmVmb3JlIHRoZSBvbkNvbXBsZXRlLCBidXQgdGhlIHZhbHVlcyBzaG91bGQgYmUgdXBkYXRlZCBiZWZvcmUgYW55IG9uVXBkYXRlIGlzIGNhbGxlZCwgc28gd2UgQUxTTyBwdXQgaXQgaGVyZSBhbmQgdGhlbiBpZiBpdCdzIG5vdCBjYWxsZWQsIHdlIGRvIHNvIGxhdGVyIG5lYXIgdGhlIG9uQ29tcGxldGUuXHJcblx0XHRcdFx0fVxyXG5cdFx0XHRcdGlmICghc3VwcHJlc3NFdmVudHMpIGlmICh0aGlzLl90aW1lICE9PSBwcmV2VGltZSB8fCBpc0NvbXBsZXRlIHx8IGZvcmNlKSB7XHJcblx0XHRcdFx0XHR0aGlzLl9jYWxsYmFjayhcIm9uVXBkYXRlXCIpO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cdFx0XHRpZiAoY2FsbGJhY2spIGlmICghdGhpcy5fZ2MgfHwgZm9yY2UpIHsgLy9jaGVjayBfZ2MgYmVjYXVzZSB0aGVyZSdzIGEgY2hhbmNlIHRoYXQga2lsbCgpIGNvdWxkIGJlIGNhbGxlZCBpbiBhbiBvblVwZGF0ZVxyXG5cdFx0XHRcdGlmICh0aW1lIDwgMCAmJiB0aGlzLl9zdGFydEF0ICYmICF0aGlzLl9vblVwZGF0ZSAmJiB0aW1lICE9PSAtMC4wMDAxKSB7IC8vLTAuMDAwMSBpcyBhIHNwZWNpYWwgdmFsdWUgdGhhdCB3ZSB1c2Ugd2hlbiBsb29waW5nIGJhY2sgdG8gdGhlIGJlZ2lubmluZyBvZiBhIHJlcGVhdGVkIFRpbWVsaW5lTWF4LCBpbiB3aGljaCBjYXNlIHdlIHNob3VsZG4ndCByZW5kZXIgdGhlIF9zdGFydEF0IHZhbHVlcy5cclxuXHRcdFx0XHRcdHRoaXMuX3N0YXJ0QXQucmVuZGVyKHRpbWUsIHRydWUsIGZvcmNlKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0aWYgKGlzQ29tcGxldGUpIHtcclxuXHRcdFx0XHRcdGlmICh0aGlzLl90aW1lbGluZS5hdXRvUmVtb3ZlQ2hpbGRyZW4pIHtcclxuXHRcdFx0XHRcdFx0dGhpcy5fZW5hYmxlZChmYWxzZSwgZmFsc2UpO1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0dGhpcy5fYWN0aXZlID0gZmFsc2U7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHRcdGlmICghc3VwcHJlc3NFdmVudHMgJiYgdGhpcy52YXJzW2NhbGxiYWNrXSkge1xyXG5cdFx0XHRcdFx0dGhpcy5fY2FsbGJhY2soY2FsbGJhY2spO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0XHRpZiAoZHVyYXRpb24gPT09IDAgJiYgdGhpcy5fcmF3UHJldlRpbWUgPT09IF90aW55TnVtICYmIHJhd1ByZXZUaW1lICE9PSBfdGlueU51bSkgeyAvL3RoZSBvbkNvbXBsZXRlIG9yIG9uUmV2ZXJzZUNvbXBsZXRlIGNvdWxkIHRyaWdnZXIgbW92ZW1lbnQgb2YgdGhlIHBsYXloZWFkIGFuZCBmb3IgemVyby1kdXJhdGlvbiB0d2VlbnMgKHdoaWNoIG11c3QgZGlzY2VybiBkaXJlY3Rpb24pIHRoYXQgbGFuZCBkaXJlY3RseSBiYWNrIG9uIHRoZWlyIHN0YXJ0IHRpbWUsIHdlIGRvbid0IHdhbnQgdG8gZmlyZSBhZ2FpbiBvbiB0aGUgbmV4dCByZW5kZXIuIFRoaW5rIG9mIHNldmVyYWwgYWRkUGF1c2UoKSdzIGluIGEgdGltZWxpbmUgdGhhdCBmb3JjZXMgdGhlIHBsYXloZWFkIHRvIGEgY2VydGFpbiBzcG90LCBidXQgd2hhdCBpZiBpdCdzIGFscmVhZHkgcGF1c2VkIGFuZCBhbm90aGVyIHR3ZWVuIGlzIHR3ZWVuaW5nIHRoZSBcInRpbWVcIiBvZiB0aGUgdGltZWxpbmU/IEVhY2ggdGltZSBpdCBtb3ZlcyBbZm9yd2FyZF0gcGFzdCB0aGF0IHNwb3QsIGl0IHdvdWxkIG1vdmUgYmFjaywgYW5kIHNpbmNlIHN1cHByZXNzRXZlbnRzIGlzIHRydWUsIGl0J2QgcmVzZXQgX3Jhd1ByZXZUaW1lIHRvIF90aW55TnVtIHNvIHRoYXQgd2hlbiBpdCBiZWdpbnMgYWdhaW4sIHRoZSBjYWxsYmFjayB3b3VsZCBmaXJlIChzbyB1bHRpbWF0ZWx5IGl0IGNvdWxkIGJvdW5jZSBiYWNrIGFuZCBmb3J0aCBkdXJpbmcgdGhhdCB0d2VlbikuIEFnYWluLCB0aGlzIGlzIGEgdmVyeSB1bmNvbW1vbiBzY2VuYXJpbywgYnV0IHBvc3NpYmxlIG5vbmV0aGVsZXNzLlxyXG5cdFx0XHRcdFx0dGhpcy5fcmF3UHJldlRpbWUgPSAwO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cdFx0fTtcclxuXHJcblx0XHRwLl9raWxsID0gZnVuY3Rpb24odmFycywgdGFyZ2V0LCBvdmVyd3JpdGluZ1R3ZWVuKSB7XHJcblx0XHRcdGlmICh2YXJzID09PSBcImFsbFwiKSB7XHJcblx0XHRcdFx0dmFycyA9IG51bGw7XHJcblx0XHRcdH1cclxuXHRcdFx0aWYgKHZhcnMgPT0gbnVsbCkgaWYgKHRhcmdldCA9PSBudWxsIHx8IHRhcmdldCA9PT0gdGhpcy50YXJnZXQpIHtcclxuXHRcdFx0XHR0aGlzLl9sYXp5ID0gZmFsc2U7XHJcblx0XHRcdFx0cmV0dXJuIHRoaXMuX2VuYWJsZWQoZmFsc2UsIGZhbHNlKTtcclxuXHRcdFx0fVxyXG5cdFx0XHR0YXJnZXQgPSAodHlwZW9mKHRhcmdldCkgIT09IFwic3RyaW5nXCIpID8gKHRhcmdldCB8fCB0aGlzLl90YXJnZXRzIHx8IHRoaXMudGFyZ2V0KSA6IFR3ZWVuTGl0ZS5zZWxlY3Rvcih0YXJnZXQpIHx8IHRhcmdldDtcclxuXHRcdFx0dmFyIHNpbXVsdGFuZW91c092ZXJ3cml0ZSA9IChvdmVyd3JpdGluZ1R3ZWVuICYmIHRoaXMuX3RpbWUgJiYgb3ZlcndyaXRpbmdUd2Vlbi5fc3RhcnRUaW1lID09PSB0aGlzLl9zdGFydFRpbWUgJiYgdGhpcy5fdGltZWxpbmUgPT09IG92ZXJ3cml0aW5nVHdlZW4uX3RpbWVsaW5lKSxcclxuXHRcdFx0XHRpLCBvdmVyd3JpdHRlblByb3BzLCBwLCBwdCwgcHJvcExvb2t1cCwgY2hhbmdlZCwga2lsbFByb3BzLCByZWNvcmQsIGtpbGxlZDtcclxuXHRcdFx0aWYgKChfaXNBcnJheSh0YXJnZXQpIHx8IF9pc1NlbGVjdG9yKHRhcmdldCkpICYmIHR5cGVvZih0YXJnZXRbMF0pICE9PSBcIm51bWJlclwiKSB7XHJcblx0XHRcdFx0aSA9IHRhcmdldC5sZW5ndGg7XHJcblx0XHRcdFx0d2hpbGUgKC0taSA+IC0xKSB7XHJcblx0XHRcdFx0XHRpZiAodGhpcy5fa2lsbCh2YXJzLCB0YXJnZXRbaV0sIG92ZXJ3cml0aW5nVHdlZW4pKSB7XHJcblx0XHRcdFx0XHRcdGNoYW5nZWQgPSB0cnVlO1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdH1cclxuXHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHRpZiAodGhpcy5fdGFyZ2V0cykge1xyXG5cdFx0XHRcdFx0aSA9IHRoaXMuX3RhcmdldHMubGVuZ3RoO1xyXG5cdFx0XHRcdFx0d2hpbGUgKC0taSA+IC0xKSB7XHJcblx0XHRcdFx0XHRcdGlmICh0YXJnZXQgPT09IHRoaXMuX3RhcmdldHNbaV0pIHtcclxuXHRcdFx0XHRcdFx0XHRwcm9wTG9va3VwID0gdGhpcy5fcHJvcExvb2t1cFtpXSB8fCB7fTtcclxuXHRcdFx0XHRcdFx0XHR0aGlzLl9vdmVyd3JpdHRlblByb3BzID0gdGhpcy5fb3ZlcndyaXR0ZW5Qcm9wcyB8fCBbXTtcclxuXHRcdFx0XHRcdFx0XHRvdmVyd3JpdHRlblByb3BzID0gdGhpcy5fb3ZlcndyaXR0ZW5Qcm9wc1tpXSA9IHZhcnMgPyB0aGlzLl9vdmVyd3JpdHRlblByb3BzW2ldIHx8IHt9IDogXCJhbGxcIjtcclxuXHRcdFx0XHRcdFx0XHRicmVhaztcclxuXHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdH0gZWxzZSBpZiAodGFyZ2V0ICE9PSB0aGlzLnRhcmdldCkge1xyXG5cdFx0XHRcdFx0cmV0dXJuIGZhbHNlO1xyXG5cdFx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0XHRwcm9wTG9va3VwID0gdGhpcy5fcHJvcExvb2t1cDtcclxuXHRcdFx0XHRcdG92ZXJ3cml0dGVuUHJvcHMgPSB0aGlzLl9vdmVyd3JpdHRlblByb3BzID0gdmFycyA/IHRoaXMuX292ZXJ3cml0dGVuUHJvcHMgfHwge30gOiBcImFsbFwiO1xyXG5cdFx0XHRcdH1cclxuXHJcblx0XHRcdFx0aWYgKHByb3BMb29rdXApIHtcclxuXHRcdFx0XHRcdGtpbGxQcm9wcyA9IHZhcnMgfHwgcHJvcExvb2t1cDtcclxuXHRcdFx0XHRcdHJlY29yZCA9ICh2YXJzICE9PSBvdmVyd3JpdHRlblByb3BzICYmIG92ZXJ3cml0dGVuUHJvcHMgIT09IFwiYWxsXCIgJiYgdmFycyAhPT0gcHJvcExvb2t1cCAmJiAodHlwZW9mKHZhcnMpICE9PSBcIm9iamVjdFwiIHx8ICF2YXJzLl90ZW1wS2lsbCkpOyAvL190ZW1wS2lsbCBpcyBhIHN1cGVyLXNlY3JldCB3YXkgdG8gZGVsZXRlIGEgcGFydGljdWxhciB0d2VlbmluZyBwcm9wZXJ0eSBidXQgTk9UIGhhdmUgaXQgcmVtZW1iZXJlZCBhcyBhbiBvZmZpY2lhbCBvdmVyd3JpdHRlbiBwcm9wZXJ0eSAobGlrZSBpbiBCZXppZXJQbHVnaW4pXHJcblx0XHRcdFx0XHRpZiAob3ZlcndyaXRpbmdUd2VlbiAmJiAoVHdlZW5MaXRlLm9uT3ZlcndyaXRlIHx8IHRoaXMudmFycy5vbk92ZXJ3cml0ZSkpIHtcclxuXHRcdFx0XHRcdFx0Zm9yIChwIGluIGtpbGxQcm9wcykge1xyXG5cdFx0XHRcdFx0XHRcdGlmIChwcm9wTG9va3VwW3BdKSB7XHJcblx0XHRcdFx0XHRcdFx0XHRpZiAoIWtpbGxlZCkge1xyXG5cdFx0XHRcdFx0XHRcdFx0XHRraWxsZWQgPSBbXTtcclxuXHRcdFx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdFx0XHRcdGtpbGxlZC5wdXNoKHApO1xyXG5cdFx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0XHRpZiAoKGtpbGxlZCB8fCAhdmFycykgJiYgIV9vbk92ZXJ3cml0ZSh0aGlzLCBvdmVyd3JpdGluZ1R3ZWVuLCB0YXJnZXQsIGtpbGxlZCkpIHsgLy9pZiB0aGUgb25PdmVyd3JpdGUgcmV0dXJuZWQgZmFsc2UsIHRoYXQgbWVhbnMgdGhlIHVzZXIgd2FudHMgdG8gb3ZlcnJpZGUgdGhlIG92ZXJ3cml0aW5nIChjYW5jZWwgaXQpLlxyXG5cdFx0XHRcdFx0XHRcdHJldHVybiBmYWxzZTtcclxuXHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0fVxyXG5cclxuXHRcdFx0XHRcdGZvciAocCBpbiBraWxsUHJvcHMpIHtcclxuXHRcdFx0XHRcdFx0aWYgKChwdCA9IHByb3BMb29rdXBbcF0pKSB7XHJcblx0XHRcdFx0XHRcdFx0aWYgKHNpbXVsdGFuZW91c092ZXJ3cml0ZSkgeyAvL2lmIGFub3RoZXIgdHdlZW4gb3ZlcndyaXRlcyB0aGlzIG9uZSBhbmQgdGhleSBib3RoIHN0YXJ0IGF0IGV4YWN0bHkgdGhlIHNhbWUgdGltZSwgeWV0IHRoaXMgdHdlZW4gaGFzIGFscmVhZHkgcmVuZGVyZWQgb25jZSAoZm9yIGV4YW1wbGUsIGF0IDAuMDAxKSBiZWNhdXNlIGl0J3MgZmlyc3QgaW4gdGhlIHF1ZXVlLCB3ZSBzaG91bGQgcmV2ZXJ0IHRoZSB2YWx1ZXMgdG8gd2hlcmUgdGhleSB3ZXJlIGF0IDAgc28gdGhhdCB0aGUgc3RhcnRpbmcgdmFsdWVzIGFyZW4ndCBjb250YW1pbmF0ZWQgb24gdGhlIG92ZXJ3cml0aW5nIHR3ZWVuLlxyXG5cdFx0XHRcdFx0XHRcdFx0aWYgKHB0LmYpIHtcclxuXHRcdFx0XHRcdFx0XHRcdFx0cHQudFtwdC5wXShwdC5zKTtcclxuXHRcdFx0XHRcdFx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0XHRcdFx0XHRcdHB0LnRbcHQucF0gPSBwdC5zO1xyXG5cdFx0XHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0XHRcdFx0Y2hhbmdlZCA9IHRydWU7XHJcblx0XHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0XHRcdGlmIChwdC5wZyAmJiBwdC50Ll9raWxsKGtpbGxQcm9wcykpIHtcclxuXHRcdFx0XHRcdFx0XHRcdGNoYW5nZWQgPSB0cnVlOyAvL3NvbWUgcGx1Z2lucyBuZWVkIHRvIGJlIG5vdGlmaWVkIHNvIHRoZXkgY2FuIHBlcmZvcm0gY2xlYW51cCB0YXNrcyBmaXJzdFxyXG5cdFx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdFx0XHRpZiAoIXB0LnBnIHx8IHB0LnQuX292ZXJ3cml0ZVByb3BzLmxlbmd0aCA9PT0gMCkge1xyXG5cdFx0XHRcdFx0XHRcdFx0aWYgKHB0Ll9wcmV2KSB7XHJcblx0XHRcdFx0XHRcdFx0XHRcdHB0Ll9wcmV2Ll9uZXh0ID0gcHQuX25leHQ7XHJcblx0XHRcdFx0XHRcdFx0XHR9IGVsc2UgaWYgKHB0ID09PSB0aGlzLl9maXJzdFBUKSB7XHJcblx0XHRcdFx0XHRcdFx0XHRcdHRoaXMuX2ZpcnN0UFQgPSBwdC5fbmV4dDtcclxuXHRcdFx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdFx0XHRcdGlmIChwdC5fbmV4dCkge1xyXG5cdFx0XHRcdFx0XHRcdFx0XHRwdC5fbmV4dC5fcHJldiA9IHB0Ll9wcmV2O1xyXG5cdFx0XHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0XHRcdFx0cHQuX25leHQgPSBwdC5fcHJldiA9IG51bGw7XHJcblx0XHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0XHRcdGRlbGV0ZSBwcm9wTG9va3VwW3BdO1xyXG5cdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRcdGlmIChyZWNvcmQpIHtcclxuXHRcdFx0XHRcdFx0XHRvdmVyd3JpdHRlblByb3BzW3BdID0gMTtcclxuXHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0aWYgKCF0aGlzLl9maXJzdFBUICYmIHRoaXMuX2luaXR0ZWQpIHsgLy9pZiBhbGwgdHdlZW5pbmcgcHJvcGVydGllcyBhcmUga2lsbGVkLCBraWxsIHRoZSB0d2Vlbi4gV2l0aG91dCB0aGlzIGxpbmUsIGlmIHRoZXJlJ3MgYSB0d2VlbiB3aXRoIG11bHRpcGxlIHRhcmdldHMgYW5kIHRoZW4geW91IGtpbGxUd2VlbnNPZigpIGVhY2ggdGFyZ2V0IGluZGl2aWR1YWxseSwgdGhlIHR3ZWVuIHdvdWxkIHRlY2huaWNhbGx5IHN0aWxsIHJlbWFpbiBhY3RpdmUgYW5kIGZpcmUgaXRzIG9uQ29tcGxldGUgZXZlbiB0aG91Z2ggdGhlcmUgYXJlbid0IGFueSBtb3JlIHByb3BlcnRpZXMgdHdlZW5pbmcuXHJcblx0XHRcdFx0XHRcdHRoaXMuX2VuYWJsZWQoZmFsc2UsIGZhbHNlKTtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHR9XHJcblx0XHRcdH1cclxuXHRcdFx0cmV0dXJuIGNoYW5nZWQ7XHJcblx0XHR9O1xyXG5cclxuXHRcdHAuaW52YWxpZGF0ZSA9IGZ1bmN0aW9uKCkge1xyXG5cdFx0XHRpZiAodGhpcy5fbm90aWZ5UGx1Z2luc09mRW5hYmxlZCkge1xyXG5cdFx0XHRcdFR3ZWVuTGl0ZS5fb25QbHVnaW5FdmVudChcIl9vbkRpc2FibGVcIiwgdGhpcyk7XHJcblx0XHRcdH1cclxuXHRcdFx0dGhpcy5fZmlyc3RQVCA9IHRoaXMuX292ZXJ3cml0dGVuUHJvcHMgPSB0aGlzLl9zdGFydEF0ID0gdGhpcy5fb25VcGRhdGUgPSBudWxsO1xyXG5cdFx0XHR0aGlzLl9ub3RpZnlQbHVnaW5zT2ZFbmFibGVkID0gdGhpcy5fYWN0aXZlID0gdGhpcy5fbGF6eSA9IGZhbHNlO1xyXG5cdFx0XHR0aGlzLl9wcm9wTG9va3VwID0gKHRoaXMuX3RhcmdldHMpID8ge30gOiBbXTtcclxuXHRcdFx0QW5pbWF0aW9uLnByb3RvdHlwZS5pbnZhbGlkYXRlLmNhbGwodGhpcyk7XHJcblx0XHRcdGlmICh0aGlzLnZhcnMuaW1tZWRpYXRlUmVuZGVyKSB7XHJcblx0XHRcdFx0dGhpcy5fdGltZSA9IC1fdGlueU51bTsgLy9mb3JjZXMgYSByZW5kZXIgd2l0aG91dCBoYXZpbmcgdG8gc2V0IHRoZSByZW5kZXIoKSBcImZvcmNlXCIgcGFyYW1ldGVyIHRvIHRydWUgYmVjYXVzZSB3ZSB3YW50IHRvIGFsbG93IGxhenlpbmcgYnkgZGVmYXVsdCAodXNpbmcgdGhlIFwiZm9yY2VcIiBwYXJhbWV0ZXIgYWx3YXlzIGZvcmNlcyBhbiBpbW1lZGlhdGUgZnVsbCByZW5kZXIpXHJcblx0XHRcdFx0dGhpcy5yZW5kZXIoTWF0aC5taW4oMCwgLXRoaXMuX2RlbGF5KSk7IC8vaW4gY2FzZSBkZWxheSBpcyBuZWdhdGl2ZS5cclxuXHRcdFx0fVxyXG5cdFx0XHRyZXR1cm4gdGhpcztcclxuXHRcdH07XHJcblxyXG5cdFx0cC5fZW5hYmxlZCA9IGZ1bmN0aW9uKGVuYWJsZWQsIGlnbm9yZVRpbWVsaW5lKSB7XHJcblx0XHRcdGlmICghX3RpY2tlckFjdGl2ZSkge1xyXG5cdFx0XHRcdF90aWNrZXIud2FrZSgpO1xyXG5cdFx0XHR9XHJcblx0XHRcdGlmIChlbmFibGVkICYmIHRoaXMuX2djKSB7XHJcblx0XHRcdFx0dmFyIHRhcmdldHMgPSB0aGlzLl90YXJnZXRzLFxyXG5cdFx0XHRcdFx0aTtcclxuXHRcdFx0XHRpZiAodGFyZ2V0cykge1xyXG5cdFx0XHRcdFx0aSA9IHRhcmdldHMubGVuZ3RoO1xyXG5cdFx0XHRcdFx0d2hpbGUgKC0taSA+IC0xKSB7XHJcblx0XHRcdFx0XHRcdHRoaXMuX3NpYmxpbmdzW2ldID0gX3JlZ2lzdGVyKHRhcmdldHNbaV0sIHRoaXMsIHRydWUpO1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0XHR0aGlzLl9zaWJsaW5ncyA9IF9yZWdpc3Rlcih0aGlzLnRhcmdldCwgdGhpcywgdHJ1ZSk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9XHJcblx0XHRcdEFuaW1hdGlvbi5wcm90b3R5cGUuX2VuYWJsZWQuY2FsbCh0aGlzLCBlbmFibGVkLCBpZ25vcmVUaW1lbGluZSk7XHJcblx0XHRcdGlmICh0aGlzLl9ub3RpZnlQbHVnaW5zT2ZFbmFibGVkKSBpZiAodGhpcy5fZmlyc3RQVCkge1xyXG5cdFx0XHRcdHJldHVybiBUd2VlbkxpdGUuX29uUGx1Z2luRXZlbnQoKGVuYWJsZWQgPyBcIl9vbkVuYWJsZVwiIDogXCJfb25EaXNhYmxlXCIpLCB0aGlzKTtcclxuXHRcdFx0fVxyXG5cdFx0XHRyZXR1cm4gZmFsc2U7XHJcblx0XHR9O1xyXG5cclxuXHJcbi8vLS0tLVR3ZWVuTGl0ZSBzdGF0aWMgbWV0aG9kcyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuXHRcdFR3ZWVuTGl0ZS50byA9IGZ1bmN0aW9uKHRhcmdldCwgZHVyYXRpb24sIHZhcnMpIHtcclxuXHRcdFx0cmV0dXJuIG5ldyBUd2VlbkxpdGUodGFyZ2V0LCBkdXJhdGlvbiwgdmFycyk7XHJcblx0XHR9O1xyXG5cclxuXHRcdFR3ZWVuTGl0ZS5mcm9tID0gZnVuY3Rpb24odGFyZ2V0LCBkdXJhdGlvbiwgdmFycykge1xyXG5cdFx0XHR2YXJzLnJ1bkJhY2t3YXJkcyA9IHRydWU7XHJcblx0XHRcdHZhcnMuaW1tZWRpYXRlUmVuZGVyID0gKHZhcnMuaW1tZWRpYXRlUmVuZGVyICE9IGZhbHNlKTtcclxuXHRcdFx0cmV0dXJuIG5ldyBUd2VlbkxpdGUodGFyZ2V0LCBkdXJhdGlvbiwgdmFycyk7XHJcblx0XHR9O1xyXG5cclxuXHRcdFR3ZWVuTGl0ZS5mcm9tVG8gPSBmdW5jdGlvbih0YXJnZXQsIGR1cmF0aW9uLCBmcm9tVmFycywgdG9WYXJzKSB7XHJcblx0XHRcdHRvVmFycy5zdGFydEF0ID0gZnJvbVZhcnM7XHJcblx0XHRcdHRvVmFycy5pbW1lZGlhdGVSZW5kZXIgPSAodG9WYXJzLmltbWVkaWF0ZVJlbmRlciAhPSBmYWxzZSAmJiBmcm9tVmFycy5pbW1lZGlhdGVSZW5kZXIgIT0gZmFsc2UpO1xyXG5cdFx0XHRyZXR1cm4gbmV3IFR3ZWVuTGl0ZSh0YXJnZXQsIGR1cmF0aW9uLCB0b1ZhcnMpO1xyXG5cdFx0fTtcclxuXHJcblx0XHRUd2VlbkxpdGUuZGVsYXllZENhbGwgPSBmdW5jdGlvbihkZWxheSwgY2FsbGJhY2ssIHBhcmFtcywgc2NvcGUsIHVzZUZyYW1lcykge1xyXG5cdFx0XHRyZXR1cm4gbmV3IFR3ZWVuTGl0ZShjYWxsYmFjaywgMCwge2RlbGF5OmRlbGF5LCBvbkNvbXBsZXRlOmNhbGxiYWNrLCBvbkNvbXBsZXRlUGFyYW1zOnBhcmFtcywgY2FsbGJhY2tTY29wZTpzY29wZSwgb25SZXZlcnNlQ29tcGxldGU6Y2FsbGJhY2ssIG9uUmV2ZXJzZUNvbXBsZXRlUGFyYW1zOnBhcmFtcywgaW1tZWRpYXRlUmVuZGVyOmZhbHNlLCBsYXp5OmZhbHNlLCB1c2VGcmFtZXM6dXNlRnJhbWVzLCBvdmVyd3JpdGU6MH0pO1xyXG5cdFx0fTtcclxuXHJcblx0XHRUd2VlbkxpdGUuc2V0ID0gZnVuY3Rpb24odGFyZ2V0LCB2YXJzKSB7XHJcblx0XHRcdHJldHVybiBuZXcgVHdlZW5MaXRlKHRhcmdldCwgMCwgdmFycyk7XHJcblx0XHR9O1xyXG5cclxuXHRcdFR3ZWVuTGl0ZS5nZXRUd2VlbnNPZiA9IGZ1bmN0aW9uKHRhcmdldCwgb25seUFjdGl2ZSkge1xyXG5cdFx0XHRpZiAodGFyZ2V0ID09IG51bGwpIHsgcmV0dXJuIFtdOyB9XHJcblx0XHRcdHRhcmdldCA9ICh0eXBlb2YodGFyZ2V0KSAhPT0gXCJzdHJpbmdcIikgPyB0YXJnZXQgOiBUd2VlbkxpdGUuc2VsZWN0b3IodGFyZ2V0KSB8fCB0YXJnZXQ7XHJcblx0XHRcdHZhciBpLCBhLCBqLCB0O1xyXG5cdFx0XHRpZiAoKF9pc0FycmF5KHRhcmdldCkgfHwgX2lzU2VsZWN0b3IodGFyZ2V0KSkgJiYgdHlwZW9mKHRhcmdldFswXSkgIT09IFwibnVtYmVyXCIpIHtcclxuXHRcdFx0XHRpID0gdGFyZ2V0Lmxlbmd0aDtcclxuXHRcdFx0XHRhID0gW107XHJcblx0XHRcdFx0d2hpbGUgKC0taSA+IC0xKSB7XHJcblx0XHRcdFx0XHRhID0gYS5jb25jYXQoVHdlZW5MaXRlLmdldFR3ZWVuc09mKHRhcmdldFtpXSwgb25seUFjdGl2ZSkpO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0XHRpID0gYS5sZW5ndGg7XHJcblx0XHRcdFx0Ly9ub3cgZ2V0IHJpZCBvZiBhbnkgZHVwbGljYXRlcyAodHdlZW5zIG9mIGFycmF5cyBvZiBvYmplY3RzIGNvdWxkIGNhdXNlIGR1cGxpY2F0ZXMpXHJcblx0XHRcdFx0d2hpbGUgKC0taSA+IC0xKSB7XHJcblx0XHRcdFx0XHR0ID0gYVtpXTtcclxuXHRcdFx0XHRcdGogPSBpO1xyXG5cdFx0XHRcdFx0d2hpbGUgKC0taiA+IC0xKSB7XHJcblx0XHRcdFx0XHRcdGlmICh0ID09PSBhW2pdKSB7XHJcblx0XHRcdFx0XHRcdFx0YS5zcGxpY2UoaSwgMSk7XHJcblx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHR9XHJcblx0XHRcdH0gZWxzZSBpZiAodGFyZ2V0Ll9nc1R3ZWVuSUQpIHtcclxuXHRcdFx0XHRhID0gX3JlZ2lzdGVyKHRhcmdldCkuY29uY2F0KCk7XHJcblx0XHRcdFx0aSA9IGEubGVuZ3RoO1xyXG5cdFx0XHRcdHdoaWxlICgtLWkgPiAtMSkge1xyXG5cdFx0XHRcdFx0aWYgKGFbaV0uX2djIHx8IChvbmx5QWN0aXZlICYmICFhW2ldLmlzQWN0aXZlKCkpKSB7XHJcblx0XHRcdFx0XHRcdGEuc3BsaWNlKGksIDEpO1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cdFx0XHRyZXR1cm4gYSB8fCBbXTtcclxuXHRcdH07XHJcblxyXG5cdFx0VHdlZW5MaXRlLmtpbGxUd2VlbnNPZiA9IFR3ZWVuTGl0ZS5raWxsRGVsYXllZENhbGxzVG8gPSBmdW5jdGlvbih0YXJnZXQsIG9ubHlBY3RpdmUsIHZhcnMpIHtcclxuXHRcdFx0aWYgKHR5cGVvZihvbmx5QWN0aXZlKSA9PT0gXCJvYmplY3RcIikge1xyXG5cdFx0XHRcdHZhcnMgPSBvbmx5QWN0aXZlOyAvL2ZvciBiYWNrd2FyZHMgY29tcGF0aWJpbGl0eSAoYmVmb3JlIFwib25seUFjdGl2ZVwiIHBhcmFtZXRlciB3YXMgaW5zZXJ0ZWQpXHJcblx0XHRcdFx0b25seUFjdGl2ZSA9IGZhbHNlO1xyXG5cdFx0XHR9XHJcblx0XHRcdHZhciBhID0gVHdlZW5MaXRlLmdldFR3ZWVuc09mKHRhcmdldCwgb25seUFjdGl2ZSksXHJcblx0XHRcdFx0aSA9IGEubGVuZ3RoO1xyXG5cdFx0XHR3aGlsZSAoLS1pID4gLTEpIHtcclxuXHRcdFx0XHRhW2ldLl9raWxsKHZhcnMsIHRhcmdldCk7XHJcblx0XHRcdH1cclxuXHRcdH07XHJcblxyXG5cclxuXHJcbi8qXHJcbiAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICogVHdlZW5QbHVnaW4gICAoY291bGQgZWFzaWx5IGJlIHNwbGl0IG91dCBhcyBhIHNlcGFyYXRlIGZpbGUvY2xhc3MsIGJ1dCBpbmNsdWRlZCBmb3IgZWFzZSBvZiB1c2UgKHNvIHRoYXQgcGVvcGxlIGRvbid0IG5lZWQgdG8gaW5jbHVkZSBhbm90aGVyIHNjcmlwdCBjYWxsIGJlZm9yZSBsb2FkaW5nIHBsdWdpbnMgd2hpY2ggaXMgZWFzeSB0byBmb3JnZXQpXHJcbiAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICovXHJcblx0XHR2YXIgVHdlZW5QbHVnaW4gPSBfY2xhc3MoXCJwbHVnaW5zLlR3ZWVuUGx1Z2luXCIsIGZ1bmN0aW9uKHByb3BzLCBwcmlvcml0eSkge1xyXG5cdFx0XHRcdFx0dGhpcy5fb3ZlcndyaXRlUHJvcHMgPSAocHJvcHMgfHwgXCJcIikuc3BsaXQoXCIsXCIpO1xyXG5cdFx0XHRcdFx0dGhpcy5fcHJvcE5hbWUgPSB0aGlzLl9vdmVyd3JpdGVQcm9wc1swXTtcclxuXHRcdFx0XHRcdHRoaXMuX3ByaW9yaXR5ID0gcHJpb3JpdHkgfHwgMDtcclxuXHRcdFx0XHRcdHRoaXMuX3N1cGVyID0gVHdlZW5QbHVnaW4ucHJvdG90eXBlO1xyXG5cdFx0XHRcdH0sIHRydWUpO1xyXG5cclxuXHRcdHAgPSBUd2VlblBsdWdpbi5wcm90b3R5cGU7XHJcblx0XHRUd2VlblBsdWdpbi52ZXJzaW9uID0gXCIxLjE5LjBcIjtcclxuXHRcdFR3ZWVuUGx1Z2luLkFQSSA9IDI7XHJcblx0XHRwLl9maXJzdFBUID0gbnVsbDtcclxuXHRcdHAuX2FkZFR3ZWVuID0gX2FkZFByb3BUd2VlbjtcclxuXHRcdHAuc2V0UmF0aW8gPSBfc2V0UmF0aW87XHJcblxyXG5cdFx0cC5fa2lsbCA9IGZ1bmN0aW9uKGxvb2t1cCkge1xyXG5cdFx0XHR2YXIgYSA9IHRoaXMuX292ZXJ3cml0ZVByb3BzLFxyXG5cdFx0XHRcdHB0ID0gdGhpcy5fZmlyc3RQVCxcclxuXHRcdFx0XHRpO1xyXG5cdFx0XHRpZiAobG9va3VwW3RoaXMuX3Byb3BOYW1lXSAhPSBudWxsKSB7XHJcblx0XHRcdFx0dGhpcy5fb3ZlcndyaXRlUHJvcHMgPSBbXTtcclxuXHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHRpID0gYS5sZW5ndGg7XHJcblx0XHRcdFx0d2hpbGUgKC0taSA+IC0xKSB7XHJcblx0XHRcdFx0XHRpZiAobG9va3VwW2FbaV1dICE9IG51bGwpIHtcclxuXHRcdFx0XHRcdFx0YS5zcGxpY2UoaSwgMSk7XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9XHJcblx0XHRcdHdoaWxlIChwdCkge1xyXG5cdFx0XHRcdGlmIChsb29rdXBbcHQubl0gIT0gbnVsbCkge1xyXG5cdFx0XHRcdFx0aWYgKHB0Ll9uZXh0KSB7XHJcblx0XHRcdFx0XHRcdHB0Ll9uZXh0Ll9wcmV2ID0gcHQuX3ByZXY7XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRpZiAocHQuX3ByZXYpIHtcclxuXHRcdFx0XHRcdFx0cHQuX3ByZXYuX25leHQgPSBwdC5fbmV4dDtcclxuXHRcdFx0XHRcdFx0cHQuX3ByZXYgPSBudWxsO1xyXG5cdFx0XHRcdFx0fSBlbHNlIGlmICh0aGlzLl9maXJzdFBUID09PSBwdCkge1xyXG5cdFx0XHRcdFx0XHR0aGlzLl9maXJzdFBUID0gcHQuX25leHQ7XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0fVxyXG5cdFx0XHRcdHB0ID0gcHQuX25leHQ7XHJcblx0XHRcdH1cclxuXHRcdFx0cmV0dXJuIGZhbHNlO1xyXG5cdFx0fTtcclxuXHJcblx0XHRwLl9tb2QgPSBwLl9yb3VuZFByb3BzID0gZnVuY3Rpb24obG9va3VwKSB7XHJcblx0XHRcdHZhciBwdCA9IHRoaXMuX2ZpcnN0UFQsXHJcblx0XHRcdFx0dmFsO1xyXG5cdFx0XHR3aGlsZSAocHQpIHtcclxuXHRcdFx0XHR2YWwgPSBsb29rdXBbdGhpcy5fcHJvcE5hbWVdIHx8IChwdC5uICE9IG51bGwgJiYgbG9va3VwWyBwdC5uLnNwbGl0KHRoaXMuX3Byb3BOYW1lICsgXCJfXCIpLmpvaW4oXCJcIikgXSk7XHJcblx0XHRcdFx0aWYgKHZhbCAmJiB0eXBlb2YodmFsKSA9PT0gXCJmdW5jdGlvblwiKSB7IC8vc29tZSBwcm9wZXJ0aWVzIHRoYXQgYXJlIHZlcnkgcGx1Z2luLXNwZWNpZmljIGFkZCBhIHByZWZpeCBuYW1lZCBhZnRlciB0aGUgX3Byb3BOYW1lIHBsdXMgYW4gdW5kZXJzY29yZSwgc28gd2UgbmVlZCB0byBpZ25vcmUgdGhhdCBleHRyYSBzdHVmZiBoZXJlLlxyXG5cdFx0XHRcdFx0aWYgKHB0LmYgPT09IDIpIHtcclxuXHRcdFx0XHRcdFx0cHQudC5fYXBwbHlQVC5tID0gdmFsO1xyXG5cdFx0XHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHRcdFx0cHQubSA9IHZhbDtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0cHQgPSBwdC5fbmV4dDtcclxuXHRcdFx0fVxyXG5cdFx0fTtcclxuXHJcblx0XHRUd2VlbkxpdGUuX29uUGx1Z2luRXZlbnQgPSBmdW5jdGlvbih0eXBlLCB0d2Vlbikge1xyXG5cdFx0XHR2YXIgcHQgPSB0d2Vlbi5fZmlyc3RQVCxcclxuXHRcdFx0XHRjaGFuZ2VkLCBwdDIsIGZpcnN0LCBsYXN0LCBuZXh0O1xyXG5cdFx0XHRpZiAodHlwZSA9PT0gXCJfb25Jbml0QWxsUHJvcHNcIikge1xyXG5cdFx0XHRcdC8vc29ydHMgdGhlIFByb3BUd2VlbiBsaW5rZWQgbGlzdCBpbiBvcmRlciBvZiBwcmlvcml0eSBiZWNhdXNlIHNvbWUgcGx1Z2lucyBuZWVkIHRvIHJlbmRlciBlYXJsaWVyL2xhdGVyIHRoYW4gb3RoZXJzLCBsaWtlIE1vdGlvbkJsdXJQbHVnaW4gYXBwbGllcyBpdHMgZWZmZWN0cyBhZnRlciBhbGwgeC95L2FscGhhIHR3ZWVucyBoYXZlIHJlbmRlcmVkIG9uIGVhY2ggZnJhbWUuXHJcblx0XHRcdFx0d2hpbGUgKHB0KSB7XHJcblx0XHRcdFx0XHRuZXh0ID0gcHQuX25leHQ7XHJcblx0XHRcdFx0XHRwdDIgPSBmaXJzdDtcclxuXHRcdFx0XHRcdHdoaWxlIChwdDIgJiYgcHQyLnByID4gcHQucHIpIHtcclxuXHRcdFx0XHRcdFx0cHQyID0gcHQyLl9uZXh0O1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0aWYgKChwdC5fcHJldiA9IHB0MiA/IHB0Mi5fcHJldiA6IGxhc3QpKSB7XHJcblx0XHRcdFx0XHRcdHB0Ll9wcmV2Ll9uZXh0ID0gcHQ7XHJcblx0XHRcdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdFx0XHRmaXJzdCA9IHB0O1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0aWYgKChwdC5fbmV4dCA9IHB0MikpIHtcclxuXHRcdFx0XHRcdFx0cHQyLl9wcmV2ID0gcHQ7XHJcblx0XHRcdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdFx0XHRsYXN0ID0gcHQ7XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRwdCA9IG5leHQ7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHRcdHB0ID0gdHdlZW4uX2ZpcnN0UFQgPSBmaXJzdDtcclxuXHRcdFx0fVxyXG5cdFx0XHR3aGlsZSAocHQpIHtcclxuXHRcdFx0XHRpZiAocHQucGcpIGlmICh0eXBlb2YocHQudFt0eXBlXSkgPT09IFwiZnVuY3Rpb25cIikgaWYgKHB0LnRbdHlwZV0oKSkge1xyXG5cdFx0XHRcdFx0Y2hhbmdlZCA9IHRydWU7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHRcdHB0ID0gcHQuX25leHQ7XHJcblx0XHRcdH1cclxuXHRcdFx0cmV0dXJuIGNoYW5nZWQ7XHJcblx0XHR9O1xyXG5cclxuXHRcdFR3ZWVuUGx1Z2luLmFjdGl2YXRlID0gZnVuY3Rpb24ocGx1Z2lucykge1xyXG5cdFx0XHR2YXIgaSA9IHBsdWdpbnMubGVuZ3RoO1xyXG5cdFx0XHR3aGlsZSAoLS1pID4gLTEpIHtcclxuXHRcdFx0XHRpZiAocGx1Z2luc1tpXS5BUEkgPT09IFR3ZWVuUGx1Z2luLkFQSSkge1xyXG5cdFx0XHRcdFx0X3BsdWdpbnNbKG5ldyBwbHVnaW5zW2ldKCkpLl9wcm9wTmFtZV0gPSBwbHVnaW5zW2ldO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cdFx0XHRyZXR1cm4gdHJ1ZTtcclxuXHRcdH07XHJcblxyXG5cdFx0Ly9wcm92aWRlcyBhIG1vcmUgY29uY2lzZSB3YXkgdG8gZGVmaW5lIHBsdWdpbnMgdGhhdCBoYXZlIG5vIGRlcGVuZGVuY2llcyBiZXNpZGVzIFR3ZWVuUGx1Z2luIGFuZCBUd2VlbkxpdGUsIHdyYXBwaW5nIGNvbW1vbiBib2lsZXJwbGF0ZSBzdHVmZiBpbnRvIG9uZSBmdW5jdGlvbiAoYWRkZWQgaW4gMS45LjApLiBZb3UgZG9uJ3QgTkVFRCB0byB1c2UgdGhpcyB0byBkZWZpbmUgYSBwbHVnaW4gLSB0aGUgb2xkIHdheSBzdGlsbCB3b3JrcyBhbmQgY2FuIGJlIHVzZWZ1bCBpbiBjZXJ0YWluIChyYXJlKSBzaXR1YXRpb25zLlxyXG5cdFx0X2dzRGVmaW5lLnBsdWdpbiA9IGZ1bmN0aW9uKGNvbmZpZykge1xyXG5cdFx0XHRpZiAoIWNvbmZpZyB8fCAhY29uZmlnLnByb3BOYW1lIHx8ICFjb25maWcuaW5pdCB8fCAhY29uZmlnLkFQSSkgeyB0aHJvdyBcImlsbGVnYWwgcGx1Z2luIGRlZmluaXRpb24uXCI7IH1cclxuXHRcdFx0dmFyIHByb3BOYW1lID0gY29uZmlnLnByb3BOYW1lLFxyXG5cdFx0XHRcdHByaW9yaXR5ID0gY29uZmlnLnByaW9yaXR5IHx8IDAsXHJcblx0XHRcdFx0b3ZlcndyaXRlUHJvcHMgPSBjb25maWcub3ZlcndyaXRlUHJvcHMsXHJcblx0XHRcdFx0bWFwID0ge2luaXQ6XCJfb25Jbml0VHdlZW5cIiwgc2V0Olwic2V0UmF0aW9cIiwga2lsbDpcIl9raWxsXCIsIHJvdW5kOlwiX21vZFwiLCBtb2Q6XCJfbW9kXCIsIGluaXRBbGw6XCJfb25Jbml0QWxsUHJvcHNcIn0sXHJcblx0XHRcdFx0UGx1Z2luID0gX2NsYXNzKFwicGx1Z2lucy5cIiArIHByb3BOYW1lLmNoYXJBdCgwKS50b1VwcGVyQ2FzZSgpICsgcHJvcE5hbWUuc3Vic3RyKDEpICsgXCJQbHVnaW5cIixcclxuXHRcdFx0XHRcdGZ1bmN0aW9uKCkge1xyXG5cdFx0XHRcdFx0XHRUd2VlblBsdWdpbi5jYWxsKHRoaXMsIHByb3BOYW1lLCBwcmlvcml0eSk7XHJcblx0XHRcdFx0XHRcdHRoaXMuX292ZXJ3cml0ZVByb3BzID0gb3ZlcndyaXRlUHJvcHMgfHwgW107XHJcblx0XHRcdFx0XHR9LCAoY29uZmlnLmdsb2JhbCA9PT0gdHJ1ZSkpLFxyXG5cdFx0XHRcdHAgPSBQbHVnaW4ucHJvdG90eXBlID0gbmV3IFR3ZWVuUGx1Z2luKHByb3BOYW1lKSxcclxuXHRcdFx0XHRwcm9wO1xyXG5cdFx0XHRwLmNvbnN0cnVjdG9yID0gUGx1Z2luO1xyXG5cdFx0XHRQbHVnaW4uQVBJID0gY29uZmlnLkFQSTtcclxuXHRcdFx0Zm9yIChwcm9wIGluIG1hcCkge1xyXG5cdFx0XHRcdGlmICh0eXBlb2YoY29uZmlnW3Byb3BdKSA9PT0gXCJmdW5jdGlvblwiKSB7XHJcblx0XHRcdFx0XHRwW21hcFtwcm9wXV0gPSBjb25maWdbcHJvcF07XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9XHJcblx0XHRcdFBsdWdpbi52ZXJzaW9uID0gY29uZmlnLnZlcnNpb247XHJcblx0XHRcdFR3ZWVuUGx1Z2luLmFjdGl2YXRlKFtQbHVnaW5dKTtcclxuXHRcdFx0cmV0dXJuIFBsdWdpbjtcclxuXHRcdH07XHJcblxyXG5cclxuXHRcdC8vbm93IHJ1biB0aHJvdWdoIGFsbCB0aGUgZGVwZW5kZW5jaWVzIGRpc2NvdmVyZWQgYW5kIGlmIGFueSBhcmUgbWlzc2luZywgbG9nIHRoYXQgdG8gdGhlIGNvbnNvbGUgYXMgYSB3YXJuaW5nLiBUaGlzIGlzIHdoeSBpdCdzIGJlc3QgdG8gaGF2ZSBUd2VlbkxpdGUgbG9hZCBsYXN0IC0gaXQgY2FuIGNoZWNrIGFsbCB0aGUgZGVwZW5kZW5jaWVzIGZvciB5b3UuXHJcblx0XHRhID0gd2luZG93Ll9nc1F1ZXVlO1xyXG5cdFx0aWYgKGEpIHtcclxuXHRcdFx0Zm9yIChpID0gMDsgaSA8IGEubGVuZ3RoOyBpKyspIHtcclxuXHRcdFx0XHRhW2ldKCk7XHJcblx0XHRcdH1cclxuXHRcdFx0Zm9yIChwIGluIF9kZWZMb29rdXApIHtcclxuXHRcdFx0XHRpZiAoIV9kZWZMb29rdXBbcF0uZnVuYykge1xyXG5cdFx0XHRcdFx0d2luZG93LmNvbnNvbGUubG9nKFwiR1NBUCBlbmNvdW50ZXJlZCBtaXNzaW5nIGRlcGVuZGVuY3k6IFwiICsgcCk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9XHJcblx0XHR9XHJcblxyXG5cdFx0X3RpY2tlckFjdGl2ZSA9IGZhbHNlOyAvL2Vuc3VyZXMgdGhhdCB0aGUgZmlyc3Qgb2ZmaWNpYWwgYW5pbWF0aW9uIGZvcmNlcyBhIHRpY2tlci50aWNrKCkgdG8gdXBkYXRlIHRoZSB0aW1lIHdoZW4gaXQgaXMgaW5zdGFudGlhdGVkXHJcblxyXG59KSgodHlwZW9mKG1vZHVsZSkgIT09IFwidW5kZWZpbmVkXCIgJiYgbW9kdWxlLmV4cG9ydHMgJiYgdHlwZW9mKGdsb2JhbCkgIT09IFwidW5kZWZpbmVkXCIpID8gZ2xvYmFsIDogdGhpcyB8fCB3aW5kb3csIFwiVHdlZW5MaXRlXCIpO1xuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL19IVE1ML3NyYy9qcy9fdmVuZG9ycy9nc2FwL1R3ZWVuTGl0ZS5qcyJdLCJzb3VyY2VSb290IjoiIn0=