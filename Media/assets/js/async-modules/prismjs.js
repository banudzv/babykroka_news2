webpackJsonp([10],Array(107).concat([
/* 107 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * Подключение плагина `prismjs`
 * @module
 */

// ----------------------------------------
// Imports
// ----------------------------------------

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _Preloader = __webpack_require__(1);

var _Preloader2 = _interopRequireDefault(_Preloader);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// ----------------------------------------
// Public
// ----------------------------------------

/**
 * @param {JQuery.Selector} [selector="code[class*='lang']"]- поиск элементов по селектору
 * @param {JQuery} [$context=null] - контекст в котором будет выполнен поиск селектора
 * @sourceCode
 */
function prism() {
	var selector = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'code[class*="lang"]';
	var $context = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;

	var $code = selector && selector.jquery ? selector : $(selector, $context);
	if ($code.length) {
		var preloader = new _Preloader2.default($code.parent('pre'));
		preloader.show();
		new Promise(function(resolve) { resolve(); }).then(__webpack_require__.bind(null, 108)).then(function () {
			preloader.hide();
		});
	}
}

// ----------------------------------------
// Exports
// ----------------------------------------

exports.default = prism;

/***/ }),
/* 108 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * Импорт нужных компонентов и плагинов
 * @module
 */

// ----------------------------------------
// Imports
// ----------------------------------------

__webpack_require__(109);

__webpack_require__(117);

__webpack_require__(118);

__webpack_require__(119);

__webpack_require__(120);

__webpack_require__(121);

__webpack_require__(122);

__webpack_require__(123);

__webpack_require__(125);

__webpack_require__(126);

__webpack_require__(128);

__webpack_require__(129);

__webpack_require__(131);

__webpack_require__(132);

__webpack_require__(133);

__webpack_require__(134);

__webpack_require__(135);

__webpack_require__(136);

__webpack_require__(137);

__webpack_require__(139);

/***/ }),
/* 109 */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;(function (global, factory) {
    if (true) {
        !(__WEBPACK_AMD_DEFINE_ARRAY__ = [module, __webpack_require__(110), __webpack_require__(112), __webpack_require__(113)], __WEBPACK_AMD_DEFINE_FACTORY__ = (factory),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
    } else if (typeof exports !== "undefined") {
        factory(module, require('./clipboard-action'), require('tiny-emitter'), require('good-listener'));
    } else {
        var mod = {
            exports: {}
        };
        factory(mod, global.clipboardAction, global.tinyEmitter, global.goodListener);
        global.clipboard = mod.exports;
    }
})(this, function (module, _clipboardAction, _tinyEmitter, _goodListener) {
    'use strict';

    var _clipboardAction2 = _interopRequireDefault(_clipboardAction);

    var _tinyEmitter2 = _interopRequireDefault(_tinyEmitter);

    var _goodListener2 = _interopRequireDefault(_goodListener);

    function _interopRequireDefault(obj) {
        return obj && obj.__esModule ? obj : {
            default: obj
        };
    }

    var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) {
        return typeof obj;
    } : function (obj) {
        return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };

    function _classCallCheck(instance, Constructor) {
        if (!(instance instanceof Constructor)) {
            throw new TypeError("Cannot call a class as a function");
        }
    }

    var _createClass = function () {
        function defineProperties(target, props) {
            for (var i = 0; i < props.length; i++) {
                var descriptor = props[i];
                descriptor.enumerable = descriptor.enumerable || false;
                descriptor.configurable = true;
                if ("value" in descriptor) descriptor.writable = true;
                Object.defineProperty(target, descriptor.key, descriptor);
            }
        }

        return function (Constructor, protoProps, staticProps) {
            if (protoProps) defineProperties(Constructor.prototype, protoProps);
            if (staticProps) defineProperties(Constructor, staticProps);
            return Constructor;
        };
    }();

    function _possibleConstructorReturn(self, call) {
        if (!self) {
            throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
        }

        return call && (typeof call === "object" || typeof call === "function") ? call : self;
    }

    function _inherits(subClass, superClass) {
        if (typeof superClass !== "function" && superClass !== null) {
            throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
        }

        subClass.prototype = Object.create(superClass && superClass.prototype, {
            constructor: {
                value: subClass,
                enumerable: false,
                writable: true,
                configurable: true
            }
        });
        if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
    }

    var Clipboard = function (_Emitter) {
        _inherits(Clipboard, _Emitter);

        /**
         * @param {String|HTMLElement|HTMLCollection|NodeList} trigger
         * @param {Object} options
         */
        function Clipboard(trigger, options) {
            _classCallCheck(this, Clipboard);

            var _this = _possibleConstructorReturn(this, (Clipboard.__proto__ || Object.getPrototypeOf(Clipboard)).call(this));

            _this.resolveOptions(options);
            _this.listenClick(trigger);
            return _this;
        }

        /**
         * Defines if attributes would be resolved using internal setter functions
         * or custom functions that were passed in the constructor.
         * @param {Object} options
         */


        _createClass(Clipboard, [{
            key: 'resolveOptions',
            value: function resolveOptions() {
                var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

                this.action = typeof options.action === 'function' ? options.action : this.defaultAction;
                this.target = typeof options.target === 'function' ? options.target : this.defaultTarget;
                this.text = typeof options.text === 'function' ? options.text : this.defaultText;
                this.container = _typeof(options.container) === 'object' ? options.container : document.body;
            }
        }, {
            key: 'listenClick',
            value: function listenClick(trigger) {
                var _this2 = this;

                this.listener = (0, _goodListener2.default)(trigger, 'click', function (e) {
                    return _this2.onClick(e);
                });
            }
        }, {
            key: 'onClick',
            value: function onClick(e) {
                var trigger = e.delegateTarget || e.currentTarget;

                if (this.clipboardAction) {
                    this.clipboardAction = null;
                }

                this.clipboardAction = new _clipboardAction2.default({
                    action: this.action(trigger),
                    target: this.target(trigger),
                    text: this.text(trigger),
                    container: this.container,
                    trigger: trigger,
                    emitter: this
                });
            }
        }, {
            key: 'defaultAction',
            value: function defaultAction(trigger) {
                return getAttributeValue('action', trigger);
            }
        }, {
            key: 'defaultTarget',
            value: function defaultTarget(trigger) {
                var selector = getAttributeValue('target', trigger);

                if (selector) {
                    return document.querySelector(selector);
                }
            }
        }, {
            key: 'defaultText',
            value: function defaultText(trigger) {
                return getAttributeValue('text', trigger);
            }
        }, {
            key: 'destroy',
            value: function destroy() {
                this.listener.destroy();

                if (this.clipboardAction) {
                    this.clipboardAction.destroy();
                    this.clipboardAction = null;
                }
            }
        }], [{
            key: 'isSupported',
            value: function isSupported() {
                var action = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : ['copy', 'cut'];

                var actions = typeof action === 'string' ? [action] : action;
                var support = !!document.queryCommandSupported;

                actions.forEach(function (action) {
                    support = support && !!document.queryCommandSupported(action);
                });

                return support;
            }
        }]);

        return Clipboard;
    }(_tinyEmitter2.default);

    /**
     * Helper function to retrieve attribute value.
     * @param {String} suffix
     * @param {Element} element
     */
    function getAttributeValue(suffix, element) {
        var attribute = 'data-clipboard-' + suffix;

        if (!element.hasAttribute(attribute)) {
            return;
        }

        return element.getAttribute(attribute);
    }

    module.exports = Clipboard;
});

/***/ }),
/* 110 */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;(function (global, factory) {
    if (true) {
        !(__WEBPACK_AMD_DEFINE_ARRAY__ = [module, __webpack_require__(111)], __WEBPACK_AMD_DEFINE_FACTORY__ = (factory),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
    } else if (typeof exports !== "undefined") {
        factory(module, require('select'));
    } else {
        var mod = {
            exports: {}
        };
        factory(mod, global.select);
        global.clipboardAction = mod.exports;
    }
})(this, function (module, _select) {
    'use strict';

    var _select2 = _interopRequireDefault(_select);

    function _interopRequireDefault(obj) {
        return obj && obj.__esModule ? obj : {
            default: obj
        };
    }

    var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) {
        return typeof obj;
    } : function (obj) {
        return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };

    function _classCallCheck(instance, Constructor) {
        if (!(instance instanceof Constructor)) {
            throw new TypeError("Cannot call a class as a function");
        }
    }

    var _createClass = function () {
        function defineProperties(target, props) {
            for (var i = 0; i < props.length; i++) {
                var descriptor = props[i];
                descriptor.enumerable = descriptor.enumerable || false;
                descriptor.configurable = true;
                if ("value" in descriptor) descriptor.writable = true;
                Object.defineProperty(target, descriptor.key, descriptor);
            }
        }

        return function (Constructor, protoProps, staticProps) {
            if (protoProps) defineProperties(Constructor.prototype, protoProps);
            if (staticProps) defineProperties(Constructor, staticProps);
            return Constructor;
        };
    }();

    var ClipboardAction = function () {
        /**
         * @param {Object} options
         */
        function ClipboardAction(options) {
            _classCallCheck(this, ClipboardAction);

            this.resolveOptions(options);
            this.initSelection();
        }

        /**
         * Defines base properties passed from constructor.
         * @param {Object} options
         */


        _createClass(ClipboardAction, [{
            key: 'resolveOptions',
            value: function resolveOptions() {
                var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

                this.action = options.action;
                this.container = options.container;
                this.emitter = options.emitter;
                this.target = options.target;
                this.text = options.text;
                this.trigger = options.trigger;

                this.selectedText = '';
            }
        }, {
            key: 'initSelection',
            value: function initSelection() {
                if (this.text) {
                    this.selectFake();
                } else if (this.target) {
                    this.selectTarget();
                }
            }
        }, {
            key: 'selectFake',
            value: function selectFake() {
                var _this = this;

                var isRTL = document.documentElement.getAttribute('dir') == 'rtl';

                this.removeFake();

                this.fakeHandlerCallback = function () {
                    return _this.removeFake();
                };
                this.fakeHandler = this.container.addEventListener('click', this.fakeHandlerCallback) || true;

                this.fakeElem = document.createElement('textarea');
                // Prevent zooming on iOS
                this.fakeElem.style.fontSize = '12pt';
                // Reset box model
                this.fakeElem.style.border = '0';
                this.fakeElem.style.padding = '0';
                this.fakeElem.style.margin = '0';
                // Move element out of screen horizontally
                this.fakeElem.style.position = 'absolute';
                this.fakeElem.style[isRTL ? 'right' : 'left'] = '-9999px';
                // Move element to the same position vertically
                var yPosition = window.pageYOffset || document.documentElement.scrollTop;
                this.fakeElem.style.top = yPosition + 'px';

                this.fakeElem.setAttribute('readonly', '');
                this.fakeElem.value = this.text;

                this.container.appendChild(this.fakeElem);

                this.selectedText = (0, _select2.default)(this.fakeElem);
                this.copyText();
            }
        }, {
            key: 'removeFake',
            value: function removeFake() {
                if (this.fakeHandler) {
                    this.container.removeEventListener('click', this.fakeHandlerCallback);
                    this.fakeHandler = null;
                    this.fakeHandlerCallback = null;
                }

                if (this.fakeElem) {
                    this.container.removeChild(this.fakeElem);
                    this.fakeElem = null;
                }
            }
        }, {
            key: 'selectTarget',
            value: function selectTarget() {
                this.selectedText = (0, _select2.default)(this.target);
                this.copyText();
            }
        }, {
            key: 'copyText',
            value: function copyText() {
                var succeeded = void 0;

                try {
                    succeeded = document.execCommand(this.action);
                } catch (err) {
                    succeeded = false;
                }

                this.handleResult(succeeded);
            }
        }, {
            key: 'handleResult',
            value: function handleResult(succeeded) {
                this.emitter.emit(succeeded ? 'success' : 'error', {
                    action: this.action,
                    text: this.selectedText,
                    trigger: this.trigger,
                    clearSelection: this.clearSelection.bind(this)
                });
            }
        }, {
            key: 'clearSelection',
            value: function clearSelection() {
                if (this.trigger) {
                    this.trigger.focus();
                }

                window.getSelection().removeAllRanges();
            }
        }, {
            key: 'destroy',
            value: function destroy() {
                this.removeFake();
            }
        }, {
            key: 'action',
            set: function set() {
                var action = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'copy';

                this._action = action;

                if (this._action !== 'copy' && this._action !== 'cut') {
                    throw new Error('Invalid "action" value, use either "copy" or "cut"');
                }
            },
            get: function get() {
                return this._action;
            }
        }, {
            key: 'target',
            set: function set(target) {
                if (target !== undefined) {
                    if (target && (typeof target === 'undefined' ? 'undefined' : _typeof(target)) === 'object' && target.nodeType === 1) {
                        if (this.action === 'copy' && target.hasAttribute('disabled')) {
                            throw new Error('Invalid "target" attribute. Please use "readonly" instead of "disabled" attribute');
                        }

                        if (this.action === 'cut' && (target.hasAttribute('readonly') || target.hasAttribute('disabled'))) {
                            throw new Error('Invalid "target" attribute. You can\'t cut text from elements with "readonly" or "disabled" attributes');
                        }

                        this._target = target;
                    } else {
                        throw new Error('Invalid "target" value, use a valid Element');
                    }
                }
            },
            get: function get() {
                return this._target;
            }
        }]);

        return ClipboardAction;
    }();

    module.exports = ClipboardAction;
});

/***/ }),
/* 111 */
/***/ (function(module, exports) {

function select(element) {
    var selectedText;

    if (element.nodeName === 'SELECT') {
        element.focus();

        selectedText = element.value;
    }
    else if (element.nodeName === 'INPUT' || element.nodeName === 'TEXTAREA') {
        var isReadOnly = element.hasAttribute('readonly');

        if (!isReadOnly) {
            element.setAttribute('readonly', '');
        }

        element.select();
        element.setSelectionRange(0, element.value.length);

        if (!isReadOnly) {
            element.removeAttribute('readonly');
        }

        selectedText = element.value;
    }
    else {
        if (element.hasAttribute('contenteditable')) {
            element.focus();
        }

        var selection = window.getSelection();
        var range = document.createRange();

        range.selectNodeContents(element);
        selection.removeAllRanges();
        selection.addRange(range);

        selectedText = selection.toString();
    }

    return selectedText;
}

module.exports = select;


/***/ }),
/* 112 */
/***/ (function(module, exports) {

function E () {
  // Keep this empty so it's easier to inherit from
  // (via https://github.com/lipsmack from https://github.com/scottcorgan/tiny-emitter/issues/3)
}

E.prototype = {
  on: function (name, callback, ctx) {
    var e = this.e || (this.e = {});

    (e[name] || (e[name] = [])).push({
      fn: callback,
      ctx: ctx
    });

    return this;
  },

  once: function (name, callback, ctx) {
    var self = this;
    function listener () {
      self.off(name, listener);
      callback.apply(ctx, arguments);
    };

    listener._ = callback
    return this.on(name, listener, ctx);
  },

  emit: function (name) {
    var data = [].slice.call(arguments, 1);
    var evtArr = ((this.e || (this.e = {}))[name] || []).slice();
    var i = 0;
    var len = evtArr.length;

    for (i; i < len; i++) {
      evtArr[i].fn.apply(evtArr[i].ctx, data);
    }

    return this;
  },

  off: function (name, callback) {
    var e = this.e || (this.e = {});
    var evts = e[name];
    var liveEvents = [];

    if (evts && callback) {
      for (var i = 0, len = evts.length; i < len; i++) {
        if (evts[i].fn !== callback && evts[i].fn._ !== callback)
          liveEvents.push(evts[i]);
      }
    }

    // Remove event from queue to prevent memory leak
    // Suggested by https://github.com/lazd
    // Ref: https://github.com/scottcorgan/tiny-emitter/commit/c6ebfaa9bc973b33d110a84a307742b7cf94c953#commitcomment-5024910

    (liveEvents.length)
      ? e[name] = liveEvents
      : delete e[name];

    return this;
  }
};

module.exports = E;


/***/ }),
/* 113 */
/***/ (function(module, exports, __webpack_require__) {

var is = __webpack_require__(114);
var delegate = __webpack_require__(115);

/**
 * Validates all params and calls the right
 * listener function based on its target type.
 *
 * @param {String|HTMLElement|HTMLCollection|NodeList} target
 * @param {String} type
 * @param {Function} callback
 * @return {Object}
 */
function listen(target, type, callback) {
    if (!target && !type && !callback) {
        throw new Error('Missing required arguments');
    }

    if (!is.string(type)) {
        throw new TypeError('Second argument must be a String');
    }

    if (!is.fn(callback)) {
        throw new TypeError('Third argument must be a Function');
    }

    if (is.node(target)) {
        return listenNode(target, type, callback);
    }
    else if (is.nodeList(target)) {
        return listenNodeList(target, type, callback);
    }
    else if (is.string(target)) {
        return listenSelector(target, type, callback);
    }
    else {
        throw new TypeError('First argument must be a String, HTMLElement, HTMLCollection, or NodeList');
    }
}

/**
 * Adds an event listener to a HTML element
 * and returns a remove listener function.
 *
 * @param {HTMLElement} node
 * @param {String} type
 * @param {Function} callback
 * @return {Object}
 */
function listenNode(node, type, callback) {
    node.addEventListener(type, callback);

    return {
        destroy: function() {
            node.removeEventListener(type, callback);
        }
    }
}

/**
 * Add an event listener to a list of HTML elements
 * and returns a remove listener function.
 *
 * @param {NodeList|HTMLCollection} nodeList
 * @param {String} type
 * @param {Function} callback
 * @return {Object}
 */
function listenNodeList(nodeList, type, callback) {
    Array.prototype.forEach.call(nodeList, function(node) {
        node.addEventListener(type, callback);
    });

    return {
        destroy: function() {
            Array.prototype.forEach.call(nodeList, function(node) {
                node.removeEventListener(type, callback);
            });
        }
    }
}

/**
 * Add an event listener to a selector
 * and returns a remove listener function.
 *
 * @param {String} selector
 * @param {String} type
 * @param {Function} callback
 * @return {Object}
 */
function listenSelector(selector, type, callback) {
    return delegate(document.body, selector, type, callback);
}

module.exports = listen;


/***/ }),
/* 114 */
/***/ (function(module, exports) {

/**
 * Check if argument is a HTML element.
 *
 * @param {Object} value
 * @return {Boolean}
 */
exports.node = function(value) {
    return value !== undefined
        && value instanceof HTMLElement
        && value.nodeType === 1;
};

/**
 * Check if argument is a list of HTML elements.
 *
 * @param {Object} value
 * @return {Boolean}
 */
exports.nodeList = function(value) {
    var type = Object.prototype.toString.call(value);

    return value !== undefined
        && (type === '[object NodeList]' || type === '[object HTMLCollection]')
        && ('length' in value)
        && (value.length === 0 || exports.node(value[0]));
};

/**
 * Check if argument is a string.
 *
 * @param {Object} value
 * @return {Boolean}
 */
exports.string = function(value) {
    return typeof value === 'string'
        || value instanceof String;
};

/**
 * Check if argument is a function.
 *
 * @param {Object} value
 * @return {Boolean}
 */
exports.fn = function(value) {
    var type = Object.prototype.toString.call(value);

    return type === '[object Function]';
};


/***/ }),
/* 115 */
/***/ (function(module, exports, __webpack_require__) {

var closest = __webpack_require__(116);

/**
 * Delegates event to a selector.
 *
 * @param {Element} element
 * @param {String} selector
 * @param {String} type
 * @param {Function} callback
 * @param {Boolean} useCapture
 * @return {Object}
 */
function _delegate(element, selector, type, callback, useCapture) {
    var listenerFn = listener.apply(this, arguments);

    element.addEventListener(type, listenerFn, useCapture);

    return {
        destroy: function() {
            element.removeEventListener(type, listenerFn, useCapture);
        }
    }
}

/**
 * Delegates event to a selector.
 *
 * @param {Element|String|Array} [elements]
 * @param {String} selector
 * @param {String} type
 * @param {Function} callback
 * @param {Boolean} useCapture
 * @return {Object}
 */
function delegate(elements, selector, type, callback, useCapture) {
    // Handle the regular Element usage
    if (typeof elements.addEventListener === 'function') {
        return _delegate.apply(null, arguments);
    }

    // Handle Element-less usage, it defaults to global delegation
    if (typeof type === 'function') {
        // Use `document` as the first parameter, then apply arguments
        // This is a short way to .unshift `arguments` without running into deoptimizations
        return _delegate.bind(null, document).apply(null, arguments);
    }

    // Handle Selector-based usage
    if (typeof elements === 'string') {
        elements = document.querySelectorAll(elements);
    }

    // Handle Array-like based usage
    return Array.prototype.map.call(elements, function (element) {
        return _delegate(element, selector, type, callback, useCapture);
    });
}

/**
 * Finds closest match and invokes callback.
 *
 * @param {Element} element
 * @param {String} selector
 * @param {String} type
 * @param {Function} callback
 * @return {Function}
 */
function listener(element, selector, type, callback) {
    return function(e) {
        e.delegateTarget = closest(e.target, selector);

        if (e.delegateTarget) {
            callback.call(element, e);
        }
    }
}

module.exports = delegate;


/***/ }),
/* 116 */
/***/ (function(module, exports) {

var DOCUMENT_NODE_TYPE = 9;

/**
 * A polyfill for Element.matches()
 */
if (typeof Element !== 'undefined' && !Element.prototype.matches) {
    var proto = Element.prototype;

    proto.matches = proto.matchesSelector ||
                    proto.mozMatchesSelector ||
                    proto.msMatchesSelector ||
                    proto.oMatchesSelector ||
                    proto.webkitMatchesSelector;
}

/**
 * Finds the closest parent that matches a selector.
 *
 * @param {Element} element
 * @param {String} selector
 * @return {Function}
 */
function closest (element, selector) {
    while (element && element.nodeType !== DOCUMENT_NODE_TYPE) {
        if (typeof element.matches === 'function' &&
            element.matches(selector)) {
          return element;
        }
        element = element.parentNode;
    }
}

module.exports = closest;


/***/ }),
/* 117 */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {
/* **********************************************
     Begin prism-core.js
********************************************** */

var _self = (typeof window !== 'undefined')
	? window   // if in browser
	: (
		(typeof WorkerGlobalScope !== 'undefined' && self instanceof WorkerGlobalScope)
		? self // if in worker
		: {}   // if in node js
	);

/**
 * Prism: Lightweight, robust, elegant syntax highlighting
 * MIT license http://www.opensource.org/licenses/mit-license.php/
 * @author Lea Verou http://lea.verou.me
 */

var Prism = (function(){

// Private helper vars
var lang = /\blang(?:uage)?-(\w+)\b/i;
var uniqueId = 0;

var _ = _self.Prism = {
	manual: _self.Prism && _self.Prism.manual,
	disableWorkerMessageHandler: _self.Prism && _self.Prism.disableWorkerMessageHandler,
	util: {
		encode: function (tokens) {
			if (tokens instanceof Token) {
				return new Token(tokens.type, _.util.encode(tokens.content), tokens.alias);
			} else if (_.util.type(tokens) === 'Array') {
				return tokens.map(_.util.encode);
			} else {
				return tokens.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/\u00a0/g, ' ');
			}
		},

		type: function (o) {
			return Object.prototype.toString.call(o).match(/\[object (\w+)\]/)[1];
		},

		objId: function (obj) {
			if (!obj['__id']) {
				Object.defineProperty(obj, '__id', { value: ++uniqueId });
			}
			return obj['__id'];
		},

		// Deep clone a language definition (e.g. to extend it)
		clone: function (o, visited) {
			var type = _.util.type(o);
			visited = visited || {};

			switch (type) {
				case 'Object':
					if (visited[_.util.objId(o)]) {
						return visited[_.util.objId(o)];
					}
					var clone = {};
					visited[_.util.objId(o)] = clone;

					for (var key in o) {
						if (o.hasOwnProperty(key)) {
							clone[key] = _.util.clone(o[key], visited);
						}
					}

					return clone;

				case 'Array':
					if (visited[_.util.objId(o)]) {
						return visited[_.util.objId(o)];
					}
					var clone = [];
					visited[_.util.objId(o)] = clone;

					o.forEach(function (v, i) {
						clone[i] = _.util.clone(v, visited);
					});

					return clone;
			}

			return o;
		}
	},

	languages: {
		extend: function (id, redef) {
			var lang = _.util.clone(_.languages[id]);

			for (var key in redef) {
				lang[key] = redef[key];
			}

			return lang;
		},

		/**
		 * Insert a token before another token in a language literal
		 * As this needs to recreate the object (we cannot actually insert before keys in object literals),
		 * we cannot just provide an object, we need anobject and a key.
		 * @param inside The key (or language id) of the parent
		 * @param before The key to insert before. If not provided, the function appends instead.
		 * @param insert Object with the key/value pairs to insert
		 * @param root The object that contains `inside`. If equal to Prism.languages, it can be omitted.
		 */
		insertBefore: function (inside, before, insert, root) {
			root = root || _.languages;
			var grammar = root[inside];

			if (arguments.length == 2) {
				insert = arguments[1];

				for (var newToken in insert) {
					if (insert.hasOwnProperty(newToken)) {
						grammar[newToken] = insert[newToken];
					}
				}

				return grammar;
			}

			var ret = {};

			for (var token in grammar) {

				if (grammar.hasOwnProperty(token)) {

					if (token == before) {

						for (var newToken in insert) {

							if (insert.hasOwnProperty(newToken)) {
								ret[newToken] = insert[newToken];
							}
						}
					}

					ret[token] = grammar[token];
				}
			}

			// Update references in other language definitions
			_.languages.DFS(_.languages, function(key, value) {
				if (value === root[inside] && key != inside) {
					this[key] = ret;
				}
			});

			return root[inside] = ret;
		},

		// Traverse a language definition with Depth First Search
		DFS: function(o, callback, type, visited) {
			visited = visited || {};
			for (var i in o) {
				if (o.hasOwnProperty(i)) {
					callback.call(o, i, o[i], type || i);

					if (_.util.type(o[i]) === 'Object' && !visited[_.util.objId(o[i])]) {
						visited[_.util.objId(o[i])] = true;
						_.languages.DFS(o[i], callback, null, visited);
					}
					else if (_.util.type(o[i]) === 'Array' && !visited[_.util.objId(o[i])]) {
						visited[_.util.objId(o[i])] = true;
						_.languages.DFS(o[i], callback, i, visited);
					}
				}
			}
		}
	},
	plugins: {},

	highlightAll: function(async, callback) {
		_.highlightAllUnder(document, async, callback);
	},

	highlightAllUnder: function(container, async, callback) {
		var env = {
			callback: callback,
			selector: 'code[class*="language-"], [class*="language-"] code, code[class*="lang-"], [class*="lang-"] code'
		};

		_.hooks.run("before-highlightall", env);

		var elements = env.elements || container.querySelectorAll(env.selector);

		for (var i=0, element; element = elements[i++];) {
			_.highlightElement(element, async === true, env.callback);
		}
	},

	highlightElement: function(element, async, callback) {
		// Find language
		var language, grammar, parent = element;

		while (parent && !lang.test(parent.className)) {
			parent = parent.parentNode;
		}

		if (parent) {
			language = (parent.className.match(lang) || [,''])[1].toLowerCase();
			grammar = _.languages[language];
		}

		// Set language on the element, if not present
		element.className = element.className.replace(lang, '').replace(/\s+/g, ' ') + ' language-' + language;

		if (element.parentNode) {
			// Set language on the parent, for styling
			parent = element.parentNode;

			if (/pre/i.test(parent.nodeName)) {
				parent.className = parent.className.replace(lang, '').replace(/\s+/g, ' ') + ' language-' + language;
			}
		}

		var code = element.textContent;

		var env = {
			element: element,
			language: language,
			grammar: grammar,
			code: code
		};

		_.hooks.run('before-sanity-check', env);

		if (!env.code || !env.grammar) {
			if (env.code) {
				_.hooks.run('before-highlight', env);
				env.element.textContent = env.code;
				_.hooks.run('after-highlight', env);
			}
			_.hooks.run('complete', env);
			return;
		}

		_.hooks.run('before-highlight', env);

		if (async && _self.Worker) {
			var worker = new Worker(_.filename);

			worker.onmessage = function(evt) {
				env.highlightedCode = evt.data;

				_.hooks.run('before-insert', env);

				env.element.innerHTML = env.highlightedCode;

				callback && callback.call(env.element);
				_.hooks.run('after-highlight', env);
				_.hooks.run('complete', env);
			};

			worker.postMessage(JSON.stringify({
				language: env.language,
				code: env.code,
				immediateClose: true
			}));
		}
		else {
			env.highlightedCode = _.highlight(env.code, env.grammar, env.language);

			_.hooks.run('before-insert', env);

			env.element.innerHTML = env.highlightedCode;

			callback && callback.call(element);

			_.hooks.run('after-highlight', env);
			_.hooks.run('complete', env);
		}
	},

	highlight: function (text, grammar, language) {
		var env = {
			text: text,
			grammar: grammar,
			language: language
		};
		env.tokens = _.tokenize(text, grammar);
		_.hooks.run('after-tokenize', env);
		return Token.stringify(_.util.encode(env.tokens), language);
	},

	matchGrammar: function (text, strarr, grammar, index, startPos, oneshot, target) {
		var Token = _.Token;

		for (var token in grammar) {
			if(!grammar.hasOwnProperty(token) || !grammar[token]) {
				continue;
			}

			if (token == target) {
				return;
			}

			var patterns = grammar[token];
			patterns = (_.util.type(patterns) === "Array") ? patterns : [patterns];

			for (var j = 0; j < patterns.length; ++j) {
				var pattern = patterns[j],
					inside = pattern.inside,
					lookbehind = !!pattern.lookbehind,
					greedy = !!pattern.greedy,
					lookbehindLength = 0,
					alias = pattern.alias;

				if (greedy && !pattern.pattern.global) {
					// Without the global flag, lastIndex won't work
					var flags = pattern.pattern.toString().match(/[imuy]*$/)[0];
					pattern.pattern = RegExp(pattern.pattern.source, flags + "g");
				}

				pattern = pattern.pattern || pattern;

				// Don’t cache length as it changes during the loop
				for (var i = index, pos = startPos; i < strarr.length; pos += strarr[i].length, ++i) {

					var str = strarr[i];

					if (strarr.length > text.length) {
						// Something went terribly wrong, ABORT, ABORT!
						return;
					}

					if (str instanceof Token) {
						continue;
					}

					pattern.lastIndex = 0;

					var match = pattern.exec(str),
					    delNum = 1;

					// Greedy patterns can override/remove up to two previously matched tokens
					if (!match && greedy && i != strarr.length - 1) {
						pattern.lastIndex = pos;
						match = pattern.exec(text);
						if (!match) {
							break;
						}

						var from = match.index + (lookbehind ? match[1].length : 0),
						    to = match.index + match[0].length,
						    k = i,
						    p = pos;

						for (var len = strarr.length; k < len && (p < to || (!strarr[k].type && !strarr[k - 1].greedy)); ++k) {
							p += strarr[k].length;
							// Move the index i to the element in strarr that is closest to from
							if (from >= p) {
								++i;
								pos = p;
							}
						}

						/*
						 * If strarr[i] is a Token, then the match starts inside another Token, which is invalid
						 * If strarr[k - 1] is greedy we are in conflict with another greedy pattern
						 */
						if (strarr[i] instanceof Token || strarr[k - 1].greedy) {
							continue;
						}

						// Number of tokens to delete and replace with the new match
						delNum = k - i;
						str = text.slice(pos, p);
						match.index -= pos;
					}

					if (!match) {
						if (oneshot) {
							break;
						}

						continue;
					}

					if(lookbehind) {
						lookbehindLength = match[1] ? match[1].length : 0;
					}

					var from = match.index + lookbehindLength,
					    match = match[0].slice(lookbehindLength),
					    to = from + match.length,
					    before = str.slice(0, from),
					    after = str.slice(to);

					var args = [i, delNum];

					if (before) {
						++i;
						pos += before.length;
						args.push(before);
					}

					var wrapped = new Token(token, inside? _.tokenize(match, inside) : match, alias, match, greedy);

					args.push(wrapped);

					if (after) {
						args.push(after);
					}

					Array.prototype.splice.apply(strarr, args);

					if (delNum != 1)
						_.matchGrammar(text, strarr, grammar, i, pos, true, token);

					if (oneshot)
						break;
				}
			}
		}
	},

	tokenize: function(text, grammar, language) {
		var strarr = [text];

		var rest = grammar.rest;

		if (rest) {
			for (var token in rest) {
				grammar[token] = rest[token];
			}

			delete grammar.rest;
		}

		_.matchGrammar(text, strarr, grammar, 0, 0, false);

		return strarr;
	},

	hooks: {
		all: {},

		add: function (name, callback) {
			var hooks = _.hooks.all;

			hooks[name] = hooks[name] || [];

			hooks[name].push(callback);
		},

		run: function (name, env) {
			var callbacks = _.hooks.all[name];

			if (!callbacks || !callbacks.length) {
				return;
			}

			for (var i=0, callback; callback = callbacks[i++];) {
				callback(env);
			}
		}
	}
};

var Token = _.Token = function(type, content, alias, matchedStr, greedy) {
	this.type = type;
	this.content = content;
	this.alias = alias;
	// Copy of the full string this token was created from
	this.length = (matchedStr || "").length|0;
	this.greedy = !!greedy;
};

Token.stringify = function(o, language, parent) {
	if (typeof o == 'string') {
		return o;
	}

	if (_.util.type(o) === 'Array') {
		return o.map(function(element) {
			return Token.stringify(element, language, o);
		}).join('');
	}

	var env = {
		type: o.type,
		content: Token.stringify(o.content, language, parent),
		tag: 'span',
		classes: ['token', o.type],
		attributes: {},
		language: language,
		parent: parent
	};

	if (o.alias) {
		var aliases = _.util.type(o.alias) === 'Array' ? o.alias : [o.alias];
		Array.prototype.push.apply(env.classes, aliases);
	}

	_.hooks.run('wrap', env);

	var attributes = Object.keys(env.attributes).map(function(name) {
		return name + '="' + (env.attributes[name] || '').replace(/"/g, '&quot;') + '"';
	}).join(' ');

	return '<' + env.tag + ' class="' + env.classes.join(' ') + '"' + (attributes ? ' ' + attributes : '') + '>' + env.content + '</' + env.tag + '>';

};

if (!_self.document) {
	if (!_self.addEventListener) {
		// in Node.js
		return _self.Prism;
	}

	if (!_.disableWorkerMessageHandler) {
		// In worker
		_self.addEventListener('message', function (evt) {
			var message = JSON.parse(evt.data),
				lang = message.language,
				code = message.code,
				immediateClose = message.immediateClose;

			_self.postMessage(_.highlight(code, _.languages[lang], lang));
			if (immediateClose) {
				_self.close();
			}
		}, false);
	}

	return _self.Prism;
}

//Get current script and highlight
var script = document.currentScript || [].slice.call(document.getElementsByTagName("script")).pop();

if (script) {
	_.filename = script.src;

	if (!_.manual && !script.hasAttribute('data-manual')) {
		if(document.readyState !== "loading") {
			if (window.requestAnimationFrame) {
				window.requestAnimationFrame(_.highlightAll);
			} else {
				window.setTimeout(_.highlightAll, 16);
			}
		}
		else {
			document.addEventListener('DOMContentLoaded', _.highlightAll);
		}
	}
}

return _self.Prism;

})();

if (typeof module !== 'undefined' && module.exports) {
	module.exports = Prism;
}

// hack for components to work correctly in node.js
if (typeof global !== 'undefined') {
	global.Prism = Prism;
}


/* **********************************************
     Begin prism-markup.js
********************************************** */

Prism.languages.markup = {
	'comment': /<!--[\s\S]*?-->/,
	'prolog': /<\?[\s\S]+?\?>/,
	'doctype': /<!DOCTYPE[\s\S]+?>/i,
	'cdata': /<!\[CDATA\[[\s\S]*?]]>/i,
	'tag': {
		pattern: /<\/?(?!\d)[^\s>\/=$<%]+(?:\s+[^\s>\/=]+(?:=(?:("|')(?:\\[\s\S]|(?!\1)[^\\])*\1|[^\s'">=]+))?)*\s*\/?>/i,
		greedy: true,
		inside: {
			'tag': {
				pattern: /^<\/?[^\s>\/]+/i,
				inside: {
					'punctuation': /^<\/?/,
					'namespace': /^[^\s>\/:]+:/
				}
			},
			'attr-value': {
				pattern: /=(?:("|')(?:\\[\s\S]|(?!\1)[^\\])*\1|[^\s'">=]+)/i,
				inside: {
					'punctuation': [
						/^=/,
						{
							pattern: /(^|[^\\])["']/,
							lookbehind: true
						}
					]
				}
			},
			'punctuation': /\/?>/,
			'attr-name': {
				pattern: /[^\s>\/]+/,
				inside: {
					'namespace': /^[^\s>\/:]+:/
				}
			}

		}
	},
	'entity': /&#?[\da-z]{1,8};/i
};

Prism.languages.markup['tag'].inside['attr-value'].inside['entity'] =
	Prism.languages.markup['entity'];

// Plugin to make entity title show the real entity, idea by Roman Komarov
Prism.hooks.add('wrap', function(env) {

	if (env.type === 'entity') {
		env.attributes['title'] = env.content.replace(/&amp;/, '&');
	}
});

Prism.languages.xml = Prism.languages.markup;
Prism.languages.html = Prism.languages.markup;
Prism.languages.mathml = Prism.languages.markup;
Prism.languages.svg = Prism.languages.markup;


/* **********************************************
     Begin prism-css.js
********************************************** */

Prism.languages.css = {
	'comment': /\/\*[\s\S]*?\*\//,
	'atrule': {
		pattern: /@[\w-]+?.*?(?:;|(?=\s*\{))/i,
		inside: {
			'rule': /@[\w-]+/
			// See rest below
		}
	},
	'url': /url\((?:(["'])(?:\\(?:\r\n|[\s\S])|(?!\1)[^\\\r\n])*\1|.*?)\)/i,
	'selector': /[^{}\s][^{};]*?(?=\s*\{)/,
	'string': {
		pattern: /("|')(?:\\(?:\r\n|[\s\S])|(?!\1)[^\\\r\n])*\1/,
		greedy: true
	},
	'property': /[-_a-z\xA0-\uFFFF][-\w\xA0-\uFFFF]*(?=\s*:)/i,
	'important': /\B!important\b/i,
	'function': /[-a-z0-9]+(?=\()/i,
	'punctuation': /[(){};:]/
};

Prism.languages.css['atrule'].inside.rest = Prism.languages.css;

if (Prism.languages.markup) {
	Prism.languages.insertBefore('markup', 'tag', {
		'style': {
			pattern: /(<style[\s\S]*?>)[\s\S]*?(?=<\/style>)/i,
			lookbehind: true,
			inside: Prism.languages.css,
			alias: 'language-css',
			greedy: true
		}
	});

	Prism.languages.insertBefore('inside', 'attr-value', {
		'style-attr': {
			pattern: /\s*style=("|')(?:\\[\s\S]|(?!\1)[^\\])*\1/i,
			inside: {
				'attr-name': {
					pattern: /^\s*style/i,
					inside: Prism.languages.markup.tag.inside
				},
				'punctuation': /^\s*=\s*['"]|['"]\s*$/,
				'attr-value': {
					pattern: /.+/i,
					inside: Prism.languages.css
				}
			},
			alias: 'language-css'
		}
	}, Prism.languages.markup.tag);
}

/* **********************************************
     Begin prism-clike.js
********************************************** */

Prism.languages.clike = {
	'comment': [
		{
			pattern: /(^|[^\\])\/\*[\s\S]*?(?:\*\/|$)/,
			lookbehind: true
		},
		{
			pattern: /(^|[^\\:])\/\/.*/,
			lookbehind: true
		}
	],
	'string': {
		pattern: /(["'])(?:\\(?:\r\n|[\s\S])|(?!\1)[^\\\r\n])*\1/,
		greedy: true
	},
	'class-name': {
		pattern: /((?:\b(?:class|interface|extends|implements|trait|instanceof|new)\s+)|(?:catch\s+\())[\w.\\]+/i,
		lookbehind: true,
		inside: {
			punctuation: /[.\\]/
		}
	},
	'keyword': /\b(?:if|else|while|do|for|return|in|instanceof|function|new|try|throw|catch|finally|null|break|continue)\b/,
	'boolean': /\b(?:true|false)\b/,
	'function': /[a-z0-9_]+(?=\()/i,
	'number': /\b0x[\da-f]+\b|(?:\b\d+\.?\d*|\B\.\d+)(?:e[+-]?\d+)?/i,
	'operator': /--?|\+\+?|!=?=?|<=?|>=?|==?=?|&&?|\|\|?|\?|\*|\/|~|\^|%/,
	'punctuation': /[{}[\];(),.:]/
};


/* **********************************************
     Begin prism-javascript.js
********************************************** */

Prism.languages.javascript = Prism.languages.extend('clike', {
	'keyword': /\b(?:as|async|await|break|case|catch|class|const|continue|debugger|default|delete|do|else|enum|export|extends|finally|for|from|function|get|if|implements|import|in|instanceof|interface|let|new|null|of|package|private|protected|public|return|set|static|super|switch|this|throw|try|typeof|var|void|while|with|yield)\b/,
	'number': /\b(?:0[xX][\dA-Fa-f]+|0[bB][01]+|0[oO][0-7]+|NaN|Infinity)\b|(?:\b\d+\.?\d*|\B\.\d+)(?:[Ee][+-]?\d+)?/,
	// Allow for all non-ASCII characters (See http://stackoverflow.com/a/2008444)
	'function': /[_$a-z\xA0-\uFFFF][$\w\xA0-\uFFFF]*(?=\s*\()/i,
	'operator': /-[-=]?|\+[+=]?|!=?=?|<<?=?|>>?>?=?|=(?:==?|>)?|&[&=]?|\|[|=]?|\*\*?=?|\/=?|~|\^=?|%=?|\?|\.{3}/
});

Prism.languages.insertBefore('javascript', 'keyword', {
	'regex': {
		pattern: /(^|[^/])\/(?!\/)(\[[^\]\r\n]+]|\\.|[^/\\\[\r\n])+\/[gimyu]{0,5}(?=\s*($|[\r\n,.;})]))/,
		lookbehind: true,
		greedy: true
	},
	// This must be declared before keyword because we use "function" inside the look-forward
	'function-variable': {
		pattern: /[_$a-z\xA0-\uFFFF][$\w\xA0-\uFFFF]*(?=\s*=\s*(?:function\b|(?:\([^()]*\)|[_$a-z\xA0-\uFFFF][$\w\xA0-\uFFFF]*)\s*=>))/i,
		alias: 'function'
	}
});

Prism.languages.insertBefore('javascript', 'string', {
	'template-string': {
		pattern: /`(?:\\[\s\S]|[^\\`])*`/,
		greedy: true,
		inside: {
			'interpolation': {
				pattern: /\$\{[^}]+\}/,
				inside: {
					'interpolation-punctuation': {
						pattern: /^\$\{|\}$/,
						alias: 'punctuation'
					},
					rest: Prism.languages.javascript
				}
			},
			'string': /[\s\S]+/
		}
	}
});

if (Prism.languages.markup) {
	Prism.languages.insertBefore('markup', 'tag', {
		'script': {
			pattern: /(<script[\s\S]*?>)[\s\S]*?(?=<\/script>)/i,
			lookbehind: true,
			inside: Prism.languages.javascript,
			alias: 'language-javascript',
			greedy: true
		}
	});
}

Prism.languages.js = Prism.languages.javascript;


/* **********************************************
     Begin prism-file-highlight.js
********************************************** */

(function () {
	if (typeof self === 'undefined' || !self.Prism || !self.document || !document.querySelector) {
		return;
	}

	self.Prism.fileHighlight = function() {

		var Extensions = {
			'js': 'javascript',
			'py': 'python',
			'rb': 'ruby',
			'ps1': 'powershell',
			'psm1': 'powershell',
			'sh': 'bash',
			'bat': 'batch',
			'h': 'c',
			'tex': 'latex'
		};

		Array.prototype.slice.call(document.querySelectorAll('pre[data-src]')).forEach(function (pre) {
			var src = pre.getAttribute('data-src');

			var language, parent = pre;
			var lang = /\blang(?:uage)?-(?!\*)(\w+)\b/i;
			while (parent && !lang.test(parent.className)) {
				parent = parent.parentNode;
			}

			if (parent) {
				language = (pre.className.match(lang) || [, ''])[1];
			}

			if (!language) {
				var extension = (src.match(/\.(\w+)$/) || [, ''])[1];
				language = Extensions[extension] || extension;
			}

			var code = document.createElement('code');
			code.className = 'language-' + language;

			pre.textContent = '';

			code.textContent = 'Loading…';

			pre.appendChild(code);

			var xhr = new XMLHttpRequest();

			xhr.open('GET', src, true);

			xhr.onreadystatechange = function () {
				if (xhr.readyState == 4) {

					if (xhr.status < 400 && xhr.responseText) {
						code.textContent = xhr.responseText;

						Prism.highlightElement(code);
					}
					else if (xhr.status >= 400) {
						code.textContent = '✖ Error ' + xhr.status + ' while fetching file: ' + xhr.statusText;
					}
					else {
						code.textContent = '✖ Error: File does not exist or is empty';
					}
				}
			};

			xhr.send(null);
		});

	};

	document.addEventListener('DOMContentLoaded', self.Prism.fileHighlight);

})();

/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(3)))

/***/ }),
/* 118 */
/***/ (function(module, exports) {

(function () {

	if (typeof self === 'undefined' || !self.Prism || !self.document || !document.createRange) {
		return;
	}

	Prism.plugins.KeepMarkup = true;

	Prism.hooks.add('before-highlight', function (env) {
		if (!env.element.children.length) {
			return;
		}

		var pos = 0;
		var data = [];
		var f = function (elt, baseNode) {
			var o = {};
			if (!baseNode) {
				// Clone the original tag to keep all attributes
				o.clone = elt.cloneNode(false);
				o.posOpen = pos;
				data.push(o);
			}
			for (var i = 0, l = elt.childNodes.length; i < l; i++) {
				var child = elt.childNodes[i];
				if (child.nodeType === 1) { // element
					f(child);
				} else if(child.nodeType === 3) { // text
					pos += child.data.length;
				}
			}
			if (!baseNode) {
				o.posClose = pos;
			}
		};
		f(env.element, true);

		if (data && data.length) {
			// data is an array of all existing tags
			env.keepMarkup = data;
		}
	});

	Prism.hooks.add('after-highlight', function (env) {
		if(env.keepMarkup && env.keepMarkup.length) {

			var walk = function (elt, nodeState) {
				for (var i = 0, l = elt.childNodes.length; i < l; i++) {

					var child = elt.childNodes[i];

					if (child.nodeType === 1) { // element
						if (!walk(child, nodeState)) {
							return false;
						}

					} else if (child.nodeType === 3) { // text
						if(!nodeState.nodeStart && nodeState.pos + child.data.length > nodeState.node.posOpen) {
							// We found the start position
							nodeState.nodeStart = child;
							nodeState.nodeStartPos = nodeState.node.posOpen - nodeState.pos;
						}
						if(nodeState.nodeStart && nodeState.pos + child.data.length >= nodeState.node.posClose) {
							// We found the end position
							nodeState.nodeEnd = child;
							nodeState.nodeEndPos = nodeState.node.posClose - nodeState.pos;
						}

						nodeState.pos += child.data.length;
					}

					if (nodeState.nodeStart && nodeState.nodeEnd) {
						// Select the range and wrap it with the clone
						var range = document.createRange();
						range.setStart(nodeState.nodeStart, nodeState.nodeStartPos);
						range.setEnd(nodeState.nodeEnd, nodeState.nodeEndPos);
						nodeState.node.clone.appendChild(range.extractContents());
						range.insertNode(nodeState.node.clone);
						range.detach();

						// Process is over
						return false;
					}
				}
				return true;
			};

			// For each tag, we walk the DOM to reinsert it
			env.keepMarkup.forEach(function (node) {
				walk(env.element, {
					node: node,
					pos: 0
				});
			});
			// Store new highlightedCode for later hooks calls
			env.highlightedCode = env.element.innerHTML;
		}
	});
}());


/***/ }),
/* 119 */
/***/ (function(module, exports) {

(function () {

	if (typeof self === 'undefined' || !self.Prism || !self.document || !Prism.languages.markup) {
		return;
	}

	Prism.plugins.UnescapedMarkup = true;

	Prism.hooks.add('before-highlightall', function (env) {
		env.selector += ", [class*='lang-'] script[type='text/plain'], [class*='language-'] script[type='text/plain']" +
		                ", script[type='text/plain'][class*='lang-'], script[type='text/plain'][class*='language-']";
	});

	Prism.hooks.add('before-sanity-check', function (env) {
		if ((env.element.matches || env.element.msMatchesSelector).call(env.element, "script[type='text/plain']")) {
			var code = document.createElement("code");
			var pre = document.createElement("pre");

			pre.className = code.className = env.element.className;

			if (env.element.dataset) {
				Object.keys(env.element.dataset).forEach(function (key) {
					if (Object.prototype.hasOwnProperty.call(env.element.dataset, key)) {
						pre.dataset[key] = env.element.dataset[key];
					}
				});
			}

			env.code = env.code.replace(/&lt;\/script(>|&gt;)/gi, "</scri" + "pt>");
			code.textContent = env.code;

			pre.appendChild(code);
			env.element.parentNode.replaceChild(pre, env.element);
			env.element = code;
			return;
		}

		var pre = env.element.parentNode;
		if (!env.code && pre && pre.nodeName.toLowerCase() == 'pre' &&
				env.element.childNodes.length && env.element.childNodes[0].nodeName == "#comment") {
			env.element.textContent = env.code = env.element.childNodes[0].textContent;
		}
	});
}());


/***/ }),
/* 120 */
/***/ (function(module, exports) {

(function() {

var assign = Object.assign || function (obj1, obj2) {
	for (var name in obj2) {
		if (obj2.hasOwnProperty(name))
			obj1[name] = obj2[name];
	}
	return obj1;
}

function NormalizeWhitespace(defaults) {
	this.defaults = assign({}, defaults);
}

function toCamelCase(value) {
	return value.replace(/-(\w)/g, function(match, firstChar) {
		return firstChar.toUpperCase();
	});
}

function tabLen(str) {
	var res = 0;
	for (var i = 0; i < str.length; ++i) {
		if (str.charCodeAt(i) == '\t'.charCodeAt(0))
			res += 3;
	}
	return str.length + res;
}

NormalizeWhitespace.prototype = {
	setDefaults: function (defaults) {
		this.defaults = assign(this.defaults, defaults);
	},
	normalize: function (input, settings) {
		settings = assign(this.defaults, settings);

		for (var name in settings) {
			var methodName = toCamelCase(name);
			if (name !== "normalize" && methodName !== 'setDefaults' &&
					settings[name] && this[methodName]) {
				input = this[methodName].call(this, input, settings[name]);
			}
		}

		return input;
	},

	/*
	 * Normalization methods
	 */
	leftTrim: function (input) {
		return input.replace(/^\s+/, '');
	},
	rightTrim: function (input) {
		return input.replace(/\s+$/, '');
	},
	tabsToSpaces: function (input, spaces) {
		spaces = spaces|0 || 4;
		return input.replace(/\t/g, new Array(++spaces).join(' '));
	},
	spacesToTabs: function (input, spaces) {
		spaces = spaces|0 || 4;
		return input.replace(new RegExp(' {' + spaces + '}', 'g'), '\t');
	},
	removeTrailing: function (input) {
		return input.replace(/\s*?$/gm, '');
	},
	// Support for deprecated plugin remove-initial-line-feed
	removeInitialLineFeed: function (input) {
		return input.replace(/^(?:\r?\n|\r)/, '');
	},
	removeIndent: function (input) {
		var indents = input.match(/^[^\S\n\r]*(?=\S)/gm);

		if (!indents || !indents[0].length)
			return input;

		indents.sort(function(a, b){return a.length - b.length; });

		if (!indents[0].length)
			return input;

		return input.replace(new RegExp('^' + indents[0], 'gm'), '');
	},
	indent: function (input, tabs) {
		return input.replace(/^[^\S\n\r]*(?=\S)/gm, new Array(++tabs).join('\t') + '$&');
	},
	breakLines: function (input, characters) {
		characters = (characters === true) ? 80 : characters|0 || 80;

		var lines = input.split('\n');
		for (var i = 0; i < lines.length; ++i) {
			if (tabLen(lines[i]) <= characters)
				continue;

			var line = lines[i].split(/(\s+)/g),
			    len = 0;

			for (var j = 0; j < line.length; ++j) {
				var tl = tabLen(line[j]);
				len += tl;
				if (len > characters) {
					line[j] = '\n' + line[j];
					len = tl;
				}
			}
			lines[i] = line.join('');
		}
		return lines.join('\n');
	}
};

// Support node modules
if (typeof module !== 'undefined' && module.exports) {
	module.exports = NormalizeWhitespace;
}

// Exit if prism is not loaded
if (typeof Prism === 'undefined') {
	return;
}

Prism.plugins.NormalizeWhitespace = new NormalizeWhitespace({
	'remove-trailing': true,
	'remove-indent': true,
	'left-trim': true,
	'right-trim': true,
	/*'break-lines': 80,
	'indent': 2,
	'remove-initial-line-feed': false,
	'tabs-to-spaces': 4,
	'spaces-to-tabs': 4*/
});

Prism.hooks.add('before-sanity-check', function (env) {
	var Normalizer = Prism.plugins.NormalizeWhitespace;

	// Check settings
	if (env.settings && env.settings['whitespace-normalization'] === false) {
		return;
	}

	// Simple mode if there is no env.element
	if ((!env.element || !env.element.parentNode) && env.code) {
		env.code = Normalizer.normalize(env.code, env.settings);
		return;
	}

	// Normal mode
	var pre = env.element.parentNode;
	var clsReg = /\bno-whitespace-normalization\b/;
	if (!env.code || !pre || pre.nodeName.toLowerCase() !== 'pre' ||
			clsReg.test(pre.className) || clsReg.test(env.element.className))
		return;

	var children = pre.childNodes,
	    before = '',
	    after = '',
	    codeFound = false;

	// Move surrounding whitespace from the <pre> tag into the <code> tag
	for (var i = 0; i < children.length; ++i) {
		var node = children[i];

		if (node == env.element) {
			codeFound = true;
		} else if (node.nodeName === "#text") {
			if (codeFound) {
				after += node.nodeValue;
			} else {
				before += node.nodeValue;
			}

			pre.removeChild(node);
			--i;
		}
	}

	if (!env.element.children.length || !Prism.plugins.KeepMarkup) {
		env.code = before + env.code + after;
		env.code = Normalizer.normalize(env.code, env.settings);
	} else {
		// Preserve markup for keep-markup plugin
		var html = before + env.element.innerHTML + after;
		env.element.innerHTML = Normalizer.normalize(html, env.settings);
		env.code = env.element.textContent;
	}
});

}());

/***/ }),
/* 121 */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {(function(){

if (
	typeof self !== 'undefined' && !self.Prism ||
	typeof global !== 'undefined' && !global.Prism
) {
	return;
}

Prism.hooks.add('wrap', function(env) {
	if (env.type !== "keyword") {
		return;
	}
	env.classes.push('keyword-' + env.content);
});

})();

/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(3)))

/***/ }),
/* 122 */
/***/ (function(module, exports) {

(function(){
	if (typeof self === 'undefined' || !self.Prism || !self.document) {
		return;
	}

	var callbacks = [];
	var map = {};
	var noop = function() {};

	Prism.plugins.toolbar = {};

	/**
	 * Register a button callback with the toolbar.
	 *
	 * @param {string} key
	 * @param {Object|Function} opts
	 */
	var registerButton = Prism.plugins.toolbar.registerButton = function (key, opts) {
		var callback;

		if (typeof opts === 'function') {
			callback = opts;
		} else {
			callback = function (env) {
				var element;

				if (typeof opts.onClick === 'function') {
					element = document.createElement('button');
					element.type = 'button';
					element.addEventListener('click', function () {
						opts.onClick.call(this, env);
					});
				} else if (typeof opts.url === 'string') {
					element = document.createElement('a');
					element.href = opts.url;
				} else {
					element = document.createElement('span');
				}

				element.textContent = opts.text;

				return element;
			};
		}

		callbacks.push(map[key] = callback);
	};

	/**
	 * Post-highlight Prism hook callback.
	 *
	 * @param env
	 */
	var hook = Prism.plugins.toolbar.hook = function (env) {
		// Check if inline or actual code block (credit to line-numbers plugin)
		var pre = env.element.parentNode;
		if (!pre || !/pre/i.test(pre.nodeName)) {
			return;
		}

		// Autoloader rehighlights, so only do this once.
		if (pre.parentNode.classList.contains('code-toolbar')) {
			return;
		}

		// Create wrapper for <pre> to prevent scrolling toolbar with content
		var wrapper = document.createElement("div");
		wrapper.classList.add("code-toolbar");
		pre.parentNode.insertBefore(wrapper, pre);
		wrapper.appendChild(pre);

		// Setup the toolbar
		var toolbar = document.createElement('div');
		toolbar.classList.add('toolbar');

		if (document.body.hasAttribute('data-toolbar-order')) {
			callbacks = document.body.getAttribute('data-toolbar-order').split(',').map(function(key) {
				return map[key] || noop;
			});
		}

		callbacks.forEach(function(callback) {
			var element = callback(env);

			if (!element) {
				return;
			}

			var item = document.createElement('div');
			item.classList.add('toolbar-item');

			item.appendChild(element);
			toolbar.appendChild(item);
		});

		// Add our toolbar to the currently created wrapper of <pre> tag
		wrapper.appendChild(toolbar);
	};

	registerButton('label', function(env) {
		var pre = env.element.parentNode;
		if (!pre || !/pre/i.test(pre.nodeName)) {
			return;
		}

		if (!pre.hasAttribute('data-label')) {
			return;
		}

		var element, template;
		var text = pre.getAttribute('data-label');
		try {
			// Any normal text will blow up this selector.
			template = document.querySelector('template#' + text);
		} catch (e) {}

		if (template) {
			element = template.content;
		} else {
			if (pre.hasAttribute('data-url')) {
				element = document.createElement('a');
				element.href = pre.getAttribute('data-url');
			} else {
				element = document.createElement('span');
			}

			element.textContent = text;
		}

		return element;
	});

	/**
	 * Register the toolbar with Prism.
	 */
	Prism.hooks.add('complete', hook);
})();


/***/ }),
/* 123 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(124);
if(typeof content === 'string') content = [[module.i, content, '']];
// Prepare cssTransformation
var transform;

var options = {"insertAt":{"before":"#webpack-style-loader-insert-before-this"},"hmr":true}
options.transform = transform
// add the styles to the DOM
var update = __webpack_require__(10)(content, options);
if(content.locals) module.exports = content.locals;
// Hot Module Replacement
if(false) {
	// When the styles change, update the <style> tags
	if(!content.locals) {
		module.hot.accept("!!../../../css-loader/index.js??ref--1-1!../../../postcss-loader/lib/index.js??ref--1-2!./prism-toolbar.css", function() {
			var newContent = require("!!../../../css-loader/index.js??ref--1-1!../../../postcss-loader/lib/index.js??ref--1-2!./prism-toolbar.css");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
	}
	// When the module is disposed, remove the <style> tags
	module.hot.dispose(function() { update(); });
}

/***/ }),
/* 124 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(9)(false);
// imports


// module
exports.push([module.i, "div.code-toolbar {\n\tposition: relative;\n}\n\ndiv.code-toolbar > .toolbar {\n\tposition: absolute;\n\ttop: .3em;\n\tright: .2em;\n\ttransition: opacity 0.3s ease-in-out;\n\topacity: 0;\n}\n\ndiv.code-toolbar:hover > .toolbar {\n\topacity: 1;\n}\n\ndiv.code-toolbar > .toolbar .toolbar-item {\n\tdisplay: inline-block;\n}\n\ndiv.code-toolbar > .toolbar a {\n\tcursor: pointer;\n}\n\ndiv.code-toolbar > .toolbar button {\n\tbackground: none;\n\tborder: 0;\n\tcolor: inherit;\n\tfont: inherit;\n\tline-height: normal;\n\toverflow: visible;\n\tpadding: 0;\n\t-webkit-user-select: none; /* for button */\n\t-moz-user-select: none;\n\t-ms-user-select: none;\n}\n\ndiv.code-toolbar > .toolbar a,\ndiv.code-toolbar > .toolbar button,\ndiv.code-toolbar > .toolbar span {\n\tcolor: #bbb;\n\tfont-size: .8em;\n\tpadding: 0 .5em;\n\tbackground: #f5f2f0;\n\tbackground: rgba(224, 224, 224, 0.2);\n\tbox-shadow: 0 2px 0 0 rgba(0,0,0,0.2);\n\tborder-radius: .5em;\n}\n\ndiv.code-toolbar > .toolbar a:hover,\ndiv.code-toolbar > .toolbar a:focus,\ndiv.code-toolbar > .toolbar button:hover,\ndiv.code-toolbar > .toolbar button:focus,\ndiv.code-toolbar > .toolbar span:hover,\ndiv.code-toolbar > .toolbar span:focus {\n\tcolor: inherit;\n\ttext-decoration: none;\n}\n", ""]);

// exports


/***/ }),
/* 125 */
/***/ (function(module, exports) {

(function(){

if (typeof self === 'undefined' || !self.Prism || !self.document) {
	return;
}

if (!Prism.plugins.toolbar) {
	console.warn('Show Languages plugin loaded before Toolbar plugin.');

	return;
}

// The languages map is built automatically with gulp
var Languages = /*languages_placeholder[*/{"html":"HTML","xml":"XML","svg":"SVG","mathml":"MathML","css":"CSS","clike":"C-like","javascript":"JavaScript","abap":"ABAP","actionscript":"ActionScript","apacheconf":"Apache Configuration","apl":"APL","applescript":"AppleScript","arff":"ARFF","asciidoc":"AsciiDoc","asm6502":"6502 Assembly","aspnet":"ASP.NET (C#)","autohotkey":"AutoHotkey","autoit":"AutoIt","basic":"BASIC","csharp":"C#","cpp":"C++","coffeescript":"CoffeeScript","csp":"Content-Security-Policy","css-extras":"CSS Extras","django":"Django/Jinja2","erb":"ERB","fsharp":"F#","glsl":"GLSL","graphql":"GraphQL","http":"HTTP","hpkp":"HTTP Public-Key-Pins","hsts":"HTTP Strict-Transport-Security","ichigojam":"IchigoJam","inform7":"Inform 7","json":"JSON","latex":"LaTeX","livescript":"LiveScript","lolcode":"LOLCODE","matlab":"MATLAB","mel":"MEL","n4js":"N4JS","nasm":"NASM","nginx":"nginx","nsis":"NSIS","objectivec":"Objective-C","ocaml":"OCaml","opencl":"OpenCL","parigp":"PARI/GP","php":"PHP","php-extras":"PHP Extras","plsql":"PL/SQL","powershell":"PowerShell","properties":".properties","protobuf":"Protocol Buffers","q":"Q (kdb+ database)","jsx":"React JSX","tsx":"React TSX","renpy":"Ren'py","rest":"reST (reStructuredText)","sas":"SAS","sass":"Sass (Sass)","scss":"Sass (Scss)","sql":"SQL","typescript":"TypeScript","vbnet":"VB.Net","vhdl":"VHDL","vim":"vim","wiki":"Wiki markup","xojo":"Xojo (REALbasic)","yaml":"YAML"}/*]*/;
Prism.plugins.toolbar.registerButton('show-language', function(env) {
	var pre = env.element.parentNode;
	if (!pre || !/pre/i.test(pre.nodeName)) {
		return;
	}
	var language = pre.getAttribute('data-language') || Languages[env.language] || (env.language.substring(0, 1).toUpperCase() + env.language.substring(1));

	var element = document.createElement('span');
	element.textContent = language;

	return element;
});

})();


/***/ }),
/* 126 */
/***/ (function(module, exports, __webpack_require__) {

(function(){
	if (typeof self === 'undefined' || !self.Prism || !self.document) {
		return;
	}

	if (!Prism.plugins.toolbar) {
		console.warn('Copy to Clipboard plugin loaded before Toolbar plugin.');

		return;
	}

	var ClipboardJS = window.ClipboardJS || undefined;

	if (!ClipboardJS && "function" === 'function') {
		ClipboardJS = __webpack_require__(127);
	}

	var callbacks = [];

	if (!ClipboardJS) {
		var script = document.createElement('script');
		var head = document.querySelector('head');

		script.onload = function() {
			ClipboardJS = window.ClipboardJS;

			if (ClipboardJS) {
				while (callbacks.length) {
					callbacks.pop()();
				}
			}
		};

		script.src = 'https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.0/clipboard.min.js';
		head.appendChild(script);
	}

	Prism.plugins.toolbar.registerButton('copy-to-clipboard', function (env) {
		var linkCopy = document.createElement('a');
		linkCopy.textContent = 'Copy';

		if (!ClipboardJS) {
			callbacks.push(registerClipboard);
		} else {
			registerClipboard();
		}

		return linkCopy;

		function registerClipboard() {
			var clip = new ClipboardJS(linkCopy, {
				'text': function () {
					return env.code;
				}
			});

			clip.on('success', function() {
				linkCopy.textContent = 'Copied!';

				resetText();
			});
			clip.on('error', function () {
				linkCopy.textContent = 'Press Ctrl+C to copy';

				resetText();
			});
		}

		function resetText() {
			setTimeout(function () {
				linkCopy.textContent = 'Copy';
			}, 5000);
		}
	});
})();


/***/ }),
/* 127 */
/***/ (function(module, exports, __webpack_require__) {

/*!
 * clipboard.js v2.0.0
 * https://zenorocha.github.io/clipboard.js
 * 
 * Licensed MIT © Zeno Rocha
 */
(function webpackUniversalModuleDefinition(root, factory) {
	if(true)
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else if(typeof exports === 'object')
		exports["ClipboardJS"] = factory();
	else
		root["ClipboardJS"] = factory();
})(this, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 3);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;(function (global, factory) {
    if (true) {
        !(__WEBPACK_AMD_DEFINE_ARRAY__ = [module, __webpack_require__(7)], __WEBPACK_AMD_DEFINE_FACTORY__ = (factory),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
    } else if (typeof exports !== "undefined") {
        factory(module, require('select'));
    } else {
        var mod = {
            exports: {}
        };
        factory(mod, global.select);
        global.clipboardAction = mod.exports;
    }
})(this, function (module, _select) {
    'use strict';

    var _select2 = _interopRequireDefault(_select);

    function _interopRequireDefault(obj) {
        return obj && obj.__esModule ? obj : {
            default: obj
        };
    }

    var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) {
        return typeof obj;
    } : function (obj) {
        return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };

    function _classCallCheck(instance, Constructor) {
        if (!(instance instanceof Constructor)) {
            throw new TypeError("Cannot call a class as a function");
        }
    }

    var _createClass = function () {
        function defineProperties(target, props) {
            for (var i = 0; i < props.length; i++) {
                var descriptor = props[i];
                descriptor.enumerable = descriptor.enumerable || false;
                descriptor.configurable = true;
                if ("value" in descriptor) descriptor.writable = true;
                Object.defineProperty(target, descriptor.key, descriptor);
            }
        }

        return function (Constructor, protoProps, staticProps) {
            if (protoProps) defineProperties(Constructor.prototype, protoProps);
            if (staticProps) defineProperties(Constructor, staticProps);
            return Constructor;
        };
    }();

    var ClipboardAction = function () {
        /**
         * @param {Object} options
         */
        function ClipboardAction(options) {
            _classCallCheck(this, ClipboardAction);

            this.resolveOptions(options);
            this.initSelection();
        }

        /**
         * Defines base properties passed from constructor.
         * @param {Object} options
         */


        _createClass(ClipboardAction, [{
            key: 'resolveOptions',
            value: function resolveOptions() {
                var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

                this.action = options.action;
                this.container = options.container;
                this.emitter = options.emitter;
                this.target = options.target;
                this.text = options.text;
                this.trigger = options.trigger;

                this.selectedText = '';
            }
        }, {
            key: 'initSelection',
            value: function initSelection() {
                if (this.text) {
                    this.selectFake();
                } else if (this.target) {
                    this.selectTarget();
                }
            }
        }, {
            key: 'selectFake',
            value: function selectFake() {
                var _this = this;

                var isRTL = document.documentElement.getAttribute('dir') == 'rtl';

                this.removeFake();

                this.fakeHandlerCallback = function () {
                    return _this.removeFake();
                };
                this.fakeHandler = this.container.addEventListener('click', this.fakeHandlerCallback) || true;

                this.fakeElem = document.createElement('textarea');
                // Prevent zooming on iOS
                this.fakeElem.style.fontSize = '12pt';
                // Reset box model
                this.fakeElem.style.border = '0';
                this.fakeElem.style.padding = '0';
                this.fakeElem.style.margin = '0';
                // Move element out of screen horizontally
                this.fakeElem.style.position = 'absolute';
                this.fakeElem.style[isRTL ? 'right' : 'left'] = '-9999px';
                // Move element to the same position vertically
                var yPosition = window.pageYOffset || document.documentElement.scrollTop;
                this.fakeElem.style.top = yPosition + 'px';

                this.fakeElem.setAttribute('readonly', '');
                this.fakeElem.value = this.text;

                this.container.appendChild(this.fakeElem);

                this.selectedText = (0, _select2.default)(this.fakeElem);
                this.copyText();
            }
        }, {
            key: 'removeFake',
            value: function removeFake() {
                if (this.fakeHandler) {
                    this.container.removeEventListener('click', this.fakeHandlerCallback);
                    this.fakeHandler = null;
                    this.fakeHandlerCallback = null;
                }

                if (this.fakeElem) {
                    this.container.removeChild(this.fakeElem);
                    this.fakeElem = null;
                }
            }
        }, {
            key: 'selectTarget',
            value: function selectTarget() {
                this.selectedText = (0, _select2.default)(this.target);
                this.copyText();
            }
        }, {
            key: 'copyText',
            value: function copyText() {
                var succeeded = void 0;

                try {
                    succeeded = document.execCommand(this.action);
                } catch (err) {
                    succeeded = false;
                }

                this.handleResult(succeeded);
            }
        }, {
            key: 'handleResult',
            value: function handleResult(succeeded) {
                this.emitter.emit(succeeded ? 'success' : 'error', {
                    action: this.action,
                    text: this.selectedText,
                    trigger: this.trigger,
                    clearSelection: this.clearSelection.bind(this)
                });
            }
        }, {
            key: 'clearSelection',
            value: function clearSelection() {
                if (this.trigger) {
                    this.trigger.focus();
                }

                window.getSelection().removeAllRanges();
            }
        }, {
            key: 'destroy',
            value: function destroy() {
                this.removeFake();
            }
        }, {
            key: 'action',
            set: function set() {
                var action = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'copy';

                this._action = action;

                if (this._action !== 'copy' && this._action !== 'cut') {
                    throw new Error('Invalid "action" value, use either "copy" or "cut"');
                }
            },
            get: function get() {
                return this._action;
            }
        }, {
            key: 'target',
            set: function set(target) {
                if (target !== undefined) {
                    if (target && (typeof target === 'undefined' ? 'undefined' : _typeof(target)) === 'object' && target.nodeType === 1) {
                        if (this.action === 'copy' && target.hasAttribute('disabled')) {
                            throw new Error('Invalid "target" attribute. Please use "readonly" instead of "disabled" attribute');
                        }

                        if (this.action === 'cut' && (target.hasAttribute('readonly') || target.hasAttribute('disabled'))) {
                            throw new Error('Invalid "target" attribute. You can\'t cut text from elements with "readonly" or "disabled" attributes');
                        }

                        this._target = target;
                    } else {
                        throw new Error('Invalid "target" value, use a valid Element');
                    }
                }
            },
            get: function get() {
                return this._target;
            }
        }]);

        return ClipboardAction;
    }();

    module.exports = ClipboardAction;
});

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

var is = __webpack_require__(6);
var delegate = __webpack_require__(5);

/**
 * Validates all params and calls the right
 * listener function based on its target type.
 *
 * @param {String|HTMLElement|HTMLCollection|NodeList} target
 * @param {String} type
 * @param {Function} callback
 * @return {Object}
 */
function listen(target, type, callback) {
    if (!target && !type && !callback) {
        throw new Error('Missing required arguments');
    }

    if (!is.string(type)) {
        throw new TypeError('Second argument must be a String');
    }

    if (!is.fn(callback)) {
        throw new TypeError('Third argument must be a Function');
    }

    if (is.node(target)) {
        return listenNode(target, type, callback);
    }
    else if (is.nodeList(target)) {
        return listenNodeList(target, type, callback);
    }
    else if (is.string(target)) {
        return listenSelector(target, type, callback);
    }
    else {
        throw new TypeError('First argument must be a String, HTMLElement, HTMLCollection, or NodeList');
    }
}

/**
 * Adds an event listener to a HTML element
 * and returns a remove listener function.
 *
 * @param {HTMLElement} node
 * @param {String} type
 * @param {Function} callback
 * @return {Object}
 */
function listenNode(node, type, callback) {
    node.addEventListener(type, callback);

    return {
        destroy: function() {
            node.removeEventListener(type, callback);
        }
    }
}

/**
 * Add an event listener to a list of HTML elements
 * and returns a remove listener function.
 *
 * @param {NodeList|HTMLCollection} nodeList
 * @param {String} type
 * @param {Function} callback
 * @return {Object}
 */
function listenNodeList(nodeList, type, callback) {
    Array.prototype.forEach.call(nodeList, function(node) {
        node.addEventListener(type, callback);
    });

    return {
        destroy: function() {
            Array.prototype.forEach.call(nodeList, function(node) {
                node.removeEventListener(type, callback);
            });
        }
    }
}

/**
 * Add an event listener to a selector
 * and returns a remove listener function.
 *
 * @param {String} selector
 * @param {String} type
 * @param {Function} callback
 * @return {Object}
 */
function listenSelector(selector, type, callback) {
    return delegate(document.body, selector, type, callback);
}

module.exports = listen;


/***/ }),
/* 2 */
/***/ (function(module, exports) {

function E () {
  // Keep this empty so it's easier to inherit from
  // (via https://github.com/lipsmack from https://github.com/scottcorgan/tiny-emitter/issues/3)
}

E.prototype = {
  on: function (name, callback, ctx) {
    var e = this.e || (this.e = {});

    (e[name] || (e[name] = [])).push({
      fn: callback,
      ctx: ctx
    });

    return this;
  },

  once: function (name, callback, ctx) {
    var self = this;
    function listener () {
      self.off(name, listener);
      callback.apply(ctx, arguments);
    };

    listener._ = callback
    return this.on(name, listener, ctx);
  },

  emit: function (name) {
    var data = [].slice.call(arguments, 1);
    var evtArr = ((this.e || (this.e = {}))[name] || []).slice();
    var i = 0;
    var len = evtArr.length;

    for (i; i < len; i++) {
      evtArr[i].fn.apply(evtArr[i].ctx, data);
    }

    return this;
  },

  off: function (name, callback) {
    var e = this.e || (this.e = {});
    var evts = e[name];
    var liveEvents = [];

    if (evts && callback) {
      for (var i = 0, len = evts.length; i < len; i++) {
        if (evts[i].fn !== callback && evts[i].fn._ !== callback)
          liveEvents.push(evts[i]);
      }
    }

    // Remove event from queue to prevent memory leak
    // Suggested by https://github.com/lazd
    // Ref: https://github.com/scottcorgan/tiny-emitter/commit/c6ebfaa9bc973b33d110a84a307742b7cf94c953#commitcomment-5024910

    (liveEvents.length)
      ? e[name] = liveEvents
      : delete e[name];

    return this;
  }
};

module.exports = E;


/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;(function (global, factory) {
    if (true) {
        !(__WEBPACK_AMD_DEFINE_ARRAY__ = [module, __webpack_require__(0), __webpack_require__(2), __webpack_require__(1)], __WEBPACK_AMD_DEFINE_FACTORY__ = (factory),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
    } else if (typeof exports !== "undefined") {
        factory(module, require('./clipboard-action'), require('tiny-emitter'), require('good-listener'));
    } else {
        var mod = {
            exports: {}
        };
        factory(mod, global.clipboardAction, global.tinyEmitter, global.goodListener);
        global.clipboard = mod.exports;
    }
})(this, function (module, _clipboardAction, _tinyEmitter, _goodListener) {
    'use strict';

    var _clipboardAction2 = _interopRequireDefault(_clipboardAction);

    var _tinyEmitter2 = _interopRequireDefault(_tinyEmitter);

    var _goodListener2 = _interopRequireDefault(_goodListener);

    function _interopRequireDefault(obj) {
        return obj && obj.__esModule ? obj : {
            default: obj
        };
    }

    var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) {
        return typeof obj;
    } : function (obj) {
        return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };

    function _classCallCheck(instance, Constructor) {
        if (!(instance instanceof Constructor)) {
            throw new TypeError("Cannot call a class as a function");
        }
    }

    var _createClass = function () {
        function defineProperties(target, props) {
            for (var i = 0; i < props.length; i++) {
                var descriptor = props[i];
                descriptor.enumerable = descriptor.enumerable || false;
                descriptor.configurable = true;
                if ("value" in descriptor) descriptor.writable = true;
                Object.defineProperty(target, descriptor.key, descriptor);
            }
        }

        return function (Constructor, protoProps, staticProps) {
            if (protoProps) defineProperties(Constructor.prototype, protoProps);
            if (staticProps) defineProperties(Constructor, staticProps);
            return Constructor;
        };
    }();

    function _possibleConstructorReturn(self, call) {
        if (!self) {
            throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
        }

        return call && (typeof call === "object" || typeof call === "function") ? call : self;
    }

    function _inherits(subClass, superClass) {
        if (typeof superClass !== "function" && superClass !== null) {
            throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
        }

        subClass.prototype = Object.create(superClass && superClass.prototype, {
            constructor: {
                value: subClass,
                enumerable: false,
                writable: true,
                configurable: true
            }
        });
        if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
    }

    var Clipboard = function (_Emitter) {
        _inherits(Clipboard, _Emitter);

        /**
         * @param {String|HTMLElement|HTMLCollection|NodeList} trigger
         * @param {Object} options
         */
        function Clipboard(trigger, options) {
            _classCallCheck(this, Clipboard);

            var _this = _possibleConstructorReturn(this, (Clipboard.__proto__ || Object.getPrototypeOf(Clipboard)).call(this));

            _this.resolveOptions(options);
            _this.listenClick(trigger);
            return _this;
        }

        /**
         * Defines if attributes would be resolved using internal setter functions
         * or custom functions that were passed in the constructor.
         * @param {Object} options
         */


        _createClass(Clipboard, [{
            key: 'resolveOptions',
            value: function resolveOptions() {
                var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

                this.action = typeof options.action === 'function' ? options.action : this.defaultAction;
                this.target = typeof options.target === 'function' ? options.target : this.defaultTarget;
                this.text = typeof options.text === 'function' ? options.text : this.defaultText;
                this.container = _typeof(options.container) === 'object' ? options.container : document.body;
            }
        }, {
            key: 'listenClick',
            value: function listenClick(trigger) {
                var _this2 = this;

                this.listener = (0, _goodListener2.default)(trigger, 'click', function (e) {
                    return _this2.onClick(e);
                });
            }
        }, {
            key: 'onClick',
            value: function onClick(e) {
                var trigger = e.delegateTarget || e.currentTarget;

                if (this.clipboardAction) {
                    this.clipboardAction = null;
                }

                this.clipboardAction = new _clipboardAction2.default({
                    action: this.action(trigger),
                    target: this.target(trigger),
                    text: this.text(trigger),
                    container: this.container,
                    trigger: trigger,
                    emitter: this
                });
            }
        }, {
            key: 'defaultAction',
            value: function defaultAction(trigger) {
                return getAttributeValue('action', trigger);
            }
        }, {
            key: 'defaultTarget',
            value: function defaultTarget(trigger) {
                var selector = getAttributeValue('target', trigger);

                if (selector) {
                    return document.querySelector(selector);
                }
            }
        }, {
            key: 'defaultText',
            value: function defaultText(trigger) {
                return getAttributeValue('text', trigger);
            }
        }, {
            key: 'destroy',
            value: function destroy() {
                this.listener.destroy();

                if (this.clipboardAction) {
                    this.clipboardAction.destroy();
                    this.clipboardAction = null;
                }
            }
        }], [{
            key: 'isSupported',
            value: function isSupported() {
                var action = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : ['copy', 'cut'];

                var actions = typeof action === 'string' ? [action] : action;
                var support = !!document.queryCommandSupported;

                actions.forEach(function (action) {
                    support = support && !!document.queryCommandSupported(action);
                });

                return support;
            }
        }]);

        return Clipboard;
    }(_tinyEmitter2.default);

    /**
     * Helper function to retrieve attribute value.
     * @param {String} suffix
     * @param {Element} element
     */
    function getAttributeValue(suffix, element) {
        var attribute = 'data-clipboard-' + suffix;

        if (!element.hasAttribute(attribute)) {
            return;
        }

        return element.getAttribute(attribute);
    }

    module.exports = Clipboard;
});

/***/ }),
/* 4 */
/***/ (function(module, exports) {

var DOCUMENT_NODE_TYPE = 9;

/**
 * A polyfill for Element.matches()
 */
if (typeof Element !== 'undefined' && !Element.prototype.matches) {
    var proto = Element.prototype;

    proto.matches = proto.matchesSelector ||
                    proto.mozMatchesSelector ||
                    proto.msMatchesSelector ||
                    proto.oMatchesSelector ||
                    proto.webkitMatchesSelector;
}

/**
 * Finds the closest parent that matches a selector.
 *
 * @param {Element} element
 * @param {String} selector
 * @return {Function}
 */
function closest (element, selector) {
    while (element && element.nodeType !== DOCUMENT_NODE_TYPE) {
        if (typeof element.matches === 'function' &&
            element.matches(selector)) {
          return element;
        }
        element = element.parentNode;
    }
}

module.exports = closest;


/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

var closest = __webpack_require__(4);

/**
 * Delegates event to a selector.
 *
 * @param {Element} element
 * @param {String} selector
 * @param {String} type
 * @param {Function} callback
 * @param {Boolean} useCapture
 * @return {Object}
 */
function _delegate(element, selector, type, callback, useCapture) {
    var listenerFn = listener.apply(this, arguments);

    element.addEventListener(type, listenerFn, useCapture);

    return {
        destroy: function() {
            element.removeEventListener(type, listenerFn, useCapture);
        }
    }
}

/**
 * Delegates event to a selector.
 *
 * @param {Element|String|Array} [elements]
 * @param {String} selector
 * @param {String} type
 * @param {Function} callback
 * @param {Boolean} useCapture
 * @return {Object}
 */
function delegate(elements, selector, type, callback, useCapture) {
    // Handle the regular Element usage
    if (typeof elements.addEventListener === 'function') {
        return _delegate.apply(null, arguments);
    }

    // Handle Element-less usage, it defaults to global delegation
    if (typeof type === 'function') {
        // Use `document` as the first parameter, then apply arguments
        // This is a short way to .unshift `arguments` without running into deoptimizations
        return _delegate.bind(null, document).apply(null, arguments);
    }

    // Handle Selector-based usage
    if (typeof elements === 'string') {
        elements = document.querySelectorAll(elements);
    }

    // Handle Array-like based usage
    return Array.prototype.map.call(elements, function (element) {
        return _delegate(element, selector, type, callback, useCapture);
    });
}

/**
 * Finds closest match and invokes callback.
 *
 * @param {Element} element
 * @param {String} selector
 * @param {String} type
 * @param {Function} callback
 * @return {Function}
 */
function listener(element, selector, type, callback) {
    return function(e) {
        e.delegateTarget = closest(e.target, selector);

        if (e.delegateTarget) {
            callback.call(element, e);
        }
    }
}

module.exports = delegate;


/***/ }),
/* 6 */
/***/ (function(module, exports) {

/**
 * Check if argument is a HTML element.
 *
 * @param {Object} value
 * @return {Boolean}
 */
exports.node = function(value) {
    return value !== undefined
        && value instanceof HTMLElement
        && value.nodeType === 1;
};

/**
 * Check if argument is a list of HTML elements.
 *
 * @param {Object} value
 * @return {Boolean}
 */
exports.nodeList = function(value) {
    var type = Object.prototype.toString.call(value);

    return value !== undefined
        && (type === '[object NodeList]' || type === '[object HTMLCollection]')
        && ('length' in value)
        && (value.length === 0 || exports.node(value[0]));
};

/**
 * Check if argument is a string.
 *
 * @param {Object} value
 * @return {Boolean}
 */
exports.string = function(value) {
    return typeof value === 'string'
        || value instanceof String;
};

/**
 * Check if argument is a function.
 *
 * @param {Object} value
 * @return {Boolean}
 */
exports.fn = function(value) {
    var type = Object.prototype.toString.call(value);

    return type === '[object Function]';
};


/***/ }),
/* 7 */
/***/ (function(module, exports) {

function select(element) {
    var selectedText;

    if (element.nodeName === 'SELECT') {
        element.focus();

        selectedText = element.value;
    }
    else if (element.nodeName === 'INPUT' || element.nodeName === 'TEXTAREA') {
        var isReadOnly = element.hasAttribute('readonly');

        if (!isReadOnly) {
            element.setAttribute('readonly', '');
        }

        element.select();
        element.setSelectionRange(0, element.value.length);

        if (!isReadOnly) {
            element.removeAttribute('readonly');
        }

        selectedText = element.value;
    }
    else {
        if (element.hasAttribute('contenteditable')) {
            element.focus();
        }

        var selection = window.getSelection();
        var range = document.createRange();

        range.selectNodeContents(element);
        selection.removeAllRanges();
        selection.addRange(range);

        selectedText = selection.toString();
    }

    return selectedText;
}

module.exports = select;


/***/ })
/******/ ]);
});

/***/ }),
/* 128 */
/***/ (function(module, exports) {

(function() {

	if (
		typeof self !== 'undefined' && !self.Prism ||
		!self.document || !Function.prototype.bind
	) {
		return;
	}

	var previewers = {
		// gradient must be defined before color and angle
		'gradient': {
			create: (function () {

				// Stores already processed gradients so that we don't
				// make the conversion every time the previewer is shown
				var cache = {};

				/**
				 * Returns a W3C-valid linear gradient
				 * @param {string} prefix Vendor prefix if any ("-moz-", "-webkit-", etc.)
				 * @param {string} func Gradient function name ("linear-gradient")
				 * @param {string[]} values Array of the gradient function parameters (["0deg", "red 0%", "blue 100%"])
				 */
				var convertToW3CLinearGradient = function(prefix, func, values) {
					// Default value for angle
					var angle = '180deg';

					if (/^(?:-?\d*\.?\d+(?:deg|rad)|to\b|top|right|bottom|left)/.test(values[0])) {
						angle = values.shift();
						if (angle.indexOf('to ') < 0) {
							// Angle uses old keywords
							// W3C syntax uses "to" + opposite keywords
							if (angle.indexOf('top') >= 0) {
								if (angle.indexOf('left') >= 0) {
									angle = 'to bottom right';
								} else if (angle.indexOf('right') >= 0) {
									angle = 'to bottom left';
								} else {
									angle = 'to bottom';
								}
							} else if (angle.indexOf('bottom') >= 0) {
								if (angle.indexOf('left') >= 0) {
									angle = 'to top right';
								} else if (angle.indexOf('right') >= 0) {
									angle = 'to top left';
								} else {
									angle = 'to top';
								}
							} else if (angle.indexOf('left') >= 0) {
								angle = 'to right';
							} else if (angle.indexOf('right') >= 0) {
								angle = 'to left';
							} else if (prefix) {
								// Angle is shifted by 90deg in prefixed gradients
								if (angle.indexOf('deg') >= 0) {
									angle = (90 - parseFloat(angle)) + 'deg';
								} else if (angle.indexOf('rad') >= 0) {
									angle = (Math.PI / 2 - parseFloat(angle)) + 'rad';
								}
							}
						}
					}

					return func + '(' + angle + ',' + values.join(',') + ')';
				};

				/**
				 * Returns a W3C-valid radial gradient
				 * @param {string} prefix Vendor prefix if any ("-moz-", "-webkit-", etc.)
				 * @param {string} func Gradient function name ("linear-gradient")
				 * @param {string[]} values Array of the gradient function parameters (["0deg", "red 0%", "blue 100%"])
				 */
				var convertToW3CRadialGradient = function(prefix, func, values) {
					if (values[0].indexOf('at') < 0) {
						// Looks like old syntax

						// Default values
						var position = 'center';
						var shape = 'ellipse';
						var size = 'farthest-corner';

						if (/\bcenter|top|right|bottom|left\b|^\d+/.test(values[0])) {
							// Found a position
							// Remove angle value, if any
							position = values.shift().replace(/\s*-?\d+(?:rad|deg)\s*/, '');
						}
						if (/\bcircle|ellipse|closest|farthest|contain|cover\b/.test(values[0])) {
							// Found a shape and/or size
							var shapeSizeParts = values.shift().split(/\s+/);
							if (shapeSizeParts[0] && (shapeSizeParts[0] === 'circle' || shapeSizeParts[0] === 'ellipse')) {
								shape = shapeSizeParts.shift();
							}
							if (shapeSizeParts[0]) {
								size = shapeSizeParts.shift();
							}

							// Old keywords are converted to their synonyms
							if (size === 'cover') {
								size = 'farthest-corner';
							} else if (size === 'contain') {
								size = 'clothest-side';
							}
						}

						return func + '(' + shape + ' ' + size + ' at ' + position + ',' + values.join(',') + ')';
					}
					return func + '(' + values.join(',') + ')';
				};

				/**
				 * Converts a gradient to a W3C-valid one
				 * Does not support old webkit syntax (-webkit-gradient(linear...) and -webkit-gradient(radial...))
				 * @param {string} gradient The CSS gradient
				 */
				var convertToW3CGradient = function(gradient) {
					if (cache[gradient]) {
						return cache[gradient];
					}
					var parts = gradient.match(/^(\b|\B-[a-z]{1,10}-)((?:repeating-)?(?:linear|radial)-gradient)/);
					// "", "-moz-", etc.
					var prefix = parts && parts[1];
					// "linear-gradient", "radial-gradient", etc.
					var func = parts && parts[2];

					var values = gradient.replace(/^(?:\b|\B-[a-z]{1,10}-)(?:repeating-)?(?:linear|radial)-gradient\(|\)$/g, '').split(/\s*,\s*/);

					if (func.indexOf('linear') >= 0) {
						return cache[gradient] = convertToW3CLinearGradient(prefix, func, values);
					} else if (func.indexOf('radial') >= 0) {
						return cache[gradient] = convertToW3CRadialGradient(prefix, func, values);
					}
					return cache[gradient] = func + '(' + values.join(',') + ')';
				};

				return function () {
					new Prism.plugins.Previewer('gradient', function(value) {
						this.firstChild.style.backgroundImage = '';
						this.firstChild.style.backgroundImage = convertToW3CGradient(value);
						return !!this.firstChild.style.backgroundImage;
					}, '*', function () {
						this._elt.innerHTML = '<div></div>';
					});
				};
			}()),
			tokens: {
				'gradient': {
					pattern: /(?:\b|\B-[a-z]{1,10}-)(?:repeating-)?(?:linear|radial)-gradient\((?:(?:rgb|hsl)a?\(.+?\)|[^\)])+\)/gi,
					inside: {
						'function': /[\w-]+(?=\()/,
						'punctuation': /[(),]/
					}
				}
			},
			languages: {
				'css': true,
				'less': true,
				'sass': [
					{
						lang: 'sass',
						before: 'punctuation',
						inside: 'inside',
						root: Prism.languages.sass && Prism.languages.sass['variable-line']
					},
					{
						lang: 'sass',
						before: 'punctuation',
						inside: 'inside',
						root: Prism.languages.sass && Prism.languages.sass['property-line']
					}
				],
				'scss': true,
				'stylus': [
					{
						lang: 'stylus',
						before: 'func',
						inside: 'rest',
						root: Prism.languages.stylus && Prism.languages.stylus['property-declaration'].inside
					},
					{
						lang: 'stylus',
						before: 'func',
						inside: 'rest',
						root: Prism.languages.stylus && Prism.languages.stylus['variable-declaration'].inside
					}
				]
			}
		},
		'angle': {
			create: function () {
				new Prism.plugins.Previewer('angle', function(value) {
					var num = parseFloat(value);
					var unit = value.match(/[a-z]+$/i);
					var max, percentage;
					if (!num || !unit) {
						return false;
					}
					unit = unit[0];

					switch(unit) {
						case 'deg':
							max = 360;
							break;
						case 'grad':
							max = 400;
							break;
						case 'rad':
							max = 2 * Math.PI;
							break;
						case 'turn':
							max = 1;
					}

					percentage = 100 * num/max;
					percentage %= 100;

					this[(num < 0? 'set' : 'remove') + 'Attribute']('data-negative', '');
					this.querySelector('circle').style.strokeDasharray = Math.abs(percentage) + ',500';
					return true;
				}, '*', function () {
					this._elt.innerHTML = '<svg viewBox="0 0 64 64">' +
						'<circle r="16" cy="32" cx="32"></circle>' +
						'</svg>';
				});
			},
			tokens: {
				'angle': /(?:\b|\B-|(?=\B\.))\d*\.?\d+(?:deg|g?rad|turn)\b/i
			},
			languages: {
				'css': true,
				'less': true,
				'markup': {
					lang: 'markup',
					before: 'punctuation',
					inside: 'inside',
					root: Prism.languages.markup && Prism.languages.markup['tag'].inside['attr-value']
				},
				'sass': [
					{
						lang: 'sass',
						inside: 'inside',
						root: Prism.languages.sass && Prism.languages.sass['property-line']
					},
					{
						lang: 'sass',
						before: 'operator',
						inside: 'inside',
						root: Prism.languages.sass && Prism.languages.sass['variable-line']
					}
				],
				'scss': true,
				'stylus': [
					{
						lang: 'stylus',
						before: 'func',
						inside: 'rest',
						root: Prism.languages.stylus && Prism.languages.stylus['property-declaration'].inside
					},
					{
						lang: 'stylus',
						before: 'func',
						inside: 'rest',
						root: Prism.languages.stylus && Prism.languages.stylus['variable-declaration'].inside
					}
				]
			}
		},
		'color': {
			create: function () {
				new Prism.plugins.Previewer('color', function(value) {
					this.style.backgroundColor = '';
					this.style.backgroundColor = value;
					return !!this.style.backgroundColor;
				});
			},
			tokens: {
				'color': {
					pattern: /\B#(?:[0-9a-f]{3}){1,2}\b|\b(?:rgb|hsl)\(\s*\d{1,3}\s*,\s*\d{1,3}%?\s*,\s*\d{1,3}%?\s*\)\B|\b(?:rgb|hsl)a\(\s*\d{1,3}\s*,\s*\d{1,3}%?\s*,\s*\d{1,3}%?\s*,\s*(?:0|0?\.\d+|1)\s*\)\B|\b(?:AliceBlue|AntiqueWhite|Aqua|Aquamarine|Azure|Beige|Bisque|Black|BlanchedAlmond|Blue|BlueViolet|Brown|BurlyWood|CadetBlue|Chartreuse|Chocolate|Coral|CornflowerBlue|Cornsilk|Crimson|Cyan|DarkBlue|DarkCyan|DarkGoldenRod|DarkGray|DarkGreen|DarkKhaki|DarkMagenta|DarkOliveGreen|DarkOrange|DarkOrchid|DarkRed|DarkSalmon|DarkSeaGreen|DarkSlateBlue|DarkSlateGray|DarkTurquoise|DarkViolet|DeepPink|DeepSkyBlue|DimGray|DodgerBlue|FireBrick|FloralWhite|ForestGreen|Fuchsia|Gainsboro|GhostWhite|Gold|GoldenRod|Gray|Green|GreenYellow|HoneyDew|HotPink|IndianRed|Indigo|Ivory|Khaki|Lavender|LavenderBlush|LawnGreen|LemonChiffon|LightBlue|LightCoral|LightCyan|LightGoldenRodYellow|LightGray|LightGreen|LightPink|LightSalmon|LightSeaGreen|LightSkyBlue|LightSlateGray|LightSteelBlue|LightYellow|Lime|LimeGreen|Linen|Magenta|Maroon|MediumAquaMarine|MediumBlue|MediumOrchid|MediumPurple|MediumSeaGreen|MediumSlateBlue|MediumSpringGreen|MediumTurquoise|MediumVioletRed|MidnightBlue|MintCream|MistyRose|Moccasin|NavajoWhite|Navy|OldLace|Olive|OliveDrab|Orange|OrangeRed|Orchid|PaleGoldenRod|PaleGreen|PaleTurquoise|PaleVioletRed|PapayaWhip|PeachPuff|Peru|Pink|Plum|PowderBlue|Purple|Red|RosyBrown|RoyalBlue|SaddleBrown|Salmon|SandyBrown|SeaGreen|SeaShell|Sienna|Silver|SkyBlue|SlateBlue|SlateGray|Snow|SpringGreen|SteelBlue|Tan|Teal|Thistle|Tomato|Turquoise|Violet|Wheat|White|WhiteSmoke|Yellow|YellowGreen)\b/i,
					inside: {
						'function': /[\w-]+(?=\()/,
						'punctuation': /[(),]/
					}
				}
			},
			languages: {
				'css': true,
				'less': true,
				'markup': {
					lang: 'markup',
					before: 'punctuation',
					inside: 'inside',
					root: Prism.languages.markup && Prism.languages.markup['tag'].inside['attr-value']
				},
				'sass': [
					{
						lang: 'sass',
						before: 'punctuation',
						inside: 'inside',
						root: Prism.languages.sass && Prism.languages.sass['variable-line']
					},
					{
						lang: 'sass',
						inside: 'inside',
						root: Prism.languages.sass && Prism.languages.sass['property-line']
					}
				],
				'scss': true,
				'stylus': [
					{
						lang: 'stylus',
						before: 'hexcode',
						inside: 'rest',
						root: Prism.languages.stylus && Prism.languages.stylus['property-declaration'].inside
					},
					{
						lang: 'stylus',
						before: 'hexcode',
						inside: 'rest',
						root: Prism.languages.stylus && Prism.languages.stylus['variable-declaration'].inside
					}
				]
			}
		},
		'easing': {
			create: function () {
				new Prism.plugins.Previewer('easing', function (value) {

					value = {
						'linear': '0,0,1,1',
						'ease': '.25,.1,.25,1',
						'ease-in': '.42,0,1,1',
						'ease-out': '0,0,.58,1',
						'ease-in-out':'.42,0,.58,1'
					}[value] || value;

					var p = value.match(/-?\d*\.?\d+/g);

					if(p.length === 4) {
						p = p.map(function(p, i) { return (i % 2? 1 - p : p) * 100; });

						this.querySelector('path').setAttribute('d', 'M0,100 C' + p[0] + ',' + p[1] + ', ' + p[2] + ',' + p[3] + ', 100,0');

						var lines = this.querySelectorAll('line');
						lines[0].setAttribute('x2', p[0]);
						lines[0].setAttribute('y2', p[1]);
						lines[1].setAttribute('x2', p[2]);
						lines[1].setAttribute('y2', p[3]);

						return true;
					}

					return false;
				}, '*', function () {
					this._elt.innerHTML = '<svg viewBox="-20 -20 140 140" width="100" height="100">' +
						'<defs>' +
						'<marker id="prism-previewer-easing-marker" viewBox="0 0 4 4" refX="2" refY="2" markerUnits="strokeWidth">' +
						'<circle cx="2" cy="2" r="1.5" />' +
						'</marker>' +
						'</defs>' +
						'<path d="M0,100 C20,50, 40,30, 100,0" />' +
						'<line x1="0" y1="100" x2="20" y2="50" marker-start="url(' + location.href + '#prism-previewer-easing-marker)" marker-end="url(' + location.href + '#prism-previewer-easing-marker)" />' +
						'<line x1="100" y1="0" x2="40" y2="30" marker-start="url(' + location.href + '#prism-previewer-easing-marker)" marker-end="url(' + location.href + '#prism-previewer-easing-marker)" />' +
						'</svg>';
				});
			},
			tokens: {
				'easing': {
					pattern: /\bcubic-bezier\((?:-?\d*\.?\d+,\s*){3}-?\d*\.?\d+\)\B|\b(?:linear|ease(?:-in)?(?:-out)?)(?=\s|[;}]|$)/i,
					inside: {
						'function': /[\w-]+(?=\()/,
						'punctuation': /[(),]/
					}
				}
			},
			languages: {
				'css': true,
				'less': true,
				'sass': [
					{
						lang: 'sass',
						inside: 'inside',
						before: 'punctuation',
						root: Prism.languages.sass && Prism.languages.sass['variable-line']
					},
					{
						lang: 'sass',
						inside: 'inside',
						root: Prism.languages.sass && Prism.languages.sass['property-line']
					}
				],
				'scss': true,
				'stylus': [
					{
						lang: 'stylus',
						before: 'hexcode',
						inside: 'rest',
						root: Prism.languages.stylus && Prism.languages.stylus['property-declaration'].inside
					},
					{
						lang: 'stylus',
						before: 'hexcode',
						inside: 'rest',
						root: Prism.languages.stylus && Prism.languages.stylus['variable-declaration'].inside
					}
				]
			}
		},

		'time': {
			create: function () {
				new Prism.plugins.Previewer('time', function(value) {
					var num = parseFloat(value);
					var unit = value.match(/[a-z]+$/i);
					if (!num || !unit) {
						return false;
					}
					unit = unit[0];
					this.querySelector('circle').style.animationDuration = 2 * num + unit;
					return true;
				}, '*', function () {
					this._elt.innerHTML = '<svg viewBox="0 0 64 64">' +
						'<circle r="16" cy="32" cx="32"></circle>' +
						'</svg>';
				});
			},
			tokens: {
				'time': /(?:\b|\B-|(?=\B\.))\d*\.?\d+m?s\b/i
			},
			languages: {
				'css': true,
				'less': true,
				'markup': {
					lang: 'markup',
					before: 'punctuation',
					inside: 'inside',
					root: Prism.languages.markup && Prism.languages.markup['tag'].inside['attr-value']
				},
				'sass': [
					{
						lang: 'sass',
						inside: 'inside',
						root: Prism.languages.sass && Prism.languages.sass['property-line']
					},
					{
						lang: 'sass',
						before: 'operator',
						inside: 'inside',
						root: Prism.languages.sass && Prism.languages.sass['variable-line']
					}
				],
				'scss': true,
				'stylus': [
					{
						lang: 'stylus',
						before: 'hexcode',
						inside: 'rest',
						root: Prism.languages.stylus && Prism.languages.stylus['property-declaration'].inside
					},
					{
						lang: 'stylus',
						before: 'hexcode',
						inside: 'rest',
						root: Prism.languages.stylus && Prism.languages.stylus['variable-declaration'].inside
					}
				]
			}
		}
	};

	/**
	 * Returns the absolute X, Y offsets for an element
	 * @param {HTMLElement} element
	 * @returns {{top: number, right: number, bottom: number, left: number}}
	 */
	var getOffset = function (element) {
		var left = 0, top = 0, el = element;

		if (el.parentNode) {
			do {
				left += el.offsetLeft;
				top += el.offsetTop;
			} while ((el = el.offsetParent) && el.nodeType < 9);

			el = element;

			do {
				left -= el.scrollLeft;
				top -= el.scrollTop;
			} while ((el = el.parentNode) && !/body/i.test(el.nodeName));
		}

		return {
			top: top,
			right: innerWidth - left - element.offsetWidth,
			bottom: innerHeight - top - element.offsetHeight,
			left: left
		};
	};

	var tokenRegexp = /(?:^|\s)token(?=$|\s)/;
	var activeRegexp = /(?:^|\s)active(?=$|\s)/g;
	var flippedRegexp = /(?:^|\s)flipped(?=$|\s)/g;

	/**
	 * Previewer constructor
	 * @param {string} type Unique previewer type
	 * @param {function} updater Function that will be called on mouseover.
	 * @param {string[]|string=} supportedLanguages Aliases of the languages this previewer must be enabled for. Defaults to "*", all languages.
	 * @param {function=} initializer Function that will be called on initialization.
	 * @constructor
	 */
	var Previewer = function (type, updater, supportedLanguages, initializer) {
		this._elt = null;
		this._type = type;
		this._clsRegexp = RegExp('(?:^|\\s)' + type + '(?=$|\\s)');
		this._token = null;
		this.updater = updater;
		this._mouseout = this.mouseout.bind(this);
		this.initializer = initializer;

		var self = this;

		if (!supportedLanguages) {
			supportedLanguages = ['*'];
		}
		if (Prism.util.type(supportedLanguages) !== 'Array') {
			supportedLanguages = [supportedLanguages];
		}
		supportedLanguages.forEach(function (lang) {
			if (typeof lang !== 'string') {
				lang = lang.lang;
			}
			if (!Previewer.byLanguages[lang]) {
				Previewer.byLanguages[lang] = [];
			}
			if (Previewer.byLanguages[lang].indexOf(self) < 0) {
				Previewer.byLanguages[lang].push(self);
			}
		});
		Previewer.byType[type] = this;
	};

	/**
	 * Creates the HTML element for the previewer.
	 */
	Previewer.prototype.init = function () {
		if (this._elt) {
			return;
		}
		this._elt = document.createElement('div');
		this._elt.className = 'prism-previewer prism-previewer-' + this._type;
		document.body.appendChild(this._elt);
		if(this.initializer) {
			this.initializer();
		}
	};

	Previewer.prototype.isDisabled = function (token) {
		do {
			if (token.hasAttribute && token.hasAttribute('data-previewers')) {
				var previewers = token.getAttribute('data-previewers');
				return (previewers || '').split(/\s+/).indexOf(this._type) === -1;
			}
		} while(token = token.parentNode);
		return false;
	};

	/**
	 * Checks the class name of each hovered element
	 * @param token
	 */
	Previewer.prototype.check = function (token) {
		if (tokenRegexp.test(token.className) && this.isDisabled(token)) {
			return;
		}
		do {
			if (tokenRegexp.test(token.className) && this._clsRegexp.test(token.className)) {
				break;
			}
		} while(token = token.parentNode);

		if (token && token !== this._token) {
			this._token = token;
			this.show();
		}
	};

	/**
	 * Called on mouseout
	 */
	Previewer.prototype.mouseout = function() {
		this._token.removeEventListener('mouseout', this._mouseout, false);
		this._token = null;
		this.hide();
	};

	/**
	 * Shows the previewer positioned properly for the current token.
	 */
	Previewer.prototype.show = function () {
		if (!this._elt) {
			this.init();
		}
		if (!this._token) {
			return;
		}

		if (this.updater.call(this._elt, this._token.textContent)) {
			this._token.addEventListener('mouseout', this._mouseout, false);

			var offset = getOffset(this._token);
			this._elt.className += ' active';

			if (offset.top - this._elt.offsetHeight > 0) {
				this._elt.className = this._elt.className.replace(flippedRegexp, '');
				this._elt.style.top = offset.top + 'px';
				this._elt.style.bottom = '';
			} else {
				this._elt.className +=  ' flipped';
				this._elt.style.bottom = offset.bottom + 'px';
				this._elt.style.top = '';
			}

			this._elt.style.left = offset.left + Math.min(200, this._token.offsetWidth / 2) + 'px';
		} else {
			this.hide();
		}
	};

	/**
	 * Hides the previewer.
	 */
	Previewer.prototype.hide = function () {
		this._elt.className = this._elt.className.replace(activeRegexp, '');
	};

	/**
	 * Map of all registered previewers by language
	 * @type {{}}
	 */
	Previewer.byLanguages = {};

	/**
	 * Map of all registered previewers by type
	 * @type {{}}
	 */
	Previewer.byType = {};

	/**
	 * Initializes the mouseover event on the code block.
	 * @param {HTMLElement} elt The code block (env.element)
	 * @param {string} lang The language (env.language)
	 */
	Previewer.initEvents = function (elt, lang) {
		var previewers = [];
		if (Previewer.byLanguages[lang]) {
			previewers = previewers.concat(Previewer.byLanguages[lang]);
		}
		if (Previewer.byLanguages['*']) {
			previewers = previewers.concat(Previewer.byLanguages['*']);
		}
		elt.addEventListener('mouseover', function (e) {
			var target = e.target;
			previewers.forEach(function (previewer) {
				previewer.check(target);
			});
		}, false);
	};
	Prism.plugins.Previewer = Previewer;

	Prism.hooks.add('before-highlight', function (env) {
		for (var previewer in previewers) {
			var languages = previewers[previewer].languages;
			if (env.language && languages[env.language] && !languages[env.language].initialized) {
				var lang = languages[env.language];
				if (Prism.util.type(lang) !== 'Array') {
					lang = [lang];
				}
				lang.forEach(function (lang) {
					var before, inside, root, skip;
					if (lang === true) {
						before = 'important';
						inside = env.language;
						lang = env.language;
					} else {
						before = lang.before || 'important';
						inside = lang.inside || lang.lang;
						root = lang.root || Prism.languages;
						skip = lang.skip;
						lang = env.language;
					}

					if (!skip && Prism.languages[lang]) {
						Prism.languages.insertBefore(inside, before, previewers[previewer].tokens, root);
						env.grammar = Prism.languages[lang];

						languages[env.language] = {initialized: true};
					}
				});
			}
		}
	});

	// Initialize the previewers only when needed
	Prism.hooks.add('after-highlight', function (env) {
		if(Previewer.byLanguages['*'] || Previewer.byLanguages[env.language]) {
			Previewer.initEvents(env.element, env.language);
		}
	});

	for (var previewer in previewers) {
		previewers[previewer].create();
	}

}());

/***/ }),
/* 129 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(130);
if(typeof content === 'string') content = [[module.i, content, '']];
// Prepare cssTransformation
var transform;

var options = {"insertAt":{"before":"#webpack-style-loader-insert-before-this"},"hmr":true}
options.transform = transform
// add the styles to the DOM
var update = __webpack_require__(10)(content, options);
if(content.locals) module.exports = content.locals;
// Hot Module Replacement
if(false) {
	// When the styles change, update the <style> tags
	if(!content.locals) {
		module.hot.accept("!!../../../css-loader/index.js??ref--1-1!../../../postcss-loader/lib/index.js??ref--1-2!./prism-previewers.css", function() {
			var newContent = require("!!../../../css-loader/index.js??ref--1-1!../../../postcss-loader/lib/index.js??ref--1-2!./prism-previewers.css");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
	}
	// When the module is disposed, remove the <style> tags
	module.hot.dispose(function() { update(); });
}

/***/ }),
/* 130 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(9)(false);
// imports


// module
exports.push([module.i, ".prism-previewer,\n.prism-previewer:before,\n.prism-previewer:after {\n\tposition: absolute;\n\tpointer-events: none;\n}\n.prism-previewer,\n.prism-previewer:after {\n\tleft: 50%;\n}\n.prism-previewer {\n\tmargin-top: -48px;\n\twidth: 32px;\n\theight: 32px;\n\tmargin-left: -16px;\n\n\topacity: 0;\n\ttransition: opacity .25s;\n}\n.prism-previewer.flipped {\n\tmargin-top: 0;\n\tmargin-bottom: -48px;\n}\n.prism-previewer:before,\n.prism-previewer:after {\n\tcontent: '';\n\tposition: absolute;\n\tpointer-events: none;\n}\n.prism-previewer:before {\n\ttop: -5px;\n\tright: -5px;\n\tleft: -5px;\n\tbottom: -5px;\n\tborder-radius: 10px;\n\tborder: 5px solid #fff;\n\tbox-shadow: 0 0 3px rgba(0, 0, 0, 0.5) inset, 0 0 10px rgba(0, 0, 0, 0.75);\n}\n.prism-previewer:after {\n\ttop: 100%;\n\twidth: 0;\n\theight: 0;\n\tmargin: 5px 0 0 -7px;\n\tborder: 7px solid transparent;\n\tborder-color: rgba(255, 0, 0, 0);\n\tborder-top-color: #fff;\n}\n.prism-previewer.flipped:after {\n\ttop: auto;\n\tbottom: 100%;\n\tmargin-top: 0;\n\tmargin-bottom: 5px;\n\tborder-top-color: rgba(255, 0, 0, 0);\n\tborder-bottom-color: #fff;\n}\n.prism-previewer.active {\n\topacity: 1;\n}\n\n.prism-previewer-angle:before {\n\tborder-radius: 50%;\n\tbackground: #fff;\n}\n.prism-previewer-angle:after {\n\tmargin-top: 4px;\n}\n.prism-previewer-angle svg {\n\twidth: 32px;\n\theight: 32px;\n\ttransform: rotate(-90deg);\n}\n.prism-previewer-angle[data-negative] svg {\n\ttransform: scaleX(-1) rotate(-90deg);\n}\n.prism-previewer-angle circle {\n\tfill: transparent;\n\tstroke: hsl(200, 10%, 20%);\n\tstroke-opacity: 0.9;\n\tstroke-width: 32;\n\tstroke-dasharray: 0, 500;\n}\n\n.prism-previewer-gradient {\n\tbackground-image: linear-gradient(45deg, #bbb 25%, transparent 25%, transparent 75%, #bbb 75%, #bbb), linear-gradient(45deg, #bbb 25%, #eee 25%, #eee 75%, #bbb 75%, #bbb);\n\tbackground-size: 10px 10px;\n\tbackground-position: 0 0, 5px 5px;\n\n\twidth: 64px;\n\tmargin-left: -32px;\n}\n.prism-previewer-gradient:before {\n\tcontent: none;\n}\n.prism-previewer-gradient div {\n\tposition: absolute;\n\ttop: -5px;\n\tleft: -5px;\n\tright: -5px;\n\tbottom: -5px;\n\tborder-radius: 10px;\n\tborder: 5px solid #fff;\n\tbox-shadow: 0 0 3px rgba(0, 0, 0, 0.5) inset, 0 0 10px rgba(0, 0, 0, 0.75);\n}\n\n.prism-previewer-color {\n\tbackground-image: linear-gradient(45deg, #bbb 25%, transparent 25%, transparent 75%, #bbb 75%, #bbb), linear-gradient(45deg, #bbb 25%, #eee 25%, #eee 75%, #bbb 75%, #bbb);\n\tbackground-size: 10px 10px;\n\tbackground-position: 0 0, 5px 5px;\n}\n.prism-previewer-color:before {\n\tbackground-color: inherit;\n\tbackground-clip: padding-box;\n}\n\n.prism-previewer-easing {\n\tmargin-top: -76px;\n\tmargin-left: -30px;\n\twidth: 60px;\n\theight: 60px;\n\tbackground: #333;\n}\n.prism-previewer-easing.flipped {\n\tmargin-bottom: -116px;\n}\n.prism-previewer-easing svg {\n\twidth: 60px;\n\theight: 60px;\n}\n.prism-previewer-easing circle {\n\tfill: hsl(200, 10%, 20%);\n\tstroke: white;\n}\n.prism-previewer-easing path {\n\tfill: none;\n\tstroke: white;\n\tstroke-linecap: round;\n\tstroke-width: 4;\n}\n.prism-previewer-easing line {\n\tstroke: white;\n\tstroke-opacity: 0.5;\n\tstroke-width: 2;\n}\n\n@keyframes prism-previewer-time {\n\t0% {\n\t\tstroke-dasharray: 0, 500;\n\t\tstroke-dashoffset: 0;\n\t}\n\t50% {\n\t\tstroke-dasharray: 100, 500;\n\t\tstroke-dashoffset: 0;\n\t}\n\t100% {\n\t\tstroke-dasharray: 0, 500;\n\t\tstroke-dashoffset: -100;\n\t}\n}\n\n.prism-previewer-time:before {\n\tborder-radius: 50%;\n\tbackground: #fff;\n}\n.prism-previewer-time:after {\n\tmargin-top: 4px;\n}\n.prism-previewer-time svg {\n\twidth: 32px;\n\theight: 32px;\n\ttransform: rotate(-90deg);\n}\n.prism-previewer-time circle {\n\tfill: transparent;\n\tstroke: hsl(200, 10%, 20%);\n\tstroke-opacity: 0.9;\n\tstroke-width: 32;\n\tstroke-dasharray: 0, 500;\n\tstroke-dashoffset: 0;\n\tanimation: prism-previewer-time linear infinite 3s;\n}", ""]);

// exports


/***/ }),
/* 131 */
/***/ (function(module, exports) {

Prism.languages.css = {
	'comment': /\/\*[\s\S]*?\*\//,
	'atrule': {
		pattern: /@[\w-]+?.*?(?:;|(?=\s*\{))/i,
		inside: {
			'rule': /@[\w-]+/
			// See rest below
		}
	},
	'url': /url\((?:(["'])(?:\\(?:\r\n|[\s\S])|(?!\1)[^\\\r\n])*\1|.*?)\)/i,
	'selector': /[^{}\s][^{};]*?(?=\s*\{)/,
	'string': {
		pattern: /("|')(?:\\(?:\r\n|[\s\S])|(?!\1)[^\\\r\n])*\1/,
		greedy: true
	},
	'property': /[-_a-z\xA0-\uFFFF][-\w\xA0-\uFFFF]*(?=\s*:)/i,
	'important': /\B!important\b/i,
	'function': /[-a-z0-9]+(?=\()/i,
	'punctuation': /[(){};:]/
};

Prism.languages.css['atrule'].inside.rest = Prism.languages.css;

if (Prism.languages.markup) {
	Prism.languages.insertBefore('markup', 'tag', {
		'style': {
			pattern: /(<style[\s\S]*?>)[\s\S]*?(?=<\/style>)/i,
			lookbehind: true,
			inside: Prism.languages.css,
			alias: 'language-css',
			greedy: true
		}
	});

	Prism.languages.insertBefore('inside', 'attr-value', {
		'style-attr': {
			pattern: /\s*style=("|')(?:\\[\s\S]|(?!\1)[^\\])*\1/i,
			inside: {
				'attr-name': {
					pattern: /^\s*style/i,
					inside: Prism.languages.markup.tag.inside
				},
				'punctuation': /^\s*=\s*['"]|['"]\s*$/,
				'attr-value': {
					pattern: /.+/i,
					inside: Prism.languages.css
				}
			},
			alias: 'language-css'
		}
	}, Prism.languages.markup.tag);
}

/***/ }),
/* 132 */
/***/ (function(module, exports) {

Prism.languages.css.selector = {
	pattern: /[^{}\s][^{}]*(?=\s*\{)/,
	inside: {
		'pseudo-element': /:(?:after|before|first-letter|first-line|selection)|::[-\w]+/,
		'pseudo-class': /:[-\w]+(?:\(.*\))?/,
		'class': /\.[-:.\w]+/,
		'id': /#[-:.\w]+/,
		'attribute': /\[[^\]]+\]/
	}
};

Prism.languages.insertBefore('css', 'function', {
	'hexcode': /#[\da-f]{3,8}/i,
	'entity': /\\[\da-f]{1,8}/i,
	'number': /[\d%.]+/
});

/***/ }),
/* 133 */
/***/ (function(module, exports) {

(function(Prism) {
	Prism.languages.sass = Prism.languages.extend('css', {
		// Sass comments don't need to be closed, only indented
		'comment': {
			pattern: /^([ \t]*)\/[\/*].*(?:(?:\r?\n|\r)\1[ \t]+.+)*/m,
			lookbehind: true
		}
	});

	Prism.languages.insertBefore('sass', 'atrule', {
		// We want to consume the whole line
		'atrule-line': {
			// Includes support for = and + shortcuts
			pattern: /^(?:[ \t]*)[@+=].+/m,
			inside: {
				'atrule': /(?:@[\w-]+|[+=])/m
			}
		}
	});
	delete Prism.languages.sass.atrule;


	var variable = /\$[-\w]+|#\{\$[-\w]+\}/;
	var operator = [
		/[+*\/%]|[=!]=|<=?|>=?|\b(?:and|or|not)\b/,
		{
			pattern: /(\s+)-(?=\s)/,
			lookbehind: true
		}
	];

	Prism.languages.insertBefore('sass', 'property', {
		// We want to consume the whole line
		'variable-line': {
			pattern: /^[ \t]*\$.+/m,
			inside: {
				'punctuation': /:/,
				'variable': variable,
				'operator': operator
			}
		},
		// We want to consume the whole line
		'property-line': {
			pattern: /^[ \t]*(?:[^:\s]+ *:.*|:[^:\s]+.*)/m,
			inside: {
				'property': [
					/[^:\s]+(?=\s*:)/,
					{
						pattern: /(:)[^:\s]+/,
						lookbehind: true
					}
				],
				'punctuation': /:/,
				'variable': variable,
				'operator': operator,
				'important': Prism.languages.sass.important
			}
		}
	});
	delete Prism.languages.sass.property;
	delete Prism.languages.sass.important;

	// Now that whole lines for other patterns are consumed,
	// what's left should be selectors
	delete Prism.languages.sass.selector;
	Prism.languages.insertBefore('sass', 'punctuation', {
		'selector': {
			pattern: /([ \t]*)\S(?:,?[^,\r\n]+)*(?:,(?:\r?\n|\r)\1[ \t]+\S(?:,?[^,\r\n]+)*)*/,
			lookbehind: true
		}
	});

}(Prism));

/***/ }),
/* 134 */
/***/ (function(module, exports) {

Prism.languages.scss = Prism.languages.extend('css', {
	'comment': {
		pattern: /(^|[^\\])(?:\/\*[\s\S]*?\*\/|\/\/.*)/,
		lookbehind: true
	},
	'atrule': {
		pattern: /@[\w-]+(?:\([^()]+\)|[^(])*?(?=\s+[{;])/,
		inside: {
			'rule': /@[\w-]+/
			// See rest below
		}
	},
	// url, compassified
	'url': /(?:[-a-z]+-)*url(?=\()/i,
	// CSS selector regex is not appropriate for Sass
	// since there can be lot more things (var, @ directive, nesting..)
	// a selector must start at the end of a property or after a brace (end of other rules or nesting)
	// it can contain some characters that aren't used for defining rules or end of selector, & (parent selector), or interpolated variable
	// the end of a selector is found when there is no rules in it ( {} or {\s}) or if there is a property (because an interpolated var
	// can "pass" as a selector- e.g: proper#{$erty})
	// this one was hard to do, so please be careful if you edit this one :)
	'selector': {
		// Initial look-ahead is used to prevent matching of blank selectors
		pattern: /(?=\S)[^@;{}()]?(?:[^@;{}()]|&|#\{\$[-\w]+\})+(?=\s*\{(?:\}|\s|[^}]+[:{][^}]+))/m,
		inside: {
			'parent': {
				pattern: /&/,
				alias: 'important'
			},
			'placeholder': /%[-\w]+/,
			'variable': /\$[-\w]+|#\{\$[-\w]+\}/
		}
	}
});

Prism.languages.insertBefore('scss', 'atrule', {
	'keyword': [
		/@(?:if|else(?: if)?|for|each|while|import|extend|debug|warn|mixin|include|function|return|content)/i,
		{
			pattern: /( +)(?:from|through)(?= )/,
			lookbehind: true
		}
	]
});

Prism.languages.scss.property = {
	pattern: /(?:[\w-]|\$[-\w]+|#\{\$[-\w]+\})+(?=\s*:)/i,
	inside: {
		'variable': /\$[-\w]+|#\{\$[-\w]+\}/
	}
};

Prism.languages.insertBefore('scss', 'important', {
	// var and interpolated vars
	'variable': /\$[-\w]+|#\{\$[-\w]+\}/
});

Prism.languages.insertBefore('scss', 'function', {
	'placeholder': {
		pattern: /%[-\w]+/,
		alias: 'selector'
	},
	'statement': {
		pattern: /\B!(?:default|optional)\b/i,
		alias: 'keyword'
	},
	'boolean': /\b(?:true|false)\b/,
	'null': /\bnull\b/,
	'operator': {
		pattern: /(\s)(?:[-+*\/%]|[=!]=|<=?|>=?|and|or|not)(?=\s)/,
		lookbehind: true
	}
});

Prism.languages.scss['atrule'].inside.rest = Prism.languages.scss;

/***/ }),
/* 135 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/* global Prism */

(function () {
	var _annotation = '@\\w+';

	// Match an annotation
	var annotation = {
		pattern: RegExp('^' + _annotation, 'gi'),
		alias: 'atrule'
	};

	var _type = '{[^}]+}';

	// Match a type (always following an annotation)
	var type = {
		pattern: RegExp('^(' + _annotation + ')\\s+' + _type, 'gi'),
		lookbehind: true,
		alias: 'string'
	};

	var _param = '[\\$%]?[\\w\\._-]+';

	// Match a param (always following an annotation and optionally a type)
	var param = {
		pattern: RegExp('^(' + _annotation + '(\\s+' + _type + ')?)\\s+' + _param, 'gi'),
		lookbehind: true,
		alias: 'variable'
	};

	// Match a delimited URL
	var url = /<[^>]+>/g;

	Prism.languages.insertBefore('scss', 'comment', {
		'docblock': {
			pattern: /(^|[^\\])(\/\*\*[\w\W]*?\*\/|\/\/\/.*?(\r?\n|$))/g,
			lookbehind: true,
			alias: 'comment',
			inside: {

				// Annotation with param
				'annotation-param': {
					pattern: /@(access|example|alias|since)( .*|\n)/g,
					inside: {
						'param': param,
						'annotation': annotation,
						'url': url
					}
				},

				// Annotation with type and param
				'annotation-type-param-default': {
					pattern: /@(param|arg(ument)?|prop|requires|see)( .*|\r?\n|$)/g,
					inside: {
						'default': {
							pattern: RegExp('^(' + _annotation + '(\\s+' + _type + ')?\\s+' + _param + ')\\s+\\[[^\\)]+\\]', 'gi'),
							lookbehind: true,
							alias: 'string'
						},
						'param': param,
						'type': type,
						'annotation': annotation,
						'url': url
					}
				},

				// Annotation with only type
				'annotation-type': {
					pattern: /@(returns?)( .*|\r?\n|$)/g,
					inside: {
						'type': type,
						'annotation': annotation,
						'url': url
					}
				},

				// Annation with an URL
				'annotation-url': {
					pattern: /@(link|source)( .*|\r?\n|$)/g,
					inside: {
						'annotation': annotation,
						'url': /[^ ]+/
					}
				},

				// Type annotation
				'annotation-type-custom': {
					pattern: /@(type)( .*|\r?\n|$)/g,
					inside: {
						'annotation': annotation,
						'type': {
							pattern: /.*/,
							alias: 'string'
						}
					}
				},

				// Group annotation
				'annotation-group-custom': {
					pattern: /@(group)( .*|\r?\n|$)/g,
					inside: {
						'annotation': annotation,
						'group': {
							pattern: /.*/,
							alias: 'variable'
						}
					}
				},

				// Other annotations
				'annotation-single': {
					pattern: /@(content|deprecated|ignore|output|author|todo|throws?|exception)( .*|\r?\n|$)/g,
					inside: {
						'annotation': annotation,
						'url': url
					}
				}
			}
		}
	});
})();

/***/ }),
/* 136 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/* global Prism */

(function () {
	var _annotation = '@\\w+';

	// Match an annotation
	var annotation = {
		pattern: RegExp('^' + _annotation, 'gi'),
		alias: 'atrule'
	};

	var _type = '{[^}]+}';

	// Match a type (always following an annotation)
	var type = {
		pattern: RegExp('^(' + _annotation + ')\\s+' + _type, 'gi'),
		lookbehind: true,
		alias: 'string'
	};

	var _param = '[\\$%]?[\\w\\._-]+';

	// Match a param (always following an annotation and optionally a type)
	var param = {
		pattern: RegExp('^(' + _annotation + '(\\s+' + _type + ')?)\\s+' + _param, 'gi'),
		lookbehind: true,
		alias: 'variable'
	};

	// Match a delimited URL
	var url = /<[^>]+>/g;

	Prism.languages.insertBefore('javascript', 'comment', {
		'docblock': {
			pattern: /(^|[^\\])(\/\*\*[\w\W]*?\*\/|\/\/\/.*?(\r?\n|$))/g,
			lookbehind: true,
			alias: 'comment',
			inside: {

				// Annotation with param
				'annotation-param': {
					pattern: /@(access|example|alias|since)( .*|\n)/g,
					inside: {
						'param': param,
						'annotation': annotation,
						'url': url
					}
				},

				// Annotation with type and param
				'annotation-type-param-default': {
					pattern: /@(param|arg(ument)?|prop|requires|see)( .*|\r?\n|$)/g,
					inside: {
						'default': {
							pattern: RegExp('^(' + _annotation + '(\\s+' + _type + ')?\\s+' + _param + ')\\s+\\[[^\\)]+\\]', 'gi'),
							lookbehind: true,
							alias: 'string'
						},
						'param': param,
						'type': type,
						'annotation': annotation,
						'url': url
					}
				},

				// Annotation with only type
				'annotation-type': {
					pattern: /@(returns?)( .*|\r?\n|$)/g,
					inside: {
						'type': type,
						'annotation': annotation,
						'url': url
					}
				},

				// Annation with an URL
				'annotation-url': {
					pattern: /@(link|source)( .*|\r?\n|$)/g,
					inside: {
						'annotation': annotation,
						'url': /[^ ]+/
					}
				},

				// Type annotation
				'annotation-type-custom': {
					pattern: /@(type)( .*|\r?\n|$)/g,
					inside: {
						'annotation': annotation,
						'type': {
							pattern: /.*/,
							alias: 'string'
						}
					}
				},

				// Group annotation
				'annotation-group-custom': {
					pattern: /@(group)( .*|\r?\n|$)/g,
					inside: {
						'annotation': annotation,
						'group': {
							pattern: /.*/,
							alias: 'variable'
						}
					}
				},

				// Other annotations
				'annotation-single': {
					pattern: /@(content|deprecated|ignore|sourceCode|module|name|namespace|fileOverview|output|author|todo|throws?|exception)( .*|\r?\n|$)/g,
					inside: {
						'annotation': annotation,
						'url': url
					}
				}
			}
		}
	});
})();

/***/ }),
/* 137 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(138);
if(typeof content === 'string') content = [[module.i, content, '']];
// Prepare cssTransformation
var transform;

var options = {"insertAt":{"before":"#webpack-style-loader-insert-before-this"},"hmr":true}
options.transform = transform
// add the styles to the DOM
var update = __webpack_require__(10)(content, options);
if(content.locals) module.exports = content.locals;
// Hot Module Replacement
if(false) {
	// When the styles change, update the <style> tags
	if(!content.locals) {
		module.hot.accept("!!../../../../../node_modules/css-loader/index.js??ref--2-1!../../../../../node_modules/postcss-loader/lib/index.js??ref--2-2!../../../../../node_modules/sass-loader/lib/loader.js??ref--2-3!./prismjs-material-theme.scss", function() {
			var newContent = require("!!../../../../../node_modules/css-loader/index.js??ref--2-1!../../../../../node_modules/postcss-loader/lib/index.js??ref--2-2!../../../../../node_modules/sass-loader/lib/loader.js??ref--2-3!./prismjs-material-theme.scss");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
	}
	// When the module is disposed, remove the <style> tags
	module.hot.dispose(function() { update(); });
}

/***/ }),
/* 138 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(9)(false);
// imports


// module
exports.push([module.i, "pre[class*=\"language-\"],\ncode[class*=\"language-\"] {\n  color: #CED3DF;\n  text-shadow: none;\n  font-weight: 300;\n  font-size: 1rem;\n  font-family: 'Fira Code', monospace;\n  direction: ltr;\n  text-align: left;\n  white-space: pre;\n  word-spacing: normal;\n  word-break: normal;\n  word-wrap: normal;\n  line-height: 1.4em;\n  -moz-tab-size: 4;\n  -o-tab-size: 4;\n  tab-size: 4;\n  -webkit-hyphens: none;\n  -ms-hyphens: none;\n  hyphens: none;\n  background: #212121; }\n  pre[class*=\"language-\"]::-moz-selection,\n  pre[class*=\"language-\"] ::-moz-selection, pre[class*=\"language-\"]::-moz-selection,\n  pre[class*=\"language-\"] ::-moz-selection,\n  code[class*=\"language-\"]::-moz-selection,\n  code[class*=\"language-\"] ::-moz-selection,\n  code[class*=\"language-\"]::-moz-selection,\n  code[class*=\"language-\"] ::-moz-selection {\n    background: rgba(255, 255, 255, 0.2); }\n  pre[class*=\"language-\"]::-moz-selection,\n  pre[class*=\"language-\"] ::-moz-selection, pre[class*=\"language-\"]::selection,\n  pre[class*=\"language-\"] ::selection,\n  code[class*=\"language-\"]::-moz-selection,\n  code[class*=\"language-\"] ::-moz-selection,\n  code[class*=\"language-\"]::selection,\n  code[class*=\"language-\"] ::selection {\n    background: rgba(255, 255, 255, 0.2); }\n\npre[class*=\"language-\"] {\n  padding: 2em 1.2rem;\n  margin: 1em 0;\n  overflow: auto; }\n  pre[class*=\"language-\"]::-webkit-scrollbar-track {\n    -webkit-box-shadow: inset 0 0 6px rgba(255, 255, 255, 0.3);\n    background-color: transparent; }\n  pre[class*=\"language-\"]::-webkit-scrollbar {\n    width: 1rem;\n    height: 1rem;\n    background-color: transparent; }\n  pre[class*=\"language-\"]::-webkit-scrollbar-thumb {\n    background: rgba(255, 255, 255, 0.35); }\n  pre[class*=\"language-\"].code-toolbar .toolbar {\n    opacity: 1;\n    top: .3em;\n    right: 2em; }\n    pre[class*=\"language-\"].code-toolbar .toolbar a,\n    pre[class*=\"language-\"].code-toolbar .toolbar span {\n      font-size: 1em;\n      background: transparent;\n      box-shadow: none;\n      color: #666;\n      -webkit-user-select: none;\n      -moz-user-select: none;\n      -ms-user-select: none;\n      user-select: none; }\n    pre[class*=\"language-\"].code-toolbar .toolbar span {\n      pointer-events: none; }\n  pre[class*=\"language-\"].code-toolbar.language-ejs .toolbar span {\n    text-transform: uppercase; }\n\n:not(pre) > code[class*=\"language-\"] {\n  padding: .1em .3em;\n  border-radius: .3em;\n  white-space: normal; }\n\n[class*=\"language-\"] .namespace {\n  opacity: .7; }\n\n[class*=\"language-\"] .token.comment, [class*=\"language-\"] .token.prolog, [class*=\"language-\"] .token.doctype, [class*=\"language-\"] .token.cdata {\n  color: #666;\n  font-weight: normal; }\n\n[class*=\"language-\"] .token.operator, [class*=\"language-\"] .token.punctuation {\n  color: #6dc2b8; }\n\n[class*=\"language-\"] .token.tag {\n  color: #ff5370; }\n\n[class*=\"language-\"] .token.property, [class*=\"language-\"] .token.constant, [class*=\"language-\"] .token.function, [class*=\"language-\"] .token.symbol, [class*=\"language-\"] .token.deleted {\n  color: #76d4ff; }\n\n.style [class*=\"language-\"] .token.function,\n.language-css [class*=\"language-\"] .token.function,\n.language-sass [class*=\"language-\"] .token.function,\n.language-scss [class*=\"language-\"] .token.function {\n  color: #f78c6c; }\n\n.style [class*=\"language-\"] .token.property,\n.language-css [class*=\"language-\"] .token.property,\n.language-sass [class*=\"language-\"] .token.property,\n.language-scss [class*=\"language-\"] .token.property {\n  color: #fdcf7b; }\n\n[class*=\"language-\"] .token.selector {\n  color: #ffc251; }\n  [class*=\"language-\"] .token.selector .id {\n    color: #ffff00; }\n  [class*=\"language-\"] .token.selector .class {\n    color: #ffc251; }\n  [class*=\"language-\"] .token.selector .attribute {\n    color: #6dc2b8; }\n\n[class*=\"language-\"] .token.attr-name, [class*=\"language-\"] .token.string, [class*=\"language-\"] .token.char, [class*=\"language-\"] .token.builtin, [class*=\"language-\"] .token.inserted {\n  color: #c3e887; }\n  .comment [class*=\"language-\"] .token.attr-name, .comment [class*=\"language-\"] .token.string, .comment [class*=\"language-\"] .token.char, .comment [class*=\"language-\"] .token.builtin, .comment [class*=\"language-\"] .token.inserted {\n    color: #666; }\n\n[class*=\"language-\"] .token.entity, [class*=\"language-\"] .token.url,\n.language-css [class*=\"language-\"] .token.string,\n.style [class*=\"language-\"] .token.string {\n  color: #ffc251; }\n\n[class*=\"language-\"] .token.atrule, [class*=\"language-\"] .token.attr-value {\n  color: #C2E78C; }\n\n[class*=\"language-\"] .token.number, [class*=\"language-\"] .token.important, [class*=\"language-\"] .token.time, [class*=\"language-\"] .token.easing, [class*=\"language-\"] .token.angle, [class*=\"language-\"] .token.boolean, [class*=\"language-\"] .token.keyword {\n  color: #c792ea; }\n\n[class*=\"language-\"] .token.regex, [class*=\"language-\"] .token.important, [class*=\"language-\"] .token.variable {\n  color: #ffc251; }\n  .comment [class*=\"language-\"] .token.regex, .comment [class*=\"language-\"] .token.important, .comment [class*=\"language-\"] .token.variable {\n    color: #666; }\n\n[class*=\"language-\"] .token.operator, [class*=\"language-\"] .token.important, [class*=\"language-\"] .token.bold {\n  font-weight: bold; }\n\n[class*=\"language-\"] .token.italic {\n  font-style: italic; }\n\n[class*=\"language-\"] .token.entity {\n  cursor: help; }\n\n[class*=\"language-\"] .token.annotation {\n  color: #6b517f; }\n\n[class*=\"language-\"] .token.tab:not(:empty)::before, [class*=\"language-\"] .token.cr::before, [class*=\"language-\"] .token.lf::before, [class*=\"language-\"] .token.space::before {\n  color: #000; }\n\n.prism-previewer-time,\n.prism-previewer-angle,\n.prism-previewer-color,\n.prism-previewer-gradient {\n  width: 4em;\n  height: 4em;\n  margin-top: -5em;\n  margin-left: -2em; }\n  .prism-previewer-time svg,\n  .prism-previewer-angle svg,\n  .prism-previewer-color svg,\n  .prism-previewer-gradient svg {\n    width: 4em;\n    height: 4em; }\n  @media screen and (min-width: 641px) {\n    pre[class*=\"language-\"] {\n      padding: 2em; } }\n", ""]);

// exports


/***/ }),
/* 139 */
/***/ (function(module, exports) {

(function () {
	'use strict';

	if (typeof Prism === 'undefined') {
		console.log('Prism is not defined');
		return false;
	}

	if (!Prism.languages.markup || !Prism.languages.javascript) {
		console.log('prism-ejs-language required markup and javascript languages');
		return false;
	}

	Prism.languages.ejs = Prism.languages.extend('markup', {
		'comment': /(<%%?#)[\s\S]*?([-]?%%?>)/g,
		'punctuation': /(<%%?[-|=|_]?|[-|_]?%%?>|\/?>)/g,
		'entity': /&#?[\da-z]{1,8};/i,
		'attr-name': {
			pattern: /[^\s>\/]+/,
			inside: {
				'namespace': /^[^\s>\/:]+:/,
				'tag': {
					pattern: /<\w+/,
					inside: {
						punctuation: /^</
					}
				},
				'attr-value': {
					pattern: /=(?:("|')(?:\\[\s\S]|(?!\1)[^\\])*\1|[^\s'">=]+)/i,
					inside: {
						'punctuation': [
							/^=/,
							{
								pattern: /(^|[^\\])["']/,
								lookbehind: true
							}
						]
					}
				},
				'punctuation': /=/
			}
		}
	});

	Prism.languages.insertBefore('ejs', 'tag', {
		script: {
			pattern: /(<%%?[-|=|_]?)[\s\S]*?(?=[-|_]?%%?>)/i,
			lookbehind: !0,
			inside: Prism.languages.javascript,
			alias: 'language-javascript'
		}
	});
}());


/***/ })
]));
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9fSFRNTC9zcmMvanMvX21vZHVsZXMvcHJpc21qcy9wcmlzbS5qcyIsIndlYnBhY2s6Ly8vLi9fSFRNTC9zcmMvanMvX21vZHVsZXMvcHJpc21qcy9wcmlzbWpzLWltcG9ydC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvY2xpcGJvYXJkL2xpYi9jbGlwYm9hcmQuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL2NsaXBib2FyZC9saWIvY2xpcGJvYXJkLWFjdGlvbi5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvc2VsZWN0L3NyYy9zZWxlY3QuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3RpbnktZW1pdHRlci9pbmRleC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvZ29vZC1saXN0ZW5lci9zcmMvbGlzdGVuLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9nb29kLWxpc3RlbmVyL3NyYy9pcy5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvZGVsZWdhdGUvc3JjL2RlbGVnYXRlLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9kZWxlZ2F0ZS9zcmMvY2xvc2VzdC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvcHJpc21qcy9wcmlzbS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvcHJpc21qcy9wbHVnaW5zL2tlZXAtbWFya3VwL3ByaXNtLWtlZXAtbWFya3VwLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9wcmlzbWpzL3BsdWdpbnMvdW5lc2NhcGVkLW1hcmt1cC9wcmlzbS11bmVzY2FwZWQtbWFya3VwLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9wcmlzbWpzL3BsdWdpbnMvbm9ybWFsaXplLXdoaXRlc3BhY2UvcHJpc20tbm9ybWFsaXplLXdoaXRlc3BhY2UuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3ByaXNtanMvcGx1Z2lucy9oaWdobGlnaHQta2V5d29yZHMvcHJpc20taGlnaGxpZ2h0LWtleXdvcmRzLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9wcmlzbWpzL3BsdWdpbnMvdG9vbGJhci9wcmlzbS10b29sYmFyLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9wcmlzbWpzL3BsdWdpbnMvdG9vbGJhci9wcmlzbS10b29sYmFyLmNzcz81YjU0Iiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9wcmlzbWpzL3BsdWdpbnMvdG9vbGJhci9wcmlzbS10b29sYmFyLmNzcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvcHJpc21qcy9wbHVnaW5zL3Nob3ctbGFuZ3VhZ2UvcHJpc20tc2hvdy1sYW5ndWFnZS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvcHJpc21qcy9wbHVnaW5zL2NvcHktdG8tY2xpcGJvYXJkL3ByaXNtLWNvcHktdG8tY2xpcGJvYXJkLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9wcmlzbWpzL25vZGVfbW9kdWxlcy9jbGlwYm9hcmQvZGlzdC9jbGlwYm9hcmQuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3ByaXNtanMvcGx1Z2lucy9wcmV2aWV3ZXJzL3ByaXNtLXByZXZpZXdlcnMuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3ByaXNtanMvcGx1Z2lucy9wcmV2aWV3ZXJzL3ByaXNtLXByZXZpZXdlcnMuY3NzPzhkZDUiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3ByaXNtanMvcGx1Z2lucy9wcmV2aWV3ZXJzL3ByaXNtLXByZXZpZXdlcnMuY3NzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9wcmlzbWpzL2NvbXBvbmVudHMvcHJpc20tY3NzLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9wcmlzbWpzL2NvbXBvbmVudHMvcHJpc20tY3NzLWV4dHJhcy5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvcHJpc21qcy9jb21wb25lbnRzL3ByaXNtLXNhc3MuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3ByaXNtanMvY29tcG9uZW50cy9wcmlzbS1zY3NzLmpzIiwid2VicGFjazovLy8uL19IVE1ML3NyYy9qcy9fbW9kdWxlcy9wcmlzbWpzL3ByaXNtLXNjc3Mtc2Fzc2RvYy5qcyIsIndlYnBhY2s6Ly8vLi9fSFRNTC9zcmMvanMvX21vZHVsZXMvcHJpc21qcy9wcmlzbS1zY3NzLWpzZG9jLmpzIiwid2VicGFjazovLy8uL19IVE1ML3NyYy9qcy9fbW9kdWxlcy9wcmlzbWpzL3ByaXNtanMtbWF0ZXJpYWwtdGhlbWUuc2Nzcz9iOTg4Iiwid2VicGFjazovLy8uL19IVE1ML3NyYy9qcy9fbW9kdWxlcy9wcmlzbWpzL3ByaXNtanMtbWF0ZXJpYWwtdGhlbWUuc2NzcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvcHJpc20tZWpzLWxhbmd1YWdlL3ByaXNtLWVqcy1sYW5ndWFnZS5qcyJdLCJuYW1lcyI6WyJwcmlzbSIsInNlbGVjdG9yIiwiJGNvbnRleHQiLCIkY29kZSIsImpxdWVyeSIsIiQiLCJsZW5ndGgiLCJwcmVsb2FkZXIiLCJwYXJlbnQiLCJzaG93IiwidGhlbiIsImhpZGUiLCJfYW5ub3RhdGlvbiIsImFubm90YXRpb24iLCJwYXR0ZXJuIiwiUmVnRXhwIiwiYWxpYXMiLCJfdHlwZSIsInR5cGUiLCJsb29rYmVoaW5kIiwiX3BhcmFtIiwicGFyYW0iLCJ1cmwiLCJQcmlzbSIsImxhbmd1YWdlcyIsImluc2VydEJlZm9yZSIsImluc2lkZSJdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQTs7QUFFQTs7Ozs7QUFLQTtBQUNBO0FBQ0E7Ozs7OztBQUVBOzs7Ozs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7O0FBS0EsU0FBU0EsS0FBVCxHQUFtRTtBQUFBLEtBQW5EQyxRQUFtRCx1RUFBeEMscUJBQXdDO0FBQUEsS0FBakJDLFFBQWlCLHVFQUFOLElBQU07O0FBQ2xFLEtBQUlDLFFBQVNGLFlBQVlBLFNBQVNHLE1BQXRCLEdBQWdDSCxRQUFoQyxHQUEyQ0ksRUFBRUosUUFBRixFQUFZQyxRQUFaLENBQXZEO0FBQ0EsS0FBSUMsTUFBTUcsTUFBVixFQUFrQjtBQUNqQixNQUFJQyxZQUFZLHdCQUFjSixNQUFNSyxNQUFOLENBQWEsS0FBYixDQUFkLENBQWhCO0FBQ0FELFlBQVVFLElBQVY7QUFDQSwwRkFHRUMsSUFIRixDQUdPLFlBQU07QUFDWkgsYUFBVUksSUFBVjtBQUNBLEdBTEQ7QUFNQTtBQUNEOztBQUVEO0FBQ0E7QUFDQTs7a0JBRWVYLEs7Ozs7Ozs7QUN4Q2Y7O0FBRUE7Ozs7O0FBS0E7QUFDQTtBQUNBOztBQUVBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBLHlCOzs7Ozs7QUM5QkE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0EsS0FBSztBQUNMO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUM7QUFDRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSwyQkFBMkIsa0JBQWtCO0FBQzdDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLOztBQUVMO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0EsbUJBQW1CLDJDQUEyQztBQUM5RCxtQkFBbUIsT0FBTztBQUMxQjtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsbUJBQW1CLE9BQU87QUFDMUI7OztBQUdBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLGlCQUFpQjs7QUFFakI7QUFDQTtBQUNBLFNBQVM7O0FBRVQ7QUFDQSxLQUFLOztBQUVMO0FBQ0E7QUFDQSxlQUFlLE9BQU87QUFDdEIsZUFBZSxRQUFRO0FBQ3ZCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBLENBQUMsRTs7Ozs7O0FDOU1EO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBLEtBQUs7QUFDTDtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsMkJBQTJCLGtCQUFrQjtBQUM3QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSzs7QUFFTDtBQUNBO0FBQ0EsbUJBQW1CLE9BQU87QUFDMUI7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsbUJBQW1CLE9BQU87QUFDMUI7OztBQUdBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0EscUJBQXFCO0FBQ3JCO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBO0FBQ0E7QUFDQSxTQUFTOztBQUVUO0FBQ0EsS0FBSzs7QUFFTDtBQUNBLENBQUMsRTs7Ozs7O0FDcE9EO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTs7Ozs7OztBQzFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0Esa0NBQWtDOztBQUVsQztBQUNBO0FBQ0E7QUFDQSxLQUFLOztBQUVMO0FBQ0EsR0FBRzs7QUFFSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEdBQUc7O0FBRUg7QUFDQTtBQUNBLHlDQUF5QztBQUN6QztBQUNBOztBQUVBLFdBQVcsU0FBUztBQUNwQjtBQUNBOztBQUVBO0FBQ0EsR0FBRzs7QUFFSDtBQUNBLGtDQUFrQztBQUNsQztBQUNBOztBQUVBO0FBQ0Esd0NBQXdDLFNBQVM7QUFDakQ7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7Ozs7QUNqRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsMkNBQTJDO0FBQ3RELFdBQVcsT0FBTztBQUNsQixXQUFXLFNBQVM7QUFDcEIsWUFBWTtBQUNaO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsWUFBWTtBQUN2QixXQUFXLE9BQU87QUFDbEIsV0FBVyxTQUFTO0FBQ3BCLFlBQVk7QUFDWjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVyx3QkFBd0I7QUFDbkMsV0FBVyxPQUFPO0FBQ2xCLFdBQVcsU0FBUztBQUNwQixZQUFZO0FBQ1o7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLOztBQUVMO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsT0FBTztBQUNsQixXQUFXLE9BQU87QUFDbEIsV0FBVyxTQUFTO0FBQ3BCLFlBQVk7QUFDWjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7OztBQzlGQTtBQUNBO0FBQ0E7QUFDQSxXQUFXLE9BQU87QUFDbEIsWUFBWTtBQUNaO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxXQUFXLE9BQU87QUFDbEIsWUFBWTtBQUNaO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsT0FBTztBQUNsQixZQUFZO0FBQ1o7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxXQUFXLE9BQU87QUFDbEIsWUFBWTtBQUNaO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOzs7Ozs7O0FDaERBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsUUFBUTtBQUNuQixXQUFXLE9BQU87QUFDbEIsV0FBVyxPQUFPO0FBQ2xCLFdBQVcsU0FBUztBQUNwQixXQUFXLFFBQVE7QUFDbkIsWUFBWTtBQUNaO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsV0FBVyxxQkFBcUI7QUFDaEMsV0FBVyxPQUFPO0FBQ2xCLFdBQVcsT0FBTztBQUNsQixXQUFXLFNBQVM7QUFDcEIsV0FBVyxRQUFRO0FBQ25CLFlBQVk7QUFDWjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxXQUFXLFFBQVE7QUFDbkIsV0FBVyxPQUFPO0FBQ2xCLFdBQVcsT0FBTztBQUNsQixXQUFXLFNBQVM7QUFDcEIsWUFBWTtBQUNaO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7Ozs7QUM3RUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsV0FBVyxRQUFRO0FBQ25CLFdBQVcsT0FBTztBQUNsQixZQUFZO0FBQ1o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7Ozs7O0FDL0JBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsTUFBTTtBQUNOOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0EsSUFBSTtBQUNKLHNDQUFzQyxzQkFBc0I7QUFDNUQ7QUFDQSxHQUFHOztBQUVIO0FBQ0E7QUFDQSxHQUFHOztBQUVIO0FBQ0E7QUFDQSx3Q0FBd0Msb0JBQW9CO0FBQzVEO0FBQ0E7QUFDQSxHQUFHOztBQUVIO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxNQUFNOztBQUVOO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEVBQUU7O0FBRUY7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLEdBQUc7O0FBRUg7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJOztBQUVKO0FBQ0EsR0FBRzs7QUFFSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEVBQUU7QUFDRixZQUFZOztBQUVaO0FBQ0E7QUFDQSxFQUFFOztBQUVGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUEsd0JBQXdCLHlCQUF5QjtBQUNqRDtBQUNBO0FBQ0EsRUFBRTs7QUFFRjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxFQUFFOztBQUVGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEVBQUU7O0FBRUY7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSxrQkFBa0IscUJBQXFCO0FBQ3ZDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0EsdUNBQXVDLG1CQUFtQjs7QUFFMUQ7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsbUNBQW1DLG1FQUFtRTtBQUN0RztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsRUFBRTs7QUFFRjtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQSxFQUFFOztBQUVGO0FBQ0EsU0FBUzs7QUFFVDtBQUNBOztBQUVBOztBQUVBO0FBQ0EsR0FBRzs7QUFFSDtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQSwwQkFBMEIsMkJBQTJCO0FBQ3JEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGdCQUFnQjtBQUNoQjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQSx5RUFBeUU7QUFDekUsRUFBRTs7QUFFRjs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUEsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUk7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLEVBQUU7QUFDRix1QkFBdUIsS0FBSztBQUM1Qjs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQSxzREFBc0Q7QUFDdEQ7QUFDQSxDQUFDOztBQUVEO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsMkJBQTJCLFNBQVM7QUFDcEM7QUFDQTtBQUNBO0FBQ0E7QUFDQSxFQUFFO0FBQ0Y7QUFDQSxrQkFBa0IsUUFBUSxXQUFXO0FBQ3JDO0FBQ0E7QUFDQTtBQUNBLEVBQUU7QUFDRjtBQUNBO0FBQ0E7QUFDQSx1QkFBdUI7QUFDdkI7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsRUFBRTs7QUFFRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQSxFQUFFO0FBQ0Y7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEVBQUU7QUFDRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxFQUFFO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG9CQUFvQixJQUFJO0FBQ3hCOzs7QUFHQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDBHQUEwRyxFQUFFO0FBQzVHLENBQUM7O0FBRUQ7QUFDQTtBQUNBLHVFQUF1RSxJQUFJLGtCQUFrQjtBQUM3RjtBQUNBO0FBQ0EsRUFBRTtBQUNGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDOztBQUVEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGtCQUFrQixHQUFHLElBQUk7QUFDekI7QUFDQTtBQUNBLHFCQUFxQixHQUFHO0FBQ3hCO0FBQ0EsTUFBTTtBQUNOO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxFQUFFO0FBQ0Y7O0FBRUE7OztBQUdBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxHQUFHOztBQUVIOztBQUVBOztBQUVBLENBQUM7Ozs7Ozs7O0FDMzFCRDs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSw2Q0FBNkMsT0FBTztBQUNwRDtBQUNBLCtCQUErQjtBQUMvQjtBQUNBLEtBQUssZ0NBQWdDO0FBQ3JDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxFQUFFOztBQUVGO0FBQ0E7O0FBRUE7QUFDQSw4Q0FBOEMsT0FBTzs7QUFFckQ7O0FBRUEsZ0NBQWdDO0FBQ2hDO0FBQ0E7QUFDQTs7QUFFQSxNQUFNLGlDQUFpQztBQUN2QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0wsSUFBSTtBQUNKO0FBQ0E7QUFDQTtBQUNBLEVBQUU7QUFDRixDQUFDOzs7Ozs7O0FDbEdEOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxFQUFFOztBQUVGO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7O0FBRUEsb0NBQW9DLGVBQWU7QUFDbkQ7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsRUFBRTtBQUNGLENBQUM7Ozs7Ozs7QUMzQ0Q7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSwwQkFBMEI7QUFDMUI7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsRUFBRTtBQUNGOztBQUVBO0FBQ0E7QUFDQSxnQkFBZ0IsZ0JBQWdCO0FBQ2hDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsRUFBRTtBQUNGO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxFQUFFOztBQUVGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxFQUFFO0FBQ0Y7QUFDQTtBQUNBLEVBQUU7QUFDRjtBQUNBO0FBQ0E7QUFDQSxFQUFFO0FBQ0Y7QUFDQTtBQUNBLHFDQUFxQyxlQUFlO0FBQ3BELEVBQUU7QUFDRjtBQUNBO0FBQ0EsRUFBRTtBQUNGO0FBQ0E7QUFDQTtBQUNBLEVBQUU7QUFDRjtBQUNBOztBQUVBO0FBQ0E7O0FBRUEsOEJBQThCLDJCQUEyQixFQUFFOztBQUUzRDtBQUNBOztBQUVBO0FBQ0EsRUFBRTtBQUNGO0FBQ0E7QUFDQSxFQUFFO0FBQ0Y7QUFDQTs7QUFFQTtBQUNBLGlCQUFpQixrQkFBa0I7QUFDbkM7QUFDQTs7QUFFQTtBQUNBOztBQUVBLGtCQUFrQixpQkFBaUI7QUFDbkM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0EsZ0JBQWdCLHFCQUFxQjtBQUNyQzs7QUFFQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxFQUFFO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUM7O0FBRUQsQ0FBQyxJOzs7Ozs7QUM3TEQ7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDOztBQUVELENBQUM7Ozs7Ozs7O0FDaEJEO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxZQUFZLE9BQU87QUFDbkIsWUFBWSxnQkFBZ0I7QUFDNUI7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsTUFBTTtBQUNOLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsR0FBRzs7QUFFSDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7O0FBRUg7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0EsRUFBRTs7QUFFRjtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUM7Ozs7Ozs7QUN4SUQ7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxlQUFlLFlBQVksb0RBQW9EO0FBQy9FO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0EsZ0NBQWdDLFVBQVUsRUFBRTtBQUM1QyxDOzs7Ozs7QUN6QkE7QUFDQTs7O0FBR0E7QUFDQSwyQ0FBNEMsdUJBQXVCLEdBQUcsaUNBQWlDLHVCQUF1QixjQUFjLGdCQUFnQix5Q0FBeUMsZUFBZSxHQUFHLHVDQUF1QyxlQUFlLEdBQUcsK0NBQStDLDBCQUEwQixHQUFHLG1DQUFtQyxvQkFBb0IsR0FBRyx3Q0FBd0MscUJBQXFCLGNBQWMsbUJBQW1CLGtCQUFrQix3QkFBd0Isc0JBQXNCLGVBQWUsOEJBQThCLDRDQUE0QywwQkFBMEIsR0FBRywyR0FBMkcsZ0JBQWdCLG9CQUFvQixvQkFBb0Isd0JBQXdCLHlDQUF5QywwQ0FBMEMsd0JBQXdCLEdBQUcsdVBBQXVQLG1CQUFtQiwwQkFBMEIsR0FBRzs7QUFFMXZDOzs7Ozs7O0FDUEE7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBLDJDQUEyQyxpM0NBQWkzQztBQUM1NUM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQSxDQUFDOztBQUVELENBQUM7Ozs7Ozs7QUMzQkQ7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUk7O0FBRUo7QUFDQTs7QUFFQTtBQUNBLElBQUk7QUFDSjtBQUNBOztBQUVBO0FBQ0EsSUFBSTtBQUNKOztBQUVBO0FBQ0E7QUFDQTtBQUNBLElBQUk7QUFDSjtBQUNBLEVBQUU7QUFDRixDQUFDOzs7Ozs7O0FDMUVEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUM7QUFDRCxvQ0FBb0M7QUFDcEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1EQUFtRCxjQUFjO0FBQ2pFO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsbUNBQW1DLDBCQUEwQixFQUFFO0FBQy9ELHlDQUF5QyxlQUFlO0FBQ3hEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSw4REFBOEQsK0RBQStEO0FBQzdIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFVBQVU7QUFDVjtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxnR0FBZ0c7QUFDaEc7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsMkJBQTJCLGtCQUFrQjtBQUM3QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSzs7QUFFTDtBQUNBO0FBQ0EsbUJBQW1CLE9BQU87QUFDMUI7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsbUJBQW1CLE9BQU87QUFDMUI7OztBQUdBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0EscUJBQXFCO0FBQ3JCO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBO0FBQ0E7QUFDQSxTQUFTOztBQUVUO0FBQ0EsS0FBSzs7QUFFTDtBQUNBLENBQUM7O0FBRUQsT0FBTztBQUNQO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsMkNBQTJDO0FBQ3RELFdBQVcsT0FBTztBQUNsQixXQUFXLFNBQVM7QUFDcEIsWUFBWTtBQUNaO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsWUFBWTtBQUN2QixXQUFXLE9BQU87QUFDbEIsV0FBVyxTQUFTO0FBQ3BCLFlBQVk7QUFDWjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVyx3QkFBd0I7QUFDbkMsV0FBVyxPQUFPO0FBQ2xCLFdBQVcsU0FBUztBQUNwQixZQUFZO0FBQ1o7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLOztBQUVMO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsT0FBTztBQUNsQixXQUFXLE9BQU87QUFDbEIsV0FBVyxTQUFTO0FBQ3BCLFlBQVk7QUFDWjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7O0FBR0EsT0FBTztBQUNQO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLGtDQUFrQzs7QUFFbEM7QUFDQTtBQUNBO0FBQ0EsS0FBSzs7QUFFTDtBQUNBLEdBQUc7O0FBRUg7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxHQUFHOztBQUVIO0FBQ0E7QUFDQSx5Q0FBeUM7QUFDekM7QUFDQTs7QUFFQSxXQUFXLFNBQVM7QUFDcEI7QUFDQTs7QUFFQTtBQUNBLEdBQUc7O0FBRUg7QUFDQSxrQ0FBa0M7QUFDbEM7QUFDQTs7QUFFQTtBQUNBLHdDQUF3QyxTQUFTO0FBQ2pEO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBOzs7QUFHQSxPQUFPO0FBQ1A7QUFDQTs7QUFFQSxnR0FBZ0c7QUFDaEc7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsMkJBQTJCLGtCQUFrQjtBQUM3QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSzs7QUFFTDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBLG1CQUFtQiwyQ0FBMkM7QUFDOUQsbUJBQW1CLE9BQU87QUFDMUI7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLG1CQUFtQixPQUFPO0FBQzFCOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxpQkFBaUI7O0FBRWpCO0FBQ0E7QUFDQSxTQUFTOztBQUVUO0FBQ0EsS0FBSzs7QUFFTDtBQUNBO0FBQ0EsZUFBZSxPQUFPO0FBQ3RCLGVBQWUsUUFBUTtBQUN2QjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQSxDQUFDOztBQUVELE9BQU87QUFDUDtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsUUFBUTtBQUNuQixXQUFXLE9BQU87QUFDbEIsWUFBWTtBQUNaO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOzs7QUFHQSxPQUFPO0FBQ1A7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxXQUFXLFFBQVE7QUFDbkIsV0FBVyxPQUFPO0FBQ2xCLFdBQVcsT0FBTztBQUNsQixXQUFXLFNBQVM7QUFDcEIsV0FBVyxRQUFRO0FBQ25CLFlBQVk7QUFDWjtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLFdBQVcscUJBQXFCO0FBQ2hDLFdBQVcsT0FBTztBQUNsQixXQUFXLE9BQU87QUFDbEIsV0FBVyxTQUFTO0FBQ3BCLFdBQVcsUUFBUTtBQUNuQixZQUFZO0FBQ1o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsV0FBVyxRQUFRO0FBQ25CLFdBQVcsT0FBTztBQUNsQixXQUFXLE9BQU87QUFDbEIsV0FBVyxTQUFTO0FBQ3BCLFlBQVk7QUFDWjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOzs7QUFHQSxPQUFPO0FBQ1A7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxXQUFXLE9BQU87QUFDbEIsWUFBWTtBQUNaO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxXQUFXLE9BQU87QUFDbEIsWUFBWTtBQUNaO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsT0FBTztBQUNsQixZQUFZO0FBQ1o7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxXQUFXLE9BQU87QUFDbEIsWUFBWTtBQUNaO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOzs7QUFHQSxPQUFPO0FBQ1A7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7OztBQUdBLE9BQU87QUFDUDtBQUNBLENBQUMsRTs7Ozs7O0FDMTZCRDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxlQUFlLE9BQU87QUFDdEIsZUFBZSxPQUFPO0FBQ3RCLGVBQWUsU0FBUztBQUN4QjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQSxRQUFRO0FBQ1I7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0EsUUFBUTtBQUNSO0FBQ0EsUUFBUTtBQUNSO0FBQ0EsUUFBUTtBQUNSO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLGVBQWUsT0FBTztBQUN0QixlQUFlLE9BQU87QUFDdEIsZUFBZSxTQUFTO0FBQ3hCO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsUUFBUTtBQUNSO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxlQUFlLE9BQU87QUFDdEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtDQUErQyxLQUFLO0FBQ3BEO0FBQ0E7QUFDQTtBQUNBOztBQUVBLG9EQUFvRCxLQUFLOztBQUV6RDtBQUNBO0FBQ0EsTUFBTTtBQUNOO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxNQUFNO0FBQ047QUFDQSxNQUFNO0FBQ047QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBLDhCQUE4QixLQUFLO0FBQ25DO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsTUFBTTtBQUNOO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxNQUFNO0FBQ047QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0wsSUFBSTtBQUNKO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsTUFBTTtBQUNOO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxNQUFNO0FBQ047QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0wsSUFBSTtBQUNKO0FBQ0E7QUFDQSw4QkFBOEIsRUFBRSxFQUFFLElBQUksd0JBQXdCLElBQUksVUFBVSxJQUFJLFlBQVksSUFBSSxnQ0FBZ0MsSUFBSSxVQUFVLElBQUksWUFBWSxJQUFJO0FBQ2xLO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxNQUFNO0FBQ047QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxNQUFNO0FBQ047QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsTUFBTTs7QUFFTjs7QUFFQTtBQUNBLGdDQUFnQyxpQ0FBaUMsRUFBRTs7QUFFbkU7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMLElBQUk7QUFDSjtBQUNBO0FBQ0EsbURBQW1ELEVBQUUsMkRBQTJEO0FBQ2hIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsTUFBTTtBQUNOO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsTUFBTTtBQUNOO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHOztBQUVIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMLElBQUk7QUFDSjtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE1BQU07QUFDTjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsTUFBTTtBQUNOO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxZQUFZLFlBQVk7QUFDeEIsZUFBZTtBQUNmO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUk7O0FBRUo7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsSUFBSTtBQUNKOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsWUFBWSxPQUFPO0FBQ25CLFlBQVksU0FBUztBQUNyQixZQUFZLGlCQUFpQjtBQUM3QixZQUFZLFVBQVU7QUFDdEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7O0FBRUg7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUk7QUFDSjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsWUFBWSxZQUFZO0FBQ3hCLFlBQVksT0FBTztBQUNuQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUk7QUFDSixHQUFHO0FBQ0g7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsTUFBTTtBQUNOO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUEsaUNBQWlDO0FBQ2pDO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxFQUFFOztBQUVGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxFQUFFOztBQUVGO0FBQ0E7QUFDQTs7QUFFQSxDQUFDLEk7Ozs7OztBQzFzQkQ7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxlQUFlLFlBQVksb0RBQW9EO0FBQy9FO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0EsZ0NBQWdDLFVBQVUsRUFBRTtBQUM1QyxDOzs7Ozs7QUN6QkE7QUFDQTs7O0FBR0E7QUFDQSw4RkFBK0YsdUJBQXVCLHlCQUF5QixHQUFHLDZDQUE2QyxjQUFjLEdBQUcsb0JBQW9CLHNCQUFzQixnQkFBZ0IsaUJBQWlCLHVCQUF1QixpQkFBaUIsNkJBQTZCLEdBQUcsNEJBQTRCLGtCQUFrQix5QkFBeUIsR0FBRyxvREFBb0QsZ0JBQWdCLHVCQUF1Qix5QkFBeUIsR0FBRywyQkFBMkIsY0FBYyxnQkFBZ0IsZUFBZSxpQkFBaUIsd0JBQXdCLDJCQUEyQiwrRUFBK0UsR0FBRywwQkFBMEIsY0FBYyxhQUFhLGNBQWMseUJBQXlCLGtDQUFrQyxxQ0FBcUMsMkJBQTJCLEdBQUcsa0NBQWtDLGNBQWMsaUJBQWlCLGtCQUFrQix1QkFBdUIseUNBQXlDLDhCQUE4QixHQUFHLDJCQUEyQixlQUFlLEdBQUcsbUNBQW1DLHVCQUF1QixxQkFBcUIsR0FBRyxnQ0FBZ0Msb0JBQW9CLEdBQUcsOEJBQThCLGdCQUFnQixpQkFBaUIsOEJBQThCLEdBQUcsNkNBQTZDLHlDQUF5QyxHQUFHLGlDQUFpQyxzQkFBc0IsK0JBQStCLHdCQUF3QixxQkFBcUIsNkJBQTZCLEdBQUcsK0JBQStCLCtLQUErSywrQkFBK0Isc0NBQXNDLGtCQUFrQix1QkFBdUIsR0FBRyxvQ0FBb0Msa0JBQWtCLEdBQUcsaUNBQWlDLHVCQUF1QixjQUFjLGVBQWUsZ0JBQWdCLGlCQUFpQix3QkFBd0IsMkJBQTJCLCtFQUErRSxHQUFHLDRCQUE0QiwrS0FBK0ssK0JBQStCLHNDQUFzQyxHQUFHLGlDQUFpQyw4QkFBOEIsaUNBQWlDLEdBQUcsNkJBQTZCLHNCQUFzQix1QkFBdUIsZ0JBQWdCLGlCQUFpQixxQkFBcUIsR0FBRyxtQ0FBbUMsMEJBQTBCLEdBQUcsK0JBQStCLGdCQUFnQixpQkFBaUIsR0FBRyxrQ0FBa0MsNkJBQTZCLGtCQUFrQixHQUFHLGdDQUFnQyxlQUFlLGtCQUFrQiwwQkFBMEIsb0JBQW9CLEdBQUcsZ0NBQWdDLGtCQUFrQix3QkFBd0Isb0JBQW9CLEdBQUcscUNBQXFDLFFBQVEsK0JBQStCLDJCQUEyQixLQUFLLFNBQVMsaUNBQWlDLDJCQUEyQixLQUFLLFVBQVUsK0JBQStCLDhCQUE4QixLQUFLLEdBQUcsa0NBQWtDLHVCQUF1QixxQkFBcUIsR0FBRywrQkFBK0Isb0JBQW9CLEdBQUcsNkJBQTZCLGdCQUFnQixpQkFBaUIsOEJBQThCLEdBQUcsZ0NBQWdDLHNCQUFzQiwrQkFBK0Isd0JBQXdCLHFCQUFxQiw2QkFBNkIseUJBQXlCLHVEQUF1RCxHQUFHOztBQUU1M0g7Ozs7Ozs7QUNQQTtBQUNBO0FBQ0E7QUFDQSwyQkFBMkIsU0FBUztBQUNwQztBQUNBO0FBQ0E7QUFDQTtBQUNBLEVBQUU7QUFDRjtBQUNBLGtCQUFrQixRQUFRLFdBQVc7QUFDckM7QUFDQTtBQUNBO0FBQ0EsRUFBRTtBQUNGO0FBQ0E7QUFDQTtBQUNBLHVCQUF1QjtBQUN2Qjs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxFQUFFOztBQUVGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBLEVBQUU7QUFDRixDOzs7Ozs7QUNuREE7QUFDQSxlQUFlLE9BQU8sVUFBVTtBQUNoQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0Esc0JBQXNCLElBQUk7QUFDMUIsc0JBQXNCLElBQUk7QUFDMUI7QUFDQSxDQUFDLEU7Ozs7OztBQ2ZEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsRUFBRTs7QUFFRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxFQUFFO0FBQ0Y7OztBQUdBLDZCQUE2QixVQUFVO0FBQ3ZDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsRUFBRTtBQUNGO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEVBQUU7O0FBRUYsQ0FBQyxTOzs7Ozs7QUN4RUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQSxFQUFFO0FBQ0Y7QUFDQSxpREFBaUQ7QUFDakQ7QUFDQTtBQUNBO0FBQ0E7QUFDQSxFQUFFO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esb0VBQW9FLEtBQUssR0FBRztBQUM1RSwyQ0FBMkMsTUFBTTtBQUNqRDtBQUNBO0FBQ0E7QUFDQSx3QkFBd0IsYUFBYSxTQUFTLFVBQVUsVUFBVSxLQUFLLE9BQU8sS0FBSyxJQUFJO0FBQ3ZGO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0EsNEJBQTRCLFVBQVU7QUFDdEM7QUFDQTtBQUNBLENBQUM7O0FBRUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUM7O0FBRUQ7QUFDQSxnQ0FBZ0MsVUFBVTtBQUMxQztBQUNBLDJCQUEyQixVQUFVO0FBQ3JDO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLDBCQUEwQixVQUFVO0FBQ3BDLENBQUM7O0FBRUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQSxFQUFFO0FBQ0Y7QUFDQTtBQUNBO0FBQ0EsRUFBRTtBQUNGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUM7O0FBRUQsa0U7Ozs7Ozs7OztBQzFFQTs7QUFFQyxhQUFZO0FBQ1osS0FBSVksY0FBYyxPQUFsQjs7QUFFQTtBQUNBLEtBQUlDLGFBQWE7QUFDaEJDLFdBQVNDLE9BQU8sTUFBTUgsV0FBYixFQUEwQixJQUExQixDQURPO0FBRWhCSSxTQUFPO0FBRlMsRUFBakI7O0FBS0EsS0FBSUMsUUFBUSxTQUFaOztBQUVBO0FBQ0EsS0FBSUMsT0FBTztBQUNWSixXQUFTQyxPQUFPLE9BQU9ILFdBQVAsR0FBcUIsT0FBckIsR0FBK0JLLEtBQXRDLEVBQTZDLElBQTdDLENBREM7QUFFVkUsY0FBWSxJQUZGO0FBR1ZILFNBQU87QUFIRyxFQUFYOztBQU1BLEtBQUlJLFNBQVMsb0JBQWI7O0FBRUE7QUFDQSxLQUFJQyxRQUFRO0FBQ1hQLFdBQVNDLE9BQU8sT0FBT0gsV0FBUCxHQUFxQixPQUFyQixHQUErQkssS0FBL0IsR0FBdUMsU0FBdkMsR0FBbURHLE1BQTFELEVBQWtFLElBQWxFLENBREU7QUFFWEQsY0FBWSxJQUZEO0FBR1hILFNBQU87QUFISSxFQUFaOztBQU1BO0FBQ0EsS0FBSU0sTUFBTSxVQUFWOztBQUVBQyxPQUFNQyxTQUFOLENBQWdCQyxZQUFoQixDQUE2QixNQUE3QixFQUFxQyxTQUFyQyxFQUFnRDtBQUMvQyxjQUFZO0FBQ1hYLFlBQVMsbURBREU7QUFFWEssZUFBWSxJQUZEO0FBR1hILFVBQU8sU0FISTtBQUlYVSxXQUFROztBQUVQO0FBQ0Esd0JBQW9CO0FBQ25CWixjQUFTLHdDQURVO0FBRW5CWSxhQUFRO0FBQ1AsZUFBU0wsS0FERjtBQUVQLG9CQUFjUixVQUZQO0FBR1AsYUFBT1M7QUFIQTtBQUZXLEtBSGI7O0FBWVA7QUFDQSxxQ0FBaUM7QUFDaENSLGNBQVMsc0RBRHVCO0FBRWhDWSxhQUFRO0FBQ1AsaUJBQVc7QUFDVlosZ0JBQVNDLE9BQU8sT0FBT0gsV0FBUCxHQUFxQixPQUFyQixHQUErQkssS0FBL0IsR0FBdUMsUUFBdkMsR0FBa0RHLE1BQWxELEdBQTJELG9CQUFsRSxFQUF3RixJQUF4RixDQURDO0FBRVZELG1CQUFZLElBRkY7QUFHVkgsY0FBTztBQUhHLE9BREo7QUFNUCxlQUFTSyxLQU5GO0FBT1AsY0FBUUgsSUFQRDtBQVFQLG9CQUFjTCxVQVJQO0FBU1AsYUFBT1M7QUFUQTtBQUZ3QixLQWIxQjs7QUE0QlA7QUFDQSx1QkFBbUI7QUFDbEJSLGNBQVMsMkJBRFM7QUFFbEJZLGFBQVE7QUFDUCxjQUFRUixJQUREO0FBRVAsb0JBQWNMLFVBRlA7QUFHUCxhQUFPUztBQUhBO0FBRlUsS0E3Qlo7O0FBc0NQO0FBQ0Esc0JBQWtCO0FBQ2pCUixjQUFTLDhCQURRO0FBRWpCWSxhQUFRO0FBQ1Asb0JBQWNiLFVBRFA7QUFFUCxhQUFPO0FBRkE7QUFGUyxLQXZDWDs7QUErQ1A7QUFDQSw4QkFBMEI7QUFDekJDLGNBQVMsdUJBRGdCO0FBRXpCWSxhQUFRO0FBQ1Asb0JBQWNiLFVBRFA7QUFFUCxjQUFRO0FBQ1BDLGdCQUFTLElBREY7QUFFUEUsY0FBTztBQUZBO0FBRkQ7QUFGaUIsS0FoRG5COztBQTJEUDtBQUNBLCtCQUEyQjtBQUMxQkYsY0FBUyx3QkFEaUI7QUFFMUJZLGFBQVE7QUFDUCxvQkFBY2IsVUFEUDtBQUVQLGVBQVM7QUFDUkMsZ0JBQVMsSUFERDtBQUVSRSxjQUFPO0FBRkM7QUFGRjtBQUZrQixLQTVEcEI7O0FBdUVQO0FBQ0EseUJBQXFCO0FBQ3BCRixjQUFTLGlGQURXO0FBRXBCWSxhQUFRO0FBQ1Asb0JBQWNiLFVBRFA7QUFFUCxhQUFPUztBQUZBO0FBRlk7QUF4RWQ7QUFKRztBQURtQyxFQUFoRDtBQXVGQSxDQXJIQSxHQUFELEM7Ozs7Ozs7OztBQ0ZBOztBQUVDLGFBQVk7QUFDWixLQUFJVixjQUFjLE9BQWxCOztBQUVBO0FBQ0EsS0FBSUMsYUFBYTtBQUNoQkMsV0FBU0MsT0FBTyxNQUFNSCxXQUFiLEVBQTBCLElBQTFCLENBRE87QUFFaEJJLFNBQU87QUFGUyxFQUFqQjs7QUFLQSxLQUFJQyxRQUFRLFNBQVo7O0FBRUE7QUFDQSxLQUFJQyxPQUFPO0FBQ1ZKLFdBQVNDLE9BQU8sT0FBT0gsV0FBUCxHQUFxQixPQUFyQixHQUErQkssS0FBdEMsRUFBNkMsSUFBN0MsQ0FEQztBQUVWRSxjQUFZLElBRkY7QUFHVkgsU0FBTztBQUhHLEVBQVg7O0FBTUEsS0FBSUksU0FBUyxvQkFBYjs7QUFFQTtBQUNBLEtBQUlDLFFBQVE7QUFDWFAsV0FBU0MsT0FBTyxPQUFPSCxXQUFQLEdBQXFCLE9BQXJCLEdBQStCSyxLQUEvQixHQUF1QyxTQUF2QyxHQUFtREcsTUFBMUQsRUFBa0UsSUFBbEUsQ0FERTtBQUVYRCxjQUFZLElBRkQ7QUFHWEgsU0FBTztBQUhJLEVBQVo7O0FBTUE7QUFDQSxLQUFJTSxNQUFNLFVBQVY7O0FBRUFDLE9BQU1DLFNBQU4sQ0FBZ0JDLFlBQWhCLENBQTZCLFlBQTdCLEVBQTJDLFNBQTNDLEVBQXNEO0FBQ3JELGNBQVk7QUFDWFgsWUFBUyxtREFERTtBQUVYSyxlQUFZLElBRkQ7QUFHWEgsVUFBTyxTQUhJO0FBSVhVLFdBQVE7O0FBRVA7QUFDQSx3QkFBb0I7QUFDbkJaLGNBQVMsd0NBRFU7QUFFbkJZLGFBQVE7QUFDUCxlQUFTTCxLQURGO0FBRVAsb0JBQWNSLFVBRlA7QUFHUCxhQUFPUztBQUhBO0FBRlcsS0FIYjs7QUFZUDtBQUNBLHFDQUFpQztBQUNoQ1IsY0FBUyxzREFEdUI7QUFFaENZLGFBQVE7QUFDUCxpQkFBVztBQUNWWixnQkFBU0MsT0FBTyxPQUFPSCxXQUFQLEdBQXFCLE9BQXJCLEdBQStCSyxLQUEvQixHQUF1QyxRQUF2QyxHQUFrREcsTUFBbEQsR0FBMkQsb0JBQWxFLEVBQXdGLElBQXhGLENBREM7QUFFVkQsbUJBQVksSUFGRjtBQUdWSCxjQUFPO0FBSEcsT0FESjtBQU1QLGVBQVNLLEtBTkY7QUFPUCxjQUFRSCxJQVBEO0FBUVAsb0JBQWNMLFVBUlA7QUFTUCxhQUFPUztBQVRBO0FBRndCLEtBYjFCOztBQTRCUDtBQUNBLHVCQUFtQjtBQUNsQlIsY0FBUywyQkFEUztBQUVsQlksYUFBUTtBQUNQLGNBQVFSLElBREQ7QUFFUCxvQkFBY0wsVUFGUDtBQUdQLGFBQU9TO0FBSEE7QUFGVSxLQTdCWjs7QUFzQ1A7QUFDQSxzQkFBa0I7QUFDakJSLGNBQVMsOEJBRFE7QUFFakJZLGFBQVE7QUFDUCxvQkFBY2IsVUFEUDtBQUVQLGFBQU87QUFGQTtBQUZTLEtBdkNYOztBQStDUDtBQUNBLDhCQUEwQjtBQUN6QkMsY0FBUyx1QkFEZ0I7QUFFekJZLGFBQVE7QUFDUCxvQkFBY2IsVUFEUDtBQUVQLGNBQVE7QUFDUEMsZ0JBQVMsSUFERjtBQUVQRSxjQUFPO0FBRkE7QUFGRDtBQUZpQixLQWhEbkI7O0FBMkRQO0FBQ0EsK0JBQTJCO0FBQzFCRixjQUFTLHdCQURpQjtBQUUxQlksYUFBUTtBQUNQLG9CQUFjYixVQURQO0FBRVAsZUFBUztBQUNSQyxnQkFBUyxJQUREO0FBRVJFLGNBQU87QUFGQztBQUZGO0FBRmtCLEtBNURwQjs7QUF1RVA7QUFDQSx5QkFBcUI7QUFDcEJGLGNBQVMsK0hBRFc7QUFFcEJZLGFBQVE7QUFDUCxvQkFBY2IsVUFEUDtBQUVQLGFBQU9TO0FBRkE7QUFGWTtBQXhFZDtBQUpHO0FBRHlDLEVBQXREO0FBdUZBLENBckhBLEdBQUQsQzs7Ozs7O0FDRkE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxlQUFlLFlBQVksb0RBQW9EO0FBQy9FO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0EsZ0NBQWdDLFVBQVUsRUFBRTtBQUM1QyxDOzs7Ozs7QUN6QkE7QUFDQTs7O0FBR0E7QUFDQSxpRkFBa0YsbUJBQW1CLHNCQUFzQixxQkFBcUIsb0JBQW9CLHdDQUF3QyxtQkFBbUIscUJBQXFCLHFCQUFxQix5QkFBeUIsdUJBQXVCLHNCQUFzQix1QkFBdUIscUJBQXFCLG1CQUFtQixnQkFBZ0IsMEJBQTBCLHNCQUFzQixrQkFBa0Isd0JBQXdCLEVBQUUsc1hBQXNYLDJDQUEyQyxFQUFFLGtXQUFrVywyQ0FBMkMsRUFBRSwrQkFBK0Isd0JBQXdCLGtCQUFrQixtQkFBbUIsRUFBRSx3REFBd0QsaUVBQWlFLG9DQUFvQyxFQUFFLGtEQUFrRCxrQkFBa0IsbUJBQW1CLG9DQUFvQyxFQUFFLHdEQUF3RCw0Q0FBNEMsRUFBRSxxREFBcUQsaUJBQWlCLGdCQUFnQixpQkFBaUIsRUFBRSxvSEFBb0gsdUJBQXVCLGdDQUFnQyx5QkFBeUIsb0JBQW9CLGtDQUFrQywrQkFBK0IsOEJBQThCLDBCQUEwQixFQUFFLDREQUE0RCw2QkFBNkIsRUFBRSx1RUFBdUUsZ0NBQWdDLEVBQUUsNENBQTRDLHVCQUF1Qix3QkFBd0Isd0JBQXdCLEVBQUUsdUNBQXVDLGdCQUFnQixFQUFFLDZKQUE2SixnQkFBZ0Isd0JBQXdCLEVBQUUsdUZBQXVGLG1CQUFtQixFQUFFLHVDQUF1QyxtQkFBbUIsRUFBRSx5TUFBeU0sbUJBQW1CLEVBQUUsME5BQTBOLG1CQUFtQixFQUFFLDBOQUEwTixtQkFBbUIsRUFBRSw0Q0FBNEMsbUJBQW1CLEVBQUUsZ0RBQWdELHFCQUFxQixFQUFFLG1EQUFtRCxxQkFBcUIsRUFBRSx1REFBdUQscUJBQXFCLEVBQUUsc01BQXNNLG1CQUFtQixFQUFFLG1QQUFtUCxrQkFBa0IsRUFBRSxnTEFBZ0wsbUJBQW1CLEVBQUUsb0ZBQW9GLG1CQUFtQixFQUFFLGdSQUFnUixtQkFBbUIsRUFBRSwwSEFBMEgsbUJBQW1CLEVBQUUscUpBQXFKLGtCQUFrQixFQUFFLHlIQUF5SCxzQkFBc0IsRUFBRSwwQ0FBMEMsdUJBQXVCLEVBQUUsMENBQTBDLGlCQUFpQixFQUFFLDhDQUE4QyxtQkFBbUIsRUFBRSw0TEFBNEwsZ0JBQWdCLEVBQUUseUdBQXlHLGVBQWUsZ0JBQWdCLHFCQUFxQixzQkFBc0IsRUFBRSwrSEFBK0gsaUJBQWlCLGtCQUFrQixFQUFFLDBDQUEwQyxpQ0FBaUMscUJBQXFCLEVBQUUsRUFBRTs7QUFFanRNOzs7Ozs7O0FDUEE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSx3QkFBd0IsS0FBSztBQUM3QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxFQUFFOztBQUVGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsRUFBRTtBQUNGLENBQUMiLCJmaWxlIjoiYXN5bmMtbW9kdWxlcy9wcmlzbWpzLmpzP3Y9MzhkZTE4ODZhNTE2OGZkYzkwYWYiLCJzb3VyY2VzQ29udGVudCI6WyIndXNlIHN0cmljdCc7XHJcblxyXG4vKipcclxuICog0J/QvtC00LrQu9GO0YfQtdC90LjQtSDQv9C70LDQs9C40L3QsCBgcHJpc21qc2BcclxuICogQG1vZHVsZVxyXG4gKi9cclxuXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuLy8gSW1wb3J0c1xyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG5pbXBvcnQgUHJlbG9hZGVyIGZyb20gJy4uL1ByZWxvYWRlcic7XHJcblxyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbi8vIFB1YmxpY1xyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG4vKipcclxuICogQHBhcmFtIHtKUXVlcnkuU2VsZWN0b3J9IFtzZWxlY3Rvcj1cImNvZGVbY2xhc3MqPSdsYW5nJ11cIl0tINC/0L7QuNGB0Log0Y3Qu9C10LzQtdC90YLQvtCyINC/0L4g0YHQtdC70LXQutGC0L7RgNGDXHJcbiAqIEBwYXJhbSB7SlF1ZXJ5fSBbJGNvbnRleHQ9bnVsbF0gLSDQutC+0L3RgtC10LrRgdGCINCyINC60L7RgtC+0YDQvtC8INCx0YPQtNC10YIg0LLRi9C/0L7Qu9C90LXQvSDQv9C+0LjRgdC6INGB0LXQu9C10LrRgtC+0YDQsFxyXG4gKiBAc291cmNlQ29kZVxyXG4gKi9cclxuZnVuY3Rpb24gcHJpc20gKHNlbGVjdG9yID0gJ2NvZGVbY2xhc3MqPVwibGFuZ1wiXScsICRjb250ZXh0ID0gbnVsbCkge1xyXG5cdGxldCAkY29kZSA9IChzZWxlY3RvciAmJiBzZWxlY3Rvci5qcXVlcnkpID8gc2VsZWN0b3IgOiAkKHNlbGVjdG9yLCAkY29udGV4dCk7XHJcblx0aWYgKCRjb2RlLmxlbmd0aCkge1xyXG5cdFx0bGV0IHByZWxvYWRlciA9IG5ldyBQcmVsb2FkZXIoJGNvZGUucGFyZW50KCdwcmUnKSk7XHJcblx0XHRwcmVsb2FkZXIuc2hvdygpO1xyXG5cdFx0aW1wb3J0KFxyXG5cdFx0XHQvKiB3ZWJwYWNrQ2h1bmtOYW1lOiBcInByaXNtanNcIiAqL1xyXG5cdFx0XHQnLi9wcmlzbWpzLWltcG9ydCdcclxuXHRcdCkudGhlbigoKSA9PiB7XHJcblx0XHRcdHByZWxvYWRlci5oaWRlKCk7XHJcblx0XHR9KTtcclxuXHR9XHJcbn1cclxuXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuLy8gRXhwb3J0c1xyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG5leHBvcnQgZGVmYXVsdCBwcmlzbTtcclxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vX0hUTUwvc3JjL2pzL19tb2R1bGVzL3ByaXNtanMvcHJpc20uanMiLCIndXNlIHN0cmljdCc7XHJcblxyXG4vKipcclxuICog0JjQvNC/0L7RgNGCINC90YPQttC90YvRhSDQutC+0LzQv9C+0L3QtdC90YLQvtCyINC4INC/0LvQsNCz0LjQvdC+0LJcclxuICogQG1vZHVsZVxyXG4gKi9cclxuXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuLy8gSW1wb3J0c1xyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG5pbXBvcnQgJ2NsaXBib2FyZCc7XHJcbmltcG9ydCAncHJpc21qcyc7XHJcbmltcG9ydCAncHJpc21qcy9wbHVnaW5zL2tlZXAtbWFya3VwL3ByaXNtLWtlZXAtbWFya3VwJztcclxuaW1wb3J0ICdwcmlzbWpzL3BsdWdpbnMvdW5lc2NhcGVkLW1hcmt1cC9wcmlzbS11bmVzY2FwZWQtbWFya3VwJztcclxuaW1wb3J0ICdwcmlzbWpzL3BsdWdpbnMvbm9ybWFsaXplLXdoaXRlc3BhY2UvcHJpc20tbm9ybWFsaXplLXdoaXRlc3BhY2UnO1xyXG5pbXBvcnQgJ3ByaXNtanMvcGx1Z2lucy9oaWdobGlnaHQta2V5d29yZHMvcHJpc20taGlnaGxpZ2h0LWtleXdvcmRzJztcclxuaW1wb3J0ICdwcmlzbWpzL3BsdWdpbnMvdG9vbGJhci9wcmlzbS10b29sYmFyJztcclxuaW1wb3J0ICdwcmlzbWpzL3BsdWdpbnMvdG9vbGJhci9wcmlzbS10b29sYmFyLmNzcyc7XHJcbmltcG9ydCAncHJpc21qcy9wbHVnaW5zL3Nob3ctbGFuZ3VhZ2UvcHJpc20tc2hvdy1sYW5ndWFnZSc7XHJcbmltcG9ydCAncHJpc21qcy9wbHVnaW5zL2NvcHktdG8tY2xpcGJvYXJkL3ByaXNtLWNvcHktdG8tY2xpcGJvYXJkJztcclxuaW1wb3J0ICdwcmlzbWpzL3BsdWdpbnMvcHJldmlld2Vycy9wcmlzbS1wcmV2aWV3ZXJzJztcclxuaW1wb3J0ICdwcmlzbWpzL3BsdWdpbnMvcHJldmlld2Vycy9wcmlzbS1wcmV2aWV3ZXJzLmNzcyc7XHJcbmltcG9ydCAncHJpc21qcy9jb21wb25lbnRzL3ByaXNtLWNzcyc7XHJcbmltcG9ydCAncHJpc21qcy9jb21wb25lbnRzL3ByaXNtLWNzcy1leHRyYXMnO1xyXG5pbXBvcnQgJ3ByaXNtanMvY29tcG9uZW50cy9wcmlzbS1zYXNzJztcclxuaW1wb3J0ICdwcmlzbWpzL2NvbXBvbmVudHMvcHJpc20tc2Nzcyc7XHJcbmltcG9ydCAnLi9wcmlzbS1zY3NzLXNhc3Nkb2MnO1xyXG5pbXBvcnQgJy4vcHJpc20tc2Nzcy1qc2RvYyc7XHJcbmltcG9ydCAnLi9wcmlzbWpzLW1hdGVyaWFsLXRoZW1lLnNjc3MnO1xyXG5pbXBvcnQgJ3ByaXNtLWVqcy1sYW5ndWFnZSc7XHJcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL19IVE1ML3NyYy9qcy9fbW9kdWxlcy9wcmlzbWpzL3ByaXNtanMtaW1wb3J0LmpzIiwiKGZ1bmN0aW9uIChnbG9iYWwsIGZhY3RvcnkpIHtcbiAgICBpZiAodHlwZW9mIGRlZmluZSA9PT0gXCJmdW5jdGlvblwiICYmIGRlZmluZS5hbWQpIHtcbiAgICAgICAgZGVmaW5lKFsnbW9kdWxlJywgJy4vY2xpcGJvYXJkLWFjdGlvbicsICd0aW55LWVtaXR0ZXInLCAnZ29vZC1saXN0ZW5lciddLCBmYWN0b3J5KTtcbiAgICB9IGVsc2UgaWYgKHR5cGVvZiBleHBvcnRzICE9PSBcInVuZGVmaW5lZFwiKSB7XG4gICAgICAgIGZhY3RvcnkobW9kdWxlLCByZXF1aXJlKCcuL2NsaXBib2FyZC1hY3Rpb24nKSwgcmVxdWlyZSgndGlueS1lbWl0dGVyJyksIHJlcXVpcmUoJ2dvb2QtbGlzdGVuZXInKSk7XG4gICAgfSBlbHNlIHtcbiAgICAgICAgdmFyIG1vZCA9IHtcbiAgICAgICAgICAgIGV4cG9ydHM6IHt9XG4gICAgICAgIH07XG4gICAgICAgIGZhY3RvcnkobW9kLCBnbG9iYWwuY2xpcGJvYXJkQWN0aW9uLCBnbG9iYWwudGlueUVtaXR0ZXIsIGdsb2JhbC5nb29kTGlzdGVuZXIpO1xuICAgICAgICBnbG9iYWwuY2xpcGJvYXJkID0gbW9kLmV4cG9ydHM7XG4gICAgfVxufSkodGhpcywgZnVuY3Rpb24gKG1vZHVsZSwgX2NsaXBib2FyZEFjdGlvbiwgX3RpbnlFbWl0dGVyLCBfZ29vZExpc3RlbmVyKSB7XG4gICAgJ3VzZSBzdHJpY3QnO1xuXG4gICAgdmFyIF9jbGlwYm9hcmRBY3Rpb24yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY2xpcGJvYXJkQWN0aW9uKTtcblxuICAgIHZhciBfdGlueUVtaXR0ZXIyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfdGlueUVtaXR0ZXIpO1xuXG4gICAgdmFyIF9nb29kTGlzdGVuZXIyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZ29vZExpc3RlbmVyKTtcblxuICAgIGZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7XG4gICAgICAgIHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7XG4gICAgICAgICAgICBkZWZhdWx0OiBvYmpcbiAgICAgICAgfTtcbiAgICB9XG5cbiAgICB2YXIgX3R5cGVvZiA9IHR5cGVvZiBTeW1ib2wgPT09IFwiZnVuY3Rpb25cIiAmJiB0eXBlb2YgU3ltYm9sLml0ZXJhdG9yID09PSBcInN5bWJvbFwiID8gZnVuY3Rpb24gKG9iaikge1xuICAgICAgICByZXR1cm4gdHlwZW9mIG9iajtcbiAgICB9IDogZnVuY3Rpb24gKG9iaikge1xuICAgICAgICByZXR1cm4gb2JqICYmIHR5cGVvZiBTeW1ib2wgPT09IFwiZnVuY3Rpb25cIiAmJiBvYmouY29uc3RydWN0b3IgPT09IFN5bWJvbCAmJiBvYmogIT09IFN5bWJvbC5wcm90b3R5cGUgPyBcInN5bWJvbFwiIDogdHlwZW9mIG9iajtcbiAgICB9O1xuXG4gICAgZnVuY3Rpb24gX2NsYXNzQ2FsbENoZWNrKGluc3RhbmNlLCBDb25zdHJ1Y3Rvcikge1xuICAgICAgICBpZiAoIShpbnN0YW5jZSBpbnN0YW5jZW9mIENvbnN0cnVjdG9yKSkge1xuICAgICAgICAgICAgdGhyb3cgbmV3IFR5cGVFcnJvcihcIkNhbm5vdCBjYWxsIGEgY2xhc3MgYXMgYSBmdW5jdGlvblwiKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIHZhciBfY3JlYXRlQ2xhc3MgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgIGZ1bmN0aW9uIGRlZmluZVByb3BlcnRpZXModGFyZ2V0LCBwcm9wcykge1xuICAgICAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCBwcm9wcy5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgICAgIHZhciBkZXNjcmlwdG9yID0gcHJvcHNbaV07XG4gICAgICAgICAgICAgICAgZGVzY3JpcHRvci5lbnVtZXJhYmxlID0gZGVzY3JpcHRvci5lbnVtZXJhYmxlIHx8IGZhbHNlO1xuICAgICAgICAgICAgICAgIGRlc2NyaXB0b3IuY29uZmlndXJhYmxlID0gdHJ1ZTtcbiAgICAgICAgICAgICAgICBpZiAoXCJ2YWx1ZVwiIGluIGRlc2NyaXB0b3IpIGRlc2NyaXB0b3Iud3JpdGFibGUgPSB0cnVlO1xuICAgICAgICAgICAgICAgIE9iamVjdC5kZWZpbmVQcm9wZXJ0eSh0YXJnZXQsIGRlc2NyaXB0b3Iua2V5LCBkZXNjcmlwdG9yKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiBmdW5jdGlvbiAoQ29uc3RydWN0b3IsIHByb3RvUHJvcHMsIHN0YXRpY1Byb3BzKSB7XG4gICAgICAgICAgICBpZiAocHJvdG9Qcm9wcykgZGVmaW5lUHJvcGVydGllcyhDb25zdHJ1Y3Rvci5wcm90b3R5cGUsIHByb3RvUHJvcHMpO1xuICAgICAgICAgICAgaWYgKHN0YXRpY1Byb3BzKSBkZWZpbmVQcm9wZXJ0aWVzKENvbnN0cnVjdG9yLCBzdGF0aWNQcm9wcyk7XG4gICAgICAgICAgICByZXR1cm4gQ29uc3RydWN0b3I7XG4gICAgICAgIH07XG4gICAgfSgpO1xuXG4gICAgZnVuY3Rpb24gX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4oc2VsZiwgY2FsbCkge1xuICAgICAgICBpZiAoIXNlbGYpIHtcbiAgICAgICAgICAgIHRocm93IG5ldyBSZWZlcmVuY2VFcnJvcihcInRoaXMgaGFzbid0IGJlZW4gaW5pdGlhbGlzZWQgLSBzdXBlcigpIGhhc24ndCBiZWVuIGNhbGxlZFwiKTtcbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiBjYWxsICYmICh0eXBlb2YgY2FsbCA9PT0gXCJvYmplY3RcIiB8fCB0eXBlb2YgY2FsbCA9PT0gXCJmdW5jdGlvblwiKSA/IGNhbGwgOiBzZWxmO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIF9pbmhlcml0cyhzdWJDbGFzcywgc3VwZXJDbGFzcykge1xuICAgICAgICBpZiAodHlwZW9mIHN1cGVyQ2xhc3MgIT09IFwiZnVuY3Rpb25cIiAmJiBzdXBlckNsYXNzICE9PSBudWxsKSB7XG4gICAgICAgICAgICB0aHJvdyBuZXcgVHlwZUVycm9yKFwiU3VwZXIgZXhwcmVzc2lvbiBtdXN0IGVpdGhlciBiZSBudWxsIG9yIGEgZnVuY3Rpb24sIG5vdCBcIiArIHR5cGVvZiBzdXBlckNsYXNzKTtcbiAgICAgICAgfVxuXG4gICAgICAgIHN1YkNsYXNzLnByb3RvdHlwZSA9IE9iamVjdC5jcmVhdGUoc3VwZXJDbGFzcyAmJiBzdXBlckNsYXNzLnByb3RvdHlwZSwge1xuICAgICAgICAgICAgY29uc3RydWN0b3I6IHtcbiAgICAgICAgICAgICAgICB2YWx1ZTogc3ViQ2xhc3MsXG4gICAgICAgICAgICAgICAgZW51bWVyYWJsZTogZmFsc2UsXG4gICAgICAgICAgICAgICAgd3JpdGFibGU6IHRydWUsXG4gICAgICAgICAgICAgICAgY29uZmlndXJhYmxlOiB0cnVlXG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgICAgICBpZiAoc3VwZXJDbGFzcykgT2JqZWN0LnNldFByb3RvdHlwZU9mID8gT2JqZWN0LnNldFByb3RvdHlwZU9mKHN1YkNsYXNzLCBzdXBlckNsYXNzKSA6IHN1YkNsYXNzLl9fcHJvdG9fXyA9IHN1cGVyQ2xhc3M7XG4gICAgfVxuXG4gICAgdmFyIENsaXBib2FyZCA9IGZ1bmN0aW9uIChfRW1pdHRlcikge1xuICAgICAgICBfaW5oZXJpdHMoQ2xpcGJvYXJkLCBfRW1pdHRlcik7XG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIEBwYXJhbSB7U3RyaW5nfEhUTUxFbGVtZW50fEhUTUxDb2xsZWN0aW9ufE5vZGVMaXN0fSB0cmlnZ2VyXG4gICAgICAgICAqIEBwYXJhbSB7T2JqZWN0fSBvcHRpb25zXG4gICAgICAgICAqL1xuICAgICAgICBmdW5jdGlvbiBDbGlwYm9hcmQodHJpZ2dlciwgb3B0aW9ucykge1xuICAgICAgICAgICAgX2NsYXNzQ2FsbENoZWNrKHRoaXMsIENsaXBib2FyZCk7XG5cbiAgICAgICAgICAgIHZhciBfdGhpcyA9IF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuKHRoaXMsIChDbGlwYm9hcmQuX19wcm90b19fIHx8IE9iamVjdC5nZXRQcm90b3R5cGVPZihDbGlwYm9hcmQpKS5jYWxsKHRoaXMpKTtcblxuICAgICAgICAgICAgX3RoaXMucmVzb2x2ZU9wdGlvbnMob3B0aW9ucyk7XG4gICAgICAgICAgICBfdGhpcy5saXN0ZW5DbGljayh0cmlnZ2VyKTtcbiAgICAgICAgICAgIHJldHVybiBfdGhpcztcbiAgICAgICAgfVxuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiBEZWZpbmVzIGlmIGF0dHJpYnV0ZXMgd291bGQgYmUgcmVzb2x2ZWQgdXNpbmcgaW50ZXJuYWwgc2V0dGVyIGZ1bmN0aW9uc1xuICAgICAgICAgKiBvciBjdXN0b20gZnVuY3Rpb25zIHRoYXQgd2VyZSBwYXNzZWQgaW4gdGhlIGNvbnN0cnVjdG9yLlxuICAgICAgICAgKiBAcGFyYW0ge09iamVjdH0gb3B0aW9uc1xuICAgICAgICAgKi9cblxuXG4gICAgICAgIF9jcmVhdGVDbGFzcyhDbGlwYm9hcmQsIFt7XG4gICAgICAgICAgICBrZXk6ICdyZXNvbHZlT3B0aW9ucycsXG4gICAgICAgICAgICB2YWx1ZTogZnVuY3Rpb24gcmVzb2x2ZU9wdGlvbnMoKSB7XG4gICAgICAgICAgICAgICAgdmFyIG9wdGlvbnMgPSBhcmd1bWVudHMubGVuZ3RoID4gMCAmJiBhcmd1bWVudHNbMF0gIT09IHVuZGVmaW5lZCA/IGFyZ3VtZW50c1swXSA6IHt9O1xuXG4gICAgICAgICAgICAgICAgdGhpcy5hY3Rpb24gPSB0eXBlb2Ygb3B0aW9ucy5hY3Rpb24gPT09ICdmdW5jdGlvbicgPyBvcHRpb25zLmFjdGlvbiA6IHRoaXMuZGVmYXVsdEFjdGlvbjtcbiAgICAgICAgICAgICAgICB0aGlzLnRhcmdldCA9IHR5cGVvZiBvcHRpb25zLnRhcmdldCA9PT0gJ2Z1bmN0aW9uJyA/IG9wdGlvbnMudGFyZ2V0IDogdGhpcy5kZWZhdWx0VGFyZ2V0O1xuICAgICAgICAgICAgICAgIHRoaXMudGV4dCA9IHR5cGVvZiBvcHRpb25zLnRleHQgPT09ICdmdW5jdGlvbicgPyBvcHRpb25zLnRleHQgOiB0aGlzLmRlZmF1bHRUZXh0O1xuICAgICAgICAgICAgICAgIHRoaXMuY29udGFpbmVyID0gX3R5cGVvZihvcHRpb25zLmNvbnRhaW5lcikgPT09ICdvYmplY3QnID8gb3B0aW9ucy5jb250YWluZXIgOiBkb2N1bWVudC5ib2R5O1xuICAgICAgICAgICAgfVxuICAgICAgICB9LCB7XG4gICAgICAgICAgICBrZXk6ICdsaXN0ZW5DbGljaycsXG4gICAgICAgICAgICB2YWx1ZTogZnVuY3Rpb24gbGlzdGVuQ2xpY2sodHJpZ2dlcikge1xuICAgICAgICAgICAgICAgIHZhciBfdGhpczIgPSB0aGlzO1xuXG4gICAgICAgICAgICAgICAgdGhpcy5saXN0ZW5lciA9ICgwLCBfZ29vZExpc3RlbmVyMi5kZWZhdWx0KSh0cmlnZ2VyLCAnY2xpY2snLCBmdW5jdGlvbiAoZSkge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gX3RoaXMyLm9uQ2xpY2soZSk7XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0sIHtcbiAgICAgICAgICAgIGtleTogJ29uQ2xpY2snLFxuICAgICAgICAgICAgdmFsdWU6IGZ1bmN0aW9uIG9uQ2xpY2soZSkge1xuICAgICAgICAgICAgICAgIHZhciB0cmlnZ2VyID0gZS5kZWxlZ2F0ZVRhcmdldCB8fCBlLmN1cnJlbnRUYXJnZXQ7XG5cbiAgICAgICAgICAgICAgICBpZiAodGhpcy5jbGlwYm9hcmRBY3Rpb24pIHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5jbGlwYm9hcmRBY3Rpb24gPSBudWxsO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIHRoaXMuY2xpcGJvYXJkQWN0aW9uID0gbmV3IF9jbGlwYm9hcmRBY3Rpb24yLmRlZmF1bHQoe1xuICAgICAgICAgICAgICAgICAgICBhY3Rpb246IHRoaXMuYWN0aW9uKHRyaWdnZXIpLFxuICAgICAgICAgICAgICAgICAgICB0YXJnZXQ6IHRoaXMudGFyZ2V0KHRyaWdnZXIpLFxuICAgICAgICAgICAgICAgICAgICB0ZXh0OiB0aGlzLnRleHQodHJpZ2dlciksXG4gICAgICAgICAgICAgICAgICAgIGNvbnRhaW5lcjogdGhpcy5jb250YWluZXIsXG4gICAgICAgICAgICAgICAgICAgIHRyaWdnZXI6IHRyaWdnZXIsXG4gICAgICAgICAgICAgICAgICAgIGVtaXR0ZXI6IHRoaXNcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSwge1xuICAgICAgICAgICAga2V5OiAnZGVmYXVsdEFjdGlvbicsXG4gICAgICAgICAgICB2YWx1ZTogZnVuY3Rpb24gZGVmYXVsdEFjdGlvbih0cmlnZ2VyKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIGdldEF0dHJpYnV0ZVZhbHVlKCdhY3Rpb24nLCB0cmlnZ2VyKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSwge1xuICAgICAgICAgICAga2V5OiAnZGVmYXVsdFRhcmdldCcsXG4gICAgICAgICAgICB2YWx1ZTogZnVuY3Rpb24gZGVmYXVsdFRhcmdldCh0cmlnZ2VyKSB7XG4gICAgICAgICAgICAgICAgdmFyIHNlbGVjdG9yID0gZ2V0QXR0cmlidXRlVmFsdWUoJ3RhcmdldCcsIHRyaWdnZXIpO1xuXG4gICAgICAgICAgICAgICAgaWYgKHNlbGVjdG9yKSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKHNlbGVjdG9yKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH0sIHtcbiAgICAgICAgICAgIGtleTogJ2RlZmF1bHRUZXh0JyxcbiAgICAgICAgICAgIHZhbHVlOiBmdW5jdGlvbiBkZWZhdWx0VGV4dCh0cmlnZ2VyKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIGdldEF0dHJpYnV0ZVZhbHVlKCd0ZXh0JywgdHJpZ2dlcik7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0sIHtcbiAgICAgICAgICAgIGtleTogJ2Rlc3Ryb3knLFxuICAgICAgICAgICAgdmFsdWU6IGZ1bmN0aW9uIGRlc3Ryb3koKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5saXN0ZW5lci5kZXN0cm95KCk7XG5cbiAgICAgICAgICAgICAgICBpZiAodGhpcy5jbGlwYm9hcmRBY3Rpb24pIHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5jbGlwYm9hcmRBY3Rpb24uZGVzdHJveSgpO1xuICAgICAgICAgICAgICAgICAgICB0aGlzLmNsaXBib2FyZEFjdGlvbiA9IG51bGw7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9XSwgW3tcbiAgICAgICAgICAgIGtleTogJ2lzU3VwcG9ydGVkJyxcbiAgICAgICAgICAgIHZhbHVlOiBmdW5jdGlvbiBpc1N1cHBvcnRlZCgpIHtcbiAgICAgICAgICAgICAgICB2YXIgYWN0aW9uID0gYXJndW1lbnRzLmxlbmd0aCA+IDAgJiYgYXJndW1lbnRzWzBdICE9PSB1bmRlZmluZWQgPyBhcmd1bWVudHNbMF0gOiBbJ2NvcHknLCAnY3V0J107XG5cbiAgICAgICAgICAgICAgICB2YXIgYWN0aW9ucyA9IHR5cGVvZiBhY3Rpb24gPT09ICdzdHJpbmcnID8gW2FjdGlvbl0gOiBhY3Rpb247XG4gICAgICAgICAgICAgICAgdmFyIHN1cHBvcnQgPSAhIWRvY3VtZW50LnF1ZXJ5Q29tbWFuZFN1cHBvcnRlZDtcblxuICAgICAgICAgICAgICAgIGFjdGlvbnMuZm9yRWFjaChmdW5jdGlvbiAoYWN0aW9uKSB7XG4gICAgICAgICAgICAgICAgICAgIHN1cHBvcnQgPSBzdXBwb3J0ICYmICEhZG9jdW1lbnQucXVlcnlDb21tYW5kU3VwcG9ydGVkKGFjdGlvbik7XG4gICAgICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgICAgICByZXR1cm4gc3VwcG9ydDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfV0pO1xuXG4gICAgICAgIHJldHVybiBDbGlwYm9hcmQ7XG4gICAgfShfdGlueUVtaXR0ZXIyLmRlZmF1bHQpO1xuXG4gICAgLyoqXG4gICAgICogSGVscGVyIGZ1bmN0aW9uIHRvIHJldHJpZXZlIGF0dHJpYnV0ZSB2YWx1ZS5cbiAgICAgKiBAcGFyYW0ge1N0cmluZ30gc3VmZml4XG4gICAgICogQHBhcmFtIHtFbGVtZW50fSBlbGVtZW50XG4gICAgICovXG4gICAgZnVuY3Rpb24gZ2V0QXR0cmlidXRlVmFsdWUoc3VmZml4LCBlbGVtZW50KSB7XG4gICAgICAgIHZhciBhdHRyaWJ1dGUgPSAnZGF0YS1jbGlwYm9hcmQtJyArIHN1ZmZpeDtcblxuICAgICAgICBpZiAoIWVsZW1lbnQuaGFzQXR0cmlidXRlKGF0dHJpYnV0ZSkpIHtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiBlbGVtZW50LmdldEF0dHJpYnV0ZShhdHRyaWJ1dGUpO1xuICAgIH1cblxuICAgIG1vZHVsZS5leHBvcnRzID0gQ2xpcGJvYXJkO1xufSk7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvY2xpcGJvYXJkL2xpYi9jbGlwYm9hcmQuanNcbi8vIG1vZHVsZSBpZCA9IDEwOVxuLy8gbW9kdWxlIGNodW5rcyA9IDEwIiwiKGZ1bmN0aW9uIChnbG9iYWwsIGZhY3RvcnkpIHtcbiAgICBpZiAodHlwZW9mIGRlZmluZSA9PT0gXCJmdW5jdGlvblwiICYmIGRlZmluZS5hbWQpIHtcbiAgICAgICAgZGVmaW5lKFsnbW9kdWxlJywgJ3NlbGVjdCddLCBmYWN0b3J5KTtcbiAgICB9IGVsc2UgaWYgKHR5cGVvZiBleHBvcnRzICE9PSBcInVuZGVmaW5lZFwiKSB7XG4gICAgICAgIGZhY3RvcnkobW9kdWxlLCByZXF1aXJlKCdzZWxlY3QnKSk7XG4gICAgfSBlbHNlIHtcbiAgICAgICAgdmFyIG1vZCA9IHtcbiAgICAgICAgICAgIGV4cG9ydHM6IHt9XG4gICAgICAgIH07XG4gICAgICAgIGZhY3RvcnkobW9kLCBnbG9iYWwuc2VsZWN0KTtcbiAgICAgICAgZ2xvYmFsLmNsaXBib2FyZEFjdGlvbiA9IG1vZC5leHBvcnRzO1xuICAgIH1cbn0pKHRoaXMsIGZ1bmN0aW9uIChtb2R1bGUsIF9zZWxlY3QpIHtcbiAgICAndXNlIHN0cmljdCc7XG5cbiAgICB2YXIgX3NlbGVjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9zZWxlY3QpO1xuXG4gICAgZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHtcbiAgICAgICAgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHtcbiAgICAgICAgICAgIGRlZmF1bHQ6IG9ialxuICAgICAgICB9O1xuICAgIH1cblxuICAgIHZhciBfdHlwZW9mID0gdHlwZW9mIFN5bWJvbCA9PT0gXCJmdW5jdGlvblwiICYmIHR5cGVvZiBTeW1ib2wuaXRlcmF0b3IgPT09IFwic3ltYm9sXCIgPyBmdW5jdGlvbiAob2JqKSB7XG4gICAgICAgIHJldHVybiB0eXBlb2Ygb2JqO1xuICAgIH0gOiBmdW5jdGlvbiAob2JqKSB7XG4gICAgICAgIHJldHVybiBvYmogJiYgdHlwZW9mIFN5bWJvbCA9PT0gXCJmdW5jdGlvblwiICYmIG9iai5jb25zdHJ1Y3RvciA9PT0gU3ltYm9sICYmIG9iaiAhPT0gU3ltYm9sLnByb3RvdHlwZSA/IFwic3ltYm9sXCIgOiB0eXBlb2Ygb2JqO1xuICAgIH07XG5cbiAgICBmdW5jdGlvbiBfY2xhc3NDYWxsQ2hlY2soaW5zdGFuY2UsIENvbnN0cnVjdG9yKSB7XG4gICAgICAgIGlmICghKGluc3RhbmNlIGluc3RhbmNlb2YgQ29uc3RydWN0b3IpKSB7XG4gICAgICAgICAgICB0aHJvdyBuZXcgVHlwZUVycm9yKFwiQ2Fubm90IGNhbGwgYSBjbGFzcyBhcyBhIGZ1bmN0aW9uXCIpO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgdmFyIF9jcmVhdGVDbGFzcyA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgZnVuY3Rpb24gZGVmaW5lUHJvcGVydGllcyh0YXJnZXQsIHByb3BzKSB7XG4gICAgICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8IHByb3BzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICAgICAgdmFyIGRlc2NyaXB0b3IgPSBwcm9wc1tpXTtcbiAgICAgICAgICAgICAgICBkZXNjcmlwdG9yLmVudW1lcmFibGUgPSBkZXNjcmlwdG9yLmVudW1lcmFibGUgfHwgZmFsc2U7XG4gICAgICAgICAgICAgICAgZGVzY3JpcHRvci5jb25maWd1cmFibGUgPSB0cnVlO1xuICAgICAgICAgICAgICAgIGlmIChcInZhbHVlXCIgaW4gZGVzY3JpcHRvcikgZGVzY3JpcHRvci53cml0YWJsZSA9IHRydWU7XG4gICAgICAgICAgICAgICAgT2JqZWN0LmRlZmluZVByb3BlcnR5KHRhcmdldCwgZGVzY3JpcHRvci5rZXksIGRlc2NyaXB0b3IpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIGZ1bmN0aW9uIChDb25zdHJ1Y3RvciwgcHJvdG9Qcm9wcywgc3RhdGljUHJvcHMpIHtcbiAgICAgICAgICAgIGlmIChwcm90b1Byb3BzKSBkZWZpbmVQcm9wZXJ0aWVzKENvbnN0cnVjdG9yLnByb3RvdHlwZSwgcHJvdG9Qcm9wcyk7XG4gICAgICAgICAgICBpZiAoc3RhdGljUHJvcHMpIGRlZmluZVByb3BlcnRpZXMoQ29uc3RydWN0b3IsIHN0YXRpY1Byb3BzKTtcbiAgICAgICAgICAgIHJldHVybiBDb25zdHJ1Y3RvcjtcbiAgICAgICAgfTtcbiAgICB9KCk7XG5cbiAgICB2YXIgQ2xpcGJvYXJkQWN0aW9uID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAvKipcbiAgICAgICAgICogQHBhcmFtIHtPYmplY3R9IG9wdGlvbnNcbiAgICAgICAgICovXG4gICAgICAgIGZ1bmN0aW9uIENsaXBib2FyZEFjdGlvbihvcHRpb25zKSB7XG4gICAgICAgICAgICBfY2xhc3NDYWxsQ2hlY2sodGhpcywgQ2xpcGJvYXJkQWN0aW9uKTtcblxuICAgICAgICAgICAgdGhpcy5yZXNvbHZlT3B0aW9ucyhvcHRpb25zKTtcbiAgICAgICAgICAgIHRoaXMuaW5pdFNlbGVjdGlvbigpO1xuICAgICAgICB9XG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIERlZmluZXMgYmFzZSBwcm9wZXJ0aWVzIHBhc3NlZCBmcm9tIGNvbnN0cnVjdG9yLlxuICAgICAgICAgKiBAcGFyYW0ge09iamVjdH0gb3B0aW9uc1xuICAgICAgICAgKi9cblxuXG4gICAgICAgIF9jcmVhdGVDbGFzcyhDbGlwYm9hcmRBY3Rpb24sIFt7XG4gICAgICAgICAgICBrZXk6ICdyZXNvbHZlT3B0aW9ucycsXG4gICAgICAgICAgICB2YWx1ZTogZnVuY3Rpb24gcmVzb2x2ZU9wdGlvbnMoKSB7XG4gICAgICAgICAgICAgICAgdmFyIG9wdGlvbnMgPSBhcmd1bWVudHMubGVuZ3RoID4gMCAmJiBhcmd1bWVudHNbMF0gIT09IHVuZGVmaW5lZCA/IGFyZ3VtZW50c1swXSA6IHt9O1xuXG4gICAgICAgICAgICAgICAgdGhpcy5hY3Rpb24gPSBvcHRpb25zLmFjdGlvbjtcbiAgICAgICAgICAgICAgICB0aGlzLmNvbnRhaW5lciA9IG9wdGlvbnMuY29udGFpbmVyO1xuICAgICAgICAgICAgICAgIHRoaXMuZW1pdHRlciA9IG9wdGlvbnMuZW1pdHRlcjtcbiAgICAgICAgICAgICAgICB0aGlzLnRhcmdldCA9IG9wdGlvbnMudGFyZ2V0O1xuICAgICAgICAgICAgICAgIHRoaXMudGV4dCA9IG9wdGlvbnMudGV4dDtcbiAgICAgICAgICAgICAgICB0aGlzLnRyaWdnZXIgPSBvcHRpb25zLnRyaWdnZXI7XG5cbiAgICAgICAgICAgICAgICB0aGlzLnNlbGVjdGVkVGV4dCA9ICcnO1xuICAgICAgICAgICAgfVxuICAgICAgICB9LCB7XG4gICAgICAgICAgICBrZXk6ICdpbml0U2VsZWN0aW9uJyxcbiAgICAgICAgICAgIHZhbHVlOiBmdW5jdGlvbiBpbml0U2VsZWN0aW9uKCkge1xuICAgICAgICAgICAgICAgIGlmICh0aGlzLnRleHQpIHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zZWxlY3RGYWtlKCk7XG4gICAgICAgICAgICAgICAgfSBlbHNlIGlmICh0aGlzLnRhcmdldCkge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLnNlbGVjdFRhcmdldCgpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfSwge1xuICAgICAgICAgICAga2V5OiAnc2VsZWN0RmFrZScsXG4gICAgICAgICAgICB2YWx1ZTogZnVuY3Rpb24gc2VsZWN0RmFrZSgpIHtcbiAgICAgICAgICAgICAgICB2YXIgX3RoaXMgPSB0aGlzO1xuXG4gICAgICAgICAgICAgICAgdmFyIGlzUlRMID0gZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LmdldEF0dHJpYnV0ZSgnZGlyJykgPT0gJ3J0bCc7XG5cbiAgICAgICAgICAgICAgICB0aGlzLnJlbW92ZUZha2UoKTtcblxuICAgICAgICAgICAgICAgIHRoaXMuZmFrZUhhbmRsZXJDYWxsYmFjayA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIF90aGlzLnJlbW92ZUZha2UoKTtcbiAgICAgICAgICAgICAgICB9O1xuICAgICAgICAgICAgICAgIHRoaXMuZmFrZUhhbmRsZXIgPSB0aGlzLmNvbnRhaW5lci5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIHRoaXMuZmFrZUhhbmRsZXJDYWxsYmFjaykgfHwgdHJ1ZTtcblxuICAgICAgICAgICAgICAgIHRoaXMuZmFrZUVsZW0gPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCd0ZXh0YXJlYScpO1xuICAgICAgICAgICAgICAgIC8vIFByZXZlbnQgem9vbWluZyBvbiBpT1NcbiAgICAgICAgICAgICAgICB0aGlzLmZha2VFbGVtLnN0eWxlLmZvbnRTaXplID0gJzEycHQnO1xuICAgICAgICAgICAgICAgIC8vIFJlc2V0IGJveCBtb2RlbFxuICAgICAgICAgICAgICAgIHRoaXMuZmFrZUVsZW0uc3R5bGUuYm9yZGVyID0gJzAnO1xuICAgICAgICAgICAgICAgIHRoaXMuZmFrZUVsZW0uc3R5bGUucGFkZGluZyA9ICcwJztcbiAgICAgICAgICAgICAgICB0aGlzLmZha2VFbGVtLnN0eWxlLm1hcmdpbiA9ICcwJztcbiAgICAgICAgICAgICAgICAvLyBNb3ZlIGVsZW1lbnQgb3V0IG9mIHNjcmVlbiBob3Jpem9udGFsbHlcbiAgICAgICAgICAgICAgICB0aGlzLmZha2VFbGVtLnN0eWxlLnBvc2l0aW9uID0gJ2Fic29sdXRlJztcbiAgICAgICAgICAgICAgICB0aGlzLmZha2VFbGVtLnN0eWxlW2lzUlRMID8gJ3JpZ2h0JyA6ICdsZWZ0J10gPSAnLTk5OTlweCc7XG4gICAgICAgICAgICAgICAgLy8gTW92ZSBlbGVtZW50IHRvIHRoZSBzYW1lIHBvc2l0aW9uIHZlcnRpY2FsbHlcbiAgICAgICAgICAgICAgICB2YXIgeVBvc2l0aW9uID0gd2luZG93LnBhZ2VZT2Zmc2V0IHx8IGRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5zY3JvbGxUb3A7XG4gICAgICAgICAgICAgICAgdGhpcy5mYWtlRWxlbS5zdHlsZS50b3AgPSB5UG9zaXRpb24gKyAncHgnO1xuXG4gICAgICAgICAgICAgICAgdGhpcy5mYWtlRWxlbS5zZXRBdHRyaWJ1dGUoJ3JlYWRvbmx5JywgJycpO1xuICAgICAgICAgICAgICAgIHRoaXMuZmFrZUVsZW0udmFsdWUgPSB0aGlzLnRleHQ7XG5cbiAgICAgICAgICAgICAgICB0aGlzLmNvbnRhaW5lci5hcHBlbmRDaGlsZCh0aGlzLmZha2VFbGVtKTtcblxuICAgICAgICAgICAgICAgIHRoaXMuc2VsZWN0ZWRUZXh0ID0gKDAsIF9zZWxlY3QyLmRlZmF1bHQpKHRoaXMuZmFrZUVsZW0pO1xuICAgICAgICAgICAgICAgIHRoaXMuY29weVRleHQoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSwge1xuICAgICAgICAgICAga2V5OiAncmVtb3ZlRmFrZScsXG4gICAgICAgICAgICB2YWx1ZTogZnVuY3Rpb24gcmVtb3ZlRmFrZSgpIHtcbiAgICAgICAgICAgICAgICBpZiAodGhpcy5mYWtlSGFuZGxlcikge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLmNvbnRhaW5lci5yZW1vdmVFdmVudExpc3RlbmVyKCdjbGljaycsIHRoaXMuZmFrZUhhbmRsZXJDYWxsYmFjayk7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZmFrZUhhbmRsZXIgPSBudWxsO1xuICAgICAgICAgICAgICAgICAgICB0aGlzLmZha2VIYW5kbGVyQ2FsbGJhY2sgPSBudWxsO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIGlmICh0aGlzLmZha2VFbGVtKSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuY29udGFpbmVyLnJlbW92ZUNoaWxkKHRoaXMuZmFrZUVsZW0pO1xuICAgICAgICAgICAgICAgICAgICB0aGlzLmZha2VFbGVtID0gbnVsbDtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH0sIHtcbiAgICAgICAgICAgIGtleTogJ3NlbGVjdFRhcmdldCcsXG4gICAgICAgICAgICB2YWx1ZTogZnVuY3Rpb24gc2VsZWN0VGFyZ2V0KCkge1xuICAgICAgICAgICAgICAgIHRoaXMuc2VsZWN0ZWRUZXh0ID0gKDAsIF9zZWxlY3QyLmRlZmF1bHQpKHRoaXMudGFyZ2V0KTtcbiAgICAgICAgICAgICAgICB0aGlzLmNvcHlUZXh0KCk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0sIHtcbiAgICAgICAgICAgIGtleTogJ2NvcHlUZXh0JyxcbiAgICAgICAgICAgIHZhbHVlOiBmdW5jdGlvbiBjb3B5VGV4dCgpIHtcbiAgICAgICAgICAgICAgICB2YXIgc3VjY2VlZGVkID0gdm9pZCAwO1xuXG4gICAgICAgICAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgICAgICAgICAgc3VjY2VlZGVkID0gZG9jdW1lbnQuZXhlY0NvbW1hbmQodGhpcy5hY3Rpb24pO1xuICAgICAgICAgICAgICAgIH0gY2F0Y2ggKGVycikge1xuICAgICAgICAgICAgICAgICAgICBzdWNjZWVkZWQgPSBmYWxzZTtcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICB0aGlzLmhhbmRsZVJlc3VsdChzdWNjZWVkZWQpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9LCB7XG4gICAgICAgICAgICBrZXk6ICdoYW5kbGVSZXN1bHQnLFxuICAgICAgICAgICAgdmFsdWU6IGZ1bmN0aW9uIGhhbmRsZVJlc3VsdChzdWNjZWVkZWQpIHtcbiAgICAgICAgICAgICAgICB0aGlzLmVtaXR0ZXIuZW1pdChzdWNjZWVkZWQgPyAnc3VjY2VzcycgOiAnZXJyb3InLCB7XG4gICAgICAgICAgICAgICAgICAgIGFjdGlvbjogdGhpcy5hY3Rpb24sXG4gICAgICAgICAgICAgICAgICAgIHRleHQ6IHRoaXMuc2VsZWN0ZWRUZXh0LFxuICAgICAgICAgICAgICAgICAgICB0cmlnZ2VyOiB0aGlzLnRyaWdnZXIsXG4gICAgICAgICAgICAgICAgICAgIGNsZWFyU2VsZWN0aW9uOiB0aGlzLmNsZWFyU2VsZWN0aW9uLmJpbmQodGhpcylcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSwge1xuICAgICAgICAgICAga2V5OiAnY2xlYXJTZWxlY3Rpb24nLFxuICAgICAgICAgICAgdmFsdWU6IGZ1bmN0aW9uIGNsZWFyU2VsZWN0aW9uKCkge1xuICAgICAgICAgICAgICAgIGlmICh0aGlzLnRyaWdnZXIpIHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy50cmlnZ2VyLmZvY3VzKCk7XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgd2luZG93LmdldFNlbGVjdGlvbigpLnJlbW92ZUFsbFJhbmdlcygpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9LCB7XG4gICAgICAgICAgICBrZXk6ICdkZXN0cm95JyxcbiAgICAgICAgICAgIHZhbHVlOiBmdW5jdGlvbiBkZXN0cm95KCkge1xuICAgICAgICAgICAgICAgIHRoaXMucmVtb3ZlRmFrZSgpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9LCB7XG4gICAgICAgICAgICBrZXk6ICdhY3Rpb24nLFxuICAgICAgICAgICAgc2V0OiBmdW5jdGlvbiBzZXQoKSB7XG4gICAgICAgICAgICAgICAgdmFyIGFjdGlvbiA9IGFyZ3VtZW50cy5sZW5ndGggPiAwICYmIGFyZ3VtZW50c1swXSAhPT0gdW5kZWZpbmVkID8gYXJndW1lbnRzWzBdIDogJ2NvcHknO1xuXG4gICAgICAgICAgICAgICAgdGhpcy5fYWN0aW9uID0gYWN0aW9uO1xuXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMuX2FjdGlvbiAhPT0gJ2NvcHknICYmIHRoaXMuX2FjdGlvbiAhPT0gJ2N1dCcpIHtcbiAgICAgICAgICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKCdJbnZhbGlkIFwiYWN0aW9uXCIgdmFsdWUsIHVzZSBlaXRoZXIgXCJjb3B5XCIgb3IgXCJjdXRcIicpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5fYWN0aW9uO1xuICAgICAgICAgICAgfVxuICAgICAgICB9LCB7XG4gICAgICAgICAgICBrZXk6ICd0YXJnZXQnLFxuICAgICAgICAgICAgc2V0OiBmdW5jdGlvbiBzZXQodGFyZ2V0KSB7XG4gICAgICAgICAgICAgICAgaWYgKHRhcmdldCAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICAgICAgICAgIGlmICh0YXJnZXQgJiYgKHR5cGVvZiB0YXJnZXQgPT09ICd1bmRlZmluZWQnID8gJ3VuZGVmaW5lZCcgOiBfdHlwZW9mKHRhcmdldCkpID09PSAnb2JqZWN0JyAmJiB0YXJnZXQubm9kZVR5cGUgPT09IDEpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLmFjdGlvbiA9PT0gJ2NvcHknICYmIHRhcmdldC5oYXNBdHRyaWJ1dGUoJ2Rpc2FibGVkJykpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ0ludmFsaWQgXCJ0YXJnZXRcIiBhdHRyaWJ1dGUuIFBsZWFzZSB1c2UgXCJyZWFkb25seVwiIGluc3RlYWQgb2YgXCJkaXNhYmxlZFwiIGF0dHJpYnV0ZScpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy5hY3Rpb24gPT09ICdjdXQnICYmICh0YXJnZXQuaGFzQXR0cmlidXRlKCdyZWFkb25seScpIHx8IHRhcmdldC5oYXNBdHRyaWJ1dGUoJ2Rpc2FibGVkJykpKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKCdJbnZhbGlkIFwidGFyZ2V0XCIgYXR0cmlidXRlLiBZb3UgY2FuXFwndCBjdXQgdGV4dCBmcm9tIGVsZW1lbnRzIHdpdGggXCJyZWFkb25seVwiIG9yIFwiZGlzYWJsZWRcIiBhdHRyaWJ1dGVzJyk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuX3RhcmdldCA9IHRhcmdldDtcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcignSW52YWxpZCBcInRhcmdldFwiIHZhbHVlLCB1c2UgYSB2YWxpZCBFbGVtZW50Jyk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMuX3RhcmdldDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfV0pO1xuXG4gICAgICAgIHJldHVybiBDbGlwYm9hcmRBY3Rpb247XG4gICAgfSgpO1xuXG4gICAgbW9kdWxlLmV4cG9ydHMgPSBDbGlwYm9hcmRBY3Rpb247XG59KTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9jbGlwYm9hcmQvbGliL2NsaXBib2FyZC1hY3Rpb24uanNcbi8vIG1vZHVsZSBpZCA9IDExMFxuLy8gbW9kdWxlIGNodW5rcyA9IDEwIiwiZnVuY3Rpb24gc2VsZWN0KGVsZW1lbnQpIHtcbiAgICB2YXIgc2VsZWN0ZWRUZXh0O1xuXG4gICAgaWYgKGVsZW1lbnQubm9kZU5hbWUgPT09ICdTRUxFQ1QnKSB7XG4gICAgICAgIGVsZW1lbnQuZm9jdXMoKTtcblxuICAgICAgICBzZWxlY3RlZFRleHQgPSBlbGVtZW50LnZhbHVlO1xuICAgIH1cbiAgICBlbHNlIGlmIChlbGVtZW50Lm5vZGVOYW1lID09PSAnSU5QVVQnIHx8IGVsZW1lbnQubm9kZU5hbWUgPT09ICdURVhUQVJFQScpIHtcbiAgICAgICAgdmFyIGlzUmVhZE9ubHkgPSBlbGVtZW50Lmhhc0F0dHJpYnV0ZSgncmVhZG9ubHknKTtcblxuICAgICAgICBpZiAoIWlzUmVhZE9ubHkpIHtcbiAgICAgICAgICAgIGVsZW1lbnQuc2V0QXR0cmlidXRlKCdyZWFkb25seScsICcnKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGVsZW1lbnQuc2VsZWN0KCk7XG4gICAgICAgIGVsZW1lbnQuc2V0U2VsZWN0aW9uUmFuZ2UoMCwgZWxlbWVudC52YWx1ZS5sZW5ndGgpO1xuXG4gICAgICAgIGlmICghaXNSZWFkT25seSkge1xuICAgICAgICAgICAgZWxlbWVudC5yZW1vdmVBdHRyaWJ1dGUoJ3JlYWRvbmx5Jyk7XG4gICAgICAgIH1cblxuICAgICAgICBzZWxlY3RlZFRleHQgPSBlbGVtZW50LnZhbHVlO1xuICAgIH1cbiAgICBlbHNlIHtcbiAgICAgICAgaWYgKGVsZW1lbnQuaGFzQXR0cmlidXRlKCdjb250ZW50ZWRpdGFibGUnKSkge1xuICAgICAgICAgICAgZWxlbWVudC5mb2N1cygpO1xuICAgICAgICB9XG5cbiAgICAgICAgdmFyIHNlbGVjdGlvbiA9IHdpbmRvdy5nZXRTZWxlY3Rpb24oKTtcbiAgICAgICAgdmFyIHJhbmdlID0gZG9jdW1lbnQuY3JlYXRlUmFuZ2UoKTtcblxuICAgICAgICByYW5nZS5zZWxlY3ROb2RlQ29udGVudHMoZWxlbWVudCk7XG4gICAgICAgIHNlbGVjdGlvbi5yZW1vdmVBbGxSYW5nZXMoKTtcbiAgICAgICAgc2VsZWN0aW9uLmFkZFJhbmdlKHJhbmdlKTtcblxuICAgICAgICBzZWxlY3RlZFRleHQgPSBzZWxlY3Rpb24udG9TdHJpbmcoKTtcbiAgICB9XG5cbiAgICByZXR1cm4gc2VsZWN0ZWRUZXh0O1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IHNlbGVjdDtcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3NlbGVjdC9zcmMvc2VsZWN0LmpzXG4vLyBtb2R1bGUgaWQgPSAxMTFcbi8vIG1vZHVsZSBjaHVua3MgPSAxMCIsImZ1bmN0aW9uIEUgKCkge1xuICAvLyBLZWVwIHRoaXMgZW1wdHkgc28gaXQncyBlYXNpZXIgdG8gaW5oZXJpdCBmcm9tXG4gIC8vICh2aWEgaHR0cHM6Ly9naXRodWIuY29tL2xpcHNtYWNrIGZyb20gaHR0cHM6Ly9naXRodWIuY29tL3Njb3R0Y29yZ2FuL3RpbnktZW1pdHRlci9pc3N1ZXMvMylcbn1cblxuRS5wcm90b3R5cGUgPSB7XG4gIG9uOiBmdW5jdGlvbiAobmFtZSwgY2FsbGJhY2ssIGN0eCkge1xuICAgIHZhciBlID0gdGhpcy5lIHx8ICh0aGlzLmUgPSB7fSk7XG5cbiAgICAoZVtuYW1lXSB8fCAoZVtuYW1lXSA9IFtdKSkucHVzaCh7XG4gICAgICBmbjogY2FsbGJhY2ssXG4gICAgICBjdHg6IGN0eFxuICAgIH0pO1xuXG4gICAgcmV0dXJuIHRoaXM7XG4gIH0sXG5cbiAgb25jZTogZnVuY3Rpb24gKG5hbWUsIGNhbGxiYWNrLCBjdHgpIHtcbiAgICB2YXIgc2VsZiA9IHRoaXM7XG4gICAgZnVuY3Rpb24gbGlzdGVuZXIgKCkge1xuICAgICAgc2VsZi5vZmYobmFtZSwgbGlzdGVuZXIpO1xuICAgICAgY2FsbGJhY2suYXBwbHkoY3R4LCBhcmd1bWVudHMpO1xuICAgIH07XG5cbiAgICBsaXN0ZW5lci5fID0gY2FsbGJhY2tcbiAgICByZXR1cm4gdGhpcy5vbihuYW1lLCBsaXN0ZW5lciwgY3R4KTtcbiAgfSxcblxuICBlbWl0OiBmdW5jdGlvbiAobmFtZSkge1xuICAgIHZhciBkYXRhID0gW10uc2xpY2UuY2FsbChhcmd1bWVudHMsIDEpO1xuICAgIHZhciBldnRBcnIgPSAoKHRoaXMuZSB8fCAodGhpcy5lID0ge30pKVtuYW1lXSB8fCBbXSkuc2xpY2UoKTtcbiAgICB2YXIgaSA9IDA7XG4gICAgdmFyIGxlbiA9IGV2dEFyci5sZW5ndGg7XG5cbiAgICBmb3IgKGk7IGkgPCBsZW47IGkrKykge1xuICAgICAgZXZ0QXJyW2ldLmZuLmFwcGx5KGV2dEFycltpXS5jdHgsIGRhdGEpO1xuICAgIH1cblxuICAgIHJldHVybiB0aGlzO1xuICB9LFxuXG4gIG9mZjogZnVuY3Rpb24gKG5hbWUsIGNhbGxiYWNrKSB7XG4gICAgdmFyIGUgPSB0aGlzLmUgfHwgKHRoaXMuZSA9IHt9KTtcbiAgICB2YXIgZXZ0cyA9IGVbbmFtZV07XG4gICAgdmFyIGxpdmVFdmVudHMgPSBbXTtcblxuICAgIGlmIChldnRzICYmIGNhbGxiYWNrKSB7XG4gICAgICBmb3IgKHZhciBpID0gMCwgbGVuID0gZXZ0cy5sZW5ndGg7IGkgPCBsZW47IGkrKykge1xuICAgICAgICBpZiAoZXZ0c1tpXS5mbiAhPT0gY2FsbGJhY2sgJiYgZXZ0c1tpXS5mbi5fICE9PSBjYWxsYmFjaylcbiAgICAgICAgICBsaXZlRXZlbnRzLnB1c2goZXZ0c1tpXSk7XG4gICAgICB9XG4gICAgfVxuXG4gICAgLy8gUmVtb3ZlIGV2ZW50IGZyb20gcXVldWUgdG8gcHJldmVudCBtZW1vcnkgbGVha1xuICAgIC8vIFN1Z2dlc3RlZCBieSBodHRwczovL2dpdGh1Yi5jb20vbGF6ZFxuICAgIC8vIFJlZjogaHR0cHM6Ly9naXRodWIuY29tL3Njb3R0Y29yZ2FuL3RpbnktZW1pdHRlci9jb21taXQvYzZlYmZhYTliYzk3M2IzM2QxMTBhODRhMzA3NzQyYjdjZjk0Yzk1MyNjb21taXRjb21tZW50LTUwMjQ5MTBcblxuICAgIChsaXZlRXZlbnRzLmxlbmd0aClcbiAgICAgID8gZVtuYW1lXSA9IGxpdmVFdmVudHNcbiAgICAgIDogZGVsZXRlIGVbbmFtZV07XG5cbiAgICByZXR1cm4gdGhpcztcbiAgfVxufTtcblxubW9kdWxlLmV4cG9ydHMgPSBFO1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvdGlueS1lbWl0dGVyL2luZGV4LmpzXG4vLyBtb2R1bGUgaWQgPSAxMTJcbi8vIG1vZHVsZSBjaHVua3MgPSAxMCIsInZhciBpcyA9IHJlcXVpcmUoJy4vaXMnKTtcbnZhciBkZWxlZ2F0ZSA9IHJlcXVpcmUoJ2RlbGVnYXRlJyk7XG5cbi8qKlxuICogVmFsaWRhdGVzIGFsbCBwYXJhbXMgYW5kIGNhbGxzIHRoZSByaWdodFxuICogbGlzdGVuZXIgZnVuY3Rpb24gYmFzZWQgb24gaXRzIHRhcmdldCB0eXBlLlxuICpcbiAqIEBwYXJhbSB7U3RyaW5nfEhUTUxFbGVtZW50fEhUTUxDb2xsZWN0aW9ufE5vZGVMaXN0fSB0YXJnZXRcbiAqIEBwYXJhbSB7U3RyaW5nfSB0eXBlXG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSBjYWxsYmFja1xuICogQHJldHVybiB7T2JqZWN0fVxuICovXG5mdW5jdGlvbiBsaXN0ZW4odGFyZ2V0LCB0eXBlLCBjYWxsYmFjaykge1xuICAgIGlmICghdGFyZ2V0ICYmICF0eXBlICYmICFjYWxsYmFjaykge1xuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ01pc3NpbmcgcmVxdWlyZWQgYXJndW1lbnRzJyk7XG4gICAgfVxuXG4gICAgaWYgKCFpcy5zdHJpbmcodHlwZSkpIHtcbiAgICAgICAgdGhyb3cgbmV3IFR5cGVFcnJvcignU2Vjb25kIGFyZ3VtZW50IG11c3QgYmUgYSBTdHJpbmcnKTtcbiAgICB9XG5cbiAgICBpZiAoIWlzLmZuKGNhbGxiYWNrKSkge1xuICAgICAgICB0aHJvdyBuZXcgVHlwZUVycm9yKCdUaGlyZCBhcmd1bWVudCBtdXN0IGJlIGEgRnVuY3Rpb24nKTtcbiAgICB9XG5cbiAgICBpZiAoaXMubm9kZSh0YXJnZXQpKSB7XG4gICAgICAgIHJldHVybiBsaXN0ZW5Ob2RlKHRhcmdldCwgdHlwZSwgY2FsbGJhY2spO1xuICAgIH1cbiAgICBlbHNlIGlmIChpcy5ub2RlTGlzdCh0YXJnZXQpKSB7XG4gICAgICAgIHJldHVybiBsaXN0ZW5Ob2RlTGlzdCh0YXJnZXQsIHR5cGUsIGNhbGxiYWNrKTtcbiAgICB9XG4gICAgZWxzZSBpZiAoaXMuc3RyaW5nKHRhcmdldCkpIHtcbiAgICAgICAgcmV0dXJuIGxpc3RlblNlbGVjdG9yKHRhcmdldCwgdHlwZSwgY2FsbGJhY2spO1xuICAgIH1cbiAgICBlbHNlIHtcbiAgICAgICAgdGhyb3cgbmV3IFR5cGVFcnJvcignRmlyc3QgYXJndW1lbnQgbXVzdCBiZSBhIFN0cmluZywgSFRNTEVsZW1lbnQsIEhUTUxDb2xsZWN0aW9uLCBvciBOb2RlTGlzdCcpO1xuICAgIH1cbn1cblxuLyoqXG4gKiBBZGRzIGFuIGV2ZW50IGxpc3RlbmVyIHRvIGEgSFRNTCBlbGVtZW50XG4gKiBhbmQgcmV0dXJucyBhIHJlbW92ZSBsaXN0ZW5lciBmdW5jdGlvbi5cbiAqXG4gKiBAcGFyYW0ge0hUTUxFbGVtZW50fSBub2RlXG4gKiBAcGFyYW0ge1N0cmluZ30gdHlwZVxuICogQHBhcmFtIHtGdW5jdGlvbn0gY2FsbGJhY2tcbiAqIEByZXR1cm4ge09iamVjdH1cbiAqL1xuZnVuY3Rpb24gbGlzdGVuTm9kZShub2RlLCB0eXBlLCBjYWxsYmFjaykge1xuICAgIG5vZGUuYWRkRXZlbnRMaXN0ZW5lcih0eXBlLCBjYWxsYmFjayk7XG5cbiAgICByZXR1cm4ge1xuICAgICAgICBkZXN0cm95OiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIG5vZGUucmVtb3ZlRXZlbnRMaXN0ZW5lcih0eXBlLCBjYWxsYmFjayk7XG4gICAgICAgIH1cbiAgICB9XG59XG5cbi8qKlxuICogQWRkIGFuIGV2ZW50IGxpc3RlbmVyIHRvIGEgbGlzdCBvZiBIVE1MIGVsZW1lbnRzXG4gKiBhbmQgcmV0dXJucyBhIHJlbW92ZSBsaXN0ZW5lciBmdW5jdGlvbi5cbiAqXG4gKiBAcGFyYW0ge05vZGVMaXN0fEhUTUxDb2xsZWN0aW9ufSBub2RlTGlzdFxuICogQHBhcmFtIHtTdHJpbmd9IHR5cGVcbiAqIEBwYXJhbSB7RnVuY3Rpb259IGNhbGxiYWNrXG4gKiBAcmV0dXJuIHtPYmplY3R9XG4gKi9cbmZ1bmN0aW9uIGxpc3Rlbk5vZGVMaXN0KG5vZGVMaXN0LCB0eXBlLCBjYWxsYmFjaykge1xuICAgIEFycmF5LnByb3RvdHlwZS5mb3JFYWNoLmNhbGwobm9kZUxpc3QsIGZ1bmN0aW9uKG5vZGUpIHtcbiAgICAgICAgbm9kZS5hZGRFdmVudExpc3RlbmVyKHR5cGUsIGNhbGxiYWNrKTtcbiAgICB9KTtcblxuICAgIHJldHVybiB7XG4gICAgICAgIGRlc3Ryb3k6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgQXJyYXkucHJvdG90eXBlLmZvckVhY2guY2FsbChub2RlTGlzdCwgZnVuY3Rpb24obm9kZSkge1xuICAgICAgICAgICAgICAgIG5vZGUucmVtb3ZlRXZlbnRMaXN0ZW5lcih0eXBlLCBjYWxsYmFjayk7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfVxuICAgIH1cbn1cblxuLyoqXG4gKiBBZGQgYW4gZXZlbnQgbGlzdGVuZXIgdG8gYSBzZWxlY3RvclxuICogYW5kIHJldHVybnMgYSByZW1vdmUgbGlzdGVuZXIgZnVuY3Rpb24uXG4gKlxuICogQHBhcmFtIHtTdHJpbmd9IHNlbGVjdG9yXG4gKiBAcGFyYW0ge1N0cmluZ30gdHlwZVxuICogQHBhcmFtIHtGdW5jdGlvbn0gY2FsbGJhY2tcbiAqIEByZXR1cm4ge09iamVjdH1cbiAqL1xuZnVuY3Rpb24gbGlzdGVuU2VsZWN0b3Ioc2VsZWN0b3IsIHR5cGUsIGNhbGxiYWNrKSB7XG4gICAgcmV0dXJuIGRlbGVnYXRlKGRvY3VtZW50LmJvZHksIHNlbGVjdG9yLCB0eXBlLCBjYWxsYmFjayk7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gbGlzdGVuO1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvZ29vZC1saXN0ZW5lci9zcmMvbGlzdGVuLmpzXG4vLyBtb2R1bGUgaWQgPSAxMTNcbi8vIG1vZHVsZSBjaHVua3MgPSAxMCIsIi8qKlxuICogQ2hlY2sgaWYgYXJndW1lbnQgaXMgYSBIVE1MIGVsZW1lbnQuXG4gKlxuICogQHBhcmFtIHtPYmplY3R9IHZhbHVlXG4gKiBAcmV0dXJuIHtCb29sZWFufVxuICovXG5leHBvcnRzLm5vZGUgPSBmdW5jdGlvbih2YWx1ZSkge1xuICAgIHJldHVybiB2YWx1ZSAhPT0gdW5kZWZpbmVkXG4gICAgICAgICYmIHZhbHVlIGluc3RhbmNlb2YgSFRNTEVsZW1lbnRcbiAgICAgICAgJiYgdmFsdWUubm9kZVR5cGUgPT09IDE7XG59O1xuXG4vKipcbiAqIENoZWNrIGlmIGFyZ3VtZW50IGlzIGEgbGlzdCBvZiBIVE1MIGVsZW1lbnRzLlxuICpcbiAqIEBwYXJhbSB7T2JqZWN0fSB2YWx1ZVxuICogQHJldHVybiB7Qm9vbGVhbn1cbiAqL1xuZXhwb3J0cy5ub2RlTGlzdCA9IGZ1bmN0aW9uKHZhbHVlKSB7XG4gICAgdmFyIHR5cGUgPSBPYmplY3QucHJvdG90eXBlLnRvU3RyaW5nLmNhbGwodmFsdWUpO1xuXG4gICAgcmV0dXJuIHZhbHVlICE9PSB1bmRlZmluZWRcbiAgICAgICAgJiYgKHR5cGUgPT09ICdbb2JqZWN0IE5vZGVMaXN0XScgfHwgdHlwZSA9PT0gJ1tvYmplY3QgSFRNTENvbGxlY3Rpb25dJylcbiAgICAgICAgJiYgKCdsZW5ndGgnIGluIHZhbHVlKVxuICAgICAgICAmJiAodmFsdWUubGVuZ3RoID09PSAwIHx8IGV4cG9ydHMubm9kZSh2YWx1ZVswXSkpO1xufTtcblxuLyoqXG4gKiBDaGVjayBpZiBhcmd1bWVudCBpcyBhIHN0cmluZy5cbiAqXG4gKiBAcGFyYW0ge09iamVjdH0gdmFsdWVcbiAqIEByZXR1cm4ge0Jvb2xlYW59XG4gKi9cbmV4cG9ydHMuc3RyaW5nID0gZnVuY3Rpb24odmFsdWUpIHtcbiAgICByZXR1cm4gdHlwZW9mIHZhbHVlID09PSAnc3RyaW5nJ1xuICAgICAgICB8fCB2YWx1ZSBpbnN0YW5jZW9mIFN0cmluZztcbn07XG5cbi8qKlxuICogQ2hlY2sgaWYgYXJndW1lbnQgaXMgYSBmdW5jdGlvbi5cbiAqXG4gKiBAcGFyYW0ge09iamVjdH0gdmFsdWVcbiAqIEByZXR1cm4ge0Jvb2xlYW59XG4gKi9cbmV4cG9ydHMuZm4gPSBmdW5jdGlvbih2YWx1ZSkge1xuICAgIHZhciB0eXBlID0gT2JqZWN0LnByb3RvdHlwZS50b1N0cmluZy5jYWxsKHZhbHVlKTtcblxuICAgIHJldHVybiB0eXBlID09PSAnW29iamVjdCBGdW5jdGlvbl0nO1xufTtcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL2dvb2QtbGlzdGVuZXIvc3JjL2lzLmpzXG4vLyBtb2R1bGUgaWQgPSAxMTRcbi8vIG1vZHVsZSBjaHVua3MgPSAxMCIsInZhciBjbG9zZXN0ID0gcmVxdWlyZSgnLi9jbG9zZXN0Jyk7XG5cbi8qKlxuICogRGVsZWdhdGVzIGV2ZW50IHRvIGEgc2VsZWN0b3IuXG4gKlxuICogQHBhcmFtIHtFbGVtZW50fSBlbGVtZW50XG4gKiBAcGFyYW0ge1N0cmluZ30gc2VsZWN0b3JcbiAqIEBwYXJhbSB7U3RyaW5nfSB0eXBlXG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSBjYWxsYmFja1xuICogQHBhcmFtIHtCb29sZWFufSB1c2VDYXB0dXJlXG4gKiBAcmV0dXJuIHtPYmplY3R9XG4gKi9cbmZ1bmN0aW9uIF9kZWxlZ2F0ZShlbGVtZW50LCBzZWxlY3RvciwgdHlwZSwgY2FsbGJhY2ssIHVzZUNhcHR1cmUpIHtcbiAgICB2YXIgbGlzdGVuZXJGbiA9IGxpc3RlbmVyLmFwcGx5KHRoaXMsIGFyZ3VtZW50cyk7XG5cbiAgICBlbGVtZW50LmFkZEV2ZW50TGlzdGVuZXIodHlwZSwgbGlzdGVuZXJGbiwgdXNlQ2FwdHVyZSk7XG5cbiAgICByZXR1cm4ge1xuICAgICAgICBkZXN0cm95OiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIGVsZW1lbnQucmVtb3ZlRXZlbnRMaXN0ZW5lcih0eXBlLCBsaXN0ZW5lckZuLCB1c2VDYXB0dXJlKTtcbiAgICAgICAgfVxuICAgIH1cbn1cblxuLyoqXG4gKiBEZWxlZ2F0ZXMgZXZlbnQgdG8gYSBzZWxlY3Rvci5cbiAqXG4gKiBAcGFyYW0ge0VsZW1lbnR8U3RyaW5nfEFycmF5fSBbZWxlbWVudHNdXG4gKiBAcGFyYW0ge1N0cmluZ30gc2VsZWN0b3JcbiAqIEBwYXJhbSB7U3RyaW5nfSB0eXBlXG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSBjYWxsYmFja1xuICogQHBhcmFtIHtCb29sZWFufSB1c2VDYXB0dXJlXG4gKiBAcmV0dXJuIHtPYmplY3R9XG4gKi9cbmZ1bmN0aW9uIGRlbGVnYXRlKGVsZW1lbnRzLCBzZWxlY3RvciwgdHlwZSwgY2FsbGJhY2ssIHVzZUNhcHR1cmUpIHtcbiAgICAvLyBIYW5kbGUgdGhlIHJlZ3VsYXIgRWxlbWVudCB1c2FnZVxuICAgIGlmICh0eXBlb2YgZWxlbWVudHMuYWRkRXZlbnRMaXN0ZW5lciA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgICByZXR1cm4gX2RlbGVnYXRlLmFwcGx5KG51bGwsIGFyZ3VtZW50cyk7XG4gICAgfVxuXG4gICAgLy8gSGFuZGxlIEVsZW1lbnQtbGVzcyB1c2FnZSwgaXQgZGVmYXVsdHMgdG8gZ2xvYmFsIGRlbGVnYXRpb25cbiAgICBpZiAodHlwZW9mIHR5cGUgPT09ICdmdW5jdGlvbicpIHtcbiAgICAgICAgLy8gVXNlIGBkb2N1bWVudGAgYXMgdGhlIGZpcnN0IHBhcmFtZXRlciwgdGhlbiBhcHBseSBhcmd1bWVudHNcbiAgICAgICAgLy8gVGhpcyBpcyBhIHNob3J0IHdheSB0byAudW5zaGlmdCBgYXJndW1lbnRzYCB3aXRob3V0IHJ1bm5pbmcgaW50byBkZW9wdGltaXphdGlvbnNcbiAgICAgICAgcmV0dXJuIF9kZWxlZ2F0ZS5iaW5kKG51bGwsIGRvY3VtZW50KS5hcHBseShudWxsLCBhcmd1bWVudHMpO1xuICAgIH1cblxuICAgIC8vIEhhbmRsZSBTZWxlY3Rvci1iYXNlZCB1c2FnZVxuICAgIGlmICh0eXBlb2YgZWxlbWVudHMgPT09ICdzdHJpbmcnKSB7XG4gICAgICAgIGVsZW1lbnRzID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbChlbGVtZW50cyk7XG4gICAgfVxuXG4gICAgLy8gSGFuZGxlIEFycmF5LWxpa2UgYmFzZWQgdXNhZ2VcbiAgICByZXR1cm4gQXJyYXkucHJvdG90eXBlLm1hcC5jYWxsKGVsZW1lbnRzLCBmdW5jdGlvbiAoZWxlbWVudCkge1xuICAgICAgICByZXR1cm4gX2RlbGVnYXRlKGVsZW1lbnQsIHNlbGVjdG9yLCB0eXBlLCBjYWxsYmFjaywgdXNlQ2FwdHVyZSk7XG4gICAgfSk7XG59XG5cbi8qKlxuICogRmluZHMgY2xvc2VzdCBtYXRjaCBhbmQgaW52b2tlcyBjYWxsYmFjay5cbiAqXG4gKiBAcGFyYW0ge0VsZW1lbnR9IGVsZW1lbnRcbiAqIEBwYXJhbSB7U3RyaW5nfSBzZWxlY3RvclxuICogQHBhcmFtIHtTdHJpbmd9IHR5cGVcbiAqIEBwYXJhbSB7RnVuY3Rpb259IGNhbGxiYWNrXG4gKiBAcmV0dXJuIHtGdW5jdGlvbn1cbiAqL1xuZnVuY3Rpb24gbGlzdGVuZXIoZWxlbWVudCwgc2VsZWN0b3IsIHR5cGUsIGNhbGxiYWNrKSB7XG4gICAgcmV0dXJuIGZ1bmN0aW9uKGUpIHtcbiAgICAgICAgZS5kZWxlZ2F0ZVRhcmdldCA9IGNsb3Nlc3QoZS50YXJnZXQsIHNlbGVjdG9yKTtcblxuICAgICAgICBpZiAoZS5kZWxlZ2F0ZVRhcmdldCkge1xuICAgICAgICAgICAgY2FsbGJhY2suY2FsbChlbGVtZW50LCBlKTtcbiAgICAgICAgfVxuICAgIH1cbn1cblxubW9kdWxlLmV4cG9ydHMgPSBkZWxlZ2F0ZTtcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL2RlbGVnYXRlL3NyYy9kZWxlZ2F0ZS5qc1xuLy8gbW9kdWxlIGlkID0gMTE1XG4vLyBtb2R1bGUgY2h1bmtzID0gMTAiLCJ2YXIgRE9DVU1FTlRfTk9ERV9UWVBFID0gOTtcblxuLyoqXG4gKiBBIHBvbHlmaWxsIGZvciBFbGVtZW50Lm1hdGNoZXMoKVxuICovXG5pZiAodHlwZW9mIEVsZW1lbnQgIT09ICd1bmRlZmluZWQnICYmICFFbGVtZW50LnByb3RvdHlwZS5tYXRjaGVzKSB7XG4gICAgdmFyIHByb3RvID0gRWxlbWVudC5wcm90b3R5cGU7XG5cbiAgICBwcm90by5tYXRjaGVzID0gcHJvdG8ubWF0Y2hlc1NlbGVjdG9yIHx8XG4gICAgICAgICAgICAgICAgICAgIHByb3RvLm1vek1hdGNoZXNTZWxlY3RvciB8fFxuICAgICAgICAgICAgICAgICAgICBwcm90by5tc01hdGNoZXNTZWxlY3RvciB8fFxuICAgICAgICAgICAgICAgICAgICBwcm90by5vTWF0Y2hlc1NlbGVjdG9yIHx8XG4gICAgICAgICAgICAgICAgICAgIHByb3RvLndlYmtpdE1hdGNoZXNTZWxlY3Rvcjtcbn1cblxuLyoqXG4gKiBGaW5kcyB0aGUgY2xvc2VzdCBwYXJlbnQgdGhhdCBtYXRjaGVzIGEgc2VsZWN0b3IuXG4gKlxuICogQHBhcmFtIHtFbGVtZW50fSBlbGVtZW50XG4gKiBAcGFyYW0ge1N0cmluZ30gc2VsZWN0b3JcbiAqIEByZXR1cm4ge0Z1bmN0aW9ufVxuICovXG5mdW5jdGlvbiBjbG9zZXN0IChlbGVtZW50LCBzZWxlY3Rvcikge1xuICAgIHdoaWxlIChlbGVtZW50ICYmIGVsZW1lbnQubm9kZVR5cGUgIT09IERPQ1VNRU5UX05PREVfVFlQRSkge1xuICAgICAgICBpZiAodHlwZW9mIGVsZW1lbnQubWF0Y2hlcyA9PT0gJ2Z1bmN0aW9uJyAmJlxuICAgICAgICAgICAgZWxlbWVudC5tYXRjaGVzKHNlbGVjdG9yKSkge1xuICAgICAgICAgIHJldHVybiBlbGVtZW50O1xuICAgICAgICB9XG4gICAgICAgIGVsZW1lbnQgPSBlbGVtZW50LnBhcmVudE5vZGU7XG4gICAgfVxufVxuXG5tb2R1bGUuZXhwb3J0cyA9IGNsb3Nlc3Q7XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9kZWxlZ2F0ZS9zcmMvY2xvc2VzdC5qc1xuLy8gbW9kdWxlIGlkID0gMTE2XG4vLyBtb2R1bGUgY2h1bmtzID0gMTAiLCJcbi8qICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipcbiAgICAgQmVnaW4gcHJpc20tY29yZS5qc1xuKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiAqL1xuXG52YXIgX3NlbGYgPSAodHlwZW9mIHdpbmRvdyAhPT0gJ3VuZGVmaW5lZCcpXG5cdD8gd2luZG93ICAgLy8gaWYgaW4gYnJvd3NlclxuXHQ6IChcblx0XHQodHlwZW9mIFdvcmtlckdsb2JhbFNjb3BlICE9PSAndW5kZWZpbmVkJyAmJiBzZWxmIGluc3RhbmNlb2YgV29ya2VyR2xvYmFsU2NvcGUpXG5cdFx0PyBzZWxmIC8vIGlmIGluIHdvcmtlclxuXHRcdDoge30gICAvLyBpZiBpbiBub2RlIGpzXG5cdCk7XG5cbi8qKlxuICogUHJpc206IExpZ2h0d2VpZ2h0LCByb2J1c3QsIGVsZWdhbnQgc3ludGF4IGhpZ2hsaWdodGluZ1xuICogTUlUIGxpY2Vuc2UgaHR0cDovL3d3dy5vcGVuc291cmNlLm9yZy9saWNlbnNlcy9taXQtbGljZW5zZS5waHAvXG4gKiBAYXV0aG9yIExlYSBWZXJvdSBodHRwOi8vbGVhLnZlcm91Lm1lXG4gKi9cblxudmFyIFByaXNtID0gKGZ1bmN0aW9uKCl7XG5cbi8vIFByaXZhdGUgaGVscGVyIHZhcnNcbnZhciBsYW5nID0gL1xcYmxhbmcoPzp1YWdlKT8tKFxcdyspXFxiL2k7XG52YXIgdW5pcXVlSWQgPSAwO1xuXG52YXIgXyA9IF9zZWxmLlByaXNtID0ge1xuXHRtYW51YWw6IF9zZWxmLlByaXNtICYmIF9zZWxmLlByaXNtLm1hbnVhbCxcblx0ZGlzYWJsZVdvcmtlck1lc3NhZ2VIYW5kbGVyOiBfc2VsZi5QcmlzbSAmJiBfc2VsZi5QcmlzbS5kaXNhYmxlV29ya2VyTWVzc2FnZUhhbmRsZXIsXG5cdHV0aWw6IHtcblx0XHRlbmNvZGU6IGZ1bmN0aW9uICh0b2tlbnMpIHtcblx0XHRcdGlmICh0b2tlbnMgaW5zdGFuY2VvZiBUb2tlbikge1xuXHRcdFx0XHRyZXR1cm4gbmV3IFRva2VuKHRva2Vucy50eXBlLCBfLnV0aWwuZW5jb2RlKHRva2Vucy5jb250ZW50KSwgdG9rZW5zLmFsaWFzKTtcblx0XHRcdH0gZWxzZSBpZiAoXy51dGlsLnR5cGUodG9rZW5zKSA9PT0gJ0FycmF5Jykge1xuXHRcdFx0XHRyZXR1cm4gdG9rZW5zLm1hcChfLnV0aWwuZW5jb2RlKTtcblx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdHJldHVybiB0b2tlbnMucmVwbGFjZSgvJi9nLCAnJmFtcDsnKS5yZXBsYWNlKC88L2csICcmbHQ7JykucmVwbGFjZSgvXFx1MDBhMC9nLCAnICcpO1xuXHRcdFx0fVxuXHRcdH0sXG5cblx0XHR0eXBlOiBmdW5jdGlvbiAobykge1xuXHRcdFx0cmV0dXJuIE9iamVjdC5wcm90b3R5cGUudG9TdHJpbmcuY2FsbChvKS5tYXRjaCgvXFxbb2JqZWN0IChcXHcrKVxcXS8pWzFdO1xuXHRcdH0sXG5cblx0XHRvYmpJZDogZnVuY3Rpb24gKG9iaikge1xuXHRcdFx0aWYgKCFvYmpbJ19faWQnXSkge1xuXHRcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkob2JqLCAnX19pZCcsIHsgdmFsdWU6ICsrdW5pcXVlSWQgfSk7XG5cdFx0XHR9XG5cdFx0XHRyZXR1cm4gb2JqWydfX2lkJ107XG5cdFx0fSxcblxuXHRcdC8vIERlZXAgY2xvbmUgYSBsYW5ndWFnZSBkZWZpbml0aW9uIChlLmcuIHRvIGV4dGVuZCBpdClcblx0XHRjbG9uZTogZnVuY3Rpb24gKG8sIHZpc2l0ZWQpIHtcblx0XHRcdHZhciB0eXBlID0gXy51dGlsLnR5cGUobyk7XG5cdFx0XHR2aXNpdGVkID0gdmlzaXRlZCB8fCB7fTtcblxuXHRcdFx0c3dpdGNoICh0eXBlKSB7XG5cdFx0XHRcdGNhc2UgJ09iamVjdCc6XG5cdFx0XHRcdFx0aWYgKHZpc2l0ZWRbXy51dGlsLm9iaklkKG8pXSkge1xuXHRcdFx0XHRcdFx0cmV0dXJuIHZpc2l0ZWRbXy51dGlsLm9iaklkKG8pXTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0dmFyIGNsb25lID0ge307XG5cdFx0XHRcdFx0dmlzaXRlZFtfLnV0aWwub2JqSWQobyldID0gY2xvbmU7XG5cblx0XHRcdFx0XHRmb3IgKHZhciBrZXkgaW4gbykge1xuXHRcdFx0XHRcdFx0aWYgKG8uaGFzT3duUHJvcGVydHkoa2V5KSkge1xuXHRcdFx0XHRcdFx0XHRjbG9uZVtrZXldID0gXy51dGlsLmNsb25lKG9ba2V5XSwgdmlzaXRlZCk7XG5cdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0fVxuXG5cdFx0XHRcdFx0cmV0dXJuIGNsb25lO1xuXG5cdFx0XHRcdGNhc2UgJ0FycmF5Jzpcblx0XHRcdFx0XHRpZiAodmlzaXRlZFtfLnV0aWwub2JqSWQobyldKSB7XG5cdFx0XHRcdFx0XHRyZXR1cm4gdmlzaXRlZFtfLnV0aWwub2JqSWQobyldO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0XHR2YXIgY2xvbmUgPSBbXTtcblx0XHRcdFx0XHR2aXNpdGVkW18udXRpbC5vYmpJZChvKV0gPSBjbG9uZTtcblxuXHRcdFx0XHRcdG8uZm9yRWFjaChmdW5jdGlvbiAodiwgaSkge1xuXHRcdFx0XHRcdFx0Y2xvbmVbaV0gPSBfLnV0aWwuY2xvbmUodiwgdmlzaXRlZCk7XG5cdFx0XHRcdFx0fSk7XG5cblx0XHRcdFx0XHRyZXR1cm4gY2xvbmU7XG5cdFx0XHR9XG5cblx0XHRcdHJldHVybiBvO1xuXHRcdH1cblx0fSxcblxuXHRsYW5ndWFnZXM6IHtcblx0XHRleHRlbmQ6IGZ1bmN0aW9uIChpZCwgcmVkZWYpIHtcblx0XHRcdHZhciBsYW5nID0gXy51dGlsLmNsb25lKF8ubGFuZ3VhZ2VzW2lkXSk7XG5cblx0XHRcdGZvciAodmFyIGtleSBpbiByZWRlZikge1xuXHRcdFx0XHRsYW5nW2tleV0gPSByZWRlZltrZXldO1xuXHRcdFx0fVxuXG5cdFx0XHRyZXR1cm4gbGFuZztcblx0XHR9LFxuXG5cdFx0LyoqXG5cdFx0ICogSW5zZXJ0IGEgdG9rZW4gYmVmb3JlIGFub3RoZXIgdG9rZW4gaW4gYSBsYW5ndWFnZSBsaXRlcmFsXG5cdFx0ICogQXMgdGhpcyBuZWVkcyB0byByZWNyZWF0ZSB0aGUgb2JqZWN0ICh3ZSBjYW5ub3QgYWN0dWFsbHkgaW5zZXJ0IGJlZm9yZSBrZXlzIGluIG9iamVjdCBsaXRlcmFscyksXG5cdFx0ICogd2UgY2Fubm90IGp1c3QgcHJvdmlkZSBhbiBvYmplY3QsIHdlIG5lZWQgYW5vYmplY3QgYW5kIGEga2V5LlxuXHRcdCAqIEBwYXJhbSBpbnNpZGUgVGhlIGtleSAob3IgbGFuZ3VhZ2UgaWQpIG9mIHRoZSBwYXJlbnRcblx0XHQgKiBAcGFyYW0gYmVmb3JlIFRoZSBrZXkgdG8gaW5zZXJ0IGJlZm9yZS4gSWYgbm90IHByb3ZpZGVkLCB0aGUgZnVuY3Rpb24gYXBwZW5kcyBpbnN0ZWFkLlxuXHRcdCAqIEBwYXJhbSBpbnNlcnQgT2JqZWN0IHdpdGggdGhlIGtleS92YWx1ZSBwYWlycyB0byBpbnNlcnRcblx0XHQgKiBAcGFyYW0gcm9vdCBUaGUgb2JqZWN0IHRoYXQgY29udGFpbnMgYGluc2lkZWAuIElmIGVxdWFsIHRvIFByaXNtLmxhbmd1YWdlcywgaXQgY2FuIGJlIG9taXR0ZWQuXG5cdFx0ICovXG5cdFx0aW5zZXJ0QmVmb3JlOiBmdW5jdGlvbiAoaW5zaWRlLCBiZWZvcmUsIGluc2VydCwgcm9vdCkge1xuXHRcdFx0cm9vdCA9IHJvb3QgfHwgXy5sYW5ndWFnZXM7XG5cdFx0XHR2YXIgZ3JhbW1hciA9IHJvb3RbaW5zaWRlXTtcblxuXHRcdFx0aWYgKGFyZ3VtZW50cy5sZW5ndGggPT0gMikge1xuXHRcdFx0XHRpbnNlcnQgPSBhcmd1bWVudHNbMV07XG5cblx0XHRcdFx0Zm9yICh2YXIgbmV3VG9rZW4gaW4gaW5zZXJ0KSB7XG5cdFx0XHRcdFx0aWYgKGluc2VydC5oYXNPd25Qcm9wZXJ0eShuZXdUb2tlbikpIHtcblx0XHRcdFx0XHRcdGdyYW1tYXJbbmV3VG9rZW5dID0gaW5zZXJ0W25ld1Rva2VuXTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdH1cblxuXHRcdFx0XHRyZXR1cm4gZ3JhbW1hcjtcblx0XHRcdH1cblxuXHRcdFx0dmFyIHJldCA9IHt9O1xuXG5cdFx0XHRmb3IgKHZhciB0b2tlbiBpbiBncmFtbWFyKSB7XG5cblx0XHRcdFx0aWYgKGdyYW1tYXIuaGFzT3duUHJvcGVydHkodG9rZW4pKSB7XG5cblx0XHRcdFx0XHRpZiAodG9rZW4gPT0gYmVmb3JlKSB7XG5cblx0XHRcdFx0XHRcdGZvciAodmFyIG5ld1Rva2VuIGluIGluc2VydCkge1xuXG5cdFx0XHRcdFx0XHRcdGlmIChpbnNlcnQuaGFzT3duUHJvcGVydHkobmV3VG9rZW4pKSB7XG5cdFx0XHRcdFx0XHRcdFx0cmV0W25ld1Rva2VuXSA9IGluc2VydFtuZXdUb2tlbl07XG5cdFx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHR9XG5cblx0XHRcdFx0XHRyZXRbdG9rZW5dID0gZ3JhbW1hclt0b2tlbl07XG5cdFx0XHRcdH1cblx0XHRcdH1cblxuXHRcdFx0Ly8gVXBkYXRlIHJlZmVyZW5jZXMgaW4gb3RoZXIgbGFuZ3VhZ2UgZGVmaW5pdGlvbnNcblx0XHRcdF8ubGFuZ3VhZ2VzLkRGUyhfLmxhbmd1YWdlcywgZnVuY3Rpb24oa2V5LCB2YWx1ZSkge1xuXHRcdFx0XHRpZiAodmFsdWUgPT09IHJvb3RbaW5zaWRlXSAmJiBrZXkgIT0gaW5zaWRlKSB7XG5cdFx0XHRcdFx0dGhpc1trZXldID0gcmV0O1xuXHRcdFx0XHR9XG5cdFx0XHR9KTtcblxuXHRcdFx0cmV0dXJuIHJvb3RbaW5zaWRlXSA9IHJldDtcblx0XHR9LFxuXG5cdFx0Ly8gVHJhdmVyc2UgYSBsYW5ndWFnZSBkZWZpbml0aW9uIHdpdGggRGVwdGggRmlyc3QgU2VhcmNoXG5cdFx0REZTOiBmdW5jdGlvbihvLCBjYWxsYmFjaywgdHlwZSwgdmlzaXRlZCkge1xuXHRcdFx0dmlzaXRlZCA9IHZpc2l0ZWQgfHwge307XG5cdFx0XHRmb3IgKHZhciBpIGluIG8pIHtcblx0XHRcdFx0aWYgKG8uaGFzT3duUHJvcGVydHkoaSkpIHtcblx0XHRcdFx0XHRjYWxsYmFjay5jYWxsKG8sIGksIG9baV0sIHR5cGUgfHwgaSk7XG5cblx0XHRcdFx0XHRpZiAoXy51dGlsLnR5cGUob1tpXSkgPT09ICdPYmplY3QnICYmICF2aXNpdGVkW18udXRpbC5vYmpJZChvW2ldKV0pIHtcblx0XHRcdFx0XHRcdHZpc2l0ZWRbXy51dGlsLm9iaklkKG9baV0pXSA9IHRydWU7XG5cdFx0XHRcdFx0XHRfLmxhbmd1YWdlcy5ERlMob1tpXSwgY2FsbGJhY2ssIG51bGwsIHZpc2l0ZWQpO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0XHRlbHNlIGlmIChfLnV0aWwudHlwZShvW2ldKSA9PT0gJ0FycmF5JyAmJiAhdmlzaXRlZFtfLnV0aWwub2JqSWQob1tpXSldKSB7XG5cdFx0XHRcdFx0XHR2aXNpdGVkW18udXRpbC5vYmpJZChvW2ldKV0gPSB0cnVlO1xuXHRcdFx0XHRcdFx0Xy5sYW5ndWFnZXMuREZTKG9baV0sIGNhbGxiYWNrLCBpLCB2aXNpdGVkKTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdH1cblx0XHRcdH1cblx0XHR9XG5cdH0sXG5cdHBsdWdpbnM6IHt9LFxuXG5cdGhpZ2hsaWdodEFsbDogZnVuY3Rpb24oYXN5bmMsIGNhbGxiYWNrKSB7XG5cdFx0Xy5oaWdobGlnaHRBbGxVbmRlcihkb2N1bWVudCwgYXN5bmMsIGNhbGxiYWNrKTtcblx0fSxcblxuXHRoaWdobGlnaHRBbGxVbmRlcjogZnVuY3Rpb24oY29udGFpbmVyLCBhc3luYywgY2FsbGJhY2spIHtcblx0XHR2YXIgZW52ID0ge1xuXHRcdFx0Y2FsbGJhY2s6IGNhbGxiYWNrLFxuXHRcdFx0c2VsZWN0b3I6ICdjb2RlW2NsYXNzKj1cImxhbmd1YWdlLVwiXSwgW2NsYXNzKj1cImxhbmd1YWdlLVwiXSBjb2RlLCBjb2RlW2NsYXNzKj1cImxhbmctXCJdLCBbY2xhc3MqPVwibGFuZy1cIl0gY29kZSdcblx0XHR9O1xuXG5cdFx0Xy5ob29rcy5ydW4oXCJiZWZvcmUtaGlnaGxpZ2h0YWxsXCIsIGVudik7XG5cblx0XHR2YXIgZWxlbWVudHMgPSBlbnYuZWxlbWVudHMgfHwgY29udGFpbmVyLnF1ZXJ5U2VsZWN0b3JBbGwoZW52LnNlbGVjdG9yKTtcblxuXHRcdGZvciAodmFyIGk9MCwgZWxlbWVudDsgZWxlbWVudCA9IGVsZW1lbnRzW2krK107KSB7XG5cdFx0XHRfLmhpZ2hsaWdodEVsZW1lbnQoZWxlbWVudCwgYXN5bmMgPT09IHRydWUsIGVudi5jYWxsYmFjayk7XG5cdFx0fVxuXHR9LFxuXG5cdGhpZ2hsaWdodEVsZW1lbnQ6IGZ1bmN0aW9uKGVsZW1lbnQsIGFzeW5jLCBjYWxsYmFjaykge1xuXHRcdC8vIEZpbmQgbGFuZ3VhZ2Vcblx0XHR2YXIgbGFuZ3VhZ2UsIGdyYW1tYXIsIHBhcmVudCA9IGVsZW1lbnQ7XG5cblx0XHR3aGlsZSAocGFyZW50ICYmICFsYW5nLnRlc3QocGFyZW50LmNsYXNzTmFtZSkpIHtcblx0XHRcdHBhcmVudCA9IHBhcmVudC5wYXJlbnROb2RlO1xuXHRcdH1cblxuXHRcdGlmIChwYXJlbnQpIHtcblx0XHRcdGxhbmd1YWdlID0gKHBhcmVudC5jbGFzc05hbWUubWF0Y2gobGFuZykgfHwgWywnJ10pWzFdLnRvTG93ZXJDYXNlKCk7XG5cdFx0XHRncmFtbWFyID0gXy5sYW5ndWFnZXNbbGFuZ3VhZ2VdO1xuXHRcdH1cblxuXHRcdC8vIFNldCBsYW5ndWFnZSBvbiB0aGUgZWxlbWVudCwgaWYgbm90IHByZXNlbnRcblx0XHRlbGVtZW50LmNsYXNzTmFtZSA9IGVsZW1lbnQuY2xhc3NOYW1lLnJlcGxhY2UobGFuZywgJycpLnJlcGxhY2UoL1xccysvZywgJyAnKSArICcgbGFuZ3VhZ2UtJyArIGxhbmd1YWdlO1xuXG5cdFx0aWYgKGVsZW1lbnQucGFyZW50Tm9kZSkge1xuXHRcdFx0Ly8gU2V0IGxhbmd1YWdlIG9uIHRoZSBwYXJlbnQsIGZvciBzdHlsaW5nXG5cdFx0XHRwYXJlbnQgPSBlbGVtZW50LnBhcmVudE5vZGU7XG5cblx0XHRcdGlmICgvcHJlL2kudGVzdChwYXJlbnQubm9kZU5hbWUpKSB7XG5cdFx0XHRcdHBhcmVudC5jbGFzc05hbWUgPSBwYXJlbnQuY2xhc3NOYW1lLnJlcGxhY2UobGFuZywgJycpLnJlcGxhY2UoL1xccysvZywgJyAnKSArICcgbGFuZ3VhZ2UtJyArIGxhbmd1YWdlO1xuXHRcdFx0fVxuXHRcdH1cblxuXHRcdHZhciBjb2RlID0gZWxlbWVudC50ZXh0Q29udGVudDtcblxuXHRcdHZhciBlbnYgPSB7XG5cdFx0XHRlbGVtZW50OiBlbGVtZW50LFxuXHRcdFx0bGFuZ3VhZ2U6IGxhbmd1YWdlLFxuXHRcdFx0Z3JhbW1hcjogZ3JhbW1hcixcblx0XHRcdGNvZGU6IGNvZGVcblx0XHR9O1xuXG5cdFx0Xy5ob29rcy5ydW4oJ2JlZm9yZS1zYW5pdHktY2hlY2snLCBlbnYpO1xuXG5cdFx0aWYgKCFlbnYuY29kZSB8fCAhZW52LmdyYW1tYXIpIHtcblx0XHRcdGlmIChlbnYuY29kZSkge1xuXHRcdFx0XHRfLmhvb2tzLnJ1bignYmVmb3JlLWhpZ2hsaWdodCcsIGVudik7XG5cdFx0XHRcdGVudi5lbGVtZW50LnRleHRDb250ZW50ID0gZW52LmNvZGU7XG5cdFx0XHRcdF8uaG9va3MucnVuKCdhZnRlci1oaWdobGlnaHQnLCBlbnYpO1xuXHRcdFx0fVxuXHRcdFx0Xy5ob29rcy5ydW4oJ2NvbXBsZXRlJywgZW52KTtcblx0XHRcdHJldHVybjtcblx0XHR9XG5cblx0XHRfLmhvb2tzLnJ1bignYmVmb3JlLWhpZ2hsaWdodCcsIGVudik7XG5cblx0XHRpZiAoYXN5bmMgJiYgX3NlbGYuV29ya2VyKSB7XG5cdFx0XHR2YXIgd29ya2VyID0gbmV3IFdvcmtlcihfLmZpbGVuYW1lKTtcblxuXHRcdFx0d29ya2VyLm9ubWVzc2FnZSA9IGZ1bmN0aW9uKGV2dCkge1xuXHRcdFx0XHRlbnYuaGlnaGxpZ2h0ZWRDb2RlID0gZXZ0LmRhdGE7XG5cblx0XHRcdFx0Xy5ob29rcy5ydW4oJ2JlZm9yZS1pbnNlcnQnLCBlbnYpO1xuXG5cdFx0XHRcdGVudi5lbGVtZW50LmlubmVySFRNTCA9IGVudi5oaWdobGlnaHRlZENvZGU7XG5cblx0XHRcdFx0Y2FsbGJhY2sgJiYgY2FsbGJhY2suY2FsbChlbnYuZWxlbWVudCk7XG5cdFx0XHRcdF8uaG9va3MucnVuKCdhZnRlci1oaWdobGlnaHQnLCBlbnYpO1xuXHRcdFx0XHRfLmhvb2tzLnJ1bignY29tcGxldGUnLCBlbnYpO1xuXHRcdFx0fTtcblxuXHRcdFx0d29ya2VyLnBvc3RNZXNzYWdlKEpTT04uc3RyaW5naWZ5KHtcblx0XHRcdFx0bGFuZ3VhZ2U6IGVudi5sYW5ndWFnZSxcblx0XHRcdFx0Y29kZTogZW52LmNvZGUsXG5cdFx0XHRcdGltbWVkaWF0ZUNsb3NlOiB0cnVlXG5cdFx0XHR9KSk7XG5cdFx0fVxuXHRcdGVsc2Uge1xuXHRcdFx0ZW52LmhpZ2hsaWdodGVkQ29kZSA9IF8uaGlnaGxpZ2h0KGVudi5jb2RlLCBlbnYuZ3JhbW1hciwgZW52Lmxhbmd1YWdlKTtcblxuXHRcdFx0Xy5ob29rcy5ydW4oJ2JlZm9yZS1pbnNlcnQnLCBlbnYpO1xuXG5cdFx0XHRlbnYuZWxlbWVudC5pbm5lckhUTUwgPSBlbnYuaGlnaGxpZ2h0ZWRDb2RlO1xuXG5cdFx0XHRjYWxsYmFjayAmJiBjYWxsYmFjay5jYWxsKGVsZW1lbnQpO1xuXG5cdFx0XHRfLmhvb2tzLnJ1bignYWZ0ZXItaGlnaGxpZ2h0JywgZW52KTtcblx0XHRcdF8uaG9va3MucnVuKCdjb21wbGV0ZScsIGVudik7XG5cdFx0fVxuXHR9LFxuXG5cdGhpZ2hsaWdodDogZnVuY3Rpb24gKHRleHQsIGdyYW1tYXIsIGxhbmd1YWdlKSB7XG5cdFx0dmFyIGVudiA9IHtcblx0XHRcdHRleHQ6IHRleHQsXG5cdFx0XHRncmFtbWFyOiBncmFtbWFyLFxuXHRcdFx0bGFuZ3VhZ2U6IGxhbmd1YWdlXG5cdFx0fTtcblx0XHRlbnYudG9rZW5zID0gXy50b2tlbml6ZSh0ZXh0LCBncmFtbWFyKTtcblx0XHRfLmhvb2tzLnJ1bignYWZ0ZXItdG9rZW5pemUnLCBlbnYpO1xuXHRcdHJldHVybiBUb2tlbi5zdHJpbmdpZnkoXy51dGlsLmVuY29kZShlbnYudG9rZW5zKSwgbGFuZ3VhZ2UpO1xuXHR9LFxuXG5cdG1hdGNoR3JhbW1hcjogZnVuY3Rpb24gKHRleHQsIHN0cmFyciwgZ3JhbW1hciwgaW5kZXgsIHN0YXJ0UG9zLCBvbmVzaG90LCB0YXJnZXQpIHtcblx0XHR2YXIgVG9rZW4gPSBfLlRva2VuO1xuXG5cdFx0Zm9yICh2YXIgdG9rZW4gaW4gZ3JhbW1hcikge1xuXHRcdFx0aWYoIWdyYW1tYXIuaGFzT3duUHJvcGVydHkodG9rZW4pIHx8ICFncmFtbWFyW3Rva2VuXSkge1xuXHRcdFx0XHRjb250aW51ZTtcblx0XHRcdH1cblxuXHRcdFx0aWYgKHRva2VuID09IHRhcmdldCkge1xuXHRcdFx0XHRyZXR1cm47XG5cdFx0XHR9XG5cblx0XHRcdHZhciBwYXR0ZXJucyA9IGdyYW1tYXJbdG9rZW5dO1xuXHRcdFx0cGF0dGVybnMgPSAoXy51dGlsLnR5cGUocGF0dGVybnMpID09PSBcIkFycmF5XCIpID8gcGF0dGVybnMgOiBbcGF0dGVybnNdO1xuXG5cdFx0XHRmb3IgKHZhciBqID0gMDsgaiA8IHBhdHRlcm5zLmxlbmd0aDsgKytqKSB7XG5cdFx0XHRcdHZhciBwYXR0ZXJuID0gcGF0dGVybnNbal0sXG5cdFx0XHRcdFx0aW5zaWRlID0gcGF0dGVybi5pbnNpZGUsXG5cdFx0XHRcdFx0bG9va2JlaGluZCA9ICEhcGF0dGVybi5sb29rYmVoaW5kLFxuXHRcdFx0XHRcdGdyZWVkeSA9ICEhcGF0dGVybi5ncmVlZHksXG5cdFx0XHRcdFx0bG9va2JlaGluZExlbmd0aCA9IDAsXG5cdFx0XHRcdFx0YWxpYXMgPSBwYXR0ZXJuLmFsaWFzO1xuXG5cdFx0XHRcdGlmIChncmVlZHkgJiYgIXBhdHRlcm4ucGF0dGVybi5nbG9iYWwpIHtcblx0XHRcdFx0XHQvLyBXaXRob3V0IHRoZSBnbG9iYWwgZmxhZywgbGFzdEluZGV4IHdvbid0IHdvcmtcblx0XHRcdFx0XHR2YXIgZmxhZ3MgPSBwYXR0ZXJuLnBhdHRlcm4udG9TdHJpbmcoKS5tYXRjaCgvW2ltdXldKiQvKVswXTtcblx0XHRcdFx0XHRwYXR0ZXJuLnBhdHRlcm4gPSBSZWdFeHAocGF0dGVybi5wYXR0ZXJuLnNvdXJjZSwgZmxhZ3MgKyBcImdcIik7XG5cdFx0XHRcdH1cblxuXHRcdFx0XHRwYXR0ZXJuID0gcGF0dGVybi5wYXR0ZXJuIHx8IHBhdHRlcm47XG5cblx0XHRcdFx0Ly8gRG9u4oCZdCBjYWNoZSBsZW5ndGggYXMgaXQgY2hhbmdlcyBkdXJpbmcgdGhlIGxvb3Bcblx0XHRcdFx0Zm9yICh2YXIgaSA9IGluZGV4LCBwb3MgPSBzdGFydFBvczsgaSA8IHN0cmFyci5sZW5ndGg7IHBvcyArPSBzdHJhcnJbaV0ubGVuZ3RoLCArK2kpIHtcblxuXHRcdFx0XHRcdHZhciBzdHIgPSBzdHJhcnJbaV07XG5cblx0XHRcdFx0XHRpZiAoc3RyYXJyLmxlbmd0aCA+IHRleHQubGVuZ3RoKSB7XG5cdFx0XHRcdFx0XHQvLyBTb21ldGhpbmcgd2VudCB0ZXJyaWJseSB3cm9uZywgQUJPUlQsIEFCT1JUIVxuXHRcdFx0XHRcdFx0cmV0dXJuO1xuXHRcdFx0XHRcdH1cblxuXHRcdFx0XHRcdGlmIChzdHIgaW5zdGFuY2VvZiBUb2tlbikge1xuXHRcdFx0XHRcdFx0Y29udGludWU7XG5cdFx0XHRcdFx0fVxuXG5cdFx0XHRcdFx0cGF0dGVybi5sYXN0SW5kZXggPSAwO1xuXG5cdFx0XHRcdFx0dmFyIG1hdGNoID0gcGF0dGVybi5leGVjKHN0ciksXG5cdFx0XHRcdFx0ICAgIGRlbE51bSA9IDE7XG5cblx0XHRcdFx0XHQvLyBHcmVlZHkgcGF0dGVybnMgY2FuIG92ZXJyaWRlL3JlbW92ZSB1cCB0byB0d28gcHJldmlvdXNseSBtYXRjaGVkIHRva2Vuc1xuXHRcdFx0XHRcdGlmICghbWF0Y2ggJiYgZ3JlZWR5ICYmIGkgIT0gc3RyYXJyLmxlbmd0aCAtIDEpIHtcblx0XHRcdFx0XHRcdHBhdHRlcm4ubGFzdEluZGV4ID0gcG9zO1xuXHRcdFx0XHRcdFx0bWF0Y2ggPSBwYXR0ZXJuLmV4ZWModGV4dCk7XG5cdFx0XHRcdFx0XHRpZiAoIW1hdGNoKSB7XG5cdFx0XHRcdFx0XHRcdGJyZWFrO1xuXHRcdFx0XHRcdFx0fVxuXG5cdFx0XHRcdFx0XHR2YXIgZnJvbSA9IG1hdGNoLmluZGV4ICsgKGxvb2tiZWhpbmQgPyBtYXRjaFsxXS5sZW5ndGggOiAwKSxcblx0XHRcdFx0XHRcdCAgICB0byA9IG1hdGNoLmluZGV4ICsgbWF0Y2hbMF0ubGVuZ3RoLFxuXHRcdFx0XHRcdFx0ICAgIGsgPSBpLFxuXHRcdFx0XHRcdFx0ICAgIHAgPSBwb3M7XG5cblx0XHRcdFx0XHRcdGZvciAodmFyIGxlbiA9IHN0cmFyci5sZW5ndGg7IGsgPCBsZW4gJiYgKHAgPCB0byB8fCAoIXN0cmFycltrXS50eXBlICYmICFzdHJhcnJbayAtIDFdLmdyZWVkeSkpOyArK2spIHtcblx0XHRcdFx0XHRcdFx0cCArPSBzdHJhcnJba10ubGVuZ3RoO1xuXHRcdFx0XHRcdFx0XHQvLyBNb3ZlIHRoZSBpbmRleCBpIHRvIHRoZSBlbGVtZW50IGluIHN0cmFyciB0aGF0IGlzIGNsb3Nlc3QgdG8gZnJvbVxuXHRcdFx0XHRcdFx0XHRpZiAoZnJvbSA+PSBwKSB7XG5cdFx0XHRcdFx0XHRcdFx0KytpO1xuXHRcdFx0XHRcdFx0XHRcdHBvcyA9IHA7XG5cdFx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHRcdH1cblxuXHRcdFx0XHRcdFx0Lypcblx0XHRcdFx0XHRcdCAqIElmIHN0cmFycltpXSBpcyBhIFRva2VuLCB0aGVuIHRoZSBtYXRjaCBzdGFydHMgaW5zaWRlIGFub3RoZXIgVG9rZW4sIHdoaWNoIGlzIGludmFsaWRcblx0XHRcdFx0XHRcdCAqIElmIHN0cmFycltrIC0gMV0gaXMgZ3JlZWR5IHdlIGFyZSBpbiBjb25mbGljdCB3aXRoIGFub3RoZXIgZ3JlZWR5IHBhdHRlcm5cblx0XHRcdFx0XHRcdCAqL1xuXHRcdFx0XHRcdFx0aWYgKHN0cmFycltpXSBpbnN0YW5jZW9mIFRva2VuIHx8IHN0cmFycltrIC0gMV0uZ3JlZWR5KSB7XG5cdFx0XHRcdFx0XHRcdGNvbnRpbnVlO1xuXHRcdFx0XHRcdFx0fVxuXG5cdFx0XHRcdFx0XHQvLyBOdW1iZXIgb2YgdG9rZW5zIHRvIGRlbGV0ZSBhbmQgcmVwbGFjZSB3aXRoIHRoZSBuZXcgbWF0Y2hcblx0XHRcdFx0XHRcdGRlbE51bSA9IGsgLSBpO1xuXHRcdFx0XHRcdFx0c3RyID0gdGV4dC5zbGljZShwb3MsIHApO1xuXHRcdFx0XHRcdFx0bWF0Y2guaW5kZXggLT0gcG9zO1xuXHRcdFx0XHRcdH1cblxuXHRcdFx0XHRcdGlmICghbWF0Y2gpIHtcblx0XHRcdFx0XHRcdGlmIChvbmVzaG90KSB7XG5cdFx0XHRcdFx0XHRcdGJyZWFrO1xuXHRcdFx0XHRcdFx0fVxuXG5cdFx0XHRcdFx0XHRjb250aW51ZTtcblx0XHRcdFx0XHR9XG5cblx0XHRcdFx0XHRpZihsb29rYmVoaW5kKSB7XG5cdFx0XHRcdFx0XHRsb29rYmVoaW5kTGVuZ3RoID0gbWF0Y2hbMV0gPyBtYXRjaFsxXS5sZW5ndGggOiAwO1xuXHRcdFx0XHRcdH1cblxuXHRcdFx0XHRcdHZhciBmcm9tID0gbWF0Y2guaW5kZXggKyBsb29rYmVoaW5kTGVuZ3RoLFxuXHRcdFx0XHRcdCAgICBtYXRjaCA9IG1hdGNoWzBdLnNsaWNlKGxvb2tiZWhpbmRMZW5ndGgpLFxuXHRcdFx0XHRcdCAgICB0byA9IGZyb20gKyBtYXRjaC5sZW5ndGgsXG5cdFx0XHRcdFx0ICAgIGJlZm9yZSA9IHN0ci5zbGljZSgwLCBmcm9tKSxcblx0XHRcdFx0XHQgICAgYWZ0ZXIgPSBzdHIuc2xpY2UodG8pO1xuXG5cdFx0XHRcdFx0dmFyIGFyZ3MgPSBbaSwgZGVsTnVtXTtcblxuXHRcdFx0XHRcdGlmIChiZWZvcmUpIHtcblx0XHRcdFx0XHRcdCsraTtcblx0XHRcdFx0XHRcdHBvcyArPSBiZWZvcmUubGVuZ3RoO1xuXHRcdFx0XHRcdFx0YXJncy5wdXNoKGJlZm9yZSk7XG5cdFx0XHRcdFx0fVxuXG5cdFx0XHRcdFx0dmFyIHdyYXBwZWQgPSBuZXcgVG9rZW4odG9rZW4sIGluc2lkZT8gXy50b2tlbml6ZShtYXRjaCwgaW5zaWRlKSA6IG1hdGNoLCBhbGlhcywgbWF0Y2gsIGdyZWVkeSk7XG5cblx0XHRcdFx0XHRhcmdzLnB1c2god3JhcHBlZCk7XG5cblx0XHRcdFx0XHRpZiAoYWZ0ZXIpIHtcblx0XHRcdFx0XHRcdGFyZ3MucHVzaChhZnRlcik7XG5cdFx0XHRcdFx0fVxuXG5cdFx0XHRcdFx0QXJyYXkucHJvdG90eXBlLnNwbGljZS5hcHBseShzdHJhcnIsIGFyZ3MpO1xuXG5cdFx0XHRcdFx0aWYgKGRlbE51bSAhPSAxKVxuXHRcdFx0XHRcdFx0Xy5tYXRjaEdyYW1tYXIodGV4dCwgc3RyYXJyLCBncmFtbWFyLCBpLCBwb3MsIHRydWUsIHRva2VuKTtcblxuXHRcdFx0XHRcdGlmIChvbmVzaG90KVxuXHRcdFx0XHRcdFx0YnJlYWs7XG5cdFx0XHRcdH1cblx0XHRcdH1cblx0XHR9XG5cdH0sXG5cblx0dG9rZW5pemU6IGZ1bmN0aW9uKHRleHQsIGdyYW1tYXIsIGxhbmd1YWdlKSB7XG5cdFx0dmFyIHN0cmFyciA9IFt0ZXh0XTtcblxuXHRcdHZhciByZXN0ID0gZ3JhbW1hci5yZXN0O1xuXG5cdFx0aWYgKHJlc3QpIHtcblx0XHRcdGZvciAodmFyIHRva2VuIGluIHJlc3QpIHtcblx0XHRcdFx0Z3JhbW1hclt0b2tlbl0gPSByZXN0W3Rva2VuXTtcblx0XHRcdH1cblxuXHRcdFx0ZGVsZXRlIGdyYW1tYXIucmVzdDtcblx0XHR9XG5cblx0XHRfLm1hdGNoR3JhbW1hcih0ZXh0LCBzdHJhcnIsIGdyYW1tYXIsIDAsIDAsIGZhbHNlKTtcblxuXHRcdHJldHVybiBzdHJhcnI7XG5cdH0sXG5cblx0aG9va3M6IHtcblx0XHRhbGw6IHt9LFxuXG5cdFx0YWRkOiBmdW5jdGlvbiAobmFtZSwgY2FsbGJhY2spIHtcblx0XHRcdHZhciBob29rcyA9IF8uaG9va3MuYWxsO1xuXG5cdFx0XHRob29rc1tuYW1lXSA9IGhvb2tzW25hbWVdIHx8IFtdO1xuXG5cdFx0XHRob29rc1tuYW1lXS5wdXNoKGNhbGxiYWNrKTtcblx0XHR9LFxuXG5cdFx0cnVuOiBmdW5jdGlvbiAobmFtZSwgZW52KSB7XG5cdFx0XHR2YXIgY2FsbGJhY2tzID0gXy5ob29rcy5hbGxbbmFtZV07XG5cblx0XHRcdGlmICghY2FsbGJhY2tzIHx8ICFjYWxsYmFja3MubGVuZ3RoKSB7XG5cdFx0XHRcdHJldHVybjtcblx0XHRcdH1cblxuXHRcdFx0Zm9yICh2YXIgaT0wLCBjYWxsYmFjazsgY2FsbGJhY2sgPSBjYWxsYmFja3NbaSsrXTspIHtcblx0XHRcdFx0Y2FsbGJhY2soZW52KTtcblx0XHRcdH1cblx0XHR9XG5cdH1cbn07XG5cbnZhciBUb2tlbiA9IF8uVG9rZW4gPSBmdW5jdGlvbih0eXBlLCBjb250ZW50LCBhbGlhcywgbWF0Y2hlZFN0ciwgZ3JlZWR5KSB7XG5cdHRoaXMudHlwZSA9IHR5cGU7XG5cdHRoaXMuY29udGVudCA9IGNvbnRlbnQ7XG5cdHRoaXMuYWxpYXMgPSBhbGlhcztcblx0Ly8gQ29weSBvZiB0aGUgZnVsbCBzdHJpbmcgdGhpcyB0b2tlbiB3YXMgY3JlYXRlZCBmcm9tXG5cdHRoaXMubGVuZ3RoID0gKG1hdGNoZWRTdHIgfHwgXCJcIikubGVuZ3RofDA7XG5cdHRoaXMuZ3JlZWR5ID0gISFncmVlZHk7XG59O1xuXG5Ub2tlbi5zdHJpbmdpZnkgPSBmdW5jdGlvbihvLCBsYW5ndWFnZSwgcGFyZW50KSB7XG5cdGlmICh0eXBlb2YgbyA9PSAnc3RyaW5nJykge1xuXHRcdHJldHVybiBvO1xuXHR9XG5cblx0aWYgKF8udXRpbC50eXBlKG8pID09PSAnQXJyYXknKSB7XG5cdFx0cmV0dXJuIG8ubWFwKGZ1bmN0aW9uKGVsZW1lbnQpIHtcblx0XHRcdHJldHVybiBUb2tlbi5zdHJpbmdpZnkoZWxlbWVudCwgbGFuZ3VhZ2UsIG8pO1xuXHRcdH0pLmpvaW4oJycpO1xuXHR9XG5cblx0dmFyIGVudiA9IHtcblx0XHR0eXBlOiBvLnR5cGUsXG5cdFx0Y29udGVudDogVG9rZW4uc3RyaW5naWZ5KG8uY29udGVudCwgbGFuZ3VhZ2UsIHBhcmVudCksXG5cdFx0dGFnOiAnc3BhbicsXG5cdFx0Y2xhc3NlczogWyd0b2tlbicsIG8udHlwZV0sXG5cdFx0YXR0cmlidXRlczoge30sXG5cdFx0bGFuZ3VhZ2U6IGxhbmd1YWdlLFxuXHRcdHBhcmVudDogcGFyZW50XG5cdH07XG5cblx0aWYgKG8uYWxpYXMpIHtcblx0XHR2YXIgYWxpYXNlcyA9IF8udXRpbC50eXBlKG8uYWxpYXMpID09PSAnQXJyYXknID8gby5hbGlhcyA6IFtvLmFsaWFzXTtcblx0XHRBcnJheS5wcm90b3R5cGUucHVzaC5hcHBseShlbnYuY2xhc3NlcywgYWxpYXNlcyk7XG5cdH1cblxuXHRfLmhvb2tzLnJ1bignd3JhcCcsIGVudik7XG5cblx0dmFyIGF0dHJpYnV0ZXMgPSBPYmplY3Qua2V5cyhlbnYuYXR0cmlidXRlcykubWFwKGZ1bmN0aW9uKG5hbWUpIHtcblx0XHRyZXR1cm4gbmFtZSArICc9XCInICsgKGVudi5hdHRyaWJ1dGVzW25hbWVdIHx8ICcnKS5yZXBsYWNlKC9cIi9nLCAnJnF1b3Q7JykgKyAnXCInO1xuXHR9KS5qb2luKCcgJyk7XG5cblx0cmV0dXJuICc8JyArIGVudi50YWcgKyAnIGNsYXNzPVwiJyArIGVudi5jbGFzc2VzLmpvaW4oJyAnKSArICdcIicgKyAoYXR0cmlidXRlcyA/ICcgJyArIGF0dHJpYnV0ZXMgOiAnJykgKyAnPicgKyBlbnYuY29udGVudCArICc8LycgKyBlbnYudGFnICsgJz4nO1xuXG59O1xuXG5pZiAoIV9zZWxmLmRvY3VtZW50KSB7XG5cdGlmICghX3NlbGYuYWRkRXZlbnRMaXN0ZW5lcikge1xuXHRcdC8vIGluIE5vZGUuanNcblx0XHRyZXR1cm4gX3NlbGYuUHJpc207XG5cdH1cblxuXHRpZiAoIV8uZGlzYWJsZVdvcmtlck1lc3NhZ2VIYW5kbGVyKSB7XG5cdFx0Ly8gSW4gd29ya2VyXG5cdFx0X3NlbGYuYWRkRXZlbnRMaXN0ZW5lcignbWVzc2FnZScsIGZ1bmN0aW9uIChldnQpIHtcblx0XHRcdHZhciBtZXNzYWdlID0gSlNPTi5wYXJzZShldnQuZGF0YSksXG5cdFx0XHRcdGxhbmcgPSBtZXNzYWdlLmxhbmd1YWdlLFxuXHRcdFx0XHRjb2RlID0gbWVzc2FnZS5jb2RlLFxuXHRcdFx0XHRpbW1lZGlhdGVDbG9zZSA9IG1lc3NhZ2UuaW1tZWRpYXRlQ2xvc2U7XG5cblx0XHRcdF9zZWxmLnBvc3RNZXNzYWdlKF8uaGlnaGxpZ2h0KGNvZGUsIF8ubGFuZ3VhZ2VzW2xhbmddLCBsYW5nKSk7XG5cdFx0XHRpZiAoaW1tZWRpYXRlQ2xvc2UpIHtcblx0XHRcdFx0X3NlbGYuY2xvc2UoKTtcblx0XHRcdH1cblx0XHR9LCBmYWxzZSk7XG5cdH1cblxuXHRyZXR1cm4gX3NlbGYuUHJpc207XG59XG5cbi8vR2V0IGN1cnJlbnQgc2NyaXB0IGFuZCBoaWdobGlnaHRcbnZhciBzY3JpcHQgPSBkb2N1bWVudC5jdXJyZW50U2NyaXB0IHx8IFtdLnNsaWNlLmNhbGwoZG9jdW1lbnQuZ2V0RWxlbWVudHNCeVRhZ05hbWUoXCJzY3JpcHRcIikpLnBvcCgpO1xuXG5pZiAoc2NyaXB0KSB7XG5cdF8uZmlsZW5hbWUgPSBzY3JpcHQuc3JjO1xuXG5cdGlmICghXy5tYW51YWwgJiYgIXNjcmlwdC5oYXNBdHRyaWJ1dGUoJ2RhdGEtbWFudWFsJykpIHtcblx0XHRpZihkb2N1bWVudC5yZWFkeVN0YXRlICE9PSBcImxvYWRpbmdcIikge1xuXHRcdFx0aWYgKHdpbmRvdy5yZXF1ZXN0QW5pbWF0aW9uRnJhbWUpIHtcblx0XHRcdFx0d2luZG93LnJlcXVlc3RBbmltYXRpb25GcmFtZShfLmhpZ2hsaWdodEFsbCk7XG5cdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHR3aW5kb3cuc2V0VGltZW91dChfLmhpZ2hsaWdodEFsbCwgMTYpO1xuXHRcdFx0fVxuXHRcdH1cblx0XHRlbHNlIHtcblx0XHRcdGRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoJ0RPTUNvbnRlbnRMb2FkZWQnLCBfLmhpZ2hsaWdodEFsbCk7XG5cdFx0fVxuXHR9XG59XG5cbnJldHVybiBfc2VsZi5QcmlzbTtcblxufSkoKTtcblxuaWYgKHR5cGVvZiBtb2R1bGUgIT09ICd1bmRlZmluZWQnICYmIG1vZHVsZS5leHBvcnRzKSB7XG5cdG1vZHVsZS5leHBvcnRzID0gUHJpc207XG59XG5cbi8vIGhhY2sgZm9yIGNvbXBvbmVudHMgdG8gd29yayBjb3JyZWN0bHkgaW4gbm9kZS5qc1xuaWYgKHR5cGVvZiBnbG9iYWwgIT09ICd1bmRlZmluZWQnKSB7XG5cdGdsb2JhbC5QcmlzbSA9IFByaXNtO1xufVxuXG5cbi8qICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipcbiAgICAgQmVnaW4gcHJpc20tbWFya3VwLmpzXG4qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqICovXG5cblByaXNtLmxhbmd1YWdlcy5tYXJrdXAgPSB7XG5cdCdjb21tZW50JzogLzwhLS1bXFxzXFxTXSo/LS0+Lyxcblx0J3Byb2xvZyc6IC88XFw/W1xcc1xcU10rP1xcPz4vLFxuXHQnZG9jdHlwZSc6IC88IURPQ1RZUEVbXFxzXFxTXSs/Pi9pLFxuXHQnY2RhdGEnOiAvPCFcXFtDREFUQVxcW1tcXHNcXFNdKj9dXT4vaSxcblx0J3RhZyc6IHtcblx0XHRwYXR0ZXJuOiAvPFxcLz8oPyFcXGQpW15cXHM+XFwvPSQ8JV0rKD86XFxzK1teXFxzPlxcLz1dKyg/Oj0oPzooXCJ8JykoPzpcXFxcW1xcc1xcU118KD8hXFwxKVteXFxcXF0pKlxcMXxbXlxccydcIj49XSspKT8pKlxccypcXC8/Pi9pLFxuXHRcdGdyZWVkeTogdHJ1ZSxcblx0XHRpbnNpZGU6IHtcblx0XHRcdCd0YWcnOiB7XG5cdFx0XHRcdHBhdHRlcm46IC9ePFxcLz9bXlxccz5cXC9dKy9pLFxuXHRcdFx0XHRpbnNpZGU6IHtcblx0XHRcdFx0XHQncHVuY3R1YXRpb24nOiAvXjxcXC8/Lyxcblx0XHRcdFx0XHQnbmFtZXNwYWNlJzogL15bXlxccz5cXC86XSs6L1xuXHRcdFx0XHR9XG5cdFx0XHR9LFxuXHRcdFx0J2F0dHItdmFsdWUnOiB7XG5cdFx0XHRcdHBhdHRlcm46IC89KD86KFwifCcpKD86XFxcXFtcXHNcXFNdfCg/IVxcMSlbXlxcXFxdKSpcXDF8W15cXHMnXCI+PV0rKS9pLFxuXHRcdFx0XHRpbnNpZGU6IHtcblx0XHRcdFx0XHQncHVuY3R1YXRpb24nOiBbXG5cdFx0XHRcdFx0XHQvXj0vLFxuXHRcdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0XHRwYXR0ZXJuOiAvKF58W15cXFxcXSlbXCInXS8sXG5cdFx0XHRcdFx0XHRcdGxvb2tiZWhpbmQ6IHRydWVcblx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHRdXG5cdFx0XHRcdH1cblx0XHRcdH0sXG5cdFx0XHQncHVuY3R1YXRpb24nOiAvXFwvPz4vLFxuXHRcdFx0J2F0dHItbmFtZSc6IHtcblx0XHRcdFx0cGF0dGVybjogL1teXFxzPlxcL10rLyxcblx0XHRcdFx0aW5zaWRlOiB7XG5cdFx0XHRcdFx0J25hbWVzcGFjZSc6IC9eW15cXHM+XFwvOl0rOi9cblx0XHRcdFx0fVxuXHRcdFx0fVxuXG5cdFx0fVxuXHR9LFxuXHQnZW50aXR5JzogLyYjP1tcXGRhLXpdezEsOH07L2lcbn07XG5cblByaXNtLmxhbmd1YWdlcy5tYXJrdXBbJ3RhZyddLmluc2lkZVsnYXR0ci12YWx1ZSddLmluc2lkZVsnZW50aXR5J10gPVxuXHRQcmlzbS5sYW5ndWFnZXMubWFya3VwWydlbnRpdHknXTtcblxuLy8gUGx1Z2luIHRvIG1ha2UgZW50aXR5IHRpdGxlIHNob3cgdGhlIHJlYWwgZW50aXR5LCBpZGVhIGJ5IFJvbWFuIEtvbWFyb3ZcblByaXNtLmhvb2tzLmFkZCgnd3JhcCcsIGZ1bmN0aW9uKGVudikge1xuXG5cdGlmIChlbnYudHlwZSA9PT0gJ2VudGl0eScpIHtcblx0XHRlbnYuYXR0cmlidXRlc1sndGl0bGUnXSA9IGVudi5jb250ZW50LnJlcGxhY2UoLyZhbXA7LywgJyYnKTtcblx0fVxufSk7XG5cblByaXNtLmxhbmd1YWdlcy54bWwgPSBQcmlzbS5sYW5ndWFnZXMubWFya3VwO1xuUHJpc20ubGFuZ3VhZ2VzLmh0bWwgPSBQcmlzbS5sYW5ndWFnZXMubWFya3VwO1xuUHJpc20ubGFuZ3VhZ2VzLm1hdGhtbCA9IFByaXNtLmxhbmd1YWdlcy5tYXJrdXA7XG5QcmlzbS5sYW5ndWFnZXMuc3ZnID0gUHJpc20ubGFuZ3VhZ2VzLm1hcmt1cDtcblxuXG4vKiAqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqXG4gICAgIEJlZ2luIHByaXNtLWNzcy5qc1xuKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiAqL1xuXG5QcmlzbS5sYW5ndWFnZXMuY3NzID0ge1xuXHQnY29tbWVudCc6IC9cXC9cXCpbXFxzXFxTXSo/XFwqXFwvLyxcblx0J2F0cnVsZSc6IHtcblx0XHRwYXR0ZXJuOiAvQFtcXHctXSs/Lio/KD86O3woPz1cXHMqXFx7KSkvaSxcblx0XHRpbnNpZGU6IHtcblx0XHRcdCdydWxlJzogL0BbXFx3LV0rL1xuXHRcdFx0Ly8gU2VlIHJlc3QgYmVsb3dcblx0XHR9XG5cdH0sXG5cdCd1cmwnOiAvdXJsXFwoKD86KFtcIiddKSg/OlxcXFwoPzpcXHJcXG58W1xcc1xcU10pfCg/IVxcMSlbXlxcXFxcXHJcXG5dKSpcXDF8Lio/KVxcKS9pLFxuXHQnc2VsZWN0b3InOiAvW157fVxcc11bXnt9O10qPyg/PVxccypcXHspLyxcblx0J3N0cmluZyc6IHtcblx0XHRwYXR0ZXJuOiAvKFwifCcpKD86XFxcXCg/OlxcclxcbnxbXFxzXFxTXSl8KD8hXFwxKVteXFxcXFxcclxcbl0pKlxcMS8sXG5cdFx0Z3JlZWR5OiB0cnVlXG5cdH0sXG5cdCdwcm9wZXJ0eSc6IC9bLV9hLXpcXHhBMC1cXHVGRkZGXVstXFx3XFx4QTAtXFx1RkZGRl0qKD89XFxzKjopL2ksXG5cdCdpbXBvcnRhbnQnOiAvXFxCIWltcG9ydGFudFxcYi9pLFxuXHQnZnVuY3Rpb24nOiAvWy1hLXowLTldKyg/PVxcKCkvaSxcblx0J3B1bmN0dWF0aW9uJzogL1soKXt9OzpdL1xufTtcblxuUHJpc20ubGFuZ3VhZ2VzLmNzc1snYXRydWxlJ10uaW5zaWRlLnJlc3QgPSBQcmlzbS5sYW5ndWFnZXMuY3NzO1xuXG5pZiAoUHJpc20ubGFuZ3VhZ2VzLm1hcmt1cCkge1xuXHRQcmlzbS5sYW5ndWFnZXMuaW5zZXJ0QmVmb3JlKCdtYXJrdXAnLCAndGFnJywge1xuXHRcdCdzdHlsZSc6IHtcblx0XHRcdHBhdHRlcm46IC8oPHN0eWxlW1xcc1xcU10qPz4pW1xcc1xcU10qPyg/PTxcXC9zdHlsZT4pL2ksXG5cdFx0XHRsb29rYmVoaW5kOiB0cnVlLFxuXHRcdFx0aW5zaWRlOiBQcmlzbS5sYW5ndWFnZXMuY3NzLFxuXHRcdFx0YWxpYXM6ICdsYW5ndWFnZS1jc3MnLFxuXHRcdFx0Z3JlZWR5OiB0cnVlXG5cdFx0fVxuXHR9KTtcblxuXHRQcmlzbS5sYW5ndWFnZXMuaW5zZXJ0QmVmb3JlKCdpbnNpZGUnLCAnYXR0ci12YWx1ZScsIHtcblx0XHQnc3R5bGUtYXR0cic6IHtcblx0XHRcdHBhdHRlcm46IC9cXHMqc3R5bGU9KFwifCcpKD86XFxcXFtcXHNcXFNdfCg/IVxcMSlbXlxcXFxdKSpcXDEvaSxcblx0XHRcdGluc2lkZToge1xuXHRcdFx0XHQnYXR0ci1uYW1lJzoge1xuXHRcdFx0XHRcdHBhdHRlcm46IC9eXFxzKnN0eWxlL2ksXG5cdFx0XHRcdFx0aW5zaWRlOiBQcmlzbS5sYW5ndWFnZXMubWFya3VwLnRhZy5pbnNpZGVcblx0XHRcdFx0fSxcblx0XHRcdFx0J3B1bmN0dWF0aW9uJzogL15cXHMqPVxccypbJ1wiXXxbJ1wiXVxccyokLyxcblx0XHRcdFx0J2F0dHItdmFsdWUnOiB7XG5cdFx0XHRcdFx0cGF0dGVybjogLy4rL2ksXG5cdFx0XHRcdFx0aW5zaWRlOiBQcmlzbS5sYW5ndWFnZXMuY3NzXG5cdFx0XHRcdH1cblx0XHRcdH0sXG5cdFx0XHRhbGlhczogJ2xhbmd1YWdlLWNzcydcblx0XHR9XG5cdH0sIFByaXNtLmxhbmd1YWdlcy5tYXJrdXAudGFnKTtcbn1cblxuLyogKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKlxuICAgICBCZWdpbiBwcmlzbS1jbGlrZS5qc1xuKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiAqL1xuXG5QcmlzbS5sYW5ndWFnZXMuY2xpa2UgPSB7XG5cdCdjb21tZW50JzogW1xuXHRcdHtcblx0XHRcdHBhdHRlcm46IC8oXnxbXlxcXFxdKVxcL1xcKltcXHNcXFNdKj8oPzpcXCpcXC98JCkvLFxuXHRcdFx0bG9va2JlaGluZDogdHJ1ZVxuXHRcdH0sXG5cdFx0e1xuXHRcdFx0cGF0dGVybjogLyhefFteXFxcXDpdKVxcL1xcLy4qLyxcblx0XHRcdGxvb2tiZWhpbmQ6IHRydWVcblx0XHR9XG5cdF0sXG5cdCdzdHJpbmcnOiB7XG5cdFx0cGF0dGVybjogLyhbXCInXSkoPzpcXFxcKD86XFxyXFxufFtcXHNcXFNdKXwoPyFcXDEpW15cXFxcXFxyXFxuXSkqXFwxLyxcblx0XHRncmVlZHk6IHRydWVcblx0fSxcblx0J2NsYXNzLW5hbWUnOiB7XG5cdFx0cGF0dGVybjogLygoPzpcXGIoPzpjbGFzc3xpbnRlcmZhY2V8ZXh0ZW5kc3xpbXBsZW1lbnRzfHRyYWl0fGluc3RhbmNlb2Z8bmV3KVxccyspfCg/OmNhdGNoXFxzK1xcKCkpW1xcdy5cXFxcXSsvaSxcblx0XHRsb29rYmVoaW5kOiB0cnVlLFxuXHRcdGluc2lkZToge1xuXHRcdFx0cHVuY3R1YXRpb246IC9bLlxcXFxdL1xuXHRcdH1cblx0fSxcblx0J2tleXdvcmQnOiAvXFxiKD86aWZ8ZWxzZXx3aGlsZXxkb3xmb3J8cmV0dXJufGlufGluc3RhbmNlb2Z8ZnVuY3Rpb258bmV3fHRyeXx0aHJvd3xjYXRjaHxmaW5hbGx5fG51bGx8YnJlYWt8Y29udGludWUpXFxiLyxcblx0J2Jvb2xlYW4nOiAvXFxiKD86dHJ1ZXxmYWxzZSlcXGIvLFxuXHQnZnVuY3Rpb24nOiAvW2EtejAtOV9dKyg/PVxcKCkvaSxcblx0J251bWJlcic6IC9cXGIweFtcXGRhLWZdK1xcYnwoPzpcXGJcXGQrXFwuP1xcZCp8XFxCXFwuXFxkKykoPzplWystXT9cXGQrKT8vaSxcblx0J29wZXJhdG9yJzogLy0tP3xcXCtcXCs/fCE9Pz0/fDw9P3w+PT98PT0/PT98JiY/fFxcfFxcfD98XFw/fFxcKnxcXC98fnxcXF58JS8sXG5cdCdwdW5jdHVhdGlvbic6IC9be31bXFxdOygpLC46XS9cbn07XG5cblxuLyogKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKlxuICAgICBCZWdpbiBwcmlzbS1qYXZhc2NyaXB0LmpzXG4qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqICovXG5cblByaXNtLmxhbmd1YWdlcy5qYXZhc2NyaXB0ID0gUHJpc20ubGFuZ3VhZ2VzLmV4dGVuZCgnY2xpa2UnLCB7XG5cdCdrZXl3b3JkJzogL1xcYig/OmFzfGFzeW5jfGF3YWl0fGJyZWFrfGNhc2V8Y2F0Y2h8Y2xhc3N8Y29uc3R8Y29udGludWV8ZGVidWdnZXJ8ZGVmYXVsdHxkZWxldGV8ZG98ZWxzZXxlbnVtfGV4cG9ydHxleHRlbmRzfGZpbmFsbHl8Zm9yfGZyb218ZnVuY3Rpb258Z2V0fGlmfGltcGxlbWVudHN8aW1wb3J0fGlufGluc3RhbmNlb2Z8aW50ZXJmYWNlfGxldHxuZXd8bnVsbHxvZnxwYWNrYWdlfHByaXZhdGV8cHJvdGVjdGVkfHB1YmxpY3xyZXR1cm58c2V0fHN0YXRpY3xzdXBlcnxzd2l0Y2h8dGhpc3x0aHJvd3x0cnl8dHlwZW9mfHZhcnx2b2lkfHdoaWxlfHdpdGh8eWllbGQpXFxiLyxcblx0J251bWJlcic6IC9cXGIoPzowW3hYXVtcXGRBLUZhLWZdK3wwW2JCXVswMV0rfDBbb09dWzAtN10rfE5hTnxJbmZpbml0eSlcXGJ8KD86XFxiXFxkK1xcLj9cXGQqfFxcQlxcLlxcZCspKD86W0VlXVsrLV0/XFxkKyk/Lyxcblx0Ly8gQWxsb3cgZm9yIGFsbCBub24tQVNDSUkgY2hhcmFjdGVycyAoU2VlIGh0dHA6Ly9zdGFja292ZXJmbG93LmNvbS9hLzIwMDg0NDQpXG5cdCdmdW5jdGlvbic6IC9bXyRhLXpcXHhBMC1cXHVGRkZGXVskXFx3XFx4QTAtXFx1RkZGRl0qKD89XFxzKlxcKCkvaSxcblx0J29wZXJhdG9yJzogLy1bLT1dP3xcXCtbKz1dP3whPT89P3w8PD89P3w+Pj8+Pz0/fD0oPzo9PT98Pik/fCZbJj1dP3xcXHxbfD1dP3xcXCpcXCo/PT98XFwvPT98fnxcXF49P3wlPT98XFw/fFxcLnszfS9cbn0pO1xuXG5QcmlzbS5sYW5ndWFnZXMuaW5zZXJ0QmVmb3JlKCdqYXZhc2NyaXB0JywgJ2tleXdvcmQnLCB7XG5cdCdyZWdleCc6IHtcblx0XHRwYXR0ZXJuOiAvKF58W14vXSlcXC8oPyFcXC8pKFxcW1teXFxdXFxyXFxuXStdfFxcXFwufFteL1xcXFxcXFtcXHJcXG5dKStcXC9bZ2lteXVdezAsNX0oPz1cXHMqKCR8W1xcclxcbiwuO30pXSkpLyxcblx0XHRsb29rYmVoaW5kOiB0cnVlLFxuXHRcdGdyZWVkeTogdHJ1ZVxuXHR9LFxuXHQvLyBUaGlzIG11c3QgYmUgZGVjbGFyZWQgYmVmb3JlIGtleXdvcmQgYmVjYXVzZSB3ZSB1c2UgXCJmdW5jdGlvblwiIGluc2lkZSB0aGUgbG9vay1mb3J3YXJkXG5cdCdmdW5jdGlvbi12YXJpYWJsZSc6IHtcblx0XHRwYXR0ZXJuOiAvW18kYS16XFx4QTAtXFx1RkZGRl1bJFxcd1xceEEwLVxcdUZGRkZdKig/PVxccyo9XFxzKig/OmZ1bmN0aW9uXFxifCg/OlxcKFteKCldKlxcKXxbXyRhLXpcXHhBMC1cXHVGRkZGXVskXFx3XFx4QTAtXFx1RkZGRl0qKVxccyo9PikpL2ksXG5cdFx0YWxpYXM6ICdmdW5jdGlvbidcblx0fVxufSk7XG5cblByaXNtLmxhbmd1YWdlcy5pbnNlcnRCZWZvcmUoJ2phdmFzY3JpcHQnLCAnc3RyaW5nJywge1xuXHQndGVtcGxhdGUtc3RyaW5nJzoge1xuXHRcdHBhdHRlcm46IC9gKD86XFxcXFtcXHNcXFNdfFteXFxcXGBdKSpgLyxcblx0XHRncmVlZHk6IHRydWUsXG5cdFx0aW5zaWRlOiB7XG5cdFx0XHQnaW50ZXJwb2xhdGlvbic6IHtcblx0XHRcdFx0cGF0dGVybjogL1xcJFxce1tefV0rXFx9Lyxcblx0XHRcdFx0aW5zaWRlOiB7XG5cdFx0XHRcdFx0J2ludGVycG9sYXRpb24tcHVuY3R1YXRpb24nOiB7XG5cdFx0XHRcdFx0XHRwYXR0ZXJuOiAvXlxcJFxce3xcXH0kLyxcblx0XHRcdFx0XHRcdGFsaWFzOiAncHVuY3R1YXRpb24nXG5cdFx0XHRcdFx0fSxcblx0XHRcdFx0XHRyZXN0OiBQcmlzbS5sYW5ndWFnZXMuamF2YXNjcmlwdFxuXHRcdFx0XHR9XG5cdFx0XHR9LFxuXHRcdFx0J3N0cmluZyc6IC9bXFxzXFxTXSsvXG5cdFx0fVxuXHR9XG59KTtcblxuaWYgKFByaXNtLmxhbmd1YWdlcy5tYXJrdXApIHtcblx0UHJpc20ubGFuZ3VhZ2VzLmluc2VydEJlZm9yZSgnbWFya3VwJywgJ3RhZycsIHtcblx0XHQnc2NyaXB0Jzoge1xuXHRcdFx0cGF0dGVybjogLyg8c2NyaXB0W1xcc1xcU10qPz4pW1xcc1xcU10qPyg/PTxcXC9zY3JpcHQ+KS9pLFxuXHRcdFx0bG9va2JlaGluZDogdHJ1ZSxcblx0XHRcdGluc2lkZTogUHJpc20ubGFuZ3VhZ2VzLmphdmFzY3JpcHQsXG5cdFx0XHRhbGlhczogJ2xhbmd1YWdlLWphdmFzY3JpcHQnLFxuXHRcdFx0Z3JlZWR5OiB0cnVlXG5cdFx0fVxuXHR9KTtcbn1cblxuUHJpc20ubGFuZ3VhZ2VzLmpzID0gUHJpc20ubGFuZ3VhZ2VzLmphdmFzY3JpcHQ7XG5cblxuLyogKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKlxuICAgICBCZWdpbiBwcmlzbS1maWxlLWhpZ2hsaWdodC5qc1xuKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiAqL1xuXG4oZnVuY3Rpb24gKCkge1xuXHRpZiAodHlwZW9mIHNlbGYgPT09ICd1bmRlZmluZWQnIHx8ICFzZWxmLlByaXNtIHx8ICFzZWxmLmRvY3VtZW50IHx8ICFkb2N1bWVudC5xdWVyeVNlbGVjdG9yKSB7XG5cdFx0cmV0dXJuO1xuXHR9XG5cblx0c2VsZi5QcmlzbS5maWxlSGlnaGxpZ2h0ID0gZnVuY3Rpb24oKSB7XG5cblx0XHR2YXIgRXh0ZW5zaW9ucyA9IHtcblx0XHRcdCdqcyc6ICdqYXZhc2NyaXB0Jyxcblx0XHRcdCdweSc6ICdweXRob24nLFxuXHRcdFx0J3JiJzogJ3J1YnknLFxuXHRcdFx0J3BzMSc6ICdwb3dlcnNoZWxsJyxcblx0XHRcdCdwc20xJzogJ3Bvd2Vyc2hlbGwnLFxuXHRcdFx0J3NoJzogJ2Jhc2gnLFxuXHRcdFx0J2JhdCc6ICdiYXRjaCcsXG5cdFx0XHQnaCc6ICdjJyxcblx0XHRcdCd0ZXgnOiAnbGF0ZXgnXG5cdFx0fTtcblxuXHRcdEFycmF5LnByb3RvdHlwZS5zbGljZS5jYWxsKGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoJ3ByZVtkYXRhLXNyY10nKSkuZm9yRWFjaChmdW5jdGlvbiAocHJlKSB7XG5cdFx0XHR2YXIgc3JjID0gcHJlLmdldEF0dHJpYnV0ZSgnZGF0YS1zcmMnKTtcblxuXHRcdFx0dmFyIGxhbmd1YWdlLCBwYXJlbnQgPSBwcmU7XG5cdFx0XHR2YXIgbGFuZyA9IC9cXGJsYW5nKD86dWFnZSk/LSg/IVxcKikoXFx3KylcXGIvaTtcblx0XHRcdHdoaWxlIChwYXJlbnQgJiYgIWxhbmcudGVzdChwYXJlbnQuY2xhc3NOYW1lKSkge1xuXHRcdFx0XHRwYXJlbnQgPSBwYXJlbnQucGFyZW50Tm9kZTtcblx0XHRcdH1cblxuXHRcdFx0aWYgKHBhcmVudCkge1xuXHRcdFx0XHRsYW5ndWFnZSA9IChwcmUuY2xhc3NOYW1lLm1hdGNoKGxhbmcpIHx8IFssICcnXSlbMV07XG5cdFx0XHR9XG5cblx0XHRcdGlmICghbGFuZ3VhZ2UpIHtcblx0XHRcdFx0dmFyIGV4dGVuc2lvbiA9IChzcmMubWF0Y2goL1xcLihcXHcrKSQvKSB8fCBbLCAnJ10pWzFdO1xuXHRcdFx0XHRsYW5ndWFnZSA9IEV4dGVuc2lvbnNbZXh0ZW5zaW9uXSB8fCBleHRlbnNpb247XG5cdFx0XHR9XG5cblx0XHRcdHZhciBjb2RlID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnY29kZScpO1xuXHRcdFx0Y29kZS5jbGFzc05hbWUgPSAnbGFuZ3VhZ2UtJyArIGxhbmd1YWdlO1xuXG5cdFx0XHRwcmUudGV4dENvbnRlbnQgPSAnJztcblxuXHRcdFx0Y29kZS50ZXh0Q29udGVudCA9ICdMb2FkaW5n4oCmJztcblxuXHRcdFx0cHJlLmFwcGVuZENoaWxkKGNvZGUpO1xuXG5cdFx0XHR2YXIgeGhyID0gbmV3IFhNTEh0dHBSZXF1ZXN0KCk7XG5cblx0XHRcdHhoci5vcGVuKCdHRVQnLCBzcmMsIHRydWUpO1xuXG5cdFx0XHR4aHIub25yZWFkeXN0YXRlY2hhbmdlID0gZnVuY3Rpb24gKCkge1xuXHRcdFx0XHRpZiAoeGhyLnJlYWR5U3RhdGUgPT0gNCkge1xuXG5cdFx0XHRcdFx0aWYgKHhoci5zdGF0dXMgPCA0MDAgJiYgeGhyLnJlc3BvbnNlVGV4dCkge1xuXHRcdFx0XHRcdFx0Y29kZS50ZXh0Q29udGVudCA9IHhoci5yZXNwb25zZVRleHQ7XG5cblx0XHRcdFx0XHRcdFByaXNtLmhpZ2hsaWdodEVsZW1lbnQoY29kZSk7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRcdGVsc2UgaWYgKHhoci5zdGF0dXMgPj0gNDAwKSB7XG5cdFx0XHRcdFx0XHRjb2RlLnRleHRDb250ZW50ID0gJ+KcliBFcnJvciAnICsgeGhyLnN0YXR1cyArICcgd2hpbGUgZmV0Y2hpbmcgZmlsZTogJyArIHhoci5zdGF0dXNUZXh0O1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0XHRlbHNlIHtcblx0XHRcdFx0XHRcdGNvZGUudGV4dENvbnRlbnQgPSAn4pyWIEVycm9yOiBGaWxlIGRvZXMgbm90IGV4aXN0IG9yIGlzIGVtcHR5Jztcblx0XHRcdFx0XHR9XG5cdFx0XHRcdH1cblx0XHRcdH07XG5cblx0XHRcdHhoci5zZW5kKG51bGwpO1xuXHRcdH0pO1xuXG5cdH07XG5cblx0ZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcignRE9NQ29udGVudExvYWRlZCcsIHNlbGYuUHJpc20uZmlsZUhpZ2hsaWdodCk7XG5cbn0pKCk7XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9wcmlzbWpzL3ByaXNtLmpzXG4vLyBtb2R1bGUgaWQgPSAxMTdcbi8vIG1vZHVsZSBjaHVua3MgPSAxMCIsIihmdW5jdGlvbiAoKSB7XG5cblx0aWYgKHR5cGVvZiBzZWxmID09PSAndW5kZWZpbmVkJyB8fCAhc2VsZi5QcmlzbSB8fCAhc2VsZi5kb2N1bWVudCB8fCAhZG9jdW1lbnQuY3JlYXRlUmFuZ2UpIHtcblx0XHRyZXR1cm47XG5cdH1cblxuXHRQcmlzbS5wbHVnaW5zLktlZXBNYXJrdXAgPSB0cnVlO1xuXG5cdFByaXNtLmhvb2tzLmFkZCgnYmVmb3JlLWhpZ2hsaWdodCcsIGZ1bmN0aW9uIChlbnYpIHtcblx0XHRpZiAoIWVudi5lbGVtZW50LmNoaWxkcmVuLmxlbmd0aCkge1xuXHRcdFx0cmV0dXJuO1xuXHRcdH1cblxuXHRcdHZhciBwb3MgPSAwO1xuXHRcdHZhciBkYXRhID0gW107XG5cdFx0dmFyIGYgPSBmdW5jdGlvbiAoZWx0LCBiYXNlTm9kZSkge1xuXHRcdFx0dmFyIG8gPSB7fTtcblx0XHRcdGlmICghYmFzZU5vZGUpIHtcblx0XHRcdFx0Ly8gQ2xvbmUgdGhlIG9yaWdpbmFsIHRhZyB0byBrZWVwIGFsbCBhdHRyaWJ1dGVzXG5cdFx0XHRcdG8uY2xvbmUgPSBlbHQuY2xvbmVOb2RlKGZhbHNlKTtcblx0XHRcdFx0by5wb3NPcGVuID0gcG9zO1xuXHRcdFx0XHRkYXRhLnB1c2gobyk7XG5cdFx0XHR9XG5cdFx0XHRmb3IgKHZhciBpID0gMCwgbCA9IGVsdC5jaGlsZE5vZGVzLmxlbmd0aDsgaSA8IGw7IGkrKykge1xuXHRcdFx0XHR2YXIgY2hpbGQgPSBlbHQuY2hpbGROb2Rlc1tpXTtcblx0XHRcdFx0aWYgKGNoaWxkLm5vZGVUeXBlID09PSAxKSB7IC8vIGVsZW1lbnRcblx0XHRcdFx0XHRmKGNoaWxkKTtcblx0XHRcdFx0fSBlbHNlIGlmKGNoaWxkLm5vZGVUeXBlID09PSAzKSB7IC8vIHRleHRcblx0XHRcdFx0XHRwb3MgKz0gY2hpbGQuZGF0YS5sZW5ndGg7XG5cdFx0XHRcdH1cblx0XHRcdH1cblx0XHRcdGlmICghYmFzZU5vZGUpIHtcblx0XHRcdFx0by5wb3NDbG9zZSA9IHBvcztcblx0XHRcdH1cblx0XHR9O1xuXHRcdGYoZW52LmVsZW1lbnQsIHRydWUpO1xuXG5cdFx0aWYgKGRhdGEgJiYgZGF0YS5sZW5ndGgpIHtcblx0XHRcdC8vIGRhdGEgaXMgYW4gYXJyYXkgb2YgYWxsIGV4aXN0aW5nIHRhZ3Ncblx0XHRcdGVudi5rZWVwTWFya3VwID0gZGF0YTtcblx0XHR9XG5cdH0pO1xuXG5cdFByaXNtLmhvb2tzLmFkZCgnYWZ0ZXItaGlnaGxpZ2h0JywgZnVuY3Rpb24gKGVudikge1xuXHRcdGlmKGVudi5rZWVwTWFya3VwICYmIGVudi5rZWVwTWFya3VwLmxlbmd0aCkge1xuXG5cdFx0XHR2YXIgd2FsayA9IGZ1bmN0aW9uIChlbHQsIG5vZGVTdGF0ZSkge1xuXHRcdFx0XHRmb3IgKHZhciBpID0gMCwgbCA9IGVsdC5jaGlsZE5vZGVzLmxlbmd0aDsgaSA8IGw7IGkrKykge1xuXG5cdFx0XHRcdFx0dmFyIGNoaWxkID0gZWx0LmNoaWxkTm9kZXNbaV07XG5cblx0XHRcdFx0XHRpZiAoY2hpbGQubm9kZVR5cGUgPT09IDEpIHsgLy8gZWxlbWVudFxuXHRcdFx0XHRcdFx0aWYgKCF3YWxrKGNoaWxkLCBub2RlU3RhdGUpKSB7XG5cdFx0XHRcdFx0XHRcdHJldHVybiBmYWxzZTtcblx0XHRcdFx0XHRcdH1cblxuXHRcdFx0XHRcdH0gZWxzZSBpZiAoY2hpbGQubm9kZVR5cGUgPT09IDMpIHsgLy8gdGV4dFxuXHRcdFx0XHRcdFx0aWYoIW5vZGVTdGF0ZS5ub2RlU3RhcnQgJiYgbm9kZVN0YXRlLnBvcyArIGNoaWxkLmRhdGEubGVuZ3RoID4gbm9kZVN0YXRlLm5vZGUucG9zT3Blbikge1xuXHRcdFx0XHRcdFx0XHQvLyBXZSBmb3VuZCB0aGUgc3RhcnQgcG9zaXRpb25cblx0XHRcdFx0XHRcdFx0bm9kZVN0YXRlLm5vZGVTdGFydCA9IGNoaWxkO1xuXHRcdFx0XHRcdFx0XHRub2RlU3RhdGUubm9kZVN0YXJ0UG9zID0gbm9kZVN0YXRlLm5vZGUucG9zT3BlbiAtIG5vZGVTdGF0ZS5wb3M7XG5cdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XHRpZihub2RlU3RhdGUubm9kZVN0YXJ0ICYmIG5vZGVTdGF0ZS5wb3MgKyBjaGlsZC5kYXRhLmxlbmd0aCA+PSBub2RlU3RhdGUubm9kZS5wb3NDbG9zZSkge1xuXHRcdFx0XHRcdFx0XHQvLyBXZSBmb3VuZCB0aGUgZW5kIHBvc2l0aW9uXG5cdFx0XHRcdFx0XHRcdG5vZGVTdGF0ZS5ub2RlRW5kID0gY2hpbGQ7XG5cdFx0XHRcdFx0XHRcdG5vZGVTdGF0ZS5ub2RlRW5kUG9zID0gbm9kZVN0YXRlLm5vZGUucG9zQ2xvc2UgLSBub2RlU3RhdGUucG9zO1xuXHRcdFx0XHRcdFx0fVxuXG5cdFx0XHRcdFx0XHRub2RlU3RhdGUucG9zICs9IGNoaWxkLmRhdGEubGVuZ3RoO1xuXHRcdFx0XHRcdH1cblxuXHRcdFx0XHRcdGlmIChub2RlU3RhdGUubm9kZVN0YXJ0ICYmIG5vZGVTdGF0ZS5ub2RlRW5kKSB7XG5cdFx0XHRcdFx0XHQvLyBTZWxlY3QgdGhlIHJhbmdlIGFuZCB3cmFwIGl0IHdpdGggdGhlIGNsb25lXG5cdFx0XHRcdFx0XHR2YXIgcmFuZ2UgPSBkb2N1bWVudC5jcmVhdGVSYW5nZSgpO1xuXHRcdFx0XHRcdFx0cmFuZ2Uuc2V0U3RhcnQobm9kZVN0YXRlLm5vZGVTdGFydCwgbm9kZVN0YXRlLm5vZGVTdGFydFBvcyk7XG5cdFx0XHRcdFx0XHRyYW5nZS5zZXRFbmQobm9kZVN0YXRlLm5vZGVFbmQsIG5vZGVTdGF0ZS5ub2RlRW5kUG9zKTtcblx0XHRcdFx0XHRcdG5vZGVTdGF0ZS5ub2RlLmNsb25lLmFwcGVuZENoaWxkKHJhbmdlLmV4dHJhY3RDb250ZW50cygpKTtcblx0XHRcdFx0XHRcdHJhbmdlLmluc2VydE5vZGUobm9kZVN0YXRlLm5vZGUuY2xvbmUpO1xuXHRcdFx0XHRcdFx0cmFuZ2UuZGV0YWNoKCk7XG5cblx0XHRcdFx0XHRcdC8vIFByb2Nlc3MgaXMgb3ZlclxuXHRcdFx0XHRcdFx0cmV0dXJuIGZhbHNlO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fVxuXHRcdFx0XHRyZXR1cm4gdHJ1ZTtcblx0XHRcdH07XG5cblx0XHRcdC8vIEZvciBlYWNoIHRhZywgd2Ugd2FsayB0aGUgRE9NIHRvIHJlaW5zZXJ0IGl0XG5cdFx0XHRlbnYua2VlcE1hcmt1cC5mb3JFYWNoKGZ1bmN0aW9uIChub2RlKSB7XG5cdFx0XHRcdHdhbGsoZW52LmVsZW1lbnQsIHtcblx0XHRcdFx0XHRub2RlOiBub2RlLFxuXHRcdFx0XHRcdHBvczogMFxuXHRcdFx0XHR9KTtcblx0XHRcdH0pO1xuXHRcdFx0Ly8gU3RvcmUgbmV3IGhpZ2hsaWdodGVkQ29kZSBmb3IgbGF0ZXIgaG9va3MgY2FsbHNcblx0XHRcdGVudi5oaWdobGlnaHRlZENvZGUgPSBlbnYuZWxlbWVudC5pbm5lckhUTUw7XG5cdFx0fVxuXHR9KTtcbn0oKSk7XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9wcmlzbWpzL3BsdWdpbnMva2VlcC1tYXJrdXAvcHJpc20ta2VlcC1tYXJrdXAuanNcbi8vIG1vZHVsZSBpZCA9IDExOFxuLy8gbW9kdWxlIGNodW5rcyA9IDEwIiwiKGZ1bmN0aW9uICgpIHtcblxuXHRpZiAodHlwZW9mIHNlbGYgPT09ICd1bmRlZmluZWQnIHx8ICFzZWxmLlByaXNtIHx8ICFzZWxmLmRvY3VtZW50IHx8ICFQcmlzbS5sYW5ndWFnZXMubWFya3VwKSB7XG5cdFx0cmV0dXJuO1xuXHR9XG5cblx0UHJpc20ucGx1Z2lucy5VbmVzY2FwZWRNYXJrdXAgPSB0cnVlO1xuXG5cdFByaXNtLmhvb2tzLmFkZCgnYmVmb3JlLWhpZ2hsaWdodGFsbCcsIGZ1bmN0aW9uIChlbnYpIHtcblx0XHRlbnYuc2VsZWN0b3IgKz0gXCIsIFtjbGFzcyo9J2xhbmctJ10gc2NyaXB0W3R5cGU9J3RleHQvcGxhaW4nXSwgW2NsYXNzKj0nbGFuZ3VhZ2UtJ10gc2NyaXB0W3R5cGU9J3RleHQvcGxhaW4nXVwiICtcblx0XHQgICAgICAgICAgICAgICAgXCIsIHNjcmlwdFt0eXBlPSd0ZXh0L3BsYWluJ11bY2xhc3MqPSdsYW5nLSddLCBzY3JpcHRbdHlwZT0ndGV4dC9wbGFpbiddW2NsYXNzKj0nbGFuZ3VhZ2UtJ11cIjtcblx0fSk7XG5cblx0UHJpc20uaG9va3MuYWRkKCdiZWZvcmUtc2FuaXR5LWNoZWNrJywgZnVuY3Rpb24gKGVudikge1xuXHRcdGlmICgoZW52LmVsZW1lbnQubWF0Y2hlcyB8fCBlbnYuZWxlbWVudC5tc01hdGNoZXNTZWxlY3RvcikuY2FsbChlbnYuZWxlbWVudCwgXCJzY3JpcHRbdHlwZT0ndGV4dC9wbGFpbiddXCIpKSB7XG5cdFx0XHR2YXIgY29kZSA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJjb2RlXCIpO1xuXHRcdFx0dmFyIHByZSA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJwcmVcIik7XG5cblx0XHRcdHByZS5jbGFzc05hbWUgPSBjb2RlLmNsYXNzTmFtZSA9IGVudi5lbGVtZW50LmNsYXNzTmFtZTtcblxuXHRcdFx0aWYgKGVudi5lbGVtZW50LmRhdGFzZXQpIHtcblx0XHRcdFx0T2JqZWN0LmtleXMoZW52LmVsZW1lbnQuZGF0YXNldCkuZm9yRWFjaChmdW5jdGlvbiAoa2V5KSB7XG5cdFx0XHRcdFx0aWYgKE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChlbnYuZWxlbWVudC5kYXRhc2V0LCBrZXkpKSB7XG5cdFx0XHRcdFx0XHRwcmUuZGF0YXNldFtrZXldID0gZW52LmVsZW1lbnQuZGF0YXNldFtrZXldO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fSk7XG5cdFx0XHR9XG5cblx0XHRcdGVudi5jb2RlID0gZW52LmNvZGUucmVwbGFjZSgvJmx0O1xcL3NjcmlwdCg+fCZndDspL2dpLCBcIjwvc2NyaVwiICsgXCJwdD5cIik7XG5cdFx0XHRjb2RlLnRleHRDb250ZW50ID0gZW52LmNvZGU7XG5cblx0XHRcdHByZS5hcHBlbmRDaGlsZChjb2RlKTtcblx0XHRcdGVudi5lbGVtZW50LnBhcmVudE5vZGUucmVwbGFjZUNoaWxkKHByZSwgZW52LmVsZW1lbnQpO1xuXHRcdFx0ZW52LmVsZW1lbnQgPSBjb2RlO1xuXHRcdFx0cmV0dXJuO1xuXHRcdH1cblxuXHRcdHZhciBwcmUgPSBlbnYuZWxlbWVudC5wYXJlbnROb2RlO1xuXHRcdGlmICghZW52LmNvZGUgJiYgcHJlICYmIHByZS5ub2RlTmFtZS50b0xvd2VyQ2FzZSgpID09ICdwcmUnICYmXG5cdFx0XHRcdGVudi5lbGVtZW50LmNoaWxkTm9kZXMubGVuZ3RoICYmIGVudi5lbGVtZW50LmNoaWxkTm9kZXNbMF0ubm9kZU5hbWUgPT0gXCIjY29tbWVudFwiKSB7XG5cdFx0XHRlbnYuZWxlbWVudC50ZXh0Q29udGVudCA9IGVudi5jb2RlID0gZW52LmVsZW1lbnQuY2hpbGROb2Rlc1swXS50ZXh0Q29udGVudDtcblx0XHR9XG5cdH0pO1xufSgpKTtcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3ByaXNtanMvcGx1Z2lucy91bmVzY2FwZWQtbWFya3VwL3ByaXNtLXVuZXNjYXBlZC1tYXJrdXAuanNcbi8vIG1vZHVsZSBpZCA9IDExOVxuLy8gbW9kdWxlIGNodW5rcyA9IDEwIiwiKGZ1bmN0aW9uKCkge1xuXG52YXIgYXNzaWduID0gT2JqZWN0LmFzc2lnbiB8fCBmdW5jdGlvbiAob2JqMSwgb2JqMikge1xuXHRmb3IgKHZhciBuYW1lIGluIG9iajIpIHtcblx0XHRpZiAob2JqMi5oYXNPd25Qcm9wZXJ0eShuYW1lKSlcblx0XHRcdG9iajFbbmFtZV0gPSBvYmoyW25hbWVdO1xuXHR9XG5cdHJldHVybiBvYmoxO1xufVxuXG5mdW5jdGlvbiBOb3JtYWxpemVXaGl0ZXNwYWNlKGRlZmF1bHRzKSB7XG5cdHRoaXMuZGVmYXVsdHMgPSBhc3NpZ24oe30sIGRlZmF1bHRzKTtcbn1cblxuZnVuY3Rpb24gdG9DYW1lbENhc2UodmFsdWUpIHtcblx0cmV0dXJuIHZhbHVlLnJlcGxhY2UoLy0oXFx3KS9nLCBmdW5jdGlvbihtYXRjaCwgZmlyc3RDaGFyKSB7XG5cdFx0cmV0dXJuIGZpcnN0Q2hhci50b1VwcGVyQ2FzZSgpO1xuXHR9KTtcbn1cblxuZnVuY3Rpb24gdGFiTGVuKHN0cikge1xuXHR2YXIgcmVzID0gMDtcblx0Zm9yICh2YXIgaSA9IDA7IGkgPCBzdHIubGVuZ3RoOyArK2kpIHtcblx0XHRpZiAoc3RyLmNoYXJDb2RlQXQoaSkgPT0gJ1xcdCcuY2hhckNvZGVBdCgwKSlcblx0XHRcdHJlcyArPSAzO1xuXHR9XG5cdHJldHVybiBzdHIubGVuZ3RoICsgcmVzO1xufVxuXG5Ob3JtYWxpemVXaGl0ZXNwYWNlLnByb3RvdHlwZSA9IHtcblx0c2V0RGVmYXVsdHM6IGZ1bmN0aW9uIChkZWZhdWx0cykge1xuXHRcdHRoaXMuZGVmYXVsdHMgPSBhc3NpZ24odGhpcy5kZWZhdWx0cywgZGVmYXVsdHMpO1xuXHR9LFxuXHRub3JtYWxpemU6IGZ1bmN0aW9uIChpbnB1dCwgc2V0dGluZ3MpIHtcblx0XHRzZXR0aW5ncyA9IGFzc2lnbih0aGlzLmRlZmF1bHRzLCBzZXR0aW5ncyk7XG5cblx0XHRmb3IgKHZhciBuYW1lIGluIHNldHRpbmdzKSB7XG5cdFx0XHR2YXIgbWV0aG9kTmFtZSA9IHRvQ2FtZWxDYXNlKG5hbWUpO1xuXHRcdFx0aWYgKG5hbWUgIT09IFwibm9ybWFsaXplXCIgJiYgbWV0aG9kTmFtZSAhPT0gJ3NldERlZmF1bHRzJyAmJlxuXHRcdFx0XHRcdHNldHRpbmdzW25hbWVdICYmIHRoaXNbbWV0aG9kTmFtZV0pIHtcblx0XHRcdFx0aW5wdXQgPSB0aGlzW21ldGhvZE5hbWVdLmNhbGwodGhpcywgaW5wdXQsIHNldHRpbmdzW25hbWVdKTtcblx0XHRcdH1cblx0XHR9XG5cblx0XHRyZXR1cm4gaW5wdXQ7XG5cdH0sXG5cblx0Lypcblx0ICogTm9ybWFsaXphdGlvbiBtZXRob2RzXG5cdCAqL1xuXHRsZWZ0VHJpbTogZnVuY3Rpb24gKGlucHV0KSB7XG5cdFx0cmV0dXJuIGlucHV0LnJlcGxhY2UoL15cXHMrLywgJycpO1xuXHR9LFxuXHRyaWdodFRyaW06IGZ1bmN0aW9uIChpbnB1dCkge1xuXHRcdHJldHVybiBpbnB1dC5yZXBsYWNlKC9cXHMrJC8sICcnKTtcblx0fSxcblx0dGFic1RvU3BhY2VzOiBmdW5jdGlvbiAoaW5wdXQsIHNwYWNlcykge1xuXHRcdHNwYWNlcyA9IHNwYWNlc3wwIHx8IDQ7XG5cdFx0cmV0dXJuIGlucHV0LnJlcGxhY2UoL1xcdC9nLCBuZXcgQXJyYXkoKytzcGFjZXMpLmpvaW4oJyAnKSk7XG5cdH0sXG5cdHNwYWNlc1RvVGFiczogZnVuY3Rpb24gKGlucHV0LCBzcGFjZXMpIHtcblx0XHRzcGFjZXMgPSBzcGFjZXN8MCB8fCA0O1xuXHRcdHJldHVybiBpbnB1dC5yZXBsYWNlKG5ldyBSZWdFeHAoJyB7JyArIHNwYWNlcyArICd9JywgJ2cnKSwgJ1xcdCcpO1xuXHR9LFxuXHRyZW1vdmVUcmFpbGluZzogZnVuY3Rpb24gKGlucHV0KSB7XG5cdFx0cmV0dXJuIGlucHV0LnJlcGxhY2UoL1xccyo/JC9nbSwgJycpO1xuXHR9LFxuXHQvLyBTdXBwb3J0IGZvciBkZXByZWNhdGVkIHBsdWdpbiByZW1vdmUtaW5pdGlhbC1saW5lLWZlZWRcblx0cmVtb3ZlSW5pdGlhbExpbmVGZWVkOiBmdW5jdGlvbiAoaW5wdXQpIHtcblx0XHRyZXR1cm4gaW5wdXQucmVwbGFjZSgvXig/Olxccj9cXG58XFxyKS8sICcnKTtcblx0fSxcblx0cmVtb3ZlSW5kZW50OiBmdW5jdGlvbiAoaW5wdXQpIHtcblx0XHR2YXIgaW5kZW50cyA9IGlucHV0Lm1hdGNoKC9eW15cXFNcXG5cXHJdKig/PVxcUykvZ20pO1xuXG5cdFx0aWYgKCFpbmRlbnRzIHx8ICFpbmRlbnRzWzBdLmxlbmd0aClcblx0XHRcdHJldHVybiBpbnB1dDtcblxuXHRcdGluZGVudHMuc29ydChmdW5jdGlvbihhLCBiKXtyZXR1cm4gYS5sZW5ndGggLSBiLmxlbmd0aDsgfSk7XG5cblx0XHRpZiAoIWluZGVudHNbMF0ubGVuZ3RoKVxuXHRcdFx0cmV0dXJuIGlucHV0O1xuXG5cdFx0cmV0dXJuIGlucHV0LnJlcGxhY2UobmV3IFJlZ0V4cCgnXicgKyBpbmRlbnRzWzBdLCAnZ20nKSwgJycpO1xuXHR9LFxuXHRpbmRlbnQ6IGZ1bmN0aW9uIChpbnB1dCwgdGFicykge1xuXHRcdHJldHVybiBpbnB1dC5yZXBsYWNlKC9eW15cXFNcXG5cXHJdKig/PVxcUykvZ20sIG5ldyBBcnJheSgrK3RhYnMpLmpvaW4oJ1xcdCcpICsgJyQmJyk7XG5cdH0sXG5cdGJyZWFrTGluZXM6IGZ1bmN0aW9uIChpbnB1dCwgY2hhcmFjdGVycykge1xuXHRcdGNoYXJhY3RlcnMgPSAoY2hhcmFjdGVycyA9PT0gdHJ1ZSkgPyA4MCA6IGNoYXJhY3RlcnN8MCB8fCA4MDtcblxuXHRcdHZhciBsaW5lcyA9IGlucHV0LnNwbGl0KCdcXG4nKTtcblx0XHRmb3IgKHZhciBpID0gMDsgaSA8IGxpbmVzLmxlbmd0aDsgKytpKSB7XG5cdFx0XHRpZiAodGFiTGVuKGxpbmVzW2ldKSA8PSBjaGFyYWN0ZXJzKVxuXHRcdFx0XHRjb250aW51ZTtcblxuXHRcdFx0dmFyIGxpbmUgPSBsaW5lc1tpXS5zcGxpdCgvKFxccyspL2cpLFxuXHRcdFx0ICAgIGxlbiA9IDA7XG5cblx0XHRcdGZvciAodmFyIGogPSAwOyBqIDwgbGluZS5sZW5ndGg7ICsraikge1xuXHRcdFx0XHR2YXIgdGwgPSB0YWJMZW4obGluZVtqXSk7XG5cdFx0XHRcdGxlbiArPSB0bDtcblx0XHRcdFx0aWYgKGxlbiA+IGNoYXJhY3RlcnMpIHtcblx0XHRcdFx0XHRsaW5lW2pdID0gJ1xcbicgKyBsaW5lW2pdO1xuXHRcdFx0XHRcdGxlbiA9IHRsO1xuXHRcdFx0XHR9XG5cdFx0XHR9XG5cdFx0XHRsaW5lc1tpXSA9IGxpbmUuam9pbignJyk7XG5cdFx0fVxuXHRcdHJldHVybiBsaW5lcy5qb2luKCdcXG4nKTtcblx0fVxufTtcblxuLy8gU3VwcG9ydCBub2RlIG1vZHVsZXNcbmlmICh0eXBlb2YgbW9kdWxlICE9PSAndW5kZWZpbmVkJyAmJiBtb2R1bGUuZXhwb3J0cykge1xuXHRtb2R1bGUuZXhwb3J0cyA9IE5vcm1hbGl6ZVdoaXRlc3BhY2U7XG59XG5cbi8vIEV4aXQgaWYgcHJpc20gaXMgbm90IGxvYWRlZFxuaWYgKHR5cGVvZiBQcmlzbSA9PT0gJ3VuZGVmaW5lZCcpIHtcblx0cmV0dXJuO1xufVxuXG5QcmlzbS5wbHVnaW5zLk5vcm1hbGl6ZVdoaXRlc3BhY2UgPSBuZXcgTm9ybWFsaXplV2hpdGVzcGFjZSh7XG5cdCdyZW1vdmUtdHJhaWxpbmcnOiB0cnVlLFxuXHQncmVtb3ZlLWluZGVudCc6IHRydWUsXG5cdCdsZWZ0LXRyaW0nOiB0cnVlLFxuXHQncmlnaHQtdHJpbSc6IHRydWUsXG5cdC8qJ2JyZWFrLWxpbmVzJzogODAsXG5cdCdpbmRlbnQnOiAyLFxuXHQncmVtb3ZlLWluaXRpYWwtbGluZS1mZWVkJzogZmFsc2UsXG5cdCd0YWJzLXRvLXNwYWNlcyc6IDQsXG5cdCdzcGFjZXMtdG8tdGFicyc6IDQqL1xufSk7XG5cblByaXNtLmhvb2tzLmFkZCgnYmVmb3JlLXNhbml0eS1jaGVjaycsIGZ1bmN0aW9uIChlbnYpIHtcblx0dmFyIE5vcm1hbGl6ZXIgPSBQcmlzbS5wbHVnaW5zLk5vcm1hbGl6ZVdoaXRlc3BhY2U7XG5cblx0Ly8gQ2hlY2sgc2V0dGluZ3Ncblx0aWYgKGVudi5zZXR0aW5ncyAmJiBlbnYuc2V0dGluZ3NbJ3doaXRlc3BhY2Utbm9ybWFsaXphdGlvbiddID09PSBmYWxzZSkge1xuXHRcdHJldHVybjtcblx0fVxuXG5cdC8vIFNpbXBsZSBtb2RlIGlmIHRoZXJlIGlzIG5vIGVudi5lbGVtZW50XG5cdGlmICgoIWVudi5lbGVtZW50IHx8ICFlbnYuZWxlbWVudC5wYXJlbnROb2RlKSAmJiBlbnYuY29kZSkge1xuXHRcdGVudi5jb2RlID0gTm9ybWFsaXplci5ub3JtYWxpemUoZW52LmNvZGUsIGVudi5zZXR0aW5ncyk7XG5cdFx0cmV0dXJuO1xuXHR9XG5cblx0Ly8gTm9ybWFsIG1vZGVcblx0dmFyIHByZSA9IGVudi5lbGVtZW50LnBhcmVudE5vZGU7XG5cdHZhciBjbHNSZWcgPSAvXFxibm8td2hpdGVzcGFjZS1ub3JtYWxpemF0aW9uXFxiLztcblx0aWYgKCFlbnYuY29kZSB8fCAhcHJlIHx8IHByZS5ub2RlTmFtZS50b0xvd2VyQ2FzZSgpICE9PSAncHJlJyB8fFxuXHRcdFx0Y2xzUmVnLnRlc3QocHJlLmNsYXNzTmFtZSkgfHwgY2xzUmVnLnRlc3QoZW52LmVsZW1lbnQuY2xhc3NOYW1lKSlcblx0XHRyZXR1cm47XG5cblx0dmFyIGNoaWxkcmVuID0gcHJlLmNoaWxkTm9kZXMsXG5cdCAgICBiZWZvcmUgPSAnJyxcblx0ICAgIGFmdGVyID0gJycsXG5cdCAgICBjb2RlRm91bmQgPSBmYWxzZTtcblxuXHQvLyBNb3ZlIHN1cnJvdW5kaW5nIHdoaXRlc3BhY2UgZnJvbSB0aGUgPHByZT4gdGFnIGludG8gdGhlIDxjb2RlPiB0YWdcblx0Zm9yICh2YXIgaSA9IDA7IGkgPCBjaGlsZHJlbi5sZW5ndGg7ICsraSkge1xuXHRcdHZhciBub2RlID0gY2hpbGRyZW5baV07XG5cblx0XHRpZiAobm9kZSA9PSBlbnYuZWxlbWVudCkge1xuXHRcdFx0Y29kZUZvdW5kID0gdHJ1ZTtcblx0XHR9IGVsc2UgaWYgKG5vZGUubm9kZU5hbWUgPT09IFwiI3RleHRcIikge1xuXHRcdFx0aWYgKGNvZGVGb3VuZCkge1xuXHRcdFx0XHRhZnRlciArPSBub2RlLm5vZGVWYWx1ZTtcblx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdGJlZm9yZSArPSBub2RlLm5vZGVWYWx1ZTtcblx0XHRcdH1cblxuXHRcdFx0cHJlLnJlbW92ZUNoaWxkKG5vZGUpO1xuXHRcdFx0LS1pO1xuXHRcdH1cblx0fVxuXG5cdGlmICghZW52LmVsZW1lbnQuY2hpbGRyZW4ubGVuZ3RoIHx8ICFQcmlzbS5wbHVnaW5zLktlZXBNYXJrdXApIHtcblx0XHRlbnYuY29kZSA9IGJlZm9yZSArIGVudi5jb2RlICsgYWZ0ZXI7XG5cdFx0ZW52LmNvZGUgPSBOb3JtYWxpemVyLm5vcm1hbGl6ZShlbnYuY29kZSwgZW52LnNldHRpbmdzKTtcblx0fSBlbHNlIHtcblx0XHQvLyBQcmVzZXJ2ZSBtYXJrdXAgZm9yIGtlZXAtbWFya3VwIHBsdWdpblxuXHRcdHZhciBodG1sID0gYmVmb3JlICsgZW52LmVsZW1lbnQuaW5uZXJIVE1MICsgYWZ0ZXI7XG5cdFx0ZW52LmVsZW1lbnQuaW5uZXJIVE1MID0gTm9ybWFsaXplci5ub3JtYWxpemUoaHRtbCwgZW52LnNldHRpbmdzKTtcblx0XHRlbnYuY29kZSA9IGVudi5lbGVtZW50LnRleHRDb250ZW50O1xuXHR9XG59KTtcblxufSgpKTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9wcmlzbWpzL3BsdWdpbnMvbm9ybWFsaXplLXdoaXRlc3BhY2UvcHJpc20tbm9ybWFsaXplLXdoaXRlc3BhY2UuanNcbi8vIG1vZHVsZSBpZCA9IDEyMFxuLy8gbW9kdWxlIGNodW5rcyA9IDEwIiwiKGZ1bmN0aW9uKCl7XG5cbmlmIChcblx0dHlwZW9mIHNlbGYgIT09ICd1bmRlZmluZWQnICYmICFzZWxmLlByaXNtIHx8XG5cdHR5cGVvZiBnbG9iYWwgIT09ICd1bmRlZmluZWQnICYmICFnbG9iYWwuUHJpc21cbikge1xuXHRyZXR1cm47XG59XG5cblByaXNtLmhvb2tzLmFkZCgnd3JhcCcsIGZ1bmN0aW9uKGVudikge1xuXHRpZiAoZW52LnR5cGUgIT09IFwia2V5d29yZFwiKSB7XG5cdFx0cmV0dXJuO1xuXHR9XG5cdGVudi5jbGFzc2VzLnB1c2goJ2tleXdvcmQtJyArIGVudi5jb250ZW50KTtcbn0pO1xuXG59KSgpO1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvcHJpc21qcy9wbHVnaW5zL2hpZ2hsaWdodC1rZXl3b3Jkcy9wcmlzbS1oaWdobGlnaHQta2V5d29yZHMuanNcbi8vIG1vZHVsZSBpZCA9IDEyMVxuLy8gbW9kdWxlIGNodW5rcyA9IDEwIiwiKGZ1bmN0aW9uKCl7XG5cdGlmICh0eXBlb2Ygc2VsZiA9PT0gJ3VuZGVmaW5lZCcgfHwgIXNlbGYuUHJpc20gfHwgIXNlbGYuZG9jdW1lbnQpIHtcblx0XHRyZXR1cm47XG5cdH1cblxuXHR2YXIgY2FsbGJhY2tzID0gW107XG5cdHZhciBtYXAgPSB7fTtcblx0dmFyIG5vb3AgPSBmdW5jdGlvbigpIHt9O1xuXG5cdFByaXNtLnBsdWdpbnMudG9vbGJhciA9IHt9O1xuXG5cdC8qKlxuXHQgKiBSZWdpc3RlciBhIGJ1dHRvbiBjYWxsYmFjayB3aXRoIHRoZSB0b29sYmFyLlxuXHQgKlxuXHQgKiBAcGFyYW0ge3N0cmluZ30ga2V5XG5cdCAqIEBwYXJhbSB7T2JqZWN0fEZ1bmN0aW9ufSBvcHRzXG5cdCAqL1xuXHR2YXIgcmVnaXN0ZXJCdXR0b24gPSBQcmlzbS5wbHVnaW5zLnRvb2xiYXIucmVnaXN0ZXJCdXR0b24gPSBmdW5jdGlvbiAoa2V5LCBvcHRzKSB7XG5cdFx0dmFyIGNhbGxiYWNrO1xuXG5cdFx0aWYgKHR5cGVvZiBvcHRzID09PSAnZnVuY3Rpb24nKSB7XG5cdFx0XHRjYWxsYmFjayA9IG9wdHM7XG5cdFx0fSBlbHNlIHtcblx0XHRcdGNhbGxiYWNrID0gZnVuY3Rpb24gKGVudikge1xuXHRcdFx0XHR2YXIgZWxlbWVudDtcblxuXHRcdFx0XHRpZiAodHlwZW9mIG9wdHMub25DbGljayA9PT0gJ2Z1bmN0aW9uJykge1xuXHRcdFx0XHRcdGVsZW1lbnQgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdidXR0b24nKTtcblx0XHRcdFx0XHRlbGVtZW50LnR5cGUgPSAnYnV0dG9uJztcblx0XHRcdFx0XHRlbGVtZW50LmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgZnVuY3Rpb24gKCkge1xuXHRcdFx0XHRcdFx0b3B0cy5vbkNsaWNrLmNhbGwodGhpcywgZW52KTtcblx0XHRcdFx0XHR9KTtcblx0XHRcdFx0fSBlbHNlIGlmICh0eXBlb2Ygb3B0cy51cmwgPT09ICdzdHJpbmcnKSB7XG5cdFx0XHRcdFx0ZWxlbWVudCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2EnKTtcblx0XHRcdFx0XHRlbGVtZW50LmhyZWYgPSBvcHRzLnVybDtcblx0XHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0XHRlbGVtZW50ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnc3BhbicpO1xuXHRcdFx0XHR9XG5cblx0XHRcdFx0ZWxlbWVudC50ZXh0Q29udGVudCA9IG9wdHMudGV4dDtcblxuXHRcdFx0XHRyZXR1cm4gZWxlbWVudDtcblx0XHRcdH07XG5cdFx0fVxuXG5cdFx0Y2FsbGJhY2tzLnB1c2gobWFwW2tleV0gPSBjYWxsYmFjayk7XG5cdH07XG5cblx0LyoqXG5cdCAqIFBvc3QtaGlnaGxpZ2h0IFByaXNtIGhvb2sgY2FsbGJhY2suXG5cdCAqXG5cdCAqIEBwYXJhbSBlbnZcblx0ICovXG5cdHZhciBob29rID0gUHJpc20ucGx1Z2lucy50b29sYmFyLmhvb2sgPSBmdW5jdGlvbiAoZW52KSB7XG5cdFx0Ly8gQ2hlY2sgaWYgaW5saW5lIG9yIGFjdHVhbCBjb2RlIGJsb2NrIChjcmVkaXQgdG8gbGluZS1udW1iZXJzIHBsdWdpbilcblx0XHR2YXIgcHJlID0gZW52LmVsZW1lbnQucGFyZW50Tm9kZTtcblx0XHRpZiAoIXByZSB8fCAhL3ByZS9pLnRlc3QocHJlLm5vZGVOYW1lKSkge1xuXHRcdFx0cmV0dXJuO1xuXHRcdH1cblxuXHRcdC8vIEF1dG9sb2FkZXIgcmVoaWdobGlnaHRzLCBzbyBvbmx5IGRvIHRoaXMgb25jZS5cblx0XHRpZiAocHJlLnBhcmVudE5vZGUuY2xhc3NMaXN0LmNvbnRhaW5zKCdjb2RlLXRvb2xiYXInKSkge1xuXHRcdFx0cmV0dXJuO1xuXHRcdH1cblxuXHRcdC8vIENyZWF0ZSB3cmFwcGVyIGZvciA8cHJlPiB0byBwcmV2ZW50IHNjcm9sbGluZyB0b29sYmFyIHdpdGggY29udGVudFxuXHRcdHZhciB3cmFwcGVyID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcImRpdlwiKTtcblx0XHR3cmFwcGVyLmNsYXNzTGlzdC5hZGQoXCJjb2RlLXRvb2xiYXJcIik7XG5cdFx0cHJlLnBhcmVudE5vZGUuaW5zZXJ0QmVmb3JlKHdyYXBwZXIsIHByZSk7XG5cdFx0d3JhcHBlci5hcHBlbmRDaGlsZChwcmUpO1xuXG5cdFx0Ly8gU2V0dXAgdGhlIHRvb2xiYXJcblx0XHR2YXIgdG9vbGJhciA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2RpdicpO1xuXHRcdHRvb2xiYXIuY2xhc3NMaXN0LmFkZCgndG9vbGJhcicpO1xuXG5cdFx0aWYgKGRvY3VtZW50LmJvZHkuaGFzQXR0cmlidXRlKCdkYXRhLXRvb2xiYXItb3JkZXInKSkge1xuXHRcdFx0Y2FsbGJhY2tzID0gZG9jdW1lbnQuYm9keS5nZXRBdHRyaWJ1dGUoJ2RhdGEtdG9vbGJhci1vcmRlcicpLnNwbGl0KCcsJykubWFwKGZ1bmN0aW9uKGtleSkge1xuXHRcdFx0XHRyZXR1cm4gbWFwW2tleV0gfHwgbm9vcDtcblx0XHRcdH0pO1xuXHRcdH1cblxuXHRcdGNhbGxiYWNrcy5mb3JFYWNoKGZ1bmN0aW9uKGNhbGxiYWNrKSB7XG5cdFx0XHR2YXIgZWxlbWVudCA9IGNhbGxiYWNrKGVudik7XG5cblx0XHRcdGlmICghZWxlbWVudCkge1xuXHRcdFx0XHRyZXR1cm47XG5cdFx0XHR9XG5cblx0XHRcdHZhciBpdGVtID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnZGl2Jyk7XG5cdFx0XHRpdGVtLmNsYXNzTGlzdC5hZGQoJ3Rvb2xiYXItaXRlbScpO1xuXG5cdFx0XHRpdGVtLmFwcGVuZENoaWxkKGVsZW1lbnQpO1xuXHRcdFx0dG9vbGJhci5hcHBlbmRDaGlsZChpdGVtKTtcblx0XHR9KTtcblxuXHRcdC8vIEFkZCBvdXIgdG9vbGJhciB0byB0aGUgY3VycmVudGx5IGNyZWF0ZWQgd3JhcHBlciBvZiA8cHJlPiB0YWdcblx0XHR3cmFwcGVyLmFwcGVuZENoaWxkKHRvb2xiYXIpO1xuXHR9O1xuXG5cdHJlZ2lzdGVyQnV0dG9uKCdsYWJlbCcsIGZ1bmN0aW9uKGVudikge1xuXHRcdHZhciBwcmUgPSBlbnYuZWxlbWVudC5wYXJlbnROb2RlO1xuXHRcdGlmICghcHJlIHx8ICEvcHJlL2kudGVzdChwcmUubm9kZU5hbWUpKSB7XG5cdFx0XHRyZXR1cm47XG5cdFx0fVxuXG5cdFx0aWYgKCFwcmUuaGFzQXR0cmlidXRlKCdkYXRhLWxhYmVsJykpIHtcblx0XHRcdHJldHVybjtcblx0XHR9XG5cblx0XHR2YXIgZWxlbWVudCwgdGVtcGxhdGU7XG5cdFx0dmFyIHRleHQgPSBwcmUuZ2V0QXR0cmlidXRlKCdkYXRhLWxhYmVsJyk7XG5cdFx0dHJ5IHtcblx0XHRcdC8vIEFueSBub3JtYWwgdGV4dCB3aWxsIGJsb3cgdXAgdGhpcyBzZWxlY3Rvci5cblx0XHRcdHRlbXBsYXRlID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcigndGVtcGxhdGUjJyArIHRleHQpO1xuXHRcdH0gY2F0Y2ggKGUpIHt9XG5cblx0XHRpZiAodGVtcGxhdGUpIHtcblx0XHRcdGVsZW1lbnQgPSB0ZW1wbGF0ZS5jb250ZW50O1xuXHRcdH0gZWxzZSB7XG5cdFx0XHRpZiAocHJlLmhhc0F0dHJpYnV0ZSgnZGF0YS11cmwnKSkge1xuXHRcdFx0XHRlbGVtZW50ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnYScpO1xuXHRcdFx0XHRlbGVtZW50LmhyZWYgPSBwcmUuZ2V0QXR0cmlidXRlKCdkYXRhLXVybCcpO1xuXHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0ZWxlbWVudCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ3NwYW4nKTtcblx0XHRcdH1cblxuXHRcdFx0ZWxlbWVudC50ZXh0Q29udGVudCA9IHRleHQ7XG5cdFx0fVxuXG5cdFx0cmV0dXJuIGVsZW1lbnQ7XG5cdH0pO1xuXG5cdC8qKlxuXHQgKiBSZWdpc3RlciB0aGUgdG9vbGJhciB3aXRoIFByaXNtLlxuXHQgKi9cblx0UHJpc20uaG9va3MuYWRkKCdjb21wbGV0ZScsIGhvb2spO1xufSkoKTtcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3ByaXNtanMvcGx1Z2lucy90b29sYmFyL3ByaXNtLXRvb2xiYXIuanNcbi8vIG1vZHVsZSBpZCA9IDEyMlxuLy8gbW9kdWxlIGNodW5rcyA9IDEwIiwiLy8gc3R5bGUtbG9hZGVyOiBBZGRzIHNvbWUgY3NzIHRvIHRoZSBET00gYnkgYWRkaW5nIGEgPHN0eWxlPiB0YWdcblxuLy8gbG9hZCB0aGUgc3R5bGVzXG52YXIgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tMS0xIS4uLy4uLy4uL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS0xLTIhLi9wcmlzbS10b29sYmFyLmNzc1wiKTtcbmlmKHR5cGVvZiBjb250ZW50ID09PSAnc3RyaW5nJykgY29udGVudCA9IFtbbW9kdWxlLmlkLCBjb250ZW50LCAnJ11dO1xuLy8gUHJlcGFyZSBjc3NUcmFuc2Zvcm1hdGlvblxudmFyIHRyYW5zZm9ybTtcblxudmFyIG9wdGlvbnMgPSB7XCJpbnNlcnRBdFwiOntcImJlZm9yZVwiOlwiI3dlYnBhY2stc3R5bGUtbG9hZGVyLWluc2VydC1iZWZvcmUtdGhpc1wifSxcImhtclwiOnRydWV9XG5vcHRpb25zLnRyYW5zZm9ybSA9IHRyYW5zZm9ybVxuLy8gYWRkIHRoZSBzdHlsZXMgdG8gdGhlIERPTVxudmFyIHVwZGF0ZSA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vc3R5bGUtbG9hZGVyL2xpYi9hZGRTdHlsZXMuanNcIikoY29udGVudCwgb3B0aW9ucyk7XG5pZihjb250ZW50LmxvY2FscykgbW9kdWxlLmV4cG9ydHMgPSBjb250ZW50LmxvY2Fscztcbi8vIEhvdCBNb2R1bGUgUmVwbGFjZW1lbnRcbmlmKG1vZHVsZS5ob3QpIHtcblx0Ly8gV2hlbiB0aGUgc3R5bGVzIGNoYW5nZSwgdXBkYXRlIHRoZSA8c3R5bGU+IHRhZ3Ncblx0aWYoIWNvbnRlbnQubG9jYWxzKSB7XG5cdFx0bW9kdWxlLmhvdC5hY2NlcHQoXCIhIS4uLy4uLy4uL2Nzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tMS0xIS4uLy4uLy4uL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS0xLTIhLi9wcmlzbS10b29sYmFyLmNzc1wiLCBmdW5jdGlvbigpIHtcblx0XHRcdHZhciBuZXdDb250ZW50ID0gcmVxdWlyZShcIiEhLi4vLi4vLi4vY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS0xLTEhLi4vLi4vLi4vcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTEtMiEuL3ByaXNtLXRvb2xiYXIuY3NzXCIpO1xuXHRcdFx0aWYodHlwZW9mIG5ld0NvbnRlbnQgPT09ICdzdHJpbmcnKSBuZXdDb250ZW50ID0gW1ttb2R1bGUuaWQsIG5ld0NvbnRlbnQsICcnXV07XG5cdFx0XHR1cGRhdGUobmV3Q29udGVudCk7XG5cdFx0fSk7XG5cdH1cblx0Ly8gV2hlbiB0aGUgbW9kdWxlIGlzIGRpc3Bvc2VkLCByZW1vdmUgdGhlIDxzdHlsZT4gdGFnc1xuXHRtb2R1bGUuaG90LmRpc3Bvc2UoZnVuY3Rpb24oKSB7IHVwZGF0ZSgpOyB9KTtcbn1cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9wcmlzbWpzL3BsdWdpbnMvdG9vbGJhci9wcmlzbS10b29sYmFyLmNzc1xuLy8gbW9kdWxlIGlkID0gMTIzXG4vLyBtb2R1bGUgY2h1bmtzID0gMTAiLCJleHBvcnRzID0gbW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiLi4vLi4vLi4vY3NzLWxvYWRlci9saWIvY3NzLWJhc2UuanNcIikoZmFsc2UpO1xuLy8gaW1wb3J0c1xuXG5cbi8vIG1vZHVsZVxuZXhwb3J0cy5wdXNoKFttb2R1bGUuaWQsIFwiZGl2LmNvZGUtdG9vbGJhciB7XFxuXFx0cG9zaXRpb246IHJlbGF0aXZlO1xcbn1cXG5cXG5kaXYuY29kZS10b29sYmFyID4gLnRvb2xiYXIge1xcblxcdHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG5cXHR0b3A6IC4zZW07XFxuXFx0cmlnaHQ6IC4yZW07XFxuXFx0dHJhbnNpdGlvbjogb3BhY2l0eSAwLjNzIGVhc2UtaW4tb3V0O1xcblxcdG9wYWNpdHk6IDA7XFxufVxcblxcbmRpdi5jb2RlLXRvb2xiYXI6aG92ZXIgPiAudG9vbGJhciB7XFxuXFx0b3BhY2l0eTogMTtcXG59XFxuXFxuZGl2LmNvZGUtdG9vbGJhciA+IC50b29sYmFyIC50b29sYmFyLWl0ZW0ge1xcblxcdGRpc3BsYXk6IGlubGluZS1ibG9jaztcXG59XFxuXFxuZGl2LmNvZGUtdG9vbGJhciA+IC50b29sYmFyIGEge1xcblxcdGN1cnNvcjogcG9pbnRlcjtcXG59XFxuXFxuZGl2LmNvZGUtdG9vbGJhciA+IC50b29sYmFyIGJ1dHRvbiB7XFxuXFx0YmFja2dyb3VuZDogbm9uZTtcXG5cXHRib3JkZXI6IDA7XFxuXFx0Y29sb3I6IGluaGVyaXQ7XFxuXFx0Zm9udDogaW5oZXJpdDtcXG5cXHRsaW5lLWhlaWdodDogbm9ybWFsO1xcblxcdG92ZXJmbG93OiB2aXNpYmxlO1xcblxcdHBhZGRpbmc6IDA7XFxuXFx0LXdlYmtpdC11c2VyLXNlbGVjdDogbm9uZTsgLyogZm9yIGJ1dHRvbiAqL1xcblxcdC1tb3otdXNlci1zZWxlY3Q6IG5vbmU7XFxuXFx0LW1zLXVzZXItc2VsZWN0OiBub25lO1xcbn1cXG5cXG5kaXYuY29kZS10b29sYmFyID4gLnRvb2xiYXIgYSxcXG5kaXYuY29kZS10b29sYmFyID4gLnRvb2xiYXIgYnV0dG9uLFxcbmRpdi5jb2RlLXRvb2xiYXIgPiAudG9vbGJhciBzcGFuIHtcXG5cXHRjb2xvcjogI2JiYjtcXG5cXHRmb250LXNpemU6IC44ZW07XFxuXFx0cGFkZGluZzogMCAuNWVtO1xcblxcdGJhY2tncm91bmQ6ICNmNWYyZjA7XFxuXFx0YmFja2dyb3VuZDogcmdiYSgyMjQsIDIyNCwgMjI0LCAwLjIpO1xcblxcdGJveC1zaGFkb3c6IDAgMnB4IDAgMCByZ2JhKDAsMCwwLDAuMik7XFxuXFx0Ym9yZGVyLXJhZGl1czogLjVlbTtcXG59XFxuXFxuZGl2LmNvZGUtdG9vbGJhciA+IC50b29sYmFyIGE6aG92ZXIsXFxuZGl2LmNvZGUtdG9vbGJhciA+IC50b29sYmFyIGE6Zm9jdXMsXFxuZGl2LmNvZGUtdG9vbGJhciA+IC50b29sYmFyIGJ1dHRvbjpob3ZlcixcXG5kaXYuY29kZS10b29sYmFyID4gLnRvb2xiYXIgYnV0dG9uOmZvY3VzLFxcbmRpdi5jb2RlLXRvb2xiYXIgPiAudG9vbGJhciBzcGFuOmhvdmVyLFxcbmRpdi5jb2RlLXRvb2xiYXIgPiAudG9vbGJhciBzcGFuOmZvY3VzIHtcXG5cXHRjb2xvcjogaW5oZXJpdDtcXG5cXHR0ZXh0LWRlY29yYXRpb246IG5vbmU7XFxufVxcblwiLCBcIlwiXSk7XG5cbi8vIGV4cG9ydHNcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXI/P3JlZi0tMS0xIS4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYj8/cmVmLS0xLTIhLi9ub2RlX21vZHVsZXMvcHJpc21qcy9wbHVnaW5zL3Rvb2xiYXIvcHJpc20tdG9vbGJhci5jc3Ncbi8vIG1vZHVsZSBpZCA9IDEyNFxuLy8gbW9kdWxlIGNodW5rcyA9IDEwIiwiKGZ1bmN0aW9uKCl7XG5cbmlmICh0eXBlb2Ygc2VsZiA9PT0gJ3VuZGVmaW5lZCcgfHwgIXNlbGYuUHJpc20gfHwgIXNlbGYuZG9jdW1lbnQpIHtcblx0cmV0dXJuO1xufVxuXG5pZiAoIVByaXNtLnBsdWdpbnMudG9vbGJhcikge1xuXHRjb25zb2xlLndhcm4oJ1Nob3cgTGFuZ3VhZ2VzIHBsdWdpbiBsb2FkZWQgYmVmb3JlIFRvb2xiYXIgcGx1Z2luLicpO1xuXG5cdHJldHVybjtcbn1cblxuLy8gVGhlIGxhbmd1YWdlcyBtYXAgaXMgYnVpbHQgYXV0b21hdGljYWxseSB3aXRoIGd1bHBcbnZhciBMYW5ndWFnZXMgPSAvKmxhbmd1YWdlc19wbGFjZWhvbGRlclsqL3tcImh0bWxcIjpcIkhUTUxcIixcInhtbFwiOlwiWE1MXCIsXCJzdmdcIjpcIlNWR1wiLFwibWF0aG1sXCI6XCJNYXRoTUxcIixcImNzc1wiOlwiQ1NTXCIsXCJjbGlrZVwiOlwiQy1saWtlXCIsXCJqYXZhc2NyaXB0XCI6XCJKYXZhU2NyaXB0XCIsXCJhYmFwXCI6XCJBQkFQXCIsXCJhY3Rpb25zY3JpcHRcIjpcIkFjdGlvblNjcmlwdFwiLFwiYXBhY2hlY29uZlwiOlwiQXBhY2hlIENvbmZpZ3VyYXRpb25cIixcImFwbFwiOlwiQVBMXCIsXCJhcHBsZXNjcmlwdFwiOlwiQXBwbGVTY3JpcHRcIixcImFyZmZcIjpcIkFSRkZcIixcImFzY2lpZG9jXCI6XCJBc2NpaURvY1wiLFwiYXNtNjUwMlwiOlwiNjUwMiBBc3NlbWJseVwiLFwiYXNwbmV0XCI6XCJBU1AuTkVUIChDIylcIixcImF1dG9ob3RrZXlcIjpcIkF1dG9Ib3RrZXlcIixcImF1dG9pdFwiOlwiQXV0b0l0XCIsXCJiYXNpY1wiOlwiQkFTSUNcIixcImNzaGFycFwiOlwiQyNcIixcImNwcFwiOlwiQysrXCIsXCJjb2ZmZWVzY3JpcHRcIjpcIkNvZmZlZVNjcmlwdFwiLFwiY3NwXCI6XCJDb250ZW50LVNlY3VyaXR5LVBvbGljeVwiLFwiY3NzLWV4dHJhc1wiOlwiQ1NTIEV4dHJhc1wiLFwiZGphbmdvXCI6XCJEamFuZ28vSmluamEyXCIsXCJlcmJcIjpcIkVSQlwiLFwiZnNoYXJwXCI6XCJGI1wiLFwiZ2xzbFwiOlwiR0xTTFwiLFwiZ3JhcGhxbFwiOlwiR3JhcGhRTFwiLFwiaHR0cFwiOlwiSFRUUFwiLFwiaHBrcFwiOlwiSFRUUCBQdWJsaWMtS2V5LVBpbnNcIixcImhzdHNcIjpcIkhUVFAgU3RyaWN0LVRyYW5zcG9ydC1TZWN1cml0eVwiLFwiaWNoaWdvamFtXCI6XCJJY2hpZ29KYW1cIixcImluZm9ybTdcIjpcIkluZm9ybSA3XCIsXCJqc29uXCI6XCJKU09OXCIsXCJsYXRleFwiOlwiTGFUZVhcIixcImxpdmVzY3JpcHRcIjpcIkxpdmVTY3JpcHRcIixcImxvbGNvZGVcIjpcIkxPTENPREVcIixcIm1hdGxhYlwiOlwiTUFUTEFCXCIsXCJtZWxcIjpcIk1FTFwiLFwibjRqc1wiOlwiTjRKU1wiLFwibmFzbVwiOlwiTkFTTVwiLFwibmdpbnhcIjpcIm5naW54XCIsXCJuc2lzXCI6XCJOU0lTXCIsXCJvYmplY3RpdmVjXCI6XCJPYmplY3RpdmUtQ1wiLFwib2NhbWxcIjpcIk9DYW1sXCIsXCJvcGVuY2xcIjpcIk9wZW5DTFwiLFwicGFyaWdwXCI6XCJQQVJJL0dQXCIsXCJwaHBcIjpcIlBIUFwiLFwicGhwLWV4dHJhc1wiOlwiUEhQIEV4dHJhc1wiLFwicGxzcWxcIjpcIlBML1NRTFwiLFwicG93ZXJzaGVsbFwiOlwiUG93ZXJTaGVsbFwiLFwicHJvcGVydGllc1wiOlwiLnByb3BlcnRpZXNcIixcInByb3RvYnVmXCI6XCJQcm90b2NvbCBCdWZmZXJzXCIsXCJxXCI6XCJRIChrZGIrIGRhdGFiYXNlKVwiLFwianN4XCI6XCJSZWFjdCBKU1hcIixcInRzeFwiOlwiUmVhY3QgVFNYXCIsXCJyZW5weVwiOlwiUmVuJ3B5XCIsXCJyZXN0XCI6XCJyZVNUIChyZVN0cnVjdHVyZWRUZXh0KVwiLFwic2FzXCI6XCJTQVNcIixcInNhc3NcIjpcIlNhc3MgKFNhc3MpXCIsXCJzY3NzXCI6XCJTYXNzIChTY3NzKVwiLFwic3FsXCI6XCJTUUxcIixcInR5cGVzY3JpcHRcIjpcIlR5cGVTY3JpcHRcIixcInZibmV0XCI6XCJWQi5OZXRcIixcInZoZGxcIjpcIlZIRExcIixcInZpbVwiOlwidmltXCIsXCJ3aWtpXCI6XCJXaWtpIG1hcmt1cFwiLFwieG9qb1wiOlwiWG9qbyAoUkVBTGJhc2ljKVwiLFwieWFtbFwiOlwiWUFNTFwifS8qXSovO1xuUHJpc20ucGx1Z2lucy50b29sYmFyLnJlZ2lzdGVyQnV0dG9uKCdzaG93LWxhbmd1YWdlJywgZnVuY3Rpb24oZW52KSB7XG5cdHZhciBwcmUgPSBlbnYuZWxlbWVudC5wYXJlbnROb2RlO1xuXHRpZiAoIXByZSB8fCAhL3ByZS9pLnRlc3QocHJlLm5vZGVOYW1lKSkge1xuXHRcdHJldHVybjtcblx0fVxuXHR2YXIgbGFuZ3VhZ2UgPSBwcmUuZ2V0QXR0cmlidXRlKCdkYXRhLWxhbmd1YWdlJykgfHwgTGFuZ3VhZ2VzW2Vudi5sYW5ndWFnZV0gfHwgKGVudi5sYW5ndWFnZS5zdWJzdHJpbmcoMCwgMSkudG9VcHBlckNhc2UoKSArIGVudi5sYW5ndWFnZS5zdWJzdHJpbmcoMSkpO1xuXG5cdHZhciBlbGVtZW50ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnc3BhbicpO1xuXHRlbGVtZW50LnRleHRDb250ZW50ID0gbGFuZ3VhZ2U7XG5cblx0cmV0dXJuIGVsZW1lbnQ7XG59KTtcblxufSkoKTtcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3ByaXNtanMvcGx1Z2lucy9zaG93LWxhbmd1YWdlL3ByaXNtLXNob3ctbGFuZ3VhZ2UuanNcbi8vIG1vZHVsZSBpZCA9IDEyNVxuLy8gbW9kdWxlIGNodW5rcyA9IDEwIiwiKGZ1bmN0aW9uKCl7XG5cdGlmICh0eXBlb2Ygc2VsZiA9PT0gJ3VuZGVmaW5lZCcgfHwgIXNlbGYuUHJpc20gfHwgIXNlbGYuZG9jdW1lbnQpIHtcblx0XHRyZXR1cm47XG5cdH1cblxuXHRpZiAoIVByaXNtLnBsdWdpbnMudG9vbGJhcikge1xuXHRcdGNvbnNvbGUud2FybignQ29weSB0byBDbGlwYm9hcmQgcGx1Z2luIGxvYWRlZCBiZWZvcmUgVG9vbGJhciBwbHVnaW4uJyk7XG5cblx0XHRyZXR1cm47XG5cdH1cblxuXHR2YXIgQ2xpcGJvYXJkSlMgPSB3aW5kb3cuQ2xpcGJvYXJkSlMgfHwgdW5kZWZpbmVkO1xuXG5cdGlmICghQ2xpcGJvYXJkSlMgJiYgdHlwZW9mIHJlcXVpcmUgPT09ICdmdW5jdGlvbicpIHtcblx0XHRDbGlwYm9hcmRKUyA9IHJlcXVpcmUoJ2NsaXBib2FyZCcpO1xuXHR9XG5cblx0dmFyIGNhbGxiYWNrcyA9IFtdO1xuXG5cdGlmICghQ2xpcGJvYXJkSlMpIHtcblx0XHR2YXIgc2NyaXB0ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnc2NyaXB0Jyk7XG5cdFx0dmFyIGhlYWQgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCdoZWFkJyk7XG5cblx0XHRzY3JpcHQub25sb2FkID0gZnVuY3Rpb24oKSB7XG5cdFx0XHRDbGlwYm9hcmRKUyA9IHdpbmRvdy5DbGlwYm9hcmRKUztcblxuXHRcdFx0aWYgKENsaXBib2FyZEpTKSB7XG5cdFx0XHRcdHdoaWxlIChjYWxsYmFja3MubGVuZ3RoKSB7XG5cdFx0XHRcdFx0Y2FsbGJhY2tzLnBvcCgpKCk7XG5cdFx0XHRcdH1cblx0XHRcdH1cblx0XHR9O1xuXG5cdFx0c2NyaXB0LnNyYyA9ICdodHRwczovL2NkbmpzLmNsb3VkZmxhcmUuY29tL2FqYXgvbGlicy9jbGlwYm9hcmQuanMvMi4wLjAvY2xpcGJvYXJkLm1pbi5qcyc7XG5cdFx0aGVhZC5hcHBlbmRDaGlsZChzY3JpcHQpO1xuXHR9XG5cblx0UHJpc20ucGx1Z2lucy50b29sYmFyLnJlZ2lzdGVyQnV0dG9uKCdjb3B5LXRvLWNsaXBib2FyZCcsIGZ1bmN0aW9uIChlbnYpIHtcblx0XHR2YXIgbGlua0NvcHkgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdhJyk7XG5cdFx0bGlua0NvcHkudGV4dENvbnRlbnQgPSAnQ29weSc7XG5cblx0XHRpZiAoIUNsaXBib2FyZEpTKSB7XG5cdFx0XHRjYWxsYmFja3MucHVzaChyZWdpc3RlckNsaXBib2FyZCk7XG5cdFx0fSBlbHNlIHtcblx0XHRcdHJlZ2lzdGVyQ2xpcGJvYXJkKCk7XG5cdFx0fVxuXG5cdFx0cmV0dXJuIGxpbmtDb3B5O1xuXG5cdFx0ZnVuY3Rpb24gcmVnaXN0ZXJDbGlwYm9hcmQoKSB7XG5cdFx0XHR2YXIgY2xpcCA9IG5ldyBDbGlwYm9hcmRKUyhsaW5rQ29weSwge1xuXHRcdFx0XHQndGV4dCc6IGZ1bmN0aW9uICgpIHtcblx0XHRcdFx0XHRyZXR1cm4gZW52LmNvZGU7XG5cdFx0XHRcdH1cblx0XHRcdH0pO1xuXG5cdFx0XHRjbGlwLm9uKCdzdWNjZXNzJywgZnVuY3Rpb24oKSB7XG5cdFx0XHRcdGxpbmtDb3B5LnRleHRDb250ZW50ID0gJ0NvcGllZCEnO1xuXG5cdFx0XHRcdHJlc2V0VGV4dCgpO1xuXHRcdFx0fSk7XG5cdFx0XHRjbGlwLm9uKCdlcnJvcicsIGZ1bmN0aW9uICgpIHtcblx0XHRcdFx0bGlua0NvcHkudGV4dENvbnRlbnQgPSAnUHJlc3MgQ3RybCtDIHRvIGNvcHknO1xuXG5cdFx0XHRcdHJlc2V0VGV4dCgpO1xuXHRcdFx0fSk7XG5cdFx0fVxuXG5cdFx0ZnVuY3Rpb24gcmVzZXRUZXh0KCkge1xuXHRcdFx0c2V0VGltZW91dChmdW5jdGlvbiAoKSB7XG5cdFx0XHRcdGxpbmtDb3B5LnRleHRDb250ZW50ID0gJ0NvcHknO1xuXHRcdFx0fSwgNTAwMCk7XG5cdFx0fVxuXHR9KTtcbn0pKCk7XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9wcmlzbWpzL3BsdWdpbnMvY29weS10by1jbGlwYm9hcmQvcHJpc20tY29weS10by1jbGlwYm9hcmQuanNcbi8vIG1vZHVsZSBpZCA9IDEyNlxuLy8gbW9kdWxlIGNodW5rcyA9IDEwIiwiLyohXG4gKiBjbGlwYm9hcmQuanMgdjIuMC4wXG4gKiBodHRwczovL3plbm9yb2NoYS5naXRodWIuaW8vY2xpcGJvYXJkLmpzXG4gKiBcbiAqIExpY2Vuc2VkIE1JVCDCqSBaZW5vIFJvY2hhXG4gKi9cbihmdW5jdGlvbiB3ZWJwYWNrVW5pdmVyc2FsTW9kdWxlRGVmaW5pdGlvbihyb290LCBmYWN0b3J5KSB7XG5cdGlmKHR5cGVvZiBleHBvcnRzID09PSAnb2JqZWN0JyAmJiB0eXBlb2YgbW9kdWxlID09PSAnb2JqZWN0Jylcblx0XHRtb2R1bGUuZXhwb3J0cyA9IGZhY3RvcnkoKTtcblx0ZWxzZSBpZih0eXBlb2YgZGVmaW5lID09PSAnZnVuY3Rpb24nICYmIGRlZmluZS5hbWQpXG5cdFx0ZGVmaW5lKFtdLCBmYWN0b3J5KTtcblx0ZWxzZSBpZih0eXBlb2YgZXhwb3J0cyA9PT0gJ29iamVjdCcpXG5cdFx0ZXhwb3J0c1tcIkNsaXBib2FyZEpTXCJdID0gZmFjdG9yeSgpO1xuXHRlbHNlXG5cdFx0cm9vdFtcIkNsaXBib2FyZEpTXCJdID0gZmFjdG9yeSgpO1xufSkodGhpcywgZnVuY3Rpb24oKSB7XG5yZXR1cm4gLyoqKioqKi8gKGZ1bmN0aW9uKG1vZHVsZXMpIHsgLy8gd2VicGFja0Jvb3RzdHJhcFxuLyoqKioqKi8gXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4vKioqKioqLyBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG4vKioqKioqL1xuLyoqKioqKi8gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuLyoqKioqKi8gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG4vKioqKioqL1xuLyoqKioqKi8gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuLyoqKioqKi8gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4vKioqKioqLyBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0cztcbi8qKioqKiovIFx0XHR9XG4vKioqKioqLyBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbi8qKioqKiovIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4vKioqKioqLyBcdFx0XHRpOiBtb2R1bGVJZCxcbi8qKioqKiovIFx0XHRcdGw6IGZhbHNlLFxuLyoqKioqKi8gXHRcdFx0ZXhwb3J0czoge31cbi8qKioqKiovIFx0XHR9O1xuLyoqKioqKi9cbi8qKioqKiovIFx0XHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cbi8qKioqKiovIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcbi8qKioqKiovXG4vKioqKioqLyBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuLyoqKioqKi8gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcbi8qKioqKiovXG4vKioqKioqLyBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbi8qKioqKiovIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4vKioqKioqLyBcdH1cbi8qKioqKiovXG4vKioqKioqL1xuLyoqKioqKi8gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuLyoqKioqKi8gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuLyoqKioqKi9cbi8qKioqKiovIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbi8qKioqKiovIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcbi8qKioqKiovXG4vKioqKioqLyBcdC8vIGlkZW50aXR5IGZ1bmN0aW9uIGZvciBjYWxsaW5nIGhhcm1vbnkgaW1wb3J0cyB3aXRoIHRoZSBjb3JyZWN0IGNvbnRleHRcbi8qKioqKiovIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5pID0gZnVuY3Rpb24odmFsdWUpIHsgcmV0dXJuIHZhbHVlOyB9O1xuLyoqKioqKi9cbi8qKioqKiovIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4vKioqKioqLyBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuLyoqKioqKi8gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbi8qKioqKiovIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7XG4vKioqKioqLyBcdFx0XHRcdGNvbmZpZ3VyYWJsZTogZmFsc2UsXG4vKioqKioqLyBcdFx0XHRcdGVudW1lcmFibGU6IHRydWUsXG4vKioqKioqLyBcdFx0XHRcdGdldDogZ2V0dGVyXG4vKioqKioqLyBcdFx0XHR9KTtcbi8qKioqKiovIFx0XHR9XG4vKioqKioqLyBcdH07XG4vKioqKioqL1xuLyoqKioqKi8gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuLyoqKioqKi8gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbi8qKioqKiovIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbi8qKioqKiovIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4vKioqKioqLyBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuLyoqKioqKi8gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbi8qKioqKiovIFx0XHRyZXR1cm4gZ2V0dGVyO1xuLyoqKioqKi8gXHR9O1xuLyoqKioqKi9cbi8qKioqKiovIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4vKioqKioqLyBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcbi8qKioqKiovXG4vKioqKioqLyBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4vKioqKioqLyBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG4vKioqKioqL1xuLyoqKioqKi8gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbi8qKioqKiovIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gMyk7XG4vKioqKioqLyB9KVxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cbi8qKioqKiovIChbXG4vKiAwICovXG4vKioqLyAoZnVuY3Rpb24obW9kdWxlLCBleHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKSB7XG5cbnZhciBfX1dFQlBBQ0tfQU1EX0RFRklORV9GQUNUT1JZX18sIF9fV0VCUEFDS19BTURfREVGSU5FX0FSUkFZX18sIF9fV0VCUEFDS19BTURfREVGSU5FX1JFU1VMVF9fOyhmdW5jdGlvbiAoZ2xvYmFsLCBmYWN0b3J5KSB7XG4gICAgaWYgKHRydWUpIHtcbiAgICAgICAgIShfX1dFQlBBQ0tfQU1EX0RFRklORV9BUlJBWV9fID0gW21vZHVsZSwgX193ZWJwYWNrX3JlcXVpcmVfXyg3KV0sIF9fV0VCUEFDS19BTURfREVGSU5FX0ZBQ1RPUllfXyA9IChmYWN0b3J5KSxcblx0XHRcdFx0X19XRUJQQUNLX0FNRF9ERUZJTkVfUkVTVUxUX18gPSAodHlwZW9mIF9fV0VCUEFDS19BTURfREVGSU5FX0ZBQ1RPUllfXyA9PT0gJ2Z1bmN0aW9uJyA/XG5cdFx0XHRcdChfX1dFQlBBQ0tfQU1EX0RFRklORV9GQUNUT1JZX18uYXBwbHkoZXhwb3J0cywgX19XRUJQQUNLX0FNRF9ERUZJTkVfQVJSQVlfXykpIDogX19XRUJQQUNLX0FNRF9ERUZJTkVfRkFDVE9SWV9fKSxcblx0XHRcdFx0X19XRUJQQUNLX0FNRF9ERUZJTkVfUkVTVUxUX18gIT09IHVuZGVmaW5lZCAmJiAobW9kdWxlLmV4cG9ydHMgPSBfX1dFQlBBQ0tfQU1EX0RFRklORV9SRVNVTFRfXykpO1xuICAgIH0gZWxzZSBpZiAodHlwZW9mIGV4cG9ydHMgIT09IFwidW5kZWZpbmVkXCIpIHtcbiAgICAgICAgZmFjdG9yeShtb2R1bGUsIHJlcXVpcmUoJ3NlbGVjdCcpKTtcbiAgICB9IGVsc2Uge1xuICAgICAgICB2YXIgbW9kID0ge1xuICAgICAgICAgICAgZXhwb3J0czoge31cbiAgICAgICAgfTtcbiAgICAgICAgZmFjdG9yeShtb2QsIGdsb2JhbC5zZWxlY3QpO1xuICAgICAgICBnbG9iYWwuY2xpcGJvYXJkQWN0aW9uID0gbW9kLmV4cG9ydHM7XG4gICAgfVxufSkodGhpcywgZnVuY3Rpb24gKG1vZHVsZSwgX3NlbGVjdCkge1xuICAgICd1c2Ugc3RyaWN0JztcblxuICAgIHZhciBfc2VsZWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3NlbGVjdCk7XG5cbiAgICBmdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikge1xuICAgICAgICByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDoge1xuICAgICAgICAgICAgZGVmYXVsdDogb2JqXG4gICAgICAgIH07XG4gICAgfVxuXG4gICAgdmFyIF90eXBlb2YgPSB0eXBlb2YgU3ltYm9sID09PSBcImZ1bmN0aW9uXCIgJiYgdHlwZW9mIFN5bWJvbC5pdGVyYXRvciA9PT0gXCJzeW1ib2xcIiA/IGZ1bmN0aW9uIChvYmopIHtcbiAgICAgICAgcmV0dXJuIHR5cGVvZiBvYmo7XG4gICAgfSA6IGZ1bmN0aW9uIChvYmopIHtcbiAgICAgICAgcmV0dXJuIG9iaiAmJiB0eXBlb2YgU3ltYm9sID09PSBcImZ1bmN0aW9uXCIgJiYgb2JqLmNvbnN0cnVjdG9yID09PSBTeW1ib2wgJiYgb2JqICE9PSBTeW1ib2wucHJvdG90eXBlID8gXCJzeW1ib2xcIiA6IHR5cGVvZiBvYmo7XG4gICAgfTtcblxuICAgIGZ1bmN0aW9uIF9jbGFzc0NhbGxDaGVjayhpbnN0YW5jZSwgQ29uc3RydWN0b3IpIHtcbiAgICAgICAgaWYgKCEoaW5zdGFuY2UgaW5zdGFuY2VvZiBDb25zdHJ1Y3RvcikpIHtcbiAgICAgICAgICAgIHRocm93IG5ldyBUeXBlRXJyb3IoXCJDYW5ub3QgY2FsbCBhIGNsYXNzIGFzIGEgZnVuY3Rpb25cIik7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICB2YXIgX2NyZWF0ZUNsYXNzID0gZnVuY3Rpb24gKCkge1xuICAgICAgICBmdW5jdGlvbiBkZWZpbmVQcm9wZXJ0aWVzKHRhcmdldCwgcHJvcHMpIHtcbiAgICAgICAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgcHJvcHMubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgICAgICB2YXIgZGVzY3JpcHRvciA9IHByb3BzW2ldO1xuICAgICAgICAgICAgICAgIGRlc2NyaXB0b3IuZW51bWVyYWJsZSA9IGRlc2NyaXB0b3IuZW51bWVyYWJsZSB8fCBmYWxzZTtcbiAgICAgICAgICAgICAgICBkZXNjcmlwdG9yLmNvbmZpZ3VyYWJsZSA9IHRydWU7XG4gICAgICAgICAgICAgICAgaWYgKFwidmFsdWVcIiBpbiBkZXNjcmlwdG9yKSBkZXNjcmlwdG9yLndyaXRhYmxlID0gdHJ1ZTtcbiAgICAgICAgICAgICAgICBPYmplY3QuZGVmaW5lUHJvcGVydHkodGFyZ2V0LCBkZXNjcmlwdG9yLmtleSwgZGVzY3JpcHRvcik7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gZnVuY3Rpb24gKENvbnN0cnVjdG9yLCBwcm90b1Byb3BzLCBzdGF0aWNQcm9wcykge1xuICAgICAgICAgICAgaWYgKHByb3RvUHJvcHMpIGRlZmluZVByb3BlcnRpZXMoQ29uc3RydWN0b3IucHJvdG90eXBlLCBwcm90b1Byb3BzKTtcbiAgICAgICAgICAgIGlmIChzdGF0aWNQcm9wcykgZGVmaW5lUHJvcGVydGllcyhDb25zdHJ1Y3Rvciwgc3RhdGljUHJvcHMpO1xuICAgICAgICAgICAgcmV0dXJuIENvbnN0cnVjdG9yO1xuICAgICAgICB9O1xuICAgIH0oKTtcblxuICAgIHZhciBDbGlwYm9hcmRBY3Rpb24gPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgIC8qKlxuICAgICAgICAgKiBAcGFyYW0ge09iamVjdH0gb3B0aW9uc1xuICAgICAgICAgKi9cbiAgICAgICAgZnVuY3Rpb24gQ2xpcGJvYXJkQWN0aW9uKG9wdGlvbnMpIHtcbiAgICAgICAgICAgIF9jbGFzc0NhbGxDaGVjayh0aGlzLCBDbGlwYm9hcmRBY3Rpb24pO1xuXG4gICAgICAgICAgICB0aGlzLnJlc29sdmVPcHRpb25zKG9wdGlvbnMpO1xuICAgICAgICAgICAgdGhpcy5pbml0U2VsZWN0aW9uKCk7XG4gICAgICAgIH1cblxuICAgICAgICAvKipcbiAgICAgICAgICogRGVmaW5lcyBiYXNlIHByb3BlcnRpZXMgcGFzc2VkIGZyb20gY29uc3RydWN0b3IuXG4gICAgICAgICAqIEBwYXJhbSB7T2JqZWN0fSBvcHRpb25zXG4gICAgICAgICAqL1xuXG5cbiAgICAgICAgX2NyZWF0ZUNsYXNzKENsaXBib2FyZEFjdGlvbiwgW3tcbiAgICAgICAgICAgIGtleTogJ3Jlc29sdmVPcHRpb25zJyxcbiAgICAgICAgICAgIHZhbHVlOiBmdW5jdGlvbiByZXNvbHZlT3B0aW9ucygpIHtcbiAgICAgICAgICAgICAgICB2YXIgb3B0aW9ucyA9IGFyZ3VtZW50cy5sZW5ndGggPiAwICYmIGFyZ3VtZW50c1swXSAhPT0gdW5kZWZpbmVkID8gYXJndW1lbnRzWzBdIDoge307XG5cbiAgICAgICAgICAgICAgICB0aGlzLmFjdGlvbiA9IG9wdGlvbnMuYWN0aW9uO1xuICAgICAgICAgICAgICAgIHRoaXMuY29udGFpbmVyID0gb3B0aW9ucy5jb250YWluZXI7XG4gICAgICAgICAgICAgICAgdGhpcy5lbWl0dGVyID0gb3B0aW9ucy5lbWl0dGVyO1xuICAgICAgICAgICAgICAgIHRoaXMudGFyZ2V0ID0gb3B0aW9ucy50YXJnZXQ7XG4gICAgICAgICAgICAgICAgdGhpcy50ZXh0ID0gb3B0aW9ucy50ZXh0O1xuICAgICAgICAgICAgICAgIHRoaXMudHJpZ2dlciA9IG9wdGlvbnMudHJpZ2dlcjtcblxuICAgICAgICAgICAgICAgIHRoaXMuc2VsZWN0ZWRUZXh0ID0gJyc7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0sIHtcbiAgICAgICAgICAgIGtleTogJ2luaXRTZWxlY3Rpb24nLFxuICAgICAgICAgICAgdmFsdWU6IGZ1bmN0aW9uIGluaXRTZWxlY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgaWYgKHRoaXMudGV4dCkge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLnNlbGVjdEZha2UoKTtcbiAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKHRoaXMudGFyZ2V0KSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc2VsZWN0VGFyZ2V0KCk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9LCB7XG4gICAgICAgICAgICBrZXk6ICdzZWxlY3RGYWtlJyxcbiAgICAgICAgICAgIHZhbHVlOiBmdW5jdGlvbiBzZWxlY3RGYWtlKCkge1xuICAgICAgICAgICAgICAgIHZhciBfdGhpcyA9IHRoaXM7XG5cbiAgICAgICAgICAgICAgICB2YXIgaXNSVEwgPSBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuZ2V0QXR0cmlidXRlKCdkaXInKSA9PSAncnRsJztcblxuICAgICAgICAgICAgICAgIHRoaXMucmVtb3ZlRmFrZSgpO1xuXG4gICAgICAgICAgICAgICAgdGhpcy5mYWtlSGFuZGxlckNhbGxiYWNrID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gX3RoaXMucmVtb3ZlRmFrZSgpO1xuICAgICAgICAgICAgICAgIH07XG4gICAgICAgICAgICAgICAgdGhpcy5mYWtlSGFuZGxlciA9IHRoaXMuY29udGFpbmVyLmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgdGhpcy5mYWtlSGFuZGxlckNhbGxiYWNrKSB8fCB0cnVlO1xuXG4gICAgICAgICAgICAgICAgdGhpcy5mYWtlRWxlbSA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ3RleHRhcmVhJyk7XG4gICAgICAgICAgICAgICAgLy8gUHJldmVudCB6b29taW5nIG9uIGlPU1xuICAgICAgICAgICAgICAgIHRoaXMuZmFrZUVsZW0uc3R5bGUuZm9udFNpemUgPSAnMTJwdCc7XG4gICAgICAgICAgICAgICAgLy8gUmVzZXQgYm94IG1vZGVsXG4gICAgICAgICAgICAgICAgdGhpcy5mYWtlRWxlbS5zdHlsZS5ib3JkZXIgPSAnMCc7XG4gICAgICAgICAgICAgICAgdGhpcy5mYWtlRWxlbS5zdHlsZS5wYWRkaW5nID0gJzAnO1xuICAgICAgICAgICAgICAgIHRoaXMuZmFrZUVsZW0uc3R5bGUubWFyZ2luID0gJzAnO1xuICAgICAgICAgICAgICAgIC8vIE1vdmUgZWxlbWVudCBvdXQgb2Ygc2NyZWVuIGhvcml6b250YWxseVxuICAgICAgICAgICAgICAgIHRoaXMuZmFrZUVsZW0uc3R5bGUucG9zaXRpb24gPSAnYWJzb2x1dGUnO1xuICAgICAgICAgICAgICAgIHRoaXMuZmFrZUVsZW0uc3R5bGVbaXNSVEwgPyAncmlnaHQnIDogJ2xlZnQnXSA9ICctOTk5OXB4JztcbiAgICAgICAgICAgICAgICAvLyBNb3ZlIGVsZW1lbnQgdG8gdGhlIHNhbWUgcG9zaXRpb24gdmVydGljYWxseVxuICAgICAgICAgICAgICAgIHZhciB5UG9zaXRpb24gPSB3aW5kb3cucGFnZVlPZmZzZXQgfHwgZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LnNjcm9sbFRvcDtcbiAgICAgICAgICAgICAgICB0aGlzLmZha2VFbGVtLnN0eWxlLnRvcCA9IHlQb3NpdGlvbiArICdweCc7XG5cbiAgICAgICAgICAgICAgICB0aGlzLmZha2VFbGVtLnNldEF0dHJpYnV0ZSgncmVhZG9ubHknLCAnJyk7XG4gICAgICAgICAgICAgICAgdGhpcy5mYWtlRWxlbS52YWx1ZSA9IHRoaXMudGV4dDtcblxuICAgICAgICAgICAgICAgIHRoaXMuY29udGFpbmVyLmFwcGVuZENoaWxkKHRoaXMuZmFrZUVsZW0pO1xuXG4gICAgICAgICAgICAgICAgdGhpcy5zZWxlY3RlZFRleHQgPSAoMCwgX3NlbGVjdDIuZGVmYXVsdCkodGhpcy5mYWtlRWxlbSk7XG4gICAgICAgICAgICAgICAgdGhpcy5jb3B5VGV4dCgpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9LCB7XG4gICAgICAgICAgICBrZXk6ICdyZW1vdmVGYWtlJyxcbiAgICAgICAgICAgIHZhbHVlOiBmdW5jdGlvbiByZW1vdmVGYWtlKCkge1xuICAgICAgICAgICAgICAgIGlmICh0aGlzLmZha2VIYW5kbGVyKSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuY29udGFpbmVyLnJlbW92ZUV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgdGhpcy5mYWtlSGFuZGxlckNhbGxiYWNrKTtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5mYWtlSGFuZGxlciA9IG51bGw7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZmFrZUhhbmRsZXJDYWxsYmFjayA9IG51bGw7XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMuZmFrZUVsZW0pIHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5jb250YWluZXIucmVtb3ZlQ2hpbGQodGhpcy5mYWtlRWxlbSk7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZmFrZUVsZW0gPSBudWxsO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfSwge1xuICAgICAgICAgICAga2V5OiAnc2VsZWN0VGFyZ2V0JyxcbiAgICAgICAgICAgIHZhbHVlOiBmdW5jdGlvbiBzZWxlY3RUYXJnZXQoKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5zZWxlY3RlZFRleHQgPSAoMCwgX3NlbGVjdDIuZGVmYXVsdCkodGhpcy50YXJnZXQpO1xuICAgICAgICAgICAgICAgIHRoaXMuY29weVRleHQoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSwge1xuICAgICAgICAgICAga2V5OiAnY29weVRleHQnLFxuICAgICAgICAgICAgdmFsdWU6IGZ1bmN0aW9uIGNvcHlUZXh0KCkge1xuICAgICAgICAgICAgICAgIHZhciBzdWNjZWVkZWQgPSB2b2lkIDA7XG5cbiAgICAgICAgICAgICAgICB0cnkge1xuICAgICAgICAgICAgICAgICAgICBzdWNjZWVkZWQgPSBkb2N1bWVudC5leGVjQ29tbWFuZCh0aGlzLmFjdGlvbik7XG4gICAgICAgICAgICAgICAgfSBjYXRjaCAoZXJyKSB7XG4gICAgICAgICAgICAgICAgICAgIHN1Y2NlZWRlZCA9IGZhbHNlO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIHRoaXMuaGFuZGxlUmVzdWx0KHN1Y2NlZWRlZCk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0sIHtcbiAgICAgICAgICAgIGtleTogJ2hhbmRsZVJlc3VsdCcsXG4gICAgICAgICAgICB2YWx1ZTogZnVuY3Rpb24gaGFuZGxlUmVzdWx0KHN1Y2NlZWRlZCkge1xuICAgICAgICAgICAgICAgIHRoaXMuZW1pdHRlci5lbWl0KHN1Y2NlZWRlZCA/ICdzdWNjZXNzJyA6ICdlcnJvcicsIHtcbiAgICAgICAgICAgICAgICAgICAgYWN0aW9uOiB0aGlzLmFjdGlvbixcbiAgICAgICAgICAgICAgICAgICAgdGV4dDogdGhpcy5zZWxlY3RlZFRleHQsXG4gICAgICAgICAgICAgICAgICAgIHRyaWdnZXI6IHRoaXMudHJpZ2dlcixcbiAgICAgICAgICAgICAgICAgICAgY2xlYXJTZWxlY3Rpb246IHRoaXMuY2xlYXJTZWxlY3Rpb24uYmluZCh0aGlzKVxuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfVxuICAgICAgICB9LCB7XG4gICAgICAgICAgICBrZXk6ICdjbGVhclNlbGVjdGlvbicsXG4gICAgICAgICAgICB2YWx1ZTogZnVuY3Rpb24gY2xlYXJTZWxlY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgaWYgKHRoaXMudHJpZ2dlcikge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLnRyaWdnZXIuZm9jdXMoKTtcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICB3aW5kb3cuZ2V0U2VsZWN0aW9uKCkucmVtb3ZlQWxsUmFuZ2VzKCk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0sIHtcbiAgICAgICAgICAgIGtleTogJ2Rlc3Ryb3knLFxuICAgICAgICAgICAgdmFsdWU6IGZ1bmN0aW9uIGRlc3Ryb3koKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5yZW1vdmVGYWtlKCk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0sIHtcbiAgICAgICAgICAgIGtleTogJ2FjdGlvbicsXG4gICAgICAgICAgICBzZXQ6IGZ1bmN0aW9uIHNldCgpIHtcbiAgICAgICAgICAgICAgICB2YXIgYWN0aW9uID0gYXJndW1lbnRzLmxlbmd0aCA+IDAgJiYgYXJndW1lbnRzWzBdICE9PSB1bmRlZmluZWQgPyBhcmd1bWVudHNbMF0gOiAnY29weSc7XG5cbiAgICAgICAgICAgICAgICB0aGlzLl9hY3Rpb24gPSBhY3Rpb247XG5cbiAgICAgICAgICAgICAgICBpZiAodGhpcy5fYWN0aW9uICE9PSAnY29weScgJiYgdGhpcy5fYWN0aW9uICE9PSAnY3V0Jykge1xuICAgICAgICAgICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ0ludmFsaWQgXCJhY3Rpb25cIiB2YWx1ZSwgdXNlIGVpdGhlciBcImNvcHlcIiBvciBcImN1dFwiJyk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLl9hY3Rpb247XG4gICAgICAgICAgICB9XG4gICAgICAgIH0sIHtcbiAgICAgICAgICAgIGtleTogJ3RhcmdldCcsXG4gICAgICAgICAgICBzZXQ6IGZ1bmN0aW9uIHNldCh0YXJnZXQpIHtcbiAgICAgICAgICAgICAgICBpZiAodGFyZ2V0ICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKHRhcmdldCAmJiAodHlwZW9mIHRhcmdldCA9PT0gJ3VuZGVmaW5lZCcgPyAndW5kZWZpbmVkJyA6IF90eXBlb2YodGFyZ2V0KSkgPT09ICdvYmplY3QnICYmIHRhcmdldC5ub2RlVHlwZSA9PT0gMSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMuYWN0aW9uID09PSAnY29weScgJiYgdGFyZ2V0Lmhhc0F0dHJpYnV0ZSgnZGlzYWJsZWQnKSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcignSW52YWxpZCBcInRhcmdldFwiIGF0dHJpYnV0ZS4gUGxlYXNlIHVzZSBcInJlYWRvbmx5XCIgaW5zdGVhZCBvZiBcImRpc2FibGVkXCIgYXR0cmlidXRlJyk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLmFjdGlvbiA9PT0gJ2N1dCcgJiYgKHRhcmdldC5oYXNBdHRyaWJ1dGUoJ3JlYWRvbmx5JykgfHwgdGFyZ2V0Lmhhc0F0dHJpYnV0ZSgnZGlzYWJsZWQnKSkpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ0ludmFsaWQgXCJ0YXJnZXRcIiBhdHRyaWJ1dGUuIFlvdSBjYW5cXCd0IGN1dCB0ZXh0IGZyb20gZWxlbWVudHMgd2l0aCBcInJlYWRvbmx5XCIgb3IgXCJkaXNhYmxlZFwiIGF0dHJpYnV0ZXMnKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5fdGFyZ2V0ID0gdGFyZ2V0O1xuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKCdJbnZhbGlkIFwidGFyZ2V0XCIgdmFsdWUsIHVzZSBhIHZhbGlkIEVsZW1lbnQnKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5fdGFyZ2V0O1xuICAgICAgICAgICAgfVxuICAgICAgICB9XSk7XG5cbiAgICAgICAgcmV0dXJuIENsaXBib2FyZEFjdGlvbjtcbiAgICB9KCk7XG5cbiAgICBtb2R1bGUuZXhwb3J0cyA9IENsaXBib2FyZEFjdGlvbjtcbn0pO1xuXG4vKioqLyB9KSxcbi8qIDEgKi9cbi8qKiovIChmdW5jdGlvbihtb2R1bGUsIGV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pIHtcblxudmFyIGlzID0gX193ZWJwYWNrX3JlcXVpcmVfXyg2KTtcbnZhciBkZWxlZ2F0ZSA9IF9fd2VicGFja19yZXF1aXJlX18oNSk7XG5cbi8qKlxuICogVmFsaWRhdGVzIGFsbCBwYXJhbXMgYW5kIGNhbGxzIHRoZSByaWdodFxuICogbGlzdGVuZXIgZnVuY3Rpb24gYmFzZWQgb24gaXRzIHRhcmdldCB0eXBlLlxuICpcbiAqIEBwYXJhbSB7U3RyaW5nfEhUTUxFbGVtZW50fEhUTUxDb2xsZWN0aW9ufE5vZGVMaXN0fSB0YXJnZXRcbiAqIEBwYXJhbSB7U3RyaW5nfSB0eXBlXG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSBjYWxsYmFja1xuICogQHJldHVybiB7T2JqZWN0fVxuICovXG5mdW5jdGlvbiBsaXN0ZW4odGFyZ2V0LCB0eXBlLCBjYWxsYmFjaykge1xuICAgIGlmICghdGFyZ2V0ICYmICF0eXBlICYmICFjYWxsYmFjaykge1xuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ01pc3NpbmcgcmVxdWlyZWQgYXJndW1lbnRzJyk7XG4gICAgfVxuXG4gICAgaWYgKCFpcy5zdHJpbmcodHlwZSkpIHtcbiAgICAgICAgdGhyb3cgbmV3IFR5cGVFcnJvcignU2Vjb25kIGFyZ3VtZW50IG11c3QgYmUgYSBTdHJpbmcnKTtcbiAgICB9XG5cbiAgICBpZiAoIWlzLmZuKGNhbGxiYWNrKSkge1xuICAgICAgICB0aHJvdyBuZXcgVHlwZUVycm9yKCdUaGlyZCBhcmd1bWVudCBtdXN0IGJlIGEgRnVuY3Rpb24nKTtcbiAgICB9XG5cbiAgICBpZiAoaXMubm9kZSh0YXJnZXQpKSB7XG4gICAgICAgIHJldHVybiBsaXN0ZW5Ob2RlKHRhcmdldCwgdHlwZSwgY2FsbGJhY2spO1xuICAgIH1cbiAgICBlbHNlIGlmIChpcy5ub2RlTGlzdCh0YXJnZXQpKSB7XG4gICAgICAgIHJldHVybiBsaXN0ZW5Ob2RlTGlzdCh0YXJnZXQsIHR5cGUsIGNhbGxiYWNrKTtcbiAgICB9XG4gICAgZWxzZSBpZiAoaXMuc3RyaW5nKHRhcmdldCkpIHtcbiAgICAgICAgcmV0dXJuIGxpc3RlblNlbGVjdG9yKHRhcmdldCwgdHlwZSwgY2FsbGJhY2spO1xuICAgIH1cbiAgICBlbHNlIHtcbiAgICAgICAgdGhyb3cgbmV3IFR5cGVFcnJvcignRmlyc3QgYXJndW1lbnQgbXVzdCBiZSBhIFN0cmluZywgSFRNTEVsZW1lbnQsIEhUTUxDb2xsZWN0aW9uLCBvciBOb2RlTGlzdCcpO1xuICAgIH1cbn1cblxuLyoqXG4gKiBBZGRzIGFuIGV2ZW50IGxpc3RlbmVyIHRvIGEgSFRNTCBlbGVtZW50XG4gKiBhbmQgcmV0dXJucyBhIHJlbW92ZSBsaXN0ZW5lciBmdW5jdGlvbi5cbiAqXG4gKiBAcGFyYW0ge0hUTUxFbGVtZW50fSBub2RlXG4gKiBAcGFyYW0ge1N0cmluZ30gdHlwZVxuICogQHBhcmFtIHtGdW5jdGlvbn0gY2FsbGJhY2tcbiAqIEByZXR1cm4ge09iamVjdH1cbiAqL1xuZnVuY3Rpb24gbGlzdGVuTm9kZShub2RlLCB0eXBlLCBjYWxsYmFjaykge1xuICAgIG5vZGUuYWRkRXZlbnRMaXN0ZW5lcih0eXBlLCBjYWxsYmFjayk7XG5cbiAgICByZXR1cm4ge1xuICAgICAgICBkZXN0cm95OiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIG5vZGUucmVtb3ZlRXZlbnRMaXN0ZW5lcih0eXBlLCBjYWxsYmFjayk7XG4gICAgICAgIH1cbiAgICB9XG59XG5cbi8qKlxuICogQWRkIGFuIGV2ZW50IGxpc3RlbmVyIHRvIGEgbGlzdCBvZiBIVE1MIGVsZW1lbnRzXG4gKiBhbmQgcmV0dXJucyBhIHJlbW92ZSBsaXN0ZW5lciBmdW5jdGlvbi5cbiAqXG4gKiBAcGFyYW0ge05vZGVMaXN0fEhUTUxDb2xsZWN0aW9ufSBub2RlTGlzdFxuICogQHBhcmFtIHtTdHJpbmd9IHR5cGVcbiAqIEBwYXJhbSB7RnVuY3Rpb259IGNhbGxiYWNrXG4gKiBAcmV0dXJuIHtPYmplY3R9XG4gKi9cbmZ1bmN0aW9uIGxpc3Rlbk5vZGVMaXN0KG5vZGVMaXN0LCB0eXBlLCBjYWxsYmFjaykge1xuICAgIEFycmF5LnByb3RvdHlwZS5mb3JFYWNoLmNhbGwobm9kZUxpc3QsIGZ1bmN0aW9uKG5vZGUpIHtcbiAgICAgICAgbm9kZS5hZGRFdmVudExpc3RlbmVyKHR5cGUsIGNhbGxiYWNrKTtcbiAgICB9KTtcblxuICAgIHJldHVybiB7XG4gICAgICAgIGRlc3Ryb3k6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgQXJyYXkucHJvdG90eXBlLmZvckVhY2guY2FsbChub2RlTGlzdCwgZnVuY3Rpb24obm9kZSkge1xuICAgICAgICAgICAgICAgIG5vZGUucmVtb3ZlRXZlbnRMaXN0ZW5lcih0eXBlLCBjYWxsYmFjayk7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfVxuICAgIH1cbn1cblxuLyoqXG4gKiBBZGQgYW4gZXZlbnQgbGlzdGVuZXIgdG8gYSBzZWxlY3RvclxuICogYW5kIHJldHVybnMgYSByZW1vdmUgbGlzdGVuZXIgZnVuY3Rpb24uXG4gKlxuICogQHBhcmFtIHtTdHJpbmd9IHNlbGVjdG9yXG4gKiBAcGFyYW0ge1N0cmluZ30gdHlwZVxuICogQHBhcmFtIHtGdW5jdGlvbn0gY2FsbGJhY2tcbiAqIEByZXR1cm4ge09iamVjdH1cbiAqL1xuZnVuY3Rpb24gbGlzdGVuU2VsZWN0b3Ioc2VsZWN0b3IsIHR5cGUsIGNhbGxiYWNrKSB7XG4gICAgcmV0dXJuIGRlbGVnYXRlKGRvY3VtZW50LmJvZHksIHNlbGVjdG9yLCB0eXBlLCBjYWxsYmFjayk7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gbGlzdGVuO1xuXG5cbi8qKiovIH0pLFxuLyogMiAqL1xuLyoqKi8gKGZ1bmN0aW9uKG1vZHVsZSwgZXhwb3J0cykge1xuXG5mdW5jdGlvbiBFICgpIHtcbiAgLy8gS2VlcCB0aGlzIGVtcHR5IHNvIGl0J3MgZWFzaWVyIHRvIGluaGVyaXQgZnJvbVxuICAvLyAodmlhIGh0dHBzOi8vZ2l0aHViLmNvbS9saXBzbWFjayBmcm9tIGh0dHBzOi8vZ2l0aHViLmNvbS9zY290dGNvcmdhbi90aW55LWVtaXR0ZXIvaXNzdWVzLzMpXG59XG5cbkUucHJvdG90eXBlID0ge1xuICBvbjogZnVuY3Rpb24gKG5hbWUsIGNhbGxiYWNrLCBjdHgpIHtcbiAgICB2YXIgZSA9IHRoaXMuZSB8fCAodGhpcy5lID0ge30pO1xuXG4gICAgKGVbbmFtZV0gfHwgKGVbbmFtZV0gPSBbXSkpLnB1c2goe1xuICAgICAgZm46IGNhbGxiYWNrLFxuICAgICAgY3R4OiBjdHhcbiAgICB9KTtcblxuICAgIHJldHVybiB0aGlzO1xuICB9LFxuXG4gIG9uY2U6IGZ1bmN0aW9uIChuYW1lLCBjYWxsYmFjaywgY3R4KSB7XG4gICAgdmFyIHNlbGYgPSB0aGlzO1xuICAgIGZ1bmN0aW9uIGxpc3RlbmVyICgpIHtcbiAgICAgIHNlbGYub2ZmKG5hbWUsIGxpc3RlbmVyKTtcbiAgICAgIGNhbGxiYWNrLmFwcGx5KGN0eCwgYXJndW1lbnRzKTtcbiAgICB9O1xuXG4gICAgbGlzdGVuZXIuXyA9IGNhbGxiYWNrXG4gICAgcmV0dXJuIHRoaXMub24obmFtZSwgbGlzdGVuZXIsIGN0eCk7XG4gIH0sXG5cbiAgZW1pdDogZnVuY3Rpb24gKG5hbWUpIHtcbiAgICB2YXIgZGF0YSA9IFtdLnNsaWNlLmNhbGwoYXJndW1lbnRzLCAxKTtcbiAgICB2YXIgZXZ0QXJyID0gKCh0aGlzLmUgfHwgKHRoaXMuZSA9IHt9KSlbbmFtZV0gfHwgW10pLnNsaWNlKCk7XG4gICAgdmFyIGkgPSAwO1xuICAgIHZhciBsZW4gPSBldnRBcnIubGVuZ3RoO1xuXG4gICAgZm9yIChpOyBpIDwgbGVuOyBpKyspIHtcbiAgICAgIGV2dEFycltpXS5mbi5hcHBseShldnRBcnJbaV0uY3R4LCBkYXRhKTtcbiAgICB9XG5cbiAgICByZXR1cm4gdGhpcztcbiAgfSxcblxuICBvZmY6IGZ1bmN0aW9uIChuYW1lLCBjYWxsYmFjaykge1xuICAgIHZhciBlID0gdGhpcy5lIHx8ICh0aGlzLmUgPSB7fSk7XG4gICAgdmFyIGV2dHMgPSBlW25hbWVdO1xuICAgIHZhciBsaXZlRXZlbnRzID0gW107XG5cbiAgICBpZiAoZXZ0cyAmJiBjYWxsYmFjaykge1xuICAgICAgZm9yICh2YXIgaSA9IDAsIGxlbiA9IGV2dHMubGVuZ3RoOyBpIDwgbGVuOyBpKyspIHtcbiAgICAgICAgaWYgKGV2dHNbaV0uZm4gIT09IGNhbGxiYWNrICYmIGV2dHNbaV0uZm4uXyAhPT0gY2FsbGJhY2spXG4gICAgICAgICAgbGl2ZUV2ZW50cy5wdXNoKGV2dHNbaV0pO1xuICAgICAgfVxuICAgIH1cblxuICAgIC8vIFJlbW92ZSBldmVudCBmcm9tIHF1ZXVlIHRvIHByZXZlbnQgbWVtb3J5IGxlYWtcbiAgICAvLyBTdWdnZXN0ZWQgYnkgaHR0cHM6Ly9naXRodWIuY29tL2xhemRcbiAgICAvLyBSZWY6IGh0dHBzOi8vZ2l0aHViLmNvbS9zY290dGNvcmdhbi90aW55LWVtaXR0ZXIvY29tbWl0L2M2ZWJmYWE5YmM5NzNiMzNkMTEwYTg0YTMwNzc0MmI3Y2Y5NGM5NTMjY29tbWl0Y29tbWVudC01MDI0OTEwXG5cbiAgICAobGl2ZUV2ZW50cy5sZW5ndGgpXG4gICAgICA/IGVbbmFtZV0gPSBsaXZlRXZlbnRzXG4gICAgICA6IGRlbGV0ZSBlW25hbWVdO1xuXG4gICAgcmV0dXJuIHRoaXM7XG4gIH1cbn07XG5cbm1vZHVsZS5leHBvcnRzID0gRTtcblxuXG4vKioqLyB9KSxcbi8qIDMgKi9cbi8qKiovIChmdW5jdGlvbihtb2R1bGUsIGV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pIHtcblxudmFyIF9fV0VCUEFDS19BTURfREVGSU5FX0ZBQ1RPUllfXywgX19XRUJQQUNLX0FNRF9ERUZJTkVfQVJSQVlfXywgX19XRUJQQUNLX0FNRF9ERUZJTkVfUkVTVUxUX187KGZ1bmN0aW9uIChnbG9iYWwsIGZhY3RvcnkpIHtcbiAgICBpZiAodHJ1ZSkge1xuICAgICAgICAhKF9fV0VCUEFDS19BTURfREVGSU5FX0FSUkFZX18gPSBbbW9kdWxlLCBfX3dlYnBhY2tfcmVxdWlyZV9fKDApLCBfX3dlYnBhY2tfcmVxdWlyZV9fKDIpLCBfX3dlYnBhY2tfcmVxdWlyZV9fKDEpXSwgX19XRUJQQUNLX0FNRF9ERUZJTkVfRkFDVE9SWV9fID0gKGZhY3RvcnkpLFxuXHRcdFx0XHRfX1dFQlBBQ0tfQU1EX0RFRklORV9SRVNVTFRfXyA9ICh0eXBlb2YgX19XRUJQQUNLX0FNRF9ERUZJTkVfRkFDVE9SWV9fID09PSAnZnVuY3Rpb24nID9cblx0XHRcdFx0KF9fV0VCUEFDS19BTURfREVGSU5FX0ZBQ1RPUllfXy5hcHBseShleHBvcnRzLCBfX1dFQlBBQ0tfQU1EX0RFRklORV9BUlJBWV9fKSkgOiBfX1dFQlBBQ0tfQU1EX0RFRklORV9GQUNUT1JZX18pLFxuXHRcdFx0XHRfX1dFQlBBQ0tfQU1EX0RFRklORV9SRVNVTFRfXyAhPT0gdW5kZWZpbmVkICYmIChtb2R1bGUuZXhwb3J0cyA9IF9fV0VCUEFDS19BTURfREVGSU5FX1JFU1VMVF9fKSk7XG4gICAgfSBlbHNlIGlmICh0eXBlb2YgZXhwb3J0cyAhPT0gXCJ1bmRlZmluZWRcIikge1xuICAgICAgICBmYWN0b3J5KG1vZHVsZSwgcmVxdWlyZSgnLi9jbGlwYm9hcmQtYWN0aW9uJyksIHJlcXVpcmUoJ3RpbnktZW1pdHRlcicpLCByZXF1aXJlKCdnb29kLWxpc3RlbmVyJykpO1xuICAgIH0gZWxzZSB7XG4gICAgICAgIHZhciBtb2QgPSB7XG4gICAgICAgICAgICBleHBvcnRzOiB7fVxuICAgICAgICB9O1xuICAgICAgICBmYWN0b3J5KG1vZCwgZ2xvYmFsLmNsaXBib2FyZEFjdGlvbiwgZ2xvYmFsLnRpbnlFbWl0dGVyLCBnbG9iYWwuZ29vZExpc3RlbmVyKTtcbiAgICAgICAgZ2xvYmFsLmNsaXBib2FyZCA9IG1vZC5leHBvcnRzO1xuICAgIH1cbn0pKHRoaXMsIGZ1bmN0aW9uIChtb2R1bGUsIF9jbGlwYm9hcmRBY3Rpb24sIF90aW55RW1pdHRlciwgX2dvb2RMaXN0ZW5lcikge1xuICAgICd1c2Ugc3RyaWN0JztcblxuICAgIHZhciBfY2xpcGJvYXJkQWN0aW9uMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NsaXBib2FyZEFjdGlvbik7XG5cbiAgICB2YXIgX3RpbnlFbWl0dGVyMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3RpbnlFbWl0dGVyKTtcblxuICAgIHZhciBfZ29vZExpc3RlbmVyMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2dvb2RMaXN0ZW5lcik7XG5cbiAgICBmdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikge1xuICAgICAgICByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDoge1xuICAgICAgICAgICAgZGVmYXVsdDogb2JqXG4gICAgICAgIH07XG4gICAgfVxuXG4gICAgdmFyIF90eXBlb2YgPSB0eXBlb2YgU3ltYm9sID09PSBcImZ1bmN0aW9uXCIgJiYgdHlwZW9mIFN5bWJvbC5pdGVyYXRvciA9PT0gXCJzeW1ib2xcIiA/IGZ1bmN0aW9uIChvYmopIHtcbiAgICAgICAgcmV0dXJuIHR5cGVvZiBvYmo7XG4gICAgfSA6IGZ1bmN0aW9uIChvYmopIHtcbiAgICAgICAgcmV0dXJuIG9iaiAmJiB0eXBlb2YgU3ltYm9sID09PSBcImZ1bmN0aW9uXCIgJiYgb2JqLmNvbnN0cnVjdG9yID09PSBTeW1ib2wgJiYgb2JqICE9PSBTeW1ib2wucHJvdG90eXBlID8gXCJzeW1ib2xcIiA6IHR5cGVvZiBvYmo7XG4gICAgfTtcblxuICAgIGZ1bmN0aW9uIF9jbGFzc0NhbGxDaGVjayhpbnN0YW5jZSwgQ29uc3RydWN0b3IpIHtcbiAgICAgICAgaWYgKCEoaW5zdGFuY2UgaW5zdGFuY2VvZiBDb25zdHJ1Y3RvcikpIHtcbiAgICAgICAgICAgIHRocm93IG5ldyBUeXBlRXJyb3IoXCJDYW5ub3QgY2FsbCBhIGNsYXNzIGFzIGEgZnVuY3Rpb25cIik7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICB2YXIgX2NyZWF0ZUNsYXNzID0gZnVuY3Rpb24gKCkge1xuICAgICAgICBmdW5jdGlvbiBkZWZpbmVQcm9wZXJ0aWVzKHRhcmdldCwgcHJvcHMpIHtcbiAgICAgICAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgcHJvcHMubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgICAgICB2YXIgZGVzY3JpcHRvciA9IHByb3BzW2ldO1xuICAgICAgICAgICAgICAgIGRlc2NyaXB0b3IuZW51bWVyYWJsZSA9IGRlc2NyaXB0b3IuZW51bWVyYWJsZSB8fCBmYWxzZTtcbiAgICAgICAgICAgICAgICBkZXNjcmlwdG9yLmNvbmZpZ3VyYWJsZSA9IHRydWU7XG4gICAgICAgICAgICAgICAgaWYgKFwidmFsdWVcIiBpbiBkZXNjcmlwdG9yKSBkZXNjcmlwdG9yLndyaXRhYmxlID0gdHJ1ZTtcbiAgICAgICAgICAgICAgICBPYmplY3QuZGVmaW5lUHJvcGVydHkodGFyZ2V0LCBkZXNjcmlwdG9yLmtleSwgZGVzY3JpcHRvcik7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gZnVuY3Rpb24gKENvbnN0cnVjdG9yLCBwcm90b1Byb3BzLCBzdGF0aWNQcm9wcykge1xuICAgICAgICAgICAgaWYgKHByb3RvUHJvcHMpIGRlZmluZVByb3BlcnRpZXMoQ29uc3RydWN0b3IucHJvdG90eXBlLCBwcm90b1Byb3BzKTtcbiAgICAgICAgICAgIGlmIChzdGF0aWNQcm9wcykgZGVmaW5lUHJvcGVydGllcyhDb25zdHJ1Y3Rvciwgc3RhdGljUHJvcHMpO1xuICAgICAgICAgICAgcmV0dXJuIENvbnN0cnVjdG9yO1xuICAgICAgICB9O1xuICAgIH0oKTtcblxuICAgIGZ1bmN0aW9uIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuKHNlbGYsIGNhbGwpIHtcbiAgICAgICAgaWYgKCFzZWxmKSB7XG4gICAgICAgICAgICB0aHJvdyBuZXcgUmVmZXJlbmNlRXJyb3IoXCJ0aGlzIGhhc24ndCBiZWVuIGluaXRpYWxpc2VkIC0gc3VwZXIoKSBoYXNuJ3QgYmVlbiBjYWxsZWRcIik7XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gY2FsbCAmJiAodHlwZW9mIGNhbGwgPT09IFwib2JqZWN0XCIgfHwgdHlwZW9mIGNhbGwgPT09IFwiZnVuY3Rpb25cIikgPyBjYWxsIDogc2VsZjtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBfaW5oZXJpdHMoc3ViQ2xhc3MsIHN1cGVyQ2xhc3MpIHtcbiAgICAgICAgaWYgKHR5cGVvZiBzdXBlckNsYXNzICE9PSBcImZ1bmN0aW9uXCIgJiYgc3VwZXJDbGFzcyAhPT0gbnVsbCkge1xuICAgICAgICAgICAgdGhyb3cgbmV3IFR5cGVFcnJvcihcIlN1cGVyIGV4cHJlc3Npb24gbXVzdCBlaXRoZXIgYmUgbnVsbCBvciBhIGZ1bmN0aW9uLCBub3QgXCIgKyB0eXBlb2Ygc3VwZXJDbGFzcyk7XG4gICAgICAgIH1cblxuICAgICAgICBzdWJDbGFzcy5wcm90b3R5cGUgPSBPYmplY3QuY3JlYXRlKHN1cGVyQ2xhc3MgJiYgc3VwZXJDbGFzcy5wcm90b3R5cGUsIHtcbiAgICAgICAgICAgIGNvbnN0cnVjdG9yOiB7XG4gICAgICAgICAgICAgICAgdmFsdWU6IHN1YkNsYXNzLFxuICAgICAgICAgICAgICAgIGVudW1lcmFibGU6IGZhbHNlLFxuICAgICAgICAgICAgICAgIHdyaXRhYmxlOiB0cnVlLFxuICAgICAgICAgICAgICAgIGNvbmZpZ3VyYWJsZTogdHJ1ZVxuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICAgICAgaWYgKHN1cGVyQ2xhc3MpIE9iamVjdC5zZXRQcm90b3R5cGVPZiA/IE9iamVjdC5zZXRQcm90b3R5cGVPZihzdWJDbGFzcywgc3VwZXJDbGFzcykgOiBzdWJDbGFzcy5fX3Byb3RvX18gPSBzdXBlckNsYXNzO1xuICAgIH1cblxuICAgIHZhciBDbGlwYm9hcmQgPSBmdW5jdGlvbiAoX0VtaXR0ZXIpIHtcbiAgICAgICAgX2luaGVyaXRzKENsaXBib2FyZCwgX0VtaXR0ZXIpO1xuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiBAcGFyYW0ge1N0cmluZ3xIVE1MRWxlbWVudHxIVE1MQ29sbGVjdGlvbnxOb2RlTGlzdH0gdHJpZ2dlclxuICAgICAgICAgKiBAcGFyYW0ge09iamVjdH0gb3B0aW9uc1xuICAgICAgICAgKi9cbiAgICAgICAgZnVuY3Rpb24gQ2xpcGJvYXJkKHRyaWdnZXIsIG9wdGlvbnMpIHtcbiAgICAgICAgICAgIF9jbGFzc0NhbGxDaGVjayh0aGlzLCBDbGlwYm9hcmQpO1xuXG4gICAgICAgICAgICB2YXIgX3RoaXMgPSBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybih0aGlzLCAoQ2xpcGJvYXJkLl9fcHJvdG9fXyB8fCBPYmplY3QuZ2V0UHJvdG90eXBlT2YoQ2xpcGJvYXJkKSkuY2FsbCh0aGlzKSk7XG5cbiAgICAgICAgICAgIF90aGlzLnJlc29sdmVPcHRpb25zKG9wdGlvbnMpO1xuICAgICAgICAgICAgX3RoaXMubGlzdGVuQ2xpY2sodHJpZ2dlcik7XG4gICAgICAgICAgICByZXR1cm4gX3RoaXM7XG4gICAgICAgIH1cblxuICAgICAgICAvKipcbiAgICAgICAgICogRGVmaW5lcyBpZiBhdHRyaWJ1dGVzIHdvdWxkIGJlIHJlc29sdmVkIHVzaW5nIGludGVybmFsIHNldHRlciBmdW5jdGlvbnNcbiAgICAgICAgICogb3IgY3VzdG9tIGZ1bmN0aW9ucyB0aGF0IHdlcmUgcGFzc2VkIGluIHRoZSBjb25zdHJ1Y3Rvci5cbiAgICAgICAgICogQHBhcmFtIHtPYmplY3R9IG9wdGlvbnNcbiAgICAgICAgICovXG5cblxuICAgICAgICBfY3JlYXRlQ2xhc3MoQ2xpcGJvYXJkLCBbe1xuICAgICAgICAgICAga2V5OiAncmVzb2x2ZU9wdGlvbnMnLFxuICAgICAgICAgICAgdmFsdWU6IGZ1bmN0aW9uIHJlc29sdmVPcHRpb25zKCkge1xuICAgICAgICAgICAgICAgIHZhciBvcHRpb25zID0gYXJndW1lbnRzLmxlbmd0aCA+IDAgJiYgYXJndW1lbnRzWzBdICE9PSB1bmRlZmluZWQgPyBhcmd1bWVudHNbMF0gOiB7fTtcblxuICAgICAgICAgICAgICAgIHRoaXMuYWN0aW9uID0gdHlwZW9mIG9wdGlvbnMuYWN0aW9uID09PSAnZnVuY3Rpb24nID8gb3B0aW9ucy5hY3Rpb24gOiB0aGlzLmRlZmF1bHRBY3Rpb247XG4gICAgICAgICAgICAgICAgdGhpcy50YXJnZXQgPSB0eXBlb2Ygb3B0aW9ucy50YXJnZXQgPT09ICdmdW5jdGlvbicgPyBvcHRpb25zLnRhcmdldCA6IHRoaXMuZGVmYXVsdFRhcmdldDtcbiAgICAgICAgICAgICAgICB0aGlzLnRleHQgPSB0eXBlb2Ygb3B0aW9ucy50ZXh0ID09PSAnZnVuY3Rpb24nID8gb3B0aW9ucy50ZXh0IDogdGhpcy5kZWZhdWx0VGV4dDtcbiAgICAgICAgICAgICAgICB0aGlzLmNvbnRhaW5lciA9IF90eXBlb2Yob3B0aW9ucy5jb250YWluZXIpID09PSAnb2JqZWN0JyA/IG9wdGlvbnMuY29udGFpbmVyIDogZG9jdW1lbnQuYm9keTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSwge1xuICAgICAgICAgICAga2V5OiAnbGlzdGVuQ2xpY2snLFxuICAgICAgICAgICAgdmFsdWU6IGZ1bmN0aW9uIGxpc3RlbkNsaWNrKHRyaWdnZXIpIHtcbiAgICAgICAgICAgICAgICB2YXIgX3RoaXMyID0gdGhpcztcblxuICAgICAgICAgICAgICAgIHRoaXMubGlzdGVuZXIgPSAoMCwgX2dvb2RMaXN0ZW5lcjIuZGVmYXVsdCkodHJpZ2dlciwgJ2NsaWNrJywgZnVuY3Rpb24gKGUpIHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIF90aGlzMi5vbkNsaWNrKGUpO1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfVxuICAgICAgICB9LCB7XG4gICAgICAgICAgICBrZXk6ICdvbkNsaWNrJyxcbiAgICAgICAgICAgIHZhbHVlOiBmdW5jdGlvbiBvbkNsaWNrKGUpIHtcbiAgICAgICAgICAgICAgICB2YXIgdHJpZ2dlciA9IGUuZGVsZWdhdGVUYXJnZXQgfHwgZS5jdXJyZW50VGFyZ2V0O1xuXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMuY2xpcGJvYXJkQWN0aW9uKSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuY2xpcGJvYXJkQWN0aW9uID0gbnVsbDtcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICB0aGlzLmNsaXBib2FyZEFjdGlvbiA9IG5ldyBfY2xpcGJvYXJkQWN0aW9uMi5kZWZhdWx0KHtcbiAgICAgICAgICAgICAgICAgICAgYWN0aW9uOiB0aGlzLmFjdGlvbih0cmlnZ2VyKSxcbiAgICAgICAgICAgICAgICAgICAgdGFyZ2V0OiB0aGlzLnRhcmdldCh0cmlnZ2VyKSxcbiAgICAgICAgICAgICAgICAgICAgdGV4dDogdGhpcy50ZXh0KHRyaWdnZXIpLFxuICAgICAgICAgICAgICAgICAgICBjb250YWluZXI6IHRoaXMuY29udGFpbmVyLFxuICAgICAgICAgICAgICAgICAgICB0cmlnZ2VyOiB0cmlnZ2VyLFxuICAgICAgICAgICAgICAgICAgICBlbWl0dGVyOiB0aGlzXG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0sIHtcbiAgICAgICAgICAgIGtleTogJ2RlZmF1bHRBY3Rpb24nLFxuICAgICAgICAgICAgdmFsdWU6IGZ1bmN0aW9uIGRlZmF1bHRBY3Rpb24odHJpZ2dlcikge1xuICAgICAgICAgICAgICAgIHJldHVybiBnZXRBdHRyaWJ1dGVWYWx1ZSgnYWN0aW9uJywgdHJpZ2dlcik7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0sIHtcbiAgICAgICAgICAgIGtleTogJ2RlZmF1bHRUYXJnZXQnLFxuICAgICAgICAgICAgdmFsdWU6IGZ1bmN0aW9uIGRlZmF1bHRUYXJnZXQodHJpZ2dlcikge1xuICAgICAgICAgICAgICAgIHZhciBzZWxlY3RvciA9IGdldEF0dHJpYnV0ZVZhbHVlKCd0YXJnZXQnLCB0cmlnZ2VyKTtcblxuICAgICAgICAgICAgICAgIGlmIChzZWxlY3Rvcikge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihzZWxlY3Rvcik7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9LCB7XG4gICAgICAgICAgICBrZXk6ICdkZWZhdWx0VGV4dCcsXG4gICAgICAgICAgICB2YWx1ZTogZnVuY3Rpb24gZGVmYXVsdFRleHQodHJpZ2dlcikge1xuICAgICAgICAgICAgICAgIHJldHVybiBnZXRBdHRyaWJ1dGVWYWx1ZSgndGV4dCcsIHRyaWdnZXIpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9LCB7XG4gICAgICAgICAgICBrZXk6ICdkZXN0cm95JyxcbiAgICAgICAgICAgIHZhbHVlOiBmdW5jdGlvbiBkZXN0cm95KCkge1xuICAgICAgICAgICAgICAgIHRoaXMubGlzdGVuZXIuZGVzdHJveSgpO1xuXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMuY2xpcGJvYXJkQWN0aW9uKSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuY2xpcGJvYXJkQWN0aW9uLmRlc3Ryb3koKTtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5jbGlwYm9hcmRBY3Rpb24gPSBudWxsO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfV0sIFt7XG4gICAgICAgICAgICBrZXk6ICdpc1N1cHBvcnRlZCcsXG4gICAgICAgICAgICB2YWx1ZTogZnVuY3Rpb24gaXNTdXBwb3J0ZWQoKSB7XG4gICAgICAgICAgICAgICAgdmFyIGFjdGlvbiA9IGFyZ3VtZW50cy5sZW5ndGggPiAwICYmIGFyZ3VtZW50c1swXSAhPT0gdW5kZWZpbmVkID8gYXJndW1lbnRzWzBdIDogWydjb3B5JywgJ2N1dCddO1xuXG4gICAgICAgICAgICAgICAgdmFyIGFjdGlvbnMgPSB0eXBlb2YgYWN0aW9uID09PSAnc3RyaW5nJyA/IFthY3Rpb25dIDogYWN0aW9uO1xuICAgICAgICAgICAgICAgIHZhciBzdXBwb3J0ID0gISFkb2N1bWVudC5xdWVyeUNvbW1hbmRTdXBwb3J0ZWQ7XG5cbiAgICAgICAgICAgICAgICBhY3Rpb25zLmZvckVhY2goZnVuY3Rpb24gKGFjdGlvbikge1xuICAgICAgICAgICAgICAgICAgICBzdXBwb3J0ID0gc3VwcG9ydCAmJiAhIWRvY3VtZW50LnF1ZXJ5Q29tbWFuZFN1cHBvcnRlZChhY3Rpb24pO1xuICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAgICAgcmV0dXJuIHN1cHBvcnQ7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1dKTtcblxuICAgICAgICByZXR1cm4gQ2xpcGJvYXJkO1xuICAgIH0oX3RpbnlFbWl0dGVyMi5kZWZhdWx0KTtcblxuICAgIC8qKlxuICAgICAqIEhlbHBlciBmdW5jdGlvbiB0byByZXRyaWV2ZSBhdHRyaWJ1dGUgdmFsdWUuXG4gICAgICogQHBhcmFtIHtTdHJpbmd9IHN1ZmZpeFxuICAgICAqIEBwYXJhbSB7RWxlbWVudH0gZWxlbWVudFxuICAgICAqL1xuICAgIGZ1bmN0aW9uIGdldEF0dHJpYnV0ZVZhbHVlKHN1ZmZpeCwgZWxlbWVudCkge1xuICAgICAgICB2YXIgYXR0cmlidXRlID0gJ2RhdGEtY2xpcGJvYXJkLScgKyBzdWZmaXg7XG5cbiAgICAgICAgaWYgKCFlbGVtZW50Lmhhc0F0dHJpYnV0ZShhdHRyaWJ1dGUpKSB7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gZWxlbWVudC5nZXRBdHRyaWJ1dGUoYXR0cmlidXRlKTtcbiAgICB9XG5cbiAgICBtb2R1bGUuZXhwb3J0cyA9IENsaXBib2FyZDtcbn0pO1xuXG4vKioqLyB9KSxcbi8qIDQgKi9cbi8qKiovIChmdW5jdGlvbihtb2R1bGUsIGV4cG9ydHMpIHtcblxudmFyIERPQ1VNRU5UX05PREVfVFlQRSA9IDk7XG5cbi8qKlxuICogQSBwb2x5ZmlsbCBmb3IgRWxlbWVudC5tYXRjaGVzKClcbiAqL1xuaWYgKHR5cGVvZiBFbGVtZW50ICE9PSAndW5kZWZpbmVkJyAmJiAhRWxlbWVudC5wcm90b3R5cGUubWF0Y2hlcykge1xuICAgIHZhciBwcm90byA9IEVsZW1lbnQucHJvdG90eXBlO1xuXG4gICAgcHJvdG8ubWF0Y2hlcyA9IHByb3RvLm1hdGNoZXNTZWxlY3RvciB8fFxuICAgICAgICAgICAgICAgICAgICBwcm90by5tb3pNYXRjaGVzU2VsZWN0b3IgfHxcbiAgICAgICAgICAgICAgICAgICAgcHJvdG8ubXNNYXRjaGVzU2VsZWN0b3IgfHxcbiAgICAgICAgICAgICAgICAgICAgcHJvdG8ub01hdGNoZXNTZWxlY3RvciB8fFxuICAgICAgICAgICAgICAgICAgICBwcm90by53ZWJraXRNYXRjaGVzU2VsZWN0b3I7XG59XG5cbi8qKlxuICogRmluZHMgdGhlIGNsb3Nlc3QgcGFyZW50IHRoYXQgbWF0Y2hlcyBhIHNlbGVjdG9yLlxuICpcbiAqIEBwYXJhbSB7RWxlbWVudH0gZWxlbWVudFxuICogQHBhcmFtIHtTdHJpbmd9IHNlbGVjdG9yXG4gKiBAcmV0dXJuIHtGdW5jdGlvbn1cbiAqL1xuZnVuY3Rpb24gY2xvc2VzdCAoZWxlbWVudCwgc2VsZWN0b3IpIHtcbiAgICB3aGlsZSAoZWxlbWVudCAmJiBlbGVtZW50Lm5vZGVUeXBlICE9PSBET0NVTUVOVF9OT0RFX1RZUEUpIHtcbiAgICAgICAgaWYgKHR5cGVvZiBlbGVtZW50Lm1hdGNoZXMgPT09ICdmdW5jdGlvbicgJiZcbiAgICAgICAgICAgIGVsZW1lbnQubWF0Y2hlcyhzZWxlY3RvcikpIHtcbiAgICAgICAgICByZXR1cm4gZWxlbWVudDtcbiAgICAgICAgfVxuICAgICAgICBlbGVtZW50ID0gZWxlbWVudC5wYXJlbnROb2RlO1xuICAgIH1cbn1cblxubW9kdWxlLmV4cG9ydHMgPSBjbG9zZXN0O1xuXG5cbi8qKiovIH0pLFxuLyogNSAqL1xuLyoqKi8gKGZ1bmN0aW9uKG1vZHVsZSwgZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXykge1xuXG52YXIgY2xvc2VzdCA9IF9fd2VicGFja19yZXF1aXJlX18oNCk7XG5cbi8qKlxuICogRGVsZWdhdGVzIGV2ZW50IHRvIGEgc2VsZWN0b3IuXG4gKlxuICogQHBhcmFtIHtFbGVtZW50fSBlbGVtZW50XG4gKiBAcGFyYW0ge1N0cmluZ30gc2VsZWN0b3JcbiAqIEBwYXJhbSB7U3RyaW5nfSB0eXBlXG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSBjYWxsYmFja1xuICogQHBhcmFtIHtCb29sZWFufSB1c2VDYXB0dXJlXG4gKiBAcmV0dXJuIHtPYmplY3R9XG4gKi9cbmZ1bmN0aW9uIF9kZWxlZ2F0ZShlbGVtZW50LCBzZWxlY3RvciwgdHlwZSwgY2FsbGJhY2ssIHVzZUNhcHR1cmUpIHtcbiAgICB2YXIgbGlzdGVuZXJGbiA9IGxpc3RlbmVyLmFwcGx5KHRoaXMsIGFyZ3VtZW50cyk7XG5cbiAgICBlbGVtZW50LmFkZEV2ZW50TGlzdGVuZXIodHlwZSwgbGlzdGVuZXJGbiwgdXNlQ2FwdHVyZSk7XG5cbiAgICByZXR1cm4ge1xuICAgICAgICBkZXN0cm95OiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIGVsZW1lbnQucmVtb3ZlRXZlbnRMaXN0ZW5lcih0eXBlLCBsaXN0ZW5lckZuLCB1c2VDYXB0dXJlKTtcbiAgICAgICAgfVxuICAgIH1cbn1cblxuLyoqXG4gKiBEZWxlZ2F0ZXMgZXZlbnQgdG8gYSBzZWxlY3Rvci5cbiAqXG4gKiBAcGFyYW0ge0VsZW1lbnR8U3RyaW5nfEFycmF5fSBbZWxlbWVudHNdXG4gKiBAcGFyYW0ge1N0cmluZ30gc2VsZWN0b3JcbiAqIEBwYXJhbSB7U3RyaW5nfSB0eXBlXG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSBjYWxsYmFja1xuICogQHBhcmFtIHtCb29sZWFufSB1c2VDYXB0dXJlXG4gKiBAcmV0dXJuIHtPYmplY3R9XG4gKi9cbmZ1bmN0aW9uIGRlbGVnYXRlKGVsZW1lbnRzLCBzZWxlY3RvciwgdHlwZSwgY2FsbGJhY2ssIHVzZUNhcHR1cmUpIHtcbiAgICAvLyBIYW5kbGUgdGhlIHJlZ3VsYXIgRWxlbWVudCB1c2FnZVxuICAgIGlmICh0eXBlb2YgZWxlbWVudHMuYWRkRXZlbnRMaXN0ZW5lciA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgICByZXR1cm4gX2RlbGVnYXRlLmFwcGx5KG51bGwsIGFyZ3VtZW50cyk7XG4gICAgfVxuXG4gICAgLy8gSGFuZGxlIEVsZW1lbnQtbGVzcyB1c2FnZSwgaXQgZGVmYXVsdHMgdG8gZ2xvYmFsIGRlbGVnYXRpb25cbiAgICBpZiAodHlwZW9mIHR5cGUgPT09ICdmdW5jdGlvbicpIHtcbiAgICAgICAgLy8gVXNlIGBkb2N1bWVudGAgYXMgdGhlIGZpcnN0IHBhcmFtZXRlciwgdGhlbiBhcHBseSBhcmd1bWVudHNcbiAgICAgICAgLy8gVGhpcyBpcyBhIHNob3J0IHdheSB0byAudW5zaGlmdCBgYXJndW1lbnRzYCB3aXRob3V0IHJ1bm5pbmcgaW50byBkZW9wdGltaXphdGlvbnNcbiAgICAgICAgcmV0dXJuIF9kZWxlZ2F0ZS5iaW5kKG51bGwsIGRvY3VtZW50KS5hcHBseShudWxsLCBhcmd1bWVudHMpO1xuICAgIH1cblxuICAgIC8vIEhhbmRsZSBTZWxlY3Rvci1iYXNlZCB1c2FnZVxuICAgIGlmICh0eXBlb2YgZWxlbWVudHMgPT09ICdzdHJpbmcnKSB7XG4gICAgICAgIGVsZW1lbnRzID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbChlbGVtZW50cyk7XG4gICAgfVxuXG4gICAgLy8gSGFuZGxlIEFycmF5LWxpa2UgYmFzZWQgdXNhZ2VcbiAgICByZXR1cm4gQXJyYXkucHJvdG90eXBlLm1hcC5jYWxsKGVsZW1lbnRzLCBmdW5jdGlvbiAoZWxlbWVudCkge1xuICAgICAgICByZXR1cm4gX2RlbGVnYXRlKGVsZW1lbnQsIHNlbGVjdG9yLCB0eXBlLCBjYWxsYmFjaywgdXNlQ2FwdHVyZSk7XG4gICAgfSk7XG59XG5cbi8qKlxuICogRmluZHMgY2xvc2VzdCBtYXRjaCBhbmQgaW52b2tlcyBjYWxsYmFjay5cbiAqXG4gKiBAcGFyYW0ge0VsZW1lbnR9IGVsZW1lbnRcbiAqIEBwYXJhbSB7U3RyaW5nfSBzZWxlY3RvclxuICogQHBhcmFtIHtTdHJpbmd9IHR5cGVcbiAqIEBwYXJhbSB7RnVuY3Rpb259IGNhbGxiYWNrXG4gKiBAcmV0dXJuIHtGdW5jdGlvbn1cbiAqL1xuZnVuY3Rpb24gbGlzdGVuZXIoZWxlbWVudCwgc2VsZWN0b3IsIHR5cGUsIGNhbGxiYWNrKSB7XG4gICAgcmV0dXJuIGZ1bmN0aW9uKGUpIHtcbiAgICAgICAgZS5kZWxlZ2F0ZVRhcmdldCA9IGNsb3Nlc3QoZS50YXJnZXQsIHNlbGVjdG9yKTtcblxuICAgICAgICBpZiAoZS5kZWxlZ2F0ZVRhcmdldCkge1xuICAgICAgICAgICAgY2FsbGJhY2suY2FsbChlbGVtZW50LCBlKTtcbiAgICAgICAgfVxuICAgIH1cbn1cblxubW9kdWxlLmV4cG9ydHMgPSBkZWxlZ2F0ZTtcblxuXG4vKioqLyB9KSxcbi8qIDYgKi9cbi8qKiovIChmdW5jdGlvbihtb2R1bGUsIGV4cG9ydHMpIHtcblxuLyoqXG4gKiBDaGVjayBpZiBhcmd1bWVudCBpcyBhIEhUTUwgZWxlbWVudC5cbiAqXG4gKiBAcGFyYW0ge09iamVjdH0gdmFsdWVcbiAqIEByZXR1cm4ge0Jvb2xlYW59XG4gKi9cbmV4cG9ydHMubm9kZSA9IGZ1bmN0aW9uKHZhbHVlKSB7XG4gICAgcmV0dXJuIHZhbHVlICE9PSB1bmRlZmluZWRcbiAgICAgICAgJiYgdmFsdWUgaW5zdGFuY2VvZiBIVE1MRWxlbWVudFxuICAgICAgICAmJiB2YWx1ZS5ub2RlVHlwZSA9PT0gMTtcbn07XG5cbi8qKlxuICogQ2hlY2sgaWYgYXJndW1lbnQgaXMgYSBsaXN0IG9mIEhUTUwgZWxlbWVudHMuXG4gKlxuICogQHBhcmFtIHtPYmplY3R9IHZhbHVlXG4gKiBAcmV0dXJuIHtCb29sZWFufVxuICovXG5leHBvcnRzLm5vZGVMaXN0ID0gZnVuY3Rpb24odmFsdWUpIHtcbiAgICB2YXIgdHlwZSA9IE9iamVjdC5wcm90b3R5cGUudG9TdHJpbmcuY2FsbCh2YWx1ZSk7XG5cbiAgICByZXR1cm4gdmFsdWUgIT09IHVuZGVmaW5lZFxuICAgICAgICAmJiAodHlwZSA9PT0gJ1tvYmplY3QgTm9kZUxpc3RdJyB8fCB0eXBlID09PSAnW29iamVjdCBIVE1MQ29sbGVjdGlvbl0nKVxuICAgICAgICAmJiAoJ2xlbmd0aCcgaW4gdmFsdWUpXG4gICAgICAgICYmICh2YWx1ZS5sZW5ndGggPT09IDAgfHwgZXhwb3J0cy5ub2RlKHZhbHVlWzBdKSk7XG59O1xuXG4vKipcbiAqIENoZWNrIGlmIGFyZ3VtZW50IGlzIGEgc3RyaW5nLlxuICpcbiAqIEBwYXJhbSB7T2JqZWN0fSB2YWx1ZVxuICogQHJldHVybiB7Qm9vbGVhbn1cbiAqL1xuZXhwb3J0cy5zdHJpbmcgPSBmdW5jdGlvbih2YWx1ZSkge1xuICAgIHJldHVybiB0eXBlb2YgdmFsdWUgPT09ICdzdHJpbmcnXG4gICAgICAgIHx8IHZhbHVlIGluc3RhbmNlb2YgU3RyaW5nO1xufTtcblxuLyoqXG4gKiBDaGVjayBpZiBhcmd1bWVudCBpcyBhIGZ1bmN0aW9uLlxuICpcbiAqIEBwYXJhbSB7T2JqZWN0fSB2YWx1ZVxuICogQHJldHVybiB7Qm9vbGVhbn1cbiAqL1xuZXhwb3J0cy5mbiA9IGZ1bmN0aW9uKHZhbHVlKSB7XG4gICAgdmFyIHR5cGUgPSBPYmplY3QucHJvdG90eXBlLnRvU3RyaW5nLmNhbGwodmFsdWUpO1xuXG4gICAgcmV0dXJuIHR5cGUgPT09ICdbb2JqZWN0IEZ1bmN0aW9uXSc7XG59O1xuXG5cbi8qKiovIH0pLFxuLyogNyAqL1xuLyoqKi8gKGZ1bmN0aW9uKG1vZHVsZSwgZXhwb3J0cykge1xuXG5mdW5jdGlvbiBzZWxlY3QoZWxlbWVudCkge1xuICAgIHZhciBzZWxlY3RlZFRleHQ7XG5cbiAgICBpZiAoZWxlbWVudC5ub2RlTmFtZSA9PT0gJ1NFTEVDVCcpIHtcbiAgICAgICAgZWxlbWVudC5mb2N1cygpO1xuXG4gICAgICAgIHNlbGVjdGVkVGV4dCA9IGVsZW1lbnQudmFsdWU7XG4gICAgfVxuICAgIGVsc2UgaWYgKGVsZW1lbnQubm9kZU5hbWUgPT09ICdJTlBVVCcgfHwgZWxlbWVudC5ub2RlTmFtZSA9PT0gJ1RFWFRBUkVBJykge1xuICAgICAgICB2YXIgaXNSZWFkT25seSA9IGVsZW1lbnQuaGFzQXR0cmlidXRlKCdyZWFkb25seScpO1xuXG4gICAgICAgIGlmICghaXNSZWFkT25seSkge1xuICAgICAgICAgICAgZWxlbWVudC5zZXRBdHRyaWJ1dGUoJ3JlYWRvbmx5JywgJycpO1xuICAgICAgICB9XG5cbiAgICAgICAgZWxlbWVudC5zZWxlY3QoKTtcbiAgICAgICAgZWxlbWVudC5zZXRTZWxlY3Rpb25SYW5nZSgwLCBlbGVtZW50LnZhbHVlLmxlbmd0aCk7XG5cbiAgICAgICAgaWYgKCFpc1JlYWRPbmx5KSB7XG4gICAgICAgICAgICBlbGVtZW50LnJlbW92ZUF0dHJpYnV0ZSgncmVhZG9ubHknKTtcbiAgICAgICAgfVxuXG4gICAgICAgIHNlbGVjdGVkVGV4dCA9IGVsZW1lbnQudmFsdWU7XG4gICAgfVxuICAgIGVsc2Uge1xuICAgICAgICBpZiAoZWxlbWVudC5oYXNBdHRyaWJ1dGUoJ2NvbnRlbnRlZGl0YWJsZScpKSB7XG4gICAgICAgICAgICBlbGVtZW50LmZvY3VzKCk7XG4gICAgICAgIH1cblxuICAgICAgICB2YXIgc2VsZWN0aW9uID0gd2luZG93LmdldFNlbGVjdGlvbigpO1xuICAgICAgICB2YXIgcmFuZ2UgPSBkb2N1bWVudC5jcmVhdGVSYW5nZSgpO1xuXG4gICAgICAgIHJhbmdlLnNlbGVjdE5vZGVDb250ZW50cyhlbGVtZW50KTtcbiAgICAgICAgc2VsZWN0aW9uLnJlbW92ZUFsbFJhbmdlcygpO1xuICAgICAgICBzZWxlY3Rpb24uYWRkUmFuZ2UocmFuZ2UpO1xuXG4gICAgICAgIHNlbGVjdGVkVGV4dCA9IHNlbGVjdGlvbi50b1N0cmluZygpO1xuICAgIH1cblxuICAgIHJldHVybiBzZWxlY3RlZFRleHQ7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gc2VsZWN0O1xuXG5cbi8qKiovIH0pXG4vKioqKioqLyBdKTtcbn0pO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3ByaXNtanMvbm9kZV9tb2R1bGVzL2NsaXBib2FyZC9kaXN0L2NsaXBib2FyZC5qc1xuLy8gbW9kdWxlIGlkID0gMTI3XG4vLyBtb2R1bGUgY2h1bmtzID0gMTAiLCIoZnVuY3Rpb24oKSB7XG5cblx0aWYgKFxuXHRcdHR5cGVvZiBzZWxmICE9PSAndW5kZWZpbmVkJyAmJiAhc2VsZi5QcmlzbSB8fFxuXHRcdCFzZWxmLmRvY3VtZW50IHx8ICFGdW5jdGlvbi5wcm90b3R5cGUuYmluZFxuXHQpIHtcblx0XHRyZXR1cm47XG5cdH1cblxuXHR2YXIgcHJldmlld2VycyA9IHtcblx0XHQvLyBncmFkaWVudCBtdXN0IGJlIGRlZmluZWQgYmVmb3JlIGNvbG9yIGFuZCBhbmdsZVxuXHRcdCdncmFkaWVudCc6IHtcblx0XHRcdGNyZWF0ZTogKGZ1bmN0aW9uICgpIHtcblxuXHRcdFx0XHQvLyBTdG9yZXMgYWxyZWFkeSBwcm9jZXNzZWQgZ3JhZGllbnRzIHNvIHRoYXQgd2UgZG9uJ3Rcblx0XHRcdFx0Ly8gbWFrZSB0aGUgY29udmVyc2lvbiBldmVyeSB0aW1lIHRoZSBwcmV2aWV3ZXIgaXMgc2hvd25cblx0XHRcdFx0dmFyIGNhY2hlID0ge307XG5cblx0XHRcdFx0LyoqXG5cdFx0XHRcdCAqIFJldHVybnMgYSBXM0MtdmFsaWQgbGluZWFyIGdyYWRpZW50XG5cdFx0XHRcdCAqIEBwYXJhbSB7c3RyaW5nfSBwcmVmaXggVmVuZG9yIHByZWZpeCBpZiBhbnkgKFwiLW1vei1cIiwgXCItd2Via2l0LVwiLCBldGMuKVxuXHRcdFx0XHQgKiBAcGFyYW0ge3N0cmluZ30gZnVuYyBHcmFkaWVudCBmdW5jdGlvbiBuYW1lIChcImxpbmVhci1ncmFkaWVudFwiKVxuXHRcdFx0XHQgKiBAcGFyYW0ge3N0cmluZ1tdfSB2YWx1ZXMgQXJyYXkgb2YgdGhlIGdyYWRpZW50IGZ1bmN0aW9uIHBhcmFtZXRlcnMgKFtcIjBkZWdcIiwgXCJyZWQgMCVcIiwgXCJibHVlIDEwMCVcIl0pXG5cdFx0XHRcdCAqL1xuXHRcdFx0XHR2YXIgY29udmVydFRvVzNDTGluZWFyR3JhZGllbnQgPSBmdW5jdGlvbihwcmVmaXgsIGZ1bmMsIHZhbHVlcykge1xuXHRcdFx0XHRcdC8vIERlZmF1bHQgdmFsdWUgZm9yIGFuZ2xlXG5cdFx0XHRcdFx0dmFyIGFuZ2xlID0gJzE4MGRlZyc7XG5cblx0XHRcdFx0XHRpZiAoL14oPzotP1xcZCpcXC4/XFxkKyg/OmRlZ3xyYWQpfHRvXFxifHRvcHxyaWdodHxib3R0b218bGVmdCkvLnRlc3QodmFsdWVzWzBdKSkge1xuXHRcdFx0XHRcdFx0YW5nbGUgPSB2YWx1ZXMuc2hpZnQoKTtcblx0XHRcdFx0XHRcdGlmIChhbmdsZS5pbmRleE9mKCd0byAnKSA8IDApIHtcblx0XHRcdFx0XHRcdFx0Ly8gQW5nbGUgdXNlcyBvbGQga2V5d29yZHNcblx0XHRcdFx0XHRcdFx0Ly8gVzNDIHN5bnRheCB1c2VzIFwidG9cIiArIG9wcG9zaXRlIGtleXdvcmRzXG5cdFx0XHRcdFx0XHRcdGlmIChhbmdsZS5pbmRleE9mKCd0b3AnKSA+PSAwKSB7XG5cdFx0XHRcdFx0XHRcdFx0aWYgKGFuZ2xlLmluZGV4T2YoJ2xlZnQnKSA+PSAwKSB7XG5cdFx0XHRcdFx0XHRcdFx0XHRhbmdsZSA9ICd0byBib3R0b20gcmlnaHQnO1xuXHRcdFx0XHRcdFx0XHRcdH0gZWxzZSBpZiAoYW5nbGUuaW5kZXhPZigncmlnaHQnKSA+PSAwKSB7XG5cdFx0XHRcdFx0XHRcdFx0XHRhbmdsZSA9ICd0byBib3R0b20gbGVmdCc7XG5cdFx0XHRcdFx0XHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0XHRcdFx0XHRcdGFuZ2xlID0gJ3RvIGJvdHRvbSc7XG5cdFx0XHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFx0XHR9IGVsc2UgaWYgKGFuZ2xlLmluZGV4T2YoJ2JvdHRvbScpID49IDApIHtcblx0XHRcdFx0XHRcdFx0XHRpZiAoYW5nbGUuaW5kZXhPZignbGVmdCcpID49IDApIHtcblx0XHRcdFx0XHRcdFx0XHRcdGFuZ2xlID0gJ3RvIHRvcCByaWdodCc7XG5cdFx0XHRcdFx0XHRcdFx0fSBlbHNlIGlmIChhbmdsZS5pbmRleE9mKCdyaWdodCcpID49IDApIHtcblx0XHRcdFx0XHRcdFx0XHRcdGFuZ2xlID0gJ3RvIHRvcCBsZWZ0Jztcblx0XHRcdFx0XHRcdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRcdFx0XHRcdFx0YW5nbGUgPSAndG8gdG9wJztcblx0XHRcdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XHRcdH0gZWxzZSBpZiAoYW5nbGUuaW5kZXhPZignbGVmdCcpID49IDApIHtcblx0XHRcdFx0XHRcdFx0XHRhbmdsZSA9ICd0byByaWdodCc7XG5cdFx0XHRcdFx0XHRcdH0gZWxzZSBpZiAoYW5nbGUuaW5kZXhPZigncmlnaHQnKSA+PSAwKSB7XG5cdFx0XHRcdFx0XHRcdFx0YW5nbGUgPSAndG8gbGVmdCc7XG5cdFx0XHRcdFx0XHRcdH0gZWxzZSBpZiAocHJlZml4KSB7XG5cdFx0XHRcdFx0XHRcdFx0Ly8gQW5nbGUgaXMgc2hpZnRlZCBieSA5MGRlZyBpbiBwcmVmaXhlZCBncmFkaWVudHNcblx0XHRcdFx0XHRcdFx0XHRpZiAoYW5nbGUuaW5kZXhPZignZGVnJykgPj0gMCkge1xuXHRcdFx0XHRcdFx0XHRcdFx0YW5nbGUgPSAoOTAgLSBwYXJzZUZsb2F0KGFuZ2xlKSkgKyAnZGVnJztcblx0XHRcdFx0XHRcdFx0XHR9IGVsc2UgaWYgKGFuZ2xlLmluZGV4T2YoJ3JhZCcpID49IDApIHtcblx0XHRcdFx0XHRcdFx0XHRcdGFuZ2xlID0gKE1hdGguUEkgLyAyIC0gcGFyc2VGbG9hdChhbmdsZSkpICsgJ3JhZCc7XG5cdFx0XHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0fVxuXG5cdFx0XHRcdFx0cmV0dXJuIGZ1bmMgKyAnKCcgKyBhbmdsZSArICcsJyArIHZhbHVlcy5qb2luKCcsJykgKyAnKSc7XG5cdFx0XHRcdH07XG5cblx0XHRcdFx0LyoqXG5cdFx0XHRcdCAqIFJldHVybnMgYSBXM0MtdmFsaWQgcmFkaWFsIGdyYWRpZW50XG5cdFx0XHRcdCAqIEBwYXJhbSB7c3RyaW5nfSBwcmVmaXggVmVuZG9yIHByZWZpeCBpZiBhbnkgKFwiLW1vei1cIiwgXCItd2Via2l0LVwiLCBldGMuKVxuXHRcdFx0XHQgKiBAcGFyYW0ge3N0cmluZ30gZnVuYyBHcmFkaWVudCBmdW5jdGlvbiBuYW1lIChcImxpbmVhci1ncmFkaWVudFwiKVxuXHRcdFx0XHQgKiBAcGFyYW0ge3N0cmluZ1tdfSB2YWx1ZXMgQXJyYXkgb2YgdGhlIGdyYWRpZW50IGZ1bmN0aW9uIHBhcmFtZXRlcnMgKFtcIjBkZWdcIiwgXCJyZWQgMCVcIiwgXCJibHVlIDEwMCVcIl0pXG5cdFx0XHRcdCAqL1xuXHRcdFx0XHR2YXIgY29udmVydFRvVzNDUmFkaWFsR3JhZGllbnQgPSBmdW5jdGlvbihwcmVmaXgsIGZ1bmMsIHZhbHVlcykge1xuXHRcdFx0XHRcdGlmICh2YWx1ZXNbMF0uaW5kZXhPZignYXQnKSA8IDApIHtcblx0XHRcdFx0XHRcdC8vIExvb2tzIGxpa2Ugb2xkIHN5bnRheFxuXG5cdFx0XHRcdFx0XHQvLyBEZWZhdWx0IHZhbHVlc1xuXHRcdFx0XHRcdFx0dmFyIHBvc2l0aW9uID0gJ2NlbnRlcic7XG5cdFx0XHRcdFx0XHR2YXIgc2hhcGUgPSAnZWxsaXBzZSc7XG5cdFx0XHRcdFx0XHR2YXIgc2l6ZSA9ICdmYXJ0aGVzdC1jb3JuZXInO1xuXG5cdFx0XHRcdFx0XHRpZiAoL1xcYmNlbnRlcnx0b3B8cmlnaHR8Ym90dG9tfGxlZnRcXGJ8XlxcZCsvLnRlc3QodmFsdWVzWzBdKSkge1xuXHRcdFx0XHRcdFx0XHQvLyBGb3VuZCBhIHBvc2l0aW9uXG5cdFx0XHRcdFx0XHRcdC8vIFJlbW92ZSBhbmdsZSB2YWx1ZSwgaWYgYW55XG5cdFx0XHRcdFx0XHRcdHBvc2l0aW9uID0gdmFsdWVzLnNoaWZ0KCkucmVwbGFjZSgvXFxzKi0/XFxkKyg/OnJhZHxkZWcpXFxzKi8sICcnKTtcblx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHRcdGlmICgvXFxiY2lyY2xlfGVsbGlwc2V8Y2xvc2VzdHxmYXJ0aGVzdHxjb250YWlufGNvdmVyXFxiLy50ZXN0KHZhbHVlc1swXSkpIHtcblx0XHRcdFx0XHRcdFx0Ly8gRm91bmQgYSBzaGFwZSBhbmQvb3Igc2l6ZVxuXHRcdFx0XHRcdFx0XHR2YXIgc2hhcGVTaXplUGFydHMgPSB2YWx1ZXMuc2hpZnQoKS5zcGxpdCgvXFxzKy8pO1xuXHRcdFx0XHRcdFx0XHRpZiAoc2hhcGVTaXplUGFydHNbMF0gJiYgKHNoYXBlU2l6ZVBhcnRzWzBdID09PSAnY2lyY2xlJyB8fCBzaGFwZVNpemVQYXJ0c1swXSA9PT0gJ2VsbGlwc2UnKSkge1xuXHRcdFx0XHRcdFx0XHRcdHNoYXBlID0gc2hhcGVTaXplUGFydHMuc2hpZnQoKTtcblx0XHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFx0XHRpZiAoc2hhcGVTaXplUGFydHNbMF0pIHtcblx0XHRcdFx0XHRcdFx0XHRzaXplID0gc2hhcGVTaXplUGFydHMuc2hpZnQoKTtcblx0XHRcdFx0XHRcdFx0fVxuXG5cdFx0XHRcdFx0XHRcdC8vIE9sZCBrZXl3b3JkcyBhcmUgY29udmVydGVkIHRvIHRoZWlyIHN5bm9ueW1zXG5cdFx0XHRcdFx0XHRcdGlmIChzaXplID09PSAnY292ZXInKSB7XG5cdFx0XHRcdFx0XHRcdFx0c2l6ZSA9ICdmYXJ0aGVzdC1jb3JuZXInO1xuXHRcdFx0XHRcdFx0XHR9IGVsc2UgaWYgKHNpemUgPT09ICdjb250YWluJykge1xuXHRcdFx0XHRcdFx0XHRcdHNpemUgPSAnY2xvdGhlc3Qtc2lkZSc7XG5cdFx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHRcdH1cblxuXHRcdFx0XHRcdFx0cmV0dXJuIGZ1bmMgKyAnKCcgKyBzaGFwZSArICcgJyArIHNpemUgKyAnIGF0ICcgKyBwb3NpdGlvbiArICcsJyArIHZhbHVlcy5qb2luKCcsJykgKyAnKSc7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRcdHJldHVybiBmdW5jICsgJygnICsgdmFsdWVzLmpvaW4oJywnKSArICcpJztcblx0XHRcdFx0fTtcblxuXHRcdFx0XHQvKipcblx0XHRcdFx0ICogQ29udmVydHMgYSBncmFkaWVudCB0byBhIFczQy12YWxpZCBvbmVcblx0XHRcdFx0ICogRG9lcyBub3Qgc3VwcG9ydCBvbGQgd2Via2l0IHN5bnRheCAoLXdlYmtpdC1ncmFkaWVudChsaW5lYXIuLi4pIGFuZCAtd2Via2l0LWdyYWRpZW50KHJhZGlhbC4uLikpXG5cdFx0XHRcdCAqIEBwYXJhbSB7c3RyaW5nfSBncmFkaWVudCBUaGUgQ1NTIGdyYWRpZW50XG5cdFx0XHRcdCAqL1xuXHRcdFx0XHR2YXIgY29udmVydFRvVzNDR3JhZGllbnQgPSBmdW5jdGlvbihncmFkaWVudCkge1xuXHRcdFx0XHRcdGlmIChjYWNoZVtncmFkaWVudF0pIHtcblx0XHRcdFx0XHRcdHJldHVybiBjYWNoZVtncmFkaWVudF07XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRcdHZhciBwYXJ0cyA9IGdyYWRpZW50Lm1hdGNoKC9eKFxcYnxcXEItW2Etel17MSwxMH0tKSgoPzpyZXBlYXRpbmctKT8oPzpsaW5lYXJ8cmFkaWFsKS1ncmFkaWVudCkvKTtcblx0XHRcdFx0XHQvLyBcIlwiLCBcIi1tb3otXCIsIGV0Yy5cblx0XHRcdFx0XHR2YXIgcHJlZml4ID0gcGFydHMgJiYgcGFydHNbMV07XG5cdFx0XHRcdFx0Ly8gXCJsaW5lYXItZ3JhZGllbnRcIiwgXCJyYWRpYWwtZ3JhZGllbnRcIiwgZXRjLlxuXHRcdFx0XHRcdHZhciBmdW5jID0gcGFydHMgJiYgcGFydHNbMl07XG5cblx0XHRcdFx0XHR2YXIgdmFsdWVzID0gZ3JhZGllbnQucmVwbGFjZSgvXig/OlxcYnxcXEItW2Etel17MSwxMH0tKSg/OnJlcGVhdGluZy0pPyg/OmxpbmVhcnxyYWRpYWwpLWdyYWRpZW50XFwofFxcKSQvZywgJycpLnNwbGl0KC9cXHMqLFxccyovKTtcblxuXHRcdFx0XHRcdGlmIChmdW5jLmluZGV4T2YoJ2xpbmVhcicpID49IDApIHtcblx0XHRcdFx0XHRcdHJldHVybiBjYWNoZVtncmFkaWVudF0gPSBjb252ZXJ0VG9XM0NMaW5lYXJHcmFkaWVudChwcmVmaXgsIGZ1bmMsIHZhbHVlcyk7XG5cdFx0XHRcdFx0fSBlbHNlIGlmIChmdW5jLmluZGV4T2YoJ3JhZGlhbCcpID49IDApIHtcblx0XHRcdFx0XHRcdHJldHVybiBjYWNoZVtncmFkaWVudF0gPSBjb252ZXJ0VG9XM0NSYWRpYWxHcmFkaWVudChwcmVmaXgsIGZ1bmMsIHZhbHVlcyk7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRcdHJldHVybiBjYWNoZVtncmFkaWVudF0gPSBmdW5jICsgJygnICsgdmFsdWVzLmpvaW4oJywnKSArICcpJztcblx0XHRcdFx0fTtcblxuXHRcdFx0XHRyZXR1cm4gZnVuY3Rpb24gKCkge1xuXHRcdFx0XHRcdG5ldyBQcmlzbS5wbHVnaW5zLlByZXZpZXdlcignZ3JhZGllbnQnLCBmdW5jdGlvbih2YWx1ZSkge1xuXHRcdFx0XHRcdFx0dGhpcy5maXJzdENoaWxkLnN0eWxlLmJhY2tncm91bmRJbWFnZSA9ICcnO1xuXHRcdFx0XHRcdFx0dGhpcy5maXJzdENoaWxkLnN0eWxlLmJhY2tncm91bmRJbWFnZSA9IGNvbnZlcnRUb1czQ0dyYWRpZW50KHZhbHVlKTtcblx0XHRcdFx0XHRcdHJldHVybiAhIXRoaXMuZmlyc3RDaGlsZC5zdHlsZS5iYWNrZ3JvdW5kSW1hZ2U7XG5cdFx0XHRcdFx0fSwgJyonLCBmdW5jdGlvbiAoKSB7XG5cdFx0XHRcdFx0XHR0aGlzLl9lbHQuaW5uZXJIVE1MID0gJzxkaXY+PC9kaXY+Jztcblx0XHRcdFx0XHR9KTtcblx0XHRcdFx0fTtcblx0XHRcdH0oKSksXG5cdFx0XHR0b2tlbnM6IHtcblx0XHRcdFx0J2dyYWRpZW50Jzoge1xuXHRcdFx0XHRcdHBhdHRlcm46IC8oPzpcXGJ8XFxCLVthLXpdezEsMTB9LSkoPzpyZXBlYXRpbmctKT8oPzpsaW5lYXJ8cmFkaWFsKS1ncmFkaWVudFxcKCg/Oig/OnJnYnxoc2wpYT9cXCguKz9cXCl8W15cXCldKStcXCkvZ2ksXG5cdFx0XHRcdFx0aW5zaWRlOiB7XG5cdFx0XHRcdFx0XHQnZnVuY3Rpb24nOiAvW1xcdy1dKyg/PVxcKCkvLFxuXHRcdFx0XHRcdFx0J3B1bmN0dWF0aW9uJzogL1soKSxdL1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fVxuXHRcdFx0fSxcblx0XHRcdGxhbmd1YWdlczoge1xuXHRcdFx0XHQnY3NzJzogdHJ1ZSxcblx0XHRcdFx0J2xlc3MnOiB0cnVlLFxuXHRcdFx0XHQnc2Fzcyc6IFtcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRsYW5nOiAnc2FzcycsXG5cdFx0XHRcdFx0XHRiZWZvcmU6ICdwdW5jdHVhdGlvbicsXG5cdFx0XHRcdFx0XHRpbnNpZGU6ICdpbnNpZGUnLFxuXHRcdFx0XHRcdFx0cm9vdDogUHJpc20ubGFuZ3VhZ2VzLnNhc3MgJiYgUHJpc20ubGFuZ3VhZ2VzLnNhc3NbJ3ZhcmlhYmxlLWxpbmUnXVxuXHRcdFx0XHRcdH0sXG5cdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0bGFuZzogJ3Nhc3MnLFxuXHRcdFx0XHRcdFx0YmVmb3JlOiAncHVuY3R1YXRpb24nLFxuXHRcdFx0XHRcdFx0aW5zaWRlOiAnaW5zaWRlJyxcblx0XHRcdFx0XHRcdHJvb3Q6IFByaXNtLmxhbmd1YWdlcy5zYXNzICYmIFByaXNtLmxhbmd1YWdlcy5zYXNzWydwcm9wZXJ0eS1saW5lJ11cblx0XHRcdFx0XHR9XG5cdFx0XHRcdF0sXG5cdFx0XHRcdCdzY3NzJzogdHJ1ZSxcblx0XHRcdFx0J3N0eWx1cyc6IFtcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRsYW5nOiAnc3R5bHVzJyxcblx0XHRcdFx0XHRcdGJlZm9yZTogJ2Z1bmMnLFxuXHRcdFx0XHRcdFx0aW5zaWRlOiAncmVzdCcsXG5cdFx0XHRcdFx0XHRyb290OiBQcmlzbS5sYW5ndWFnZXMuc3R5bHVzICYmIFByaXNtLmxhbmd1YWdlcy5zdHlsdXNbJ3Byb3BlcnR5LWRlY2xhcmF0aW9uJ10uaW5zaWRlXG5cdFx0XHRcdFx0fSxcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRsYW5nOiAnc3R5bHVzJyxcblx0XHRcdFx0XHRcdGJlZm9yZTogJ2Z1bmMnLFxuXHRcdFx0XHRcdFx0aW5zaWRlOiAncmVzdCcsXG5cdFx0XHRcdFx0XHRyb290OiBQcmlzbS5sYW5ndWFnZXMuc3R5bHVzICYmIFByaXNtLmxhbmd1YWdlcy5zdHlsdXNbJ3ZhcmlhYmxlLWRlY2xhcmF0aW9uJ10uaW5zaWRlXG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRdXG5cdFx0XHR9XG5cdFx0fSxcblx0XHQnYW5nbGUnOiB7XG5cdFx0XHRjcmVhdGU6IGZ1bmN0aW9uICgpIHtcblx0XHRcdFx0bmV3IFByaXNtLnBsdWdpbnMuUHJldmlld2VyKCdhbmdsZScsIGZ1bmN0aW9uKHZhbHVlKSB7XG5cdFx0XHRcdFx0dmFyIG51bSA9IHBhcnNlRmxvYXQodmFsdWUpO1xuXHRcdFx0XHRcdHZhciB1bml0ID0gdmFsdWUubWF0Y2goL1thLXpdKyQvaSk7XG5cdFx0XHRcdFx0dmFyIG1heCwgcGVyY2VudGFnZTtcblx0XHRcdFx0XHRpZiAoIW51bSB8fCAhdW5pdCkge1xuXHRcdFx0XHRcdFx0cmV0dXJuIGZhbHNlO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0XHR1bml0ID0gdW5pdFswXTtcblxuXHRcdFx0XHRcdHN3aXRjaCh1bml0KSB7XG5cdFx0XHRcdFx0XHRjYXNlICdkZWcnOlxuXHRcdFx0XHRcdFx0XHRtYXggPSAzNjA7XG5cdFx0XHRcdFx0XHRcdGJyZWFrO1xuXHRcdFx0XHRcdFx0Y2FzZSAnZ3JhZCc6XG5cdFx0XHRcdFx0XHRcdG1heCA9IDQwMDtcblx0XHRcdFx0XHRcdFx0YnJlYWs7XG5cdFx0XHRcdFx0XHRjYXNlICdyYWQnOlxuXHRcdFx0XHRcdFx0XHRtYXggPSAyICogTWF0aC5QSTtcblx0XHRcdFx0XHRcdFx0YnJlYWs7XG5cdFx0XHRcdFx0XHRjYXNlICd0dXJuJzpcblx0XHRcdFx0XHRcdFx0bWF4ID0gMTtcblx0XHRcdFx0XHR9XG5cblx0XHRcdFx0XHRwZXJjZW50YWdlID0gMTAwICogbnVtL21heDtcblx0XHRcdFx0XHRwZXJjZW50YWdlICU9IDEwMDtcblxuXHRcdFx0XHRcdHRoaXNbKG51bSA8IDA/ICdzZXQnIDogJ3JlbW92ZScpICsgJ0F0dHJpYnV0ZSddKCdkYXRhLW5lZ2F0aXZlJywgJycpO1xuXHRcdFx0XHRcdHRoaXMucXVlcnlTZWxlY3RvcignY2lyY2xlJykuc3R5bGUuc3Ryb2tlRGFzaGFycmF5ID0gTWF0aC5hYnMocGVyY2VudGFnZSkgKyAnLDUwMCc7XG5cdFx0XHRcdFx0cmV0dXJuIHRydWU7XG5cdFx0XHRcdH0sICcqJywgZnVuY3Rpb24gKCkge1xuXHRcdFx0XHRcdHRoaXMuX2VsdC5pbm5lckhUTUwgPSAnPHN2ZyB2aWV3Qm94PVwiMCAwIDY0IDY0XCI+JyArXG5cdFx0XHRcdFx0XHQnPGNpcmNsZSByPVwiMTZcIiBjeT1cIjMyXCIgY3g9XCIzMlwiPjwvY2lyY2xlPicgK1xuXHRcdFx0XHRcdFx0Jzwvc3ZnPic7XG5cdFx0XHRcdH0pO1xuXHRcdFx0fSxcblx0XHRcdHRva2Vuczoge1xuXHRcdFx0XHQnYW5nbGUnOiAvKD86XFxifFxcQi18KD89XFxCXFwuKSlcXGQqXFwuP1xcZCsoPzpkZWd8Zz9yYWR8dHVybilcXGIvaVxuXHRcdFx0fSxcblx0XHRcdGxhbmd1YWdlczoge1xuXHRcdFx0XHQnY3NzJzogdHJ1ZSxcblx0XHRcdFx0J2xlc3MnOiB0cnVlLFxuXHRcdFx0XHQnbWFya3VwJzoge1xuXHRcdFx0XHRcdGxhbmc6ICdtYXJrdXAnLFxuXHRcdFx0XHRcdGJlZm9yZTogJ3B1bmN0dWF0aW9uJyxcblx0XHRcdFx0XHRpbnNpZGU6ICdpbnNpZGUnLFxuXHRcdFx0XHRcdHJvb3Q6IFByaXNtLmxhbmd1YWdlcy5tYXJrdXAgJiYgUHJpc20ubGFuZ3VhZ2VzLm1hcmt1cFsndGFnJ10uaW5zaWRlWydhdHRyLXZhbHVlJ11cblx0XHRcdFx0fSxcblx0XHRcdFx0J3Nhc3MnOiBbXG5cdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0bGFuZzogJ3Nhc3MnLFxuXHRcdFx0XHRcdFx0aW5zaWRlOiAnaW5zaWRlJyxcblx0XHRcdFx0XHRcdHJvb3Q6IFByaXNtLmxhbmd1YWdlcy5zYXNzICYmIFByaXNtLmxhbmd1YWdlcy5zYXNzWydwcm9wZXJ0eS1saW5lJ11cblx0XHRcdFx0XHR9LFxuXHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdGxhbmc6ICdzYXNzJyxcblx0XHRcdFx0XHRcdGJlZm9yZTogJ29wZXJhdG9yJyxcblx0XHRcdFx0XHRcdGluc2lkZTogJ2luc2lkZScsXG5cdFx0XHRcdFx0XHRyb290OiBQcmlzbS5sYW5ndWFnZXMuc2FzcyAmJiBQcmlzbS5sYW5ndWFnZXMuc2Fzc1sndmFyaWFibGUtbGluZSddXG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRdLFxuXHRcdFx0XHQnc2Nzcyc6IHRydWUsXG5cdFx0XHRcdCdzdHlsdXMnOiBbXG5cdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0bGFuZzogJ3N0eWx1cycsXG5cdFx0XHRcdFx0XHRiZWZvcmU6ICdmdW5jJyxcblx0XHRcdFx0XHRcdGluc2lkZTogJ3Jlc3QnLFxuXHRcdFx0XHRcdFx0cm9vdDogUHJpc20ubGFuZ3VhZ2VzLnN0eWx1cyAmJiBQcmlzbS5sYW5ndWFnZXMuc3R5bHVzWydwcm9wZXJ0eS1kZWNsYXJhdGlvbiddLmluc2lkZVxuXHRcdFx0XHRcdH0sXG5cdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0bGFuZzogJ3N0eWx1cycsXG5cdFx0XHRcdFx0XHRiZWZvcmU6ICdmdW5jJyxcblx0XHRcdFx0XHRcdGluc2lkZTogJ3Jlc3QnLFxuXHRcdFx0XHRcdFx0cm9vdDogUHJpc20ubGFuZ3VhZ2VzLnN0eWx1cyAmJiBQcmlzbS5sYW5ndWFnZXMuc3R5bHVzWyd2YXJpYWJsZS1kZWNsYXJhdGlvbiddLmluc2lkZVxuXHRcdFx0XHRcdH1cblx0XHRcdFx0XVxuXHRcdFx0fVxuXHRcdH0sXG5cdFx0J2NvbG9yJzoge1xuXHRcdFx0Y3JlYXRlOiBmdW5jdGlvbiAoKSB7XG5cdFx0XHRcdG5ldyBQcmlzbS5wbHVnaW5zLlByZXZpZXdlcignY29sb3InLCBmdW5jdGlvbih2YWx1ZSkge1xuXHRcdFx0XHRcdHRoaXMuc3R5bGUuYmFja2dyb3VuZENvbG9yID0gJyc7XG5cdFx0XHRcdFx0dGhpcy5zdHlsZS5iYWNrZ3JvdW5kQ29sb3IgPSB2YWx1ZTtcblx0XHRcdFx0XHRyZXR1cm4gISF0aGlzLnN0eWxlLmJhY2tncm91bmRDb2xvcjtcblx0XHRcdFx0fSk7XG5cdFx0XHR9LFxuXHRcdFx0dG9rZW5zOiB7XG5cdFx0XHRcdCdjb2xvcic6IHtcblx0XHRcdFx0XHRwYXR0ZXJuOiAvXFxCIyg/OlswLTlhLWZdezN9KXsxLDJ9XFxifFxcYig/OnJnYnxoc2wpXFwoXFxzKlxcZHsxLDN9XFxzKixcXHMqXFxkezEsM30lP1xccyosXFxzKlxcZHsxLDN9JT9cXHMqXFwpXFxCfFxcYig/OnJnYnxoc2wpYVxcKFxccypcXGR7MSwzfVxccyosXFxzKlxcZHsxLDN9JT9cXHMqLFxccypcXGR7MSwzfSU/XFxzKixcXHMqKD86MHwwP1xcLlxcZCt8MSlcXHMqXFwpXFxCfFxcYig/OkFsaWNlQmx1ZXxBbnRpcXVlV2hpdGV8QXF1YXxBcXVhbWFyaW5lfEF6dXJlfEJlaWdlfEJpc3F1ZXxCbGFja3xCbGFuY2hlZEFsbW9uZHxCbHVlfEJsdWVWaW9sZXR8QnJvd258QnVybHlXb29kfENhZGV0Qmx1ZXxDaGFydHJldXNlfENob2NvbGF0ZXxDb3JhbHxDb3JuZmxvd2VyQmx1ZXxDb3Juc2lsa3xDcmltc29ufEN5YW58RGFya0JsdWV8RGFya0N5YW58RGFya0dvbGRlblJvZHxEYXJrR3JheXxEYXJrR3JlZW58RGFya0toYWtpfERhcmtNYWdlbnRhfERhcmtPbGl2ZUdyZWVufERhcmtPcmFuZ2V8RGFya09yY2hpZHxEYXJrUmVkfERhcmtTYWxtb258RGFya1NlYUdyZWVufERhcmtTbGF0ZUJsdWV8RGFya1NsYXRlR3JheXxEYXJrVHVycXVvaXNlfERhcmtWaW9sZXR8RGVlcFBpbmt8RGVlcFNreUJsdWV8RGltR3JheXxEb2RnZXJCbHVlfEZpcmVCcmlja3xGbG9yYWxXaGl0ZXxGb3Jlc3RHcmVlbnxGdWNoc2lhfEdhaW5zYm9yb3xHaG9zdFdoaXRlfEdvbGR8R29sZGVuUm9kfEdyYXl8R3JlZW58R3JlZW5ZZWxsb3d8SG9uZXlEZXd8SG90UGlua3xJbmRpYW5SZWR8SW5kaWdvfEl2b3J5fEtoYWtpfExhdmVuZGVyfExhdmVuZGVyQmx1c2h8TGF3bkdyZWVufExlbW9uQ2hpZmZvbnxMaWdodEJsdWV8TGlnaHRDb3JhbHxMaWdodEN5YW58TGlnaHRHb2xkZW5Sb2RZZWxsb3d8TGlnaHRHcmF5fExpZ2h0R3JlZW58TGlnaHRQaW5rfExpZ2h0U2FsbW9ufExpZ2h0U2VhR3JlZW58TGlnaHRTa3lCbHVlfExpZ2h0U2xhdGVHcmF5fExpZ2h0U3RlZWxCbHVlfExpZ2h0WWVsbG93fExpbWV8TGltZUdyZWVufExpbmVufE1hZ2VudGF8TWFyb29ufE1lZGl1bUFxdWFNYXJpbmV8TWVkaXVtQmx1ZXxNZWRpdW1PcmNoaWR8TWVkaXVtUHVycGxlfE1lZGl1bVNlYUdyZWVufE1lZGl1bVNsYXRlQmx1ZXxNZWRpdW1TcHJpbmdHcmVlbnxNZWRpdW1UdXJxdW9pc2V8TWVkaXVtVmlvbGV0UmVkfE1pZG5pZ2h0Qmx1ZXxNaW50Q3JlYW18TWlzdHlSb3NlfE1vY2Nhc2lufE5hdmFqb1doaXRlfE5hdnl8T2xkTGFjZXxPbGl2ZXxPbGl2ZURyYWJ8T3JhbmdlfE9yYW5nZVJlZHxPcmNoaWR8UGFsZUdvbGRlblJvZHxQYWxlR3JlZW58UGFsZVR1cnF1b2lzZXxQYWxlVmlvbGV0UmVkfFBhcGF5YVdoaXB8UGVhY2hQdWZmfFBlcnV8UGlua3xQbHVtfFBvd2RlckJsdWV8UHVycGxlfFJlZHxSb3N5QnJvd258Um95YWxCbHVlfFNhZGRsZUJyb3dufFNhbG1vbnxTYW5keUJyb3dufFNlYUdyZWVufFNlYVNoZWxsfFNpZW5uYXxTaWx2ZXJ8U2t5Qmx1ZXxTbGF0ZUJsdWV8U2xhdGVHcmF5fFNub3d8U3ByaW5nR3JlZW58U3RlZWxCbHVlfFRhbnxUZWFsfFRoaXN0bGV8VG9tYXRvfFR1cnF1b2lzZXxWaW9sZXR8V2hlYXR8V2hpdGV8V2hpdGVTbW9rZXxZZWxsb3d8WWVsbG93R3JlZW4pXFxiL2ksXG5cdFx0XHRcdFx0aW5zaWRlOiB7XG5cdFx0XHRcdFx0XHQnZnVuY3Rpb24nOiAvW1xcdy1dKyg/PVxcKCkvLFxuXHRcdFx0XHRcdFx0J3B1bmN0dWF0aW9uJzogL1soKSxdL1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fVxuXHRcdFx0fSxcblx0XHRcdGxhbmd1YWdlczoge1xuXHRcdFx0XHQnY3NzJzogdHJ1ZSxcblx0XHRcdFx0J2xlc3MnOiB0cnVlLFxuXHRcdFx0XHQnbWFya3VwJzoge1xuXHRcdFx0XHRcdGxhbmc6ICdtYXJrdXAnLFxuXHRcdFx0XHRcdGJlZm9yZTogJ3B1bmN0dWF0aW9uJyxcblx0XHRcdFx0XHRpbnNpZGU6ICdpbnNpZGUnLFxuXHRcdFx0XHRcdHJvb3Q6IFByaXNtLmxhbmd1YWdlcy5tYXJrdXAgJiYgUHJpc20ubGFuZ3VhZ2VzLm1hcmt1cFsndGFnJ10uaW5zaWRlWydhdHRyLXZhbHVlJ11cblx0XHRcdFx0fSxcblx0XHRcdFx0J3Nhc3MnOiBbXG5cdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0bGFuZzogJ3Nhc3MnLFxuXHRcdFx0XHRcdFx0YmVmb3JlOiAncHVuY3R1YXRpb24nLFxuXHRcdFx0XHRcdFx0aW5zaWRlOiAnaW5zaWRlJyxcblx0XHRcdFx0XHRcdHJvb3Q6IFByaXNtLmxhbmd1YWdlcy5zYXNzICYmIFByaXNtLmxhbmd1YWdlcy5zYXNzWyd2YXJpYWJsZS1saW5lJ11cblx0XHRcdFx0XHR9LFxuXHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdGxhbmc6ICdzYXNzJyxcblx0XHRcdFx0XHRcdGluc2lkZTogJ2luc2lkZScsXG5cdFx0XHRcdFx0XHRyb290OiBQcmlzbS5sYW5ndWFnZXMuc2FzcyAmJiBQcmlzbS5sYW5ndWFnZXMuc2Fzc1sncHJvcGVydHktbGluZSddXG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRdLFxuXHRcdFx0XHQnc2Nzcyc6IHRydWUsXG5cdFx0XHRcdCdzdHlsdXMnOiBbXG5cdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0bGFuZzogJ3N0eWx1cycsXG5cdFx0XHRcdFx0XHRiZWZvcmU6ICdoZXhjb2RlJyxcblx0XHRcdFx0XHRcdGluc2lkZTogJ3Jlc3QnLFxuXHRcdFx0XHRcdFx0cm9vdDogUHJpc20ubGFuZ3VhZ2VzLnN0eWx1cyAmJiBQcmlzbS5sYW5ndWFnZXMuc3R5bHVzWydwcm9wZXJ0eS1kZWNsYXJhdGlvbiddLmluc2lkZVxuXHRcdFx0XHRcdH0sXG5cdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0bGFuZzogJ3N0eWx1cycsXG5cdFx0XHRcdFx0XHRiZWZvcmU6ICdoZXhjb2RlJyxcblx0XHRcdFx0XHRcdGluc2lkZTogJ3Jlc3QnLFxuXHRcdFx0XHRcdFx0cm9vdDogUHJpc20ubGFuZ3VhZ2VzLnN0eWx1cyAmJiBQcmlzbS5sYW5ndWFnZXMuc3R5bHVzWyd2YXJpYWJsZS1kZWNsYXJhdGlvbiddLmluc2lkZVxuXHRcdFx0XHRcdH1cblx0XHRcdFx0XVxuXHRcdFx0fVxuXHRcdH0sXG5cdFx0J2Vhc2luZyc6IHtcblx0XHRcdGNyZWF0ZTogZnVuY3Rpb24gKCkge1xuXHRcdFx0XHRuZXcgUHJpc20ucGx1Z2lucy5QcmV2aWV3ZXIoJ2Vhc2luZycsIGZ1bmN0aW9uICh2YWx1ZSkge1xuXG5cdFx0XHRcdFx0dmFsdWUgPSB7XG5cdFx0XHRcdFx0XHQnbGluZWFyJzogJzAsMCwxLDEnLFxuXHRcdFx0XHRcdFx0J2Vhc2UnOiAnLjI1LC4xLC4yNSwxJyxcblx0XHRcdFx0XHRcdCdlYXNlLWluJzogJy40MiwwLDEsMScsXG5cdFx0XHRcdFx0XHQnZWFzZS1vdXQnOiAnMCwwLC41OCwxJyxcblx0XHRcdFx0XHRcdCdlYXNlLWluLW91dCc6Jy40MiwwLC41OCwxJ1xuXHRcdFx0XHRcdH1bdmFsdWVdIHx8IHZhbHVlO1xuXG5cdFx0XHRcdFx0dmFyIHAgPSB2YWx1ZS5tYXRjaCgvLT9cXGQqXFwuP1xcZCsvZyk7XG5cblx0XHRcdFx0XHRpZihwLmxlbmd0aCA9PT0gNCkge1xuXHRcdFx0XHRcdFx0cCA9IHAubWFwKGZ1bmN0aW9uKHAsIGkpIHsgcmV0dXJuIChpICUgMj8gMSAtIHAgOiBwKSAqIDEwMDsgfSk7XG5cblx0XHRcdFx0XHRcdHRoaXMucXVlcnlTZWxlY3RvcigncGF0aCcpLnNldEF0dHJpYnV0ZSgnZCcsICdNMCwxMDAgQycgKyBwWzBdICsgJywnICsgcFsxXSArICcsICcgKyBwWzJdICsgJywnICsgcFszXSArICcsIDEwMCwwJyk7XG5cblx0XHRcdFx0XHRcdHZhciBsaW5lcyA9IHRoaXMucXVlcnlTZWxlY3RvckFsbCgnbGluZScpO1xuXHRcdFx0XHRcdFx0bGluZXNbMF0uc2V0QXR0cmlidXRlKCd4MicsIHBbMF0pO1xuXHRcdFx0XHRcdFx0bGluZXNbMF0uc2V0QXR0cmlidXRlKCd5MicsIHBbMV0pO1xuXHRcdFx0XHRcdFx0bGluZXNbMV0uc2V0QXR0cmlidXRlKCd4MicsIHBbMl0pO1xuXHRcdFx0XHRcdFx0bGluZXNbMV0uc2V0QXR0cmlidXRlKCd5MicsIHBbM10pO1xuXG5cdFx0XHRcdFx0XHRyZXR1cm4gdHJ1ZTtcblx0XHRcdFx0XHR9XG5cblx0XHRcdFx0XHRyZXR1cm4gZmFsc2U7XG5cdFx0XHRcdH0sICcqJywgZnVuY3Rpb24gKCkge1xuXHRcdFx0XHRcdHRoaXMuX2VsdC5pbm5lckhUTUwgPSAnPHN2ZyB2aWV3Qm94PVwiLTIwIC0yMCAxNDAgMTQwXCIgd2lkdGg9XCIxMDBcIiBoZWlnaHQ9XCIxMDBcIj4nICtcblx0XHRcdFx0XHRcdCc8ZGVmcz4nICtcblx0XHRcdFx0XHRcdCc8bWFya2VyIGlkPVwicHJpc20tcHJldmlld2VyLWVhc2luZy1tYXJrZXJcIiB2aWV3Qm94PVwiMCAwIDQgNFwiIHJlZlg9XCIyXCIgcmVmWT1cIjJcIiBtYXJrZXJVbml0cz1cInN0cm9rZVdpZHRoXCI+JyArXG5cdFx0XHRcdFx0XHQnPGNpcmNsZSBjeD1cIjJcIiBjeT1cIjJcIiByPVwiMS41XCIgLz4nICtcblx0XHRcdFx0XHRcdCc8L21hcmtlcj4nICtcblx0XHRcdFx0XHRcdCc8L2RlZnM+JyArXG5cdFx0XHRcdFx0XHQnPHBhdGggZD1cIk0wLDEwMCBDMjAsNTAsIDQwLDMwLCAxMDAsMFwiIC8+JyArXG5cdFx0XHRcdFx0XHQnPGxpbmUgeDE9XCIwXCIgeTE9XCIxMDBcIiB4Mj1cIjIwXCIgeTI9XCI1MFwiIG1hcmtlci1zdGFydD1cInVybCgnICsgbG9jYXRpb24uaHJlZiArICcjcHJpc20tcHJldmlld2VyLWVhc2luZy1tYXJrZXIpXCIgbWFya2VyLWVuZD1cInVybCgnICsgbG9jYXRpb24uaHJlZiArICcjcHJpc20tcHJldmlld2VyLWVhc2luZy1tYXJrZXIpXCIgLz4nICtcblx0XHRcdFx0XHRcdCc8bGluZSB4MT1cIjEwMFwiIHkxPVwiMFwiIHgyPVwiNDBcIiB5Mj1cIjMwXCIgbWFya2VyLXN0YXJ0PVwidXJsKCcgKyBsb2NhdGlvbi5ocmVmICsgJyNwcmlzbS1wcmV2aWV3ZXItZWFzaW5nLW1hcmtlcilcIiBtYXJrZXItZW5kPVwidXJsKCcgKyBsb2NhdGlvbi5ocmVmICsgJyNwcmlzbS1wcmV2aWV3ZXItZWFzaW5nLW1hcmtlcilcIiAvPicgK1xuXHRcdFx0XHRcdFx0Jzwvc3ZnPic7XG5cdFx0XHRcdH0pO1xuXHRcdFx0fSxcblx0XHRcdHRva2Vuczoge1xuXHRcdFx0XHQnZWFzaW5nJzoge1xuXHRcdFx0XHRcdHBhdHRlcm46IC9cXGJjdWJpYy1iZXppZXJcXCgoPzotP1xcZCpcXC4/XFxkKyxcXHMqKXszfS0/XFxkKlxcLj9cXGQrXFwpXFxCfFxcYig/OmxpbmVhcnxlYXNlKD86LWluKT8oPzotb3V0KT8pKD89XFxzfFs7fV18JCkvaSxcblx0XHRcdFx0XHRpbnNpZGU6IHtcblx0XHRcdFx0XHRcdCdmdW5jdGlvbic6IC9bXFx3LV0rKD89XFwoKS8sXG5cdFx0XHRcdFx0XHQncHVuY3R1YXRpb24nOiAvWygpLF0vXG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9XG5cdFx0XHR9LFxuXHRcdFx0bGFuZ3VhZ2VzOiB7XG5cdFx0XHRcdCdjc3MnOiB0cnVlLFxuXHRcdFx0XHQnbGVzcyc6IHRydWUsXG5cdFx0XHRcdCdzYXNzJzogW1xuXHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdGxhbmc6ICdzYXNzJyxcblx0XHRcdFx0XHRcdGluc2lkZTogJ2luc2lkZScsXG5cdFx0XHRcdFx0XHRiZWZvcmU6ICdwdW5jdHVhdGlvbicsXG5cdFx0XHRcdFx0XHRyb290OiBQcmlzbS5sYW5ndWFnZXMuc2FzcyAmJiBQcmlzbS5sYW5ndWFnZXMuc2Fzc1sndmFyaWFibGUtbGluZSddXG5cdFx0XHRcdFx0fSxcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRsYW5nOiAnc2FzcycsXG5cdFx0XHRcdFx0XHRpbnNpZGU6ICdpbnNpZGUnLFxuXHRcdFx0XHRcdFx0cm9vdDogUHJpc20ubGFuZ3VhZ2VzLnNhc3MgJiYgUHJpc20ubGFuZ3VhZ2VzLnNhc3NbJ3Byb3BlcnR5LWxpbmUnXVxuXHRcdFx0XHRcdH1cblx0XHRcdFx0XSxcblx0XHRcdFx0J3Njc3MnOiB0cnVlLFxuXHRcdFx0XHQnc3R5bHVzJzogW1xuXHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdGxhbmc6ICdzdHlsdXMnLFxuXHRcdFx0XHRcdFx0YmVmb3JlOiAnaGV4Y29kZScsXG5cdFx0XHRcdFx0XHRpbnNpZGU6ICdyZXN0Jyxcblx0XHRcdFx0XHRcdHJvb3Q6IFByaXNtLmxhbmd1YWdlcy5zdHlsdXMgJiYgUHJpc20ubGFuZ3VhZ2VzLnN0eWx1c1sncHJvcGVydHktZGVjbGFyYXRpb24nXS5pbnNpZGVcblx0XHRcdFx0XHR9LFxuXHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdGxhbmc6ICdzdHlsdXMnLFxuXHRcdFx0XHRcdFx0YmVmb3JlOiAnaGV4Y29kZScsXG5cdFx0XHRcdFx0XHRpbnNpZGU6ICdyZXN0Jyxcblx0XHRcdFx0XHRcdHJvb3Q6IFByaXNtLmxhbmd1YWdlcy5zdHlsdXMgJiYgUHJpc20ubGFuZ3VhZ2VzLnN0eWx1c1sndmFyaWFibGUtZGVjbGFyYXRpb24nXS5pbnNpZGVcblx0XHRcdFx0XHR9XG5cdFx0XHRcdF1cblx0XHRcdH1cblx0XHR9LFxuXG5cdFx0J3RpbWUnOiB7XG5cdFx0XHRjcmVhdGU6IGZ1bmN0aW9uICgpIHtcblx0XHRcdFx0bmV3IFByaXNtLnBsdWdpbnMuUHJldmlld2VyKCd0aW1lJywgZnVuY3Rpb24odmFsdWUpIHtcblx0XHRcdFx0XHR2YXIgbnVtID0gcGFyc2VGbG9hdCh2YWx1ZSk7XG5cdFx0XHRcdFx0dmFyIHVuaXQgPSB2YWx1ZS5tYXRjaCgvW2Etel0rJC9pKTtcblx0XHRcdFx0XHRpZiAoIW51bSB8fCAhdW5pdCkge1xuXHRcdFx0XHRcdFx0cmV0dXJuIGZhbHNlO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0XHR1bml0ID0gdW5pdFswXTtcblx0XHRcdFx0XHR0aGlzLnF1ZXJ5U2VsZWN0b3IoJ2NpcmNsZScpLnN0eWxlLmFuaW1hdGlvbkR1cmF0aW9uID0gMiAqIG51bSArIHVuaXQ7XG5cdFx0XHRcdFx0cmV0dXJuIHRydWU7XG5cdFx0XHRcdH0sICcqJywgZnVuY3Rpb24gKCkge1xuXHRcdFx0XHRcdHRoaXMuX2VsdC5pbm5lckhUTUwgPSAnPHN2ZyB2aWV3Qm94PVwiMCAwIDY0IDY0XCI+JyArXG5cdFx0XHRcdFx0XHQnPGNpcmNsZSByPVwiMTZcIiBjeT1cIjMyXCIgY3g9XCIzMlwiPjwvY2lyY2xlPicgK1xuXHRcdFx0XHRcdFx0Jzwvc3ZnPic7XG5cdFx0XHRcdH0pO1xuXHRcdFx0fSxcblx0XHRcdHRva2Vuczoge1xuXHRcdFx0XHQndGltZSc6IC8oPzpcXGJ8XFxCLXwoPz1cXEJcXC4pKVxcZCpcXC4/XFxkK20/c1xcYi9pXG5cdFx0XHR9LFxuXHRcdFx0bGFuZ3VhZ2VzOiB7XG5cdFx0XHRcdCdjc3MnOiB0cnVlLFxuXHRcdFx0XHQnbGVzcyc6IHRydWUsXG5cdFx0XHRcdCdtYXJrdXAnOiB7XG5cdFx0XHRcdFx0bGFuZzogJ21hcmt1cCcsXG5cdFx0XHRcdFx0YmVmb3JlOiAncHVuY3R1YXRpb24nLFxuXHRcdFx0XHRcdGluc2lkZTogJ2luc2lkZScsXG5cdFx0XHRcdFx0cm9vdDogUHJpc20ubGFuZ3VhZ2VzLm1hcmt1cCAmJiBQcmlzbS5sYW5ndWFnZXMubWFya3VwWyd0YWcnXS5pbnNpZGVbJ2F0dHItdmFsdWUnXVxuXHRcdFx0XHR9LFxuXHRcdFx0XHQnc2Fzcyc6IFtcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRsYW5nOiAnc2FzcycsXG5cdFx0XHRcdFx0XHRpbnNpZGU6ICdpbnNpZGUnLFxuXHRcdFx0XHRcdFx0cm9vdDogUHJpc20ubGFuZ3VhZ2VzLnNhc3MgJiYgUHJpc20ubGFuZ3VhZ2VzLnNhc3NbJ3Byb3BlcnR5LWxpbmUnXVxuXHRcdFx0XHRcdH0sXG5cdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0bGFuZzogJ3Nhc3MnLFxuXHRcdFx0XHRcdFx0YmVmb3JlOiAnb3BlcmF0b3InLFxuXHRcdFx0XHRcdFx0aW5zaWRlOiAnaW5zaWRlJyxcblx0XHRcdFx0XHRcdHJvb3Q6IFByaXNtLmxhbmd1YWdlcy5zYXNzICYmIFByaXNtLmxhbmd1YWdlcy5zYXNzWyd2YXJpYWJsZS1saW5lJ11cblx0XHRcdFx0XHR9XG5cdFx0XHRcdF0sXG5cdFx0XHRcdCdzY3NzJzogdHJ1ZSxcblx0XHRcdFx0J3N0eWx1cyc6IFtcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRsYW5nOiAnc3R5bHVzJyxcblx0XHRcdFx0XHRcdGJlZm9yZTogJ2hleGNvZGUnLFxuXHRcdFx0XHRcdFx0aW5zaWRlOiAncmVzdCcsXG5cdFx0XHRcdFx0XHRyb290OiBQcmlzbS5sYW5ndWFnZXMuc3R5bHVzICYmIFByaXNtLmxhbmd1YWdlcy5zdHlsdXNbJ3Byb3BlcnR5LWRlY2xhcmF0aW9uJ10uaW5zaWRlXG5cdFx0XHRcdFx0fSxcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRsYW5nOiAnc3R5bHVzJyxcblx0XHRcdFx0XHRcdGJlZm9yZTogJ2hleGNvZGUnLFxuXHRcdFx0XHRcdFx0aW5zaWRlOiAncmVzdCcsXG5cdFx0XHRcdFx0XHRyb290OiBQcmlzbS5sYW5ndWFnZXMuc3R5bHVzICYmIFByaXNtLmxhbmd1YWdlcy5zdHlsdXNbJ3ZhcmlhYmxlLWRlY2xhcmF0aW9uJ10uaW5zaWRlXG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRdXG5cdFx0XHR9XG5cdFx0fVxuXHR9O1xuXG5cdC8qKlxuXHQgKiBSZXR1cm5zIHRoZSBhYnNvbHV0ZSBYLCBZIG9mZnNldHMgZm9yIGFuIGVsZW1lbnRcblx0ICogQHBhcmFtIHtIVE1MRWxlbWVudH0gZWxlbWVudFxuXHQgKiBAcmV0dXJucyB7e3RvcDogbnVtYmVyLCByaWdodDogbnVtYmVyLCBib3R0b206IG51bWJlciwgbGVmdDogbnVtYmVyfX1cblx0ICovXG5cdHZhciBnZXRPZmZzZXQgPSBmdW5jdGlvbiAoZWxlbWVudCkge1xuXHRcdHZhciBsZWZ0ID0gMCwgdG9wID0gMCwgZWwgPSBlbGVtZW50O1xuXG5cdFx0aWYgKGVsLnBhcmVudE5vZGUpIHtcblx0XHRcdGRvIHtcblx0XHRcdFx0bGVmdCArPSBlbC5vZmZzZXRMZWZ0O1xuXHRcdFx0XHR0b3AgKz0gZWwub2Zmc2V0VG9wO1xuXHRcdFx0fSB3aGlsZSAoKGVsID0gZWwub2Zmc2V0UGFyZW50KSAmJiBlbC5ub2RlVHlwZSA8IDkpO1xuXG5cdFx0XHRlbCA9IGVsZW1lbnQ7XG5cblx0XHRcdGRvIHtcblx0XHRcdFx0bGVmdCAtPSBlbC5zY3JvbGxMZWZ0O1xuXHRcdFx0XHR0b3AgLT0gZWwuc2Nyb2xsVG9wO1xuXHRcdFx0fSB3aGlsZSAoKGVsID0gZWwucGFyZW50Tm9kZSkgJiYgIS9ib2R5L2kudGVzdChlbC5ub2RlTmFtZSkpO1xuXHRcdH1cblxuXHRcdHJldHVybiB7XG5cdFx0XHR0b3A6IHRvcCxcblx0XHRcdHJpZ2h0OiBpbm5lcldpZHRoIC0gbGVmdCAtIGVsZW1lbnQub2Zmc2V0V2lkdGgsXG5cdFx0XHRib3R0b206IGlubmVySGVpZ2h0IC0gdG9wIC0gZWxlbWVudC5vZmZzZXRIZWlnaHQsXG5cdFx0XHRsZWZ0OiBsZWZ0XG5cdFx0fTtcblx0fTtcblxuXHR2YXIgdG9rZW5SZWdleHAgPSAvKD86XnxcXHMpdG9rZW4oPz0kfFxccykvO1xuXHR2YXIgYWN0aXZlUmVnZXhwID0gLyg/Ol58XFxzKWFjdGl2ZSg/PSR8XFxzKS9nO1xuXHR2YXIgZmxpcHBlZFJlZ2V4cCA9IC8oPzpefFxccylmbGlwcGVkKD89JHxcXHMpL2c7XG5cblx0LyoqXG5cdCAqIFByZXZpZXdlciBjb25zdHJ1Y3RvclxuXHQgKiBAcGFyYW0ge3N0cmluZ30gdHlwZSBVbmlxdWUgcHJldmlld2VyIHR5cGVcblx0ICogQHBhcmFtIHtmdW5jdGlvbn0gdXBkYXRlciBGdW5jdGlvbiB0aGF0IHdpbGwgYmUgY2FsbGVkIG9uIG1vdXNlb3Zlci5cblx0ICogQHBhcmFtIHtzdHJpbmdbXXxzdHJpbmc9fSBzdXBwb3J0ZWRMYW5ndWFnZXMgQWxpYXNlcyBvZiB0aGUgbGFuZ3VhZ2VzIHRoaXMgcHJldmlld2VyIG11c3QgYmUgZW5hYmxlZCBmb3IuIERlZmF1bHRzIHRvIFwiKlwiLCBhbGwgbGFuZ3VhZ2VzLlxuXHQgKiBAcGFyYW0ge2Z1bmN0aW9uPX0gaW5pdGlhbGl6ZXIgRnVuY3Rpb24gdGhhdCB3aWxsIGJlIGNhbGxlZCBvbiBpbml0aWFsaXphdGlvbi5cblx0ICogQGNvbnN0cnVjdG9yXG5cdCAqL1xuXHR2YXIgUHJldmlld2VyID0gZnVuY3Rpb24gKHR5cGUsIHVwZGF0ZXIsIHN1cHBvcnRlZExhbmd1YWdlcywgaW5pdGlhbGl6ZXIpIHtcblx0XHR0aGlzLl9lbHQgPSBudWxsO1xuXHRcdHRoaXMuX3R5cGUgPSB0eXBlO1xuXHRcdHRoaXMuX2Nsc1JlZ2V4cCA9IFJlZ0V4cCgnKD86XnxcXFxccyknICsgdHlwZSArICcoPz0kfFxcXFxzKScpO1xuXHRcdHRoaXMuX3Rva2VuID0gbnVsbDtcblx0XHR0aGlzLnVwZGF0ZXIgPSB1cGRhdGVyO1xuXHRcdHRoaXMuX21vdXNlb3V0ID0gdGhpcy5tb3VzZW91dC5iaW5kKHRoaXMpO1xuXHRcdHRoaXMuaW5pdGlhbGl6ZXIgPSBpbml0aWFsaXplcjtcblxuXHRcdHZhciBzZWxmID0gdGhpcztcblxuXHRcdGlmICghc3VwcG9ydGVkTGFuZ3VhZ2VzKSB7XG5cdFx0XHRzdXBwb3J0ZWRMYW5ndWFnZXMgPSBbJyonXTtcblx0XHR9XG5cdFx0aWYgKFByaXNtLnV0aWwudHlwZShzdXBwb3J0ZWRMYW5ndWFnZXMpICE9PSAnQXJyYXknKSB7XG5cdFx0XHRzdXBwb3J0ZWRMYW5ndWFnZXMgPSBbc3VwcG9ydGVkTGFuZ3VhZ2VzXTtcblx0XHR9XG5cdFx0c3VwcG9ydGVkTGFuZ3VhZ2VzLmZvckVhY2goZnVuY3Rpb24gKGxhbmcpIHtcblx0XHRcdGlmICh0eXBlb2YgbGFuZyAhPT0gJ3N0cmluZycpIHtcblx0XHRcdFx0bGFuZyA9IGxhbmcubGFuZztcblx0XHRcdH1cblx0XHRcdGlmICghUHJldmlld2VyLmJ5TGFuZ3VhZ2VzW2xhbmddKSB7XG5cdFx0XHRcdFByZXZpZXdlci5ieUxhbmd1YWdlc1tsYW5nXSA9IFtdO1xuXHRcdFx0fVxuXHRcdFx0aWYgKFByZXZpZXdlci5ieUxhbmd1YWdlc1tsYW5nXS5pbmRleE9mKHNlbGYpIDwgMCkge1xuXHRcdFx0XHRQcmV2aWV3ZXIuYnlMYW5ndWFnZXNbbGFuZ10ucHVzaChzZWxmKTtcblx0XHRcdH1cblx0XHR9KTtcblx0XHRQcmV2aWV3ZXIuYnlUeXBlW3R5cGVdID0gdGhpcztcblx0fTtcblxuXHQvKipcblx0ICogQ3JlYXRlcyB0aGUgSFRNTCBlbGVtZW50IGZvciB0aGUgcHJldmlld2VyLlxuXHQgKi9cblx0UHJldmlld2VyLnByb3RvdHlwZS5pbml0ID0gZnVuY3Rpb24gKCkge1xuXHRcdGlmICh0aGlzLl9lbHQpIHtcblx0XHRcdHJldHVybjtcblx0XHR9XG5cdFx0dGhpcy5fZWx0ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnZGl2Jyk7XG5cdFx0dGhpcy5fZWx0LmNsYXNzTmFtZSA9ICdwcmlzbS1wcmV2aWV3ZXIgcHJpc20tcHJldmlld2VyLScgKyB0aGlzLl90eXBlO1xuXHRcdGRvY3VtZW50LmJvZHkuYXBwZW5kQ2hpbGQodGhpcy5fZWx0KTtcblx0XHRpZih0aGlzLmluaXRpYWxpemVyKSB7XG5cdFx0XHR0aGlzLmluaXRpYWxpemVyKCk7XG5cdFx0fVxuXHR9O1xuXG5cdFByZXZpZXdlci5wcm90b3R5cGUuaXNEaXNhYmxlZCA9IGZ1bmN0aW9uICh0b2tlbikge1xuXHRcdGRvIHtcblx0XHRcdGlmICh0b2tlbi5oYXNBdHRyaWJ1dGUgJiYgdG9rZW4uaGFzQXR0cmlidXRlKCdkYXRhLXByZXZpZXdlcnMnKSkge1xuXHRcdFx0XHR2YXIgcHJldmlld2VycyA9IHRva2VuLmdldEF0dHJpYnV0ZSgnZGF0YS1wcmV2aWV3ZXJzJyk7XG5cdFx0XHRcdHJldHVybiAocHJldmlld2VycyB8fCAnJykuc3BsaXQoL1xccysvKS5pbmRleE9mKHRoaXMuX3R5cGUpID09PSAtMTtcblx0XHRcdH1cblx0XHR9IHdoaWxlKHRva2VuID0gdG9rZW4ucGFyZW50Tm9kZSk7XG5cdFx0cmV0dXJuIGZhbHNlO1xuXHR9O1xuXG5cdC8qKlxuXHQgKiBDaGVja3MgdGhlIGNsYXNzIG5hbWUgb2YgZWFjaCBob3ZlcmVkIGVsZW1lbnRcblx0ICogQHBhcmFtIHRva2VuXG5cdCAqL1xuXHRQcmV2aWV3ZXIucHJvdG90eXBlLmNoZWNrID0gZnVuY3Rpb24gKHRva2VuKSB7XG5cdFx0aWYgKHRva2VuUmVnZXhwLnRlc3QodG9rZW4uY2xhc3NOYW1lKSAmJiB0aGlzLmlzRGlzYWJsZWQodG9rZW4pKSB7XG5cdFx0XHRyZXR1cm47XG5cdFx0fVxuXHRcdGRvIHtcblx0XHRcdGlmICh0b2tlblJlZ2V4cC50ZXN0KHRva2VuLmNsYXNzTmFtZSkgJiYgdGhpcy5fY2xzUmVnZXhwLnRlc3QodG9rZW4uY2xhc3NOYW1lKSkge1xuXHRcdFx0XHRicmVhaztcblx0XHRcdH1cblx0XHR9IHdoaWxlKHRva2VuID0gdG9rZW4ucGFyZW50Tm9kZSk7XG5cblx0XHRpZiAodG9rZW4gJiYgdG9rZW4gIT09IHRoaXMuX3Rva2VuKSB7XG5cdFx0XHR0aGlzLl90b2tlbiA9IHRva2VuO1xuXHRcdFx0dGhpcy5zaG93KCk7XG5cdFx0fVxuXHR9O1xuXG5cdC8qKlxuXHQgKiBDYWxsZWQgb24gbW91c2VvdXRcblx0ICovXG5cdFByZXZpZXdlci5wcm90b3R5cGUubW91c2VvdXQgPSBmdW5jdGlvbigpIHtcblx0XHR0aGlzLl90b2tlbi5yZW1vdmVFdmVudExpc3RlbmVyKCdtb3VzZW91dCcsIHRoaXMuX21vdXNlb3V0LCBmYWxzZSk7XG5cdFx0dGhpcy5fdG9rZW4gPSBudWxsO1xuXHRcdHRoaXMuaGlkZSgpO1xuXHR9O1xuXG5cdC8qKlxuXHQgKiBTaG93cyB0aGUgcHJldmlld2VyIHBvc2l0aW9uZWQgcHJvcGVybHkgZm9yIHRoZSBjdXJyZW50IHRva2VuLlxuXHQgKi9cblx0UHJldmlld2VyLnByb3RvdHlwZS5zaG93ID0gZnVuY3Rpb24gKCkge1xuXHRcdGlmICghdGhpcy5fZWx0KSB7XG5cdFx0XHR0aGlzLmluaXQoKTtcblx0XHR9XG5cdFx0aWYgKCF0aGlzLl90b2tlbikge1xuXHRcdFx0cmV0dXJuO1xuXHRcdH1cblxuXHRcdGlmICh0aGlzLnVwZGF0ZXIuY2FsbCh0aGlzLl9lbHQsIHRoaXMuX3Rva2VuLnRleHRDb250ZW50KSkge1xuXHRcdFx0dGhpcy5fdG9rZW4uYWRkRXZlbnRMaXN0ZW5lcignbW91c2VvdXQnLCB0aGlzLl9tb3VzZW91dCwgZmFsc2UpO1xuXG5cdFx0XHR2YXIgb2Zmc2V0ID0gZ2V0T2Zmc2V0KHRoaXMuX3Rva2VuKTtcblx0XHRcdHRoaXMuX2VsdC5jbGFzc05hbWUgKz0gJyBhY3RpdmUnO1xuXG5cdFx0XHRpZiAob2Zmc2V0LnRvcCAtIHRoaXMuX2VsdC5vZmZzZXRIZWlnaHQgPiAwKSB7XG5cdFx0XHRcdHRoaXMuX2VsdC5jbGFzc05hbWUgPSB0aGlzLl9lbHQuY2xhc3NOYW1lLnJlcGxhY2UoZmxpcHBlZFJlZ2V4cCwgJycpO1xuXHRcdFx0XHR0aGlzLl9lbHQuc3R5bGUudG9wID0gb2Zmc2V0LnRvcCArICdweCc7XG5cdFx0XHRcdHRoaXMuX2VsdC5zdHlsZS5ib3R0b20gPSAnJztcblx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdHRoaXMuX2VsdC5jbGFzc05hbWUgKz0gICcgZmxpcHBlZCc7XG5cdFx0XHRcdHRoaXMuX2VsdC5zdHlsZS5ib3R0b20gPSBvZmZzZXQuYm90dG9tICsgJ3B4Jztcblx0XHRcdFx0dGhpcy5fZWx0LnN0eWxlLnRvcCA9ICcnO1xuXHRcdFx0fVxuXG5cdFx0XHR0aGlzLl9lbHQuc3R5bGUubGVmdCA9IG9mZnNldC5sZWZ0ICsgTWF0aC5taW4oMjAwLCB0aGlzLl90b2tlbi5vZmZzZXRXaWR0aCAvIDIpICsgJ3B4Jztcblx0XHR9IGVsc2Uge1xuXHRcdFx0dGhpcy5oaWRlKCk7XG5cdFx0fVxuXHR9O1xuXG5cdC8qKlxuXHQgKiBIaWRlcyB0aGUgcHJldmlld2VyLlxuXHQgKi9cblx0UHJldmlld2VyLnByb3RvdHlwZS5oaWRlID0gZnVuY3Rpb24gKCkge1xuXHRcdHRoaXMuX2VsdC5jbGFzc05hbWUgPSB0aGlzLl9lbHQuY2xhc3NOYW1lLnJlcGxhY2UoYWN0aXZlUmVnZXhwLCAnJyk7XG5cdH07XG5cblx0LyoqXG5cdCAqIE1hcCBvZiBhbGwgcmVnaXN0ZXJlZCBwcmV2aWV3ZXJzIGJ5IGxhbmd1YWdlXG5cdCAqIEB0eXBlIHt7fX1cblx0ICovXG5cdFByZXZpZXdlci5ieUxhbmd1YWdlcyA9IHt9O1xuXG5cdC8qKlxuXHQgKiBNYXAgb2YgYWxsIHJlZ2lzdGVyZWQgcHJldmlld2VycyBieSB0eXBlXG5cdCAqIEB0eXBlIHt7fX1cblx0ICovXG5cdFByZXZpZXdlci5ieVR5cGUgPSB7fTtcblxuXHQvKipcblx0ICogSW5pdGlhbGl6ZXMgdGhlIG1vdXNlb3ZlciBldmVudCBvbiB0aGUgY29kZSBibG9jay5cblx0ICogQHBhcmFtIHtIVE1MRWxlbWVudH0gZWx0IFRoZSBjb2RlIGJsb2NrIChlbnYuZWxlbWVudClcblx0ICogQHBhcmFtIHtzdHJpbmd9IGxhbmcgVGhlIGxhbmd1YWdlIChlbnYubGFuZ3VhZ2UpXG5cdCAqL1xuXHRQcmV2aWV3ZXIuaW5pdEV2ZW50cyA9IGZ1bmN0aW9uIChlbHQsIGxhbmcpIHtcblx0XHR2YXIgcHJldmlld2VycyA9IFtdO1xuXHRcdGlmIChQcmV2aWV3ZXIuYnlMYW5ndWFnZXNbbGFuZ10pIHtcblx0XHRcdHByZXZpZXdlcnMgPSBwcmV2aWV3ZXJzLmNvbmNhdChQcmV2aWV3ZXIuYnlMYW5ndWFnZXNbbGFuZ10pO1xuXHRcdH1cblx0XHRpZiAoUHJldmlld2VyLmJ5TGFuZ3VhZ2VzWycqJ10pIHtcblx0XHRcdHByZXZpZXdlcnMgPSBwcmV2aWV3ZXJzLmNvbmNhdChQcmV2aWV3ZXIuYnlMYW5ndWFnZXNbJyonXSk7XG5cdFx0fVxuXHRcdGVsdC5hZGRFdmVudExpc3RlbmVyKCdtb3VzZW92ZXInLCBmdW5jdGlvbiAoZSkge1xuXHRcdFx0dmFyIHRhcmdldCA9IGUudGFyZ2V0O1xuXHRcdFx0cHJldmlld2Vycy5mb3JFYWNoKGZ1bmN0aW9uIChwcmV2aWV3ZXIpIHtcblx0XHRcdFx0cHJldmlld2VyLmNoZWNrKHRhcmdldCk7XG5cdFx0XHR9KTtcblx0XHR9LCBmYWxzZSk7XG5cdH07XG5cdFByaXNtLnBsdWdpbnMuUHJldmlld2VyID0gUHJldmlld2VyO1xuXG5cdFByaXNtLmhvb2tzLmFkZCgnYmVmb3JlLWhpZ2hsaWdodCcsIGZ1bmN0aW9uIChlbnYpIHtcblx0XHRmb3IgKHZhciBwcmV2aWV3ZXIgaW4gcHJldmlld2Vycykge1xuXHRcdFx0dmFyIGxhbmd1YWdlcyA9IHByZXZpZXdlcnNbcHJldmlld2VyXS5sYW5ndWFnZXM7XG5cdFx0XHRpZiAoZW52Lmxhbmd1YWdlICYmIGxhbmd1YWdlc1tlbnYubGFuZ3VhZ2VdICYmICFsYW5ndWFnZXNbZW52Lmxhbmd1YWdlXS5pbml0aWFsaXplZCkge1xuXHRcdFx0XHR2YXIgbGFuZyA9IGxhbmd1YWdlc1tlbnYubGFuZ3VhZ2VdO1xuXHRcdFx0XHRpZiAoUHJpc20udXRpbC50eXBlKGxhbmcpICE9PSAnQXJyYXknKSB7XG5cdFx0XHRcdFx0bGFuZyA9IFtsYW5nXTtcblx0XHRcdFx0fVxuXHRcdFx0XHRsYW5nLmZvckVhY2goZnVuY3Rpb24gKGxhbmcpIHtcblx0XHRcdFx0XHR2YXIgYmVmb3JlLCBpbnNpZGUsIHJvb3QsIHNraXA7XG5cdFx0XHRcdFx0aWYgKGxhbmcgPT09IHRydWUpIHtcblx0XHRcdFx0XHRcdGJlZm9yZSA9ICdpbXBvcnRhbnQnO1xuXHRcdFx0XHRcdFx0aW5zaWRlID0gZW52Lmxhbmd1YWdlO1xuXHRcdFx0XHRcdFx0bGFuZyA9IGVudi5sYW5ndWFnZTtcblx0XHRcdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRcdFx0YmVmb3JlID0gbGFuZy5iZWZvcmUgfHwgJ2ltcG9ydGFudCc7XG5cdFx0XHRcdFx0XHRpbnNpZGUgPSBsYW5nLmluc2lkZSB8fCBsYW5nLmxhbmc7XG5cdFx0XHRcdFx0XHRyb290ID0gbGFuZy5yb290IHx8IFByaXNtLmxhbmd1YWdlcztcblx0XHRcdFx0XHRcdHNraXAgPSBsYW5nLnNraXA7XG5cdFx0XHRcdFx0XHRsYW5nID0gZW52Lmxhbmd1YWdlO1xuXHRcdFx0XHRcdH1cblxuXHRcdFx0XHRcdGlmICghc2tpcCAmJiBQcmlzbS5sYW5ndWFnZXNbbGFuZ10pIHtcblx0XHRcdFx0XHRcdFByaXNtLmxhbmd1YWdlcy5pbnNlcnRCZWZvcmUoaW5zaWRlLCBiZWZvcmUsIHByZXZpZXdlcnNbcHJldmlld2VyXS50b2tlbnMsIHJvb3QpO1xuXHRcdFx0XHRcdFx0ZW52LmdyYW1tYXIgPSBQcmlzbS5sYW5ndWFnZXNbbGFuZ107XG5cblx0XHRcdFx0XHRcdGxhbmd1YWdlc1tlbnYubGFuZ3VhZ2VdID0ge2luaXRpYWxpemVkOiB0cnVlfTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdH0pO1xuXHRcdFx0fVxuXHRcdH1cblx0fSk7XG5cblx0Ly8gSW5pdGlhbGl6ZSB0aGUgcHJldmlld2VycyBvbmx5IHdoZW4gbmVlZGVkXG5cdFByaXNtLmhvb2tzLmFkZCgnYWZ0ZXItaGlnaGxpZ2h0JywgZnVuY3Rpb24gKGVudikge1xuXHRcdGlmKFByZXZpZXdlci5ieUxhbmd1YWdlc1snKiddIHx8IFByZXZpZXdlci5ieUxhbmd1YWdlc1tlbnYubGFuZ3VhZ2VdKSB7XG5cdFx0XHRQcmV2aWV3ZXIuaW5pdEV2ZW50cyhlbnYuZWxlbWVudCwgZW52Lmxhbmd1YWdlKTtcblx0XHR9XG5cdH0pO1xuXG5cdGZvciAodmFyIHByZXZpZXdlciBpbiBwcmV2aWV3ZXJzKSB7XG5cdFx0cHJldmlld2Vyc1twcmV2aWV3ZXJdLmNyZWF0ZSgpO1xuXHR9XG5cbn0oKSk7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvcHJpc21qcy9wbHVnaW5zL3ByZXZpZXdlcnMvcHJpc20tcHJldmlld2Vycy5qc1xuLy8gbW9kdWxlIGlkID0gMTI4XG4vLyBtb2R1bGUgY2h1bmtzID0gMTAiLCIvLyBzdHlsZS1sb2FkZXI6IEFkZHMgc29tZSBjc3MgdG8gdGhlIERPTSBieSBhZGRpbmcgYSA8c3R5bGU+IHRhZ1xuXG4vLyBsb2FkIHRoZSBzdHlsZXNcbnZhciBjb250ZW50ID0gcmVxdWlyZShcIiEhLi4vLi4vLi4vY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS0xLTEhLi4vLi4vLi4vcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTEtMiEuL3ByaXNtLXByZXZpZXdlcnMuY3NzXCIpO1xuaWYodHlwZW9mIGNvbnRlbnQgPT09ICdzdHJpbmcnKSBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG4vLyBQcmVwYXJlIGNzc1RyYW5zZm9ybWF0aW9uXG52YXIgdHJhbnNmb3JtO1xuXG52YXIgb3B0aW9ucyA9IHtcImluc2VydEF0XCI6e1wiYmVmb3JlXCI6XCIjd2VicGFjay1zdHlsZS1sb2FkZXItaW5zZXJ0LWJlZm9yZS10aGlzXCJ9LFwiaG1yXCI6dHJ1ZX1cbm9wdGlvbnMudHJhbnNmb3JtID0gdHJhbnNmb3JtXG4vLyBhZGQgdGhlIHN0eWxlcyB0byB0aGUgRE9NXG52YXIgdXBkYXRlID0gcmVxdWlyZShcIiEuLi8uLi8uLi9zdHlsZS1sb2FkZXIvbGliL2FkZFN0eWxlcy5qc1wiKShjb250ZW50LCBvcHRpb25zKTtcbmlmKGNvbnRlbnQubG9jYWxzKSBtb2R1bGUuZXhwb3J0cyA9IGNvbnRlbnQubG9jYWxzO1xuLy8gSG90IE1vZHVsZSBSZXBsYWNlbWVudFxuaWYobW9kdWxlLmhvdCkge1xuXHQvLyBXaGVuIHRoZSBzdHlsZXMgY2hhbmdlLCB1cGRhdGUgdGhlIDxzdHlsZT4gdGFnc1xuXHRpZighY29udGVudC5sb2NhbHMpIHtcblx0XHRtb2R1bGUuaG90LmFjY2VwdChcIiEhLi4vLi4vLi4vY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS0xLTEhLi4vLi4vLi4vcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTEtMiEuL3ByaXNtLXByZXZpZXdlcnMuY3NzXCIsIGZ1bmN0aW9uKCkge1xuXHRcdFx0dmFyIG5ld0NvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi8uLi9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTEtMSEuLi8uLi8uLi9wb3N0Y3NzLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tMS0yIS4vcHJpc20tcHJldmlld2Vycy5jc3NcIik7XG5cdFx0XHRpZih0eXBlb2YgbmV3Q29udGVudCA9PT0gJ3N0cmluZycpIG5ld0NvbnRlbnQgPSBbW21vZHVsZS5pZCwgbmV3Q29udGVudCwgJyddXTtcblx0XHRcdHVwZGF0ZShuZXdDb250ZW50KTtcblx0XHR9KTtcblx0fVxuXHQvLyBXaGVuIHRoZSBtb2R1bGUgaXMgZGlzcG9zZWQsIHJlbW92ZSB0aGUgPHN0eWxlPiB0YWdzXG5cdG1vZHVsZS5ob3QuZGlzcG9zZShmdW5jdGlvbigpIHsgdXBkYXRlKCk7IH0pO1xufVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3ByaXNtanMvcGx1Z2lucy9wcmV2aWV3ZXJzL3ByaXNtLXByZXZpZXdlcnMuY3NzXG4vLyBtb2R1bGUgaWQgPSAxMjlcbi8vIG1vZHVsZSBjaHVua3MgPSAxMCIsImV4cG9ydHMgPSBtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCIuLi8uLi8uLi9jc3MtbG9hZGVyL2xpYi9jc3MtYmFzZS5qc1wiKShmYWxzZSk7XG4vLyBpbXBvcnRzXG5cblxuLy8gbW9kdWxlXG5leHBvcnRzLnB1c2goW21vZHVsZS5pZCwgXCIucHJpc20tcHJldmlld2VyLFxcbi5wcmlzbS1wcmV2aWV3ZXI6YmVmb3JlLFxcbi5wcmlzbS1wcmV2aWV3ZXI6YWZ0ZXIge1xcblxcdHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG5cXHRwb2ludGVyLWV2ZW50czogbm9uZTtcXG59XFxuLnByaXNtLXByZXZpZXdlcixcXG4ucHJpc20tcHJldmlld2VyOmFmdGVyIHtcXG5cXHRsZWZ0OiA1MCU7XFxufVxcbi5wcmlzbS1wcmV2aWV3ZXIge1xcblxcdG1hcmdpbi10b3A6IC00OHB4O1xcblxcdHdpZHRoOiAzMnB4O1xcblxcdGhlaWdodDogMzJweDtcXG5cXHRtYXJnaW4tbGVmdDogLTE2cHg7XFxuXFxuXFx0b3BhY2l0eTogMDtcXG5cXHR0cmFuc2l0aW9uOiBvcGFjaXR5IC4yNXM7XFxufVxcbi5wcmlzbS1wcmV2aWV3ZXIuZmxpcHBlZCB7XFxuXFx0bWFyZ2luLXRvcDogMDtcXG5cXHRtYXJnaW4tYm90dG9tOiAtNDhweDtcXG59XFxuLnByaXNtLXByZXZpZXdlcjpiZWZvcmUsXFxuLnByaXNtLXByZXZpZXdlcjphZnRlciB7XFxuXFx0Y29udGVudDogJyc7XFxuXFx0cG9zaXRpb246IGFic29sdXRlO1xcblxcdHBvaW50ZXItZXZlbnRzOiBub25lO1xcbn1cXG4ucHJpc20tcHJldmlld2VyOmJlZm9yZSB7XFxuXFx0dG9wOiAtNXB4O1xcblxcdHJpZ2h0OiAtNXB4O1xcblxcdGxlZnQ6IC01cHg7XFxuXFx0Ym90dG9tOiAtNXB4O1xcblxcdGJvcmRlci1yYWRpdXM6IDEwcHg7XFxuXFx0Ym9yZGVyOiA1cHggc29saWQgI2ZmZjtcXG5cXHRib3gtc2hhZG93OiAwIDAgM3B4IHJnYmEoMCwgMCwgMCwgMC41KSBpbnNldCwgMCAwIDEwcHggcmdiYSgwLCAwLCAwLCAwLjc1KTtcXG59XFxuLnByaXNtLXByZXZpZXdlcjphZnRlciB7XFxuXFx0dG9wOiAxMDAlO1xcblxcdHdpZHRoOiAwO1xcblxcdGhlaWdodDogMDtcXG5cXHRtYXJnaW46IDVweCAwIDAgLTdweDtcXG5cXHRib3JkZXI6IDdweCBzb2xpZCB0cmFuc3BhcmVudDtcXG5cXHRib3JkZXItY29sb3I6IHJnYmEoMjU1LCAwLCAwLCAwKTtcXG5cXHRib3JkZXItdG9wLWNvbG9yOiAjZmZmO1xcbn1cXG4ucHJpc20tcHJldmlld2VyLmZsaXBwZWQ6YWZ0ZXIge1xcblxcdHRvcDogYXV0bztcXG5cXHRib3R0b206IDEwMCU7XFxuXFx0bWFyZ2luLXRvcDogMDtcXG5cXHRtYXJnaW4tYm90dG9tOiA1cHg7XFxuXFx0Ym9yZGVyLXRvcC1jb2xvcjogcmdiYSgyNTUsIDAsIDAsIDApO1xcblxcdGJvcmRlci1ib3R0b20tY29sb3I6ICNmZmY7XFxufVxcbi5wcmlzbS1wcmV2aWV3ZXIuYWN0aXZlIHtcXG5cXHRvcGFjaXR5OiAxO1xcbn1cXG5cXG4ucHJpc20tcHJldmlld2VyLWFuZ2xlOmJlZm9yZSB7XFxuXFx0Ym9yZGVyLXJhZGl1czogNTAlO1xcblxcdGJhY2tncm91bmQ6ICNmZmY7XFxufVxcbi5wcmlzbS1wcmV2aWV3ZXItYW5nbGU6YWZ0ZXIge1xcblxcdG1hcmdpbi10b3A6IDRweDtcXG59XFxuLnByaXNtLXByZXZpZXdlci1hbmdsZSBzdmcge1xcblxcdHdpZHRoOiAzMnB4O1xcblxcdGhlaWdodDogMzJweDtcXG5cXHR0cmFuc2Zvcm06IHJvdGF0ZSgtOTBkZWcpO1xcbn1cXG4ucHJpc20tcHJldmlld2VyLWFuZ2xlW2RhdGEtbmVnYXRpdmVdIHN2ZyB7XFxuXFx0dHJhbnNmb3JtOiBzY2FsZVgoLTEpIHJvdGF0ZSgtOTBkZWcpO1xcbn1cXG4ucHJpc20tcHJldmlld2VyLWFuZ2xlIGNpcmNsZSB7XFxuXFx0ZmlsbDogdHJhbnNwYXJlbnQ7XFxuXFx0c3Ryb2tlOiBoc2woMjAwLCAxMCUsIDIwJSk7XFxuXFx0c3Ryb2tlLW9wYWNpdHk6IDAuOTtcXG5cXHRzdHJva2Utd2lkdGg6IDMyO1xcblxcdHN0cm9rZS1kYXNoYXJyYXk6IDAsIDUwMDtcXG59XFxuXFxuLnByaXNtLXByZXZpZXdlci1ncmFkaWVudCB7XFxuXFx0YmFja2dyb3VuZC1pbWFnZTogbGluZWFyLWdyYWRpZW50KDQ1ZGVnLCAjYmJiIDI1JSwgdHJhbnNwYXJlbnQgMjUlLCB0cmFuc3BhcmVudCA3NSUsICNiYmIgNzUlLCAjYmJiKSwgbGluZWFyLWdyYWRpZW50KDQ1ZGVnLCAjYmJiIDI1JSwgI2VlZSAyNSUsICNlZWUgNzUlLCAjYmJiIDc1JSwgI2JiYik7XFxuXFx0YmFja2dyb3VuZC1zaXplOiAxMHB4IDEwcHg7XFxuXFx0YmFja2dyb3VuZC1wb3NpdGlvbjogMCAwLCA1cHggNXB4O1xcblxcblxcdHdpZHRoOiA2NHB4O1xcblxcdG1hcmdpbi1sZWZ0OiAtMzJweDtcXG59XFxuLnByaXNtLXByZXZpZXdlci1ncmFkaWVudDpiZWZvcmUge1xcblxcdGNvbnRlbnQ6IG5vbmU7XFxufVxcbi5wcmlzbS1wcmV2aWV3ZXItZ3JhZGllbnQgZGl2IHtcXG5cXHRwb3NpdGlvbjogYWJzb2x1dGU7XFxuXFx0dG9wOiAtNXB4O1xcblxcdGxlZnQ6IC01cHg7XFxuXFx0cmlnaHQ6IC01cHg7XFxuXFx0Ym90dG9tOiAtNXB4O1xcblxcdGJvcmRlci1yYWRpdXM6IDEwcHg7XFxuXFx0Ym9yZGVyOiA1cHggc29saWQgI2ZmZjtcXG5cXHRib3gtc2hhZG93OiAwIDAgM3B4IHJnYmEoMCwgMCwgMCwgMC41KSBpbnNldCwgMCAwIDEwcHggcmdiYSgwLCAwLCAwLCAwLjc1KTtcXG59XFxuXFxuLnByaXNtLXByZXZpZXdlci1jb2xvciB7XFxuXFx0YmFja2dyb3VuZC1pbWFnZTogbGluZWFyLWdyYWRpZW50KDQ1ZGVnLCAjYmJiIDI1JSwgdHJhbnNwYXJlbnQgMjUlLCB0cmFuc3BhcmVudCA3NSUsICNiYmIgNzUlLCAjYmJiKSwgbGluZWFyLWdyYWRpZW50KDQ1ZGVnLCAjYmJiIDI1JSwgI2VlZSAyNSUsICNlZWUgNzUlLCAjYmJiIDc1JSwgI2JiYik7XFxuXFx0YmFja2dyb3VuZC1zaXplOiAxMHB4IDEwcHg7XFxuXFx0YmFja2dyb3VuZC1wb3NpdGlvbjogMCAwLCA1cHggNXB4O1xcbn1cXG4ucHJpc20tcHJldmlld2VyLWNvbG9yOmJlZm9yZSB7XFxuXFx0YmFja2dyb3VuZC1jb2xvcjogaW5oZXJpdDtcXG5cXHRiYWNrZ3JvdW5kLWNsaXA6IHBhZGRpbmctYm94O1xcbn1cXG5cXG4ucHJpc20tcHJldmlld2VyLWVhc2luZyB7XFxuXFx0bWFyZ2luLXRvcDogLTc2cHg7XFxuXFx0bWFyZ2luLWxlZnQ6IC0zMHB4O1xcblxcdHdpZHRoOiA2MHB4O1xcblxcdGhlaWdodDogNjBweDtcXG5cXHRiYWNrZ3JvdW5kOiAjMzMzO1xcbn1cXG4ucHJpc20tcHJldmlld2VyLWVhc2luZy5mbGlwcGVkIHtcXG5cXHRtYXJnaW4tYm90dG9tOiAtMTE2cHg7XFxufVxcbi5wcmlzbS1wcmV2aWV3ZXItZWFzaW5nIHN2ZyB7XFxuXFx0d2lkdGg6IDYwcHg7XFxuXFx0aGVpZ2h0OiA2MHB4O1xcbn1cXG4ucHJpc20tcHJldmlld2VyLWVhc2luZyBjaXJjbGUge1xcblxcdGZpbGw6IGhzbCgyMDAsIDEwJSwgMjAlKTtcXG5cXHRzdHJva2U6IHdoaXRlO1xcbn1cXG4ucHJpc20tcHJldmlld2VyLWVhc2luZyBwYXRoIHtcXG5cXHRmaWxsOiBub25lO1xcblxcdHN0cm9rZTogd2hpdGU7XFxuXFx0c3Ryb2tlLWxpbmVjYXA6IHJvdW5kO1xcblxcdHN0cm9rZS13aWR0aDogNDtcXG59XFxuLnByaXNtLXByZXZpZXdlci1lYXNpbmcgbGluZSB7XFxuXFx0c3Ryb2tlOiB3aGl0ZTtcXG5cXHRzdHJva2Utb3BhY2l0eTogMC41O1xcblxcdHN0cm9rZS13aWR0aDogMjtcXG59XFxuXFxuQGtleWZyYW1lcyBwcmlzbS1wcmV2aWV3ZXItdGltZSB7XFxuXFx0MCUge1xcblxcdFxcdHN0cm9rZS1kYXNoYXJyYXk6IDAsIDUwMDtcXG5cXHRcXHRzdHJva2UtZGFzaG9mZnNldDogMDtcXG5cXHR9XFxuXFx0NTAlIHtcXG5cXHRcXHRzdHJva2UtZGFzaGFycmF5OiAxMDAsIDUwMDtcXG5cXHRcXHRzdHJva2UtZGFzaG9mZnNldDogMDtcXG5cXHR9XFxuXFx0MTAwJSB7XFxuXFx0XFx0c3Ryb2tlLWRhc2hhcnJheTogMCwgNTAwO1xcblxcdFxcdHN0cm9rZS1kYXNob2Zmc2V0OiAtMTAwO1xcblxcdH1cXG59XFxuXFxuLnByaXNtLXByZXZpZXdlci10aW1lOmJlZm9yZSB7XFxuXFx0Ym9yZGVyLXJhZGl1czogNTAlO1xcblxcdGJhY2tncm91bmQ6ICNmZmY7XFxufVxcbi5wcmlzbS1wcmV2aWV3ZXItdGltZTphZnRlciB7XFxuXFx0bWFyZ2luLXRvcDogNHB4O1xcbn1cXG4ucHJpc20tcHJldmlld2VyLXRpbWUgc3ZnIHtcXG5cXHR3aWR0aDogMzJweDtcXG5cXHRoZWlnaHQ6IDMycHg7XFxuXFx0dHJhbnNmb3JtOiByb3RhdGUoLTkwZGVnKTtcXG59XFxuLnByaXNtLXByZXZpZXdlci10aW1lIGNpcmNsZSB7XFxuXFx0ZmlsbDogdHJhbnNwYXJlbnQ7XFxuXFx0c3Ryb2tlOiBoc2woMjAwLCAxMCUsIDIwJSk7XFxuXFx0c3Ryb2tlLW9wYWNpdHk6IDAuOTtcXG5cXHRzdHJva2Utd2lkdGg6IDMyO1xcblxcdHN0cm9rZS1kYXNoYXJyYXk6IDAsIDUwMDtcXG5cXHRzdHJva2UtZGFzaG9mZnNldDogMDtcXG5cXHRhbmltYXRpb246IHByaXNtLXByZXZpZXdlci10aW1lIGxpbmVhciBpbmZpbml0ZSAzcztcXG59XCIsIFwiXCJdKTtcblxuLy8gZXhwb3J0c1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlcj8/cmVmLS0xLTEhLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliPz9yZWYtLTEtMiEuL25vZGVfbW9kdWxlcy9wcmlzbWpzL3BsdWdpbnMvcHJldmlld2Vycy9wcmlzbS1wcmV2aWV3ZXJzLmNzc1xuLy8gbW9kdWxlIGlkID0gMTMwXG4vLyBtb2R1bGUgY2h1bmtzID0gMTAiLCJQcmlzbS5sYW5ndWFnZXMuY3NzID0ge1xuXHQnY29tbWVudCc6IC9cXC9cXCpbXFxzXFxTXSo/XFwqXFwvLyxcblx0J2F0cnVsZSc6IHtcblx0XHRwYXR0ZXJuOiAvQFtcXHctXSs/Lio/KD86O3woPz1cXHMqXFx7KSkvaSxcblx0XHRpbnNpZGU6IHtcblx0XHRcdCdydWxlJzogL0BbXFx3LV0rL1xuXHRcdFx0Ly8gU2VlIHJlc3QgYmVsb3dcblx0XHR9XG5cdH0sXG5cdCd1cmwnOiAvdXJsXFwoKD86KFtcIiddKSg/OlxcXFwoPzpcXHJcXG58W1xcc1xcU10pfCg/IVxcMSlbXlxcXFxcXHJcXG5dKSpcXDF8Lio/KVxcKS9pLFxuXHQnc2VsZWN0b3InOiAvW157fVxcc11bXnt9O10qPyg/PVxccypcXHspLyxcblx0J3N0cmluZyc6IHtcblx0XHRwYXR0ZXJuOiAvKFwifCcpKD86XFxcXCg/OlxcclxcbnxbXFxzXFxTXSl8KD8hXFwxKVteXFxcXFxcclxcbl0pKlxcMS8sXG5cdFx0Z3JlZWR5OiB0cnVlXG5cdH0sXG5cdCdwcm9wZXJ0eSc6IC9bLV9hLXpcXHhBMC1cXHVGRkZGXVstXFx3XFx4QTAtXFx1RkZGRl0qKD89XFxzKjopL2ksXG5cdCdpbXBvcnRhbnQnOiAvXFxCIWltcG9ydGFudFxcYi9pLFxuXHQnZnVuY3Rpb24nOiAvWy1hLXowLTldKyg/PVxcKCkvaSxcblx0J3B1bmN0dWF0aW9uJzogL1soKXt9OzpdL1xufTtcblxuUHJpc20ubGFuZ3VhZ2VzLmNzc1snYXRydWxlJ10uaW5zaWRlLnJlc3QgPSBQcmlzbS5sYW5ndWFnZXMuY3NzO1xuXG5pZiAoUHJpc20ubGFuZ3VhZ2VzLm1hcmt1cCkge1xuXHRQcmlzbS5sYW5ndWFnZXMuaW5zZXJ0QmVmb3JlKCdtYXJrdXAnLCAndGFnJywge1xuXHRcdCdzdHlsZSc6IHtcblx0XHRcdHBhdHRlcm46IC8oPHN0eWxlW1xcc1xcU10qPz4pW1xcc1xcU10qPyg/PTxcXC9zdHlsZT4pL2ksXG5cdFx0XHRsb29rYmVoaW5kOiB0cnVlLFxuXHRcdFx0aW5zaWRlOiBQcmlzbS5sYW5ndWFnZXMuY3NzLFxuXHRcdFx0YWxpYXM6ICdsYW5ndWFnZS1jc3MnLFxuXHRcdFx0Z3JlZWR5OiB0cnVlXG5cdFx0fVxuXHR9KTtcblxuXHRQcmlzbS5sYW5ndWFnZXMuaW5zZXJ0QmVmb3JlKCdpbnNpZGUnLCAnYXR0ci12YWx1ZScsIHtcblx0XHQnc3R5bGUtYXR0cic6IHtcblx0XHRcdHBhdHRlcm46IC9cXHMqc3R5bGU9KFwifCcpKD86XFxcXFtcXHNcXFNdfCg/IVxcMSlbXlxcXFxdKSpcXDEvaSxcblx0XHRcdGluc2lkZToge1xuXHRcdFx0XHQnYXR0ci1uYW1lJzoge1xuXHRcdFx0XHRcdHBhdHRlcm46IC9eXFxzKnN0eWxlL2ksXG5cdFx0XHRcdFx0aW5zaWRlOiBQcmlzbS5sYW5ndWFnZXMubWFya3VwLnRhZy5pbnNpZGVcblx0XHRcdFx0fSxcblx0XHRcdFx0J3B1bmN0dWF0aW9uJzogL15cXHMqPVxccypbJ1wiXXxbJ1wiXVxccyokLyxcblx0XHRcdFx0J2F0dHItdmFsdWUnOiB7XG5cdFx0XHRcdFx0cGF0dGVybjogLy4rL2ksXG5cdFx0XHRcdFx0aW5zaWRlOiBQcmlzbS5sYW5ndWFnZXMuY3NzXG5cdFx0XHRcdH1cblx0XHRcdH0sXG5cdFx0XHRhbGlhczogJ2xhbmd1YWdlLWNzcydcblx0XHR9XG5cdH0sIFByaXNtLmxhbmd1YWdlcy5tYXJrdXAudGFnKTtcbn1cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9wcmlzbWpzL2NvbXBvbmVudHMvcHJpc20tY3NzLmpzXG4vLyBtb2R1bGUgaWQgPSAxMzFcbi8vIG1vZHVsZSBjaHVua3MgPSAxMCIsIlByaXNtLmxhbmd1YWdlcy5jc3Muc2VsZWN0b3IgPSB7XG5cdHBhdHRlcm46IC9bXnt9XFxzXVtee31dKig/PVxccypcXHspLyxcblx0aW5zaWRlOiB7XG5cdFx0J3BzZXVkby1lbGVtZW50JzogLzooPzphZnRlcnxiZWZvcmV8Zmlyc3QtbGV0dGVyfGZpcnN0LWxpbmV8c2VsZWN0aW9uKXw6OlstXFx3XSsvLFxuXHRcdCdwc2V1ZG8tY2xhc3MnOiAvOlstXFx3XSsoPzpcXCguKlxcKSk/Lyxcblx0XHQnY2xhc3MnOiAvXFwuWy06Llxcd10rLyxcblx0XHQnaWQnOiAvI1stOi5cXHddKy8sXG5cdFx0J2F0dHJpYnV0ZSc6IC9cXFtbXlxcXV0rXFxdL1xuXHR9XG59O1xuXG5QcmlzbS5sYW5ndWFnZXMuaW5zZXJ0QmVmb3JlKCdjc3MnLCAnZnVuY3Rpb24nLCB7XG5cdCdoZXhjb2RlJzogLyNbXFxkYS1mXXszLDh9L2ksXG5cdCdlbnRpdHknOiAvXFxcXFtcXGRhLWZdezEsOH0vaSxcblx0J251bWJlcic6IC9bXFxkJS5dKy9cbn0pO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3ByaXNtanMvY29tcG9uZW50cy9wcmlzbS1jc3MtZXh0cmFzLmpzXG4vLyBtb2R1bGUgaWQgPSAxMzJcbi8vIG1vZHVsZSBjaHVua3MgPSAxMCIsIihmdW5jdGlvbihQcmlzbSkge1xuXHRQcmlzbS5sYW5ndWFnZXMuc2FzcyA9IFByaXNtLmxhbmd1YWdlcy5leHRlbmQoJ2NzcycsIHtcblx0XHQvLyBTYXNzIGNvbW1lbnRzIGRvbid0IG5lZWQgdG8gYmUgY2xvc2VkLCBvbmx5IGluZGVudGVkXG5cdFx0J2NvbW1lbnQnOiB7XG5cdFx0XHRwYXR0ZXJuOiAvXihbIFxcdF0qKVxcL1tcXC8qXS4qKD86KD86XFxyP1xcbnxcXHIpXFwxWyBcXHRdKy4rKSovbSxcblx0XHRcdGxvb2tiZWhpbmQ6IHRydWVcblx0XHR9XG5cdH0pO1xuXG5cdFByaXNtLmxhbmd1YWdlcy5pbnNlcnRCZWZvcmUoJ3Nhc3MnLCAnYXRydWxlJywge1xuXHRcdC8vIFdlIHdhbnQgdG8gY29uc3VtZSB0aGUgd2hvbGUgbGluZVxuXHRcdCdhdHJ1bGUtbGluZSc6IHtcblx0XHRcdC8vIEluY2x1ZGVzIHN1cHBvcnQgZm9yID0gYW5kICsgc2hvcnRjdXRzXG5cdFx0XHRwYXR0ZXJuOiAvXig/OlsgXFx0XSopW0ArPV0uKy9tLFxuXHRcdFx0aW5zaWRlOiB7XG5cdFx0XHRcdCdhdHJ1bGUnOiAvKD86QFtcXHctXSt8Wys9XSkvbVxuXHRcdFx0fVxuXHRcdH1cblx0fSk7XG5cdGRlbGV0ZSBQcmlzbS5sYW5ndWFnZXMuc2Fzcy5hdHJ1bGU7XG5cblxuXHR2YXIgdmFyaWFibGUgPSAvXFwkWy1cXHddK3wjXFx7XFwkWy1cXHddK1xcfS87XG5cdHZhciBvcGVyYXRvciA9IFtcblx0XHQvWysqXFwvJV18Wz0hXT18PD0/fD49P3xcXGIoPzphbmR8b3J8bm90KVxcYi8sXG5cdFx0e1xuXHRcdFx0cGF0dGVybjogLyhcXHMrKS0oPz1cXHMpLyxcblx0XHRcdGxvb2tiZWhpbmQ6IHRydWVcblx0XHR9XG5cdF07XG5cblx0UHJpc20ubGFuZ3VhZ2VzLmluc2VydEJlZm9yZSgnc2FzcycsICdwcm9wZXJ0eScsIHtcblx0XHQvLyBXZSB3YW50IHRvIGNvbnN1bWUgdGhlIHdob2xlIGxpbmVcblx0XHQndmFyaWFibGUtbGluZSc6IHtcblx0XHRcdHBhdHRlcm46IC9eWyBcXHRdKlxcJC4rL20sXG5cdFx0XHRpbnNpZGU6IHtcblx0XHRcdFx0J3B1bmN0dWF0aW9uJzogLzovLFxuXHRcdFx0XHQndmFyaWFibGUnOiB2YXJpYWJsZSxcblx0XHRcdFx0J29wZXJhdG9yJzogb3BlcmF0b3Jcblx0XHRcdH1cblx0XHR9LFxuXHRcdC8vIFdlIHdhbnQgdG8gY29uc3VtZSB0aGUgd2hvbGUgbGluZVxuXHRcdCdwcm9wZXJ0eS1saW5lJzoge1xuXHRcdFx0cGF0dGVybjogL15bIFxcdF0qKD86W146XFxzXSsgKjouKnw6W146XFxzXSsuKikvbSxcblx0XHRcdGluc2lkZToge1xuXHRcdFx0XHQncHJvcGVydHknOiBbXG5cdFx0XHRcdFx0L1teOlxcc10rKD89XFxzKjopLyxcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRwYXR0ZXJuOiAvKDopW146XFxzXSsvLFxuXHRcdFx0XHRcdFx0bG9va2JlaGluZDogdHJ1ZVxuXHRcdFx0XHRcdH1cblx0XHRcdFx0XSxcblx0XHRcdFx0J3B1bmN0dWF0aW9uJzogLzovLFxuXHRcdFx0XHQndmFyaWFibGUnOiB2YXJpYWJsZSxcblx0XHRcdFx0J29wZXJhdG9yJzogb3BlcmF0b3IsXG5cdFx0XHRcdCdpbXBvcnRhbnQnOiBQcmlzbS5sYW5ndWFnZXMuc2Fzcy5pbXBvcnRhbnRcblx0XHRcdH1cblx0XHR9XG5cdH0pO1xuXHRkZWxldGUgUHJpc20ubGFuZ3VhZ2VzLnNhc3MucHJvcGVydHk7XG5cdGRlbGV0ZSBQcmlzbS5sYW5ndWFnZXMuc2Fzcy5pbXBvcnRhbnQ7XG5cblx0Ly8gTm93IHRoYXQgd2hvbGUgbGluZXMgZm9yIG90aGVyIHBhdHRlcm5zIGFyZSBjb25zdW1lZCxcblx0Ly8gd2hhdCdzIGxlZnQgc2hvdWxkIGJlIHNlbGVjdG9yc1xuXHRkZWxldGUgUHJpc20ubGFuZ3VhZ2VzLnNhc3Muc2VsZWN0b3I7XG5cdFByaXNtLmxhbmd1YWdlcy5pbnNlcnRCZWZvcmUoJ3Nhc3MnLCAncHVuY3R1YXRpb24nLCB7XG5cdFx0J3NlbGVjdG9yJzoge1xuXHRcdFx0cGF0dGVybjogLyhbIFxcdF0qKVxcUyg/Oiw/W14sXFxyXFxuXSspKig/OiwoPzpcXHI/XFxufFxccilcXDFbIFxcdF0rXFxTKD86LD9bXixcXHJcXG5dKykqKSovLFxuXHRcdFx0bG9va2JlaGluZDogdHJ1ZVxuXHRcdH1cblx0fSk7XG5cbn0oUHJpc20pKTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9wcmlzbWpzL2NvbXBvbmVudHMvcHJpc20tc2Fzcy5qc1xuLy8gbW9kdWxlIGlkID0gMTMzXG4vLyBtb2R1bGUgY2h1bmtzID0gMTAiLCJQcmlzbS5sYW5ndWFnZXMuc2NzcyA9IFByaXNtLmxhbmd1YWdlcy5leHRlbmQoJ2NzcycsIHtcblx0J2NvbW1lbnQnOiB7XG5cdFx0cGF0dGVybjogLyhefFteXFxcXF0pKD86XFwvXFwqW1xcc1xcU10qP1xcKlxcL3xcXC9cXC8uKikvLFxuXHRcdGxvb2tiZWhpbmQ6IHRydWVcblx0fSxcblx0J2F0cnVsZSc6IHtcblx0XHRwYXR0ZXJuOiAvQFtcXHctXSsoPzpcXChbXigpXStcXCl8W14oXSkqPyg/PVxccytbeztdKS8sXG5cdFx0aW5zaWRlOiB7XG5cdFx0XHQncnVsZSc6IC9AW1xcdy1dKy9cblx0XHRcdC8vIFNlZSByZXN0IGJlbG93XG5cdFx0fVxuXHR9LFxuXHQvLyB1cmwsIGNvbXBhc3NpZmllZFxuXHQndXJsJzogLyg/OlstYS16XSstKSp1cmwoPz1cXCgpL2ksXG5cdC8vIENTUyBzZWxlY3RvciByZWdleCBpcyBub3QgYXBwcm9wcmlhdGUgZm9yIFNhc3Ncblx0Ly8gc2luY2UgdGhlcmUgY2FuIGJlIGxvdCBtb3JlIHRoaW5ncyAodmFyLCBAIGRpcmVjdGl2ZSwgbmVzdGluZy4uKVxuXHQvLyBhIHNlbGVjdG9yIG11c3Qgc3RhcnQgYXQgdGhlIGVuZCBvZiBhIHByb3BlcnR5IG9yIGFmdGVyIGEgYnJhY2UgKGVuZCBvZiBvdGhlciBydWxlcyBvciBuZXN0aW5nKVxuXHQvLyBpdCBjYW4gY29udGFpbiBzb21lIGNoYXJhY3RlcnMgdGhhdCBhcmVuJ3QgdXNlZCBmb3IgZGVmaW5pbmcgcnVsZXMgb3IgZW5kIG9mIHNlbGVjdG9yLCAmIChwYXJlbnQgc2VsZWN0b3IpLCBvciBpbnRlcnBvbGF0ZWQgdmFyaWFibGVcblx0Ly8gdGhlIGVuZCBvZiBhIHNlbGVjdG9yIGlzIGZvdW5kIHdoZW4gdGhlcmUgaXMgbm8gcnVsZXMgaW4gaXQgKCB7fSBvciB7XFxzfSkgb3IgaWYgdGhlcmUgaXMgYSBwcm9wZXJ0eSAoYmVjYXVzZSBhbiBpbnRlcnBvbGF0ZWQgdmFyXG5cdC8vIGNhbiBcInBhc3NcIiBhcyBhIHNlbGVjdG9yLSBlLmc6IHByb3BlciN7JGVydHl9KVxuXHQvLyB0aGlzIG9uZSB3YXMgaGFyZCB0byBkbywgc28gcGxlYXNlIGJlIGNhcmVmdWwgaWYgeW91IGVkaXQgdGhpcyBvbmUgOilcblx0J3NlbGVjdG9yJzoge1xuXHRcdC8vIEluaXRpYWwgbG9vay1haGVhZCBpcyB1c2VkIHRvIHByZXZlbnQgbWF0Y2hpbmcgb2YgYmxhbmsgc2VsZWN0b3JzXG5cdFx0cGF0dGVybjogLyg/PVxcUylbXkA7e30oKV0/KD86W15AO3t9KCldfCZ8I1xce1xcJFstXFx3XStcXH0pKyg/PVxccypcXHsoPzpcXH18XFxzfFtefV0rWzp7XVtefV0rKSkvbSxcblx0XHRpbnNpZGU6IHtcblx0XHRcdCdwYXJlbnQnOiB7XG5cdFx0XHRcdHBhdHRlcm46IC8mLyxcblx0XHRcdFx0YWxpYXM6ICdpbXBvcnRhbnQnXG5cdFx0XHR9LFxuXHRcdFx0J3BsYWNlaG9sZGVyJzogLyVbLVxcd10rLyxcblx0XHRcdCd2YXJpYWJsZSc6IC9cXCRbLVxcd10rfCNcXHtcXCRbLVxcd10rXFx9L1xuXHRcdH1cblx0fVxufSk7XG5cblByaXNtLmxhbmd1YWdlcy5pbnNlcnRCZWZvcmUoJ3Njc3MnLCAnYXRydWxlJywge1xuXHQna2V5d29yZCc6IFtcblx0XHQvQCg/OmlmfGVsc2UoPzogaWYpP3xmb3J8ZWFjaHx3aGlsZXxpbXBvcnR8ZXh0ZW5kfGRlYnVnfHdhcm58bWl4aW58aW5jbHVkZXxmdW5jdGlvbnxyZXR1cm58Y29udGVudCkvaSxcblx0XHR7XG5cdFx0XHRwYXR0ZXJuOiAvKCArKSg/OmZyb218dGhyb3VnaCkoPz0gKS8sXG5cdFx0XHRsb29rYmVoaW5kOiB0cnVlXG5cdFx0fVxuXHRdXG59KTtcblxuUHJpc20ubGFuZ3VhZ2VzLnNjc3MucHJvcGVydHkgPSB7XG5cdHBhdHRlcm46IC8oPzpbXFx3LV18XFwkWy1cXHddK3wjXFx7XFwkWy1cXHddK1xcfSkrKD89XFxzKjopL2ksXG5cdGluc2lkZToge1xuXHRcdCd2YXJpYWJsZSc6IC9cXCRbLVxcd10rfCNcXHtcXCRbLVxcd10rXFx9L1xuXHR9XG59O1xuXG5QcmlzbS5sYW5ndWFnZXMuaW5zZXJ0QmVmb3JlKCdzY3NzJywgJ2ltcG9ydGFudCcsIHtcblx0Ly8gdmFyIGFuZCBpbnRlcnBvbGF0ZWQgdmFyc1xuXHQndmFyaWFibGUnOiAvXFwkWy1cXHddK3wjXFx7XFwkWy1cXHddK1xcfS9cbn0pO1xuXG5QcmlzbS5sYW5ndWFnZXMuaW5zZXJ0QmVmb3JlKCdzY3NzJywgJ2Z1bmN0aW9uJywge1xuXHQncGxhY2Vob2xkZXInOiB7XG5cdFx0cGF0dGVybjogLyVbLVxcd10rLyxcblx0XHRhbGlhczogJ3NlbGVjdG9yJ1xuXHR9LFxuXHQnc3RhdGVtZW50Jzoge1xuXHRcdHBhdHRlcm46IC9cXEIhKD86ZGVmYXVsdHxvcHRpb25hbClcXGIvaSxcblx0XHRhbGlhczogJ2tleXdvcmQnXG5cdH0sXG5cdCdib29sZWFuJzogL1xcYig/OnRydWV8ZmFsc2UpXFxiLyxcblx0J251bGwnOiAvXFxibnVsbFxcYi8sXG5cdCdvcGVyYXRvcic6IHtcblx0XHRwYXR0ZXJuOiAvKFxccykoPzpbLSsqXFwvJV18Wz0hXT18PD0/fD49P3xhbmR8b3J8bm90KSg/PVxccykvLFxuXHRcdGxvb2tiZWhpbmQ6IHRydWVcblx0fVxufSk7XG5cblByaXNtLmxhbmd1YWdlcy5zY3NzWydhdHJ1bGUnXS5pbnNpZGUucmVzdCA9IFByaXNtLmxhbmd1YWdlcy5zY3NzO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3ByaXNtanMvY29tcG9uZW50cy9wcmlzbS1zY3NzLmpzXG4vLyBtb2R1bGUgaWQgPSAxMzRcbi8vIG1vZHVsZSBjaHVua3MgPSAxMCIsIi8qIGdsb2JhbCBQcmlzbSAqL1xyXG5cclxuKGZ1bmN0aW9uICgpIHtcclxuXHR2YXIgX2Fubm90YXRpb24gPSAnQFxcXFx3Kyc7XHJcblxyXG5cdC8vIE1hdGNoIGFuIGFubm90YXRpb25cclxuXHR2YXIgYW5ub3RhdGlvbiA9IHtcclxuXHRcdHBhdHRlcm46IFJlZ0V4cCgnXicgKyBfYW5ub3RhdGlvbiwgJ2dpJyksXHJcblx0XHRhbGlhczogJ2F0cnVsZSdcclxuXHR9O1xyXG5cclxuXHR2YXIgX3R5cGUgPSAne1tefV0rfSc7XHJcblxyXG5cdC8vIE1hdGNoIGEgdHlwZSAoYWx3YXlzIGZvbGxvd2luZyBhbiBhbm5vdGF0aW9uKVxyXG5cdHZhciB0eXBlID0ge1xyXG5cdFx0cGF0dGVybjogUmVnRXhwKCdeKCcgKyBfYW5ub3RhdGlvbiArICcpXFxcXHMrJyArIF90eXBlLCAnZ2knKSxcclxuXHRcdGxvb2tiZWhpbmQ6IHRydWUsXHJcblx0XHRhbGlhczogJ3N0cmluZydcclxuXHR9O1xyXG5cclxuXHR2YXIgX3BhcmFtID0gJ1tcXFxcJCVdP1tcXFxcd1xcXFwuXy1dKyc7XHJcblxyXG5cdC8vIE1hdGNoIGEgcGFyYW0gKGFsd2F5cyBmb2xsb3dpbmcgYW4gYW5ub3RhdGlvbiBhbmQgb3B0aW9uYWxseSBhIHR5cGUpXHJcblx0dmFyIHBhcmFtID0ge1xyXG5cdFx0cGF0dGVybjogUmVnRXhwKCdeKCcgKyBfYW5ub3RhdGlvbiArICcoXFxcXHMrJyArIF90eXBlICsgJyk/KVxcXFxzKycgKyBfcGFyYW0sICdnaScpLFxyXG5cdFx0bG9va2JlaGluZDogdHJ1ZSxcclxuXHRcdGFsaWFzOiAndmFyaWFibGUnXHJcblx0fTtcclxuXHJcblx0Ly8gTWF0Y2ggYSBkZWxpbWl0ZWQgVVJMXHJcblx0dmFyIHVybCA9IC88W14+XSs+L2c7XHJcblxyXG5cdFByaXNtLmxhbmd1YWdlcy5pbnNlcnRCZWZvcmUoJ3Njc3MnLCAnY29tbWVudCcsIHtcclxuXHRcdCdkb2NibG9jayc6IHtcclxuXHRcdFx0cGF0dGVybjogLyhefFteXFxcXF0pKFxcL1xcKlxcKltcXHdcXFddKj9cXCpcXC98XFwvXFwvXFwvLio/KFxccj9cXG58JCkpL2csXHJcblx0XHRcdGxvb2tiZWhpbmQ6IHRydWUsXHJcblx0XHRcdGFsaWFzOiAnY29tbWVudCcsXHJcblx0XHRcdGluc2lkZToge1xyXG5cclxuXHRcdFx0XHQvLyBBbm5vdGF0aW9uIHdpdGggcGFyYW1cclxuXHRcdFx0XHQnYW5ub3RhdGlvbi1wYXJhbSc6IHtcclxuXHRcdFx0XHRcdHBhdHRlcm46IC9AKGFjY2Vzc3xleGFtcGxlfGFsaWFzfHNpbmNlKSggLip8XFxuKS9nLFxyXG5cdFx0XHRcdFx0aW5zaWRlOiB7XHJcblx0XHRcdFx0XHRcdCdwYXJhbSc6IHBhcmFtLFxyXG5cdFx0XHRcdFx0XHQnYW5ub3RhdGlvbic6IGFubm90YXRpb24sXHJcblx0XHRcdFx0XHRcdCd1cmwnOiB1cmxcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHR9LFxyXG5cclxuXHRcdFx0XHQvLyBBbm5vdGF0aW9uIHdpdGggdHlwZSBhbmQgcGFyYW1cclxuXHRcdFx0XHQnYW5ub3RhdGlvbi10eXBlLXBhcmFtLWRlZmF1bHQnOiB7XHJcblx0XHRcdFx0XHRwYXR0ZXJuOiAvQChwYXJhbXxhcmcodW1lbnQpP3xwcm9wfHJlcXVpcmVzfHNlZSkoIC4qfFxccj9cXG58JCkvZyxcclxuXHRcdFx0XHRcdGluc2lkZToge1xyXG5cdFx0XHRcdFx0XHQnZGVmYXVsdCc6IHtcclxuXHRcdFx0XHRcdFx0XHRwYXR0ZXJuOiBSZWdFeHAoJ14oJyArIF9hbm5vdGF0aW9uICsgJyhcXFxccysnICsgX3R5cGUgKyAnKT9cXFxccysnICsgX3BhcmFtICsgJylcXFxccytcXFxcW1teXFxcXCldK1xcXFxdJywgJ2dpJyksXHJcblx0XHRcdFx0XHRcdFx0bG9va2JlaGluZDogdHJ1ZSxcclxuXHRcdFx0XHRcdFx0XHRhbGlhczogJ3N0cmluZydcclxuXHRcdFx0XHRcdFx0fSxcclxuXHRcdFx0XHRcdFx0J3BhcmFtJzogcGFyYW0sXHJcblx0XHRcdFx0XHRcdCd0eXBlJzogdHlwZSxcclxuXHRcdFx0XHRcdFx0J2Fubm90YXRpb24nOiBhbm5vdGF0aW9uLFxyXG5cdFx0XHRcdFx0XHQndXJsJzogdXJsXHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0fSxcclxuXHJcblx0XHRcdFx0Ly8gQW5ub3RhdGlvbiB3aXRoIG9ubHkgdHlwZVxyXG5cdFx0XHRcdCdhbm5vdGF0aW9uLXR5cGUnOiB7XHJcblx0XHRcdFx0XHRwYXR0ZXJuOiAvQChyZXR1cm5zPykoIC4qfFxccj9cXG58JCkvZyxcclxuXHRcdFx0XHRcdGluc2lkZToge1xyXG5cdFx0XHRcdFx0XHQndHlwZSc6IHR5cGUsXHJcblx0XHRcdFx0XHRcdCdhbm5vdGF0aW9uJzogYW5ub3RhdGlvbixcclxuXHRcdFx0XHRcdFx0J3VybCc6IHVybFxyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdH0sXHJcblxyXG5cdFx0XHRcdC8vIEFubmF0aW9uIHdpdGggYW4gVVJMXHJcblx0XHRcdFx0J2Fubm90YXRpb24tdXJsJzoge1xyXG5cdFx0XHRcdFx0cGF0dGVybjogL0AobGlua3xzb3VyY2UpKCAuKnxcXHI/XFxufCQpL2csXHJcblx0XHRcdFx0XHRpbnNpZGU6IHtcclxuXHRcdFx0XHRcdFx0J2Fubm90YXRpb24nOiBhbm5vdGF0aW9uLFxyXG5cdFx0XHRcdFx0XHQndXJsJzogL1teIF0rL1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdH0sXHJcblxyXG5cdFx0XHRcdC8vIFR5cGUgYW5ub3RhdGlvblxyXG5cdFx0XHRcdCdhbm5vdGF0aW9uLXR5cGUtY3VzdG9tJzoge1xyXG5cdFx0XHRcdFx0cGF0dGVybjogL0AodHlwZSkoIC4qfFxccj9cXG58JCkvZyxcclxuXHRcdFx0XHRcdGluc2lkZToge1xyXG5cdFx0XHRcdFx0XHQnYW5ub3RhdGlvbic6IGFubm90YXRpb24sXHJcblx0XHRcdFx0XHRcdCd0eXBlJzoge1xyXG5cdFx0XHRcdFx0XHRcdHBhdHRlcm46IC8uKi8sXHJcblx0XHRcdFx0XHRcdFx0YWxpYXM6ICdzdHJpbmcnXHJcblx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHR9LFxyXG5cclxuXHRcdFx0XHQvLyBHcm91cCBhbm5vdGF0aW9uXHJcblx0XHRcdFx0J2Fubm90YXRpb24tZ3JvdXAtY3VzdG9tJzoge1xyXG5cdFx0XHRcdFx0cGF0dGVybjogL0AoZ3JvdXApKCAuKnxcXHI/XFxufCQpL2csXHJcblx0XHRcdFx0XHRpbnNpZGU6IHtcclxuXHRcdFx0XHRcdFx0J2Fubm90YXRpb24nOiBhbm5vdGF0aW9uLFxyXG5cdFx0XHRcdFx0XHQnZ3JvdXAnOiB7XHJcblx0XHRcdFx0XHRcdFx0cGF0dGVybjogLy4qLyxcclxuXHRcdFx0XHRcdFx0XHRhbGlhczogJ3ZhcmlhYmxlJ1xyXG5cdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0fSxcclxuXHJcblx0XHRcdFx0Ly8gT3RoZXIgYW5ub3RhdGlvbnNcclxuXHRcdFx0XHQnYW5ub3RhdGlvbi1zaW5nbGUnOiB7XHJcblx0XHRcdFx0XHRwYXR0ZXJuOiAvQChjb250ZW50fGRlcHJlY2F0ZWR8aWdub3JlfG91dHB1dHxhdXRob3J8dG9kb3x0aHJvd3M/fGV4Y2VwdGlvbikoIC4qfFxccj9cXG58JCkvZyxcclxuXHRcdFx0XHRcdGluc2lkZToge1xyXG5cdFx0XHRcdFx0XHQnYW5ub3RhdGlvbic6IGFubm90YXRpb24sXHJcblx0XHRcdFx0XHRcdCd1cmwnOiB1cmxcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHR9XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHR9KTtcclxufSgpKTtcclxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vX0hUTUwvc3JjL2pzL19tb2R1bGVzL3ByaXNtanMvcHJpc20tc2Nzcy1zYXNzZG9jLmpzIiwiLyogZ2xvYmFsIFByaXNtICovXHJcblxyXG4oZnVuY3Rpb24gKCkge1xyXG5cdHZhciBfYW5ub3RhdGlvbiA9ICdAXFxcXHcrJztcclxuXHJcblx0Ly8gTWF0Y2ggYW4gYW5ub3RhdGlvblxyXG5cdHZhciBhbm5vdGF0aW9uID0ge1xyXG5cdFx0cGF0dGVybjogUmVnRXhwKCdeJyArIF9hbm5vdGF0aW9uLCAnZ2knKSxcclxuXHRcdGFsaWFzOiAnYXRydWxlJ1xyXG5cdH07XHJcblxyXG5cdHZhciBfdHlwZSA9ICd7W159XSt9JztcclxuXHJcblx0Ly8gTWF0Y2ggYSB0eXBlIChhbHdheXMgZm9sbG93aW5nIGFuIGFubm90YXRpb24pXHJcblx0dmFyIHR5cGUgPSB7XHJcblx0XHRwYXR0ZXJuOiBSZWdFeHAoJ14oJyArIF9hbm5vdGF0aW9uICsgJylcXFxccysnICsgX3R5cGUsICdnaScpLFxyXG5cdFx0bG9va2JlaGluZDogdHJ1ZSxcclxuXHRcdGFsaWFzOiAnc3RyaW5nJ1xyXG5cdH07XHJcblxyXG5cdHZhciBfcGFyYW0gPSAnW1xcXFwkJV0/W1xcXFx3XFxcXC5fLV0rJztcclxuXHJcblx0Ly8gTWF0Y2ggYSBwYXJhbSAoYWx3YXlzIGZvbGxvd2luZyBhbiBhbm5vdGF0aW9uIGFuZCBvcHRpb25hbGx5IGEgdHlwZSlcclxuXHR2YXIgcGFyYW0gPSB7XHJcblx0XHRwYXR0ZXJuOiBSZWdFeHAoJ14oJyArIF9hbm5vdGF0aW9uICsgJyhcXFxccysnICsgX3R5cGUgKyAnKT8pXFxcXHMrJyArIF9wYXJhbSwgJ2dpJyksXHJcblx0XHRsb29rYmVoaW5kOiB0cnVlLFxyXG5cdFx0YWxpYXM6ICd2YXJpYWJsZSdcclxuXHR9O1xyXG5cclxuXHQvLyBNYXRjaCBhIGRlbGltaXRlZCBVUkxcclxuXHR2YXIgdXJsID0gLzxbXj5dKz4vZztcclxuXHJcblx0UHJpc20ubGFuZ3VhZ2VzLmluc2VydEJlZm9yZSgnamF2YXNjcmlwdCcsICdjb21tZW50Jywge1xyXG5cdFx0J2RvY2Jsb2NrJzoge1xyXG5cdFx0XHRwYXR0ZXJuOiAvKF58W15cXFxcXSkoXFwvXFwqXFwqW1xcd1xcV10qP1xcKlxcL3xcXC9cXC9cXC8uKj8oXFxyP1xcbnwkKSkvZyxcclxuXHRcdFx0bG9va2JlaGluZDogdHJ1ZSxcclxuXHRcdFx0YWxpYXM6ICdjb21tZW50JyxcclxuXHRcdFx0aW5zaWRlOiB7XHJcblxyXG5cdFx0XHRcdC8vIEFubm90YXRpb24gd2l0aCBwYXJhbVxyXG5cdFx0XHRcdCdhbm5vdGF0aW9uLXBhcmFtJzoge1xyXG5cdFx0XHRcdFx0cGF0dGVybjogL0AoYWNjZXNzfGV4YW1wbGV8YWxpYXN8c2luY2UpKCAuKnxcXG4pL2csXHJcblx0XHRcdFx0XHRpbnNpZGU6IHtcclxuXHRcdFx0XHRcdFx0J3BhcmFtJzogcGFyYW0sXHJcblx0XHRcdFx0XHRcdCdhbm5vdGF0aW9uJzogYW5ub3RhdGlvbixcclxuXHRcdFx0XHRcdFx0J3VybCc6IHVybFxyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdH0sXHJcblxyXG5cdFx0XHRcdC8vIEFubm90YXRpb24gd2l0aCB0eXBlIGFuZCBwYXJhbVxyXG5cdFx0XHRcdCdhbm5vdGF0aW9uLXR5cGUtcGFyYW0tZGVmYXVsdCc6IHtcclxuXHRcdFx0XHRcdHBhdHRlcm46IC9AKHBhcmFtfGFyZyh1bWVudCk/fHByb3B8cmVxdWlyZXN8c2VlKSggLip8XFxyP1xcbnwkKS9nLFxyXG5cdFx0XHRcdFx0aW5zaWRlOiB7XHJcblx0XHRcdFx0XHRcdCdkZWZhdWx0Jzoge1xyXG5cdFx0XHRcdFx0XHRcdHBhdHRlcm46IFJlZ0V4cCgnXignICsgX2Fubm90YXRpb24gKyAnKFxcXFxzKycgKyBfdHlwZSArICcpP1xcXFxzKycgKyBfcGFyYW0gKyAnKVxcXFxzK1xcXFxbW15cXFxcKV0rXFxcXF0nLCAnZ2knKSxcclxuXHRcdFx0XHRcdFx0XHRsb29rYmVoaW5kOiB0cnVlLFxyXG5cdFx0XHRcdFx0XHRcdGFsaWFzOiAnc3RyaW5nJ1xyXG5cdFx0XHRcdFx0XHR9LFxyXG5cdFx0XHRcdFx0XHQncGFyYW0nOiBwYXJhbSxcclxuXHRcdFx0XHRcdFx0J3R5cGUnOiB0eXBlLFxyXG5cdFx0XHRcdFx0XHQnYW5ub3RhdGlvbic6IGFubm90YXRpb24sXHJcblx0XHRcdFx0XHRcdCd1cmwnOiB1cmxcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHR9LFxyXG5cclxuXHRcdFx0XHQvLyBBbm5vdGF0aW9uIHdpdGggb25seSB0eXBlXHJcblx0XHRcdFx0J2Fubm90YXRpb24tdHlwZSc6IHtcclxuXHRcdFx0XHRcdHBhdHRlcm46IC9AKHJldHVybnM/KSggLip8XFxyP1xcbnwkKS9nLFxyXG5cdFx0XHRcdFx0aW5zaWRlOiB7XHJcblx0XHRcdFx0XHRcdCd0eXBlJzogdHlwZSxcclxuXHRcdFx0XHRcdFx0J2Fubm90YXRpb24nOiBhbm5vdGF0aW9uLFxyXG5cdFx0XHRcdFx0XHQndXJsJzogdXJsXHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0fSxcclxuXHJcblx0XHRcdFx0Ly8gQW5uYXRpb24gd2l0aCBhbiBVUkxcclxuXHRcdFx0XHQnYW5ub3RhdGlvbi11cmwnOiB7XHJcblx0XHRcdFx0XHRwYXR0ZXJuOiAvQChsaW5rfHNvdXJjZSkoIC4qfFxccj9cXG58JCkvZyxcclxuXHRcdFx0XHRcdGluc2lkZToge1xyXG5cdFx0XHRcdFx0XHQnYW5ub3RhdGlvbic6IGFubm90YXRpb24sXHJcblx0XHRcdFx0XHRcdCd1cmwnOiAvW14gXSsvXHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0fSxcclxuXHJcblx0XHRcdFx0Ly8gVHlwZSBhbm5vdGF0aW9uXHJcblx0XHRcdFx0J2Fubm90YXRpb24tdHlwZS1jdXN0b20nOiB7XHJcblx0XHRcdFx0XHRwYXR0ZXJuOiAvQCh0eXBlKSggLip8XFxyP1xcbnwkKS9nLFxyXG5cdFx0XHRcdFx0aW5zaWRlOiB7XHJcblx0XHRcdFx0XHRcdCdhbm5vdGF0aW9uJzogYW5ub3RhdGlvbixcclxuXHRcdFx0XHRcdFx0J3R5cGUnOiB7XHJcblx0XHRcdFx0XHRcdFx0cGF0dGVybjogLy4qLyxcclxuXHRcdFx0XHRcdFx0XHRhbGlhczogJ3N0cmluZydcclxuXHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdH0sXHJcblxyXG5cdFx0XHRcdC8vIEdyb3VwIGFubm90YXRpb25cclxuXHRcdFx0XHQnYW5ub3RhdGlvbi1ncm91cC1jdXN0b20nOiB7XHJcblx0XHRcdFx0XHRwYXR0ZXJuOiAvQChncm91cCkoIC4qfFxccj9cXG58JCkvZyxcclxuXHRcdFx0XHRcdGluc2lkZToge1xyXG5cdFx0XHRcdFx0XHQnYW5ub3RhdGlvbic6IGFubm90YXRpb24sXHJcblx0XHRcdFx0XHRcdCdncm91cCc6IHtcclxuXHRcdFx0XHRcdFx0XHRwYXR0ZXJuOiAvLiovLFxyXG5cdFx0XHRcdFx0XHRcdGFsaWFzOiAndmFyaWFibGUnXHJcblx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHR9LFxyXG5cclxuXHRcdFx0XHQvLyBPdGhlciBhbm5vdGF0aW9uc1xyXG5cdFx0XHRcdCdhbm5vdGF0aW9uLXNpbmdsZSc6IHtcclxuXHRcdFx0XHRcdHBhdHRlcm46IC9AKGNvbnRlbnR8ZGVwcmVjYXRlZHxpZ25vcmV8c291cmNlQ29kZXxtb2R1bGV8bmFtZXxuYW1lc3BhY2V8ZmlsZU92ZXJ2aWV3fG91dHB1dHxhdXRob3J8dG9kb3x0aHJvd3M/fGV4Y2VwdGlvbikoIC4qfFxccj9cXG58JCkvZyxcclxuXHRcdFx0XHRcdGluc2lkZToge1xyXG5cdFx0XHRcdFx0XHQnYW5ub3RhdGlvbic6IGFubm90YXRpb24sXHJcblx0XHRcdFx0XHRcdCd1cmwnOiB1cmxcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHR9XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHR9KTtcclxufSgpKTtcclxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vX0hUTUwvc3JjL2pzL19tb2R1bGVzL3ByaXNtanMvcHJpc20tc2Nzcy1qc2RvYy5qcyIsIi8vIHN0eWxlLWxvYWRlcjogQWRkcyBzb21lIGNzcyB0byB0aGUgRE9NIGJ5IGFkZGluZyBhIDxzdHlsZT4gdGFnXG5cbi8vIGxvYWQgdGhlIHN0eWxlc1xudmFyIGNvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS0yLTEhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Bvc3Rjc3MtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS0yLTIhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Nhc3MtbG9hZGVyL2xpYi9sb2FkZXIuanM/P3JlZi0tMi0zIS4vcHJpc21qcy1tYXRlcmlhbC10aGVtZS5zY3NzXCIpO1xuaWYodHlwZW9mIGNvbnRlbnQgPT09ICdzdHJpbmcnKSBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG4vLyBQcmVwYXJlIGNzc1RyYW5zZm9ybWF0aW9uXG52YXIgdHJhbnNmb3JtO1xuXG52YXIgb3B0aW9ucyA9IHtcImluc2VydEF0XCI6e1wiYmVmb3JlXCI6XCIjd2VicGFjay1zdHlsZS1sb2FkZXItaW5zZXJ0LWJlZm9yZS10aGlzXCJ9LFwiaG1yXCI6dHJ1ZX1cbm9wdGlvbnMudHJhbnNmb3JtID0gdHJhbnNmb3JtXG4vLyBhZGQgdGhlIHN0eWxlcyB0byB0aGUgRE9NXG52YXIgdXBkYXRlID0gcmVxdWlyZShcIiEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvc3R5bGUtbG9hZGVyL2xpYi9hZGRTdHlsZXMuanNcIikoY29udGVudCwgb3B0aW9ucyk7XG5pZihjb250ZW50LmxvY2FscykgbW9kdWxlLmV4cG9ydHMgPSBjb250ZW50LmxvY2Fscztcbi8vIEhvdCBNb2R1bGUgUmVwbGFjZW1lbnRcbmlmKG1vZHVsZS5ob3QpIHtcblx0Ly8gV2hlbiB0aGUgc3R5bGVzIGNoYW5nZSwgdXBkYXRlIHRoZSA8c3R5bGU+IHRhZ3Ncblx0aWYoIWNvbnRlbnQubG9jYWxzKSB7XG5cdFx0bW9kdWxlLmhvdC5hY2NlcHQoXCIhIS4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTItMSEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTItMiEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvc2Fzcy1sb2FkZXIvbGliL2xvYWRlci5qcz8/cmVmLS0yLTMhLi9wcmlzbWpzLW1hdGVyaWFsLXRoZW1lLnNjc3NcIiwgZnVuY3Rpb24oKSB7XG5cdFx0XHR2YXIgbmV3Q29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTItMSEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvcG9zdGNzcy1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTItMiEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvc2Fzcy1sb2FkZXIvbGliL2xvYWRlci5qcz8/cmVmLS0yLTMhLi9wcmlzbWpzLW1hdGVyaWFsLXRoZW1lLnNjc3NcIik7XG5cdFx0XHRpZih0eXBlb2YgbmV3Q29udGVudCA9PT0gJ3N0cmluZycpIG5ld0NvbnRlbnQgPSBbW21vZHVsZS5pZCwgbmV3Q29udGVudCwgJyddXTtcblx0XHRcdHVwZGF0ZShuZXdDb250ZW50KTtcblx0XHR9KTtcblx0fVxuXHQvLyBXaGVuIHRoZSBtb2R1bGUgaXMgZGlzcG9zZWQsIHJlbW92ZSB0aGUgPHN0eWxlPiB0YWdzXG5cdG1vZHVsZS5ob3QuZGlzcG9zZShmdW5jdGlvbigpIHsgdXBkYXRlKCk7IH0pO1xufVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vX0hUTUwvc3JjL2pzL19tb2R1bGVzL3ByaXNtanMvcHJpc21qcy1tYXRlcmlhbC10aGVtZS5zY3NzXG4vLyBtb2R1bGUgaWQgPSAxMzdcbi8vIG1vZHVsZSBjaHVua3MgPSAxMCIsImV4cG9ydHMgPSBtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCIuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9saWIvY3NzLWJhc2UuanNcIikoZmFsc2UpO1xuLy8gaW1wb3J0c1xuXG5cbi8vIG1vZHVsZVxuZXhwb3J0cy5wdXNoKFttb2R1bGUuaWQsIFwicHJlW2NsYXNzKj1cXFwibGFuZ3VhZ2UtXFxcIl0sXFxuY29kZVtjbGFzcyo9XFxcImxhbmd1YWdlLVxcXCJdIHtcXG4gIGNvbG9yOiAjQ0VEM0RGO1xcbiAgdGV4dC1zaGFkb3c6IG5vbmU7XFxuICBmb250LXdlaWdodDogMzAwO1xcbiAgZm9udC1zaXplOiAxcmVtO1xcbiAgZm9udC1mYW1pbHk6ICdGaXJhIENvZGUnLCBtb25vc3BhY2U7XFxuICBkaXJlY3Rpb246IGx0cjtcXG4gIHRleHQtYWxpZ246IGxlZnQ7XFxuICB3aGl0ZS1zcGFjZTogcHJlO1xcbiAgd29yZC1zcGFjaW5nOiBub3JtYWw7XFxuICB3b3JkLWJyZWFrOiBub3JtYWw7XFxuICB3b3JkLXdyYXA6IG5vcm1hbDtcXG4gIGxpbmUtaGVpZ2h0OiAxLjRlbTtcXG4gIC1tb3otdGFiLXNpemU6IDQ7XFxuICAtby10YWItc2l6ZTogNDtcXG4gIHRhYi1zaXplOiA0O1xcbiAgLXdlYmtpdC1oeXBoZW5zOiBub25lO1xcbiAgLW1zLWh5cGhlbnM6IG5vbmU7XFxuICBoeXBoZW5zOiBub25lO1xcbiAgYmFja2dyb3VuZDogIzIxMjEyMTsgfVxcbiAgcHJlW2NsYXNzKj1cXFwibGFuZ3VhZ2UtXFxcIl06Oi1tb3otc2VsZWN0aW9uLFxcbiAgcHJlW2NsYXNzKj1cXFwibGFuZ3VhZ2UtXFxcIl0gOjotbW96LXNlbGVjdGlvbiwgcHJlW2NsYXNzKj1cXFwibGFuZ3VhZ2UtXFxcIl06Oi1tb3otc2VsZWN0aW9uLFxcbiAgcHJlW2NsYXNzKj1cXFwibGFuZ3VhZ2UtXFxcIl0gOjotbW96LXNlbGVjdGlvbixcXG4gIGNvZGVbY2xhc3MqPVxcXCJsYW5ndWFnZS1cXFwiXTo6LW1vei1zZWxlY3Rpb24sXFxuICBjb2RlW2NsYXNzKj1cXFwibGFuZ3VhZ2UtXFxcIl0gOjotbW96LXNlbGVjdGlvbixcXG4gIGNvZGVbY2xhc3MqPVxcXCJsYW5ndWFnZS1cXFwiXTo6LW1vei1zZWxlY3Rpb24sXFxuICBjb2RlW2NsYXNzKj1cXFwibGFuZ3VhZ2UtXFxcIl0gOjotbW96LXNlbGVjdGlvbiB7XFxuICAgIGJhY2tncm91bmQ6IHJnYmEoMjU1LCAyNTUsIDI1NSwgMC4yKTsgfVxcbiAgcHJlW2NsYXNzKj1cXFwibGFuZ3VhZ2UtXFxcIl06Oi1tb3otc2VsZWN0aW9uLFxcbiAgcHJlW2NsYXNzKj1cXFwibGFuZ3VhZ2UtXFxcIl0gOjotbW96LXNlbGVjdGlvbiwgcHJlW2NsYXNzKj1cXFwibGFuZ3VhZ2UtXFxcIl06OnNlbGVjdGlvbixcXG4gIHByZVtjbGFzcyo9XFxcImxhbmd1YWdlLVxcXCJdIDo6c2VsZWN0aW9uLFxcbiAgY29kZVtjbGFzcyo9XFxcImxhbmd1YWdlLVxcXCJdOjotbW96LXNlbGVjdGlvbixcXG4gIGNvZGVbY2xhc3MqPVxcXCJsYW5ndWFnZS1cXFwiXSA6Oi1tb3otc2VsZWN0aW9uLFxcbiAgY29kZVtjbGFzcyo9XFxcImxhbmd1YWdlLVxcXCJdOjpzZWxlY3Rpb24sXFxuICBjb2RlW2NsYXNzKj1cXFwibGFuZ3VhZ2UtXFxcIl0gOjpzZWxlY3Rpb24ge1xcbiAgICBiYWNrZ3JvdW5kOiByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuMik7IH1cXG5cXG5wcmVbY2xhc3MqPVxcXCJsYW5ndWFnZS1cXFwiXSB7XFxuICBwYWRkaW5nOiAyZW0gMS4ycmVtO1xcbiAgbWFyZ2luOiAxZW0gMDtcXG4gIG92ZXJmbG93OiBhdXRvOyB9XFxuICBwcmVbY2xhc3MqPVxcXCJsYW5ndWFnZS1cXFwiXTo6LXdlYmtpdC1zY3JvbGxiYXItdHJhY2sge1xcbiAgICAtd2Via2l0LWJveC1zaGFkb3c6IGluc2V0IDAgMCA2cHggcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjMpO1xcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDsgfVxcbiAgcHJlW2NsYXNzKj1cXFwibGFuZ3VhZ2UtXFxcIl06Oi13ZWJraXQtc2Nyb2xsYmFyIHtcXG4gICAgd2lkdGg6IDFyZW07XFxuICAgIGhlaWdodDogMXJlbTtcXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7IH1cXG4gIHByZVtjbGFzcyo9XFxcImxhbmd1YWdlLVxcXCJdOjotd2Via2l0LXNjcm9sbGJhci10aHVtYiB7XFxuICAgIGJhY2tncm91bmQ6IHJnYmEoMjU1LCAyNTUsIDI1NSwgMC4zNSk7IH1cXG4gIHByZVtjbGFzcyo9XFxcImxhbmd1YWdlLVxcXCJdLmNvZGUtdG9vbGJhciAudG9vbGJhciB7XFxuICAgIG9wYWNpdHk6IDE7XFxuICAgIHRvcDogLjNlbTtcXG4gICAgcmlnaHQ6IDJlbTsgfVxcbiAgICBwcmVbY2xhc3MqPVxcXCJsYW5ndWFnZS1cXFwiXS5jb2RlLXRvb2xiYXIgLnRvb2xiYXIgYSxcXG4gICAgcHJlW2NsYXNzKj1cXFwibGFuZ3VhZ2UtXFxcIl0uY29kZS10b29sYmFyIC50b29sYmFyIHNwYW4ge1xcbiAgICAgIGZvbnQtc2l6ZTogMWVtO1xcbiAgICAgIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xcbiAgICAgIGJveC1zaGFkb3c6IG5vbmU7XFxuICAgICAgY29sb3I6ICM2NjY7XFxuICAgICAgLXdlYmtpdC11c2VyLXNlbGVjdDogbm9uZTtcXG4gICAgICAtbW96LXVzZXItc2VsZWN0OiBub25lO1xcbiAgICAgIC1tcy11c2VyLXNlbGVjdDogbm9uZTtcXG4gICAgICB1c2VyLXNlbGVjdDogbm9uZTsgfVxcbiAgICBwcmVbY2xhc3MqPVxcXCJsYW5ndWFnZS1cXFwiXS5jb2RlLXRvb2xiYXIgLnRvb2xiYXIgc3BhbiB7XFxuICAgICAgcG9pbnRlci1ldmVudHM6IG5vbmU7IH1cXG4gIHByZVtjbGFzcyo9XFxcImxhbmd1YWdlLVxcXCJdLmNvZGUtdG9vbGJhci5sYW5ndWFnZS1lanMgLnRvb2xiYXIgc3BhbiB7XFxuICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7IH1cXG5cXG46bm90KHByZSkgPiBjb2RlW2NsYXNzKj1cXFwibGFuZ3VhZ2UtXFxcIl0ge1xcbiAgcGFkZGluZzogLjFlbSAuM2VtO1xcbiAgYm9yZGVyLXJhZGl1czogLjNlbTtcXG4gIHdoaXRlLXNwYWNlOiBub3JtYWw7IH1cXG5cXG5bY2xhc3MqPVxcXCJsYW5ndWFnZS1cXFwiXSAubmFtZXNwYWNlIHtcXG4gIG9wYWNpdHk6IC43OyB9XFxuXFxuW2NsYXNzKj1cXFwibGFuZ3VhZ2UtXFxcIl0gLnRva2VuLmNvbW1lbnQsIFtjbGFzcyo9XFxcImxhbmd1YWdlLVxcXCJdIC50b2tlbi5wcm9sb2csIFtjbGFzcyo9XFxcImxhbmd1YWdlLVxcXCJdIC50b2tlbi5kb2N0eXBlLCBbY2xhc3MqPVxcXCJsYW5ndWFnZS1cXFwiXSAudG9rZW4uY2RhdGEge1xcbiAgY29sb3I6ICM2NjY7XFxuICBmb250LXdlaWdodDogbm9ybWFsOyB9XFxuXFxuW2NsYXNzKj1cXFwibGFuZ3VhZ2UtXFxcIl0gLnRva2VuLm9wZXJhdG9yLCBbY2xhc3MqPVxcXCJsYW5ndWFnZS1cXFwiXSAudG9rZW4ucHVuY3R1YXRpb24ge1xcbiAgY29sb3I6ICM2ZGMyYjg7IH1cXG5cXG5bY2xhc3MqPVxcXCJsYW5ndWFnZS1cXFwiXSAudG9rZW4udGFnIHtcXG4gIGNvbG9yOiAjZmY1MzcwOyB9XFxuXFxuW2NsYXNzKj1cXFwibGFuZ3VhZ2UtXFxcIl0gLnRva2VuLnByb3BlcnR5LCBbY2xhc3MqPVxcXCJsYW5ndWFnZS1cXFwiXSAudG9rZW4uY29uc3RhbnQsIFtjbGFzcyo9XFxcImxhbmd1YWdlLVxcXCJdIC50b2tlbi5mdW5jdGlvbiwgW2NsYXNzKj1cXFwibGFuZ3VhZ2UtXFxcIl0gLnRva2VuLnN5bWJvbCwgW2NsYXNzKj1cXFwibGFuZ3VhZ2UtXFxcIl0gLnRva2VuLmRlbGV0ZWQge1xcbiAgY29sb3I6ICM3NmQ0ZmY7IH1cXG5cXG4uc3R5bGUgW2NsYXNzKj1cXFwibGFuZ3VhZ2UtXFxcIl0gLnRva2VuLmZ1bmN0aW9uLFxcbi5sYW5ndWFnZS1jc3MgW2NsYXNzKj1cXFwibGFuZ3VhZ2UtXFxcIl0gLnRva2VuLmZ1bmN0aW9uLFxcbi5sYW5ndWFnZS1zYXNzIFtjbGFzcyo9XFxcImxhbmd1YWdlLVxcXCJdIC50b2tlbi5mdW5jdGlvbixcXG4ubGFuZ3VhZ2Utc2NzcyBbY2xhc3MqPVxcXCJsYW5ndWFnZS1cXFwiXSAudG9rZW4uZnVuY3Rpb24ge1xcbiAgY29sb3I6ICNmNzhjNmM7IH1cXG5cXG4uc3R5bGUgW2NsYXNzKj1cXFwibGFuZ3VhZ2UtXFxcIl0gLnRva2VuLnByb3BlcnR5LFxcbi5sYW5ndWFnZS1jc3MgW2NsYXNzKj1cXFwibGFuZ3VhZ2UtXFxcIl0gLnRva2VuLnByb3BlcnR5LFxcbi5sYW5ndWFnZS1zYXNzIFtjbGFzcyo9XFxcImxhbmd1YWdlLVxcXCJdIC50b2tlbi5wcm9wZXJ0eSxcXG4ubGFuZ3VhZ2Utc2NzcyBbY2xhc3MqPVxcXCJsYW5ndWFnZS1cXFwiXSAudG9rZW4ucHJvcGVydHkge1xcbiAgY29sb3I6ICNmZGNmN2I7IH1cXG5cXG5bY2xhc3MqPVxcXCJsYW5ndWFnZS1cXFwiXSAudG9rZW4uc2VsZWN0b3Ige1xcbiAgY29sb3I6ICNmZmMyNTE7IH1cXG4gIFtjbGFzcyo9XFxcImxhbmd1YWdlLVxcXCJdIC50b2tlbi5zZWxlY3RvciAuaWQge1xcbiAgICBjb2xvcjogI2ZmZmYwMDsgfVxcbiAgW2NsYXNzKj1cXFwibGFuZ3VhZ2UtXFxcIl0gLnRva2VuLnNlbGVjdG9yIC5jbGFzcyB7XFxuICAgIGNvbG9yOiAjZmZjMjUxOyB9XFxuICBbY2xhc3MqPVxcXCJsYW5ndWFnZS1cXFwiXSAudG9rZW4uc2VsZWN0b3IgLmF0dHJpYnV0ZSB7XFxuICAgIGNvbG9yOiAjNmRjMmI4OyB9XFxuXFxuW2NsYXNzKj1cXFwibGFuZ3VhZ2UtXFxcIl0gLnRva2VuLmF0dHItbmFtZSwgW2NsYXNzKj1cXFwibGFuZ3VhZ2UtXFxcIl0gLnRva2VuLnN0cmluZywgW2NsYXNzKj1cXFwibGFuZ3VhZ2UtXFxcIl0gLnRva2VuLmNoYXIsIFtjbGFzcyo9XFxcImxhbmd1YWdlLVxcXCJdIC50b2tlbi5idWlsdGluLCBbY2xhc3MqPVxcXCJsYW5ndWFnZS1cXFwiXSAudG9rZW4uaW5zZXJ0ZWQge1xcbiAgY29sb3I6ICNjM2U4ODc7IH1cXG4gIC5jb21tZW50IFtjbGFzcyo9XFxcImxhbmd1YWdlLVxcXCJdIC50b2tlbi5hdHRyLW5hbWUsIC5jb21tZW50IFtjbGFzcyo9XFxcImxhbmd1YWdlLVxcXCJdIC50b2tlbi5zdHJpbmcsIC5jb21tZW50IFtjbGFzcyo9XFxcImxhbmd1YWdlLVxcXCJdIC50b2tlbi5jaGFyLCAuY29tbWVudCBbY2xhc3MqPVxcXCJsYW5ndWFnZS1cXFwiXSAudG9rZW4uYnVpbHRpbiwgLmNvbW1lbnQgW2NsYXNzKj1cXFwibGFuZ3VhZ2UtXFxcIl0gLnRva2VuLmluc2VydGVkIHtcXG4gICAgY29sb3I6ICM2NjY7IH1cXG5cXG5bY2xhc3MqPVxcXCJsYW5ndWFnZS1cXFwiXSAudG9rZW4uZW50aXR5LCBbY2xhc3MqPVxcXCJsYW5ndWFnZS1cXFwiXSAudG9rZW4udXJsLFxcbi5sYW5ndWFnZS1jc3MgW2NsYXNzKj1cXFwibGFuZ3VhZ2UtXFxcIl0gLnRva2VuLnN0cmluZyxcXG4uc3R5bGUgW2NsYXNzKj1cXFwibGFuZ3VhZ2UtXFxcIl0gLnRva2VuLnN0cmluZyB7XFxuICBjb2xvcjogI2ZmYzI1MTsgfVxcblxcbltjbGFzcyo9XFxcImxhbmd1YWdlLVxcXCJdIC50b2tlbi5hdHJ1bGUsIFtjbGFzcyo9XFxcImxhbmd1YWdlLVxcXCJdIC50b2tlbi5hdHRyLXZhbHVlIHtcXG4gIGNvbG9yOiAjQzJFNzhDOyB9XFxuXFxuW2NsYXNzKj1cXFwibGFuZ3VhZ2UtXFxcIl0gLnRva2VuLm51bWJlciwgW2NsYXNzKj1cXFwibGFuZ3VhZ2UtXFxcIl0gLnRva2VuLmltcG9ydGFudCwgW2NsYXNzKj1cXFwibGFuZ3VhZ2UtXFxcIl0gLnRva2VuLnRpbWUsIFtjbGFzcyo9XFxcImxhbmd1YWdlLVxcXCJdIC50b2tlbi5lYXNpbmcsIFtjbGFzcyo9XFxcImxhbmd1YWdlLVxcXCJdIC50b2tlbi5hbmdsZSwgW2NsYXNzKj1cXFwibGFuZ3VhZ2UtXFxcIl0gLnRva2VuLmJvb2xlYW4sIFtjbGFzcyo9XFxcImxhbmd1YWdlLVxcXCJdIC50b2tlbi5rZXl3b3JkIHtcXG4gIGNvbG9yOiAjYzc5MmVhOyB9XFxuXFxuW2NsYXNzKj1cXFwibGFuZ3VhZ2UtXFxcIl0gLnRva2VuLnJlZ2V4LCBbY2xhc3MqPVxcXCJsYW5ndWFnZS1cXFwiXSAudG9rZW4uaW1wb3J0YW50LCBbY2xhc3MqPVxcXCJsYW5ndWFnZS1cXFwiXSAudG9rZW4udmFyaWFibGUge1xcbiAgY29sb3I6ICNmZmMyNTE7IH1cXG4gIC5jb21tZW50IFtjbGFzcyo9XFxcImxhbmd1YWdlLVxcXCJdIC50b2tlbi5yZWdleCwgLmNvbW1lbnQgW2NsYXNzKj1cXFwibGFuZ3VhZ2UtXFxcIl0gLnRva2VuLmltcG9ydGFudCwgLmNvbW1lbnQgW2NsYXNzKj1cXFwibGFuZ3VhZ2UtXFxcIl0gLnRva2VuLnZhcmlhYmxlIHtcXG4gICAgY29sb3I6ICM2NjY7IH1cXG5cXG5bY2xhc3MqPVxcXCJsYW5ndWFnZS1cXFwiXSAudG9rZW4ub3BlcmF0b3IsIFtjbGFzcyo9XFxcImxhbmd1YWdlLVxcXCJdIC50b2tlbi5pbXBvcnRhbnQsIFtjbGFzcyo9XFxcImxhbmd1YWdlLVxcXCJdIC50b2tlbi5ib2xkIHtcXG4gIGZvbnQtd2VpZ2h0OiBib2xkOyB9XFxuXFxuW2NsYXNzKj1cXFwibGFuZ3VhZ2UtXFxcIl0gLnRva2VuLml0YWxpYyB7XFxuICBmb250LXN0eWxlOiBpdGFsaWM7IH1cXG5cXG5bY2xhc3MqPVxcXCJsYW5ndWFnZS1cXFwiXSAudG9rZW4uZW50aXR5IHtcXG4gIGN1cnNvcjogaGVscDsgfVxcblxcbltjbGFzcyo9XFxcImxhbmd1YWdlLVxcXCJdIC50b2tlbi5hbm5vdGF0aW9uIHtcXG4gIGNvbG9yOiAjNmI1MTdmOyB9XFxuXFxuW2NsYXNzKj1cXFwibGFuZ3VhZ2UtXFxcIl0gLnRva2VuLnRhYjpub3QoOmVtcHR5KTo6YmVmb3JlLCBbY2xhc3MqPVxcXCJsYW5ndWFnZS1cXFwiXSAudG9rZW4uY3I6OmJlZm9yZSwgW2NsYXNzKj1cXFwibGFuZ3VhZ2UtXFxcIl0gLnRva2VuLmxmOjpiZWZvcmUsIFtjbGFzcyo9XFxcImxhbmd1YWdlLVxcXCJdIC50b2tlbi5zcGFjZTo6YmVmb3JlIHtcXG4gIGNvbG9yOiAjMDAwOyB9XFxuXFxuLnByaXNtLXByZXZpZXdlci10aW1lLFxcbi5wcmlzbS1wcmV2aWV3ZXItYW5nbGUsXFxuLnByaXNtLXByZXZpZXdlci1jb2xvcixcXG4ucHJpc20tcHJldmlld2VyLWdyYWRpZW50IHtcXG4gIHdpZHRoOiA0ZW07XFxuICBoZWlnaHQ6IDRlbTtcXG4gIG1hcmdpbi10b3A6IC01ZW07XFxuICBtYXJnaW4tbGVmdDogLTJlbTsgfVxcbiAgLnByaXNtLXByZXZpZXdlci10aW1lIHN2ZyxcXG4gIC5wcmlzbS1wcmV2aWV3ZXItYW5nbGUgc3ZnLFxcbiAgLnByaXNtLXByZXZpZXdlci1jb2xvciBzdmcsXFxuICAucHJpc20tcHJldmlld2VyLWdyYWRpZW50IHN2ZyB7XFxuICAgIHdpZHRoOiA0ZW07XFxuICAgIGhlaWdodDogNGVtOyB9XFxuICBAbWVkaWEgc2NyZWVuIGFuZCAobWluLXdpZHRoOiA2NDFweCkge1xcbiAgICBwcmVbY2xhc3MqPVxcXCJsYW5ndWFnZS1cXFwiXSB7XFxuICAgICAgcGFkZGluZzogMmVtOyB9IH1cXG5cIiwgXCJcIl0pO1xuXG4vLyBleHBvcnRzXG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyPz9yZWYtLTItMSEuL25vZGVfbW9kdWxlcy9wb3N0Y3NzLWxvYWRlci9saWI/P3JlZi0tMi0yIS4vbm9kZV9tb2R1bGVzL3Nhc3MtbG9hZGVyL2xpYi9sb2FkZXIuanM/P3JlZi0tMi0zIS4vX0hUTUwvc3JjL2pzL19tb2R1bGVzL3ByaXNtanMvcHJpc21qcy1tYXRlcmlhbC10aGVtZS5zY3NzXG4vLyBtb2R1bGUgaWQgPSAxMzhcbi8vIG1vZHVsZSBjaHVua3MgPSAxMCIsIihmdW5jdGlvbiAoKSB7XHJcblx0J3VzZSBzdHJpY3QnO1xyXG5cclxuXHRpZiAodHlwZW9mIFByaXNtID09PSAndW5kZWZpbmVkJykge1xyXG5cdFx0Y29uc29sZS5sb2coJ1ByaXNtIGlzIG5vdCBkZWZpbmVkJyk7XHJcblx0XHRyZXR1cm4gZmFsc2U7XHJcblx0fVxyXG5cclxuXHRpZiAoIVByaXNtLmxhbmd1YWdlcy5tYXJrdXAgfHwgIVByaXNtLmxhbmd1YWdlcy5qYXZhc2NyaXB0KSB7XHJcblx0XHRjb25zb2xlLmxvZygncHJpc20tZWpzLWxhbmd1YWdlIHJlcXVpcmVkIG1hcmt1cCBhbmQgamF2YXNjcmlwdCBsYW5ndWFnZXMnKTtcclxuXHRcdHJldHVybiBmYWxzZTtcclxuXHR9XHJcblxyXG5cdFByaXNtLmxhbmd1YWdlcy5lanMgPSBQcmlzbS5sYW5ndWFnZXMuZXh0ZW5kKCdtYXJrdXAnLCB7XHJcblx0XHQnY29tbWVudCc6IC8oPCUlPyMpW1xcc1xcU10qPyhbLV0/JSU/PikvZyxcclxuXHRcdCdwdW5jdHVhdGlvbic6IC8oPCUlP1stfD18X10/fFstfF9dPyUlPz58XFwvPz4pL2csXHJcblx0XHQnZW50aXR5JzogLyYjP1tcXGRhLXpdezEsOH07L2ksXHJcblx0XHQnYXR0ci1uYW1lJzoge1xyXG5cdFx0XHRwYXR0ZXJuOiAvW15cXHM+XFwvXSsvLFxyXG5cdFx0XHRpbnNpZGU6IHtcclxuXHRcdFx0XHQnbmFtZXNwYWNlJzogL15bXlxccz5cXC86XSs6LyxcclxuXHRcdFx0XHQndGFnJzoge1xyXG5cdFx0XHRcdFx0cGF0dGVybjogLzxcXHcrLyxcclxuXHRcdFx0XHRcdGluc2lkZToge1xyXG5cdFx0XHRcdFx0XHRwdW5jdHVhdGlvbjogL148L1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdH0sXHJcblx0XHRcdFx0J2F0dHItdmFsdWUnOiB7XHJcblx0XHRcdFx0XHRwYXR0ZXJuOiAvPSg/OihcInwnKSg/OlxcXFxbXFxzXFxTXXwoPyFcXDEpW15cXFxcXSkqXFwxfFteXFxzJ1wiPj1dKykvaSxcclxuXHRcdFx0XHRcdGluc2lkZToge1xyXG5cdFx0XHRcdFx0XHQncHVuY3R1YXRpb24nOiBbXHJcblx0XHRcdFx0XHRcdFx0L149LyxcclxuXHRcdFx0XHRcdFx0XHR7XHJcblx0XHRcdFx0XHRcdFx0XHRwYXR0ZXJuOiAvKF58W15cXFxcXSlbXCInXS8sXHJcblx0XHRcdFx0XHRcdFx0XHRsb29rYmVoaW5kOiB0cnVlXHJcblx0XHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0XHRdXHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0fSxcclxuXHRcdFx0XHQncHVuY3R1YXRpb24nOiAvPS9cclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdH0pO1xyXG5cclxuXHRQcmlzbS5sYW5ndWFnZXMuaW5zZXJ0QmVmb3JlKCdlanMnLCAndGFnJywge1xyXG5cdFx0c2NyaXB0OiB7XHJcblx0XHRcdHBhdHRlcm46IC8oPCUlP1stfD18X10/KVtcXHNcXFNdKj8oPz1bLXxfXT8lJT8+KS9pLFxyXG5cdFx0XHRsb29rYmVoaW5kOiAhMCxcclxuXHRcdFx0aW5zaWRlOiBQcmlzbS5sYW5ndWFnZXMuamF2YXNjcmlwdCxcclxuXHRcdFx0YWxpYXM6ICdsYW5ndWFnZS1qYXZhc2NyaXB0J1xyXG5cdFx0fVxyXG5cdH0pO1xyXG59KCkpO1xyXG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9wcmlzbS1lanMtbGFuZ3VhZ2UvcHJpc20tZWpzLWxhbmd1YWdlLmpzXG4vLyBtb2R1bGUgaWQgPSAxMzlcbi8vIG1vZHVsZSBjaHVua3MgPSAxMCJdLCJzb3VyY2VSb290IjoiIn0=