webpackJsonp([13],{

/***/ 142:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * Оборачивание элементов для пропорцинальной адаптации
 * @module
 */

// ----------------------------------------
// Imports
// ----------------------------------------

Object.defineProperty(exports, "__esModule", {
	value: true
});

__webpack_require__(8);

// ----------------------------------------
// Public
// ----------------------------------------

/**
 * @param {JQuery.Selector|JQuery} [selector=".js-form"]- поиск элементов по селектору
 * @param {JQuery} [$context=null] - контекст в котором будет выполнен поиск селектора
 * @sourceCode
 */
function wrapMedia(selector) {
	var $context = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;

	/**
  * @type {JQuery}
  */
	var $elements = selector && selector.jquery ? selector : $(selector, $context);
	$elements.each(function (i, el) {
		var $el = $(el);
		if ($el.hasInitedKey('wrap-media', false)) {
			return true;
		}

		var width = parseInt($el.attr('width') || $el[0].offsetWidth);
		var height = parseInt($el.attr('height') || $el[0].offsetHeight);
		var ratio = parseFloat((height / width * 100).toFixed(2));

		$el.unwrap('p').wrap('\n\t\t<div class="ratio-wrapper" style="max-width:' + width + 'px;margin-left:auto;margin-right:auto;">\n\t\t\t<div class="ratio" style="padding-top:' + ratio + '%;"></div>\n\t\t</div>\n\t\t');
	});
}

// ----------------------------------------
// Exports
// ----------------------------------------

exports.default = wrapMedia;

/***/ })

});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9fSFRNTC9zcmMvanMvX21vZHVsZXMvd3JhcC1tZWRpYS5qcyJdLCJuYW1lcyI6WyJ3cmFwTWVkaWEiLCJzZWxlY3RvciIsIiRjb250ZXh0IiwiJGVsZW1lbnRzIiwianF1ZXJ5IiwiJCIsImVhY2giLCJpIiwiZWwiLCIkZWwiLCJoYXNJbml0ZWRLZXkiLCJ3aWR0aCIsInBhcnNlSW50IiwiYXR0ciIsIm9mZnNldFdpZHRoIiwiaGVpZ2h0Iiwib2Zmc2V0SGVpZ2h0IiwicmF0aW8iLCJwYXJzZUZsb2F0IiwidG9GaXhlZCIsInVud3JhcCIsIndyYXAiXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBOztBQUVBOzs7OztBQUtBO0FBQ0E7QUFDQTs7Ozs7O0FBRUE7O0FBRUE7QUFDQTtBQUNBOztBQUVBOzs7OztBQUtBLFNBQVNBLFNBQVQsQ0FBb0JDLFFBQXBCLEVBQStDO0FBQUEsS0FBakJDLFFBQWlCLHVFQUFOLElBQU07O0FBQzlDOzs7QUFHQSxLQUFJQyxZQUFhRixZQUFZQSxTQUFTRyxNQUF0QixHQUFnQ0gsUUFBaEMsR0FBMkNJLEVBQUVKLFFBQUYsRUFBWUMsUUFBWixDQUEzRDtBQUNBQyxXQUFVRyxJQUFWLENBQWUsVUFBQ0MsQ0FBRCxFQUFJQyxFQUFKLEVBQVc7QUFDekIsTUFBSUMsTUFBTUosRUFBRUcsRUFBRixDQUFWO0FBQ0EsTUFBSUMsSUFBSUMsWUFBSixDQUFpQixZQUFqQixFQUErQixLQUEvQixDQUFKLEVBQTJDO0FBQzFDLFVBQU8sSUFBUDtBQUNBOztBQUVELE1BQUlDLFFBQVFDLFNBQVNILElBQUlJLElBQUosQ0FBUyxPQUFULEtBQXFCSixJQUFJLENBQUosRUFBT0ssV0FBckMsQ0FBWjtBQUNBLE1BQUlDLFNBQVNILFNBQVNILElBQUlJLElBQUosQ0FBUyxRQUFULEtBQXNCSixJQUFJLENBQUosRUFBT08sWUFBdEMsQ0FBYjtBQUNBLE1BQUlDLFFBQVFDLFdBQVcsQ0FBQ0gsU0FBU0osS0FBVCxHQUFpQixHQUFsQixFQUF1QlEsT0FBdkIsQ0FBK0IsQ0FBL0IsQ0FBWCxDQUFaOztBQUVBVixNQUFJVyxNQUFKLENBQVcsR0FBWCxFQUFnQkMsSUFBaEIsd0RBQzhDVixLQUQ5Qyw4RkFFeUNNLEtBRnpDO0FBS0EsRUFmRDtBQWdCQTs7QUFFRDtBQUNBO0FBQ0E7O2tCQUVlakIsUyIsImZpbGUiOiJhc3luYy1tb2R1bGVzL3dyYXAtbWVkaWEuanM/dj1kMzQ1MGNiMjQxYmU4MzYyOTQ3MSIsInNvdXJjZXNDb250ZW50IjpbIid1c2Ugc3RyaWN0JztcclxuXHJcbi8qKlxyXG4gKiDQntCx0L7RgNCw0YfQuNCy0LDQvdC40LUg0Y3Qu9C10LzQtdC90YLQvtCyINC00LvRjyDQv9GA0L7Qv9C+0YDRhtC40L3QsNC70YzQvdC+0Lkg0LDQtNCw0L/RgtCw0YbQuNC4XHJcbiAqIEBtb2R1bGVcclxuICovXHJcblxyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbi8vIEltcG9ydHNcclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuaW1wb3J0ICdjdXN0b20tanF1ZXJ5LW1ldGhvZHMvZm4vaGFzLWluaXRlZC1rZXknO1xyXG5cclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4vLyBQdWJsaWNcclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuLyoqXHJcbiAqIEBwYXJhbSB7SlF1ZXJ5LlNlbGVjdG9yfEpRdWVyeX0gW3NlbGVjdG9yPVwiLmpzLWZvcm1cIl0tINC/0L7QuNGB0Log0Y3Qu9C10LzQtdC90YLQvtCyINC/0L4g0YHQtdC70LXQutGC0L7RgNGDXHJcbiAqIEBwYXJhbSB7SlF1ZXJ5fSBbJGNvbnRleHQ9bnVsbF0gLSDQutC+0L3RgtC10LrRgdGCINCyINC60L7RgtC+0YDQvtC8INCx0YPQtNC10YIg0LLRi9C/0L7Qu9C90LXQvSDQv9C+0LjRgdC6INGB0LXQu9C10LrRgtC+0YDQsFxyXG4gKiBAc291cmNlQ29kZVxyXG4gKi9cclxuZnVuY3Rpb24gd3JhcE1lZGlhIChzZWxlY3RvciwgJGNvbnRleHQgPSBudWxsKSB7XHJcblx0LyoqXHJcblx0ICogQHR5cGUge0pRdWVyeX1cclxuXHQgKi9cclxuXHRsZXQgJGVsZW1lbnRzID0gKHNlbGVjdG9yICYmIHNlbGVjdG9yLmpxdWVyeSkgPyBzZWxlY3RvciA6ICQoc2VsZWN0b3IsICRjb250ZXh0KTtcclxuXHQkZWxlbWVudHMuZWFjaCgoaSwgZWwpID0+IHtcclxuXHRcdGxldCAkZWwgPSAkKGVsKTtcclxuXHRcdGlmICgkZWwuaGFzSW5pdGVkS2V5KCd3cmFwLW1lZGlhJywgZmFsc2UpKSB7XHJcblx0XHRcdHJldHVybiB0cnVlO1xyXG5cdFx0fVxyXG5cclxuXHRcdGxldCB3aWR0aCA9IHBhcnNlSW50KCRlbC5hdHRyKCd3aWR0aCcpIHx8ICRlbFswXS5vZmZzZXRXaWR0aCk7XHJcblx0XHRsZXQgaGVpZ2h0ID0gcGFyc2VJbnQoJGVsLmF0dHIoJ2hlaWdodCcpIHx8ICRlbFswXS5vZmZzZXRIZWlnaHQpO1xyXG5cdFx0bGV0IHJhdGlvID0gcGFyc2VGbG9hdCgoaGVpZ2h0IC8gd2lkdGggKiAxMDApLnRvRml4ZWQoMikpO1xyXG5cclxuXHRcdCRlbC51bndyYXAoJ3AnKS53cmFwKGBcclxuXHRcdDxkaXYgY2xhc3M9XCJyYXRpby13cmFwcGVyXCIgc3R5bGU9XCJtYXgtd2lkdGg6JHt3aWR0aH1weDttYXJnaW4tbGVmdDphdXRvO21hcmdpbi1yaWdodDphdXRvO1wiPlxyXG5cdFx0XHQ8ZGl2IGNsYXNzPVwicmF0aW9cIiBzdHlsZT1cInBhZGRpbmctdG9wOiR7cmF0aW99JTtcIj48L2Rpdj5cclxuXHRcdDwvZGl2PlxyXG5cdFx0YCk7XHJcblx0fSk7XHJcbn1cclxuXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuLy8gRXhwb3J0c1xyXG4vLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG5leHBvcnQgZGVmYXVsdCB3cmFwTWVkaWE7XHJcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL19IVE1ML3NyYy9qcy9fbW9kdWxlcy93cmFwLW1lZGlhLmpzIl0sInNvdXJjZVJvb3QiOiIifQ==