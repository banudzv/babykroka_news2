<?php

ini_set('display_errors', 'on'); // Display all errors on screen
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
header("Cache-Control: public");
header("Expires: " . date("r", time() + 3600));
header('Content-Type: text/html; charset=UTF-8');
ob_start();
@session_start();
//    define('DS', DIRECTORY_SEPARATOR);
define('DS', '/');
define('HOST', dirname(__FILE__)); // Root path
define('MULTI_LANGUAGE', false);
define('APPLICATION', 'frontend'); // Choose application - backend|frontend
define('PROFILER', false); // On/off profiler
define('START_TIME', microtime(true)); // For profiler. Don't touch!
define('START_MEMORY', memory_get_usage()); // For profiler. Don't touch!

require_once 'loader.php';

$res = \Core\Common::factory('config')->getRow('export_link', 'key');
$domain = $res->zna;

//Инициализация curl
$curlInit = curl_init();
curl_setopt($curlInit, CURLOPT_HEADER, 0);
curl_setopt($curlInit, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($curlInit, CURLOPT_URL, $domain);

//Получаем ответ
$response = curl_exec($curlInit);

curl_close($curlInit);
//$response = '{"products":[{"id":"164","status":"1","name":"Комбинезон1","parent_id":"63","new":"0","sale":"0","top":"1","cost":"420","cost_old":"0","artikul":"584SDFS","brand_id":"47","age_id":null,"color_id":"2","color_group_id":null,"sizes":{"10":"10","20":"20","25":"25","30":"30","40":"40","45":"45"},"additional":null},{"id":"166","status":"1","name":"Носки детские1","parent_id":"77","new":"0","sale":"0","top":"1","cost":"15","cost_old":"0","artikul":"8С353","brand_id":"50","age_id":null,"color_id":"2","color_group_id":"1","sizes":[],"additional":null},{"id":"168","status":"1","name":"Смарт носки с турбо подогревом, телефоном и прочим","parent_id":"77","new":"0","sale":"0","top":"1","cost":"52185","cost_old":"0","artikul":"56DVSHJ218","brand_id":"48","age_id":null,"color_id":"3","color_group_id":null,"sizes":[],"additional":null},{"id":"169","status":"1","name":"Носки Rollo Wolf SET31B 43-46 8 пар Разноцветные","parent_id":"77","new":"1","sale":"0","top":"1","cost":"1700","cost_old":"0","artikul":"ROZ6205008251","brand_id":"48","age_id":null,"color_id":"5","color_group_id":"1","sizes":[],"additional":null},{"id":"170","status":"1","name":"Комбинезон2","parent_id":"63","new":"0","sale":"0","top":"1","cost":"420","cost_old":"0","artikul":"FSDF874DS","brand_id":"48","age_id":null,"color_id":"1","color_group_id":null,"sizes":[],"additional":null},{"id":"171","status":"1","name":"Комбинезон3","parent_id":"63","new":"1","sale":"0","top":"0","cost":"420","cost_old":"0","artikul":"SDF48DSF15","brand_id":"47","age_id":null,"color_id":"2","color_group_id":"2","sizes":[],"additional":null},{"id":"172","status":"1","name":"Комбинезон1","parent_id":"113","new":"0","sale":"1","top":"1","cost":"420","cost_old":"700","artikul":"5S6D4F9DS1","brand_id":"50","age_id":null,"color_id":"1","color_group_id":null,"sizes":[],"additional":null},{"id":"173","status":"1","name":"Носки детские1","parent_id":"100","new":"1","sale":"1","top":"1","cost":"15","cost_old":"25","artikul":"S6D5SD","brand_id":"50","age_id":null,"color_id":"3","color_group_id":"1","sizes":{"1":"1","2":"2","3":"3"},"additional":null},{"id":"174","status":"1","name":"Смарт носки с турбо подогревом, телефоном и прочим","parent_id":"100","new":"0","sale":"0","top":"1","cost":"52185","cost_old":"0","artikul":"56DVSHJ218","brand_id":"49","age_id":null,"color_id":"2","color_group_id":null,"sizes":[],"additional":null},{"id":"175","status":"1","name":"Носки Rollo Wolf SET31B 43-46 8 пар Разноцветные","parent_id":"100","new":"0","sale":"1","top":"1","cost":"1700","cost_old":"1922","artikul":"ROZ6205008251","brand_id":"47","age_id":"2","color_id":"5","color_group_id":null,"sizes":{"10":"0","20":"0","30":"10","40":"0","50":"10","60":"2"},"additional":null},{"id":"176","status":"1","name":"Комбинезон2","parent_id":"113","new":"1","sale":"0","top":"1","cost":"420","cost_old":"450","artikul":"548SD4F1S9","brand_id":"50","age_id":null,"color_id":"3","color_group_id":null,"sizes":[],"additional":null},{"id":"177","status":"1","name":"Комбинезон3","parent_id":"113","new":"0","sale":"0","top":"0","cost":"420","cost_old":"0","artikul":"SDFSDFQWECG58","brand_id":"47","age_id":null,"color_id":"4","color_group_id":null,"sizes":[],"additional":null},{"id":"178","status":"1","name":"Мягкая игрушка","parent_id":"58","new":"1","sale":"1","top":"1","cost":"111","cost_old":"123489","artikul":"71F198A","brand_id":"47","age_id":null,"color_id":null,"color_group_id":null,"sizes":[],"additional":null},{"id":"179","status":"1","name":"Комбинезон4","parent_id":"63","new":"0","sale":"0","top":"1","cost":"424","cost_old":"0","artikul":"KJGDJHN6","brand_id":"48","age_id":null,"color_id":"2","color_group_id":null,"sizes":[],"additional":null},{"id":"180","status":"1","name":"Комбинезон5","parent_id":"63","new":"0","sale":"0","top":"1","cost":"425","cost_old":"0","artikul":"41VGEWR81","brand_id":"49","age_id":null,"color_id":"1","color_group_id":null,"sizes":[],"additional":null},{"id":"181","status":"1","name":"Комбинезон6","parent_id":"63","new":"0","sale":"0","top":"1","cost":"426","cost_old":"0","artikul":"A98REW416","brand_id":"48","age_id":null,"color_id":"2","color_group_id":null,"sizes":[],"additional":null},{"id":"182","status":"1","name":"Комбинезон7","parent_id":"63","new":"0","sale":"0","top":"1","cost":"427","cost_old":"0","artikul":"5F4A1JUT98","brand_id":"48","age_id":null,"color_id":"2","color_group_id":null,"sizes":[],"additional":null},{"id":"184","status":"1","name":"Комбинезон8","parent_id":"63","new":"0","sale":"0","top":"1","cost":"428","cost_old":"0","artikul":"ASD5ERT41GS","brand_id":"47","age_id":null,"color_id":"5","color_group_id":null,"sizes":[],"additional":null},{"id":"185","status":"1","name":"Комбинезон9","parent_id":"63","new":"1","sale":"0","top":"1","cost":"429","cost_old":"0","artikul":"54S1HFS56","brand_id":"50","age_id":null,"color_id":"1","color_group_id":null,"sizes":[],"additional":null},{"id":"186","status":"1","name":"Комбинезон10","parent_id":"63","new":"0","sale":"1","top":"1","cost":"430","cost_old":"500","artikul":"9ARHJ78F8","brand_id":"49","age_id":null,"color_id":"3","color_group_id":null,"sizes":[],"additional":null},{"id":"187","status":"1","name":"Комбинезон11","parent_id":"63","new":"0","sale":"0","top":"1","cost":"730","cost_old":"0","artikul":"A98H16A","brand_id":"48","age_id":null,"color_id":"4","color_group_id":null,"sizes":[],"additional":null},{"id":"188","status":"1","name":"Комбинезон12","parent_id":"63","new":"0","sale":"0","top":"1","cost":"432","cost_old":"0","artikul":"21ASHJ9Y7UG","brand_id":"49","age_id":null,"color_id":"3","color_group_id":null,"sizes":[],"additional":null},{"id":"189","status":"1","name":"Комбинезон13","parent_id":"63","new":"0","sale":"0","top":"1","cost":"433","cost_old":"0","artikul":"JKA23T9UIDGQ","brand_id":"47","age_id":null,"color_id":"3","color_group_id":"2","sizes":[],"additional":null},{"id":"190","status":"1","name":"Комбинезон14","parent_id":"63","new":"0","sale":"0","top":"1","cost":"434","cost_old":"0","artikul":"V73HGFQ","brand_id":"48","age_id":null,"color_id":"4","color_group_id":"2","sizes":[],"additional":null},{"id":"192","status":"1","name":"Комбинезон15","parent_id":"63","new":"0","sale":"1","top":"1","cost":"435","cost_old":"450","artikul":"JGH3QWGJ89","brand_id":"50","age_id":"2","color_id":"5","color_group_id":"2","sizes":{"10":"10","20":"1234"},"additional":null}],"brands":[{"id":"50","name":"Франция"},{"id":"49","name":"Китай"},{"id":"48","name":"Турция"},{"id":"47","name":"Собственное производство"}],"colors":[{"id":"5","name":"Желтый"},{"id":"4","name":"Белый"},{"id":"3","name":"Черный"},{"id":"2","name":"Красный"},{"id":"1","name":"Зеленый"}],"color_groups":[{"id":null},{"id":null}],"groups":[{"id":"55","parent_id":"0","name":"Мальчикам"},{"id":"56","parent_id":"0","name":"Девочкам"},{"id":"57","parent_id":"0","name":"Новорожденным"},{"id":"58","parent_id":"0","name":"Игрушки"},{"id":"59","parent_id":"55","name":"Верхняя одежда"},{"id":"61","parent_id":"55","name":"Головные уборы"},{"id":"62","parent_id":"55","name":"Одежда"},{"id":"63","parent_id":"59","name":"Комбинезоны"},{"id":"64","parent_id":"59","name":"Куртки"},{"id":"66","parent_id":"59","name":"Полукомбинезоны"},{"id":"67","parent_id":"61","name":"Шапки"},{"id":"68","parent_id":"61","name":"Бейсболки"},{"id":"69","parent_id":"61","name":"Перчатки, руковицы"},{"id":"70","parent_id":"62","name":"Брюки"},{"id":"71","parent_id":"62","name":"Джинсы"},{"id":"72","parent_id":"62","name":"Водолазки"},{"id":"73","parent_id":"62","name":"Гольфы"},{"id":"74","parent_id":"62","name":"Костюмы"},{"id":"75","parent_id":"62","name":"Майки"},{"id":"76","parent_id":"62","name":"Нижнее белье"},{"id":"77","parent_id":"62","name":"Носки"},{"id":"78","parent_id":"62","name":"Рубашки"},{"id":"79","parent_id":"62","name":"Толстовки"},{"id":"80","parent_id":"62","name":"Футболки"},{"id":"81","parent_id":"62","name":"Шорты"},{"id":"82","parent_id":"62","name":"Бриджи"},{"id":"83","parent_id":"62","name":"Свитера"},{"id":"84","parent_id":"62","name":"Джемпера"},{"id":"85","parent_id":"62","name":"Пижамы"},{"id":"89","parent_id":"56","name":"Верхняя одежда"},{"id":"90","parent_id":"56","name":"Головные уборы"},{"id":"91","parent_id":"56","name":"Одежда"},{"id":"92","parent_id":"91","name":"Пижамы"},{"id":"93","parent_id":"91","name":"Джемпера"},{"id":"94","parent_id":"91","name":"Свитера"},{"id":"95","parent_id":"91","name":"Бриджи"},{"id":"96","parent_id":"91","name":"Шорты"},{"id":"97","parent_id":"91","name":"Футболки"},{"id":"98","parent_id":"91","name":"Толстовки"},{"id":"99","parent_id":"91","name":"Рубашки"},{"id":"100","parent_id":"91","name":"Носки"},{"id":"101","parent_id":"91","name":"Нижнее белье"},{"id":"102","parent_id":"91","name":"Майки"},{"id":"103","parent_id":"91","name":"Костюмы"},{"id":"104","parent_id":"91","name":"Гольфы"},{"id":"105","parent_id":"91","name":"Водолазки"},{"id":"106","parent_id":"91","name":"Джинсы"},{"id":"107","parent_id":"91","name":"Брюки"},{"id":"108","parent_id":"90","name":"Перчатки, руковицы"},{"id":"109","parent_id":"90","name":"Бейсболки"},{"id":"110","parent_id":"90","name":"Шапки"},{"id":"111","parent_id":"89","name":"Полукомбинезоны"},{"id":"112","parent_id":"89","name":"Куртки"},{"id":"113","parent_id":"89","name":"Комбинезоны"}],"ages":[{"id":"1","name":"От 0 до 1 года"},{"id":"2","name":"От 1 до 2 лет"},{"id":"3","name":"От 2 до 3 лет"},{"id":"4","name":"От 4 до 5 лет"},{"id":"5","name":"От 5 до 6 лет"},{"id":"6","name":"От 6 до 7 лет"}]}';
$json = json_decode($response, true);
$products = $json['products'];

foreach ($products as $obj):

    foreach ($obj['sizes'] as $size => $amount):

        $size_db = \Core\QB\DB::select()
            ->from('catalog_size_amount')
            ->where('catalog_id', '=', $obj['id'])
            ->where('size', '=', $size)
            ->find();

        if ($size_db):
            if ($size_db->amount != $amount):
                $res = \Modules\Catalog\Models\Sizes::update(['amount' => $amount], $size_db->id);
                if ($res):
                    $success = true;
                else:
                    $success = false;
                endif;
            else:
                $success = true;
            endif;
        else:

            $data = [
                'catalog_id' => $obj['id'],
                'size' => $size,
                'amount' => $amount,
            ];
            $res = \Modules\Catalog\Models\Sizes::insert($data);
            if ($res) :
                $success = true;
            else :
                $success = false;
            endif;
        endif;

    endforeach;

endforeach;

echo $success;
die();