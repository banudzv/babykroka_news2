<!DOCTYPE html>
<html lang="ru" dir="ltr">
<!-- (c) студия Wezom | www.wezom.com.ua -->
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="robots" content="noindex, nofollow">
    <title><?php echo __('Техобслуживание') ?></title>
</head>
    <body>
        <style>
            body { box-sizing: border-box; margin: 0; line-height: 1.5;}

            h1 { font-size: 35px; }
            body { font: 20px Helvetica, sans-serif; color: #333; }

            a { color: #dc8100; text-decoration: none; }
            a:hover { color: #333; text-decoration: none; }

            .wrapper {
                max-width: 600px;
                text-align: center;
                margin-left: auto;
                margin-right: auto;
                height: 100vh;
                position: relative;
            }
            .noty {
                margin: 0;
                position: absolute;
                top: 50%;
                left: 50%;
                margin-right: -50%;
                transform: translate(-50%, -50%)
            }
            @media screen and (max-width: 500px){
                body { font: 14px Helvetica, sans-serif; color: #333; padding: 0 15px}
                h1 { font-size: 20px; }
            }
        </style>
        <div class="wrapper">
            <div class="noty">
                <h1><?php echo __('Мы скоро вернёмся!') ?></h1>
                <div>
                    <p><?php echo __('Приносим извинения за неудобства, но в настоящее время мы проводим техническое обслуживание. Вы всегда можете  <a href="mailto:of@babykroha.ua">связаться с нами</a> или связаться с <a href="mailto:help@babykroha.ua">технической поддержкой</a>. Сайт вернётся онлайн в ближайшее время!') ?></p>
                    <p>&mdash; babykroha.ua</p>
                </div>
            </div>
        </div>
    </body>
</html>
