<?php

use Core\HTML;
use Core\Support;
use Core\User;

$price = 0;
$sell_size = $_SESSION['sell_size'];
?>
<div class="cart-window__content">
    <div class="cart-window__top">
        <?php if (!empty($cart)): ?>
            <div class="cart-window__title"><?php echo __('Сформировать заказ') ?></div>
            <span class="cart__clean js-cart-clean"
                  data-url="<?php echo HTML::link('ajax/cartClear') ?>"><?php echo __('Сбросить все') ?></span>
            <div class="cart-item__remove-confirm js-remove-confirm">
                <p class="cart-item__confirm-title"><?php echo __('Удалить') ?>?</p>
                <div>
                    <button class="yellow-border-btn cart-item__confirm-btn js-confirm"><?php echo __('Да') ?>
                    </button>
                    <button class="yellow-btn cart-item__confirm-btn js-unconfirm"><?php echo __('Нет') ?>
                    </button>
                </div>
            </div>
        <?php else: ?>
            <div class="cart-window__title"><?php echo __('Ваша корзина пуста') ?></div>
        <?php endif; ?>
    </div>
    <?php if (!empty($cart)): ?>
        <div class="cart-window__items-wrap js-init" data-scrollbar>

            <?php foreach ($cart as $item):
                $obj = $item['obj'];

                if ($obj->sizes):
                    $counts = [];
                    $count = 0;

                    foreach ($obj->sizes as $size):
                        $counts[$size->size] = $size->count;
                        $count += $size->count;
                    endforeach;
                else:
                    $counts = [$item['count']];
                    $count = $item['count'];
                endif;
                $data = Support::calculatePriceWithCurrency($obj,$_currency,$_currentCurrency);
                $cost = $data['cost'];
                $cost_old = $data['cost_old'];
                $currency = $data['currency'];
                $price += $cost * $count;
                $wholeSale = $sell_size && $obj->sell_size ? 1 : 0;
                ?>
                <div class="cart-item__wrap">
                    <div class="cart-item js-cart-item" data-id="<?php echo $obj->id ?>">
                        <div class="cart-item__inner">
                            <div class="cart-item__img-box">
                                <div class="cart-item__img-wrap">
                                    <a href="<?php echo HTML::link($obj->alias); ?>">
                                        <?php if (is_file(HOST . Core\HTML::media('images/catalog/original/'.$obj->image, false))): ?>
                                            <img class="cart-item__img lozad"
                                                 src="<?php echo HTML::media('images/catalog/original/' . $obj->image); ?>"
                                                 alt="<?php echo $obj->name; ?>">
                                        <?php else: ?>
                                            <img class="cart-item__img lozad"
                                                 src="<?php echo HTML::media('assets/images/placeholders/no-image.jpg'); ?>"
                                                 alt="<?php echo $obj->name; ?>">
                                        <?php endif; ?>
                                    </a>
                                </div>
                            </div>
                            <div class="cart-item__info-box">
                                <div class="cart-item__info-item cart-item__info-item--first ">
                                    <p class="cart-item__code-box"><?php echo __('Код товара') ?>:
                                        <span class="cart-item__code"><?php echo $obj->id ?></span>
                                    </p>
                                    <p class="cart-item__name"
                                       title="<?php echo $obj->name ?>"><?php echo \Core\Text::limit_words2($obj->name, 3); ?></p>
                                    <?php if ($obj->color): ?>
                                        <div class="cart-item__color-box">
                                            <span><?php echo __('Цвет') ?>:</span>
                                            <span class="cart-item__color"
                                                  style="background-color: <?php echo $obj->color ?>"
                                                  title="<?php echo $obj->color_name; ?>"></span>
                                        </div>
                                    <?php endif; ?>

                                </div>

                                <div class="cart-item__info-item cart-item__info-item--second">
                                    <span><?php echo __('Цена за шт') ?>:</span>
                                    <p class="cart-item__price">
                                        <span><?php echo $cost ?></span>
                                        <span> <?php echo $currency; ?></span>
                                    </p>
                                </div>
                                <div class="cart-item__info-item cart-item__info-item--second _ms-hide">
                                    <span><?php echo __('Количество') ?>:</span>
                                    <p class="cart-item__count"><?php echo $count; ?></p>
                                </div>
                                <div class="cart-item__info-item cart-item__info-item--third  ">
                                    <span><?php echo __('Сумма') ?>:</span>
                                    <p class="cart-item__price">
                                        <span><?php echo $cost * $count ?></span>
                                        <span> <?php echo $currency; ?></span>
                                    </p>
                                </div>

                                <div class="cart-item__info-item cart-item__info-item--fourth">

									<span class="cart-item__redact js-init" data-mfp="ajax"
                                          data-mfp-src="<?php echo \Core\HTML::link('popup/getPopup'); ?>"
                                        <?php if ($obj->sizes): ?>
                                          data-param='<?php echo json_encode(['name' => 'CartSizes', 'param' => ['item_id' => $obj->id,'wholeSale' => $wholeSale, 'counts' => $counts, 'from' => 2]]); ?>'>
											<?php else: ?>
                                                data-param='<?php echo json_encode(['name' => 'CartWithoutSizes', 'param' => ['item_id' => $obj->id,'wholeSale' => $wholeSale, 'count' => $count, 'from' => 1]]); ?>'>
                                            <?php endif; ?>
                                        <svg>
											<use xlink:href="<?php echo HTML::media('assets/images/sprites/icons.svg#pencil-icon'); ?>"></use>
										</svg>
									</span>

                                    <div class="cart-item__redact">
										<span class="js-cart-remove"
                                              data-url="<?php echo HTML::link('ajax/deleteItemFromCartPopup') ?>"
                                              data-id="<?php echo $obj->id ?>">
											<svg>
												<use xlink:href="<?php echo HTML::media('assets/images/sprites/icons.svg#garbage-icon'); ?>">"></use>
											</svg>
										</span>
                                        <div class="cart-item__remove-confirm js-remove-confirm">
                                            <p class="cart-item__confirm-title"><?php echo __('Удалить') ?>?</p>
                                            <div>
                                                <button class="yellow-border-btn cart-item__confirm-btn js-confirm"><?php echo __('Да') ?>
                                                </button>
                                                <button class="yellow-btn cart-item__confirm-btn js-unconfirm"><?php echo __('Нет') ?>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <?php if ($obj->sizes): ?>
                            <div class="grid grid--space-def _flex-nowrap cart-item__size-box--order">
                                <div class="gcell gcell--24 _flex-grow">
                                    <form class="js-form js-init" data-ajax="<?php echo \Core\HTML::link('ajax/cartSizes'); ?>">
                                        <input type="hidden" data-name="catalog_id" value="<?php echo $obj->id ?>">
                                        <input type="hidden" data-name="from" value="2">
                                        <?php foreach ($obj->sizes as $size): ?>
                                            <div class="cart-item__size-item" data-id="<?php echo $obj->id ?>">
                                                <div class="size__item">
                                                    <span class="cart-item__redact js-init" data-mfp="ajax"
                                                          data-mfp-src="<?php echo \Core\HTML::link('popup/getPopup'); ?>"
                                                        <?php if ($obj->sizes): ?>
                                                          data-param='<?php echo json_encode(['name' => 'CartSizes', 'param' => ['item_id' => $obj->id,'wholeSale' => $wholeSale, 'counts' => $counts, 'from' => 2]]); ?>'>
                                                        <?php else: ?>
                                                            data-param='<?php echo json_encode(['name' => 'CartWithoutSizes', 'param' => ['item_id' => $obj->id,'wholeSale' => $wholeSale, 'count' => $count, 'from' => 1]]); ?>'>
                                                        <?php endif; ?>
                                                        <?php echo $size->size ?>
                                                    </span>
                                                    <span class="size__count js-product-count"><?php echo $size->count ?></span>
                                                </div>
                                                <div class="js-product-item">
                                                    <div class="sizes-item__counter-wrap sizes-item__counter--row">
                                                        <div class="sizes-item__counter-btn js-product-minus js-form-trigger">-</div>
                                                        <input class="sizes-item__counter-input js-product-input js-form-trigger"
                                                               type="tel"
                                                               name="size-count-<?php echo $size->size; ?>"
                                                               value="<?= $size->count ?>"
                                                               data-name="count[<?php echo $size->size; ?>]"
                                                               data-max="<?= $size->amount ?>">
                                                        <div class="sizes-item__counter-btn js-product-plus js-form-trigger">+</div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endforeach; ?>
                                        <?php echo \Core\HTML::csrfField();?>
                                    </form>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endforeach; ?>
            <?php if ($sizes==1 and $sizes[0]->size == '-'): ?>
                <div class="maximum-noty" style="display: none">
                    <?php echo __('Выбрано максимальное количество штук.') ?>
                </div>
            <?php else: ?>
                <div class="maximum-noty" style="display: none">
                    <?php echo __('Выбрано максимальное количество по данному размеру.') ?>
                </div>
            <?php endif; ?>

        </div>
    <?php endif; ?>
</div>
<div class="cart-window__bottom">
    <div class="cart-window__totals">
        <?php if (!empty($cart)): ?>
            <div>
                <?php echo __('Всего товаров') ?>:
                <span class="cart-window__total-count"><?php echo $totalCount; ?></span>
            </div>
            <div>
                <?php echo __('Общая сумма без учета доставки') ?>:
                <span class="cart-window__total"><?php echo $price; ?>
                    <span class="cart-window__currency"> <?php echo $currency; ?></span>
			</span>
            </div>
        <?php else: ?>
            <div><?php echo __('В Вашей корзине пока нет товаров для покупки. Ни в чем себе не отказывайте!') ?></div>
        <?php endif; ?>
    </div>
    <div class="cart__main-buttons">
        <button class="cart__back-btn cart__back-btn--full js-cart-close"><?php echo __('Продолжить покупки') ?></button>
        <?php if (!empty($cart)): ?>
            <a href="<?php echo HTML::link('cart') ?>" class="cart__order-btn"><?php echo __('Сформировать заказ') ?></a>
        <?php endif; ?>
    </div>
</div>