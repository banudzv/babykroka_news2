<?php

use Core\Cookie;
use Modules\Catalog\Models\Filter;
use Core\HTML;
use Core\Route;
use Core\Widgets;

$active = Route::param('group');
$noSizes = false;

$s = [];
foreach ($sizes as $size) {
    if ($size->size !== '-') {
        $s[] = $size;
    }
}
$sizes = $s;

if (!count($sizes)) {
    $noSizes = true;
}
$currency = Cookie::getWithoutSalt('currency');
?>
<div class="gcell--def-6 gcell--lg-4 catalog__filters">

    <div class="catalog-filters__wrap">
        <div class="filters__search">
            <form class="js-form filters__search-form js-init"
                  data-ajax="<?php echo HTML::link('ajax/searchFilter') ?>">
                    <span class="filters__search-icon">
                        <svg>
                            <use xlink:href="/Media/assets/images/sprites/icons.svg?v=1532332938000#search-icon"></use>
                        </svg>
                    </span>
                <input type="text" data-name="search" class="simple-input filters__search-input"
                       placeholder="<?php echo __('Поиск') ?>">
<!--                <div class="filters__search-selection select2-wrap">-->
<!--                    <select id="" class="select2">-->
<!--                        <option value="">Цвет</option>-->
<!--                        <option value="">Бренд</option>-->
<!--                        <option value="">Производитель</option>-->
<!--                        <option value="">Материал</option>-->
<!--                    </select>-->
<!--                </div>-->
            </form>
        </div>

        <?php if (isset($groups) && !empty($groups)): ?>

                <div class="filters__items filters__categories _mb-def">
                    <?php foreach ($groups[$group_id] as $obj): ?>
                        <div class="filters__item">
                            <div class="filters__item-top filters__categories-top">
                                <div>
                                    <a <?php echo ($active == $obj->id) ? '' : 'href="' . HTML::link('catalog/' . $obj->alias) . '"'; ?>
                                            class="filters__item-name <?php echo ($active == $obj->id) ? 'is-active' : ''; ?>"><?php echo $obj->name; ?></a>
                                </div>
                                <?php if (isset($groups[$obj->id])): ?>
                                    <button class="filters__toggle"></button>
                                <?php endif; ?>
                            </div>
                            <?php if (isset($groups[$obj->id])): ?>
                                <div class="filters__child-wrap" style="display: none">
                                    <?php foreach ($groups[$obj->id] as $item): ?>
                                        <a <?php echo ($active == $item->id) ? '' : 'href="' . HTML::link('catalog/' . $item->alias) . '"'; ?>
                                                class="filters__child-name <?php echo ($active == $item->id) ? 'is-active' : ''; ?>"><?php echo $item->name; ?></a>
                                    <?php endforeach; ?>
                                </div>
                            <?php endif; ?>
                        </div>
                    <?php endforeach; ?>
                </div>

        <?php endif; ?>


        <div class="filters__items desktop main-filter-wrapper" data-category="<?= $active ?>">
            <?php echo Widgets::get('Groups_CatalogMenuSelected'); ?>
            <?php if (count($colors)): ?>
                <div class="filters__item">
                    <div class="filters__item-top">
                        <div>
                            <span class="filters__item-name"><?php echo __('Цвет') ?></span>
                            <span class="filters__item-chosen"><?php echo($currentColor ? $currentColor : 'Любой'); ?></span>
                        </div>
                        <button class="filters__toggle"></button>
                    </div>
                    <div class="filters__child-wrap" style="display: none">
                        <?php foreach ($colors as $obj): ?>
                            <?php echo Filter::generateInput($filter, $obj, 'color', 'color'); ?>
                        <?php endforeach; ?>
                    </div>
                </div>
            <?php endif; ?>
            <?php if (count($brands)): ?>
                <div class="filters__item">
                    <div class="filters__item-top">
                        <div>
                            <span class="filters__item-name"><?php echo __('Бренд') ?></span>
                            <span class="filters__item-chosen"><?php echo($currentBrand ? $currentBrand : "Любой"); ?></span>
                        </div>
                        <button class="filters__toggle"></button>
                    </div>
                    <div class="filters__child-wrap" style="display: none">
                        <?php foreach ($brands as $obj): ?>
                            <?php echo Filter::generateInput($filter, $obj, 'brand', 'brand'); ?>
                        <?php endforeach; ?>
                    </div>
                </div>
            <?php endif; ?>
            <?php if (count($manufacturer)): ?>
            <div class="filters__item">
                <div class="filters__item-top">
                    <div>
                        <span class="filters__item-name"><?php echo __('Производитель') ?></span>
                        <span class="filters__item-chosen"><?php echo($currentManufacturer ? $currentManufacturer : "Любой"); ?></span>
                    </div>
                    <button class="filters__toggle"></button>
                </div>
                <div class="filters__child-wrap" style="display: none">
                    <?php foreach ($manufacturer as $obj): ?>
                        <?php echo Filter::generateInput($filter, $obj, 'manufacturer', 'manufacturer'); ?>
                    <?php endforeach; ?>
                </div>
            </div>
            <?php endif; ?>
            <?php if (!empty($ages)): ?>
                <div class="filters__item">
                    <div class="filters__item-top">
                        <div>
                            <span class="filters__item-name"><?php echo __('Возраст') ?></span>
                            <span class="filters__item-chosen"><?php echo($currentAges ? $currentAges : "Любой"); ?></span>
                        </div>
                        <button class="filters__toggle"></button>
                    </div>
                    <div class="filters__child-wrap" style="display: none">
                        <?php foreach ($ages as $obj): ?>
                            <?php echo Filter::generateInput($filter, $obj, 'age', 'age'); ?>
                        <?php endforeach; ?>
                    </div>
                </div>
            <?php endif; ?>
            <?php foreach ($specifications['list'] as $specification_alias => $specification_name): ?>
                <?php if (isset($specifications['values'][$specification_alias]) and count($specifications['values'][$specification_alias])): ?>
                    <?php $spec = end($specifications['values'][$specification_alias]) ?>
                    <div class="filters__item">
                        <div class="filters__item-top">
                            <div>
                                <span class="filters__item-name"><?php echo $spec->specification_name; ?></span>
                                <span class="filters__item-chosen"><?php echo __('Любой') ?></span>
                            </div>
                            <button class="filters__toggle"></button>
                        </div>
                        <div class="filters__child-wrap" style="display: none">
                            <?php foreach ($specifications['values'][$specification_alias] as $obj): ?>
                                <?php echo Filter::generateInput($filter, $obj, $obj->specification_alias, $obj->specification_alias); ?>
                            <?php endforeach ?>
                        </div>
                    </div>
                <?php endif ?>
            <?php endforeach ?>
            <?php if (!$noSizes): ?>
                <div class="filters__item">
                    <div class="filters__item-top">
                        <div>
                            <span class="filters__item-name"><?php echo __('Размер') ?></span>
                            <span class="filters__item-chosen"><?php echo __('Любой') ?></span>
                        </div>
                        <button class="filters__toggle"></button>
                    </div>
                    <div class="filters__child-wrap" style="display: none">
                        <div class="filters__child-sizes">
                            <?php foreach ($sizes as $key => $obj): ?>
                                <?php echo Filter::generateInput($filter, $obj, 'size', 'size'); ?>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
            <?php if ($discounts): ?>
                <div class="filters__item">
                    <div class="filters__item-top">
                        <div>
                            <span class="filters__item-name"><?php echo __('Скидки') ?></span>
                        </div>
                        <button class="filters__toggle"></button>
                    </div>
                    <div class="filters__child-wrap" style="display: none">
                        <?php echo Filter::generateInput($filter, null, 'discounts', 'discounts'); ?>
                    </div>
                </div>
            <?php endif; ?>
            <div class="filters__item">
                <div class="filters__item-top">
                    <div>
                        <span class="filters__item-name <?php echo ($mincost || $maxcost ? 'is-active' : ''); ?>">
                           <?php echo __('Диапазон цен') ?>
                        </span>
                    </div>
                    <button class="filters__toggle"></button>
                </div>
                <div class="filters__child-wrap" style="display: none">
                    <div class="filters__price-box">
                        <form class="filters__price-form js-form js-init" data-ajax="<?php echo HTML::link('ajax/cost'); ?>">
                            <div class="filters__price-group">
                                <div class="filters__price-wrap">
                                    <div class="filters__price-text">
                                        <input type="text" name="min-cost" data-name="mincost" id="min-cost" data-min="<?= floor($min) ?>"
                                               class="filters__price-input min-cost" placeholder="От" value="<?= $mincost ? floor($mincost) : floor($min) ?>">
                                        <span class="filters__price-currency"><?= $currency ?></span>
                                    </div>
                                    <span class="filters__price-divider">-</span>
                                    <div class="filters__price-text">
                                    <input type="text" name="max-cost" data-name="maxcost" id="max-cost" data-max="<?= ceil($max) ?>"
                                           class="filters__price-input max-cost" placeholder="До" value="<?= $maxcost ? ceil($maxcost) : ceil($max) ?>">
                                        <span class="filters__price-currency"><?= $currency ?></span>
                                    </div>
                                </div>
                                <div class="price-slider__wrap">
                                    <div class="price-slider"
                                         data-min="<?= floor($min) ?>"
                                         data-max="<?= ceil($max) ?>"
                                         data-start="<?= $mincost ? floor($mincost) : floor($min) ?>"
                                         data-end="<?= $maxcost ? ceil($maxcost) : ceil($max) ?>"
                                    ></div>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
