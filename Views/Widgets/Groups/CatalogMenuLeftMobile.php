<?php

use Core\Cookie;
use Core\HTML;
use Core\Route;
use Modules\Catalog\Models\Filter;
use Core\Widgets;

$active = Route::param('group');
$noSizes = false;

$s = [];
foreach ($sizes as $size) {
    if ($size->size !== '-') {
        $s[] = $size;
    }
}
$sizes = $s;

if (!count($sizes)) {
    $noSizes = true;
}
$currency = Cookie::getWithoutSalt('currency');
?>

<ul class="catalog__filter--mobile mobile__filter-items main-filter-wrapper" data-category="<?= $active ?>">
    <?php echo Widgets::get('Groups_CatalogMenuSelected'); ?>
    <?php if (count($colors)): ?>
        <li class="catalog-filters__item filters__item">
        <span class="filters__item-top">
            <div>
                <span class="filters__item-name"><?php echo __('Цвет') ?></span>
                <span class="filters__item-chosen"><?php echo($currentColor ? $currentColor : 'Любой'); ?></span>
            </div>
            <button class="filters__toggle"></button>
        </span>
            <span class="filters__child-wrap" style="display: none;">
           <?php foreach ($colors as $obj): ?>
               <?php echo Filter::generateInput($filter, $obj, 'color', 'color'); ?>
           <?php endforeach; ?>
        </span>
        </li>
    <?php endif; ?>
    <?php if (count($brands)): ?>
        <li class="catalog-filters__item filters__item">
        <span class="filters__item-top">
            <div>
                <span class="filters__item-name"><?php echo __('Производитель') ?></span>
                <span class="filters__item-chosen"><?php echo($currentBrand ? $currentBrand : __('Любой')); ?></span>
            </div>
            <button class="filters__toggle"></button>
        </span>
            <span class="filters__child-wrap" style="display: none;">
           <?php foreach ($brands as $obj): ?>
               <?php echo Filter::generateInput($filter, $obj, 'brand', 'brand'); ?>
           <?php endforeach; ?>
        </span>
        </li>
    <?php endif; ?>
    <?php if (count($ages)): ?>
        <li class="catalog-filters__item filters__item">
            <span class="filters__item-top">
                <div>
                    <span class="filters__item-name"><?php echo __('Возраст') ?></span>
                    <span class="filters__item-chosen"><?php echo($currentAges ? $currentAges :  __('Любой')); ?></span>
                </div>
                <button class="filters__toggle"></button>
            </span>
                <span class="filters__child-wrap" style="display: none;">
               <?php foreach ($ages as $obj): ?>
                   <?php echo Filter::generateInput($filter, $obj, 'age', 'age'); ?>
               <?php endforeach; ?>
            </span>
        </li>
    <?php endif; ?>
    <?php foreach ($specifications['list'] as $specification_alias => $specification_name): ?>
        <?php if (isset($specifications['values'][$specification_alias]) and count($specifications['values'][$specification_alias])): ?>
            <?php $spec = end($specifications['values'][$specification_alias]) ?>
            <li class="catalog-filters__item filters__item">
				<span class="filters__item-top">
					<div>
						<span class="filters__item-name"><?php echo $spec->specification_name; ?></span>
						<span class="filters__item-chosen"><?php echo __('Любой') ?></span>
					</div>
					<button class="filters__toggle"></button>
				</span>
                <span class="filters__child-wrap" style="display: none;">
					<?php foreach ($specifications['values'][$specification_alias] as $obj): ?>
                        <?php echo Filter::generateInput($filter, $obj, $obj->specification_alias, $obj->specification_alias); ?>
                    <?php endforeach ?>
				</span>
            </li>
        <?php endif ?>
    <?php endforeach ?>
    <?php if (!$noSizes): ?>
        <li class="catalog-filters__item filters__item">
            <span class="filters__item-top">
                <div>
                    <span class="filters__item-name"><?php echo __('Размер') ?></span>
                    <span class="filters__item-chosen"><?php echo __('Любой') ?></span>
                </div>
                <button class="filters__toggle"></button>
            </span>
            <span class="filters__child-wrap" style="display: none;">
                <?php foreach ($sizes as $key => $obj): ?>
                    <?php echo Filter::generateInput($filter, $obj, 'size', 'size'); ?>
                <?php endforeach; ?>
            </span>
        </li>
    <?php endif; ?>
    <?php if ($discounts): ?>
        <div class="filters__item">
            <div class="filters__item-top">
                <div>
                    <span class="filters__item-name"><?php echo __('Скидки') ?></span>
                </div>
                <button class="filters__toggle"></button>
            </div>
            <div class="filters__child-wrap" style="display: none">
                <?php echo Filter::generateInput($filter, null, 'discounts', 'discounts'); ?>
            </div>
        </div>
    <?php endif; ?>
    <li class="catalog-filters__item filters__item">
        <span class="filters__item-top">
            <div>
                <span class="filters__item-name <?php echo ($mincost || $maxcost ? 'is-active' : ''); ?>">
                   <?php echo __('Диапазон цен') ?>
                </span>
            </div>
            <button class="filters__toggle"></button>
        </span>
        <span class="filters__child-wrap" style="display: none">
            <div class="filters__price-box">
                <form class="filters__price-form js-form js-init" data-ajax="<?php echo HTML::link('ajax/cost'); ?>">
                    <div class="filters__price-group">
                        <div class="filters__price-wrap">
                            <div class="filters__price-text">
                                <input type="text" name="min-cost" data-name="mincost" id="min-cost" data-min="<?= floor($min) ?>"
                                       class="filters__price-input min-cost" placeholder="От" value="<?= $mincost ? floor($mincost) : floor($min) ?>">
                                    <span class="filters__price-currency"><?= $currency ?></span>
                                </div>
                                <span class="filters__price-divider">-</span>
                                <div class="filters__price-text">
                                    <input type="text" name="max-cost" data-name="maxcost" id="max-cost" data-max="<?= ceil($max) ?>"
                                        class="filters__price-input max-cost" placeholder="До" value="<?= $maxcost ? ceil($maxcost) : ceil($max) ?>">
                                <span class="filters__price-currency"><?= $currency ?></span>
                            </div>
                        </div>
                        <div class="price-slider__wrap">
                            <div class="price-slider"
                                 data-min="<?= $min ?>"
                                 data-max="<?= $max ?>"
                                 data-start="<?= $mincost ? floor($mincost) : floor($min) ?>"
                                 data-end="<?= $maxcost ? ceil($maxcost) : ceil($max) ?>"
                            ></div>
                        </div>
                    </div>
                </form>
            </div>
        </span>
    </li>
</ul>
