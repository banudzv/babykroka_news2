<?php

use Core\HTML;

?>
<div class="index-seo-text">
    <div class="container grid drops__info">
        <div class="gcell--24 gcell--md-12 drops__info-text">
            <div class="text-wrapper">
                <div class="drops__info-title"><?= $information->name ?></div>
                <div class="drops__info-details"><?= $information->description ?></div>

                <div class="drops__info-community">
                    <div class="drops__info-members">
                        <div class="drops__quantity">8431</div>
                        <div class="drops__description"><?= __('участников за всё время') ?></div>
                    </div>
                    <div class="drops__info-rating">
                        <div class="drops__quantity">98%</div>
                        <div class="drops__description"><?= __('положительных отзывов') ?></div>
                    </div>
                </div>
                <div class="drops__application">
                    <button class="drops__application-btn"><?= __('Заявка на сотрудничество') ?></button>
                    <a href="<?= HTML::link('wholesale') ?>" class="drops__application-details"><?= __('Подробнее') ?></a>
                </div>
            </div>
        </div>
        <div class="gcell--24 gcell--md-12 index-seo-text__right">
            <div class="index-seo-text__image">
                <img src="/Media/assets/images/Layer-2.png?v=1523945764000" alt class="lozad drops__info-image">
            </div>
        </div>
    </div>
</div>