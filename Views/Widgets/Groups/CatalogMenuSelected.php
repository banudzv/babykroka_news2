<?php
use Modules\Catalog\Models\Filter;
use Core\HTML;
use Core\Route;
?>
<div class="filtration__items-scope">
    <span class="filtration__item-name"><?php echo __('Фильтрация') ?></span>
    <?php if (!empty($selected)): ?>
        <a href="<?php echo HTML::link('catalog/' . Route::param('alias'));?>" class="catalog__chosen-clear"><?php echo __('Сбросить все'); ?></a>
    <?php endif; ?>
</div>
<?php if (!empty($selected)): ?>
	<div class="catalog__filters-chosen">
		<?php foreach ($selected as $alias => $select): ?>
				<?php if (is_array($select)): ?>
					<?php foreach ($select as $obj): ?>
						<div class="catalog__chosen-item">
							<span><?php  echo $obj->name ? $obj->name : $obj; ?></span>
							<a href="<?php echo Filter::generateLinkWithFilter($alias, $obj->alias ? $obj->alias : $obj); ?>">
								<button class="catalog__chosen-delete">
									<svg>
										<use xlink:href="<?php echo HTML::media('assets/images/sprites/icons.svg#close-icon-fat');?>"></use>
									</svg>
								</button>
							</a>
						</div>
					<?php endforeach; ?>
					<?php else: ?>
						<div class="catalog__chosen-item">
							<span><?php echo $select->name ? $select->name : $select; ?></span>
							<a href="<?php echo Filter::generateLinkWithFilter($alias, $select->alias ? $select->alias : $select); ?>">
								<button class="catalog__chosen-delete">
									<svg>
                                        <use xlink:href="<?php echo HTML::media('assets/images/sprites/icons.svg#close-icon-fat');?>"></use>
									</svg>
								</button>
							</a>
						</div>
					<?php endif; ?>
		<?php endforeach; ?>

	</div>
<?php endif;?>
