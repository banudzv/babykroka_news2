<?php

use Core\HTML;
use Core\Support;
use Core\User;

?>
<?php if (!empty($result)): ?>
    <div class="product-slider__wrap small">
        <div class="product-slider__title-box container">
            <p class="product-slider__title main-title"><?php echo __('С ЭТИМ ТОВАРОМ ЧАСТО ПОКУПАЮТ') ?></p>
        </div>
        <div class="product-slider__controls product-slider__related-controls">
            <div class="product-slider__buttons-box product-slider__related-buttons"></div>
        </div>
        <div class="product-slider small">
            <?php foreach ($result as $obj):
                $data = Support::calculatePriceWithCurrency($obj,$_currency,$_currentCurrency);
                $cost = $data['cost'];
                $cost_old = $data['cost_old'];
                $currency = $data['currency'];
                ?>

                <div class="product-item__wrap gcell--24 gcell--sm-12 gcell--ms-8">
                    <div class="product-item js-cart-item" data-id="<?php echo $obj->id; ?>"
                         data-url="<?php echo HTML::link('ajax/addToCart'); ?>">
                        <div class="product-item__img-box">
                            <?php if (User::info()): ?>
                            <button class="product-item__favorite js-favorite-btn <?php echo (in_array($obj->id, $favorite)) ? 'active' : ''; ?>"
                                    data-url="<?php echo HTML::link('ajax/addToFavorite'); ?>">
                                <?php else: ?>
                                <button class="product-item__favorite js-init"
                                        data-mfp="ajax" data-mfp-src="<?php echo HTML::link('popup/getPopup'); ?>"
                                        data-param='<?php echo json_encode(['name' => 'Login', 'param' => []]); ?>'>
                                    <?php endif; ?>
                                    <span class="product-item__favorite-icon">
                                    <svg>
                                        <use xlink:href="<?php echo HTML::media('assets/images/sprites/icons.svg#favorite-icon'); ?>"></use>
                                    </svg>
                                </span>
                                    <span class="product-item__favorite-icon--fill">
                                    <svg>
                                        <use xlink:href="<?php echo HTML::media('assets/images/sprites/icons.svg#like-icon'); ?>"></use>
                                    </svg>
                                </span>
                                </button>
                                <div class="product-item__img-wrap">
                                    <a href="<?php echo HTML::link($obj->alias) ?>">
                                        <?php if (is_file(HOST . Core\HTML::media('images/catalog/original/'.$obj->image, false))): ?>
                                            <img class="product-item__img lozad"
                                                 src="<?php echo HTML::media('images/catalog/original/' . $obj->image); ?>"
                                                 alt="<?php echo $obj->name; ?>">
                                        <?php else: ?>
                                            <img class="product-item__img lozad"
                                                 src="<?php echo HTML::media('assets/images/placeholders/no-image.jpg'); ?>"
                                                 alt="<?php echo $obj->name; ?>">
                                        <?php endif; ?>
                                    </a>
                                </div>
                        </div>
                        <div class="product-item__text-box">
                            <p class="product-item__name">
                                <a href="<?php echo HTML::link($obj->alias) ?>"
                                   title="<?php echo $obj->name; ?>">
                                    <?php echo \Core\Text::limit_words2($obj->name, 5) ?>
                                </a>
                            </p>
                            <a href="<?php echo HTML::link('catalog/' . $obj->cat_alias) ?>"
                               class="product-item__category"><?php echo $obj->cat_name; ?></a>
                            <div class="product-item__price-box">
                                <span class="product-item__price">
                                    <span><?= $currency == 'USD' ? $cost : ceil($cost) ?></span>
                                    <span class="product-item__price-currency"> <?php echo $currency; ?></span>
                                </span>
                            </div>
                            <button class="yellow-btn product-item__btn js-cart-add"
                                    data-url="<?php echo HTML::link('ajax/addToCart') ?>"
                                    data-id="<?php echo $obj->id; ?>">
                                <?php echo __('Купить') ?>
                            </button>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>

        </div>
    </div>
<?php endif; ?>
