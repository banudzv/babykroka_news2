<?php
use Core\HTML;
$data_ulogin = 'data-ulogin="display=buttons;fields=first_name,last_name,middle_name,email,phone;redirect_uri=https%3A%2F%2F' . $_SERVER['HTTP_HOST'] . '%2Faccount%2Flogin-by-social-network"';
?>
<div class="popup login-popup" id="uLogin">
    <div class="grid">
        <div class="gcell--24 login-popup__left">
            <div class="login-popup__tab wstabs-block is-active" data-wstabs-ns="group-2" data-wstabs-block="1">
                <div class="login-popup__form-wrap">
                    <p class="login-popup__title"><?php echo __('АВТОРИЗАЦИЯ') ?></p>
                    <div class="login-popup__social-wrap">
                        <p class="login-popup__social-title"><?php echo __('Войти без регистрации через') ?>:</p>
                        <div id="ulogin" class="cart-enter__socials" <?php echo $data_ulogin ?>>
                            <a href="#" class="cart-enter__social-link" data-uloginbutton="google">
                                <svg>
                                    <use xlink:href="<?php echo \Core\HTML::media('assets/images/sprites/icons.svg#google-icon'); ?>"></use>
                                </svg>
                            </a>
                            <a href="#" class="cart-enter__social-link" data-uloginbutton="facebook">
                                <svg>
                                    <use xlink:href="<?php echo \Core\HTML::media('assets/images/sprites/icons.svg#fb-icon'); ?>"></use>
                                </svg>
                            </a>
                            <a href="#" class="cart-enter__social-link" data-uloginbutton="twitter">
                                <svg>
                                    <use xlink:href="<?php echo \Core\HTML::media('assets/images/sprites/icons.svg#twitter-icon'); ?>"></use>
                                </svg>
                            </a>
                        </div>
                    </div>
                    <p class="login-popup__sub-title"><?php echo __('Войти с помощью E-mail и пароля') ?></p>
                    <form class="login-form js-form js-init" data-ajax="<?php echo \Core\HTML::link('form/login');?>">
                        <label class="cart-registration__label">
                            <input type="email" required name="enter-mail" data-name="email" placeholder="<?php echo __('E-mail') ?>" class="cart-registration__input simple-input">
                        </label>
                        <label class="cart-registration__label">
                            <input type="password" required name="password" data-name="password" placeholder="<?php echo __('Пароль') ?>" class="cart-registration__input simple-input" data-rule-minlength="6">
                        </label>
                        <div class="login-form__controls-wrap">
                            <label class="login-form__checkbox-label">
                                <input type="checkbox" name="remember" data-name="remember" id="enter-remember" class="cart-registration__checkbox">
                                <span class="cart-registration__checkbox-text"><?php echo __('Восстановить пароль') ?></span>
                            </label>
                            <span class="wstabs-button login-form__toggle" data-wstabs-ns="group-2" data-wstabs-button="2"><?php echo __('Восстановить пароль') ?></span>
                        </div>
                        <?php echo \Core\HTML::csrfField();?>
                        <button class="login-popup__submit login-popup__submit--gray"><?php echo __('Войти') ?></button>
                    </form>

                    <div class="login-popup__registration-link js-init" data-mfp="ajax"
                         data-mfp-src="<?php echo \Core\HTML::link('popup/getPopup'); ?>"
                         data-param='<?php echo json_encode(['name' => 'Register', 'param' => []]); ?>'>
                        <span class="header-login__enter"><?php echo __('Ещё нет аккаунта?') ?> <b><?php echo __('Зарегистрироваться') ?></b></span>
                    </div>

                    <div class="cart-registration__callback-block">
                        <?php echo __('Нужна помощь? Мы Вам перезвоним!') ?>
                        <span class="cart-registration__callback js-init" data-mfp="ajax" data-mfp-src="<?php echo \Core\HTML::link('popup/getPopup'); ?>" data-param='<?php echo json_encode(['name' => 'Callback', 'param' => []]); ?>'><?php echo __('Перезвонить?') ?></span>
                    </div>
                </div>
            </div>
            <div class="login-popup__tab wstabs-block" data-wstabs-ns="group-2" data-wstabs-block="2">
                <p class="login-popup__title"><?php echo __('ВОССТАНОВЛЕНИЕ ПАРОЛЯ') ?></p>
                <p class="login-popup__password-title"><?php echo __('Вы можете выбрать способ восстановления') ?></p>
                <form class="js-form js-init" data-ajax="<?php echo \Core\HTML::link('form/forgot_password');?>">
                    <label class="cart-registration__label">
                        <input required type="email" name="mail" data-name="email" placeholder="<?php echo __('') ?>E-mail" class="cart-registration__input simple-input">
                    </label>
                    <p class="login-popup__chose-title"><?php echo __('или') ?></p>
                    <label class="cart-registration__label">
                        <input type="tel" id="forgot-tel" name="tel" placeholder="+38(***) ***-**-**" data-rule-phoneua="true" data-phonemask="+38(***) ***-**-**" data-phonemask-android="+38(***) ***-**-**" class="cart-registration__input simple-input js-init">
                        <span class="required-dot"></span>
                        <label class="has-error" for="forgot-tel" style="display: none"></label>
                    </label>
                    <span class="wstabs-button cart-info__toggle enter is-active" data-wstabs-ns="group-2" data-wstabs-button="1"><?php echo __('Войти с помощью логина и пароля') ?></span>
                    <?php echo \Core\HTML::csrfField();?>
                    <button class="login-popup__submit login-popup__submit--gray"><?php echo __('Отправить') ?></button>
                </form>
            </div>
        </div>
    </div>
</div>
