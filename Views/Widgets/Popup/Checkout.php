<div class="popup popup--medium">
    <div class="container">
        <div class="title title--small"><?php echo __('Заказать технику');?></div>
        <form class="form js-init" method="post" data-ajax="<?php echo \Core\HTML::link('form/order_simple');?>">
            <div class="form-group">
                <div class="form-controller">
                    <input type="text" name="checkout-name" data-name="name" data-rule-minlength="2" pattern="[A-Za-zА-Яа-яЁёґєії`´ʼ’ʼ’ \-]+" data-msg-pattern="<?php echo __('Введено некорректное имя');?>" class="form-input">
                    <label for="callback-name" class="form-label"><?php echo __('Ваше имя');?></label>
                </div>
                <label id="checkout-email-error" class="has-error" for="checkout-name" style="display: none;"></label>
            </div>
            <div class="form-group">
                <div class="form-controller">
                    <input type="tel" name="checkout-tel" data-name="phone" required pattern="\+([0-9]+)\(?([0-9]{3})\)?([ .-]?)([0-9]{3})(([ .-]?)([0-9]{2})){2}(\d+)?" data-msg-pattern="<?php echo __('Введите корректный телефонный номер');?>" class="form-input js-init">
                    <label for="checkout-tel" class="form-label"><?php echo __('Ваш телефон');?></label>
                </div>
                <label id="checkout-tel-error" class="has-error" for="callback-tel" style="display: none;"></label>
            </div>
            <div class="form-group">
                <div class="form-controller">
                    <input type="email" name="checkout-email" data-name="email" class="form-input js-init">
                    <label for="checkout-email" class="form-label"><?php echo __('Ваш email');?></label>
                </div>
                <label id="checkout-email-error" class="has-error" for="checkout-email" style="display: none;"></label>
            </div>
            <div class="form-group">
                <div class="form-controller">
                    <textarea name="checkout-comment" data-name="text" rows="8" class="form-textarea" data-rule-minlength="10"></textarea>
                    <label for="checkout-comment" class="form-label"><?php echo __('Ваш комментарий');?></label>
                </div>
                <label id="checkout-comment-error" class="has-error" for="checkout-comment" style="display: none;"></label>
            </div>
            <input type="hidden" name="" data-name="id" value="<?php echo $id;?>" id="">
            <?php echo Core\HTML::csrfField();?>
            <div class="form-group">
                <input type="submit" class="form-button form-button--submit button button--black-opacity popup__button--black" value="<?php echo __('Отправить');?>">
            </div>
        </form>
    </div>
</div>
