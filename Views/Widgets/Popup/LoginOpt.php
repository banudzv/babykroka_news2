<?php
use Core\HTML;
?>
<div class="popup login-popup">
	<div class="grid">
		<div class="gcell--24 gcell--ms-12 login-popup__left">
			<div class="login-popup__tab wstabs-block is-active" data-wstabs-ns="group-2" data-wstabs-block="1">
				<div class="login-popup__form-wrap">
					<p class="login-popup__title"><?php echo __('ВОЙТИ КАК ОПТОВИК / ДРОП') ?></p>
					<form class="login-form js-form js-init" data-ajax="<?php echo \Core\HTML::link('form/wholesaleLogin');?>">
						<label class="cart-registration__label">
							<input type="email" required name="email" data-name="email" placeholder="<?php echo __('E-mail') ?>" class="cart-registration__input simple-input">
						</label>
						<label class="cart-registration__label">
							<input type="password" required name="password" data-name="password" placeholder="<?php echo __('Пароль') ?>" class="cart-registration__input simple-input" data-rule-minlength="6">
						</label>
						<div class="login-form__controls-wrap">
							<label class="login-form__checkbox-label">
								<input type="checkbox" name="remember" data-name="remember" class="cart-registration__checkbox">
								<span class="cart-registration__checkbox-text"><?php echo __('Запомнить меня') ?></span>
							</label>
							<span class="wstabs-button login-form__toggle" data-wstabs-ns="group-2" data-wstabs-button="2"><?php echo __('Восстановить пароль') ?></span>
						</div>
						<?php echo \Core\HTML::csrfField();?>
						<button class="login-popup__submit login-popup__submit--gray"><?php echo __('Войти') ?></button>
					</form>
				</div>
			</div>
			<div class="login-popup__tab wstabs-block" data-wstabs-ns="group-2" data-wstabs-block="2">
				<p class="login-popup__title"><?php echo __('ВОССТАНОВЛЕНИЕ ПАРОЛЯ') ?></p>
				<p class="login-popup__password-title"><?php echo __('Вы можете выбрать способ восстановления') ?></p>
				<form class="js-form js-init" data-ajax="<?php echo \Core\HTML::link('form/forgot_password');?>">
					<label class="cart-registration__label">
						<input required type="email" name="mail" data-name="email" placeholder="<?php echo __('E-mail') ?>" class="cart-registration__input simple-input">
					</label>
					<p class="login-popup__chose-title"><?php echo __('или') ?></p>
					<label class="cart-registration__label">
						<input type="tel" id="opt-forgot-tel" name="tel" placeholder="+38(***) ***-**-**" data-rule-phoneua="true" data-phonemask="+38(***) ***-**-**" data-phonemask-android="+38(***) ***-**-**" class="cart-registration__input simple-input js-init">
						<span class="required-dot"></span>
						<label class="has-error" for="opt-forgot-tel" style="display: none"></label>
					</label>
					<span class="wstabs-button cart-info__toggle enter is-active" data-wstabs-ns="group-2" data-wstabs-button="1"><?php echo __('Войти с помощью логина и пароля') ?></span>
					<?php echo \Core\HTML::csrfField();?>
					<button class="login-popup__submit login-popup__submit--gray"><?php echo __('Отправить') ?></button>
				</form>
			</div>
		</div>
		<div class="gcell--24 gcell--ms-12 login-popup__right">
			<p class="login-popup__title"><?php echo __('РЕГИСТРАЦИЯ ОПТОВИКА / ДРОПШИПЕРА') ?></p>
			<form class="registration-form js-form js-init" data-ajax="<?php echo \Core\HTML::link('form/wholesaleRegistration');?>">
				<div class="cart-registration__info">
					<label class="cart-registration__label">
						<input type="text" id="opt-reg-name" required name="name" data-name="name" placeholder="<?php echo __('Имя') ?>" class="cart-registration__input cart-registration__input--white simple-input" data-rule-word="true" data-rule-minlength="2">
						<span class="required-dot"></span>
						<label class="has-error" for="opt-reg-name" style="display: none"></label>
					</label>
					<label class="cart-registration__label">
						<input type="text" id="opt-reg-surname" required name="last_name" data-name="last_name" placeholder="<?php echo __('Фамилия') ?>" class="cart-registration__input cart-registration__input--white simple-input" data-rule-word="true" data-rule-minlength="2">
                        <span class="required-dot"></span>
						<label class="has-error" for="opt-reg-surname" style="display: none"></label>
					</label>
					<label class="cart-registration__label">
						<input type="text" id="opt-reg-middle_name"  name="middle_name" data-name="middle_name" placeholder="<?php echo __('Отчество') ?>" class="cart-registration__input cart-registration__input--white simple-input" data-rule-word="true" data-rule-minlength="2">
						<label class="has-error" for="opt-reg-middle_name" style="display: none"></label>
					</label>
					<label class="cart-registration__label">
						<input type="email" id="opt-reg-mail" name="email" data-name="email" required placeholder="<?php echo __('E-mail') ?>" class="cart-registration__input cart-registration__input--white simple-input">
						<span class="required-dot"></span>
						<label class="has-error" for="opt-reg-mail" style="display: none"></label>
					</label>
					<label class="cart-registration__label">
						<input type="tel" id="opt-reg-tel" name="tel" data-name="tel" placeholder="+38(***) ***-**-**" required data-rule-phoneua="true" data-phonemask="+38(***) ***-**-**" data-phonemask-android="+38(***) ***-**-**" class="cart-registration__input cart-registration__input--white simple-input js-init">
						<span class="required-dot"></span>
						<label class="has-error" for="opt-reg-tel" style="display: none"></label>
					</label>
					<label class="cart-registration__label">
						<input type="password" id="opt-reg-password" required name="reg-password" data-name="reg-password" placeholder="<?php echo __('Пароль ( мин. 6 символов)') ?>" data-rule-minlength="6" class="cart-registration__input cart-registration__input--white simple-input">
						<span class="required-dot"></span>
						<label class="has-error" for="opt-reg-password" style="display: none"></label>
					</label>
					<label class="cart-registration__label">
						<input type="password" id="opt-reg-password-repeat" required name="reg-password-repeat" placeholder="<?php echo __('Пароль еще раз ( мин. 6 символов)') ?>" data-rule-minlength="6" class="cart-registration__input cart-registration__input--white simple-input" data-rule-equalTo="[name='reg-password']">
						<span class="required-dot"></span>
						<label class="has-error" for="opt-reg-password-repeat" style="display: none"></label>
					</label>

                   <div class="cart-registration__status">
                       <label class="cart-registration__checkbox-label">
                           <input required type="checkbox" name="rozn" data-name="rozn" id="rozn" class="cart-registration__checkbox">
                           <span class="cart-registration__checkbox-text cart-registration__checkbox-text--block cart-registration__checkbox-text--white"><?php echo __('Розница') ?>
                       </label>

                       <label class="cart-registration__checkbox-label">
                           <input type="checkbox" name="opt" data-name="opt" id="opt" class="cart-registration__checkbox">
                           <span class="cart-registration__checkbox-text cart-registration__checkbox-text--block cart-registration__checkbox-text--white"><?php echo __('Опт') ?>
                       </label>

                       <label class="cart-registration__checkbox-label">
                           <input type="checkbox" name="drop" data-name="drop" id="drop" class="cart-registration__checkbox">
                           <span class="cart-registration__checkbox-text cart-registration__checkbox-text--block cart-registration__checkbox-text--white"><?php echo __('Дроп') ?>
                       </label>
                   </div>



					<label class="cart-registration__checkbox-label ">
						<input required type="checkbox" name="agree" data-name="agree" id="opt-reg-agree" class="cart-registration__checkbox">
						<span class="cart-registration__checkbox-text cart-registration__checkbox-text--block cart-registration__checkbox-text--white">
                            <?php echo __('Я согласен с условиями использования и на обработку персональных данных') ?>
					</label>
					<?php echo \Core\HTML::csrfField();?>
					<button class="login-popup__submit login-popup__submit--yellow"><?php echo __('Отправить заявку') ?></button>
				</div>
			</form>
		</div>
	</div>
</div>
