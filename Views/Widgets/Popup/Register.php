<?php
use Core\HTML;
$data_ulogin = 'data-ulogin="display=buttons;fields=first_name,last_name,middle_name,email,phone;redirect_uri=https%3A%2F%2F' . $_SERVER['HTTP_HOST'] . '%2Faccount%2Flogin-by-social-network"';
?>
<div class="popup login-popup" id="uLogin">
    <div class="grid">
        <div class="gcell--24 login-popup__right">
            <p class="login-popup__title"><?php echo __('НОВЫЙ ПОЛЬЗОВАТЕЛЬ') ?></p>
            <form class="registration-form js-form js-init" data-ajax="<?php echo \Core\HTML::link('form/registration');?>">
                <div class="cart-registration__info">
                    <label class="cart-registration__label">
                        <input type="text" id="reg-name" required name="reg-name" data-name="name" placeholder="<?php echo __('Имя') ?>" class="cart-registration__input cart-registration__input--white simple-input" data-rule-word="true" data-rule-minlength="2">
                        <span class="required-dot"></span>
                        <label class="has-error" for="reg-name" style="display: none"></label>
                    </label>
                    <label class="cart-registration__label">
                        <input type="text" name="reg-surname" required data-name="surname" placeholder="<?php echo __('Фамилия') ?>" class="cart-registration__input cart-registration__input--white simple-input" data-rule-word="true" data-rule-minlength="2">
                        <span class="required-dot"></span>
                        <label class="has-error" for="reg-surname" style="display: none"></label>
                    </label>
                    <label class="cart-registration__label">
                        <input type="text" name="reg-middle_name" data-name="middle_name" placeholder="<?php echo __('Отчество') ?>" class="cart-registration__input cart-registration__input--white simple-input" data-rule-word="true" data-rule-minlength="2">
                    </label>
                    <label class="cart-registration__label">
                        <input type="email" id="reg-mail" name="reg-mail" data-name="email" required placeholder="<?php echo __('E-mail') ?>" class="cart-registration__input cart-registration__input--white simple-input">
                        <span class="required-dot"></span>
                        <label class="has-error" for="reg-mail" style="display: none"></label>
                    </label>
                    <label class="cart-registration__label">
                        <input type="tel" id="tel" name="reg-tel" data-name="phone" placeholder="+38(***) ***-**-**" required data-rule-phoneua="true" data-phonemask="+38(***) ***-**-**" data-phonemask-android="+38(***) ***-**-**" class="cart-registration__input cart-registration__input--white simple-input js-init">
                        <span class="required-dot"></span>
                        <label class="has-error" for="reg-tel" style="display: none"></label>
                    </label>
                    <label class="cart-registration__label">
                        <input type="password" id="reg-password" required name="reg-password" data-name="password" placeholder="<?php echo __('Пароль ( мин. 6 символов)') ?>" class="cart-registration__input cart-registration__input--white simple-input" data-rule-minlength="6">
                        <span class="required-dot"></span>
                        <label class="has-error" for="reg-password" style="display: none"></label>
                    </label>
                    <label class="cart-registration__label">
                        <input type="password" id="reg-password-repeat" required name="reg-password-repeat" placeholder="<?php echo __('Пароль еще раз ( мин. 6 символов)') ?>" class="cart-registration__input cart-registration__input--white simple-input" data-rule-minlength="6" data-rule-equalTo="[name='reg-password']">
                        <span class="required-dot"></span>
                        <label class="has-error" for="reg-password-repeat" style="display: none"></label>
                    </label>

                    <div class="cart-registration__status">
                        <label class="cart-registration__checkbox-label">
                            <input type="checkbox" value="rozn" name="rozn" data-name="rozn" id="rozn" class="cart-registration__checkbox" required>
                            <span class="cart-registration__checkbox-text cart-registration__checkbox-text--block cart-registration__checkbox-text--white"><?php echo __('Розница') ?>
                        </label>

                        <label class="cart-registration__checkbox-label">
                            <input type="checkbox" value="opt" name="opt" data-name="opt" id="opt" class="cart-registration__checkbox">
                            <span class="cart-registration__checkbox-text cart-registration__checkbox-text--block cart-registration__checkbox-text--white"><?php echo __('Опт') ?>
                        </label>

                        <label class="cart-registration__checkbox-label">
                            <input type="checkbox" value="drop" name="drop" data-name="drop" id="drop" class="cart-registration__checkbox">
                            <span class="cart-registration__checkbox-text cart-registration__checkbox-text--block cart-registration__checkbox-text--white"><?php echo __('Дроп') ?>
                        </label>
                    </div>

                    <div class="rozn-birthday">
                        <div class="title"><?php echo __('Дата рождения и имя Вашего ребенка') ?></div>
                        <div class="_flex _justify-around">
                            <label class="cart-registration__label gcell--9 _mr-auto">
                                <input type="text" pattern="(0[1-9]|1[0-9]|2[0-9]|3[01]).(0[1-9]|1[012]).[0-9]{4}" id="birthday" name="birthday" data-name="birthday" placeholder="<?php echo __('Дата') ?>" autocomplete="off" class="cart-registration__input cart-registration__input--white simple-input js-birthday">
                            </label>
                            <label class="cart-registration__label gcell--13">
                                <input type="text" id="babyName" name="babyName" data-name="babyName" placeholder="<?php echo __('Имя') ?>" autocomplete="off" class="cart-registration__input cart-registration__input--white simple-input">
                            </label>
                        </div>
                    </div>
                    <label class="cart-registration__checkbox-label ">
                        <input required type="checkbox" name="remember" data-name="remember" id="reg-remember" class="cart-registration__checkbox">
                        <span class="cart-registration__checkbox-text cart-registration__checkbox-text--block cart-registration__checkbox-text--white"><?php echo __('Я согласен с условиями использования и на обработку персональных данных') ?>
                    </label>
                    <?php echo \Core\HTML::csrfField();?>
                    <button class="login-popup__submit login-popup__submit--yellow"><?php echo __('Зарегистрироваться') ?></button>
            </form>
        </div>
    </div>
</div>
