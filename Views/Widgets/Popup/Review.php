<?php
use Core\HTML;
?>
<div class="popup review-popup">
    <div class="review-popup__inner">
        <p class="review-popup__title"><?php echo __('') ?><?= __('НАПИСАТЬ ОТЗЫВ') ?></p>
        <p class="review-popup__sub-title">
            <?php echo __('Пожалуйста оставте ваш отзыв. <br> После модерации он будет опубликован на сайте.') ?>
        </p>
        <form class="js-form js-init" data-ajax="<?php echo HTML::link('ajax/review')?>">
            <div class="review-popup__rating">
                <div class="rating-group">
                    <input disabled checked class="rating__input rating__input--none" name="rating" data-name="rating" id="rating-none" value="0" type="radio">
                    <label aria-label="1 star" class="rating__label" for="rating-1"></label>
                    <input class="rating__input" name="rating" data-name="rating" id="rating-1" value="1" type="radio" checked>
                    <label aria-label="2 stars" class="rating__label" for="rating-2"></label>
                    <input class="rating__input" name="rating" data-name="rating" id="rating-2" value="2" type="radio" checked>
                    <label aria-label="3 stars" class="rating__label" for="rating-3"></label>
                    <input class="rating__input" name="rating" data-name="rating" id="rating-3" value="3" type="radio" checked>
                    <label aria-label="4 stars" class="rating__label" for="rating-4"></label>
                    <input class="rating__input" name="rating" data-name="rating" id="rating-4" value="4" type="radio" checked>
                    <label aria-label="5 stars" class="rating__label" for="rating-5"></label>
                    <input class="rating__input" name="rating" data-name="rating" id="rating-5" value="5" type="radio" checked>
                </div>
            </div>
            <input type="hidden" data-name="id" value="<?php echo $id; ?>">
            <label class="cart-registration__label">
                <input type="text" id="reg-name" required name="reg-name" data-name="name" placeholder="<?php echo __('Ваше имя') ?>" class="cart-registration__input simple-input" data-rule-word="true" data-rule-minlength="2" <?php echo (\Core\User::info()) ? 'value="'.\Core\User::info()->name.'"' : '' ?>>
            </label>
            <label class="cart-registration__label">
                <input type="tel" id="tel" name="tel" data-name="phone" placeholder="+38(***) ***-**-**" data-rule-phoneua="true" data-phonemask="+38(***) ***-**-**" data-phonemask-android="+38(***) ***-**-**" class="cart-registration__input simple-input js-init" <?php echo (\Core\User::info()) ? 'value="'.\Core\User::info()->phone.'"' : '' ?>>
            </label>
            <label class="cart-registration__label">
                <input type="text" id="advantages" name="advantages" data-name="advantages" placeholder="<?php echo __('Достоинства') ?>" class="cart-registration__input simple-input" data-rule-minlength="2">
            </label>
            <label class="cart-registration__label">
                <input type="text" id="disadvantages" name="disadvantages" data-name="disadvantages" placeholder="<?php echo __('Недостатки') ?>" class="cart-registration__input simple-input"  data-rule-minlength="2">
            </label>
            <label class="cart-registration__label contacts__data-label--textarea _mb-def">
                <textarea class="review-popup__textarea" placeholder="<?= __('Ваш отзыв') ?> *" name="contacts-msg" id="contacts-msg" data-name="message" required cols="30" rows="10"></textarea>
            </label>
            <label class="login-form__checkbox-label">
                <input required type="checkbox" name="remember" id="remember" class="cart-registration__checkbox">
                <span class="cart-registration__checkbox-text"><?php echo __('Я согласен на обработку персональных данных') ?></span>
                <label class="has-error" for="remember" style="display: none;"><?php echo __('Этот параметр обязателен') ?></label>
            </label>
            <?php echo HTML::csrfField(); ?>
            <button class="login-popup__submit login-popup__submit--yellow"><?php echo __('Отправить') ?></button>
        </form>
    </div>
</div>
