<?php

use Core\Cookie;
use Core\HTML;
use Core\Support;
use Core\User;
use Modules\Catalog\Models\Prices;
$cost = $data['cost'];
$cost_old = $data['cost_old'];
$currency = $currency = Cookie::getWithoutSalt('currency');
$prices_rozn = null;
if (User::info() && Cookie::getWithoutSalt('user-type') !== 'rozn') {
    $prices_rozn = Prices::getPricesRowByPriceType($item['id'], '875e4eb9-364f-11ea-80cc-a4bf01075a5b');
    $dataRozn = null;
    if (!empty($prices_rozn)) {
        $dataRozn = Support::calculatePriceWithCurrency($prices_rozn, $_currency, $_currentCurrency);
    }
}

//var_dump($active_count); die();
if (count($sizes) == 1 && $sizes[0]['size'] == '-') {
    $count = 1;
    $max_count = $sizes[0]['amount'] - $active_count['-'];
    $noSizes = true;
}

?>

<div class="popup sizes-gallery-popup js-product-all">
    <div class="product__top-wrap grid grid--space-def <?php echo ($wholeSale && count($sizes)) ? 'wholesale' : '' ?> js-product-item js-cart-item"
         data-id="<?php echo $item['id']; ?>"
         data-url="<?php echo HTML::link('ajax/addToCart') ?>"
         data-count="<?php echo $count; ?>"
         data-sizes='<?php echo json_encode($sizes_arr); ?>'
         data-counts='<?php echo json_encode($counts); ?>'>
        <div class="gcell--24 gcell--md-13 product__gallery-wrap">
            <div class="product__gallery grid">
                <?php if (count($images) > 1): ?>
                    <div class="product__gallery-preview gcell--24 gcell--md-5">
                        <div class="gallery-preview__wrap popup-gallery">
                            <?php foreach ($images as $image): ?>
                                <div style="width: 100%; display: inline-block;">
                                    <div class="gallery-preview__item lozad"
                                         style="background-image: url(<?php echo (is_file(HOST . Core\HTML::media('images/catalog/original/' . $image['image'], false))) ? HTML::media('images/catalog/original/' . $image['image']) : HTML::media('assets/images/placeholders/no-image.jpg'); ?>)"></div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <div class="product__gallery-main gcell--24 gcell--md-19">
                        <?php foreach ($images as $image): ?>
                            <div class="product__gallery-item">
                                <img class="product__gallery-img lozad zoom-img"
                                     data-zoomed="<?php echo (is_file(HOST . Core\HTML::media('images/catalog/original/' . $image['image'], false))) ? HTML::media('images/catalog/original/' . $image['image']) : HTML::media('assets/images/placeholders/no-image.jpg'); ?>"
                                     src="<?php echo (is_file(HOST . Core\HTML::media('images/catalog/original/' . $image['image'], false))) ? HTML::media('images/catalog/original/' . $image['image']) : HTML::media('assets/images/placeholders/no-image.jpg'); ?>"
                                     alt="">
                                <?php if ($obj->sale): ?>
                                    <div class="product-item__label product-item__label--red">
                                        <span><?php echo __('Скидка') ?></span>
                                    </div>
                                <?php endif; ?>
                            </div>
                        <?php endforeach; ?>
                    </div>
                <?php else: ?>
                    <div class="product-main">
                        <img class="product__gallery-img lozad zoom-img"
                             data-zoomed="<?php echo (is_file(HOST . Core\HTML::media('images/catalog/original/' . $item['image'], false))) ? HTML::media('images/catalog/original/' . $item['image']) : HTML::media('assets/images/placeholders/no-image.jpg'); ?>"
                             src="<?php echo (is_file(HOST . Core\HTML::media('images/catalog/original/' . $item['image'], false))) ? HTML::media('images/catalog/original/' . $item['image']) : HTML::media('assets/images/placeholders/no-image.jpg'); ?>"
                             alt="<?php echo $item['name']; ?>" title="<?php echo $item['name']; ?>">
                        <?php if ($item['sale']): ?>
                            <div class="product-item__label product-item__label--red">
                                <span><?php echo __('Скидка') ?></span>
                            </div>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>
            </div>

            <div class="product__gallery-360 is-hide js-product-media-360 threesixty js-threesixty"
                 data-totalframes="<?php echo $count_3d; ?>"
                 data-endframe="<?php echo $count_3d; ?>"
                 data-imagepath="<?php echo HTML::media('images/catalog_3d/' . $item['id']); ?>/"
                 data-ext=".png"
                 data-fileprefix="">
                <div class="threesixty__spinner">
                    <span>0%</span>
                </div>
                <ol class="threesixty__images"></ol>
                <div class="threesixty__buttons js-threesixty-buttons">
                    <div class="threesixty__button js-threesixty-prev">
                        <svg>
                            <use xlink:href="/Media/assets/images/sprites/icons.svg?v=1528183368000#left-arrow-slider"></use>
                        </svg>
                    </div>
                    <div class="threesixty__button js-threesixty-play">
                        <svg>
                            <use xlink:href="/Media/assets/images/sprites/icons.svg?v=1528183368000#play-icon"></use>
                        </svg>
                    </div>
                    <div class="threesixty__button js-threesixty-pause">
                        <svg>
                            <use xlink:href="/Media/assets/images/sprites/icons.svg?v=1528183368000#pause-icon"></use>
                        </svg>
                    </div>
                    <div class="threesixty__button js-threesixty-next">
                        <svg>
                            <use xlink:href="/Media/assets/images/sprites/icons.svg?v=1528183368000#right-arrow-slider"></use>
                        </svg>
                    </div>
                </div>
            </div>

        </div>
        <div class="gcell--24 gcell--md-11 product__info-wrap">
            <p class="product__code">
                <span><?php echo __('Код товара') ?>:</span>
                <span class="code"> <?php echo $item['id']; ?></span>
            </p>
            <div class="product__name-wrap">
                <p class="product__name"><?php echo $item['name']; ?></p>
                <?php if (User::info()): ?>
                <button class="product__favorite-btn js-favorite-btn <?php echo (!empty($favorite) && in_array($item['id'], $favorite)) ? 'active' : ''; ?>"
                        data-url="<?php echo HTML::link('ajax/addToFavorite') ?>">
                    <?php else: ?>
                    <button class="product__favorite-btn js-init"
                            data-mfp="ajax" data-mfp-src="<?php echo HTML::link('popup/getPopup'); ?>"
                            data-param='<?php echo json_encode(['name' => 'Login', 'param' => []]); ?>'>
                        <?php endif; ?>

                        <span class="product__favorite-icon">
                    <svg>
                        <use xlink:href="//babykroha.loc/Media/assets/images/sprites/icons.svg#favorite-icon"></use>
                    </svg>
                </span>
                        <span class="product__favorite-icon--fill">
                    <svg>
                        <use xlink:href="//babykroha.loc/Media/assets/images/sprites/icons.svg#like-icon"></use>
                    </svg>
                </span>
                    </button>
            </div>
            <div class="product__price-wrap">
                <div class="product__price-box">
                    <?php if ($item['sale']): ?>
                        <span class="product__price-old"><?= $currency == 'USD' ? $cost_old : ceil($cost_old) ?>
                        <span class="currency"><?php echo $currency; ?></span>
                </span>
                    <?php endif; ?>
                    <span class="product__price-current">
                    <span class="currency"><?php echo Support::productLabel(); ?></span>
                    <?= $currency == 'USD' ? $cost : ceil($cost) ?>
                    <span class="currency"><?php echo $currency; ?></span>
                    <?php if (!empty($dataRozn)): ?>
                        <span class="currency">(розн <?php echo $dataRozn['cost']; ?> <?php echo $currency; ?>)</span>
                    <?php endif; ?>
                </span>
                </div>
            </div>
            <?php if ($item['color']): ?>
                <div class="product__colors-wrap">
                    <span><?php echo __('Цвет') ?>:</span>
                    <?php if ($item['color_group']): ?>
                        <?php foreach ($item['color_group'] as $color): ?>
                            <a href="<?php echo HTML::link($color['alias'] . '?popup=true') ?>"
                               class="product__color-link <?php echo ($item['color_name'] == $color['color_name'] && $item['id'] == $color['id'])? 'active' : NULL; ?>">
                            <span style="background-color: <?php echo $color['color'] ?>"
                                  title="<?php echo $color['color_name']; ?>"></span>
                            </a>
                        <?php endforeach; ?>

                    <?php endif; ?>
                </div>
            <?php endif; ?>

            <div class="product__sizes-wrap">
                <?php if (!empty($table) || !$noSizes): ?>
                    <div class="product__sizes-text">
                        <?php if (!$noSizes): ?>
                            <span><?php echo __('Доступные размеры (см.)') ?>:</span>
                        <?php endif; ?>
                        <?php if (!empty($table)): ?>
                            <span class="product__sizes-link js-init" data-mfp="ajax"
                                  data-mfp-src="<?php echo HTML::link('popup/getPopup'); ?>"
                                  data-param='<?php echo json_encode(['name' => 'TableSizes', 'param' => ['id' => $item['parent_id']]]); ?>'>Таблица размеров
						<span class="product__sizes-icon">
							<svg>
								<use xlink:href="<?php echo HTML::media('assets/images/sprites/icons.svg#table-icon'); ?>"></use>
							</svg>
						</span>
					</span>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>
                <?php if (!$noSizes): ?>
                    <div class="product__sizes js-product-counts">
                        <?php foreach ($sizes as $size): ?>
                        <?php if(!array_search($size['size'], $active_sizes)):?>
                            <div class="product__size-item js-product-count-item <?php echo(!$size['amount'] ? 'disable' : ''); ?> <?php echo ($size['amount'] && $wholeSale) ? 'active' : '' ?> js-init"
                                 data-mfp="ajax" data-size="<?php echo $size['size']; ?>"
                                 data-mfp-src="<?php echo HTML::link('popup/getPopup'); ?>"
                                 data-param='<?php echo json_encode(['bigPopup' => 'true', 'name' => 'CartSizes', 'param' => ['item_id' => $item['id'], 'counts' => $counts, 'wholeSale' => $wholeSale, 'from' => 1]]); ?>'>
                                <span class="product__size-text"><?php echo $size['size']; ?></span>
                                <?php if ($wholeSale && $size['amount']): ?>
                                    <span class="product__size-count js-product-count"
                                          data-max="<?php echo $size['amount']; ?>"></span>
                                <?php else: ?>
                                    <span class="product__size-count js-product-count" data-max="0"></span>
                                <?php endif; ?>
                            </div>
                            <?php else:?>
                        <div class="product__size-item js-product-count-item <?php echo(!$size['amount'] ? 'disable' : ''); ?> <?php echo ($size['amount'] && $wholeSale) ? 'active' : '' ?> js-init"
                             data-mfp="ajax" data-size="<?php echo $size['size']; ?>"
                             data-mfp-src="<?php echo HTML::link('popup/getPopup'); ?>"
                             data-param='<?php echo json_encode(['bigPopup' => 'true', 'name' => 'CartSizes', 'param' => ['item_id' => $item['id'], 'counts' => $counts, 'wholeSale' => $wholeSale, 'from' => 1]]); ?>'>
                                <span class="size__item"><?php echo $size['size']; ?></span>
                                <span class="size__count js-product-count"><?php echo $active_count[$size['size']]?></span>
                        </div>
                        <?php endif?>
                        <?php endforeach; ?>
                    </div>
                <?php endif; ?>
            </div>
            <div class="product__buttons-wrap">

                <div class="product__counts-box">
                    <?php if ($wholeSale || $noSizes): ?>
                        <div class="product__counter">
                            <button class="product__counter-btn minus js-product-minus">-</button>
                            <input type="tel" class="product__counter-input js-product-input"
                                   value="<?php echo ($count) ? $count : '1'; ?>"
                                   data-max="<?php echo $max_count; ?>" disabled>
                            <button class="product__counter-btn plus js-product-plus">+</button>
                        </div>
                    <div class="product__counts-text">
                        <p class="product__count-total"><?php echo __('Всего товаров') ?>:
                            <span class="product__count js-product-count-all"><?php echo ($count) ? $count : '0'; ?></span>
                        </p>
                        <p><?php echo __('Общая стоимость') ?>:
                            <span class="product__total-price">
                    <span class="js-product-price"
                          data-price="<?php echo $cost ?>"><?php echo $count * $cost ?></span> <?php echo $currency; ?></span>
                        </p>
                    </div>
                </div>
                <div class="maximum-noty itemPage" style="display: none">
                    <?php echo __('Выбрано максимальное количество штук') ?>.
                </div>
                    <?php if(!$noSizes):?>
                        <button class="yellow-btn product__buy-btn  <?php echo(!$size->amount ? 'disable' : ''); ?> <?php echo ($size->amount && $wholeSale) ? 'active' : '' ?> js-init"
                <div class="product__size-item js-product-count-item <?php echo(!$size['amount'] ? 'disable' : ''); ?> <?php echo ($size['amount'] && $wholeSale) ? 'active' : '' ?> js-init"
                     data-mfp="ajax" data-size="<?php echo $size['size']; ?>"
                     data-mfp-src="<?php echo HTML::link('popup/getPopup'); ?>"
                     data-param='<?php echo json_encode(['bigPopup' => 'true', 'name' => 'CartSizes', 'param' => ['item_id' => $item['id'], 'counts' => $counts, 'wholeSale' => $wholeSale, 'from' => 1]]); ?>'>
                    <span class="product__size-text"><?php echo __('Купить') ?></span>
                        </button>

                    <?php else:?>
                        <div style="flex-basis: 100%;">
                            <button class="yellow-btn product__buy-btn js-cart-add"
                                    data-url="<?php echo HTML::link('ajax/addToCart') ?>" data-id="<?php echo $obj->id; ?>">
                                <?php echo __('Купить') ?>
                            </button>
                        </div>
                    <?php endif; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
