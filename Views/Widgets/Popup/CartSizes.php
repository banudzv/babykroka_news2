<div class="popup sizes-popup js-product-all <?php echo $bigPopup ? "sizes-big-popup": ''?>">
    <div class="sizes-popup__title-wrap">
        <p class="sizes-popup__title"><?php echo __('Доступные размеры') ?> </p>
        <span class="sizes-popup__clean js-product-clean all">Сбросить</span>
    </div>
    <p class="sizes-popup__sub-title"><?php echo __('Для заказа укажите нужное количество размеров одежды') ?></p>
    <form class="js-form js-init" data-ajax="<?php echo \Core\HTML::link('ajax/cartSizes'); ?>">
        <input type="hidden" data-name="catalog_id" value="<?php echo $item_id?>">
        <input type="hidden" data-name="from" value="<?php echo $from; ?>">
        <div class="sizes-popup__items-wrap">
            <?php if (count($sizes)): ?>
                <?php foreach ($sizes as $size): ?>
                    <?php if ($size->amount): ?>
                        <?php $size->amount = $max_count_current[$size->size]? $size->amount - $max_count_current[$size->size] : $size->amount; ?>
                        <div class="sizes-popup__item <?php echo $wholeSale ? 'wholesale' :'' ?> js-product-item with-zero">
                            <div class="product__size-item active">
                                <span class="product__size-text"><?php echo $size->size; ?></span>
                            </div>
                            <div class="sizes-item__counter-wrap">
                                <div class="sizes-item__counter-btn js-product-plus">+</div>
                                <input name="size-count-<?php echo $size->size; ?>" class="sizes-item__counter-input js-product-input" data-name="count[<?php echo $size->size; ?>]" type="tel" value="<?php echo ($counts[$size->size]) ? $counts[$size->size] : '0';?>" data-max="<?php echo $size->amount ?>" data-message="Вы">
                                <div class="sizes-item__counter-btn js-product-minus">-</div>
                            </div>
                            <div class="sizes-popup__clean-item js-product-clean">
                                <svg>
                                    <use xlink:href="<?php echo \Core\HTML::media('assets/images/sprites/icons.svg#garbage-icon');?>"></use>
                                </svg>
                            </div>
                        </div>
                    <?php else: ?>
                        <div class="sizes-popup__item js-product-item with-zero">
                            <div class="product__size-item disable">
                                <span class="product__size-text"><?php echo $size->size; ?></span>
                            </div>
                            <input type="hidden" name="size-count-<?php echo $size->size; ?>" data-name="count[<?php echo $size->size; ?>]" value="0">
                        </div>
                    <?php endif;?>
                <?php endforeach;?>
            <?php endif; ?>
            <?php if (count($sizes)==1 and $sizes[0]->size == '-'): ?>
            <div class="maximum-noty" style="display: none">
                <?php echo __('Выбрано максимальное количество штук.') ?>
            </div>
            <?php else: ?>
                <div class="maximum-noty" style="display: none">
                    <?php echo __('Выбрано максимальное количество по данному размеру.') ?>
                </div>
            <?php endif; ?>
           <!-- <?php /*if ($obj->color): */?>
                <div class="product__colors-wrap">
                    <span>Цвет:</span>

                        <?php /*foreach ($obj->color_group as $item):*/?>
                            <a href=""
                               class="product__color-link <?php /*echo ($obj->color_alias === $item['color_alias'] && $obj->id == $item['id']) ? 'active' : NULL; */?>">
                            <span style="background-color: <?php /*echo $item['color'] */?>"
                                  title="<?php /*echo $item['color_name']; */?>"></span>
                            </a>
                        <?php /*endforeach; */?>


                </div>
            --><?php /*endif; */?>

        </div>
        <?php echo \Core\HTML::csrfField();?>
        <button type="submit" class="login-popup__submit login-popup__submit--yellow js-accept"><?php echo __('Добавить в корзину') ?></button>
    </form>
</div>
