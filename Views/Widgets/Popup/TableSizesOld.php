<?php

use Core\HTML;

$arr = [];
for ($i = 0; $i < count($label); $i++){
    $arr[$i+1] = $label[$i]['key'] .'. '. $label[$i]['name'];
}

?>
<div class="popup table-size__popup">
    <div class="table-size__title-wrap">
        <p class="table-size__title"><?php echo __('') ?>ТАБЛИЦА РАЗМЕРОВ</p>
        <p class="table-size__text"><?php echo __('') ?>Единица измерения:
            <span class="table-size__sm"><?php echo __('') ?>сантиметры</span>
        </p>
    </div>
    <div class="table-size__content-wrap">
        <div class="table-size__img-wrap">
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                 viewBox="0 0 133 317">
                <image width="376" height="1013" opacity="0.5" xlink:href="<?php echo HTML::media('assets/images/size-image.png'); ?>"
                       transform="matrix(0.3112 0 0 0.3129 0 -1.464844e-005)"></image>
                <?php foreach ($labels as $obj): ?>
                    <?php if ($obj->label == 1):?>
                    <g id="a" transform="translate(6.000000, 18.000000)">
                        <text transform="matrix(1 0 0 1 49.7253 17.5047)" fill="#343434" font-family="Arial"
                              font-size="18px">a</text>
                        <path id="Line-2" fill="none" stroke="#E1323D" stroke-linecap="round" stroke-dasharray="5,3"
                              d="M0.1,22.5h106.4"/>
                    </g>
                    <?php elseif ($obj->label == 2):?>
                    <g id="b" transform="translate(2.000000, 141.000000)">
                        <text transform="matrix(1 0 0 1 0.6145 50.4014)" fill="#343434" font-family="ArialMT"
                              font-size="18px">b</text>
                        <path fill="none" stroke="#E1323D" stroke-linecap="round" stroke-dasharray="5,4"
                              d="M18.2,0.6v31v17.6V77"/>
                    </g>
                    <?php elseif ($obj->label == 3):?>
                    <g id="c" transform="translate(32.000000, 144.000000)">
                        <text transform="matrix(1 0 0 1 22.6368 17.7723)" fill="#343434" font-family="ArialMT"
                              font-size="18px">c</text>
                        <path fill="none" stroke="#E1323D" stroke-linecap="round" stroke-dasharray="5,4"
                              d="M0.2,27.5c0,0,18.3,0,54.9,0"/>
                    </g>
                    <?php elseif ($obj->label == 4):?>
                    <g id="d" transform="translate(87.000000, 139.000000)">
                        <text transform="matrix(1 0 0 1 13 17)" fill="#343434" font-family="ArialMT" font-size="18px">d</text>
                        <path fill="none" stroke="#E1323D" stroke-linecap="round" stroke-dasharray="5,4"
                              d="M0.4,1c7.2,16,11.9,30.7,14.3,44s3.5,32.5,3.4,57.5"/>
                    </g>
                    <?php elseif ($obj->label == 5):?>
                    <g id="e" transform="translate(118.000000, 131.000000)">
                        <text transform="matrix(1 0 0 1 5.741 97.9963)" fill="#343434" font-family="Arial"
                              font-size="18px">e</text>
                        <path fill="none" stroke="#E1323D" stroke-linecap="round" stroke-dasharray="5,4"
                              d="M0.5,0.2v185.2"/>
                    </g>
                    <?php elseif ($obj->label == 6):?>
                    <g id="f" transform="translate(17.000000, 235.000000)">
                        <text transform="matrix(1 0 0 1 0 41)" fill="#343434" font-family="ArialMT" font-size="18px">f</text>
                        <path fill="none" stroke="#E1323D" stroke-linecap="round" stroke-dasharray="5,4"
                              d="M13.5,0.1v76.4"/>
                    </g>
                    <?php elseif ($obj->label == 7):?>
                    <g id="g" transform="translate(32.000000, 184.000000)">
                        <text transform="matrix(1 0 0 1 23 17)" fill="#343434" font-family="Arial" font-size="18px">g</text>
                        <path fill="#D8D8D8" stroke="#E1323D" stroke-linecap="round" stroke-dasharray="5,4"
                              d="M0.2,28.5c0,0,18.3,0,54.9,0"/>
                    </g>
                    <?php endif; ?>
                <?php endforeach; ?>
            </svg>
        </div>
		<div class="table-size-wrap js-init" data-scrollbar data-axis="x">
			<table class="table-size">
				<tr class="table-size__head">
					<td><?php echo __('Метка') ?></td>
                    <td><?php echo __('от') ?> 0-1 <?php echo __('года') ?></td>
                    <td><?php echo __('от') ?> 1-2 <?php echo __('років') ?></td>
                    <td><?php echo __('от') ?> 2-3 <?php echo __('года') ?></td>
                    <td><?php echo __('от') ?> 3-4 <?php echo __('лет') ?></td>
                    <td><?php echo __('от') ?> 4-5 <?php echo __('лет') ?></td>
                    <td><?php echo __('от') ?> 5-6 <?php echo __('лет') ?></td>
                    <td><?php echo __('от') ?> 6-7 <?php echo __('лет') ?></td>
                    <td><?php echo __('от') ?> 7-8 <?php echo __('лет') ?></td>
                    <td><?php echo __('от') ?> 8-9 <?php echo __('лет') ?></td>
                    <td><?php echo __('от') ?> 9-10 <?php echo __('лет') ?></td>
                    <td><?php echo __('от') ?> 10-11 <?php echo __('лет') ?></td>
                    <td><?php echo __('от') ?> 11-12 <?php echo __('лет') ?></td>
				</tr>
				<?php foreach ($result as $obj):?>
				<tr>
					<td><?php echo $arr[$obj->label];?></td>
					<td><?php echo ($obj->age01) ? $obj->age01 : '-'; ?></td>
					<td><?php echo ($obj->age12) ? $obj->age12 : '-'; ?></td>
					<td><?php echo ($obj->age23) ? $obj->age23 : '-'; ?></td>
					<td><?php echo ($obj->age34) ? $obj->age34 : '-'; ?></td>
					<td><?php echo ($obj->age45) ? $obj->age45 : '-'; ?></td>
					<td><?php echo ($obj->age56) ? $obj->age56 : '-'; ?></td>
					<td><?php echo ($obj->age67) ? $obj->age67 : '-'; ?></td>
					<td><?php echo ($obj->age78) ? $obj->age78 : '-'; ?></td>
					<td><?php echo ($obj->age89) ? $obj->age89 : '-'; ?></td>
					<td><?php echo ($obj->age910) ? $obj->age910 : '-'; ?></td>
					<td><?php echo ($obj->age1011) ? $obj->age1011 : '-'; ?></td>
					<td><?php echo ($obj->age1112) ? $obj->age1112 : '-'; ?></td>
				</tr>
				<?php endforeach; ?>
			</table>
		</div>
    </div>
</div>
