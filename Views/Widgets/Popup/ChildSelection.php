<?php
use Core\Widgets;
?>
<div class="popup login-popup selection-show">
    <div class="grid">
        <div class="gcell--24">
            <div class="child-add__wrap">
                <div class="child-add__wrap <?php echo $class ?? ''?>">
                    <div class="child-add__info">
                        <div class="child-creation__button">
                            <div class="child-add__pic"></div>
                            <div class="child-inf">
                                <div class="child-add__title"><?= __('Добавить ребенка') ?></div>
                                <div class="q-mark child-add__description tooltip">
                                    <div class="tooltip__text">
                                        <p><?= __('Добавьте своего ребенка для :') ?></p>
                                        <ul>
                                            <li><?= __('быстрой фильтрации товаров') ?></li>
                                            <li><?= __('экономии времени') ?></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="child-selection__wrap">
                            <div class="child-selection__gender">
                                <ul>
                                    <li class="child-selection__tab <?= in_array('malchik', $selectedData['sex']) || empty($selectedData['sex']) ? 'active' : '' ?>" data-id="1" data-value="malchik">
                                        <span <?= in_array('malchik', $selectedData['sex']) ? 'checked' : '' ?>>
                                            <?= __('Мальчик') ?>
                                        </span>
                                    </li>
                                    <li class="child-selection__tab <?= in_array('devochka', $selectedData['sex']) ? 'active' : '' ?>" data-id="2" data-value="devochka">
                                        <span <?= in_array('devochka', $selectedData['sex']) ? 'checked' : '' ?>>
                                            <?= __('Девочка') ?>
                                        </span>
                                    </li>
                                </ul>
                            </div>
                            <form class="child-selection__content">
                                <div class="child-age select2-container <?= in_array('malchik', $selectedData['sex']) || empty($selectedData['sex']) ? 'is-active' : '' ?>" data-content-id="1">
                                    <div class="child-age__container select2-wrap">
                                        <select name="age_selection" class="select2 age-selection">
                                            <option class="_color-white" disabled <?= empty($selectedData['age']) || !in_array('malchik', $selectedData['sex']) ? 'selected' : '' ?> value=""><?= __('Выберите возраст') ?></option>
                                            <?php foreach ($ages['malchik'] as $age) : ?>
                                                <?php $checked = in_array($age->alias, $selectedData['age']) && in_array('malchik', $selectedData['sex']) ? 'selected' : ''; ?>
                                                <option value="<?= $age->alias ?>" <?= $checked ?>><?= $age->name ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="child-age select2-container <?= in_array('devochka', $selectedData['sex']) ? 'is-active' : '' ?>" data-content-id="2">
                                    <div class="child-age__container select2-wrap">
                                        <select name="age_selection" class="select2 age-selection">
                                            <option disabled <?= empty($selectedData['age']) || !in_array('devochka', $selectedData['sex']) ? 'selected' : '' ?> value=""><?= __('Выберите возраст') ?></option>
                                            <?php foreach ($ages['devochka'] as $age) : ?>
                                                <?php $checked = in_array($age->alias, $selectedData['age']) && in_array('devochka', $selectedData['sex']) ? 'selected' : ''; ?>
                                                <option value="<?= $age->alias ?>" <?= $checked ?>><?= $age->name ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $('.selection-show').find('select').each(function (i,e){
        $(e).select2({
            minimumResultsForSearch: Infinity,
            dropdownParent: $(e).closest('.mfp-container'),
            width: 'resolve',
            language: $('html').attr('lang'),
            positionDropdown: false
        });
    })
</script>