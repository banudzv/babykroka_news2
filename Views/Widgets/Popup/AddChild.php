<div class="popup review-popup">
    <div class="review-popup__inner">
        <p class="review-popup__title"><?php echo __('Добавить информацию о ребёнке') ?></p>

        <form class="js-form js-init" data-ajax="<?php echo \Core\HTML::link('form/add_child');?>">
            <div class="cart-registration__sex">
                <input type="radio" name="sex" data-name="sex" id="option-1" value="malchik">
                <input type="radio" name="sex" data-name="sex" id="option-2" value="devochka">
                <label for="option-1" class="option option-1">
                    <div class="dot"></div>
                    <span><?= __('Мальчик') ?></span>
                </label>
                <label for="option-2" class="option option-2">
                    <div class="dot"></div>
                    <span><?= __('Девочка') ?></span>
                </label>
            </div>
            <label class="cart-registration__label">
                <input type="text" id="reg-name" required name="reg-name" data-name="name" placeholder="<?php echo __('Имя ребёнка') ?>" class="cart-registration__input simple-input" data-rule-word="true" data-rule-minlength="2" value="">
                <span class="required-dot"></span>
                <label id="reg-name-error" class="has-error" for="reg-name" style="display: none"></label>
            </label>
            <label class="cart-registration__label">
                <input type="text" id="birthday" placeholder="<?php echo __('День рождения ребёнка') ?>" autocomplete="off" name="birthday" value="" class="cart-registration__input simple-input js-birthday" data-name="birthday">
            </label>
            <input type="hidden" data-name="uid" value="<?php echo \Core\User::info()->id;?>"/>
            <?php echo \Core\HTML::csrfField();?>
            <button class="login-popup__submit login-popup__submit--yellow"><?php echo __('Сохранить') ?></button>
        </form>
    </div>
</div>
