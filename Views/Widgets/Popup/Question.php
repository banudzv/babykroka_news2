<div class="popup review-popup">
    <div class="review-popup__inner">
        <p class="review-popup__title"><?php echo __('Задайте свой вопрос') ?></p>
        <p class="review-popup__sub-title">
            <?php echo __('Оставьте свой вопрос и наш менеджер ответит на него') ?>
        </p>
        <form class="js-form js-init" data-ajax="<?php echo \Core\HTML::link('form/asqQuestion');?>">
            <label class="cart-registration__label">
                <input type="text" id="reg-name" required name="reg-name" data-name="name" placeholder="<?php echo __('Ваше имя') ?>" class="cart-registration__input simple-input" data-rule-word="true" data-rule-minlength="2" <?php echo (\Core\User::info()) ? 'value="'.\Core\User::info()->name.'"' : '' ?>>
                <span class="required-dot"></span>
                <label id="reg-name-error" class="has-error" for="reg-name" style="display: none"></label>
            </label>
            <label class="cart-registration__label">
                <input type="tel" id="tel" name="tel" data-name="phone" placeholder="+38(***) ***-**-**" required data-rule-phoneua="true" data-phonemask="+38(***) ***-**-**" data-phonemask-android="+38(***) ***-**-**" class="cart-registration__input simple-input js-init" <?php echo (\Core\User::info()) ? 'value="'.\Core\User::info()->phone.'"' : '' ?>>
                <span class="required-dot"></span>
                <label id="tel-error" class="has-error" for="tel" style="display: none"></label>
            </label>
            <label class="profile__data-label profile__data-label--ask">
				<textarea placeholder="Ваш вопрос" class="profile__data-textarea" name="question" data-name="question" id="question" cols="30" rows="10" data-rule-minlength="2"></textarea>
            </label>
            <label class="callback-form__checkbox-label">
                <input required type="checkbox" name="remember" id="remember" class="cart-registration__checkbox">
                <span class="cart-registration__checkbox-text"><?php echo __('Я согласен на обработку персональных данных') ?></span>
                <label class="has-error" for="remember" style="display: none;"><?php echo __('Этот параметр обязателен') ?></label>
            </label>
            <?php echo \Core\HTML::csrfField();?>
            <button class="login-popup__submit login-popup__submit--yellow"><?php echo __('Отправить') ?></button>
        </form>
    </div>
</div>
