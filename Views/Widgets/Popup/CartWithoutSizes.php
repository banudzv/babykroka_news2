<div class="popup sizes-popup _text-center js-product-all">
	<p class="sizes-popup__title"><?php echo __('Изменение количества') ?></p>
    <p class="sizes-popup__sub-title"><?php echo __('Для заказа укажите нужное количество продукции') ?></p>
    <form class="js-form _mt-def js-init" data-ajax="<?php echo \Core\HTML::link('ajax/cartWithoutSizes'); ?>">
        <input type="hidden" data-name="catalog_id" value="<?php echo $item_id?>">
        <input type="hidden" data-name="from" value="<?php echo $from; ?>">
        <div class="sizes-popup__items-wrap _justify-center">
			<div class="sizes-popup__item <?php echo $wholeSale? 'wholesale' :'' ?> js-product-item with-zero">
				<div class="product__counter">
					<button type="button" class="product__counter-btn minus js-product-minus">-</button>
					<input type="tel" class="product__counter-input js-product-input" name="count" data-name="count" value="<?php echo $count; ?>" data-max="<?php echo $amount; ?>">
					<button type="button" class="product__counter-btn plus js-product-plus">+</button>
				</div>
			</div>
        </div>
        <?php echo \Core\HTML::csrfField();?>
        <button type="submit" class="login-popup__submit login-popup__submit--yellow"><?php echo __('Применить') ?></button>
    </form>
</div>
