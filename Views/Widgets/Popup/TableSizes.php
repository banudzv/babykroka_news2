<?php

use Core\HTML;
?>
<div class="popup table-size__popup">
    <div class="table-size__title-wrap">
        <p class="table-size__title"><?php echo __('ТАБЛИЦА РАЗМЕРОВ') ?></p>
        <p class="table-size__text"><?php echo __('Единица измерения') ?>:
            <span class="table-size__sm"><?php echo __('сантиметры') ?></span>
        </p>
    </div>
    <?php if(!empty($labels)): ?>
        <div class="table-size__content-wrap">
        <div class="table-size__sizes">
            <?php if (!empty($labels)):?>
                <?php $count=1;
                foreach ($labels as $sizeName => $params):
                    $active = ($count == count($labels)) ? 'is-active': '';
                    ?>
                    <span class="product__sizes-item yellow-btn product-page__grid <?php echo $active?>"><?php echo $sizeName;?></span>
                    <?php $count++;
                endforeach; ?>
            <?php endif;?>
        </div>
        <div class="table-size__img-wrap">
            <img src="<?php echo HTML::media('assets/images/size-image2.png'); ?>">
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                 viewBox="0 0 133 317" class="baby-size" style="overflow:visible">
                <?php if (!empty($labels)):?>
                    <?php foreach ($labels as $sizeName => $params): ?>
                        <?php foreach ($params as $param): ?>
                                <?php if ($param->mark === 'a'):?>
                                    <g id="a" transform="translate(-35, 12)">
                                        <text fill="#343434" font-size="10px" transform="matrix(1 0 0 1 105 -3)">a</text>
                                        <svg viewBox="0 0 898 391" fill="none" xmlns="http://www.w3.org/2000/svg" width="200" height="50">
                                            <path stroke-dasharray="30" d="M891.5 195.5C891.5 220.331 879.969 244.507 858.053 267.068C836.107 289.66 804.034 310.303 763.896 327.78C683.64 362.724 572.312 384.5 449 384.5C325.688 384.5 214.36 362.724 134.104 327.78C93.9656 310.303 61.8932 289.66 39.947 267.068C18.0305 244.507 6.5 220.331 6.5 195.5C6.5 170.669 18.0305 146.493 39.947 123.932C61.8932 101.34 93.9656 80.6969 134.104 63.2202C214.36 28.2756 325.688 6.5 449 6.5C572.312 6.5 683.64 28.2756 763.896 63.2202C804.034 80.6969 836.107 101.34 858.053 123.932C879.969 146.493 891.5 170.669 891.5 195.5Z" stroke="#FF1C1C" stroke-width="13"></path>
                                        </svg>
                                    </g>
                                <?php elseif ($param->mark === 'b'):?>
                                    <g id="b" transform="translate(90, 145)">
                                        <text fill="#343434" font-size="10px" transform="matrix(1 0 0 1 10 30)">b</text>
                                        <svg width="15" height="60" viewBox="0 0 75 512" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path stroke-dasharray="30" d="M10.5 7L72 505" stroke="#FF1C1C" stroke-width="13" stroke-linecap="round"></path>
                                        </svg>
                                    </g>
                                <?php elseif ($param->mark === 'c'):?>
                                    <g id="c" transform="translate(40, 155)">
                                        <text fill="#343434" font-size="10px" transform="matrix(1 0 0 1 35 0)">c</text>
                                        <svg height="14" viewBox="0 0 440 14" fill="none" xmlns="http://www.w3.org/2000/svg" width="55">
                                            <path stroke-dasharray="30" d="M7 7H433.5" stroke="#FF1C1C" stroke-width="13" stroke-linecap="round"></path>
                                        </svg>
                                    </g>
                                <?php elseif ($param->mark === 'd'):?>
                                    <g id="d" transform="translate(80, 135)">
                                        <text fill="#343434" font-size="10px" transform="matrix(1 0 0 1 35 5)">d</text>
                                        <svg viewBox="0 0 591 119" fill="none" xmlns="http://www.w3.org/2000/svg" height="20" width="70">
                                            <path stroke-dasharray="30" d="M584 112L7 7" stroke="#FF1C1C" stroke-width="13" stroke-linecap="round"></path>
                                        </svg>
                                    </g>
                                <?php elseif ($param->mark === 'e'):?>
                                    <g id="e" transform="translate(25, 140)">
                                        <text fill="#343434" font-size="10px" transform="matrix(1 0 0 1 -10 70)">e</text>
                                        <svg viewBox="0 0 48 1229" fill="none" xmlns="http://www.w3.org/2000/svg" height="150" width="10">
                                            <path stroke-dasharray="30" d="M41 7L6.5 1222" stroke="#FF1C1C" stroke-width="13" stroke-linecap="round"></path>
                                        </svg>
                                    </g>
                                <?php elseif ($param->mark === 'f'):?>
                                    <g id="f" transform="translate(65, 220)">
                                        <text fill="#343434" font-size="10px" transform="matrix(1 0 0 1 2 40)">f</text>
                                        <svg width="20" height="70" viewBox="0 0 113 540" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path stroke-dasharray="30" d="M7 7L106 532.5" stroke="#FF1C1C" stroke-width="13" stroke-linecap="round"></path>
                                        </svg>
                                    </g>
                                <?php elseif ($param->mark === 'g'):?>
                                    <g id="g" transform="translate(35, 200)">
                                        <text fill="#343434" font-size="10px" transform="matrix(1 0 0 1 40 0)">g</text>
                                        <svg height="14" viewBox="0 0 493 14" fill="none" xmlns="http://www.w3.org/2000/svg" width="70">
                                            <path stroke-dasharray="30" d="M7 7H486.5" stroke="#FF1C1C" stroke-width="13" stroke-linecap="round"></path>
                                        </svg>
                                    </g>
                                <?php elseif ($param->mark === 'i'):?>
                                    <g id="i" transform="translate(95, 200)">
                                        <text fill="#343434" font-size="10px" transform="matrix(1 0 0 1 15 50)">i</text>
                                        <svg viewBox="0 0 60 702" fill="none" xmlns="http://www.w3.org/2000/svg" width="20" height="90">
                                            <path stroke-dasharray="30" d="M7 6.5L53 695.5" stroke="#FF1C1C" stroke-width="13" stroke-linecap="round"></path>
                                        </svg>
                                    </g>
                                <?php elseif ($param->mark === 'w'):?>
                                    <g id="w" transform="translate(5, 315)">
                                        <text transform="matrix(1 0 0 1 31 17)" fill="#343434" font-size="10px">w</text>
                                        <svg height="10" viewBox="0 0 414 47" fill="none" xmlns="http://www.w3.org/2000/svg" width="45">
                                            <path stroke-dasharray="30" d="M407 7L7 40.5" stroke="#FF1C1C" stroke-width="13" stroke-linecap="round"></path>
                                        </svg>
                                    </g>
                                <?php endif; ?>
                        <?php endforeach; ?>
                        <?php break; ?>
                    <?php endforeach; ?>
                <?php endif; ?>
                <?php if($product->width): ?>
                    <g id="y" transform="translate(-45, -12)">
                        <text fill="#343434" font-size="10px" transform="matrix(1 0 0 1 105 0)">y</text>
                        <svg height="14" viewBox="0 0 1871 14" fill="none" xmlns="http://www.w3.org/2000/svg" width="220">
                            <path stroke-dasharray="30" d="M1864 7.00016L7 7" stroke="#FF1C1C" stroke-width="13" stroke-linecap="round"></path>
                        </svg>
                    </g>
                <?php endif; ?>
                <?php if($product->height): ?>
                    <g id="z" transform="translate(170, 0)">
                        <text fill="#343434" font-size="10px" transform="matrix(1 0 0 1 10 150)">z</text>
                        <svg width="13" fill="none" xmlns="http://www.w3.org/2000/svg" height="100%" viewBox="0 0 13 2657">
                            <path stroke-dasharray="30" d="M6.5 7V2640" stroke="#FF1C1C" stroke-width="13" stroke-linecap="round"></path>
                        </svg>
                    </g>
                <?php endif; ?>
            </svg>
        </div>

		<div class="table-size-wrap js-init">
            <?php $count=1;
            foreach ($labels as $sizeName => $params):
                $active = ($count == count($labels)) ? '': 'display: none';?>
                <div class="tab tab-<?php echo $sizeName;?>" style="<?php echo $active; ?>">
                    <?php foreach ($params as $param): ?>
                        <div>
                            <span class="__uppercase __bold"><?php echo $param->mark ;?></span> - <?php echo $param->parameter . ' см. ' . $param->name?>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php $count++;
            endforeach; ?>
            <?php if($product->width): ?>
                <div>
                    <span class="__uppercase __bold">y</span> - <?php echo $product->width; ?>  см.
                </div>
            <?php endif; ?>
            <?php if($product->height): ?>
                <div>
                    <span class="__uppercase __bold">z</span> - <?php echo $product->height; ?> см.
                </div>
            <?php endif; ?>
		</div>
    </div>

    <?php endif;?>
</div>
