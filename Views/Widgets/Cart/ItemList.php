<?php

use Core\HTML;
use Core\Support;
use Core\User;

?>
<?php foreach ($cart as $key => $val): ?>
    <?php $obj = $cart[$val['id']]['obj'];
    if ($obj->sizes):
        $counts = [];
        $count = 0;
        foreach ($obj->sizes as $size):
            $counts[$size->size] = $size->count;
            $count += $size->count;
            if ($size->size == '-'){
                unset($obj->sizes);
            }
        endforeach;
    else:
        $counts = [$val['count']];
        $count = $val['count'];
    endif;
    $data = Support::calculatePriceWithCurrency($obj,$_currency,$_currentCurrency);
    $wholeSale = $sell_size && $obj->sell_size ? 1 : 0;
    $cost = $data['cost'];
    $cost_old = $data['cost_old'];
    $currency = $data['currency'];
    $item_sum = $cost * $val['count'];
    ?>
    <div class="cart-item__wrap">
        <div class="cart-item js-cart-item" data-id="<?php echo $obj->id; ?>"
             data-url="./hidden/cart-update.php" data-static="true">
            <div class="cart-item__inner">
                <div class="cart-item__img-box">
                    <div class="cart-item__img-wrap">
                        <a href="<?php echo HTML::link($obj->alias); ?>">
                            <?php if (is_file(HOST . Core\HTML::media('images/catalog/original/'.$obj->image, false))): ?>
                                <img class="cart-item__img lozad"
                                     src="<?php echo HTML::media('images/catalog/original/' . $obj->image); ?>"
                                     alt="<?php echo $obj->name; ?>">
                            <?php else: ?>
                                <img class="cart-item__img lozad"
                                     src="<?php echo HTML::media('assets/images/placeholders/no-image.jpg'); ?>"
                                     alt="<?php echo $obj->name; ?>">
                            <?php endif; ?>
                        </a>
                    </div>
                </div>
                <div class="cart-item__info-box">
                    <div class="cart-item__info-item cart-item__info-item--first big-item">
                        <p class="cart-item__code-box"><?php echo __('Код товара') ?>:
                            <span class="cart-item__code"><?php echo $obj->id; ?></span>
                        </p>
                        <p class="cart-item__name"><?php echo $obj->name; ?></p>

                        <?php if ($obj->color): ?>
                            <div class="cart-item__color-box">
                                <span><?php echo __('Цвет') ?>:</span>
                                <span class="cart-item__color"
                                      style="background-color: <?php echo $obj->color ?>"
                                      title="<?php echo $obj->color_name; ?>"></span>
                            </div>
                        <?php endif; ?>

                        <?php if ($obj->sizes): ?>
                            <div class="cart-item__size-box">
                                <span><?php echo __('Размер') ?>:</span>
                                <?php foreach ($obj->sizes as $size): ?>
                                    <div class="size__item"
                                         data-size="<?php echo $size->size; ?>">
                                        <span class="size__text"><?php echo $size->size; ?></span>
                                        <span class="size__count"><?php echo $size->count; ?></span>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        <?php endif; ?>

                    </div>

                    <div class="cart-item__info-item cart-item__info-item--second big-item">
                        <span><?php echo __('Цена за единицу') ?>:</span>
                        <p class="cart-item__price">
                            <span><?php echo $cost; ?></span>
                            <span> <?php echo $currency; ?></span>
                        </p>
                    </div>

                    <div class="cart-item__info-item cart-item__info-item--third big-item">

                        <span><?php echo __('Единиц товара') ?>:</span>
                        <p class="cart-item__count"><?php echo $val['count']; ?></p>

                    </div>

                    <div class="cart-item__info-item cart-item__info-item--third big-item ">
                        <span><?php echo __('Сумма') ?>:</span>
                        <p class="cart-item__price">
                            <span><?php echo $item_sum; ?></span>
                            <span> <?php echo $currency; ?></span>
                        </p>
                    </div>

                    <div class="cart-item__info-item cart-item__info-item--fourth big-item">

                        <span class="cart-item__redact js-cart-redact js-init" data-mfp="ajax"
                              data-mfp-src="<?php echo \Core\HTML::link('popup/getPopup'); ?>"
                            <?php if ($obj->sizes): ?>
                              data-param='<?php echo json_encode(['name' => 'CartSizes', 'param' => ['item_id' => $obj->id,'wholeSale' => $wholeSale, 'counts' => $counts, 'from' => 3]]); ?>'>
                            <?php else: ?>
                                data-param='<?php echo json_encode(['name' => 'CartWithoutSizes', 'param' => ['item_id' => $obj->id,'wholeSale' => $wholeSale, 'count' => $count, 'from' => 2]]); ?>'>
                            <?php endif; ?>
                            <svg>
                                <use xlink:href="<?php echo HTML::media('assets/images/sprites/icons.svg#pencil-icon'); ?>"></use>
                            </svg>
                        </span>

                        <div class="cart-item__redact">
                            <span class="js-cart-remove"
                                  data-url="<?php echo HTML::link('ajax/deleteItemFromCart'); ?>">
                                <svg>
                                    <use xlink:href="<?php echo HTML::media('assets/images/sprites/icons.svg#garbage-icon'); ?>"></use>
                                </svg>
                            </span>
                            <div class="cart-item__remove-confirm js-remove-confirm">
                                <p class="cart-item__confirm-title"><?php echo __('Удалить') ?>?</p>
                                <div>
                                    <button class="yellow-border-btn cart-item__confirm-btn js-confirm"><?php echo __('Да') ?>
                                    </button>
                                    <button class="yellow-btn cart-item__confirm-btn js-unconfirm"><?php echo __('Нет') ?>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
<?php endforeach; ?>
