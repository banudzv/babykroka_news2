<?php
use Core\HTML;
?>
<noscript>
    <link rel="stylesheet" href="<?php echo HTML::media('assets/css/noscript.css');?>">
    <div class="noscript-msg">
        <input id="noscript-msg__input" class="noscript-msg__input" type="checkbox" title="Закрыть">
        <div class="noscript-msg__container">
            <label class="noscript-msg__close" for="noscript-msg__input">&times;</label>
            <a class="noscript-msg__wezom-link" href="http://wezom.com.ua/" target="_blank" title="Агентство системных интернет-решений Wezom">&nbsp;</a>
            <div class="noscript-msg__content">
                <?php echo __('<p>В Вашем браузере <b>отключен JavaScript!</b> Для корректной работы с сайтом необходима поддержка Javascript.</p><p>Мы рекомендуем Вам включить использование JavaScript в настройках вашего браузера.</p>') ?>
            </div>
        </div>
    </div>
</noscript>
<?php if(!\Core\Cookie::getWithoutSalt('allow-cookie-usage')):?>
<div class="cookie is-hidden" data-cookie-info>
    <div class="container">
        <div class="grid grid--space-def _pt-def">
            <div class="gcell--24 gcell--ms-20 _flex-grow">
                <p class="cookie__content">
                    <?php echo __('Этот сайт использует файлы cookie и похожие технологии, чтобы гарантировать максимальное удобство пользователям. <br> При использовании данного сайта, вы подтверждаете свое согласие на использование файлов cookie в соответствии с настоящим уведомлением в отношении данного типа файлов <br> Если вы не согласны с тем, чтобы мы использовали данный тип файлов, то вы должны соответствующим образом установить настройки вашего браузера или не использовать сайт babykroha.ua.') ?>
                </p>
            </div>
            <div class="gcell--24 gcell--ms-4 _flex-noshrink">
                <button class="yellow-btn cookie-btn-confirm" data-cookie-submit>Согласен</button>
            </div>
        </div>
    </div>
</div>
<?php endif; ?>
<script>window.$wzmOld_URL_IMG = '<?php echo HTML::media('assets/css/static/pic/wezom-info-red.gif'); ?>'</script>
<script src="<?php echo HTML::media('/assets/js/static/wezom-old.min.js'); ?>"></script>
<script src="<?php echo HTML::media('/js/translations/ru.js'); ?>"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=<?php echo \Core\Config::get('basic.googlemapskey')?>" async defer></script>
<?php foreach ($js as $file_script): ?>
	<?php echo HTML::script($file_script) . "\n"; ?>
<?php endforeach; ?>
