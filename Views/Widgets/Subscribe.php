<?php

use Core\HTML;

?>
<div class="subscribe-form__wrap">
    <div class="grid subscribe-form__box">
        <div class="gcell--24">
            <p class="subscribe-form__title"><?php echo __('Подписка на рассылку (без спама)') ?></p>
        </div>
        <div class="gcell--24">
            <form class="subscribe-form js-form js-init" data-ajax="<?php echo HTML::link('ajax/subscribe'); ?>">
                <input required placeholder="E-mail" id="subscribe-mail" name="subscribe-mail" data-name="email"
                       type="email" class="subscribe-form__input simple-input">
                <button class="subscribe-form__submit yellow-btn js-form-submit"><?php echo __('Подписаться') ?></button>
            </form>
        </div>
    </div>
</div>
