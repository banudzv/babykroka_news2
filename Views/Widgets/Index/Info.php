<?php

use Core\HTML;

?>
<div class="index-info__wrap">
    <div class="container grid">
        <div class="index-info__text-box grid gcell--24 gcell--def-14">
            <div class="index-info__item gcell--24 gcell--sm-12">
                <div class="index-info__item-img">
                    <svg>
                        <use xlink:href="/Media/assets/images/sprites/icons.svg?v=1528183368709#fabric-icon"></use>
                    </svg>
                </div>
                <div class="index-info__item-text">
                    <?php echo __('Собственная фабрика') ?>
                </div>
            </div>
            <div class="index-info__item gcell--24 gcell--sm-12">
                <div class="index-info__item-img">
                    <svg>
                        <use xlink:href="/Media/assets/images/sprites/icons.svg?v=1528183368709#canpol-icon"></use>
                    </svg>
                </div>
                <div class="index-info__item-text">
                    <?php echo __('Официальный представитель Canpol') ?>
                </div>
            </div>
            <div class="index-info__item gcell--24 gcell--sm-12">
                <div class="index-info__item-img">
                    <svg>
                        <use xlink:href="/Media/assets/images/sprites/icons.svg?v=1528183368709#diler-icon"></use>
                    </svg>
                </div>
                <div class="index-info__item-text">
                    <?php echo __('Официальный диллер турецких товаров') ?>
                </div>
            </div>
            <div class="index-info__item gcell--24 gcell--sm-12">
                <div class="index-info__item-img">
                    <svg>
                        <use xlink:href="/Media/assets/images/sprites/icons.svg?v=1528183368709#avent-icon"></use>
                    </svg>
                </div>
                <div class="index-info__item-text">
                    <?php echo __('Официальный представитель Avent') ?>
                </div>
            </div>
            <div class="index-info__item gcell--24 gcell--sm-12">
                <div class="index-info__item-img">
                    <svg>
                        <use xlink:href="/Media/assets/images/sprites/icons.svg?v=1528183368709#box-icon"></use>
                    </svg>
                </div>
                <div class="index-info__item-text">
                    <?php echo __('Бесплатная доставка укрпочтой') ?>
                </div>
            </div>
        </div>
        <div class="index-info__img-box gcell--def-6">
            <div class="index-info__img">
                <img src="/Media/assets/images/bear.png?v=1523945764551" alt class="lozad">
            </div>
        </div>
    </div>
</div>
