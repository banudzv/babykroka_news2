<div class="index-seo-text">
    <div class="container grid">
        <div class="gcell--24 gcell--md-12 index-seo-text__img">
            <img src="/Media/assets/images/Layer-2.png?v=1523945764543" alt class="lozad index-seo-text__img">
        </div>
        <div class="gcell--24 gcell--md-12 index-seo-text__content">
            <div class="text-wrapper js-init" data-scrollbar>
                <?php echo $text; ?>
            </div>
        </div>
    </div>
</div>
