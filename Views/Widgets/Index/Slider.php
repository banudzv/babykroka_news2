<?php

use Core\Config;
use Core\HTML;
use Core\View;

?>
<div class="main-slider__wrap">
    <div class="main-slider">
        <?php if(!empty($result)):?>
            <?php foreach ($result as $obj): ?>
            <div class="main-slider__item-wrap">
                <div class="main-slider__item grid">
                    <div class="main-slider__text-box gcell--24 gcell--md-11">
                        <div>
                            <p class="main-slider__title">
                                <span class="main-slider__title-counter"></span>
                                <span><?php echo $obj->name; ?></span>
                            </p>
                            <a href="<?php echo HTML::link($obj->url); ?>"
                               class="yellow-btn regular-btn main-slider__btn">
                                <?php echo $obj->main_button_name ?: __('Перейти в каталог'); ?>
                            </a>
                            <div class="main-slider__controls-holder"></div>
                        </div>
                        <div class="main-slider__links grid">
                            <?php if ($obj->show_outer_wear_link):?>
                            <a href="<?php echo HTML::link($obj->link_outerwear); ?>"
                               class="main-slider__link gcell--24 gcell--sm-12">
                                <div class="main-slider__link-icon">
                                    <?php if ($obj->outerwear_button_icon):?>
                                        <svg>
                                            <use xlink:href="<?php echo HTML::media('assets/images/sprites/icons.svg#'.Config::get('main.svg')[$obj->outerwear_button_icon]); ?>"></use>
                                        </svg>
                                    <?php endif;?>
                                </div>
                                <div>
                                    <p class="main-slider__link-text"><?php echo $obj->outerwear_button_name?:'Верхняя одежда'; ?></p>
                                </div>
                            </a>
                            <?php endif;?>
                            <?php if ($obj->show_jersey_link):?>
                                <a href="<?php echo HTML::link($obj->link_jersey); ?>"
                                   class="main-slider__link gcell--24 gcell--sm-12">
                                    <div class="main-slider__link-icon">
                                        <?php if ($obj->jersey_button_icon):?>
                                            <svg>
                                                <use xlink:href="<?php echo HTML::media('assets/images/sprites/icons.svg#'.Config::get('main.svg')[$obj->jersey_button_icon]); ?>"></use>
                                            </svg>
                                        <?php endif;?>
                                    </div>
                                    <div>
                                        <p class="main-slider__link-text"><?php echo $obj->jersey_button_name?:'Трикотаж'; ?></p>
                                    </div>
                                </a>
                            <?php endif;?>
                        </div>
                    </div>
                    <?php if(HTML::fileExists(HOST.HTML::media('images/slider/original/' . $obj->image,false))):?>
                        <a <?php echo ($obj->show_image_link && $obj->image_link) ? 'href="'.$obj->image_link.'"' : NULL;?> class="main-slider__img-box gcell--md-13"
                             style="background-image: url(<?php echo HTML::media('images/slider/original/' . $obj->image); ?>)"></a>
                    <?php else:?>
                        <a <?php echo ($obj->show_image_link && $obj->image_link) ? 'href="'.$obj->image_link.'"' : NULL;?> class="main-slider__img-box gcell--md-13"
                             style="background-image: url(<?php echo HTML::media('assets/images/placeholders/no-image.jpg'); ?>)"></a>
                    <?php endif;?>
                </div>
            </div>
        <?php endforeach ?>
        <?php endif;?>
    </div>
    <div class="main-slider__controls"></div>
</div>
