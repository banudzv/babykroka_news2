<?php

use Core\HTML;
use Core\Text;

?>
<div class="index-news-block">
    <div class="container">
        <p class="index-news-block__title main-title"><?php echo __('Наши новости') ?></p>
        <div class="grid">
            <?php foreach ($results as $obj): ?>
                <div class="news-item__wrap gcell--24 gcell--sm-8">
                    <div class="news-item">
                        <div class="news-item__top">
                            <a href="<?php echo HTML::link('/articles/' . $obj->alias) ?>" class="news-item__img-link">
                                <?php if (is_file(HOST . Core\HTML::media('images/articles/original/'.$obj->image, false))): ?>
                                    <span><img src="<?php echo HTML::media('images/articles/medium/' . $obj->image); ?>" alt="<?php echo $obj->name ?>" title="<?php echo $obj->name; ?>" class="lozad"></span>
                                <?php else: ?>
									<span><img src="<?php echo HTML::media('assets/images/placeholders/no-image-articles.jpg'); ?>" alt="<?php echo $obj->name; ?>" title="<?php echo $obj->name; ?>"></span>
                                <?php endif; ?>
                            </a>
                        </div>
                        <div class="news-item__bottom">
                            <div class="news-item__date"><?php echo date('d/m', $obj->date) ?></div>
                            <div class="news-item__text-box">
                                <p class="news-item__title">
                                    <a href="<?php echo HTML::link('/articles/' . $obj->alias) ?>"
                                       class="news-item__link"
                                       title="<?php echo $obj->name ?>"><?php echo Text::limit_words2($obj->name, 10) ?></a>
                                </p>
                                <p class="news-item__text"><?php echo Text::limit_words2($obj->text, 40) ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>

        </div>
    </div>
</div>
