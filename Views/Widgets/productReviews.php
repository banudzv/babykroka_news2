<?php
use Core\HTML;
?>
<?php if (count($reviews)): ?>
    <?php foreach ($reviews as $review): ?>
        <div class="product__reviews-item js-review" data-id="<?php echo $review->id; ?>">
            <p class="product__reviews-title-wrap">
                <span class="product__reviews-title"><?php echo $review->name ?></span>
                <span class="product__reviews-date">( <?php echo $review->date ?> )</span>
            </p>
            <p class="product__reviews-text">
                <?php echo $review->text ?>
            </p>
            <div class="product__reviews-likes">
                <?php if (\Core\User::info()): ?>
                <div class="product__reviews-like js-review-like"
                     data-url="<?php echo HTML::link('ajax/like') ?>">
                <?php else: ?>
                <div class="product__reviews-like js-init"
                     data-mfp="ajax" data-mfp-src="<?php echo HTML::link('popup/getPopup'); ?>"
                     data-param='<?php echo json_encode(['name' => 'Login', 'param' => []]); ?>'>
                <?php endif; ?>

                    <span class="product__reviews-icon like">
                        <svg>
                            <use xlink:href="<?php echo HTML::media('assets/images/sprites/icons.svg#hand-icon') ?>"></use>
                        </svg>
                    </span>
                    <span class="js-like-count"><?php echo $review->likes ?></span>
                </div>
                <?php if (\Core\User::info()): ?>
                <div class="product__reviews-dislike js-review-dislike"
                         data-url="<?php echo HTML::link('ajax/dislike') ?>">
                <?php else: ?>
                <div class="product__reviews-like js-init"
                     data-mfp="ajax" data-mfp-src="<?php echo HTML::link('popup/getPopup'); ?>"
                     data-param='<?php echo json_encode(['name' => 'Login', 'param' => []]); ?>'>
                <?php endif; ?>

                    <span class="product__reviews-icon dislike">
                        <svg>
                            <use xlink:href="<?php echo HTML::media('assets/images/sprites/icons.svg#hand-down-icon') ?>"></use>
                        </svg>
                    </span>
                    <span class="js-dislike-count"><?php echo $review->dislikes ?></span>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
    <div class="product__reviews-pagination">
        <?php echo $pager; ?>
    </div>
<?php else: ?>
    <h2> <?php echo __('В данный момент отзывов о товаре нет. У тебя есть шанс оставить отзыв первым!') ?></h2>
<?php endif; ?>
