<?php
use Core\Widgets;
use Core\HTML;
use Core\Config;

?>
<?php foreach ( $counters as $script ): ?>
    <?php echo $script; ?>
<?php endforeach ?>
    <div class="section">

        <div class="footer">
            <div class="footer__top">
                <div class="container">
                    <div class="grid grid--def-space-def">
                        <div class="gcell--24 gcell--md-12">
                            <?php echo Widgets::get('Subscribe'); ?>
                            <div class="footer__contacts">
                                <p class="footer-menu__title"><?php echo __('Консультации') ?></p>
                                <div class="grid">
                                    <div class="gcell--12 gcell--def-8">
                                        <?php if (!empty(Config::get('static.footer_phone'))): ?>
                                            <a class="footer__phone"
                                               href="tel:+<?php echo Config::get('static.footer_phone'); ?>"><?php echo Config::get('static.footer_phone'); ?></a>
                                        <?php endif; ?>
                                        <?php if (!empty(Config::get('static.footer_phone_2'))): ?>
                                            <a class="footer__phone"
                                               href="tel:+<?php echo Config::get('static.footer_phone_2'); ?>"><?php echo Config::get('static.footer_phone_2'); ?></a>
                                        <?php endif; ?>
                                        <?php if (!empty(Config::get('static.footer_phone_3'))): ?>
                                            <a class="footer__phone"
                                               href="tel:+<?php echo Config::get('static.footer_phone_3'); ?>"><?php echo Config::get('static.footer_phone_3'); ?></a>
                                        <?php endif; ?>
                                        <?php if (!empty(Config::get('static.footer_phone_4'))): ?>
                                            <a class="footer__phone"
                                               href="tel:+<?php echo Config::get('static.footer_phone_4'); ?>"><?php echo Config::get('static.footer_phone_4'); ?></a>
                                        <?php endif; ?>
                                    </div>
                                    <div class="gcell--12 gcell--def-8">
                                        <span class="footer__callback js-init" data-mfp="ajax" data-mfp-src="<?php echo \Core\HTML::link('popup/getPopup');?>" data-param='<?php echo json_encode(['name' => 'Callback', 'param' => []]);?>'><?php echo __('Перезвоните мне') ?></span>
                                        <a href="mailto:<?php echo Config::get('static.email_site'); ?>" class="footer__mail"><?php echo Config::get('static.email_site'); ?></a>
                                        <span class="footer__schedule"><?php echo __('Каждый день с') ?>
                                    <span><?php echo Config::get('static.time_work'); ?></span> -
                                    <span><?php echo Config::get('static.time_work_to'); ?></span>
                                 </span>
                                    </div>
                                    <div class="gcell--24 gcell--def-8">
                                        <div class="footer__links-wrap">
                                            <div class="footer__payments footer__payments--mob">
                                                <img src="<?php echo HTML::media('assets/images/visa.png') ?>" alt="visa">
                                                <img src="<?php echo HTML::media('assets/images/mastercard.png') ?>" alt="mastercard">
                                            </div>
                                            <div class="footer__socials">
                                                <?php if(Config::get('socials.fb')):?>
                                                    <a href="<?php echo Config::get('socials.fb'); ?>" class="footer__social-item" target="_blank">
                                                        <svg>
                                                            <use xlink:href="/Media/assets/images/sprites/icons.svg?v=1528183368709#fb-icon"></use>
                                                        </svg>
                                                    </a>
                                                <?php endif; ?>
                                                <?php if(Config::get('socials.instagram')):?>
                                                    <a href="<?php echo Config::get('socials.instagram'); ?>" class="footer__social-item" target="_blank">
                                                        <svg>
                                                            <use xlink:href="/Media/assets/images/sprites/icons.svg?v=1528183368709#insta-icon"></use>
                                                        </svg>
                                                    </a>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="gcell--24 gcell--md-8 footer__client">
                            <div class="grid grid--space-def">
                                <div class="gcell--24 gcell--md-12">
                                    <div class="footer__catalog">
                                        <p class="footer-menu__title">Каталог</p>
                                        <ul class="footer-menu">
                                            <?php foreach ($categories[0] as $cat):?>
                                                <li class="footer-menu__item">
                                                    <a href="<?php echo HTML::link('catalog/'. $cat->alias) ?>" class="footer-menu__link"><?php echo $cat->name ?></a>
                                                </li>
                                            <?php endforeach;?>
                                            <?php foreach ($contentMenu as $cm):?>
                                                <li class="footer-menu__item">
                                                    <a href="<?php echo HTML::link($cm->url)?>" class="footer-menu__link"><?php echo $cm->name?></a>
                                                </li>
                                            <?php endforeach; ?>
                                        </ul>
                                    </div>
                                </div>
                                <div class="gcell--24 gcell--md-12">
                                    <div class="footer__buy">
                                        <p class="footer-menu__title"><?php echo __('Покупателю') ?></p>
                                        <div class="footer__buy-menu">
                                            <ul class="footer-menu">
                                                <?php foreach ($footerMenu as $fm):?>
                                                    <li class="footer-menu__item">
                                                        <a href="<?php echo HTML::link($fm->url)?>" class="footer-menu__link"><?php echo $fm->name?></a>
                                                    </li>
                                                <?php endforeach; ?>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer__bottom">
                <div class="container footer-bottom__content">
                    <div class="grid">
						<div class="footer__copyright">
							<div class="footer__payments footer__payments--pc">
								<img src="<?php echo HTML::media('assets/images/visa.png') ?>" alt="visa">
								<img src="<?php echo HTML::media('assets/images/mastercard.png') ?>" alt="mastercard">
							</div>
							<p>Copyright © 2016 - <?= date('Y') ?> <?= __('babykroha.ua - официальный интернет-магазин детских товаров. Все права защищены') ?></p>
						</div>
<!--						<div class="devs">-->
<!--							<span>--><?php //echo __('Разработка интернет магазина  — Wezom') ?><!--</span>-->
<!--							<a href="https://wezom.com.ua/" target="_blank" class="wezom-link">-->
<!--								<svg>-->
<!--									<use xlink:href="/Media/assets/images/sprites/icons.svg?v=1528183368709#icon-wezom"></use>-->
<!--								</svg>-->
<!--							</a>-->
<!--						</div>-->
					</div>
                </div>
            </div>
        </div>

    </div>

    <div class="go-top toTop-js">
        <svg>
            <use xlink:href="<?php echo HTML::media('assets/images/sprites/icons.svg#arrow-up'); ?>"></use>
        </svg>
    </div>

<div class="preloader l-wrapper">
    <svg style="display: none" viewBox="0 0 150 150" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
        <symbol id="s--trapez">
            <path  d="m0,33.50001l6.03125,-33.50001l20.10417,0l6.03125,33.50001l-32.16667,0z"
                   transform="scale(.8) translate(7,2) rotate(-220, 20,20)"/>
        </symbol>
    </svg>

    <svg viewBox="0 0 150 150">
        <g class="g-circles g-circles--trapez">
            <g class="g--circle">
                <use xlink:href="#s--trapez" class="u--circle"/>
            </g>
            <g class="g--circle">
                <use xlink:href="#s--trapez" class="u--circle"/>
            </g>
            <g class="g--circle">
                <use xlink:href="#s--trapez" class="u--circle"/>
            </g>
            <g class="g--circle">
                <use xlink:href="#s--trapez" class="u--circle"/>
            </g>
            <g class="g--circle">
                <use xlink:href="#s--trapez" class="u--circle"/>
            </g>
            <g class="g--circle">
                <use xlink:href="#s--trapez" class="u--circle"/>
            </g>
            <g class="g--circle">
                <use xlink:href="#s--trapez" class="u--circle"/>
            </g>
            <g class="g--circle">
                <use xlink:href="#s--trapez" class="u--circle"/>
            </g>
            <g class="g--circle">
                <use xlink:href="#s--trapez" class="u--circle"/>
            </g>
            <g class="g--circle">
                <use xlink:href="#s--trapez" class="u--circle"/>
            </g>
            <g class="g--circle">
                <use xlink:href="#s--trapez" class="u--circle"/>
            </g>
            <g class="g--circle">
                <use xlink:href="#s--trapez" class="u--circle"/>
            </g>
    </svg>

    <div class="_text-center">
        Происходит генерация файла выгрузки. <br>
        Подождите еще немного.
    </div>
</div>

<div id="popup_filter" class="filter-summary filter-summary-wrapper">
    <div class="filter-summary__result">
        <?php echo __('Выбрано'); ?>:
        <span class="count"></span>
    </div>
    <button class="filter-summary__submit summary-submit">
        <?php echo __('Применить'); ?>
    </button>
</div>

    <!--<div class="mobile-callback js-init" data-mfp="ajax"-->
    <!--     data-mfp-src="--><?php //echo \Core\HTML::link('popup/getPopup'); ?><!--"-->
    <!--     data-param='--><?php //echo json_encode(['name' => 'Callback', 'param' => []]); ?><!--'>-->
    <!--    <svg>-->
    <!--        <use xlink:href="/Media/assets/images/sprites/icons.svg#phone"></use>-->
    <!--    </svg>-->
    <!--</div>-->

<?php echo Widgets::get('FooterScripts') ?>
