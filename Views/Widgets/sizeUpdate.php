    <div class="product__sizes js-product-counts">
        <?php use Core\HTML;

        foreach ($sizes as $size):?>
            <?php if(!array_search($size->size, $active_sizes)):?>
                <div class="product__size-item js-product-count-item <?php echo(!$size->amount ? 'disable' : ''); ?> <?php echo ($size->amount && $wholeSale) ? 'active' : '' ?> js-init"
                     data-mfp="ajax" data-size="<?php echo $size->size; ?>"
                     data-mfp-src="<?php echo HTML::link('popup/getPopup'); ?>"
                     data-param='<?php echo json_encode(['name' => 'CartSizes', 'param' => ['item_id' => $obj->id, 'counts' => $counts, 'wholeSale' => $wholeSale, 'from' => 1]]); ?>'>
                    <span class="product__size-text"><?php echo $size->size; ?></span>
                    <?php if ($wholeSale && $size->amount): ?>
                        <span class="product__size-count js-product-count"
                              data-max="<?php echo $size->amount; ?>"></span>
                    <?php else: ?>
                        <span class="product__size-count js-product-count" data-max="0"></span>
                    <?php endif; ?>
                </div>
            <?php else:?>
                <div class="product__size-item js-product-count-item <?php echo(!$size->amount ? 'disable' : ''); ?> <?php echo ($size->amount && $wholeSale) ? 'active' : '' ?> js-init"
                     data-mfp="ajax" data-size="<?php echo $size->size; ?>"
                     data-mfp-src="<?php echo HTML::link('popup/getPopup'); ?>"
                     data-param='<?php echo json_encode(['name' => 'CartSizes', 'param' => ['item_id' => $obj->id, 'counts' => $counts, 'wholeSale' => $wholeSale, 'from' => 1]]); ?>'>
                    <span class="size__item"><?php echo $size->size; ?></span>
                    <span class="size__count js-product-count"><?php echo $active_count[$size->size]?></span>
                </div>
            <?php endif?>
        <?php endforeach; ?>
    </div>