<?php

use Core\HTML;
use Core\Support;
use Core\Text;
use Core\User;
use Modules\Catalog\Models\Filter;
use Core\Config;
use Core\Widgets;

$cartPrice = 0;

foreach ($cart as $item) {
    $obj = $item['obj'];
    if ($obj->sizes) {
        $counts = [];
        $count = 0;
        foreach ($obj->sizes as $size) {
            $counts[$size->size] = $size->count;
            $count += $size->count;
        }
    } else {
        $counts = [$item['count']];
        $count = $item['count'];
    }

    $data = Support::calculatePriceWithCurrency($obj,$_currency,$_currentCurrency);
    $cost = $data['cost'];
    $cost_old = $data['cost_old'];
    $currency2 = $data['currency'];
    $cartPrice += $cost * $count;
}
?>
<script type="application/ld+json">
    <?= json_encode($structuredData, JSON_UNESCAPED_UNICODE) ?>
</script>
<?php
$price = 0;
?>
<div class="header">
    <div class="header__top">
        <div class="container">
            <div class="header__top-content header__top-content--pc">
                <div style="display:none">
                    <span class="header-phones__callback callback-button js-init" data-mfp="ajax" data-mfp-src="<?php echo \Core\HTML::link('popup/getPopup'); ?>" data-param='<?php echo json_encode(['name' => 'Callback', 'param' => []]); ?>'><?php echo __('Обратный звонок') ?></span>
                </div>
                <div class="header__nav-wrap">
                    <div class="header__logo">
                        <svg>
                            <use xlink:href="<?php echo HTML::media('/assets/images/sprites/icons.svg#text'); ?>"></use>
                        </svg>
                    </div>
                    <?php if (!empty($contentMenu)) : ?>
                        <div class="header__nav">
                            <?php foreach ($contentMenu as $link) : ?>
                                <a href="<?php echo HTML::link($link->url, true, 'https') ?>" class="header__nav-item"><?= $link->name ?></a>
                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>
                    <div class="currency-block">
                        <?php foreach ($languages as $key => $language): ?>
                            <a href="<?php echo \Core\HTML::changeLanguage($key) ?>"
                               class="currency-block__link <?php echo (I18n::lang() == $key) ? 'is-active' : '' ?>">
                                <?php echo $language['short_name']; ?>
                            </a>
                        <?php endforeach; ?>
                    </div>
                    <?php if (!strpos($currentLink, 'cart')): ?>
                        <div class="currency-block">
                            <?php foreach ($currency as $cur): ?>
                                <a href="#"
                                   class="currency-block__link js-currency <?php echo ($active == $cur->currency) ? 'is-active' : '' ?>"
                                   data-currency="<?php echo $cur->currency; ?>"><?php echo $cur->currency; ?></a>
                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>
                </div>
                <div>
                    <?php if (User::info() && !empty($userTypes)):?>
                        <div class="user-type">
                            <?php foreach ($userTypes as $roleType => $roleName):?>
                                <a href="#" class="user-type__link js-user-type <?php echo ($active_type == $roleType) ? 'is-active' : '' ?>" data-user-type="<?php echo $roleType;?>"><?php echo Text::limit_vowels($roleName,4,'.',false,true);?></a>
                            <?php endforeach;?>
                        </div>
                    <?php endif;?>

                    <?php if (!Core\User::info()): ?>
                        <div class="header-login__wrap js-init" data-mfp="ajax"
                             data-mfp-src="<?php echo \Core\HTML::link('popup/getPopup'); ?>"
                             data-param='<?php echo json_encode(['name' => 'Login', 'param' => []]); ?>'>
                            <span class="header-login__enter"><?php echo __('Войти') ?> </span>
                            <span class="header-login__icon">
                                <img src="/Media/assets/images/catalog/lock.png" alt="">
                            </span>
                        </div>
                    <?php else: ?>
                        <a <?php echo ($currentLink != ('/account') && $currentLink != ('account')) ? 'href="' . HTML::link('account') . '"' : ''; ?>>
                            <div class="header-login__wrap">
                                <span class="header-login__enter"> <?php echo __('Личный кабинет') ?> </span>
                            </div>
                        </a>
                    <?php endif; ?>
                </div>
            </div>
            <div class="header__top-content header__top-content--mob">
                <div class="menu-open__wrap">
                    <button class="menu-open__button">
                            <span class="menu-open__icon">
                                <svg>
                                    <use xlink:href="<?php echo HTML::media('assets/images/sprites/icons.svg#menu-icon'); ?>"></use>
                                </svg>
                            </span>
                    </button>
                    <button class="menu-open__button--mob js-mob-menu">
                            <span class="menu-open__icon">
                               	<img class="menu-open-icon" src="/Media/assets/images/catalog/menu-open.svg" alt="">
								<img class="menu-close-icon" src="/Media/assets/images/catalog/menu-close.svg" alt="">
                            </span>
                    </button>
                </div>

                <div class="header__logo header__logo--mob">
                    <?php if ($currentLink != '/'): ?>
                    <a href="<?php echo HTML::link('/'); ?>">
                        <?php endif; ?>
                        <img src="/Media/assets/images/logo-mob.svg" alt="">
                        <?php if ($currentLink != '/'): ?>
                    </a>
                <?php endif; ?>
                </div>

                <div class="currency">
                    <div class="currency-block">
                        <?php foreach ($languages as $key => $language): ?>
                            <a href="<?php echo \Core\HTML::changeLanguage($key) ?>"
                               class="currency-block__link <?php echo (I18n::lang() == $key) ? 'is-active' : '' ?>">
                                <?php echo $language['short_name']; ?>
                            </a>
                        <?php endforeach; ?>
                    </div>

                    <?php if (!strpos($currentLink, 'cart')): ?>
                        <div class="currency-block">
                            <?php foreach ($currency as $cur): ?>
                                <a href="#"
                                   class="currency-block__link js-currency <?php echo ($active == $cur->currency) ? 'is-active' : '' ?>"
                                   data-currency="<?php echo $cur->currency; ?>"><?php echo $cur->currency; ?></a>
                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>
                </div>

                <?php if (!Core\User::info()): ?>
                    <div class="header-login__wrap js-init" data-mfp="ajax"
                         data-mfp-src="<?php echo \Core\HTML::link('popup/getPopup'); ?>"
                         data-param='<?php echo json_encode(['name' => 'Login', 'param' => []]); ?>'>
                        <span class="header-login__enter"><?php echo __('Войти') ?> </span>
                        <span class="header-login__icon">
                                <img src="/Media/assets/images/catalog/lock.png" alt="">
                            </span>
                    </div>
                <?php else: ?>
                    <a <?php echo ($currentLink != ('/account') && $currentLink != ('account')) ? 'href="' . HTML::link('account') . '"' : ''; ?>>
                        <div class="header-login__wrap">
                            <span class="header-login__enter"> <?php echo __('Личный кабинет') ?> </span>
                        </div>
                    </a>
                <?php endif; ?>

                <button class="header-cart__btn js-cart-open" data-url="<?php echo HTML::link('ajax/cart') ?>">
                    <svg>
                        <use xlink:href="<?php echo HTML::media('/assets/images/sprites/icons.svg#cart-icon'); ?>"></use>
                    </svg>
                    <span class="header-cart__badge badge js-cart-count"><?php echo $count_cart; ?></span>
                </button>
            </div>
            <div class="header__bottom <?php echo $class; ?>">
                <div class="header__bottom-content">
                    <div class="header__nav-wrap">

                        <div class="header__logo">
                            <?php if ($currentLink != '/'): ?>
                            <a href="<?php echo HTML::link('/'); ?>">
                                <?php endif; ?>
                                <img src="/Media/assets/images/logo.svg" alt="">
                                <?php if ($currentLink != '/'): ?>
                            </a>
                        <?php endif; ?>
                        </div>
                        <div class="catalog-button js-catalog-button">
                            <svg>
                                <use xlink:href="<?php echo HTML::media('assets/images/sprites/icons.svg#menu-icon'); ?>"></use>
                            </svg>
                            <span>
                            <?= __('Каталог товаров') ?>
                        </span>
                        </div>
                        <div class="header__search-wrap" data-url="<?php echo HTML::link('ajax/search') ?>">
                            <div class="header__search">
                                <form class="js-init js-form header__search-form _module-loaded"
                                      action="<?php echo HTML::link('search') ?>" method="get">
                                    <div class="header__search-icon">
                                        <svg>
                                            <use xlink:href="<?php echo HTML::media('/assets/images/sprites/icons.svg#search-icon'); ?>"></use>
                                        </svg>
                                    </div>
                                    <input required name="search" data-name="search" id="search" data-rule-minlength="2"
                                           placeholder="Подгузники" autocomplete="off" type="text" class="header__search-input">
                                    <button type="submit" class="header__search-submit">
                                        <span><?php echo __('Найти') ?></span>
                                    </button>
                                </form>
                                <div>
                                    <div class="header__search-empty">
                                        <p class="search-empty__title"><?php echo __('К сожалению, по Вашему запросу ничего не найдено.') ?></p>
                                        <p class="search-empty__sub-title"><?php echo __('Пожалуйста, уточните условия поиска') ?></p>
                                    </div>
                                    <div class="header__search-results js-init" data-scrollbar></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="header__phones-wrap">
                        <a href="tel:+<?php echo Config::get('static.phone'); ?>" class="header__phones-phone header__phones-head" >
                            <?php echo Config::get('static.phone'); ?>
                        </a>
                        <span class="header__phones-svg">
                            <svg>
                                <use xlink:href="<?php echo HTML::media('assets/images/sprites/icons.svg#arrow-up'); ?>"></use>
                            </svg>
                        </span>
                        <div class="header__phones-all">
                            <a href="tel:+<?php echo Config::get('static.phone'); ?>" class="header__phones-phone" ><?php echo Config::get('static.phone'); ?></a>
                            <a href="tel:+<?php echo Config::get('static.phone2'); ?>" class="header__phones-phone" ><?php echo Config::get('static.phone2'); ?></a>
                            <a href="tel:+<?php echo Config::get('static.phone3'); ?>" class="header__phones-phone" ><?php echo Config::get('static.phone3'); ?></a>
                        </div>
                    </div>
                    <div class="header-controls__wrap">
                        <div class="header-search__wrap">
                            <a class="js-init header-favorite__btn" data-mfp="ajax">
                                <svg>
                                    <use xlink:href="<?php echo HTML::media('/assets/images/sprites/icons.svg#compare'); ?>"></use>
                                </svg>
                                <span class="header-favorite__badge badge js-favorite-count">0</span>
                            </a>
                            <?php if (User::info()): ?>
                            <a href="<?php echo HTML::link('account/favorite'); ?>" class="header-favorite__btn">
                                <?php else: ?>
                                <a class="js-init header-favorite__btn" data-mfp="ajax"
                                   data-mfp-src="<?php echo HTML::link('popup/getPopup'); ?>"
                                   data-param='<?php echo json_encode(['name' => 'Login', 'param' => []]); ?>'>
                                    <?php endif; ?>
                                    <svg>
                                        <use xlink:href="<?php echo HTML::media('/assets/images/sprites/icons.svg#favorite-icon'); ?>"></use>
                                    </svg>
                                    <span class="header-favorite__badge badge js-favorite-count"><?php echo (User::info()) ? $count_favorite : 0; ?></span>
                                </a>
                                <button class="header-cart__btn js-cart-open" data-url="<?php echo HTML::link('ajax/cart') ?>">
                                    <svg>
                                        <use xlink:href="<?php echo HTML::media('/assets/images/sprites/icons.svg#cart-icon'); ?>"></use>
                                    </svg>
                                    <span class="header-cart__badge badge js-cart-count"><?php echo $count_cart; ?></span>
                                </button>
                                <span class="header-cart__summ">
                                    <span><b><?= $cartPrice ?></b> <?= $currency2 ?> </span>
                                </span>
                                <?php if (!Core\User::info()): ?>
                                    <div class="header-login__btn js-init" data-mfp="ajax"
                                         data-mfp-src="<?php echo \Core\HTML::link('popup/getPopup'); ?>"
                                         data-param='<?php echo json_encode(['name' => 'Login', 'param' => []]); ?>'>
                                    <span class="header-login__btn-icon">
                                        <svg>
                                            <use xlink:href="<?php echo HTML::media('/assets/images/sprites/icons.svg#account'); ?>"></use>
                                        </svg>
                                    </span>
                                    </div>
                                <?php else: ?>
                                    <a <?php echo ($currentLink != ('/account') && $currentLink != ('account')) ? 'href="' . HTML::link('account') . '"' : ''; ?>>
                                        <div class="header-login__btn">
                                        <span class="header-login__btn-icon">
                                            <svg>
                                                <use xlink:href="<?php echo HTML::media('/assets/images/sprites/icons.svg#lock-icon'); ?>"></use>
                                            </svg>
                                        </span>
                                        </div>
                                    </a>
                                <?php endif; ?>
                        </div>
                    </div>
                </div>
                <div class="container header__bottom-content header__bottom-content--mob">
                    <div class="header__search-wrap" data-url="<?php echo HTML::link('ajax/search') ?>">
                        <div class="header__search container">
                            <form class="js-init js-form header__search-form _module-loaded"
                                  action="<?php echo HTML::link('search') ?>" method="get">
                                <div class="header__search-icon">
                                    <svg>
                                        <use xlink:href="<?php echo HTML::media('/assets/images/sprites/icons.svg#search-icon'); ?>"></use>
                                    </svg>
                                </div>
                                <input required name="search" data-name="search" id="search" data-rule-minlength="2"
                                       placeholder="Подгузники" autocomplete="off" type="text" class="header__search-input">
                                <button type="submit" class="header__search-submit">
                                    <span><?php echo __('Найти') ?></span>
                                </button>
                            </form>
                            <div>
                                <div class="header__search-empty">
                                    <p class="search-empty__title"><?php echo __('К сожалению, по Вашему запросу ничего не найдено.') ?></p>
                                    <p class="search-empty__sub-title"><?php echo __('Пожалуйста, уточните условия поиска') ?></p>
                                </div>
                                <div class="header__search-results js-init" data-scrollbar></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="catalog js-mob-catalog">
        <div class="container">
            <div class="header-controls__wrap">
                <a class="js-init header-favorite__btn" data-mfp="ajax">
                    <svg>
                        <use xlink:href="<?php echo HTML::media('/assets/images/sprites/icons.svg#compare'); ?>"></use>
                    </svg>
                    <span class="header-favorite__badge badge js-favorite-count">0</span>
                </a>
                <?php if (User::info()): ?>
                <a href="<?php echo HTML::link('account/favorite'); ?>" class="header-favorite__btn">
                    <?php else: ?>
                    <a class="js-init header-favorite__btn" data-mfp="ajax"
                       data-mfp-src="<?php echo HTML::link('popup/getPopup'); ?>"
                       data-param='<?php echo json_encode(['name' => 'Login', 'param' => []]); ?>'>
                        <?php endif; ?>
                        <svg>
                            <use xlink:href="<?php echo HTML::media('/assets/images/sprites/icons.svg#favorite-icon'); ?>"></use>
                        </svg>
                        <span class="header-favorite__badge badge js-favorite-count"><?php echo (User::info()) ? $count_favorite : 0; ?></span>
                    </a>
                    <button class="header-cart__btn js-cart-open" data-url="<?php echo HTML::link('ajax/cart') ?>">
                        <svg>
                            <use xlink:href="<?php echo HTML::media('/assets/images/sprites/icons.svg#cart-icon'); ?>"></use>
                        </svg>
                        <span class="header-cart__badge badge js-cart-count"><?php echo $count_cart; ?></span>
                    </button>
                    <span class="header-cart__summ">
                        <span><b><?= $cartPrice ?></b> <?= $currency2 ?> </span>
                    </span>
            </div>
            <div class="catalog__nav">
                <?php foreach ($sidebarMenu as $item) : ?>
                    <a class="catalog__nav-item" href="<?php echo HTML::link($item->url) ?>"><?php echo $item->name; ?></a>
                <?php endforeach; ?>
            </div>
        </div>
        <div class="catalog-menu js-catalog">
            <div class="container">
                <div class="js-close">
                    <img src="/Media/assets/images/catalog/close.svg" alt="">
                </div>
                <div class="catalog-menu__nav">
                    <div class="catalog-menu__head"><?= __('Каталог') ?></div>
                    <div class="catalog__items">
                        <?php if (count($categories)): ?>
                            <?php foreach ($categories[0] as $parentId => $obj): ?>
                                <div class="catalog__item-wrap">
                                    <a href="<?php echo HTML::link('catalog/' . $obj->alias) ?>" class="catalog__item">
                                        <?php // здесь выводить картинку каждой категории ?>
                                        <div class="catalog__image">
                                            <?php if (is_file(HOST . Core\HTML::media('images/catalog_tree/' . $obj->image2, false))): ?>
                                                <img class="hov-image" src="<?= HTML::media('images/catalog_tree/' . $obj->image2) ?>" alt=""> <?php // цветная картинка ?>
                                            <?php else : ?>
                                                <img class="hov-image" src="/Media/assets/images/catalog/girls-c.svg" alt=""> <?php // цветная картинка ?>
                                            <?php endif; ?>
                                            <?php if (is_file(HOST . Core\HTML::media('images/catalog_tree/' . $obj->image, false))): ?>
                                                <img src="<?= HTML::media('images/catalog_tree/' . $obj->image) ?>" alt=""> <?php // бесцветная картинка ?>
                                            <?php else : ?>
                                                <img src="/Media/assets/images/catalog/girls-b.svg" alt=""> <?php // бесцветная картинка ?>
                                            <?php endif; ?>
                                        </div>
                                        <?php // ?>
                                        <div class="catalog__name"><?php echo $obj->name; ?></div>
                                    </a>
                                    <?php if (isset($categories[$obj->id])): ?>
                                        <div class="catalog__sub-item">
                                            <div class="catalog-menu__wrap">
                                                <div class="catalog-menu__sub catalog-menu__sub--bg">
                                                    <a href="<?php echo HTML::link('catalog/' . $obj->alias) ?>" class="catalog-menu__head"><?php echo $obj->name; ?></a>
                                                    <div class="catalog-menu__links catalog-menu__links--overflow">
                                                        <?php
                                                        $subcategories = $categories[$obj->id];
                                                        usort($subcategories, function($a, $b) {
                                                            if ($a->name == $b->name) return 0;
                                                            return ($a->name < $b->name) ? -1 : 1;
                                                        });
                                                        ?>
                                                        <?php foreach ($subcategories as $item): ?>
                                                            <div class="catalog__item-wrap catalog-menu__link-parent">
                                                                <a href="<?php echo HTML::link('catalog/' . $item->alias) ?>" class="catalog-menu__link"><?php echo $item->name; ?></a>
                                                                <?php if (isset($categories[$item->id])): ?>
                                                                    <span class="catalog__item-arrow" data-catalog-item="<?php echo $obj->alias ?>">
																		<img src="/Media/assets/images/catalog/arrow-right.svg" alt="">
																	</span>
                                                                    <div class="catalog__child-menu">
                                                                        <div class="catalog-menu__sub">
                                                                            <div class="catalog-menu__head"><?= __('Товары категории') ?></div>
                                                                            <div class="catalog-menu__links catalog-menu__category-links">
                                                                                <?php
                                                                                $subcategories = $categories[$item->id];
                                                                                usort($subcategories, function($a, $b) {
                                                                                    if ($a->name == $b->name) return 0;
                                                                                    return ($a->name < $b->name) ? -1 : 1;
                                                                                });
                                                                                ?>
                                                                                <?php foreach ($subcategories as $sub): ?>
                                                                                    <a href="<?php echo HTML::link('catalog/' . $sub->alias) ?>" class="catalog-menu__link catalog-menu__link-child"><?php echo $sub->name; ?></a>
                                                                                <?php endforeach; ?>
                                                                                <a href="<?php echo HTML::link('catalog/' . $item->alias) ?>" class="catalog-menu__link-category"><?= __('Перейти в раздел') ?></a> <?php // эта ссылка должна быть всегда ?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                <?php endif; ?>
                                                            </div>
                                                        <?php endforeach; ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="catalog-menu catalog-menu--category js-catalog-category">
            <div class="container">
                <div class="js-close">
                    <img src="/Media/assets/images/catalog/close.svg" alt="">
                </div>
            </div>
        </div>
    </div>

    <div class="_hide">
        <nav class="sidebar" id="menu">
            <ul class="sidebar-list">
                <li class="sidebar-close__wrap">
                    <button class="sidebar-close">
                        <svg>
                            <use xlink:href="<?php echo HTML::media('assets/images/sprites/icons.svg#close-icon'); ?>"></use>
                        </svg>
                    </button>
                </li><?php foreach ($sidebarMenu as $sidebar): ?>
                    <li class="sidebar-menu__item">
                        <a href="<?php echo Core\HTML::link($sidebar->url); ?>" class="sidebar-menu__link">
                            <span><?php echo($sidebar->name); ?></span>
                        </a>
                    </li>
                <?php endforeach; ?>
            </ul>
        </nav>
    </div>

    <div class="_hide">
        <nav class="mobile-menu" id="mobile-menu">
            <ul class="mobile-menu-list">
                <li class="mobile-menu__item mobile-menu__item--search">
                    <form class="js-form mobile-search__form js-init"
                          data-ajax="<?php echo HTML::link('ajax/searchFilter') ?>">
                        <input type="text" class="mobile-search__input" data-name="search">
                        <?php echo HTML::csrfField(); ?>
                        <button class="mobile-search__btn"><?php echo __('Поиск') ?></button>

                    </form>
                </li>
                <?php if (!Core\User::info()): ?>
                    <li class="mobile-menu__item js-init" data-mfp="ajax"
                        data-mfp-src="<?php echo \Core\HTML::link('popup/getPopup'); ?>"
                        data-param='<?php echo json_encode(['name' => 'Login', 'param' => []]); ?>'>
                        <span class="mobile-menu__callback"><?php echo __('Регистрация / Вход') ?></span>
                    </li>
                <?php else: ?>
                    <li class="mobile-menu__item">
                        <a <?php echo ($currentLink != ('/account') && $currentLink != ('account')) ? 'href="' . HTML::link('account') . '"' : ''; ?>>
                            <span class="mobile-menu__callback"> <?php echo __('Личный кабинет') ?> </span>
                        </a>
                    </li>
                <?php endif; ?>
                <li class="mobile-menu__item">
                    <a class="mobile-menu__link mobile-menu__link--head">Каталог</a>
                    <ul class="mobile-menu__catalog-items">
                        <?php foreach ($mobileMenu as $obj):
                            if (!$obj->url) {
                                $obj->url = 'catalog/' . $obj->alias;
                            }
                            ?>
                            <li class="mobile-menu__item">
                                <a href="<?php echo HTML::link($obj->url) ?>" class="mobile-menu__link">
                                    <span><?php echo $obj->name; ?></span>
                                </a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </li>
                <li class="mobile-menu__item">
                    <a href="tel:+<?php echo Config::get('static.phone'); ?>"
                       class="mobile-menu__phone"><?php echo Config::get('static.phone'); ?></a>
                </li>
                <li class="mobile-menu__item">
                    <a href="tel:+<?php echo Config::get('static.phone2'); ?>"
                       class="mobile-menu__phone"><?php echo Config::get('static.phone2'); ?></a>
                </li>
                <li class="mobile-menu__item">
                    <a href="tel:+<?php echo Config::get('static.phone3'); ?>"
                       class="mobile-menu__phone"><?php echo Config::get('static.phone3'); ?></a>
                </li>
                <span class="callback-button callback-button--mob js-init" data-mfp="ajax"
                      data-mfp-src="<?php echo \Core\HTML::link('popup/getPopup'); ?>"
                      data-param='<?php echo json_encode(['name' => 'Callback', 'param' => []]); ?>'>
                    <?php echo __('Обратный звонок') ?>
                </span>
            </ul>
        </nav>
    </div>

    <div class="cart-wrap">
        <div class="cart-holder">
            <button class="js-cart-close cart-close-btn">
                <svg>
                    <use xlink:href="<?php echo HTML::media('/assets/images/sprites/icons.svg#close-icon'); ?>"></use>
                </svg>
            </button>
            <div class="cart-inner js-cart"></div>
        </div>
    </div>
</div>
