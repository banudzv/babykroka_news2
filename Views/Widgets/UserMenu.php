<?php
use Core\Route;
use Core\HTML;

?>
<div class="gcell--24 gcell--sm-6 profile__sidebar">
    <div class="grid profile__sidebar-items">
        <a href="<?php echo HTML::link('account'); ?>" class="profile__sidebar-item <?php echo Route::action() == 'index' ? 'active' : ''; ?>">
            <span><?php echo __('Личные данные') ?></span>
        </a>
        <a href="<?php echo HTML::link('children'); ?>" class="profile__sidebar-item <?php echo Route::action() == 'children' ? 'active' : ''; ?>">
            <span><?php echo __('Информация о детях') ?></span>
        </a>
        <a href="<?php echo HTML::link('account/orders'); ?>" class="profile__sidebar-item <?php echo Route::action() == 'orders' ? 'active' : ''; ?>">
            <span><?php echo __('История заказов') ?></span>
        </a>
        <?php if(\Core\User::isOpt() || \Core\User::isDrop()):?>
        <a href="<?php echo HTML::link('account/export'); ?>" class="profile__sidebar-item <?php echo Route::action() == 'export' || Route::action() == 'prom' ? 'active' : ''; ?>">
            <span><?php echo __('Выгрузки/Экспорт') ?></span>
        </a>
        <?php endif;?>
        <a href="<?php echo HTML::link('account/favorite'); ?>" class="profile__sidebar-item <?php echo Route::action() == 'favorite' ? 'active' : ''; ?>">
            <span><?php echo __('Избранные товары') ?></span>
        </a>
        <a href="<?php echo HTML::link('account/logout'); ?>" class="profile__sidebar-item">
            <span><?php echo __('Выход') ?></span>
        </a>
    </div>
</div>
