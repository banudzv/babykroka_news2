<?php

use Core\HTML;
use Core\Arr;

?>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php echo isset($title) ? $title : NULL; ?></title>
<?php if (!$hide_meta): ?>
    <meta name="description" lang="ru" content="<?php echo isset($description) ? $description : NULL; ?>">
    <meta name="keywords" lang="ru" content="<?php echo isset($keywords) ? $keywords : NULL; ?>">
<?php endif; ?>
<?php if ($noindex): ?>
    <meta name="robots" content="noindex,follow">
<?php endif; ?>
<?php if ($nofollow): ?>
    <meta name="robots" content="noindex,nofollow">
<?php endif; ?>
<?php if ($canonical): ?>
    <link rel="canonical" href="<?php echo HTML::link($canonical, true,  $_SERVER['REQUEST_SCHEME']); ?>"/>
<?php endif; ?>
<?php if ($next): ?>
    <link rel="next" href="<?php echo HTML::link($next, true); ?>"/>
<?php endif; ?>
<?php if ($prev): ?>
    <link rel="prev" href="<?php echo HTML::link($prev, true); ?>"/>
<?php endif; ?>
<?php if (!empty($alternates)) : ?>
<?php foreach ($alternates as $key => $alternate) : ?>
    <link rel="alternate" hreflang="<?= $key ?>" href="<?= $alternate ?>" />
<?php endforeach; ?>
<?php endif; ?>

<?php if ($_SERVER['REQUEST_URI']==='/'):?>
    <meta name="google-site-verification" content="AOlw5w4JUUptRj2S6aAhRZ3J8dXoHv6mDzgv9Ds7bwI" />
<?php endif;?>
<meta property="fb:app_id" content="false" />
<meta property="og:type" content="website" />
<meta property="og:url" content="<?php echo HTML::link($canonical, true,  $_SERVER['REQUEST_SCHEME']); ?>" />
<meta property="og:title" content="<?php echo $title; ?>" />
<meta property="og:image" content="<?php echo HTML::media(($image)?: 'logo.png', true, $_SERVER['REQUEST_SCHEME'])?>" />
<meta property="og:description" content="<?php echo $description; ?>" />

<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport" content="width=1000">
<meta http-equiv="imagetoolbar" content="no">
<meta name="format-detection" content="telephone=no">
<meta name="format-detection" content="address=no">
<meta name="HandheldFriendly" content="True">
<meta name="MobileOptimized" content="320">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

<link rel="apple-touch-icon" sizes="180x180" href="<?php echo HTML::media('assets/favicons/apple-touch-icon.png'); ?>">
<link rel="icon" type="image/png" sizes="32x32" href="<?php echo HTML::media('assets/favicons/favicon-32x32.png'); ?>">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo HTML::media('assets/favicons/favicon-16x16.png'); ?>">
<link rel="manifest" href="<?php echo HTML::media('assets/favicons/manifest.json'); ?>">
<link rel="mask-icon" href="<?php echo HTML::media('assets/favicons/safari-pinned-tab.svg'); ?>" color="#eb2429">
<meta name="msapplication-TileColor" content="#da532c">
<meta name="msapplication-TileImage" content="<?php echo HTML::media('assets/favicons/mstile-144x144.png'); ?>">
<meta name="theme-color" content="#ffffff">
<meta name="msapplication-config" content="<?php echo HTML::media('assets/favicons/browserconfig.xml'); ?>">

<style>
    html {
        line-height: 1.15;
        -ms-text-size-adjust: 100%;
        -webkit-text-size-adjust: 100%
    }

    body {
        margin: 0
    }

    article, aside, footer, header, nav, section {
        display: block
    }

    h1 {
        font-size: 2em;
        margin: .67em 0
    }

    figcaption, figure, main {
        display: block
    }

    figure {
        margin: 1em 40px
    }

    hr {
        -webkit-box-sizing: content-box;
        box-sizing: content-box;
        height: 0;
        overflow: visible
    }

    pre {
        font-family: monospace, monospace;
        font-size: 1em
    }

    a {
        background-color: transparent;
        -webkit-text-decoration-skip: objects
    }

    abbr[title] {
        border-bottom: none;
        text-decoration: underline;
        -webkit-text-decoration: underline dotted;
        text-decoration: underline dotted
    }

    b, strong {
        font-weight: inherit;
        font-weight: bolder
    }

    code, kbd, samp {
        font-family: monospace, monospace;
        font-size: 1em
    }

    dfn {
        font-style: italic
    }

    mark {
        background-color: #ff0;
        color: #000
    }

    small {
        font-size: 80%
    }

    sub, sup {
        font-size: 75%;
        line-height: 0;
        position: relative;
        vertical-align: baseline
    }

    sub {
        bottom: -.25em
    }

    sup {
        top: -.5em
    }

    audio, video {
        display: inline-block
    }

    audio:not([controls]) {
        display: none;
        height: 0
    }

    img {
        border-style: none
    }

    svg:not(:root) {
        overflow: hidden
    }

    button, input, optgroup, select, textarea {
        font-family: sans-serif;
        font-size: 100%;
        line-height: 1.15;
        margin: 0
    }

    button, input {
        overflow: visible
    }

    button, select {
        text-transform: none
    }

    [type=reset], [type=submit], button, html [type=button] {
        -webkit-appearance: button
    }

    [type=button]::-moz-focus-inner, [type=reset]::-moz-focus-inner, [type=submit]::-moz-focus-inner, button::-moz-focus-inner {
        border-style: none;
        padding: 0
    }

    [type=button]:-moz-focusring, [type=reset]:-moz-focusring, [type=submit]:-moz-focusring, button:-moz-focusring {
        outline: 1px dotted ButtonText
    }

    fieldset {
        padding: .35em .75em .625em
    }

    legend {
        -webkit-box-sizing: border-box;
        box-sizing: border-box;
        color: inherit;
        display: table;
        max-width: 100%;
        padding: 0;
        white-space: normal
    }

    progress {
        display: inline-block;
        vertical-align: baseline
    }

    textarea {
        overflow: auto
    }

    [type=checkbox], [type=radio] {
        -webkit-box-sizing: border-box;
        box-sizing: border-box;
        padding: 0
    }

    [type=number]::-webkit-inner-spin-button, [type=number]::-webkit-outer-spin-button {
        height: auto
    }

    [type=search] {
        -webkit-appearance: textfield;
        outline-offset: -2px
    }

    [type=search]::-webkit-search-cancel-button, [type=search]::-webkit-search-decoration {
        -webkit-appearance: none
    }

    ::-webkit-file-upload-button {
        -webkit-appearance: button;
        font: inherit
    }

    details, menu {
        display: block
    }

    summary {
        display: list-item
    }

    canvas {
        display: inline-block
    }

    [hidden], template {
        display: none
    }

    html {
        position: relative;
        height: 100%;
        font-family: 'Google Sans', sans-serif;
        color: #343434;
        line-height: 1.6;
        font-size: 12px
    }

    *, :after, :before {
        -webkit-box-sizing: inherit;
        box-sizing: inherit
    }

    body {
        position: relative;
        height: 100%;
        -webkit-box-sizing: border-box;
        box-sizing: border-box;
        min-width: 320px;
        background-color: #fff
    }

    body.overflow {
        overflow: hidden;
    }

    pre {
        padding: 1.2em;
        -moz-tab-size: 4;
        -o-tab-size: 4;
        tab-size: 4;
        border-width: 0;
        white-space: pre-wrap;
        word-wrap: break-word;
        font-family: Consolas, Liberation Mono, Menlo, Courier, monospace
    }

    img {
        max-width: 100%;
        height: auto
    }

    small {
        display: inline-block;
        vertical-align: text-bottom
    }

    a svg, button svg {
        pointer-events: none
    }

    audio[controls] {
        display: block;
        width: 100%
    }

    iframe, video {
        max-width: 100%
    }

    hr {
        border-width: 0;
        border-top: 1px solid
    }

    ::-moz-selection {
        text-shadow: none;
        color: #fff;
        background: #2597ff
    }

    ::selection {
        text-shadow: none;
        color: #fff;
        background: #2597ff
    }

    button, input, select, textarea {
        font-family: 'Google Sans';
        outline: none
    }

    [type=reset], [type=submit], button, html [type=button] {
        -webkit-appearance: none
    }

    button {
        cursor: pointer
    }

    a, button {
        outline: none
    }

    a {
        color: inherit;
        text-decoration: none
    }

    @media only screen and (min-width: 481px) {
        html {
            font-size: 10px
        }
    }

    @media only screen and (min-width: 768px) {
        html {
            font-size: 10px
        }
    }

    @media only screen and (min-width: 1024px) {
        html {
            font-size: 12px
        }
    }

    @media only screen and (min-width: 1281px) {
        html {
            font-size: 14px
        }
    }

    @media only screen and (min-width: 1901px) {
        html {
            font-size: 16px
        }
    }
</style>


<script>
    !function (a, b) {
        "function" == typeof define && define.amd ? define([], function () {
            return a.svg4everybody = b()
        }) : "object" == typeof exports ? module.exports = b() : a.svg4everybody = b()
    }(this, function () {
        function a(a, b) {
            if (b) {
                var c = document.createDocumentFragment(), d = !a.getAttribute("viewBox") && b.getAttribute("viewBox");
                d && a.setAttribute("viewBox", d);
                for (var e = b.cloneNode(!0); e.childNodes.length;) c.appendChild(e.firstChild);
                a.appendChild(c)
            }
        }

        function b(b) {
            b.onreadystatechange = function () {
                if (4 === b.readyState) {
                    var c = b._cachedDocument;
                    c || (c = b._cachedDocument = document.implementation.createHTMLDocument(""), c.body.innerHTML = b.responseText, b._cachedTarget = {}), b._embeds.splice(0).map(function (d) {
                        var e = b._cachedTarget[d.id];
                        e || (e = b._cachedTarget[d.id] = c.getElementById(d.id)), a(d.svg, e)
                    })
                }
            }, b.onreadystatechange()
        }

        function c(c) {
            function d() {
                for (var c = 0; c < l.length;) {
                    var g = l[c], h = g.parentNode;
                    if (h && /svg/i.test(h.nodeName)) {
                        var i = g.getAttribute("xlink:href");
                        if (e && (!f.validate || f.validate(i, h, g))) {
                            h.removeChild(g);
                            var m = i.split("#"), n = m.shift(), o = m.join("#");
                            if (n.length) {
                                var p = j[n];
                                p || (p = j[n] = new XMLHttpRequest, p.open("GET", n), p.send(), p._embeds = []), p._embeds.push({
                                    svg: h,
                                    id: o
                                }), b(p)
                            } else a(h, document.getElementById(o))
                        }
                    } else ++c
                }
                k(d, 67)
            }

            var e, f = Object(c), g = /\bTrident\/[567]\b|\bMSIE (?:9|10)\.0\b/, h = /\bAppleWebKit\/(\d+)\b/,
                i = /\bEdge\/12\.(\d+)\b/;
            e = "polyfill" in f ? f.polyfill : g.test(navigator.userAgent) || (navigator.userAgent.match(i) || [])[1] < 10547 || (navigator.userAgent.match(h) || [])[1] < 537;
            var j = {}, k = window.requestAnimationFrame || setTimeout, l = document.getElementsByTagName("use");
            e && d()
        }

        return c
    });

    ;(function () {
        document.addEventListener("DOMContentLoaded", function () {
            svg4everybody()
        })
    })();
</script>
<script>"use strict";
    !function (o) {
        o.localStorageSupport = function (t) {
            try {
                return o.localStorage.setItem(t, t), o.localStorage.removeItem(t), !0
            } catch (t) {
                return !1
            }
        }("test-key"), o.localStorageWrite = function (t, e) {
            try {
                o.localStorage.setItem(t, e)
            } catch (t) {
                console.warn("localStorageWrite"), console.warn(t)
            }
        }
    }(window);</script>
<script>"use strict";
    !function (s) {
        function f(t) {
            var e = document.createElement("style");
            e.rel = "stylesheet", document.getElementsByTagName("head")[0].appendChild(e);
            try {
                e.innerText = t, e.innerHTML = t
            } catch (A) {
                try {
                    e.styleSheet.cssText = t
                } catch (t) {
                    console.warn(A), console.warn(t)
                }
            }
        }

        function i(A, e, o) {
            var a = new s.XMLHttpRequest;
            a.open("GET", A), a.onload = function () {
                if (200 <= a.status && a.status < 400) {
                    var t = a.responseText;
                    f(t), s.localStorageSupport && (s.localStorageWrite(e, A), s.localStorageWrite(o, t))
                } else console.warn("request loadExternalFont - fail")
            }, a.send()
        }

        s.loadFontsToLocalStorage = function (t, A, e) {
            var o = t + "-x-font-", a = o + "url", n = o + "css", l = e && function () {
                if (!s.FontFace) return !1;
                var t = "undefined" != typeof InstallTrigger ? 'url("data:application/font-woff2") format("woff2")' : 'url(data:application/font-woff2;charset=utf-8;base64,d09GMgABAAAAAAPMAAsAAAAACbAAAAOBAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAABlYAhU4RCAooQgsIAAE2AiQDDAQgBYlfBykb+wjILgpsY9rIRchcpL+zqLG4GAoeCOXBvxxKS1CtkT13+88BREUkEwWIOopZJQqQXCRyeQL3YNCcftYasfGNOEnLagBkPwT/RVJmaopA6MkZASgX/1jgmfvWsYADwGXMg//19xpvNumUeUBplAn4nyj92cyjTe/6AVNxAxhMKKKxRh0w15rojR/gERzbLehZ+RZSQCN7MKmAXCGS2KOA9Dg9Jm8ojdFWt5wUhBTPxuEPIHNI679wQZWBCP9AHFwDQIYc9QUE6gtmdzAIBDXXdOG4cy675a4nXvvopwohDmNl7MseZe+zj63roxO9GMAIRCSxhJtqcMhxJ11wzR0PPfPWF79DGg+i66Ed3YArsVz1VJgpfdCfULWlilUmKztWtkeAL++/POXLvS/jCAaVRGJIrMWImBCTYmWsi21xBuIeAHEtbsSdeAD88WFWVBuGORRCFilGKSbO1JdIZSV1ilmNovI1r71euv/thbdAINj3eZ7ZoP/FZcXg5bBR08y5Z6RFvia9RrEMBOPrpgnkawQAIE+sgQSBPORywH5z574FoBciQMj0IoBMkT4EkKtlMAEkOFKgpy1Z6LS/1FAVAZFymXK3NShQtb1wIYqvsAjfuBjF/9gSLCoiIcXiSKZmLI/kWlaVCY4UmNYBWYiFSq3qvyzNl63Mt6wsR6GmKs/WQ21VM9sN9VTdncv6frlBHHhvbMhNwuFDbgbjq7GFbIVxe7/4iXvgtq+yOL+yOG5Z7qTKy9HSQzucjcvWY8uOXgHy4zN/Q6Y3VG0rl30bfMmTX1lnyqnkAeqCNCOKbAbLaiE+FYt+Z51e8xIrNK230/usiXWRPsKvr6asR2ciB6l0xSpP33hMy+gPkx1cho/ycIpyNcznYP6scD70UA7FaKgM7RzML+f384d+hdv5nfycccpSdAZKpYNLrY0p4/IyQMX5UiimbNwMkIkkUfyUeR4PHLCZLDlZer8uS5dRoNN4ZKtlyvPyQUIj6QT+flk2jgHJDJHoxCg1xdfwKfgqxE3lh7SajQk5pvkazNB5dBQ/7YjFlgUGjsmBorMFqowfyFkayON+xkyt+MwswiYGGYhyJKZABuen1uqhNm2LgMmmLqi4ViM6Yxvn3yxr0RkdY7QEUUusuS2TlDbTsDS42f6xPDyj20EVUBqGm5eRkcfkUG1A1XbzEAEAIJ9+FhkA) format("woff2")',
                    A = new s.FontFace("t", t, {});
                return A.load().catch(function () {
                }), "loading" === A.status || "loaded" === A.status
            }() ? e : A;
            if (s.localStorageSupport) {
                var r = s.localStorage[a], c = s.localStorage[n];
                c && r === l ? f(c) : i(l, a, n)
            } else i(l, a, n)
        }
    }(window);
    ;(function (window) {
        window.loadFontsToLocalStorage('Fonts-WEZOM-STARTER-TEMPLATE-DEFAULT', '/Media/assets/css/static/fonts/b64-woff.css?v=1523945764591', '/Media/assets/css/static/fonts/b64-woff2.css?v=1523945764595');
    })(window);</script>
<style id="webpack-style-loader-insert-before-this"></style>
<?php foreach ($css as $file_style): ?>
    <?php echo HTML::style($file_style) . "\n"; ?>
<?php endforeach; ?>
<?php foreach ($scripts_no_minify as $file_script): ?>
    <?php echo HTML::script($file_script) . "\n"; ?>
<?php endforeach; ?>

<!-- Global site tag (gtag.js) - Google Ads: 331964052 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-331964052"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'AW-331964052');
</script>

<?php if (!empty($adWords)) : ?>
    <script>
        gtag('event', 'page_view', <?= json_encode($adWords, JSON_PRETTY_PRINT) ?>);
    </script>
<?php endif; ?>

<script>
        (function(w,d,u){
                var s=d.createElement('script');s.async=true;s.src=u+'?'+(Date.now()/60000|0);
                var h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);
        })(window,document,'https://bt.babykroha.com/upload/crm/site_button/loader_2_ebo0u5.js');
</script>

<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-194127265-1');
</script>

<?php if (!empty($eCommerce)) : ?>
    <script>
        gtag('event', 'purchase', <?= json_encode($eCommerce, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE) ?>);
    </script>
<?php endif; ?>
