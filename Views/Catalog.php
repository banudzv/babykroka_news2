<?php

use Core\Route;
use Core\Widgets;
use Core\Config;
use Core\Arr;
use Core\HTML;

?>
<!DOCTYPE html>
<html lang="ru" dir="ltr">
<!-- (c) студия Wezom | www.wezom.com.ua -->
<head>
    <?php echo Widgets::get('Head', $_seo); ?>
    <?php foreach ( $_seo['scripts']['head'] as $script ): ?>
        <?php echo $script; ?>
    <?php endforeach ?>
</head>
<body class="fx">
    <?php foreach ( $_seo['scripts']['body'] as $script ): ?>
        <?php echo $script; ?>
    <?php endforeach ?>
    <div class="wrapper">
        <div class="section section--main _flex-grow">
            <?php echo Widgets::get('Header'); ?>
            <div class="section gray-bg">
                <div class="container">
                    <div class="catalog__title-box">
                        <?php echo $_breadcrumbs; ?>
                        <h1 class="catalog-title main-title"><?php echo Arr::get($_seo, 'h1'); ?></h1>
                    </div>
                    <?php if (!Config::get('nofilter')):?>
                        <div class="catalog__mobile-filters">
                            <div class="_hide">
                                <nav id="filters-menu">
                                    <?php echo Widgets::get('Groups_CatalogMenuLeftMobile'); ?>
                                </nav>
                            </div>
                            <div class="mobile__summary filter-summary-wrapper">
                                <span class="mobile__summary-title">
                                    <?php echo __('Выбрано'); ?>:
                                    <span class="count"></span>
                                </span>
                                <button class="mobile__summary-submit summary-submit">
                                    <?php echo __('Применить'); ?>
                                </button>
                            </div>
                        </div>
                    <?php endif;?>
                    <div class="<?php echo (Config::get('nofilter'))? 'category-block__wrap': 'grid'?>">
                        <?php if (!Config::get('nofilter')):?>
                        <?php echo Widgets::get('Groups_CatalogMenuLeft'); ?>
                        <?php endif;?>
                        <div class="<?php echo (Config::get('nofilter'))? 'category-block grid': 'gcell--24 gcell--def-18 gcell--lg-20 catalog__items-wrap'?>">
                            <?php if (!Config::get('nofilter')):?>
                            <div class="catalog__items-top">
                                <button class="filters-menu-open yellow-btn">
                                    <?php echo __('Фильтры') ?>
                                </button>

                                <?= Widgets::get('Groups_AddChild', ['class' => '_def-show']) ?>

                                <button class="child-add__wrap js-init _def-hide" data-mfp="ajax"
                                        data-mfp-src="<?php echo \Core\HTML::link('popup/getPopup'); ?>"
                                        data-param='<?php echo json_encode(['name' => 'ChildSelection', 'param' => [
                                                'filter' => Route::param('filter'),
                                                'controller' => Route::controller(),
                                                'category_id' => Route::param('group'),
                                                'search' => $_GET['search']
                                        ]]); ?>'>
                                    <div class="child-add__info">
                                        <div class="child-creation__button">
                                            <div class="child-add__pic"></div>
                                        </div>
                                    </div>
                                </button>

                                <div class="catalog__items-select-box select2-wrap">

                                    <select id="" class="select2">
                                        <option value="<?php echo HTML::sortLink('sort', 'priceAsc');?>" <?php echo ($sort == 'priceAsc') ?'selected':'' ?>><?php echo __('По цене (по возростанию)') ?></option>
                                        <option value="<?php echo HTML::sortLink('sort', 'priceDesc');?>" <?php echo ($sort == 'priceDesc') ?'selected':'' ?>><?php echo __('По цене (по убыванию)') ?></option>
                                        <option value="<?php echo HTML::sortLink('sort', 'top');?>" <?php echo ($sort == 'top') ?'selected':'' ?>><?php echo __('По популярности') ?></option>
                                        <option value="<?php echo HTML::sortLink('sort', 'alph');?>" <?php echo ($sort == 'alph') ?'selected':'' ?>><?php echo __('По алфавиту') ?></option>
                                    </select>
                                </div>
                            </div>
                            <?php endif;?>
                            <?php echo $_content; ?>
                        </div>
                    </div>
                </div>

                <?= Widgets::get('Groups_Dropshipping') ?>

                <?php if($seotext = Arr::get($_seo, 'seo_text')): ?>
                <div class="index-seo-text">
                    <div class="container grid">
                        <div class="gcell--24 index-seo-text__content">
                            <div class="text-wrapper js-init catalog-text-wrapper" data-scrollbar>
                                <?php echo $seotext; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endif; ?>

            </div>
        </div>
        <?php echo  Widgets::get('HiddenData'); ?>
        <?php echo Widgets::get('Footer', ['counters' => Arr::get($_seo, 'scripts.counter'), 'config' => $_config]); ?>
        <?php echo $GLOBAL_MESSAGE; ?>
    </div>
</body>
</html>
