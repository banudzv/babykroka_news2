<?php
use Core\Widgets;
use Core\Config;
use Core\Arr;
?>
<!DOCTYPE html>
<html lang="ru" dir="ltr">
<!-- (c) студия Wezom | www.wezom.com.ua -->
<head>
    <?php echo Widgets::get('Head', $_seo); ?>
    <?php foreach ($_seo['scripts']['head'] as $script): ?>
        <?php echo $script; ?>
    <?php endforeach ?>
</head>
<body>
<?php foreach ($_seo['scripts']['body'] as $script): ?>
    <?php echo $script; ?>
<?php endforeach ?>

<div class="wrapper">
    <div class="section section--main _flex-grow">
        <?php echo Widgets::get('Header'); ?>
        <div class="section gray-bg _pb-100">
            <div class="container">
                <div class="<?php echo Config::get('class'); ?>__title-wrap">
                    <?php echo $_breadcrumbs; ?>
                    <h1 class="main-title <?php echo Config::get('class'); ?>__title"><?php echo Arr::get($_seo, 'h1'); ?></h1>
                </div>

            </div>
            <?php echo $_content; ?>
        </div>
        <?php if (trim(strip_tags(Arr::get($_seo, 'seo_text')))): ?>
            <div class="seoTxt" id="seoTxt">
                <div class="wSize wTxt">
                    <?php echo Arr::get($_seo, 'seo_text'); ?>
                </div>
            </div>

        <?php endif ?>

    </div>
    <?php echo Widgets::get('HiddenData'); ?>
    <?php echo Widgets::get('Footer', ['counters' => Arr::get($_seo, 'scripts.counter'), 'config' => $_config]); ?>
	<?php echo $GLOBAL_MESSAGE; ?>
</div>
</body>
</html>
<?php die(); ?>
