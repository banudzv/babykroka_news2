<?php
use Core\Widgets;
use Core\HTML;
?>
<!DOCTYPE html>
<html lang="ru" dir="ltr">
<!-- (c) студия Wezom | www.wezom.com.ua -->
<head>
    <?php echo Widgets::get('Head', $_seo); ?>
    <?php foreach ($_seo['scripts']['head'] as $script): ?>
        <?php echo $script; ?>
    <?php endforeach ?>
</head>
<body>
<div class="wrapper">
    <div class="section _flex-grow">
        <?php echo Widgets::get('Header'); ?>
        <div class="section">
            <div class="container error__content">
                <div class="error__title">404</div>
                <p class="error__text"><?php echo __('К сожалению, страница не найдена') ?></p>
                <p class="error__sub-text"></p>
                <div class="error__controls">
                    <a href="<?php echo HTML::link('/catalog')?>" class="yellow-btn error__link"><?php echo __('Каталог') ?></a>
                    <a href="<?php echo HTML::link('/')?>" class="yellow-border-btn error__link"><?php echo __('Главная страница') ?></a>
                </div>
            </div>
        </div>
    </div>

<?php echo Widgets::get('FooterScripts') ?>
<?php echo $GLOBAL_MESSAGE; ?>
</div>
</body>
</html>
