<?php
use Core\Widgets;
use Core\Arr;
?>
<!DOCTYPE html>
<html lang="ru" dir="ltr">
<!-- (c) студия Wezom | www.wezom.com.ua -->
<head>
    <?php echo Widgets::get('Head', $_seo); ?>
    <?php foreach ($_seo['scripts']['head'] as $script): ?>
        <?php echo $script; ?>
    <?php endforeach ?>
</head>
<body>
<?php foreach ($_seo['scripts']['body'] as $script): ?>
    <?php echo $script; ?>
<?php endforeach ?>
<div class="wrapper">
    <div class="section section--main _flex-grow">
        <?php echo Widgets::get('CartHeader'); ?>
        <div class="section gray-bg">
            <div class="container">
                <div class="confirm__title-wrap">
                    <h1 class="main-title faq__title"><?php echo $_seo['h1'] ?></h1>
                </div>
                <div class="order-info">
                    <div class="order-info__item <?php echo \Core\Config::get('step_one') ?>">
                        <div class="order-info__item-icon"></div>
                        <p class="order-info__item-text"><?php echo __('Корзина') ?></p>
                    </div>
                    <div class="order-info__item <?php echo \Core\Config::get('step_two') ?>">
                        <div class="order-info__item-icon"></div>
                        <p class="order-info__item-text"><?php echo __('Оформление заказа') ?></p>
                    </div>
                    <div class="order-info__item <?php echo \Core\Config::get('step_pay') ?>">
                        <div class="order-info__item-icon"></div>
                        <p class="order-info__item-text"><?php echo __('Оплата заказа') ?></p>
                    </div>
                    <div class="order-info__item <?php echo \Core\Config::get('step_three') ?>">
                        <div class="order-info__item-icon"></div>
                        <p class="order-info__item-text"><?php echo __('Заказ оформлен') ?></p>
                    </div>
                </div>
                <?php echo $_content; ?>
        </div>
    </div>
        <?php echo Widgets::get('HiddenData'); ?>
        <?php echo Widgets::get('Footer', ['counters' => Arr::get($_seo, 'scripts.counter'), 'config' => $_config]); ?>
        <?php echo $GLOBAL_MESSAGE; ?>
</div>

</body>
</html>
