<?php use Core\HTML;
use Core\Text; ?>
<div class="articles__box">
    <div class="container">
        <?php foreach ($result as $obj): ?>
            <div class="articles-item grid">
                <div class="gcell--md-12 articles-item__img-wrap">
                    <a href="<?php echo HTML::link('articles/' . $obj->alias); ?>" class="articles-item__img-link">

                        <?php if (is_file(HOST . Core\HTML::media('images/articles/original/'.$obj->image, false))): ?>
                            <img src="<?php echo HTML::media('/images/articles/original/' . $obj->image); ?>" alt="<?php echo $obj->name; ?>" class="lozad">
                        <?php else: ?>
                            <img  src="<?php echo HTML::media('assets/images/placeholders/no-image.jpg'); ?>" alt="<?php echo $obj->name; ?>">
                        <?php endif; ?>
                    </a>
                </div>
                <div class="gcell--24 gcell--md-12">
                    <div class="articles-item__text-box">
                        <div class="articles-item__title-box">
							<p class="articles-item__date"><?php echo date('d / m', $obj->date); ?></p>
                            <p class="articles-item__title">
                                <a href="<?php echo HTML::link('articles/' . $obj->alias); ?>" class="articles-item__link" title="<?php echo $obj->name; ?>"><?php echo Text::limit_words2($obj->name, 10); ?></a>
                            </p>
                        </div>
                        <p class="articles-item__text"><?php echo Text::limit_words(strip_tags($obj->text), 500); ?></p>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>
<div class="news__pagination-wrap container">
<?php echo $pager; ?>
</div>
