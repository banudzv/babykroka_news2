<?php

use Core\HTML;
use Core\Text;

?>

<?php if (is_file(HOST . HTML::media('/images/articles/original/' . $obj->inner_image, false))): ?>
    <div class="news-page__main-img">
        <img src="<?php echo HTML::media('/images/articles/original/' . $obj->inner_image); ?>"
             alt="<?php echo $obj->name; ?>" class="lozad">
    </div>
<?php endif; ?>

<div class="news-page__text_box">
    <div class="wysiwyg">
        <?php echo $obj->text; ?>
    </div>
</div>