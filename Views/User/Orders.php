<?php

use Core\HTML;
use Core\User;

?>
<?php if (!count($orders)): ?>
    <p><?php echo __('У Вас еще нет заказов!') ?></p>
<?php else: ?>
    <?php foreach ($orders as $obj):
        if ($obj->status == 1) {
            $status_class = 'ready';
        } elseif ($obj->status == 2) {
            $status_class = 'in-progress';
        } else {
            $status_class = 'cancel';
        }
        ?>
        <div class="history__item open" id="<?php echo $obj->id; ?>">
            <div class="history__item-top open">
                <div class="history__item-info">
                    <span class="history__item-icon"></span>
                    <p class="history__item-title">
                        <?php echo __('Заказ') ?>
                        <span>№<?php echo $obj->id ?></span> от
                        <span class="history__item-date"><?php echo date('d.m.Y', $obj->created_at); ?></span>
                    </p>
                </div>
                <p class="history__item-status"><?php echo __('Статус заказа') ?>:
                    <span class="history__order-status <?php echo $status_class; ?>"><?php echo $statuses[$obj->status]; ?></span>
                </p>
            </div>
            <div class="history__item-bottom">
                <div class="history__items-wrap">
                    <?php foreach ($ordersItems[$obj->id] as $item): ?>
                    <div class="cart-item__wrap">
                        <div class="cart-item js-cart-item" data-id="<?php echo $item->id; ?>">
                            <div class="cart-item__inner">
                                <div class="cart-item__img-box">
                                    <div class="cart-item__img-wrap">
                                        <?php if($item->id):?>
                                            <a href="<?php echo HTML::link($item->alias . DS . 'p' . $item->id) ?>">
                                                <img class="cart-item__img lozad"
                                                     src="<?php echo HTML::media('images/catalog/original/' . $item->image); ?>"
                                                     alt="<?php echo $item->name ?>">
                                            </a>
                                        <?php else:?>
                                            <img class="cart-item__img lozad"
                                                 src="<?php echo HTML::media('assets/images/placeholders/no-image.jpg'); ?>"
                                                 alt="<?php echo $item->old_name; ?>">
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <div class="cart-item__info-box">
                                    <div class="cart-item__info-item cart-item__info-item--first ">
                                        <?php if($item->artikul):?>
                                        <p class="cart-item__code-box"><?php echo __('Код товара') ?>:
                                            <span class="cart-item__code"><?php echo $item->id; ?></span>
                                        </p>
                                        <?php endif; ?>
                                        <p class="cart-item__name"><?php echo ($item->name) ?:$item->old_name; ?></p>
                                        <?php if ($item->color): ?>
                                        <div class="cart-item__color-box">
                                            <span><?php echo __('Цвет') ?>:</span>
                                            <span class="cart-item__color"
                                                  style="background-color: <?php echo $item->color ?>"
                                                  title="<?php echo $item->color_name;?>"></span>
                                                </div>
                                            <?php endif; ?>
                                            <?php if ($item->sizes):
                                                $item->count = 0;
                                                ?>
                                                <div class="cart - item__size - box">
                                                    <span><?php echo __('Размер') ?>:</span>
                                                    <?php foreach ($item->sizes as $size):
                                                        $item->count += $size->count;
                                                        ?>
                                                        <div class="size__item"
                                                             data-size=" <?php echo $size->size; ?>">
                                                            <span class="size__text"><?php echo $size->size; ?></span>
                                                            <span class="size__count"><?php echo $size->count; ?></span>
                                        </div>
                                        <?php endforeach; ?>
                                    </div>
                                    <?php endif; ?>
                                </div>

                                <div class="cart-item__info-item cart-item__info-item--second">

                                    <span><?php echo __('Единиц товара') ?>:</span>
                                    <p class="cart-item__count"><?php echo $item->count; ?></p>

                                </div>

                                <div class="cart-item__info-item cart-item__info-item--third  small-item">
                                    <span><?php echo __('Сумма') ?>:</span>
                                    <p class="cart-item__price">
                                        <span><?php echo $item->count * $item->price; ?></span>
                                        <span><?php echo $obj->currency; ?></span>
                                    </p>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
                <?php endforeach; ?>

            </div>
            <div class="history__total-info">
                <div class="history-total__text-box">
                    <p class="history-total__title"><?php echo __('Общая сумма заказа') ?>:</p>
                    <p class="history-total__sub-title">*<?php echo __('Сумма указана без учета доставки') ?></p>
                </div>
                <div class="history-total__price-box">
                    <span><?php echo __('Итого') ?>:</span>
                    <p class="history-total__price"><?php echo $obj->amount; ?>
                        <span><?php echo $obj->currency ;?></span>
                    </p>
                </div>
            </div>
        </div>
        </div>
    <?php endforeach ?>
<?php endif ?>
