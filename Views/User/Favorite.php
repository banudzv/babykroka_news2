<?php

use Core\HTML;
use Core\Support;
use Core\User;

?>
<div class="grid">
    <?php if (!count($result)): ?>
        <p><?php echo __('У Вас нет товаров добавленных в избранное!') ?></p>
    <?php else: ?>
        <?php foreach ($result as $obj):
            $data = Support::calculatePriceWithCurrency($obj,$_currency,$_currentCurrency);
            $cost = $data['cost'];
            $cost_old = $data['cost_old'];
            $currency = $data['currency'];
            ?>
            <div class="product-item__wrap gcell--24 gcell--sm-12 gcell--ms-8">
                <div class="product-item js-cart-item" data-id="<?php echo $obj->id; ?>"
                     data-url="<?php echo HTML::link('ajax/addToCart') ?>">
                    <div class="product-item__img-box">
                        <?php if ($obj->sale): ?>
                            <div class="product-item__label product-item__label--red">
                                <span><?php echo __('Скидка') ?></span>
                            </div>
                        <?php endif; ?>
                        <button class="product-item__favorite js-init js-favorite-btn active"
                                data-url="<?php echo HTML::link('ajax/addToFavorite'); ?>"
                                data-id="<?php echo $obj->id; ?>">
                        <span class="product-item__favorite-icon">
                            <svg>
                                <use xlink:href="<?php echo HTML::media('assets/images/sprites/icons.svg#favorite-icon'); ?>"></use>
                            </svg>
                        </span>
                            <span class="product-item__favorite-icon--fill">
                            <svg>
                                <use xlink:href="<?php echo HTML::media('assets/images/sprites/icons.svg#like-icon'); ?>"></use>
                            </svg>
                        </span>
                        </button>
                        <div class="product-item__img-wrap">
                            <a href="<?php echo HTML::link($obj->alias . DS . 'p' . $obj->id) ?>">
                                <?php if (is_file(HOST . Core\HTML::media('images/catalog/original/'.$obj->image, false))): ?>
                                    <img class="product-item__img lozad"
                                         src="<?php echo HTML::media('images/catalog/original/' . $obj->image); ?>"
                                         alt="<?php echo $obj->name; ?>">
                                <?php else: ?>
                                    <img class="product-item__img lozad"
                                         src="<?php echo HTML::media('assets/images/placeholders/no-image.jpg'); ?>"
                                         alt="<?php echo $obj->name; ?>">
                                <?php endif; ?>
                            </a>
                        </div>
                    </div>
                    <div class="product-item__text-box">
                        <p class="product-item__name">
                            <a href="<?php echo HTML::link($obj->alias . DS . 'p' . $obj->id) ?>"
                               title="<?php echo $obj->name; ?>">
                                <?php echo $obj->name; ?>
                            </a>
                        </p>
                        <a href="<?php echo HTML::link('catalog/' . $obj->parent_alias) ?>"
                           class="product-item__category"><?php echo $obj->parent_name; ?></a>
                        <div class="product-item__price-box">
                        <span class="product-item__price">
                            <span><?= $currency == 'USD' ? $cost : ceil($cost) ?></span>
                            <span class="product-item__price-currency"> <?php echo $currency; ?></span>
                        </span>
                            <?php if ($obj->sale && !empty($cost_old)): ?>
                                <span class="product-item__old-price">
                                <span><?= $currency == 'USD' ? $cost_old : ceil($cost_old) ?></span>
                                <span class="product-item__price-currency"> <?php echo $currency; ?></span>
                            </span>
                            <?php endif; ?>

                        </div>
                        <button class="yellow-btn product-item__btn js-cart-add"
                                data-url="<?php echo HTML::link('ajax/addToCart') ?>" data-id="<?php echo $obj->id; ?>">
                            <?php echo __('Купить') ?>
                        </button>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    <?php endif; ?>
</div>
