<?php
    use Core\HTML;
use Forms\Builder;

?>
<div class="profile__data-wrap grid">
    <form class="profile__data-form js-form grid" data-ajax="<?php echo \Core\HTML::link('form/edit_profile');?>">
        <div class="gcell--24 gcell--sm-12 profile__data-left">
            <div class="form-group">
                <p class="form-title"><?php echo __('Личные данные') ?></p>
                <label class="profile__data-label">
                    <input required type="text" name="personal-name" data-name="name" id="personal-name" placeholder="<?php echo __('Имя') ?>" class="simple-input profile__data-input" data-rule-word="true" data-rule-minlength="2" value="<?php echo $user->name ?>">
                </label>
                <label class="profile__data-label">
                    <input required type="text" name="personal-surname" data-name="last_name" id="personal-surname" placeholder="<?php echo __('Фамилия') ?>" class="simple-input profile__data-input" data-rule-word="true" data-rule-minlength="2" value="<?php echo $user->last_name ?>">
                </label>
                <label class="profile__data-label">
                    <input required type="text" name="personal-surname" data-name="middle_name" id="personal-middle_name" placeholder="<?php echo __('Отчество') ?>" class="simple-input profile__data-input" data-rule-word="true" data-rule-minlength="2" value="<?php echo $user->middle_name ?>">
                </label>
            </div>
            <div class="form-group">
                <p class="form-title"><?php echo __('Пароль') ?></p>
                <label class="profile__data-label">
                    <input data-rule-required="#personal-password:filled" type="password" name="old-password" data-name="old-pswd" id="old-password" placeholder="<?php echo __('Старый пароль') ?>" class="simple-input profile__data-input" data-rule-minlength="6">
                </label>
                <label class="profile__data-label">
                    <input data-rule-required="#old-password:filled" type="password" name="personal-password" data-name="new-pswd" id="personal-password" placeholder="<?php echo __('Новый пароль') ?>" class="simple-input profile__data-input" data-rule-minlength="6">
                </label>
                <label class="profile__data-label">
                    <input data-rule-required="#personal-password:filled" type="password" name="personal-password-rep" id="personal-password-rep" data-rule-equalTo="[name='personal-password']" placeholder="<?php echo __('Подтверждение пароля') ?>" class="simple-input profile__data-input" data-rule-minlength="6">
                </label>
            </div>
            <?php echo \Core\HTML::csrfField();?>
            <button class="js-form-submit yellow-btn profile__data-submit">
                <?php echo __('Сохранить') ?>
            </button>
        </div>
        <div class="gcell--24 gcell--sm-12 profile__data-right">
            <div class="form-group">
                <p class="form-title"><?php echo __('Контактные данные') ?></p>
                <label class="profile__data-label">
                    <input type="email" name="personal-mail" data-name="email" id="personal-mail" placeholder="Email" class="simple-input profile__data-input" value="<?php echo $user->email ?>">
                </label>
                <label class="profile__data-label">
                    <input type="tel" name="personal-phone" data-name="phone" id="personal-phone" class="simple-input profile__data-input js-init" placeholder="+38(***) ***-**-**" data-rule-phoneua="true" data-phonemask="+38(***) ***-**-**" data-phonemask-android="+38(***) ***-**-**" value="<?php echo $user->phone ?>">
                </label>
                <label class="profile__data-label">
                    <input type="text" name="personal-city"  data-name="city" id="personal-city" placeholder="<?php echo __('Город') ?>" class="simple-input profile__data-input" value="<?php echo $user->city ?>">
                </label>
                <label class="profile__data-label">
                    <textarea placeholder="Адрес" class="profile__data-textarea" name="personal-address"  data-name="address" id="personal-adress"><?php echo $user->address ?></textarea>
                </label>
                <?php if (!empty($customerRoles)):?>
                <p class="form-title"><?php echo __('Тип пользователя') ?></p>
                <div class="">
                    <?php foreach ($customerRoles as $id => $role):?>
                        <label class="checkerWrap-inline">
                            <?php echo Builder::checkbox(isset($customerTypes[$id]), [
                                'name' => 'ROLES['.$id.']',
                                'value' => $id,
                            ]); ?>
                            <?php echo $role; ?>
                        </label>
                    <?php endforeach;?>
                </div>
                <?php endif;?>
            </div>
        </div>
    </form>
</div>
