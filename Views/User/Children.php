<?php use Core\Config;use Core\HTML;
use Forms\Builder;
$disabled = false;

if($amount == Config::get('basic.limit_children')){
    $disabled = true;
}
?>
<div class="profile__data-wrap grid">
    <form class="profile__data-form js-form grid js-init" data-ajax="<?php echo \Core\HTML::link('form/edit_child');?>">
        <div class="gcell--24 gcell--sm-12 profile__data-left">
            <?php if (!empty($children)):?>
                <p class="form-title"><?php echo __('Дети') ?></p>

                <?php foreach ($children as $index => $child):?>

                    <div class="form-group" data-ajax="<?php echo \Core\HTML::link('ajax/remove_child');?>">
                        <div class="cart-registration__sex">
                            <input type="radio" data-name="CHILDREN[<?= $index ?>][sex]" id="option-1" value="malchik" <?= $child->sex == 'malchik' ? 'checked' : '' ?>>
                            <input type="radio" data-name="CHILDREN[<?= $index ?>][sex]" id="option-2" value="devochka" <?= $child->sex == 'devochka' ? 'checked' : '' ?>>
                            <label for="option-1" class="option option-1">
                                <div class="dot"></div>
                                <span><?= __('Мальчик') ?></span>
                            </label>
                            <label for="option-2" class="option option-2">
                                <div class="dot"></div>
                                <span><?= __('Девочка') ?></span>
                            </label>
                        </div>
                        <?php echo Builder::input([
                            'name' => 'CHILDREN['.$index.'][name]',
                            'class' => 'simple-input profile__data-input',
                            'value' => $child->name,
                        ],'Имя ребёнка'); ?>
                        <?php echo Builder::input([
                            'name' => 'CHILDREN['.$index.'][birthday]',
                            'class' => 'simple-input profile__data-input js-birthday',
                            'autocomplete' => 'off',
                            'value' => $child->birthday? date('d.m.Y',$child->birthday) : NULL,
                        ],'День рождения ребёнка'); ?>
                        <input type="hidden" data-name="<?php echo 'CHILDREN['.$index.'][id]';?>" value="<?php echo $child->id;?>" data-dell-id="<?php echo $child->id;?>"/>

                        <div class="_flex">
                            <button class="js-form-submit yellow-btn profile__data-submit _mr-sm">
                                <?php echo __('Удалить') ?>
                            </button>
                            <button class="js-form-submit yellow-btn profile__data-submit">
                                <?php echo __('Изменить') ?>
                            </button>
                        </div>
                    </div>
                <?php endforeach;?>

                <input type="hidden" data-name="uid" value="<?php echo \Core\User::info()->id;?>"/>
                <?php echo \Core\HTML::csrfField();?>

            <?php else:?>
                <p><?php echo __('Вы ещё не добавили информацию о детях') ?></p>
            <?php endif;?>
        </div>
        <div class="gcell--24 gcell--sm-12 profile__data-right">
            <?php echo \Core\HTML::csrfField();?>
            <div class="form-group">
                <button title="<?php echo $disabled ?   __('Лимит по добавлению информации достигнут') : NULL;?>" <?php echo $disabled ? 'disabled' : '' ;?> data-mfp-src="<?php echo HTML::link('popup/getPopup'); ?>"
                        data-mfp="ajax"
                        data-param='<?php echo json_encode(['name' => 'AddChild']); ?>' class="js-init yellow-btn profile__data-submit <?php echo $disabled ? 'disabled' : '' ;?>">
                    <?php echo __('Добавить ребенка') ?>
                </button>
            </div>
        </div>
    </form>
</div>
