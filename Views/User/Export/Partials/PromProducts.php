<?php
use Core\HTML;
use Core\Support;

if(count($products)):?>
<ul class="export__checkers _flex <?php echo $hide ? 'is-hide' : '';?> nesting-level nesting-level--products">
    <div class="_flex-grow">
        <div class="grid export__table-header _mb-sm">
            <div class="gcell--5 _mr-md">Название</div>
            <div class="gcell--3">КТ</div>
            <div class="gcell--3">Код</div>
            <div class="gcell--3 export__your-price">Ваша цена</div>
            <div class="gcell--3">РРЦ</div>
            <div class="gcell--3">Дроп</div>
            <div class="gcell--3">Опт</div>
        </div>
        <?php foreach ($products as $product): ?>
            <li>

                <div class="grid export__table-body _mt-sm _mb-sm">
                     <div class="gcell--5 export__product-name _mr-md">

                         <?php if($select_cat && $select):?>
                             <input class="js-product-in-table" type="checkbox" checked  data-id="<?php echo $product->id;?>">
                         <?php else:?>
                             <?php !empty($checkedProducts) && in_array($product->id, array_keys($checkedProducts)) ? $selected = 'checked': $selected = '';?>
                             <input class="js-product-in-table" type="checkbox" <?php echo $selected; ?>  data-id="<?php echo $product->id;?>">
                         <?php endif;?>

                         <span title="<?php echo $product->name;?>"><?php echo $product->name; // наимен?></span>
                    </div>
                    <div class="gcell--3"><a href="<?php echo HTML::link($product->alias) ?> " target="_blank" class="yellow-btn _pl-ms _pr-ms _pt-sm _pb-sm">Перейти</a></div>
                    <div class="gcell--3"> <?php echo $product->id; // код?></div>

                    <?php foreach($range as $r):?>
                        <?php $min_max = explode('-',$r[0])?>
                        <?php if($min_max[0] <= $product->price && $min_max[1] >= $product->price):?>
                            <?php if($r[1] == 0):?>
                                <div class="gcell--3 export__your-price" data-newPrice="<?php echo $product->price;?>"><?php echo $product->price // это цена с полузнков, сначала розн,но потом меняется?><span class="export__currency"><?php echo $_currentCurrency->currency;?></span></div>

                            <?php else:?>
                                <div class="gcell--3 export__your-price" data-newPrice="<?php echo $product->price + ($product->price * ((int)$r[1] / 100));?>"><?php echo $product->price + ($product->price * ((int)$r[1] / 100)) ?><span class="export__currency"><?php echo $_currentCurrency->currency;?></span></div>
                            <?php endif;?>
                        <?php endif;?>
                    <?php endforeach;;?>
                    <div class="gcell--3"><?php echo $product->price; // цена?><span class="export__currency"><?php echo $_currentCurrency->currency;?></span></div>
                    <?php
                        $drop_p = \Modules\Catalog\Models\Prices::getPricesRowByPriceTypeWithCurrency($product->id, '90e0c1a4-364f-11ea-80cc-a4bf01075a5b');
                        $opt_p = \Modules\Catalog\Models\Prices::getPricesRowByPriceTypeWithCurrency($product->id, '78639693-364f-11ea-80cc-a4bf01075a5b');

                    ?>
                    <div class="gcell--3"><?php echo  Support::calculateCostWithCurrency($drop_p->price, $_currency, $_currentCurrency, $drop_p->currency);// цена  дроп?><span class="export__currency"><?php echo $_currentCurrency->currency;?></span></div>
                    <div class="gcell--3"><?php echo  Support::calculateCostWithCurrency($opt_p->price, $_currency, $_currentCurrency, $opt_p->currency);// цена опт ?><span class="export__currency"><?php echo $_currentCurrency->currency;?></span></div>
                </div>
            </li>
        <?php endforeach; ?>
    </div>
</ul>
<?php else:?>
<ul class="export__checkers _flex <?php echo $hide ? 'is-hide' : '';?>" >
    <div class="_flex-grow">
<?php echo __('Нет товаров для этого фильтра!');?>
        </div>
<ul>
<?php endif;?>
