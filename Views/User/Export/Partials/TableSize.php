<?php

$sizes = [];
foreach ($labels as $label) {
    $arr = array_intersect_key($type_sizes, $label);
    foreach ($arr as $key => $item) {
        $sizes[$key] = $item;
    }
}
?>

<?php if ($description): ?>
    <?php echo strip_tags($description); ?>
<?php endif; ?>
<?php if (!empty($labels)): ?>
    <![CDATA[
    <table border="1" cellpadding="1" cellspacing="0" style="width:400px">
        <tbody>
        <tr>
            <td class="b-product-info__header" colspan="4" style="background-color:#D3D3D3;text-align:center">
                Образмерка
            </td>
        </tr>
        <tr style="background-color:#f2f2f2">
            <td align="center">Размер</td>
            <?php foreach($sizes as $size => $val):?>:
                <td align="center"><?php echo mb_strtoupper($size).'- ';?> <?php echo $val;?></td>
            <?php endforeach;?>
        </tr>
        <?php foreach($labels as $size => $label):?>
        <tr>

                <td align="center"><?php echo $size;?></td>
                <?php foreach ($sizes as $s => $v):?>
                    <td align="center"><?php echo $label[$s];?></td>
                <?php endforeach;?>
        </tr>
            <?php endforeach;?>
        </tbody>
    </table>
    ]]>
<?php endif; ?>

