<?php use Core\Cookie;
use Core\HTML;
use Core\Support;
use Core\View;

$range = ['0-50', '51-100', '101-200', '201-300', '301-500', '501-750', '751-1000', '1001-1500', '1501-2500', '2501-3500', '3501-5000', '5001']

?>
<div hidden class="js-click-product-categories" data-click="<?php echo HTML::link('/ajax/export/saveTmp') ?>"></div>

<h1 class="main-title faq__title">Выгрузка для прома</h1>
<div class="_mb-def _mt-def">
    <a href="<?php echo $prom_info->youtube_link; ?>" target="_blank" class="yellow-btn _pl-ms _pr-ms _pt-sm _pb-sm"
       data-url="">Видеоинструкция</a>
    <p style="color: red; font-size: 12px">*Для корректной работы импорта Вы должны  в своем личном кабинете Прома, в маркете приложений подключить приложение "Разновидности"!!!</p>
</div>

<div class="export__white-block _mb-def">
    <div class="export__info">
        <svg class="export__info-icon" xmlns="http://www.w3.org/2000/svg" width="24" height="24"
             viewBox="0 0 24 24">
            <path d="M12 0c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm-2.033 16.01c.564-1.789 1.632-3.932 1.821-4.474.273-.787-.211-1.136-1.74.209l-.34-.64c1.744-1.897 5.335-2.326 4.113.613-.763 1.835-1.309 3.074-1.621 4.03-.455 1.393.694.828 1.819-.211.153.25.203.331.356.619-2.498 2.378-5.271 2.588-4.408-.146zm4.742-8.169c-.532.453-1.32.443-1.761-.022-.441-.465-.367-1.208.164-1.661.532-.453 1.32-.442 1.761.022.439.466.367 1.209-.164 1.661z"/>
        </svg>
        <div class="export__info-text"><?php echo $prom_info->url_file; ?></div>
    </div>
    <label class="control-label">URL для доступа к файлу:
        <input readonly class="simple-input _ml-ms"
               type="text" name="url-for-file"
               value="<?php echo $prom->file_link; ?>">
    </label>

    <span class="yellow-btn _ml-def _pl-ms _pr-ms _pt-sm _pb-sm js-copy-url">Скопировать URL</span>
</div>

<form class="export export__form" action="">
    <div class="export__white-block _mb-def">
        <div class="_mb-def">
            <div class="export__info">
                <svg class="export__info-icon" xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                     viewBox="0 0 24 24">
                    <path d="M12 0c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm-2.033 16.01c.564-1.789 1.632-3.932 1.821-4.474.273-.787-.211-1.136-1.74.209l-.34-.64c1.744-1.897 5.335-2.326 4.113.613-.763 1.835-1.309 3.074-1.621 4.03-.455 1.393.694.828 1.819-.211.153.25.203.331.356.619-2.498 2.378-5.271 2.588-4.408-.146zm4.742-8.169c-.532.453-1.32.443-1.761-.022-.441-.465-.367-1.208.164-1.661.532-.453 1.32-.442 1.761.022.439.466.367 1.209-.164 1.661z"/>
                </svg>
                <div class="export__info-text"><?php echo $prom_info->name_shop; ?></div>
            </div>

            <label class="control-label _mr-def">*Название магазина:
                <input class="simple-input _ml-ms" type="text"
                       name="shopName" required
                       value="<?php echo $prom->shop_name ?? ''; ?>">
            </label>
        </div>
        <div class="_ml-xl _pl-md">
            <label class="control-label">*Компания:
                <input class="simple-input _ml-ms"
                       type="text" name="companyName" required
                       value="<?php echo $prom->company_name ?? ''; ?>">
            </label>
        </div>
    </div>

    <div class="export__white-block _mb-def">
        <div class="_mb-lg">
            <div class="select2-wrap _mb-def">
                <div class="export__info">
                    <svg class="export__info-icon" xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                         viewBox="0 0 24 24">
                        <path d="M12 0c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm-2.033 16.01c.564-1.789 1.632-3.932 1.821-4.474.273-.787-.211-1.136-1.74.209l-.34-.64c1.744-1.897 5.335-2.326 4.113.613-.763 1.835-1.309 3.074-1.621 4.03-.455 1.393.694.828 1.819-.211.153.25.203.331.356.619-2.498 2.378-5.271 2.588-4.408-.146zm4.742-8.169c-.532.453-1.32.443-1.761-.022-.441-.465-.367-1.208.164-1.661.532-.453 1.32-.442 1.761.022.439.466.367 1.209-.164 1.661z"/>
                    </svg>
                    <div class="export__info-text"><?php echo $prom_info->products_filter; ?></div>
                </div>
                Фильтр товаров по брендам:
                <select multiple name="sort" id="sort" class="select2">
                    <?php foreach($brands as $id => $name):?>
                    <option value="<?php echo $id;?>"><?php echo $name;?></option>
                    <?php endforeach;?>
                </select>
            </div>


            <div class="export__slider _mb-def">
                <p>
                    <label for="amount">Дапазон цен:</label>
                    <input type="text" id="amount"
                           data-min="<?php echo Support::calculateCostWithCurrency((int)$price['min'], $_currency, $_currentCurrency); ?>"
                           data-max="<?php echo Support::calculateCostWithCurrency((int)$price['max'], $_currency, $_currentCurrency); ?>"
                           readonly
                           style="border: none; background:none; font-weight:bold;"
                           data-currency="<?php echo $_currentCurrency->currency; ?>">
                </p>
                <div class="_ml-ms" id="slider-range"></div>
            </div>


            <span class="yellow-btn _pl-ms _pr-ms _pt-sm _pb-sm js-set-filter"
                  data-url="<?php echo HTML::link('/ajax/export/filterProducts') ?>">Применить фильтр к каталогу</span>

            <span class="export__filter-noty _ml-auto">Фильтер применен</span>
        </div>
    </div>

    <div class="export__white-block _mb-def">
        <div>
            <div class="export__buttons _mb-def" data-export-table>
                <span class="export__button js-set-checkers" data-set-checkers="all">Выбрать все</span>
                <span class="export__button js-set-checkers" data-set-checkers="none">Убрать все</span>
            </div>

            <div class="export__buttons _mb-def" data-export-table>
                <span class="export__button js-set-checkers" data-set-checkers="onlyCheck">Оставить только выбранное</span>
                <span class="export__button js-set-checkers" data-set-checkers="showAll">Показать все</span>
            </div>

        </div>

        <?php if (count($categories)): ?>
            <div class="_flex">
                <div class="export__info">
                    <svg class="export__info-icon" xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                         viewBox="0 0 24 24">
                        <path d="M12 0c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm-2.033 16.01c.564-1.789 1.632-3.932 1.821-4.474.273-.787-.211-1.136-1.74.209l-.34-.64c1.744-1.897 5.335-2.326 4.113.613-.763 1.835-1.309 3.074-1.621 4.03-.455 1.393.694.828 1.819-.211.153.25.203.331.356.619-2.498 2.378-5.271 2.588-4.408-.146zm4.742-8.169c-.532.453-1.32.443-1.761-.022-.441-.465-.367-1.208.164-1.661.532-.453 1.32-.442 1.761.022.439.466.367 1.209-.164 1.661z"/>
                    </svg>
                    <div class="export__info-text"><?php echo $prom_info->products; ?></div>
                </div>
                <div class="_flex-grow">
                    <?php foreach ($categories[0] as $parentId => $obj): ?>
                        <?php $obj = (object)$obj; ?>
                        <ul class="export__checkers nesting-level" data-level="1"
                            data-url="<?php echo HTML::link('/ajax/export/productsCategories') ?>">
                            <li>
                                <?php if ($obj->products && !isset($categories[$obj->id])): ?>
                                    <span class="checkbox__icon js-product-show"
                                          data-id="<?php echo $obj->id; ?>"></span>
                                    <?php if (!empty($products) && in_array($obj->id, array_column(array_values($products), 'parent_id'))): ?>
                                        <?php echo View::tpl([
                                            'products' => $products,
                                            'checkedProducts' => $checkedProducts,
                                            'range' => json_decode($prom->slider),
                                            'prom_info' => $prom_info,
                                            'hide' => true
                                        ], 'User/Export/Partials/PromProducts'); ?>
                                    <?php endif; ?>
                                <?php elseif ($obj->products): ?>
                                    <span class="checkbox__icon checkbox__icon--main-category"></span>
                                <?php endif; ?>

                                <a href="#">
                                    <input type="checkbox" <?php echo in_array($obj->id, $checkedGroups) ? 'checked' : ''; ?>><span><?= $obj->name; ?></span>
                                </a>
                                    <?php if (isset($categories[$obj->id])): ?>
                                        <ul class="export__checkers is-hide nesting-level" data-level="2">
                                        <?php foreach ($categories[$obj->id] as $item): ?>
                                            <?php $item = (object)$item; ?>
                                        <?php if(($item->products && !isset($categories[$item->id]) || isset($categories[$item->id]))):?>
                                            <li>
                                                <?php if ($item->products && !isset($categories[$item->id])): ?>
                                                    <?php $prod = true; ?>
                                                    <span class="checkbox__icon js-product-show"
                                                          data-id="<?php echo $item->id; ?>"></span>


                                                    <?php if (!empty($products) && in_array($item->id, array_column(array_values($products), 'parent_id'))): ?>
                                                        <a href="#">
                                                            <input type="checkbox" <?php echo in_array($item->id, $checkedGroups) ? 'checked' : ''; ?>
                                                            <?php echo $prod ? 'data-id="' . $item->id . '"' : ''; ?>>
                                                            <span><?= $item->name; ?></span>
                                                        </a>
                                                        <?php echo View::tpl([
                                                            'products' => $products,
                                                            'checkedProducts' => $checkedProducts,
                                                            'range' => json_decode($prom->slider),
                                                            'prom_info' => $prom_info,
                                                            'hide' => true
                                                        ], 'User/Export/Partials/PromProducts'); ?>

                                                    <?php else: ?>
                                                        <a href="#">
                                                            <input type="checkbox" <?php echo in_array($item->id, $checkedGroups) ? 'checked' : ''; ?>
                                                            <?php echo $prod ? 'data-id="' . $item->id . '"' : ''; ?>>
                                                            <span><?= $item->name; ?></span>
                                                        </a>
                                                    <?php endif; ?>
                                                <?php elseif ($item->products): ?>
                                                    <?php $prod = false; ?>
                                                    <span class="checkbox__icon"></span>
                                                    <a href="#">
                                                        <input type="checkbox" <?php echo in_array($item->id, $checkedGroups) ? 'checked' : ''; ?>
                                                        <?php echo $prod ? 'data-id="' . $item->id . '"' : ''; ?>>
                                                        <span><?= $item->name; ?></span>
                                                    </a>
                                                <?php endif; ?>



                                                <?php if (isset($categories[$item->id])): ?>
                                                    <ul class="export__checkers is-hide nesting-level" data-level="3">
                                                        <?php foreach ($categories[$item->id] as $sub): ?>
                                                            <?php $sub = (object)$sub; ?>
                                                            <li>
                                                                <?php if ($sub->products): ?>
                                                                    <span class="checkbox__icon js-product-show"
                                                                          data-id="<?php echo $sub->id; ?>"></span>

                                                                    <a href="#">
                                                                        <input type="checkbox" <?php echo in_array($sub->id, $checkedGroups) ? 'checked' : ''; ?>
                                                                               data-id="<?php echo $sub->id; ?>"><span><?= $sub->name; ?></span>
                                                                    </a>

                                                                    <?php if (!empty($products) && in_array($sub->id, array_column(array_values($products), 'parent_id'))): ?>

                                                                        <?php echo View::tpl([
                                                                            'products' => $products,
                                                                            'checkedProducts' => $checkedProducts,
                                                                            'range' => json_decode($prom->slider),
                                                                            'prom_info' => $prom_info,
                                                                            'hide' => true
                                                                        ], 'User/Export/Partials/PromProducts'); ?>
                                                                    <?php endif; ?>
                                                                <?php endif; ?>
                                                            </li>
                                                        <?php endforeach; ?>
                                                    </ul>

                                                <?php endif; ?>
                                            </li>
                                            <?php endif;?>
                                        <?php endforeach; ?>
                                </ul>
                            <?php endif; ?>
                            </li>
                        </ul>
                    <?php endforeach; ?>
                </div>
            </div>
        <?php endif; ?>
    </div>

    <div class="export__white-block _mb-def">
        <div>
            <div class="export__info">
                <svg class="export__info-icon" xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                     viewBox="0 0 24 24">
                    <path d="M12 0c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm-2.033 16.01c.564-1.789 1.632-3.932 1.821-4.474.273-.787-.211-1.136-1.74.209l-.34-.64c1.744-1.897 5.335-2.326 4.113.613-.763 1.835-1.309 3.074-1.621 4.03-.455 1.393.694.828 1.819-.211.153.25.203.331.356.619-2.498 2.378-5.271 2.588-4.408-.146zm4.742-8.169c-.532.453-1.32.443-1.761-.022-.441-.465-.367-1.208.164-1.661.532-.453 1.32-.442 1.761.022.439.466.367 1.209-.164 1.661z"/>
                </svg>
                <div class="export__info-text"><?php echo $prom_info->products_prices; ?></div>
            </div>

            <span>Увеличение цен РРЦ:</span>
            <?php foreach ($range as $slider): ?>
                <?php if ($prom->slider): ?>
                    <?php foreach (json_decode($prom->slider) as $slider_save): ?>
                        <?php if ($slider == $slider_save[0]): ?>
                            <div class="export__slider--client" data-percent="<?php echo (int)$slider_save[1]; ?>">
                        <?php endif; ?>
                    <?php endforeach; ?>
                <?php else: ?>
                    <div class="export__slider--client" data-percent="0">
                <?php endif; ?>
                <p>
                    <label for="<?php echo $slider ?>">От <?php echo $slider ?> грн: </label>
                    <input type="text" id="<?php echo $slider ?>" data-min="0" data-max="300" readonly
                           style="border: none; background:none; font-weight:bold;">
                </p>
                <div class="_ml-ms" id="slider-range<?php echo $slider ?>"
                     style="width: 300px;align-self: center;">
                </div>
            </div>
            <?php endforeach ?>
            </div>

            <div class="_mt-def">
                <span class="yellow-btn _pl-ms _pr-ms _pt-sm _pb-sm js-set-filter"
                      data-url="<?php echo HTML::link('/ajax/export/filterProducts') ?>">Применить цены
                </span>

                <span class="export__saved-noty _ml-auto">Сохранено</span>

                <span class="export__filter-noty _ml-auto">Фильтер применен</span>

                <div class="_text-center">
                    <div class="export__info">
                        <svg class="export__info-icon" xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                             viewBox="0 0 24 24">
                            <path d="M12 0c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm-2.033 16.01c.564-1.789 1.632-3.932 1.821-4.474.273-.787-.211-1.136-1.74.209l-.34-.64c1.744-1.897 5.335-2.326 4.113.613-.763 1.835-1.309 3.074-1.621 4.03-.455 1.393.694.828 1.819-.211.153.25.203.331.356.619-2.498 2.378-5.271 2.588-4.408-.146zm4.742-8.169c-.532.453-1.32.443-1.761-.022-.441-.465-.367-1.208.164-1.661.532-.453 1.32-.442 1.761.022.439.466.367 1.209-.164 1.661z"/>
                        </svg>
                        <div class="export__info-text"><?php echo $prom_info->button_save; ?></div>
                    </div>

                    <button type="submit" class="yellow-btn export__save _pl-ms _pr-ms _pt-sm _pb-sm js-saveXML"
                            data-promId="<?php echo $prom->id ?? null ?>"
                            data-url="<?php echo HTML::link('/ajax/export/saveProm') ?>">Сохранить
                    </button>
                </div>
            </div>
        </div>
</form>
