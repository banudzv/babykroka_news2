<?php

use Core\Config;
use Core\HTML;
use Core\Support;
use Core\User;

$total = 0;
$data_ulogin = 'data-ulogin="display=buttons;fields=first_name,last_name,middle_name,email,phone;redirect_uri=https%3A%2F%2F' . $_SERVER['HTTP_HOST'] . '%2Faccount%2Flogin-by-social-network"';
$text = Config::get('basic.payment_text');
?>
<?php foreach ($cart as $key => $val):
    $obj = $cart[$val['id']]['obj'];
    $data = Support::calculatePriceWithCurrency($obj, $_currency, $_currentCurrency);
    $cost = $data['cost'];
    $cost_old = $data['cost_old'];
    $currency = $data['currency'];

    $item_sum = $cost * $val['count'];
    $total += $item_sum;
    if (count($obj->sizes) == 1 && $obj->sizes[0]->size === '-') {
        unset($obj->sizes);
    }
    ?>
<?php endforeach; ?>
<div class="cart-info__content grid">
    <div class="gcell--24 gcell--md-14">
        <div class="wstabs-block cart-registration__tab <?php echo (!\Core\User::info()) ? 'is-active' : ''; ?>"
             data-wstabs-ns="group-1" data-wstabs-block="1">
            <form id="cart-form1" class="js-form js-init" data-ajax="<?php echo HTML::link('form/checkout') ?>">
                <div class="cart-info__item">
                    <div class="cart-info__item-title">
                        <span><?php echo __('Личные данные') ?></span>
                    </div>
                    <div class="cart-registration__tab-buttons">
                        <button class="wstabs-button cart-registration__tab-btn <?php echo (!\Core\User::info()) ? 'is-active' : ''; ?>"
                                data-wstabs-ns="group-1" data-wstabs-button="1">
                            <?php echo __('Новый покупатель') ?>
                        </button>
                        <button class="wstabs-button cart-registration__tab-btn" data-wstabs-ns="group-1"
                                data-wstabs-button="2">
                            <?php echo __('Я уже зарегистрирован') ?>
                        </button>
                    </div>
                    <div>
                        <div class="cart-enter__social-wrap">
                            <p><?php echo __('Войти через соц. сеть:') ?></p>
                            <div class="cart-enter__socials" <?php echo $data_ulogin ?>>
                                <a href="#" class="cart-enter__social-link" data-uloginbutton="google">
                                    <svg>
                                        <use xlink:href="<?php echo HTML::svgSymbol('google-icon'); ?>"></use>
                                    </svg>
                                </a>
                                <a href="#" class="cart-enter__social-link" data-uloginbutton="facebook">
                                    <svg>
                                        <use xlink:href="<?php echo HTML::svgSymbol('fb-icon'); ?>"></use>
                                    </svg>
                                </a>
                                <a href="#" class="cart-enter__social-link" data-uloginbutton="twitter">
                                    <svg>
                                        <use xlink:href="<?php echo HTML::svgSymbol('twitter-icon'); ?>"></use>
                                    </svg>
                                </a>
                            </div>
                        </div>
                        <div class="cart-registration__info">
                            <label class="cart-registration__label">
                                <input type="text" required name="name" data-name="name" placeholder="Имя"
                                       class="cart-registration__input simple-input" data-rule-word="true"
                                       data-rule-minlength="2" value="<?php echo $order ? $order->name : '';?>">
                                <span class="required-dot"></span>
                                <label class="has-error" for="name" style="display: none"></label>
                            </label>
                            <label class="cart-registration__label">
                                <input type="text" required name="surname" data-name="last_name"
                                       placeholder="Фамилия" class="cart-registration__input simple-input"
                                       data-rule-word="true" data-rule-minlength="2" value="<?php echo $order ? $order->last_name : '';?>">
                                <span class="required-dot"></span>
                                <label class="has-error" for="surname" style="display: none"></label>
                            </label>
                            <label class="cart-registration__label">
                                <input type="text" required name="middle_name" data-name="middle_name"
                                       placeholder="Отчество" class="cart-registration__input simple-input"
                                       data-rule-word="true" data-rule-minlength="2" value="<?php echo $order ? $order->middle_name : '';?>">
                                <label class="has-error" for="middle_name" style="display: none"></label>
                            </label>
                            <label class="cart-registration__label">
                                <input type="email" name="mail" data-name="email" placeholder="E-mail"
                                       class="cart-registration__input simple-input" value="<?php echo $order ? $order->email : '';?>">
                            </label>
                            <label class="cart-registration__label">
                                <input type="tel" name="tel" data-name="phone" placeholder="+38(***) ***-**-**"
                                       required data-rule-phoneua="true" data-phonemask="+38(***) ***-**-**"
                                       data-phonemask-android="+38(***) ***-**-**"
                                       class="cart-registration__input simple-input js-init" value="<?php echo $order ? $order->phone : '';?>">
                                <span class="required-dot"></span>
                                <label class="has-error" for="tel" style="display: none"></label>
                            </label>

                            <div class="cart-registration__status">
                                <label class="cart-registration__checkbox-label">
                                    <input type="checkbox" value="rozn" name="rozn" data-name="rozn" id="rozn"
                                           class="cart-registration__checkbox" required>
                                    <span class="cart-registration__checkbox-text cart-registration__checkbox-text--block cart-registration__checkbox-text--white">Розница
                                </label>

                                <label class="cart-registration__checkbox-label">
                                    <input type="checkbox" value="opt" name="opt" data-name="opt" id="opt"
                                           class="cart-registration__checkbox">
                                    <span class="cart-registration__checkbox-text cart-registration__checkbox-text--block cart-registration__checkbox-text--white">Опт
                                </label>

                                <label class="cart-registration__checkbox-label">
                                    <input type="checkbox" value="drop" name="drop" data-name="drop" id="drop"
                                           class="cart-registration__checkbox">
                                    <span class="cart-registration__checkbox-text cart-registration__checkbox-text--block cart-registration__checkbox-text--white">Дроп
                                </label>
                            </div>

                            <label class="cart-registration__checkbox-label">
                                <input type="checkbox" name="no-profile" data-name="profile"
                                       class="cart-registration__checkbox" value="true" <?php echo $order && $order->user_id == 0 ? 'checked' : '';?>>
                                <span class="cart-registration__checkbox-text"><?php echo __('Не регистрироваться на сайте') ?>
															<span class="gray"><?php echo __('(Не нужен личный кабинет)') ?></span>
														</span>
                            </label>
                            <label class="cart-registration__checkbox-label">
                                <input required type="checkbox" name="agree" data-name="agree"
                                       class="cart-registration__checkbox" <?php echo $order ? 'checked' : '';?>>
                                <span class="cart-registration__checkbox-text">Я согласен с
															<a href="<?php echo HTML::link('uslovija-ispolzovanija'); ?>"
                                                               target="_blank"><?php echo __('условиями использования') ?></a>
														</span>
                                <label class="has-error" for="agree" style="display: none"></label>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="cart-info__item">
                    <div class="cart-info__item-title">
                        <span><?php echo __('Способ и данные для доставки') ?></span>
                    </div>
                    <div>
                        <p class="cart-info__item-sub-title"><?php echo __('Выберите способ доставки') ?></p>
                        <div class="cart-info__delivery-items">
                            <label class="has-error radio-has-error" for="delivery"
                                   style="display: none"><?php echo __('Укажите способ оплаты') ?></label>
                            <div class="cart-info__delivery-item">
                                <label class="cart-info__label">
                                    <input required type="radio" class="radio-input cart-info__radio" id="delivery11"
                                           name="delivery" data-name="delivery" value="1"
                                           <?php echo $order->delivery == 1 ?'checked':'';?>>
                                    <span class="radio-input__text"><?php echo __('В отделение Новой почты') ?></span>
                                </label>
                                <div class="cart-info__delivery-content js-choose-ajax-container">
                                    <div class="grid grid--hspace-md">
                                        <div class="gcell gcell--24 gcell--sm-8">
                                            <label class="cart-info__label cart-info__label--select select2-wrap">
                                                <select data-rule-required="#delivery11:checked" name="region1"
                                                        data-name="region[1]"
                                                        class="select2 js-choose-ajax" data-placeholder="Регион"
                                                        style="width: 100%"
                                                        data-url="<?php echo HTML::link('ajax/getNPCity'); ?>">
                                                    <option></option>
                                                    <?php foreach ($regions as $key => $name): ?>
                                                        <option value="<?php echo $key ?>"
                                                        >
                                                            <?php echo $name ?>
                                                        </option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </label>
                                        </div>
                                        <div class="gcell gcell--24 gcell--sm-8">
                                            <label class="cart-info__label cart-info__label--select select2-wrap">
                                                <select data-rule-required="#delivery11:checked" name="city1"
                                                        data-name="city[1]"
                                                        class="select2 js-choose-ajax js-delivery-np-city"
                                                        data-placeholder="Город" style="width: 100%"
                                                        data-url="<?php echo HTML::link('ajax/getNPWarehouse'); ?>">
                                                    <option></option>
                                                </select>
                                            </label>
                                        </div>
                                        <div class="gcell gcell--24 gcell--sm-8">
                                            <label class="cart-info__label cart-info__label--select select2-wrap">
                                                <select data-rule-required="#delivery11:checked" name="section1"
                                                        data-name="section[1]" class="select2 js-delivery-np-section"
                                                        data-placeholder="<?php echo __('Отделение') ?>"
                                                        style="width: 100%">
                                                    <option></option>
                                                </select>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="cart-info__delivery-item">
                                <label class="cart-info__label">
                                    <input required type="radio" class="radio-input cart-info__radio" id="delivery41"
                                           name="delivery" data-name="delivery" value="4">
                                    <span class="radio-input__text"><?php echo __('Курьером Новой почты') ?></span>
                                </label>
                                <div class="cart-info__delivery-content js-choose-ajax-container">
                                    <div class="grid grid--hspace-md">
                                        <div class="gcell gcell--24 gcell--sm-8" style="width: 50%;">
                                            <label class="cart-info__label cart-info__label--select select2-wrap">
                                                <select data-rule-required="#delivery11:checked" name="region1"
                                                        data-name="region[4]"
                                                        class="select2 js-choose-ajax" data-placeholder="<?= __('Регион') ?>"
                                                        style="width: 100%"
                                                        data-url="<?php echo HTML::link('ajax/getNPCity'); ?>">
                                                    <option></option>
                                                    <?php foreach ($regions as $key => $name): ?>
                                                        <option value="<?php echo $key ?>"><?php echo $name ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </label>
                                        </div>
                                        <div class="gcell gcell--24 gcell--sm-8" style="width: 50%;">
                                            <label class="cart-info__label cart-info__label--select select2-wrap">
                                                <select data-rule-required="#delivery11:checked" name="city1"
                                                        data-name="city[4]"
                                                        class="select2 js-choose-ajax js-delivery-np-city js-courier-city"
                                                        data-placeholder="<?= __('Город') ?>" style="width: 100%"
                                                        data-url="<?php echo HTML::link('ajax/getNPWarehouse'); ?>">
                                                    <option></option>
                                                </select>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="grid grid--hspace-md">
                                        <div class="gcell gcell--24 gcell--sm-8" style="width: 50%;">
                                            <label class="cart-info__label cart-info__label--select select2-wrap">
                                                <select data-rule-required="#delivery11:checked" name="section[4]"
                                                        data-name="section[4]"
                                                        class="select2 ajaxSelect2 js-delivery-np-street"
                                                        data-placeholder="<?= __('Улица') ?>" style="width: 100%"
                                                        data-city=""
                                                        data-url="<?php echo HTML::link('ajax/searchStreet'); ?>">
                                                    <option></option>
                                                </select>
                                            </label>
                                        </div>
										<div class="gcell gcell--24 gcell--sm-8" style="width: 50%;">
											<label class="cart-info__label cart-info__label--select">
												<input type="text" required="" name="home" data-name="home" placeholder="<?= __('Дом') ?>" class="cart-registration__input simple-input js-delivery-np-home" data-rule-minlength="1" value="" style="border: 1px solid #dfdfdf;margin: 0">
												<label class="has-error" for="home" style="display: none"></label>
											</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="cart-info__delivery-item">
                                <label class="cart-info__label">
                                    <input required type="radio" class="radio-input cart-info__radio" id="delivery21"
                                           name="delivery" data-name="delivery" value="2"
                                        <?php echo $order->delivery == 2 ?'checked':'';?>>
                                    <span class="radio-input__text"><?php echo __('Укрпочта') ?></span>
                                </label>
                                <div class="cart-info__delivery-content _mb-sm">
                                    <div class="grid grid--hspace-md">
                                        <div class="gcell gcell--24 gcell--sm-6">
                                            <label class="cart-registration__label">
                                                <input type="text" data-rule-required="#delivery21:checked"
                                                       name="region-2" data-name="region[2]"
                                                       placeholder="<?php echo __('Регион') ?>"
                                                       class="cart-registration__input cart-registration__input--md simple-input is-required"
                                                       data-rule-minlength="2"
                                                value="<?php echo $order->delivery == 2? $order_delivery->region : '';?>">
                                                <span class="required-dot"></span>
                                                <label class="has-error" for="region-2" style="display: none"></label>
                                            </label>
                                        </div>
                                        <div class="gcell gcell--24 gcell--sm-6">
                                            <label class="cart-registration__label">
                                                <input type="text" data-rule-required="#delivery21:checked"
                                                       name="city-2"
                                                       data-name="city[2]" placeholder="<?php echo __('Город') ?>"
                                                       class="cart-registration__input cart-registration__input--md simple-input is-required"
                                                       data-rule-minlength="2"
                                                       value="<?php echo $order->delivery == 2? $order_delivery->city : '';?>">
                                                <span class="required-dot"></span>
                                                <label class="has-error" for="city-2" style="display: none"></label>
                                            </label>
                                        </div>
                                        <div class="gcell gcell--24 gcell--sm-6">
                                            <label class="cart-registration__label">
                                                <input type="text" data-rule-required="#delivery21:checked"
                                                       name="address-2" data-name="section[2]"
                                                       placeholder="<?php echo __('Адрес') ?>"
                                                       class="cart-registration__input cart-registration__input--md simple-input is-required"
                                                       data-rule-minlength="2"
                                                       value="<?php echo $order->delivery == 2? $order_delivery->warehouse : '';?>">
                                                <span class="required-dot"></span>
                                                <label class="has-error" for="address-2" style="display: none"></label>
                                            </label>
                                        </div>
                                        <div class="gcell gcell--24 gcell--sm-6">
                                            <label class="cart-registration__label">
                                                <input type="text" data-rule-required="#delivery22:checked"
                                                       name="index-2" data-name="index"
                                                       placeholder="<?php echo __('Индекс') ?>"
                                                       class="cart-registration__input cart-registration__input--md simple-input is-required"
                                                       data-rule-minlength="5"
                                                       value="<?php echo $order->delivery == 2? $order_delivery->index : '';?>">
                                                <span class="required-dot"></span>
                                                <label class="has-error" for="index-2" style="display: none"></label>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="cart-info__delivery-item">
                                <label class="cart-info__label">
                                    <input required type="radio" class="radio-input cart-info__radio" id="delivery31"
                                           name="delivery" data-name="delivery" value="3"
                                        <?php echo $order->delivery == 3 ?'checked':'';?>>
                                    <span class="radio-input__text"><?php echo __('Самовывоз') ?></span>
                                </label>
                            </div>
                            <input type="hidden" name="old_order" data-name="old_order" value="<?php echo $order->id;?>">
                        </div>
                    </div>
                </div>
                <div class="cart-info__item">
                    <div>
                        <a href="<?php echo HTML::link('cart'); ?>" class="yellow-border-btn cart-info__btn">
                            <?php echo __('Вернуться в корзину') ?>
                        </a>
                        <button class="yellow-btn cart-info__btn">
                            <?php echo __('Перейти к оплате') ?>
                        </button>
                    </div>
                </div>
                <?php echo \Core\HTML::csrfField(); ?>
            </form>
        </div>
        <div class="wstabs-block cart-registration__tab <?php echo (\Core\User::info()) ? 'is-active' : ''; ?>"
             data-wstabs-ns="group-1" data-wstabs-block="2">
            <div class="cart-info__item">
                <div class="cart-info__item-title">
                    <span><?php echo __('Личные данные') ?></span>
                </div>
                <div class="cart-registration__tab-buttons">
                    <button class="wstabs-button cart-registration__tab-btn" data-wstabs-ns="group-1"
                            data-wstabs-button="1">
                        <?php echo __('Новый покупатель') ?>
                    </button>
                    <button class="wstabs-button cart-registration__tab-btn is-active" data-wstabs-ns="group-1"
                            data-wstabs-button="2">
                        <?php echo __('Я уже зарегистрирован') ?>
                    </button>
                </div>
                <div>
                    <?php if (!\Core\User::info()): ?>
                        <div class="cart-enter__social-wrap">
                            <p><?php echo __('Войти через соц. сеть:') ?></p>
                            <div class="cart-enter__socials" <?php echo $data_ulogin ?>>
                                <a href="#" class="cart-enter__social-link" data-uloginbutton="google">
                                    <svg>
                                        <use xlink:href="<?php echo HTML::svgSymbol('google-icon'); ?>"></use>
                                    </svg>
                                </a>
                                <a href="#" class="cart-enter__social-link" data-uloginbutton="facebook">
                                    <svg>
                                        <use xlink:href="<?php echo HTML::svgSymbol('fb-icon'); ?>"></use>
                                    </svg>
                                </a>
                                <a href="#" class="cart-enter__social-link" data-uloginbutton="twitter">
                                    <svg>
                                        <use xlink:href="<?php echo HTML::svgSymbol('twitter-icon'); ?>"></use>
                                    </svg>
                                </a>
                            </div>
                        </div>
                        <div class="cart-enter wstabs-block is-active" data-wstabs-ns="group-2" data-wstabs-block="1">

                            <form class="js-form js-init cart-enter__form"
                                  data-ajax="<?php echo HTML::link('ajax/login'); ?>">
                                <label class="cart-registration__label">
                                    <input type="email" required name="enter-mail" data-name="email"
                                           placeholder="E-mail" class="cart-registration__input simple-input">
                                    <span class="required-dot"></span>
                                    <label class="has-error" for="email" style="display: none"></label>
                                </label>
                                <label class="cart-registration__label">
                                    <input type="password" required name="password" data-name="password"
                                           placeholder="<?php echo __('Пароль') ?>"
                                           class="cart-registration__input simple-input"
                                           data-rule-minlength="6">
                                    <span class="required-dot"></span>
                                    <label class="has-error" for="password"
                                           style="display: none"></label>
                                </label>
                                <?php echo \Core\HTML::csrfField(); ?>
                                <button class="yellow-btn cart-info__btn cart-info__btn--remind"><?php echo __('Войти') ?></button>
                            </form>
                            <span class="wstabs-button cart-info__toggle reming" data-wstabs-ns="group-2"
                                  data-wstabs-button="2"><?php echo __('Забыли пароль?') ?></span>
                        </div>
                        <div class="cart-remind wstabs-block" data-wstabs-ns="group-2" data-wstabs-block="2">
                            <form class="js-form js-init cart-enter__form" data-ajax="./hidden/response/demo.json">
                                <label class="cart-registration__label">
                                    <input required type="email" name="send-password"
                                           placeholder="E-mail" class="cart-registration__input simple-input">
                                    <span class="required-dot"></span>
                                    <label class="has-error" for="send-password"
                                           style="display: none"></label>
                                </label>
                                <button class="yellow-btn cart-info__btn cart-info__btn--remind">
                                    <?php echo __('Восстановить пароль') ?>
                                </button>
                            </form>
                            <span class="wstabs-button cart-info__toggle enter is-active" data-wstabs-ns="group-2"
                                  data-wstabs-button="1"><?php echo __('Войти') ?></span>
                        </div>
                    <?php else: ?>
                        <div class="cart-enter wstabs-block is-active" data-wstabs-ns="group-2" data-wstabs-block="1">
                            <div class="cart-enter__login-info">
                                <p class="cart-enter__login-item _bold"><?php echo __('Имя и фамилия') ?></p>
                                <p class="cart-enter__login-item"><?php echo \Core\User::info()->name . ' ' . \Core\User::info()->last_name . ' ' . User::info()->middle_name ?></p>
                                <p class="cart-enter__login-item _bold">E-mail</p>
                                <p class="cart-enter__login-item"><?php echo \Core\User::info()->email ?></p>
                                <p class="cart-enter__login-item _bold"><?php echo __('Номер телефона') ?></p>
                                <p class="cart-enter__login-item"><?php echo \Core\User::info()->phone ?></p>
                            </div>
                            <span class="wstabs-button cart-info__toggle reming" data-wstabs-ns="group-2"
                                  data-wstabs-button="2"><?php echo __('Изменить данные') ?></span>
                        </div>
                        <div class="cart-remind wstabs-block" data-wstabs-ns="group-2" data-wstabs-block="2">
                            <form class="js-form js-init cart-enter__form"
                                  data-ajax="<?php echo HTML::link('form/edit_profile') ?>">
                                <label class="cart-registration__label">
                                    <input type="text" required name="name" data-name="name"
                                           placeholder="<?php echo __('Имя') ?>"
                                           class="cart-registration__input simple-input"
                                           value="<?php echo \Core\User::info()->name; ?>" data-rule-word="true"
                                           data-rule-minlength="2">
                                </label>
                                <label class="cart-registration__label">
                                    <input type="text" required name="surname" data-name="last_name"
                                           placeholder="<?php echo __('Фамилия') ?>"
                                           class="cart-registration__input simple-input"
                                           value="<?php echo \Core\User::info()->last_name; ?>" data-rule-word="true"
                                           data-rule-minlength="2">
                                    <label class="has-error" for="surname"
                                           style="display: none"></label>
                                </label>
                                <label class="cart-registration__label">
                                    <input type="text" required name="middle_name" data-name="middle_name"
                                           placeholder="<?php echo __('Отчество') ?>"
                                           class="cart-registration__input simple-input"
                                           value="<?php echo \Core\User::info()->middle_name; ?>" data-rule-word="true"
                                           data-rule-minlength="2">
                                    <label class="has-error" for="middle_name"
                                           style="display: none"></label>
                                </label>
                                <label class="cart-registration__label">
                                    <input type="email" name="mail" data-name="email" placeholder="E-mail"
                                           class="cart-registration__input simple-input"
                                           value="<?php echo \Core\User::info()->email; ?>">
                                </label>
                                <label class="cart-registration__label">
                                    <input type="tel" name="tel" data-name="phone"
                                           placeholder="+38(***) ***-**-**"
                                           value="<?php echo \Core\User::info()->phone; ?>" required
                                           data-rule-phoneua="true" data-phonemask="+38(***) ***-**-**"
                                           data-phonemask-android="+38(***) ***-**-**"
                                           class="cart-registration__input simple-input js-init">
                                    <label class="has-error" for="tel" style="display: none"></label>
                                </label>
                                <?php echo \Core\HTML::csrfField(); ?>
                                <input type="hidden" data-name="redirect"
                                       value="<?php echo HTML::link('cart/ordering') ?>">
                                <button class="yellow-btn cart-info__btn cart-info__btn--remind">
                                    Сохранить
                                </button>
                            </form>
                            <span class="wstabs-button cart-info__toggle enter is-active" data-wstabs-ns="group-2"
                                  data-wstabs-button="1"><?php echo __('Отменить') ?></span>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
            <form id="cart-form2" class="js-form js-init" data-ajax="<?php echo HTML::link('form/checkoutLogged'); ?>">
                <?php echo \Core\HTML::csrfField(); ?>
                <div class="cart-info__item <?php echo (!\Core\User::info()) ? 'disabled' : ''; ?>">
                    <div class="cart-info__item-title">
                        <span><?php echo __('Способ и данные для доставки') ?></span>
                    </div>
                    <div>
                        <p class="cart-info__item-sub-title"><?php echo __('Выберите способ доставки') ?></p>
                        <div class="cart-info__delivery-items">
                            <label class="has-error radio-has-error" for="delivery"
                                   style="display: none"><?php echo __('Укажите способ оплаты') ?></label>
                            <div class="cart-info__delivery-item">
                                <label class="cart-info__label">
                                    <input required type="radio" class="radio-input cart-info__radio" name="delivery"
                                           data-name="delivery" value="1" id="delivery12"
                                        <?php echo $order->delivery == 1 ?'checked':'';?>>
                                    <span class="radio-input__text"><?php echo __('В отделение Новой почты') ?></span>
                                </label>
                                <div class="cart-info__delivery-content js-choose-ajax-container">
                                    <div class="grid grid--hspace-md">
                                        <div class="gcell gcell--24 gcell--sm-8">
                                            <label class="cart-info__label cart-info__label--select select2-wrap">
                                                <select data-rule-required="#delivery12:checked" name="region1"
                                                        data-name="region[1]"
                                                        class="select2 js-choose-ajax"
                                                        data-placeholder="<?php echo __('Регион') ?>"
                                                        style="width: 100%"
                                                        data-url="<?php echo HTML::link('ajax/getNPCity'); ?>">
                                                    <option></option>
                                                    <?php foreach ($regions as $key => $name): ?>
                                                        <option value="<?php echo $key ?>"
                                                        >
                                                            <?php echo $name ?>
                                                        </option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </label>
                                        </div>

                                        <div class="gcell gcell--24 gcell--sm-8">
                                            <label class="cart-info__label cart-info__label--select select2-wrap">
                                                <select data-rule-required="#delivery12:checked" name="city1"
                                                        data-name="city[1]"
                                                        class="select2 js-choose-ajax js-delivery-np-city"
                                                        data-placeholder="<?php echo __('') ?>Город" style="width: 100%"
                                                        data-url="<?php echo HTML::link('ajax/getNPWarehouse'); ?>">
                                                    <option></option>
                                                </select>
                                            </label>
                                        </div>
                                        <div class="gcell gcell--24 gcell--sm-8">
                                            <label class="cart-info__label cart-info__label--select select2-wrap">
                                                <select data-rule-required="#delivery12:checked" name="section1"
                                                        data-name="section[1]" class="select2 js-delivery-np-section"
                                                        data-placeholder="<?php echo __('Отделение') ?>"
                                                        style="width: 100%">
                                                    <option></option>
                                                </select>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="cart-info__delivery-item">
                                <label class="cart-info__label">
                                    <input required type="radio" class="radio-input cart-info__radio" id="delivery41"
                                           name="delivery" data-name="delivery" value="4">
                                    <span class="radio-input__text"><?php echo __('Курьером Новой почты') ?></span>
                                </label>
                                <div class="cart-info__delivery-content js-choose-ajax-container">
                                    <div class="grid grid--hspace-md">
                                        <div class="gcell gcell--24 gcell--sm-8" style="width: 50%;">
                                            <label class="cart-info__label cart-info__label--select select2-wrap">
                                                <select data-rule-required="#delivery11:checked" name="region1"
                                                        data-name="region[4]"
                                                        class="select2 js-choose-ajax" data-placeholder="<?= __('Регион') ?>"
                                                        style="width: 100%"
                                                        data-url="<?php echo HTML::link('ajax/getNPCity'); ?>">
                                                    <option></option>
                                                    <?php foreach ($regions as $key => $name): ?>
                                                        <option value="<?php echo $key ?>"><?php echo $name ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </label>
                                        </div>
                                        <div class="gcell gcell--24 gcell--sm-8" style="width: 50%;">
                                            <label class="cart-info__label cart-info__label--select select2-wrap">
                                                <select data-rule-required="#delivery11:checked" name="city1"
                                                        data-name="city[4]"
                                                        class="select2 js-choose-ajax js-delivery-np-city js-courier-city-logged"
                                                        data-placeholder="<?= __('Город') ?>" style="width: 100%"
                                                        data-url="<?php echo HTML::link('ajax/getNPWarehouse'); ?>">
                                                    <option></option>
                                                </select>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="grid grid--hspace-md">
                                        <div class="gcell gcell--24 gcell--sm-8" style="width: 50%;">
                                            <label class="cart-info__label cart-info__label--select select2-wrap">
                                                <select data-rule-required="#delivery11:checked" name="section[4]"
                                                        data-name="section[4]"
                                                        class="select2 ajaxSelect3 js-delivery-np-street"
                                                        data-placeholder="<?= __('Улица') ?>" style="width: 100%"
                                                        data-city=""
                                                        data-url="<?php echo HTML::link('ajax/searchStreet'); ?>">
                                                    <option></option>
                                                </select>
                                            </label>
                                        </div>
                                        <div class="gcell gcell--24 gcell--sm-8" style="width: 50%;">
                                            <label class="cart-info__label cart-info__label--select">
                                                <input type="text" required="" name="home" data-name="home" placeholder="<?= __('Дом') ?>" class="cart-registration__input simple-input" data-rule-minlength="1" value="" style="border: 1px solid #dfdfdf;margin: 0">
                                                <label class="has-error" for="home" style="display: none"></label>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="cart-info__delivery-item">
                                <label class="cart-info__label">
                                    <input required type="radio" class="radio-input cart-info__radio" name="delivery"
                                           data-name="delivery" value="2" id="delivery22"
                                        <?php echo $order->delivery == 2 ?'checked':'';?>>
                                    <span class="radio-input__text"><?php echo __('Укрпочта') ?></span>
                                </label>
                                <div class="cart-info__delivery-content _mb-sm">
                                    <div class="grid grid--hspace-md">
                                        <div class="gcell gcell--24 gcell--sm-6">
                                            <label class="cart-registration__label">
                                                <input type="text" data-rule-required="#delivery22:checked"
                                                       name="region-2" data-name="region[2]"
                                                       placeholder="<?php echo __('Регион') ?>"
                                                       class="cart-registration__input cart-registration__input--md simple-input is-required"
                                                       data-rule-minlength="2"
                                                       value="<?php echo $order->delivery == 2? $order_delivery->region : '';?>">
                                                <span class="required-dot"></span>
                                                <label class="has-error" for="region-2" style="display: none"></label>
                                            </label>
                                        </div>
                                        <div class="gcell gcell--24 gcell--sm-6">
                                            <label class="cart-registration__label">
                                                <input type="text" data-rule-required="#delivery22:checked"
                                                       name="city-2"
                                                       data-name="city[2]" placeholder="<?php echo __('Город') ?>"
                                                       class="cart-registration__input cart-registration__input--md simple-input is-required"
                                                       data-rule-minlength="2"
                                                       value="<?php echo $order->delivery == 2? $order_delivery->city : '';?>">
                                                <span class="required-dot"></span>
                                                <label class="has-error" for="city-2" style="display: none"></label>
                                            </label>
                                        </div>
                                        <div class="gcell gcell--24 gcell--sm-6">
                                            <label class="cart-registration__label">
                                                <input type="text" data-rule-required="#delivery22:checked"
                                                       name="address-2" data-name="section[2]"
                                                       placeholder="<?php echo __('Адрес') ?>"
                                                       class="cart-registration__input cart-registration__input--md simple-input is-required"
                                                       data-rule-minlength="2"
                                                       value="<?php echo $order->delivery == 2? $order_delivery->warehouse : '';?>">
                                                <span class="required-dot"></span>
                                                <label class="has-error" for="address-2" style="display: none"></label>
                                            </label>
                                        </div>
                                        <div class="gcell gcell--24 gcell--sm-6">
                                            <label class="cart-registration__label">
                                                <input type="text" data-rule-required="#delivery22:checked"
                                                       name="index-2" data-name="index"
                                                       placeholder="<?php echo __('Индекс') ?>"
                                                       class="cart-registration__input cart-registration__input--md simple-input is-required"
                                                       data-rule-minlength="5"
                                                       value="<?php echo $order->delivery == 2? $order_delivery->index : '';?>">
                                                <span class="required-dot"></span>
                                                <label class="has-error" for="index-2" style="display: none"></label>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="cart-info__delivery-item">
                                <label class="cart-info__label">
                                    <input required type="radio" class="radio-input cart-info__radio" name="delivery"
                                           data-name="delivery" value="3" id="delivery32"
                                        <?php echo $order->delivery == 3 ?'checked':'';?>>
                                    <span class="radio-input__text"><?php echo __('Самовывоз') ?></span>
                                </label>
                            </div>
                            <input type="hidden" name="old_order" data-name="old_order" value="<?php echo $order->id;?>">
                        </div>
                    </div>
                </div>
                <div class="cart-info__item <?php echo (!\Core\User::info()) ? 'disabled' : ''; ?>">
                    <div>
                        <a href="<?php echo HTML::link('cart'); ?>" class="yellow-border-btn cart-info__btn">
                            <?php echo __('Вернуться в корзину') ?>
                        </a>
                        <button class="yellow-btn cart-info__btn">
                            <?php echo __('Перейти к оплате') ?>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="gcell--24 gcell--md-10 cart-info__right">
        <div class="cart-info__order-box">
            <p class="cart_bold"><?php echo __('Ваш заказ') ?></p>
            <p class="cart-info__order-text"><?php echo __('Сейчас в Вашей корзине:') ?></p>
            <p class="cart-info__order-text">
                <span class="cart_bold"><?php echo count($cart); ?></span> <?php echo __('товара на сумму') ?>
                <span class="cart_bold"><?php echo $total; ?><?php echo $currency ?> </span>
            </p>
            <div class="cart-info__items">
                <?php foreach ($cart as $key => $val):
                    $obj = $cart[$val['id']]['obj'];

                    $data = Support::calculatePriceWithCurrency($obj, $_currency, $_currentCurrency);
                    $cost = $data['cost'];
                    $cost_old = $data['cost_old'];
                    $currency = $data['currency'];
                    $item_sum = $cost * $val['count']; ?>
                    <div class="cart-item cart-item--justify">
                        <div class="cart-item__img-box">
                            <div class="cart-item__img-wrap">
                                <a href="<?php echo HTML::link($obj->alias); ?>">
                                    <?php if (is_file(HOST . Core\HTML::media('images/catalog/original/' . $obj->image, false))): ?>
                                        <img class="cart-item__img lozad"
                                             src="<?php echo HTML::media('images/catalog/original/' . $obj->image); ?>"
                                             alt="<?php echo $obj->name; ?>">
                                    <?php else: ?>
                                        <img class="cart-item__img lozad"
                                             src="<?php echo HTML::media('assets/images/placeholders/no-image.jpg'); ?>"
                                             alt="<?php echo $obj->name; ?>">
                                    <?php endif; ?>
                                </a>
                            </div>
                        </div>
                        <div class="cart-item__info-box cart-item__info-box--center">
                            <div class="cart-info__text">
                                <p class="cart-item__name"><?php echo $obj->name; ?></p>
                                <?php if ($obj->sizes): ?>
                                    <div class="">
                                        <span><?php echo __('Размер') ?>: </span>
                                        <span class="_dark">
                                <?php
                                $i = 0;
                                foreach ($obj->sizes as $size) {
                                    echo $size->size;
                                    $i++;
                                    if ($i < count($obj->sizes)) {
                                        echo ', ';
                                    }
                                } ?>
                                </span>
                                    </div>
                                <?php endif; ?>
                                <div class="">
                                    <span><?php echo __('Кол.во всего:') ?></span>
                                    <span class="_dark"><?php echo $val['count']; ?> шт.</span>
                                </div>
                                <?php if ($obj->color): ?>
                                    <div class="cart-item__color-box">
                                        <span><?php echo __('Цвет') ?>:</span>
                                        <span class="cart-item__color"
                                              style="background-color: <?php echo $obj->color ?>"
                                              title="<?php echo $obj->color_name; ?>"></span>
                                    </div>
                                <?php endif; ?>
                            </div>
                            <div class="cart-info__price">
                                <span class="cart-info__order-text"><?php echo __('Сумма') ?>:</span>
                                <br>
                                <span class="cart-info__order-price">
                                    <?php echo $item_sum; ?>
                                    <span class="currency"><?php echo $currency; ?></span>
                                </span>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
            <div class="cart-info__order-total">
                <a href="<?php echo HTML::link('cart'); ?>"
                   class="yellow-btn cart-info__btn"><?php echo __('Редактировать') ?></a>
                <div>
                    <span class="cart-info__order-text"><?php echo __('Общая сумма заказа') ?>:</span>
                    <br>
                    <span class="cart-info__order-price">
                        <?php echo $total; ?>
                        <span class="currency"><?php echo $currency; ?></span>
											</span>
                </div>
            </div>
        </div>
    </div>
</div>
