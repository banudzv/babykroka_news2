<?php

use Core\HTML;
use Core\Support;
use Core\User;

?>
<table align="left" border="1" cellpadding="5" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th></th>
        <th><?php echo __('Наименование') ?></th>
        <th><?php echo __('Цена') ?></th>
        <th><?php echo __('Количество') ?></th>
        <th><?php echo __('Итог') ?></th>
    </tr>
    </thead>
    <tbody>
    <?php $amount = 0; ?>
    <?php foreach ($cart as $obj): ?>
        <?php
        $data = Support::calculatePriceWithCurrency($obj,$_currency,$_currentCurrency);
        $cost = $data['cost'];
        $cost_old = $data['cost_old'];
        $currency = $data['currency'];

        $amt = (int)$obj->count * $cost; ?>
        <?php $amount += $amt; ?>
        <tr>
            <td>
                <?php if (is_file(HOST . HTML::media('images/catalog/original/' . $obj->image, false))): ?>
                    <img src="<?php echo HTML::media('images/catalog/original/' . $obj->image, true, $scheme); ?>"
                         width="150" alt="<?php echo $obj->name; ?>" title="<?php echo $obj->name; ?>"/>
                <?php else: ?>
                    <img src="<?php echo HTML::media('assets/images/placeholders/no-image.jpg', true, $scheme); ?>"
                         width="80" alt="<?php echo $obj->name; ?>" title="<?php echo $obj->name; ?>"/>
                <?php endif; ?>
            </td>
            <td>
                <?php echo $obj->name; ?>
                <br>
                <?php echo __('Код товара') ?>: <?php echo $obj->id; ?>
                <br>
                <?php if ($obj->sizes): ?>
                    <?php echo 'Размер'; ?>: <?php
                    $i = 0;
                    foreach ($obj->sizes as $size) {
                        echo $size->size;
                        $i++;
                        if ($i < count($obj->sizes)) {
                            echo ', ';
                        }
                    }
                    ?>
                    <br>
                <?php endif; ?>
                <?php if (!empty($obj->color_name)): ?>
                    <?php echo __('') ?><?php echo 'Цвет'; ?>: <?php echo $obj->color_name; ?>
                <?php endif; ?>
            </td>
            <td><?php echo $cost; ?><?php echo $currency ?></td>
            <td><?php echo $obj->count; ?></td>
            <td><?php echo $obj->count * $cost; ?><?php echo $currency ?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
    <tfoot>
    <tr>
        <th colspan="4"><b><?php echo __('ВСЕГО') ?>:</b></th>
        <th><?php echo $amount; ?><?php echo $currency ?> </th>
    </tr>
    </tfoot>
</table>
