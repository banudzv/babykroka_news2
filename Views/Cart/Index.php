<?php

use Core\HTML;
use Core\Support;
use Core\User;

$sell_size = $_SESSION['sell_size'];
if (count($cart)) {
    $total = 0;
    foreach ($cart as $key => $val) {
        $obj = $cart[$val['id']]['obj'];
        $wholeSale = $sell_size && $obj->sell_size ? 1 : 0;
        $data = Support::calculatePriceWithCurrency($obj, $_currency, $_currentCurrency);
        $cost = $data['cost'];
        $cost_old = $data['cost_old'];
        $currency = $data['currency'];
        $item_sum = $cost * $val['count'];
        $total += $item_sum;

    }
}
?>
<div class="cart__content grid">
    <?php if (count($cart)): ?>
        <div class="cart__items-wrap js-cart-static">
            <?php echo \Core\Widgets::get('Cart/ItemList', ['cart' => $cart, 'sell_size' => $sell_size, 'counts' => $counts]); ?>
        </div>
        <div class="cart__total-wrap" data-total>
            <div>
                <p class="history-total__title"><?php echo __('Общая сумма заказа') ?></p>
                <p class="history-total__sub-title">*<?php echo __('Сумма указана без учета доставки') ?> </p>
            </div>
            <div class="history-total__price-box history-total__price-box--right">
                <span><?php echo __('Итого') ?>:</span>
                <p class="history-total__price">
                    <span class="js-total-price"><?php echo $total; ?></span>
                    <span><?php echo $currency; ?></span>
                </p>
            </div>
        </div>
    <?php endif ?>
    <p class="emptyCartBlock <?php echo (count($cart)) ? '_hide' : ''; ?>"
       data-total-empty><?php echo __('Ваша корзина пуста') ?>.<a
                href="<?php echo HTML::link('catalog'); ?>"><?php echo __('Начните делать покупки прямо сейчас!') ?></a>
    </p>

    <div class="cart__total-buttons">
        <a href="<?php echo HTML::link('catalog') ?>" class="cart__total-btn yellow-border-btn">
            <?php echo __('Продолжить покупки') ?>
        </a>
        <a href="<?php echo HTML::link('cart/ordering'); ?>" data-total
           class="cart__total-btn yellow-btn  <?php echo (count($cart)) ? '' : '_hide'; ?>">
            <?php echo __('Перейти к оформлению') ?>
        </a>
    </div>
    <?php if (count($cart)): ?>
        <?php echo \Core\Widgets::get('Item_Related'); ?>
    <?php endif ?>
</div>
