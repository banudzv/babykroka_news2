<?php

use Core\HTML;
use Core\Config;

?>


<div class="confirm-order__content grid">
    <div class="gcell--24 gcell--md-12 confirm-order__text-box">
        <?php if($status):?>
            <p class="confirm-order__text">
                Ваш заказ
                <span class="confirm-order__number">№ <?php echo $order->id; ?></span>
                на сумму <?php echo $order->amount . ' '. $order->currency; ?> создан!
            </p>
            <p class="confirm-order__phone-text">Статус заказа: <?php echo $status;?></p>
        <?php else:?>
            <p class="confirm-order__text">
                Ваш заказ
                <span class="confirm-order__number">№ <?php echo $order->id; ?></span>
                на сумму <?php echo $order->amount . ' '. $order->currency; ?> оформлен!
            </p>
            <p class="confirm-order__phone-text">На ваш номер телефона будет отправлена смс с данными о заказе.</p>
        <?php endif;?>
        <a href="<?php echo HTML::link('/'); ?>" class="yellow-btn confirm-order__link">
            Перейти на главную
        </a>
    </div>
    <div class="gcell--md-12 confirm-order__img">
        <img class="lozad" src="<?php echo HTML::media('assets/images/like.png'); ?>" alt>
    </div>
</div>
