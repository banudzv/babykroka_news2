<?php
use Core\HTML;
?>
<div class="container container--faq">
    <div class="faq__content grid">
        <div class="gcell--24 gcell--sm-6 faq__sidebar">
            <div class="grid faq__sidebar-items">
                <div class="gcell--12 faq__sidebar-item">
                    <div class="faq__sidebar-toggle is-active wstabs-button" data-wstabs-ns="group-1" data-wstabs-button="1">
                        <span class="faq__sidebar-icon">
                            <svg>
                                <use xlink:href="<?php echo HTML::media('assets/images/sprites/icons.svg#delivery-icon');?>"></use>
                            </svg>
                        </span>
                        <span class="faq__sidebar-text"><?php echo __('Доставка') ?></span>
                    </div>
                </div>
                <div class="gcell--12 faq__sidebar-item">
                    <div href="#" class="faq__sidebar-toggle wstabs-button" data-wstabs-ns="group-1" data-wstabs-button="2">
                        <span class="faq__sidebar-icon">
                            <svg>
                                <use xlink:href="<?php echo HTML::media('assets/images/sprites/icons.svg#pay-icon');?>"></use>
                            </svg>
                        </span>
                        <span class="faq__sidebar-text"><?php echo __('Оплата') ?></span>
                    </div>
                </div>
                <div class="gcell--12 faq__sidebar-item">
                    <div class="faq__sidebar-toggle wstabs-button" data-wstabs-ns="group-1" data-wstabs-button="3">
                        <span class="faq__sidebar-icon">
                            <svg>
                                <use xlink:href="<?php echo HTML::media('assets/images/sprites/icons.svg#box-icon-2');?>"></use>
                            </svg>
                        </span>
                        <span class="faq__sidebar-text"><?php echo __('') ?>Оптовикам</span>
                    </div>
                </div>
                <div class="gcell--12 faq__sidebar-item">
                    <div class="faq__sidebar-toggle wstabs-button" data-wstabs-ns="group-1" data-wstabs-button="4">
                        <span class="faq__sidebar-icon">
                            <svg>
                                <use xlink:href="<?php echo HTML::media('assets/images/sprites/icons.svg#percentage-icon');?>"></use>
                            </svg>
                        </span>
                        <span class="faq__sidebar-text"><?php echo __('Акции') ?></span>
                    </div>
                </div>
            </div>
            <div class="faq__sidebar-bottom">
                <button class="faq__sidebar-callback js-init" data-mfp="ajax" data-mfp-src="<?php echo \Core\HTML::link('popup/getPopup');?>" data-param='<?php echo json_encode(['name' => 'Question', 'param' => []]);?>'>
                    <span class="faq__callback-text"><?php echo __('Задать вопрос менеджеру?') ?></span>
                </button>
            </div>
        </div>
        <div class="gcell--24 gcell--sm-18 faq__main">
            <?php foreach ($result as $key => $value):?>
                <div class="wstabs-block <?php echo ($key == 1)? 'is-active' : '' ;?>" data-wstabs-ns="group-1" data-wstabs-block="<?php echo ( $key) ?>">
                    <?php foreach ($result[$key] as $obj) :?>
                        <div class="faq__item">
                            <div class="faq__item-top">
                                <span class="faq__item-icon"></span>
                                <span class="faq__item-title"><?php echo $obj->name; ?></span>
                            </div>
                            <div class="faq__item-bottom">
                                <p class="faq__item-text">
                                    <?php echo $obj->text; ?>
                                </p>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>
