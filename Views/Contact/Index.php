<div class="container">
    <?php echo $text; ?>
</div>

<div class="contacts__box">
    <?php foreach ($result as $key => $obj):
        $phones = explode(',', $obj->phones);
    ?>
    <div class="contacts-item grid">
        <div class="contacts-item__top-wrap" data-index="<?php echo $key; ?>">
            <div class="container">
                <div class="contacts-item__top">
                    <div class="contacts-item__address-box">
                        <p class="contacts-item__name"><?php echo $obj->name; ?></p>
                        <p class="contacts-item__address"><?php echo $obj->address; ?></p>
                    </div>
                    <?php if (!empty($phones)):?>
                    <div class="contacts-item__phones-box">
                        <?php foreach ($phones as $phone):?>
                        <a href="tel:+<?php echo $phone; ?>" class="contacts-item__phone"><?php echo $phone; ?></a>
                        <?php endforeach; ?>
                    </div>
                    <?php endif ?>
                    <div class="contacts-item__schedule-box">
                        <div class="contacts-item__schedule-item">
                            <div>
                                <?php echo $obj->schedule; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="contacts-item__bottom">
            <div class="map" id="map" data-lat="<?php echo $obj->latitude; ?>" data-lng="<?php echo $obj->longitude; ?>" style="position: relative; overflow: hidden;"></div>
        </div>
    </div>
    <?php endforeach;?>
</div>
<div class="container contacts__form-wrap">
    <div class="contacts__form-box grid">
        <div class="gcell--24 gcell--ms-12 contacts__form-text">
            <p class="main-title"><?php echo __('Написать нам') ?></p>
            <p class="form-text">
                <?php echo __('Наша компания меняется непрерывно. Каждый день мы работаем над улучшением и оптимизацией деятельности Babykroha. Если, на ваш взгляд,мы что-то упустили в системе нашей работы, мы с радостью примем ваши предложения.') ?>
            </p>
        </div>
        <div class="gcell--24 gcell--ms-12">
            <form class="js-form contacts__form grid js-init" data-ajax="<?php echo \Core\HTML::link('form/contacts');?>">
                <label class="contacts__data-label">
                    <input required type="text" name="contacts-name" data-name="name" id="contacts-name" placeholder="<?php echo __('Ваше имя') ?>" class="simple-input contacts__data-input" data-rule-word="true" data-rule-minlength="2" <?php echo (\Core\User::info())? 'value="'.\Core\User::info()->name.'"' : ''?>>
                </label>
                <label class="contacts__data-label">
                    <input type="email" name="contacts-mail" data-name="email" id="contacts-mail" placeholder="Email" class="simple-input contacts__data-input" <?php echo (\Core\User::info())? 'value="'.\Core\User::info()->email.'"' : ''?>>
                </label>
                <label class="contacts__data-label">
                    <input type="tel" name="contacts-phone" data-name="phone" id="contacts-phone" class="simple-input contacts__data-input js-init" placeholder="+38(***) ***-**-**" required data-rule-phoneua="true" data-phonemask="+38(***) ***-**-**" data-phonemask-android="+38(***) ***-**-**" <?php echo (\Core\User::info())? 'value="'.\Core\User::info()->phone.'"' : '';?>>
                </label>
                <label class="contacts__data-label contacts__data-label--textarea">
                    <textarea placeholder="Сообщение" class="contacts__data-textarea" name="contacts-msg" data-name="text" id="contacts-msg" cols="30" rows="10"></textarea>
                </label>
                <?php echo \Core\HTML::csrfField();?>
                <button class="yellow-btn contacts__form-submit">
                    <?php echo __('Отправить') ?>
                </button>
            </form>
        </div>
    </div>
</div>
