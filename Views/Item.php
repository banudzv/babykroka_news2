<?php

use Core\Widgets;
use Core\Arr;

?>
<!DOCTYPE html>
<html lang="ru" dir="ltr">
<!-- (c) студия Wezom | www.wezom.com.ua -->
<head>
    <?php echo Widgets::get('Head', $_seo); ?>
    <?php foreach ($_seo['scripts']['head'] as $script): ?>
        <?php echo $script; ?>
    <?php endforeach ?>
</head>
<body>
<?php foreach ($_seo['scripts']['body'] as $script): ?>
    <?php echo $script; ?>
<?php endforeach ?>
<div class="wrapper">
    <div class="section section--main _flex-grow">
        <?php echo Widgets::get('Header'); ?>
        <div class="section gray-bg">
            <div class="container">
                <div>
                    <button class="yellow-btn product-page__back-btn" onclick="history.back();">
                        <?php echo __('Вернуться назад') ?>
                    </button>
                </div>
                <div class="product-page__title-box">
                    <?php echo $_breadcrumbs; ?>
                    <div class="product__pagination-wrap">
                        <a href="<?php echo \Core\HTML::link($previous['alias']) ?>"
                           class="product__pagination-link first">
                            <span class="product__pagination-link-icon">
                                <svg>
                                    <use xlink:href="<?php echo \Core\HTML::media('assets/images/sprites/icons.svg#right-arrow'); ?>"></use>
                                </svg>
                            </span>
                            <span><?php echo __('Предыдущий') ?></span>
                        </a>
                        <span>
                            <span class="product__pagination-current _medium"><?php echo $current; ?></span>
                            <span>/<?php echo $all; ?></span>
                        </span>
                        <a href="<?php echo \Core\HTML::link($next['alias']) ?>"
                           class="product__pagination-link second">
                            <span><?php echo __('Следующий') ?></span>
                            <span class="product__pagination-link-icon">
                                <svg>
                                    <use xlink:href="<?php echo \Core\HTML::media('assets/images/sprites/icons.svg#right-arrow'); ?>"></use>
                                </svg>
                            </span>
                        </a>
                    </div>

                </div>

                <?php echo $_content; ?>

                <?php echo Widgets::get('Item_ItemsSame'); ?>
                <?php echo Widgets::get('Item_Related'); ?>
            </div>
        </div>

        <?php echo Widgets::get('HiddenData'); ?>
        <?php echo Widgets::get('Footer', ['counters' => Arr::get($_seo, 'scripts.counter'), 'config' => $_config]); ?>
        <?php echo $GLOBAL_MESSAGE; ?>
    </div>
</body>
</html>
