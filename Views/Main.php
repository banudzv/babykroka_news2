<?php

use Core\Widgets;

?>
<!DOCTYPE html>
<html lang="ru" dir="ltr">
<!-- (c) студия Wezom | www.wezom.com.ua -->
<head>
    <?php echo Widgets::get('Head', $_seo); ?>
    <?php foreach ($_seo['scripts']['head'] as $script): ?>
        <?php echo $script; ?>
    <?php endforeach ?>
</head>
<body>
<?php foreach ($_seo['scripts']['body'] as $script): ?>
    <?php echo $script; ?>
<?php endforeach ?>

<div class="wrapper">
    <div class="section section--main _flex-grow">
        <?php echo Widgets::get('Header', ['class' => 'index'], false, false); ?>
        <div class="section">
            <?php echo Widgets::get('Index_Slider'); ?>
            <?php echo Widgets::get('Index_Category'); ?>
            <?php echo Widgets::get('Index_Novely'); ?>
            <?php echo Widgets::get('Index_Sale'); ?>
            <?php echo Widgets::get('Index_Info'); ?>
            <?php echo Widgets::get('Index_MostViewed'); ?>
            <div class="gray-bg">
                <?php echo Widgets::get('Index_News'); ?>
                <?php echo Widgets::get('Index_Seo'); ?>
            </div>
        </div>
    </div>
    <?php echo Widgets::get('HiddenData'); ?>
    <?= Widgets::get('OrganizationStructuredData') ?>
    <?php echo Widgets::get('Footer', ['counters' => Core\Arr::get($_seo, 'scripts.counter'), 'config' => $_config]); ?>
    <?php echo $GLOBAL_MESSAGE; ?>
</div>
</body>
</html>
