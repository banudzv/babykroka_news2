<?php
use Core\Widgets;
use Core\Arr;
use Core\HTML;
use Core\Text;
?>
<!DOCTYPE html>
<html lang="ru" dir="ltr">
<!-- (c) студия Wezom | www.wezom.com.ua -->
<head>
    <?php echo Widgets::get('Head', $_seo); ?>
    <?php foreach ($_seo['scripts']['head'] as $script): ?>
        <?php echo $script; ?>
    <?php endforeach ?>
</head>
<body>
<?php foreach ($_seo['scripts']['body'] as $script): ?>
    <?php echo $script; ?>
<?php endforeach ?>

<div class="wrapper">
    <div class="section section--main _flex-grow">
        <?php echo Widgets::get('Header'); ?>
        <div class="section gray-bg _pb-100">
            <div class="container">
                <div class="news-page__title-box">
                    <?php echo $_breadcrumbs; ?>
                    <h1 class="main-title"><?php echo Arr::get($_seo, 'h1'); ?></h1>
                    <p class="news-page__date-wrap">
                        <span class="news-page__date-text"><?php echo __('Опубликовано') ?>:</span>
                        <span class="news-page__date">
                            <?php echo date('d', $_date) .' '. Text::month($_date) .' '. date('Y', $_date); ?></span>
                    </p>
                </div>
                <?php echo $_content; ?>
            </div>

            <?php
            if(count($also_news)):?>
                <div class="index-news-block">
                    <div class="container">
                        <p class="index-news-block__title main-title"><?php echo __('Читайте также') ?></p>
                        <div class="grid">
                            <?php foreach ($also_news as $news):?>
                                <div class="news-item__wrap gcell--24 gcell--sm-8">
                                    <div class="news-item">
                                        <div class="news-item__top">
                                            <a href="<?php echo HTML::link('articles/' . $news->alias); ?>" class="news-item__img-link">
                                                <?php if (is_file(HOST . Core\HTML::media('images/articles/original/'.$news->image, false))): ?>
                                                    <span><img src="<?php echo HTML::media('/images/articles/medium/' . $news->image)?>" alt="<?php echo $news->name; ?>" class="lozad"></span>
                                                <?php else: ?>
													<span><img src="<?php echo HTML::media('assets/images/placeholders/no-image-articles.jpg'); ?>" alt="<?php echo $obj->name; ?>"></span>
                                                <?php endif; ?>

                                            </a>
                                        </div>
                                        <div class="news-item__bottom">
                                            <div class="news-item__date"><?php echo date('d/m', $news->date); ?></div>
                                            <div class="news-item__text-box">
                                                <p class="news-item__title">
                                                    <a href="<?php echo HTML::link('articles/' . $news->alias); ?>" class="news-item__link"><?php echo $news->name; ?></a>
                                                </p>
                                                <p class="news-item__text"><?php echo Text::limit_words(strip_tags($news->text), 500); ?></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>

                        </div>
                    </div>
                </div>
            <?php endif; ?>

        </div>
        <?php if (trim(strip_tags(Arr::get($_seo, 'seo_text')))): ?>
            <div class="seoTxt" id="seoTxt">
                <div class="wSize wTxt">
                    <?php echo Arr::get($_seo, 'seo_text'); ?>
                </div>
            </div>

        <?php endif ?>

    </div>
    <?php echo Widgets::get('HiddenData'); ?>
    <?php echo Widgets::get('Footer', ['counters' => Arr::get($_seo, 'scripts.counter'), 'config' => $_config]); ?>
	<?php echo $GLOBAL_MESSAGE; ?>
</div>
</body>
</html>
