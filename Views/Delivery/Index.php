<?php
use Core\Widgets;
use Core\HTML;
?>
<!DOCTYPE html>
<html lang="ru" dir="ltr">
<!-- (c) студия Wezom | www.wezom.com.ua -->
<head>
    <?php echo Widgets::get('Head', $_seo); ?>
    <?php foreach ($_seo['scripts']['head'] as $script): ?>
        <?php echo $script; ?>
    <?php endforeach ?>
</head>
<body>
<?php foreach ($_seo['scripts']['body'] as $script): ?>
    <?php echo $script; ?>
<?php endforeach ?>

<div class="wrapper">
    <div class="section section--main _flex-grow">
        <?php echo Widgets::get('Header'); ?>
        <div class="section gray-bg">
            <div class="container">
                <div class="delivery-info__title-wrap">
                    <?php echo $_breadcrumbs; ?>
                    <h1 class="main-title"><?php echo \Core\Arr::get($_seo, 'h1'); ?></h1>
                </div>
                <div class="delivery-info__content">
                    <div class="gcell--24 gcell--lg-12">
                        <div class="grid delivery-info__item">
                            <div class="gcell--24 second__block">
                                <div class="delivery-info__item-left">
                                    <div class="open_delivery"><span>+</span></div>
                                    <p class="delivery-info__title"><?php echo __('Оплата') ?></p>
                                </div>
                                <div class="delivery-info__item-right">
                                    <div class="delivery-info__icon">
                                        <svg>
                                            <use xlink:href="<?php echo HTML::media('assets/images/sprites/icons.svg#payment-icon');?>"></use>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                            <div class="gcell--24 delivery-info__content none__block">
                                <?php echo $_pay; ?>
                            </div>
                        </div>
                        <div class="grid delivery-info__item">
                            <div class="gcell--24 second__block">
                                <div class="delivery-info__item-left">
                                    <div class="open_delivery"><span>+</span></div>
                                    <p class="delivery-info__title"><?php echo __('Возврат') ?></p>
                                </div>
                                <div class="delivery-info__item-right">
                                    <div class="delivery-info__icon">
                                        <svg>
                                            <use xlink:href="<?php echo HTML::media('assets/images/sprites/icons.svg#return-icon');?>"></use>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                            <div class="gcell--24 delivery-info__content none__block">
                                <?php echo $_return; ?>
                            </div>
                        </div>
                    </div>
                    <div class="gcell--24 gcell--lg-12">
                        <div class="grid delivery-info__item">
                            <div class="gcell--24 second__block">
                                <div class="delivery-info__item-left">
                                    <div class="open_delivery"><span>+</span></div>
                                    <p class="delivery-info__title"><?php echo __('Доставка товаров') ?></p>
                                </div>
                                <div class="delivery-info__item-right">
                                    <div class="delivery-info__icon">
                                        <svg>
                                            <use xlink:href="<?php echo HTML::media('assets/images/sprites/icons.svg#delivery-icon-new');?>"></use>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                            <div class="gcell--24 delivery-info__content none__block">
                                <?php echo $_delivery; ?>
                            </div>
                        </div>
<!--                        <div class="grid delivery-info__item">-->
<!--                            <div class="gcell--24 second__block">-->
<!--                                <div class="delivery-info__item-left">-->
<!--                                    <div class="open_delivery"><span>+</span></div>-->
<!--                                    <p class="delivery-info__title">--><?php //echo __('Публичный договор и условия конфиденциальности') ?><!--</p>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="gcell--24 delivery-info__content none__block">-->
<!--                                --><?php //echo $_delivery; ?>
<!--                            </div>-->
<!--                        </div>-->
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<?php echo Widgets::get('HiddenData'); ?>
<?php echo Widgets::get('Footer', ['counters' => Core\Arr::get($_seo, 'scripts.counter'), 'config' => $_config]); ?>
<?php echo $GLOBAL_MESSAGE; ?>
</div>
</body>
</html>
