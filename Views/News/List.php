<?php
use Core\HTML;
use Core\Text;

?>
    <div class="articles__box">
        <div class="container">
<?php /**


            <div class="articles-item grid">
                <div class="gcell--md-12 articles-item__img-wrap">
                    <a href="#" class="articles-item__img-link">
                        <img src="/Media/assets/images/news-img-4.png?v=1525382850651" alt class="lozad">
                    </a>
                </div>
                <div class="gcell--24 gcell--md-12">
                    <div class="articles-item__text-box">
                        <div class="articles-item__title-box">
                            <p class="articles-item__title">
                                <a href="#" class="articles-item__link">Тренды 2018: Клетка</a>
                            </p>
                            <p class="articles-item__date">22 / 04</p>
                        </div>
                        <p class="articles-item__text">Приведем несколько примеров того, как с помощью этого тренда можно разнообразить свой образ. Лиловое пальто в крупную клетку с тонкими белыми полосами будет стройнить вашу фигуру и придаст вам нежности. Оно подойдет всем, кто предпочитает естественность и спокойствие в одежде. С открытым верхом можно сочетать шарф или однотонный свитер. </p>
                    </div>
                </div>
            </div>

            <div class="articles-item grid">
                <div class="gcell--md-12 articles-item__img-wrap">
                    <a href="#" class="articles-item__img-link">
                        <img src="/Media/assets/images/news-img-5.png?v=1525382850651" alt class="lozad">
                    </a>
                </div>
                <div class="gcell--24 gcell--md-12">
                    <div class="articles-item__text-box">
                        <div class="articles-item__title-box">
                            <p class="articles-item__title">
                                <a href="#" class="articles-item__link">Тренды 2018: Клетка</a>
                            </p>
                            <p class="articles-item__date">22 / 04</p>
                        </div>
                        <p class="articles-item__text">Приведем несколько примеров того, как с помощью этого тренда можно разнообразить свой образ. Лиловое пальто в крупную клетку с тонкими белыми полосами будет стройнить вашу фигуру и придаст вам нежности. Оно подойдет всем, кто предпочитает естественность и спокойствие в одежде. С открытым верхом можно сочетать шарф или однотонный свитер. </p>
                    </div>
                </div>
            </div>

            <div class="articles-item grid">
                <div class="gcell--md-12 articles-item__img-wrap">
                    <a href="#" class="articles-item__img-link">
                        <img src="/Media/assets/images/news-img-4.png?v=1525382850651" alt class="lozad">
                    </a>
                </div>
                <div class="gcell--24 gcell--md-12">
                    <div class="articles-item__text-box">
                        <div class="articles-item__title-box">
                            <p class="articles-item__title">
                                <a href="#" class="articles-item__link">Тренды 2018: Клетка</a>
                            </p>
                            <p class="articles-item__date">22 / 04</p>
                        </div>
                        <p class="articles-item__text">Приведем несколько примеров того, как с помощью этого тренда можно разнообразить свой образ. Лиловое пальто в крупную клетку с тонкими белыми полосами будет стройнить вашу фигуру и придаст вам нежности. Оно подойдет всем, кто предпочитает естественность и спокойствие в одежде. С открытым верхом можно сочетать шарф или однотонный свитер. </p>
                    </div>
                </div>
            </div>

            <div class="articles-item grid">
                <div class="gcell--md-12 articles-item__img-wrap">
                    <a href="#" class="articles-item__img-link">
                        <img src="/Media/assets/images/news-img-5.png?v=1525382850651" alt class="lozad">
                    </a>
                </div>
                <div class="gcell--24 gcell--md-12">
                    <div class="articles-item__text-box">
                        <div class="articles-item__title-box">
                            <p class="articles-item__title">
                                <a href="#" class="articles-item__link">Тренды 2018: Клетка</a>
                            </p>
                            <p class="articles-item__date">22 / 04</p>
                        </div>
                        <p class="articles-item__text">Приведем несколько примеров того, как с помощью этого тренда можно разнообразить свой образ. Лиловое пальто в крупную клетку с тонкими белыми полосами будет стройнить вашу фигуру и придаст вам нежности. Оно подойдет всем, кто предпочитает естественность и спокойствие в одежде. С открытым верхом можно сочетать шарф или однотонный свитер. </p>
                    </div>
                </div>
            </div>

        **/?>

<?php foreach ($result as $obj): ?>

    <div class="news clearFix">
        <?php if (is_file(HOST . HTML::media('images/news/big/' . $obj->image, false))): ?>
            <div class="fll">
                <a href="<?php echo HTML::link('news/' . $obj->alias); ?>" class="news_img">
                    <img src="<?php echo HTML::media('images/news/big/' . $obj->image); ?>" alt=""/>
                </a>
            </div>
        <?php endif ?>
        <div class="flr">
            <a href="<?php echo HTML::link('news/' . $obj->alias); ?>"
               class="name_news"><?php echo $obj->name; ?></a>
            <?php if ($obj->date): ?>
                <span class="dateNews"><?php echo date('d.m.Y', $obj->date); ?></span>
            <?php endif; ?>
            <p><?php echo Text::limit_words(strip_tags($obj->text), 100); ?></p>
            <a href="<?php echo HTML::link('news/' . $obj->alias); ?>" class="slide_but"><?php echo __('подробнее') ?></a>
        </div>
    </div>
<?php endforeach; ?>
        </div>
    </div>
<?php echo $pager; ?>
