<?php use Core\HTML; ?>
    <div class="category-block__item-wrap gcell--24 gcell--sm-12 gcell--md-6">
        <a href="<?php echo HTML::link('/catalog/malchikam')?>" class="category-block__item" style="background-image: url(<?php echo HTML::media('assets/images/shutterstock_774976798.png');?>)">
            <p class="category-block__item-title"><?php echo __('Мальчикам')?></p>
        </a>
    </div>
    <div class="category-block__item-wrap gcell--24 gcell--sm-12 gcell--md-6">
        <a href="<?php echo HTML::link('/catalog/devochkam')?>" class="category-block__item" style="background-image: url(<?php echo HTML::media('assets/images/img-girl.png');?>)">
            <p class="category-block__item-title"><?php echo __('Девочкам')?></p>
        </a>
    </div>
    <div class="category-block__item-wrap gcell--24 gcell--sm-12 gcell--md-6">
        <a href="<?php echo HTML::link('/catalog/novorozhdennye')?>" class="category-block__item" style="background-image: url(<?php echo HTML::media('assets/images/baby.png');?>)">
            <p class="category-block__item-title"><?php echo __('Новорожденным')?></p>
        </a>
    </div>
    <div class="category-block__item-wrap gcell--24 gcell--sm-12 gcell--md-6">
        <a href="<?php echo HTML::link('/catalog/igrushki')?>" class="category-block__item" style="background-image: url(<?php echo HTML::media('assets/images/toys.png');?>)">
            <p class="category-block__item-title"><?php echo __('Игрушки')?></p>
        </a>
    </div>
