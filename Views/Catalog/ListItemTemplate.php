<?php

use Core\HTML;
use Core\Support;
use Core\User;
$data = Support::calculatePriceWithCurrency($obj,$_currency,$_currentCurrency);
$cost = $data['cost'];
$cost_old = $data['cost_old'];
$currency = $data['currency'];
?>
<li>
    <a href="<?php echo HTML::link($obj->alias); ?>" class="img_tovar">
        <?php if (is_file(HOST . HTML::media('images/catalog/original/' . $obj->image, false))): ?>
            <img src="<?php echo HTML::media('images/catalog/original/' . $obj->image); ?>"
                 alt="<?php echo $obj->name; ?>">
        <?php else: ?>
            <img src="<?php echo HTML::media('assets/images/placeholders/no-image.jpg'); ?>" alt="">
        <?php endif ?>
        <?php echo Support::addItemTag($obj); ?>
    </a>
    <a href="<?php echo HTML::link($obj->alias); ?>"
       class="tovar_name"><span><?php echo $obj->name; ?></span></a>
    <?php if ($obj->sale && !empty($cost_old) && $cost_old !== '0.00'): ?>
        <div class="old_price"><span><?php echo $cost_old; ?></span> <?php echo $currency; ?></div>
    <?php endif; ?>
    <div class="tovar_price"><span><?php echo $cost; ?></span> <?php echo $currency; ?></div>
    <a href="<?php echo HTML::link($obj->alias); ?>" class="buy_but"><span><?php echo __('Купить') ?></span></a>
    <a href="#enterReg5" class="enterReg5 buy_for_click"
       data-id="<?php echo $obj->id; ?>"><span><?php echo __('Купить в один клик') ?></span></a>
</li>
