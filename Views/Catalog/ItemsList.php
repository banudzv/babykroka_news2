<?php

use Core\Config;
use Core\Cookie;
use Core\Route;
use Core\Support;
use Core\User;
use Core\HTML;
use Modules\Catalog\Models\Items;
use Modules\Catalog\Models\Prices;

?>


<div class="catalog__items grid js-load-wrap for-cookie">
    <?php foreach ($result as $obj):
        $sizes = Items::getProductSizeAmountById($obj->id)->as_array();
        $noSizes = false;
        $count = 0;
        $max_count = 0;
        $counts = [];
        $sizes_arr = [];
        $sell_size = $_SESSION['sell_size'];
        $wholeSale = $sell_size && $obj->sell_size ? 1 : 0;
        foreach ($sizes as $size) {
            if ($wholeSale) {
                if ($size->amount > 0) {
                    $count++;
                }
                $max_count += $size->amount - $active_count[$size->size];
                $counts[] = (!$size->amount) ? '0' : 1;
            } else {
                $counts[] = '0';
            }
            if($size->size !== '-') {
                $sizes_arr[] = $size->size;
            }
        }
        if (!count($sizes)) {
            $count = 1;
            $max_count = 10000;
            $noSizes = true;
        }

        if (count($sizes) == 1 && $sizes[0]->size === '-') {
            $count = 1;
            $max_count = $sizes[0]->amount - $active_count[$size->size];
            $noSizes = true;
        }
        $data = Support::calculatePriceWithCurrency($obj,$_currency,$_currentCurrency);
        $cost = $data['cost'];
        $cost_old = $data['cost_old'];
        $currency = $currency = Cookie::getWithoutSalt('currency');;
        ?>
        <div class="product-item__wrap gcell--24 gcell--ms-12 gcell--def-8 gcell--lg-6">
            <div class="product-item js-cart-item <?php if ($obj->sale): ?>product-item--sale<?php endif; ?>"  data-id="<?php echo $obj->id; ?>" data-url="<?php echo HTML::link('ajax/addToCart'); ?>">
                <div class="product-item__img-box">
                    <?php if ($obj->sale): ?>
                        <div class="product-item__label product-item__label--red">
                            <span><?php echo __('Скидка') ?></span>
                        </div>
                    <?php endif; ?>
                    <?php if ($obj->new): ?>
                        <div class="product-item__label product-item__label--blue">
                            <span><?php echo __('Новинка') ?></span>
                        </div>
                    <?php endif; ?>
                    <?php if ($obj->ucenka) : ?>
                        <div class="product-item__label product-item__label--green">
                            <span><?php echo __('Уценка') ?></span>
                        </div>
                    <?php endif; ?>

                    <!-- start fav -->
                    <?php if (User::info()): ?>
                        <button class="product-item__favorite js-favorite-btn <?php echo (in_array($obj->id, $favorite)) ? 'active' : ''; ?>"
                                data-url="<?php echo HTML::link('ajax/addToFavorite'); ?>">
                    <?php else: ?>
                        <button class="product-item__favorite js-init" data-mfp="ajax"
                                data-mfp-src="<?php echo HTML::link('popup/getPopup'); ?>"
                                data-param='<?php echo json_encode(['name' => 'Login', 'param' => []]); ?>'>
                    <?php endif; ?>
                            <span class="product-item__favorite-icon">
                                <svg>
                                    <use xlink:href="<?php echo HTML::svgSymbol('favorite-icon'); ?>"></use>
                                </svg>
                            </span>
                            <span class="product-item__favorite-icon--fill">
                                <svg>
                                    <use xlink:href="/Media/assets/images/sprites/icons.svg?v=1528183368000#like-icon"></use>
                                </svg>
                            </span>
                        </button>
                    <!-- finish fav -->

                    <button class="product-item__compare js-compare-btn <?php echo (in_array($obj->id, $favorite)) ? 'active' : ''; ?>"
                                data-url="<?php echo HTML::link('ajax/addToCompare'); ?>">
                        <span class="product-item__compare-icon">
                            <svg>
                                <use xlink:href="<?php echo HTML::svgSymbol('compare'); ?>"></use>
                            </svg>
                        </span>
                    </button>

                    <div class="product-item__img-wrap">
                        <div class="product-img-slider">
                            <?php if (!empty($obj->images)) : $hasRealImages = false; ?>
                                <?php foreach($obj->images as $image) : ?>
                                    <?php if (is_file(HOST . Core\HTML::media('images/catalog/original/'.$image->image, false))) : ?>
                                        <?php $hasRealImages = true; ?>
                                        <div>
                                            <a href="<?php echo HTML::link($obj->alias); ?>">
                                                <img class="product-item__img lozad"
                                                     src="<?php echo HTML::media('images/catalog/original/' . $image->image); ?>"
                                                     alt="<?php echo $obj->name; ?>">
                                            </a>
                                        </div>
                                    <?php endif; ?>
                                <?php endforeach; ?>

                                <?php if (!$hasRealImages) : ?>
                                    <div>
                                        <a href="<?php echo HTML::link($obj->alias); ?>">
                                            <img class="product-item__img lozad"
                                                 src="<?php echo HTML::media('assets/images/placeholders/no-image.jpg'); ?>"
                                                 alt="<?php echo $obj->name; ?>">
                                        </a>
                                    </div>
                                <?php endif; ?>
                            <?php else : ?>
                                <div>
                                    <a href="<?php echo HTML::link($obj->alias); ?>">
                                        <img class="product-item__img lozad"
                                             src="<?php echo HTML::media('assets/images/placeholders/no-image.jpg'); ?>"
                                             alt="<?php echo $obj->name; ?>">
                                    </a>
                                </div>
                            <?php endif; ?>
                        </div>
                        <div class="product-slider__controls product-item__buttons-box"></div>
                    </div>
                </div>
                <div class="product-item__rating">
                    <?php if ($obj->comments_count) : ?>
                        <div class="product-item__stars">
                            <ul class="rating-stars">
                                <?php for($i=1; $i <= 5; $i++): ?>
                                    <li class="rating-stars__item">
                                        <svg class="svg-icon <?php echo $obj->rating >= $i ? '_fill_yellow' : ''; ?>" viewBox="0 0 12 12" width="12" height="12"><use xlink:href="<?php echo HTML::media('assets/images/sprites/icons.svg#rating-star-icon');?>"></use></svg>
                                    </li>
                                <?php endfor; ?>
                            </ul>
                        </div>
                        <div class="product-item__reviews"><?= __('Отзывов') ?> <?= $obj->comments_count ?></div>
                    <?php endif; ?>
                </div>
                <div class="product-item__text-box">
                    <p class="product-item__name">
                        <a href="<?php echo HTML::link($obj->alias); ?>" title="<?php echo $obj->name; ?>">
                            <?php echo \Core\Text::limit_words2($obj->name, 10); ?>
                        </a>
                    </p>
                    <?php if (!$noSizes): ?>
                        <div class="product__sizes js-product-counts">
                            <?php foreach ($sizes as $size): ?>
                                <div class="product__size-item js-product-count-item <?php echo(!$size->amount ? 'disable' : ''); ?> <?php echo ($size->amount && $wholeSale) ? 'active' : '' ?> js-init">
                                    <span class="product__size-text"><?php echo $size->size; ?></span>
                                    <?php if ($wholeSale && $size->amount): ?>
                                        <span class="product__size-count js-product-count"
                                              data-max="<?php echo $size->amount; ?>"></span>
                                    <?php else: ?>
                                        <span class="product__size-count js-product-count" data-max="0"></span>
                                    <?php endif; ?>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>
                    <div class="product-item__bottom">
                        <div class="product-item__price-box">
                        <span class="product-item__price">
                            <span class="product-item__price-currency"><?php echo Support::productLabel();?></span>
                            <span class="<?php echo ($obj->sale && !empty($cost_old) && $cost_old !== '0.00') ? 'product-item__price-currency--sale' : '';?>">
                                <?= $currency == 'USD' ? $cost : ceil($cost) ?>
                            </span>
                            <span class="product-item__price-currency <?php echo ($obj->sale && !empty($cost_old) && $cost_old !== '0.00') ? 'product-item__price-currency--sale' : ''?>"><?php echo $currency;?></span>
                        </span>
                            <?php if ($obj->sale && !empty($cost_old) && $cost_old !== '0.00'): ?>
                                <span class="product-item__old-price">
                            <span><?= $currency == 'USD' ? $cost_old : ceil($cost_old) ?></span>
                        </span>
                            <?php endif; ?>
                        </div>
                        <?php if (User::info() && Cookie::getWithoutSalt('user-type') !== 'rozn'): ?>
                            <button class="yellow-btn  js-cart-add product-item__btn--hidden product-item__btn js-product-count-item active js-init"
                                    data-url="<?php echo HTML::link('ajax/addToCart') ?>" data-id="<?php echo $obj->id; ?>">
                                <?php echo __('Купить') ?>
                                <svg class="svg-icon"><use xlink:href="<?php echo HTML::media('assets/images/sprites/icons.svg#shopping-cart-icon');?>"></use></svg>
                            </button>
                        <?php elseif(User::info() && Cookie::getWithoutSalt('user-type') == 'rozn'):?>
                            <?php if (!$noSizes): ?>
                                <button class="yellow-btn product-item__btn--hidden product-item__btn js-product-count-item active js-init"
                                        data-mfp="ajax" data-size="<?php echo $sizes[0]->size; ?>"
                                        data-mfp-src="<?php echo \Core\HTML::link('popup/getPopup'); ?>"
                                        data-param='<?php echo json_encode(['name' => 'CartSizes', 'param' => ['item_id' => $obj->id, 'counts' => $counts,'wholeSale' => $wholeSale, 'from' => 1]]); ?>'>
                                    <?php echo __('Купить') ?>
                                    <svg class="svg-icon"><use xlink:href="<?php echo HTML::media('assets/images/sprites/icons.svg#shopping-cart-icon');?>"></use></svg>
                                </button>
                            <?php else:?>
                                <button class="yellow-btn  js-cart-add product-item__btn--hidden product-item__btn js-product-count-item active js-init"
                                        data-url="<?php echo HTML::link('ajax/addToCart') ?>" data-id="<?php echo $obj->id; ?>">
                                    <?php echo __('Купить') ?>
                                    <svg class="svg-icon"><use xlink:href="<?php echo HTML::media('assets/images/sprites/icons.svg#shopping-cart-icon');?>"></use></svg>
                                </button>
                            <?php endif; ?>

                        <?php else:?>
                            <?php if (!$noSizes): ?>
                                <button class="yellow-btn product-item__btn--hidden product-item__btn js-product-count-item active js-init"
                                        data-mfp="ajax" data-size="<?php echo $sizes[0]->size; ?>"
                                        data-mfp-src="<?php echo \Core\HTML::link('popup/getPopup'); ?>"
                                        data-param='<?php echo json_encode(['name' => 'CartSizes', 'param' => ['item_id' => $obj->id, 'counts' => $counts,'wholeSale' => $wholeSale, 'from' => 1]]); ?>'>
                                    <?php echo __('Купить') ?>
                                    <svg class="svg-icon"><use xlink:href="<?php echo HTML::media('assets/images/sprites/icons.svg#shopping-cart-icon');?>"></use></svg>
                                </button>
                            <?php else:?>
                                <button class="yellow-btn  js-cart-add product-item__btn--hidden product-item__btn js-product-count-item active js-init"
                                        data-url="<?php echo HTML::link('ajax/addToCart') ?>" data-id="<?php echo $obj->id; ?>">
                                    <?php echo __('Купить') ?>
                                    <svg class="svg-icon"><use xlink:href="<?php echo HTML::media('assets/images/sprites/icons.svg#shopping-cart-icon');?>"></use></svg>
                                </button>
                            <?php endif; ?>
                        <?php endif; ?>
                    </div>
                    <button class="product-item__btn--drops js-init"
                            data-mfp="ajax" data-mfp-src="<?php echo \Core\HTML::link('popup/getPopup');?>" data-param='<?php echo json_encode(['name' => 'Login', 'param' => []]);?>'>
                        <?php echo __('Стать оптовиком / Дропом') ?>
                    </button>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>
<div class="catalog__items-info">
    <?php if(!empty($pager) && !$last):?>
        <div class="catalog__items-counter">
            <?php echo __('Товаров на странице'); ?>
            <div class="catalog__items-select select2-wrap">
                <?php
                $baseUri = isset($_GET['search']) ? 'search' : 'catalog/' . Route::param('alias');
                $baseUri .= Route::param('filter') ? '/' . Route::param('filter') : '';
                $defaultLimit = Config::get('basic.limit_groups');
                ?>
                <select name="per-page" class="select2 pages-counter"
                        data-base="<?= HTML::link($baseUri) ?>"
                        data-default_limit="<?= $defaultLimit ?>"
                >
                    <option <?= !isset($_GET['per_page']) ? 'selected="selected"' : '' ?> value="<?= $defaultLimit ?>"><?= $defaultLimit ?></option>
                    <option <?= isset($_GET['per_page']) && $_GET['per_page'] == 40 ? 'selected="selected"' : '' ?> value="40">40</option>
                    <option <?= isset($_GET['per_page']) && $_GET['per_page'] == 100 ? 'selected="selected"' : '' ?> value="100">100</option>
                </select>
            </div>
        </div>

        <button class="catalog__show-more js-load-more"
                data-page="<?php echo (\Core\Route::param('page'))?:1?>"
                data-limit="<?= $_GET['per_page'] ?? $defaultLimit ?>"
                data-params='<?php echo json_encode( [
                    "group_id" => \Core\Route::param('group'),
                    "alias" => \Core\Route::controller(),
                    "page_params" => \Core\Route::params(),
                    "page_active" => \Core\Cookie::getWithoutSalt('page_active'),
                    'search' => ($_GET['search'])?: '',
                ])?>'
                data-url="<?php echo HTML::link('ajax/loadMore') ?>"
                data-backToCatalogUrl="<?php echo HTML::link('ajax/loadMoreBack') ?>">
            <?php echo __('Показать еще') ?>
        </button>
    <?php endif; ?>
    <div class="catalog__items-pagination js-pagination-container"><?php echo $pager ?></div>
</div>

