<?php

use Core\HTML;
use Core\Cookie;
use Core\Support;
use Core\Widgets;
use Core\User;

//$noSizes = false;
$count = 0;
$max_count = 0;
$counts = [];
$sizes_arr = [];
$sell_size = $_SESSION['sell_size'];
$wholeSale = $sell_size && $obj->sell_size ? 1 : 0;
foreach ($sizes as $size) {
    if ($wholeSale) {
        if ($size->amount > 0) {
            $count++;
        }
        $max_count += $size->amount - $active_count[$size->size];
        $counts[] = (!$size->amount) ? '0' : 1;
    } else {
        $counts[] = '0';
    }
    if ($size->size !== '-') {
        $sizes_arr[] = $size->size;
    }
}
if (!count($sizes)) {

    $count = 1;
    $max_count = 10000;
    $noSizes = true;
}

if (count($sizes) == 1 && $sizes[0]->size === '-') {
    $count = 1;
    $max_count = $sizes[0]->amount - $active_count[$size->size];
    $noSizes = true;
}

$data = Support::calculatePriceWithCurrency($obj, $_currency, $_currentCurrency);
$dataRozn = null;
if (!empty($prices_rozn)) {
    $dataRozn = Support::calculatePriceWithCurrency($prices_rozn, $_currency, $_currentCurrency);
}
$cost = $data['cost'];
$cost_old = $data['cost_old'];
$currency = $currency = Cookie::getWithoutSalt('currency');
?>

<?php
$class = "";
if ($popup):
    $class = "click_me";
endif;
?>
<script type="application/ld+json">
    <?= json_encode($structuredData, JSON_UNESCAPED_UNICODE) ?>

</script>
<div class="product__top-wrap grid grid--space-def <?php echo ($wholeSale && count($sizes)) ? 'wholesale' : '' ?> js-product-item js-cart-item"
     data-id="<?php echo $obj->id ?>" data-url="<?php echo HTML::link('ajax/addToCart') ?>">
    <div class="gcell--24 gcell--md-13 product__gallery-wrap">
        <div class="product__gallery grid">
            <?php if (count($images) > 0): ?>
                <div class="product__gallery-preview gcell--24 gcell--md-4">
                    <div class="gallery-preview__wrap popup-gallery">
                        <?php foreach ($images as $image): ?>
                            <div>
                                <div class="gallery-preview__item lozad"
                                     style="background-image: url(<?php echo (is_file(HOST . Core\HTML::media('images/catalog/original/' . $image->image, false))) ? HTML::media('images/catalog/original/' . $image->image) : HTML::media('assets/images/placeholders/no-image.jpg'); ?>)"></div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
                <div class="product__gallery-main gcell--24 gcell--md-20">
                    <?php foreach ($images as $image): ?>
                        <div data-mfp-src="<?php echo HTML::link('popup/getPopup'); ?>"
                             data-mfp="ajax"
                             data-param='<?php echo json_encode(['name' => 'Gallery', 'param' => [
                                 'images' => $images,
                                 'item' => $obj,
                                 'favorite' => $favorite,
                                 'data' => $data,
                                 'count' => $count,
                                 'count_3d' => $count_3d,
                                 'sizes' => $sizes,
                                 'noSizes' => $noSizes,
                                 'counts' => $counts,
                                 'table' => $table,
                                 'sizes_arr' => $sizes_arr,
                                 'wholeSale' => $wholeSale,
                             ]], JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES); ?>'
                             class="product__gallery-item js-init <?php echo $class ?>">
                            <img class="product__gallery-img lozad"
                                 src="<?php echo (is_file(HOST . Core\HTML::media('images/catalog/original/' . $obj->image, false))) ? HTML::media('images/catalog/original/' . $image->image) : HTML::media('assets/images/placeholders/no-image.jpg'); ?>"
                                 alt="<?php echo $obj->name; ?>" title="<?php echo $obj->name; ?>">
                            <?php if ($obj->sale && !empty($cost_old) && $cost_old !== '0.00'): ?>
                                <div class="product-item__label product-item__label--red">
                                    <span><?php echo __('Скидка') ?></span>
                                </div>
                            <?php endif; ?>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php else: ?>
                <div class="product-main">
                    <img class="product__gallery-img lozad"
                         src="<?php echo (is_file(HOST . Core\HTML::media('images/catalog/original/' . $obj->image, false))) ? HTML::media('images/catalog/original/' . $obj->image) : HTML::media('assets/images/placeholders/no-image.jpg'); ?>"
                         alt="<?php echo $obj->name; ?>" title="<?php echo $obj->name; ?>">
                    <?php if ($obj->sale && !empty($cost_old) && $cost_old !== '0.00'): ?>
                        <div class="product-item__label product-item__label--red">
                            <span><?php echo __('Скидка') ?></span>
                        </div>
                    <?php endif; ?>
                </div>
            <?php endif; ?>
        </div>

        <div class="product__gallery-360 is-hide js-product-media-360 threesixty js-threesixty"
             data-totalFrames="<?php echo $count_3d ?>"
             data-endFrame="<?php echo $count_3d ?>"
             data-imagePath="<?php echo HTML::media('images/catalog_3d/' . $obj->id); ?>/" data-ext=".png"
             data-filePrefix="">
            <div class="threesixty__spinner">
                <span>0%</span>
            </div>
            <ol class="threesixty__images"></ol>
            <div class="threesixty__buttons js-threesixty-buttons">
                <div class="threesixty__button js-threesixty-prev">
                    <svg>
                        <use xlink:href="/Media/assets/images/sprites/icons.svg?v=1528183368000#left-arrow-slider"></use>
                    </svg>
                </div>
                <div class="threesixty__button js-threesixty-play">
                    <svg>
                        <use xlink:href="/Media/assets/images/sprites/icons.svg?v=1528183368000#play-icon"></use>
                    </svg>
                </div>
                <div class="threesixty__button js-threesixty-pause">
                    <svg>
                        <use xlink:href="/Media/assets/images/sprites/icons.svg?v=1528183368000#pause-icon"></use>
                    </svg>
                </div>
                <div class="threesixty__button js-threesixty-next">
                    <svg>
                        <use xlink:href="/Media/assets/images/sprites/icons.svg?v=1528183368000#right-arrow-slider"></use>
                    </svg>
                </div>
            </div>
        </div>

        <?php if ($obj->show_3d): ?>
            <button class="product__3d-btn js-product-media-link is-image">
				<span class="_3d-icon">
					<svg>
						<use xlink:href="<?php echo HTML::media('assets/images/sprites/icons.svg#3d-icon'); ?>"></use>
					</svg>
				</span>
                <span class="picture-icon">
					<svg>
						<use xlink:href="<?php echo HTML::media('assets/images/sprites/icons.svg#picture-icon'); ?>"></use>
					</svg>
				</span>
            </button>
        <?php endif; ?>
    </div>
    <div class="gcell--24 gcell--md-11 product__info-wrap">
        <p class="product__code">
            <span><?php echo __('Код товара') ?>:</span>
            <span class="code"> <?php echo $obj->id ?></span>
        </p>
        <div class="product__name-wrap">
            <h1 class="product__name"><?php echo $obj->name; ?></h1>
            <?php if (User::info()): ?>
            <button class="product__favorite-btn js-favorite-btn <?php echo (in_array($obj->id, $favorite)) ? 'active' : ''; ?>"
                    data-url="<?php echo HTML::link('ajax/addToFavorite') ?>">
                <?php else: ?>
                <button class="product__favorite-btn js-init"
                        data-mfp="ajax" data-mfp-src="<?php echo HTML::link('popup/getPopup'); ?>"
                        data-param='<?php echo json_encode(['name' => 'Login', 'param' => []]); ?>'>
                    <?php endif; ?>

                    <span class="product__favorite-icon">
                    <svg>
                        <use xlink:href="<?php echo HTML::media('assets/images/sprites/icons.svg#favorite-icon'); ?>"></use>
                    </svg>
                </span>
                    <span class="product__favorite-icon--fill">
                    <svg>
                        <use xlink:href="<?php echo HTML::media('assets/images/sprites/icons.svg#like-icon'); ?>"></use>
                    </svg>
                </span>
                </button>
        </div>
        <div class="product__price-wrap">
            <div class="product__price-box">
                <?php if ($obj->sale && !empty($cost_old) && $cost_old !== '0.00'): ?>
                    <span class="product__price-old">
                        <span class="red-line-through"><?= $currency == 'USD' ?  $cost_old : ceil($cost_old) ?></span>
                        <span class="currency"><?php echo $currency; ?></span>
                </span>
                <?php endif; ?>
                <span class="product__price-current">
                    <span class="currency"><?php echo Support::productLabel(); ?></span>
                    <span class="<?php echo ($obj->sale && !empty($cost_old) && $cost_old !== '0.00') ? 'currency-red' : ''; ?>">
                       <?= $currency == 'USD' ? $cost : ceil($cost) ?>
                    </span>
                    <span class="currency"><?php echo $currency; ?></span>
                    <?php if (!empty($dataRozn)): ?>
                        <span class="currency-small" title="<?php echo __('Розничная цена') ?>">
                            (<?php echo "РОЗН " . $dataRozn['cost']; ?>
                            <?php echo $currency; ?>)</span>
                    <?php endif; ?>
                </span>
            </div>
            <?php /* Возможно пригодится эта надпись
            <!--            <p class="product__availability">-->
            <!--                <span>Есть в наличии</span>-->
            <!--                <span class="product__availability-icon">-->
            <!--                    <svg>-->
            <!--                        <use xlink:href="-->
            <?php //echo HTML::media('assets/images/sprites/icons.svg#check-icon'); ?><!--"></use>-->
            <!--                    </svg>-->
            <!--                </span>-->
            <!--            </p>-->
                */ ?>
        </div>
        <div class="product__availability <?php if ($obj->status == 0):  echo "product__availability-none" ?><?php endif; ?> " style="margin-top: 15px">
            <?php if ($obj->status == 1): ?>
                <?php echo __('В наличии') ?>
                <?php else: ?>
                <?php echo __('Нет в наличии') ?>
            <?php endif; ?>
        </div>
        <?php if ($obj->color): ?>
            <div class="product__colors-wrap">
                <span><?php echo __('Цвет') ?>:</span>
                <?php if ($obj->color_group): ?>
                    <?php foreach ($obj->color_group as $item): ?>
                        <a href="<?php echo HTML::link($item['alias']) ?>"
                           class="product__color-link <?php echo ($obj->color_alias === $item['color_alias'] && $obj->id == $item['id']) ? 'active' : NULL; ?>">
                            <span style="background-color: <?php echo $item['color'] ?>"
                                  title="<?php echo $item['color_name']; ?>"></span>
                        </a>
                    <?php endforeach; ?>

                <?php endif; ?>
            </div>
        <?php endif; ?>
        <div class="product__sizes-wrap">
            <?php if (count($table) || !$noSizes): ?>
                <div class="product__sizes-text">
                    <?php if (!$noSizes): ?>
                        <span><?php echo __('Доступные размеры (см.)') ?>:</span>
                    <?php endif; ?>
                    <?php if (count($table)): ?>
                        <span class="product__sizes-link js-init" data-mfp="ajax"
                              data-mfp-src="<?php echo \Core\HTML::link('popup/getPopup'); ?>"
                              data-param='<?php echo json_encode(['name' => 'TableSizes', 'param' => ['id' => $obj->id]]); ?>'><?php echo __('') ?>Таблица размеров
						<span class="product__sizes-icon">
							<svg>
								<use xlink:href="<?php echo HTML::media('assets/images/sprites/icons.svg#table-icon'); ?>"></use>
							</svg>
						</span>
					</span>
                    <?php endif; ?>
                </div>
                <?php if ($labels): ?>
                    <span class="product__sizes-item yellow-btn product-page__grid js-init" data-mfp="ajax"
                          data-mfp-src="<?php echo \Core\HTML::link('popup/getPopup'); ?>"
                          data-param='<?php echo json_encode(['name' => 'TableSizes', 'param' => ['id' => $obj->id]]); ?>'>
                             <?php echo __('Размерная сетка') ?>
                    </span>
                <?php endif; ?>
            <?php else: ?>
                <?php if ($labels): ?>
                    <div class="product__sizes-text"></div>

                    <span class="product__sizes-item yellow-btn product-page__grid js-init" data-mfp="ajax" style="margin-top: -20px;"
                          data-mfp-src="<?php echo \Core\HTML::link('popup/getPopup'); ?>"
                          data-param='<?php echo json_encode(['name' => 'TableSizes', 'param' => ['id' => $obj->id]]); ?>'>
                    <?php echo __('Размерная сетка') ?>
                   </span>
                <?php endif; ?>
            <?php endif; ?>
            <?php if (!$noSizes): ?>
                <div class="product__sizes js-product-counts">
                    <?php foreach ($sizes as $size): ?>
                        <?php if (!array_search($size->size, $active_sizes)): ?>
                            <div class="product__size-item js-product-count-item <?php echo(!$size->amount ? 'disable' : ''); ?> <?php echo ($size->amount && $wholeSale) ? 'active' : '' ?> js-init"
                                 data-mfp="ajax" data-size="<?php echo $size->size; ?>"
                                 data-mfp-src="<?php echo HTML::link('popup/getPopup'); ?>"
                                 data-param='<?php echo json_encode(['name' => 'CartSizes', 'param' => ['item_id' => $obj->id, 'counts' => $counts, 'wholeSale' => $wholeSale, 'from' => 1]]); ?>'>
                                <span class="product__size-text"><?php echo $size->size; ?></span>
                                <?php if ($wholeSale && $size->amount): ?>
                                    <span class="product__size-count js-product-count"
                                          data-max="<?php echo $size->amount; ?>"></span>
                                <?php else: ?>
                                    <span class="product__size-count js-product-count" data-max="0"></span>
                                <?php endif; ?>
                            </div>
                        <?php else: ?>
                            <div class="product__size-item js-product-count-item <?php echo(!$size->amount ? 'disable' : ''); ?> <?php echo ($size->amount && $wholeSale) ? 'active' : '' ?> js-init"
                                 data-mfp="ajax" data-size="<?php echo $size->size; ?>"
                                 data-mfp-src="<?php echo HTML::link('popup/getPopup'); ?>"
                                 data-param='<?php echo json_encode(['name' => 'CartSizes', 'param' => ['item_id' => $obj->id, 'counts' => $counts, 'wholeSale' => $wholeSale, 'from' => 1]]); ?>'>
                                <span class="size__item"><?php echo $size->size; ?></span>
                                <span class="size__count js-product-count"><?php echo $active_count[$size->size] ?></span>
                            </div>
                        <?php endif ?>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
        </div>
        <div class="product__info-items">
            <?php foreach ($specifications as $key => $value): ?>
                <div class="product__info-item">
                    <p class="product__info-text"><?php echo $key; ?>:</p>
                    <span class="product__info-dots"></span>
                    <p class="product__info-text"><?php echo $value; ?></p>
                </div>
            <?php endforeach; ?>
            <?php if (0/*!empty($itemAges)*/): ?>
                <div class="product__info-item product__info-item--ageBlock">
                    <p class="product__info-text"><?php echo __('Возраст') ?>:</p>
                    <div class="ageBlock">
                        <?php foreach ($itemAges as $age): ?>
                            <div class="ageBlock-item">
                                <span class="product__info-dots"></span>
                                <p class="product__info-text"><?php echo $age; ?></p>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            <?php endif; ?>
            <?php if ($manufacturer): ?>
                <div class="product__info-item">
                    <p class="product__info-text"><?php echo __('Производитель') ?>:</p>
                    <span class="product__info-dots"></span>
                    <p class="product__info-text"><?php echo $manufacturer->name; ?></p>
                </div>
            <?php endif; ?>
            <?php if ($brand): ?>
                <div class="product__info-item">
                    <p class="product__info-text"><?php echo __('Бренд') ?>:</p>
                    <span class="product__info-dots"></span>
                    <p class="product__info-text"><?php echo $brand->name; ?></p>
                </div>
            <?php endif; ?>
        </div>
        <div class="product__counts-box">
            <?php if ($noSizes): ?>
            <div class="product__counter">
                <button class="product__counter-btn minus js-product-minus">-</button>
                <input type="tel" class="product__counter-input js-product-input"
                       value="<?php echo ($count) ? $count : '1'; ?>"
                       data-max="<?php echo $max_count; ?>" disabled>
                <button class="product__counter-btn plus js-product-plus">+</button>
            </div>

            <div class="product__counts-text">
                <p class="product__count-total"><?php echo __('Всего товаров') ?>:
                    <span class="product__count js-product-count-all"><?php echo ($count) ? $count : '0'; ?></span>
                </p>
                <p><?php echo __('Общая стоимость') ?>:
                    <span class="product__total-price">
                    <span class="js-product-price"
                          data-price="<?php echo $cost ?>"><?php echo $count * $cost ?></span> <?php echo $currency; ?></span>
                </p>
            </div>
        </div>

    <?php if ($noSizes): ?>
        <div class="maximum-noty itemPage" style="display: none">
            <?php echo __('Выбрано максимальное количество штук.') ?>
        </div>
    <?php else: ?>
        <div class="maximum-noty itemPage" style="display: none">
            <?php echo __('Выбрано максимальное количество по данному размеру.') ?>
        </div>
    <?php endif; ?>
    <?php endif; ?>
        <div class="product__buttons-wrap">
            <?php if (!$noSizes): ?>
                <button class="yellow-btn product__buy-btn  <?php echo(!$size->amount ? 'disable' : ''); ?> <?php echo ($size->amount && $wholeSale) ? 'active' : '' ?> js-init"
                        data-mfp="ajax" data-size="<?php echo $size->size; ?>"
                        data-mfp-src="<?php echo HTML::link('popup/getPopup'); ?>"
                        data-param='<?php echo json_encode(['name' => 'CartSizes', 'param' => ['item_id' => $obj->id, 'counts' => $counts, 'wholeSale' => $wholeSale, 'from' => 1]]); ?>'>
                    <span class="product__size-text"><?php echo __('Купить') ?></span>
                </button>

            <?php else: ?>
                <button class="yellow-btn product__buy-btn js-cart-add"
                        data-url="<?php echo HTML::link('ajax/addToCart') ?>" data-id="<?php echo $obj->id; ?>">
                    <?php echo __('Купить') ?>
                </button>
            <?php endif; ?>
            <?php if (User::info() && Cookie::getWithoutSalt('user-type') !== 'rozn'): ?>
                <!--<button class="yellow-border-btn product__buy-btn js-cart-order"
                        data-url="<?php echo HTML::link('ajax/addToCartOrder') ?>">
                    Сформировать заказ
                </button>-->
            <?php elseif (User::info() && Cookie::getWithoutSalt('user-type') == 'rozn'): ?>
                <button class="yellow-border-btn product__buy-btn js-init" data-mfp="ajax"
                        data-mfp-src="<?php echo \Core\HTML::link('popup/getPopup'); ?>"
                        data-param='<?php echo json_encode(['name' => 'Callback', 'param' => []]); ?>'>
                    <?php echo __('Стать оптовиком / Дропом') ?>
                </button>
            <?php else: ?>
                <button class="yellow-border-btn product__buy-btn js-init" data-mfp="ajax"
                        data-mfp-src="<?php echo \Core\HTML::link('popup/getPopup'); ?>"
                        data-param='<?php echo json_encode(['name' => 'Login', 'param' => []]); ?>'>
                    <?php echo __('Стать оптовиком / Дропом') ?>
                </button>
            <?php endif; ?>
        </div>
    </div>
</div>
<div class="product__bottom-wrap grid grid--justify-between">
    <div class="product__bottom-left gcell--24 gcell--ms-14 _pr-def">
        <div class="product__tab-buttons  ">
            <button class="wstabs-button is-active product__tab-btn" data-wstabs-ns="group-1" data-wstabs-button="1">
                <?php echo __('Описание') ?>
            </button>
            <button class="wstabs-button product__tab-btn" data-wstabs-ns="group-1" data-wstabs-button="2">
                <?php echo __('Отзывы') ?>
            </button>
            <?php if(!empty($obj->video)) : ?>
            <button class="wstabs-button product__tab-btn" data-wstabs-ns="group-1" data-wstabs-button="3">
                <?php echo __('Видеообзор') ?>
            </button>
            <?php endif; ?>
        </div>
        <div>
            <div class="wstabs-block is-active product__tab" data-wstabs-ns="group-1" data-wstabs-block="1">
                <p class="product__text">
                    <?php echo $obj->content ?>
                </p>
            </div>
            <div class="wstabs-block product__tab" data-wstabs-ns="group-1" data-wstabs-block="2">
                <div class="product__reviews-box">
                    <button class="yellow-border-btn product__reviews-btn js-init" data-mfp="ajax"
                            data-mfp-src="<?php echo HTML::link('popup/getPopup'); ?>"
                            data-param='<?php echo json_encode(['name' => 'Review', 'param' => ['id' => $obj->id]]); ?>'>
                        <?php echo __('Оставить отзыв') ?>
                    </button>
                    <div class="product__reviews">
                        <?php echo Widgets::get('productReviews', ['reviews' => $reviews, 'pager' => $pager]); ?>
                    </div>
                </div>
            </div>
            <?php if(!empty($obj->video)) : ?>
            <div class="wstabs-block product__tab" data-wstabs-ns="group-1" data-wstabs-block="3">
                <div class="product__reviews-box">
                    <div class="yt-video">
                        <iframe class="yt-video__frame"
                                src="<?= $obj->video ?>"
                                title="YouTube video player"
                                frameborder="0"
                                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                allowfullscreen
                        ></iframe>
                    </div>
                </div>
            </div>
            <?php endif; ?>
        </div>
    </div>
    <div class="product__bottom-right gcell--24 gcell--ms-10">
        <div class="product__delivery-box">
            <?php foreach ($informations[0] as $information): ?>
                <?php if ($informations[$information->id]): ?>
                    <p class="product__delivery-title"><?php echo $information->name; ?></p>
                    <div class="product__delivery-item">
                        <?php foreach ($informations[$information->id] as $info): ?>
                            <div class="grid product__delivery-sub-item">
                                <div class="gcell--10">
                                    <p class="product__delivery-sub-title"><?php echo $info->name; ?></p>
                                </div>
                                <div class="gcell--14">
                                    <p class="product__delivery-text"><?php echo $info->description; ?></p>
                                </div>
                            </div>
                        <?php endforeach; ?>

                    </div>
                <?php else: ?>
                    <div class="product__delivery-item js-delivery-item">
                        <div class="product__delivery-title">
                            <?php echo $information->name; ?>
                            <span class="product__delivery-svg js-delivery-trigger">
                                <svg>
                                    <use xlink:href="<?php echo HTML::media('assets/images/sprites/icons.svg#arrow-up'); ?>"></use>
                                </svg>
                            </span>
                        </div>
                        <div class="product__delivery-text js-delivery-text"><?php echo $information->description; ?></div>
                    </div>
                <?php endif; ?>
            <?php endforeach; ?>
        </div>
    </div>
</div>

