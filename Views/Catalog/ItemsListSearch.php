<?php

use Core\Cookie;
use Core\Support;
use Core\User;
use Core\HTML;
use Modules\Catalog\Models\Prices;

?>

    <div class="catalog__items grid js-load-wrap">
        <?php foreach ($result as $obj):
            $data = Support::calculatePriceWithCurrency($obj,$_currency,$_currentCurrency);
            $cost = $data['cost'];
            $cost_old = $data['cost_old'];
            $currency = $data['currency'];
            ?>
            <div class="product-item__wrap gcell--24 gcell--sm-12 gcell--ms-8">
                <div class="product-item js-cart-item" data-id="<?php echo $obj->id; ?>"
                     data-url="<?php echo HTML::link('ajax/addToCart'); ?>">
                    <div class="product-item__img-box">
                        <?php if ($obj->sale): ?>
                            <div class="product-item__label product-item__label--red">
                                <span><?php echo __('Скидка') ?></span>
                            </div>
                        <?php endif; ?>
                        <?php if (\Core\User::info()): ?>
                        <button class="product-item__favorite js-favorite-btn <?php echo (in_array($obj->id, $favorite)) ? 'active' : ''; ?>"
                                data-url="<?php echo HTML::link('ajax/addToFavorite'); ?>">
                            <?php else: ?>
                            <button class="product-item__favorite js-init" data-mfp="ajax"
                                    data-mfp-src="<?php echo HTML::link('popup/getPopup'); ?>"
                                    data-param='<?php echo json_encode(['name' => 'Login', 'param' => []]); ?>'>
                                <?php endif; ?>
                                <span class="product-item__favorite-icon">
                                <svg>
                                    <use xlink:href="<?php echo HTML::svgSymbol('favorite-icon'); ?>"></use>
                                </svg>
                            </span>
                                <span class="product-item__favorite-icon--fill">
                                <svg>
                                    <use xlink:href="/Media/assets/images/sprites/icons.svg?v=1528183368000#like-icon"></use>
                                </svg>
                            </span>
                            </button>
                            <div class="product-item__img-wrap">
                                <a href="<?php echo HTML::link($obj->alias); ?>">
                                    <?php if (is_file(HOST . Core\HTML::media('images/catalog/original/'.$obj->image, false))): ?>
                                        <img class="product-item__img lozad"
                                             src="<?php echo HTML::media('images/catalog/original/' . $obj->image); ?>"
                                             alt="<?php echo $obj->name; ?>">
                                    <?php else: ?>
                                        <img class="product-item__img lozad"
                                             src="<?php echo HTML::media('assets/images/placeholders/no-image.jpg'); ?>"
                                             alt="<?php echo $obj->name; ?>">
                                    <?php endif; ?>
                                </a>
                            </div>
                    </div>
                    <div class="product-item__text-box">
                        <p class="product-item__name">
                            <a href="<?php echo HTML::link($obj->alias); ?>" title="<?php echo $obj->name; ?>">
                                <span class="is-select"><?php echo \Core\Text::limit_words2($obj->name, 5); ?></span>
                            </a>
                        </p>
                        <a href="<?php echo HTML::link('catalog/' . $cats[$obj->parent_id]['alias']); ?>"
                           class="product-item__category"><?php echo $cats[$obj->parent_id]['name'] ?></a>
                        <div class="product-item__price-box">
                        <span class="product-item__price">
                            <span class="product-item__price-currency"><?php echo Support::productLabel();?></span>
                            <span><?= $currency == 'USD' ? $cost : ceil($cost) ?></span>
                            <span class="product-item__price-currency"><?php echo $currency;?></span>
                        </span>
                            <?php if ($obj->sale && !empty($cost_old) && $cost_old !== '0.00'): ?>
                                <span class="product-item__old-price">
                            <span><?= $currency == 'USD' ? $cost_old : ceil($cost_old) ?></span>
                            <span class="product-item__price-currency"><?php echo $currency;?></span>
                        </span>
                            <?php endif; ?>
                        </div>
                        <button class="yellow-btn product-item__btn js-cart-add">
                            <?php echo __('Купить') ?>
                        </button>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
    <div class="catalog__items-info">
        <?php

        $text = 'товар';
        if (strlen((string)$total) > 1) {
            if ($total > 20) {
                $count = (string)$total;
                $count = (int)$count[strlen($count) - 1];
            } else {
                $count = 0;
            }
        } else {
            $count = $total;
        }
        if ($count == 0) {
            $text .= 'ов';
        } elseif ($count == 1) {
            $text .= '';
        } elseif ($count >= 2 && $count <= 4) {
            $text .= 'а';
        } elseif ($count > 4) {
            $text .= 'ов';
        }
        ?>
        <div class="catalog__items-counter"><?php echo $total . ' ' . $text ?></div>
        <div class="catalog__items-pagination js-pagination-container"><?php echo $pager ?></div>
    </div>

<?php if (!empty($pager) && !$last): ?>
    <button class="catalog__show-more js-load-more"
            data-page="1"
            data-params='<?php echo json_encode( [
                "group_id" => \Core\Route::param('group'),
                "alias" => \Core\Route::controller(),
                "page_params" => \Core\Route::params(),
                'search' => ($_GET['search'])? $_GET['search'] : '',
            ])?>'
            data-url="<?php echo HTML::link('ajax/loadMore') ?>"><?php echo __('показать еще') ?></button>
<?php endif; ?>
