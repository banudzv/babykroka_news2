<?php
use Core\HTML;
use Core\Text;

?>

<div class="container">
    <?php foreach ($result as $obj): ?>
        <div class="review-item">
            <div class="review-item__wrapper">
                <div class="review-item__wrapper-left">
                    <div class="review-item__name"><?php echo $obj->name; ?></div>
                    <div class="review-item__rating">
                        <span class="<?php if ( $obj->rating >= 1 ) echo 'active'?>"></span>
                        <span class="<?php if ( $obj->rating >= 2 ) echo 'active'?>"></span>
                        <span class="<?php if ( $obj->rating >= 3 ) echo 'active'?>"></span>
                        <span class="<?php if ( $obj->rating >= 4 ) echo 'active'?>"></span>
                        <span class="<?php if ( $obj->rating >= 5 ) echo 'active'?>"></span>
                    </div>
                </div>
                <div class="review-item__wrapper-right">
                    <div class="review-item__date"><?php echo date('d.m.Y', $obj->date); ?></div>
                </div>
            </div>
            <p><?php echo $obj->text; ?></p>

            <?php if ($obj->answer) :?>
                <div class="review-item__answer">
                    <div class="review-item__answer-name"><?php echo __('Админитратор') ?> <?php echo date('d.m.Y', $obj->date_answer); ?></div>
                    <div class="review-item__answer-text"><?php echo $obj->answer; ?></div>
                </div>
            <?php endif; ?>
        </div>
    <?php endforeach; ?>

    <?php echo $pager; ?>
</div>

<div class="container">
    <div class="review-container">
        <form class="js-form js-init" data-ajax="<?= HTML::link('form/review') ?>">
            <div class="review-container__title"><?php echo __('оставь свой отзыв') ?></div>
            <div class="review-popup__rating review-container--rating-wrapper">
                <div class="rating-group review-container--rating">
                    <input disabled checked class="rating__input rating__input--none" name="rating" data-name="rating" id="rating-none" value="0" type="radio">
                    <label aria-label="1 star" class="rating__label" for="rating-1"></label>
                    <input class="rating__input" name="rating" data-name="rating" id="rating-1" value="1" type="radio" checked>
                    <label aria-label="2 stars" class="rating__label" for="rating-2"></label>
                    <input class="rating__input" name="rating" data-name="rating" id="rating-2" value="2" type="radio" checked>
                    <label aria-label="3 stars" class="rating__label" for="rating-3"></label>
                    <input class="rating__input" name="rating" data-name="rating" id="rating-3" value="3" type="radio" checked>
                    <label aria-label="4 stars" class="rating__label" for="rating-4"></label>
                    <input class="rating__input" name="rating" data-name="rating" id="rating-4" value="4" type="radio" checked>
                    <label aria-label="5 stars" class="rating__label" for="rating-5"></label>
                    <input class="rating__input" name="rating" data-name="rating" id="rating-5" value="5" type="radio" checked>
                </div>
            </div>
            <label class="cart-registration__label">
                <input class="cart-registration__input simple-input" type="text" data-name="name" name="name" placeholder="<?php echo __('Имя') ?>" data-rule-bykvu="true"
                       data-rule-minlength="2" required="">
            </label>
            <label class="cart-registration__label">
                <input class="cart-registration__input simple-input" type="email" data-name="email" name="email" data-rule-email="true" placeholder="<?php echo __('E-mail') ?>"
                       data-rule-minlength="2" data-rule-required="true">
            </label>
            <label class="cart-registration__label contacts__data-label--textarea">
                <textarea class="review-popup__textarea" name="text" data-name="text" placeholder="Ваш отзыв" required=""></textarea>
            </label>
            <?php if (array_key_exists('token', $_SESSION)): ?>
                <input type="hidden" data-name="token" value="<?php echo $_SESSION['token']; ?>"/>
            <?php endif; ?>
            <button class="login-popup__submit login-popup__submit--yellow"><?php echo __('Отправить') ?></button>
        </form>
    </div>
</div>
