<?php
use Core\Widgets;
?>
<!DOCTYPE html>
<html lang="ru" dir="ltr">
<!-- (c) студия Wezom | www.wezom.com.ua -->
<head>
    <?php echo Widgets::get('Head', $_seo); ?>
    <?php foreach ($_seo['scripts']['head'] as $script): ?>
        <?php echo $script; ?>
    <?php endforeach ?>
</head>
<body>
<?php foreach ($_seo['scripts']['body'] as $script): ?>
    <?php echo $script; ?>
<?php endforeach ?>

<div class="wrapper">
    <div class="section section--main _flex-grow">
        <?php echo Widgets::get('Header'); ?>
        <div class="section _pb-100">
            <div class="container _ptb-lg">
                <div class="title main-title"><?php echo __('Карта сайта') ?></div>

                <div class="sitemap">
                    <?php echo \Core\View::tpl(['result' => $result, 'links' =>$links], 'Sitemap/Index');?>
                </div>
            </div>
        </div>
    </div>

    <?php echo Widgets::get('HiddenData'); ?>
    <?php echo Widgets::get('Footer', ['counters' => Core\Arr::get($_seo, 'scripts.counter'), 'config' => $_config]); ?>
	<?php echo $GLOBAL_MESSAGE; ?>
</div>
</body>
</html>
