<?php
use Core\Widgets;
use Core\Config;
use Core\Arr;
use Core\HTML;

$text = '';
if ($_count==1):
    $text = __('товар');
elseif ($_count < 5):
    $text = __('товара');
else:
    $text = __('товаров');
endif;

?>
<!DOCTYPE html>
<html lang="ru" dir="ltr">
<!-- (c) студия Wezom | www.wezom.com.ua -->
<head>
    <?php echo Widgets::get('Head', $_seo); ?>
    <?php foreach ($_seo['scripts']['head'] as $script): ?>
        <?php echo $script; ?>
    <?php endforeach ?>
</head>
<body>
<?php foreach ($_seo['scripts']['body'] as $script): ?>
    <?php echo $script; ?>
<?php endforeach ?>

<div class="seoTxt" id="seoTxt">
    <div class="wSize wTxt">
        <?php echo trim(strip_tags(Config::get('seo_text'))) ? Config::get('seo_text') : ''; ?>
    </div>
</div>
<div class="wrapper">
    <div class="section section--main _flex-grow">
        <?php echo Widgets::get('Header'); ?>
        <div class="section gray-bg">
            <div class="container">
                <div class="catalog__title-box">
                    <?php echo $_breadcrumbs; ?>
                    <h1 class="catalog-title main-title"><?php echo __('РЕЗУЛЬТАТ ПОИСКА') ?></h1>
                    <p class="js-select-word search-result__sub-title" data-select='Комбинезон'><?php echo __('По запросу') ?>
                        <span class="search-result__word">«<?php echo $_query?>»</span> <?php echo __('найдено') ?>: <?php echo $_count ?> <?php echo $text; ?></p>
                </div>
                <div class="catalog__mobile-filters">
                    <div class="_hide">
                        <nav id="filters-menu">

                            <?php echo Widgets::get('Groups_CatalogMenuLeftMobile'); ?>

                        </nav>
                    </div>
                </div>
                <div class="grid">
                    <?php echo Widgets::get('Groups_CatalogMenuLeft'); ?>
                    <div class="gcell--24 gcell--def-18 catalog__items-wrap">
                        <div class="catalog__items-top">
                            <button class="filters-menu-open yellow-btn">
                                <?php echo __('Фильтры') ?>
                            </button>

                            <?= Widgets::get('Groups_AddChild', ['class' => '_def-show']) ?>

                            <div class="catalog__filters-chosen"></div>
                            <div class="catalog__items-select-box select2-wrap">
                                <span><?php echo __('Сортировать по') ?>:</span>
                                <select id="" class="select2" style="width: 145px">
                                    <option value="<?php echo HTML::sortLink('sort', 'priceAsc');?>" <?php echo ($sort == 'priceAsc') ?'selected':'' ?>>Цене (по возростанию)</option>
                                    <option value="<?php echo HTML::sortLink('sort', 'priceDesc');?>" <?php echo ($sort == 'priceDesc') ?'selected':'' ?>>Цене (по убыванию)</option>
                                    <option value="<?php echo HTML::sortLink('sort', 'top');?>" <?php echo ($sort == 'top') ?'selected':'' ?>>Популярности</option>
                                    <option value="<?php echo HTML::sortLink('sort', 'alph');?>" <?php echo ($sort == 'alph') ?'selected':'' ?>>Алфавиту</option>
                                </select>
                            </div>
                        </div>
                        <?php echo $_content; ?>
                    </div>
                </div>
            </div>

            <?= Widgets::get('Groups_Dropshipping') ?>

        </div>
    </div>
    <?php echo Widgets::get('Footer', ['counters' => Arr::get($_seo, 'scripts.counter'), 'config' => $_config]); ?>
    <?php echo Widgets::get('HiddenData'); ?>
    <?php echo $GLOBAL_MESSAGE; ?>
</div>

</body>
</html>
