<?php
use Core\HTML;
use Core\Text;

?>
    <div class="articles__box">
        <div class="container">

<?php foreach ($result as $obj): ?>

    <div class="news clearFix">
        <?php if (is_file(HOST . HTML::media('images/news2/big/' . $obj->image, false))): ?>
            <div class="fll">
                <a href="<?php echo HTML::link('news2/' . $obj->alias); ?>" class="news_img">
                    <img src="<?php echo HTML::media('images/news2/big/' . $obj->image); ?>" alt=""/>
                </a>
            </div>
        <?php endif ?>
        <div class="flr">
            <a href="<?php echo HTML::link('news2/' . $obj->alias); ?>"
               class="name_news">
               <h2><?php echo $obj->name; ?></h2></a>
               <br>
               <p><?php  echo __('Просмотры:') .' ' . $obj->views ?></p>
               <br>
            <?php if ($obj->date): ?>
                <span class="dateNews"><?php echo date('d.m.Y', $obj->date); ?></span>
            <?php endif; ?>
            <p><?php echo Text::limit_words(strip_tags($obj->text), 200); ?></p>
            <a href="<?php echo HTML::link('news2/' . $obj->alias); ?>" class="slide_but"><?php echo __('подробнее') ?></a>
        </div>
    </div>
    <hr>
<?php endforeach; ?>
        </div>
    </div>
<?php echo $pager; ?>
