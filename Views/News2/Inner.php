<?php use Core\HTML; ?>
<div class="new clearFix">
    <?php if (is_file(HOST . HTML::media('images/news2/small/' . $obj->image, false))): ?>
        <div class="news_img">
            <img src="<?php echo HTML::media('images/news2/big/' . $obj->image); ?>" alt=""/>
        </div>
    <?php endif; ?>
    <div class="wTxt">
       <h2><?php echo $obj->name; ?></h2>
        <?php echo $obj->text; ?>
        <?php  echo __('Просмотры:') .' ' . $obj->views ?>
        <br>
        <a href="<?php echo HTML::link('news2'); ?>"
           class="back_to_news"><span><?php echo __('вернуться к списку новостей') ?></span></a>
    </div>
</div>
