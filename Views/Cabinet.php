<?php
use Core\Widgets;
use Core\Arr;
use Core\Config;
?>
<!DOCTYPE html>
<html lang="ru" dir="ltr">
<!-- (c) студия Wezom | www.wezom.com.ua -->
<head>
    <?php echo Widgets::get('Head', $_seo); ?>
    <?php foreach ($_seo['scripts']['head'] as $script): ?>
        <?php echo $script; ?>
    <?php endforeach ?>
</head>
<body>
<?php foreach ($_seo['scripts']['body'] as $script): ?>
    <?php echo $script; ?>
<?php endforeach ?>
<div class="wrapper">
    <div class="section section--main _flex-grow">
        <?php echo Widgets::get('Header'); ?>
        <div class="section gray-bg _pb-100">
            <div class="container">
                <div class="<?php echo Config::get('class'); ?>__title-wrap">
                    <?php echo $_breadcrumbs; ?>
                    <h1 class="main-title faq__title"><?php echo Arr::get($_seo,'h1'); ?></h1>
                </div>
                <div class="<?php echo Config::get('class'); ?>__content grid">
                    <?php echo Widgets::get('UserMenu'); ?>
                    <div class="gcell--24 gcell--sm-18 profile__main">
                        <?php echo $_content; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php echo Widgets::get('HiddenData'); ?>
    <?php echo Widgets::get('Footer', ['counters' => Arr::get($_seo, 'scripts.counter'), 'config' => $_config]); ?>
    <?php echo $GLOBAL_MESSAGE; ?>
</div>

</body>
</html>
