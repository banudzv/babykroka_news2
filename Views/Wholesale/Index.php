<?php
use Core\Widgets;
use Core\HTML;
?>
<!DOCTYPE html>
<html lang="ru" dir="ltr">
<!-- (c) студия Wezom | www.wezom.com.ua -->
<head>
    <?php echo Widgets::get('Head', $_seo); ?>
    <?php foreach ($_seo['scripts']['head'] as $script): ?>
        <?php echo $script; ?>
    <?php endforeach ?>
</head>
<body>
<?php foreach ($_seo['scripts']['body'] as $script): ?>
    <?php echo $script; ?>
<?php endforeach ?>

<div class="wrapper">
    <div class="section section--main _flex-grow">
        <?php echo Widgets::get('Header'); ?>
        <div class="section gray-bg _pb-100">
            <div class="wholesale-bg" style="background-image: linear-gradient(179deg, rgba(238, 238, 238, 0) 0%, #f5f6f8 100%)">
                <div class="container">
                    <div class="about__title-wrap">
                        <?php echo $_breadcrumbs; ?>
                        <h1 class="main-title"><?php echo \Core\Arr::get($_seo, 'h1'); ?></h1>
                    </div>
                </div>
            </div>
            <?php echo $_content;?>
        </div>

    </div>
</div>

<?php echo Widgets::get('HiddenData'); ?>
<?php echo Widgets::get('Footer', ['counters' => Core\Arr::get($_seo, 'scripts.counter'), 'config' => $_config]); ?>
<?php echo $GLOBAL_MESSAGE; ?>
</div>
</body>
</html>
