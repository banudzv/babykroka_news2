<?php

    ini_set('display_errors', 'on'); // Display all errors on screen
    header("Cache-Control: public");
    header("Expires: " . date("r", time() + 3600));
    header('Content-Type: text/html; charset=UTF-8');
    ob_start();
    @session_start();
    define('DS', '/');
    define('HOST', dirname(__FILE__)); // Root path
    define('MULTI_LANGUAGE', false);
    define('APPLICATION', 'frontend'); // Choose application - backend|frontend
    define('PROFILER', false); // On/off profiler
    define('START_TIME', microtime(true)); // For profiler. Don't touch!
    define('START_MEMORY', memory_get_usage()); // For profiler. Don't touch!

    require_once 'loader.php';

    set_time_limit(3600);
/* This is old version connect and i comment this part, but later it's may be used
    $client = new \SoapClient("http://sr.babykroha.com:81/BABYKROHA/ws/babykrohawww?wsdl", ['login' => 'Sayt', 'password' => 'BabyKroha5456@', 'location' => 'http://sr.babykroha.com:81/BABYKROHA/ws/BabyKrohawww']);
//    die();
    $brands = $client->getBrands()->return->Brands;
    $colors = $client->getColors()->return->Сolors;
    $colorsGroup = $client->getColorGroups()->return->id;
    $ages = $client->getAges()->return->Ages;
    $courses = $client->getCourses(['Id' => 840])->return;
    $groups = $client->getGroups()->return->Groups;

*/

//    $dom = new DOMDocument("1.0", "utf-8");
//    $dom->load('test1.xml');
//    $root = $dom->documentElement;


    $ftpHost = '109.87.24.5';
    $ftpLogin = "babykroha.ua";
    $ftpPasswd = "FTP172839654Site";

    $ftp = ftp_connect($ftpHost, '21', 3600) or die("Couldn't connect to $ftpHost");

    $login = ftp_login($ftp, $ftpLogin, $ftpPasswd);

    if (!$ftp || !$login) {
        die("FTP Connection Failed");
    }

    $passive = ftp_pasv($ftp, true);

    ftp_chdir($ftp, 'exchange');

    $contents = ftp_nlist($ftp, '.');

    foreach ($contents as $file) {

        if ($file == '.' || $file == '..') {
            continue;
        }

        $localFile = 'Media/exchange.xml';

        $download = ftp_get($ftp, $localFile, $file, FTP_BINARY);
    }

    ftp_close($ftp);

    if (!$download) {
        die("Couldn't download file from $ftpHost");
    }

    $document = simplexml_load_file($localFile);

    $brands = $document->brands;
    $colors = $document->colors;
    $colorsGroup = $document->color_groups;
    $ages = $document->ages;
    $courses = $document->courses;
    $groups = $document->groups;
    $products = $document->products;

    //parse Brands +++
    foreach ($brands->brand as $obj) {
//        $obj = json_decode(json_encode($obj));

        $brand = \Modules\Catalog\Models\Brands::getRow($obj->id, 'id');

        if ($brand) {
            $data = [];
            $changed = false;

            if ($brand->name != $obj->name) {
                $data['name'] = $obj->name;
                $data['alias'] = \Modules\Catalog\Models\Brands::getUniqueAlias(\Core\Text::translit(trim($obj->name)));
                $changed = true;
            }

            if ($changed) {
                $res = \Modules\Catalog\Models\Brands::update($data, $obj->id);
                if ($res) {
                    $success = true;
                } else {
                    $success = false;
                }
            }

        } else {

            $data = [
                'id' => $obj->id,
                'name' => $obj->name,
                'alias' => \Modules\Catalog\Models\Brands::getUniqueAlias(\Core\Text::translit(trim($obj->name))),
                'status' => 1,
            ];
            $res = \Modules\Catalog\Models\Brands::insert($data);
            if ($res) {
                $success = true;
            } else {
                $success = false;
            }
        }

    }

    echo "Brands is done </br>";

    //parse Colors +++
    foreach ($colors->color as $obj) {

        $color = \Modules\Catalog\Models\Colors::getRow($obj->id, 'id');

        if ($color) {
            $data = [];
            $changed = false;

            if ($color->name != $obj->name) {
                $data['name'] = $obj->name;
                $data['alias'] = \Modules\Catalog\Models\Colors::getUniqueAlias(\Core\Text::translit(trim($obj->name), 'only_chars_and_numbers'));
                $changed = true;
            }

            if ($changed) {
                $res = \Modules\Catalog\Models\Colors::update($data, $obj->id);
                if ($res) {
                    $success = true;
                } else {
                    $success = false;
                }
            }

        } else {

            $data = [
                'id' => $obj->id,
                'name' => $obj->name,
                'alias' => \Modules\Catalog\Models\Colors::getUniqueAlias(\Core\Text::translit(trim($obj->name), 'only_chars_and_numbers')),
            ];
            $res = \Modules\Catalog\Models\Colors::insert($data);
            if ($res) {
                $success = true;
            } else {
                $success = false;
            }
        }

    }

    echo "Colors is done </br>";

    //parse Colors Group +++
    foreach ($colorsGroup->color_group as $obj) {

        $id = htmlentities($obj->id);
        $id = str_replace('&nbsp;', '', $id);
        if ($id) {
            $colorGroup = Core\QB\DB::select()->from('catalog_color_group')->where('id', '=', $id)->find();

            if (!$colorGroup) {
                $res = \Core\QB\DB::insert('catalog_color_group', ['id'])->values([$id])->execute();
                if ($res) {
                    $success = true;
                } else {
                    $success = false;
                }
            }
        } else {
            $colorGroup = Core\QB\DB::select()->from('catalog_color_group')->where('id', '=', 9999)->find();

            if (!$colorGroup) {
                $res = \Core\QB\DB::insert('catalog_color_group', ['id'])->values([9999])->execute();
                if ($res) {
                    $success = true;
                } else {
                    $success = false;
                }
            }
        }

    }

    echo "Colors group is done </br>";

    //parse Ages +++
    foreach ($ages->age as $obj) {

        $age = \Modules\Catalog\Models\Ages::getRow($obj->id, 'id');

        if ($age) {
            $data = [];
            $changed = false;

            if ($age->name != $obj->name) {
                $data['name'] = $obj->name;
                $data['alias'] = \Modules\Catalog\Models\Ages::getUniqueAlias(\Core\Text::translit(trim($obj->name), 'only_chars_and_numbers'));
                $changed = true;
            }

            if ($changed) {
                $res = \Modules\Catalog\Models\Ages::update($data, $obj->id);
                if ($res) {
                    $success = true;
                } else {
                    $success = false;
                }
            }

        } else {

            $data = [
                'id' => $obj->id,
                'name' => $obj->name,
                'alias' => \Modules\Catalog\Models\Ages::getUniqueAlias(\Core\Text::translit(trim($obj->name), 'only_chars_and_numbers')),
                'status' => 1,
                'menu' => 1,
                'sort' => $obj->id
            ];
            $res = \Modules\Catalog\Models\Ages::insert($data);
            if ($res) {
                $success = true;
            } else {
                $success = false;
            }
        }

    }

    echo "Ages is done </br>";

    //parse Courses +++

    foreach ($courses->course as $obj) {

        $course = Core\QB\DB::select()->from('currencies_courses')->where('id', '=', $obj->id)->find();

        if ($course) {

            $res = \Core\QB\DB::update('currencies_courses')->set(['course' => $obj->Cours]);
            if ($res) {
                $success = true;
            } else {
                $success = false;
            }

        } else {

            if ($obj->id == 643) {
                $text = 'RUB';
                $display = 'руб.';
            } elseif ($obj->id == 840) {
                $text = 'USD';
                $display = '$';
            } elseif ($obj->id == 980) {
                $text ='UAH';
                $display = 'грн.';
            } else {
                continue;
            }

            $res = \Core\QB\DB::insert('currencies_courses', ['id', 'course', 'currency', 'display'])->values([$obj->id, $obj->Cours, $text, $display])->execute();
            if ($res) {
                $success = true;
            } else {
                $success = false;
            }
        }

    }


    echo "Course is done </br>";

    //parse Groups +++
    // set empty products array +++

    foreach ($groups->group as $obj) {

        if ((int)$obj->parent_id == null) {
            $fakeId = $obj->id;
            continue;
        }


        $group = \Modules\Catalog\Models\Groups::getRow(trim($obj->id), 'artikul');
        $parent = \Modules\Catalog\Models\Groups::getRow(trim($obj->parent_id), 'artikul');
        if ($group) {
            $data = [];
            $changed = false;

            if ($group->parent_id != $parent->id) {
                if (!$parent->id) {
                    $data['parent_id'] = '0';
                } else {
                    $data['parent_id'] = $parent->id;
                }
                $changed = true;
            }

            if ($group->name != $obj->name) {
                $data['name'] = $obj->name;
                $data['alias'] = \Modules\Catalog\Models\Groups::getUniqueAlias(\Core\Text::translit(trim($obj->name)));
                $changed = true;
            }

            if ($changed) {
                $res = \Modules\Catalog\Models\Groups::update($data, $group->id);
                if ($res) {
                    $success = true;
                } else {
                    $success = false;
                }
            }

        } else {

            if ($parent->id) {
                $parentId = $parent->id;
            } else {
                $parentId = 0;
            }

            $data = [
                'artikul' => trim($obj->id),
                'status' => 1,
                'name' => $obj->name,
                'alias' => \Modules\Catalog\Models\Groups::getUniqueAlias(\Core\Text::translit(trim($obj->name))),
                'parent_id' => $parentId,
                'sort' => ($parentId != 0) ? (int)$obj->id : 0,
            ];
            $res = \Modules\Catalog\Models\Groups::insert($data);
            if ($res) {
                $success = true;
            } else {
                $success = false;
            }
        }

    }

    echo "Groups is done </br>";

    //parse Products
    foreach ($products->product as $obj) {
        $product = \Modules\Catalog\Models\Items::getRow((int)$obj->id, 'id');
        $parent = \Modules\Catalog\Models\Groups::getRow(trim($obj->parent_id), 'artikul');
        if (!$parent->id) {
            continue;
        }

        if (count((array)$obj->additional)) {
            $specifications = $obj->additional;
        } else {
            $specifications = null;
        }
        if ($product) {
            $data = [];
            $changed = false;
            if ($product->status != $obj->status) {
                $data['status'] = $obj->status;
                $changed = true;

            }
            if ($product->name != $obj->name) {
                $data['name'] = $obj->name;
                $data['alias'] = \Modules\Catalog\Models\Items::getUniqueAlias(\Core\Text::translit(trim($obj->name)));
                $changed = true;
            }
            if ($product->parent_id != $parent->id) {
                $data['parent_id'] = $parent->id;
                $changed = true;
            }
            if ($product->new != $obj->new) {
                $data['new'] = $obj->new;
                $changed = true;
            }
            if ($product->sale != $obj->sale) {
                $data['sale'] = $obj->sale;
                $changed = true;
            }
            if ($product->top != $obj->top) {
                $data['top'] = $obj->top;
                $changed = true;
            }

            if (property_exists($obj, 'Opis')) {
                if ($product->content != $obj->Opis) {
                    $data['content'] = $obj->Opis;
                }
            }

/* start Set up prices and currency */
            if ((double)$product->cost != (double)$obj->cost) {
                $data['cost'] = (double)$obj->cost ? (double)$obj->cost : 0;
                $changed = true;
            }
            if ((double)$product->cost_old != (double)$obj->cost_old) {
                $data['cost_old'] = (double)$obj->cost_old ? (double)$obj->cost_old : 0;
                $changed = true;
            }
            if ($product->cost_currency != (int)$obj->cost_val) {
                $data['cost_currency'] = (int)$obj->cost_val;
                $changed = true;
            }

            if ((double)$product->cost_opt != (double)$obj->cost_opt) {
                $data['cost_opt'] = (double)$obj->cost_opt ? (double)$obj->cost_opt : 0;
                $changed = true;
            }
            if ((double)$product->cost_opt_old != (double)$obj->cost_opt_old) {
                $data['cost_opt_old'] = (double)$obj->cost_opt_old ? (double)$obj->cost_opt_old : 0;
                $changed = true;
            }
            if ($product->cost_opt_currency != (int)$obj->cost_opt_val) {
                $data['cost_opt_currency'] = (int)$obj->cost_opt_val ? (int)$obj->cost_opt_val : '980';
                $changed = true;
            }

            if ((double)$product->cost_drop != (double)$obj->cost_dsh) {
                $data['cost_drop'] = (double)$obj->cost_dsh ? (double)$obj->cost_dsh : 0;
                $changed = true;
            }
            if ((double)$product->cost_drop_old != (double)$obj->cost_dsh_old) {
                $data['cost_drop_old'] = (double)$obj->cost_dsh_old ? (double)$obj->cost_dsh_old : 0;
                $changed = true;
            }
            if ($product->cost_drop_currency != (int)$obj->cost_dsh_val) {
                $data['cost_drop_currency'] = (int)$obj->cost_dsh_val ? (int)$obj->cost_dsh_val : '980';
                $changed = true;
            }
/* stop Set up prices and currency */

            if ($product->artikul != $obj->artikul) {
                $data['artikul'] = $obj->artikul;
                $changed = true;
            }
            if ($product->catalog_color_group_id != $obj->color_group_id) {
                $colorGroupId = htmlentities($obj->color_group_id);
                $colorGroupId = str_replace('&nbsp;', '', $colorGroupId);
                $data['catalog_color_group_id'] = $colorGroupId ? $colorGroupId : '9999';
                $changed = true;
            }
            if ($specifications) {
                $specifications = (array)$specifications;
                $arraySpec = [];

                foreach ($specifications as $key => $value) {
                    if (!$key || !$value) {
                        continue;
                    }
                    $specAlias = mb_strtolower($key);
                    $specAlias = str_replace(' ', '', $specAlias);
                    $specification = \Core\QB\DB::select()->from('specifications')->where('alias', '=', $specAlias)->find();
                    if (!$specification) {
                        $specId = \Core\QB\DB::insert('specifications', ['status', 'name', 'alias', 'type_id'])->values([1, trim($key), $specAlias, 2])->execute();
                        $specId = $specId[0];
                    } else {
                        $specId = $specification->id;
                    }
                    $specValue = \Core\QB\DB::select()->from('specifications_values')->where('name', '=', trim($value))->where('specification_id', '=', $specId)->find();
                    if (!$specValue) {
                        $specValAlias = \Modules\Catalog\Models\Items::getUniqueAlias(\Core\Text::translit(trim($value)));
                        $specValId = \Core\QB\DB::insert('specifications_values', ['status', 'name', 'alias', 'specification_id'])->values([1, trim($value), $specValAlias, $specId])->execute();
                    } else {
                        $specValId = $specValue->id;
                        $specValAlias = $specValue->alias;
                    }
                    $arraySpec = array_merge($arraySpec, [$specAlias => $specValAlias]);

                    $catalogSpecificationValues = \Core\QB\DB::select()->from('catalog_specifications_values')
                        ->where('catalog_id', '=', $product->id)
                        ->where('specification_value_alias', '=', $specValAlias)
                        ->where('specification_alias', '=', $specAlias)
                        ->find();

                    $catalogTreeSpecification = \Core\QB\DB::select()->from('catalog_tree_specifications')
                        ->where('catalog_tree_id', '=', $product->parent_id)
                        ->where('specification_id', '=', $specId)
                        ->find();

                    if (!$catalogSpecificationValues->id) {
                        $res = \Core\QB\DB::insert('catalog_specifications_values', ['catalog_id', 'specification_value_alias', 'specification_alias'])->values([$product->id, $specValAlias, $specAlias])->execute();
                    }

                    if (!$catalogTreeSpecification) {
                        $res = \Core\QB\DB::insert('catalog_tree_specifications', ['catalog_tree_id', 'specification_id'])->values([$product->parent_id, $specId])->execute();
                    }
                }
                $data['specifications'] = json_encode($arraySpec);
            }

            $age = \Modules\Catalog\Models\Items::getAgeByAlias($product->age_alias);
            $color = \Modules\Catalog\Models\Items::getColorByAlias($product->color_alias);
            $brand = \Modules\Catalog\Models\Items::getBrandByAlias($product->brand_alias);

            if ($brand->id != $obj->brand_id) {
                $brand = \Modules\Catalog\Models\Brands::getRow($obj->brand_id, 'id');
                $data['brand_alias'] = $brand->alias;
                $changed = true;
            }
            if ($age->id != $obj->age_id) {
                $age = \Modules\Catalog\Models\Ages::getRow((int)$obj->age_id, 'id');
                $data['age_alias'] = $age->alias;
                $changed = true;
            }
            if ($color->id != $obj->color_id) {
                $color = \Modules\Catalog\Models\Colors::getRow((int)$obj->color_id, 'id');
                $data['color_alias'] = $color->alias;
                $changed = true;
            }
            if ($changed) {
                $res = \Modules\Catalog\Models\Items::update($data, $obj->id);
                if ($res) {
                    $success = true;
                } else {
                    $success = false;
                }
            }
        } else {
            $age = \Modules\Catalog\Models\Ages::getRow((int)$obj->age_id, 'id');
            $color = \Modules\Catalog\Models\Colors::getRow((int)$obj->color_id, 'id');
            $brand = \Modules\Catalog\Models\Brands::getRow((int)$obj->brand_id, 'id');
            $colorGroupId = htmlentities($obj->color_group_id);
            $colorGroupId = str_replace('&nbsp;', '', $colorGroupId);
            if ($colorGroupId) {
                $colorGroup = Core\QB\DB::select()->from('catalog_color_group')->where('id', '=', $colorGroupId)->find();
            } else {
                $colorGroup = Core\QB\DB::select()->from('catalog_color_group')->where('id', '=', '9999')->find();
            }

            $alias = \Modules\Catalog\Models\Items::getUniqueAlias(\Core\Text::translit(trim($obj->name)));
            $alias = $alias . '_' . $obj->id;
            $alias = trim($alias);

            $data = [
                'id' => (int)$obj->id,
                'status' => $obj->status,
                'name' => $obj->name,
                'alias' => $alias,
                'parent_id' => $parent->id,
                'new' => $obj->new,
                'sale' => $obj->sale,
                'top' => $obj->top,
                'content' => $obj->Opis,
                'cost' => (double)$obj->cost ? (double)$obj->cost : 0,
                'cost_old' => (double)$obj->cost_old ? (double)$obj->cost_old : 0,
                'cost_currency' => (int)$obj->cost_val ? (int)$obj->cost_val : '980',
                'cost_opt' => (double)$obj->cost_opt ? (double)$obj->cost_opt : 0,
                'cost_opt_old' => (double)$obj->cost_opt_old ? (double)$obj->cost_opt_old : 0,
                'cost_opt_currency' => (int)$obj->cost_opt_val ? (int)$obj->cost_opt_val : '980',
                'cost_drop' => (double)$obj->cost_dsh ? (double)$obj->cost_dsh : 0,
                'cost_drop_old' => (double)$obj->cost_dsh_old ? (double)$obj->cost_dsh_old : 0,
                'cost_drop_currency' => (int)$obj->cost_dsh_val ? (int)$obj->cost_dsh_val : '980',
                'artikul' => $obj->artikul,
                'brand_alias' => $brand->alias,
                'age_alias' => ($age->id) ? $age->alias : null,
                'color_alias' => $color->alias,
                'catalog_color_group_id' => $colorGroup->id ? $colorGroup->id : null
            ];
            $res = \Modules\Catalog\Models\Items::insert($data);
            if ($res) {
                $success = true;
            } else {
                $success = false;
            }

            if ($specifications) {
                $specifications = (array)$specifications;
                $arraySpec = [];

                foreach ($specifications as $key => $value) {
                    if (!$key || !$value) {
                        continue;
                    }
                    $specAlias = mb_strtolower($key);
                    $specAlias = str_replace(' ', '', $specAlias);
                    $specification = \Core\QB\DB::select()->from('specifications')->where('alias', '=', $specAlias)->find();
                    if (!$specification) {
                        $specId = \Core\QB\DB::insert('specifications', ['status', 'name', 'alias', 'type_id'])->values([1, trim($key), $specAlias, 2])->execute();
                        $specId = $specId[0];
                    } else {
                        $specId = $specification->id;
                    }
                    $specValue = \Core\QB\DB::select()->from('specifications_values')->where('name', '=', trim($value))->where('specification_id', '=', $specId)->find();
                    if (!$specValue) {
                        $specValAlias = \Modules\Catalog\Models\Items::getUniqueAlias(\Core\Text::translit(trim($value)));
                        $specValId = \Core\QB\DB::insert('specifications_values', ['status', 'name', 'alias', 'specification_id'])->values([1, trim($value), $specValAlias, $specId])->execute();
                    } else {
                        $specValId = $specValue->id;
                        $specValAlias = $specValue->alias;
                    }
                    $arraySpec = array_merge($arraySpec, [$specAlias => $specValAlias]);

                    $catalogSpecificationValues = \Core\QB\DB::select()->from('catalog_specifications_values')
                        ->where('catalog_id', '=', (int)$obj->id)
                        ->where('specification_value_alias', '=', $specValAlias)
                        ->where('specification_alias', '=', $specAlias)
                        ->find();

                    $catalogTreeSpecification = \Core\QB\DB::select()->from('catalog_tree_specifications')
                        ->where('catalog_tree_id', '=', $parent->id)
                        ->where('specification_id', '=', $specId)
                        ->find();

                    if (!$catalogSpecificationValues->id) {
                        $res = \Core\QB\DB::insert('catalog_specifications_values', ['catalog_id', 'specification_value_alias', 'specification_alias'])->values([(int)$obj->id, $specValAlias, $specAlias])->execute();
                    }

                    if (!$catalogTreeSpecification) {
                        $res = \Core\QB\DB::insert('catalog_tree_specifications', ['catalog_tree_id', 'specification_id'])->values([$parent->id, $specId])->execute();
                    }
                }
                $data['specifications'] = json_encode($arraySpec);
            }
        }

        $sizes = [];

        foreach ($obj->sizes->size as $value) {
            $sizes[(int)$value['size_']] = (int)$value;
        }

        foreach ($sizes as $key => $value) {

            $size_db = \Core\QB\DB::select()
                ->from('catalog_size_amount')
                ->where('catalog_id', '=', $obj->id)
                ->where('size', '=', $key)
                ->find();

            if ($size_db) {
                if ($size_db->amount != $value) {
                    $res = \Modules\Catalog\Models\Sizes::update(['amount' => $value], $size_db->id);
                    if ($res) {
                        $success = true;
                    } else {
                        $success = false;
                    }
                }
            } else {
                if ($key != "<Свойства не назначены>") {
                    $data = [
                        'catalog_id' => (int)$obj->id,
                        'size' => $key,
                        'amount' => $value,
                    ];
                    $res = \Modules\Catalog\Models\Sizes::insert($data);
                    if ($res) {
                        $success = true;
                    } else {
                        $success = false;
                    }
                }
            }
        }

    }

    echo "Products is done </br>";

    echo $success;

