#Разворачивание на локалке
```bash
$ cp .env.example .env
$ cp phinx.example.php phinx.php
$ cp Config/database.example.php Config/database.php
$ sudo chmod -R 777 Cache

$ docker-compose up -d --build
$ docker-compose exec app bash
$ composer install
$ vendor/bin/phinx migrate
$ npm i
```