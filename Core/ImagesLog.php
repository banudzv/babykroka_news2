<?php


namespace Core;


use PHPExcel_Exception;
use PHPExcel_Style_Alignment;
use PHPExcel_Writer_Excel2007;

/**
 * Class @ImagesLog
 * @package Core
 */
class ImagesLog
{
    public static $table = 'images_log';

    /**
     * @param string $folder
     * @return false|int
     */
    public static function createLog(string $folder)
    {
        return Common::factory(self::$table)->insert([
            'created_at' => time(),
            'folder' => $folder,
            'start_time' => time(),
        ]);
    }

    /**
     * @param integer $logID
     * @return bool
     */
    public static function updateLog($logID): bool
    {
         Common::factory(self::$table)->update([
            'finish_time' => time(),
        ],$logID);
        return true;
    }

    /**
     * Проверяем существует ли запись в базе
     * @param string $folder
     * @return bool
     */
    public static function logExistence(string $folder): bool
    {
        $row = Common::factory(self::$table)->getRow($folder, 'folder');
        if(!$row){
            return false;
        }
        return true;
    }

    /**
     * @throws PHPExcel_Exception
     */
    public static function generateLog(): void
    {
        $images_logs = Common::factory(self::$table)->getRows()->as_array();
        $totalElements = Common::factory(self::$table)->getRows()->count();
        $excel = new \PHPExcel();
        $excel->setActiveSheetIndex(0);
        $sheet = $excel->getActiveSheet();
        $sheet->setTitle('Выгрузка изображений');
        for($i = 0; $i <= $totalElements; $i++){
            for ($j = 2; $j < $totalElements+2; $j++){
                $sheet->setCellValueByColumnAndRow($i,$j-2,$images_logs[$i]->folder);
                $sheet->getStyleByColumnAndRow($i, $j-2)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            }
        }
        header("Expires: Mon, 1 Apr 1974 05:00:00 GMT");
        header("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
        header("Cache-Control: no-cache, must-revalidate");
        header("Pragma: no-cache");
        header("Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header("Content-Disposition: attachment; filename=file.xlsx");

        $objWriter = new PHPExcel_Writer_Excel2007($excel);
        $objWriter->save('php://output');
        exit();

    }

}
