<?php
namespace Core;

use Core\QB\DB;
use Core\QB\Database;
use Core\Validation\Valid;

class Common
{

    public static $table;
    public static $image;
    public static $filters = []; // Filter that gets parameters from global array $_GET. This is array with trusted filter keys
    public static $rules = []; // Fields and their rules for validation

    public static function factory($table, $image = null)
    {
        return new Common($table, $image);
    }

    public function __construct($table = null, $image = null)
    {
        if ($table !== null) {
            static::$table = $table;
        }
        if ($image !== null) {
            static::$image = $image;
        }
    }


    public static function table()
    {
        return static::$table;
    }


    public static function image()
    {
        return static::$image;
    }


    /**
     * @param null $ip - current ip
     * @param null $email - email in POST
     * @param null $phone - phone in POST
     * @param null $date - 0 or 1 (60 seconds timeout)
     * @return int
     */
    public static function isBot($ip = null, $email = null, $phone = null, $date = null)
    {
        if ($ip === null && $email === null && $phone === null && $date === null) {
            return false;
        }
        $result = DB::select([DB::expr('COUNT(id)'), 'count'])->from(static::$table);
        $result->where_open();
        if ($ip !== null) {
            $result->or_where('ip', '=', $ip);
        }
        if ($email !== null) {
            $result->or_where('email', '=', $email);
        }
        if ($phone !== null) {
            $result->or_where('phone', '=', $phone);
        }
        $result->where_close();
        if ($date !== null) {
            $result->where('created_at', '>', time() - 60);
        }
        return $result->count_all();
    }


    /**
     * @param array $data - associative array with insert data
     * @return integer - inserted row ID
     */
    public static function insert($data)
    {
        if (!isset($data['created_at']) && self::checkField(static::$table, 'created_at')) {
            $data['created_at'] = time();
        }
        $keys = $values = [];
        foreach ($data as $key => $value) {
            $keys[] = $key;
            $values[] = $value;
        }
        $result = DB::insert(static::$table, $keys)->values($values)->execute();
        if (!$result) {
            return false;
        }
        return $result[0];
    }


    /**
     * @param array $data - associative array with data to update
     * @param string /integer $value - value for where clause
     * @param string $field - field for where clause
     * @return bool|QB\Database_Result_Cached|object
     */
    public static function update($data, $value, $field = 'id')
    {
        if (!isset($data['updated_at']) && self::checkField(static::$table, 'updated_at')) {
            $data['updated_at'] = time();
        }
        return DB::update(static::$table)->set($data)->where($field, '=', $value)->execute();
    }


    /**
     * @param mixed $value - value
     * @param string $field - field
     * @return object
     */
    public static function delete($value, $field = 'id')
    {
        return DB::delete(static::$table)->where($field, '=', $value)->execute();
    }


    /**
     * @param string $table - table where we check the field
     * @param string $field - check this field
     * @return bool
     */
    public static function checkField($table, $field): bool
    {
        $cResult = DB::query(Database::SELECT, 'SHOW FIELDS FROM `' . $table . '`')->execute();
        $found = false;
        foreach ($cResult as $arr) {
            if ($arr['Field'] == $field) {
                $found = true;
            }
        }
        return $found;
    }


    /**
     * @param string $value - checked alias
     * @param null|integer $id - ID if need off current row with ID = $id
     * @return string - unique alias
     * @throws \Exception
     */
    public static function getUniqueAlias($value, $id = null): string
    {
        $value = Text::translit($value);
        $count = DB::select([DB::expr('COUNT(id)'), 'count'])
            ->from(static::$table)
            ->where('alias', '=', $value);
        if ($id !== null) {
            $count->where('id', '!=', $id);
        }
        $count = $count->count_all();
        if ($count) {
            return $value . random_int(1000, 9999);
        }
        return $value;
    }


    /**
     * @param mixed $value - value
     * @param string $field - field
     * @param null /integer $status - 0 or 1
     * @return object
     */
    public static function getRow($value, $field = 'id', $status = null)
    {
        $result = DB::select()->from(static::$table)->where($field, '=', $value);
        if ($status !== null) {
            $result->where('status', '=', $status);
        }
        return $result->find();
    }


    /**
     * @param null /integer $status - 0 or 1
     * @param null /string $sort
     * @param null /string $type - ASC or DESC. No $sort - no $type
     * @param null /integer $limit
     * @param null /integer $offset - no $limit - no $offset
     * @param bool $filter
     * @return object
     */
    public static function getRows($status = null, $sort = null, $type = null, $limit = null, $offset = null, $filter = true)
    {
        $result = DB::select()->from(static::$table);
        if ($status !== null) {
            $result->where('status', '=', $status);
        }
        if ($filter) {
            $result = static::setFilter($result);
        }
        if ($sort !== null) {
            if ($type !== null) {
                $result->order_by($sort, $type);
            } else {
                $result->order_by($sort);
            }
        }
        $result->order_by('id', 'DESC');
        if ($limit !== null) {
            $result->limit($limit);
            if ($offset !== null) {
                $result->offset($offset);
            }
        }
        return $result->find_all();
    }

    /**
     * @param array $fields = ['field' => 'value']
     * @param null|string $sort
     * @param null|string $type - ASC or DESC. No $sort - no $type
     * @param null|integer $limit
     * @param bool $filter
     * @param null|integer $offset - no $limit - no $offset
     * @return object
     */
    public static function getCustomRows($fields = [], $sort = null, $type = null, $limit = null, $offset = null, $filter = true)
    {
        $result = DB::select()->from(static::$table);

        if(!empty($fields)){
            foreach ($fields as $field => $value){
                $result->where($field, '=', $value);
            }
        }

        if ($filter) {
            $result = static::setFilter($result);
        }
        if ($sort !== null) {
            if ($type !== null) {
                $result->order_by($sort, $type);
            } else {
                $result->order_by($sort);
            }
        }
        $result->order_by('id', 'DESC');
        if ($limit !== null) {
            $result->limit($limit);
            if ($offset !== null) {
                $result->offset($offset);
            }
        }
        return $result->find_all();
    }


    public static function setFilter($result)
    {
        if (!is_array(static::$filters)) {
            return $result;
        }
        foreach (static::$filters as $key => $value) {
            if (isset($key) && isset($_GET[$key]) && trim($_GET[$key])) {
                $get = strip_tags($_GET[$key]);
                $get = trim($get);
                if (!Arr::get($value, 'action', null)) {
                    $action = '=';
                } else {
                    $action = Arr::get($value, 'action');
                }
                $table = false;
                if (Arr::get($value, 'table', null)) {
                    $table = Arr::get($value, 'table');
                } else if (Arr::get($value, 'table', null) === null) {
                    $table = static::$table;
                }
                if ($action == 'LIKE') {
                    $get = '%' . $get . '%';
                }
                if (Arr::get($value, 'field')) {
                    $key = Arr::get($value, 'field');
                }
                if ($table == 'catalog' && $key == 'parent_id'){
                    $arr = [$get];
                    $catalogs = DB::select()->from('catalog_tree')
                        ->join('catalog_tree_i18n')->on('catalog_tree.id', '=', 'catalog_tree_i18n.row_id')
                        ->where('catalog_tree_i18n.language', '=', \I18n::lang())
                        ->where('parent_id', '=', $get)->find_all();
                    foreach ($catalogs as $cat) {
                        $check = DB::select()->from('catalog_tree')
                            ->join('catalog_tree_i18n')->on('catalog_tree.id', '=', 'catalog_tree_i18n.row_id')
                            ->where('catalog_tree_i18n.language', '=', \I18n::lang())
                            ->where('parent_id', '=', $cat->id)->find_all();
                        if (count($check)){
                            foreach ($check as $c) {
                                $arr[] = $c->id;
                            }
                        } else{
                            $arr[] = $cat->id;
                        }
                    }
                    $result->where('catalog.parent_id', 'IN', $arr);
                } else {
                    if ($table !== false) {
                        $result->where($table . '.' . $key, $action, $get);
                    } else {
                        $result->where(DB::expr($key), $action, $get);
                    }
                }
            }
        }
        return $result;
    }


    /**
     * @param null /integer $status - 0 or 1
     * @return int
     */
    public static function countRows($status = null, $filter = true)
    {
        $result = DB::select([DB::expr('COUNT(' . static::$table . '.id)'), 'count'])->from(static::$table);
        if ($status !== null) {
            $result->where(static::$table . '.status', '=', $status);
        }
        if ($filter) {
            $result = static::setFilter($result);
        }
        return $result->count_all();
    }


    /**
     * Upload images for current type
     * @param integer $id - ID in the table for this image
     * @param string $name - name of the input in form for uploaded image
     * @param string $field - field name in the table to save new image name
     * @return bool|object
     */
    public static function uploadImage($id, $name = 'file', $field = 'image')
    {
        if (!static::$image OR !$id) {
            return false;
        }
        $filename = Files::uploadImage(static::$image, $name);
        if (!$filename) {
            return false;
        }
        if (!Common::checkField(static::$table, $field)) {
            return true;
        }
        return DB::update(static::$table)->set([$field => $filename])->where(static::$table . '.id', '=', $id)->execute();
    }


    /**
     * Delete images for current type
     * @param string $filename - file name
     * @param null|integer $id - ID in the table for this image
     * @param string $field - field name in the table to save new image name
     * @return bool
     */
    public static function deleteImage($filename, $id = null, $field = 'image')
    {
        if (!static::$image OR !$filename) {
            return false;
        }
        $result = Files::deleteImage(static::$image, $filename);
        if (!$result) {
            return false;
        }
        if (!Common::checkField(static::$table, $field)) {
            return true;
        }
        if ($id !== null) {
            return DB::update(static::$table)->set([$field => null])->where(static::$table . '.id', '=', $id)->execute();
        }
        return true;
    }


    /**
     *  Adding +1 in field `views`
     * @param object $row - object
     * @return object
     */
    public static function addView($row)
    {
        $row->views = $row->views + 1;
        static::update(['views' => $row->views], $row->id);
        return $row;
    }


    /**
     * @param array $data
     * @return bool
     */
    public static function valid($data = [])
    {
        if (!static::$rules) {
            return true;
        }
        $valid = new Valid($data, static::$rules);
        $errors = $valid->execute();
        if (!$errors) {
            return true;
        }
        $message = Valid::message($errors);
        Message::GetMessage(0, $message, false);
        return false;
    }

	/**
	 * @param $id
	 * @param $name
	 * @param $mainFolder
	 * @return bool
	 */
	public static function upload3D($id, $name, $mainFolder) {
			$id = (int) $id;
		if( !isset($_FILES[$name]) || !Arr::get( $_FILES[$name], 'name' ) ) {
			return false;
		}
		$dir = HOST.'/Media/images/'.$mainFolder.'/'.$id;
		if(!is_dir( $dir )) {
			Files::createFolder($dir, 0777);
		}
		ini_set('error_reporting', E_ALL);
		ini_set('display_errors', 1);
		ini_set('display_startup_errors', 1);
		move_uploaded_file($_FILES[$name]['tmp_name'], $dir.'/zip.zip');
		$zip = new \ZipArchive();
		$res = $zip->open($dir.'/zip.zip', false);
		if ( $res === true) {
			$zip->extractTo($dir);
			$zip->close();
		} else {
			return false;
		}
		unlink($dir.'/zip.zip');
		$files = scandir($dir);
		foreach($files as $file) {
			if($file <> '.' AND $file <> '..') {
				if( is_dir($dir.'/'.$file) ) {
					$filesInner = scandir($dir.'/'.$file);
					foreach($filesInner as $key => $fileInner) {
						if($fileInner <> '.' AND $filesInner <> '..') {
							$ext = end(explode('.', $fileInner));
							if ($ext) {
								rename($dir.'/'.$file.'/'.$fileInner, $dir.'/'.($key-1).'.'.$ext);
							} else {
								rename($dir.'/'.$file.'/'.$fileInner, $dir.'/'.$fileInner);
							}
						}
					}
					rmdir($dir.'/'.$file);
				}
			}
		}
	}

	/**
	 * @param $id
	 * @param $mainFolder
	 * @return bool
	 */
	public static function remove3D($id, $mainFolder){
		$dir = HOST.'/Media/images/'.$mainFolder.'/'.$id;
		if(!is_dir($dir)) {
			return false;
		}
		$files = scandir($dir);
		foreach($files as $file) {
			if($file <> '.' AND $file <> '..') {
				unlink($dir . '/' . $file);
			}
		}
		rmdir($dir);
	}
}
