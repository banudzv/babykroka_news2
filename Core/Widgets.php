<?php

namespace Core;

use Modules\Catalog\Models\Brands;
use Modules\Catalog\Models\Colors;
use Modules\Catalog\Models\Favorite;
use Modules\Catalog\Models\Groups;
use Modules\Catalog\Models\Information;
use Modules\Catalog\Models\Items;
use Modules\Catalog\Models\Manufacturers;
use Modules\News\Models\News;
use Core\QB\DB;
use Modules\Cart\Models\Cart;
use Modules\Catalog\Models\Filter;
use Modules\User\Models\CustomerRoles;
use Wezom\Modules\Catalog\Models\Currency;
use Wezom\Modules\Catalog\Models\SpecificationsValues;
use Wezom\Modules\Menu\Models\Menu;
use Wezom\Modules\Subscribe\Models\Subscribe;
use Wezom\Modules\Catalog\Models\Ages;

/**
 *  Class that helps with widgets on the site
 */
class Widgets
{

    static $_instance; // Constant that consists self class

    public $_data = []; // Array of called widgets
    public $_tree = []; // Only for catalog menus on footer and header. Minus one query
    public $_newProducts = [];
    public $_saleProducts = [];
    public $_viewedProducts = [];
    public $userFavorites = [];

    // Instance method
    static function factory()
    {
        if (self::$_instance == NULL) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    /**
     *  Get widget
     * @param  string $name [Name of template file]
     * @param  array $array [Array with data -> go to template]
     * @return string        [Widget HTML]
     */
    public static function get($name, $array = [], $save = true, $cache = false)
    {
        $arr = explode('_', $name);
        $viewpath = implode('/', $arr);

        if (APPLICATION == 'backend' && !Config::get('error')) {
            $w = WidgetsBackend::factory();
        } else {
            $w = Widgets::factory();
        }

        $_cache = Cache::instance();
        if ($cache) {
            if (!$_cache->get($name)) {
                $data = NULL;
                if ($save && isset($w->_data[$name])) {
                    $data = $w->_data[$name];
                } else {
                    if ($save && isset($w->_data[$name])) {
                        $data = $w->_data[$name];
                    } else if (method_exists($w, $name)) {
                        $result = $w->$name($array);
                        if ($result !== null && $result !== false) {
                            $array = array_merge($array, $result);
                            $data = View::widget($array, $viewpath);
                        } else {
                            $data = null;
                        }
                    } else {
                        $data = $w->common($viewpath, $array);
                    }
                }
                $_cache->set($name, HTML::compress($data, true));
                return $w->_data[$name] = $data;
            } else {
                return $_cache->get($name);
            }
        }
        if ($_cache->get($name)) {
            $_cache->delete($name);
        }
        if ($save && isset($w->_data[$name])) {
            return $w->_data[$name];
        }
        if (method_exists($w, $name)) {
            $result = $w->$name($array);
            if ($result !== null && $result !== false) {
                if (is_array($result)) {
                    $array = array_merge($array, $result);
                }
                return $w->_data[$name] = View::widget($array, $viewpath);
            } else {
                return $w->_data[$name] = null;
            }
        }
        return $w->_data[$name] = $w->common($viewpath, $array);
    }

    /**
     *  Common widget method. Uses when we have no widgets called $name
     * @param  string $viewpath [Name of template file]
     * @param  array $array [Array with data -> go to template]
     * @return string            [Widget HTML or NULL if template doesn't exist]
     */
    public function common($viewpath, $array)
    {
        if (file_exists(HOST . '/Views/Widgets/' . $viewpath . '.php')) {
            return View::widget($array, $viewpath);
        }
        return null;
    }

    public function HiddenData()
    {
        $cart = Cart::factory()->get_list_for_basket();
        return ['cart' => $cart];
    }

    public function Item_Comments()
    {
        $id = Route::param('id');
        if (!$id) {
            return $this->_data['comments'] = '';
        }
        $result = DB::select()->from('catalog_comments')->where('status', '=', 1)->where('catalog_id', '=', $id)->order_by('date', 'DESC')->find_all();
        return ['result' => $result];
    }

    public function Item_Recommended() {
        $result = DB::select('catalog.*',
            'catalog_i18n.*',
            'product_prices.price',
            'product_prices.price_old',
            'product_prices.currency_id')
            ->from('catalog')
            ->join('catalog_i18n', 'inner')
            ->on('catalog.id', '=', 'catalog_i18n.row_id')
            ->join('product_prices')
            ->on('catalog.id','=','product_prices.product_id')
            ->where('catalog_i18n.language', '=', \I18n::lang())
            ->where('product_prices.price_type', '=', $_SESSION['prices'])
            ->where('status', '=', 1)
            ->where('top', '=', 1)
            ->order_by('catalog.id', 'DESC')
            ->find_all();

        foreach ($result as $item) {
            $cat = DB::select()
                ->from('catalog_tree')
                ->join('catalog_tree_i18n')
                ->on('catalog_tree.id', '=', 'catalog_tree_i18n.row_id')
                ->where('catalog_tree_i18n.language', '=', \I18n::lang())
                ->where('catalog_tree.id', '=', $item->parent_id)
                ->find();
            $item->cat_name = $cat->name;
            $item->cat_alias = $cat->alias;
            $arr[] = $item;
        }
        $fav = [];
        if (\Core\User::info()) {
            $favs = Favorite::getFavorites(\Core\User::info()->id);
            foreach ($favs as $f) {
                $fav[] = (int) $f->id;
            }
        }
        return ['result' => $arr, 'favorite' => $fav];
    }

    public function Item_Related()
    {
        $alias = Route::param('alias');
        $item = Items::getRowSimple($alias, 'alias');
        $productId = $item->id;

        $result = DB::select('catalog.id',
            'catalog.alias',
            'catalog.image',
            'catalog_i18n.name',
            'product_prices.price',
            'product_prices.price_old',
            'product_prices.currency_id')
            ->from('catalog')
            ->join('catalog_i18n', 'inner')
            ->on('catalog.id', '=', 'catalog_i18n.row_id')
            ->join('related_products', 'inner')
            ->on('catalog.id', '=', 'related_products.related_id')
            ->join('product_prices')
            ->on('catalog.id','=','product_prices.product_id')
            ->where('related_products.product_id', '=', $productId)
            ->where('catalog_i18n.language', '=', \I18n::lang())
            ->where('product_prices.price_type', '=', $_SESSION['prices'])
            ->where('status', '=', 1)
            ->order_by('catalog.id', 'DESC')
            ->find_all()
            ->as_array();

        $fav = [];
        if (\Core\User::info()) {
            $favs = Favorite::getFavorites(\Core\User::info()->id);
            foreach ($favs as $f) {
                $fav[] = (int) $f->id;
            }
        }
        return ['result' => $result, 'favorite' => $fav];
    }

    public function CatalogFilter()
    {
        $array = Filter::getClickableFilterElements();
        $brands = Filter::getBrandsWidget();
        $colors = Filter::getColorsWidget();
        $specifications = Filter::getSpecificationsWidget();
        return [
            'brands' => $brands,
            'colors' => $colors,
            'specifications' => $specifications,
            'filter' => $array['filter'],
            'min' => $array['min'],
            'max' => $array['max'],
            'filter_list' => Widgets::get('Catalog_FilterList'),
        ];
    }

    public function Item_InfoItemPage()
    {
        $pages = [5, 6, 7, 8];
        $result = DB::select()
            ->from('content')
            ->where('status', '=', 1)
            ->where('id', 'IN', $pages)
            ->order_by('sort')
            ->find_all();
        return ['result' => $result];
    }

    public function ItemsViewed()
    {
        $ids = Items::getViewedIDs();
        if (!$ids) {
            return $this->_data['itemsViewed'] = '';
        }
        $result = DB::select('catalog.*')
            ->from('catalog')
            ->where('catalog.id', 'IN', $ids)
            ->where('catalog.status', '=', 1)
            ->limit(5)
            ->find_all()->as_array();
        if (!count($result)) {
            return false;
        }
        return ['result' => $result];
    }

    public function Item_ItemsSame()
    {
        $alias = Route::param('alias');
        $item = Items::getRowSimple($alias, 'alias');

        $cat = DB::select()
            ->from('catalog_tree')
            ->join('catalog_tree_i18n')
            ->on('catalog_tree.id', '=', 'catalog_tree_i18n.row_id')
            ->where('catalog_tree_i18n.language', '=', \I18n::lang())
            ->where('catalog_tree.id', '=', (int)Route::param('group'))
            ->find();

        $result = DB::select('catalog.*',
            'catalog_i18n.*',
            'product_prices.price',
            'product_prices.price_old',
            'product_prices.currency_id')
            ->from('catalog')
            ->join('catalog_i18n', 'inner')
            ->on('catalog.id', '=', 'catalog_i18n.row_id')
            ->join('catalog_related')
            ->on('catalog_related.with_id', '=', 'catalog.id')
            ->join('product_prices')
            ->on('catalog.id','=','product_prices.product_id')
            ->where('catalog_i18n.language', '=', \I18n::lang())
            ->where('product_prices.price_type', '=', $_SESSION['prices'])
            ->where('catalog_related.who_id', '=', $item->id)
            ->find_all();

        if (!count($result)) {
            $result = DB::select('catalog.*','product_prices.price',
                'catalog_i18n.*',
                'product_prices.price_old',
                'product_prices.currency_id')
                ->from('catalog')
                ->join('catalog_i18n', 'inner')
                ->on('catalog.id', '=', 'catalog_i18n.row_id')
                ->join('product_prices')
                ->on('catalog.id','=','product_prices.product_id')
                ->where('catalog_i18n.language', '=', \I18n::lang())
                ->where('product_prices.price_type', '=', $_SESSION['prices'])
                ->where('catalog.parent_id', '=', Route::param('group'))
                ->where('catalog.status', '=', 1)
                ->where('catalog.id', '!=', $item->id)
                ->order_by(DB::expr('rand()'))
                ->limit(5)
                ->find_all();
        }

        if (!count($result)) {
            return false;
        }
        $alias = Groups::getRow(Route::param('group'))->alias;
        return ['result' => $result, 'alias' => $alias, 'cat' => $cat];
    }

    public function Groups_CatalogMenuLeft()
    {
        $groups = null;
        $group_id = null;
        if (!empty(Route::param('group'))){
            $group_id = Route::param('group');
            $groups = Groups::getInnerGroups($group_id)->as_array();

            $arr = [];
            $index = 0;
            foreach ($groups as $group) {
                $ids = Groups::getInnerIds($group->id);
                $tmp = Items::getProductsIdByGroups($ids);
                $count = count($tmp->execute());
                if($count) {
                    $in = Groups::getInnerGroups($group->id);
                    foreach ($in as $obj) {
                        $child = (int) \Wezom\Modules\Catalog\Models\Groups::getCountChildRows($obj->id);

                        if ($child){
                            Groups::$innerIds = [];
                            $ids = Groups::getInnerIds($obj->id);
                            $tmp = Items::getProductsIdByGroups($ids);
                            $count = count($tmp->execute());
                        } else{
                            $count = count(Items::getProductByCatId($obj->id));
                        }

                        if($count > 0 ) {
                            $arr[$group->id][] = $obj;
                        }
                    }
                    $arr[$group_id][$index] = $group;
                    $index++;
                }
            }
            $groups = $arr;
        }
		$array = Filter::getClickableFilterElements();
        $manufacturers = Filter::getManufacturerWidget();
		$brands = Filter::getBrandsWidget();
		$colors = Filter::getColorsWidget();
        $ages = Filter::getAgesWidget()->as_array();
        usort($ages, function($a, $b) {
            return strnatcmp($a->name, $b->name);
        });
        $discounts = Filter::hasDiscounts($group_id);
        $sizes = Filter::getSizeWidget();
		$currentBrand = "";
		foreach ($brands as  $brand) {
			$check = Filter::checked($brand->alias, 'brand');
			if ($check) {
				$currentBrand .= $brand->name.' ';
			}
		}

		foreach ($manufacturers as  $manufacturer) {
			$check = Filter::checked($manufacturer->alias, 'manufacturer');
			if ($check) {
				$currentManufacturer .= $manufacturer->name.' ';
			}
		}

		$currentColor = "";
		foreach ($colors as $color) {
			$check = Filter::checked($color->alias, 'color');
			if ($check) {
				$currentColor .= $color->name.' ';
			}
		}

		$currentAges = "";
		foreach ($ages as $age){
		    $check = Filter::checked($age->alias, 'age');
		    if ($check){
		        $currentAges .= $age->name .' ';
            }
        }

		$specifications = Filter::getSpecificationsWidget();

		$mincost = Arr::get(Config::get('filter_array'), 'mincost', $array['min']);
		$maxcost = Arr::get(Config::get('filter_array'), 'maxcost', $array['max']);
		if (is_array($mincost)){
            $mincost = $mincost[0];
        }
		if (is_array($maxcost)){
            $maxcost = $maxcost[0];
        }

		return [
		    'groups' => $groups,
		    'group_id' => ($group_id)? $group_id : Route::param('group'),
			'brands' => $brands,
            'manufacturer' => $manufacturers,
			'colors' => $colors,
			'ages' => $ages,
			'sizes' => $sizes,
            'discounts' => $discounts,
			'currentColor' => $currentColor,
			'currentAges' => $currentAges,
			'currentBrand' => $currentBrand,
            'currentManufacturer' => $currentManufacturer,
			'specifications' => $specifications,
			'filter' => $array['filter'],
			'min' => $array['min'],
			'max' => $array['max'],
			'mincost' => $mincost,
			'maxcost' => $maxcost,
		];
    }

    public function Groups_CatalogMenuLeftMobile()
    {
        $groups = null;
        $group_id = null;
        if (!empty(Route::param('group'))){
            $group_id = Route::param('group');
            $groups = Groups::getInnerGroups($group_id);

            $arr = [];
            $index = 0;
            foreach ($groups as $group) {
                $ids = Groups::getInnerIds($group->id);
                $tmp = Items::getProductsIdByGroups($ids);
                $count = count($tmp->execute());
                if($count) {
                    $in = Groups::getInnerGroups($group->id);
                    foreach ($in as $obj) {
                        $child = (int) \Wezom\Modules\Catalog\Models\Groups::getCountChildRows($obj->id);

                        if ($child){
                            Groups::$innerIds = [];
                            $ids = Groups::getInnerIds($obj->id);
                            $tmp = Items::getProductsIdByGroups($ids);
                            $count = count($tmp->execute());
                        } else{
                            $count = count(Items::getProductByCatId($obj->id));
                        }

                        if($count > 0 ) {
                            $arr[$group->id][] = $obj;
                        }
                    }
                    $arr[$group_id][$index] = $group;
                    $index++;
                }
            }
            $groups = $arr;
        }

		$array = Filter::getClickableFilterElements();

		$brands = Filter::getBrandsWidget();
		$colors = Filter::getColorsWidget();
		$ages = Filter::getAgesWidget();
        $discounts = Filter::hasDiscounts($group_id);
		$sizes = Filter::getSizeWidget();

		$currentBrand = "";
		foreach ($brands as  $brand) {
			$check = Filter::checked($brand->alias, 'brand');
			if ($check) {
				$currentBrand .= $brand->name.' ';
			}

		}

		$currentColor = "";
		foreach ($colors as $color) {
			$check = Filter::checked($color->alias, 'color');
			if ($check) {
				$currentColor .= $color->name.' ';
			}

		}

		$currentAges = "";
		foreach ($ages as $age){
		    $check = Filter::checked($age->alias, 'age');
		    if ($check){
		        $currentAges .= $age->name .' ';
            }
        }

		$specifications = Filter::getSpecificationsWidget();

		$mincost = Arr::get(Config::get('filter_array'), 'mincost', $array['min']);
		$maxcost = Arr::get(Config::get('filter_array'), 'maxcost', $array['max']);
        if (is_array($mincost)){
            $mincost = $mincost[0];
        }
        if (is_array($maxcost)){
            $maxcost = $maxcost[0];
        }
		return [
            'groups' => $groups,
            'group_id' => ($group_id)? $group_id : Route::param('group'),
			'brands' => $brands,
			'colors' => $colors,
            'ages' => $ages,
			'sizes' => $sizes,
            'discounts' => $discounts,
			'currentColor' => $currentColor,
            'currentAges' => $currentAges,
			'currentBrand' => $currentBrand,
			'specifications' => $specifications,
			'filter' => $array['filter'],
			'min' => $array['min'],
			'max' => $array['max'],
			'mincost' => $mincost,
			'maxcost' => $maxcost,
		];

//        $colors = Colors::getRows();
//        $specifications = Filter::getSpecificationsWidget();
//        return ['colors' => $colors, 'specifications' => $specifications];
    }

    public function CatalogMenuTop()
    {
        if (!empty($this->_tree)) {
            $result = $this->_tree;
        } else {
            $result = Groups::getRows(1, 'sort');
            $this->_tree = $result;
        }
        $arr = [];
        foreach ($result as $obj) {
            $arr[$obj->parent_id][] = $obj;
        }
        $uri = explode('?', $_SERVER['REQUEST_URI']);

        return ['result' => $arr, 'currentLink' => $uri[0]];
    }

    public function CatalogMenuBottom()
    {
        if (!empty($this->_tree)) {
            $result = $this->_tree;
        } else {
            $result = Groups::getRows(1, 'sort');
            $this->_tree = $result;
        }
        $arr = [];
        foreach ($result as $obj) {
            $arr[$obj->parent_id][] = $obj;
        }
        $uri = explode('?', $_SERVER['REQUEST_URI']);
        return ['result' => $arr, 'currentLink' => $uri[0]];
    }

    public function Index_Slider(): array
    {
        $result = CommonI18n::factory('slider')->getRows(1, 'sort')->as_array();

        return ['result' => $result];
    }

    public function Index_Category()
    {

        $result = DB::select()
            ->from('catalog_tree')
            ->join('catalog_tree_i18n')->on('catalog_tree.id', '=', 'catalog_tree_i18n.row_id')
            ->where('catalog_tree_i18n.language', '=', \I18n::lang())
            ->where('status', '=', 1)
            ->and_where('parent_id', '=', 0)
            ->limit(4)->find_all()->as_array();
        if (!count($result)) {
            return false;
        }
        return ['result' => $result];
    }

    public function getUserFavorites()
    {
        if (!empty($this->userFavorites)) return $this->userFavorites;

        if (\Core\User::info()) {
            $favs = Favorite::getFavorites(\Core\User::info()->id);
            foreach ($favs as $f) {
                $fav[] = (int) $f->id;
            }
        }
        $this->userFavorites = $favs;

        return $favs;
    }

    public function Index_Novely()
    {
        if (!empty($this->_newProducts)) {
            $result = $this->_newProducts;
        } else {
            $result = DB::select('catalog_i18n.*','catalog.*',
                'product_prices.price',
                'product_prices.price_old',
                'product_prices.currency_id')
                ->from('catalog')
                ->join('catalog_i18n')
                ->on('catalog_i18n.id','=','catalog_i18n.row_id')
                ->where('catalog_i18n.language', '=', \I18n::lang())
                ->join('product_prices')
                ->on('catalog.id','=','product_prices.product_id')
                ->where('catalog.status', '=', 1)
                ->where('catalog.new', '=', 1)
                ->where('product_prices.price_type', '=', $_SESSION['prices'])
                ->limit(10)
                ->group_by('catalog.id')
                ->find_all()->as_array();
            $this->_newProducts = $result;
        }

        if (!count($result)) {
            return false;
        }
        $fav = $this->getUserFavorites();

        return ['result' => $result, 'favorite' => $fav];
    }

    public function Index_Sale()
    {
        if (!empty($this->_saleProducts)) {
            $result = $this->_saleProducts;
        } else {
            $result = DB::select('catalog.*',
                ['catalog_tree_i18n.name', 'parent_name'],
                'product_prices.price',
                'product_prices.price_old',
                'product_prices.currency_id',
                ['catalog_tree.alias', 'parent_alias'])
                ->from('catalog')
                ->join('catalog_tree')
                ->on('catalog.parent_id','=','catalog_tree.id')
                ->join('catalog_tree_i18n')->on('catalog_tree.id', '=', 'catalog_tree_i18n.row_id')
                ->where('catalog_tree_i18n.language', '=', \I18n::lang())
                ->join('product_prices')
                ->on('catalog.id','=','product_prices.product_id')
                ->where('catalog.status', '=', 1)
                ->where('product_prices.price_old', '<>', '0.00')
                ->where('product_prices.price_type', '=', $_SESSION['prices'])
                ->where('catalog.sale', '=', 1)
                ->limit(10)
                ->find_all()->as_array();
            $this->_saleProducts = $result;
        }

        if (!count($result)) {
            return false;
        }
        $fav = $this->getUserFavorites();

        return ['result' => $result, 'favorite' => $fav];
    }


    public function Index_News()
    {

        $result = DB::select()
            ->from('articles')
            ->where('status', '=', 1)
            ->order_by('date', 'DESC')
            ->limit(Config::get('basic.limit_articles_main_page'))->find_all()->as_array();

        if (!count($result)) {
            return false;
        }
        return ['results' => $result];
    }

    public function Index_MostViewed()
    {
        if (!empty($this->_viewedProducts)) {
            $result = $this->_viewedProducts;
        } else {
            $result = DB::select('catalog.*',
                ['catalog_tree_i18n.name', 'parent_name'],
                'product_prices.price',
                'product_prices.price_old',
                'product_prices.currency_id',
                ['catalog_tree.alias', 'parent_alias'])
                ->from('catalog')
                ->join('catalog_i18n')->on('catalog.id', '=', 'catalog_i18n.row_id')
                ->where('catalog_i18n.language', '=', \I18n::lang())

                ->join('catalog_tree')->on('catalog_tree.id', '=', 'catalog.parent_id')
                ->join('catalog_tree_i18n')->on('catalog_tree.id', '=', 'catalog_tree_i18n.row_id')
                ->where('catalog_tree_i18n.language', '=', \I18n::lang())
                ->join('product_prices')
                ->on('catalog.id','=','product_prices.product_id')
                ->where('product_prices.price_type', '=', $_SESSION['prices'])
                ->where('catalog.status', '=', 1)
                ->order_by('catalog.views', 'DESC')
                ->limit(10)
                ->find_all()->as_array();
            $this->_viewedProducts = $result;
        }

        if (!count($result)) {
            return false;
        }

        $fav = $this->getUserFavorites();

        return ['result' => $result, 'favorite' => $fav];
    }

    public function Index_Seo(){
        $result = CommonI18n::factory('control')->getRowSimple('index','alias');
        return ['text' =>$result->text];
    }
    /*
        TODO: Index_Info
     */
    public function News()
    {
        $result = News::getRows(1, 'date', 'DESC', 1)->as_array();
        if (!count($result)) {
            return false;
        }
        return ['obj' => $result[0]];
    }

    public function Articles()
    {
        $result = CommonI18n::factory('articles')->getRows(1, 'id', 'DESC', Config::get('basic.limit_articles_main_page'))->as_array();
        if (!count($result)) {
            return false;
        }
        return ['result' => $result];
    }

    public function Info()
    {
        $result = DB::select()
            ->from('content')
            ->where('status', '=', 1)
            ->where('id', 'IN', [5, 6, 7, 8])
            ->order_by('sort')
            ->find_all()->as_array();
        if (!count($result)) {
            return false;
        }
        return ['result' => $result];
    }

    public function HeaderCart()
    {
        $contentMenu = Common::factory('sitemenu')->getRows(1, 'sort');
        return ['contentMenu' => $contentMenu];
    }

    public function FooterScripts()
    {
        $contentMenu = Common::factory('sitemenu')->getRows(1, 'sort');
        $array['contentMenu'] = $contentMenu;
        $uri = explode('?', $_SERVER['REQUEST_URI']);
        $array['currentLink'] = $uri[0];
        $scripts = [
            HTML::media('assets/js/modernizr.js', false),
            HTML::media('assets/js/vendors.js', false),
			HTML::media('js/programmer/ulogin.js', false),
            HTML::media('assets/js/initialize.js', false),
        ];
        $js = \Minify\Core::factory('js')->minify($scripts);

        return [$array, 'js' => $js];
    }

    public function Footer(){
        if (!empty($this->_tree)) {
            $result = $this->_tree;
        } else {
            $result = Groups::getRows(1, 'sort');
            $this->_tree = $result;
        }
        $arr = [];
        foreach ($result as $obj) {
            $arr[$obj->parent_id][] = $obj;
        }
        $array['categories'] = $arr;
        $contentMenu = Menu::getRowsByGroup(0, 1, 'sort');
        $footerMenu = Menu::getRowsByGroup(2, 1, 'sort');
        $array['footerMenu'] = $footerMenu;
        $array['contentMenu'] = $contentMenu;
        $uri = explode('?', $_SERVER['REQUEST_URI']);
        $array['currentLink'] = $uri[0];
        return $array;
    }

    public function OrganizationStructuredData() {
        return [
            'data' => [
                '@context' => 'https://schema.org',
                '@type' => 'Organization',
                'url' => HTML::link('', true, 'https'),
                'name' => __('Babykroha - интернет магазин детских товаров'),
                'logo' => [
                    '@type' => 'ImageObject',
                    'url' => HTML::media('/assets/images/logo.svg')
                ],
                'contactPoint' => [
                    [
                        '@type' => 'ContactPoint',
                        'telephone' => Config::get('static.phone'),
                        'email' => Config::get('static.email_site')
                    ],
                    [
                        '@type' => 'ContactPoint',
                        'telephone' => Config::get('static.phone2')
                    ],
                    [
                        '@type' => 'ContactPoint',
                        'telephone' => Config::get('static.phone3')
                    ]
                ]
            ]
        ];
    }

    public function Header()
    {
        if (!empty($this->_tree)) {
            $result = $this->_tree;
        } else {
            $result = Groups::getRows(1, 'id', 'ASC')->as_array();
            $this->_tree = $result;
        }
        $arr = [];
        $top = [];
        $structuredData = [
            '@context' => 'https://schema.org',
            '@graph' => []
        ];
        foreach ($result as $obj) {
            $child = (int) \Wezom\Modules\Catalog\Models\Groups::getCountChildRows($obj->id);

            if ($child){
                Groups::$innerIds = [];
                $ids = Groups::getInnerIds($obj->id);
                $tmp = Items::getProductsIdByGroups($ids);
                $count = count($tmp->execute());
            } else{
                $count = count(Items::getProductByCatId($obj->id));
            }

            if($count > 0 || $obj->parent_id == 0) {
                $arr[$obj->parent_id][$obj->sort] = $obj;
                if($obj->parent_id == 0) {
                    $top[] = $obj;
                }
            }

            $structuredData['@graph'][] = [
                '@context' => 'https://schema.org',
                '@type' => 'SiteNavigationElement',
                'name' => $obj->name,
                'url' => HTML::link('catalog/' . $obj->alias, true, 'https')
            ];
        }

        $currency = Currency::getRows(1);
        $contentMenu = Menu::getRowsByGroup(3, 1, 'sort')->as_array();
        $ageMenu = Ages::getMenuItems(1,1);
        $ageBabyMenu = Ages::getMenuItems(2,1);
        $sidebarMenu = Menu::getRowsByGroup(1, 1, 'sort');
        $array['contentMenu'] = $contentMenu;
        $array['ageMenu'] = $ageMenu;
        $array['ageBabyMenu'] = $ageBabyMenu;
        $array['currency'] = $currency;
        if (Cookie::getWithoutSalt('currency')){
            $array['active'] = Cookie::getWithoutSalt('currency');
        } else{
            $currencyDB = DB::select()->from('currencies_courses')->where('id', '=', 980)->find();
            $currencyDB = $currencyDB->currency ?: 'USD';
            Cookie::setWithoutSalt('currency', $currencyDB);
            $array['active'] = $currencyDB;
        }
        if (Cookie::getWithoutSalt('user-type')){
            $array['active_type'] = Cookie::getWithoutSalt('user-type');
        }
        $array['sidebarMenu'] = $sidebarMenu;
//        dd($arr);
        $array['categories'] = $arr;
        $uri = explode('?', $_SERVER['REQUEST_URI']);
        $array['currentLink'] = $uri[0];
        $array['count_favorite'] = Favorite::favoriteCount(User::info()->id);
        $array['count_cart'] = count(Cart::factory()->get_list_for_basket());

        $mm = [];
        foreach ($top as $t){
            $mm[] = $t;
        }
        foreach ($contentMenu as $cm) {
            $mm[] = $cm;
        }
        $agesArr = [];
        foreach ($arr[0] as $key => $obj){
            $age_values = DB::select('age_id')->from('catalog_age_values')->where('group_id', '=', $obj->id)->find_all();
            $ids =[];
            if (count($age_values)>0){
                foreach ($age_values as $id){
                    $ids[] =  (int)$id->age_id;
                }
                    $agesByMenu = Ages::getMenuItemsByIds($ids);
                    $agesArr[$obj->id] = $agesByMenu;
            }else{
                $agesArr[$obj->id] = [];
            }

        }
        $array['agesArr'] = $agesArr;
        $array['mobileMenu'] = $mm;
        ksort($arr);
        $userTypes = CustomerRoles::getCustomRows(['visible' => '1'],'id')->as_array('alias','name');
        $role = CustomerRoles::getCustomerTypesByUserId(User::info()->id);
        foreach ($userTypes as $roleAlias => $roleName){
            if(!isset($role[$roleAlias])){
                unset($userTypes[$roleAlias]);
            }
        }

        $array['cart'] = Cart::factory()->get_list_for_basket();

        $array['userTypes'] = $userTypes;
        $array['languages'] = Config::get('languages');

        $array['structuredData'] = $structuredData;
        return $array;
    }

    public function CartHeader()
    {
        if (!empty($this->_tree)) {
            $result = $this->_tree;
        } else {
            $result = Groups::getRows(1, 'id', 'ASC')->as_array();
            $this->_tree = $result;
        }
        $arr = [];
        $top = [];
        $structuredData = [
            '@context' => 'https://schema.org',
            '@graph' => []
        ];
        foreach ($result as $obj) {
            $child = (int) \Wezom\Modules\Catalog\Models\Groups::getCountChildRows($obj->id);

            if ($child){
                Groups::$innerIds = [];
                $ids = Groups::getInnerIds($obj->id);
                $tmp = Items::getProductsIdByGroups($ids);
                $count = count($tmp->execute());
            } else{
                $count = count(Items::getProductByCatId($obj->id));
            }

            if($count > 0 || $obj->parent_id == 0) {
                $arr[$obj->parent_id][$obj->sort] = $obj;
                if($obj->parent_id == 0) {
                    $top[] = $obj;
                }
            }

            $structuredData['@graph'][] = [
                '@context' => 'https://schema.org',
                '@type' => 'SiteNavigationElement',
                'name' => $obj->name,
                'url' => HTML::link('catalog/' . $obj->alias, true, 'https')
            ];
        }

        $currency = Currency::getRows(1);
        $contentMenu = Menu::getRowsByGroup(3, 1, 'sort')->as_array();
        $ageMenu = Ages::getMenuItems(1,1);
        $ageBabyMenu = Ages::getMenuItems(2,1);
        $sidebarMenu = Menu::getRowsByGroup(1, 1, 'sort');
        $array['contentMenu'] = $contentMenu;
        $array['ageMenu'] = $ageMenu;
        $array['ageBabyMenu'] = $ageBabyMenu;
        $array['currency'] = $currency;
        if (Cookie::getWithoutSalt('currency')){
            $array['active'] = Cookie::getWithoutSalt('currency');
        } else{
            $currencyDB = DB::select()->from('currencies_courses')->where('id', '=', 980)->find();
            $currencyDB = $currencyDB->currency ?: 'USD';
            Cookie::setWithoutSalt('currency', $currencyDB);
            $array['active'] = $currencyDB;
        }
        if (Cookie::getWithoutSalt('user-type')){
            $array['active_type'] = Cookie::getWithoutSalt('user-type');
        }
        $array['sidebarMenu'] = $sidebarMenu;
//        dd($arr);
        $array['categories'] = $arr;
        $uri = explode('?', $_SERVER['REQUEST_URI']);
        $array['currentLink'] = $uri[0];
        $array['count_favorite'] = Favorite::favoriteCount(User::info()->id);
        $array['count_cart'] = count(Cart::factory()->get_list_for_basket());

        $mm = [];
        foreach ($top as $t){
            $mm[] = $t;
        }
        foreach ($contentMenu as $cm) {
            $mm[] = $cm;
        }
        $agesArr = [];
        foreach ($arr[0] as $key => $obj){
            $age_values = DB::select('age_id')->from('catalog_age_values')->where('group_id', '=', $obj->id)->find_all();
            $ids =[];
            if (count($age_values)>0){
                foreach ($age_values as $id){
                    $ids[] =  (int)$id->age_id;
                }
                $agesByMenu = Ages::getMenuItemsByIds($ids);
                $agesArr[$obj->id] = $agesByMenu;
            }else{
                $agesArr[$obj->id] = [];
            }

        }
        $array['agesArr'] = $agesArr;
        $array['mobileMenu'] = $mm;
        ksort($arr);
        $userTypes = CustomerRoles::getCustomRows(['visible' => '1'],'id')->as_array('alias','name');
        $role = CustomerRoles::getCustomerTypesByUserId(User::info()->id);
        foreach ($userTypes as $roleAlias => $roleName){
            if(!isset($role[$roleAlias])){
                unset($userTypes[$roleAlias]);
            }
        }

        $array['cart'] = Cart::factory()->get_list_for_basket();

        $array['userTypes'] = $userTypes;
        $array['languages'] = Config::get('languages');

        $array['structuredData'] = $structuredData;
        return $array;
    }

    public function Head()
    {
        $styles = [
            HTML::media('assets/css/vendors.css', false),
            HTML::media('assets/css/editor.css', false),
            HTML::media('assets/css/helpers.css', false),
			HTML::media('assets/css/style.css', false),
        ];
        $scripts = [];
        $scripts_no_minify = [];
        $css = \Minify\Core::factory('css')->minify($styles);
        $js = \Minify\Core::factory('js')->minify($scripts);

        $eCommerce = array();

        $adWords = array(
            'send_to' => 'AW-331964052'
        );

        $currencies = DB::select()->from('currencies_courses')->where('status','=','1')->find_all()->as_array('currency');
        $current_currency = DB::select()->from('currencies_courses')->where('currency','=', 'грн')->find();

        if ($_GET['page_type'] == 'home') {
            $adWords['dynx_pagetype'] = 'home';
            $adWords['dynx_itemid'] = array();
            $adWords['dynx_totalvalue'] = array();

            $newProducts = $this->Index_Novely();
            if (!empty($newProducts['result'])) {
                foreach ($newProducts['result'] as $product) {
                    $adWords['dynx_itemid'][] = $product->id;
                    $data = Support::calculatePriceWithCurrency($product, $currencies, $current_currency);
                    $adWords['dynx_totalvalue'][] = (float)$data['cost'];
                }
            }

            $saleProducts = $this->Index_Sale();
            if (!empty($saleProducts['result'])) {
                foreach ($saleProducts['result'] as $product) {
                    $adWords['dynx_itemid'][] = $product->id;
                    $data = Support::calculatePriceWithCurrency($product, $currencies, $current_currency);
                    $adWords['dynx_totalvalue'][] = (float)$data['cost'];
                }
            }

            $viewedProducts = $this->Index_MostViewed();
            if (!empty($viewedProducts['result'])) {
                foreach ($viewedProducts['result'] as $product) {
                    $adWords['dynx_itemid'][] = $product->id;
                    $data = Support::calculatePriceWithCurrency($product, $currencies, $current_currency);
                    $adWords['dynx_totalvalue'][] = (float)$data['cost'];
                }
            }
        }
        elseif (!empty($_GET['products'])) {
            switch ($_GET['page_type']) {
                case 'category': $adWords['dynx_pagetype'] = 'category'; break;
                case 'searchresults': $adWords['dynx_pagetype'] = 'searchresults'; break;
            }

            $adWords['dynx_itemid'] = array();
            $adWords['dynx_totalvalue'] = array();
            foreach ($_GET['products'] as $product) {
                $adWords['dynx_itemid'][] = $product->id;
                $data = Support::calculatePriceWithCurrency($product, $currencies, $current_currency);
                $adWords['dynx_totalvalue'][] = (float)$data['cost'];
            }
        }
        elseif (!empty($_GET['product'])) {
            $adWords['dynx_pagetype'] = 'oferdetail';
            $adWords['dynx_itemid'] = $_GET['product']->id;
            $data = Support::calculatePriceWithCurrency($_GET['product'], $currencies, $current_currency);
            $adWords['dynx_totalvalue'] = (float)$data['cost'];
        }
        elseif (!empty($_GET['cart_products'])) {
            $adWords['dynx_pagetype'] = 'conversionintent';

            foreach ($_GET['cart_products'] as $product) {
                $adWords['dynx_itemid'][] = $product['id'];
                $data = Support::calculatePriceWithCurrency($product['obj'], $currencies, $current_currency);
                $adWords['dynx_totalvalue'][] = (float)$data['cost'];
            }
        }
        elseif (!empty($_GET['order_products'])) {
            $adWords['dynx_pagetype'] = 'conversion';
            $eCommerce['transaction_id'] = $_GET['transaction_id'];
            $eCommerce['currency'] = 'UAH';
            $eCommerce['shipping'] = 0;
            $eCommerce['items'] = array();

            $sumPrice = 0;
            foreach ($_GET['order_products'] as $product) {
                $adWords['dynx_itemid'][] = $product->id;
                $data = Support::calculatePriceWithCurrency($product, $currencies, $current_currency);
                $adWords['dynx_totalvalue'][] = (float)$data['cost'];

                $price = (float)$data['cost'];
                $quantity = (int)$product->count;
                $eCommerce['items'][] = array(
                    'id' => $product->id,
                    'name' => $product->name,
                    'variant' => $product->size,
                    'quantity' => $quantity,
                    'price' => $price
                );
                $sumPrice += $price * $quantity;
            }
            $eCommerce['value'] = $sumPrice;
        }

        return ['js' => $js, 'css' => $css, 'scripts_no_minify' => $scripts_no_minify, 'adWords' => $adWords, 'eCommerce' => $eCommerce];
    }

    public function Popup_CartSizes() {
        $productId = Arr::get($_POST, 'param.item_id');
        $count_current = DB::select('carts_items_sizes.size', 'carts_items_sizes.count')
            ->from('carts_items_sizes')
            ->join('carts_items')
            ->on('carts_items.id','=', 'carts_items_sizes.cart_item_id')
            ->where('carts_items.catalog_id', '=', $productId)
            ->where('carts_items.cart_id', '=', Cart::factory()->_cart_id)
            ->find_all()->as_array();

    	if (!$productId) {
    		return false;
		}
    	$bigPopup = Arr::get($_POST, 'bigPopup');
        if ($bigPopup){
            $bigPopup = true;
        }
		$result = DB::select()
			->from('catalog_size_amount')
			->where('catalog_id', '=', $productId)
            ->order_by(DB::expr('ABS(size)'))
			->find_all();
    	$counts = Arr::get(Arr::get($_POST, 'param'),'counts', false);

        $item = Items::getRow($productId);
        if (isset($item->color_alias) && !empty($item->color_alias)){
            $res = Colors::getRow($item->color_alias, 'alias');
            $item->color = $res->color;
            $item->color_name = $res->name;
        }
        if(isset($item->catalog_color_group_id)){
            $item->color_group = Items::getProductByColorCatId($item->catalog_color_group_id);
        }
        $arr = [];
        if (isset($counts[0])) {
            $i = 0;
            foreach ($result as $size) {
                $arr[$size->size] = $counts[$i];
                $i++;
            }
        } else {
            foreach ($counts as $size=>$count){
                $arr[$size] = $count;
            }
        }

        $max_count_current = array();
        foreach ($count_current as $cr){
            $max_count_current[$cr->size] = $cr->count;
        }

		return ['max_count_current' => $max_count_current, 'sizes' => $result, 'obj' => $item, 'counts' => $arr, 'bigPopup'=>$bigPopup];
	}

    public function Popup_CartSizePage() {

        $productId = Arr::get($_POST, 'param.item_id');
        $counts = Arr::get($_POST, 'param.counts');

        if (!$productId) {
            return false;
        }
        $result = DB::select()
            ->from('catalog_size_amount')
            ->where('catalog_id', '=', $productId)
            ->order_by(DB::expr('ABS(size)'))
            ->find_all();
        $item = Items::getRow($productId);
        if (isset($item->color_alias) && !empty($item->color_alias)){
            $res = Colors::getRow($item->color_alias, 'alias');
            $item->color = $res->color;
            $item->color_name = $res->name;
        }
        if(isset($item->catalog_color_group_id)){
            $item->color_group = Items::getProductByColorCatId($item->catalog_color_group_id);
        }
        if(count($result) == count($counts)){
            $arr = [];
            $index = 0;
            foreach ($result as $obj){
                if ($counts[$obj->size]){
                    $arr[$obj->size] = $counts[$obj->size];
                } else {
                    $arr[$obj->size] = $counts[$index];
                }
                $index++;
            }
            $counts = $arr;
        }
        return ['sizes' => $result, 'item_id' => $productId, 'obj' => $item, 'counts' => $counts];
    }

    public function Popup_CartWithoutSizes() {

        $productId = Arr::get($_POST, 'param.item_id');
        $count = Arr::get($_POST, 'param.count');
        $from = Arr::get($_POST, 'param.from');

        if (!$productId) {
            return false;
        }
        $result = DB::select()
            ->from('catalog_size_amount')
            ->where('catalog_id', '=', $productId)
            ->where('size', '=', '-')
            ->find();

        return ['amount' => $result->amount, 'item_id' => $productId, 'count' => $count, 'from' => $from];
    }

	public function Popup_CartUpdate() {

    	$productId = Arr::get($_POST, 'param.item_id');
        $counts = Arr::get($_POST, 'param.counts');

    	if (!$productId) {
    		return false;
		}
		$result = DB::select()
			->from('catalog_size_amount')
			->where('catalog_id', '=', $productId)
            ->where('amount', '>', 0)
            ->order_by(DB::expr('ABS(size)'))
			->find_all();
        $arr = [];
        $index = 0;
        foreach ($result as $obj){
            if ($counts[$obj->size]){
                $arr[$obj->size] = $counts[$obj->size];
            } else {
                $arr[$obj->size] = $counts[$index];
            }
            $index++;
        }
        $counts = $arr;
		return ['sizes' => $result, 'item_id' => $productId, 'counts' => $counts];
	}
	public function Popup_Gallery(){
        $item = DB::select()->from('carts_items')
            ->where('cart_id', '=', Cart::factory()->_cart_id)
            ->where('catalog_id', '=', $_POST['param']['item']['id'])
            ->find();
        //var_dump($item); die();
        if ($item){
            $sizes = DB::select()->from('carts_items_sizes')
                ->where('cart_item_id', '=', $item->id)
                ->find_all();
            $active_sizes = ['null'];
            $active_count = [];
            foreach ($sizes as $size){
                $active_sizes [] = $size->size;
                $active_count [$size->size] = $size->count;

            }
        }else{
            $active_count = [];
            $active_sizes = [];
        }
        return['active_sizes'=>$active_sizes, 'active_count'=>$active_count, 'sizes_find' => Items::getProductSizeAmountById($_POST['param']['item']['id'])->as_array(),];

    }

	public function Groups_CatalogMenuSelected() {

		$checkedFilter = Filter::getFilterArr(Route::param('filter'));
		$checkValues = null;
        if(!empty($checkedFilter)){
            foreach ($checkedFilter as $key => $checked) {

                if (is_array($checked)) {
                    foreach ($checked as $check) {
                        switch ($key){
                            case 'brand': $obj = Brands::getRowSimple($check, 'alias'); break;
                            case 'manufacturer': $obj = Manufacturers::getRowSimple($check, 'alias'); break;
                            case 'color': $obj = Colors::getRowSimple($check, 'alias'); break;
                            case 'size': $obj = $check; break;
                            case 'age': $obj = Ages::getRowSimple($check, 'alias'); break;
                            case 'discounts': $obj = __('Скидка'); break;
                            default: $obj = SpecificationsValues::getRowSimple($check, 'alias'); break;
                        }
                        if ($obj) {
                            $checkValues[$key][] = $obj;
                        }
                    }
                } else {
                    switch ($key){
                        case 'brand': $obj = Brands::getRowSimple($checked, 'alias'); break;
                        case 'manufacturer': $obj = Manufacturers::getRowSimple($checked, 'alias'); break;
                        case 'color': $obj = Colors::getRowSimple($checked, 'alias'); break;
                        case 'size': $obj = $checked; break;
                        case 'age': $obj = Ages::getRowSimple($checked, 'alias'); break;
                        case 'discounts': $obj = __('Скидка'); break;
                        default: $obj = SpecificationsValues::getRowSimple($checked, 'alias'); break;
                    }
                    if ($obj) {
                        $checkValues[$key] = $obj;
                    }
                }

            }
        }
		return ['selected' => $checkValues];

	}

	public function Popup_TableSizes(): array
    {
        $param = Arr::get($_POST,'param');
        $product_id = null;
        if($param && isset($param['id'])){
            $product_id = $param['id'];
        }
        if(empty($product_id)){
            return ['labels' => null, 'sizes' => null];
        }
        $product = Items::getRowSimple($product_id);
        $product_sizes = Items::getProductSizeAmountById($product_id)->as_array('code','amount');
        $sizes = DB::select()->from('helper_characteristics')->find_all()->as_array();
        $labels = [];
        foreach ($sizes as $size){
            if(isset($product_sizes[$size->code])){
                $label = DB::select()
                    ->from('helper_characteristics_sizes')
                    ->join(['catalog_reference', 'cr'], 'inner')
                    ->on('cr.key', '=', 'helper_characteristics_sizes.mark')
                    ->join(['catalog_reference_i18n', 'crt'], 'inner')
                    ->on('crt.row_id', '=', 'cr.id')
                    ->where('helper_characteristics_sizes.helper_characteristics_id', '=', $size->id)
                    ->where('helper_characteristics_sizes.parameter', '<>', '0')
                    ->where('crt.language', '=', \I18n::lang())
                    ->find_all()
                    ->as_array();
                if(!empty($label)){
                    $labels[$size->name] = $label;
                }
            }
        }
        return ['labels' => $labels, 'sizes' => $sizes, 'product' => $product];
    }

    public function Popup_ChildSelection() {
        $params = Arr::get($_POST, 'param');
        $allAges = Filter::getAgesWidget2($params)->as_array();

        $ages = array();
        foreach ($allAges as $age) {
            $ages[$age->specification_value_alias][] = $age;
        }

        foreach ($ages as $key => $age) {
            usort($ages[$key], function($a, $b) {
                return strnatcmp($a->name, $b->name);
            });
        }

        $checkedFilter = Filter::getFilterArr($params['filter']);

        $selectedAges = Arr::get($checkedFilter, 'age', []);
        $selectedAges = is_array($selectedAges) ? $selectedAges : [$selectedAges];
        $selectedSex = Arr::get($checkedFilter, 'polrebenka', []);
        $selectedSex = is_array($selectedSex) ? $selectedSex : [$selectedSex];

        $selectedData = array(
            'age' => $selectedAges,
            'sex' => $selectedSex
        );

        return compact('ages', 'selectedData');
    }

    public function Groups_AddChild() {
        $allAges = Filter::getAgesWidget(true)->as_array();

        $ages = array();
        foreach ($allAges as $age) {
            $ages[$age->specification_value_alias][] = $age;
        }

        foreach ($ages as $key => $age) {
            usort($ages[$key], function($a, $b) {
                return strnatcmp($a->name, $b->name);
            });
        }

        $checkedFilter = Filter::getFilterArr(Route::param('filter'));

        $selectedAges = Arr::get($checkedFilter, 'age', []);
        $selectedAges = is_array($selectedAges) ? $selectedAges : [$selectedAges];
        $selectedSex = Arr::get($checkedFilter, 'polrebenka', []);
        $selectedSex = is_array($selectedSex) ? $selectedSex : [$selectedSex];

        $selectedData = array(
            'age' => $selectedAges,
            'sex' => $selectedSex
        );

        return compact('ages', 'selectedData');
    }

    public function Groups_Dropshipping() {
        $information = Information::getRowSimple(9);

        return compact('information');
    }
}
