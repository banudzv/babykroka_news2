<?php
namespace Core;

use Core\QB\DB;

class View
{

    public static function tpl($array, $tpl, $cron = false)
    {
        $currencyDB = DB::select()->from('currencies_courses')->where('id', '=', 980)->find();
        $currencyDB = $currencyDB->currency ?: 'USD';
        $currency = (Cookie::getWithoutSalt('currency')) ?: $currencyDB;
        $role = (Cookie::getWithoutSalt('user-type'))?:'opt';
        $array['_currency'] = DB::select()->from('currencies_courses')->where('status','=','1')->find_all()->as_array('currency');
        $array['_currentCurrency'] = DB::select()->from('currencies_courses')->where('currency','=', $currency)->find();
        $array['_currentRole'] = DB::select()->from('customer_roles')->where('alias','=', $role)->find();
        return View::show_tpl($array, $tpl . '.php', $cron);
    }


    public static function widget($array, $tpl)
    {
        $currencyDB = DB::select()->from('currencies_courses')->where('id', '=', 980)->find();
        $currencyDB = $currencyDB->currency ?: 'USD';
        $currency = (Cookie::getWithoutSalt('currency'))?: $currencyDB;
        $array['_currency'] = DB::select()->from('currencies_courses')->where('status','=','1')->find_all()->as_array('currency');
        $array['_currentCurrency'] = DB::select()->from('currencies_courses')->where('currency','=', $currency)->find();
        return View::show_tpl($array, 'Widgets' . DS . $tpl . '.php');
    }


    static function show_tpl($tpl_data, $name_file, $cron = false)
    {
        ob_start();
        extract($tpl_data, EXTR_SKIP);
        $filepath = HOST . DS . 'Views' . DS . $name_file;
        if (!Config::get('error') && APPLICATION == 'backend' && !$cron ) {
            $filepath = HOST . DS . 'Wezom' . DS . 'Views' . DS . $name_file;
        }
        include $filepath;
        return ob_get_clean();
    }


    static function core($data, $name_file)
    {
        ob_start();
        extract($data, EXTR_SKIP);
        include(HOST . DS . 'Core' . DS . $name_file . '.php');
        return ob_get_clean();
    }

}
