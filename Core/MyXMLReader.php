<?php


namespace Core;

use XMLReader;

/**
 * Class MyXMLReader
 * @package Core
 */
class MyXMLReader extends XMLReader
{
    /**
     * @return bool
     */
    public function readToNextElement()
    {
        while ($result = $this->read() and $this->nodeType !== self::ELEMENT) {
            return $result;
        }
    }

    /**
     * @param $localname
     * @return bool
     */
    public function readToNext($localname)
    {
        while ($result = $this->readToNextElement() and $this->localName !== $localname) {
            return $result;
        }
    }

    /**
     * @param $depth
     * @return bool|false
     */
    public function readToNextChildElement($depth)
    {
        // if the current element is the parent and
        // empty there are no children to go into
        if ($this->depth === $depth && $this->isEmptyElement) {
            return false;
        }

        while ($result = $this->read()) {
            if ($this->depth <= $depth) {
                return false;
            }
            if ($this->nodeType === self::ELEMENT) {
                break;
            }
        }

        return $result;
    }

    /**
     * @param null $default
     * @return string|null
     */
    public function getNodeValue($default = NULL)
    {
        $node = $this->expand();
        return $node ? trim($node->nodeValue) : $default;
    }
}
