<?php

namespace Core\Delivery;

use Core\Config;

class NP {

    public static $key; //'7585ce9f2808e6d008fb710ee95933f9';76713a9ea32d4f7a4af61cad56f472cd

    function __construct()
    {
        static::$key = Config::get('basic.npkey');
    }

    //TODO продумать кеширование данных от новой почты в нашей базе
    public function getRegions()
    {
        $lang = \I18n::$lang;
        $modelName = 'Address';
        $calledMethod = 'getAreas';
        $methodProperties = array();

        $data = $this->sendRequest($modelName, $calledMethod, $methodProperties);
        $region_arr = array();
        foreach ($data['data'] as $key => $val) {
            // TODO: Вернуть Крым
            if($val['Ref'] != '71508128-9b87-11de-822f-000c2965ae0e') {
                $region_arr[$val['Ref']] = $lang == 'ru' ? $val['DescriptionRu'] : $val['Description'];
            }
        }
        return $region_arr;
    }

    public function getCities($region_id)
    {
        $lang = \I18n::$lang;
        $modelName = 'Address';
        $calledMethod = 'getCities';
        $methodProperties = array('AreaRef' => $region_id);

        $data = $this->sendRequest($modelName, $calledMethod, $methodProperties);

        $city_arr = array();
        foreach ($data['data'] as $key => $val) {
            if($val['Area'] == $region_id) {
                $city_arr[$val['Ref']] = $lang == 'ru' ? $val['DescriptionRu'] : $val['Description'];
            }
        }
        return $city_arr;
    }

    public function getSettlements($region_id)
    {
        $lang = \I18n::$lang;
        $modelName = 'Address';
        $calledMethod = 'getSettlements';
        $methodProperties = array('AreaRef' => $region_id);

        $data = $this->sendRequest($modelName, $calledMethod, $methodProperties);

        $city_arr = array();
        foreach ($data['data'] as $key => $val) {
            if($val['Area'] == $region_id) {
                $key = $lang == 'ru' ? $val['DescriptionRu'] : $val['Description'];
                $city_arr[$key] = $val['Ref'];
            }
        }
        return $city_arr;
    }

    public function getWarehouses($city_id)
    {
        $lang = \I18n::$lang;
        $modelName = 'Address';
        $calledMethod = 'getWarehouses';
        $methodProperties = array('CityRef' => $city_id);

        $data = $this->sendRequest($modelName, $calledMethod, $methodProperties);

        $warehouses_arr = array();
        foreach ($data['data'] as $key => $val) {
            $warehouses_arr[$val['Ref']] = $lang == 'ru' ? $val['DescriptionRu'] : $val['Description'];
        }

        return $warehouses_arr;
    }

    public function searchStreet($name, $cityRef)
    {
        $modelName = 'Address';
        $calledMethod = 'getStreet';
        $methodProperties = array(
            'FindByString' => $name,
            'CityRef' => $cityRef
        );

        $data = $this->sendRequest($modelName, $calledMethod, $methodProperties);

        $streets = array();
        foreach ($data['data'] as $key => $val) {
            $streets[] = array(
                'id' => $val['Ref'],
                'text' => $val['Description']
            );
        }

        return $streets;
    }

    public function getStreets($cityRef, $page = 1, $streets = [])
    {
        $modelName = 'Address';
        $calledMethod = 'getStreet';
        $methodProperties = array(
            'CityRef' => $cityRef,
            'Page' => $page
        );

        $data = $this->sendRequest($modelName, $calledMethod, $methodProperties);

        foreach ($data['data'] as $key => $val) {
            $streets[$val['Ref']] = $val['Description'];
        }

        $page++;

        if (!empty($data['data'])) return $this->getStreets($cityRef, $page, $streets);

        return $streets;
    }

    public function getTracking($ttn_numbers = []){
        if(count($ttn_numbers)) {
            $modelName = 'TrackingDocument';
            $calledMethod = 'getStatusDocuments';
            foreach ($ttn_numbers as $obj) {
                $methodProperties['Documents'][] = [
                    'DocumentNumber' => $obj,
                    'Phone' => ''
                ];
            }

            $data = $this->sendRequest($modelName, $calledMethod, $methodProperties, false);
            $result = [];
            if(count($data['data'])){
                foreach ($data['data'] as $arr){
                    $result[$arr['Number']] = $arr;
                }
            }

            return $result;
        }
    }

    public function getRegion($id)
    {
        $regions = $this->getRegions();
        if (isset($regions[$id]))
        {
            return $regions[$id];
        } else{
            return 'Region not found!';
        }
    }

    public function getCity($region, $id)
    {
        $cities = $this->getCities($region);
        if (isset($cities[$id]))
        {
            return $cities[$id];
        } else{
            return 'City not found!';
        }
    }
    public function getWarehouse($city, $id)
    {
        $warehouses = $this->getWarehouses($city);
        if (isset($warehouses[$id]))
        {
            return $warehouses[$id];
        } else{
            return 'Warehouse not found!';
        }
    }

    private function sendRequest($modelName, $calledMethod, $methodProperties, $json_force_object = true)
    {
        $post = array(
            'modelName' => $modelName,
            'calledMethod' => $calledMethod,
            'methodProperties' => $methodProperties,
            'apiKey' => self::$key,
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://api.novaposhta.ua/v2.0/json/');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: text/html"));
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post, $json_force_object ? JSON_FORCE_OBJECT : null));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);
        $data = json_decode($response, 1);

        return $data;

    }
}
