<?php
namespace Core;

use Core\QB\DB;
use DOMDocument;
use Modules\Base;
use Modules\Catalog\Models\PriceTypes;
use Wezom\Modules\Catalog\Models\Currency;

class Support
{
    public static function curlSend($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_REFERER, NULL);
        curl_setopt($ch, CURLOPT_USERAGENT, "Opera/9.80 (Windows NT 5.1; U; ru) Presto/2.9.168 Version/11.51");
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_COOKIEFILE, 'cookie.txt');
        curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie.txt');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0); // не проверять SSL сертификат
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0); // не проверять Host SSL сертификата
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $ret = curl_exec($ch);
        curl_close($ch);
        return $ret;
    }

    /**
     * @param $xmlContent
     * @param string $version
     * @param string $encoding
     * @return bool
     */
    public static function isXMLContentValid($xmlContent, $version = '1.0', $encoding = 'utf-8')
    {
        if (trim($xmlContent) === '') {
            return false;
        }

        libxml_use_internal_errors(true);

        $doc = new DOMDocument($version, $encoding);
        $doc->loadXML($xmlContent);

        $errors = libxml_get_errors();
        libxml_clear_errors();

        return empty($errors);
    }

    public static function selectData($elements, $valueField, $nameField, $emptyField = null)
    {
        $data = [];
        if ($emptyField !== null) {
            if (is_array($emptyField)) {
                $data[$emptyField[0]] = $emptyField[1];
            } else {
                $data[$emptyField] = '';
            }
        }
        foreach ($elements AS $obj) {
            if (is_array($obj)) {
                $data[$obj[$valueField]] = $obj[$nameField];
            } else {
                $data[$obj->{$valueField}] = $obj->{$nameField};
            }
        }
        return $data;
    }


    public static function addItemTag($obj)
    {
        if ($obj->sale) {
            return '<div class="splash2"><span>акция</span></div>';
        }
        if ($obj->top) {
            return '<div class="splash3"><span>популярное</span></div>';
        }
        if ($obj->new and time() - (int)$obj->new_from < Config::get('basic.new_days') * 24 * 60 * 60) {
            return '<div class="splash"><span>новинка</span></div>';
        }
    }

    public static function getRootParent($result, $id)
    {
        if (!$id) {
            return 0;
        }
        foreach ($result AS $obj) {
            if ($obj->id == $id) {
                if ($obj->parent_id == 0) {
                    return $obj->id;
                } else {
                    return Support::getRootParent($result, $obj->parent_id);
                }
            }
        }
    }

    public static function addZero($number)
    {
        $string = (string)$number;
        if (strlen($string) == 2) {
            return $string;
        }
        return '0' . $string;
    }

    public static function getHumanDateRange($from, $to)
    {
        if ($from == $to) {
            return date('d', $from) . '&nbsp;' . Dates::month(date('m', $from));
        }
        if (date('m', $from) == date('m', $to)) {
            return date('d', $from) . ' — ' . date('d', $to) . '&nbsp;' . Dates::month(date('m', $from));
        }
        return date('d', $from) . '&nbsp;' . Dates::month(date('m', $from)) . ' — ' . date('d', $to) . '&nbsp;' . Dates::month(date('m', $to));
    }

    public static function firstWordInSpan($string)
    {
        $words = explode(' ', $string);
        $words[0] = '<span>' . $words[0] . '</span>';
        $string = implode(' ', $words);
        return $string;
    }

    public static function firstWordWithBr($string)
    {
        $words = explode(' ', $string);
        $words[0] = $words[0] . '<br />';
        $string = implode(' ', $words);
        return $string;
    }

    public static function firstWordInSpanWithBr($string)
    {
        $words = explode(' ', $string);
        $words[0] = '<span>' . $words[0] . '</span><br />';
        $string = implode(' ', $words);
        return $string;
    }

    // Options/optgroups tree for tag select
    public static function getSelectOptions($filename, $table, $parentID = NULL, $currentElement = 0, $sort = 'sort', $parentAlias = 'parent_id')
    {
        if ($filename != 'Catalog/Items/Select') {
            $current = Route::param('id');
        } else {
            $current = 0;
        }
        $tree = [];
        $result = DB::select($table. '_i18n.*', $table. '.*')->from($table)->where('status','=', 1)
            ->join($table. '_i18n')->on($table. '.id', '=', $table.'_i18n.row_id')
            ->where($table. '_i18n.language', '=', \I18n::$defaultLangBackend)
            ->order_by($sort)->as_object()->execute();
        foreach ($result AS $obj) {
            if (!$current) {
                $tree[$obj->$parentAlias][] = $obj;
            } else if ($obj->parent_id != $current AND $obj->id != $current) {
                $tree[$obj->$parentAlias][] = $obj;
            }
        }
        return View::tpl([
            'result' => $tree,
            'currentParent' => 0,
            'space' => '',
            'filename' => $filename,
            'parentID' => $parentID,
            'parentAlias' => $parentAlias,
            'currentID' => $currentElement,
        ], $filename);
    }

    // Get human readable date range for admin filter widget
    public static function getWidgetDatesRange()
    {
        $dates = [];
        if (Arr::get($_GET, 'date_s')) {
            $dates[] = Support::getWidgetDate(Arr::get($_GET, 'date_s'));
        }
        if (Arr::get($_GET, 'date_po') and Arr::get($_GET, 'date_s') != Arr::get($_GET, 'date_po')) {
            $dates[] = Support::getWidgetDate(Arr::get($_GET, 'date_po'));
        }
        return implode(' - ', $dates);
    }

    // Get human readable date range
    public static function getWidgetDate($date)
    {
        $time = strtotime($date);
        return date('j', $time) . ' ' . Dates::month(date('m', $time)) . ' ' . date('Y', $time);
    }

    // Generate link for filters in wezom
    public static function generateLink($key, $value = null, $fakeLink = null)
    {
        $link = $fakeLink ? $fakeLink : Arr::get($_SERVER, 'REQUEST_URI');
        $uri = explode('?', $link);

        $__get = [];
        if (count($uri) > 1) {
            $arr = explode('&', $uri[1]);
            foreach ($arr AS $_a) {
                $g = rawurldecode($_a);
                $g = strip_tags($g);
                $g = stripslashes($g);
                $g = trim($g);
                $___get = explode('=', $g);
                $__get[$___get[0]] = $___get[1];
            }
        }

        if ($value === null) {
            if (!isset($__get[$key])) {
                return $link;
            }
            $arr = explode('&', $uri[1]);
            $get = [];
            foreach ($arr AS $el) {
                $h = explode('=', $el);
                if ($key != $h[0]) {
                    $get[] = $h[0] . '=' . $h[1];
                }
            }
            $uri[1] = implode('&', $get);
            if ($uri[1]) {
                return $uri[0] . '?' . $uri[1];
            }
            return $uri[0];
        }

        if (!isset($__get[$key])) {
            if (isset($uri[1])) {
                return Arr::get($uri, 0) . '?' . Arr::get($uri, 1) . '&' . $key . '=' . $value;
            }
            return Arr::get($uri, 0) . '?' . $key . '=' . $value;
        }
        if (Arr::get($__get, $key) == $value) {
            return $link;
        }
        $arr = explode('&', $uri[1]);
        $get = [];
        foreach ($arr AS $el) {
            $h = explode('=', $el);
            if ($key == $h[0]) {
                $get[] = $key . '=' . $value;
            } else {
                $get[] = $h[0] . '=' . $h[1];
            }
        }

        $uri[1] = implode('&', $get);
        return $uri[0] . '?' . $uri[1];
    }

    public static function getFiles($path = null, $ext = [])
    {
        $dir = HOST;
        if ($path) {
            $dir .= '/' . $path;
        }
        $files = scandir($dir);
        if (sizeof($ext)) {
            $arr = [];
            foreach ($files as $key => $val) {
                if (is_file(HOST . '/' . $val) and in_array(end(explode('.', $val)), $ext)) {
                    $arr[] = $val;
                }
            }
            return $arr;
        }

        return $files;
    }

    /**
     * Добавляет тип цены на сайт, смотрим на код цены($_SESSION['prices']) установленный в сессии
     * если такого нет возвращаем пустоту
     * @see Base::checkUser()
     * @return string|null
     */
    public static function productLabel(): ?string
    {
        $code = $_SESSION['prices'];
        $type = PriceTypes::getRow($code,'code');
        if (!$type){
            return null;
        }
        return '<span>'.mb_strtolower(Text::limit_chars($type->name,4,'.')).'</span>';
    }


    public static function calculatePriceWithCurrency($obj,$_currency,$_currentCurrency){
        if(is_array($obj)){
            $obj = (object)$obj;
        }
        $currency = Currency::getRow($obj->currency_id)->currency;
        $cost = $obj->price;
        $cost_old = $obj->price_old;
        if ($currency !== $_currentCurrency->currency) {
            if ($_currentCurrency->currency == 'грн' or $_currentCurrency->currency == 'UAH') {
                if ($currency == 'USD') {
                    $cost = round($cost * $_currency['USD']->course, 2);
                    $cost_old = round($cost_old * $_currency['USD']->course, 2);
                } else {
                    $cost = round($cost / $_currentCurrency->course, 2);
                    $cost_old = round($cost_old / $_currentCurrency->course, 2);
                }
            } elseif ($_currentCurrency->currency == 'RUB') {
                if ($currency == 'USD') {
                    $cost = round($cost * $_currency['USD']->course, 2);
                    $cost_old = round($cost_old * $_currency['USD']->course, 2);
                }
                $cost = round($cost / $_currentCurrency->course, 2);
                $cost_old = round($cost_old / $_currentCurrency->course, 2);
            } else {
                $cost = round($cost / $_currentCurrency->course, 2);
                $cost_old = round($cost_old / $_currentCurrency->course, 2);
            }
            $currency = $_currentCurrency->currency;
        }
        return ['currency' => $currency,'cost' => $cost, 'cost_old' => $cost_old];
    }

    public static function calculateCostWithCurrency($cost,$_currency,$_currentCurrency, $product_curr = 'грн'){

        if(!$_currentCurrency){
            $current = Currency::getRow('980', 'id');

        }else{
            $current = $_currentCurrency;
        }
        $currency = $product_curr;
        if ($currency !== $current->currency) {
            if ($_currentCurrency->currency == 'грн' or $_currentCurrency->currency == 'UAH') {
                if ($currency == 'USD') {
                    $cost = round($cost * $_currency['USD']->course, 2);

                } else {
                    $cost = round($cost / $current->course, 2);
                }
            } elseif ($current->currency == 'RUB') {
                if ($currency == 'USD') {
                    $cost = round($cost * $_currency['USD']->course, 2);
                }
                $cost = round($cost / $current->course, 2);
            } else {
                $cost = round($cost / $current->course, 2);
            }
        }
        return $cost;
    }

}
