<?php
use Core\HTML;
// Beginning group of pages: $n1...$n2
$n1 = 1;
$n2 = min($_count_out, $_total_pages);

// Ending group of pages: $n7...$n8
$n7 = max(1, $_total_pages - $_count_out + 1);
$n8 = $_total_pages;

// Middle group of pages: $n4...$n5
$n4 = max($n2 + 1, $_current - $_count_in);
$n5 = min($n7 - 1, $_current + $_count_in);
$use_middle = ($n5 + 1 >= $n4);

// Point $n3 between $n2 and $n4
$n3 = (int)(($n2 + $n4) / 2);
$use_n3 = ($use_middle && (($n4 - $n2) > 1));

// Point $n6 between $n5 and $n7
$n6 = (int)(($n5 + $n7) / 2);
$use_n6 = ($use_middle && (($n7 - $n5) > 1));

// Links to display as array(page => content)
$links = [];

// Generate links data in accordance with calculated numbers
for ($i = $n1; $i <= $n2; $i++) {
    $links[$i] = $i;
}
if ($use_n3) {
    $links[$n3] = '&hellip;';
}
for ($i = $n4; $i <= $n5; $i++) {
    $links[$i] = $i;
}
if ($use_n6) {
    $links[$n6] = '&hellip;';
}
for ($i = $n7; $i <= $n8; $i++) {
    $links[$i] = $i;
}

$tmp = $_start;
if (isset($_start)){
    $_previous = $_start - 1;
}
?>
<div class="pagination grid ">
    <?php if ($_navigation): ?>
        <?php if ($_previous != false): ?>
        <a href="<?php echo HTML::chars($page->url($_previous)) ?>" rel="prev"  class="pagination__item pagination__item--btn left">
            <svg>
                <use xlink:href="<?php echo HTML::media('assets/images/sprites/icons.svg#left-arrow');?>"></use>
            </svg>
        </a>
        <?php endif ?>
    <?php endif ?>
    <?php foreach ($links as $number => $content): ?>
        <?php if (isset($_start)): ?>
            <?php if($number >= $_start && $number < $_current):?>
                <a href="<?php echo HTML::chars($page->url($number)) ?>" class="pagination__item active" data-page="<?php echo $content ?>"><?php echo $content ?></a>
            <?php elseif($number === $_current):?>
                <a href="<?php echo HTML::chars($page->url($number)) ?>" class="pagination__item active" data-page="<?php echo $content ?>"><?php echo $content ?></a>
            <?php else:?>
                <a href="<?php echo HTML::chars($page->url($number)) ?>" class="pagination__item" data-page="<?php echo $content ?>"><?php echo $content ?></a>
            <?php endif; ?>
        <?php elseif($number === $_current):?>
            <a href="<?php echo HTML::chars($page->url($number)) ?>" class="pagination__item active" data-page="<?php echo $content ?>"><?php echo $content ?></a>
        <?php else:?>
            <a href="<?php echo HTML::chars($page->url($number)) ?>" class="pagination__item" data-page="<?php echo $content ?>"><?php echo $content ?></a>
        <?php endif ?>
    <?php
        if ($tmp >= $_start && $tmp < $_current){
            $tmp++;
        }
    ?>
    <?php endforeach ?>
    <?php if ($_navigation): ?>
        <?php if ($_next !== false): ?>
            <a href="<?php echo HTML::chars($page->url($_next)) ?>" rel="next" class="pagination__item pagination__item--btn right">
                <svg>
                    <use xlink:href="<?php echo HTML::media('assets/images/sprites/icons.svg#right-arrow');?>"></use>
                </svg>
            </a>
        <?php endif ?>
    <?php endif ?>

</div>