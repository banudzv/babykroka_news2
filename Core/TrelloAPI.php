<?php
namespace Core;

/**
 * Trello ® API v 1.0
 * @package Core\TrelloAPI
 * @author Artem Belyi <belyi.a@wezom.com.ua>
 * @link   https://developers.trello.com/reference
 * @license The MIT License (MIT)
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * This Source Code Form is “Incompatible With Secondary Licenses”,
 * as defined by the Mozilla Public License, v. 2.0.
 * Go to https://trello.com/1/appKey/generate to get your API and OAuth keys
 *
 * 5cde9d5e968d3d362136dc3d - SupportHelp Board
 * 5cde9d6aeec856480c1e1362 - Входящие карточка
 * secret 6c67b6130ff91f58590a6b546ef74f83
 * token 1b79fa4601edfe97d7155886ba75e9bbd64121fe5dfe257415832872211d4707
 */

class TrelloAPI
{
	private $key;
	private $token;
	/**
	 * Trello API Version
	 */
	protected $apiVersion = 1;
	/**
	 * Trello API Endpoint
	 */
	protected $apiEndpoint = 'https://api.trello.com';
	/**
	 * Trello Auth endpoint
	 */
	protected $authEndpoint = 'https://trello.com';


	/**
	 * @var array  preferred attributes for Cards
	 */
	public static $query_params = [
		'name',
		'desc',
		'pos',
		'due',
		'dueComplete',
		'idMembers',
		'idLabels',
		'urlSource',
		'fileSource',
		'idCardCourse',
		'keepFromSource'
	];

	public function __construct($key, $token)
	{
		$this->key = $key;
		$this->token = $token;
	}

	private function args(): string
    {
		return '&key=' . $this->key . '&token=' . $this->token;
	}

	public function getBoards(){
		return $this->request('GET',('member/me/boards'));
	}

	/**
	 * Create a new card
	 * @param $idList - The ID of the list the card should be created in
	 * @param array $attributes
     * @link https://developers.trello.com/reference#cards-2
	 * @return mixed
	 */
	public function createCard($idList, $attributes = []){
		$query = $this->attributes($attributes);
		return $this->request('POST',('cards?idList='.$idList.$query));
	}

	public function request($type, $request, $args = false)
	{
		if (!$args) {
			$args = array();
		} elseif (!is_array($args)) {
			$args = array($args);
		}

		if (strstr($request, '?')) {
			$url = 'https://api.trello.com/1/' . $request . $this->args();
		} else {
			$url = 'https://api.trello.com/1/' . $request . $this->args();
		}
		$c = curl_init();
		curl_setopt($c, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
		curl_setopt($c, CURLOPT_HEADER, 0);
		curl_setopt($c, CURLOPT_VERBOSE, 0);
		curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($c, CURLOPT_URL, $url);
		if (count($args)) curl_setopt($c, CURLOPT_POSTFIELDS, http_build_query($args));
		switch ($type) {
			case 'POST':
				curl_setopt($c, CURLOPT_POST, 1);
				break;
			case 'PUT':
				curl_setopt($c, CURLOPT_PUT,1);
				break;
			case 'GET':
				curl_setopt($c, CURLOPT_HTTPGET, 1);
				break;
			default:
				curl_setopt($c, CURLOPT_CUSTOMREQUEST, $type);
		}

		$data = curl_exec($c);
		if (!$data) {
            return curl_error($c);
        }
		curl_close($c);

		return json_decode($data);
	}

    /**
     * Compiles an array of Query params into an attribute string.
     * Attributes will be sorted using HTML::$attribute_order for consistency.
     *
     * @param array|null $attributes attribute list
     * @return  string
     */
	private function attributes(array $attributes = null): string
    {
		if (empty($attributes))
			return '';

		$compiled = '';
		foreach ($attributes as $query => $param) {
			if ($param === null) {
				// Skip attributes that have NULL values
				continue;
			}

			if (is_int($query)) {
				// Assume non-associative keys are mirrored attributes
				$query = $param;
			}

			if ($query == 'desc')
				$param = urlencode($param);

			if (is_array($param)) {
				$param = array_unique($param);
				$param = implode(' ', $param);
			}

			// Add the attribute key
			$compiled .= '&'.$query;

			if ($param) {
				// Add the attribute value
				$compiled .= '=' . $this->chars($param);
			}
		}
		return $compiled;
	}

	/**
	 * Convert special characters to HTML entities. All untrusted content
	 * should be passed through this method to prevent XSS injections.
	 *
	 * @param   string $value string to convert
	 * @param   boolean $double_encode encode existing entities
	 * @return  string
	 */
	private function chars($value, $double_encode = true): string
    {
		return htmlspecialchars((string)$value, ENT_QUOTES, 'UTF-8', $double_encode);
	}
}
