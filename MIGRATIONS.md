Основные этапы миграций
------------------------------------------
###### **Важно:** версия PHP в _системе_ должна быть 7.2 или выше.
###Установка
- `composer i`
- `vendor/bin/phinx init`

В файле phinx.yml указать соединение к базе, по-умолчанию используется среда development. Для подключения к локальной базе данных указывать вместо localhost -> 127.0.0.1

###Создание миграций
(https://book.cakephp.org/phinx/0/en/migrations.html)
 
`vendor/bin/phinx create MyNewMigration`

###Запуск миграций

`vendor/bin/phinx migrate` 
