'use strict';

/**
 * Метод составления справочника хлебных крошек
 * @module
 */

// ----------------------------------------
// Public
// ----------------------------------------

/**
 * @memberOf frontendComponents
 * @param {app} app
 * @requires app.data.views
 * @returns {Array}
 */
function breadcrumbs (app) {
	const {views} = app.data;

	/**
	 * Справочник хлебных крошек
	 * Ключ каждого свойство - имя страницы к которой Вы можете обращатся
	 * Значение каждого свойства - массив объектов, который представляет собой путь к странице
	 *
	 * Вы можете составить любую последовательность
	 * с любым наполнением и контентом на свое усмотрение
	 *
	 * Каждый элемент массива должен иметь значение title для текста
	 * а также при необходимости href - для составления ссылки
	 *
	 * Упрощенный варинат набора данных - подставление с объектов views
	 * которые уже имеют значения `title` и `href`
	 *
	 * @private
	 * @type {Array}
	 */
	const list = {
		'faq': [
			{
				href: views.index.href,
				title: views.index.title
			}, {
				title: views.faq.title
			}
		],

		'catalog': [
			{
				href: views.index.href,
				title: views.index.title
			}, {
				title: 'Мальчикам'
			}
		],

		'catalog-with-filters': [
			{
				href: views.index.href,
				title: views.index.title
			}, {
				title: 'Мальчикам'
			}
		],

		'product-page': [
			{
				href: views.index.href,
				title: views.index.title
			}, {
				href: views.catalog.href,
				title: 'Мальчикам'
			}, {
				title: 'Костюм "классика"'
			}
		],

		'product-page-wholesale': [
			{
				href: views.index.href,
				title: views.index.title
			}, {
				href: views.catalog.href,
				title: 'Мальчикам'
			}, {
				title: 'Костюм "классика"'
			}
		],

		'news': [
			{
				href: views.index.href,
				title: views.index.title
			}, {
				title: views.news.title
			}
		],

		'news-page': [
			{
				href: views.index.href,
				title: views.index.title
			}, {
				href: views.news.href,
				title: views.news.title
			}, {
				title: views['news-page'].title
			}
		],

		'about': [
			{
				href: views.index.href,
				title: views.index.title
			}, {
				title: views.about.title
			}
		],

		'wholesale': [
			{
				href: views.index.href,
				title: views.index.title
			}, {
				title: views.wholesale.title
			}
		],

		'profile-data': [
			{
				href: views.index.href,
				title: views.index.title
			}, {
				title: views['profile-data'].title
			}
		],

		'profile-favorite': [
			{
				href: views.index.href,
				title: views.index.title
			}, {
				title: views['profile-favorite'].title
			}
		],

		'profile-history': [
			{
				href: views.index.href,
				title: views.index.title
			}, {
				title: views['profile-history'].title
			}
		],

		'confirm-order': [
			{
				href: views.index.href,
				title: views.index.title
			}, {
				href: views.index.href,
				title: 'Мальчикам'
			}, {
				title: views['confirm-order'].title
			}
		],

		'cart': [
			{
				href: views.index.href,
				title: views.index.title
			}, {
				href: views.index.href,
				title: 'Мальчикам'
			}, {
				title: 'Корзина'
			}
		],

		'cart-info': [
			{
				href: views.index.href,
				title: views.index.title
			}, {
				href: views.index.href,
				title: 'Мальчикам'
			}, {
				title: 'Корзина'
			}
		],

		'cart-info-login': [
			{
				href: views.index.href,
				title: views.index.title
			}, {
				href: views.index.href,
				title: 'Мальчикам'
			}, {
				title: 'Корзина'
			}
		],

		'delivery-info': [
			{
				href: views.index.href,
				title: views.index.title
			}, {
				title: views['delivery-info'].title
			}
		],

		'contacts': [
			{
				href: views.index.href,
				title: views.index.title
			}, {
				title: views.contacts.title
			}
		],

		'ui': [
			{
				href: views.index.href, // если значение будет логически отрицательным - ссылки не будет
				title: views.index.title // или используем значение с views или можем указать свое строкой - 'Страница Bla-Bla-Bla'
			}, {
				title: views.ui.title
			}
		],

		'ui-svg': [
			views.index,
			views.ui,
			views['ui-svg'] // если элемент последний в списке - ссылки, так же, не будет (по умолчанию шаблона разметки)
		],

		'ui-forms': [
			views.index,
			views.ui,
			views['ui-forms']
		],

		'ui-wysiwyg': [
			views.index,
			views.ui,
			views['ui-wysiwyg']
		],

		'sitemap': [
			views.index,
			views.sitemap
		]
	};

	return list;
}

// ----------------------------------------
// Exports
// ----------------------------------------

module.exports = breadcrumbs;
