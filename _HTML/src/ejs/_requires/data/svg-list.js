'use strict';

// ----------------------------------------
// Public
// ----------------------------------------

/* eslint-disable quotes */

/**
 * Справочник по символам  SVG спрайта
 *
 * > ___ЭТОТ ФАЙЛ ГЕНЕРИРУЕТСЯ АВТОМАТИЧЕСКИ___
 * > ___НЕ РЕДАКТИРУЙТЕ ЕГО!!!___
 *
 * @type {Object}
 * @memberOf app.data
 * @sourceCode
 */
let svg = {
	"list": {
		"3d-icon": {
			"viewBox": "0 0 20 26",
			"width": "20",
			"height": "26"
		},
		"account": {
			"viewBox": "0 0 48 48",
			"width": "48",
			"height": "48"
		},
		"arrow-up": {
			"viewBox": "0 0 32 32",
			"width": "32",
			"height": "32"
		},
		"avent-icon": {
			"viewBox": "0 0 84 21.05",
			"width": "84",
			"height": "21.05"
		},
		"big-delivery-icon": {
			"viewBox": "0 0 157.6 75.9",
			"width": "157.6",
			"height": "75.9"
		},
		"box-icon": {
			"viewBox": "0 0 68.01 46.68",
			"width": "68.01",
			"height": "46.68"
		},
		"box-icon-2": {
			"viewBox": "0 0 18 18.99",
			"width": "18",
			"height": "18.99"
		},
		"calculation-icon": {
			"viewBox": "0 0 262 277",
			"width": "262",
			"height": "277"
		},
		"canpol-icon": {
			"viewBox": "0 0 74.75 41.75",
			"width": "74.75",
			"height": "41.75"
		},
		"cart-icon": {
			"viewBox": "0 0 77 77",
			"width": "77",
			"height": "77"
		},
		"check-icon": {
			"viewBox": "0 0 14 11",
			"width": "14",
			"height": "11"
		},
		"close-icon": {
			"viewBox": "0 0 47.97 47.97",
			"width": "47.97",
			"height": "47.97"
		},
		"close-icon-fat": {
			"viewBox": "0 0 8 8",
			"width": "8",
			"height": "8"
		},
		"close-icon-thin": {
			"viewBox": "0 0 22.41 22.41",
			"width": "22.41",
			"height": "22.41"
		},
		"coat-icon": {
			"viewBox": "0 0 136.52 96.4",
			"width": "136.52",
			"height": "96.4"
		},
		"compare": {
			"viewBox": "0 0 77 77",
			"width": "77",
			"height": "77"
		},
		"credit-icon": {
			"viewBox": "0 0 133.1 85.6",
			"width": "133.1",
			"height": "85.6"
		},
		"delivery-icon": {
			"viewBox": "0 0 30 18.83",
			"width": "30",
			"height": "18.83"
		},
		"delivery-icon-new": {
			"viewBox": "0 0 100 59",
			"width": "100",
			"height": "59"
		},
		"diler-icon": {
			"viewBox": "0 0 79.89 38.64",
			"width": "79.89",
			"height": "38.64"
		},
		"fabric-icon": {
			"viewBox": "0 0 78.32 40.02",
			"width": "78.32",
			"height": "40.02"
		},
		"fav-btn": {
			"viewBox": "0 0 32 28",
			"width": "32",
			"height": "28"
		},
		"favorite-icon": {
			"viewBox": "0 0 477.534 477.534",
			"width": "477.534",
			"height": "477.534"
		},
		"fb-icon": {
			"viewBox": "0 0 39.98 76.98",
			"width": "39.98",
			"height": "76.98"
		},
		"filter-mobile-icon": {
			"viewBox": "0 0 18 18",
			"width": "18",
			"height": "18"
		},
		"garbage-icon": {
			"viewBox": "0 0 48 59",
			"width": "48",
			"height": "59"
		},
		"google-icon": {
			"viewBox": "0 0 20 13",
			"width": "20",
			"height": "13"
		},
		"hand-down-icon": {
			"viewBox": "0 0 12 13",
			"width": "12",
			"height": "13"
		},
		"hand-icon": {
			"viewBox": "0 0 12 13",
			"width": "12",
			"height": "13"
		},
		"icon-wezom": {
			"viewBox": "0 0 26 11",
			"width": "26",
			"height": "11"
		},
		"icon-youtube-play": {
			"viewBox": "0 0 100 100",
			"width": "100",
			"height": "100"
		},
		"insta-icon": {
			"viewBox": "0 0 14 14",
			"width": "14",
			"height": "14"
		},
		"kroha": {
			"viewBox": "0 0 1039.66 732.29",
			"width": "1039.66",
			"height": "732.29"
		},
		"left-arrow": {
			"viewBox": "0 0 10 6.99",
			"width": "10",
			"height": "6.99"
		},
		"left-arrow-slider": {
			"viewBox": "0 0 7 12.83",
			"width": "7",
			"height": "12.83"
		},
		"lens": {
			"viewBox": "0 0 509.158 509.158",
			"width": "509.158",
			"height": "509.158"
		},
		"like-icon": {
			"viewBox": "0 0 51.997 51.997",
			"width": "51.997",
			"height": "51.997"
		},
		"lock-icon": {
			"viewBox": "0 0 7 9",
			"width": "7",
			"height": "9"
		},
		"logo": {
			"viewBox": "0 0 105 74",
			"width": "105",
			"height": "74"
		},
		"menu-icon": {
			"viewBox": "0 0 20.03 11",
			"width": "20.03",
			"height": "11"
		},
		"pause-icon": {
			"viewBox": "0 0 15 42",
			"width": "15",
			"height": "42"
		},
		"pay-icon": {
			"viewBox": "0 0 19 19",
			"width": "19",
			"height": "19"
		},
		"payment-icon": {
			"viewBox": "0 0 80 80",
			"width": "80",
			"height": "80"
		},
		"pencil-icon": {
			"viewBox": "0 0 17 17",
			"width": "17",
			"height": "17"
		},
		"percentage-icon": {
			"viewBox": "0 0 20 18.99",
			"width": "20",
			"height": "18.99"
		},
		"phone": {
			"viewBox": "0 0 32.666 32.666",
			"width": "32.666",
			"height": "32.666"
		},
		"picture-icon": {
			"viewBox": "0 0 489.4 489.4",
			"width": "489.4",
			"height": "489.4"
		},
		"play-icon": {
			"viewBox": "0 0 31 42",
			"width": "31",
			"height": "42"
		},
		"price-icon": {
			"viewBox": "0 0 18 18",
			"width": "18",
			"height": "18"
		},
		"rating-star-icon": {
			"viewBox": "0 0 12 12",
			"width": "12",
			"height": "12"
		},
		"return-icon": {
			"viewBox": "0 0 80 65",
			"width": "80",
			"height": "65"
		},
		"right-arrow": {
			"viewBox": "0 0 10 7",
			"width": "10",
			"height": "7"
		},
		"right-arrow-slider": {
			"viewBox": "0 0 7 12.83",
			"width": "7",
			"height": "12.83"
		},
		"search-icon": {
			"viewBox": "0 0 22 22",
			"width": "22",
			"height": "22"
		},
		"shopping-cart-icon": {
			"viewBox": "0 0 26 26",
			"width": "26",
			"height": "26"
		},
		"table-icon": {
			"viewBox": "0 0 14 14",
			"width": "14",
			"height": "14"
		},
		"text": {
			"viewBox": "0 0 94 20",
			"width": "94",
			"height": "20"
		},
		"tshirt-icon": {
			"viewBox": "0 0 129.64 109.1",
			"width": "129.64",
			"height": "109.1"
		},
		"twitter-icon": {
			"viewBox": "0 0 21.28 17.29",
			"width": "21.28",
			"height": "17.29"
		}
	},
	"hasGradients": false
};

// ----------------------------------------
// Exports
// ----------------------------------------

module.exports = svg;
