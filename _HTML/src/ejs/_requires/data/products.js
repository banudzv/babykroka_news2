'use strict';

// ----------------------------------------
// Public
// ----------------------------------------

/** News
 *
 * @type {string}
 * @memberOf app.data
 * @sourceCode
 */

function wrapper (app) {
	let products = {
		new: [
			{
				label: 'Новинка',
				labelColor: 'yellow',
				name: 'Комбинезон',
				price: '420',
				category: 'Одежда для мальчиков',
				img: 'Carters-Boys-3-24-Months-2-Piece-Overall-Set.png',
				btnTitle: 'Сформировать заказ'
			}, {
				label: 'Новинка',
				labelColor: 'yellow',
				name: 'Комбинезон',
				price: '420',
				category: 'Одежда для мальчиков',
				img: 'Carters-Boys-3-24-Months-2-Piece-Overall-Set.png',
				btnTitle: 'Сформировать заказ'
			}, {
				label: 'Новинка',
				labelColor: 'yellow',
				name: 'Комбинезон',
				price: '420',
				category: 'Одежда для мальчиков',
				img: 'Carters-Boys-3-24-Months-2-Piece-Overall-Set.png',
				btnTitle: 'Сформировать заказ'
			}, {
				label: 'Новинка',
				labelColor: 'yellow',
				name: 'Комбинезон',
				price: '420',
				category: 'Одежда для мальчиков',
				img: 'Carters-Boys-3-24-Months-2-Piece-Overall-Set.png',
				btnTitle: 'Сформировать заказ'
			}, {
				label: 'Новинка',
				labelColor: 'yellow',
				name: 'Комбинезон',
				price: '420',
				category: 'Одежда для мальчиков',
				img: 'Carters-Boys-3-24-Months-2-Piece-Overall-Set.png',
				btnTitle: 'Сформировать заказ'
			}, {
				label: 'Новинка',
				labelColor: 'yellow',
				name: 'Комбинезон',
				price: '420',
				category: 'Одежда для мальчиков',
				img: 'Carters-Boys-3-24-Months-2-Piece-Overall-Set.png',
				btnTitle: 'Сформировать заказ'
			}, {
				label: 'Новинка',
				labelColor: 'yellow',
				name: 'Комбинезон',
				price: '420',
				category: 'Одежда для мальчиков',
				img: 'Carters-Boys-3-24-Months-2-Piece-Overall-Set.png',
				btnTitle: 'Сформировать заказ'
			}, {
				label: 'Новинка',
				labelColor: 'yellow',
				name: 'Комбинезон',
				price: '420',
				category: 'Одежда для мальчиков',
				img: 'Carters-Boys-3-24-Months-2-Piece-Overall-Set.png',
				btnTitle: 'Сформировать заказ'
			}
		],
		sales: [
			{
				label: 'Скидка',
				labelColor: 'red',
				name: 'Ползунки',
				price: '500',
				oldPrice: '630',
				category: 'Одежда для мальчиков',
				img: 'baby clothes.png',
				btnTitle: 'Стать оптовиком'
			}, {
				label: 'Скидка',
				labelColor: 'red',
				name: 'Ползунки',
				price: '500',
				oldPrice: '630',
				category: 'Одежда для мальчиков',
				img: 'baby clothes.png',
				btnTitle: 'Стать оптовиком'
			}, {
				label: 'Скидка',
				labelColor: 'red',
				name: 'Ползунки',
				price: '500',
				oldPrice: '630',
				category: 'Одежда для мальчиков',
				img: 'baby clothes.png',
				btnTitle: 'Стать оптовиком'
			}, {
				label: 'Скидка',
				labelColor: 'red',
				name: 'Ползунки',
				price: '500',
				oldPrice: '630',
				category: 'Одежда для мальчиков',
				img: 'baby clothes.png',
				btnTitle: 'Стать оптовиком'
			}, {
				label: 'Скидка',
				labelColor: 'red',
				name: 'Ползунки',
				price: '500',
				oldPrice: '630',
				category: 'Одежда для мальчиков',
				img: 'baby clothes.png',
				btnTitle: 'Стать оптовиком'
			}
		],
		looked: [
			{
				name: 'Плюшевая игрушка',
				price: '630',
				category: 'Игрушки',
				img: '42182_-_kopiya.png',
				btnTitle: 'Стать оптовиком'
			}, {
				name: 'Плюшевая игрушка',
				price: '630',
				category: 'Игрушки',
				img: '42182_-_kopiya.png',
				btnTitle: 'Стать оптовиком'
			}, {
				name: 'Плюшевая игрушка',
				price: '630',
				category: 'Игрушки',
				img: '42182_-_kopiya.png',
				btnTitle: 'Стать оптовиком'
			}
		],
		favorite: [
			{
				label: 'Скидка',
				labelColor: 'red',
				name: 'Ползунки',
				price: '500',
				oldPrice: '630',
				category: 'Одежда для мальчиков',
				img: 'baby clothes.png',
				btnTitle: 'Стать оптовиком',
				favorite: true
			}, {
				label: 'Скидка',
				labelColor: 'red',
				name: 'Ползунки',
				price: '500',
				oldPrice: '630',
				category: 'Одежда для мальчиков',
				img: 'baby clothes.png',
				btnTitle: 'Стать оптовиком',
				favorite: true
			}, {
				label: 'Скидка',
				labelColor: 'red',
				name: 'Ползунки',
				price: '500',
				oldPrice: '630',
				category: 'Одежда для мальчиков',
				img: 'baby clothes.png',
				btnTitle: 'Стать оптовиком',
				favorite: true
			}, {
				name: 'Плюшевая игрушка',
				price: '630',
				category: 'Игрушки',
				img: '42182_-_kopiya.png',
				btnTitle: 'Стать оптовиком',
				favorite: true
			}, {
				name: 'Плюшевая игрушка',
				price: '630',
				category: 'Игрушки',
				img: '42182_-_kopiya.png',
				btnTitle: 'Стать оптовиком',
				favorite: true
			}
		]
	};

	return products;
}

// ----------------------------------------
// Exports
// ----------------------------------------

module.exports = wrapper;
