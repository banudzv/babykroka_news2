'use strict';

// ----------------------------------------
// Public
// ----------------------------------------

/**
 * [FW] Обертка
 * @param {Object} app
 * @returns {Object}
 * @private
 */
function wrapper (app) {
	const {View} = app.data;

	/**
	 * Флаг быстрого отключения критикал стилей для всех страниц
	 * @const {boolean}
	 * @private
	 */
	const disableCriticalCSSByViews = false;

	/**
	 * Справочник по основным страницам `ejs` ренедера
	 * @type {Object}
	 * @memberOf app.data
	 * @sourceCode
	 */
	let views = {
		'index': new View('index', {
			title: 'Главная страница',
			criticalCSS: [
				// 'assets/css/critical/generated/index.css'
			]
		}),

		'news': new View('news', {
			title: 'Наши новости',
			criticalCSS: [
				// 'assets/css/critical/generated/news.css'
			]
		}),

		'news-page': new View('news-page', {
			title: 'Наша новость',
			criticalCSS: [
				// 'assets/css/critical/generated/news-page.css'
			]
		}),

		'about': new View('about', {
			title: 'О компании',
			criticalCSS: [
				// 'assets/css/critical/generated/about.css'
			]
		}),

		'wholesale': new View('wholesale', {
			title: 'Оптовикам',
			criticalCSS: [
				// 'assets/css/critical/generated/wholesale.css'
			]
		}),

		'profile-data': new View('profile-data', {
			title: 'Личный кабинет',
			criticalCSS: [
				// 'assets/css/critical/generated/profile-data.css'
			]
		}),

		'profile-favorite': new View('profile-favorite', {
			title: 'Личный кабинет',
			criticalCSS: [
				// 'assets/css/critical/generated/profile-favorite.css'
			]
		}),

		'profile-history': new View('profile-history', {
			title: 'Личный кабинет',
			criticalCSS: [
				// 'assets/css/critical/generated/profile-history.css'
			]
		}),

		'confirm-order': new View('confirm-order', {
			title: 'Корзина',
			criticalCSS: [
				// 'assets/css/critical/generated/confirm-order.css'
			]
		}),

		'faq': new View('faq', {
			title: 'Помощь',
			criticalCSS: [
				// 'assets/css/critical/generated/faq.css'
			]
		}),

		'delivery-info': new View('delivery-info', {
			title: 'Доставка и оплата',
			criticalCSS: [
				// 'assets/css/critical/generated/delivery-info.css'
			]
		}),

		'contacts': new View('contacts', {
			title: 'Контакты',
			criticalCSS: [
				// 'assets/css/critical/generated/contacts.css'
			]
		}),

		'cart': new View('cart', {
			title: 'Корзина',
			criticalCSS: [
				// 'assets/css/critical/generated/cart.css'
			]
		}),

		'cart-info': new View('cart-info', {
			title: 'Оформление заказа',
			criticalCSS: [
				// 'assets/css/critical/generated/cart-info.css'
			]
		}),

		'cart-info-login': new View('cart-info-login', {
			title: 'Оформление заказа для зарегестрированного пользователя',
			criticalCSS: [
				// 'assets/css/critical/generated/cart-info-login.css'
			]
		}),

		'catalog': new View('catalog', {
			title: 'Каталог товаров',
			criticalCSS: [
				// 'assets/css/critical/generated/catalog.css'
			]
		}),

		'catalog-with-filters': new View('catalog-with-filters', {
			title: 'Каталог товаров(с фильтрами)',
			criticalCSS: [
				// 'assets/css/critical/generated/catalog-with-filters.css'
			]
		}),

		'product-page': new View('product-page', {
			title: 'Товар',
			criticalCSS: [
				// 'assets/css/critical/generated/product-page.css'
			]
		}),

		'product-page-wholesale': new View('product-page-wholesale', {
			title: 'Товар опт',
			criticalCSS: [
				// 'assets/css/critical/generated/product-page-wholesale.css'
			]
		}),

		'search-results': new View('search-results', {
			title: 'Результаты поиска',
			criticalCSS: [
				// 'assets/css/critical/generated/search-results.css'
			]
		}),

		'text-page': new View('text-page', {
			title: 'Текстовая страница',
			criticalCSS: [
				// 'assets/css/critical/generated/text-page.css'
			]
		}),

		'404': new View('404', {
			title: '404 Страница не найдена',
			criticalCSS: [
				// 'assets/css/critical/generated/404.css'
			]
		}),

		'sitemap': new View('sitemap', {
			title: 'Карта сайта',
			criticalCSS: [
				// 'assets/css/critical/generated/sitemap.css'
			]
		}),

		'ui': new View('ui', {
			title: 'UI Kit'
		}),

		'ui-svg': new View('ui-svg', {
			title: 'UI SVG Icons'
		}),

		'ui-wysiwyg': new View('ui-wysiwyg', {
			title: 'UI Типография'
		}),

		'ui-forms': new View('ui-forms', {
			title: 'UI Формы'
		})
	};

	if (disableCriticalCSSByViews) {
		for (let key in views) {
			views[key].criticalCSS = [];
		}
	}

	return views;
}

// ----------------------------------------
// Exports
// ----------------------------------------

module.exports = wrapper;
