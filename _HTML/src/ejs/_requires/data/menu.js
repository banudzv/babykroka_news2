'use strict';

// ----------------------------------------
// Public
// ----------------------------------------

/** News
 *
 * @type {string}
 * @memberOf app.data
 * @sourceCode
 */

function wrapper (app) {
	let menu = {
		main: [
			{
				link: '#',
				title: 'Мальчикам',
				category: 'boys',
				img: 'boys-menu.png',
				child: true
			}, {
				link: '#',
				title: 'Девочкам',
				img: 'girls-menu.png',
				category: 'girls',
				child: true

			}, {
				link: '#',
				title: 'Новорожденным'
			}, {
				link: '#',
				title: 'Игрушки'
			}, {
				link: '#',
				title: 'Акции и скидки'
			}, {
				link: '#',
				title: 'Оптовикам'
			}
		],
		sidebar: [
			{
				link: '#',
				title: 'О компании'
			}, {
				link: '#',
				title: 'Наши статьи'
			}, {
				link: '#',
				title: 'Доставка и оплата'
			}, {
				link: '#',
				title: 'Помощь'
			}, {
				link: '#',
				title: 'Контакты'
			}
		],
		footer: [
			{
				link: '#',
				title: 'Контакты'
			}
		]
	};

	return menu;
}

// ----------------------------------------
// Exports
// ----------------------------------------

module.exports = wrapper;
