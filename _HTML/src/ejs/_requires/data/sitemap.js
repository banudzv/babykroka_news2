'use strict';

// ----------------------------------------
// Public
// ----------------------------------------

/**
 * [FW] Обертка
 * @param {Object} app
 * @returns {Array}
 * @private
 */
function wrapper (app) {
	const {views} = app.data;

	/**
	 * Составление карты сайта
	 * @type {Array.<Object>}
	 * @memberOf app.data
	 * @sourceCode
	 */
	let sitemap = [{
		href: views['index'].href,
		title: views['index'].title,
		children: [{
			href: views['catalog'].href,
			title: views['catalog'].title,
			children: [{
				href: views['product-page'].href,
				title: views['product-page'].title
			}, {
				href: views['product-page-wholesale'].href,
				title: views['product-page-wholesale'].title
			}]
		}, {
			href: views['news'].href,
			title: views['news'].title,
			children: [{
				href: views['news-page'].href,
				title: views['news-page'].title
			}]
		}, {
			href: views['cart'].href,
			title: views['cart'].title
		}, {
			href: views['cart-info'].href,
			title: views['cart-info'].title
		}, {
			href: views['cart-info-login'].href,
			title: views['cart-info-login'].title
		}, {
			href: views['confirm-order'].href,
			title: 'Успешное оформление заказа'
		}, {
			href: views['about'].href,
			title: views['about'].title
		}, {
			href: views['contacts'].href,
			title: views['contacts'].title
		}, {
			href: views['delivery-info'].href,
			title: views['delivery-info'].title
		}, {
			href: views['faq'].href,
			title: views['faq'].title
		}, {
			href: views['profile-data'].href,
			title: 'Личный кабинет - данные'
		}, {
			href: views['profile-favorite'].href,
			title: 'Личный кабинет - избранное'
		}, {
			href: views['profile-history'].href,
			title: 'Личный кабинет - история заказов'
		}, {
			href: views['search-results'].href,
			title: views['search-results'].title
		}, {
			href: views['404'].href,
			title: views['404'].title
		}, {
			href: views['wholesale'].href,
			title: views['wholesale'].title
		}]
	}];

	return sitemap;
}

// ----------------------------------------
// Exports
// ----------------------------------------

module.exports = wrapper;
