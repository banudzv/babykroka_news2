'use strict';

/**
 * Прослойка для загрузки модуля `scroll-window`
 * @module
 */

// ----------------------------------------
// Imports
// ----------------------------------------

import scrollWindow from '#/_modules/scroll-window';

// ----------------------------------------
// Public
// ----------------------------------------

/**
 * @param {JQuery} $elements
 * @param {ModuleLoader} moduleLoader
 */
function loaderInit ($elements, moduleLoader) {
	scrollWindow($elements);
}

// ----------------------------------------
// Exports
// ----------------------------------------

export {loaderInit};
