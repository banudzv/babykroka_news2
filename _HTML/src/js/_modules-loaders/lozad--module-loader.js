'use strict';

/**
 * Прослойка для загрузки модуля `lozad`
 * @module
 */

// ----------------------------------------
// Imports
// ----------------------------------------

import lozadLoad from '#/_modules/lozad-load';

// ----------------------------------------
// Public
// ----------------------------------------

/**
 * @param {JQuery} $elements
 * @param {ModuleLoader} moduleLoader
 */
function loaderInit ($elements, moduleLoader) {
	lozadLoad($elements);
}

// ----------------------------------------
// Exports
// ----------------------------------------

export {loaderInit};
