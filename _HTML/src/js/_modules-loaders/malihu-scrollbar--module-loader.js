'use strict';

/**
 * Прослойка для загрузки модуля `malihu-custom-scrollbar`
 * @module
 */

// ----------------------------------------
// Imports
// ----------------------------------------

import 'malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min';

// ----------------------------------------
// Public
// ----------------------------------------

/**
 * @param {JQuery} $elements
 * @param {ModuleLoader} moduleLoader
 */
function loaderInit ($elements, moduleLoader) {
	$elements.each(function (index) {
		let $this = $(this);

		if ($this.hasClass('mCustomScrollbar')) {
			$this.mCustomScrollbar('destroy');
		}

		$this.mCustomScrollbar({
			axis: $this.data('axis') || 'y',
			mouseWheel: {
				enable: true,
				mouseWheelPixels: 30,
				scrollInertia: 300
			}
		});
	});
}

// ----------------------------------------
// Exports
// ----------------------------------------

export {loaderInit};
