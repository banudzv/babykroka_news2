'use strict';

/**
 * @module
 */

// ----------------------------------------
// Imports
// ----------------------------------------

import ModuleLoader from 'wezom-module-loader';

// ----------------------------------------
// Public
// ----------------------------------------

const moduleLoader = new ModuleLoader({
	debug: IS_DEVELOP,
	importPromise: moduleName => import('./_modules-loaders/' + moduleName),
	initSelector: '.js-init',
	initFunctionName: 'loaderInit',
	loadingClass: '_module-loading',
	loadedClass: '_module-loaded',
	list: {
		/* eslint-disable key-spacing */
		'jquery-validation--module-loader': 'form',
		'inputmask--module-loader':         '[data-phonemask]',
		'magnific-popup--module-loader':    '[data-mfp]',
		'wrap-media--module-loader':        '[data-wrap-media]',
		'prismjs--module-loader':           '[data-prismjs]',
		'lozad--module-loader':             ['[data-lozad]', 'picture'],
		'scroll-window--module-loader':     '[data-scroll-window]',
		'draggable-table--module-loader':   '[data-draggable-table]',
		'malihu-scrollbar--module-loader':   '[data-scrollbar]'
	}
});

// ----------------------------------------
// Exports
// ----------------------------------------

export default moduleLoader;
