'use strict';

/**
 *
 * @module
 */

// ----------------------------------------
// Imports
// ----------------------------------------

// ----------------------------------------
// Private
// ----------------------------------------

/**
 * @param {JQuery} $element
 * @return {string}
 * @private
 */

// ----------------------------------------
// Public
// ----------------------------------------

/**
 * @param {JQuery} $elements
 */
function productsDownload () {
	$('.js-product-show').on('click', function (e) {
		let $this = $(this),
			id = $this.data('id'),
			url = $('.export__checkers').data('url'),
			sort = $('#sort').val(),
			amount = $('#amount').val(),
			availability = $('.export__availability input').prop('checked'),
			checkeredProducts = [],
			select = null,
			range = [],
			lengthAll = $this.closest('li').find('.export__checkers input').length,
			lengthChecketed = $this.closest('li').find('.export__checkers input:checked').length;

		if ($this.next().find('input').is(':checked')) {
			// console.log('Чекнутая категория');
			select = null;

			if (lengthAll == lengthChecketed) {
				select = id;
			}
		} else {
			// console.log('Не чекнутая категория');

			select = null;
			if ($this.closest('li').find('.export__checkers').length) {
				// console.log('есть товар');
				if (lengthAll == lengthChecketed) {
					// console.log('все выбрано');
					// select = id;
				} else {
					// console.log('ничего не выбрано');
					// select = null
				}
			} else {
				// console.log('нет товров');
				select = null;
			}
		}

		// console.log(select);

		if ($(this).hasClass('opened')) {
			$this.parent().find('.export__checkers input:checked').each(function () {
				checkeredProducts.push($(this).data('id'));
			});

			$('.export__slider--client').each(function () {
				let name = $(this).find('input').attr('id'),
					value = $(this).find('input').val();

				range.push([name, value])
			})

			$.ajax({
				url: url,
				type: 'post',
				data: {
					category_id: id,
					sort: sort,
					availability: availability,
					amount: amount,
					select: select,
					checkeredProducts: checkeredProducts,
					range: range
				},
				dataType: 'json',
				success: function (response) {
					if (response.success) {
						if ($this.closest('li').find('.export__checkers').length) {
							$this.closest('li').find('.export__checkers').replaceWith(response.html)
						} else {
							$this.closest('li').append(response.html)
						}
					}
				}
			});
		}
	})
}

// ----------------------------------------
// Exports
// ----------------------------------------

export default productsDownload;
