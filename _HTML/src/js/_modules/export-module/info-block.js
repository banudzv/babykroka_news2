'use strict';

/**
 *
 * @module
 */

// ----------------------------------------
// Imports
// ----------------------------------------

// ----------------------------------------
// Private
// ----------------------------------------

/**
 * @param {JQuery} $element
 * @return {string}
 * @private
 */

// ----------------------------------------
// Public
// ----------------------------------------

/**
 * @param {JQuery} $elements
 */
function infoBlock () {
	$('.export__info-icon').on('click', function (e) {
		let $this = $(this);

		$('.export__info-icon').next().hide();
		$this.next().toggle();
	})

	$('body').on('click', function (e) {
		let div = $('.export__info-icon')

		if (!div.is(e.target) && div.has(e.target).length === 0) {
			div.next().hide();
		}
	})
}

// ----------------------------------------
// Exports
// ----------------------------------------

export default infoBlock;
