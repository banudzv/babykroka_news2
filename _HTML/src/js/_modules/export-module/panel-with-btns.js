'use strict';

/**
 *
 * @module
 */

// ----------------------------------------
// Imports
// ----------------------------------------

// ----------------------------------------
// Private
// ----------------------------------------

/**
 * @param {JQuery} $element
 * @return {string}
 * @private
 */

// ----------------------------------------
// Public
// ----------------------------------------

/**
 * @param {JQuery} $elements
 */
function panelWithBtns () {
	$('.js-set-checkers').on('click', function () {
		let action = $(this).data('set-checkers'),
			url = $('.js-click-product-categories').data('click');

		function setCheckers(status) {
			$(".export__checkers input[type='checkbox']").each(function () {
				$(this).prop('checked', status)
			})
		}

		function toggleCheckers(status) {
			$("input[type='checkbox']:not(:checked)").each(function () {
				$(this).closest('li').css('display', status);
			})
		}

		switch (action) {
			case 'all':
				setCheckers(true)

				let checkeredEmptyCategories = [],
					sort = $('#sort').val(),
					availability = $('.export__availability input').prop('checked'),
					amount = $('#amount').val();

				$('.js-product-show').each(function () {
					checkeredEmptyCategories.push($(this).next().find('input').data('id'));
				})

				$.ajax({
					url: url,
					type: 'post',
					data: {
						sort: sort,
						availability: availability,
						amount: amount,
						checkeredEmptyCategories: checkeredEmptyCategories
					},
					dataType: 'json'
				});

				break;
			case 'none':
				setCheckers(false)

				$.ajax({
					url: url,
					type: 'post',
					data: {
						dellAll: true,
					},
					dataType: 'json'
				});

				break;
			case 'onlyCheck':
				toggleCheckers('none');
				break;
			default:
				toggleCheckers('block');
		}
	})
}

// ----------------------------------------
// Exports
// ----------------------------------------

export default panelWithBtns;
