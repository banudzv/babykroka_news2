'use strict';

/**
 *
 * @module
 */

// ----------------------------------------
// Imports
// ----------------------------------------

// ----------------------------------------
// Private
// ----------------------------------------

/**
 * @param {JQuery} $element
 * @return {string}
 * @private
 */

// ----------------------------------------
// Public
// ----------------------------------------

/**
 * @param {JQuery} $elements
 */
function save () {
	$('.js-saveXML').on('click', function (e) {
		let url = $(this).data('url'),
			promId = $(this).attr('data-promid'),
			shopName = $('input[name="shopName"]').val(),
			companyName = $('input[name="companyName"]').val(),
			availability = $('.export__availability input').prop('checked'),
			checkeredEmptyCategories = [],
			products = [],
			range = [];

		if (shopName.length > 0 && companyName.length > 0) {
			e.preventDefault();

			$('.js-product-show:not(.opened)').each(function () {
				let $this = $(this);

				if ($this.parent().children('.export__checkers').length && $this.next().find('input').is(":checked")) {
					checkeredEmptyCategories.push($this.data('id'));
				}
			})

			$('.export__table-body').each(function () {
				$(this).find('input:checked').each(function () {
					let id = $(this).data('id'),
						newPrice = $(this).closest('.export__table-body').find('.gcell--3[data-newprice]').attr('data-newprice');

					products.push([id, newPrice]);
				})
			});

			$('.export__slider--client').each(function () {
				let name = $(this).find('input').attr('id'),
					value = $(this).find('input').val();

				range.push([name, value])
			})


			$.ajax({
				url: url,
				type: 'post',
				data: {
					shopName: shopName,
					promId: promId,
					availability: availability,
					range: range,
					companyName: companyName,
					checkeredEmptyCategories: checkeredEmptyCategories,
					products: products,
				},
				dataType: 'json',
				beforeSend: function () {
					$('.preloader.l-wrapper').css('display', 'flex');
				},
				success: function (response) {
					if (response.success) {
						$('.preloader.l-wrapper').hide();

						$('.export__saved-noty').show("slow")
						setTimeout(function () {
							$('.export__saved-noty').hide("slow")
						}, 3000)

						setTimeout(function () {
							location.reload();
						}, 2500)
					}
				}
			});
		}
	});
}

// ----------------------------------------
// Exports
// ----------------------------------------

export default save;
