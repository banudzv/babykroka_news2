'use strict';

/**
 *
 * @module
 */

// ----------------------------------------
// Imports
// ----------------------------------------

// ----------------------------------------
// Private
// ----------------------------------------

/**
 * @param {JQuery} $element
 * @return {string}
 * @private
 */

// ----------------------------------------
// Public
// ----------------------------------------

/**
 * @param {JQuery} $elements
 */
function listToggle () {
	$('.checkbox__icon').on('click', function (e) {
		let icon = $(this);

		if (icon.hasClass('checkbox__icon--main-category opened')) {
			icon.toggleClass('opened').closest('li').find('.export__checkers').addClass('is-hide').each(function () {
				$(this).find('.checkbox__icon.opened').removeClass('opened')
			})
		} else {
			icon.toggleClass('opened').parent().children('ul').toggleClass('is-hide');
		}
	})
}

// ----------------------------------------
// Exports
// ----------------------------------------

export default listToggle;
