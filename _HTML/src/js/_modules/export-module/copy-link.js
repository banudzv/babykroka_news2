'use strict';

/**
 *
 * @module
 */

// ----------------------------------------
// Imports
// ----------------------------------------

// ----------------------------------------
// Private
// ----------------------------------------

/**
 * @param {JQuery} $element
 * @return {string}
 * @private
 */

// ----------------------------------------
// Public
// ----------------------------------------

/**
 * @param {JQuery} $elements
 */
function copyLink () {
	$('.js-copy-url').on('click', function () {
		var $temp = $("<input>");
		$("body").append($temp);
		$temp.val($('input[name="url-for-file"]').val()).select();
		document.execCommand("copy");
		$temp.remove();
	})
}

// ----------------------------------------
// Exports
// ----------------------------------------

export default copyLink;
