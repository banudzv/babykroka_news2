'use strict';

/**
 *
 * @module
 */

// ----------------------------------------
// Imports
// ----------------------------------------

// ----------------------------------------
// Private
// ----------------------------------------

/**
 * @param {JQuery} $element
 * @return {string}
 * @private
 */

// ----------------------------------------
// Public
// ----------------------------------------

/**
 * @param {JQuery} $elements
 */
function priceFromTo () {
	if ($('.export__slider').length) {
		let amountSlider = $('#amount'),
			sliderRange = $("#slider-range"),
			min = amountSlider.data('min'),
			max = amountSlider.data('max'),
			currency = $('input[data-currency]').data('currency');

		sliderRange.slider({
			range: true,
			min: min,
			max: max,
			values: [min, max],
			slide: function (event, ui) {
				$("#amount").val(ui.values[0] + ' ' + currency + " - " + ui.values[1] + ' ' + currency);
			}
		});

		amountSlider.val(sliderRange.slider("values", 0) + ' ' + currency +
			" - " + sliderRange.slider("values", 1) + ' ' +
			currency);
	}
}

// ----------------------------------------
// Exports
// ----------------------------------------

export default priceFromTo;
