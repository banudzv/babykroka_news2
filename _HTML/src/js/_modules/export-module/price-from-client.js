'use strict';

/**
 *
 * @module
 */

// ----------------------------------------
// Imports
// ----------------------------------------

// ----------------------------------------
// Private
// ----------------------------------------

/**
 * @param {JQuery} $element
 * @return {string}
 * @private
 */

// ----------------------------------------
// Public
// ----------------------------------------

/**
 * @param {JQuery} $elements
 */
function priceFromClient () {
	if ($('.export__slider--client').length) {
		$('.export__slider--client').each(function () {
			let $this = $(this),
				name = $this.find('input').attr('id'),
				min = $('#' + name).data('min'),
				max = $('#' + name).data('max'),
				percent = $this.data('percent') || 0;

			$("#slider-range" + name).slider({
				min: min,
				max: max,
				values: [percent],
				slide: function (event, ui) {
					$("#" + name).val(ui.values[0] + "%");
				}
			});

			$("#" + name).val(percent + '%')
		})
	}
}

// ----------------------------------------
// Exports
// ----------------------------------------

export default priceFromClient;
