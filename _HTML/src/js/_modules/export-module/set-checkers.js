'use strict';

/**
 *
 * @module
 */

// ----------------------------------------
// Imports
// ----------------------------------------

// ----------------------------------------
// Private
// ----------------------------------------

/**
 * @param {JQuery} $element
 * @return {string}
 * @private
 */

// ----------------------------------------
// Public
// ----------------------------------------

/**
 * @param {JQuery} $elements
 */
function setCheckers () {
	jQuery.fn.reverse = [].reverse;

	$("body").on('click', 'input:checkbox', function () {
		let $this = $(this),
			isChecked = $this.is(":checked"),
			url = $('.js-click-product-categories').data('click'),
			categoriesId = [],
			sort = $('#sort').val(),
			availability = $('.export__availability input').prop('checked'),
			amount = $('#amount').val();

		$this.parent().next().find("input:checkbox").prop("checked", isChecked);

		$this.parents("ul").prev("a").find("input:checkbox").reverse().each(function (a, b) {
			$(b).prop("checked", function () {
				return $(b).parent("a").next("ul").find(":checked").length;
			});
		});

		// Отправка id категории при checketed
		if ($this.is(':checked')) {
			$this.closest('li').find('.js-product-show').each(function () {
				categoriesId.push($(this).data('id'))
			})

			$.ajax({
				url: url,
				type: 'post',
				data: {
					categoriesId: categoriesId,
					sort: sort,
					availability: availability,
					amount: amount,
					checkedCategories: true
				},
				dataType: 'json'
			});
		} else {
			$this.closest('li').find('.js-product-show').each(function () {
				categoriesId.push($(this).data('id'))
			})

			$.ajax({
				url: url,
				type: 'post',
				data: {
					categoriesId: categoriesId,
					checkedCategories: false
				},
				dataType: 'json'
			});
		}
	});

	// Отправка id товара
	$('body').on('click', '.js-product-in-table', function () {
		let $this = $(this),
			url = $('.js-click-product-categories').data('click'),
			productId = [],
			sort = $('#sort').val(),
			availability = $('.export__availability input').prop('checked'),
			amount = $('#amount').val();

		if ($this.is(':checked')) {
			productId.push($this.data('id'))

			$.ajax({
				url: url,
				type: 'post',
				data: {
					sort: sort,
					availability: availability,
					amount: amount,
					productId: productId,
					checkedProduct: true
				},
				dataType: 'json'
			});
		} else {
			productId.push($this.data('id'))

			$.ajax({
				url: url,
				type: 'post',
				data: {
					productId: productId,
					checkedProduct: false
				},
				dataType: 'json'
			});
		}
	})

	$("body").on('click', 'input:checkbox', function (e) {
		let $this = $(this),
			level = $this.closest('.nesting-level').data('level'),
			allInputs = $this.closest('.nesting-level').find('li input').length,
			checkedInputs = $this.closest('.nesting-level').find('li input:checked').length;

		if (level === 1) {
			if ($this.is(':checked')) {
				$this.attr('data-notAll', true).closest('.nesting-level[data-level="1"]').find('input').attr('data-notAll', true)
			}
		} else if (level === 2) {
			if (allInputs === checkedInputs) {
				$this.closest('.nesting-level[data-level="1"]').find('.checkbox__icon--main-category').next().find('input').attr('data-notAll', true)
			} else {
				$this.closest('.nesting-level[data-level="1"]').find('.checkbox__icon--main-category').next().find('input').attr('data-notAll', false)
			}
		} else if (level === 3) {
			if (allInputs === checkedInputs) {
				$this.closest('.nesting-level[data-level="3"]').prev().find('input').attr('data-notAll', true).closest('.nesting-level[data-level="1"]').find('.checkbox__icon--main-category').next().find('input').attr('data-notAll', true)
			} else {
				$this.closest('.nesting-level[data-level="3"]').prev().find('input').attr('data-notAll', false).closest('.nesting-level[data-level="1"]').find('.checkbox__icon--main-category').next().find('input').attr('data-notAll', false)
			}
		} else {
			if (allInputs === checkedInputs) {
				$this.closest('.nesting-level').prev().find('input').attr('data-notAll', true).closest('.nesting-level[data-level="1"]').find('.checkbox__icon--main-category').next().find('input').attr('data-notAll', true)
			} else {
				$this.closest('.nesting-level').prev().find('input').attr('data-notAll', false).closest('.nesting-level[data-level="1"]').find('.checkbox__icon--main-category').next().find('input').attr('data-notAll', false)
			}
		}
	})
}

// ----------------------------------------
// Exports
// ----------------------------------------

export default setCheckers;
