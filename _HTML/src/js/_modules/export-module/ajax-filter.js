'use strict';

/**
 *
 * @module
 */

// ----------------------------------------
// Imports
// ----------------------------------------

// ----------------------------------------
// Private
// ----------------------------------------

/**
 * @param {JQuery} $element
 * @return {string}
 * @private
 */

// ----------------------------------------
// Public
// ----------------------------------------

/**
 * @param {JQuery} $elements
 */
function ajaxFilter () {
	$('.js-set-filter').on('click', function () {
		let url = $(this).data('url'),
			sort = $('#sort').val(),
			amount = $('#amount').val(),
			availability = $('.export__availability input').prop('checked'),
			openedCategories = [],
			range = [],
			checkeredProducts = [];

		$('.js-product-show.opened').parent().find('.export__product-name input:checked').each(function () {
			checkeredProducts.push($(this).data('id'));
		});

		$('.js-product-show.opened').each(function () {
			openedCategories.push($(this).next().find('input').data('id'));
		});

		$('.export__slider--client').each(function () {
			let name = $(this).find('input').attr('id'),
				value = $(this).find('input').val();

			range.push([name, value])
		})

		$.ajax({
			url: url,
			type: 'post',
			data: {
				sort: sort,
				availability: availability,
				amount: amount,
				checkeredProducts: checkeredProducts,
				openedCategories: openedCategories,
				range: range
			},
			dataType: 'json',

			success: function (response) {
				if (response.success) {
					$.each(response.products, function (index, value) {
						let block = $('.js-product-show[data-id="' + index + '"]').parent();
						block.find('.export__checkers').remove();
						block.append(value)
					})

					$('.export__filter-noty').show("slow")
					setTimeout(function () {
						$('.export__filter-noty').hide("slow")
					}, 3000)

					$(".export__checkers input[type='checkbox']").not(".nesting-level--products input[type='checkbox']").each(function () {
						$(this).prop('checked', false)
					})

					$.each(response.checkedCategories, function (id, notAll) {
						$('input[data-id="' + id + '"]').prop('checked', 'checked').attr('data-notAll', !notAll)
					})

					$('[data-level="1"]').each(function (){
						let $this = $(this),
							allInputs = $this.find('[data-level="2"] li a input').length,
							checkedInputs = $this.find('[data-level="2"] li a input:checked').length;

						if (allInputs === checkedInputs) {
							$this.find('.checkbox__icon--main-category').next().find('input').prop('checked', 'checked').attr('data-notAll', true)
						} else if (checkedInputs > 0){
							$this.find('.checkbox__icon--main-category').next().find('input').prop('checked', true).attr('data-notAll', false)
						} else if (checkedInputs === 0) {
							$this.find('.checkbox__icon--main-category').next().find('input').prop('checked', false).attr('data-notAll', true)
						}
					})

					$('[data-level="3"]').each(function (){
						let $this = $(this),
							allInputs = $this.find('li a input').length,
							checkedInputs = $this.find('li a input:checked').length;

						if (allInputs === checkedInputs) {
							$this.prev().find('input').prop('checked', 'checked').attr('data-notAll', true)
						} else if (checkedInputs > 0){
							$this.prev().find('input').prop('checked', true).attr('data-notAll', false)
						} else if (checkedInputs === 0) {
							$this.prev().find('input').prop('checked', false).attr('data-notAll', true)
						}
					})
				}
			}
		});
	});
}

// ----------------------------------------
// Exports
// ----------------------------------------

export default ajaxFilter;
