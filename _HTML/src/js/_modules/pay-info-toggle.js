'use strict';

// ----------------------------------------
// Imports
// ----------------------------------------

// ----------------------------------------
// Public
// ----------------------------------------

function payToggle () {
	let wrap = $('[data-pay-wrap]');
	let inputs = wrap.find('input');
	let input = $('[data-pay-cart]');
	let info = $('[data-pay-cart-block]');
	let payType = $('.cart-info__label');

	if(wrap.length) {
		inputs.not(input).on('change', () => {
			info.slideUp(150);
		});
		input.on('change', () => {
			if(input.is(':checked')) {
				info.slideDown(150);
			}
		});

		payType.on('click', function (){
			$('.cart-info__label.is-checked').removeClass('is-checked');
			$(this).addClass('is-checked');
		})
	}
}

// ----------------------------------------
// Exports
// ----------------------------------------

export default payToggle;
