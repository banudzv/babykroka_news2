'use strict';

/**
 * Пользовательские обработчики формы
 * @module
 */

// ----------------------------------------
// Public
// ----------------------------------------

/**
 * @param {JQuery} $form
 * @param {Object} Validator {@link https://jqueryvalidation.org/?s=validator}
 * @param {Function} Validator.resetForm {@link https://jqueryvalidation.org/Validator.resetForm/}
 * @param {Function} Validator.element {@link https://jqueryvalidation.org/Validator.element/}
 */
function validateHandlers ($form, Validator) {
	// сабмит формы, блок отправки при ошибке скриптов
	const method = $form.attr('method') || 'post';
	$form.prop('method', method);
	$form.on('submit', event => {
		const action = $form.attr('action');
		if (!action) {
			event.preventDefault();
		}
	});

	// сброс формы
	$form.on('reset', () => Validator.resetForm());

	// проверка файлов, при смене
	$form.on('change', 'input[type="file"]', function () {
		Validator.element(this);
	});

	// дальше ваш код
}

// ----------------------------------------
// Exports
// ----------------------------------------

export default validateHandlers;
