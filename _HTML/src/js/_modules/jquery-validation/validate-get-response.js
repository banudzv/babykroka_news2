'use strict';

/**
 * Обработка ответа от сервера
 * @module
 */

// ----------------------------------------
// Imports
// ----------------------------------------

import Preloader from '#/_modules/Preloader';
import moduleLoader from '#/module-loader';
import '#/_modules/magnific-popup/mfp-init';

// ----------------------------------------
// Private
// ----------------------------------------

/**
 * Вывод уведомлений
 * @param {JQuery} $form
 * @param {string} message
 * @param {boolean} [success]
 * @private
 */

let magnificPopup = $.magnificPopup.instance;
let url = '/Views/Widgets/Popup/Thank.php';

function showMessage ($form, message, success) {
	if (magnificPopup.isOpen) {
		let $popup = $form.closest('.popup');

		if ($('.login-popup').length) {
			$popup.find('.grid').hide();
			$popup.append(`<div class="grid popup-noty"><div class="gcell--24"><p class='thank-popup__text thank-popup__text--inner'>` + message + `</p></div><button class="this-close" id="custom-close-btn" style="color:red; font-size: 15px">Вернуться назад</button></div>`);

			$('.this-close').on('click', function (){
				$popup.find('.grid').show();
				$('.popup-noty').remove();
			})
		} else {
			$popup.html(`<p class='thank-popup__text thank-popup__text--inner'>` + message + `</p><button class='mfp-close' id='custom-close-btn'></button>`);
		}

		return false;
	}

	$.magnificPopup.open({
		items: {
			src: url
		},
		type: 'ajax',
		removalDelay: 300,
		mainClass: 'zoom-in',
		closeMarkup: `<button class='mfp-close' id='custom-close-btn'></button>`,
		callbacks: {
			ajaxContentAdded: function () {
				$('.thank-popup__text').html(message);
			}
		}
	});
}

function showWidgetMessage ($form, message, widget) {
	if (magnificPopup.isOpen) {
		let $popup = $form.closest('.popup');

		$popup.html(`<p class='thank-popup__text thank-popup__text--inner'>` + message + `</p><button class='mfp-close' id='custom-close-btn'></button>`+widget);

		return false;
	}

	$.magnificPopup.open({
		items: {
			src: url
		},
		type: 'ajax',
		removalDelay: 300,
		mainClass: 'zoom-in',
		closeMarkup: `<button class='mfp-close' id='custom-close-btn'></button>`,
		callbacks: {
			ajaxContentAdded: function () {
				$('.thank-popup__text').html(message);
			}
		}
	});
}

function closePopup () {
	if (magnificPopup.isOpen) {
		$.magnificPopup.close();
	}
}

function sum (arr) {
	const reducer = (accumulator, currentValue) => parseInt(accumulator) + parseInt(currentValue);

	return arr.reduce(reducer);
}

// Thousand separator

function setThousands (numberText, newSeparator, separator) {
	newSeparator = newSeparator || '.';
	separator = separator || '.';
	numberText = '' + numberText;
	numberText = numberText.split(separator);

	let numberPenny = numberText[1] || '';
	let numberValue = numberText[0];

	let thousandsValue = [];
	let counter = 0;

	for (let i = numberValue.length - 1; i >= 0; i--) {
		let num = numberValue[i];
		thousandsValue.push(num);

		if (++counter === 3 && i) {
			thousandsValue.push(' ');
			counter = 0;
		}
	}

	thousandsValue = thousandsValue.reverse().join('');
	if (numberPenny.length) {
		return [thousandsValue, numberPenny].join(newSeparator);
	}
	return thousandsValue;
}

function updateStaticCart (response) {
	if ($('.js-cart-static').length && typeof response.static !== 'undefined') {
		$('.js-cart-static').html(response.static);

		if (!response.count) {
			$('[data-total]').addClass('_hide');
			$('[data-total-empty]').removeClass('_hide');
		}

		if ($('.js-total-price').length && typeof response.totalPrice !== 'undefined') {
			$('.js-total-price').text(response.totalPrice.toFixed(2));
		}

		moduleLoader.init($('.js-cart-static'));
	}

	if ($('.js-cart').length && typeof response.html !== 'undefined') {
		let $html = $(response.html);
		$('.js-cart').html($html);

		moduleLoader.init($('.js-cart'));
	}

	if ($('.js-cart-count').length && typeof response.count !== 'undefined') {
		$('.js-cart-count').html(response.count);
	}
}

function changeSizes ($form, sizes, counts) {
	let $wrap = $('.js-product-counts');
	let $items = $wrap.find('.js-product-count-item');
	let $cartItem = $wrap.closest('.js-cart-item');

	let $counter = $cartItem.find('.js-product-count-all');
	let $counterInput = $cartItem.find('.js-product-input');
	let $price = $cartItem.find('.js-product-price');

	let count = sum(counts);
	let price = $price.data('price') * count;

	$cartItem.data('sizes', sizes);
	$cartItem.data('counts', counts);
	$cartItem.data('count', count);
	$counter.text(count);
	$counterInput.val(count);
	$price.text(setThousands(price.toFixed(2)));

	$items.removeClass('active');

	$items.each(function (i, el) {
		let $item = $(this);
		let $count = $(this).find('.js-product-count');

		let param = $item.data('param');
		param.param.counts = counts;

		$item.attr('data-param', JSON.stringify(param));
		$count.text('');

		sizes.forEach((item, i) => {
			if ($item.data('size') === sizes[i]) {
				if (counts[i] > 0) {
					$count.html(counts[i]);
					$item.addClass('active');
				}
			}
		});
	});
}

// ----------------------------------------
// Public
// ----------------------------------------

/**
 * @param {JQuery} $form
 * @param {Object} response
 * @param {string} statusText
 * @param {Object} xhr
 * @param {string} response.response - текстовое сообщение
 * @param {boolean} response.success - успешный запрос
 * @param {string} [response.redirect] - урл для редиректа, если равен текущему урлу - перегражаем страницу
 * @param {boolean} [response.reload] - перегрузить страницу
 * @param {boolean} [response.reset] - сбросить форму
 * @param {Array} [response.clear] - сбросить форму
 * @private
 */
function validateGetResponse ($form, response, statusText, xhr) {
	const preloader = $form.data('preloader');
	if (preloader instanceof Preloader) {
		preloader.hide();
	}

	// обрабатываем ответ
	// ------------------
	if (typeof response === 'string') {
		response = JSON.parse(response);
	}

	if (response.reload || window.location.href === response.redirect) {
		return window.location.reload();
	}

	if (response.redirect) {
		return (window.location.href = response.redirect);
	}

	if (response.success) {
		if (response.clear) {
			if (Array.isArray(response.clear)) {
				response.clear.forEach(clearSelector => {
					$form.find(clearSelector).val('');
				});
			} else {
				// игнорируемые типы инпутов
				let ignoredInputsType = [
					'submit',
					'reset',
					'button',
					'image'
				];
				$form.find('input, textarea, select').each((i, element) => {
					if (~ignoredInputsType.indexOf(element.type)) {
						return true;
					}
					element.value = '';
				});
			}
		} else if (response.reset) {
			$form.trigger('reset');
		}
	}
	if (response.sizes) {
		changeSizes($form, response.sizes, response.counts);
	}
	if (response.cart) {
		updateStaticCart(response);
	}
	if (response.response) {
		if (response.widget) {
			showWidgetMessage($form, response.message,response.response);
		}else{
			showMessage($form, response.response, response.success);
		}
	}
	if (response.close) {
		closePopup();
	}
	if (response.form) {
		let $html = $('<div class="_hide">' + response.form  + '</div>');
		$form.append($html);
		$html.find('form').get(0).submit();
	}
}

// ----------------------------------------
// Exports
// ----------------------------------------

export default validateGetResponse;
