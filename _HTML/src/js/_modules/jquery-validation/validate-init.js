'use strict';

/**
 * Инит валидации форм
 * @module
 */

// ----------------------------------------
// Imports
// ----------------------------------------

import 'jquery-validation';
import 'custom-jquery-methods/fn/node-name';
import 'custom-jquery-methods/fn/has-inited-key';
import './extend/validate-extend';
import validateHandlers from './validate-handlers';
import validateGetResponse from './validate-get-response';
import Preloader from '#/_modules/Preloader';
import {mfpAjax} from "../magnific-popup/mfp-init";

// ----------------------------------------
// Private
// ----------------------------------------

/**
 * @const {Object}
 * @private
 * @sourceCode
 */
const configDefault = {
	ignore: ':hidden, .js-ignore',
	get classes () {
		return {
			error: 'has-error',
			valid: 'is-valid',
			labelError: 'label-error',
			formError: 'form--error',
			formValid: 'form--valid',
			formPending: 'form--pending'
		};
	},
	get errorClass () {
		return this.classes.error;
	},
	get validClass () {
		return this.classes.error;
	},

	/**
	 * Валидировать элементы при потере фокуса.
	 * Или false или функция
	 * @type {Boolean|Function}
	 * @prop {HTMLElement} element
	 * @prop {Event} event
	 * @see {@link https://jqueryvalidation.org/validate/#onfocusout}
	 */
	onfocusout (element) {
		if (element.value.length || element.classList.contains(this.settings.classes.error)) {
			this.element(element);
		}
	},

	/**
	 * Валидировать элементы при keyup.
	 * Или false или функция
	 * @type {Boolean|Function}
	 * @prop {HTMLElement} element
	 * @prop {Event} event
	 * @see {@link https://jqueryvalidation.org/validate/#onkeyup}
	 */
	onkeyup (element) {
		if (element.classList.contains(this.settings.classes.error)) {
			this.element(element);
		}
	},

	/**
	 * Подсветка элементов с ошибками
	 * @param {HTMLElement} element
	 */
	highlight (element) {
		element.classList.remove(this.settings.classes.valid);
		element.classList.add(this.settings.classes.error);
	},

	/**
	 * Удаление подсветки элементов с ошибками
	 * @param {HTMLElement} element
	 */
	unhighlight (element) {
		element.classList.remove(this.settings.classes.error);
		element.classList.add(this.settings.classes.valid);
	},

	/**
	 * Обработчик сабмита формы
	 * @param {HTMLFormElement} form
	 * @returns {boolean}
	 */
	submitHandler (form) {
		let $form = $(form);
		let actionUrl = $form.data('ajax');

		if (!actionUrl) {
			return true;
		}

		let preloader = new Preloader($form);
		preloader.show();

		let formData = new window.FormData();
		formData.append('xhr-lang', $('html').attr('lang') || 'ru');

		// игнорируемые типы инпутов
		let ignoredInputsType = [
			'submit',
			'reset',
			'button',
			'image'
		];

		$form.find('input, textarea, select').each((i, element) => {
			let {value, type} = element;
			if (~ignoredInputsType.indexOf(type)) {
				return true;
			}

			let $element = $(element);
			let elementNode = $element.nodeName();
			let elementName = $element.data('name') || null;

			if (elementName === null) {
				return true;
			}

			switch (elementNode) {
				case 'input':
					if (type === 'file' && element.files && element.files.length) {
						for (let i = 0; i < element.files.length; i++) {
							let file = element.files[i];
							formData.append(elementName, file);
						}
					} else {
						if ((type === 'checkbox' || type === 'radio') && !element.checked) {
							break;
						}
						formData.append(elementName, value);
					}
					break;

				case 'textarea':
					formData.append(elementName, value);
					break;

				case 'select':
					let multiName = /\[\]$/;
					if ((multiName.test(elementName) || element.multiple) && element.selectedOptions && element.selectedOptions.length) {
						for (let i = 0; i < element.selectedOptions.length; i++) {
							let option = element.selectedOptions[i];
							if (option.disabled) {
								continue;
							}
							formData.append(elementName, option.value);
						}
					} else {
						formData.append(elementName, value);
					}
					break;
			}
		});

		let xhr = new window.XMLHttpRequest();
		xhr.open('POST', actionUrl);
		xhr.onreadystatechange = function () {
			if (xhr.readyState === 4) {
				let {status, statusText, response} = xhr;
				// все плохо
				if (status !== 200) {
					console.log(xhr);
					preloader.hide();
					return false;
				}

				if (typeof response === 'string') {
					response = JSON.parse(response);
				}

				if (actionUrl === '//' + location.host + '/ajax/cartSizes') {
					let $sizeCountBlock = $('.js-product-counts');
					if($sizeCountBlock.length) {
						for (let size in response.active_count) {
							let $sizeButton = $sizeCountBlock.find('.js-product-count-item[data-size=' + size + ']');
							$sizeButton.find('span').first().removeClass('product__size-text').addClass('size__item');
							$sizeButton.find('.js-product-count').removeClass('product__size-count').addClass('size__count').text(response.active_count[size]);
						}
					}
				}

				validateGetResponse($form, response, statusText, xhr);
			}
		};

		xhr.send(formData);
		return false;
	}
};

// ----------------------------------------
// Public
// ----------------------------------------

/**
 * @param {JQuery} $elements
 */
function validate ($elements) {
	$elements.each((i, el) => {
		const $form = $(el);
		if ($form.hasInitedKey('validate-inited')) {
			return true;
		}

		const config = $.extend(true, {}, configDefault, {
			// current element custom config
		});

		$form.validate(config);
		validateHandlers($form, $form.data('validator'));
	});
}

// ----------------------------------------
// Exports
// ----------------------------------------

export default validate;
