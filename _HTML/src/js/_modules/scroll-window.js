'use strict';

/**
 * Скролл window
 * @see {@link https://greensock.com/docs/Plugins/ScrollToPlugin}
 * @module
 */

// ----------------------------------------
// Imports
// ----------------------------------------

import GSAP from '#/_vendors/gsap/gsap';
import TweenLite from '#/_vendors/gsap/TweenLite';
import '#/_vendors/gsap/plugins/ScrollToPlugin';

// ----------------------------------------
// Private
// ----------------------------------------

/**
 * Длительность скролла, в секундах
 * @type {number}
 * @private
 */
const defaultDuration = 1;

/**
 * @type {JQuery}
 * @private
 */
const $window = $(window);

/**
 * @type {number}
 * @private
 */
const showHeight = 500;

/**
 * @type {string}
 * @private
 */
const showClass = 'scroll-up--show';

// ----------------------------------------
// Public
// ----------------------------------------

function scrollWindow ($elements) {
	$elements.on('click', function () {
		let $this = $(this);
		let scroll = $this.data('scroll-window') || 0;

		if (scroll === 'up') {
			scroll = 0;
		} else if (scroll === 'down') {
			scroll = 'max';
		}

		TweenLite.to(window, defaultDuration, {
			scrollTo: scroll,
			ease: GSAP.Power2.easeOut,
			onComplete () {
				// console.log('complete');
			}
		});
	});

	const $scrollUp = $elements.filter('[data-scroll-window="up"]');
	if ($scrollUp.length) {
		const onScroll = () => {
			let scroll = $window.scrollTop();
			let doClass = scroll > showHeight ? 'addClass' : 'removeClass';
			$scrollUp[doClass](showClass);
		};

		$window.on('scroll', onScroll);
		onScroll();
	}
}

// ----------------------------------------
// Exports
// ----------------------------------------

export default scrollWindow;
