'use strict';

// ----------------------------------------
// Imports
// ----------------------------------------

import 'malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min';
import 'malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css';

// ----------------------------------------
// Public
// ----------------------------------------

function customScroll ($container = $('body')) {
	$container.find('.scroll-box').each(function () {
		let $this = $(this);

		if ($this.hasClass('mCustomScrollbar')) {
			$this.mCustomScrollbar('destroy');
		}

		$this.mCustomScrollbar({
			mouseWheel: {
				enable: true,
				mouseWheelPixels: 30
			},
			advanced: {
				updateOnContentResize: true
			},
			live: true,
			liveSelector: '.scroll-box-ajax'
		});
	});
}

// ----------------------------------------
// Exports
// ----------------------------------------

export {customScroll};
