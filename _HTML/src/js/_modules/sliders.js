'use strict';

// ----------------------------------------
// Imports
// ----------------------------------------

import 'slick-carousel';

// ----------------------------------------
// Private
// ----------------------------------------

let $mainSlider = $('.main-slider');
let $productSlider = $('.product-slider');
// let $similarSlider = $('.similar-slider');
let $container = $('.container');
let $dotsControls = $('.main-slider__controls');
let $dotsHolder = $('.main-slider__controls-holder');
let $productPhotoSlider = $('.product__gallery-main');
let $productNavSlider = $('.gallery-preview__wrap');

function dotsPosition (that) {
	let padding = ($(window).width() - $container.outerWidth()) / 2;
	let dotsPosTop = $dotsHolder.position().top;

	that.find('.slick-list').css('padding-left', padding);
	$dotsControls.css('top', dotsPosTop).css('left', padding);
}

function setPadding (that) {
	if (!that.hasClass('small')) {
		let padding = ($(window).width() > 1023) ? parseInt(($(window).width() - $container.outerWidth()) / 2) : 0;
		that.find('.slick-list').css('padding-left', padding).css('padding-right', padding);
	}
}

// ----------------------------------------
// Public
// ----------------------------------------

function mainSlider () {
	if (!$mainSlider.length) {
		return false;
	}

	$mainSlider.on('init', function (event, slick, direction) {
		dotsPosition($(this));
	});

	$mainSlider.slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		infinite: true,
		dots: true,
		arrows: false,
		autoplay: false,
		fade: true,
		appendDots: $('.main-slider__controls'),
		customPaging: function (slider, i) {
			let num = ((i + 1) <= 9) ? '0'.concat(i + 1) : (i + 1);
			return `<a class="main-slider__dot">` + num + `</a>`;
		},
		prevArrow: `<button type='button' class='slick-arrow slick-prev'></button>`,
		nextArrow: `<button type='button' class='slick-arrow slick-next'></button>`,
		responsive: [
			{
				breakpoint: 768,
				settings: {
					autoplay: true,
					pauseOnHover: false
				}
			}
		]
	});

	$(window).resize(function () {
		dotsPosition($mainSlider);
	});
}

function productImgSlider () {
	let $productImgSlider = $('.product-img-slider');

	if (!$productImgSlider.length) {
		return false;
	}
	$productImgSlider.not('.slick-initialized').each(function (i,e) {
		let $this = $(e);

		$this.slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			infinite: false,
			dots: false,
			arrows: true,
			autoplay: false,
			fade: true,
			appendArrows: $this.parent().find('.product-item__buttons-box'),
			prevArrow: '<button type=\'button\' class=\'slick-arrow slick-prev\'></button>',
			nextArrow: '<button type=\'button\' class=\'slick-arrow slick-next\'></button>',

			// responsive: [{
			// 	breakpoint: 768,
			// 	settings: {
			// 		autoplay: true,
			// 		pauseOnHover: false
			// 	}
			// }]
		});

	});

}

function productSlider () {
	if (!$productSlider.length) {
		return false;
	}

	$productSlider.each(function (i) {
		let $this = $(this);

		$this.on('init', function (event, slick, direction) {
			setPadding($this);
		});

		$this.slick({
			slidesToShow: 4,
			slidesToScroll: 1,
			infinite: false,
			arrows: true,
			dots: true,
			appendDots: $this.parent().find('.product-slider__counter'),
			customPaging: function (slider, i) {
				let num = i + 1;
				return '<span class="first-num">' + num + '</span><span class="second-num"> / ' + slider.slideCount + '</span>';
			},
			appendArrows: $this.parent().find('.product-slider__buttons-box'),
			prevArrow: `<button type='button' class='slick-arrow slick-prev'></button>`,
			nextArrow: `<button type='button' class='slick-arrow slick-next'></button>`,
			responsive: [
				{
					breakpoint: 1025,
					settings: {
						slidesToShow: 3
					}
				},
				{
					breakpoint: 768,
					settings: {
						slidesToShow: 2
					}
				}, {
					breakpoint: 640,
					settings: {
						slidesToShow: 2
					}
				}, {
					breakpoint: 480,
					settings: {
						slidesToShow: 1
					}
				}
			]
		});

		$(window).resize(function () {
			setPadding($this);
		});
	});
}

// function similarSlider () {
// 	if (!$similarSlider.length) {
// 		return false;
// 	}
//
// 	$similarSlider.slick({
// 		slidesToShow: 4,
// 		slidesToScroll: 1,
// 		infinite: true,
// 		dots: true,
// 		appendDots: $similarControls,
// 		prevArrow: `<button type='button' class='slick-arrow slick-prev'></button>`,
// 		nextArrow: `<button type='button' class='slick-arrow slick-next'></button>`,
// 		responsive: [
// 			{
// 				breakpoint: 1025,
// 				settings: {
// 					slidesToShow: 2
// 				}
// 			}, {
// 				breakpoint: 641,
// 				settings: {
// 					slidesToShow: 1
// 				}
// 			}
// 		]
// 	});
// }

function productPhotoSlider () {
	if (!$productPhotoSlider.length) {
		return false;
	}

	let photoSlider = $('.product__gallery-main').slick({
		slidesToShow: 1,
		infinite: false,
		fade: true,
		dots: false,
		arrows: false,
		accessibility: false,
		asNavFor: '.gallery-preview__wrap'
	});

	$productNavSlider.slick({
		slidesToShow: 4,
		slidesToScroll: 1,
		asNavFor: '.product__gallery-main',
		focusOnSelect: true,
		vertical: true,
		infinite: false,
		prevArrow: '<button type="button" class="slick-arrow slick-prev"></button>',
		nextArrow: '<button type="button" class="slick-arrow slick-next"></button>',

		responsive: [
			{
				breakpoint: 768,
				settings: {
					vertical: false,
					slidesToShow: 6,
					infinite: true
				}
			}, {
				breakpoint: 480,
				settings: {
					vertical: false,
					slidesToShow: 3,
					infinite: true
				}
			}
		]
	});

	photoSlider.on('beforeChange', function (event, slick, currentSlide, nextSlide) {

	});
}

// ----------------------------------------
// Exports
// ----------------------------------------

export {mainSlider, productSlider, productPhotoSlider, productImgSlider};
