'use strict';

/**
 *
 * @module
 */

// ----------------------------------------
// Imports
// ----------------------------------------

import 'inputmask/dist/jquery.inputmask.bundle';
import 'custom-jquery-methods/fn/has-inited-key';

// ----------------------------------------
// Private
// ----------------------------------------

/**
 * @param {JQuery} $element
 * @return {string}
 * @private
 */
function getMask ($element) {
	let {
		phonemask = '+38(999) 99-99-999',
		phonemaskAndroid = '+389999999999'
	} = $element.data();

	return window.Modernizr.android5 ? phonemaskAndroid : phonemask;
}

// ----------------------------------------
// Public
// ----------------------------------------

/**
 * @param {JQuery} $elements
 */
function phonemask ($elements) {
	$elements.each((i, el) => {
		const $element = $(el);
		if ($element.hasInitedKey('phonemask-inited')) {
			return true;
		}

		let isComplete = () => $element.data('valid', $element.inputmask('isComplete'));
		$element.inputmask({
			mask: getMask($element),
			// clearMaskOnLostFocus: false,
			oncomplete () {
				$element.data('valid', true);
			},
			onincomplete () {
				$element.data('valid', false);
			}
		}).on('change.isComplete', isComplete);
	});
}

// ----------------------------------------
// Exports
// ----------------------------------------

export default phonemask;
