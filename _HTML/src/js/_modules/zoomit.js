'use strict';

// ----------------------------------------
// Imports
// ----------------------------------------

import '#/_vendors/zoomit.min'

// ----------------------------------------
// Private
// ----------------------------------------

/**
 * @type {Object}
 * @private
 */

export default function zoomit() {
	$('.zoom-img').each(function (){
		$(this).zoomIt()
	});
};
