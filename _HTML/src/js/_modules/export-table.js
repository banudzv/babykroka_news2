'use strict';

// ----------------------------------------
// Imports
// ----------------------------------------

import setCheckers from "./export-module/set-checkers";
import panelWithBtns from "./export-module/panel-with-btns"
import listToggle from "./export-module/list-toggle"
import productsDownload from "./export-module/products-download";
import priceFromTo from "./export-module/price-from-to";
import priceFromClient from "./export-module/price-from-client";
import infoBlock from "./export-module/info-block";
import ajaxFilter from "./export-module/ajax-filter";
import save from "./export-module/save";
import copyLink from "./export-module/copy-link";

// ----------------------------------------
// Public
// ----------------------------------------

function exportTable() {
	if ($('[data-export-table]').length) {

		// Простановка чекеров + отправка id категории
		setCheckers();

		// Панель кнопок для чекеров
		panelWithBtns();

		// Разворачивание списков
		listToggle();

		// Подгрузка товаров при расскрытии списка с применением фильтра
		productsDownload();

		// Слайдер цены от и до
		priceFromTo();

		// Слайдер цен клиента
		priceFromClient();

		// Info блок
		infoBlock();

		// Ajax на фильтр
		ajaxFilter();

		// Сохранение
		save();

		// Копирование URL
		copyLink();
	}
}

// ----------------------------------------
// Exports
// ----------------------------------------

export {exportTable};
