'use strict';

// ----------------------------------------
// Public
// ----------------------------------------

function subMenuToggle (item, wrap, slide, btn) {
	let $wrap;
	let $openWrap = $(wrap + '.open');
	let $this;

	if ($openWrap) {
		$openWrap.find(slide).slideDown('fast');
	}

	if (btn) {
		$(item).find(btn).click(function (e) {
			$this = $(this);
			$wrap = $this.closest($(wrap));

			if ($wrap.find(slide).length) {
				e.preventDefault();

				$wrap.find(slide).slideToggle('fast');
				$wrap.toggleClass('open');
				$this.closest(item).toggleClass('open');
			}
		});
	} else {
		$(item).click(function (e) {
			$this = $(this);
			$wrap = $this.closest($(wrap));

			if ($wrap.find(slide).length) {
				e.preventDefault();

				$wrap.find(slide).slideToggle('fast');
				$wrap.toggleClass('open');
				$this.toggleClass('open');
			}
		});
	}
}

// ----------------------------------------
// Exports
// ----------------------------------------

export {subMenuToggle};
