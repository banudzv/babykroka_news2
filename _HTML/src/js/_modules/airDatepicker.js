'use strict';

// ----------------------------------------
// Imports
// ----------------------------------------

import '../_vendors/airdatepicker';

// ----------------------------------------
// Public
// ----------------------------------------

function airDatepicker () {
	$(".js-birthday").datepicker({
		maxDate: new Date()
	})

	$('.js-birthday').on('input', function() {
		$(this).val($(this).val().replace(/[A-Za-zА-Яа-яЁё,0-9]/, ''))
	});
}

// ----------------------------------------
// Exports
// ----------------------------------------

export {airDatepicker};

