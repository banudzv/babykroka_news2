'use strict';

// ----------------------------------------
// Imports
// ----------------------------------------


// ----------------------------------------
// Public
// ----------------------------------------

function deliveryRegistration ($container = $('body')) {
	$container.find('.scroll-box').each(function () {

	});

	let rozn = $('input[name="rozn"]'),
		opt = $('input[name="opt"]'),
		drop = $('input[name="drop"]'),
		noReg = $('input[name="no-profile"]')

	function checkReq(input) {
		rozn.prop('required', false);

		if (!rozn.prop('checked') && !opt.prop('checked') && !drop.prop('checked')){
			rozn.prop('required', true);
		}
	}

	rozn.on('click', function () {
		checkReq();
		opt.prop('checked', false);
		drop.prop('checked', false);
	});

	opt.on('click', function () {
		checkReq();
		rozn.prop('checked', false);
	});

	drop.on('click', function () {
		checkReq();
		rozn.prop('checked', false);
	});

	noReg.on('click', function () {
		rozn.prop('checked', false);
		opt.prop('checked', false);
		drop.prop('checked', false);

		if($(this).prop('checked')) {
			$('.cart-info__item').find('.cart-registration__status').hide();
		} else {
			$('.cart-info__item').find('.cart-registration__status').show();
		}
	});

	if (noReg.prop('checked')) {
		rozn.prop('checked', false);
		opt.prop('checked', false);
		drop.prop('checked', false);
		$('.cart-info__item').find('.cart-registration__status').hide();
	}
}

// ----------------------------------------
// Exports
// ----------------------------------------

export {deliveryRegistration};
