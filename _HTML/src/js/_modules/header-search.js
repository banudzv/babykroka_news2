'use strict';

// ----------------------------------------
// Private
// ----------------------------------------

let $header = $('.header'); // Меню
let $headerSearch = $('.header__search-wrap');
let $headerSearchResult = $('.header__search-results');
let $headerSearchEmpty = $('.header__search-empty');
let $headerSearchInput = $('.header__search-input');
let searchUrl = $headerSearch.data('url');
let top = $header.outerHeight();

// ----------------------------------------
// Public
// ----------------------------------------

function headerSearchToggle () {
	$('body').on('click', '.header-search__btn', function () {
		$headerSearch.css({
			'top': top + 'px',
			'height': 'calc(100vh - ' + top + 'px)'
		});
		$(this).toggleClass('open');

		setTimeout(function (){
			$('.header__search-input').focus();
		}, 500)

		$headerSearch.slideToggle('fast');

		if ($header.hasClass('open')) {
			$header.toggleClass('open');
			$headerSearchInput.val('');
			$headerSearchResult.removeClass('is-show');
		} else {
			setTimeout(function () {
				$header.toggleClass('open');
			}, 400);
		}
	});
}

function searchAutocomplete () {
	if (!$headerSearchInput.length) {
		return false;
	}

	$headerSearchInput.on('keyup', function (e) {
		let $this = $(this);
		let text = $this.val();

		if (text.length >= 2) {
			$.ajax({
				url: searchUrl,
				data: text,
				dataType: 'json',
				success: function (response) {
					if (response.success) {
						let html = '';
						if (response.items.length > 0) {
							for (let i = 0; i < response.items.length; i++) {
								html +=
									`<div class="search-result__item">` +
									(response.items[i].img ? `<img class="search-result__img" src="` + response.items[i].img + `">` : '') +
									`<div class="search-result__info">` +
									(response.items[i].code ? `<span class="search-result__code">Код товара: <span class="dark">` + response.items[i].code + `</span> </span>` : '') +
									`<a class="search-result__name" href="` + response.items[i].href + `">` + response.items[i].name + `</a>` +
									(response.items[i].price ? `<span class="search-result__price">` + response.items[i].price + `</span>` : '') +
									`</div>` +
									`</div>`;
							}
							$('.header__search-results .mCSB_container').html(html);
							$headerSearchResult.addClass('is-show');
							$headerSearchEmpty.removeClass('is-show');
						} else {
							$headerSearchEmpty.addClass('is-show');
							$headerSearchResult.removeClass('is-show');
						}
					}
					if (response.error) {
						console.log('error');
					}
				}
			});
		} else {
			$headerSearchResult.removeClass('is-show');
			$headerSearchEmpty.removeClass('is-show');
		}
	});
}

// ----------------------------------------
// Exports
// ----------------------------------------

export {headerSearchToggle, searchAutocomplete};
