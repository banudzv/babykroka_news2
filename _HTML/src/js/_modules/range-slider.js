'use strict';

// ----------------------------------------
// Imports
// ----------------------------------------

import '../_vendors/jquery-ui.min';
import '../_vendors/jquery.ui.touch-punch.min';

// ----------------------------------------
// Private
// ----------------------------------------

let $rangeSlider = $('.price-slider');

// ----------------------------------------
// Public
// ----------------------------------------

function initRangeSlider () {
	if (!$rangeSlider.length) {
		return false;
	}

	$rangeSlider.each(function (item) {
		let $this = $(this);
		let $wrap = $this.closest('.filters__price-group');
		let $minCost = $wrap.find('.min-cost');
		let $maxCost = $wrap.find('.max-cost');
		let min = parseFloat($this.data('min'));
		let max = parseFloat($this.data('max'));
		let start = parseFloat($this.data('start'));
		let end = parseFloat($this.data('end'));

		let startValue = (start !== undefined) ? start : min;
		let endValue = (end !== undefined) ? end : max;

		$(this).slider({
			min: min,
			max: max,
			range: true,
			values: [startValue, endValue],

			stop: function (event, ui) {
				$minCost.val(ui.values[0]);
				$maxCost.val(ui.values[1]);
			},

			slide: function (event, ui) {
				$minCost.val(ui.values[0]);
				$maxCost.val(ui.values[1]);
			},

			change: function (event, ui) {
				let $filterWrapper = $(this).parents('.main-filter-wrapper');
				let $counter = $('.filter-summary-wrapper');
				let $input = $(this).parents('.filters__item');
				let position = $input.offset();

				let category_id = $filterWrapper.data('category');
				let filters = {};
				$('.filter-input:checked').each(function() {
					let name = $(this).attr('name');
					if (!filters.hasOwnProperty(name)) filters[name] = [];

					let value = $(this).val();

					if (!filters[name].includes(value)) filters[name].push(value);
				});

				let $minRangeInput = $filterWrapper.find('.min-cost');
				let $maxRangeInput = $filterWrapper.find('.max-cost');
				let minRangeCost = $minRangeInput.val();
				let maxRangeCost = $maxRangeInput.val();
				let minPrice = $minRangeInput.data('min');
				let maxPrice = $maxRangeInput.data('max');
				if (minPrice < minRangeCost || maxPrice > maxRangeCost) {
					filters['mincost'] = minRangeCost;
					filters['maxcost'] = maxRangeCost;
				}

				$.ajax({
					type: 'post',
					url: '/ajax/prepareFilter',
					data: {
						category_id,
						filters
					},
					dataType: 'json',
					success: function(response) {
						if (response.success) {
							$counter.find('.count').text(response.count);
							$counter.find('.summary-submit').data('url', response.url);

							if($(window).width()  > 1023) {
								$counter.css({
									'left': parseInt(position.left + $input.width()) - $counter.width() / 2,
									'top': position.top,
									'display': 'block'
								});
							}
						}
					}
				});
			}
		});

		$minCost.change(function () {
			let minValue = $(this).val();
			let maxValue = $maxCost.val();

			if (parseInt(minValue) > parseInt(maxValue)) {
				minValue = maxValue;
				$minCost.val(minValue);
			}

			$rangeSlider.slider('values', 0, minValue);
		});

		$maxCost.change(function () {
			let minValue = $minCost.val();
			let maxValue = $(this).val();

			if (maxValue > max) {
				maxValue = max;
				$maxCost.val(max);
			}

			if (parseInt(minValue) > parseInt(maxValue)) {
				maxValue = minValue;
				$maxCost.val(maxValue);
			}

			$rangeSlider.slider('values', 1, maxValue);
		});

		$minCost.keypress(function (event) {
			let key;
			let keyChar;

			if (event.keyCode) {
				key = event.keyCode;
			} else if (event.which) {
				key = event.which;
			}

			if (key === null || key === 0 || key === 8 || key === 13 || key === 9 || key === 46 || key === 37 || key === 39) {
				return true;
			}
			keyChar = String.fromCharCode(key);

			if (!/^[0-9]+$/.test(keyChar)) {
				return false;
			}
		});

		$maxCost.keypress(function (event) {
			let key;
			let keyChar;

			if (event.keyCode) {
				key = event.keyCode;
			} else if (event.which) {
				key = event.which;
			}

			if (key === null || key === 0 || key === 8 || key === 13 || key === 9 || key === 46 || key === 37 || key === 39) {
				return true;
			}
			keyChar = String.fromCharCode(key);

			if (!/^[0-9]+$/.test(keyChar)) {
				return false;
			}
		});
	});
}

// ----------------------------------------
// Exports
// ----------------------------------------

export {initRangeSlider};
