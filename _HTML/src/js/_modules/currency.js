'use strict';

// ----------------------------------------
// Imports
// ----------------------------------------

import cookieData from '#/_modules/cookie-data';

// ----------------------------------------
// Public
// ----------------------------------------

function currencyInit () {
	$('.js-currency').on('click', function () {
		let currency = $(this).data('currency');
		cookieData.add('currency', currency, {
			expires: 3600 * 24 * 30,
			path: '/'
		});
		window.location.href = window.location.href;
	});
}

// ----------------------------------------
// Exports
// ----------------------------------------

export default currencyInit;
