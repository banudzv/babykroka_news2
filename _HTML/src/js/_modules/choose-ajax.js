'use strict';

// ----------------------------------------
// Imports
// ----------------------------------------

import Preloader from './Preloader';
import {initSelect} from '#/_modules/select';

// ----------------------------------------
// Public
// ----------------------------------------

function chooseAjax ($content = $('body')) {
	let $target = $content.find('.js-choose-ajax');

	function sendAjax ($this, $container) {
		let url = $this.data('url');
		let name = $this.attr('name') || $this.data('name');
		let value = $this.val() || $this.data('val');
		let preloader = new Preloader($container);

		if (url && name) {
			preloader.show();

			$.ajax({
				url: url,
				type: 'post',
				data: {
					name: name,
					value: value
				},
				dataType: 'json',
				success: function (response) {
					if (response.success) {
						if (response.items) {
							response.items.forEach(function (item) {
								if (item.html && item.selector) {
									let $html = $(item.html);
									let $selector = $(item.selector);

									if ($selector.length) {
										let $select2 = $container.find('select.select2');

										$select2.each(function () {
											let $th = $(this);
											if ($th.data('select2')) {
												$th.select2('destroy');
											}
										});

										$selector.html($html);

										$select2.each(function () {
											let $th = $(this).parent();
											initSelect($th);
										});

										chooseAjax($html);
									}
								}
							});
						}
					}

					preloader.hide();
				}
			});
		}
	}

	if ($target.length) {
		$target.each(function () {
			let $this = $(this);
			let $container = $this.closest('.js-choose-ajax-container');
			let tagName = this.tagName.toLowerCase();

			if ($this.data('select2')) {
				$this.on('select2:select', function (e) {
					sendAjax($this, $container);
				});
			} else if (tagName === 'select' || (tagName === 'input' && ($this.attr('type') === 'radio' || $this.attr('type') === 'checkbox'))) {
				$this.on('change', function (e) {
					sendAjax($this, $container);
				});
			} else if (tagName === 'textarea' || (tagName === 'input' && $this.attr('type') !== 'radio' && $this.attr('type') !== 'checkbox')) {
				$this.on('keyup', function (e) {
					sendAjax($this, $container);
				});
			} else {
				$this.on('click', function (e) {
					sendAjax($this, $container);
				});
			}
		});
	}
}

// ----------------------------------------
// Exports
// ----------------------------------------

export default chooseAjax;
