'use strict';

// ----------------------------------------
// Imports
// ----------------------------------------

import Preloader from '#/_modules/Preloader';
import moduleLoader from '#/module-loader';

// ----------------------------------------
// Private
// ----------------------------------------

let openCartBtn = '.js-cart-open';
let closeCartBtn = '.js-cart-close';
let cartWrap = '.cart-wrap';
let $cartInner = $('.cart-inner');
let magnificPopup = $.magnificPopup.instance;
let url = '/Views/Widgets/Popup/Thank.php';

function showMessage (message) {
	if (magnificPopup.isOpen) {
		$.magnificPopup.close();
	}

	$.magnificPopup.open({
		items: {
			src: url
		},
		type: 'ajax',
		removalDelay: 300,
		mainClass: 'zoom-in',
		closeMarkup: `<button class='mfp-close' id='custom-close-btn'></button>`,
		callbacks: {
			ajaxContentAdded: function () {
				$('.thank-popup__text').html(message);
			}
		}
	});
}

function toggle (block, value, speed) {
	$(block).animate({
		right: value
	}, speed);
}

function openCart () {
	toggle(cartWrap, '0px', 450);
	setTimeout(function () {
		$(cartWrap).addClass('open');
	}, 500);
	setTimeout(function () {
		$('body').addClass('overflow');
	}, 500);
	moduleLoader.init($(cartWrap));
}

function closeCart () {
	$(cartWrap).removeClass('open');
	setTimeout(function () {
		toggle(cartWrap, '-100%', 300);
	}, 500);
	$('body').removeClass('overflow');
}

function cartRemove (e) {
	let $item = $(this).closest('.js-cart-item');
	let remove = $(this).data('url');
	let $confirm = $item.find('.js-remove-confirm');
	let $confirmBtn = $confirm.find('.js-confirm');
	let $unConfirmBtn = $confirm.find('.js-unconfirm');

	if ($confirm.hasClass('is-show')) {
		$confirm.fadeOut('fast').removeClass('is-show');
		return false;
	} else {
		$confirm.fadeIn('fast').addClass('is-show');
	}

	$confirmBtn.on('click', function () {
		if (remove) {
			cartUpdate($item, 0, remove);
		} else {
			cartUpdate($item, 0);
		}
	});

	$unConfirmBtn.on('click', function () {
		$confirm.fadeOut('fast').removeClass('is-show');
	});

	$(document).click(function (e) {
		if ($confirm.hasClass('is-show')) {
			if (!$item.is(e.target) && $item.has(e.target).length === 0) {
				$confirm.fadeOut('fast').removeClass('is-show');
			}
		}
	});
}

function cartMinus (e) {
	let $this = $(this);
	let $item = $this.closest('.js-cart-item');
	let $input = $item.find('.js-cart-input');

	let text = $input.val();
	let max = $input.data('max') || 1000000;
	let count = 1;
	if ($.isNumeric(text)) {
		count = parseInt(text);
		count--;
		count = (count > 1) ? ((count > parseInt(max)) ? max : count) : 1;
	}
	$input.val(count);

	cartUpdate($item, count);
}

function cartPlus (e) {
	let $this = $(this);
	let $item = $this.closest('.js-cart-item');
	let $input = $item.find('.js-cart-input');

	let text = $input.val();
	let max = $input.data('max') || 1000000;
	let count = 1;
	if ($.isNumeric(text)) {
		count = parseInt(text);
		count++;
		count = (count > 1) ? ((count > parseInt(max)) ? max : count) : 1;
	}
	$input.val(count);

	cartUpdate($item, count);
}

function cartInput (e) {
	let $this = $(this);
	let $item = $this.closest('.js-cart-item');
	let $input = $item.find('.js-cart-input');

	let text = $input.val();
	let max = $input.data('max') || 1000000;
	if (!$.isNumeric(text) || parseInt(text) < 1) {
		$input.val(1);
	}
	if (parseInt(text) > parseInt(max)) {
		$input.val(max);
	}
	let count = parseInt($input.val());

	cartUpdate($item, count);
}

function cartAdd (e) {
	let $item = $(this).closest('.js-cart-item');
	let count = $item.data('count') || 1;

	cartUpdate($item, count);
}

function cartUpdate ($item, count, remove = null) {
	let url = remove || $item.data('url');
	let staticItem = $item.data('static');
	let sizes = $item.data('sizes');
	let counts = $item.data('counts');
	let id = $item.data('id');
	let preloaderContainer = (staticItem !== undefined) ? $('.js-cart-static') : $('.cart-inner');

	if (url && id) {
		let preloader = new Preloader(preloaderContainer);
		preloader.show();

		let data = {
			id: id
		};

		if (!remove) {
			data.count = count;
		}

		if (sizes !== undefined) {
			data.sizes = sizes;
		}

		if (counts !== undefined) {
			data.counts = counts;
		}

		$.ajax({
			url: url,
			type: 'post',
			data: data,
			dataType: 'json',
			success: function (response) {
				preloader.hide();

				if (response.success) {
					if ($('.js-cart-count').length && typeof response.count !== 'undefined') {
						$('.js-cart-count').html(response.count);
					}

					if ($('.js-cart-price').length && typeof response.price !== 'undefined') {
						$('.js-cart-price').html(response.price);
					}

					if ($('.js-cart-static').length && typeof response.static !== 'undefined') {
						$('.js-cart-static').html(response.static);

						moduleLoader.init($('.js-cart-static'));

						if (!response.count) {
							$('[data-total]').addClass('_hide');
							$('[data-total-empty]').removeClass('_hide');
						}
					}

					if ($('.js-cart').length && typeof response.html !== 'undefined') {
						let $html = $(response.html);
						$('.js-cart').html($html);

						moduleLoader.init($('.js-cart'));

						if (!staticItem) {
							openCart();
						}
					}

					if (response.price) {
						$('.header-cart__summ span b').text(response.price);
					} else {
						$('.header-cart__summ span b').text(0);
					}
				} else {
					if (response.response) {
						showMessage(response.response);
					}
				}
			}
		});
	}
}

function cartClean (e) {
	let url = $(this).data('url');
	let $item = $(this).closest('.cart-window__top');

	let $confirm = $item.find('.js-remove-confirm');
	let $confirmBtn = $confirm.find('.js-confirm');
	let $unConfirmBtn = $confirm.find('.js-unconfirm');

	if ($confirm.hasClass('is-show')) {
		$confirm.fadeOut('fast').removeClass('is-show');
		return false;
	} else {
		$confirm.fadeIn('fast').addClass('is-show');
	}

	$confirmBtn.on('click', function () {
		if (url) {
			$.ajax({
				url: url,
				type: 'post',
				dataType: 'json',
				success: function (response) {
					if (response.success) {
						if ($('.js-cart-count').length && typeof response.count !== 'undefined') {
							$('.js-cart-count').html(response.count);
						}

						if ($('.js-cart-price').length && typeof response.price !== 'undefined') {
							$('.js-cart-price').html(response.price);
						}

						if ($('.js-cart-static').length && typeof response.static !== 'undefined') {
							$('.js-cart-static').html(response.static);

							moduleLoader.init($('.js-cart-static'));

							if (!response.count) {
								$('[data-total]').addClass('_hide');
								$('[data-total-empty]').removeClass('_hide');
							}
						}

						if ($('.js-cart').length && typeof response.html !== 'undefined') {
							let $html = $(response.html);
							$('.js-cart').html($html);

							moduleLoader.init($('.js-cart'));

							openCart();
						}

						if (response.totalPrice) {
							$('.js-total-price').text(response.totalPrice.toFixed(2));
						}
					} else {
						if (response.response) {
							showMessage(response.response);
						}
					}
				}
			});
		}
	});

	$unConfirmBtn.on('click', function () {
		$confirm.fadeOut('fast').removeClass('is-show');
	});

	$(document).click(function (e) {
		if ($confirm.hasClass('is-show')) {
			if (!$item.is(e.target) && $item.has(e.target).length === 0) {
				$confirm.fadeOut('fast').removeClass('is-show');
			}
		}
	});
}

function cartShow (target) {
	let url = $(this).data('url');
	let data = {};
	let bigPopup = $('.sizes-big-popup').length;

	if (url) {
		$.ajax({
			url: url,
			type: 'post',
			data: data,
			dataType: 'json',
			success: function (response) {
				if (response.success) {
					if ($('.js-cart-count').length && typeof response.count !== 'undefined') {
						$('.js-cart-count').html(response.count);
					}

					if ($('.js-cart-price').length && typeof response.price !== 'undefined') {
						$('.js-cart-price').html(response.price);
					}

					if ($('.js-cart-static').length && typeof response.static !== 'undefined') {
						$('.js-cart-static').html(response.static);

						moduleLoader.init($('.js-cart-static'));

						if (!response.count) {
							$('[data-total]').addClass('_hide');
							$('[data-total-empty]').removeClass('_hide');
						}
					}

					if ($('.js-cart').length && typeof response.html !== 'undefined') {
						let $html = $(response.html);
						$('.js-cart').html($html);

						moduleLoader.init($('.js-cart'));

						if (!bigPopup) {
							openCart();
						}
					}

                    if (response.price) {
                        $('.header-cart__summ span b').text(response.price);
                    }

				} else {
					if (response.response) {
						showMessage(response.response);
					}
				}
			}
		});
	}
}

function cartOrder (e) {
	let url = $(this).data('url');
	let $item = $(this).closest('.js-cart-item');
	let id = $item.data('id');
	let count = ($item.data('count') !== undefined) ? $item.data('count') : 1;

	let data = {
		id: id,
		count: count
	};

	$.ajax({
		url: url,
		type: 'post',
		data: data,
		dataType: 'json',
		success: function (response) {
			if (response.success) {
				if ($('.js-cart-count').length && typeof response.count !== 'undefined') {
					$('.js-cart-count').html(response.count);
				}

				if ($('.js-cart-price').length && typeof response.price !== 'undefined') {
					$('.js-cart-price').html(response.price);
				}

				if ($('.js-cart-static').length && typeof response.static !== 'undefined') {
					$('.js-cart-static').html(response.static);

					moduleLoader.init($('.js-cart-static'));

					if (!response.count) {
						$('[data-total]').addClass('_hide');
						$('[data-total-empty]').removeClass('_hide');
					}
				}

				if ($('.js-cart').length && typeof response.html !== 'undefined') {
					let $html = $(response.html);
					$('.js-cart').html($html);

					moduleLoader.init($('.js-cart'));

					openCart();
				}

				if (response.totalPrice) {
					$('.js-total-price').text(response.totalPrice.toFixed(2));
				}
			} else {
				if (response.response) {
					showMessage(response.response);
				}
			}
		}
	});
}

// ----------------------------------------
// Public
// ----------------------------------------

let cartInit = function ($container = $('body')) {
	$container.on('click.cart', '.js-cart-remove', cartRemove);
	$container.on('click.cart', '.js-cart-minus', cartMinus);
	$container.on('click.cart', '.js-cart-plus', cartPlus);
	$container.on('keyup.cart', '.js-cart-input', cartInput);
	$container.on('click.cart', '.js-cart-add', cartAdd);
	$container.on('click.cart', '.js-cart-clean', cartClean);
	$container.on('click', '.js-cart-redact', cartShow);
	$container.on('click', '.js-cart-order', cartOrder);
	$container.on('click', openCartBtn, cartShow);
	$container.on('click', closeCartBtn, closeCart);
};

function cartDefaultClose () {
	$(document).click(function (e) {
		if ($(cartWrap).hasClass('open')) {
			if (!$cartInner.is(e.target) && $cartInner.has(e.target).length === 0 && !$('.mfp-container').is(e.target) && $('.mfp-container').has(e.target).length === 0) {
				closeCart();
			}
		}
	});
}

// ----------------------------------------
// Exports
// ----------------------------------------

export {cartInit, cartDefaultClose};
