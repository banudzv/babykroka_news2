'use strict';

// ----------------------------------------
// Imports
// ----------------------------------------

import cookieData from '#/_modules/cookie-data';

// ----------------------------------------
// Public
// ----------------------------------------

function userTypeInit () {
	$('.js-user-type').on('click', function () {
		let userType = $(this).data('user-type');
		cookieData.add('user-type', userType, {
			expires: 3600 * 24 * 30,
			path: '/'
		});
		window.location.href = window.location.href;
	});
}

// ----------------------------------------
// Exports
// ----------------------------------------

export default userTypeInit;
