'use strict';

// ----------------------------------------
// Public
// ----------------------------------------

function selectWord ($selector, selectClass, word) {
	$selector = $selector || $('.js-select');
	selectClass = selectClass || 'is-select';
	word = word || $('.js-select-word').data('select');

	if ($selector.length && word) {
		$selector.each(function () {
			let $this = $(this);
			let html = $this.html();
			$this.html(html.replace(new RegExp(word, 'ig'), '<span class="' + selectClass + '">$&</span>'));
		});
	}
}

// ----------------------------------------
// Exports
// ----------------------------------------

export {selectWord};
