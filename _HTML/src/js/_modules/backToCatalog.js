'use strict';

// ----------------------------------------
// Imports
// ----------------------------------------

import '#/_vendors/jquery.cookie'
import moduleLoader from "#/module-loader";
import Preloader from "#/_modules/Preloader";

// ----------------------------------------
// Private
// ----------------------------------------

/**
 * @type {Object}
 * @private
 */

export default function backToCatalog() {
	$('.for-cookie').on('click', '.product-item', function (){
		let scroll = $(this).offset().top;
		$.cookie('page_scroll', scroll);

		let startPage = $(".pagination__item.active").first().attr('data-page');
		let finishPage = $(".pagination__item.active").last().attr('data-page');

		$.cookie('page_numberStart', startPage);
		$.cookie('page_numberFinish', ++finishPage);
	});

	if ($.cookie('page_numberFinish') > 0) {

		let $this = $('.js-load-more');
		let $loadMoreWrap = $('.js-load-wrap');
		let preloader = new Preloader($loadMoreWrap);

		let page = $.cookie('page_numberFinish');
		let params = $this.data('params');

		params.startPage =  $.cookie('page_numberStart');;
		params.endPage = --page;

		let url = $this.data('backtocatalogurl')

		$.ajax({
			type: 'post',
			url: url,
			data: {
				page: page,
				params: params
			},
			dataType: 'json',
			success: function (response) {
				if (response.success) {
					$this.data('page', response.page);

					$loadMoreWrap.append(response.html);

					if (response.pagination) {
						$('.js-pagination-container').html(response.pagination);
					}

					if (response.last) {
						$this.hide();
					}

					setTimeout(function () {
						$('html, body').animate({scrollTop: $.cookie('page_scroll')},1000);
						clearCookies();
					}, 500);

					preloader.hide();
				}

				moduleLoader.init($(document));
			}
		});
	}



	$('body').on('click', '.pagination__item', function (){
		clearCookies();
	});

	function clearCookies () {
		$.cookie('page_numberStart', null);
		$.cookie('page_numberFinish', null);
		$.cookie('page_scroll', null);
	}

};
