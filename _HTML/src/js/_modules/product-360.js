'use strict';

// ----------------------------------------
// Imports
// ----------------------------------------

import '../_vendors/threesixty';

// ----------------------------------------
// Private
// ----------------------------------------

let threesixtyFlag = false;
let $productImage = $('.product__gallery');
let $product360 = $('.product__gallery-360');

function threesixtyInit () {
	$('.js-threesixty').each(function (index, el) {
		let $this = $(el);
		let totalFrames = ($this.attr('data-totalFrames')) ? parseInt($this.attr('data-totalFrames')) : 180;
		let imagePath = ($this.attr('data-imagePath')) ? $this.attr('data-imagePath') : 'images/360/';
		let ext = ($this.attr('data-ext')) ? $this.attr('data-ext') : '.png';
		let filePrefix = ($this.attr('data-filePrefix')) ? $this.attr('data-filePrefix') : '';

		let ts = $this.ThreeSixty({
			currentFrame: 1,
			imgList: '.threesixty__images',
			progress: '.threesixty__spinner',
			navigation: false,
			totalFrames: totalFrames,
			// endFrame: endFrame,
			imagePath: imagePath,
			ext: ext,
			filePrefix: filePrefix,
			speedMultiplier: -7,
			// autoplayDirection: -1,
			onReady: function () {
				$('.js-threesixty-buttons').addClass('is-show');

				$this.find('.js-threesixty-next').on('click', function (e) {
					ts.next();
				});
				$this.find('.js-threesixty-prev').on('click', function (e) {
					ts.previous();
				});
				$this.find('.js-threesixty-play').on('click', function (e) {
					ts.play();
				});
				$this.find('.js-threesixty-pause').on('click', function (e) {
					ts.stop();
				});
			}
		});
	});
}

// ----------------------------------------
// Public
// ----------------------------------------

function productMedia () {
	let $mediaLink = $('.js-product-media-link');

	$mediaLink.on('click', function () {
		if (!threesixtyFlag) {
			threesixtyInit();
			threesixtyFlag = true;
		}

		if ($mediaLink.hasClass('is-image')) {
			$mediaLink.removeClass('is-image');
			$productImage.addClass('is-hide');
			$product360.removeClass('is-hide');
		} else {
			$mediaLink.addClass('is-image');
			$productImage.removeClass('is-hide');
			$product360.addClass('is-hide');
		}
	});
}

// ----------------------------------------
// Exports
// ----------------------------------------

export {productMedia};
