'use strict';

/**
 *
 * @module
 */

// ----------------------------------------
// Imports
// ----------------------------------------

import 'custom-jquery-methods/fn/has-inited-key';
import 'magnific-popup';
import './mfp-extend';
import './mfp-extend.scss';
import moduleLoader from '#/module-loader';
import {mainSlider, productPhotoSlider, productSlider} from "#/_modules/sliders";
import zoomit from "#/_modules/zoomit";
import {airDatepicker} from "#/_modules/airDatepicker";

// ----------------------------------------
// Public
// ----------------------------------------

/**
 * @param {JQuery} $elements
 * @sourceCode
 */
function mfpAjax ($elements) {
	$elements.each((i, el) => {
		const $element = $(el);
		if ($element.hasInitedKey('mfpAjax-inited')) {
			return true;
		}

		const config = $.extend(true, {
			type: 'ajax',
			mainClass: 'mfp-animate-zoom-in',
			closeMarkup: `<button class='mfp-close' id='custom-close-btn'></button>`,
			removalDelay: 300,
			autoFocusLast: false,
			callbacks: {
				elementParse (item) {
					const {
						url,
						type = 'POST',
						param: data = {}
					} = item.el.data();
					this.st.ajax.settings = {url, type, data};
				},
				ajaxContentAdded: function () {
					let $container = this.contentContainer || [];
					if ($container) {
						moduleLoader.init($container);

						if ($('.login-popup').length > 0) {
							airDatepicker();

							let rozn = $('input[name="rozn"]'),
								opt = $('input[name="opt"]'),
								drop = $('input[name="drop"]'),
								roznBirth = $('.rozn-birthday')

							function checkReq(input) {
								rozn.prop('required', false);

								if (!rozn.prop('checked') && !opt.prop('checked') && !drop.prop('checked')){
									rozn.prop('required', true);
								}
							}

							rozn.on('click', function () {
								checkReq();
								opt.prop('checked', false);
								drop.prop('checked', false);


								if (rozn.prop('checked')){
									roznBirth.show();
								} else {
									roznBirth.hide().find('input').val('');
								}
							});

							opt.on('click', function () {
								checkReq();
								rozn.prop('checked', false);
								roznBirth.hide().find('input').val('');
							});

							drop.on('click', function () {
								checkReq();
								rozn.prop('checked', false);
								roznBirth.hide().find('input').val('');
							});

							$('.login-popup__submit').on('click', function (){
								if ($("#birthday").val().length || $("#babyName").val().length){
									$("#birthday, #babyName").prop('required', true)
								} else {
									$("#birthday, #babyName").removeAttr('required').removeClass('has-error');
								}
							})
						}

						if ($('.sizes-gallery-popup').length > 0) {

							let $productNavSlider = $('.sizes-gallery-popup').find('.gallery-preview__wrap');

							function productPhotoSlider() {

								let photoSlider = $('.sizes-gallery-popup .product__gallery-main').slick({
									slidesToShow: 1,
									infinite: false,
									fade: true,
									dots: false,
									arrows: false,
									accessibility: false,
									asNavFor: '.sizes-gallery-popup .gallery-preview__wrap'
								});

								$productNavSlider.slick({
									slidesToShow: 4,
									slidesToScroll: 1,
									asNavFor: '.sizes-gallery-popup .product__gallery-main',
									focusOnSelect: true,
									vertical: true,
									infinite: false,
									prevArrow: '<button type="button" class="slick-arrow slick-prev"></button>',
									nextArrow: '<button type="button" class="slick-arrow slick-next"></button>',

									responsive: [
										{
											breakpoint: 768,
											settings: {
												vertical: false,
												slidesToShow: 6,
												infinite: true
											}
										}, {
											breakpoint: 480,
											settings: {
												vertical: false,
												slidesToShow: 3,
												infinite: true
											}
										}
									]
								});

								photoSlider.on('beforeChange', function (event, slick, currentSlide, nextSlide) {

								});

								zoomit();
							}

							productPhotoSlider();

							$(".sizes-gallery-popup ").one('click', function (){
								$('.product__gallery-item img').each(function (){
									let aspectRatio = $(this).width() / $(this).height();
									$(this).data("aspect-ratio", aspectRatio);

									if (aspectRatio > 1) {
										$(this).closest('.product__gallery-item').addClass('landscape');
									} else if (aspectRatio < 1) {
										$(this).closest('.product__gallery-item').addClass('portrait');
									}
								});
							})


						}

						if ($('.sizes-big-popup').length > 0) {
							$('.sizes-big-popup .mfp-close').on('click', function (){
								setTimeout(function (){
									$('.product__gallery-item').trigger('click');
								}, 500)
							})
						}

						if ($('.review-popup').length > 0) {
							airDatepicker();
						}
					}

					$('body').on('click', '.popup-information-block .cart-registration__callback', function (){
						$('.header-phones__callback').trigger('click')
					})
				}
			}
		}, {
			// current element custom config
		});

		$element.magnificPopup(config);
	});
}

/**
 * @param {JQuery} $elements
 * @sourceCode
 */
function mfpIframe ($elements) {
	$elements.each((i, el) => {
		const $element = $(el);
		if ($element.hasInitedKey('mfpIframe-inited')) {
			return true;
		}

		const config = $.extend(true, {
			type: 'iframe',
			removalDelay: 300,
			mainClass: 'mfp-animate-zoom-in',
			closeBtnInside: true
		}, {
			// current element custom config
		});

		$element.magnificPopup(config);
	});
}

/**
 * @param {JQuery} $elements
 * @sourceCode
 */
function mfpInline ($elements) {
	$elements.each((i, el) => {
		const $element = $(el);
		if ($element.hasInitedKey('mfpInline-inited')) {
			return true;
		}

		const config = $.extend(true, {
			type: 'inline',
			removalDelay: 300,
			mainClass: 'mfp-animate-zoom-in',
			closeBtnInside: true
		}, {
			// current element custom config
		});

		$element.magnificPopup(config);
	});
}

/**
 * @param {JQuery} $elements
 * @sourceCode
 */
function mfpGallery ($elements) {
	$elements.each((i, el) => {
		let $element = $(el);
		if ($element.hasInitedKey('mfpGallery-inited')) {
			return true;
		}

		const config = $.extend(true, {
			type: 'image',
			removalDelay: 300,
			mainClass: 'mfp-animate-zoom-in',
			delegate: '[data-mfp-src]',
			closeMarkup: `<button class='mfp-close' id='custom-close-btn'></button>`,
			closeBtnInside: true,
			gallery: {
				enabled: true,
				preload: [0, 2],
				navigateByImgClick: true,
				arrowMarkup: '<button title="%title%" type="button" class="mfp-arrow mfp-arrow-%dir%"></button>'
			}
		}, {
			// current element custom config
		});

		$element.magnificPopup(config);
	});
}

// ----------------------------------------
// Exports
// ----------------------------------------

export {mfpAjax, mfpIframe, mfpInline, mfpGallery};
