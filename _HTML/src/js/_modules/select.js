'use strict';

// ----------------------------------------
// Imports
// ----------------------------------------

import 'select2';
import validate from '#/_modules/jquery-validation/validate-init';

// ----------------------------------------
// Private
// ----------------------------------------

let selectSelector = '.select2';
let wrap;
let placeholder;
let $closeBtn = $('.filter-close-btn');
let isLang = false;

function select2Init (lang, $container = $('body')) {
	$container.find(selectSelector).each(function (index) {
		let $this = $(this);
		wrap = $this.closest('.select2-wrap');
		placeholder = $this.data('placeholder');
		let $form = $this.closest('.js-form');

		if ($this.hasClass('ajaxSelect2')) {
            $this.select2({
                minimumResultsForSearch:  1,
                dropdownParent: wrap,
                placeholder: placeholder,
                width: 'resolve',
                language: lang,
                ajax: {
                    url: $this.data('url'),
                    type: 'POST',
                    data: function (params) {
                        let query = {
                            search: params.term,
                            cityRef: $('.js-courier-city').val()
                        }

                        return query;
                    },
                    processResults: function (data) {
                        return {
                            results: data.result,
                        };
                    }
                }
            });
        } else if ($this.hasClass('ajaxSelect3')) {
            $this.select2({
                minimumResultsForSearch:  1,
                dropdownParent: wrap,
                placeholder: placeholder,
                width: 'resolve',
                language: lang,
                ajax: {
                    url: $this.data('url'),
                    type: 'POST',
                    data: function (params) {
                        let query = {
                            search: params.term,
                            cityRef: $('.js-courier-city-logged').val()
                        }

                        return query;
                    },
                    processResults: function (data) {
                        return {
                            results: data.result,
                        };
                    }
                }
            });
        } else {
            $this.select2({
                minimumResultsForSearch: 1,
                dropdownParent: wrap,
                placeholder: placeholder,
                width: 'resolve',
                language: lang
            });
			$('.js-delivery-np-home').val('');
			$('.js-delivery-np-street').val('');
        }

		$this.on('select2:select', function () {
			let $container = $this.next('.select2-container');
			let $wrap = $container.closest('.select2-wrap');
			$container.addClass('selected');
			$wrap.addClass('selected');

			const $form = $(this.form);
			const Validator = $form.data('validator');
			if (Validator) {
				Validator.element($this);
			}
		});

		$this.on('select2:unselect', function () {
			let $container = $this.next('.select2-container');
			let $wrap = $container.closest('.select2-wrap');
			$container.removeClass('selected');
			$wrap.removeClass('selected');

			const $form = $(this.form);
			const Validator = $form.data('validator');
			if (Validator) {
				Validator.element($this);
			}
		});
	});
}

// ----------------------------------------
// Public
// ----------------------------------------

function initSelect ($container = $('body')) {
	let lang = $('html').attr('lang');
	if (lang && !isLang) {
		isLang = true;
		$.getScript('/Media/assets/js/static/select2/' + lang + '.js', function () {
			select2Init(lang, $container);
		});
	} else {
		select2Init(lang, $container);
	}

	$closeBtn.on('click', function () {
		let $parent = $(this).closest('.select2-wrap');
		let $selected = $parent.find(selectSelector);
		$selected.val('').trigger('change');
		$selected.trigger('select2:unselect');
	});
}

// ----------------------------------------
// Exports
// ----------------------------------------

export {initSelect};
