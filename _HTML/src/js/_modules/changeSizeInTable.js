'use strict';

// Public
// ----------------------------------------

function changeSizeInTable () {
	 $('body').on('click', '.table-size__popup .product__sizes-item', function (){
		 let tabName = $(this).text();
		 $('.table-size__popup .product__sizes-item').removeClass('is-active')

		 $('.tab').hide();
		 $(this).addClass('is-active');

		 $('.tab-'+tabName).show()
	 })
}

// ----------------------------------------
// Exports
// ----------------------------------------

export {changeSizeInTable};
