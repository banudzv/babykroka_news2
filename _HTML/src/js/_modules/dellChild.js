'use strict';

// ----------------------------------------
// Imports
// ----------------------------------------



// ----------------------------------------
// Public
// ----------------------------------------

function dellChild () {
	$("body").on('click', '.js-dellInfo', function (event) {
		event.preventDefault();
		let id = $(this).closest('.form-group').find('input[data-dell-id]').data('dell-id');
		let action =  $(this).closest('.form-group').data('ajax');

		$.ajax({
			url: action,
			type: 'POST',
			dataType: 'JSON',
			data: {
				id:id
			},
			success: function (data) {
				if (data.success) {
					if (data.response) {
						generate(data.response, 'success');
					}
					if (data.reload) {
						window.location.reload();
					} else {
						preloader();
					}
				} else {
					if (data.response) {
						generate(data.response, 'warning');
					}
					preloader();
				}
			}
		});
	});
}

// ----------------------------------------
// Exports
// ----------------------------------------

export {dellChild};

