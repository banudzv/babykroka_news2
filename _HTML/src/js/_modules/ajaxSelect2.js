'use strict';

// ----------------------------------------
// Imports
// ----------------------------------------

import 'select2';
import validate from '#/_modules/jquery-validation/validate-init';

// ----------------------------------------
// Private
// ----------------------------------------

let selectSelector = '.ajaxSelect2';

function initAjaxSelect2($container = $('body')) {
	$container.find(selectSelector).each(function (index) {
		let $this = $(this);

		$this.select2({
			ajax: {
				url: $this.data('url'),
				data: function (params) {
					var query = {
						search: params.term,
						cityRef: $this.data['city']
					}

					return query;
				}
			}
		});
	});
}

// ----------------------------------------
// Exports
// ----------------------------------------

export default initAjaxSelect2;