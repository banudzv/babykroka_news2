'use strict';

// ----------------------------------------
// Private
// ----------------------------------------

let header = $('.header__top'); // Меню
let scrollPrev = 0; // Предыдущее значение скролла
let $menuChildItem = $('.header-menu__item.child');
let body = $('body');

// ----------------------------------------
// Public
// ----------------------------------------

function fixedHeader () {
	body.attr('data-param', $('.header').height());

	if ($(window).width() > 1023  && body.hasClass('fx')) {
		$(window).scroll(function () {
			let scrolled = $(window).scrollTop(); // Высота скролла в px
			let firstScrollUp = false; // Параметр начала сколла вверх
			let firstScrollDown = false; // Параметр начала сколла вниз
			let catalogMenu = $('.js-catalog');
			let height = body.attr('data-param');

			// Если скроллим
			if (scrolled > 0) {
				// Если текущее значение скролла > предыдущего, т.е. скроллим вниз
				if (scrolled > scrollPrev) {
					firstScrollUp = false; // Обнуляем параметр начала скролла вверх
					// Если меню видно
					if (scrolled < header.height() + header.offset().top) {
						// Если только начали скроллить вниз
						if (firstScrollDown === false) {
							let topPosition = header.offset().top; // Фиксируем текущую позицию меню
							header.removeClass('__fixed');
							header.css({
								'top': '0px'
							});
							firstScrollDown = true;
						}

						// Если меню НЕ видно
					} else {
						// Позиционируем меню фиксированно вне экрана
						header.css({
							'top': '-' + header.height() + 'px'
						});
					}

					// Если текущее значение скролла < предыдущего, т.е. скроллим вверх
				} else {
					firstScrollDown = false; // Обнуляем параметр начала скролла вниз
					// Если меню не видно
					if (scrolled > header.offset().top) {
						// Если только начали скроллить вверх
						if (firstScrollUp === false) {
							let topPosition = header.offset().top; // Фиксируем текущую позицию меню
							header.addClass('__fixed');
							header.css({
								'top': '0px'
							});
							firstScrollUp = true;
						}

					}
				}
				// Присваеваем текущее значение скролла предыдущему
				scrollPrev = scrolled;
			} else {
				header.removeClass('__fixed');
				header.css({
					'top': '0px'
				});
			}

			catalogMenu.css('top', header.height());

			if (header.hasClass('__fixed')) {
				body.css('padding-top', height + 'px');
			} else {
				body.css('padding-top', 0);
			}
		});
	}
}

function subMenuShow () {
	$menuChildItem.on('mouseenter', function (e) {
		let $target = $(this);
		let speed = $target.hasClass('child') ? 0 : 400;
		$('.header-menu__child-wrap').each(function () {
			$(this).stop().css('display', 'none');
		});
		$target.find('.header-menu__child-wrap').fadeIn(speed);
	}).on('mouseleave', function () {
		let $subMenu = $(this).find('.header-menu__child-wrap');
		$subMenu.stop().fadeOut(150);
	});
}

// ----------------------------------------
// Exports
// ----------------------------------------

export {fixedHeader, subMenuShow};
