'use strict';

// Public
// ----------------------------------------

function catalogSelect() {
	let $selectWrap = $('.catalog__items-select-box');
	let $select = $selectWrap.find('.select2');

	$select.on('select2:select', function (e) {
		let $this = $(this);
		let val = $this.val();
		if (val) {
			window.location.href = val;
		}
	});
}

function catalogMenu() {
    $(window).on('load', function () {
        if (screen.width > 1024) {

			let headerHeight = $('.header__top').height();

        	$(window).scroll(function (){
				headerHeight = $('.header__top').height();
			});

            const $trigger = $('.js-catalog-button');
            const $catalog = $('.js-catalog');

            $trigger.on('click', () => {
                $catalog.css({ 'min-height': `calc(100vh - ${headerHeight}px)`, 'top' : `${headerHeight}px`});
                $('.catalog').append('<div class="lightbox"></div>');
                $('body').addClass('overflow-hidden');
                $catalog.fadeIn();
            })

            $(document).mouseup(function (e){
                if (!$catalog.is(e.target) && $catalog.has(e.target).length === 0) {
                    $('.lightbox').remove();
                    $catalog.fadeOut();
					$('body').removeClass('overflow-hidden');
                }
            });
        } else {
           let $trigger = $('.js-mob-menu');
           let $catalog = $('.js-mob-catalog');
           let $headerTop = $('.header__top-content--mob');

           $trigger.click(function () {
               $(this).toggleClass('active');
               $headerTop.toggleClass('active');
               $catalog.fadeToggle();
           })
        }
    })
}

function catalogMenuItem() {
    $(window).on('load', function () {
        if (screen.width > 1024) {
            let headerHeight = $('.header').height();
            const $trigger = $('[data-catalog-item]');
            const $catalog = $('.js-catalog-category');
            const catalogSub = $('[data-sub]');

            $trigger.click(function() {
                let targetCategory = $(this).data('catalog-item');

                $(this).addClass('active').siblings().removeClass('active');
                catalogSub.fadeOut();
                $('.catalog').append('<div class="lightbox-category"></div>');
                $catalog.css({ 'min-height': `calc(100vh - ${headerHeight}px)`}).fadeIn();
                $('.catalog-menu__sub[data-sub=' + targetCategory + ']').fadeIn();
                return false;
            });

            $(document).mouseup(function (e){
                if (!$catalog.is(e.target)
                    && $catalog.has(e.target).length === 0) {
                    $('.lightbox-category').remove();
                    $trigger.removeClass('active');
                    $catalog.fadeOut();
                }
            });
        } else {
            let $catalogItem = $('[data-catalog-item]');
            let $catalogItemChildren = $('.catalog__item-children');
            let $backBtn = $('.catalog__item-back');

            $catalogItem.on('click', function () {
                $(this).siblings($catalogItemChildren).addClass('active');
            })

            $backBtn.on('click', function () {
                $(this).closest($catalogItemChildren).removeClass('active');
            })
        }
    })
}

function catalogClose() {
    let $trigger = $('.js-close');
    let $catalog = $('.catalog-menu');

    $trigger.click(function () {
        $catalog.fadeOut();
        $('.lightbox').remove();
        $('.lightbox-category').remove();
		$('body').removeClass('overflow-hidden');

	})
}

// ----------------------------------------
// Exports
// ----------------------------------------

export {catalogSelect, catalogMenu, catalogMenuItem, catalogClose};
