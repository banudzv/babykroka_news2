'use strict';

// ----------------------------------------
// Imports
// ----------------------------------------

import Preloader from '#/_modules/Preloader';

// Public
// ----------------------------------------

function paginationInit () {
	let $wrap = $('.product__reviews');
	let preloader = new Preloader($wrap);

	$('body').on('click', '[data-pagination-item]', function () {
		let $this = $(this);
		let page = $this.data('page');
		let $pagination = $this.closest('.js-pagination');
		let url = $pagination.data('url');
		let id = $pagination.data('id');
		preloader.show();

		$.ajax({
			url: url,
			type: 'post',
			data: {
				page: page,
				id
			},
			dataType: 'json',
			success: function (response) {
				if (response.success) {
					preloader.hide();
					$('[data-pagination-item]').removeClass('active');
					$this.addClass('active');
					$wrap.html(response.html);
				}
			}
		});
	});
}

// ----------------------------------------
// Exports
// ----------------------------------------

export {paginationInit};
