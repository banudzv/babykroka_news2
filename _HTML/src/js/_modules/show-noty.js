'use strict';

// ----------------------------------------
// Imports
// ----------------------------------------

import Noty from '#/_modules/noty/noty-extend';

// ----------------------------------------
// Public
// ----------------------------------------

function messageInit () {
	window.generate = function (message, type, time) {
		const noty = new Noty({
			type: type === 'success' ? 'info' : 'error',
			text: message,
			timeout: time
		});
		noty.show();
	};
}

// ----------------------------------------
// Exports
// ----------------------------------------

export default messageInit;
