'use strict';

// ----------------------------------------
// Private
// ----------------------------------------

let $map = $('.map');
let $clickTarget = $('.contacts-item__address-box');
let icon = '/Media/assets/css/static/pic/marker-icon.png';
let mapStyle = [
	{
		'featureType': 'water',
		'elementType': 'geometry.fill',
		'stylers': [
			{
				'color': '#d3d3d3'
			}
		]
	},
	{
		'featureType': 'transit',
		'stylers': [
			{
				'color': '#808080'
			},
			{
				'visibility': 'off'
			}
		]
	},
	{
		'featureType': 'road.highway',
		'elementType': 'geometry.stroke',
		'stylers': [
			{
				'visibility': 'on'
			},
			{
				'color': '#b3b3b3'
			}
		]
	},
	{
		'featureType': 'road.highway',
		'elementType': 'geometry.fill',
		'stylers': [
			{
				'color': '#ffffff'
			}
		]
	},
	{
		'featureType': 'road.local',
		'elementType': 'geometry.fill',
		'stylers': [
			{
				'visibility': 'on'
			},
			{
				'color': '#ffffff'
			},
			{
				'weight': 1.8
			}
		]
	},
	{
		'featureType': 'road.local',
		'elementType': 'geometry.stroke',
		'stylers': [
			{
				'color': '#d7d7d7'
			}
		]
	},
	{
		'featureType': 'poi',
		'elementType': 'geometry.fill',
		'stylers': [
			{
				'visibility': 'on'
			},
			{
				'color': '#ebebeb'
			}
		]
	},
	{
		'featureType': 'administrative',
		'elementType': 'geometry',
		'stylers': [
			{
				'color': '#a7a7a7'
			}
		]
	},
	{
		'featureType': 'road.arterial',
		'elementType': 'geometry.fill',
		'stylers': [
			{
				'color': '#ffffff'
			}
		]
	},
	{
		'featureType': 'road.arterial',
		'elementType': 'geometry.fill',
		'stylers': [
			{
				'color': '#ffffff'
			}
		]
	},
	{
		'featureType': 'landscape',
		'elementType': 'geometry.fill',
		'stylers': [
			{
				'visibility': 'on'
			},
			{
				'color': '#efefef'
			}
		]
	},
	{
		'featureType': 'road',
		'elementType': 'labels.text.fill',
		'stylers': [
			{
				'color': '#696969'
			}
		]
	},
	{
		'featureType': 'administrative',
		'elementType': 'labels.text.fill',
		'stylers': [
			{
				'visibility': 'on'
			},
			{
				'color': '#737373'
			}
		]
	},
	{
		'featureType': 'poi',
		'elementType': 'labels.icon',
		'stylers': [
			{
				'visibility': 'off'
			}
		]
	},
	{
		'featureType': 'poi',
		'elementType': 'labels',
		'stylers': [
			{
				'visibility': 'off'
			}
		]
	},
	{
		'featureType': 'road.arterial',
		'elementType': 'geometry.stroke',
		'stylers': [
			{
				'color': '#d6d6d6'
			}
		]
	},
	{
		'featureType': 'road',
		'elementType': 'labels.icon',
		'stylers': [
			{
				'visibility': 'off'
			}
		]
	},
	{},
	{
		'featureType': 'poi',
		'elementType': 'geometry.fill',
		'stylers': [
			{
				'color': '#dadada'
			}
		]
	}
];

function setPlace (latitude, longitude) {
	let place = new google.maps.LatLng(latitude, longitude); // eslint-disable-line no-undef

	return place;
}

// ----------------------------------------
// Public
// ----------------------------------------

function initMap () {
	if (!$map.length) {
		return false;
	}

	let location = {
		latitude: '',
		longitude: ''
	};

	$clickTarget.on('click', function () {
		let $mapWrap = $(this).closest('.contacts-item__top-wrap').next('.contacts-item__bottom');
		let $mapTop = $(this).closest('.contacts-item__top-wrap');
		let _map = $mapWrap.find('.map');
		let index = $mapTop.data('index');

		$mapWrap.slideToggle('fast');
		$mapTop.toggleClass('open');

		location = {
			latitude: _map.data('lat'),
			longitude: _map.data('lng')
		};

		let map = new google.maps.Map(document.getElementsByClassName('map')[index], { // eslint-disable-line no-undef
			zoom: 15,
			center: setPlace(location.latitude, location.longitude),
			scrollwheel: false,
			styles: mapStyle
		});

		let marker = new google.maps.Marker({ // eslint-disable-line no-undef, no-unused-vars
			position: setPlace(location.latitude, location.longitude),
			map: map,
			optimized: false,
			icon: icon,
			title: ''
		});
	});
}

// ----------------------------------------
// Exports
// ----------------------------------------

export {initMap};
