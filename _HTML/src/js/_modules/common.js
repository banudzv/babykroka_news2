'use strict';

// ----------------------------------------
// Imports
// ----------------------------------------

import Preloader from '#/_modules/Preloader';
import moduleLoader from "#/module-loader";
import '#/_vendors/jquery.cookie'
import {productImgSlider} from '#/_modules/sliders';
import {initSelect} from "./select";

// ----------------------------------------
// Private
// ----------------------------------------

let $cartInfoRadio = $('.cart-info__radio');
let $cartDeliveryContent = $('.cart-info__delivery-content');
// let $productMinus = $('.js-product-minus');
// let $productPlus = $('.js-product-plus');

// Thousand separator

function setThousands(numberText, newSeparator, separator) {
	newSeparator = newSeparator || '.';
	separator = separator || '.';
	numberText = '' + numberText;
	numberText = numberText.split(separator);

	let numberPenny = numberText[1] || '';
	let numberValue = numberText[0];

	let thousandsValue = [];
	let counter = 0;

	for (let i = numberValue.length - 1; i >= 0; i--) {
		let num = numberValue[i];
		thousandsValue.push(num);

		if (++counter === 3 && i) {
			thousandsValue.push(' ');
			counter = 0;
		}
	}

	thousandsValue = thousandsValue.reverse().join('');
	if (numberPenny.length) {
		return [thousandsValue, numberPenny].join(newSeparator);
	}
	return thousandsValue;
}

function toggle(that) {
	let _this = that;

	$cartDeliveryContent.slideUp('fast');

	if ($(_this).prop('checked')) {
		let $content = $(_this).parent().next('.cart-info__delivery-content');

		$content.slideDown('fast');
	}
}

function counterUpdate($item, val) {
	let $input = $item.find('.js-product-input');
	let $count = $item.find('.js-product-count');
	let $countItem = $item.find('.js-product-count-item');
	let $priceAll = $item.find('.js-product-price');
	let $countAll = $item.find('.js-product-count-all');

	let wholesale = $item.hasClass('wholesale');
	let maxCountAll = parseInt($input.data('max'));
	let countOldAll = $.isNumeric($input.val()) ? parseInt($input.val()) : 1;
	let cost = parseFloat($priceAll.data('price'));
	let count = 0;
	let countNewAll = countOldAll;

	$('.js-product-plus').on('click', function () {
		let thisPlus = $(this).siblings('.js-product-input');

		if (thisPlus.val() == thisPlus.data('max')) {
			let maxNoty = $('.maximum-noty');
			maxNoty.show();

			setTimeout(function () {
				maxNoty.hide();
			}, 2000)
		}
	})

	if ($count.length) {
		let minCountAll = (wholesale && $count.length) ? $count.length : 1;
		let counts = [];

		$count.each(function () {
			let $countItem = $(this);
			let countOld = parseInt($countItem.text());
			let maxCount = parseInt($countItem.data('max'));
			let countNew = countOld;

			if (val === 'plus') {
				countNew = countOld < maxCount ? (countOld + 1) : maxCount;
			} else if (val === 'minus') {
				countNew = countOld > 1 ? (countOld - 1) : (maxCount ? 1 : 0);
			} else {
				countNew = (countOldAll > maxCount) ? maxCount : ((countOldAll < 1) ? 1 : countOldAll);
			}

			count += countNew;

			counts.push(countNew + '');

			$countItem.html(countNew ? countNew : ''); // eslint-disable-line no-unneeded-ternary
		});

		$countItem.each(function () {
			let $countItem = $(this);
			let param = $countItem.data('param');
			param.param.counts = counts;
			$countItem.data('param', param);
		});

		if (val === 'plus') {
			countNewAll = countOldAll < maxCountAll ? (countOldAll + 1) : maxCountAll;
		} else if (val === 'minus') {
			countNewAll = countOldAll > 1 ? (countOldAll - 1) : minCountAll;
		} else {
			countNewAll = (countOldAll > maxCountAll) ? maxCountAll : ((countOldAll < 1) ? minCountAll : countOldAll);
		}

		$input.val(count);

		$item.data('counts', counts);
		$item.data('count', countNewAll);

		$priceAll.html(setThousands((count * cost).toFixed(2)));
		$countAll.html(count);
	} else {
		if (wholesale) {
			$item.closest('form').find('.js-product-input').each(function () {
				let $th = $(this);

				let maxCountItem = parseInt($th.data('max'));
				let countNewItem = countOldAll;
				let countNow = $th.val();

				if (val === 'plus') {
					countNewItem = ++countNow < maxCountItem ? countNow : maxCountItem;
				} else if (val === 'minus') {
					countNewItem = --countNow > 0 ? countNow : 0;
				} else {
					countNewItem = (countOldAll > maxCountItem) ? maxCountItem : ((countOldAll < 0) ? 0 : countOldAll);
				}

				$th.val(countNewItem);
			});
		}

		if (val === 'plus') {
			countNewAll = countOldAll < maxCountAll ? (countOldAll + 1) : maxCountAll;
		} else if (val === 'minus') {
			countNewAll = countOldAll > 0 ? (countOldAll - 1) : 0;
		} else {
			countNewAll = (countOldAll > maxCountAll) ? maxCountAll : ((countOldAll < 0) ? 0 : countOldAll);
		}

		$input.val(countNewAll);

		$item.data('counts', countNewAll);
		$item.data('count', countNewAll);

		$priceAll.html(setThousands((countNewAll * cost).toFixed(2)));
		$countAll.html(countNewAll);
	}

	if ($input.val() > 0) {
		$('body').on('click', '.js-accept', function () {
			setTimeout(function () {
				$('.js-accept').closest('.sizes-popup').find('.mfp-close').trigger('click');
				$('.header-cart__btn.js-cart-open').trigger('click');
			}, 1000)
		})
	}
}

$('body').on('click change', '.js-form-trigger', function () {
	let $this = $(this);
	let $item = $this.closest('.js-product-item');
	let $itemInput = '.js-product-input';

	if ($this.hasClass('js-product-minus')) {
		counterUpdate($item, 'minus');
		$this.closest('form').submit();
	}

	if ($this.hasClass('js-product-plus')) {

		if ($this.siblings($itemInput).val() == $this.siblings($itemInput).data('max')) {

			let maxNoty = $('.maximum-noty');
			maxNoty.show();

			counterUpdate($item, false);

			setTimeout(function () {
				maxNoty.hide();
			}, 2000)

		} else {
			counterUpdate($item, 'plus');
			$this.closest('form').submit();
		}

	}

});

$('.child-creation__button').on('click', function (){
	let wrapper = $('.child-selection__wrap');
	let btn = $('.child-creation__button');

	if (wrapper.hasClass('active')) {
		wrapper.removeClass('active');
		wrapper.slideUp(500);
		btn.css('border-color', '#a1a1a1');
	} else {
		wrapper.addClass('active');
		wrapper.slideDown(500);
		btn.css('border-color', '#f8d4cd');


		initSelect();

		$('.selection-show').find('select').each(function (i,e){
			$(e).select2({
				minimumResultsForSearch: Infinity,
				dropdownParent: $(e).closest('.mfp-container'),
				width: 'resolve',
				language: $('html').attr('lang'),
				positionDropdown: false
			});
		})
	}
});

$.magnificPopup.instance.close = function () {
	$('.child-selection__wrap').removeClass('active');
	$('.child-creation__button').css('border-color', '#a1a1a1');
	$.magnificPopup.proto.close.call(this);
};

$('body').on('click', '.child-selection__tab', function (){
	let contentId = $(this).attr('data-id');
	let content = $(this).parents('.child-selection__wrap').find("[data-content-id='" + contentId + "']");

	$(this).parents('.child-selection__gender').find('.active').removeClass('active');
	$(this).addClass('active');
	$('.child-age').removeClass('is-active');
	content.addClass('is-active');

	initSelect();

	$('.selection-show').find('select').each(function (i,e){
		$(e).select2({
			minimumResultsForSearch: Infinity,
			dropdownParent: $(e).closest('.mfp-container'),
			width: 'resolve',
			language: $('html').attr('lang'),
			positionDropdown: false
		});
	})
});

$('body').on('change', '.child-selection__content', function (e){
	e.preventDefault();

	let $filterWrapper = $(this).parents('.section').find('.main-filter-wrapper');

	if ($(window).width() < 1024) {
		$filterWrapper = $(this).parents('body').find('.catalog__filter--mobile');
	}

	let category_id = $filterWrapper.data('category');
	let filters = {};

	$filterWrapper.find('.filter-input:checked').each(function() {
		let name = $(this).attr('name');
		if (!filters.hasOwnProperty(name)) filters[name] = [];

		let value = $(this).val();

		if (!filters[name].includes(value)) filters[name].push(value);
	});

	let $minRangeInput = $filterWrapper.find('.min-cost');
	let $maxRangeInput = $filterWrapper.find('.max-cost');
	let minRangeCost = $minRangeInput.val();
	let maxRangeCost = $maxRangeInput.val();
	let minPrice = $minRangeInput.data('min');
	let maxPrice = $maxRangeInput.data('max');
	if (minPrice < minRangeCost || maxPrice > maxRangeCost) {
		filters['mincost'] = minRangeCost;
		filters['maxcost'] = maxRangeCost;
	}

	let sex = $('.child-selection__tab.active').attr('data-value');
	let age = $('.child-age.is-active').find('.age-selection').val();

	filters['polrebenka'] = [sex];
	filters['age'] = [age];

	let search = null;
	const params = new URLSearchParams(window.location.search);
	if (params.has('search')) search = params.get('search');

	$.ajax({
		type: 'post',
		url: '/ajax/prepareFilter',
		data: {
			category_id,
			filters,
			search
		},
		dataType: 'json',
		success: function(response) {
			if (response.success && response.url) {
				window.location.href = response.url;
			}
		}
	});
});

$('.second__block').on('click', function (){
	let parent = $(this).parents('.delivery-info__item');
	let content = parent.find('.delivery-info__content');
	let span = parent.find('.open_delivery span');

	if (content.hasClass('none__block')){
		content.removeClass('none__block')
		content.slideDown(500);
		$(this).addClass('opened');
		span.html('-');
	} else {
		content.addClass('none__block');
		content.slideUp(500);
		$(this).removeClass('opened');
		span.html('+');
	}

});

$('.filters__item').on('click', 'a', function(event) {

	if (!$(this).hasClass('filters__child-name')) {
		event.preventDefault();
		if ($(this).hasClass('active')) {
			$(this).removeClass('active')
		} else {
			$(this).addClass('active');
		}

		let $filterWrapper = $(this).parents('.main-filter-wrapper');
		let $counter = $('.filter-summary-wrapper');
		let $input = $(this).parents('.filters__item');
		let position = $input.offset();

		let category_id = $filterWrapper.data('category');
		let filters = {};

		$filterWrapper.find('.active').each(function () {
			let name = $(this).attr('data-type');
			if (!filters.hasOwnProperty(name)) filters[name] = [];

			let value = $(this).attr('data-value');

			if (!filters[name].includes(value)) filters[name].push(value);
		});

		let $minRangeInput = $filterWrapper.find('.min-cost');
		let $maxRangeInput = $filterWrapper.find('.max-cost');
		let minRangeCost = $minRangeInput.val();
		let maxRangeCost = $maxRangeInput.val();
		let minPrice = $minRangeInput.data('min');
		let maxPrice = $maxRangeInput.data('max');
		if (minPrice < minRangeCost || maxPrice > maxRangeCost) {
			filters['mincost'] = minRangeCost;
			filters['maxcost'] = maxRangeCost;
		}

		let search = null;
		const params = new URLSearchParams(window.location.search);
		if (params.has('search')) search = params.get('search');

		if ($.isEmptyObject(filters) && !$('.catalog__chosen-item').length) {
			$counter.css('display', 'none');
			return;
		}

		$.ajax({
			type: 'post',
			url: '/ajax/prepareFilter',
			data: {
				category_id,
				filters,
				search
			},
			dataType: 'json',
			success: function (response) {
				if (response.success) {
					$counter.find('.count').text(response.count);
					$counter.find('.summary-submit').attr('data-url', response.url);

					if ($(window).width() > 1023) {
						$counter.css({
							'left': parseInt(position.left + $input.width()) - $counter.width() / 2,
							'top': position.top,
							'display': 'block'
						});
					}

				}
			}
		});
	}
});

$('.summary-submit').on('click', function() {
	location.href = $(this).data('url');
});

$('select[name=per-page]').on('change', function() {
	let defaultLimit = $(this).data('default_limit');
	let queryParams = new URLSearchParams(location.search);
	let count = $(this).val();

	if (count != defaultLimit) {
		queryParams.set('per_page', count);
	} else {
		queryParams.delete('per_page');
	}

	let query = '?' + queryParams.toString();

	$('.js-load-more').data('limit', count);
	location.href = $(this).data('base') +  query;
});

function productMinus(e) {
	let $this = $(this);
	let $item = $this.closest('.js-product-item');

	counterUpdate($item, 'minus');
}

function productPlus(e) {
	let $this = $(this);
	let $item = $this.closest('.js-product-item');

	counterUpdate($item, 'plus');
}

function productInput(e) {
	let $this = $(this);
	let $item = $this.closest('.js-product-item');

	counterUpdate($item);

	$this.keypress(function (event) {
		let key;
		let keyChar;

		if (event.keyCode) {
			key = event.keyCode;
		} else if (event.which) {
			key = event.which;
		}

		if (key === null || key === 0 || key === 8 || key === 13 || key === 9 || key === 46 || key === 37 || key === 39) {
			return true;
		}
		keyChar = String.fromCharCode(key);

		if (!/^[0-9]+$/.test(keyChar)) {
			return false;
		}
	});
}

function productClean(e) {
	let $this = $(this);
	let $item = $this.closest('.js-product-item');
	let wholesale = $item.hasClass('wholesale');
	let $all = $this.closest('.js-product-all');
	let $input = $item.find('.js-product-input');

	if ($this.hasClass('all')) {
		let $input = $all.find('.js-product-input');

		$input.each(function (item) {
			$(this).val(wholesale ? 1 : 0);
		});
	} else {
		$input.val(wholesale ? 1 : 0);
	}
}

// ----------------------------------------
// Public
// ----------------------------------------

function addToFavorite() {
	$('body').on('click', '.js-favorite-btn', function () {
		let $this = $(this);
		let $item = $this.closest('.js-cart-item');
		let id = $item.data('id');
		let url = $this.data('url');

		$.ajax({
			url: url,
			type: 'post',
			data: {
				id: id
			},
			dataType: 'json',
			success: function (response) {
				if (response.success) {
					$('.js-favorite-btn').each(function () {
						let $th = $(this);
						let $it = $th.closest('.js-cart-item');
						if ($it.data('id') === $item.data('id')) {
							if (response.add) {
								$th.addClass('active');
							} else if (response.remove) {
								$th.removeClass('active');
							}
						}
					});

					$('.js-favorite-count').text(response.count);
				}
			}
		});
	});
}

function addToCompare() {
	$('body').on('click', '.js-compare-btn', function () {
		let $this = $(this);
		let $item = $this.closest('.js-cart-item');
		let id = $item.data('id');
		let url = $this.data('url');

		$.ajax({
			url: url,
			type: 'post',
			data: {
				id: id
			},
			dataType: 'json',
			success: function (response) {
				if (response.success) {
					$('.js-compare-btn').each(function () {
						let $th = $(this);
						let $it = $th.closest('.js-cart-item');
						if ($it.data('id') === $item.data('id')) {
							if (response.add) {
								$th.addClass('active');
							} else if (response.remove) {
								$th.removeClass('active');
							}
						}
					});

					$('.js-compare-count').text(response.count);
				}
			}
		});
	});
}

function likeAdd() {
	$('body').on('click', '.js-review-like', function () {
		let $this = $(this);
		let $item = $this.closest('.js-review');
		let id = $item.data('id');
		let url = $this.data('url');
		let $dislike = $item.find('.js-review-dislike');
		let $counter = $this.find('.js-like-count');
		let $dislikeCounter = $item.find('.js-dislike-count');

		$.ajax({
			url: url,
			type: 'post',
			data: {
				id: id
			},
			dataType: 'json',
			success: function (response) {
				if (response.success) {
					if (response.add) {
						$dislike.removeClass('active');
						$this.addClass('active');
					} else if (response.remove) {
						$this.removeClass('active');
					}

					$counter.text(response.likeCount);
					$dislikeCounter.text(response.dislikeCount);
				}
			}
		});
	});
}

function dislikeAdd() {
	$('body').on('click', '.js-review-dislike', function () {
		let $this = $(this);
		let $item = $this.closest('.js-review');
		let id = $item.data('id');
		let url = $this.data('url');
		let $like = $item.find('.js-review-like');
		let $counter = $this.find('.js-dislike-count');
		let $likeCounter = $item.find('.js-like-count');

		$.ajax({
			url: url,
			type: 'post',
			data: {
				id: id
			},
			dataType: 'json',
			success: function (response) {
				if (response.success) {
					if (response.add) {
						$like.removeClass('active');
						$this.addClass('active');
					} else if (response.remove) {
						$this.removeClass('active');
					}

					$counter.text(response.dislikeCount);
					$likeCounter.text(response.likeCount);
				}
			}
		});
	});
}

function toggleCartInfo() {
	$cartInfoRadio.each(function (index) {
		if (!$(this).prop('checked')) {
			let $content = $(this).parent().next('.cart-info__delivery-content');

			$content.slideUp('fast');
		}
	});

	$cartInfoRadio.on('change', function () {
		toggle(this);
	});
}

function loadMore() {
	let $loadMoreWrap = $('.js-load-wrap');
	let preloader = new Preloader($loadMoreWrap);

	$('.js-load-more').click(function () {

		let $this = $(this);
		let url = $this.data('url');
		let page = $this.data('page');
		let limit = $this.data('limit');
		let params = $this.data('params');

		let startPage = $(".pagination__item.active").first().attr('data-page');
		let finishPage = $(".pagination__item.active").last().attr('data-page');

		$.cookie('page_numberFinish', ++finishPage);

		if (!$.cookie('page_numberStart') || $.cookie('page_numberStart') === 'null') {
			$.cookie('page_numberStart', startPage);
		}


		$this.prop('disabled', true)

		$.ajax({
			type: 'post',
			url: url,
			data: {
				page: page,
				limit,
				params: params
			},
			dataType: 'json',
			success: function (response) {
				if (response.success) {
					$this.data('page', response.page);

					$loadMoreWrap.append(response.html);

					if (response.pagination) {
						$('.js-pagination-container').html(response.pagination);
					}

					if (response.last) {
						$this.hide();
					}

				}

				setTimeout(function () {
					$this.prop('disabled', false);
				}, 500)


				moduleLoader.init($(document));

				productImgSlider();

			}
		});
	});
}

// function cartHolderSize () {
// 	let bottom = $('.cart-window__bottom').outerHeight();
// 	let content = $('.cart-window__content').outerHeight();
// 	let top = $('.cart-window__top').outerHeight();
// 	let holder = $('.cart-holder').outerHeight();
// }

function productCounter() {
	let $container = $('body');
	$container.on('click', '.js-product-minus', productMinus);
	$container.on('click', '.js-product-plus', productPlus);
	$container.on('keyup', '.js-product-input', productInput);
	$container.on('click', '.js-product-clean', productClean);
}


function scrollTop() {
	const $footer = $('.footer');
	const $footerOffsetTop = $footer.offset().top - $footer.height();
	const $trigger = $('.toTop-js');
	const activeClass = 'active';

	$(window).on('scroll', () =>  {
		let $offsetTop = $(document).scrollTop();

		if ($offsetTop > $(window).height() / 3) {
			$trigger.addClass(activeClass);
		} else {
			$trigger.removeClass(activeClass);
		}
		if ( $offsetTop > $footerOffsetTop ) {
			$trigger.removeClass(activeClass);
		}

	});

	$trigger.on("click", () => {
		$('html, body').animate({ scrollTop: 0 }, 600, 'linear');
	});
}

function headerPhonesMob() {
	let $wrap = $('.header-phones__wrap');
	let $trigger = $('.header-phones__svg');

	$trigger.on('click', function() {
		$wrap.addClass('active');
	})

	$(document).mouseup(function (e){
		if (!$wrap.is(e.target)
			&& $wrap.has(e.target).length === 0) {
			$wrap.removeClass('active');;
		}
	});
}

function toggleDeliveryInfo() {
	let $trigger = $('.js-delivery-trigger');
	let $wrap = $('.js-delivery-item');
	let $text = $('.js-delivery-text');

	$trigger.on('click', function () {
		$(this).toggleClass('rotate');
		$(this).closest($wrap).find($text).slideToggle(300);
	})
}
// ----------------------------------------
// Exports
// ----------------------------------------

export {addToFavorite, addToCompare, toggleCartInfo, loadMore, productCounter, dislikeAdd, likeAdd, scrollTop, headerPhonesMob, toggleDeliveryInfo};
