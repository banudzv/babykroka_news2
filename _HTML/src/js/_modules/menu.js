'use strict';

// ----------------------------------------
// Imports
// ----------------------------------------

import 'jquery.mmenu';

// ----------------------------------------
// Private
// ----------------------------------------

let $filtersMenu = $('#filters-menu');
let $openBnt = $('.menu-open');
let $closeBtn = $('.sidebar-close');
let $mobileOpenBtn = $('.mobile-menu-open');
let $mobileCloseBtn = $('.mobile-menu-close');
let $filtersOpenBtn = $('.filters-menu-open');
let $filtersCloseBtn = $('.filters-menu-close');

// ----------------------------------------
// Public
// ----------------------------------------

function mainMenu () {
	let $menu = $('#menu').mmenu({
		'slidingSubmenus': false,
		'extensions': [
			'fx-menu-slide',
			'border-none',
			'pagedim-black'
		],
		'navbar': {
			add: false
		}
	});

	let API = $menu.data('mmenu');

	$openBnt.on('click', function () {
		API.open();
	});

	$closeBtn.click(function () {
		API.close();
	});
}

function mobileMenu () {
	// if ($(window).width() > 1024) {
	// 	return false;
	// }

	let $menu = $('#mobile-menu').mmenu({
		'slidingSubmenus': false,
		'extensions': [
			'border-none',
			'pagedim-black',
			'fx-menu-slide'
		],
		'navbar': {
			add: false
		}
	});

	let API = $menu.data('mmenu');

	$mobileOpenBtn.on('click', function () {
		API.open();
	});

	$mobileCloseBtn.click(function () {
		API.close();
	});
}

function filtersMenu () {
	if (!$filtersMenu.length) {
		return false;
	}

	if ($(window).width() > 1024) {
		return false;
	}

	let $menu = $filtersMenu.mmenu({
		'slidingSubmenus': false,
		'extensions': [
			'border-none',
			'pagedim-black',
			'position-front'
		],
		'navbar': {
			add: false
		}
	});
	$menu.append($('.mobile__summary'));

	let API = $menu.data('mmenu');

	$filtersOpenBtn.on('click', function () {
		API.open();
	});

	$filtersCloseBtn.click(function () {
		API.close();
	});
}

// ----------------------------------------
// Exports
// ----------------------------------------

export {mainMenu, mobileMenu, filtersMenu};
