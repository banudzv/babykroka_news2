'use strict';

/**
 * @fileOverview Основной файл инициализации модулей
 */

// ----------------------------------------
// Imports
// ----------------------------------------

import '#/_vendors/promise-polyfill';
import '#/_vendors/jquery';
import '#/_vendors/gsap/gsap';

import moduleLoader from '#/module-loader';
import wsTabs from '#/_modules/wstabs';
import cookieData from '#/_modules/cookie-data';
import validate from '#/_modules/jquery-validation/validate-init';
import {mainMenu, mobileMenu, filtersMenu} from '#/_modules/menu';
import {mainSlider, productSlider, productPhotoSlider, productImgSlider} from '#/_modules/sliders';
import {subMenuShow} from '#/_modules/fixed-header';
import {initRangeSlider} from '#/_modules/range-slider';
import {initSelect} from '#/_modules/select';
import {subMenuToggle} from '#/_modules/sub-menu';
import {
	addToFavorite,
	toggleCartInfo,
	loadMore,
	productCounter,
	likeAdd,
	dislikeAdd,
	scrollTop,
	headerPhonesMob,
	toggleDeliveryInfo
} from '#/_modules/common';
import {initMap} from '#/_modules/map';
import {cartInit, cartDefaultClose} from '#/_modules/cart';
import {selectWord} from '#/_modules/select-word';
import {headerSearchToggle, searchAutocomplete} from '#/_modules/header-search';
import {productMedia} from '#/_modules/product-360';
import {paginationInit} from '#/_modules/pagination';
import {catalogSelect, catalogMenu, catalogMenuItem, catalogClose} from '#/_modules/catalog-select';
import chooseAjax from '#/_modules/choose-ajax';
import initAjaxSelect2 from '#/_modules/ajaxSelect2';
import showNoty from '#/_modules/show-noty';
import currencyInit from '#/_modules/currency';
import userTypeInit from '#/_modules/user-type';
import payToggle from '#/_modules/pay-info-toggle';
import zoomit from '#/_modules/zoomit';
import backToCatalog from "#/_modules/backToCatalog";
import {airDatepicker} from "#/_modules/airDatepicker";
import {dellChild} from "#/_modules/dellChild";
import {changeSizeInTable} from "#/_modules/changeSizeInTable";
import {clickMe} from "#/_modules/clickMe";
import {deliveryRegistration} from "#/_modules/deliveryRegistration";
import {exportTable} from "#/_modules/export-table";
import {fixedHeader} from "#/_modules/fixed-header";


// ----------------------------------------
// Public
// ----------------------------------------

window.jQuery(document).ready($ => {
	moduleLoader.init($(document));
	cookieData.askUsage(); // если нужно использовать куки
	wsTabs.init();
	// wsTabs.setActive(); // если нужно принудительно активировать табы
	validate($('.js-form'));
	mainMenu();
	mobileMenu();
	airDatepicker();
	mainSlider();
	productImgSlider();
	productSlider();
	initRangeSlider();
	initSelect();
	productPhotoSlider();
	subMenuToggle('.filters__item-top', '.filters__item', '.filters__child-wrap');
	fixedHeader();
	exportTable();
	subMenuShow();
	subMenuToggle('.faq__item-top', '.faq__item', '.faq__item-bottom');
	subMenuToggle('.history__item-top', '.history__item', '.history__item-bottom');
	addToFavorite();
	toggleCartInfo();
	initMap();
	filtersMenu();
	cartInit();
	selectWord();
	loadMore();
	headerSearchToggle();
	searchAutocomplete();
	productMedia();
	productCounter();
	scrollTop();
	headerPhonesMob();
	toggleDeliveryInfo();
	cartDefaultClose();
	likeAdd();
	dislikeAdd();
	paginationInit();
	catalogSelect();
    catalogMenu();
    catalogMenuItem();
    catalogClose();
	chooseAjax();
	initAjaxSelect2();
	showNoty();
	currencyInit();
	userTypeInit();
	payToggle();
	zoomit();
	backToCatalog();
	dellChild();
	changeSizeInTable();
	clickMe();
	deliveryRegistration();
});

if (IS_PRODUCTION) {
	import('#/_modules/wezom-log');
}
