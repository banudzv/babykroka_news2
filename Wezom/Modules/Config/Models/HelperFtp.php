<?php
namespace Wezom\Modules\Config\Models;

use Core\Common;
use Core\Files;
use Core\HTML;
use Core\QB\DB;

/**
 * Class @HelperFtp
 * @package Wezom\Modules\Config\Models
 */
class HelperFtp extends Common {

    public static $table = 'config_centre';

    public static function connect($row,$type,$folder = null){
        $connection = ftp_connect($row->ftp_address,$row->ftp_port ?:21);
        if(!$connection){
            return false;
        }
        $auth = ftp_login($connection,$row->ftp_username,$row->ftp_password);
        if($auth){
            switch($type){
                case 'photo': (new self)->preparePhotos($connection); break;
                case 'file' : (new self)->prepareFiles($connection,$folder); break;
            }

            return $connection;
        }

        self::closeConnection($connection,$type);
        return false;
    }
    public static function getRow($value, $field = 'id', $status = null)
    {
        $result = DB::select()->from(static::$table)->where($field, '=', $value);
        if ($status !== null) {
            $result->where('active', '=', $status);
        }
        return $result->find();
    }

    /**
     * Закрываем соединение с FTP-сервером
     * @link https://www.php.net/manual/ru/function.ftp-close.php
     * @param $connection
     * @param $type
     * @param null|string $folder
     */
    public static function closeConnection($connection, $type, $folder = null): void
    {
        if(CRON_MODE !== 'develop'){
            switch($type){
                case 'photo': (new self)->removePhotos($connection); break;
                case 'file' : (new self)->removeFiles($connection,$folder); break;
            }
        }
        ftp_close($connection);
    }

    /**
     * @param $connection
     * @param $fold
     */
    private function prepareFiles($connection, $fold): void
    {
        $folders = ftp_nlist($connection, ".");
        if(!empty($folders)){
            foreach ($folders as $folder){
                if($folder === $fold){

                    $directory = ftp_nlist($connection, $folder);
                    foreach ($directory as $filename){
                        $arr = explode('/', $filename);
                        if(count($arr) > 1){
                            $filename = $arr[1];
                        }
                        $local_file = HOST.DS."cron".DS.$filename;
                        $remote_file = $folder.'/'.$filename;
                        if($filename === 'cources.xml'){
                            $local_file = HOST.DS."cron".DS.'cources.xml';
                        }
                        ftp_get($connection,$local_file,$remote_file,FTP_BINARY);
                    }
                }
            }
        }
    }

    /**
     * Функция скачивает фото с удаленного сервера для того чтобы
     * в дальнейшем выгрузить и сделать разбивку по директориям
     * @param $connection
     */
    private function preparePhotos($connection): void
    {
        $folders = ftp_nlist($connection, ".");
        if(!empty($folders)){
            $folders = array_slice($folders,0,10);
            foreach ($folders as $folder){
                $directory = ftp_nlist($connection, $folder);
                foreach ($directory as $filename){
                    Files::createFolder(HOST.DS."Photo".DS.$folder.DS, 0777);
                    if(!file_exists(HOST.DS."Photo".DS.$folder.DS.$filename)){
                        ftp_get($connection,HOST.DS."Photo".DS.$folder.DS.$filename,$folder.'/'.$filename,FTP_BINARY);
                    }
                }

            }
        }
    }

    /**
     * @removePhotos
     * Смотрим по текущему соединению доступные директории и файлы, после загрузки удаляем все файлы
     * @see ftp_nlist
     * @link https://www.php.net/manual/ru/function.ftp-nlist.php
     * @see ftp_delete
     * @link https://www.php.net/manual/ru/function.ftp-delete.php
     * @param $connection
     */
    private function removePhotos($connection): void
    {
        $folders = ftp_nlist($connection, ".");
        if(!empty($folders)){
            foreach ($folders as $folder){
                $directory = ftp_nlist($connection, $folder);
                foreach ($directory as $filename){
                    ftp_delete($connection,$folder.'/'.$filename);
                }
            }
        }
    }

    /**
     * @removeFiles
     * Смотрим по текущему соединению доступные директории и файлы, после загрузки удаляем все файлы
     * @see ftp_nlist
     * @link https://www.php.net/manual/ru/function.ftp-nlist.php
     * @see ftp_delete
     * @link https://www.php.net/manual/ru/function.ftp-delete.php
     * @param string $fold - папка откуда будет происходить удаление
     * @param $connection
     */
    private function removeFiles($connection,$fold): void
    {
        $folders = ftp_nlist($connection, ".");
        if(!empty($folders)){
            foreach ($folders as $folder){
                if($folder === $fold){
                    $directory = ftp_nlist($connection, $folder);
                    foreach ($directory as $filename){
                        $remote_file = $folder.'/'.$filename;
                        ftp_delete($connection,$remote_file);
                    }
                }
            }
        }
    }

}
