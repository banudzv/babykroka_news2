<?php
    namespace Wezom\Modules\Config\Controllers;

    use Core\Arr;
    use Core\HTML;
    use Core\QB\DB;
    use Core\Route;
    use Core\Validation\Valid;
    use Core\Widgets;
    use Core\Message;
    use Core\HTTP;
    use Core\View;
    use Core\Common;
    use Wezom\Modules\Catalog\Models\Ages;
    use Wezom\Modules\Catalog\Models\Specifications;
    use Wezom\Modules\Catalog\Models\SpecificationsValues;
    use Wezom\Modules\Config\Models\HelperFtp;

    class Config extends \Wezom\Modules\Base {

        public $tpl_folder = 'Config';

        function before() {
            parent::before();
            $this->_seo['h1'] = __('Настройки сайта');
            $this->_seo['title'] = __('Настройки сайта');
            $this->setBreadcrumbs(__('Настройки сайта'), 'wezom/'.Route::controller().'/index');
        }

        function editAction () {
            if ($_POST) {
                $result = Common::factory('config')->getRows(1);
                $errors = [];
                foreach($result AS $obj) {
                    if (array_key_exists($obj->group.'-'.$obj->key, $_POST)) {
                        $value = Arr::get($_POST, $obj->group.'-'.$obj->key);
                        if( $value === NULL && $obj->valid ) {
                            $errors[] = __('Параметр должен быть заполнен!', [':param' => $obj->name]);
                        }
                    } else if($obj->type != 'checkbox') {
                        $errors[] = __('Параметр должен быть заполнен!', [':param' => $obj->name]);
                    }
                }
                if( !$errors ) {
                    foreach($result AS $obj) {
                        if (array_key_exists($obj->group.'-'.$obj->key, $_POST)) {
                            $value = Arr::get($_POST, $obj->group.'-'.$obj->key);
                            DB::update('config')->set([
                                'zna' => $value
                            ])->where('key', '=', $obj->key)->where('group', '=', $obj->group)->execute();
                        } else if($obj->type == 'checkbox') {
                            DB::update('config')->set([
                                'zna' => 0
                            ])->where('key', '=', $obj->key)->where('group', '=', $obj->group)->execute();
                        }
                    }
                    Message::GetMessage(1, __('Вы успешно изменили данные!'));
                    HTTP::redirect( 'wezom/'.Route::controller().'/edit' );
                }
                Message::GetMessage(0, Valid::message($errors), FALSE);
            }else{
                $result = Common::factory('config')->getRows(1, 'sort', 'ASC');
            }
            $arr = [];
            foreach($result AS $obj) {
                $arr[$obj->group][] = $obj;
            }
            $_groups = Common::factory('config_groups')->getRows(1, 'sort', 'ASC');
            $groups = [];
            foreach($_groups AS $group) {
                $groups[$group->side][] = $group;
            }
            $scheduler = Common::factory('scheduler')->getRows(null, 'start_time', 'DESC')->as_array('action');
            $scheduler_config = \Core\Config::get('main.scheduler');
            $scheduler_statuses = [
                ['className' => 'turn','text' => 'Поставлен в очередь'],
                ['className' => 'work','text' => 'В работе'],
                ['className' => 'done','text' => 'Выполнен'],
                ['className' => 'error','text' => 'Ошибка выгрузки'],
            ];
            $ftp_configs = HelperFtp::getRows()->as_array('action');
            $intervalTypes = \Core\Config::get('interval.types');
            $configFTP = DB::select()->from('config')->where('group','=','individual')->where('status','=',0)->execute()->as_array('key');
            $this->_toolbar = Widgets::get( 'Toolbar_EditSaveOnly' );
            //specifications block
            $specification_id = Specifications::getRow('sezon','alias');
            $specs = null;
            if($specification_id){
                $specs = SpecificationsValues::getCustomRows(['specification_id' => $specification_id->id])->as_array();
            }
            //
            //ages block
                $ages = Ages::getRows();
            $prom_info = DB::select()->from('prom_info')->find();

            //
            $this->_content = View::tpl(
                [
                    'result' => $arr,
                    'ages' => $ages,
                    'specs' => $specs,
                    'prom_info' => $prom_info,
                    'ftp_configs' => $ftp_configs,
                    'repeatTypes' => $intervalTypes,
                    'scheduler' => $scheduler,
                    'configFTP' => $configFTP,
                    'scheduler_config' => $scheduler_config,
                    'scheduler_statuses' => $scheduler_statuses,
                    'groups' => $groups,
                ], $this->tpl_folder.'/Edit');
        }
    }
