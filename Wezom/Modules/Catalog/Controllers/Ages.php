<?php
namespace Wezom\Modules\Catalog\Controllers;

use Core\Config;
use Core\Route;
use Core\Widgets;
use Core\Message;
use Core\Arr;
use Core\HTTP;
use Core\View;
use Core\Pager\Pager;

use Wezom\Modules\Catalog\Models\Ages AS Model;
use Wezom\Modules\Catalog\Models\CatalogAgeValues;

class Ages extends \Wezom\Modules\Base {

	public $tpl_folder = 'Catalog/Ages';
	public $limit;
	public $menus;


	function before() {
		parent::before();
		$this->_seo['h1'] = __('Возрастной диапазон');
		$this->_seo['title'] = __('Возрастной диапазон');
		$this->setBreadcrumbs(__('Возрастной диапазон'), 'wezom/'.Route::controller().'/index');
		$this->limit = (int) Arr::get($_GET, 'limit', Config::get('basic.limit_backend')) < 1 ?: Arr::get($_GET, 'limit', Config::get('basic.limit_backend'));
		$this->menus = Config::get('main.menus');
	}

	function indexAction () {
		$status = NULL;
		if ( isset($_GET['status']) && $_GET['status'] != '' ) { $status = Arr::get($_GET, 'status', 1); }
		$page = (int) Route::param('page') ? (int) Route::param('page') : 1;
		$count = Model::countRows($status);
		$result = Model::getRows($status, 'sort', 'ASC', $this->limit, ($page - 1) * $this->limit);
		$pager = Pager::factory( $page, $count, $this->limit )->create();
		$this->_toolbar = Widgets::get( 'Toolbar/List', ['add' => 1, 'delete' => 1]);
		$this->_content = View::tpl(
			[
				'result' => $result,
				'tablename' => Model::$table,
				'count' => $count,
				'pager' => $pager,
				'menus' => $this->menus,
				'pageName' => $this->_seo['h1'],
			], $this->tpl_folder.'/Index');
	}

	function editAction () {
		if ($_POST) {
			$post = $_POST['FORM'];
			$group_ids = $_POST['group_age'];
			$post['status'] = Arr::get( $_POST, 'status', 0 );
			$post['visible'] = Arr::get( $_POST, 'visible', 1 );
			if( Model::valid($post) ) {
				$post['alias'] = Model::getUniqueAlias(Arr::get($post, 'alias'), Route::param('id'));
				$res = Model::update($post, Route::param('id'));
				if($res) {
                    CatalogAgeValues::updateByGroups($group_ids, Route::param('id'));
					Message::GetMessage(1, __('Вы успешно изменили данные!'));
				} else {
					Message::GetMessage(0, __('Не удалось изменить данные!'));
				}
				$this->redirectAfterSave(Route::param('id'));
			}
			$result = Arr::to_object($post);
		} else {
			$result = Model::getRow((int) Route::param('id'));
            $obj = Arr::get($result, 'obj', []);
            $langs = Arr::get($result, 'langs', []);
		}
        $groups = \Wezom\Modules\Catalog\Models\Groups::getRowsGlobal();
        $groups_on = CatalogAgeValues::getItemByAbe($obj->id);
        $groups_on_arr = [];
        foreach ($groups_on as $objGroup){
            $groups_on_arr [] = $objGroup->group_id;
        }
		$this->_toolbar = Widgets::get( 'Toolbar/Edit' );
		$this->_seo['h1'] = __('Редактирование');
		$this->_seo['title'] = __('Редактирование');
		$this->setBreadcrumbs(__('Редактирование'), 'wezom/'.Route::controller().'/edit/'.(int) Route::param('id'));
		$this->_content = View::tpl(
			[
                'obj' => $obj,
                'langs' => $langs,
                'languages' => $this->_languages,
                'groups' => $groups,
				'groups_on' => $groups_on_arr,
                'menus' => $this->menus,
			], $this->tpl_folder.'/Form');
	}

	function addAction () {
		if ($_POST) {
			$post = $_POST['FORM'];
			$post['status'] = Arr::get( $_POST, 'status', 0 );
            $post['visible'] = Arr::get( $_POST, 'visible', 1 );
            $group_ids = $_POST['group_age'];
            if( Model::valid($post) ) {
				$post['alias'] = Model::getUniqueAlias(Arr::get($post, 'alias'));
				$res = Model::insert($post);
				if($res) {
					Message::GetMessage(1, __('Вы успешно добавили данные!'));
                    CatalogAgeValues::updateByGroups($group_ids, $res);
                    $this->redirectAfterSave($res);
				} else {
					Message::GetMessage(0, __('Не удалось добавить данные!'));
				}
			}
			$result = Arr::to_object($post);
		} else {
			$result = [];
			$obj = Arr::get($result, 'obj', []);
            $langs = Arr::get($result, 'langs', []);
		}
		$this->_toolbar = Widgets::get( 'Toolbar/Edit' );
		$this->_seo['h1'] = __('Добавление');
		$this->_seo['title'] = __('Добавление');
		$this->setBreadcrumbs(__('Добавление'), 'wezom/'.Route::controller().'/add');
		$this->_content = View::tpl(
			[
                'menus' => $this->menus,
                'groups' => \Wezom\Modules\Catalog\Models\Groups::getRowsGlobal(),
                'obj' => $obj,
                'langs' => $langs,
                'languages' => $this->_languages,
                ], $this->tpl_folder.'/Form');
	}

	function deleteAction() {
		$id = (int) Route::param('id');
		$page = Model::getRowSimple($id);
		if(!$page) {
			Message::GetMessage(0, __('Данные не существуют!'));
			HTTP::redirect('wezom/'.Route::controller().'/index');
		}
		Model::delete($id);
		Message::GetMessage(1, __('Данные удалены!'));
		HTTP::redirect('wezom/'.Route::controller().'/index');
	}

}
