<?php

namespace Wezom\Modules\Catalog\Controllers;

use Core\Common;
use Core\Config;
use Core\HTML;
use Core\QB\Database;
use Core\QB\Database_Exception;
use Core\QB\DB;
use Core\Route;
use Core\Widgets;
use Core\Message;
use Core\Arr;
use Core\Support;
use Core\HTTP;
use Core\View;
use Core\Pager\Pager;
use Wezom\Modules\Catalog\Models\HelperCharacteristics;
use Wezom\Modules\Catalog\Models\HelperPrices;
use Wezom\Modules\Catalog\Models\Items AS Model;
use Wezom\Modules\Catalog\Models\CatalogImages AS Images;
use Wezom\Modules\Catalog\Models\Brands;
use Wezom\Modules\Catalog\Models\Manufacturers;
use Wezom\Modules\Catalog\Models\Prices;
use Wezom\Modules\Catalog\Models\ProductAges;
use Wezom\Modules\Catalog\Models\Specifications;
use Wezom\Modules\Catalog\Models\SpecificationsValues;
use Wezom\Modules\Catalog\Models\Models;
use Wezom\Modules\Catalog\Models\Colors;
use Wezom\Modules\Catalog\Models\Ages;
use PHPExcel_Writer_Excel2007;


class Items extends \Wezom\Modules\Base {

    public $tpl_folder = 'Catalog/Items';
    public $page;
    public $limit;
    public $offset;
    public $prices_types;

    function before() {
        parent::before();
        $this->_seo['h1'] = __('Товары');
        $this->_seo['title'] = __('Товары');
        $this->setBreadcrumbs(__('Товары'), 'wezom/' . Route::controller() . '/index');
        $this->page = (int) Route::param('page') ? (int) Route::param('page') : 1;
        $this->limit = (int) Arr::get($_GET, 'limit', Config::get('basic.limit_backend')) < 1 ? : Arr::get($_GET, 'limit', Config::get('basic.limit_backend'));
        $this->offset = ($this->page - 1) * $this->limit;
        $this->prices_types = HelperPrices::getRows()->as_array('code','name');
        $this->updateAges();
    }

    function indexAction() {
        $status = NULL;
        if (isset($_GET['status']) && $_GET['status'] != '') {
            $status = Arr::get($_GET, 'status', 1);
        }
        $page = (int) Route::param('page') ? (int) Route::param('page') : 1;
        $count = Model::countRows($status);
        $result = Model::getRows($status, 'sort', 'ASC', $this->limit, ($page - 1) * $this->limit);
        $pager = Pager::factory($page, $count, $this->limit)->create();
        $this->_toolbar = Widgets::get('Toolbar_List', ['add' => 1, 'delete' => 1, 'custom'=>true, 'custom_name'=> 'Товары без фото', 'custom_url'=> '/wezom/items/no-photo']);
        $this->_content = View::tpl(
                        [
                    'result' => $result,
                    'tpl_folder' => $this->tpl_folder,
                    'tablename' => Model::$table,
                    'count' => $count,
                    'pager' => $pager,
                    'pageName' => $this->_seo['h1'],
                    'tree' => Support::getSelectOptions('Catalog/Items/Select', 'catalog_tree', Arr::get($_GET, 'parent_id')),
                        ], $this->tpl_folder . '/Index');
    }
    function noPhotoAction() {
        $this->_seo['h1'] = __('Товары без фото');
        $this->_seo['title'] = __('Товары без фото');
        $this->setBreadcrumbs(__('Товары без фото'));
        $page = (int) Route::param('page') ? (int) Route::param('page') : 1;
        $items = Model::getItemsNoPhoto($this->limit, ($page - 1) * $this->limit);
        $page = (int) Route::param('page') ? (int) Route::param('page') : 1;
        $count = Model::getItemsNoPhotoCount();
        $pager = Pager::factory($page, $count, $this->limit)->create();
        $this->_content = View::tpl(
                        [
                            'count' => $count,
                            'pager' => $pager,
                            'items'=>$items
                        ], $this->tpl_folder . '/NoPhoto');
    }
    function noPhotoExcelAction() {
        $items = Model::getItemsNoPhoto();
        $excel = new \PHPExcel();
        $excel->setActiveSheetIndex(0);
        $sheet = $excel->getActiveSheet();
        $sheet->setTitle('Выгрузка изображений');
        $loop = 1;
        $sheet->setCellValue('A'.$loop, 'ID');
        $sheet->setCellValue('B'.$loop, 'Название');
       foreach ($items as $key => $item){
           $loop++;
           $sheet->setCellValue('A'.$loop, $item->id);
           $sheet->setCellValue('B'.$loop, $item->name);

        }
        header("Expires: Mon, 1 Apr 1974 05:00:00 GMT");
        header("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
        header("Cache-Control: no-cache, must-revalidate");
        header("Pragma: no-cache");
        header("Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header("Content-Disposition: attachment; filename=NoPhoto ".date('Y-m-d').".xls");

        $objWriter = new PHPExcel_Writer_Excel2007($excel);
        $objWriter->save('php://output');
        exit();
    }

    /**
     * @throws Database_Exception
     */
    function editAction() {

        $specArray = Arr::get($_POST, 'SPEC', []);
        if ($_POST) {
            $fotos_prom = Arr::get($_POST, 'FOTOS', []);
            $urls = [];
            foreach($fotos_prom as $foto) {
                $urls[] = $foto['url'];
            }

            $post = $_POST['FORM'];
            if(isset($_POST['PRICES'])){
                $this->updatePrices($_POST['PRICES']);
            }
            if(isset($_POST['AGES'])){
                ProductAges::updateAges($_POST['AGES']);
            }
            $productId = Route::param('id');
            if (!empty($_POST['related_products'])) {
                $products = explode(',', $_POST['related_products']);
                Model::saveRelatedProducts($productId, $products);
            } else {
                Model::deleteRelatedProducts($productId);
            }
            // Set default settings for some fields
            $post['status'] = Arr::get($_POST, 'status', 0);
            $post['sell_size'] = Arr::get($_POST, 'sell_size', 0);
            $post['new'] = Arr::get($_POST, 'new', 0);
            $post['top'] = Arr::get($_POST, 'top', 0);
            $post['sale'] = Arr::get($_POST, 'sale', 0);
            $post['ucenka'] = Arr::get($_POST, 'ucenka', 0);
            $post['available'] = Arr::get($_POST, 'available', 1);
            $post['brand_alias'] = Arr::get($post, 'brand_alias') ? : NULL;
            $post['age_alias'] = Arr::get($post, 'age_alias') ? : NULL;
            $post['sort'] = (int) Arr::get($post, 'sort');
            $post['show_3d'] = Arr::get($_POST, 'show_3d', 0);
            $post['torzgest'] = (int) Arr::get($_POST, 'torzgest');
            $post['karnaval'] = (int) Arr::get($_POST, 'karnaval');
            $post['vypiska'] = (int) Arr::get($_POST, 'vypiska');
            $post['krest'] = (int) Arr::get($_POST, 'krest');
            $post['url_foto_prom'] = json_encode($urls);

            // Check form for rude errors
            if (Model::valid($post)) {
                $post['alias'] = Model::getUniqueAlias(Arr::get($post, 'alias'), Route::param('id'));
                $res = Model::update($post, Route::param('id'));
                if ($res) {
					Model::upload3D(Route::param('id'), 'zip_3d', 'catalog_3d');
                    Model::changeSpecificationsCommunications($specArray, Route::param('id'));
                    Message::GetMessage(1, __('Вы успешно изменили данные!'));
                } else {
                    Message::GetMessage(0, __('Не удалось изменить данные!'));
                }
                $this->redirectAfterSave(Route::param('id'));
            }
            $result = Arr::to_object($post);
        } else {
            $result = Model::getRow(Route::param('id'));
            $obj = Arr::get($result, 'obj', []);
            $langs = Arr::get($result, 'langs', []);
            $specArray = Model::getItemSpecificationsAliases(Route::param('id'));
        }
        $this->_toolbar = Widgets::get('Toolbar_Edit');
        $this->_seo['h1'] = __('Редактирование');
        $this->_seo['title'] = __('Редактирование');
        $this->setBreadcrumbs(__('Редактирование'), 'wezom/' . Route::controller() . '/edit/' . Route::param('id'));
        $manufactures = Manufacturers::getRows(1);
        $brands = Brands::getRows(1);
        $colors = Colors::getRows(NULL, 'sort', 'ASC')->as_array();
        $ages  = Ages::getRows(1, 'sort', 'ASC')->as_array();
        $specifications = Specifications::getRows(1)->as_array();
        $specValues = SpecificationsValues::getRowsBySpecifications($specifications);
		$colorGroupItems = Model::getVariations(Model::COLOR_GROUPS_TABLE, $obj->{Model::COLOR_GROUPS_TABLE.'_id'}, Route::param('id'));
        $arr = [];
        foreach ($specValues as $objSpec) {
            $arr[$objSpec->specification_id][] = $objSpec;
        }
        $prices = Prices::getPricesByProductId(Route::param('id'))->as_array('price_type');
//        $rel = DB::select('catalog.*', 'catalog_images.image')
//                ->from('catalog')
//                ->join('catalog_images', 'LEFT')->on('catalog.id', '=', 'catalog_images.catalog_id')->on('catalog_images.main', '=', DB::expr('1'))
//                ->join('catalog_related')->on('catalog_related.with_id', '=', 'catalog.id')
//                ->where('catalog_related.who_id', '=', Route::param('id'))
//                ->find_all();
        $currencies = DB::select()->from('currencies_courses')->find_all()->as_array('id','currency');
        $productAges = ProductAges::getAgesCodes(Route::param('id'));
        $productSizes = Model::getSizeAmount(Route::param('id'));
        $sizes = HelperCharacteristics::getRows()->as_array('id','name');
        $products = Model::getRelatedProducts(Route::param('id'));
        $related_products = implode(',', $products);
        $this->_content = View::tpl(
                        [
                            'obj' => $obj,
                            'related_products' => $related_products,
                            'langs' => $langs,
                            'languages' => $this->_languages,
                            'prices' => $prices,
                            'productAges' => $productAges,
                            'price_types' => $this->prices_types,
                            'currencies' => $currencies,
                            'tpl_folder' => $this->tpl_folder,
                            'tree' => Support::getSelectOptions('Catalog/Items/Select', 'catalog_tree', $obj->parent_id),
							'manufactures' => $manufactures,
							'brands' => $brands,
							'ages' => $ages,
							'colors' => $colors,
							'specifications' => $specifications,
							'specValues' => $arr,
							'specArray' => $specArray,
							'uploader' => View::tpl([], $this->tpl_folder . '/Upload'),
							'size' => $productSizes,
							'sizes' => $sizes,
							'colorGroups' => View::tpl([
								'parentId' => $result->parent_id,
								'itemID' => Route::param('id'),
								'items' => $colorGroupItems,
								'tplFolder' => $this->tpl_folder
							], $this->tpl_folder.'/ColorGroups'),
                        ], $this->tpl_folder . '/Form');
    }

    /**
     * @throws Database_Exception
     */
    function addAction() {
        $specArray = Arr::get($_POST, 'SPEC', []);
        if ($_POST) {
            $post = $_POST['FORM'];
            // Set default settings for some fields
            $post['status'] = Arr::get($_POST, 'status', 0);
            $post['sell_size'] = Arr::get($_POST, 'sell_size', 0);
            $post['new'] = Arr::get($_POST, 'new', 0);
            $post['top'] = Arr::get($_POST, 'top', 0);
            $post['sale'] = Arr::get($_POST, 'sale', 0);
            $post['ucenka'] = Arr::get($_POST, 'ucenka', 0);
            $post['available'] = Arr::get($_POST, 'available', 1);
            $post['brand_alias'] = Arr::get($post, 'brand_alias') ? : NULL;
            $post['age_alias'] = Arr::get($post, 'age_alias') ? : NULL;
            $post['sort'] = (int) Arr::get($post, 'sort');
			$post['show_3d'] = (int) Arr::get($post, 'show_3d');
            // Check form for rude errors
            if (Model::valid($post)) {
                $post['alias'] = Model::getUniqueAlias(Arr::get($post, 'alias'));
                $res = Model::insert($post);
                if ($res) {
					Model::upload3D($res, 'zip_3d', 'catalog_3d');
                    Model::changeSpecificationsCommunications($specArray, $res);
                    if (!empty($_POST['related_products'])) {
                        $products = explode(',', $_POST['related_products']);
                        Model::saveRelatedProducts($res, $products);
                    }
//                    if(isset($_POST['PRICES'])){
//                        $this->updatePrices($_POST['PRICES']);
//                    }
                    Message::GetMessage(1, __('Вы успешно добавили данные!'));
					$this->redirectAfterSave($res);
                } else {
                    Message::GetMessage(0, __('Не удалось добавить данные!'));
                }
            }
            $result = Arr::to_object($post);
            $parent_id = $result->parent_id;
        } else {
            $result = [];
            $obj = Arr::get($result, 'obj', []);
            $langs = Arr::get($result, 'langs', []);
            $parent_id = 0;
        }
        $this->_toolbar = Widgets::get('Toolbar_Edit');
        $this->_seo['h1'] = __('Добавление');
        $this->_seo['title'] = __('Добавление');
        $this->setBreadcrumbs(__('Добавление'), 'wezom/' . Route::controller() . '/add');
        $brands = Brands::getRows(1,'sort','ASC');
        $manufacturers = Manufacturers::getRows(1,'sort','ASC');
        $ages = Ages::getRows(1, 'sort', 'ASC');
		$colors = Colors::getRows(NULL, 'sort', 'ASC');
        $specifications = Specifications::getRows(1)->as_array();
        $specValues = SpecificationsValues::getRowsBySpecifications($specifications)->as_array();
        $manufactures = Manufacturers::getRows(1);
        $currencies = DB::select()->from('currencies_courses')->find_all()->as_array('id','currency');
        $arr = [];
        foreach ($specValues as $value) {
            $arr[$value->specification_id][] = $value;
        }

        $this->_content = View::tpl(
                        [
                            'obj' => $obj,
                            'related_products' => '',
                            'langs' => $langs,
                            'languages' => $this->_languages,
                            'price_types' => $this->prices_types,
                            'currencies' => $currencies,
                            'manufactures' => $manufactures,
                            'prices' => null,
                            'tpl_folder' => $this->tpl_folder,
                            'tree' => Support::getSelectOptions('Catalog/Items/Select', 'catalog_tree', $parent_id),
                            'colors' => $colors,
                            'brands' => $brands,
                            'manufacturers' => $manufacturers,
                            'ages' => $ages,
                            'specifications' => $specifications,
                            'specValues' => $arr,
                            'specArray' => $specArray,
                            'size' => null,
                            'uploader' => NULL,
                            'related' => NULL,
                        ], $this->tpl_folder . '/Form');
    }

    function deleteAction() {
        $id = (int) Route::param('id');
        $page = Model::getRow($id);
        if (!$page) {
            Message::GetMessage(0, __('Данные не существуют!'));
            HTTP::redirect('wezom/' . Route::controller() . '/index');
        }
        $images = Images::getRows($id);
        foreach ($images AS $im) {
            Images::deleteImage($im->image,$id);
        }
        Model::delete($id);
        Message::GetMessage(1, __('Данные удалены!'));
        HTTP::redirect('wezom/' . Route::controller() . '/index');
    }

	function remove_3dAction() {
		$id = (int) Route::param('id');
		$page = Model::getRow($id);
		if(!$page) {
			Message::GetMessage(0, 'Данные не существуют!');
			HTTP::redirect('wezom/'.Route::controller().'/index');
		}
		Model::remove3D($id, 'catalog_3d');
		Message::GetMessage(1, 'Данные удалены!');
		HTTP::redirect('wezom/'.Route::controller().'/edit/'.$id);
	}

    /**
     * @param $post
     * @throws Database_Exception
     */
    private function updatePrices($post): void
    {
        $db = Database::instance();
        foreach ($post as $priceType => $pricesArray){
            $row = Prices::getPricesRowByPriceType(Route::param('id'),$priceType);
            if(!$row){
                $this->insertPrices($post[$priceType],$priceType);
            }
            try{
                if(empty($pricesArray['price_old'])){
                    $pricesArray['price_old'] = '0.00';
                }
                if(empty($pricesArray['price'])){
                    $pricesArray['price'] = '0.00';
                }
                DB::update(Prices::table())->set($pricesArray)->where('product_id','=',Route::param('id'))->where('price_type','=',$priceType)->execute();
                $db->commit();
            }
            catch(\Exception $e)
            {
                $db->rollback();
                throw new Database_Exception(':error', [
                    ':error' => $e->getMessage()
                ], $e->getCode());
            } catch (\Throwable $e) {
                $db->rollback();
                throw new Database_Exception(':error', [
                    ':error' => $e->getMessage()
                ], $e->getCode());
            }
        }
    }


    /**
     * @param string $priceType
     * @param array $row
     */
    private function insertPrices($row,$priceType): void
    {
        if(empty($row['price_old'])){
            $row['price_old'] = '0.00';
        }
        if(empty($row['price'])){
            $row['price'] = '0.00';
        }
        if(!isset($row['price_type'])){
            $row['price_type'] = $priceType;
        }
        if(!isset($row['product_id'])){
            $row['product_id'] = Route::param('id');
        }
        Prices::insert($row);
    }

    private function updateAges()
    {
        $products = Model::getRows()->as_array();
        $ages = ProductAges::getRows()->as_array();
        if(empty($ages)){
            if(!empty($products)){
                foreach ($products as $product){
                    if(!empty($product->age_alias)){
                        $age = Ages::getRow($product->age_alias,'alias');
                        ProductAges::insertAges(['age_type' => $age->code,'product_id' => $product->id],$age->code);
                    }
                }
            }
        }
    }

}
