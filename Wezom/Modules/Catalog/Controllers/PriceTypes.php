<?php
namespace Wezom\Modules\Catalog\Controllers;

use Core\Config;
use Core\Route;
use Core\Widgets;
use Core\Message;
use Core\Arr;
use Core\HTTP;
use Core\View;
use Core\Pager\Pager;

use Wezom\Modules\Base;
use Wezom\Modules\Catalog\Models\HelperPrices AS Model;
use Wezom\Modules\Catalog\Models\Prices;

class PriceTypes extends Base {

    public $tpl_folder = 'Catalog/Prices/Types';
    public $limit;


    function before() {
        parent::before();
        $this->_seo['h1'] = __('Таблица цен');
        $this->_seo['title'] = __('Таблица цен');
        $this->setBreadcrumbs(__('Таблица цен'), 'wezom/'.Route::controller().'/index');
        $this->limit = (int) Arr::get($_GET, 'limit', Config::get('basic.limit_backend')) < 1 ?: Arr::get($_GET, 'limit', Config::get('basic.limit_backend'));
    }

    function indexAction () {
        $page = (int) Route::param('page') ? (int) Route::param('page') : 1;
        $count = Model::countRows(null);
        $result = Model::getRows(null, 'id', 'ASC', $this->limit, ($page - 1) * $this->limit);
        $pager = Pager::factory( $page, $count, $this->limit )->create();
        $this->_toolbar = Widgets::get( 'Toolbar/List', ['add' => 1, 'delete' => 1]);
        $this->_content = View::tpl(
            [
                'result' => $result,
                'tablename' => Model::$table,
                'count' => $count,
                'pager' => $pager,
                'pageName' => $this->_seo['h1'],
            ], $this->tpl_folder.'/Index');
    }

    function editAction () {
        if ($_POST) {
            $post = $_POST['FORM'];
            if( Model::valid($post) ) {
                $post['status'] = Arr::get($_POST, 'status', 0);
                $res = Model::update($post, Route::param('id'));
                if($res) {
                    Message::GetMessage(1, __('Вы успешно изменили данные!'));
                } else {
                    Message::GetMessage(0, __('Не удалось изменить данные!'));
                }
                $this->redirectAfterSave(Route::param('id'));
            }
            $result = Arr::to_object($post);
        } else {
            $result = Model::getRow((int) Route::param('id'));
            $obj = Arr::get($result, 'obj', []);
            $langs = Arr::get($result, 'langs', []);
        }
        $this->_toolbar = Widgets::get( 'Toolbar/Edit' );
        $this->_seo['h1'] = __('Редактирование');
        $this->_seo['title'] = __('Редактирование');
        $this->setBreadcrumbs(__('Редактирование'), 'wezom/'.Route::controller().'/edit/'.(int) Route::param('id'));
        $this->_content = View::tpl(
            array(
                'obj' => $obj,
                'langs' => $langs,
                'languages' => $this->_languages,
                ), $this->tpl_folder.'/Form');
    }

    function addAction () {
        if ($_POST) {
            $post = $_POST['FORM'];
            if( Model::valid($post) ) {
                $post['status'] = Arr::get($_POST, 'status', 0);
                $res = Model::insert($post);
                if($res) {
                    Message::GetMessage(1, __('Вы успешно добавили данные!'));
                    $this->redirectAfterSave($res);
                } else {
                    Message::GetMessage(0, __('Не удалось добавить данные!'));
                }
            }
            $result = Arr::to_object($post);
        } else {
            $result = array();
            $obj = Arr::get($result, 'obj', []);
            $langs = Arr::get($result, 'langs', []);
        }
        $this->_toolbar = Widgets::get( 'Toolbar/Edit' );
        $this->_seo['h1'] = __('Добавление');
        $this->_seo['title'] = __('Добавление');
        $this->setBreadcrumbs(__('Добавление'), 'wezom/'.Route::controller().'/add');
        $this->_content = View::tpl(
            array(
                'obj' => $obj,
                'langs' => $langs,
                'languages' => $this->_languages,
                ), $this->tpl_folder.'/Form');
    }

    function deleteAction() {
        $id = (int) Route::param('id');
        $page = Model::getRow($id);
        if(!$page) {
            Message::GetMessage(0, __('Данные не существуют!'));
            HTTP::redirect('wezom/'.Route::controller().'/index');
        }
        Model::delete($id);
        Message::GetMessage(1, __('Данные удалены!'));
        HTTP::redirect('wezom/'.Route::controller().'/index');
    }

    function migrateAction(){
        $products = \Modules\Catalog\Models\Items::getRows();
        foreach ($products as $product){
            $type1 = Prices::getPricesRowByPriceType($product->id,'875e4eb9-364f-11ea-80cc-a4bf01075a5b');
            if(!$type1){
                Prices::insert(['product_id' => $product->id,'price' => $product->cost,'price_old' => $product->cost_old,'currency_id' => $product->cost_currency ?: 980,'price_type' => '875e4eb9-364f-11ea-80cc-a4bf01075a5b']);
            }
            $type2 = Prices::getPricesRowByPriceType($product->id,'78639693-364f-11ea-80cc-a4bf01075a5b');
            if(!$type2){
                Prices::insert(['product_id' => $product->id,'price' => $product->cost_opt,'price_old' => $product->cost_opt_old,'currency_id' => $product->cost_opt_currency ?: 980 ,'price_type' => '78639693-364f-11ea-80cc-a4bf01075a5b']);
            }
            $type3 = Prices::getPricesRowByPriceType($product->id,'90e0c1a4-364f-11ea-80cc-a4bf01075a5b');
           if(!$type3){
               Prices::insert(['product_id' => $product->id,'price' => $product->cost_drop,'price_old' => $product->cost_drop_old,'currency_id' => $product->cost_drop_currency ?: 980 ,'price_type' => '90e0c1a4-364f-11ea-80cc-a4bf01075a5b']);
           }
        }
        Message::GetMessage(1, __('Данные загружены!'));
        HTTP::redirect('wezom/' . Route::controller() . '/index');
    }


}
