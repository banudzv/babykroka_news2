<?php
namespace Wezom\Modules\Catalog\Controllers;

use Core\Config;
use Core\Route;
use Core\Widgets;
use Core\Message;
use Core\Arr;
use Core\HTTP;
use Core\View;
use Core\Pager\Pager;

use Wezom\Modules\Catalog\Models\Currency AS Model;

class Currency extends \Wezom\Modules\Base {

    public $tpl_folder = 'Catalog/Currency';
    public $limit;


    function before() {
        parent::before();
        $this->_seo['h1'] = __('Таблица валют');
        $this->_seo['title'] = __('Таблица валют');
        $this->setBreadcrumbs(__('Таблица валют'), 'wezom/'.Route::controller().'/index');
        $this->limit = (int) Arr::get($_GET, 'limit', Config::get('basic.limit_backend')) < 1 ?: Arr::get($_GET, 'limit', Config::get('basic.limit_backend'));
    }

    function indexAction () {
        $page = (int) Route::param('page') ? (int) Route::param('page') : 1;
        $count = Model::countRows(null);
        $result = Model::getRows(null, 'id', 'ASC', $this->limit, ($page - 1) * $this->limit);
        $pager = Pager::factory( $page, $count, $this->limit )->create();
        $this->_toolbar = Widgets::get( 'Toolbar/List', ['add' => 0, 'delete' => 0, 'noStatus' => 1]);
        $this->_content = View::tpl(
            [
                'result' => $result,
                'tablename' => Model::$table,
                'count' => $count,
                'pager' => $pager,
                'pageName' => $this->_seo['h1'],
            ], $this->tpl_folder.'/Index');
    }

    function editAction () {
        if ($_POST) {
            $post = $_POST['FORM'];
            if( Model::valid($post) ) {
                $res = Model::update($post, Route::param('id'));
                //var_dump($post); die();
                if($res) {
                    Message::GetMessage(1, __('Вы успешно изменили данные!'));
                } else {
                    Message::GetMessage(0, __('Не удалось изменить данные!'));
                }
                $this->redirectAfterSave(Route::param('id'));
            }
            $result = Arr::to_object($post);
        } else {
            $result = Model::getRow((int) Route::param('id'));
        }
        $this->_toolbar = Widgets::get( 'Toolbar/Edit', ['noAdd' => true] );
        $this->_seo['h1'] = __('Редактирование');
        $this->_seo['title'] = __('Редактирование');
        $this->setBreadcrumbs(__('Редактирование'), 'wezom/'.Route::controller().'/edit/'.(int) Route::param('id'));
        $this->_content = View::tpl(
            array(
                'obj' => $result,
            ), $this->tpl_folder.'/Form');
    }

    function deleteAction() {
        $id = (int) Route::param('id');
        $currency = Model::getRow($id);
        if(!$currency) {
            Message::GetMessage(0, __('Данные не существуют!'));
            HTTP::redirect('wezom/'.Route::controller().'/index');
        }
        Model::delete($id);
        Message::GetMessage(1, __('Данные удалены!'));
        HTTP::redirect('wezom/'.Route::controller().'/index');
    }



}
