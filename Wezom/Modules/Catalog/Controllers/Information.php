<?php
namespace Wezom\Modules\Catalog\Controllers;

use Core\Arr;
use Core\HTTP;
use Core\Message;
use Core\Route;
use Core\Support;
use Core\View;
use Core\Widgets;
use Wezom\Modules\Base;
use Wezom\Modules\Catalog\Models\Information as Model;

class Information extends Base
{

    public $tpl_folder = 'Catalog/Information';

    function before() {
        parent::before();
        $this->_seo['h1'] = __('Группы информации товаров');
        $this->_seo['title'] = __('Группы информации товаров');
        $this->setBreadcrumbs(__('Группы информации товаров'), 'wezom/'.Route::controller().'/index');
    }

    function indexAction () {
        $result = Model::getRows(NULL, 'sort', 'ASC');
        $arr = [];
        foreach($result AS $obj) {
            $arr[$obj->parent_id][] = $obj;
        }
        $this->_filter = Widgets::get( 'Filter_Pages', ['open' => 1]);
        $this->_toolbar = Widgets::get( 'Toolbar_List', ['add' => 1]);
        $this->_content = View::tpl(
            [
                'result' => $arr,
                'tpl_folder' => $this->tpl_folder,
                'tablename' => Model::$table,
                'pageName' => $this->_seo['h1'],
            ], $this->tpl_folder.'/Index');
    }

    function editAction () {
        if ($_POST) {
            $post = $_POST['FORM'];
            $post['status'] = Arr::get($_POST, 'status', 0);
            if( Model::valid($post) ) {
                $res = Model::update($post, Route::param('id'));
                if($res) {
                    Message::GetMessage(1, __('Вы успешно изменили данные!'));
                } else {
                    Message::GetMessage(0, __('Не удалось изменить данные!'));
                }
                $this->redirectAfterSave(Route::param('id'));
            }
            $result = Arr::to_object($post);
        } else {
            $result = Model::getRow(Route::param('id'));
            $obj = Arr::get($result, 'obj', []);
            $langs = Arr::get($result, 'langs', []);
        }
        $this->_toolbar = Widgets::get( 'Toolbar_Edit' );
        $this->_seo['h1'] = __('Редактирование');
        $this->_seo['title'] = __('Редактирование');
        $this->setBreadcrumbs(__('Редактирование'), 'wezom/'.Route::controller().'/edit/'.(int) Route::param('id'));
        $this->_content = View::tpl(
            [
                'obj' => $obj,
                'langs' => $langs,
                'languages' => $this->_languages,
                'tpl_folder' => $this->tpl_folder,
                'tree' => Support::getSelectOptions('Catalog/Groups/Select', 'catalog_information_groups', $obj->parent_id, $obj->id),
            ], $this->tpl_folder.'/Form');
    }

    function addAction () {
        if ($_POST) {
            $post = $_POST['FORM'];
            $post['status'] = Arr::get($_POST, 'status', 0);
            $post['created_at'] = date('Y-m-d H:m');
            if( Model::valid($post) ) {
                $res = Model::insert($post);
                if($res) {
                    Message::GetMessage(1, __('Вы успешно добавили данные!'));
                    $this->redirectAfterSave($res);
                } else {
                    Message::GetMessage(0, __('Не удалось добавить данные!'));
                }
            }
            $result = Arr::to_object($post);
        } else {
            $result = [];
            $obj = Arr::get($result, 'obj', []);
            $langs = Arr::get($result, 'langs', []);
        }
        $this->_toolbar = Widgets::get( 'Toolbar_Edit' );
        $this->_seo['h1'] = __('Добавление');
        $this->_seo['title'] = __('Добавление');
        $this->setBreadcrumbs(__('Добавление'), 'wezom/'.Route::controller().'/add');
        $this->_content = View::tpl(
            [
                'obj' => $obj,
                'langs' => $langs,
                'languages' => $this->_languages,
                'tpl_folder' => $this->tpl_folder,
                'tree' => Support::getSelectOptions('Catalog/Groups/Select', 'catalog_information_groups', $obj->patent_id),
            ], $this->tpl_folder.'/Form');
    }

    function deleteAction() {
        $id = (int) Route::param('id');
        $page = Model::getRowSimple($id);
        if(!$page) {
            Message::GetMessage(0, __('Данные не существуют!'));
            HTTP::redirect('wezom/'.Route::controller().'/index');
        }
        $countChildGroups = Model::countKids($id);
        if ( $countChildGroups ) {
            Message::GetMessage(0, __('Нельзя удалить эту группу, так как у нее есть подгруппы!'));
            HTTP::redirect('wezom/'.Route::controller().'/index');
        }
        Model::delete($id);
        Message::GetMessage(1, __('Данные удалены!'));
        HTTP::redirect('wezom/'.Route::controller().'/index');
    }
}
