<?php
    namespace Wezom\Modules\Catalog\Controllers;

    use Core\Config;
    use Core\Pager\Pager;
    use Core\Route;
    use Core\Widgets;
    use Core\Message;
    use Core\Arr;
    use Core\HTTP;
    use Core\View;

    use Wezom\Modules\Catalog\Models\Colors AS Model;

    class Colors extends \Wezom\Modules\Base {

        public $tpl_folder = 'Catalog/Colors';
        public $limit;
        public $name;

        function before() {
            parent::before();
            $this->name = __('Цвета');
            $this->_seo['h1'] = $this->_seo['title'] = $this->name;
            $this->setBreadcrumbs($this->name, 'wezom/'.Route::controller().'/index');
        }

        function indexAction () {
            $status = NULL;
            if ( isset($_GET['status']) && $_GET['status'] != '' ) { $status = Arr::get($_GET, 'status', 1); }
            $result = Model::getRows($status, 'sort', 'ASC', $this->limit);
            $count = Model::countRows($status);
            $pager = Pager::factory( $this->page, $count, $this->limit )->create();
            $this->_toolbar = Widgets::get( 'Toolbar/List', array( 'add' => 1, 'delete' => 1, 'noStatus' => 1 ) );
            $this->_content = View::tpl(
                array(
                    'result' => $result,
                    'tablename' => Model::$table,
                    'pageName' => $this->name,
                    'count' => $count,
                    'pager' => $pager,
                ), $this->tpl_folder.'/Index');
        }

        function editAction () {
            if ($_POST) {
                $post = $_POST['FORM'];
                if( Model::valid($post) ) {
                    $post['alias'] = Model::getUniqueAlias(Arr::get($post, 'alias'), Route::param('id'));
                    $res = Model::update($post, Route::param('id'));
                    if($res) {
                        Message::GetMessage(1, __('Вы успешно изменили данные!'));
                    } else {
                        Message::GetMessage(0, __('Не удалось изменить данные!'));
                    }
					$this->redirectAfterSave(Route::param('id'));
                }
                $result = Arr::to_object($post);
            } else {
                $result = Model::getRow((int) Route::param('id'));
                $obj = Arr::get($result, 'obj', []);
                $langs = Arr::get($result, 'langs', []);
            }
            $this->_toolbar = Widgets::get( 'Toolbar/Edit' );
            $this->_seo['h1'] = __('Редактирование');
            $this->_seo['title'] = __('Редактирование');
            $this->setBreadcrumbs(__('Редактирование'), 'wezom/'.Route::controller().'/edit/'.(int) Route::param('id'));
            $this->_content = View::tpl(
                array(
                    'obj' => $obj,
                    'langs' => $langs,
                    'languages' => $this->_languages,
                    ), $this->tpl_folder.'/Form');
        }

        function addAction () {
			if ($_POST) {
				$post = $_POST['FORM'];
				if( Model::valid($post) ) {
					$post['alias'] = Model::getUniqueAlias(Arr::get($post, 'alias'));
					$res = Model::insert($post);
					if($res) {
						Message::GetMessage(1, __('Вы успешно добавили данные!'));
						$this->redirectAfterSave($res);
					} else {
						Message::GetMessage(0, __('Не удалось добавить данные!'));
					}
				}
				$result = Arr::to_object($post);
            } else {
                $result = array();
                $obj = Arr::get($result, 'obj', []);
                $langs = Arr::get($result, 'langs', []);
            }
            $this->_toolbar = Widgets::get( 'Toolbar/Edit' );
            $this->_seo['h1'] = __('Добавление');
            $this->_seo['title'] = __('Добавление');
            $this->setBreadcrumbs(__('Добавление'), 'wezom/'.Route::controller().'/add');
            $this->_content = View::tpl(
                array(
                    'obj' => $obj,
                    'langs' => $langs,
                    'languages' => $this->_languages,
                    ), $this->tpl_folder.'/Form');
        }

        function deleteAction() {
            $id = (int) Route::param('id');
            $page = Model::getRowSimple($id);
            if(!$page) {
                Message::GetMessage(0, __('Данные не существуют!'));
                HTTP::redirect('wezom/'.Route::controller().'/index');
            }
            Model::delete($id);
            Message::GetMessage(1, __('Данные удалены!'));
            HTTP::redirect('wezom/'.Route::controller().'/index');
        }

    }
