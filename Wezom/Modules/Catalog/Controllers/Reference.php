<?php
namespace Wezom\Modules\Catalog\Controllers;

use Core\Support;
use Core\Route;
use Core\Widgets;
use Core\Message;
use Core\Arr;
use Core\HTTP;
use Core\View;
use Core\QB\DB;

use Wezom\Modules\Catalog\Models\Reference AS Model;

class Reference extends \Wezom\Modules\Base {

    public $tpl_folder = 'Catalog/Reference';

    function before() {
        parent::before();
        $this->_seo['h1'] = __('Метки');
        $this->_seo['title'] = __('Метки');
        $this->setBreadcrumbs(__('Метки'), 'wezom/'.Route::controller().'/index');
    }

    function indexAction () {
        $result = Model::getRows(NULL,'id', 'ASC');
        $this->_content = View::tpl(
            [
                'result' => $result,
                'tpl_folder' => $this->tpl_folder,
                'tablename' => Model::$table,
                'pageName' => $this->_seo['h1'],
            ], $this->tpl_folder.'/Index');
    }

    function editAction () {
        if ($_POST) {
            $post = $_POST['FORM'];
            if( Model::valid($post) ) {
                $res = Model::update($post, Route::param('id'));
                if($res) {
                    Message::GetMessage(1, __('Вы успешно изменили данные!'));
                } else {
                    Message::GetMessage(0, __('Не удалось изменить данные!'));
                }
                $this->redirectAfterSave(Route::param('id'));
            }
            $result = Arr::to_object($post);
        } else {
            $result = Model::getRow(Route::param('id'));
            $obj = Arr::get($result, 'obj', []);
            $langs = Arr::get($result, 'langs', []);
        }
        $this->_toolbar = Widgets::get( 'Toolbar_Edit', ['noAdd' => 1]);
        $this->_seo['h1'] = __('Редактирование');
        $this->_seo['title'] = __('Редактирование');
        $this->setBreadcrumbs(__('Редактирование'), 'wezom/'.Route::controller().'/edit/'.(int) Route::param('id'));
        $this->_content = View::tpl(
            [
                'obj' => $obj,
                'langs' => $langs,
                'languages' => $this->_languages,
                'tpl_folder' => $this->tpl_folder,
                'tree' => Support::getSelectOptions('Catalog/Groups/Select', 'catalog_tree', $result->parent_id, $result->id),
            ], $this->tpl_folder.'/Form');
    }

}
