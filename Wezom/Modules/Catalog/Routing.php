<?php

    return [
        // Test
        'wezom/test/<id:[0-9]*>' => 'catalog/items/test',
        // Crop
        'wezom/crop/<id:[0-9]*>' => 'catalog/items/crop',
        // Price Types
        'wezom/priceTypes' => 'catalog/priceTypes/index',
        'wezom/priceTypes/index' => 'catalog/priceTypes/index',
        'wezom/priceTypes/migrate' => 'catalog/priceTypes/migrate',
        'wezom/priceTypes/index/page/<page:[0-9]*>' => 'catalog/priceTypes/index',
        'wezom/priceTypes/edit/<id:[0-9]*>' => 'catalog/priceTypes/edit',
        'wezom/priceTypes/delete/<id:[0-9]*>' => 'catalog/priceTypes/delete',
        'wezom/priceTypes/delete_image/<id:[0-9]*>' => 'catalog/priceTypes/deleteImage',
        'wezom/priceTypes/add' => 'catalog/priceTypes/add',
        // Groups
        'wezom/groups/index' => 'catalog/groups/index',
        'wezom/groups/index/page/<page:[0-9]*>' => 'catalog/groups/index',
        'wezom/groups/edit/<id:[0-9]*>' => 'catalog/groups/edit',
        'wezom/groups/delete/<id:[0-9]*>' => 'catalog/groups/delete',
        'wezom/groups/delete_image/<id:[0-9]*>/type/<type:[0-9]*>' => 'catalog/groups/deleteImage',
        'wezom/groups/add' => 'catalog/groups/add',
        // Items
        'wezom/items/index' => 'catalog/items/index',
        'wezom/items/no-photo' => 'catalog/items/noPhoto',
        'wezom/items/no-photo/page/<page:[0-9]*>' => 'catalog/items/noPhoto',
        'wezom/items/no-photo/excel' => 'catalog/items/noPhotoExcel',
        'wezom/items/index/page/<page:[0-9]*>' => 'catalog/items/index',
        'wezom/items/edit/<id:[0-9]*>' => 'catalog/items/edit',
        'wezom/items/delete/<id:[0-9]*>' => 'catalog/items/delete',
        'wezom/items/add' => 'catalog/items/add',
		'wezom/items/remove_3d/<id:[0-9]*>' => 'catalog/items/remove_3d',
        // Manufacturers
        'wezom/manufacturers/index' => 'catalog/manufacturers/index',
        'wezom/manufacturers/index/page/<page:[0-9]*>' => 'catalog/manufacturers/index',
        'wezom/manufacturers/edit/<id:[0-9]*>' => 'catalog/manufacturers/edit',
        'wezom/manufacturers/delete/<id:[0-9]*>' => 'catalog/manufacturers/delete',
        'wezom/manufacturers/delete_image/<id:[0-9]*>' => 'catalog/manufacturers/deleteImage',
        'wezom/manufacturers/add' => 'catalog/manufacturers/add',
        // Brands
        'wezom/brands/index' => 'catalog/brands/index',
        'wezom/brands/index/page/<page:[0-9]*>' => 'catalog/brands/index',
        'wezom/brands/edit/<id:[0-9]*>' => 'catalog/brands/edit',
        'wezom/brands/delete/<id:[0-9]*>' => 'catalog/brands/delete',
        'wezom/brands/delete_image/<id:[0-9]*>' => 'catalog/brands/deleteImage',
        'wezom/brands/add' => 'catalog/brands/add',
        // Models
        'wezom/models/index' => 'catalog/models/index',
        'wezom/models/index/page/<page:[0-9]*>' => 'catalog/models/index',
        'wezom/models/edit/<id:[0-9]*>' => 'catalog/models/edit',
        'wezom/models/delete/<id:[0-9]*>' => 'catalog/models/delete',
        'wezom/models/add' => 'catalog/models/add',
		// Ages
		'wezom/ages/index' => 'catalog/ages/index',
		'wezom/ages/index/page/<page:[0-9]*>' => 'catalog/ages/index',
		'wezom/ages/edit/<id:[0-9]*>' => 'catalog/ages/edit',
		'wezom/ages/delete/<id:[0-9]*>' => 'catalog/ages/delete',
		'wezom/ages/add' => 'catalog/ages/add',
        // Specifications
        'wezom/specifications/index' => 'catalog/specifications/index',
        'wezom/specifications/index/page/<page:[0-9]*>' => 'catalog/specifications/index',
        'wezom/specifications/edit/<id:[0-9]*>' => 'catalog/specifications/edit',
        'wezom/specifications/delete/<id:[0-9]*>' => 'catalog/specifications/delete',
        'wezom/specifications/add' => 'catalog/specifications/add',
        // ItemsComments
        'wezom/comments/index' => 'catalog/comments/index',
        'wezom/comments/index/page/<page:[0-9]*>' => 'catalog/comments/index',
        'wezom/comments/add' => 'catalog/comments/add',
        'wezom/comments/edit/<id:[0-9]*>' => 'catalog/comments/edit',
        'wezom/comments/delete/<id:[0-9]*>' => 'catalog/comments/delete',
        // Questions
        'wezom/questions/index' => 'catalog/questions/index',
        'wezom/questions/index/page/<page:[0-9]*>' => 'catalog/questions/index',
        'wezom/questions/edit/<id:[0-9]*>' => 'catalog/questions/edit',
        'wezom/questions/delete/<id:[0-9]*>' => 'catalog/questions/delete',
		// Colors
		'wezom/colors/index' => 'catalog/colors/index',
		'wezom/colors/index/page/<page:[0-9]*>' => 'catalog/colors/index',
		'wezom/colors/edit/<id:[0-9]*>' => 'catalog/colors/edit',
		'wezom/colors/delete/<id:[0-9]*>' => 'catalog/colors/delete',
		'wezom/colors/add' => 'catalog/colors/add',
        //Additional information
        'wezom/information/index' => 'catalog/information/index',
        'wezom/information/index/page/<page:[0-9]*>' => 'catalog/information/index',
        'wezom/information/edit/<id:[0-9]*>' => 'catalog/information/edit',
        'wezom/information/delete/<id:[0-9]*>' => 'catalog/information/delete',
        'wezom/information/add' => 'catalog/information/add',


        // Ages
        'wezom/currency/index' => 'catalog/currency/index',
        'wezom/currency/index/page/<page:[0-9]*>' => 'catalog/currency/index',
        'wezom/currency/edit/<id:[0-9]*>' => 'catalog/currency/edit',
        'wezom/currency/delete/<id:[0-9]*>' => 'catalog/currency/delete',
        'wezom/currency/add' => 'catalog/currency/add',


        'wezom/reference/index' => 'catalog/reference/index',
        'wezom/reference/edit/<id:[0-9]*>' => 'catalog/reference/edit',
    ];
