<?php
    namespace Wezom\Modules\Catalog\Models;

    use Core\Common;
    use Core\CommonI18n;

    /**
     * @Brands
     * @package Wezom\Modules\Catalog\Models
     * @extends Core\Common
     * @property-read string $table
     * @property-read array $filters
     */
    class Brands extends CommonI18n {

        public static $table = 'brands';
        public static $tableI18n = 'brands_i18n';
        public static $filters = [
            'name' => [
                'table' => NULL,
                'action' => 'LIKE',
            ],
        ];
        public static $rules = [];

        public static function valid($data = [])
        {
            static::$rulesI18n = [
                'name' => [
                    [
                        'error' => __('Название бренда не может быть пустым!'),
                        'key' => 'not_empty',
                    ],
                ],
            ];
            static::$rules = [
                'alias' => [
                    [
                        'error' => __('Алиас не может быть пустым!'),
                        'key' => 'not_empty',
                    ],
                    [
                        'error' => __('Алиас должен содержать только латинские буквы в нижнем регистре, цифры!'),
                        'key' => 'regex',
                        'value' => '/^[a-z0-9]*$/',
                    ],
                ],
            ];
            return parent::valid($data);
        }

    }
