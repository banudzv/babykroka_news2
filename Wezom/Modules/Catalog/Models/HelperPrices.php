<?php
namespace Wezom\Modules\Catalog\Models;

use Core\Common;
use Core\CommonI18n;

/**
 * Class @HelperPrices
 * @package Wezom\Modules\Catalog\Models
 */
class HelperPrices extends CommonI18n {

    public static $table = 'price_types';
    public static $rules = [];

}
