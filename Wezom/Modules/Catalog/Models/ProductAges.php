<?php
namespace Wezom\Modules\Catalog\Models;

use Core\Common;
use Core\QB\DB;
use Core\Route;

/**
 * Class @ProductAges
 * @package Wezom\Modules\Catalog\Models
 */
class ProductAges extends Common {

    /**
     * @var string
     */
    public static $table = 'product_ages';
    /**
     * @var array
     */
    public static $rules = [];

    /**
     * @param int $product_id
     * @param string $age_type
     * @return mixed|null
     */
    public static function getAgesRowByPriceType($product_id,$age_type)
    {
        return DB::select()->from(self::table())
            ->where('product_id', '=', $product_id)
            ->where('age_type', '=', $age_type)
            ->find();
    }

    /**
     * Get specifications ids that belongs to group with ID = $id
     * @param integer $product_id
     * @return array
     */
    public static function getAgesCodes($product_id): array
    {
        $productAges = [];
        $res = self::getCustomRows(['product_id' => $product_id]);
        foreach ($res as $obj) {
            $productAges[] = $obj->age_type;
        }
        return $productAges;
    }


    /**
     * @param $post - $_POST[]
     */
    public static function updateAges($post): void
    {
        self::delete(Route::param('id'),'product_id');
        foreach ($post as $postIndex => $ageType){
            self::insertAges(['age_type' => $ageType],$ageType);
        }
    }


    /**
     * @param string $ageType
     * @param array $row
     */
    public static function insertAges($row,$ageType): void
    {
        if(!isset($row['age_type'])){
            $row['age_type'] = $ageType;
        }
        if(!isset($row['product_id'])){
            $row['product_id'] = Route::param('id');
        }
        self::insert($row);
    }

}
