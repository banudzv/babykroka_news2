<?php
namespace Wezom\Modules\Catalog\Models;

use Core\QB\DB;
use Core\Message;
use Core\Arr;

use Wezom\Modules\Catalog\Models\CatalogTreeBrands AS CTB;
use Wezom\Modules\Catalog\Models\CatalogTreeSpecifications AS CTS;

class Reference extends \Core\CommonI18n {

    public static $table = 'catalog_reference';

}
