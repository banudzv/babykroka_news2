<?php
    namespace Wezom\Modules\Catalog\Models;

    use Core\QB\DB;
    use Core\Message;
    use Core\Arr;

    use Wezom\Modules\Catalog\Models\CatalogTreeBrands AS CTB;
    use Wezom\Modules\Catalog\Models\CatalogTreeSpecifications AS CTS;

    class Groups extends \Core\CommonI18n {

        public static $table = 'catalog_tree';
        public static $image = 'catalog_tree';
        public static $rules = [];


        public static function countKids($id) {
            $result = DB::select([DB::expr('COUNT(id)'), 'count'])
                ->from('catalog_tree')
                ->where('parent_id', '=', $id);
            return $result->count_all();
        }


        public static function countItems($id) {
            $result = DB::select([DB::expr('COUNT(id)'), 'count'])
                ->from('catalog')
                ->where('parent_id', '=', $id);
            return $result->count_all();
        }


        /**
         * Add communications brands - groups
         * @param mixed $groupBrands - integer brand id | array brand ids
         * @param integer $id - group id from catalog_tree table
         * @return bool
         */
        public static function changeBrandsCommunications($groupBrands, $id) {
            CTB::delete($id, 'catalog_tree_id');
            if( !$groupBrands ) {
                return false;
            }
            if( !is_array($groupBrands) ) {
                $groupBrands = [$groupBrands];
            }
            foreach ($groupBrands as $brand_id) {
                CTB::insert([
                    'catalog_tree_id' => $id,
                    'brand_id' => $brand_id,
                ]);
            }
            return true;
        }
        public static function getRowsGlobal($global = true, $status = null, $sort = null, $type = null, $limit = null, $offset = null, $filter = true)
        {
            $lang = \I18n::$lang;
            if (APPLICATION == 'backend') {
                $lang = \I18n::$defaultLangBackend;
            }
            static::$tableI18n = static::$table . '_i18n';

            $result = DB::select()->from(static::$table)
                ->join(static::$tableI18n, 'LEFT')->on(static::$tableI18n . '.row_id', '=', static::$table . '.id')
                ->where(static::$tableI18n . '.language', '=', $lang);
            if ($status !== null) {
                $result->where('status', '=', $status);
            }
            if ($filter) {
                $result = static::setFilter($result);
            }
            if ($sort !== null) {
                if ($type !== null) {
                    $result->order_by($sort, $type);
                } else {
                    $result->order_by($sort);
                }
            }
            if ($global){
                $result->where('parent_id', '=', 0);
        }
            $result->order_by(static::$table.'.id', 'DESC');
            if ($limit !== null) {
                $result->limit($limit);
                if ($offset !== null) {
                    $result->offset($offset);
                }
            }

            return $result->find_all();
        }

        /**
         * Add communications specifications - groups
         * @param mixed $groupSpec - integer specification id | array specification ids
         * @param
         * @return bool
         */
        public static function changeSpecificationsCommunications($groupSpec, $id) {
            CTS::delete($id, 'catalog_tree_id');
            if( !$groupSpec ) {
                return false;
            }
            if( !is_array($groupSpec) ) {
                $groupSpec = [$groupSpec];
            }
            foreach ($groupSpec as $specification_id) {
                CTS::insert([
                    'catalog_tree_id' => $id,
                    'specification_id' => $specification_id,
                ]);
            }
            return true;
        }


        /**
         * Get brands ids that belongs to group with ID = $id
         * @param integer $id - group id from catalog_tree table
         * @return array
         */
        public static function getGroupBrandsIDS($id) {
            $groupBrands = [];
            $res = CTB::getRows($id);
            foreach ($res as $obj) {
                $groupBrands[] = $obj->brand_id;
            }
            return $groupBrands;
        }


        /**
         * Get specifications ids that belongs to group with ID = $id
         * @param integer $id - group id from catalog_tree table
         * @return array
         */
        public static function getGroupSpecificationsIDS($id) {
            $groupSpec = [];
            $res = CTS::getRows($id);
            foreach ($res as $obj) {
                $groupSpec[] = $obj->specification_id;
            }
            return $groupSpec;
        }

        public static function getValueByGroupId($id)
        {
            $result = DB::select()->from('catalog_tree_size')
                ->where('catalog_id', '=', $id)
                ->find_all();
            return $result;
        }

        public static function getCountChildRows($id, $db = null)
        {
            $result = DB::select(DB::expr('COUNT(id) as total'))->from('catalog_tree')
                ->where('parent_id','=',$id)
                ->find($db)->total;
            return $result;
        }

        public static function valid($data = [])
        {
            static::$rulesI18n = [
                'name' => [
                    [
                        'error' => __('Название группы товаров не может быть пустым!'),
                        'key' => 'not_empty',
                    ],
                ],
            ];
            static::$rules = [
                'alias' => [
                    [
                        'error' => __('Алиас не может быть пустым!'),
                        'key' => 'not_empty',
                    ],
                    [
                        'error' => __('Алиас должен содержать только латинские буквы в нижнем регистре, цифры, "-" или "_"!'),
                        'key' => 'regex',
                        'value' => '/^[a-z0-9\-_]*$/',
                    ],
                ],
            ];
            return parent::valid($data);
        }

    }
