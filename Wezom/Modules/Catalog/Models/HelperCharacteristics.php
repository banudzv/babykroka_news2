<?php
namespace Wezom\Modules\Catalog\Models;

use Core\Common;

/**
 * Class @HelperCharacteristics
 * @package Wezom\Modules\Catalog\Models
 */
class HelperCharacteristics extends Common {

    public static $table = 'helper_characteristics';
    public static $rules = [];

}
