<?php
namespace Wezom\Modules\Catalog\Models;

use Core\Arr;
use Core\Common;
use Core\Message;
use Core\QB\Database_Result_Cached;
use Core\QB\DB;

class Ages extends \Core\CommonI18n {

	public static $table = 'ages';
	public static $rules = [];


	public static function valid($post = []) {
		static::$rulesI18n = [
			'name' => [
				[
					'error' => __('Название размера не может быть пустым!'),
					'key' => 'not_empty',
				],
			],
        ];
		static::$rules = [
			'alias' => [
				[
					'error' => __('Алиас не может быть пустым!'),
					'key' => 'not_empty',
				],
				[
					'error' => __('Алиас должен содержать только латинские буквы в нижнем регистре, цифры!'),
					'key' => 'regex',
					'value' => '/^[a-z0-9]*$/',
				],
			]
		];
		return parent::valid($post);
	}

    /**
     * @param int $menu
     * @param int $visible
     * @return Database_Result_Cached|object
     */
    public static function getMenuItems($menu,$visible) {
		$result = DB::select()->from(static::$table)
			->where('status', '=', 1)
			->where('menu', '=', $menu)
			->where('visible', '=', $visible)
			->order_by('sort', 'ASC');
		return $result->find_all();
	}
	public static function getMenuItemsByIds($ids) {

		$result = DB::select()->from(static::$table)
			->where('status', '=', 1)
			->where('id', 'IN', $ids)
			->order_by('sort', 'ASC');
		return $result->find_all();
	}

}
