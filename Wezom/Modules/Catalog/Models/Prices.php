<?php
namespace Wezom\Modules\Catalog\Models;

use Core\Common;
use Core\QB\DB;

/**
 * Class @Prices
 * @package Wezom\Modules\Catalog\Models
 */
class Prices extends Common {

    public static $table = 'product_prices';
    public static $rules = [];

    /**
     * @param $product_id
     * @return object
     */
    public static function getPricesByProductId($product_id): object
    {
        return DB::select()->from(self::table())
            ->where('product_id', '=', $product_id)
            ->find_all();
    }

    /**
     * @param $product_id
     * @param $price_type
     * @return mixed|null
     */
    public static function getPricesRowByPriceType($product_id,$price_type)
    {
        return DB::select()->from(self::table())
            ->where('product_id', '=', $product_id)
            ->where('price_type', '=', $price_type)
            ->find();
    }

}
