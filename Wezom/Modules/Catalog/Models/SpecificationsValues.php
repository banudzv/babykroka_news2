<?php
    namespace Wezom\Modules\Catalog\Models;

    use Core\Arr;
    use Core\Message;
    use Core\QB\DB;

    class SpecificationsValues extends \Core\CommonI18n {

        public static $table = 'specifications_values';
        public static $tableI18n = 'specifications_values_i18n';
        public static $rules = [];


        public static function getRowsBySpecifications($specifications) {
            $arr = [0];
            foreach($specifications AS $s) {
                $arr[] = $s->id;
            }
            $specValues = DB::select()
                ->from(static::$table)
                ->join(static::$tableI18n)->on(static::$tableI18n . '.row_id', '=', static::$table.'.id')
                ->where(static::$tableI18n.'.language', '=', \I18n::$defaultLangBackend)
                ->where(static::$table.'.specification_id', 'IN', $arr)
                ->order_by(static::$table.'.sort')
                ->find_all();
            return $specValues;
        }


        public static function getRowsBySpecificationsID($specifications_ids, $status = null, $sort = null, $type = null, $limit = null, $offset = null, $all_rows = false)
        {
            $lang = \I18n::$lang;
            if (APPLICATION == 'backend') {
                $lang = \I18n::$defaultLangBackend;
            }

            if (!$specifications_ids) {
                $specifications_ids = [0];
            }
            if (!is_array($specifications_ids)) {
                $specifications_ids = [(int)$specifications_ids];
            }

            if ($sort) {
                $arr = explode('.', $sort);
                if (count($arr) < 2) {
                    $sort = static::$table . '.' . $sort;
                }
            }

            $result = DB::select(
                static::$tableI18n . '.*',
                static::$table . '.*'
            )
                ->from(static::$table)
                ->join(static::$tableI18n, 'LEFT')->on(static::$tableI18n . '.row_id', '=', static::$table . '.id')
                ->where(static::$table . '.specification_id', 'IN', $specifications_ids);
            if ($all_rows) {
                // add nothing
            } else {
                $result->where(static::$tableI18n . '.language', '=', $lang);
            }
            if ($status !== null) {
                $result->where(static::$table . '.status', '=', $status);
            }
            if ($sort !== null) {
                if ($type !== null) {
                    $result->order_by($sort, $type);
                } else {
                    $result->order_by($sort);
                }
            }
            if ($limit !== null) {
                $result->limit($limit);
                if ($type !== null) {
                    $result->offset($offset);
                }
            }

            if ($all_rows) {
                $list = [];
                $list_raw = $result->find_all()->as_array();
                foreach ($list_raw as $item) {
                    $field_name = 'name_' . $item->language;
                    if ($list[$item->id]) {
                        // do nothing
                    } else {
                        $list[$item->id] = $item;
                    }
                    $list[$item->id]->$field_name = $item->name;
                }

                return $list;
            }

            return $result->find_all();
        }
        public static function getRowsBySpecificationsIDAndFieldId($specifications, $value = null, $field = null) {
            $specValues = DB::select()
                ->from(static::$table)
                ->where(static::$table.'.specification_id', '=', $specifications);
            if ($field and $value){
                $specValues->where(static::$table.'.'.$field, '=', $value);
            }

            return $specValues->order_by(static::$table.'.sort')
                    ->find();
        }


        public static function checkValue($specification_id, $alias) {
            $result = DB::select(static::$table.'.*')
                ->from(static::$table)
                ->where('alias', '=', $alias)
                ->where('specification_id', '=', $specification_id)
                ->where('status', '=', 1);
            return $result->find();
        }


        /**
         * @param integer $specification_id - Specification ID
         * @param null/integer $status - 0 or 1
         * @param null/string $sort
         * @param null/string $type - ASC or DESC. No $sort - no $type
         * @param null/integer $limit
         * @param null/integer $offset - no $limit - no $offset
         * @return object
         */
        public static function getRowsFilter($specification_id, $status = NULL, $sort = NULL, $type = NULL, $limit = NULL, $offset = NULL) {
            static::$tableI18n = static::$table. '_i18n';
            $lang = \I18n::$lang;
            if (APPLICATION == 'backend') {
                $lang = \I18n::$defaultLangBackend;
            }
            $result = DB::select()->from(static::$table)->where('specification_id', '=', $specification_id)
                    ->join(static::$tableI18n)->on(static::$table.'.id', '=', static::$tableI18n.'.row_id')
                    ->where(static::$tableI18n.'.language', '=', $lang);
            if( $status !== NULL ) {
                $result->where('status', '=', $status);
            }
            if( $sort !== NULL ) {
                if( $type !== NULL ) {
                    $result->order_by($sort, $type);
                } else {
                    $result->order_by($sort);
                }
            }
            if( $limit !== NULL ) {
                $result->limit($limit);
                if( $type !== NULL ) {
                    $result->offset($offset);
                }
            }
            return $result->find_all();
        }

        public static function valid($data = [])
        {
            static::$rules = [
                'name' => [
                    [
                        'error' => __('Название значения характеристики не может быть пустым!'),
                        'key' => 'not_empty',
                    ],
                ],
                'alias' => [
                    [
                        'error' => __('Алиас не может быть пустым!'),
                        'key' => 'not_empty',
                    ],
                    [
                        'error' => __('Алиас должен содержать только латинские буквы в нижнем регистре или цифры!'),
                        'key' => 'regex',
                        'value' => '/^[a-z0-9]*$/',
                    ],
                ],
            ];
            return parent::valid($data);
        }

    }
