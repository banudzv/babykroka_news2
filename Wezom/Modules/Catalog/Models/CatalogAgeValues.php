<?php
namespace Wezom\Modules\Catalog\Models;

use Core\Arr;
use Core\Common;
use Core\Message;
use Core\QB\Database_Result_Cached;
use Core\QB\DB;

class CatalogAgeValues extends \Core\Common {

	public static $table = 'catalog_age_values';

    /**
     * @param int $menu
     * @param int $visible
     * @return Database_Result_Cached|object
     */
    public static function getItemByAbe($age) {
		$result = DB::select()->from(static::$table)
			->where('age_id', '=', $age);
		return $result->find_all();
	}
	public static function updateByGroups($ids, $age){
        CatalogAgeValues::delete($age,'age_id');
        foreach ($ids as $id){
            CatalogAgeValues::insert(
                [
                    'age_id'=>$age,
                    'group_id'=>$id

                ]
            );
        }
        return true;


    }

}
