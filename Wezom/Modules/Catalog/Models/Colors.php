<?php
    namespace Wezom\Modules\Catalog\Models;

    class Colors extends \Core\CommonI18n {

        public static $table = 'colors';
        public static $image = 'colors';
        public static $rules;

        public static $filters = [
            'name' => [
                'table' => 'colors',
                'action' => 'LIKE',
            ],
        ];

        public static function valid($data = []) {
            static::$rules = [
                'alias' => [
                    [
                        'error' => __('Алиас не может быть пустым!'),
                        'key' => 'not_empty',
                    ],
                    [
                        'error' => __('Алиас должен содержать только латинские буквы в нижнем регистре, цифры, "-" или "_"!'),
                        'key' => 'regex',
                        'value' => '/^[a-z0-9\-_]*$/',
                    ],
                    [
                        'error' => __('Алиас должен быть уникален!'),
                        'key' => 'unique',
                        'value' => 'colors',
                    ],
                ],
                'color' => [
                    [
                        'error' => __('Выберите цвет'),
                        'key' => 'not_empty',
                    ],
                ],
            ];
			return parent::valid($data);
        }
    }
