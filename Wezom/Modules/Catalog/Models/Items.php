<?php

namespace Wezom\Modules\Catalog\Models;

use Core\Arr;
use Core\Common;
use Core\HTML;
use Core\Message;
use Core\QB\DB;
use Wezom\Modules\Catalog\Models\CatalogSpecificationsValues AS CSV;

class Items extends \Core\CommonI18n {

	const COLOR_GROUPS_TABLE = 'catalog_color_group';
	const RELATED_TABLE = 'catalog_related';

    public static $table = 'catalog';
    public static $filters = [
        'id' => [
            'table' => NULL,
            'action' => '=',
        ],
        'parent_id' => [
            'table' => NULL,
            'action' => '=',
        ],
    ];
    public static $rules = [];

    /**
     * @param null/integer $status - 0 or 1
     * @param null/string $sort
     * @param null/string $type - ASC or DESC. No $sort - no $type
     * @param null/integer $limit
     * @param null/integer $offset - no $limit - no $offset
     * @return object
     */
    public static function getRows($status = NULL, $sort = NULL, $type = NULL, $limit = NULL, $offset = NULL,$filter = true) {
        $result = DB::select(static::$table . '_i18n.*',static::$table . '.*', ['catalog_tree_i18n.name', 'catalog_tree_name'])
                        ->from(static::$table)
                        ->join('catalog_tree', 'LEFT')->on('catalog_tree.id', '=', static::$table . '.parent_id')
                        ->join(static::$table . '_i18n', 'LEFT')->on(static::$table . '_i18n.row_id', '=', static::$table . '.id')
                        ->join('catalog_tree_i18n')->on('catalog_tree.id', '=', 'catalog_tree_i18n.row_id')
                        ->where('catalog_tree_i18n.language', '=', \I18n::lang())
                        ->where(static::$table . '_i18n.language', '=', \I18n::lang());
        if($filter){
            $result = parent::setFilter($result);
        }
        if ($status !== NULL) {
            $result->where(static::$table . '.status', '=', $status);
        }
        if ($sort !== NULL) {
            if ($type !== NULL) {
                $result->order_by($sort, $type);
            } else {
                $result->order_by($sort);
            }
        }
        $result->order_by(static::$table . '.id', 'DESC');
        if ($limit !== NULL) {
            $result->limit($limit);
            if ($type !== NULL) {
                $result->offset($offset);
            }
        }
        return $result->group_by(static::$table . '.id')->find_all();
    }
    public static function getItemsNoPhoto($limit = NULL, $offset = NULL){
        $resultIds = DB::select('catalog_images.catalog_id')
            ->from('catalog_images')
        ->group_by('catalog_id')
        ->find_all()->as_array('catalog_id');
        $ids = array_keys($resultIds);

        $result = DB::select(static::$table . '_i18n.*',static::$table . '.*')
            ->from(static::$table)
            ->join(static::$table . '_i18n', 'LEFT')->on(static::$table . '_i18n.row_id', '=', static::$table . '.id')
            ->where(static::$table . '_i18n.language', '=', \I18n::lang())
            ->where(static::$table . '.id', 'NOT IN', $ids);
        if ($limit !== NULL) {
            $result->limit($limit);
            if ($offset !== NULL and $offset > 0) {
                $result->offset($offset);
            }
        }
        $result->order_by(static::$table . '.id', 'DESC');

        return $result->group_by(static::$table . '.id')->find_all();
    }
    public static function getItemsNoPhotoCount(){
        $resultIds = DB::select('catalog_images.catalog_id')
            ->from('catalog_images')
        ->group_by('catalog_id')
        ->find_all()->as_array('catalog_id');
        $ids = array_keys($resultIds);

        $result = DB::select(static::$table . '_i18n.*',static::$table . '.*')
            ->from(static::$table)
            ->join(static::$table . '_i18n', 'LEFT')->on(static::$table . '_i18n.row_id', '=', static::$table . '.id')
            ->where(static::$table . '_i18n.language', '=', \I18n::lang())
            ->where(static::$table . '.id', 'NOT IN', $ids);
        $result->order_by(static::$table . '.id', 'DESC');
        return count($result->group_by(static::$table . '.id')->find_all());
    }



    /**
     * Add communications specifications - items
     * @param mixed $specArray - integer specification value id | array specification values ids
     * @param integer $id - item id
     * @return bool
     */
    public static function changeSpecificationsCommunications($specArray, $id) {
        CSV::delete($id, 'catalog_id');
        if (!$specArray) {
            return false;
        }
        if (!is_array($specArray)) {
            $specArray = [$specArray];
        }

        $specArray = (new Items)->array_filter_recursive($specArray);
        foreach ($specArray as $key => $value) {
            if (is_array($value)) {
                foreach ($value as $specification_value_alias) {
                    if($specification_value_alias){
                        CSV::insert([
                            'catalog_id' => $id,
                            'specification_value_alias' => $specification_value_alias,
                            'specification_alias' => $key,
                        ]);
                    }
                }
            } elseif ($value) {
                CSV::insert([
                    'catalog_id' => $id,
                    'specification_value_alias' => $value,
                    'specification_alias' => $key,
                ]);
            }
        }
        Common::factory('catalog')->update(['specifications' => json_encode($specArray)], $id);
        return true;
    }

    public function array_filter_recursive($input){
        foreach ($input as &$value){
            if (is_array($value)){
                $value = self::array_filter_recursive($value);
            }
        }
        return array_filter($input);
    }


    /**
     * Get specifications ids that belongs to item with ID = $id
     * @param integer $id - item id from catalog_tree table
     * @return array
     */
    public static function getItemSpecificationsAliases($id) {
        $specArray = [];
        $res = CSV::getRowsAliases($id);
        foreach ($res as $obj) {
            if ($obj->type_id == 3) {
                $specArray[$obj->specification_alias][] = $obj->specification_value_alias;
            } else {
                $specArray[$obj->specification_alias] = $obj->specification_value_alias;
            }
        }
        return $specArray;
    }

    public static function getItemSpecificationsIDS($id) {
        $specArray = [0];
        $res = CSV::getRows($id);
        foreach ($res as $obj) {
            if ($obj->type_id == 3) {
                $specArray[$obj->specification_id][] = $obj->specification_value_id;
            } else {
                $specArray[$obj->specification_id] = $obj->specification_value_id;
            }
        }
        return $specArray;
    }

	public static function getVariations($table, $groupId, $currentId = null)
	{
		if ($groupId == null) {
			return [];
		}
		$result = DB::select(
			static::$table.'.*'
		)
			->from(static::$table)
			->where(static::$table.'.'.$table.'_id', '=', $groupId);
		if($currentId) {
			$result->where(static::$table . '.id', '!=', $currentId);
		}
		$result->order_by(static::$table.'.id', 'DESC');

		return $result->find_all();
	}

	public static function isRelated($type)
	{
		return in_array($type, [
			self::RELATED_TABLE,
		]);
	}

	public static function getSizeAmount($id) {

    	if (!$id) {
    		return null;
		}

        return DB::select()->from('catalog_size_amount')
            ->where('catalog_id', '=', $id)
            ->find_all()
            ->as_array();

	}

    public static function valid($data = [])
    {
        static::$rulesI18n = [
            'name' => [
                [
                    'error' => __('Название товара не может быть пустым!'),
                    'key' => 'not_empty',
                ],
            ]
        ];
        static::$rules = [
            'alias' => [
                [
                    'error' => __('Алиас не может быть пустым!'),
                    'key' => 'not_empty',
                ],
                [
                    'error' => __('Алиас должен содержать только латинские буквы в нижнем регистре, цифры, "-" или "_"!'),
                    'key' => 'regex',
                    'value' => '/^[a-z0-9\-_]*$/',
                ],
            ],
            'parent_id' => [
                [
                    'error' => __('Выберите группу из списка!'),
                    'key' => 'pos_numeric',
                ],


            ],
            'manufacturer_alias' => [
                [
                    'error' => __('Выберите производителя из списка!'),
                    'key' => 'not_empty',
                ],


            ],
        ];
        return parent::valid($data);
    }

    public static function getRelatedProducts($id) {
        if (empty($id)) return false;

        $products = DB::select()
            ->from('related_products')
            ->where('product_id', '=', $id)
            ->find_all()
            ->as_array();

        $data = array();
        foreach ($products as $product) {
            $data[] = $product->related_id;
        }

        return $data;
    }

    public static function saveRelatedProducts($id, $products) {
        $existedRelatedProducts = self::getRelatedProducts($id);

        foreach ($products as $product) {
            if (!in_array($product, $existedRelatedProducts)) {
                DB::insert('related_products', ['product_id', 'related_id'])
                    ->values([$id, $product])
                    ->execute();
            }
        }

        foreach ($existedRelatedProducts as $product) {
            if (!in_array($product, $products)) {
                DB::delete('related_products')
                    ->where('product_id', '=', $id)
                    ->where('related_id', '=', $product)
                    ->execute();
            }
        }
    }

    public static function deleteRelatedProducts($id) {
        DB::delete('related_products')
            ->where('product_id', '=', $id)
            ->execute();
    }

    public static function getProductsByIds($ids){
        return DB::select(static::$table.'.*', static::$table.'_i18n.*')
            ->from(static::$table)
            ->join(static::$table . '_i18n', 'LEFT')->on(static::$table . '_i18n.row_id', '=', static::$table . '.id')
            ->where(static::$table.'.id', 'IN', $ids)
            ->find_all();
    }

    public static function getItemsForProm($ids, $db = null){
        return DB::select(
            static::$table.'.*',
            static::$table.'_i18n.name',
            static::$table.'_i18n.content',
            ['brands_i18n.name', 'brand_name'],
            ['manufacturers_i18n.name', 'country'],
            ['product_prices.price', 'price'])
            ->from(static::$table)
            ->join(static::$table . '_i18n', 'LEFT')->on(static::$table . '_i18n.row_id', '=', static::$table . '.id')
            ->join('brands', 'LEFT')->on('brands.alias', '=', static::$table.'.brand_alias')
            ->join('brands_i18n', 'LEFT')->on('brands_i18n.row_id', '=', 'brands.id')
            ->join('manufacturers', 'LEFT')->on('manufacturers.alias', '=', static::$table.'.manufacturer_alias')
            ->join('manufacturers_i18n', 'LEFT')->on('manufacturers.id', '=', 'manufacturers_i18n.row_id')
            ->join('product_prices', 'LEFT')->on('product_prices.product_id', '=', static::$table.'.id')
            ->where(static::$table.'.id', 'IN', $ids)
            ->where(static::$table.'_i18n.language', '=', 'ru')
            ->where('manufacturers_i18n.language', '=', 'ru')
            ->group_by(static::$table.'.id')
            ->find_all($db);
    }

    public static function getItemUaTranslate($row_id, $db = null){
        return DB::select(static::$table.'_i18n.name',
            static::$table.'_i18n.content')
            ->from(static::$table.'_i18n')
            ->where('language','=', 'ua')
            ->where('row_id', '=', $row_id)
            ->find($db);
    }

    public static function getItemSpecificationsByID($item_id)
    {
        $specifications = DB::select('specifications_i18n.*', 'specifications.*')->from('specifications')
            ->join('specifications_i18n', 'LEFT')->on('specifications.id', '=', 'specifications_i18n.row_id')
            ->join('catalog_tree_specifications', 'LEFT')->on('catalog_tree_specifications.specification_id', '=', 'specifications.id')
            ->where('specifications.status', '=', 1)
            ->where('specifications_i18n.language', '=', \I18n::lang())
            ->order_by('specifications_i18n.name')
            ->as_object()->execute();
        $res = DB::select()->from('specifications_values')
            ->join('catalog_specifications_values', 'LEFT')->on('catalog_specifications_values.specification_value_alias', '=', 'specifications_values.alias')
            ->join('specifications_values_i18n', 'LEFT')->on('specifications_values_i18n.row_id', '=', 'specifications_values.id')
            ->where('catalog_specifications_values.catalog_id', '=', $item_id)
            ->where('specifications_values_i18n.language', '=',\I18n::lang())
            ->where('status', '=', 1)
            ->as_object()->execute();
        $specValues = [];
        foreach ($res as $obj) {
            $specValues[$obj->specification_id][] = $obj;
        }
        $spec = [];
        foreach ($specifications as $obj) {
            if (isset($specValues[$obj->id]) and is_array($specValues[$obj->id]) and count($specValues[$obj->id])) {
                if ($obj->type_id == 3) {
                    $spec[$obj->name] = '';
                    foreach ($specValues[$obj->id] AS $o) {
                        $spec[$obj->name] .= $o->name . ', ';
                    }
                    $spec[$obj->name] = substr($spec[$obj->name], 0, -2);
                } else {
                    $spec[$obj->name] = $specValues[$obj->id][0]->name;
                }
            }
        }
        return $spec;
    }

    public static function getProductByCatId($id)
    {
        $result = DB::select()
            ->from(static::$table)
            ->where('status', '=', 1)
            ->where('parent_id', '=', $id)
            ->order_by('id', 'ASC')
            ->execute()->as_array('id');

        return $result;
    }

    public static function getProductByIds($ids)
    {
        return DB::select('parent_id', 'id')
            ->from(static::$table)
            ->where('id', 'IN', $ids)
            ->group_by('parent_id')
            ->execute()->as_array('parent_id', 'id');

    }

    public static function getNewProductsByCatIds($ids, $notIds){
        return DB::select(static::$table.'.id',
            ['product_prices.price', 'price'], static::$table.'.parent_id'
        )
            ->from(static::$table)
            ->join('product_prices')->on(static::$table.'.id', '=', 'product_prices.product_id')
            ->where('new', '=', 1)
            ->where('product_prices.price_type', '=', '875e4eb9-364f-11ea-80cc-a4bf01075a5b')
            ->where('parent_id', 'IN', $ids)
            ->where(static::$table.'.id', 'NOT IN', $notIds)
            ->group_by(static::$table.'.id')
            ->execute();
    }

    public static function getProductsByIdsAndCategory($category, $ids){
        return DB::select(static::$table.'.*', static::$table.'_i18n.name', ['product_prices.price', 'price'])
                ->from(static::$table)
                ->join(static::$table . '_i18n', 'LEFT')->on(static::$table . '_i18n.row_id', '=', static::$table . '.id')
                ->join('product_prices')->on('product_prices.product_id', '=', static::$table.'.id')
                ->where(static::$table . '_i18n.language', '=', \I18n::$defaultLangBackend)
                ->where('parent_id', '=', $category)
                ->where(static::$table.'.id', 'IN', $ids)
                ->where('product_prices.price_type', '=', '875e4eb9-364f-11ea-80cc-a4bf01075a5b')
                 ->group_by(static::$table.'.id')
                ->find_all();
    }
}
