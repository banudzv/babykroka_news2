<?php
namespace Wezom\Modules\Ajax\Helpers;

use Core\Config;
use Core\Email;
use Mailer\MException;
use Monolog\Formatter\LineFormatter;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

/**
 * Class JobLogger
 * @package Wezom\Modules\Ajax\Helpers
 */
class JobLogger
{

    private $path,$logChannel;

    public static function factory($logChannel): Logger
    {
        return new Logger($logChannel);
    }

    /**
     * Constructor.
     *
     * @param string $path
     * @param string $logChannel
     */
    public function __construct($path, $logChannel)
    {
        if (empty($path)) {
            die('path is empty');
        }

        if (empty($logChannel)) {
            die('logChannel is empty');
        }

        $this->path = $path;
        $this->logChannel = $logChannel;
    }


    /**
     * Default output - "[%datetime%] %channel%.%level_name%: %message% %context% %extra%\n"
     * Default dateFormat - Y-m-d\TH:i:sP
     * @link https://github.com/Seldaek/monolog/blob/master/doc/01-usage.md#customizing-the-log-format
     * @param string|null $customDateFormat
     * @param string|null $customOutput
     * @return StreamHandler
     */
    public function setFormatter(string $customDateFormat = null, string $customOutput = null): StreamHandler
    {
        if($customDateFormat !== null){
            $dateFormat = $customDateFormat;
        }else{
            $dateFormat = "Y-m-d\TH:i:sP";
        }
        if($customOutput !== null){
            $output = $customOutput;
        }else{
            $output = "[%datetime%] %channel%.%level_name%: %message% %context% %extra%\n";
        }
        $formatter = new LineFormatter($output, $dateFormat);
        $stream = new StreamHandler($this->path);
        $stream->setFormatter($formatter);
        return $stream;
    }

    /**
     * @param $function - method where task was finished
     * @throws MException
     */
    public function sendReport(string $function): void
    {
        $receiver = Config::get('mail.receiver_email');
        if(!empty($receiver)){
            $subject = __($function);
            Email::send($subject,__('Создан отчёт по последней запланированной задаче - :task',[':task' => $subject]),Config::get('mail.receiver_email'),(array)$this->path);
        }
    }

    public function channelName(): string
    {
        return $this->logChannel;
    }

    public function filePath(): string
    {
        return $this->path;
    }

}

