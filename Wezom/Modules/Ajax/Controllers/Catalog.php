<?php
    namespace Wezom\Modules\Ajax\Controllers;

    use Core\Common;
    use Core\CommonI18n;
    use I18n;
    use Modules\Catalog\Models\Sizes;
    use Wezom\Modules\Catalog\Models\CatalogImages;
    use Wezom\Modules\Catalog\Models\HelperCharacteristics;
    use Wezom\Modules\Catalog\Models\Items as Model;
    use Core\Arr;
    use Core\HTML;
    use Core\QB\DB;
    use Core\View;
    use Core\Config;
    use Core\Files;
    use Wezom\Modules\Catalog\Models\SpecificationsValues as SV;

    class Catalog extends \Wezom\Modules\Ajax {

        public function setPositionAction() {
            $id = Arr::get($this->post, 'id');
            $sort = Arr::get($this->post, 'sort');
            Common::factory('catalog')->update(['sort' => $sort], $id);
            $this->success();
        }

        /**
         * Get models list by brand id
         * $this->post['brand_id'] => brand ID
         */
        public function getModelsByBrandIDAction() {
            $brand_alias = Arr::get($this->post, 'brand_alias');
            $models = DB::select('id', 'name', 'alias')->from('models')->where('brand_alias', '=', $brand_alias)->order_by('name')->find_all();
            $options = [];
            foreach ($models as $model) {
                $options[] = ['name' => $model->name, 'id' => $model->id, 'alias' => $model->alias];
            }
            $this->success([
                'options' => $options,
            ]);
        }


        /**
         * Get specifications list by group ID
         * $this->post['catalog_tree_id'] => group ID
         */
        public function getSpecificationsByCatalogTreeIDAction() {
            $catalog_tree_id = (int) Arr::get($this->post, 'catalog_tree_id');
            $result = DB::select('brands.*')->from('brands')
                ->join('catalog_tree_brands', 'LEFT')->on('catalog_tree_brands.brand_id', '=', 'brands.id')
                ->where('catalog_tree_brands.catalog_tree_id', '=', $catalog_tree_id)
                ->order_by('name')
                ->find_all();
            $brands = [];
            foreach ($result as $obj) {
                $brands[] = ['name' => $obj->name, 'id' => $obj->id, 'alias' => $obj->alias];
            }
            $_specifications = DB::select('specifications.name', 'specifications.id', 'specifications.alias', 'specifications.type_id')
                ->from('specifications')
                ->join('catalog_tree_specifications', 'LEFT')->on('catalog_tree_specifications.specification_id', '=', 'specifications.id')
                ->where('catalog_tree_specifications.catalog_tree_id', '=', $catalog_tree_id)
                ->order_by('specifications.sort')
                ->find_all();
            $arr = [0];
            $specifications = [];
            foreach($_specifications AS $s) {
                $arr[] = $s->id;
                $specifications[] = (array) $s;
            }
            $specValues = DB::select('specification_id', 'name', 'alias')
                ->from('specifications_values')
                ->where('specification_id', 'IN', $arr)
                ->order_by('sort')
                ->find_all();
            $arr = [];
            foreach ($specValues as $obj) {
                $arr[$obj->specification_id][] = $obj;
            }

            $this->success([
                'brands' => $brands,
                'specifications' => $specifications,
                'specValues' => $arr,
            ]);
        }


        /**
         * Add specification value
         * $this->post['name'] => specification value name
         * $this->post['alias'] => specification value alias
         * $this->post['specification_id'] => specification id
         */
        public function addSimpleSpecificationValueAction(){
            // Check data
            $post = [];
            foreach($this->_languages as $key => $lang){
                $field_name = 'name_'.$key;
                $post[$key]['name'] = Arr::get($this->post, $field_name);
                if(!$post[$key]['name']) {
                    $this->error([
                        'error' => __('Вы не ввели название(:name) характеристики', [':name' => $lang[$lang['name']]]),
                    ]);
                }
            }

            $post['alias'] = Arr::get($this->post, 'alias');
            if(!$post['alias']) {
                $this->error([
                    'error' => __('Вы не указали алиас характеристики'),
                ]);
            }
            $post['specification_id'] = Arr::get($this->post, 'specification_id');
            $post['status'] = 1;

            if(!$post['specification_id']) {
                $this->error([
                    'error' => __('Вы ввели не все данные'),
                ]);
            }

            $post['alias'] = SV::getUniqueAlias($post['alias']);

            // Trying to save data
            $new_feature = SV::insert($post);
            if(!$new_feature) {
                $this->error([
                    'error' => __('Ошибка на сервере. Повторите попытку позднее'),
                ]);
            }

            // Get full list of values for current specification
            $specification_values_raw = SV::getRowsBySpecificationsID($post['specification_id'], null, 'specifications_values_i18n.name', 'ASC', null, null, true);
            $specification_values = [];
            foreach($specification_values_raw as $specification_value){
                $specification_values[] = $specification_value;
            }
            // Answer
            $this->success([
                'result' => $specification_values,
            ]);
        }


        /**
         * Edit specification value
         * $this->post['id'] => specification value ID
         * $this->post['status'] => specification value status
         * $this->post['name'] => specification value name
         * $this->post['alias'] => specification value alias
         * $this->post['specification_id'] => specification id
         */
        public function editSimpleSpecificationValueAction(){
            $post = [];
            foreach($this->_languages as $key => $lang){
                $field_name = 'name_'.$key;
                $post[$key]['name'] = Arr::get($this->post, $field_name);
                if(!$post[$key]['name']) {
                    $this->error([
                        'error' => __('Вы не ввели название(:name) характеристики', [':name' => $lang[$lang['name']]]),
                    ]);
                }
            }
            $post['alias'] = Arr::get($this->post, 'alias');
            if(!$post['alias']) {
                $this->error([
                    'error' => __('Вы не указали алиас характеристики'),
                ]);
            }
            $post['status'] = Arr::get($this->post, 'status');
            $post['id'] = Arr::get($this->post, 'id');
            $post['specification_id'] = Arr::get($this->post, 'specification_id');
            if(!$post['id'] || !$post['specification_id']) {
                $this->error([
                    'error' => __('Вы ввели не все данные'),
                ]);
            }
            $post['alias'] = SV::getUniqueAlias($post['alias'], $post['id']);

            // Trying to save data
            $update_feature = SV::update($post, $post['id']);

            if(!$update_feature) {
                $this->error([
                    'error' => __('Ошибка на сервере. Повторите попытку позднее'),
                ]);
            }

            // Get full list of values for current specification
            $specification_values_raw = SV::getRowsBySpecificationsID($post['specification_id'], null, 'specifications_values_i18n.name', 'ASC', null, null, true);
            $specification_values = [];
            foreach($specification_values_raw as $specification_value){
                $specification_values[] = $specification_value;
            }
            // Answer
            $this->success([
                'result' => $specification_values,
            ]);
        }


        /**
         * Delete specification value
         * $this->post['id'] => specification value ID
         */
        public function deleteSpecificationValueAction(){
            // Check data
            $id = Arr::get($this->post, 'id');
            if( !$id ) {
                $this->error([
                    'error' => __('Вы ввели не все данные'),
                ]);
            }
            // Trying to delete value
            $result = CommonI18n::factory('specifications_values')->delete($id);
            // Error if failed saving
            if( !$result ) {
                $this->error([
                    'error' => __('Ошибка на сервере. Повторите попытку позднее'),
                ]);
            }
            // Answer
            $this->success();
        }


        /**
         * Add specification value
         * $this->post['name'] => specification value name
         * $this->post['color'] => specification value color hex code
         * $this->post['alias'] => specification value alias
         * $this->post['specification_id'] => specification ID
         */
        public function addColorSpecificationValueAction(){
            // Check data
            $name = Arr::get($this->post, 'name');
            $color = Arr::get($this->post, 'color');
            $alias = Arr::get($this->post, 'alias');
            $specification_id = Arr::get($this->post, 'specification_id');
            if( !$name OR !$alias OR !$specification_id OR !preg_match('/^#[0-9abcdef]{6}$/', $color, $matches) ) {
                $this->error([
                    'error' => __('Вы ввели не все данные'),
                ]);
            }
            // Get count of rows with the same alias and specification_id
            $count = DB::select([DB::expr('COUNT(id)'), 'count'])->from('specifications_values')->where('alias', '=', $alias)->count_all();
            // Error if such alias exists
            if( $count ) {
                $this->error([
                    'error' => __('Измените алиас. Такой уже есть'),
                ]);
            }
            // Trying to save data
            $result = Common::factory('specifications_values')->insert([
                'name' => $name,
                'alias' => $alias,
                'specification_id' => $specification_id,
                'status' => 1,
                'color' => $color,
            ]);
            // Error if failed saving
            if( !$result ) {
                $this->error([
                    'error' => __('Ошибка на сервере. Повторите попытку позднее'),
                ]);
            }
            // Get full list of values for current specification
            $result = DB::select()->from('specifications_values')->where('specification_id', '=', $specification_id)->order_by('name')->find_all();
            $arr = [];
            foreach( $result AS $obj ) {
                $arr[] = $obj;
            }
            // Answer
            $this->success([
                'result' => $arr,
            ]);
        }


        /**
         * Edit specification value
         * $this->post['name'] => specification value name
         * $this->post['color'] => specification value color hex code
         * $this->post['alias'] => specification value alias
         * $this->post['specification_id'] => specification ID
         * $this->post['id'] => specification value ID
         */
        public function editColorSpecificationValueAction(){
            // Check data
            $name = Arr::get($this->post, 'name');
            $color = Arr::get($this->post, 'color');
            $alias = Arr::get($this->post, 'alias');
            $status = Arr::get($this->post, 'status');
            $id = Arr::get($this->post, 'id');
            $specification_id = Arr::get($this->post, 'specification_id');
            if( !$name OR !$alias OR !$id OR !$specification_id OR !preg_match('/^#[0-9abcdef]{6}$/', $color, $matches) ) {
                $this->error([
                    'error' => __('Вы ввели не все данные'),
                ]);
            }
            // Get count of rows with the same alias and specification_id
            $count = DB::select([DB::expr('COUNT(id)'), 'count'])->from('specifications_values')
                ->where('alias', '=', $alias)
                ->where('id', '!=', $id)
                ->count_all();
            // Error if such alias exists
            if( $count ) {
                $this->error([
                    'error' => __('Измените алиас. Такой уже есть'),
                ]);
            }
            // Trying to save data
            $result = DB::update('specifications_values')->set([
                'name' => $name,
                'alias' => $alias,
                'status' => $status,
                'color' => $color,
            ])->where('id', '=', $id)->execute();
            // Error if failed saving
            if( !$result ) {
                $this->error([
                    'error' => __('Ошибка на сервере. Повторите попытку позднее'),
                ]);
            }
            // Get full list of values for current specification
            $result = DB::select()->from('specifications_values')
                ->where('specification_id', '=', $specification_id)
                ->order_by('name')
                ->find_all();
            $arr = [];
            foreach( $result AS $obj ) {
                $arr[] = $obj;
            }
            // Answer
            $this->success([
                'result' => $arr,
            ]);
        }


        /**
         * Switch flag between 0 & 1 (field "main" in this case)
         * $this->post['id'] => photo ID to work with
         */
        public function set_default_imageAction() {
            $id = (int) Arr::get($this->post, 'id');
            if (!$id) die('Error!');
            $obj = CatalogImages::getRow($id);
            if( $obj->main != 1 ) {
                DB::update( 'catalog_images' )->set( ['main' => 0])->where( 'catalog_id', '=', $obj->catalog_id )->execute();
                CatalogImages::update(['main' => 1], $id);
                Common::factory('catalog')->update(['image' => $obj->image], $obj->catalog_id);
            }
            die(json_encode([
                'status' => true,
            ]));
        }


        /**
         * Delete uploaded item photo
         * $this->post['id'] => ID from gallery images table in DB
         */
        public function delete_catalog_photoAction() {
            $id = (int) Arr::get($this->post, 'id');
            if (!$id) die('Error!');

            $image = DB::select('image')->from( 'catalog_images' )->where( 'id', '=', $id )->find()->image;
            DB::delete( 'catalog_images' )->where( 'id', '=', $id )->execute();

            Files::deleteImage('catalog', $image);

            die(json_encode([
                'status' => true,
            ]));
        }


        /**
         * Sort photos in current album
         * $this->post['order'] => array with photos IDs in right order
         */
        public function sort_imagesAction() {
            $order = Arr::get($this->post, 'order');
            if (!is_array($order)) die('Error!');
            $updated = 0;
            foreach($order as $key => $value) {
                $value = (int) $value;
                $order = $key + 1;
                DB::update( 'catalog_images' )->set( ['sort' => $order])->where( 'id', '=', $value )->execute();
                $updated++;
            }
            die(json_encode([
                'updated' => $updated,
            ]));
        }


        /**
         * Get item photos list
         */
        public function get_uploaded_imagesAction() {
            $arr = explode('/', Arr::get($_SERVER, 'HTTP_REFERER'));
            $id = (int) end($arr);
            if( !$id ) die('Error!');
            $images = DB::select()->from( 'catalog_images' )->where( 'catalog_id', '=', $id )->order_by('sort')->find_all();
            if ($images) {
                $show_images = View::tpl(['images' => $images], 'Catalog/Items/UploadedImages');
            } else {
                $show_images = 0;
            }
            die(json_encode([
                'images' => $show_images,
            ]));
        }


        /**
         * Upload photo
         * $this->files['file'] => incoming image
         */
        public function upload_imagesAction() {
            if (empty($this->files['file'])) die('No File!');
            $arr = explode('/', Arr::get($_SERVER, 'HTTP_REFERER'));
            $id_good = (int) end($arr);

            $headers = HTML::emu_getallheaders();
            if(array_key_exists('Upload-Filename', $headers)) {
                //                $data = file_get_contents('php://input');
                $name = $headers['Upload-Filename'];
            } else {
                $name = $this->files['file']['name'];
            }

            $name = explode('.', $name);
            $ext = strtolower(end($name));

            if (!in_array($ext, Config::get('images.types'))) die('Not image!');

            $filename = Files::uploadImage('catalog');

            $has_main = DB::select([DB::expr('COUNT(id)'), 'count'])->from( 'catalog_images' )->where( 'catalog_id', '=', $id_good )->where( 'main', '=', 1 )->count_all();
            $data = [
                'catalog_id' => $id_good,
                'image' => $filename,
            ];
            if( !$has_main ) {
                $data['main'] = 1;
            }
            Common::factory('catalog_images')->insert($data);
            if($data['main']) {
                Common::factory('catalog')->update(['image' => $filename], $id_good);
            }

            die(json_encode([
                'confirm' => true,
            ]));
        }


        /**
         * Get one item information
         * $this->post['id'] => item ID
         */
        public function getItemAction() {
            $id = Arr::get($this->post, 'id', 0);
            $item = DB::select(
                    'catalog.id',
                    'ct.name',
                    'catalog.cost',
                    'catalog.image',
                    ['brands.id', 'brand_id'],
                    ['brands_i18n.name', 'brand_name']
                )
                ->from('catalog')
                ->join(['catalog_i18n', 'ct'])
                ->on('ct.row_id', '=', 'catalog.id')
                ->join('brands', 'LEFT')
                ->on('brands.alias', '=', 'catalog.brand_alias')
                ->join('brands_i18n', 'LEFT')
                ->on('brands.id', '=', 'brands_i18n.row_id')
                ->where('ct.language', '=', I18n::lang())
                ->where('brands_i18n.language', '=', \I18n::lang())
                ->where('catalog.id', '=', $id)
                ->find();

            die(json_encode([
                'success' => true,
                'item' => [
                    'id' => $item->id,
                    'link' => HTML::link('wezom/items/edit/'.$item->id),
                    'brand_link' => HTML::link('wezom/brands/edit/'.$item->brand_id),
                    'name' => $item->name,
                    'cost' => $item->cost,
                    'brand_id' => $item->brand_id,
                    'brand_name' => $item->brand_name,
                    'image' => is_file(HOST.HTML::media('images/catalog/medium/'.$item->image, false)) ? HTML::media('images/catalog/medium/'.$item->image) : NULL,
                    'image_big' => is_file(HOST.HTML::media('images/catalog/medium/'.$item->image, false)) ? HTML::media('images/catalog/big/'.$item->image) : NULL,
                ],
            ]));
        }


        /**
         * Get items list by some parameters
         * $this->post['id'] => current item ID
         * $this->post['parent_id'] => search item group ID
         * $this->post['search'] => search item artikul or name (or part of it)
         */
        public function searchItemsAction() {
			$current_id = (int) Arr::get($this->post, 'id');
			$parent_id = (int) Arr::get($this->post, 'parent_id');
			$search = Arr::get($this->post, 'search');
			$type = Arr::get($this->post, 'type');
			$limit = (int) Arr::get($this->post, 'limit', 1);
			$page = (int) 1;
			$offset = ($page - 1) * $limit;

			if( !$parent_id && !trim($search) ) {
				$this->error(array(
					'items' => array(),
				));
			}

			$excluded = DB::select('id')->from(Model::$table)
				->where('parent_id', '=', $parent_id)
                ->where('id', '!=', $current_id)
				->where('status', '!=', 0)
				->find_all()
				->as_array(null, 'id');

			$excluded[] = $current_id;

			$result = DB::select('catalog.*', 'ct.name')
                ->from('catalog')
                ->join(['catalog_i18n', 'ct'], 'INNER')
                ->on('ct.row_id', '=', 'catalog.id')
                ->where('ct.language', '=', I18n::lang());
			if( $parent_id > 0 ) {
				$result->where('catalog.parent_id', '=', $parent_id);
			}
			if( trim($search) ) {
				$result
					->and_where_open()
					->or_where('catalog_i18n.name', 'LIKE', '%'.$search.'%')
					->or_where('catalog.artikul', 'LIKE', '%'.$search.'%')
					->and_where_close();
			}

			if ($type == 'catalog_color_group'){
			    $result = $result->where('catalog_color_group_id', '=', null);
            }

            $result = $result
                ->group_by('catalog.id')
                ->limit($limit)
                ->offset($offset)
                ->find_all();

            $count = DB::select([DB::expr('COUNT(DISTINCT catalog.id)'), 'count'])->from('catalog');
            if( $parent_id > 0 ) {
                $count->where('catalog.parent_id', '=', $parent_id);
            }
            if ($type == 'catalog_color_group'){
                $item = DB::select()->from('catalog')->where('id', '=', $current_id)->find();
                $count = $count->where('catalog_color_group_id', '=', $item->catalog_color_group_id);
            }

            if( trim($search) ) {
                $count
                    ->and_where_open()
                        ->or_where('catalog_i18n.name', 'LIKE', '%'.$search.'%')
                        ->or_where('catalog.artikul', 'LIKE', '%'.$search.'%')
                    ->and_where_close();
            }
            $count = $count
                ->count_all();

            $items = [];
            foreach( $result AS $obj ) {
                $items[] = [
                    'id' => $obj->id,
                    'cost' => $obj->cost,
                    'name' => $obj->name,
                    'image' => is_file(HOST.HTML::media('images/catalog/medium/'.$obj->image, false)) ? HTML::media('images/catalog/medium/'.$obj->image) : NULL,
                ];
            }
            $this->success([
                'count' => $count,
                'items' => $items,
            ]);
        }


        /**
         * Adding item to related for current item
         * $this->post['who_id'] => current item ID
         * $this->post['with_id'] => adding item ID
         */
        public function addItemToRelatedAction() {
            $who_id = (int) Arr::get($this->post, 'who_id');
            $with_id = (int) Arr::get($this->post, 'with_id');
			$type = Arr::get($this->post, 'type');

			if (!Model::isRelated($type)) {
				$row = Model::getRow($with_id)->{$type.'_id'};
			} else {
				$row = DB::select('id')
					->from('catalog_related')
					->where('who_id', '=', $who_id)
					->where('with_id', '=', $with_id)
					->find();
			}
            if($row) {
                die(json_encode([
                    'success' => false,
                    'msg' => __('Нельзя привязать выбранный товар!'),
                ]));
            }

            if (!Model::isRelated($type)) {
				$currentItem = Model::getRow($who_id);
				$id = $currentItem->{$type.'_id'};
				if (!$id) {
					$id = DB::insert($type, array())->values(array())->execute();
					$id = $id[0];
					Model::update(array($type.'_id' => $id), $who_id);
				}
				Model::update(array($type.'_id' => $id), $with_id);
			} else {
				Common::factory('catalog_related')->insert([
					'who_id' => $who_id,
					'with_id' => $with_id,
				]);
			}

			die(json_encode([
                'success' => true,
            ]));
        }


        /**
         * Remove item from current item related
         * $this->post['who_id'] => current item ID
         * $this->post['with_id'] => adding item ID
         */
        public function removeItemFromRelatedAction() {
            $who_id = Arr::get($this->post, 'who_id');
            $with_id = Arr::get($this->post, 'with_id');
			$type = Arr::get($this->post, 'type');

			if (!Model::isRelated($type)) {
				Model::update(array($type.'_id' => NULL), $with_id);
				$currentItem = Model::getRow($who_id);
				if(DB::select([DB::expr('COUNT(id)'), 'count'])->from(Model::$table)->where($type.'_id', '=', $currentItem->{$type.'_id'})->count_all() <= 1) {
					DB::delete($type)->where('id', '=', $currentItem->{$type.'_id'})->execute();
				}
			} else {
				DB::delete('catalog_related')->where('who_id', '=', $who_id)->where('with_id', '=', $with_id)->execute();
			}

			die(json_encode([
                'success' => true,
            ]));
        }

		/**
		 * Add size amount value
		 * $this->post['size'] => catalog value size
		 * $this->post['amount'] => catalog value amount
		 * $this->post['catalog_id'] => catalog id
		 */
		public function addItemSizeValueAction(){
			// Check data
			$catalog_id = Arr::get($this->post, 'catalog_id');
			$size = Arr::get($this->post, 'size');
			$amount = Arr::get($this->post, 'amount');

			if( !$catalog_id || !$size || !$amount ) {
				$this->error([
					'error' => __('Вы ввели не все данные'),
				]);
			}

			if (!is_numeric($size)) {
				$this->error([
					'error' => __('Размер должен быть числовым значением'),
				]);
			}
			if (!is_numeric($amount)) {
				$this->error([
					'error' => __('Количество должно быть числовым значением'),
				]);
			}

			$row = HelperCharacteristics::getRow($size);
			if($row){
                $size = $row->name;
            }else{
                $this->error([
                    'error' => __('Ошибка сохранения данных. Такого размера не существует!'),
                ]);
            }

            // Get count of rows with the same alias and specification_id
            $count = DB::select([DB::expr('COUNT(catalog_size_amount.id)'), 'count'])->from('catalog_size_amount')
                ->where('catalog_id', '=', $catalog_id)
                ->where('size', '=', $size)
                ->count_all();
            // Error if such alias exists
            if( $count ) {
                $this->error([
                    'error' => __('Измените размер. Такой уже есть'),
                ]);
            }

			// Trying to save data
			$result = Common::factory('catalog_size_amount')->insert([
				'catalog_id' => $catalog_id,
				'size' => $size,
				'amount' => $amount,
			]);

			// Error if failed saving
			if( !$result ) {
				$this->error([
					'error' => __('Ошибка на сервере. Повторите попытку позднее'),
				]);
			}
			// Get full list of values for current specification
			$result = DB::select()->from('catalog_size_amount')->where('catalog_id', '=', $catalog_id)->order_by('size')->find_all();
			$arr = [];
			foreach( $result AS $obj ) {
				$arr[] = $obj;
			}
			// Answer
			$this->success([
				'result' => $arr,
			]);
		}

		/**
		 * Edit size amount value
		 * $this->post['id'] => size value ID
		 * $this->post['size'] => catalog value size
		 * $this->post['amount'] => catalog value amount
		 * $this->post['catalog_id'] => catalog id
		 */
		public function editItemSizeValueAction(){
			// Check data
			$catalog_id = Arr::get($this->post, 'catalog_id');
			$size = Arr::get($this->post, 'size');
			$amount = Arr::get($this->post, 'amount');
			$id = Arr::get($this->post, 'id');

			if( !$catalog_id OR !$size OR !$id OR $amount == null ) {
				$this->error([
					'error' => __('Вы ввели не все данные'),
				]);
			}

			if (!is_numeric($amount)) {
				$this->error([
					'error' => __('Количество должно быть числовым значением'),
				]);
			}

			// Get count of rows with the same alias and specification_id
			$count = DB::select([DB::expr('COUNT(catalog_size_amount.id)'), 'count'])->from('catalog_size_amount')
				->where('catalog_id', '=', $catalog_id)
				->where('size', '=', $size)
				->where('id', '<>', $id)
				->count_all();
			// Error if such alias exists
			if( $count ) {
				$this->error([
					'error' => __('Измените размер. Такой уже есть'),
				]);
			}
			// Trying to save data
			$result = Common::factory('catalog_size_amount')->update([
				'catalog_id' => $catalog_id,
				'size' => $size,
				'amount' => $amount
			], $id);

			// Error if failed saving
			if( !$result ) {
				$this->error([
					'error' => __('Ошибка на сервере. Повторите попытку позднее'),
				]);
			}
			// Get full list of values for current specification
			$result = DB::select()->from('catalog_size_amount')->where('catalog_id', '=', $catalog_id)->order_by('size')->find_all();
			$arr = [];
			foreach( $result AS $obj ) {
				$arr[] = $obj;
			}
			// Answer
			$this->success([
			    'response' => 'Данные успешно обновлены!',
				'result' => $arr,
			]);
		}


		/**
		 * Delete size amount value
		 * $this->post['id'] => size value ID
		 */
		public function deleteItemSizeValueAction(){
			// Check data
			$id = Arr::get($this->post, 'id');
			if( !$id ) {
				$this->error([
					'error' => __('Вы ввели не все данные'),
				]);
			}
			// Trying to delete value
			$result = Sizes::delete($id);
			// Error if failed saving
			if( !$result ) {
				$this->error([
					'error' => __('Ошибка на сервере. Повторите попытку позднее'),
				]);
			}
			// Answer
			$this->success();
		}



        /**
         * Add size amount value
         * $this->post['size'] => catalog value size
         * $this->post['amount'] => catalog value amount
         * $this->post['catalog_id'] => catalog id
         */
        public function addSizeTableValueAction(){
            // Check data
            $catalog_id = Arr::get($this->post, 'catalog_id');
            $label = (!empty(Arr::get($this->post, 'label'))) ? Arr::get($this->post, 'label') : 0;
            $age01 = (!empty(Arr::get($this->post, 'age01'))) ? Arr::get($this->post, 'age01') : 0;
            $age12 = (!empty(Arr::get($this->post, 'age12'))) ? Arr::get($this->post, 'age12') : 0;
            $age23 = (!empty(Arr::get($this->post, 'age23'))) ? Arr::get($this->post, 'age23') : 0;
            $age34 = (!empty(Arr::get($this->post, 'age34'))) ? Arr::get($this->post, 'age34') : 0;
            $age45 = (!empty(Arr::get($this->post, 'age45'))) ? Arr::get($this->post, 'age45') : 0;
            $age56 = (!empty(Arr::get($this->post, 'age56'))) ? Arr::get($this->post, 'age56') : 0;
            $age67 = (!empty(Arr::get($this->post, 'age67'))) ? Arr::get($this->post, 'age67') : 0;
            $age78 = (!empty(Arr::get($this->post, 'age78'))) ? Arr::get($this->post, 'age78') : 0;
            $age89 = (!empty(Arr::get($this->post, 'age89'))) ? Arr::get($this->post, 'age89') : 0;
            $age910 = (!empty(Arr::get($this->post, 'age910'))) ? Arr::get($this->post, 'age910') : 0;
            $age1011 = (!empty(Arr::get($this->post, 'age1011'))) ? Arr::get($this->post, 'age1011') : 0;
            $age1112 = (!empty(Arr::get($this->post, 'age1112'))) ? Arr::get($this->post, 'age1112') : 0;

            $check = DB::select()->from('catalog_tree_size')
                ->where('label', '=', $label)
                ->where('catalog_id', '=', $catalog_id)
                ->find();
            if ($check){
                $this->error([
                    'error' => __('Поле с такой меткой уже существует, выберите другую метку'),
                ]);
            }
            // Trying to save data
            $result = Common::factory('catalog_tree_size')->insert([
                'catalog_id' => $catalog_id,
                'label' => $label,
                'age01' => $age01,
                'age12' => $age12,
                'age23' => $age23,
                'age34' => $age34,
                'age45' => $age45,
                'age56' => $age56,
                'age67' => $age67,
                'age78' => $age78,
                'age89' => $age89,
                'age910' => $age910,
                'age1011' => $age1011,
                'age1112' => $age1112,
            ]);

            // Error if failed saving
            if( !$result ) {
                $this->error([
                    'error' => __('Ошибка на сервере. Повторите попытку позднее'),
                ]);
            }
            // Get full list of values for current specification
            $result = DB::select()->from('catalog_tree_size')->where('catalog_id', '=', $catalog_id)->order_by('sort')->find_all();
            $arr = [];
            foreach( $result AS $obj ) {
                $arr[] = $obj;
            }
            // Answer
            $this->success([
                'result' => $arr,
            ]);
        }

        /**
         * Edit size amount value
         * $this->post['id'] => size value ID
         * $this->post['size'] => catalog value size
         * $this->post['amount'] => catalog value amount
         * $this->post['catalog_id'] => catalog id
         */
        public function editSizeTableValueAction(){
            // Check data
            $catalog_id = Arr::get($this->post, 'catalog_id');
            $label = Arr::get($this->post, 'label');
            $age01 = Arr::get($this->post, 'age01', 0);
            $age12 = Arr::get($this->post, 'age12', 0);
            $age23 = Arr::get($this->post, 'age23', 0);
            $age34 = Arr::get($this->post, 'age34', 0);
            $age45 = Arr::get($this->post, 'age45', 0);
            $age56 = Arr::get($this->post, 'age56', 0);
            $age67 = Arr::get($this->post, 'age67', 0);
            $age78 = Arr::get($this->post, 'age78', 0);
            $age89 = Arr::get($this->post, 'age89', 0);
            $age910 = Arr::get($this->post, 'age910', 0);
            $age1011 = Arr::get($this->post, 'age1011', 0);
            $age1112 = Arr::get($this->post, 'age1112', 0);
            $id = Arr::get($this->post, 'id');

            $check = DB::select()->from('catalog_tree_size')
                ->where('id', '=', $id)
                ->find();
            if($check->label != $label){
                $check = DB::select()->from('catalog_tree_size')
                    ->where('label', '=', $label)
                    ->where('catalog_id', '=', $catalog_id)
                    ->find();
                if ($check){
                    $this->error([
                        'error' => __('Поле с такой меткой уже существует, выберите другую метку'),
                    ]);
                }
            }
            // Trying to save data
            $result = Common::factory('catalog_tree_size')->update([
                'catalog_id' => $catalog_id,
                'label' => $label,
                'age01' => $age01,
                'age12' => $age12,
                'age23' => $age23,
                'age34' => $age34,
                'age45' => $age45,
                'age56' => $age56,
                'age67' => $age67,
                'age78' => $age78,
                'age89' => $age89,
                'age910' => $age910,
                'age1011' => $age1011,
                'age1112' => $age1112,
            ], $id);
            // Error if failed saving
            if( !$result ) {
                $this->error([
                    'error' => __('Ошибка на сервере. Повторите попытку позднее'),
                ]);
            }
            // Get full list of values for current specification
            $result = DB::select()->from('catalog_tree_size')->where('catalog_id', '=', $catalog_id)->order_by('sort')->find_all();
            $arr = [];
            foreach( $result AS $obj ) {
                $arr[] = $obj;
            }
            // Answer
            $this->success([
                'result' => $arr,
            ]);
        }


        /**
         * Delete size amount value
         * $this->post['id'] => size value ID
         */
        public function deleteSizeTableValueAction(){
            // Check data
            $id = Arr::get($this->post, 'id');
            if( !$id ) {
                $this->error([
                    'error' => __('Вы ввели не все данные'),
                ]);
            }
            // Trying to delete value
            $result = Common::factory('catalog_tree_size')->delete($id);
            // Error if failed saving
            if( !$result ) {
                $this->error([
                    'error' => __('Ошибка на сервере. Повторите попытку позднее'),
                ]);
            }
            // Answer
            $this->success();
        }

    }
