<?php

namespace Wezom\Modules\Ajax\Controllers;

use Core\Arr;
use Core\Common;
use Core\CommonI18n;
use Core\Config;
use Core\Dates;
use Core\Files;
use Core\HTML;
use Core\ImagesLog;
use Core\MyXMLReader;
use Core\QB\Database;
use Core\QB\Database_Exception;
use Core\QB\Database_Result_Cached;
use Core\QB\DB;
use Core\Support;
use Core\Text;
use Core\User;
use Core\View;
use DateTimeZone;
use Exception;
use Modules\Catalog\Models\Manufacturers;
use Modules\Catalog\Models\Sizes;
use Modules\Export\Models\Prom as PromFront;
use Throwable;
use Wezom\Modules\Ajax;
use Wezom\Modules\Catalog\Models\Ages;
use Wezom\Modules\Catalog\Models\Brands;
use Wezom\Modules\Catalog\Models\CatalogImages as Images;
use Wezom\Modules\Catalog\Models\CatalogSpecificationsValues;
use Wezom\Modules\Catalog\Models\Colors;
use Wezom\Modules\Catalog\Models\Currency;
use Wezom\Modules\Catalog\Models\Groups;
use Wezom\Modules\Catalog\Models\HelperCharacteristics;
use Wezom\Modules\Catalog\Models\HelperPrices;
use Wezom\Modules\Catalog\Models\Items;
use Wezom\Modules\Catalog\Models\Prices;
use Wezom\Modules\Catalog\Models\ProductAges;
use Wezom\Modules\Catalog\Models\Specifications;
use Wezom\Modules\Catalog\Models\SpecificationsValues;
use Wezom\Modules\Config\Models\HelperFtp;
use Wezom\Modules\Export\Models\Prom;
use Wezom\Modules\Export\Service\PromService;
use XMLReader;

/**
 * @Cron
 * @package Wezom\Modules\Ajax\Controllers
 * @extends Wezom\Modules\Ajax
 * @property-read string $xmlCoursesPath
 * @property-read string $xmlReferencesPath
 * @property-read string $xmlBalancePath
 * @property-read string $xmlPricePath
 * @property-read mixed $oXml read/write instance
 */
class Cron extends Ajax
{

    public $xmlCoursesPath = HOST . DS . "cron/cources.xml";
    public $xmlReferencesPath = HOST . DS . "cron/InformationReferences.xml";
    public $xmlBalancePath = HOST . DS . "cron/ProdBalance.xml";
    public $xmlPricePath = HOST . DS . "cron/PriceData.xml";
    public $photoPath = HOST . DS . "Photo/";
    public $feedDir = HOST . DS . "feed/";
    public $oXml;
    public $logger;
    public $log;
    public $queue;
    public $connection;
    public $limit;
    public $mode;

    protected $categoryRelations = array();

    public function before()
    {
        parent::before();
        $this->initCron();
    }

    private function initCron()
    {
        $this->oXml = new MyXMLReader();
        $logger = new Ajax\Helpers\JobLogger(HOST . DS . 'logs/job.log', 'Job');
        $log = $logger::factory($logger->channelName());
        $stream = $logger->setFormatter("Y-m-d H:i:s", "[%datetime%] %channel%.%level_name%: %message% %context% %extra%\n");
        $log->pushHandler($stream);
        $this->logger = $log;
        $this->log = $logger;
        $this->mode = 'develop';
        $this->limit = 500;
        $this->queue = $this->chekQueue();
        $this->connection = 0;
    }

    /**
     * Function for validating xml files @validate
     * @param MyXMLReader|XMLReader $oXml
     * @param string $pathToFile
     * @note Only for php < 7.3, if PHP version >= 7.3 should use array_key_last
     * @link https://www.php.net/manual/en/function.array-key-last.php
     */
    private function validate($oXml, $pathToFile): void
    {
        if (!$oXml->open($pathToFile)) {
            $this->logger->error(__('Произошла ошибка при открытии файла :file Проверьте существует ли он и попробуйте снова', [':file' => $pathToFile]), ['method' => __FUNCTION__]);
            $this->error(__('Произошла ошибка при открытии файла :file Проверьте существует ли он и попробуйте снова', [':file' => $pathToFile]));
        }
        $xmlContent = file_get_contents($pathToFile);
        // -> (read @note PHPDoc)
        $getPathToArray = explode('/', $pathToFile);
        end($getPathToArray);
        $fileName = $getPathToArray[key($getPathToArray)];
        //end
        if (!Support::isXMLContentValid($xmlContent)) {
            $this->logger->error(__('Файл :file не валиден и содержит ошибки', [':file' => $fileName]));
            $this->error(__('Файл :file не валиден и содержит ошибки', [':file' => $fileName]));
        }
    }

    /**
     * @param $function
     * @param $type
     * @param null $folder
     */
    private function FTP($function, $type, $folder = null): void
    {
        $ftp = HelperFtp::getRow($function, 'action', 1);
        if ($ftp) {
            $connection = HelperFtp::connect($ftp, $type, $folder);
            if (!$connection) {
                $this->logger->error(__('Не удалось установить соединение с :ftp_server', [':ftp_server' => $ftp->ftp_address]));
                $this->error(__('Не удалось установить соединение с :ftp_server', [':ftp_server' => $ftp->ftp_address]));
            }
        }
    }

    /**
     * Function @updateCurrenciesAction
     * @param boolean $ajaxCall
     */
    public function updateCurrenciesAction($ajaxCall = true): void
    {
        if (!$this->oXml) {
            $this->initCron();
        }
        $oXml = $this->oXml;
        if (!$oXml) {
            $oXml = new MyXMLReader();
        }
        if (!$this->queue) {
            $this->updateQueue(__FUNCTION__, 1);
            if ($this->connection == 0) {
                $this->FTP(__FUNCTION__, 'file', 'Course');
            }
        }
        $functionQueue = $this->chekQueue(__FUNCTION__);
        if ($functionQueue) {
            $this->validate($oXml, $this->xmlCoursesPath);
            $currencies = [];
            $i = 0;
            while ($oXml->read()) {
                if ($oXml->nodeType === XMLReader::ELEMENT) {
                    switch ($oXml->name) {
                        case 'course':
                            $i++;
                            break;
                        case 'currency_id':
                            $currency_id = trim($oXml->readString());
                            $currencies[$i]['currency_id'] = $currency_id;
                            break;
                        case 'value':
                            $value = trim($oXml->readString());
                            $currencies[$i]['value'] = $value ?: 1;
                            break;
                    }
                }
            }
            if (!empty($currencies)) {
                foreach ($currencies as $currency) {
                    Currency::update(['course' => $currency['value']], $currency['currency_id'], 'id');
                }
            }
            if ($this->mode !== 'develop') {
                @unlink($this->xmlCoursesPath);
            }
            $this->updateQueue(__FUNCTION__, 0);
            $this->clearAdditional(__FUNCTION__);
            $this->saveJobAction(__FUNCTION__);
            if ($ajaxCall) {

                $this->success(__('Таблица валют успешно обновлена'));
            }
        } elseif ($ajaxCall) {
            $this->success(__('Другая задача находится в очереди'));
        }
    }

    /**
     * Function @updateInformationReferencesAction
     * @param bool $ajaxCall
     * @note Описание некоторых полей
     * parent_id => catalog_tree table
     * brand_id => Возможно specifications table
     * manufacturer_id => brands table
     * color_id => catalog_color_group table
     * card_code => catalog table - field `catalog.id`
     * season_id => specifications table
     * gender_id => specifications table
     * material_id => specifications table
     * @throws Database_Exception
     * @throws Throwable
     * @todo В файле присутствуют в категории products параметры name_ua,name_rum,name_eng которые пока что не используются. При включении мультиязычности на сайте их нужно подвязать
     */
    public function updateInformationReferencesAction($ajaxCall = true): void
    {
        if (!$this->oXml) {
            $this->initCron();
        }
        $oXml = $this->oXml;
        if (!$this->queue) {
            $this->updateQueue(__FUNCTION__, 1);
            if ($this->connection == 0) {
                $this->FTP(__FUNCTION__, 'file', 'Directory');
            }
        }
        $functionQueue = $this->chekQueue(__FUNCTION__);
        if ($functionQueue) {
            $this->validate($oXml, $this->xmlReferencesPath);
            $prices = [];
            $pricesToUpdate = [];
            $balanceProductsToChange = [];
            $currencies = [];
            $characteristics = [];
            $products = [];
            $groups = [];
            $brands = [];
            $seasons = [];
            $genders = [];
            $materials = [];
            $manufacturers = [];
            $colors = [];
            $priceIndex = 0;
            $currencyIndex = 0;
            $characteristicIndex = 0;
            $groupsIndex = 0;
            $brandIndex = 0;
            $seasonIndex = 0;
            $genderIndex = 0;
            $materialIndex = 0;
            $manufacturerIndex = 0;
            $colorIndex = 0;
            $productIndex = 0;
            $ProdPrice = 0;
            $balance = 0;
            $drop = 0;
            while ($oXml->read()) {
                $last_node_at_depth[$oXml->depth] = $oXml->name;
                if ($oXml->nodeType === XMLReader::ELEMENT) {
                    switch ($oXml->name) {
                        case 'FULL':
                            $drop = trim($oXml->readString());
                            break;
                        case 'group':
                            if ($last_node_at_depth[$oXml->depth - 1] === 'groups') {
                                $depth = $oXml->depth;
                                $groupsIndex++;
                                while ($oXml->readToNextChildElement($depth)) {
                                    switch ($oXml->localName) {
                                        case 'id':
                                            $id = $oXml->getNodeValue();
                                            $groups[$groupsIndex]['id'] = $id;
                                            break;
                                        case 'name':
                                            $name = $oXml->getNodeValue();
                                            $groups[$groupsIndex]['ru']['name'] = $name;
                                            $groups[$groupsIndex]['ua']['name'] = $name;
                                            break;
                                        case 'name_ua':
                                            $name = $oXml->getNodeValue();
                                            if ($name == '') {
                                                $name = $groups[$groupsIndex]['ru']['name'] . '-ua';
                                            }
                                            $groups[$groupsIndex]['ua']['name'] = $name;
                                            break;
                                        case 'parent_id':
                                            $parent_id = $oXml->getNodeValue();
                                            $groups[$groupsIndex]['parent_id'] = $parent_id ?: 0;
                                            break;
                                    }
                                }
                            }
                            break;
                        case 'brand':
                            if ($last_node_at_depth[$oXml->depth - 1] === 'brands') {
                                $depth = $oXml->depth;
                                $brandIndex++;
                                while ($oXml->readToNextChildElement($depth)) {
                                    switch ($oXml->localName) {
                                        case 'id':
                                            $id = $oXml->getNodeValue();
                                            $brands[$brandIndex]['brand_id'] = $id;
                                            break;
                                        case 'name':
                                            $name = $oXml->getNodeValue();
                                            $brands[$brandIndex]['ru']['name'] = $name;
                                            $brands[$brandIndex]['ua']['name'] = $name;
                                            break;
                                        case 'name_ua':
                                            $name = $oXml->getNodeValue();
                                            if ($name == '') {
                                                $name = $brands[$brandIndex]['ru']['name'] . '-ua';
                                            }
                                            $brands[$brandIndex]['ua']['name'] = $name;
                                            break;
                                    }
                                }
                            }
                            break;
                        case 'season':
                            if ($last_node_at_depth[$oXml->depth - 1] === 'seasons') {
                                $depth = $oXml->depth;
                                $seasonIndex++;
                                while ($oXml->readToNextChildElement($depth)) {
                                    switch ($oXml->localName) {
                                        case 'id':
                                            $id = $oXml->getNodeValue();
                                            $seasons[$seasonIndex]['season_id'] = $id;
                                            break;
                                        case 'name':
                                            $name = $oXml->getNodeValue();
                                            $seasons[$seasonIndex]['ru']['name'] = Text::mb_ucfirst($name);
                                            $seasons[$seasonIndex]['ua']['name'] = Text::mb_ucfirst($name);
                                            break;
                                        case 'name_ua':
                                            $name = $oXml->getNodeValue();
                                            if ($name == '') {
                                                $name = $seasons[$seasonIndex]['ru']['name'] . '-ua';
                                            }
                                            $seasons[$seasonIndex]['ua']['name'] = $name;
                                            break;
                                    }
                                }
                            }
                            break;
                        case 'gender':
                            if ($last_node_at_depth[$oXml->depth - 1] === 'genders') {
                                $depth = $oXml->depth;
                                $genderIndex++;
                                while ($oXml->readToNextChildElement($depth)) {
                                    switch ($oXml->localName) {
                                        case 'id':
                                            $id = $oXml->getNodeValue();
                                            $genders[$genderIndex]['gender_id'] = $id;
                                            break;
                                        case 'name':
                                            $name = $oXml->getNodeValue();
                                            $genders[$genderIndex]['ru']['name'] = Text::mb_ucfirst($name);
                                            $genders[$genderIndex]['ua']['name'] = Text::mb_ucfirst($name);
                                            break;
                                        case 'name_ua':
                                            $name = $oXml->getNodeValue();
                                            if ($name == '') {
                                                $name = $genders[$genderIndex]['ru']['name'] . '-ua';
                                            }
                                            $genders[$genderIndex]['ua']['name'] = $name;
                                            break;
                                    }
                                }
                            }
                            break;
                        case 'material':
                            if ($last_node_at_depth[$oXml->depth - 1] === 'materials') {
                                $depth = $oXml->depth;
                                $materialIndex++;
                                while ($oXml->readToNextChildElement($depth)) {
                                    switch ($oXml->localName) {
                                        case 'id':
                                            $id = $oXml->getNodeValue();
                                            $materials[$materialIndex]['material_id'] = $id;
                                            break;
                                        case 'name':
                                            $name = $oXml->getNodeValue();
                                            $materials[$materialIndex]['ru']['name'] = Text::mb_ucfirst($name);
                                            $materials[$materialIndex]['ua']['name'] = Text::mb_ucfirst($name);
                                            break;
                                        case 'name_ua':
                                            $name = $oXml->getNodeValue();
                                            if ($name == '') {
                                                $name = $materials[$materialIndex]['ru']['name'] . '-ua';
                                            }
                                            $materials[$materialIndex]['ua']['name'] = $name;
                                            break;
                                    }
                                }
                            }
                            break;
                        case 'manufacturer':
                            if ($last_node_at_depth[$oXml->depth - 1] === 'manufacturers') {
                                $depth = $oXml->depth;
                                $manufacturerIndex++;
                                while ($oXml->readToNextChildElement($depth)) {
                                    switch ($oXml->localName) {
                                        case 'id':
                                            $id = $oXml->getNodeValue();
                                            $manufacturers[$manufacturerIndex]['manufacturer_id'] = $id;
                                            break;
                                        case 'name':
                                            $name = $oXml->getNodeValue();
                                            $manufacturers[$manufacturerIndex]['ru']['name'] = $name;
                                            $manufacturers[$manufacturerIndex]['ua']['name'] = $name;
                                            break;
                                        case 'name_ua':
                                            $name = $oXml->getNodeValue();
                                            if ($name == '') {
                                                $name = $manufacturers[$manufacturerIndex]['ru']['name'] . '-ua';
                                            }
                                            $manufacturers[$manufacturerIndex]['ua']['name'] = $name;
                                            break;
                                    }
                                }
                            }
                            break;
                        case 'color':
                            if ($last_node_at_depth[$oXml->depth - 1] === 'colors') {
                                $depth = $oXml->depth;
                                $colorIndex++;
                                while ($oXml->readToNextChildElement($depth)) {
                                    switch ($oXml->localName) {
                                        case 'id':
                                            $id = $oXml->getNodeValue();
                                            $colors[$colorIndex]['color_id'] = $id;
                                            break;
                                        case 'name':
                                            $name = $oXml->getNodeValue();
                                            $colors[$colorIndex]['ru']['name'] = $name;
                                            $colors[$colorIndex]['ua']['name'] = $name;
                                            break;
                                        case 'name_ua':
                                            $name = $oXml->getNodeValue();
                                            if ($name == '') {
                                                $name = $colors[$colorIndex]['ru']['name'] . '-ua';
                                            }
                                            $colors[$colorIndex]['ua']['name'] = $name;
                                            break;
                                    }
                                }
                            }
                            break;
                        case 'product':
                            if ($last_node_at_depth[$oXml->depth - 1] === 'products') {
                                $depth = $oXml->depth;
                                $productIndex++;
                                while ($oXml->readToNextChildElement($depth)) {
                                    switch ($oXml->localName) {
                                        case 'id':
                                            $id = $oXml->getNodeValue();
                                            $products[$productIndex]['id'] = $id;
                                            break;
                                        case 'name':
                                            $name = $oXml->getNodeValue();
                                            $products[$productIndex]['ru']['name'] = $name;
                                            $products[$productIndex]['ua']['name'] = $name;
                                            break;
                                        case 'name_ua':
                                            $name = $oXml->getNodeValue();
                                            if ($name == '') {
                                                $name = $products[$productIndex]['ru']['name'] . '-ua';
                                            }
                                            $products[$productIndex]['ua']['name'] = $name;
                                            break;
                                        case 'parent_id':
                                            $parent_id = $oXml->getNodeValue();
                                            $products[$productIndex]['parent_id'] = $parent_id;
                                            break;
                                        case 'new':
                                            $new = $oXml->getNodeValue();
                                            $products[$productIndex]['new'] = $new;
                                            break;
                                        case 'top':
                                            $top = $oXml->getNodeValue();
                                            $products[$productIndex]['top'] = $top;
                                            break;
                                        case 'brand_id':
                                            $brand_id = $oXml->getNodeValue();
                                            $products[$productIndex]['brand_id'] = $brand_id;
                                            break;
                                        case 'manufacturer_id':
                                            $manufacturer_id = $oXml->getNodeValue();
                                            $products[$productIndex]['manufacturer_id'] = $manufacturer_id;
                                            break;
                                        case 'color_id':
                                            $color_id = $oXml->getNodeValue();
                                            $products[$productIndex]['color_id'] = $color_id;
                                            break;
                                        case 'opis':
                                            $content = $oXml->getNodeValue();
                                            $products[$productIndex]['ru']['content'] = $content;
                                            $products[$productIndex]['ua']['content'] = $content;
                                            break;
                                        case 'opis_ua':
                                            $content = $oXml->getNodeValue();
                                            if ($name == '') {
                                                $name = $products[$productIndex]['ru']['content'] . '-ua';
                                            }
                                            $products[$productIndex]['ua']['content'] = $content;
                                            break;
                                        case 'characteristics':
                                            $subDepth = $oXml->depth;
                                            while ($oXml->readToNextChildElement($subDepth)) {
                                                switch ($oXml->localName) {
                                                    case 'id':
                                                        $id = $oXml->getNodeValue();
                                                        $products[$productIndex]['characteristics'][] = $id;
                                                        break;
                                                }
                                            }
                                            break;
                                        case 'card_code':
                                            $card_code = $oXml->getNodeValue();
                                            $products[$productIndex]['card_code'] = $card_code;
                                            break;
                                        case 'ages':
                                            $subDepth = $oXml->depth;

                                            while ($oXml->readToNextChildElement($subDepth)) {
                                                switch ($oXml->localName) {
                                                    case 'id':
                                                        $id = $oXml->getNodeValue();
                                                        $products[$productIndex]['ages'][] = $id;
                                                        break;
                                                }
                                            }
                                            break;
                                        case 'season_id':
                                            $season_id = $oXml->getNodeValue();
                                            $products[$productIndex]['season_id'] = $season_id;
                                            break;
                                        case 'gender_id':
                                            $gender_id = $oXml->getNodeValue();
                                            $products[$productIndex]['gender_id'] = $gender_id;
                                            break;
                                        case 'material_id':
                                            $material_id = $oXml->getNodeValue();
                                            $products[$productIndex]['material_id'] = $material_id;
                                            break;
                                        case 'sell_size':
                                            $sell_size = $oXml->getNodeValue();
                                            $products[$productIndex]['sell_size'] = $sell_size;
                                            break;
                                        case 'ucenka':
                                            $ucenka = $oXml->getNodeValue();
                                            $products[$productIndex]['ucenka'] = $ucenka;
                                            break;
                                        case 'yz':
                                            $subDepth = $oXml->depth;
                                            while ($oXml->readToNextChildElement($subDepth)) {
                                                switch ($oXml->localName) {
                                                    case 'y':
                                                        $y = $oXml->getNodeValue();
                                                        $products[$productIndex]['width'] = $y;
                                                        break;
                                                    case 'z':
                                                        $z = $oXml->getNodeValue();
                                                        $products[$productIndex]['height'] = $z;
                                                        break;
                                                }
                                            }
                                            break;
                                        case 'priznak':
                                            $subDepth = $oXml->depth;
                                            while ($oXml->readToNextChildElement($subDepth)) {
                                                switch ($oXml->localName) {
                                                    case 'krest':
                                                        $value = $oXml->getNodeValue();
                                                        $products[$productIndex]['krest'] = $value;
                                                        break;
                                                    case 'vypiska':
                                                        $value = $oXml->getNodeValue();
                                                        $products[$productIndex]['vypiska'] = $value;
                                                        break;
                                                    case 'karnaval':
                                                        $value = $oXml->getNodeValue();
                                                        $products[$productIndex]['karnaval'] = $value;
                                                        break;
                                                    case 'torzgest':
                                                        $value = $oXml->getNodeValue();
                                                        $products[$productIndex]['torzgest'] = $value;
                                                        break;
                                                }
                                            }
                                            break;
                                        case 'portal_category_id_prom':
                                            $portal_category_id_prom = $oXml->getNodeValue();
                                            $products[$productIndex]['portal_category_id_prom'] = $portal_category_id_prom;
                                            break;
                                        case 'url_foto_prom':
                                            $subDepth = $oXml->depth;
                                            $urls = [];
                                            while ($oXml->readToNextChildElement($subDepth)) {
                                                switch ($oXml->localName) {
                                                    case 'picture':
                                                        $value = $oXml->getNodeValue();
                                                        $urls[] = $value;
                                                        break;
                                                }
                                            }
                                            $products[$productIndex]['url_foto_prom'] = json_encode($urls);
                                            break;
                                    }
                                }
                            }
                            break;
                        //Справочник цен
                        case 'price':
                            $priceIndex++;
                            $depth = $oXml->depth;
                            while ($oXml->readToNextChildElement($depth)) {
                                switch ($oXml->localName) {
                                    case 'id':
                                        $code = $oXml->getNodeValue();
                                        $prices[$priceIndex]['code'] = $code;
                                        break;
                                    case 'name':
                                        $name = $oXml->getNodeValue();
                                        $prices[$priceIndex]['ru']['name'] = $name;
                                        $prices[$priceIndex]['ua']['name'] = $name;
                                        break;
                                    case 'name_ua':
                                        $name = $oXml->getNodeValue();
                                        if ($name == '') {
                                            $name = $prices[$priceIndex]['ru']['name'] . '-ua';
                                        }
                                        $prices[$priceIndex]['ua']['name'] = $name;
                                        break;
                                }
                            }
                            break;
                        //Справочник валют
                        case 'currency':
                            $currencyIndex++;
                            $depth = $oXml->depth;
                            while ($oXml->readToNextChildElement($depth)) {
                                switch ($oXml->localName) {
                                    case 'id':
                                        $code = $oXml->getNodeValue();
                                        $currencies[$currencyIndex]['code'] = $code;
                                        break;
                                    case 'name':
                                        //$name = $oXml->getNodeValue();
                                        //$currencies[$currencyIndex]['currency'] = $name;
                                        break;
                                }
                            }
                            break;
                        //Справочник характеристик
                        case 'characteristic':
                            if ($last_node_at_depth[$oXml->depth - 2] !== 'product') {
                                $depth = $oXml->depth;
                                $characteristicIndex++;
                                while ($oXml->readToNextChildElement($depth)) {
                                    switch ($oXml->localName) {
                                        case 'id':
                                            $code = $oXml->getNodeValue();
                                            $characteristics[$characteristicIndex]['code'] = $code;
                                            break;
                                        case 'name':
                                            $name = $oXml->getNodeValue();
                                            $characteristics[$characteristicIndex]['name'] = $name;
                                            break;
                                        case 'sizes':
                                            //Размерная сетка. Размер в см к данному параметру (для отображения на размерном малыше)
                                            $depth_size = $oXml->depth;
                                            while ($oXml->readToNextChildElement($depth_size)) {
                                                $characteristics[$characteristicIndex]['sizes'][$oXml->localName] = $oXml->getNodeValue();
                                            }
                                            break;
                                    }
                                }
                            }
                            break;
                        case 'ProdPrice':
                            $depth = $oXml->depth;
                            $ProdPrice++;
                            while ($oXml->readToNextChildElement($depth)) {
                                switch ($oXml->name) {
                                    case 'product_id':
                                        $product_id = trim($oXml->readString());
                                        $pricesToUpdate[$ProdPrice]['product_id'] = $product_id;
                                        break;
//                        case 'characteristic_id':
//                            $characteristic_id = trim($oXml->readString());
//                            $pricesToUpdate[$i]['characteristic_id'] = $characteristic_id;
//                            break;
                                    case 'status':
                                        $sale = trim($oXml->readString());
                                        $pricesToUpdate[$ProdPrice]['sale'] = $sale;
                                        break;
                                    case 'price_id':
                                        $price_id = trim($oXml->readString());
                                        $pricesToUpdate[$ProdPrice]['price_type'] = $price_id;
                                        break;
                                    case 'currency_id':
                                        $currency_id = trim($oXml->readString());
                                        $pricesToUpdate[$ProdPrice]['currency_id'] = $currency_id;
                                        break;
                                    case 'cost':
                                        $cost = trim($oXml->readString());
                                        $pricesToUpdate[$ProdPrice]['price'] = $cost;
                                        break;
                                    case 'cost_discount':
                                        $cost_discount = trim($oXml->readString());
                                        $pricesToUpdate[$ProdPrice]['price_old'] = $cost_discount;
                                        break;
                                    case 'cost_old':
                                        $cost_discount = trim($oXml->readString());
                                        $pricesToUpdate[$ProdPrice]['cost_old'] = $cost_discount;
                                        break;
                                }
                            }
                            break;

                        case 'balance':
                            $depth = $oXml->depth;
                            $balance++;
                            while ($oXml->readToNextChildElement($depth)) {
                                switch ($oXml->name) {
                                    case 'product_id':
                                        $product_id = trim($oXml->readString());
                                        $balanceProductsToChange[$balance]['catalog_id'] = $product_id;
                                        break;
                                    case 'characteristic_id':
                                        $characteristic_id = trim($oXml->readString());
                                        $balanceProductsToChange[$balance]['characteristic_id'] = $characteristic_id;
                                        break;
                                    case 'status':
                                        $status = trim($oXml->readString());
                                        $balanceProductsToChange[$balance]['status'] = $status;
                                        break;
                                    case 'amount':
                                        $amount = trim($oXml->readString());
                                        $balanceProductsToChange[$balance]['amount'] = $amount;
                                        break;
                                }
                            }
                            break;

                    }
                }
            }
            $iterate = $this->chekAdditional(__FUNCTION__, 'iterate') ?: 0;
            $this->updateAdditional(__FUNCTION__, ['iterate' => $iterate + 1]);
            if ($iterate == 0 and $drop == 1) {
                DB::update('catalog')->set(['status' => 0])->execute();
                DB::update('catalog_tree')->set(['status' => 0])->execute();
                DB::update('brands')->set(['status' => 0])->execute();
                DB::update('manufacturers')->set(['status' => 0])->execute();
                DB::update('currencies_courses')->set(['status' => 0])->execute();
                DB::update('price_types')->set(['status' => 0])->execute();
                $specification = Specifications::getRow(Text::translit('Сезон'), 'alias');
                if ($specification) {
                    DB::update('specifications_values')->set(['status' => 0])->where('specification_id', '=', $specification->id)->execute();
                }
                $specification = Specifications::getRow(Text::translit('Материал'), 'alias');
                if ($specification) {
                    DB::update('specifications_values')->set(['status' => 0])->where('specification_id', '=', $specification->id)->execute();
                }
            }
            if (!$this->chekAdditional(__FUNCTION__, 'groups')) {
                $this->modifyGroupsTable($groups, 'update');
                $this->updateAdditional(__FUNCTION__, ['groups' => 1]);
                echo "Справочник групп выгружен\n";
            } else {
                echo "Справочник групп уже был выгружен\n";
            }

            if (!$this->chekAdditional(__FUNCTION__, 'currencies')) {
                $this->modifyCurrenciesTable($currencies);
                $this->updateAdditional(__FUNCTION__, ['currencies' => 1]);
                echo "Справочник валют выгружен\n";
            } else {
                echo "Справочник валют уже был выгружен\n";
            }

            if (!$this->chekAdditional(__FUNCTION__, 'brands')) {
                $this->modifyBrandsTable($brands);
                $this->updateAdditional(__FUNCTION__, ['brands' => 1]);
                echo "Справочник брендов выгружен\n";
            } else {
                echo "Справочник брендов уже был выгружен\n";
            }

            if (!$this->chekAdditional(__FUNCTION__, 'seasons')) {
                $this->modifySeasonsTable($seasons);
                $this->updateAdditional(__FUNCTION__, ['seasons' => 1]);
                echo "Справочник сезонов выгружен\n";
            } else {
                echo "Справочник сезонов уже был выгружен\n";
            }

            if (!$this->chekAdditional(__FUNCTION__, 'genders')) {
                $this->modifyGendersTable($genders);
                $this->updateAdditional(__FUNCTION__, ['genders' => 1]);
                echo "Справочник 'пол ребенка' выгружен\n";
            } else {
                echo "Справочник 'пол ребенка' уже был выгружен\n";
            }

            if (!$this->chekAdditional(__FUNCTION__, 'materials')) {
                $this->modifyMaterialsTable($materials);
                $this->updateAdditional(__FUNCTION__, ['materials' => 1]);
                echo "Справочник материалов выгружен\n";
            } else {
                echo "Справочник материалов уже был выгружен\n";
            }
            if (!$this->chekAdditional(__FUNCTION__, 'manufacturers')) {
                $this->modifyManufacturersTable($manufacturers);
                $this->updateAdditional(__FUNCTION__, ['manufacturers' => 1]);
                echo "Производители выгружены\n";
            } else {
                echo "Производители уже были выгружены\n";
            }

            if (!$this->chekAdditional(__FUNCTION__, 'colors')) {
                $this->modifyColorsTable($colors);
                $this->updateAdditional(__FUNCTION__, ['colors' => 1]);
                echo "Цвета выгружены\n";
            } else {
                echo "Цвета уже были выгружены\n";
            }
            if ($this->chekAdditional(__FUNCTION__, 'characteristics') == 0) {
                $characteristics_offset = $this->chekAdditional(__FUNCTION__, 'characteristics_offset') ?: 0;
                $characteristics_offset_count = $characteristics_offset + 500;

                $this->updateAdditional(__FUNCTION__, ['characteristics' => 0, 'characteristics_offset' => $characteristics_offset + 500]);
                $characteristics_ounts = count($characteristics);
                $characteristics = array_slice($characteristics, $characteristics_offset, $this->limit);
                if (empty($characteristics)) {
                    $this->updateAdditional(__FUNCTION__, ['characteristics' => 1]);
                    echo "Размерная сетка выгружена всего-" . $characteristics_ounts . ", выгружено - " . $characteristics_offset_count . "\n";
                    $this->success(__('Размерная сетка выгружена'));
                } else {
                    $this->modifyHelperCharacteristicsTable($characteristics);
                    echo "Размерная сетка выгружается... всего-" . $characteristics_ounts . ", выгружено - " . $characteristics_offset_count . "\n";
                    $this->success(__('Размерная сетка находится в очереди'));
                }
            } else {
                if (!$this->chekAdditional(__FUNCTION__, 'prices')) {
                    $this->modifyHelperPricesTable($prices);
                    $this->updateAdditional(__FUNCTION__, ['prices' => 1]);
                    echo "Справочник цен выгружен\n";
                } else {
                    echo "Справочник цен уже был выгружен\n";
                }
                //
                $offset = $this->chekAdditional(__FUNCTION__, 'offset') ?: 0;
                $products_offset = $offset + 500;
                $this->updateAdditional(__FUNCTION__, ['products' => 0, 'offset' => $offset + 500]);
                $products_count = count($products);
                $products = array_slice($products, $offset, $this->limit);
                if (empty($products)) {
                    echo "Каталог товаров обновлён\n";
                    if ($this->mode !== 'develop') {
                        @unlink($this->xmlReferencesPath);
                    }
                    /*$this->updateQueue(__FUNCTION__,0);
                    $this->clearAdditional(__FUNCTION__);
                    $this->saveJobAction(__FUNCTION__);*/
                    if ($ajaxCall) {
                        $this->success(__('Таблицы успешно обновлены'));
                    }
                } else {
                    echo "Каталог товаров выгружается... всего - " . $products_count . ", выгружено - " . $products_offset . "\n";
                    $this->modifyProductsTable($products);
                    //$this->updateQueue(__FUNCTION__,1);
                    if ($ajaxCall) {
                        $this->success(__('Таблицы успешно обновлены'));
                    }
                }
                $inserted = 0;
                $updated = 0;
                $skipped = 0;
                if (!empty($pricesToUpdate)) {
                    $offset = $this->chekAdditional(__FUNCTION__, 'prices_update_offset') ?: 0;
                    $this->updateAdditional(__FUNCTION__, ['prices_update_offset' => $offset + 500]);
                    $pricesToUpdate = array_slice($pricesToUpdate, $offset, $this->limit);
                    if (!empty($pricesToUpdate)) {
                        foreach ($pricesToUpdate as $product) {
                            $productExistence = Items::getRow($product['product_id']);
                            if (!$productExistence) {
                                $this->logger->warning(__('Пропущена позиция :item Отсутствует в базе', [':item' => $product['product_id']]));
                                $skipped++;
                                continue;
                            }
                            if (empty($product['price_type'])) {
                                $this->logger->warning(__('Пропущена позиция :item Присутствует в базе,но в файле отсутствует код вида цены', [':item' => $product['product_id']]));
                                $skipped++;
                                continue;
                            }
                            $offer = Prices::getPricesRowByPriceType($product['product_id'], $product['price_type']);
                            $sale = ['sale' => $product['sale']];
                            if ($product['sale'] == 1) {
                                $product['price'] = $product['price_old'];
                                $product['price_old'] = $product['cost_old'];
                            } else {
                                unset($product['cost_old'], $product['price_old']);
                            }
                            unset($product['sale'], $product['cost_old']);
                            if (!$offer) {
                                DB::update(Items::table())->set($sale)->where('id', '=', $product['product_id'])->execute();
                                $this->insertToTable(Prices::table(), $product);
                                $inserted++;
                            } else {
                                DB::update(Items::table())->set($sale)->where('id', '=', $product['product_id'])->execute();
                                $this->updateTable(Prices::table(), $offer->id, $product);
                                $updated++;
                            }

                        }
                        $this->success(__('Цены на товар и характеристики обновляются. Обновлено - :updated, добавлено новых - :inserted, пропущено - :skip', [':updated' => $updated, ':inserted' => $inserted, ':skip' => $skipped]));
                    } else {
                        if ($ajaxCall) {
                            echo __('Цены на товар и характеристики успешно обновлены. Обновлено - :updated, добавлено новых - :inserted, пропущено - :skip', [':updated' => $updated, ':inserted' => $inserted, ':skip' => $skipped]);
                        }
                    }
                }

                $offset = $this->chekAdditional(__FUNCTION__, 'balance_offset') ?: 0;
                $iterate = $this->chekAdditional(__FUNCTION__, 'balance_iterate') ?: 0;
                $this->updateAdditional(__FUNCTION__, ['balance_offset' => $offset + 500, 'balance_iterate' => $iterate + 1]);
                $balanceProductsToChange = array_slice($balanceProductsToChange, $offset, 500);
                if ($iterate == 0 and $drop == 1) {
                    DB::update(Sizes::table())->set(['amount' => 0])->execute();
                }
                if (!empty($balanceProductsToChange)) {
                    foreach ($balanceProductsToChange as $product) {
                        $characteristic_id = HelperCharacteristics::getRow($product['characteristic_id'], 'code');
                        if (!$characteristic_id) {
                            continue;
                        }
                        $product['size'] = $characteristic_id->name;
                        unset($product['characteristic_id']);
                        DB::update(Items::table())->set(['updated_at' => time(), 'status' => 1])->where('id', '=', $product['catalog_id'])->execute();
                        unset($product['status']);
                        $searchableSize = DB::select()->from(Sizes::table())
                            ->where('catalog_id', '=', $product['catalog_id'])
                            ->where('size', '=', $product['size'])
                            ->find();
                        if ($searchableSize) {
                            DB::update(Sizes::table())->set($product)->where('catalog_id', '=', $product['catalog_id'])->where('size', '=', $product['size'])->execute();
                        }

                    }
                    echo 'Остатки обновляются';
                } else {
                    $this->updateQueue(__FUNCTION__, 0);
                    $this->clearAdditional(__FUNCTION__);
                    $this->saveJobAction(__FUNCTION__);
                    echo 'Остатки Обновлены';
                }

            }
        } else if ($ajaxCall) {
            $this->success(__('Другая задача находится в очереди'));
        }
    }

    public function updateInformationReferencesStartAction()
    {
        $queue = DB::select()->from('scheduler')->where('in_queue', '=', 1)->find();
        if (!$queue) {
            $select = DB::select()->from('scheduler')->where('action', '=', 'updateInformationReferencesAction')->find();
            DB::update('scheduler')->where('action', '=', 'updateInformationReferencesAction')->set(['status' => Job::STATUS_INIT, 'start_time' => time(), 'start_time_back' => $select->start_time])->execute();
            $this->success(__('Узел поставлен на выгружение!'));
        } else {
            $this->success(__('В очереди присутствует задача!'));
        }
    }

    /**
     * @modifyProductsTable
     * @param array $products
     * @param string $action ['update','flush']
     * @return bool
     * @throws Throwable
     * @throws Database_Exception
     * @see HelperPrices::table()
     * @see updateTable()
     * @see insertToTable()
     */
    private function modifyProductsTable($products, $action = 'update'): bool
    {
        $table = Items::table();
        if ($action === 'flush') {
            DB::delete($table)->execute();
        }
        //order - parent_id - item - specs
        if (!empty($products)) {
            foreach ($products as $product) {
                $product['status'] = 1;
                $specs = [];
                $searchableProductID = Items::getRowSimple($product['id']);
                //get brand
                $brand = Brands::getRowSimple($product['brand_id'], 'brand_id');
                if ($brand) {
                    $product['brand_alias'] = $brand->alias;
                }
                unset($product['brand_id']);
                $manufacturer = Manufacturers::getRowSimple($product['manufacturer_id'], 'manufacturer_id');
                if ($manufacturer) {
                    $product['manufacturer_alias'] = $manufacturer->alias;
                }
                unset($product['manufacturer_id']);
                $color = Colors::getRowSimple($product['color_id'], 'id');
                if ($color) {
                    $product['color_alias'] = $color->alias;
                }
                unset($product['color_id']);
                $characteristics = $product['characteristics'];
                $ages = $product['ages'];

                unset($product['characteristics'], $product['ages']);
                $specs['season_id'] = $product['season_id'];
                $specs['material_id'] = $product['material_id'];
                $specs['gender_id'] = $product['gender_id'];
                foreach (array_keys($specs) as $index => $array_key) {
                    unset($product[$array_key]);
                }
                if (empty($product['season_id'])) {
                    unset($product['season_id']);
                }
                if (empty($product['material_id'])) {
                    unset($product['material_id']);
                }
                if (empty($product['gender_id'])) {
                    unset($product['gender_id']);
                }

                $group = Groups::getRowSimple($product['parent_id']);
                if (!$group) {
                    $this->logger->warning(__('Произошла ошибка при загрузке данных. Группа товаров :parent отсутствует в базе. Позиция №:item не была загружена.Проверьте существует ли он в файле и попробуйте снова', [':item' => $product['id'], ':parent' => $product['parent_id']]), ['method' => __FUNCTION__]);
                    continue;
                }
                if (!empty($product['card_code'])) {
                    $find = DB::select()->from('catalog_color_group')->where('id', '=', $product['card_code'])->find();
                    if ($find) {
                        $product['catalog_color_group_id'] = $product['card_code'];
                    } else {
                        DB::insert('catalog_color_group', ['id'])->values([$product['card_code']])->execute();
                        $product['catalog_color_group_id'] = $product['card_code'];
                    }
                }
                if ($searchableProductID) {
                    Items::update($product, $searchableProductID->id);
                    //$this->updateTable($table,$searchableProductID->id,$product);
                    //$this->logger->warning('id - ' . $searchableProductID->id .'--'. json_encode($product));
                    //DB::update($table)->set($product)->where('id', '=', $searchableProductID->id)->execute();
                } else {
                    //$this->logger->warning('insert - id - ' . $searchableProductID->id .'--'. json_encode($product));
                    /*$aliasProduct = Text::translit(mb_strtolower($product['name']));
                    $searchableDuplicateByAlias = Items::getRow($aliasProduct,'alias');
                    if($searchableDuplicateByAlias){
                        $this->updateTable($table,$searchableDuplicateByAlias->id,$product);
                    }else{*/


                    if (!isset($product['alias']) && isset($product['ru']['name']) && Common::checkField($table, 'alias')) {
                        $alias = Text::translit(mb_strtolower($product['ru']['name']));
                        $item = DB::select()->from(Items::$table)->where('alias', '=', $alias)->find();
                        if ($item) {
                            $alias .= '_' . $item->color_alias;
                            $item = DB::select()->from(Items::$table)->where('alias', '=', $alias)->find();
                            if ($item) {
                                $alias .= random_int(1000, 9999);
                            }
                        }
                        $product['alias'] = $alias;
                    }
                    Items::insert($product);
                    //$this->insertToTable($table,$product);
                    //}
                }
                if (isset($characteristics)) {
                    $this->modifyCatalogSizes(Sizes::table(), $characteristics, $product['id'], 'update');
                }
                if (isset($ages)) {
                    $this->modifyCatalogAges($ages, $product['id'], 'update');
                } else {
                    ProductAges::delete($product['id'], 'product_id');
                }
                if (!empty($specs)) {
                    $this->modifyCatalogSpecifications($specs, $product['id'], 'update');
                }
            }
        }
        return true;
    }

    /**
     * @modifyCatalogSpecifications
     * @param array $specifications
     * @param int $product_id
     * @param string $action
     * @return bool
     */
    protected function modifyCatalogSpecifications($specifications, $product_id, $action): bool
    {
        $table = CatalogSpecificationsValues::table();
        if ($action === 'update') {
            DB::delete($table)->where('catalog_id', '=', $product_id)->execute();
            $action = 'insert';
        }
        $product = Items::getRow($product_id);
        if (!$product) {
            $this->logger->warning(__('Произошла ошибка при загрузке данных. Позиция №:item отсутствует в базе.Проверьте существует ли он в файле и попробуйте снова', [':item' => $product_id]), ['method' => __FUNCTION__]);
            return false;
        }
        $season = Specifications::getRowSimple('sezon', 'alias');
        $material = Specifications::getRowSimple('material', 'alias');
        $gender = Specifications::getRowSimple('polrebenka', 'alias');

        if ($action === 'insert') {
            foreach ($specifications as $specificationKey => $specification) {
                if ($specificationKey === 'season_id' && !empty($season) && !empty($season->alias)) {
                    $specificationValues[$season->alias] = DB::select('alias')->from(SpecificationsValues::table())->where('specification_id', '=', $season->id)->where('book_keeping_id', '=', $specification)->find();
                }
                if ($specificationKey === 'material_id' && !empty($material) && !empty($material->alias)) {
                    $specificationValues[$material->alias] = DB::select('alias')->from(SpecificationsValues::table())->where('specification_id', '=', $material->id)->where('book_keeping_id', '=', $specification)->find();
                }
                if ($specificationKey === 'gender_id' && !empty($gender) && !empty($gender->alias)) {
                    $specificationValues[$gender->alias] = DB::select('alias')->from(SpecificationsValues::table())->where('specification_id', '=', $gender->id)->where('book_keeping_id', '=', $specification)->find();
                }
            }
            if (!empty($specificationValues)) {
                $specArray = [];
                foreach ($specificationValues as $specification_alias => $specification_value_alias) {
                    if ($specification_value_alias->alias) {
                        $specArray[$specification_alias] = $specification_value_alias->alias;
                        DB::insert($table, ['catalog_id', 'specification_alias', 'specification_value_alias'])->values([$product_id, $specification_alias, $specification_value_alias->alias])->execute();
                    }
                }
                Common::factory('catalog')->update(['specifications' => empty($specArray) ? NULL : json_encode($specArray)], $product_id);
            }
        }
        return true;
    }

    /**
     * @modifyProductAges
     * @param array $ages
     * @param int $product_id
     * @param string $action
     * @return bool
     * @throws Database_Exception
     */
    protected function modifyProductAges($ages, $product_id, $action): bool
    {
        $ages_table = ProductAges::table();
        $row = null;
        $product = Items::getRow($product_id);
        if ($action === 'update') {
            DB::delete($ages_table)->where('product_id', '=', $product_id)->execute();
            $action = 'insert';
        }
        if (!$product) {
            $this->logger->warning(__('Произошла ошибка при загрузке данных. Позиция №:item отсутствует в базе.Проверьте существует ли он в файле и попробуйте снова', [':item' => $product_id]), ['method' => __FUNCTION__]);
            return false;
        }
        if ($action === 'insert') {
            foreach ($ages as $age) {
                $searchableAge = Ages::getRow(Text::translit($age), 'alias');
                if (!$searchableAge) {
                    $age_id = $this->insertToTable(Ages::table(), $age, true);
                    $row = Ages::getRow($age_id);
                } else {
                    $row = $searchableAge;
                }
                DB::insert($ages_table, ['created_at', 'product_id', 'age_type'])->values([time(), $product_id, $row->alias])->execute();
            }
        }
        return true;
    }

    /**
     * @modifyCatalogAges
     * @param array $ages
     * @param int $product_id
     * @param string $action
     * @return bool
     * @throws Database_Exception
     */
    protected function modifyCatalogAges($ages, $product_id, $action): bool
    {
        if ($action === 'update') {
            ProductAges::delete($product_id, 'product_id');
            $action = 'insert';
        }
        $product = Items::getRow($product_id);
        if (!$product) {
            $this->logger->warning(__('Произошла ошибка при загрузке данных. Позиция №:item отсутствует в базе.Проверьте существует ли он в файле и попробуйте снова', [':item' => $product_id]), ['method' => __FUNCTION__]);
            return false;
        }
        if ($action === 'insert') {
            foreach ($ages as $age) {
                $searchableAge = Ages::getRow($age, 'code');
                $searchableAgeGroup = DB::select()->from(ProductAges::table())
                    ->where('age_type', '=', $age)
                    ->where('product_id', '=', $product_id)
                    ->find();
                if ($searchableAge && !$searchableAgeGroup) {
                    $this->insertToTable(ProductAges::table(), ['product_id' => $product_id, 'age_type' => $age]);
                }
            }
        }
        return true;
    }

    /**
     * @modifyCatalogSizes
     * @param string $table_sizes
     * @param array $sizes
     * @param int $product_id
     * @param string $action
     * @return bool
     */
    protected function modifyCatalogSizes($table_sizes, $sizes, $product_id, $action): bool
    {
        if ($action === 'update') {
            DB::delete($table_sizes)->where('catalog_id', '=', $product_id)->execute();
            $action = 'insert';
        }
        $product = Items::getRow($product_id);
        if (!$product) {
            $this->logger->warning(__('Произошла ошибка при загрузке данных. Позиция №:item отсутствует в базе.Проверьте существует ли он в файле и попробуйте снова', [':item' => $product_id]), ['method' => __FUNCTION__]);
            return false;
        }
        if ($action === 'insert') {
            foreach ($sizes as $size) {
                $sizeRow = HelperCharacteristics::getRow($size, 'code');
                if (!$sizeRow and $size == '11111111-1111-1111-1111-111111111111') {
                    DB::insert(HelperCharacteristics::$table, ['created_at', 'code', 'name'])->values([time(), $size, '-'])->execute();
                    $sizeRow = HelperCharacteristics::getRow($size, 'code');
                }
                $searchableSizeGroup = DB::select()->from($table_sizes)
                    ->where('size', '=', $sizeRow->name)
                    ->where('catalog_id', '=', $product_id)
                    ->find();
                if ($sizeRow && !$searchableSizeGroup) {
                    DB::insert($table_sizes, ['catalog_id', 'size', 'amount', 'code'])->values([$product_id, $sizeRow->name, 0, $size])->execute();
                }
            }
        }
        return true;
    }

    /**
     * @modifyHelperPricesTable
     * @param array $prices
     * @param string $action ['update','flush']
     * @return bool
     * @throws Throwable
     * @throws Database_Exception
     * @see HelperPrices::table()
     * @see updateTable()
     * @see insertToTable()
     */
    private function modifyHelperPricesTable($prices, $action = 'update'): bool
    {
        $table = HelperPrices::table();
        if ($action === 'flush') {
            DB::delete($table)->execute();
        }
        if (!empty($prices)) {
            foreach ($prices as $price) {
                $price['status'] = 1;
                $searchableHelperPriceID = HelperPrices::getRowSimple($price['code'], 'code');
                if ($searchableHelperPriceID) {
                    HelperPrices::update($price, $searchableHelperPriceID->id);
                    //$this->updateTable($table,$searchableHelperPriceID->id,$price);
                } else {
                    HelperPrices::insert($price);
                    //$this->insertToTable($table,$price);
                }
            }
        }
        return true;
    }


    /**
     * @modifyHelperCharacteristicsTable
     * @param array $characteristics
     * @param string $action ['update','flush']
     * @return bool
     * @throws Throwable
     * @throws Database_Exception
     * @see Brands::table()
     * @see updateTable()
     * @see insertToTable()
     */
    private function modifyHelperCharacteristicsTable($characteristics, $action = 'update'): bool
    {
        $table = HelperCharacteristics::table();
        $table_sizes = 'helper_characteristics_sizes';
        if ($action === 'flush') {
            //DB::delete($table)->execute();
        }
        if (!empty($characteristics)) {
            foreach ($characteristics as $characteristic) {
                $searchableCharacteristicID = HelperCharacteristics::getRow($characteristic['code'], 'code');
                if ($searchableCharacteristicID) {
                    if (isset($characteristic['sizes'])) {
                        $this->modifySizes($table_sizes, $characteristic['sizes'], $searchableCharacteristicID->id, 'update');
                        unset($characteristic['sizes']);
                    }
                    $this->updateTable($table, $searchableCharacteristicID->id, $characteristic);
                } else {
                    $sizes = $characteristic['sizes'];
                    $insertedID = $this->insertToTable($table, $characteristic, true);
                    $this->modifySizes('helper_characteristics_sizes', $sizes, $insertedID, 'insert');
                }
            }
        }
        return true;
    }

    /**
     * @param string $table_sizes
     * @param array $sizes
     * @param int $helper_id
     * @param string $action
     * @return bool
     */
    protected function modifySizes($table_sizes, $sizes, $helper_id, $action): bool
    {
        if ($action === 'update') {
            DB::delete($table_sizes)->where('helper_characteristics_id', '=', $helper_id)->execute();
            $action = 'insert';
        }
        if (!empty($sizes)) {
            foreach ($sizes as $mark => $size) {
                if ($action === 'insert') {
                    DB::insert($table_sizes, ['created_at', 'helper_characteristics_id', 'parameter', 'mark'])->values([time(), $helper_id, $size, $mark])->execute();
                }
            }
        }
        return true;
    }

    /**
     * @modifyBrandsTable
     * @param array $brands
     * @param string $action ['update','flush']
     * @return bool
     * @throws Throwable
     * @throws Database_Exception
     * @see Brands::table()
     * @see updateTable()
     * @see insertToTable()
     */
    private function modifyBrandsTable($brands, $action = 'update'): bool
    {
        $table = Brands::table();
        if ($action === 'flush') {
            DB::delete($table)->execute();
        }
        if (!empty($brands)) {
            foreach ($brands as $brand) {
                if (Common::checkField($table, 'status') && !isset($brand['status'])) {
                    $brand['status'] = 1;
                }
                $searchableBrandID = Brands::getRowSimple($brand['brand_id'], 'brand_id');
                if ($searchableBrandID) {
                    Brands::update($brand, $searchableBrandID->id);
                    //$this->updateTable($table,$searchableBrandID->id,$brand);
                } else {
                    $brand['alias'] = Text::translit($brand['ru']['name']);
                    $searchableAliasBrand = DB::select()->from($table)
                        ->where('alias', '=', $brand['alias'])
                        ->find();
                    if ($searchableAliasBrand) {
                        $brand['alias'] = $brand['alias'] . Text::random('hexdec', 5);
                    }
                    Brands::insert($brand);
                }
            }
        }
        return true;
    }

    /**
     * @modifySeasonsTable
     * @param array $seasons
     * @param string $action ['update','flush']
     * @return bool
     * @throws Throwable
     * @throws Database_Exception
     * @see SpecificationsValues::table()
     * @see updateTable()
     * @see insertToTable()
     */
    private function modifySeasonsTable($seasons, $action = 'update'): bool
    {
        $table = SpecificationsValues::table();
        if ($action === 'flush') {
            DB::delete($table)->execute();
        }
        if (!empty($seasons)) {
            foreach ($seasons as $season) {
                if (!Common::checkField($table, 'book_keeping_id')) {
                    unset($season['season_id']);
                }
                if (isset($season['season_id']) && !isset($season['book_keeping_id'])) {
                    $season['book_keeping_id'] = $season['season_id'];
                    unset($season['season_id']);
                }
                $this->modifySpecificationsTables($season, 'Сезон', $table);
            }
        }
        return true;
    }

    /**
     * @modifyMaterialsTable
     * @param array $materials
     * @param string $action ['update','flush']
     * @return bool
     * @throws Throwable
     * @throws Database_Exception
     * @see SpecificationsValues::table()
     * @see updateTable()
     * @see insertToTable()
     */
    private function modifyMaterialsTable($materials, $action = 'update'): bool
    {
        $table = SpecificationsValues::table();
        if ($action === 'flush') {
            DB::delete($table)->execute();
        }
        if (!empty($materials)) {
            foreach ($materials as $material) {
                if (!Common::checkField($table, 'book_keeping_id')) {
                    unset($material['material_id']);
                }
                if (isset($material['material_id']) && !isset($material['book_keeping_id'])) {
                    $material['book_keeping_id'] = $material['material_id'];
                    unset($material['material_id']);
                }
                $this->modifySpecificationsTables($material, 'Материал', $table);
            }
        }
        return true;
    }

    /**
     * @modifyGendersTable
     * @param array $genders
     * @param string $action ['update','flush']
     * @return bool
     * @throws Throwable
     * @throws Database_Exception
     * @see SpecificationsValues::table()
     * @see updateTable()
     * @see insertToTable()
     */
    private function modifyGendersTable($genders, $action = 'update'): bool
    {
        $table = SpecificationsValues::table();
        if ($action === 'flush') {
            DB::delete($table)->execute();
        }
        if (!empty($genders)) {
            foreach ($genders as $gender) {
                if (!Common::checkField($table, 'book_keeping_id')) {
                    unset($gender['gender_id']);
                }
                if (isset($gender['gender_id']) && !isset($gender['book_keeping_id'])) {
                    $gender['book_keeping_id'] = $gender['gender_id'];
                    unset($gender['gender_id']);
                }
                $this->modifySpecificationsTables($gender, 'Пол ребенка', $table);
            }
        }
        return true;
    }


    /**
     * @modifyCurrenciesTable
     * @param array $currencies
     * @param string $action ['update','flush']
     * @return bool
     * @throws Throwable
     * @throws Database_Exception
     * @see Currency::table()
     * @see updateTable()
     * @see insertToTable()
     */
    private function modifyCurrenciesTable($currencies, $action = 'update'): bool
    {
        $table = Currency::table();
        if ($action === 'flush') {
            DB::delete($table)->execute();
        }
        if (!empty($currencies)) {
            foreach ($currencies as $currency) {
                $searchableCodeCurrency = Currency::getRow($currency['code']);
                if (!isset($currency['id'])) {
                    $currency['id'] = $currency['code'];
                    unset($currency['code']);
                }
                if (!isset($currency['display'])) {
                    $currency['display'] = $currency['name'];
                }
                if (Common::checkField($table, 'status') && !isset($currency['status'])) {
                    $currency['status'] = 1;
                }
                if ($searchableCodeCurrency) {
                    $this->updateTable($table, $searchableCodeCurrency->id, $currency);
                } else {
                    $this->insertToTable($table, $currency);
                }

            }
        }
        return true;
    }

    /**
     * @modifyGroupsTable
     * @param array $groups
     * @param string $action ['update','flush']
     * @return bool
     * @throws Throwable
     * @throws Database_Exception
     * @see Groups::table()
     * @see updateTable()
     * @see insertToTable()
     */
    private function modifyGroupsTable($groups, $action = 'update'): bool
    {
        $table = Groups::table();
        if ($action === 'flush') {
            DB::delete($table)->execute();
        }
        if (!empty($groups)) {
            foreach ($groups as $k => $group) {
                if (!isset($group['status'])) {
                    $group['status'] = 1;
                }
                if (!isset($group['sort'])) {
                    $group['sort'] = $k;
                }
                /*if($group['parent_id'] == 0){
                    $searchableParentGroup = DB::select()->from($table)
                        ->where('alias', '=', Text::translit($group['name']))
                        ->find();
                    if($searchableParentGroup){
                        Groups::update(['parent_id' => $group['parent_id'],'id'=>$group['id'], 'name'=> $group['name']],$searchableParentGroup->id);
                    }else{
                        $this->insertToTable($table,$group);
                    }
                }else{*/
                if (Common::checkField($table, 'status') && !isset($group['status'])) {
                    $group['status'] = 1;
                }
                $searchableGroup = DB::select()->from($table)
                    ->where('id', '=', $group['id'])
                    ->find();
                if ($searchableGroup) {
                    Groups::update($group, $searchableGroup->id);
                } else {
                    $group['alias'] = Text::translit($group['ru']['name']);
                    $searchableAliasGroup = DB::select()->from($table)
                        ->where('alias', '=', $group['alias'])
                        ->find();
                    if ($searchableAliasGroup) {

                        $group['alias'] = $group['alias'] . Text::random('hexdec', 5);
                    }
                    Groups::insert($group);
                }
                /*}*/
            }
        }
        return true;
    }

    /**
     * @modifyManufacturersTable
     * @param array $manufacturers
     * @param string $action ['update','flush']
     * @return bool
     * @throws Throwable
     * @throws Database_Exception
     * @see Manufacturers::table()
     * @see updateTable()
     * @see insertToTable()
     */
    private function modifyManufacturersTable($manufacturers, $action = 'update'): bool
    {
        $table = Manufacturers::table();
        if ($action === 'flush') {
            DB::delete($table)->execute();
        }
        if (!empty($manufacturers)) {
            foreach ($manufacturers as $manufacturer) {
                if (!Common::checkField($table, 'manufacturer_id')) {
                    unset($manufacturer['manufacturer_id']);
                }
                if (Common::checkField($table, 'status') && !isset($manufacturer['status'])) {
                    $manufacturer['status'] = 1;
                }
                $searchableManufacturer = DB::select('id')->from($table)
                    ->where('manufacturer_id', '=', $manufacturer['manufacturer_id'])
                    ->find();
                if ($searchableManufacturer) {
                    Manufacturers::update($manufacturer, $searchableManufacturer->id);
                    //$this->updateTable($table,$searchableManufacturer->id,$manufacturer);
                } else {
                    $manufacturer['alias'] = Text::translit($manufacturer['ru']['name']);
                    Manufacturers::insert($manufacturer);
                    //$this->insertToTable($table,$manufacturer);
                }
            }
        }
        return true;
    }

    /**
     * @modifyColorsTable
     * @param array $colors
     * @param string $action ['update','flush']
     * @return bool
     * @throws Throwable
     * @throws Database_Exception
     * @see Colors::table()
     * @see Colors()
     * @see insertToTable()
     */
    private function modifyColorsTable($colors, $action = 'update'): bool
    {
        $table = Colors::table();
        if ($action === 'flush') {
            DB::delete($table)->execute();
        }
        if (!empty($colors)) {
            foreach ($colors as $color) {
                $searchableColor = DB::select('id')->from($table)
                    ->where('id', '=', $color['color_id'])
                    ->find();
                if ($searchableColor) {
                    unset($color['color_id']);
                    Colors::update($color, $searchableColor->id);
                    //$this->updateTable($table,$searchableColor->id,$color);
                } else {
                    $color['id'] = $color['color_id'];
                    unset($color['color_id']);
                    $color['alias'] = Text::translit($color['ru']['name']);

                    Colors::insert($color);
                    //$this->insertToTable($table,$color);
                }
            }
        }
        return true;
    }

    /**
     * @updateTable
     * @param string $table
     * @param integer $idToUpdate
     * @param array $parametersToUpdate
     * @return bool
     * @throws Database_Exception
     * @throws Throwable
     */
    protected function updateTable($table, $idToUpdate, $parametersToUpdate = []): bool
    {
        $db = Database::instance();
        if (!isset($parametersToUpdate['updated_at']) && Common::checkField($table, 'updated_at')) {
            $parametersToUpdate['updated_at'] = time();
        }
        $db->begin();
        try {
            DB::update($table)->set($parametersToUpdate)->where('id', '=', $idToUpdate)->execute();
            $db->commit();
            return true;
        } catch (Exception $e) {
            $db->rollback();
            throw new Database_Exception(':error', [
                ':error' => $e->getMessage()
            ], $e->getCode());
        } catch (Throwable $e) {
            $db->rollback();
            throw new Database_Exception(':error', [
                ':error' => $e->getMessage()
            ], $e->getCode());
        }
    }

    /**
     * @insertToTable method
     * @param string $table
     * @param array $parametersToInsert
     * @param bool $returnID
     * @return bool|Database_Result_Cached|object
     * @throws Database_Exception
     */
    protected function insertToTable($table, $parametersToInsert = [], $returnID = false)
    {
        $db = Database::instance();
        $db->begin();
        try {
            $fields = $parameters = [];
            if (!isset($parametersToInsert['created_at']) && Common::checkField($table, 'created_at')) {
                $parametersToInsert['created_at'] = time();
            }
            if (!isset($parametersToInsert['alias']) && isset($parametersToInsert['name']) && Common::checkField($table, 'alias')) {
                $alias = Text::translit(mb_strtolower($parametersToInsert['name']));
                $duplicate = Common::factory($table)->getRow($alias, 'alias');
                if ($duplicate) {
                    $random = Text::random('alnum', 4);
                    $alias .= $random;
                }
                $parametersToInsert['alias'] = $alias;
            }
            if (isset($parametersToInsert['sizes']) && $table === HelperCharacteristics::table()) {
                unset($parametersToInsert['sizes']);
            }
            foreach ($parametersToInsert as $field => $parameter) {
                $fields[] = $field;
                $parameters[] = $parameter;
            }
            $insertedID = DB::insert($table, $fields)->values($parameters)->execute();
            $db->commit();
            if ($returnID) {
                return $insertedID[0];
            }
            return true;
        } catch (Exception $e) {
            $db->rollback();
            $this->logger->error($e->getMessage(), ['method' => __FUNCTION__, 'line' => $e->getLine(), 'code' => $e->getCode()]);
            throw new Database_Exception(':error', [
                ':error' => $e->getMessage()
            ], $e->getMessage());
        } catch (Throwable $e) {
            $this->logger->error($e->getMessage(), ['method' => __FUNCTION__, 'line' => $e->getLine(), 'code' => $e->getCode()]);
            throw new Database_Exception(':error', [
                ':error' => $e->getMessage()
            ], $e->getMessage());
        }
    }

    /**
     * @updateBalanceAction
     * Function for updating catalog/spec table
     * @param bool $ajaxCall
     */
    public function updateBalanceAction($ajaxCall = true): void
    {
        if (!$this->oXml) {
            $this->initCron();
        }
        $oXml = $this->oXml;
        if (!$this->queue) {
            $this->updateQueue(__FUNCTION__, 1);
            if ($this->connection == 0) {
                $this->FTP(__FUNCTION__, 'file', 'Availability');
            }
        }
        $functionQueue = $this->chekQueue(__FUNCTION__);
        if ($functionQueue) {

            $this->validate($oXml, $this->xmlBalancePath);
            $balanceProductsToChange = [];
            $i = 0;
            $drop = false;
            while ($oXml->read()) {
                if ($oXml->nodeType === XMLReader::ELEMENT) {
                    switch ($oXml->name) {
                        case 'balance':
                            $i++;
                            break;
                        case 'FULL':
                            $drop = trim($oXml->readString());
                            break;
                        case 'product_id':
                            $product_id = trim($oXml->readString());
                            $balanceProductsToChange[$i]['catalog_id'] = $product_id;
                            break;
                        case 'characteristic_id':
                            $characteristic_id = trim($oXml->readString());
                            $balanceProductsToChange[$i]['characteristic_id'] = $characteristic_id;
                            break;
                        case 'status':
                            $status = trim($oXml->readString());
                            $balanceProductsToChange[$i]['status'] = $status;
                            break;
                        case 'amount':
                            $amount = trim($oXml->readString());
                            $balanceProductsToChange[$i]['amount'] = $amount;
                            break;
                    }
                }
            }
            //var_dump($drop); die();
            $offset = $this->chekAdditional(__FUNCTION__, 'offset') ?: 0;
            $iterate = $this->chekAdditional(__FUNCTION__, 'iterate') ?: 0;
            $products_offset = $offset + 500;
            $this->updateAdditional(__FUNCTION__, ['offset' => $offset + 500, 'iterate' => $iterate + 1]);
            $balanceProductsToChange_count = count($balanceProductsToChange);
            $balanceProductsToChange = array_slice($balanceProductsToChange, $offset, 500);
            if ($balanceProductsToChange_count > $offset + 500) {
                $finish = false;
            } else {
                $finish = true;
            }
            if ($iterate == 0 and $drop == 1) {
                DB::update(Sizes::table())->set(['amount' => 0])->execute();
            }
            if (!empty($balanceProductsToChange)) {
                foreach ($balanceProductsToChange as $product) {
                    $characteristic_id = HelperCharacteristics::getRow($product['characteristic_id'], 'code');
                    if (!$characteristic_id) {
                        continue;
                    }
                    $product['size'] = $characteristic_id->name;
                    unset($product['characteristic_id']);
                    DB::update(Items::table())->set(['updated_at' => time(), 'status' => 1])->where('id', '=', $product['catalog_id'])->execute();
                    unset($product['status']);
                    $searchableSize = DB::select()->from(Sizes::table())
                        ->where('catalog_id', '=', $product['catalog_id'])
                        ->where('size', '=', $product['size'])
                        ->find();
                    if ($searchableSize) {
                        DB::update(Sizes::table())->set($product)->where('catalog_id', '=', $product['catalog_id'])->where('size', '=', $product['size'])->execute();
                    }

                }
            }
            if ($this->mode !== 'develop') {
                @unlink($this->xmlBalancePath);
            }
            if ($finish) {
                $this->updateQueue(__FUNCTION__, 0);
                $this->clearAdditional(__FUNCTION__);
                $this->saveJobAction(__FUNCTION__);
                if ($ajaxCall) {

                    $this->success(__('Остатки успешно обновлены'));
                }
            } else {
                $this->success(__('Остатки успешно обновляются'));
            }
        } else if ($ajaxCall) {
            $this->success(__('Другая задача находится в очереди'));
        }
    }

    public function updateFeedDataAction($ajaxCall = true)
    {
        if (!$this->oXml) $this->initCron();
        $oXml = $this->oXml;
        if (!$this->queue) {
            $this->updateQueue(__FUNCTION__, 1);
        }

        $functionQueue = $this->chekQueue(__FUNCTION__);

        if ($functionQueue) {
            $defaultLocaleCode = Config::get('i18n.default');
            $locales = DB::select()->from('i18n')->execute()->as_array('alias');

            foreach ($locales as $code => $locale) {
                $dom = new \domDocument('1.0', 'utf-8');
                $root = $dom->createElement('rss');
                $root->setAttribute('xmlns:g', 'http://base.google.com/ns/1.0');
                $root->setAttribute('version', '2.0');

                $channel = $dom->createElement('channel');
                $title = $dom->createElement('title', 'Babykroha');
                $channel->appendChild($title);
                $link = $dom->createElement('link', 'babykroha.loc');
                $channel->appendChild($link);

                $this->getCategoryRelations($code);
                $products = $this->getFeedProducts($code);

                foreach ($products as $product) {
                    $item = $dom->createElement('item');
                    $id = $dom->createElement('g:id', $product['id']);
                    $item->appendChild($id);
                    $name = $dom->createElement('g:title', htmlspecialchars($product['name']));
                    $item->appendChild($name);
                    $description = $dom->createElement('g:description', '<![CDATA[' . htmlspecialchars($product['content']) . ']]>');
                    $item->appendChild($description);

                    $url = $code != $defaultLocaleCode
                        ? HTML::link2($product['alias'], true, 'https', false, $code)
                        : HTML::link2($product['alias'], true, 'https');
                    $link = $dom->createElement('g:link', $url);
                    $item->appendChild($link);
                    if (!empty($product['image'])) {
                        $image = $dom->createElement(
                            'g:image_link',
                            HTML::media2('images/catalog/original/' . $product['image'], true, 'https')
                        );
                        $item->appendChild($image);
                    }
                    $images = json_decode($product['images'], true);
                    if ($images) {
                        foreach ($images as $image) {
                            if ($image == $product['image']) continue;

                            $additionalImage = $dom->createElement(
                                'g:additional_image_link',
                                HTML::media2('images/catalog/original/' . $image, true, 'https')
                            );
                            $item->appendChild($additionalImage);
                        }
                    }

                    $condition = $dom->createElement('g:condition', 'new');
                    $item->appendChild($condition);

                    $availability = $dom->createElement('g:availability', 'in stock');
                    $item->appendChild($availability);

                    $currency = $product['currency'] == 'грн' ? 'UAH' : $product['currency'];
                    if ($product['price_old'] && $product['price'] < $product['price_old']) {
                        $price = $dom->createElement('g:price', $product['price_old'] . ' ' . $currency);
                        $item->appendChild($price);
                        $salePrice = $dom->createElement('g:sale_price', $product['price'] . ' ' . $currency);
                        $item->appendChild($salePrice);
                    } else {
                        $price = $dom->createElement('g:price', $product['price'] . ' ' . $currency);
                        $item->appendChild($price);
                    }

                    $category = $dom->createElement('g:product_type', $this->getCategoryNameForFeed($product['parent_id']));
                    $item->appendChild($category);

                    $brand = $dom->createElement('g:brand', $product['brand']);
                    $item->appendChild($brand);

                    $channel->appendChild($item);
                }

                $root->appendChild($channel);
                $dom->appendChild($root);
                $dom->formatOutput = true;
                $dom->save($this->feedDir . 'google-merchant-' . $locale['alias'] . '.xml');
            }

            $this->updateQueue(__FUNCTION__, 0);
            $this->saveJobAction(__FUNCTION__);
            if ($ajaxCall) {
                $this->success(__('фид успешно обновлен'));
            }

        } elseif ($ajaxCall) {
            $this->success(__('Другая задача находится в очереди'));
        }
    }

    protected function getFeedProducts($locale)
    {
        return DB::select(
            'c.id',
            'c.parent_id',
            'c.alias',
            'ct.name',
            ['bt.name', 'brand'],
            'ct.content',
            'c.image',
            'p.price',
            'p.price_old',
            'cur.currency',
            [DB::expr("(SELECT CONCAT('[', GROUP_CONCAT(CONCAT('\"', image, '\"')), ']') FROM catalog_images WHERE catalog_id = c.id)"), 'images']
        )
            ->from(['catalog', 'c'])
            ->join(['catalog_i18n', 'ct'], 'inner')
            ->on('c.id', '=', 'ct.row_id')
            ->on('ct.language', '=', DB::expr("'$locale'"))
            ->join(['product_prices', 'p'], 'inner')
            ->on('p.product_id', '=', 'c.id')
            ->on('p.price_type', '=', DB::expr("'875e4eb9-364f-11ea-80cc-a4bf01075a5b'"))
            ->join(['currencies_courses', 'cur'], 'inner')
            ->on('cur.id', '=', 'p.currency_id')
            ->join(['brands', 'b'], 'left')
            ->on('c.brand_alias', '=', 'b.alias')
            ->join(['brands_i18n', 'bt'], 'left')
            ->on('bt.row_id', '=', 'b.id')
            ->on('bt.language', '=', DB::expr("'$locale'"))
            ->where('c.status', '=', 1)
            ->execute()
            ->as_array();
    }

    protected function getCategoryRelations($locale)
    {
        $categories = DB::select('c.id', 'c.parent_id', 'ct.name')
            ->from(['catalog_tree', 'c'])
            ->join(['catalog_tree_i18n', 'ct'], 'inner')
            ->on('ct.row_id', '=', 'c.id')
            ->on('ct.language', '=', DB::expr("'$locale'"))
            ->execute()
            ->as_array();

        foreach ($categories as $category) {
            $this->categoryRelations[$category['id']] = array(
                'parent' => $category['parent_id'],
                'name' => $category['name']
            );
        }
    }

    protected function getCategoryNameForFeed($id, $name = '')
    {
        $category = $this->categoryRelations[$id];
        $name = !empty($name) ? $category['name'] . ' > ' . $name : $category['name'];

        if ($category['parent'] == 0) return $name;

        return $this->getCategoryNameForFeed($category['parent'], $name);
    }


    /**
     * @updatePriceDataAction
     * Function for updating prices
     * @param bool $ajaxCall
     * @throws Database_Exception
     * @throws Throwable
     */
    public function updatePriceDataAction($ajaxCall = true)
    {
        if (!$this->oXml) {
            $this->initCron();
        }
        $oXml = $this->oXml;
        if (!$this->queue) {
            $this->updateQueue(__FUNCTION__, 1);
            if ($this->connection == 0) {
                $this->FTP(__FUNCTION__, 'file', 'Prices');
            }
        }
        $functionQueue = $this->chekQueue(__FUNCTION__);
        if ($functionQueue) {

            $this->validate($oXml, $this->xmlPricePath);
            $pricesToUpdate = [];
            $i = 0;
            while ($oXml->read()) {
                if ($oXml->nodeType === XMLReader::ELEMENT) {
                    switch ($oXml->name) {
                        case 'ProdPrice':
                            $i++;
                            break;
                        case 'product_id':
                            $product_id = trim($oXml->readString());
                            $pricesToUpdate[$i]['product_id'] = $product_id;
                            break;
//                        case 'characteristic_id':
//                            $characteristic_id = trim($oXml->readString());
//                            $pricesToUpdate[$i]['characteristic_id'] = $characteristic_id;
//                            break;
                        case 'status':
                            $sale = trim($oXml->readString());
                            $pricesToUpdate[$i]['sale'] = $sale;
                            break;
                        case 'price_id':
                            $price_id = trim($oXml->readString());
                            $pricesToUpdate[$i]['price_type'] = $price_id;
                            break;
                        case 'currency_id':
                            $currency_id = trim($oXml->readString());
                            $pricesToUpdate[$i]['currency_id'] = $currency_id;
                            break;
                        case 'cost':
                            $cost = trim($oXml->readString());
                            $pricesToUpdate[$i]['price'] = $cost;
                            break;
                        case 'cost_discount':
                            $cost_discount = trim($oXml->readString());
                            $pricesToUpdate[$i]['price_old'] = $cost_discount;
                            break;
                        case 'cost_old':
                            $cost_discount = trim($oXml->readString());
                            $pricesToUpdate[$i]['cost_old'] = $cost_discount;
                            break;
                    }
                }
            }
            $inserted = 0;
            $updated = 0;
            $skipped = 0;
            if (!empty($pricesToUpdate)) {
                $offset = $this->chekAdditional(__FUNCTION__, 'offset') ?: 0;
                $this->updateAdditional(__FUNCTION__, ['offset' => $offset + 500]);
                $pricesToUpdate = array_slice($pricesToUpdate, $offset, $this->limit);
                if (!empty($pricesToUpdate)) {
                    foreach ($pricesToUpdate as $product) {
                        $productExistence = Items::getRow($product['product_id']);
                        if (!$productExistence) {
                            $this->logger->warning(__('Пропущена позиция :item Отсутствует в базе', [':item' => $product['product_id']]));
                            $skipped++;
                            continue;
                        }
                        if (empty($product['price_type'])) {
                            $this->logger->warning(__('Пропущена позиция :item Присутствует в базе,но в файле отсутствует код вида цены', [':item' => $product['product_id']]));
                            $skipped++;
                            continue;
                        }
                        $offer = Prices::getPricesRowByPriceType($product['product_id'], $product['price_type']);
                        $sale = ['sale' => $product['sale']];
                        if ($product['sale'] == 1) {
                            $product['price'] = $product['price_old'];
                            $product['price_old'] = $product['cost_old'];
                        } else {
                            unset($product['cost_old'], $product['price_old']);
                        }
                        unset($product['sale'], $product['cost_old']);
                        if (!$offer) {
                            DB::update(Items::table())->set($sale)->where('id', '=', $product['product_id'])->execute();
                            $this->insertToTable(Prices::table(), $product);
                            $inserted++;
                        } else {
                            DB::update(Items::table())->set($sale)->where('id', '=', $product['product_id'])->execute();
                            $this->updateTable(Prices::table(), $offer->id, $product);
                            $updated++;
                        }

                    }
                    $this->success(__('Цены на товар и характеристики обновляются. Обновлено - :updated, добавлено новых - :inserted, пропущено - :skip', [':updated' => $updated, ':inserted' => $inserted, ':skip' => $skipped]));
                } else {
                    if ($this->mode !== 'develop') {
                        @unlink($this->xmlPricePath);
                    }
                    $this->updateQueue(__FUNCTION__, 0);
                    $this->clearAdditional(__FUNCTION__);
                    $this->saveJobAction(__FUNCTION__);
                    if ($ajaxCall) {

                        $this->logger->info(__('Цены на товар и характеристики успешно обновлены. Обновлено - :updated, добавлено новых - :inserted, пропущено - :skip', [':updated' => $updated, ':inserted' => $inserted, ':skip' => $skipped]), ['method' => __FUNCTION__]);
                        $this->success(__('Цены на товар и характеристики успешно обновлены. Обновлено - :updated, добавлено новых - :inserted, пропущено - :skip', [':updated' => $updated, ':inserted' => $inserted, ':skip' => $skipped]));
                    }
                }
            }
            if ($this->mode !== 'develop') {
                @unlink($this->xmlPricePath);
            }
            $this->updateQueue(__FUNCTION__, 0);
            $this->clearAdditional(__FUNCTION__);
            $this->saveJobAction(__FUNCTION__);
            if ($ajaxCall) {

                $this->logger->info(__('Цены на товар и характеристики успешно обновлены. Обновлено - :updated, добавлено новых - :inserted, пропущено - :skip', [':updated' => $updated, ':inserted' => $inserted, ':skip' => $skipped]), ['method' => __FUNCTION__]);
                $this->success(__('Цены на товар и характеристики успешно обновлены. Обновлено - :updated, добавлено новых - :inserted, пропущено - :skip', [':updated' => $updated, ':inserted' => $inserted, ':skip' => $skipped]));
            }
        } elseif ($ajaxCall) {
            $this->success(__('Другая задача находится в очереди'));
        }

    }


    public function updatePriceDataStartAction()
    {
        $queue = DB::select()->from('scheduler')->where('in_queue', '=', 1)->find();
        if (!$queue) {
            $select = DB::select()->from('scheduler')->where('action', '=', 'updatePriceDataAction')->find();
            DB::update('scheduler')->where('action', '=', 'updatePriceDataAction')->set(['status' => Job::STATUS_INIT, 'start_time' => time(), 'start_time_back' => $select->start_time])->execute();
            $this->success(__('Узел поставлен на выгружение!'));
        } else {
            $this->success(__('В очереди присутствует задача!'));
        }

    }

    public function updateBalanceStartAction()
    {
        $queue = DB::select()->from('scheduler')->where('in_queue', '=', 1)->find();
        if (!$queue) {
            $select = DB::select()->from('scheduler')->where('action', '=', 'updateBalanceAction')->find();
            DB::update('scheduler')->where('action', '=', 'updateBalanceAction')->set(['status' => Job::STATUS_INIT, 'start_time' => time(), 'start_time_back' => $select->start_time])->execute();
            $this->success(__('Узел поставлен на выгружение!'));
        } else {
            $this->success(__('В очереди присутствует задача!'));
        }

    }

    public function updateCurrenciesStartAction()
    {
        $queue = DB::select()->from('scheduler')->where('in_queue', '=', 1)->find();
        if (!$queue) {
            $select = DB::select()->from('scheduler')->where('action', '=', 'updateCurrenciesAction')->find();
            DB::update('scheduler')->where('action', '=', 'updateCurrenciesAction')->set(['status' => Job::STATUS_INIT, 'start_time' => time(), 'start_time_back' => $select->start_time])->execute();
            $this->success(__('Узел поставлен на выгружение!'));
        } else {
            $this->success(__('В очереди присутствует задача!'));
        }

    }

    public function uploadImagesFromFolderStartAction()
    {
        $queue = DB::select()->from('scheduler')->where('in_queue', '=', 1)->find();
        if (!$queue) {
            $select = DB::select()->from('scheduler')->where('action', '=', 'updateInformationReferencesAction')->find();
            DB::update('scheduler')->where('action', '=', 'uploadImagesFromFolderAction')->set(['status' => Job::STATUS_INIT, 'start_time' => time(), 'start_time_back' => $select->start_time])->execute();
            $this->success(__('Узел поставлен на выгружение!'));
        } else {
            $this->success(__('В очереди присутствует задача!'));
        }

    }

    /**
     * @param $specificationValues
     * @param $specificationName
     * @param $table
     * @return bool
     * @throws Database_Exception
     * @throws Throwable
     */
    protected function modifySpecificationsTables($specificationValues, $specificationName, $table): bool
    {
        $specification = Specifications::getRowSimple(Text::translit($specificationName, 'only_chars_and_numbers'), 'alias');
        if (empty($specification)) {
            $insertedID = Specifications::insert(['created_at' => time(), 'status' => 1, 'name' => $specificationName, 'alias' => Text::translit($specificationName, 'only_chars_and_numbers'), 'type_id' => 2]);
            $specification = Specifications::getRowSimple($insertedID, 'id');
        }
        if (!isset($specificationValues['specification_id']) && !empty($specification)) {
            $specificationValues['specification_id'] = $specification->id;
        } else {
            $this->logger->error(__('Отсутствует в базе идентификатор для характеристики :name', [':name' => $specificationName]), ['method' => __FUNCTION__]);
            return false;
        }
        if (!isset($specificationValues['status'])) {
            $specificationValues['status'] = 1;
        }
        $searchableSpecificationValue = SpecificationsValues::getRowsBySpecificationsIDAndFieldId($specification->id, $specificationValues['book_keeping_id'], 'book_keeping_id');
        if ($searchableSpecificationValue) {
            SpecificationsValues::update($specificationValues, $searchableSpecificationValue->id);
            //$this->updateTable($table,$searchableSpecificationValue->id,$specificationValues);
        } else {
            if (!isset($specificationValues['alias']) && isset($specificationValues['ru']['name']) && Common::checkField($table, 'alias')) {
                $alias = Text::translit(mb_strtolower($specificationValues['ru']['name']));
                $duplicate = CommonI18n::factory($table)->getRowSimple($alias, 'alias');
                if ($duplicate) {
                    $random = Text::random('alnum', 4);
                    $alias .= $random;
                }
                $specificationValues['alias'] = $alias;
            }
            SpecificationsValues::insert($specificationValues);
            //$this->insertToTable($table,$specificationValues);
        }
        return true;
    }

    /**
     * Загрузчик изображений
     * @uploadImagesFromFolderAction
     * @param bool $ajaxCall
     * @throws Exception
     */
    public function uploadImagesFromFolderAction($ajaxCall = true): void
    {
        if (!$ajaxCall) {
            $this->initCron();
        }
        if (!$this->queue) {
            $this->updateQueue(__FUNCTION__, 1);
        }
        $functionQueue = $this->chekQueue(__FUNCTION__);
        if ($functionQueue) {
            DB::update('scheduler')->where('action', '=', __FUNCTION__)->set(['status' => Job::STATUS_IN_PROGRESS])->execute();
            $limit = 10;
            $amount = 0;
            $directory = $this->photoPath;
            $ftp = HelperFtp::getRow(__FUNCTION__, 'action', 1);
            if ($ftp) {
                $connection = HelperFtp::connect($ftp, 'photo');
                $amount = count(ftp_nlist($connection, "."));
                if (!$connection) {
                    $this->logger->error(__('Не удалось установить соединение с :ftp_server', [':ftp_server' => $ftp->ftp_address]));
                    $this->updateQueue(__FUNCTION__, 0);
                    $this->clearAdditional(__FUNCTION__);
                    $this->error(__('Не удалось установить соединение с :ftp_server', [':ftp_server' => $ftp->ftp_address]));
                }
            }
            if (!is_dir($directory)) {
                $this->updateQueue(__FUNCTION__, 0);
                $this->clearAdditional(__FUNCTION__);
                $this->logger->error(__('Директория :name отсутствует!', [':name' => $directory]));
                $this->error(__('Директория :name отсутствует!', [':name' => $directory]));
            }
            $offset = $this->chekAdditional(__FUNCTION__, 'offset') ?: 0;

            $scanned_directory = array_diff(scandir($directory), array('..', '.', '.gitkeep'));
            if (count($scanned_directory) > $limit and $offset > 0) {
                $scanned_directory = array_slice($scanned_directory, $offset, $limit);
            } elseif ($offset > 0 and count($scanned_directory) < $limit) {
                $scanned_directory = array_slice($scanned_directory, $offset);
            } elseif ($offset == 0 and count($scanned_directory) > $limit) {
                $scanned_directory = array_slice($scanned_directory, $offset, $limit);
            }
            if (count($scanned_directory) == $limit) {
                $this->uploadImagesFromFolder($scanned_directory, $directory);
                $this->success('Изображения загружаются');
            } elseif (count($scanned_directory) > 0 and count($scanned_directory) < 10) {
                $this->uploadImagesFromFolder($scanned_directory, $directory);
                $this->clearAdditional('uploadImagesFromFolderAction');
                $this->saveJobAction(__FUNCTION__);
                //DB::update('scheduler')->where('action','=','uploadImagesFromFolderAction')->set(['status' => Job::STATUS_FINISHED,'in_queue' => 0])->execute();
                $this->success(__('Изображения загружены'));
            } else {
                $this->clearAdditional('uploadImagesFromFolderAction');
                $this->saveJobAction(__FUNCTION__);
                //DB::update('scheduler')->where('action','=','uploadImagesFromFolderAction')->set(['status' => Job::STATUS_FINISHED,'in_queue' => 0])->execute();
                $this->success(__('Изображения загружены'));
            }
            //
            /*$loaded = $this->chekAdditional(__FUNCTION__,'loaded') ?: 0;
            if(!$loaded){
                $this->updateAdditional(__FUNCTION__,['loaded' => $amount < $limit ? $amount : $limit]);
                $this->success('Изображения загружаются');
            }elseif($loaded < $amount){
                $this->updateAdditional(__FUNCTION__,['loaded' => $loaded+$limit]);
                $this->success('Изображения загружаются');
            }else{
                $this->updateQueue(__FUNCTION__,0);
                $this->clearAdditional(__FUNCTION__);
                $this->saveJobAction(__FUNCTION__);
                if($ajaxCall){

                    $this->success(__('Изображения загружены'));
                }
            }*/
        } elseif ($ajaxCall) {
            $this->success(__('Другая задача находится в очереди'));
        }
    }

    /**
     * @param $scanned_directory
     * @param $directory
     * @throws Exception
     */
    protected function uploadImagesFromFolder($scanned_directory, $directory): void
    {
        $delete_folder_arr = [];
        foreach ($scanned_directory as $key => $productDirectory) {
            /*if (ImagesLog::logExistence($productDirectory)){
                continue;
            }*/
            $logID = ImagesLog::createLog($productDirectory);
            $this->removeImages($productDirectory);
            $productDirectory_upd = array_diff(scandir($directory . $productDirectory), array('..', '.', '.DS_Store'));
            foreach ($productDirectory_upd as $filename) {
                $withoutExt = preg_replace('/\\.[^.\\s]{3,4}$/', '', $filename);
                $photo = explode('~', $withoutExt);
                $item = DB::select()->from('catalog')->where('id', '=', $photo[0])->find();
                if ($item) {
                    $this->uploadImages($photo, $filename, $logID);
                    $delete_folder_arr [$key] = $productDirectory;
                }
            }
        }
        $offset = $this->chekAdditional('uploadImagesFromFolderAction', 'offset') ?: 0;

        $offset = $offset + count($scanned_directory) - count($delete_folder_arr);
        $this->updateAdditional('uploadImagesFromFolderAction', ['offset' => $offset]);
        foreach ($delete_folder_arr as $dir) {
            if ($dir !== '.DS_Store') {
                Files::deleteDir($directory . $dir);
            }
        }
    }

    /**
     * @param $action
     */
    private function setJobErrorAction($action): void
    {
        $existenceAction = DB::select()->from('scheduler')->where('action', '=', $action)->execute()->current();
        if ($existenceAction) {
            DB::update('scheduler')->where('action', '=', $action)->set(['status' => Job::STATUS_FINISHED])->execute();
        }
    }

    /**
     * @param $action
     */
    private function saveJobAction($action): void
    {
        $existenceAction = DB::select()->from('scheduler')->where('action', '=', $action)->execute()->current();
        if ($existenceAction) {
            if (empty($existenceAction['finish_time'])) {
                $start_date = Dates::getJobTimestampFromUnixTime($existenceAction);
                $time = $existenceAction['start_time_back'] ? strtotime($start_date, $existenceAction['start_time_back']) : strtotime($start_date, $existenceAction['start_time']);
                DB::update('scheduler')->where('action', '=', $action)->set(['start_time' => $time, 'start_time_back' => null, 'status' => Job::STATUS_INIT, 'in_queue' => 0, 'updated_at' => time()])->execute();
            } else {
                $start_date = Dates::getJobTimestampFromUnixTime($existenceAction);
                $time = $existenceAction['start_time_back'] ? strtotime($start_date, $existenceAction['start_time_back']) : strtotime($start_date, $existenceAction['start_time']);
                DB::update('scheduler')->where('action', '=', $action)->set(['start_time' => $time, 'start_time_back' => null, 'status' => Job::STATUS_FINISHED, 'in_queue' => 0, 'updated_at' => time()])->execute();
            }
            return;
        }
        $date = new DateTimeZone('Europe/Kiev');
        $data = [];
        $data['created_at'] = time();
        $data['status'] = Job::STATUS_FINISHED;
        $data['start_time'] = time();
        $data['finish_time'] = time();
        $data['count'] = 0;
        $data['action'] = $action;
        $data['interval'] = 0;
        $data['interval_type'] = 2;
        $data['timezone'] = $date->getName();
        $data['updated_at'] = time();
        DB::insert('scheduler', array_keys($data))->values([$data['created_at'], $data['status'], $data['start_time'], $data['finish_time'], $data['count'], $data['action'], $data['interval'], $data['interval_type'], $data['timezone'], $data['updated_at']])->execute();
        $this->log->sendReport($action);
    }

    /**
     * @param array $photo
     * @param string $filename
     * @param int $logID
     * @return bool
     * @throws Exception
     */
    protected function uploadImages(array $photo, string $filename, int $logID): bool
    {
        $product_id = explode('-', $photo[0])[0];
        if (!$product_id) {
            $this->logger->error(__('Пустой идентификатор товара в имени фотографии(:file)', [':file' => $filename]));
            return false;
        }
        $image = Files::uploadImageForImport('catalog', $filename, $product_id);
        if (!$image) {
            $this->logger->error(__('Не удалось загрузить :file для позиции :item', [':file' => $filename, ':item' => $product_id]));
            return false;
        }
        $sort = 0;
        if (isset($photo[1])) {
            $sort = $photo[1];
        }
        $main = Images::issetMain($product_id) ? 0 : 1;
        Images::insert([
            'catalog_id' => $product_id,
            'image' => $image,
            'sort' => $sort,
            'main' => $main,
        ]);
        if ($main) {
            Common::factory('catalog')->update(['image' => $image], $product_id);
        }
        ImagesLog::updateLog($logID);
        return true;

    }

    /**
     * @param int $productID
     * @return bool
     */
    protected function removeImages(int $productID): bool
    {
        $images = Images::getRows($productID)->as_array();
        if (empty($images)) {
            return true;
        }
        foreach ($images as $im) {
            Files::deleteImage('catalog', $im->image);
        }
        Images::delete($productID, 'catalog_id');
        return true;
    }

    /**
     * @param null|string $action
     * @return bool|Database_Result_Cached|object
     */
    protected function chekQueue($action = null)
    {
        if ($action !== null) {
            return DB::select('in_queue')->from('scheduler')->where('in_queue', '=', 1)->where('action', '=', $action)->execute()->current()['in_queue'];
        }
        return DB::select('in_queue')->from('scheduler')->where('in_queue', '=', 1)->execute()->current()['in_queue'];
    }

    /**
     * @param string $action
     * @param $paramToCheck
     * @return bool|Database_Result_Cached|object
     */
    protected function chekAdditional(string $action, $paramToCheck)
    {
        $scheduler = DB::select()->from('scheduler')->where('action', '=', $action)->execute()->current();
        if (!empty($scheduler) && !empty($scheduler['additional'])) {
            $params = json_decode($scheduler['additional'], true);
            return $params[$paramToCheck] ?? null;
        }
        return null;
    }

    /**
     * @param string $action
     * @param $queue
     * @return bool|Database_Result_Cached|object
     */
    protected function updateQueue(string $action, $queue): bool
    {
        return DB::update('scheduler')->where('action', '=', $action)->set(['in_queue' => $queue, 'additional' => null])->execute();
    }

    /**
     * @param string $action
     * @return bool|Database_Result_Cached|object
     */
    protected function clearAdditional(string $action): bool
    {
        return DB::update('scheduler')->where('action', '=', $action)->set(['additional' => null])->execute();
    }


    /**
     * @param string $action
     * @param array $data
     * @return bool|Database_Result_Cached|object
     */
    protected function updateAdditional(string $action, $data = []): bool
    {
        if (empty($data)) {
            return false;
        }
        $scheduler = DB::select()->from('scheduler')->where('action', '=', $action)->execute()->current();
        if (!empty($scheduler['additional'])) {
            $params = json_decode($scheduler['additional'], true);
            foreach ($data as $key => $datum) {
                if (isset($params[$key])) {
                    unset($params[$key]);
                }
            }
            $data = array_merge($params, $data);
            return DB::update('scheduler')->where('action', '=', $action)->set(['additional' => json_encode($data)])->execute();
        }

        return DB::update('scheduler')->where('action', '=', $action)->set(['additional' => json_encode($data)])->execute();
    }

    public function updatePromAction($ajaxCall = true)
    {
        if (!$ajaxCall) {
            $this->initCron();
        }

        if (!$this->queue) {
            $this->updateQueue(__FUNCTION__, 1);
        }

        $start = DB::select('start')->from('scheduler')->where('action', '=', __FUNCTION__)->find();
        if(!$start->start){
            $functionQueue = $this->chekQueue(__FUNCTION__);
            if ($functionQueue) {
                DB::update('scheduler')->set(['start' => 1, 'status' => Job::STATUS_IN_PROGRESS])->where('action', '=', __FUNCTION__)->execute();
                $iterate = $this->chekAdditional(__FUNCTION__, 'iterate') ?: 0;
                $limit = 1;

                $templates = Prom::getRowsForCron($limit);
                if(count($templates)){
                    $xmlService = PromService::create();
                    DB::insert('tmp_log', ['func', 'time', 'text'])->values([__FUNCTION__, time(), '@@@'])->execute();
                    $i = 0;
                    $iterate++;
                    $additional = [];
                    $additional['iterate'] = $iterate;
                    if($iterate == 1 ){
                        $additional['count_all'] = Prom::getRowsForCronCount();
                    }
                    foreach ($templates as $template) {
                        DB::insert('tmp_log', ['func', 'time', 'text'])->values([__FUNCTION__, time(), 'start--'.$template->id])->execute();

                        $this->updateNews($template);
                        $xmlService->generateXml($template, 'User/Export/Partials/TableSize', 'User/Export/Partials/Xml', true);
                        $i++;
                        DB::insert('tmp_log', ['func', 'time', 'text'])->values([__FUNCTION__, time(), 'stop--'.$template->id])->execute();
                    }

                    DB::insert('tmp_log', ['func', 'time', 'text'])->values([__FUNCTION__, time(), $i])->execute();


                    $this->updateAdditional(__FUNCTION__, $additional);
                    DB::update('scheduler')->set(['start' => 0])->where('action', '=', __FUNCTION__)->execute();
                    $this->success('Is work!');
                }else{
                    $this->updateQueue(__FUNCTION__, 0);
                    $this->clearAdditional(__FUNCTION__);
                    $this->saveJobAction(__FUNCTION__);
                    DB::update('scheduler')->set(['start' => 0])->where('action', '=', __FUNCTION__)->execute();
                    $this->success('Шаблоны выгрузки обновлены');
                }

            }else{
                $this->success('Другая задача находится в очереди!');
            }
        }

    }

    protected function updateNews($template){
        $template_products = Prom::getProductsProm($template->id);
        $categories_products = Items::getProductByIds(array_keys($template_products));
        $products = Items::getNewProductsByCatIds(array_keys($categories_products), array_values($categories_products));
        $range = json_decode($template->slider);

        $items = [];

        foreach ($products as $value){
            $value = (object)$value;
            foreach ($range as $r) {
                $min_max = explode('-', $r[0]);
                if ($min_max[0] <= $value->price && $min_max[1] >= $value->price) {
                    $items[] = [$value->id, $value->price + ($value->price * ((int)$r[1] / 100)), $value->parent_id];
                }
            }
        }

        if(!empty($items)){
            Prom::insertRelation($template, $items);
        }
    }

}
