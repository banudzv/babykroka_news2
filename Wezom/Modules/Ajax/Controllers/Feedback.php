<?php
namespace Wezom\Modules\Ajax\Controllers;

use Core\Arr;
use Core\HTML;
use Core\TrelloAPI;
use Wezom\Modules\Ajax;

class Feedback extends Ajax {
	private $key = '6c67b6130ff91f58590a6b546ef74f83';
	private $token = '1b79fa4601edfe97d7155886ba75e9bbd64121fe5dfe257415832872211d4707';
	private $support = '5cde9d6aeec856480c1e1362';

	public function sendAction(){
		$site = Arr::get($_SERVER, 'HTTP_HOST');
		$name = Arr::get($_POST,'name');
		if (!$name){
			$this->error('Имя пустое!');
		}
		$phone = Arr::get($_POST,'phone');
		if (!$phone){
			$this->error('Телефон пустой!');
		}
		$phone = preg_replace('/[^+0-9]/', '', $phone);
        $description = Arr::get($_POST,'description');
		$client = new TrelloAPI($this->key,$this->token);
		if (!$client){
			$this->error('Ошибка соединения с сервером обратной связи!');
		}
		$card_info = $name."\n".(int)$phone."\n".$description;
		$client->createCard($this->support,[
			'keepFromSource' => 'all',
			'urlSource' => $site,
			'pos' => 'top',
			'name' => $site,
			'desc' => $card_info
		]);
		$this->success('Менеджер свяжется с вами в ближайшее время');
	}

}
