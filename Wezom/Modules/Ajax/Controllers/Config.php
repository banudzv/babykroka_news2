<?php
    namespace Wezom\Modules\Ajax\Controllers;

    use Core\Arr;
    use Core\Email;
    use Core\HTML;
    use Core\QB\DB;
    use Wezom\Modules\Ajax;
    use Wezom\Modules\Catalog\Models\Ages;
    use Wezom\Modules\Catalog\Models\Specifications;
    use Wezom\Modules\Catalog\Models\SpecificationsValues;
    use Wezom\Modules\Config\Models\HelperFtp;

    class Config extends Ajax {

        public function testEmailAction() {
            $title = trim(Arr::get($this->post, 'title', ''));
            if (!$title) {
                $this->error(__('Заполните заголовок письма'));
            }
            $body = trim(Arr::get($this->post, 'body', ''));
            if (!$body) {
                $this->error(__('Заполните тело письма'));
            }
            $email = trim(Arr::get($this->post, 'email', ''));
            if (!$email or !filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $this->error(__('Неправильный Email'));
            }

            if(Email::send($title, $body, $email)){
                $this->success(__('Письмо успешно отправлено'));
            }
            $this->error(__('Произошла ошибка при отправке письма. Проверьте настройки почты'));
        }

        /**
         *
         */
        public function setPrioritySeasonsAction(): void
        {
            $priority = Arr::get($this->post,'priority');
            $season_start = Arr::get($this->post,'start');
            if($season_start){
                DB::update('config')->set([
                    'zna' => $season_start
                ])->where('key', '=', 'season_start')->where('group', '=', 'seasons')->execute();
            }
            $season_end = Arr::get($this->post,'finish');
            if($season_end){
                DB::update('config')->set([
                    'zna' => $season_end
                ])->where('key', '=', 'season_end')->where('group', '=', 'seasons')->execute();
            }
            if(empty($priority)){
                $this->error(__('Произошла ошибка при сохранении приоритетов. Проверьте введены ли данные для отправки'));
            }
            foreach ($priority as $spec_id => $sort){
                $row = SpecificationsValues::getRow($spec_id);
                if($row){
                    SpecificationsValues::update(['sort' => $sort],$spec_id);
                }
            }
            $this->success(__('Приоритеты успешно сохранены'));

        }

        /**
         * Обновление возрастных групп в настройках сайта
         * @updateAgesAction
         * @template Wezom/Views/Config/Ages.php
         */
        public function updateAgesAction(): void
        {
            $ages = Arr::get($this->post,'ages');
            if(empty($ages)){
                $this->error(__('Произошла ошибка при сохранении возрастных соответствий. Проверьте введены ли данные для отправки'));
            }
            foreach ($ages as $age_id => $code){
                $row = Ages::getRow($age_id);
                if($row){
                    Ages::update(['code' => $code],$age_id);
                }
            }
            $this->success(__('Возрастные соответствия успешно сохранены'));
        }

        /**
         * @testConnectionAction
         * Проверяем соединение()
         * @see Шаблон View/Config/Edit.php - находится скрипт вызова ajax
         * @see Виджет View/Config/Centre.php - сам шаблон в котором проставляются значения FTP соединения
         * @see Config::editAction()
         *
         */
        public function testConnectionAction(): void
        {
            $index = Arr::get($this->post,'selectedVal');
            $scheduler_config = \Core\Config::get('main.scheduler');
            if(!isset($scheduler_config[$index])){
                $this->error(__('Произошла ошибка при подключении. Обратитесь к разработчикам.'));
            }
            $action = $scheduler_config[$index];
            $row = HelperFtp::getRow($action,'action');
            if($row){
                if($row->ftp_port === 22){
                    $this->connectSFTP($row);
                }else{
                    $this->connectFTP($row);
                }
            }else{
                $this->error(__('Произошла ошибка при подключении. Нет настроек к данному узлу.'));
            }

        }

        /**
         * @testConnectionAction
         * Проверяем соединение()
         * @see Шаблон View/Config/Edit.php - находится скрипт вызова ajax
         * @see Виджет View/Config/Centre.php - сам шаблон в котором проставляются значения FTP соединения
         * @see Config::editAction()
         *
         */
        public function closeConnectionAction(): void
        {
            $index = Arr::get($this->post,'selectedVal');
            $scheduler_config = \Core\Config::get('main.scheduler');
            if(!isset($scheduler_config[$index])){
                $this->error(__('Произошла ошибка при отключении. Обратитесь к разработчикам.'));
            }
            $action = $scheduler_config[$index];
            $row = DB::select()->from('scheduler')->where('action','=',$action)->execute()->current();
            if($row){
                if ($row['in_queue'] == 0 && $row['status'] == 3){
                    $this->error(__('Выгрузка уже отключена!'));
                }
                DB::update('scheduler')->where('action','=',$action)->set(['in_queue' => 0,'status' => 3, 'start_time'=>$row['start_time_back'] ? $row['start_time_back'] : $row['start_time'], 'start_time_back'=> null])->execute();
                $this->success(__('Выгрузка успешно была отключена'));
            }else{
                $this->error(__('Произошла ошибка при отключении задачи. Нет настроек к данному узлу.'));
            }

        }

        /**
         * Function @connectSFTP
         * Подключаемся по защищённому протоколу используя функцию ssh2
         * @link https://www.php.net/manual/ru/function.ssh2-sftp.php
         * @param $row
         */
        private function connectSFTP($row): void
        {
            if(!extension_loaded('php-ssh2')){
                $this->error(__('На сервере отсутствует расширение php-ssh2, обратитесь к системному администратору чтобы установить его'));
            }
            $connection = ssh2_connect($row->ftp_address,$row->ftp_port);
            if(!$connection){
                $this->error(__('Не удалось установить соединение с :ftp_server',[':ftp_server' => $row->ftp_address]));
            }
            $auth = ssh2_auth_password($connection,$row->ftp_username,$row->ftp_password);
            if($auth){
                ssh2_disconnect($connection);
                $this->success(__('Успешно произведён вход на :ftp_server под именем :ftp_username',[':ftp_server' => $row->ftp_address,':ftp_username' => $row->ftp_username]));
            }else{
                ssh2_disconnect($connection);
                $this->error(__('Не удалось войти под именем :ftp_username',[':ftp_username' => $row->ftp_username]));
            }

        }

        /**
         * Function @connectFTP
         * Подключаемся по стандартному протоколу
         * @link https://www.php.net/manual/ru/ref.ftp.php
         * @param $row
         */
        private function connectFTP($row): void
        {
            $connection = ftp_connect($row->ftp_address,$row->ftp_port ?:21,30);
            if(!$connection){
                $this->error(__('Не удалось установить соединение с :ftp_server',[':ftp_server' => $row->ftp_address]));
            }
            $auth = ftp_login($connection,$row->ftp_username,$row->ftp_password);
            if($auth){
                ftp_close($connection);
                $this->success(__('Успешно произведён вход на :ftp_server под именем :ftp_username',[':ftp_server' => $row->ftp_address,':ftp_username' => $row->ftp_username]));
            }else{
                ftp_close($connection);
                $this->error(__('Не удалось войти под именем :ftp_username',[':ftp_username' => $row->ftp_username]));
            }
        }

        public function changeFtpConnectionAction(){
            $ftp_connection = trim(Arr::get($this->post,'ftp_connection',1));
            DB::update('config')->set(['zna' => $ftp_connection])->where('key', '=', 'ftp_connection')->where('group', '=', 'individual')->execute();
            $this->success(__('Тип подключения успешно изменён'));
        }

        public function updateFtpSettingsAction(): void
        {
            $index = Arr::get($this->post,'selectedVal');
            $scheduler_config = \Core\Config::get('main.scheduler');
            if(!isset($scheduler_config[$index])){
                $this->error(__('Произошла ошибка при обновлении. Обратитесь к разработчикам.'));
            }
            $action = $scheduler_config[$index];
            $ftp_address = trim(Arr::get($this->post,'ftp_address',''));
            $ftp_username = trim(Arr::get($this->post,'ftp_username',''));
            $ftp_password = trim(Arr::get($this->post,'ftp_password',''));
            $ftp_port = trim(Arr::get($this->post,'ftp_port'));
            $ftp_connection = trim(Arr::get($this->post,'ftp_activeGroup',1));
            $row = HelperFtp::getRow($action,'action');
            if(!$row){
                HelperFtp::insert([
                    'action' => $action,
                    'ftp_address' => $ftp_address,
                    'ftp_password' => $ftp_password,
                    'ftp_username' => $ftp_username,
                    'ftp_port' => $ftp_port ?: 21,
                    'active' => $ftp_connection,
                ]);
            }else{
                HelperFtp::update([
                    'ftp_address' => $ftp_address,
                    'ftp_password' => $ftp_password,
                    'ftp_username' => $ftp_username,
                    'ftp_port' => $ftp_port,
                    'active' => $ftp_connection,
                ],$action,'action');
            }
            DB::update('config')->set(['zna' => $ftp_connection])->where('key', '=', 'ftp_connection')->where('group', '=', 'individual')->execute();
            $this->success(__('Настройки FTP успешно обновлены'));
        }

        public function updatePromInfoAction(){
            $data = [];
            $data['download_url'] = trim(Arr::get($this->post,'download_url',''));
            $data['youtube_link'] = trim(Arr::get($this->post,'youtube_link',''));
            $data['url_file'] = trim(Arr::get($this->post,'url_file',''));
            $data['name_shop'] = trim(Arr::get($this->post,'name_shop',''));
            $data['products'] = trim(Arr::get($this->post,'products', ''));
            $data['products_filter'] = trim(Arr::get($this->post,'products_filter',''));
            $data['products_catalog'] = trim(Arr::get($this->post,'products_catalog',''));
            $data['products_prices'] = trim(Arr::get($this->post,'products_prices',''));
            $data['button_save'] = trim(Arr::get($this->post,'button_save',''));

            $keys = [];
            $values = [];
            foreach($data as $key => $value){
                $keys[] = $key;
                $values[] = $value;
            }


            DB::delete('prom_info')->execute();
            DB::insert('prom_info', $keys)->values($values)->execute();

            $this->success(__('Информация успешно обновлена'));
        }
    }
