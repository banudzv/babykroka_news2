<?php
namespace Wezom\Modules\Ajax\Controllers;

use Core\Common;
use Core\Dates;
use Core\HTML;
use Core\QB\Database;
use Core\QB\Database_Exception;
use Core\QB\Database_Result_Cached;
use Core\QB\DB;
use DateTime;
use DateTimeZone;
use Scheduler\Job\RRule;
use Scheduler\JobRunner\JobRunner;
use Scheduler\Scheduler;
use Wezom\Modules\Ajax;

class Job extends Ajax {

    public const STATUS_INIT = 0;
    public const STATUS_IN_PROGRESS = 1;
    public const STATUS_FINISHED = 2;
    public const STATUS_FAILED = 3;
    public const STATUS_REPEAT = 4;
    public $_timezone;
    public $_jobRunner;
    public $_connection;
    public $_actionInspector;
    public $_scheduler;
    public $_max_iteration;
    public $_interval_types;
    public $queue;
    public $connection;
    public $logger;
    public $log;

    public function before()
    {
        parent::before();
        $this->initWorker();
    }

    private function initWorker(){
        $this->_timezone = new DateTimeZone('Europe/Kiev');
        $this->_jobRunner = new JobRunner();
        $this->_scheduler = new Scheduler();
        $this->_max_iteration = 2;
        $this->_interval_types = \Core\Config::get('interval.types');
        $this->queue = $this->chekQueue();
        $this->connection = Common::factory('config')->getRow('ftp_connection','key')->zna;
        $logger = new Ajax\Helpers\JobLogger(HOST.DS.'logs/job.log','Job');
        $log = $logger::factory($logger->channelName());
        $stream = $logger->setFormatter("Y-m-d H:i:s","[%datetime%] %channel%.%level_name%: %message% %context% %extra%\n");
        $log->pushHandler($stream);
        $this->logger = $log;
        $this->log = $logger;
    }

    /**
     * @param null|string $action
     * @return bool|Database_Result_Cached|object
     */
    protected function chekQueue($action = null)
    {
        if($action !== null){
            return DB::select('in_queue')->from('scheduler')->where('in_queue','=',1)->where('action','=',$action)->execute()->current()['in_queue'];
        }
        return DB::select('in_queue')->from('scheduler')->where('in_queue','=',1)->execute()->current()['in_queue'];
    }

    /**
     * @workerAction
     */
    public function workerAction()
    {
        if(!$this->_jobRunner){
            $this->initWorker();
        }
        $currentTime = new DateTime('now',$this->_timezone);
        $currentTime =  $currentTime->getTimestamp();
        $dateTime = new DateTime();
        $jobs = DB::select()->from('scheduler')
//                ->where('start_time','<=',$currentTime)
            ->where('in_queue','=',1)
            ->order_by('start_time','DESC')->execute()->current();
        if(!$jobs){
            $jobs = DB::select()->from('scheduler')
                ->where('start_time','<=',$currentTime)
                ->where('status','<>',2)
                ->order_by('start_time','DESC')->execute()->current();
        }
        if(!$jobs){
            $jobs = DB::select()->from('scheduler')
                ->where('start_time','<=',$currentTime)
                ->where('finish_time','>=',$currentTime)
                ->where('status','=',2)
                ->order_by('start_time','DESC')->execute()->current();
        }
        if(!empty($jobs)){
            if($_SESSION['started']){
                return \Core\Config::forbidden();
            }
            $_SESSION['started'] = 1;
            if($jobs['status'] === self::STATUS_FAILED){
                $this->error('Задача была запущена с ошибкой');
            }
            if($jobs['status'] === self::STATUS_IN_PROGRESS && $jobs['count'] == 0){
                if(!isset($_SESSION['iterated'] )){
                    $_SESSION['iterated'] = 1;
                }else{
                    if($_SESSION['iterated'] > $this->_max_iteration){
                        DB::update('scheduler')->set(['status' => self::STATUS_FAILED])->where('id','=',$jobs['id'])->execute();
                        unset($_SESSION['iterated']);
                    }
                    $_SESSION['iterated'] =+1;
                }
            }

            $startTime = $dateTime->setTimestamp($jobs['start_time'])->format('Y-m-d H:i:s');
            $finishTime = $jobs['finish_time'] ? $dateTime->setTimestamp($jobs['finish_time'])->format('Y-m-d H:i:s')  : NULL;
            $startTime = new \DateTime($startTime,new DateTimeZone($jobs['timezone']));
            $finishTime = $finishTime ? new \DateTime($finishTime,new DateTimeZone($jobs['timezone'])): NULL;
            $rule          = new RRule('FREQ=MONTHLY;COUNT=1', $startTime);

            $job           = new \Scheduler\Job\Job($rule, static function () use ($jobs) {
                $cron = new Cron();
                $cron->{$jobs['action']}(false);
            });

            $this->_scheduler->addJob($job);
            DB::update('scheduler')->set(['status' => self::STATUS_IN_PROGRESS])->where('id','=',$jobs['id'])->execute();
            $reports   = $this->_jobRunner->run($this->_scheduler, $startTime, $finishTime);
            if($reports && $reports[0]->getType() === 'success'){
                $this->updateJobDate($jobs,$currentTime);
                if(isset($_SESSION['iterated'])){
                    unset($_SESSION['iterated']);
                }
            }else{
                if($jobs['action'] != 'updatePromAction'){
                    //$this->logger->error($reports);
                    DB::update('scheduler')->set(['count' => 0])->where('id','=',$jobs['id'])->execute();
                    $this->setStatusWorker(self::STATUS_FINISHED,$jobs['id'], true);
                }
            }
            $_SESSION['started'] = 0;
        }else{
            if(isset($_SESSION['started'])){
                unset($_SESSION['started']);
            }
            $this->error('В очереди нет задач');
        }
        die();
    }

    /**
     * @param $job
     * @param $currentTime
     */
    private function updateJobDate($job, $currentTime): void
    {
        $start_time = $this->getStartTime($job);
        if(empty($job['finish_time'])){
            DB::update('scheduler')->set(['start_time' =>  $start_time,'status' => self::STATUS_IN_PROGRESS])->where('id','=',$job['id'])->execute();
        }
        if($job['finish_time'] <= $currentTime){
            DB::update('scheduler')->set(['start_time' => $start_time,'status' => self::STATUS_IN_PROGRESS])->where('id','=',$job['id'])->execute();
        }else{
            if($job['action'] != 'updatePromAction') {
                DB::update('scheduler')->set(['count' => 0])->where('id','=',$job['id'])->execute();
                $this->setStatusWorker(self::STATUS_FINISHED,$job['id']);
            }
        }
        $this->decreaseCount($job);

    }

    protected function setStatusWorker($status,$id, $in_queue = false): void
    {
        if ($in_queue){
            DB::update('scheduler')->set(['status' => $status,'updated_at' => time(),'in_queue'=> 0])->where('id','=',$id)->execute();
        }else{
            DB::update('scheduler')->set(['status' => $status,'updated_at' => time()])->where('id','=',$id)->execute();
        }
    }

    protected function getStartTime($job){
        $interval = Dates::getJobTimestampFromUnixTime($job);
        return strtotime($interval, $job['start_time']);
    }

    /**
     * @param $job
     */
    protected function decreaseCount($job): void
    {
        if($job['count'] !== 0){
            $count = ($job['count'] - 1) < 0 ? 0 :($job['count'] - 1);
            DB::update('scheduler')->set(['count' => $count])->where('id','=',$job['id'])->execute();
            if($count == 0){
                if($job['action'] != 'updatePromAction') {
                    $this->setStatusWorker(self::STATUS_FINISHED,$job['id']);
                }
            }else{
                $this->setStatusWorker(self::STATUS_IN_PROGRESS,$job['id']);
            }
        }else{
            $this->setStatusWorker(self::STATUS_REPEAT,$job['id']);
        }
    }

    /**
     * @throws Database_Exception
     */
    public function saveJobAction(): void
    {
        $postData = $this->getPostData();
        $existenceAction = DB::select()->from('scheduler')->where('action','=',$postData['action'])->execute()->current();
        if($existenceAction){
            $this->updateJobAction();
        }
        $date = new DateTimeZone('Europe/Kiev');
        $postData['timezone'] = $date->getName();
        $postData['updated_at'] = time();
        DB::insert('scheduler',array_keys($postData))->values([$postData['created_at'],$postData['status'],$postData['start_time'],$postData['finish_time'],$postData['action'],$postData['count'],$postData['interval'],$postData['interval_type'],$postData['timezone'],$postData['updated_at']])->execute();
        $this->success('Выгрузка успешно добавлена в очередь');
    }

    /**
     * @throws Database_Exception
     */
    private function updateJobAction(): void
    {
        $postData = $this->getPostData();
        if ($postData['finish_time'] != '' and $postData['start_time'] != ''){
            $db = Database::instance();
            try{
                DB::update('scheduler')->where('action','=',$postData['action'])->set($postData)->execute();
                $db->commit();
                $this->success('Выгрузка успешно обновлена');
            }
            catch(\Exception $e)
            {
                $db->rollback();
                throw new Database_Exception(':error', [
                    ':error' => $e->getMessage()
                ], $e->getCode());
            } catch (\Throwable $e) {
                $db->rollback();
                throw new Database_Exception(':error', [
                    ':error' => $e->getMessage()
                ], $e->getCode());
            }
        }else{
            $this->error('Заполните все обязательные поля!');

        }

    }

    private function getPostData(): array
    {
        $action = str_replace('Start','', $this->post['action']);
        if(!$action){
            $this->error('Выберите узел выгрузки!');
        }
        $currentTime = new DateTime('now',$this->_timezone);
        $start_time =  $currentTime->getTimestamp();
        if($this->post['start_time']){
            $start_time =  new DateTime($this->post['start_time'],$this->_timezone);
            $start_time = $start_time->getTimestamp();
        }
        $finish_time = NULL;
        if( $this->post['finish_time']){
            $finish_time =  new DateTime($this->post['finish_time'],$this->_timezone);
            $finish_time = $finish_time->getTimestamp();
        }
        $repeat = (int)$this->post['count'];
        $interval = (int)$this->post['interval'];
        $interval_type = (int)$this->post['interval_type'];
        if(!isset($this->_interval_types[$interval_type])){
            $this->error('Такого интервала не существует!');
        }
        $data = [];
        $from      = $start_time;
        $to        = $finish_time;
        $data['created_at'] = time();
        $data['status'] = self::STATUS_INIT;
        $data['start_time'] = $from;
        $data['finish_time'] = $to;
        $data['action'] = $action;
        $data['count'] = $repeat;
        $data['interval'] = $interval;
        $data['interval_type'] = $interval_type;
        $data['timezone'] = $this->_timezone->getName();
        return $data;
    }
}
