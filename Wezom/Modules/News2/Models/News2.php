<?php
    namespace Wezom\Modules\News2\Models;

    use Core\Arr;
    use Core\HTML;
    use Core\Message;
    use Core\QB\DB;

    class News2 extends \Core\CommonI18n {

        public static $table = 'news2';
        public static $image = 'news2';
        public static $filters = [
            'name' => [
                'table' => NULL,
                'action' => 'LIKE',
            ],
        ];
        public static $rules = [];
    }