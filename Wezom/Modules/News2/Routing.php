<?php

return [
    'wezom/news2/index' => 'news2/news2/index',
    'wezom/news2/add' => 'news2/news2/add',
    'wezom/news2/edit/<id>' => 'news2/news2/edit',
    'wezom/news2/delete_image/<id:[0-9]*>' => 'news2/news2/deleteImage',
    'wezom/news2/delete/<id:[0-9]*>' => 'news2/news2/delete',
];


