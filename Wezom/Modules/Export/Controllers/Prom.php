<?php

namespace Wezom\Modules\Export\Controllers;

use Core\Config;
use Core\Route;
use Core\Widgets;
use Core\Message;
use Core\Arr;
use Core\HTTP;
use Core\View;
use Core\Pager\Pager;

use Modules\Catalog\Models\Groups;
use Wezom\Modules\Catalog\Models\Items;
use Wezom\Modules\Export\Models\Prom as Model;

class Prom extends \Wezom\Modules\Base
{

    public $tpl_folder = 'Export/Prom';
    public $page;
    public $limit;
    public $offset;

    function before()
    {
        parent::before();
        $this->_seo['h1'] = __('Експорт Prom');
        $this->_seo['title'] = __('Експорт Prom');
        $this->setBreadcrumbs(__('Експорт Prom'), 'wezom/' . Route::controller() . '/index');
        $this->page = (int)Route::param('page') ? (int)Route::param('page') : 1;
        $this->limit = (int)Arr::get($_GET, 'limit', Config::get('basic.limit_backend')) < 1 ?: Arr::get($_GET, 'limit', Config::get('basic.limit_backend'));
        $this->offset = ($this->page - 1) * $this->limit;
    }

    function indexAction()
    {
        $status = NULL;
        if (isset($_GET['status']) && $_GET['status'] != '') {
            $status = Arr::get($_GET, 'status', 1);
        }

        $count = Model::countRows($status);
        $result = Model::getRows($status, null, null, $this->limit, $this->offset);
        $pager = Pager::factory($this->page, $count, $this->limit)->create();
        $this->_toolbar = Widgets::get('Toolbar_List', ['add' => 0, 'delete' => 0]);
        $this->_content = View::tpl(
            [
                'result' => $result,
                'tpl_folder' => $this->tpl_folder,
                'tablename' => Model::$table,
                'count' => $count,
                'pager' => $pager,
                'pageName' => $this->_seo['h1'],
            ], $this->tpl_folder . '/Index');
    }


    function addAction()
    {
        if ($_POST) {
            $post = $_POST['FORM'];
            $post['status'] = Arr::get($_POST, 'status', 0);
            if (Model::valid($post)) {
                $res = Model::insert($post);
                if ($res) {
                    Message::GetMessage(1, __('Вы успешно добавили данные!'));
                    $this->redirectAfterSave($res);
                } else {
                    Message::GetMessage(0, __('Не удалось добавить данные!'));
                }
            }
            $result = Arr::to_object($post);
        } else {
            $result = [];
        }
        $this->_toolbar = Widgets::get('Toolbar_Edit');
        $this->_seo['h1'] = __('Добавление');
        $this->_seo['title'] = __('Добавление');
        $this->setBreadcrumbs(__('Добавление'), 'wezom/' . Route::controller() . '/add');
        $this->_content = View::tpl(
            [
                'obj' => $result,
                'tpl_folder' => $this->tpl_folder,
            ], $this->tpl_folder . '/Form');
    }

    function editAction()
    {
        if ($_POST) {
            $post = $_POST['FORM'];
            $post['status'] = Arr::get($_POST, 'status', 0);
            if (Model::valid($post)) {
                $res = Model::update($post, Route::param('id'));
                if ($res) {
                    Message::GetMessage(1, __('Вы успешно изменили данные!'));
                } else {
                    Message::GetMessage(0, __('Не удалось изменить данные!'));
                }
                $this->redirectAfterSave(Route::param('id'));
            }
            $result = Arr::to_object($post);
        } else {
            $page = Model::getRow(Route::param('id'));
            $products_ids = Model::getProductsProm(Route::param('id'));
            if ($products_ids) {
                $result = Groups::getRows(1, 'id', 'ASC')->as_array();
                $arr = [];
                foreach ($result as $obj) {
                    $child = (int)\Wezom\Modules\Catalog\Models\Groups::getCountChildRows($obj->id);

                    if ($child) {
                        Groups::$innerIds = [];
                        $ids = Groups::getInnerIds($obj->id);
                        $tmp = \Modules\Catalog\Models\Items::getProductsIdByGroups($ids);

                        $count = array_intersect_key($products_ids, $tmp->find_all()->as_array('id'));

                    } else {
                        $prod = \Wezom\Modules\Catalog\Models\Items::getProductByCatId($obj->id);
                        $count = array_intersect_key($products_ids, $prod);
                    }

                    $item = (array)$obj;

                    if ($count) {
                        $item['products'] = (bool)$count;
                        $arr[$obj->parent_id][] = $item;
                    }

                }
            }
        }

        $this->_toolbar = Widgets::get('Toolbar_Edit');
        $this->_seo['h1'] = __('Редактирование');
        $this->_seo['title'] = __('Редактирование');
        $this->setBreadcrumbs(__('Редактирование'), 'wezom/' . Route::controller() . '/edit/' . Route::param('id'));
        $this->_content = View::tpl(
            [
                'edit' => true,
                'categories' => $arr,
                'checked' => $products_ids,
                'obj' => $page,
                'tpl_folder' => $this->tpl_folder,
            ], $this->tpl_folder . '/Form');
    }

    function deleteAction()
    {
        $id = (int)Route::param('id');
        $page = Model::getRow($id);
        if (!$page) {
            Message::GetMessage(0, __('Данные не существуют!'));
            HTTP::redirect('wezom/' . Route::controller() . '/index');
        }
        Model::delete($id);
        Message::GetMessage(1, __('Данные удалены!'));
        HTTP::redirect('wezom/' . Route::controller() . '/index');
    }
}
