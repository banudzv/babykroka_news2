<?php

    return [

        'wezom/prom/index' => 'export/prom/index',
        'wezom/prom/index/page/<page:[0-9]*>' => 'export/prom/index',
        'wezom/prom/edit/<id:[0-9]*>' => 'export/prom/edit',
        'wezom/prom/delete/<id:[0-9]*>' => 'export/prom/delete',
        'wezom/prom/add' => 'export/prom/add',
    ];
