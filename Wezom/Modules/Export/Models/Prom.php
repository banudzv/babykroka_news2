<?php

namespace Wezom\Modules\Export\Models;

use Core\QB\Database;
use Core\QB\DB;

class Prom extends \Core\Common
{

    public static $table = 'prom';
    public static $tableRelation = 'prom_products';
    public static $filters = [
        'name' => [
            'table' => 'faq',
            'action' => 'LIKE',
        ],
        'row_id' => [
            'table' => 'faq',
            'action' => '=',
        ]
    ];

    public static function getRowsForCron($limit = 10){
        $date = new \DateTime(date('m.d.y',time()));
        $time = $date->modify('-6 hours')->getTimestamp();

        return DB::select('id', 'shop_name', 'company_name', 'user_id', 'file_link', 'slider')
            ->from(static::$table)
            ->where('updated_at', '<=', $time)
            ->limit($limit)
            ->find_all('prom');
    }
    public static function getRowsForCronCount(){
        $date = new \DateTime(date('m.d.y',time()));
        $time = $date->modify('-6 hours')->getTimestamp();

        return count(DB::select('id', 'shop_name', 'company_name', 'user_id', 'file_link', 'slider')
            ->from(static::$table)
            ->where('updated_at', '<=', $time)
            //->offset($offset)
            ->find_all('prom')->as_array());
    }


    public static function getRows($status = null, $sort = null, $type = null, $limit = null, $offset = null, $filter = true)
    {
        $result = DB::select(static::$table.'.*',
                ['users.id', 'uid'], 'users.name', 'users.last_name', 'users.last_login', 'users.customer_id')
            ->from(static::$table)
            ->join('users')->on(static::$table.'.user_id', '=', 'users.id');
        if ($status !== null) {
            $result->where('status', '=', $status);
        }
        if ($filter) {
            $result = static::setFilter($result);
        }
        if ($sort !== null) {
            if ($type !== null) {
                $result->order_by($sort, $type);
            } else {
                $result->order_by($sort);
            }
        }
        $result->order_by(static::$table.'.id', 'DESC');
        if ($limit !== null) {
            $result->limit($limit);
            if ($offset !== null) {
                $result->offset($offset);
            }
        }
        return $result->find_all();
    }

    public static function getProductsProm($id){
        return DB::select('product_id', 'cost_client')
            ->from(static::$tableRelation)
            ->where('prom_id', '=', $id)
            ->find_all('prom')->as_array('product_id');
    }

    public static function insertRelation($prom_id, $products){
        $values = [];
        foreach($products as $product){
            $cost = $product[1] ? $product[1] : 0;
            $values[]= '('.$prom_id->id.', '.$product[0].', '.$product[2].', '.$cost.')';
        }
        $values = implode(", ", $values);
        $sql = "INSERT INTO prom_products (prom_id, product_id, category_product_id, cost_client) VALUES ". $values;
        DB::query(DATABASE::INSERT, $sql)->execute();
    }
}
