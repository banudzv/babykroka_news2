<?php

namespace Wezom\Modules\Export\Service;

use Core\Config;
use Core\QB\DB;
use Core\View;
use Modules\Catalog\Models\Prices;
use Wezom\Modules\Catalog\Models\Items;
use Wezom\Modules\Export\Models\Prom;
use Wezom\Modules\User\Models\Users;

class PromService
{

    public static function create()
    {
        return new self();
    }

    public function generateXml($template, $tpl, $main_tpl, $cron = false){
        $type_sizes = Config::get('sizes');
        $prom_info = DB::select('download_url')->from('prom_info')->find('prom');
        $ftp = DB::select('*')->from('config_centre')->where('action', '=', 'updatePromAction')->find('prom');

        $template_products = Prom::getProductsProm($template->id);

        $products = \Wezom\Modules\Catalog\Models\Items::getItemsForProm(array_keys($template_products), 'prom');

        $items = [];
        $sizes = [];
        $seasons = Config::get('export_synonyms.prom.seasons');
        $pol = Config::get('export_synonyms.prom.pol');
        $pol_ua = Config::get('export_synonyms.prom.pol_ua');
        foreach ($products as $item) {
            //875e4eb9-364f-11ea-80cc-a4bf01075a5b
            $size_amount = \Modules\Catalog\Models\Items::getProductSizeAmountById($item->id, 'prom')->as_array('code');
            $spec = \Modules\Catalog\Models\Items::getItemSpecificationsByID($item->id, 'prom');
            $color = \Modules\Catalog\Models\Colors::getRowSimple($item->color_alias, 'alias')->name;
            $ua = Items::getItemUaTranslate($item->id, 'prom');
            if($item->sale){
                $prices = Prices::getPricesRowByPriceType($item->id, '875e4eb9-364f-11ea-80cc-a4bf01075a5b', 'prom');
            }

            $sizes = DB::select()->from('helper_characteristics')->find_all('prom')->as_array();
            $labels = [];

            $sa = [];
            $current_size = [];
            foreach($size_amount as $size){
                if($size->catalog_id == $item->id){
                    $current_size = $size;
                }
                $sa[$size->code] = $size;
            }

            foreach ($sizes as $size){
                if(isset($sa[$size->code])){
                    $label = DB::select()->from('helper_characteristics_sizes')
                        ->where('helper_characteristics_sizes.helper_characteristics_id', '=', $size->id)
                        ->where('helper_characteristics_sizes.parameter', '<>', '0')
                        ->find_all('prom')->as_array('mark','parameter');
                    if(!empty($label)){
                        $labels[$size->name] = $label;
                    }
                }
            }

            $data = [];
            $data['id'] = $item->id;
            $data['name'] = $item->name;
            $data['name_ua'] = $ua->name;
            $data['available'] = $item->available;
            $data['portal_category_id'] = $item->portal_category_id;
            $data['price'] = $template_products[$item->id]->cost_client;
            $data['cost_old'] = $item->sale ? (int)$prices->price_old : 0.00;
            $data['size'] = $current_size->size;
            $data['quantity_in_stock'] = $current_size->amount;
            $data['url_foto_prom'] = $item->url_foto_prom;
            $data['brand'] = $item->brand_name;
            $data['description'] = View::tpl(['labels' => $labels, 'description' => $item->content, 'type_sizes' => $type_sizes['ru']], $tpl, $cron);
            $data['description_ua'] = View::tpl(['labels' => $labels, 'description' => $ua->content, 'type_sizes' => $type_sizes['ua']], $tpl, $cron);
            $data['country'] = $item->country;
            $data['season'] = $seasons[$spec['Сезон']];
            $data['pol'] = $pol[$spec['Пол ребенка']];
            $data['pol_ua'] = $pol_ua[$spec['Пол ребенка']];
            $data['type'] = $spec['Материал'];
            $data['color'] = $color;
            $data['portal_category_id_prom'] = $item->portal_category_id_prom;
            $data['view'] = explode(" ", $item->name)[0];
            unset($size_amount[$current_size->code]);

            $sizes_product[$item->id] = $size_amount;


            $items[] = $data;
        }


        $content = View::tpl([
            'template' => $template,
            'sizes' => $sizes_product,
            'products' => $items,
        ], $main_tpl, $cron);

        $user = Users::getRow($template->user_id);

        if($template->file_link){
            $old_filename = explode('/', $template->file_link);
            $filename = $old_filename[5];
        }else{
            $random = random_int(10000, 99999);
            $filename = 'BABYKROHA_'.$user->customer_id.'_'.$random.'.xml';
        }

        $file = fopen(HOST . '/Export/Prom/'.$filename, 'w+');
        fwrite($file, $content);
        fclose($file);

       // sleep(5);
        //Connect to the FTP server
//        $ftpstream = @ftp_connect($ftp->ftp_address);
//        $login = @ftp_login($ftpstream, $ftp->ftp_username, $ftp->ftp_password);
//
//        if($login) {
//
//            $file = fopen(HOST . '/Export/Prom/'.$filename, 'r');
//
//            @ftp_fput($ftpstream, '/'.$filename, $file, FTP_ASCII);
//            ftp_site($ftpstream,"CHMOD 0644 /".$filename);
//            ftp_close($ftpstream);
//
//        }


        DB::update('prom')
            ->set(['file_link' => $prom_info->download_url.$filename,'updated_at' => time()])
            ->where('id', '=', $template->id)->execute();


    }
}
