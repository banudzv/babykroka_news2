<?php
    namespace Wezom\Modules\Subscribe\Models;

    use Core\Arr;
    use Core\Message;

    class Subscribers extends \Core\Common {

        public static $table = 'subscribers';
        public static $filters = [
            'email' => [
                'table' => NULL,
                'action' => 'LIKE',
            ],
        ];
        public static $rules = [];

        public static function valid($data = [])
        {
            static::$rules = [
                'email' => [
                    [
                        'error' => __('Поле "E-Mail" введено некорректно!'),
                        'key' => 'email',
                    ],
                    [
                        'error' => __('Пользователь с таким email уже подписан на новости!'),
                        'key' => 'unique',
                        'value' => static::$table,
                    ],
                ],
            ];
            return parent::valid($data);
        }
    }