<?php
    namespace Wezom\Modules\Menu\Models;

    use Core\Arr;
    use Core\Message;
	use Core\QB\DB;

    class Menu extends \Core\CommonI18n {

        public static $table = 'sitemenu';
        public static $tableI18n = 'sitemenu_i18n';
        public static $rules = [];

        public static function valid($data = [])
        {
            static::$rules = [
                'url' => [
                    [
                        'error' => __('Ссылка не может быть пустой!'),
                        'key' => 'not_empty',
                    ],
                    [
                        'error' => __('Ссылка должна содержать только латинские буквы в нижнем регистре, цифры, "/", "?", "&", "=", "-" или "_"!'),
                        'key' => 'regex',
                        'value' => '/^[a-z0-9\-_\/\?\=\&]*$/',
                    ],
                ],
            ];
            static::$rulesI18n = [
                'name' => [
                    [
                        'error' => __('Название пункта меню не может быть пустым!'),
                        'key' => 'not_empty',
                    ],
                ],
            ];
            return parent::valid($data);
        }

        public static function getRowsByGroup($group, $status = null, $sort = null, $type = null, $limit = null, $offset = null, $filter = true) {
            $lang = \I18n::$lang;
            if (APPLICATION == 'backend') {
                $lang = \I18n::$defaultLangBackend;
            }
            static::$tableI18n = static::$table . '_i18n';
            $result = DB::select(static::$tableI18n . '.*', static::$table . '.*')->from(static::$table)
                ->join(static::$tableI18n, 'LEFT')->on(static::$tableI18n . '.row_id', '=', static::$table . '.id')
                ->where(static::$tableI18n . '.language', '=', $lang)
				->where('group', '=', $group);
			if ($status !== null) {
				$result->where('status', '=', $status);
			}
			if ($filter) {
				$result = static::setFilter($result);
			}
			if ($sort !== null) {
				if ($type !== null) {
					$result->order_by($sort, $type);
				} else {
					$result->order_by($sort);
				}
			}
			$result->order_by(static::$table .'.id', 'DESC');
			if ($limit !== null) {
				$result->limit($limit);
				if ($offset !== null) {
					$result->offset($offset);
				}
			}
			return $result->find_all();
		}

    }
