<?php   
    
    return [
        'wezom/menu/header' => 'menu/menu/header',
		'wezom/menu/sidebar' => 'menu/menu/sidebar',
		'wezom/menu/footer' => 'menu/menu/footer',
        'wezom/menu/index/page/<page:[0-9]*>' => 'menu/menu/index',
        'wezom/menu/edit/<id:[0-9]*>' => 'menu/menu/edit',
        'wezom/menu/delete/<id:[0-9]*>' => 'menu/menu/delete',
        'wezom/menu/add' => 'menu/menu/add',
    ];
