<?php
    namespace Wezom\Modules\Menu\Controllers;

    use Core\Route;
    use Core\Widgets;
    use Core\Message;
    use Core\Arr;
    use Core\HTTP;
    use Core\View;

    use Wezom\Modules\Ajax\Controllers\Config;
    use Wezom\Modules\Menu\Models\Menu AS Model;

    class Menu extends \Wezom\Modules\Base {

        public $tpl_folder = 'Menu';

        function before() {
            parent::before();
            $this->_seo['h1'] = __('Меню сайта');
            $this->_seo['title'] = __('Меню сайта');
        }

        function headerAction()
        {
            $this->setBreadcrumbs(__('Меню в шапке сайта'), 'wezom/'.Route::controller().'/header');
            $_SESSION['group'] = 3;
			$this->_seo['h1'] = __('Меню в шапке сайта');
			$this->_seo['title'] = __('Меню в шапке сайта');
			$result = Model::getRowsByGroup(3,NULL, 'sort', 'ASC');
			$this->_filter = Widgets::get( 'Filter_Pages' );
			$this->_toolbar = Widgets::get( 'Toolbar_List', ['add' => 1, 'delete' => 1]);
			$this->_content = View::tpl(
				[
					'result' => $result,
					'tpl_folder' => $this->tpl_folder,
					'tablename' => Model::$table,
				], $this->tpl_folder.'/Index');

		}

        function sidebarAction ()
        {
            $this->setBreadcrumbs(__('Боковое меню'), 'wezom/'.Route::controller().'/sidebar');
            $_SESSION['group'] = 1;
			$this->_seo['h1'] = __('Боковое меню');
			$this->_seo['title'] = __('Боковое меню');
            $result = Model::getRowsByGroup(1,NULL, 'sort', 'ASC');
            $this->_filter = Widgets::get( 'Filter_Pages' );
            $this->_toolbar = Widgets::get( 'Toolbar_List', ['add' => 1, 'delete' => 1]);
            $this->_content = View::tpl(
                [
                    'result' => $result,
                    'tpl_folder' => $this->tpl_folder,
                    'tablename' => Model::$table,
                ], $this->tpl_folder.'/Index');
        }

        function footerAction ()
        {
            $this->setBreadcrumbs(__('Меню в футере сайта'), 'wezom/'.Route::controller().'/footer');
            $_SESSION['group'] = 2;
            $this->_seo['h1'] = __('Меню в футере');
            $this->_seo['title'] = __('Меню в футере');
            $result = Model::getRowsByGroup(2,NULL, 'sort', 'ASC');
            $this->_filter = Widgets::get( 'Filter_Pages' );
            $this->_toolbar = Widgets::get( 'Toolbar_List', ['add' => 1, 'delete' => 1]);
            $this->_content = View::tpl(
                [
                    'result' => $result,
                    'tpl_folder' => $this->tpl_folder,
                    'tablename' => Model::$table,
                ], $this->tpl_folder.'/Index');
        }


        function editAction ()
        {
            if ($_SESSION['group'] == 0){
                $this->setBreadcrumbs(__('Меню в шапке сайта'), 'wezom/'.Route::controller().'/header');
            } elseif ($_SESSION['group'] == 1){
                $this->setBreadcrumbs(__('Боковое меню'), 'wezom/'.Route::controller().'/sidebar');
            } else {
                $this->setBreadcrumbs(__('Меню в футере сайта'), 'wezom/'.Route::controller().'/footer');
            }
            if ($_POST) {
                $post = $_POST['FORM'];
                $post['status'] = Arr::get( $_POST, 'status', 0 );
                $post['url'] = '/'.trim(Arr::get($post, 'url'), '/');
                if( Model::valid($post) ) {
                    $res = Model::update($post, Route::param('id'));
                    if($res) {
                        Message::GetMessage(1, __('Вы успешно изменили данные!'));
                    } else {
                        Message::GetMessage(0, __('Не удалось изменить данные!'));
                    }
                    $this->redirectAfterSave(Route::param('id'));
                }
                $result = Arr::to_object($post);
                $obj = Arr::get($result, 'obj', []);
                $langs = Arr::get($result, 'langs', []);
            } else {
                $result = Model::getRow(Route::param('id'));
                $obj = Arr::get($result, 'obj', []);
                $langs = Arr::get($result, 'langs', []);
            }
            if ($result->group) {
				$this->_toolbar = Widgets::get( 'Toolbar_Edit', ['list_link' => '/wezom/' . Route::controller() . '/sidebar']);
			} else {
				$this->_toolbar = Widgets::get( 'Toolbar_Edit', ['noAdd'=> 1, 'list_link' => '/wezom/' . Route::controller() . '/header']);
			}
            $this->_toolbar = Widgets::get( 'Toolbar_Edit' );
            $this->_seo['h1'] = __('Редактирование');
            $this->_seo['title'] = __('Редактирование');
            $this->setBreadcrumbs(__('Редактирование'), 'wezom/'.Route::controller().'/edit/'.(int) Route::param('id'));
            $this->_content = View::tpl(
                [
                    'obj' => $obj,
                    'langs' => $langs,
                    'languages' => $this->_languages,
                    'tpl_folder' => $this->tpl_folder,
                ], $this->tpl_folder.'/Form');
        }

        function addAction ()
        {
            if ($_SESSION['group'] == 0){
                $this->setBreadcrumbs(__('Меню в шапке сайта'), 'wezom/'.Route::controller().'/header');
            } elseif ($_SESSION['group'] == 1){
                $this->setBreadcrumbs(__('Боковое меню'), 'wezom/'.Route::controller().'/sidebar');
            } else {
                $this->setBreadcrumbs(__('Меню в футере сайта'), 'wezom/'.Route::controller().'/footer');
            }
            if ($_POST) {
                $group = $_SESSION['group'];
                $post = $_POST['FORM'];
                $post['status'] = Arr::get( $_POST, 'status', 0 );
				$post['group'] = ($group)?:1;
                if( Model::valid($post) ) {
                    $res = Model::insert($post);
                    if($res) {
                        Message::GetMessage(1, __('Вы успешно добавили данные!'));
						$this->redirectAfterSave($res);
                    } else {
                        Message::GetMessage(0, __('Не удалось добавить данные!'));
                    }
                }
                $result = Arr::to_object($post);
                $obj = Arr::get($result, 'obj', []);
                $langs = Arr::get($result, 'langs', []);
            } else {
                $result = [];
                $obj = Arr::get($result, 'obj', []);
                $langs = Arr::get($result, 'langs', []);
            }
            $this->_toolbar = Widgets::get( 'Toolbar/Edit', ['list_link' => '/wezom/' . Route::controller() . '/sidebar'] );
            $this->_seo['h1'] = __('Добавление');
            $this->_seo['title'] = __('Добавление');
            $this->setBreadcrumbs(__('Добавление'), 'wezom/'.Route::controller().'/add');
            $this->_content = View::tpl(
                [
                    'obj' => $obj,
                    'langs' => $langs,
                    'languages' => $this->_languages,
                    'tpl_folder' => $this->tpl_folder,
                ], $this->tpl_folder.'/Form');
        }

        function deleteAction()
        {
            $id = (int) Route::param('id');
            $page = Model::getRowSimple($id);

            if(!$page) {
                Message::GetMessage(0, __('Данные не существуют!'));
                if ($_SESSION['group'] == 1){
                    HTTP::redirect('wezom/'.Route::controller().'/sidebar');
                } elseif ($_SESSION['group'] == 2) {
                    HTTP::redirect('wezom/'.Route::controller().'/footer');
                } elseif ($_SESSION['group'] == 3) {
                    HTTP::redirect('wezom/'.Route::controller().'/header');
                }
            }

            Model::delete($id);
            Message::GetMessage(1, __('Данные удалены!'));

            if ($page->group == 1){
                HTTP::redirect('wezom/'.Route::controller().'/sidebar');
            } elseif ($page->group == 2) {
                HTTP::redirect('wezom/'.Route::controller().'/footer');
            } elseif ($page->group == 3) {
                HTTP::redirect('wezom/'.Route::controller().'/header');
            }
        }

		public function redirectAfterSave($id, $controller=NULL)
        {

			if ($controller===NULL) {
				$controller=Route::controller();
			}
			$result = Model::getRowSimple($id);
			switch (Arr::get($_POST, 'button', 'save')) {
				case 'save-close':
					if ($result->group == 1) {
						HTTP::redirect('wezom/' . $controller . '/sidebar');
					} elseif($result->group == 2){
                        HTTP::redirect('wezom/' . $controller . '/footer');
                    } else {
						HTTP::redirect('wezom/' . $controller . '/header');
					}
					break;
				case 'save-add':
					HTTP::redirect('wezom/' . $controller . '/add');
					break;
				default:
					HTTP::redirect('wezom/' . $controller . '/edit/' . $id);
					break;
			}

		}
    }
