<?php

namespace Wezom\Modules\Seo\Controllers;

use Core\Arr;
use Core\HTTP;
use Core\Message;
use Core\QB\DB;
use Core\Route;
use Core\Support;
use Core\View;
use Core\Widgets;
use Modules\Catalog\Models\Filter;
use Wezom\Modules\Base;
use Wezom\Modules\Catalog\Models\Brands;
use Wezom\Modules\Catalog\Models\Colors;
use Wezom\Modules\Catalog\Models\HelperCharacteristics;
use Wezom\Modules\Catalog\Models\Manufacturers;
use Wezom\Modules\Catalog\Models\Specifications;
use Wezom\Modules\Catalog\Models\SpecificationsValues;
use Wezom\Modules\Seo\Models\FilterPageAttribute;
use Wezom\Modules\Seo\Models\FilterPages;

class FilterPagesController extends Base
{
    public $tpl_folder = 'Seo/FilterPages';
    protected $filters;

    public function before()
    {
        parent::before();

        $this->_seo['h1'] = __('Страницы фильтров');
        $this->_seo['title'] = __('Страницы фильтров');
        $this->setBreadcrumbs(__('Страницы фильтров'), 'wezom/filter-pages');

        $this->filters = [
            ['type' => 'manufacturer', 'name' => 'Производитель'],
            ['type' => 'brand', 'name' => 'Бренд'],
            ['type' => 'color', 'name' => 'Цвет'],
            ['type' => 'size', 'name' => 'Размер']
        ];

        $specifications = Specifications::getRows()->as_array('id', 'name');
        foreach ($specifications as $id => $specification) {
            $this->filters[] = [
                'type' => $id,
                'name' => $specification
            ];
        }
    }

    public function indexAction ()
    {
        $this->_toolbar = Widgets::get( 'Toolbar_List',
            array(
                'addLink' => '/wezom/filter-pages/create',
                'delete' => 1
            )
        );
        $this->_content = View::tpl([], $this->tpl_folder.'/Index');

        $pages = FilterPages::getRows(NULL, 'id', 'DESC');

        $this->_content = View::tpl(
            array(
                'result'        => $pages,
                'tpl_folder'    => $this->tpl_folder,
                'tablename'     => FilterPages::$table,
            ), $this->tpl_folder.'/Index');
    }

    public function createAction()
    {
        $page = [];

        if ($_POST) {
            $post = $_POST['FORM'];
            $post['status'] = Arr::get( $_POST, 'status', 0 );

            if (FilterPages::valid($post)) {
                $pageId = FilterPages::insert($post);

                if ($pageId) {
                    $filters = $_POST['filters'];
                    foreach ($filters as $filter) {
                        FilterPageAttribute::insert([
                            'filter_page_id' => $pageId,
                            'type' => $filter['type'],
                            'value' => $filter['value']
                        ]);
                    }
                    Message::GetMessage(1, __('Вы успешно добавили данные!'));
                    $this->redirectAfterSave($pageId,'filter-pages');
                } else {
                    Message::GetMessage(0, __('Не удалось добавить данные!'));
                }

                $page = Arr::to_object($post);
            }
        }

        $this->_toolbar = Widgets::get( 'Toolbar_Edit', ['list_link' => '/wezom/filter-pages']);
        $this->_seo['h1'] = __('Добавление');
        $this->_seo['title'] = __('Добавление');
        $this->setBreadcrumbs(__('Добавление'), 'wezom/filter-pages/create');

        $categories = Support::getSelectOptions('Catalog/Items/Select', 'catalog_tree', 0);

        $this->_content = View::tpl(
            [
                'obj' => $page,
                'categories' => $categories,
                'filters' => $this->filters,
                'filterValues' => [],
                'tpl_folder' => $this->tpl_folder,
                'languages' => $this->_languages,
            ], $this->tpl_folder.'/Form');
    }

    public function editAction()
    {
        $pageId = Route::param('id');

        if ($_POST) {
            $post = $_POST['FORM'];
            $post['status'] = Arr::get( $_POST, 'status', 0 );
            if (FilterPages::valid($post)) {
                if (FilterPages::update($post, $pageId)) {
                    $filters = $_POST['filters'];
                    FilterPageAttribute::batchUpdate($filters, $pageId);

                    Message::GetMessage(1, __('Вы успешно изменили данные!'));
                    if(Arr::get($_POST, 'button', 'save') == 'save-close') {
                        HTTP::redirect('wezom/filter-pages');
                    } elseif (Arr::get($_POST, 'button', 'save') == 'save-add') {
                        HTTP::redirect('wezom/filter-pages/create');
                    }
                    else {
                        HTTP::redirect('wezom/filter-pages/edit/' . $pageId);
                    }
                } else {
                    Message::GetMessage(0, __('Не удалось изменить данные!'));
                }
            }
        }

        $page = FilterPages::getRow($pageId);

        $this->_toolbar = Widgets::get( 'Toolbar_Edit', ['list_link' => '/wezom/filter-pages']);
        $this->_seo['h1'] = __('Редактирование');
        $this->_seo['title'] = __('Редактирование');
        $this->setBreadcrumbs(__('Редактирование'), 'wezom/filter-pages/edit' . $pageId);

        $categories = Support::getSelectOptions('Catalog/Items/Select', 'catalog_tree', $page['obj']->category_id);
        $filterValues = DB::select()
            ->from('filter_page_attributes')
            ->where('filter_page_id', '=', $pageId)
            ->find_all()
            ->as_array();

        $this->_content = View::tpl(
            [
                'obj' => $page,
                'categories' => $categories,
                'filters' => $this->filters,
                'filterValues' => $filterValues,
                'tpl_folder' => $this->tpl_folder,
                'languages' => $this->_languages,
            ], $this->tpl_folder.'/Form');
    }

    public function filterValuesAction()
    {
        $filter = $_GET['filter'];
        if (!$filter) {
            echo json_encode([]); die;
        }

        $data = array();

        switch($filter) {
            case 'manufacturer':
                $data = Manufacturers::getRows()->as_array('alias', 'name');
                break;
            case 'brand':
                $data = Brands::getRows()->as_array('alias', 'name');
                break;
            case 'color':
                $data = Colors::getRows()->as_array('alias', 'name');
                break;
            case 'size':
                $data = DB::select(DB::expr('DISTINCT catalog_size_amount.size'))
                    ->from('catalog_size_amount')->find_all()->as_array('size', 'size');
                break;
            default:
                $data = SpecificationsValues::getRowsFilter($filter)->as_array('alias', 'name');
                break;
        }

        $result = array();
        foreach ($data as $key => $datum) {
            $result[] = array(
                'value' => $key,
                'label' => $datum
            );
        }

        echo json_encode($result);
        die;
    }
}