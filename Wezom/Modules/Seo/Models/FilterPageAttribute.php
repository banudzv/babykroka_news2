<?php

namespace Wezom\Modules\Seo\Models;

use Core\Common;
use Core\QB\DB;

class FilterPageAttribute extends Common
{
    public static $table = 'filter_page_attributes';

    public static function batchUpdate($filters, $pageId)
    {
        $existedFilters = self::getExistedFilters($pageId);

        $filterHashes = array();
        foreach ($filters as $filter) {
            $filterHashes[] = md5($filter['type'] . $filter['value']);

            if (isset($existedFilters[$filter['type']])) {
                $id = $existedFilters[$filter['type']]->id;
                self::update($filter, $id);
            } else {
                self::insert([
                    'filter_page_id' => $pageId,
                    'type' => $filter['type'],
                    'value' => $filter['value']
                ]);
            }
        }

        $existedFilters = self::getExistedFilters($pageId);

        foreach ($existedFilters as $filter) {
            $hash = md5($filter->type . $filter->value);

            if (!in_array($hash, $filterHashes)) {
                self::delete($filter->id);
            }
        }

        return true;
    }

    protected static function getExistedFilters($pageId)
    {
        return DB::select()
            ->from('filter_page_attributes')
            ->where('filter_page_id', '=', $pageId)
            ->find_all()
            ->as_array('type');
    }
}