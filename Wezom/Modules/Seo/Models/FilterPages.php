<?php

namespace Wezom\Modules\Seo\Models;

use Core\CommonI18n;

class FilterPages extends CommonI18n
{
    public static $table = 'filter_pages';
    public static $rulesI18n;

    public function __construct() {
        static::$rulesI18n = array(
            'h1' => array(
                array(
                    'error' => __('H1 не может быть пустым! (:lang)'),
                    'key' => 'not_empty',
                ),
            ),
            'title' => array(
                array(
                    'error' => __('Title не может быть пустым! (:lang)'),
                    'key' => 'not_empty',
                ),
            ),
            'description' => array(
                array(
                    'error' => __('Description не может быть пустым! (:lang)'),
                    'key' => 'not_empty',
                )
            )
        );
    }
}