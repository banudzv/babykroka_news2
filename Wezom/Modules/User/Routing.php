<?php

    return [
        // Auth
        'wezom/auth/login' => 'user/auth/login',
        'wezom/auth/edit' => 'user/auth/edit',
        'wezom/auth/logout' => 'user/auth/logout',
        // User
        'wezom/users' => 'user/users/index',
        'wezom/users/index' => 'user/users/index',
        'wezom/users/migrate' => 'user/users/migrate',
        'wezom/users/index/page/<page:[0-9]*>' => 'user/users/index',
        'wezom/users/add' => 'user/users/add',
        'wezom/users/edit/<id:[0-9]*>' => 'user/users/edit',
        'wezom/users/edit/<uid:[0-9]*>/<id:[0-9]*>' => 'user/customerChildren/edit',
        'wezom/users/delete/<id:[0-9]*>' => 'user/users/delete',
        // AdminscartSizes
        'wezom/admins' => 'user/admins/index',
        'wezom/admins/index' => 'user/admins/index',
        'wezom/admins/index/page/<page:[0-9]*>' => 'user/admins/index',
        'wezom/admins/add' => 'user/admins/add',
        'wezom/admins/edit/<id:[0-9]*>' => 'user/admins/edit',
        'wezom/admins/delete/<id:[0-9]*>' => 'user/admins/delete',
        // Roles
        'wezom/roles' => 'user/roles/index',
        'wezom/roles/index' => 'user/roles/index',
        'wezom/roles/edit/<id:[0-9]*>' => 'user/roles/edit',
        'wezom/roles/add' => 'user/roles/add',
        'wezom/roles/delete/<id:[0-9]*>' => 'user/roles/delete',
        // Customer Roles
        'wezom/customerRoles' => 'user/customerRoles/index',
        'wezom/customerRoles/index' => 'user/customerRoles/index',
        'wezom/customerRoles/edit/<id:[0-9]*>' => 'user/customerRoles/edit',
        'wezom/customerRoles/add' => 'user/customerRoles/add',
        'wezom/customerRoles/delete/<id:[0-9]*>' => 'user/customerRoles/delete',
        // Customer Children
        'wezom/customerChildren' => 'user/customerChildren/index',
        'wezom/customerChildren/index' => 'user/customerChildren/index',
        'wezom/customerChildren/edit/<id:[0-9]*>' => 'user/customerChildren/edit',
        'wezom/customerChildren/add' => 'user/customerChildren/add',
        'wezom/customerChildren/delete/<id:[0-9]*>' => 'user/customerChildren/delete',
    ];
