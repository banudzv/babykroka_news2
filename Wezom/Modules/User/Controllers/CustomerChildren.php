<?php
    namespace Wezom\Modules\User\Controllers;

    use Core\Config;
    use Core\HTML;
    use Core\Route;
    use Core\Widgets;
    use Core\Message;
    use Core\Arr;
    use Core\HTTP;
    use Core\View;

    use Wezom\Modules\Base;
    use Wezom\Modules\User\Models\CustomerChildren AS Model;

    class CustomerChildren extends Base {

        public $tpl_folder = 'Users/Customers/';
        public $users;

        function before() {
            parent::before();
            $this->_seo['h1'] = __('Дети покупателей');
            $this->_seo['title'] = __('Дети покупателей');
            $this->setBreadcrumbs(__('Дети покупателей'), 'wezom/'.Route::controller().'/index');
            $this->users = \Wezom\Modules\User\Models\Users::getRows()->as_array('id','name');
        }


        function indexAction () {
            $result = Model::getRows();
            $this->_toolbar = Widgets::get( 'Toolbar_ListOrders', ['add' => 1,'delete' => 1]);
            $this->_content = View::tpl(
                [
                    'result' => $result,
                    'users' => $this->users,
                    'tpl_folder' => $this->tpl_folder,
                    'tablename' => Model::$table,
                    'pageName' => $this->_seo['h1'],
                ], $this->tpl_folder.'/Index');
        }


        function editAction () {
            if ($_POST) {
                $post = $_POST['FORM'];
                unset($_POST['FORM']);
                if( Model::valid($post) ) {
                    $post['birthday'] = strtotime($post['birthday']);
                    $post['sex'] = $_POST['sex'];
                    $res = Model::update($post, Route::param('id'));
                    if($res) {
                        Message::GetMessage(1, __('Вы успешно изменили данные!'));
                    } else {
                        Message::GetMessage(0, __('Не удалось изменить данные!'));
                    }
                    if(Route::param('uid')){
                        $this->redirectAfterSave(Route::param('uid'),'users');
                    }else{
                        $this->redirectAfterSave(Route::param('id'));
                    }

                }
                $result = Arr::to_object($post);
            } else {
                $result = Model::getRow(Route::param('id'));
            }
            $this->_toolbar = Widgets::get( 'Toolbar_Edit' );
            $this->_seo['h1'] = __('Редактирование');
            $this->_seo['title'] = __('Редактирование');
            $this->setBreadcrumbs(__('Редактирование'), 'wezom/'.Route::controller().'/edit/'.(int) Route::param('id'));
            $this->_content = View::tpl(
                [
                    'obj' => $result,
                    'users' => $this->users,
                    'tpl_folder' => $this->tpl_folder,
                ], $this->tpl_folder.'/Form');
        }


        function addAction () {
            if ($_POST) {
                $post = $_POST['FORM'];
                unset($_POST['FORM']);
                $amount = Model::getChildrenByUserId($post['uid'])->count();
                if($amount == Config::get('basic.limit_children')){
                    Message::GetMessage(0, __('Не удалось добавить данные! Достигнут лимит по количеству детей у пользователя'));
                    HTTP::redirect('wezom/'.Route::controller().'/index');
                }
                if( Model::valid($post) ) {
                    if(!empty($post['birthday'])){
                        $post['birthday'] = strtotime($post['birthday']);
                    }
                    if (!empty($_POST['sex'])) {
                        $post['sex'] = $_POST['sex'];
                    }
                    $res = Model::insert($post);
                    if($res) {
                        Message::GetMessage(1, __('Вы успешно добавили данные!'));
						$this->redirectAfterSave($res);
                    } else {
                        Message::GetMessage(0, __('Не удалось добавить данные!'));
                    }
                }
                $result = Arr::to_object($post);
            } else {
                $result = [];
            }
            $users = \Wezom\Modules\User\Models\Users::getRows()->as_array('id','name');
            $this->_toolbar = Widgets::get( 'Toolbar_Edit' );
            $this->_seo['h1'] = __('Добавление');
            $this->_seo['title'] = __('Добавление');
            $this->setBreadcrumbs(__('Добавление'), 'wezom/'.Route::controller().'/add');
            $this->_content = View::tpl(
                [
                    'obj' => $result,
                    'users' => $users,
                    'tpl_folder' => $this->tpl_folder,
                ], $this->tpl_folder.'/Form');
        }


        function deleteAction() {
            $id = (int) Route::param('id');
            $page = Model::getRow($id);
            if(!$page) {
                Message::GetMessage(0, __('Данные не существуют!'));
                HTTP::redirect('wezom/'.Route::controller().'/index');
            }
            if(Model::image()){
                Model::deleteImage($page->image);
            }
            Model::delete($id);
            Message::GetMessage(1, __('Данные удалены!'));
            HTTP::redirect('wezom/'.Route::controller().'/index');
        }


    }
