<?php
    namespace Wezom\Modules\User\Controllers;

    use Core\Common;
    use Core\Config;
    use Core\Files;
    use Core\HTML;
    use Core\QB\DB;
    use Core\Route;
    use Core\User;
    use Core\Widgets;
    use Core\Message;
    use Core\Arr;
    use Core\HTTP;
    use Core\View;
    use Core\Pager\Pager;

    use Wezom\Modules\User\Models\CustomerChildren;
    use Wezom\Modules\Base;
    use Wezom\Modules\User\Models\CustomerRoles;
    use Wezom\Modules\User\Models\CustomerTypes;
    use Wezom\Modules\User\Models\Users AS Model;

    class Users extends Base {

        public $tpl_folder = 'Users';
        public $page;
        public $limit;
        public $offset;

        function before() {
            parent::before();
            $this->_seo['h1'] = __('Пользователи');
            $this->_seo['title'] = __('Пользователи');
            $this->setBreadcrumbs(__('Пользователи'), 'wezom/'.Route::controller().'/index');
            $this->page = (int) Route::param('page') ? (int) Route::param('page') : 1;
            $this->limit = (int) Arr::get($_GET, 'limit', Config::get('basic.limit_backend')) < 1 ?: Arr::get($_GET, 'limit', Config::get('basic.limit_backend'));
            $this->offset = ($this->page - 1) * $this->limit;
        }


        function indexAction () {
            $status = NULL;
            if ( isset($_GET['status']) && $_GET['status'] != '' ) { $status = Arr::get($_GET, 'status', 1); }
            $page = (int) Route::param('page') ? (int) Route::param('page') : 1;
            $count = Model::countRows($status);
            $result = Model::getRows($status, 'id', 'DESC', $this->limit, ($page - 1) * $this->limit);
            $pager = Pager::factory( $page, $count, $this->limit )->create();
            $customerRoles = CustomerRoles::getRows()->as_array('id','name');
            $customers = $customerRoles+['' => 'Все'];
            $customerTypes = [];
            foreach ($result as $user){
                $customerTypes[$user->id] = $this->getUserRoles($user,$customerRoles);
            }
            $this->_toolbar = Widgets::get( 'Toolbar/List', ['add' => 1, 'delete' => 1]);
            $this->_content = View::tpl(
                [
                    'result' => $result,
                    'customers' => $customers,
                    'customerTypes' => $customerTypes,
                    'tpl_folder' => $this->tpl_folder,
                    'tablename' => Model::$table,
                    'count' => $count,
                    'pager' => $pager,
                    'pageName' => $this->_seo['h1'],
                ], $this->tpl_folder.'/Index');
        }

        private function getUserRoles($user,$customerRoles){
            $name = '';
            $currentRoles = array_keys(CustomerTypes::getCustomerTypesByUserId($user->id));
            foreach ($currentRoles as $roleID){
                if(array_key_exists($roleID,$customerRoles)){
                    $name.=$customerRoles[$roleID].';';
                }
            }
            return ['list' => $name,'amountRoles' => count($currentRoles)];
        }


        function editAction () {
            if ($_POST) {
                $post = $_POST['FORM'];
                $roles = $_POST['ROLES'];
                $post['status'] = Arr::get( $_POST, 'status', 0 );
                $post['new_customer'] = Arr::get( $_POST, 'new_customer', 0 );
                $post['type'] = Arr::get( $_POST, 'type', 0 );
                $post['password'] = trim( Arr::get($_POST, 'password') );
                $post['birthday'] = strtotime($post['birthday']);
                $post['phone'] = '+38'. $post['phone'];
                if( Model::valid($post) ) {
                    CustomerTypes::changeCustomerTypes($roles,Route::param('id'));
                    $post['updated_at'] = time();
                    if( $post['password'] ) {
                        $post['password'] = User::factory()->hash_password($post['password']);
                    } else {
                        unset($post['password']);
                    }
                    $res = Model::update($post, Route::param('id'));
                    if($res) {
                        Message::GetMessage(1, __('Вы успешно изменили данные!'));
                    } else {
                        Message::GetMessage(0, __('Не удалось изменить данные!'));
                    }
                    $this->redirectAfterSave(Route::param('id'));
                }else{
                    $this->redirectAfterSave(Route::param('id'));
                }
                $result = Arr::to_object($post);
            } else {
                $result = Model::getRow(Route::param('id'));
            }

            $this->_toolbar = Widgets::get( 'Toolbar_Edit' );
            $this->_seo['h1'] = __('Редактирование');
            $this->_seo['title'] = __('Редактирование');
            $this->setBreadcrumbs(__('Редактирование'), 'wezom/'.Route::controller().'/edit/'.Route::param('id'));
            $customerRoles = CustomerRoles::getRows()->as_array('id','name');
            $customerTypes = CustomerTypes::getCustomerTypesByUserId(Route::param('id'));
            $children = CustomerChildren::getChildrenByUserId(Route::param('id'))->as_array();
            $this->_content = View::tpl(
                [
                    'obj' => $result,
                    'children' => $children,
                    'customerRoles' => $customerRoles,
                    'customerTypes' => $customerTypes,
                    'tpl_folder' => $this->tpl_folder,
                    'count_orders' => (int) DB::select([DB::expr('COUNT(id)'), 'count'])->from('orders')->where('user_id', '=', Route::param('id'))->count_all(),
                    'count_good_orders' => (int) DB::select([DB::expr('COUNT(id)'), 'count'])->from('orders')->where('status', '=', 1)->where('user_id', '=', Route::param('id'))->count_all(),
                    'amount_good_orders' => (int) DB::select([DB::expr('SUM(orders_items.count * orders_items.cost)'), 'amount'])
                        ->from('orders')
                        ->join('orders_items')->on('orders_items.order_id', '=', 'orders.id')
                        ->where('status', '=', 1)
                        ->where('user_id', '=', Route::param('id'))
                        ->find()
                        ->amount,
                    'socials' => DB::select()->from('users_networks')->where('user_id', '=', $result->id)->find_all(),
                ], $this->tpl_folder.'/Form');
        }

        function addAction () {
            if ($_POST) {
                $post = $_POST['FORM'];
                $roles = $_POST['ROLES'];

                $phone_number = $post['phone'];
                $phone_for_id = $this->generateIdentificationAction($phone_number);

                $post['status'] = Arr::get( $_POST, 'status', 0 );
                $post['new_customer'] = Arr::get( $_POST, 'new_customer', 1 );
                $post['type'] = Arr::get( $_POST, 'type', 0 );
                $post['password'] = trim( Arr::get($_POST, 'password') );
                $post['birthday'] = strtotime($post['birthday']);
                $post['customer_id'] = $phone_for_id;
                if( Model::valid($post) ) {
                    $post['updated_at'] = strtotime($post['updated_at']);
                    $post['password'] = User::factory()->hash_password($post['password']);
                    $post['hash'] = User::factory()->hash_user($post['email'], $post['password']);
                    $post['phone'] = '+38'. $post['phone'];
                    $res = Model::insert($post);
                    CustomerTypes::changeCustomerTypes($roles,$res);
                    if($res) {
                        Message::GetMessage(1, __('Вы успешно добавили данные!'));
						$this->redirectAfterSave($res);
                    } else {
                        Message::GetMessage(0, __('Не удалось добавить данные!'));
                    }
                }
                unset($post['password']);
                $result = Arr::to_object($post);
            } else {
                $result = [];
            }
            $this->_toolbar = Widgets::get( 'Toolbar_Edit' );
            $this->_seo['h1'] = __('Редактирование');
            $this->_seo['title'] = __('Редактирование');
            $this->setBreadcrumbs(__('Редактирование'), 'wezom/'.Route::controller().'/edit/'.Route::param('id'));
            $this->_content = View::tpl(
                [
                    'its_add' => true,
                    'obj' => $result,
                    'tpl_folder' => $this->tpl_folder,
                ], $this->tpl_folder.'/Form');
        }


        function deleteAction() {
            $id = (int) Route::param('id');
            $page = Model::getRow($id);
            if(!$page) {
                Message::GetMessage(0, __('Данные не существуют!'));
                HTTP::redirect('wezom/'.Route::controller().'/index');
            }
            Model::delete($id);
            Message::GetMessage(1, __('Данные удалены!'));
            HTTP::redirect('wezom/'.Route::controller().'/index');
        }

        function migrateAction(){
            if(!User::admin()){
                Message::GetMessage(0, __('Доступ запрещён!'));
                HTTP::redirect('wezom/'.Route::controller().'/index');
            }
            $customers = CustomerTypes::getRows()->as_array();
            if(!empty($customers)){
                Message::GetMessage(0, __('Выгрузка уже была произведена!'));
                HTTP::redirect('wezom/'.Route::controller().'/index');
            }
            $users = Model::getRows();
            if(!empty($users)){
                foreach ($users as $user){
                    if($user->role_id == 1){
                        if($user->type == 0){
                           $role_id = 1;
                        }elseif($user->type == 1){
                            $role_id = 2;
                        }elseif($user->type == 2){
                            $role_id = 3;
                        }
                        CustomerTypes::insert([
                            'uid' => $user->id,
                            'role_id' => $role_id
                        ]);
                    }
                }
            }
            Message::GetMessage(1, __('Данные перенесены успешно!'));
            HTTP::redirect('wezom/'.Route::controller().'/index');
        }

        private function generateIdentificationAction($phone){
            $uPhone = preg_replace('/[^0-9]*/', '', $phone);
            $customer_id = substr($uPhone,0);

            return $customer_id;
        }
    }
