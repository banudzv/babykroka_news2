<?php
    namespace Wezom\Modules\User\Models;

    use Core\Common;
    use Core\HTML;
    use Core\QB\DB;

    /**
     * Class @CustomerTypes
     * @package Wezom\Modules\User\Models
     * @extends Core\Common
     */
    class CustomerTypes extends Common
    {
        public static $table = 'customer_types';

        /**
         * Получаем все роли пользователя по его идентификатору
         * @param integer $uid
         * @return array
         */
        public static function getCustomerTypesByUserId($uid): array
        {
            return DB::select()
                ->from(static::$table)
                ->where('uid', '=', $uid)
                ->find_all()
                ->as_array('role_id','uid');
        }

        /**
         * Изменяем связь ролей пользователя.
         * Сравниваем значения и если они совпадают пропускаем обновление данных иначе находим пользователя в базе и удаляем все его связи. Пересоздаём в базе информацию о пользователе.
         * @param array $roles
         * @param integer $uid
         * @return bool
         */
        public static function changeCustomerTypes($roles, $uid): bool
        {
            $rowExistence = self::getRow($uid,'uid');
            if($rowExistence){
                if(!self::compareValues($roles,$uid)){
                    return true;
                }
                self::delete($uid, 'uid');
            }
            foreach ($roles as $role){
                self::insert(
                    [
                        'uid' => $uid,
                        'role_id' => $role
                    ]
                );
            }
            return true;
        }

        /**
         * Проверяем надо ли изменять данные, если изменений не найдено даем понять,что пропускаем шаг с обновлением связей
         * @param array $newValues
         * @param integer $uid
         * @return bool
         */
        protected static function compareValues(array $newValues, int $uid): bool
        {
            $oldValues = self::getCustomerTypesByUserId($uid);
            $countSameElements = 0;
            $countOldElements = count($oldValues);
            $countNewElements = count($newValues);
            foreach ($newValues as $value){
                if(isset($oldValues[$value])){
                    $countSameElements++;
                }
            }
            if($countOldElements == $countSameElements && $countNewElements == $countOldElements){
                return false;
            }
            return true;
        }

    }
