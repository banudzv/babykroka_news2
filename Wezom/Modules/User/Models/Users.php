<?php
    namespace Wezom\Modules\User\Models;

    use Core\Common;
    use Core\Config;
    use Core\Arr;
    use Core\HTML;
    use Core\Message;
    use Core\QB\Database_Result_Cached;
    use Core\QB\DB;
    use Core\Route;

    class Users extends Common {

        public static $table = 'users';
        public static $filters = [
            'name' => [
                'table' => NULL,
                'action' => 'LIKE',
            ],
            'last_name' => [
                'table' => NULL,
                'action' => 'LIKE',
            ],
            'email' => [
                'table' => NULL,
                'action' => 'LIKE',
            ],
            'phone' => [
                'table' => NULL,
                'action' => 'LIKE',
            ],
        ];
        public static $rules = [];
        public static function valid($post = []) {
            static::$rules = [
                'name' => [
                    [
                        'error' => __('Имя не может быть пустым!'),
                        'key' => 'not_empty',
                    ],
                ],
                'email' => [
                    [
                        'error' => __('Поле "E-Mail" введено некорректно!'),
                        'key' => 'email',
                    ],
                ],
                'customer_id' => [
                    [
                        'error' => __('Поле "Дата рождения" введено некорректно!'),
                        'key' => 'birthday',
                    ],
                ],
            ];
            if( Route::param('id') ) {
                if(DB::select([DB::expr('COUNT(id)'), 'count'])->from('users')->where('email', '=', Arr::get($post, 'email'))->where('id', '!=', Route::param('id'))->count_all()) {
                    Message::GetMessage(0, __('Указанный E-Mail уже занят!'));
                    return FALSE;
                }
                if(DB::select([DB::expr('COUNT(id)'), 'count'])->from('users')->where('customer_id', '=', Arr::get($post, 'customer_id'))->where('id', '!=', Route::param('id'))->count_all()) {
                    Message::GetMessage(0, __('Указанный ID клиента уже занят!'));
                    return FALSE;
                }
            } else {
                if(DB::select([DB::expr('COUNT(id)'), 'count'])->from('users')->where('email', '=', Arr::get($post, 'email'))->count_all()) {
                    Message::GetMessage(0, __('Указанный E-Mail уже занят!'));
                    return FALSE;
                }
                if(DB::select([DB::expr('COUNT(id)'), 'count'])->from('users')->where('customer_id', '=', Arr::get($post, 'customer_id'))->count_all()) {
                    Message::GetMessage(0, __('№ тел уже зарегистрирован!'));
                    return FALSE;
                }
            }
            if(Arr::get($_POST, 'password') || !Route::param('id')) {
                if(mb_strlen(Arr::get($_POST, 'password'), 'UTF-8') < Config::get('main.password_min_length') ) {
                    Message::GetMessage(0, __('Пароль должен быть не короче N символов!', [':count' => Config::get('main.password_min_length')]));
                    return FALSE;
                }
            }
            return parent::valid($post);
        }

        /**
         * @param array $data - associative array with data to update
         * @param $value
         * @param string $field - field for where clause
         * @return Database_Result_Cached|object
         */
        public static function update($data, $value, $field = 'id')
        {
            return DB::update(static::$table)->set($data)->where($field, '=', $value)->execute();
        }

        /**
         * Получаем данные об электронных адресах по роли пользователя
         * @param integer $role_id
         * @return array
         */
        public static function getUsersEmailByRoleId($role_id): array
        {
            return DB::select(static::$table.'.email')
                ->from(static::$table)
                ->join(CustomerTypes::table(),'LEFT')
                ->on(static::$table.'.id','=',CustomerTypes::table().'.uid')
                ->where(CustomerTypes::table().'.role_id', '=', $role_id)
                ->find_all()
                ->as_array();
        }

        /**
         * @param $id
         * @return mixed|null
         */
        public static function getForOrder($id) {
            $user = DB::select(
                'users.*',
                [DB::expr('COUNT(DISTINCT orders.id)'), 'orders'],
                [DB::expr('SUM(orders_items.cost * orders_items.count)'), 'amount'],
                [DB::expr('SUM(orders_items.count)'), 'count_items']
            )
                ->from('users')
                ->join('orders')->on('orders.user_id', '=', 'users.id')
                ->join('orders_items')->on('orders_items.order_id', '=', 'orders.id')
                ->where('users.id', '=', $id)
                ->where('orders.status', '=', 1);
            return $user->find();
        }

        /**
         * @param null $status
         * @param null $sort
         * @param null $type
         * @param null $limit
         * @param null $offset
         * @param bool $filter
         * @return Database_Result_Cached|object
         */
        public static function getRows($status = NULL, $sort = NULL, $type = NULL, $limit = NULL, $offset = NULL, $filter = true) {
            if($_GET['customer_type']) {
                $result = DB::select(static::$table . '.*', [CustomerTypes::table() . '.role_id', 'customer_role'])->from(static::$table)
                    ->where(static::$table . '.role_id', '=', 1);
            }else{
                $result = DB::select()->from(static::$table)
                    ->where(static::$table . '.role_id', '=', 1);
            }
            if($filter){
                $result = self::setFilter($result);
            }
            if( $status !== NULL ) {
                $result->where(static::$table.'.status', '=', $status);
            }
            if($_GET['customer_type']){
                $result
                    ->join(CustomerTypes::table(),'LEFT')
                    ->on(static::$table.'.id','=',CustomerTypes::table().'.uid')
                    ->where(CustomerTypes::table().'.role_id', '=', $_GET['customer_type']);
            }
            if( $sort !== NULL ) {
                if( $type !== NULL ) {
                    $result->order_by(static::$table.'.'.$sort, $type);
                } else {
                    $result->order_by(static::$table.'.'.$sort);
                }
            }
            if( $limit !== NULL ) {
                $result->limit($limit);
                if( $offset !== NULL ) {
                    $result->offset($offset);
                }
            }
            return $result->find_all();
        }

        /**
         * @param null $status
         * @param bool $filter
         * @return int
         */
        public static function countRows($status = NULL, $filter = true) {
            $result = DB::select([DB::expr('COUNT('.static::$table.'.id)'), 'count'])->from(static::$table)->where(static::$table.'.role_id', '=', 1);
            if($filter){
                $result = parent::setFilter($result);
            }
            if( $status !== NULL ) {
                $result->where(static::$table.'.status', '=', $status);
            }
            if($_GET['customer_type']){
                $result
                    ->join(CustomerTypes::table(),'LEFT')
                    ->on(static::$table.'.id','=',CustomerTypes::table().'.uid')
                    ->where(CustomerTypes::table().'.role_id', '=', $_GET['customer_type']);
            }
            return $result->count_all();
        }
        public static function setFilter($result)
        {
            if (!is_array(static::$filters)) {
                return $result;
            }
            foreach (static::$filters as $key => $value) {
                if (isset($key) && isset($_GET[$key]) && trim($_GET[$key])) {
                    $get = strip_tags($_GET[$key]);
                    $get = trim($get);
                    if (!Arr::get($value, 'action', null)) {
                        $action = '=';
                    } else {
                        $action = Arr::get($value, 'action');
                    }
                    $table = false;
                    if (Arr::get($value, 'table', null)) {
                        $table = Arr::get($value, 'table');
                    } else if (Arr::get($value, 'table', null) === null) {
                        $table = static::$table;
                    }
                    if ($action == 'LIKE') {
                        $get = '%' . $get . '%';
                    }
                    if (Arr::get($value, 'field')) {
                        $key = Arr::get($value, 'field');
                    }
                    if ($key == 'phone'){
                        $get = preg_replace("/[^0-9]/", '', $get);
                        $get = '%' . $get . '%';
                        $result->where(DB::expr("REPLACE( REPLACE( REPLACE(REPLACE(`users`.`phone`, '(', ''), ')', '') , '-', ''), ' ', '')"), $action, $get);
                    } else {
                        if ($table !== false) {
                            $result->where($table . '.' . $key, $action, $get);
                        } else {
                            $result->where(DB::expr($key), $action, $get);
                        }
                    }
                }
            }
            return $result;
        }

    }
