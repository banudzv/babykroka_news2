<?php
    namespace Wezom\Modules\User\Models;

    use Core\Common;
    use Core\QB\DB;

    /**
     * Class @CustomerChildren
     * @package Wezom\Modules\User\Models
     * @extends Core\Common
     */
    class CustomerChildren extends Common
    {
        public static $table = 'customer_children';
        public static $rules = [];

        /**
         * @param array $data
         * @return bool
         */
        public static function valid($data = [])
        {
            static::$rules = [
                'name' => [
                    [
                        'error' => __('Поле "Имя ребёнка" не может быть пустым!'),
                        'key' => 'not_empty',
                    ],
                ],
            ];
            return parent::valid($data);
        }

        public static $filters = [
            'name' => [
                'table' => NULL,
                'action' => 'LIKE',
            ],
            'uid' => [
                'table' => NULL,
                'action' => '=',
            ],
        ];

        /**
         * @param null /integer $status - 0 or 1
         * @param null /string $sort
         * @param null /string $type - ASC or DESC. No $sort - no $type
         * @param null /integer $limit
         * @param null /integer $offset - no $limit - no $offset
         * @param boolean $filter
         * @return object
         */
        public static function getRows($status = null, $sort = null, $type = null, $limit = null, $offset = null, $filter = true)
        {
            $result = DB::select()->from(static::$table);
            if (($status !== null) && self::checkField(static::$table, 'status')) {
                $result->where('status', '=', $status);
            }
            if( $_GET['date_s'] ) {
                $result->where(static::$table . '.birthday', '>=', $_GET['date_s']);
            }
            if( $_GET['date_po'] ) {
                $result->where(static::$table.'.birthday', '<=', $_GET['date_s'] + 24 * 60 * 60 - 1);
            }
            if ($filter) {
                $result = static::setFilter($result);
            }
            if ($sort !== null) {
                if ($type !== null) {
                    $result->order_by($sort, $type);
                } else {
                    $result->order_by($sort);
                }
            }
            $result->order_by('id', 'DESC');
            if ($limit !== null) {
                $result->limit($limit);
                if ($offset !== null) {
                    $result->offset($offset);
                }
            }
            return $result->find_all();
        }

        /**
         * Получаем информацию о детях по идентификатору пользователя
         * @param integer $uid
         * @return object
         */
        public static function getChildrenByUserId($uid)
        {
            return DB::select()
                ->from(self::table())
                ->where('uid', '=', $uid)
                ->find_all();
        }

    }
