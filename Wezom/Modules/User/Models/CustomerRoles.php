<?php
    namespace Wezom\Modules\User\Models;

    use Core\Common;

    /**
     * Class @CustomerRoles
     * @package Wezom\Modules\User\Models
     * @extends Core\Common
     */
    class CustomerRoles extends Common
    {
        public static $table = 'customer_roles';
        public static $rules = [];

        public static function valid($data = [])
        {
            static::$rules = [
                'name' => [
                    [
                        'error' => __('Поле "Название" не может быть пустым!'),
                        'key' => 'not_empty',
                    ],
                ],
                'alias' => [
                    [
                        'error' => __('Поле "Алиас" не может быть пустым!'),
                        'key' => 'not_empty',
                    ],
                ],
                'code' => [
                    [
                        'error' => __('Поле "Код" не может быть пустым!'),
                        'key' => 'not_empty',
                    ],
                ],
            ];
            return parent::valid($data);
        }

    }
