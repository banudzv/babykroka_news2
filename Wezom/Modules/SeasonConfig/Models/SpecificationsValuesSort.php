<?php
    namespace Wezom\Modules\SeasonConfig\Models;

    use Core\Arr;
    use Core\Message;
    use Core\QB\DB;

    class SpecificationsValuesSort extends \Core\Common {

        public static $table = 'specifications_values_sort';
        public static $rules = [];


        public static function getRowsBySpecificationValue($specifications) {
            $arr = [0];
            foreach($specifications AS $s) {
                $arr[] = $s->id;
            }
            $specValues = DB::select()
                ->from(static::$table)
                ->where(static::$table.'.specifications_values_id', 'IN', $arr)
                ->order_by(static::$table.'.sort')
                ->find_all();
            return $specValues;
        }


        public static function getRowsBySpecificationsValueID($specifications) {
            if( !$specifications ) {
                $specifications = [];
            }
            if( !is_array($specifications) ) {
                $specifications = [$specifications];
            }
            $specValues = DB::select()
                ->from(static::$table)
                ->where(static::$table.'.specifications_values_id', 'IN', $specifications)
                ->order_by(static::$table.'.sort')
                ->find_all();
            return $specValues;
        }


        /*public static function checkValue($specification_id, $alias) {
            $result = DB::select(static::$table.'.*')
                ->from(static::$table)
                ->where('alias', '=', $alias)
                ->where('specification_id', '=', $specification_id)
                ->where('status', '=', 1);
            return $result->find();
        }*/


        /**
         * @param integer $specification_id - Specification ID
         * @param null/integer $status - 0 or 1
         * @param null/string $sort
         * @param null/string $type - ASC or DESC. No $sort - no $type
         * @param null/integer $limit
         * @param null/integer $offset - no $limit - no $offset
         * @return object
         */

        public static function valid($data = [])
        {
            static::$rules = [
                'season_start' => [
                    [
                        'error' => __('Укажите правильную дату начала сезона!'),
                        'key' => 'not_empty',
                    ],
                ],
                'season_end' => [
                    [
                        'error' => __('Укажите правильную дату конца сезона!'),
                        'key' => 'not_empty',
                    ],
                ],
            ];
            return parent::valid($data);
        }

    }