<?php
    namespace Wezom\Modules\SeasonConfig\Models;

    use Core\Arr;
    use Core\Message;
    use Core\QB\DB;

    class SeasonTime extends \Core\Common {

        public static $table = 'season_time';
        public static $rules = [];


        public static function getRows($status = null, $sort = null, $type = null, $limit = null, $offset = null, $filter = false)
        {
            $result = DB::select()->from(static::$table);
            if ($status !== null) {
                $result->where('status', '=', $status);
            }
            if ($filter) {
                $result = static::setFilter($result);
            }
            if ($sort !== null) {
                if ($type !== null) {
                    $result->order_by($sort, $type);
                } else {
                    $result->order_by($sort);
                }
            }
            $result->order_by('id', 'DESC');
            if ($limit !== null) {
                $result->limit($limit);
                if ($offset !== null) {
                    $result->offset($offset);
                }
            }
            return $result->find_all();
        }


        /*public static function checkValue($specification_id, $alias) {
            $result = DB::select(static::$table.'.*')
                ->from(static::$table)
                ->where('alias', '=', $alias)
                ->where('specification_id', '=', $specification_id)
                ->where('status', '=', 1);
            return $result->find();
        }*/


        /**
         * @param integer $specification_id - Specification ID
         * @param null/integer $status - 0 or 1
         * @param null/string $sort
         * @param null/string $type - ASC or DESC. No $sort - no $type
         * @param null/integer $limit
         * @param null/integer $offset - no $limit - no $offset
         * @return object
         */

        public static function valid($data = [])
        {
            static::$rules = [
                'start' => [
                    [
                        'error' => __('Укажите правильную дату начала сезона!'),
                        'key' => 'date',
                    ],
                ],
                'finish' => [
                    [
                        'error' => __('Укажите правильную дату конца сезона!'),
                        'key' => 'date',
                    ],
                ],
            ];
            return parent::valid($data);
        }

    }