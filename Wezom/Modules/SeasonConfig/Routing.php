<?php   
    
    return [
        'wezom/seasons/index' => 'SeasonConfig/seasons/index',
        'wezom/seasons/add' => 'SeasonConfig/seasons/add',
        'wezom/seasons/edit/<id:[0-9]*>' => 'SeasonConfig/seasons/edit',
        'wezom/seasons/delete/<id:[0-9]*>' => 'SeasonConfig/seasons/delete',
    ];