<?php
    namespace Wezom\Modules\SeasonConfig\Controllers;

    use Core\Arr;
    use Core\HTML;
    use Core\QB\DB;
    use Core\Route;
    use Core\Validation\Valid;
    use Core\Widgets;
    use Core\Message;
    use Core\HTTP;
    use Core\View;
    use Core\Common;
    use Wezom\Modules\Catalog\Models\Specifications;
    use Wezom\Modules\Catalog\Models\SpecificationsValues;
    use Wezom\Modules\Config\Models\HelperFtp;
    use Wezom\Modules\SeasonConfig\Models\SpecificationsValuesSort;
    use Wezom\Modules\SeasonConfig\Models\SeasonTime;

    class Seasons extends \Wezom\Modules\Base
    {

        public $tpl_folder = 'Season';

        function before()
        {
            parent::before();
            $this->_seo['h1'] = __('Настройки сезонов');
            $this->_seo['title'] = __('Настройки сезонов');
            $this->setBreadcrumbs(__('Сезоны'), 'wezom/' . Route::controller() . '/index');
        }

        function indexAction()
        {
            $season = SeasonTime::getRows();

            $this->_toolbar = Widgets::get('Toolbar_ListOrders', ['add' => 1, 'delete' => 0]);
            $this->_content = View::tpl(
                [
                    'result' => $season,
                    'tpl_folder' => $this->tpl_folder,
                    'tablename' => SeasonTime::$table,

                ], $this->tpl_folder . '/Index');
        }

        function addAction()
        {
            $result = [];
            $post = $_POST;
            if ($_POST) {
                if (SeasonTime::valid($_POST)) {
                    $start_date = explode('.', Arr::get($post, 'start'));
                    $finish = explode('.', Arr::get($post, 'finish'));
                    $priority = Arr::get($post, 'priority');
                    $data_season = [
                        'start' => '20' . $start_date[2] . '-' . $start_date[1] . '-' . $start_date[0],
                        'finish' => '20' . $finish[2] . '-' . $finish[1] . '-' . $finish[0],
                        'created_at' => time(),
                    ];
                    $season_id = SeasonTime::insert($data_season);
                    if ($season_id) {
                        foreach ($priority as $id => $value) {
                            $data = [
                                'row_id' => $season_id,
                                'specifications_values_id' => $id,
                                'sort' => $value,
                            ];
                            SpecificationsValuesSort::insert($data);
                        }
                    }
                    if ($season_id) {
                        Message::GetMessage(1, __('Вы успешно добавили запись!'));
                        HTTP::redirect('wezom/' . Route::controller() . '/index/');
                    }


                    $result = Arr::to_object($post);
                }
            }
            $_groups = DB::select()->from('config_groups')->where('alias', '=', 'seasons')->as_object()->find();

            $specification_id = Specifications::getRow('sezon','alias');
            $specification_id = Specifications::getRowSimple('sezon', 'alias');
            $specs = null;
            if ($specification_id) {
                $specs = DB::select('specifications_values.id', 'specifications_values_i18n.name', 'specifications_values_sort.sort')
                    ->from('specifications_values')
                    ->join('specifications_values_i18n')
                    ->on('specifications_values.id', '=', 'specifications_values_i18n.row_id')
                    ->join('specifications_values_sort')
                    ->on('specifications_values.id', '=', 'specifications_values_sort.specifications_values_id')
                    ->where('specifications_values.specification_id', '=', $specification_id->id)
                    ->where('specifications_values_sort.row_id', '=', (int)Route::param('id'))
                    ->where('specifications_values_i18n.language', '=', \I18n::$defaultLangBackend)
                    ->find_all();
            }
            //$this->_toolbar = Widgets::get( 'Toolbar_Edit', ['noAdd' => true, 'noClose' => true]);
            $this->_seo['h1'] = __('Добавление');
            $this->_seo['title'] = __('Добавление');
            $this->_toolbar = Widgets::get('Toolbar_Edit', ['noAdd' => true, 'noClose' => true]);
            $this->setBreadcrumbs(__('Добавление'), 'wezom/'.Route::controller().'/add');
            $this->_content = View::tpl(
                [
                    'obj' => $result,
                    'specs'=>$specs,
                    'group'=>$_groups,
                    //'spec_value'=>$spec_value,
                    'tpl_folder' => $this->tpl_folder,
                ], $this->tpl_folder.'/Add');
        }
        function editAction()
        {
            $result = [];
            $post = $_POST;
            if ($_POST) {
                if (SeasonTime::valid($_POST)) {
                    $start_date = explode('.', Arr::get($post, 'start'));
                    $finish = explode('.', Arr::get($post, 'finish'));
                    $priority = Arr::get($post, 'priority');
                    $data_season = [
                        'start' => '20' . $start_date[2] . '-' . $start_date[1] . '-' . $start_date[0],
                        'finish' => '20' . $finish[2] . '-' . $finish[1] . '-' . $finish[0],
                        'created_at' => time(),
                    ];
                    $obj = SeasonTime::getRow((int)Route::param('id'), 'id');
                    if ($obj) {
                        $season_id = SeasonTime::update($data_season, (int)Route::param('id'));
                        //Message::GetMessage(0, __('Сезон обновлён!'));
                        if ($season_id) {
                            SpecificationsValuesSort::delete((int)Route::param('id'),'row_id');
                            foreach ($priority as $id => $value) {
                                $data = [
                                    'row_id' => (int)Route::param('id'),
                                    'specifications_values_id' => $id,
                                    'sort' => $value,
                                ];
                                SpecificationsValuesSort::insert($data);
                            }
                        }
                        if ($season_id) {
                            Message::GetMessage(1, __('Вы успешно изменили данные!'));
                            $this->redirectAfterSave(Route::param('id'));
                        }
                    }

                    $result = Arr::to_object($post);
                }
            }
            $_groups = DB::select()->from('config_groups')->where('alias', '=', 'seasons')->as_object()->find();
            $obj = SeasonTime::getRow((int)Route::param('id'), 'id');
            $specification_id = Specifications::getRowSimple('sezon', 'alias');
            $specs = null;
            if ($specification_id) {
                $specs = DB::select('specifications_values.id', 'specifications_values_i18n.name', 'specifications_values_sort.sort')
                    ->from('specifications_values')
                    ->join('specifications_values_i18n')
                    ->on('specifications_values.id', '=', 'specifications_values_i18n.row_id')
                    ->join('specifications_values_sort')
                    ->on('specifications_values.id', '=', 'specifications_values_sort.specifications_values_id')
                    ->where('specifications_values.specification_id', '=', $specification_id->id)
                    ->where('specifications_values_sort.row_id', '=', (int)Route::param('id'))
                    ->where('specifications_values_i18n.language', '=', \I18n::$defaultLangBackend)
                    ->find_all();
            }
            if (count($specs)<1){
                $specs = DB::select('specifications_values.id', 'specifications_values_i18n.name')
                    ->from('specifications_values')
                    ->join('specifications_values_i18n')
                    ->on('specifications_values.id', '=', 'specifications_values_i18n.row_id')
                    ->where('specifications_values.specification_id', '=', $specification_id->id)
                    ->where('specifications_values_i18n.language', '=', \I18n::$defaultLangBackend)
                    ->find_all();
            }
                $this->_toolbar = Widgets::get('Toolbar_Edit', ['noAdd' => true, 'noClose' => true]);
                $this->_seo['h1'] = __('Добавление');
                $this->_seo['title'] = __('Добавление');
                $this->setBreadcrumbs(__('Редактирование'), 'wezom/' . Route::controller() . '/edit/' . (int)Route::param('id'));
                $this->_content = View::tpl(
                    [
                        'id' => $obj->id,
                        'start' => $obj->start,
                        'finish' => $obj->finish,
                        'obj' => $result,
                        'specs' => $specs,
                        'group' => $_groups,
                        //'spec_value'=>$spec_value,
                        'tpl_folder' => $this->tpl_folder,
                    ], $this->tpl_folder . '/Edit');
            }
        public static function deleteAction(){
            $id = (int)Route::param('id');
            $find = SeasonTime::getRow($id,'id');
            if ($find) {
                SpecificationsValuesSort::delete($id, 'row_id');
                SeasonTime::delete($id, 'id');
                Message::GetMessage(1, __('Вы успешно удалили запись!'));
                HTTP::redirect('wezom/' . Route::controller() . '/index');
            }else{
                Message::GetMessage(4, __('Данных не существует!'));
                HTTP::redirect('wezom/' . Route::controller() . '/index');
            }
        }

    }

