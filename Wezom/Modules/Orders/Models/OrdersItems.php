<?php
    namespace Wezom\Modules\Orders\Models;

    use Core\QB\DB;
    use Core\Arr;
    use Core\Message;
    use Core\Route;

    class OrdersItems extends \Core\CommonI18n {

        public static $table = 'orders_items';
        public static $rules = [];

        public static function getRows($order_id) {
            $cart = DB::select(
                'catalog.*',
                [static::$table.'.cost', 'price'],
                [static::$table.'.id', 'order_item_id'],
                ['colors_i18n.name', 'color'],
                static::$table.'.count',
                static::$table.'.size'
            )
                ->from(static::$table)
                ->join('catalog', 'LEFT')->on(static::$table.'.catalog_id', '=', 'catalog.id')
                ->join('colors', 'LEFT')->on('colors.alias', '=', 'catalog.color_alias')
                ->join('colors_i18n')
                ->on('colors.id','=','colors_i18n.row_id')
                ->where('colors_i18n.language', '=', \I18n::lang())
                ->where(static::$table.'.order_id', '=', $order_id)
                ->find_all();

            return $cart;
        }

        public static function getRowsMail($order_id) {
            $cart = DB::select(
                'catalog.*',
                [static::$table.'.cost', 'price'],
                [static::$table.'.id', 'order_item_id']
            )
                ->from(static::$table)
                ->join('catalog', 'LEFT')->on(static::$table.'.catalog_id', '=', 'catalog.id')
                ->where(static::$table.'.order_id', '=', $order_id)
                ->group_by('catalog.id')
                ->find_all();

            $arr = [];
            foreach ($cart as $obj){
                $sizes = DB::select(
                    static::$table.'.size',
                    static::$table.'.count'
                )->from(static::$table)
                    ->where(static::$table.'.order_id', '=', $order_id)
                    ->where(static::$table.'.catalog_id', '=', $obj->id)
                    ->find_all();

                $color = DB::select()->from('colors')->where('alias','=',$obj->color_alias)->find();
                $count = 0;
                foreach ($sizes as $size){
                    $count += $size->count;
                }
                $obj->count = $count;
                $obj->sizes = $sizes;
                $obj->color_name = $color->name;
                $arr[] = $obj;
            }
            return $arr;
        }

        public static function getSame($order_id, $catalog_id) {
            $cart = DB::select('catalog.*', static::$table.'.count', static::$table.'.id')
                ->from(static::$table)
                ->join('catalog', 'LEFT')->on(static::$table.'.catalog_id', '=', 'catalog.id')
                ->where(static::$table.'.order_id', '=', $order_id)
                ->where(static::$table.'.catalog_id', '=', $catalog_id);
            return $cart->find();
        }

        public static function valid($data = [])
        {
            static::$rules = [
                'id' => [
                    [
                        'error' => __('Нельзя добавить товар несуществующему заказу!'),
                        'key' => 'digits',
                    ],
                ],
                'catalog_id' => [
                    [
                        'error' => __('Нужно выбрать товар для добавления!'),
                        'key' => 'digits',
                    ],
                ],
                'count' => [
                    [
                        'error' => __('Укажите количество товара больше 0!'),
                        'key' => 'digits',
                    ],
                ],
            ];
            return parent::valid($data);
        }

    }
