<?php
    namespace Wezom\Modules;

    use Core\HTML;

    class Ajax extends Base {

        protected $post;
        protected $get;
        protected $files;

        public function before() {
            parent::before();
            $this->post = $_POST;
            $this->get = $_GET;
            $this->files = $_FILES;
            header("Content-type:application/json");
        }


        // Generate Ajax answer
        public function answer( $data ) {
            echo json_encode( $data );
            die;
        }


        // Generate Ajax success answer
        public function success( $data = []) {
            $this->checkCronSession();
            if( !is_array( $data ) ) {
                $data = [
                    'response' => $data,
                ];
            }
            $data['success'] = true;
            $this->answer( $data );
        }


        // Generate Ajax answer with error
        public function error( $data = []) {
            $this->checkCronSession();
            if( !is_array( $data ) ) {
                $data = [
                    'response' => $data,
                ];
            }
            $data['success'] = false;
            $this->answer( $data );
        }

        private function checkCronSession(){
            if(isset($_SESSION['started']) && $_SESSION['started'] == 1){
                unset($_SESSION['started']);
            }
        }

    }
