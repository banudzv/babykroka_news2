<?php

    namespace Wezom\Modules\Faq\Models;

    class Faq extends \Core\CommonI18n {

        public static $table = 'faq';
        public static $filters = [
            'name' => [
                'table' => 'faq',
                'action' => 'LIKE',
            ],
            'row_id' => [
                'table' => 'faq',
                'action' => '=',
            ]
        ];
        public static $rules = [];

        public static function valid($data = [])
        {
            static::$rulesI18n = [
                'name' => [
                    [
                        'error' => __('Название не может быть пустым!'),
                        'key' => 'not_empty',
                    ],
                ],
                'text' => [
                    [
                        'error' => __('Описание не может быть пустым!'),
                        'key' => 'not_empty',
                    ]
                ]
            ];
            return parent::valid($data);
        }

    }
