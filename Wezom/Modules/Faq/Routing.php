<?php

    return [

        'wezom/faq/index' => 'faq/faq/index',
        'wezom/faq/index/page/<page:[0-9]*>' => 'faq/faq/index',
        'wezom/faq/edit/<id:[0-9]*>' => 'faq/faq/edit',
        'wezom/faq/delete/<id:[0-9]*>' => 'faq/faq/delete',
        'wezom/faq/add' => 'faq/faq/add',
    ];