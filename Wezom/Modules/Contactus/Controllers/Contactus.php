<?php
namespace Wezom\Modules\Contactus\Controllers;

use Core\Config;
use Core\Route;
use Core\Widgets;
use Core\Message;
use Core\Arr;
use Core\HTTP;
use Core\View;

use Wezom\Modules\Contactus\Models\Contactus AS Model;

class Contactus extends \Wezom\Modules\Base {

    public $tpl_folder = 'Contacts/Contacts';
    public $page;

    function before() {
        parent::before();
        $this->_seo['h1'] = __('Контакты');
        $this->_seo['title'] = __('Контакты');
        $this->setBreadcrumbs(__('Контакты'), 'wezom/'.Route::controller().'/index');
        $this->page = (int) Route::param('page') ? (int) Route::param('page') : 1;
    }

    function indexAction () {
        $status = NULL;
        if ( isset($_GET['status']) && $_GET['status'] != '' ) { $status = Arr::get($_GET, 'status', 1); }
        $result = Model::getRows($status, 'id', 'DESC');
        $this->_toolbar = Widgets::get( 'Toolbar_List', ['add' => 1, 'delete' => 1]);
        $this->_content = View::tpl(
            [
                'result' => $result,
                'tpl_folder' => $this->tpl_folder,
                'tablename' => Model::$table,
                'pageName' => $this->_seo['h1'],
            ], $this->tpl_folder.'/Index');
    }

    function editAction () {
        if ($_POST) {
            $post = $_POST['FORM'];
            $post['address'] = Arr::get( $_POST, 'address');
            $post['latitude'] = Arr::get( $_POST, 'latitude');
            $post['longitude'] = Arr::get( $_POST, 'longitude');
            $post['zoom'] = Arr::get( $_POST, 'zoom');
            $post['status'] = Arr::get( $_POST, 'status', 0 );
            if( Model::valid($post) ) {
                $res = Model::update($post, Route::param('id'));
                if($res) {
                    Message::GetMessage(1, __('Вы успешно изменили данные!'));
                } else {
                    Message::GetMessage(0, __('Не удалось изменить данные!'));
                }
                $this->redirectAfterSave(Route::param('id'));
            }
            $result     = Arr::to_object($post);
        } else {
            $result = Model::getRow(Route::param('id'));
        }
        $this->_toolbar = Widgets::get( 'Toolbar_Edit' );
        $this->_seo['h1'] = __('Редактирование');
        $this->_seo['title'] = __('Редактирование');
        $this->setBreadcrumbs(__('Редактирование'), 'wezom/'.Route::controller().'/edit/'.Route::param('id'));
        $this->_content = View::tpl(
            [
                'obj' => $result,
                'tpl_folder' => $this->tpl_folder,
            ], $this->tpl_folder.'/Form');
    }

    function addAction () {
        if ($_POST) {
            $post = $_POST['FORM'];
            $post['address'] = Arr::get( $_POST, 'address');
            $post['latitude'] = Arr::get( $_POST, 'latitude');
            $post['longitude'] = Arr::get( $_POST, 'longitude');
            $post['zoom'] = Arr::get( $_POST, 'zoom');
            $post['status'] = Arr::get( $_POST, 'status', 0 );
            if( Model::valid($post) ) {
                $res = Model::insert($post);
                if($res) {
                    Message::GetMessage(1, __('Вы успешно добавили данные!'));
                    $this->redirectAfterSave($res);
                } else {
                    Message::GetMessage(0, __('Не удалось добавить данные!'));
                }
            }
            $result = Arr::to_object($post);
        } else {
            $result = [];
        }
        $this->_toolbar = Widgets::get( 'Toolbar_Edit' );
        $this->_seo['h1'] = __('Добавление');
        $this->_seo['title'] = __('Добавление');
        $this->setBreadcrumbs(__('Добавление'), 'wezom/'.Route::controller().'/add');
        $this->_content = View::tpl(
            [
                'obj' => $result,
                'tpl_folder' => $this->tpl_folder,
            ], $this->tpl_folder.'/Form');
    }

    function deleteAction() {
        $id = (int) Route::param('id');
        $page = Model::getRow($id);
        if(!$page) {
            Message::GetMessage(0, __('Данные не существуют!'));
            HTTP::redirect('wezom/'.Route::controller().'/index');
        }
        Model::delete($id);
        Message::GetMessage(1, __('Данные удалены!'));
        HTTP::redirect('wezom/'.Route::controller().'/index');
    }

}