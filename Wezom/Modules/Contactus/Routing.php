<?php   
    
    return [
        'wezom/contactus/index' => 'contactus/contactus/index',
        'wezom/contactus/add' => 'contactus/contactus/add',
        'wezom/contactus/edit/<id:[0-9]*>' => 'contactus/contactus/edit',
        'wezom/contactus/delete/<id:[0-9]*>' => 'contactus/contactus/delete',
    ];