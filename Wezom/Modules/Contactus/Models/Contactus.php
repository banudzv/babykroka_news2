<?php
namespace Wezom\Modules\Contactus\Models;

class Contactus extends \Core\Common {

    public static $table = 'contacts_us';
    public static $filters = [
        'name' => [
            'table' => NULL,
            'action' => 'LIKE',
        ],
    ];
    public static $rules = [];

    public static function valid($data = [])
    {
        static::$rules = [
            'name' => [
                [
                    'error' => __('Название не может быть пустым!'),
                    'key' => 'not_empty',
                ],
            ],
            'address' => [
                [
                    'error' => __('Адрес не может быть пустым!'),
                    'key' => 'not_empty',
                ],
            ],
        ];
        return parent::valid($data);
    }

}