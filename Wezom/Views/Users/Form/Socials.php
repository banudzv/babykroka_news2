<div class="widget">
    <div class="widgetContent">
        <ul class="anyClass accordion harmonica">
            <li><a href="#" class="btn harFull harClose"><?php echo __('Социальные сети'); ?></a>
                <ul class="">
                    <div>
                        <table class="table table-hover table-condensed">
                            <thead>
                            <tr>
                                <th><?php echo __('Сеть'); ?></th>
                                <th>UID</th>
                                <th><?php echo __('Имя'); ?></th>
                                <th><?php echo __('Фамилия'); ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if( !count($socials) ): ?>
                                <tr style="background-color: #F3F3C3;">
                                    <td colspan="4"><?php echo __('Пользователь не прикрепил ни одной социальной сети'); ?></td>
                                </tr>
                            <?php else: ?>
                                <?php foreach( $socials AS $key => $value ): ?>
                                    <tr>
                                        <td><?php echo $value->network; ?></td>
                                        <td><a href="<?php echo $value->profile; ?>" target="_blank"><?php echo $value->uid; ?></a></td>
                                        <td><?php echo $value->first_name ?: '----'; ?></td>
                                        <td><?php echo $value->last_name ?: '----'; ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                </ul>
            </li>
        </ul>
    </div>
</div>
