<div class="widget">
    <div class="widgetContent">
        <ul class="anyClass accordion harmonica">
            <li><a href="#" class="btn harFull harClose"><?php echo __('Информация о детях'); ?></a>
                <ul class="">
                    <div>
                        <table class="table table-hover table-condensed">
                            <thead>
                            <tr>
                                <th><?php echo __('Имя'); ?></th>
                                <th><?php echo __('День рождения ребёнка'); ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if(empty($children)): ?>
                                <tr style="background-color: #F3F3C3;">
                                    <td colspan="4"><?php echo __('Пользователь не добавил информации о детях'); ?></td>
                                </tr>
                            <?php else: ?>
                                <?php foreach( $children AS $child): ?>
                                    <tr>
                                        <td><a href="<?php echo \Core\HTML::link('wezom/users/edit/'.\Core\Route::param('id').'/'.$child->id); ?>" target="_blank"><?php echo $child->name; ?></a></td>
                                        <td><?php echo $child->birthday ? date('Y.m.d',$child->birthday) :'----'; ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                </ul>
            </li>
        </ul>
    </div>
</div>
