<?php echo \Forms\Builder::open(); ?>
    <div class="form-actions" style="display: none;">
        <?php echo \Forms\Form::submit(['name' => 'name', 'value' => __('Отправить'), 'class' => 'submit btn btn-primary pull-right']); ?>
    </div>
    <div class="col-md-12">
        <div class="widget box">
            <div class="widgetHeader">
                <div class="widgetTitle">
                    <i class="fa fa-reorder"></i>
                    <?php echo __('Основные данные'); ?>
                </div>
            </div>
            <div class="widgetContent">
                <div class="form-vertical row-border">
                    <div class="form-group">
                        <label class="control-label"><?php echo __('Пол ребенка'); ?></label>
                        <div class="clear"></div>
                        <label class="checkerWrap-inline">
                        <?php echo \Forms\Builder::radio(
                            $obj->sex == 'malchik',
                            [
                                'name' => 'sex',
                                'value' => 'malchik'
                            ]
                        ); ?>
                        <?php echo __('Мальчик'); ?>
                        </label>
                        <label class="checkerWrap-inline">
                        <?php echo \Forms\Builder::radio(
                                $obj->sex == 'devochka',
                            [
                                'name' => 'sex',
                                'value' => 'devochka'
                            ]
                        ); ?>
                            <?=  __('Девочка') ?>
                        </label>
                    </div>
                    <div class="form-group">
                        <?php echo \Forms\Builder::input([
                            'name' => 'FORM[name]',
                            'value' => $obj->name,
                            'class' => 'valid',
                        ], __('Имя ребёнка')); ?>
                    </div>
                    <div class="form-group">
                        <?php echo \Forms\Builder::input([
                            'name' => 'FORM[birthday]',
                            'value' => $obj->birthday ? date('d.m.Y', $obj->birthday) : NULL,
                            'class' => 'myPicker valid',
                        ], __('День рождения')); ?>
                    </div>
                    <div class="form-group">
                        <?php echo \Forms\Builder::select($users,
                            $obj->uid, [
                                'id' => 'uid',
                                'disabled' => \Core\Route::param('uid') ? 'disabled' : NULL,
                                'name' => 'FORM[uid]',
                            ], __('Пользователь')); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php echo \Forms\Form::close(); ?>

<script>
    $(function(){
        var pickerInit = function( selector ) {
            var date = $(selector).val();
            $(selector).datepicker({
                showOtherMonths: true,
                selectOtherMonths: false
            });
            $(selector).datepicker('option', $.datepicker.regional['ru']);
            var dateFormat = $(selector).datepicker( "option", "dateFormat" );
            $(selector).datepicker( "option", "dateFormat", 'dd.mm.yy' );
            $(selector).val(date);
        };
        pickerInit('.myPicker');
        pickerInit('.myPicker2');
    });
</script>
