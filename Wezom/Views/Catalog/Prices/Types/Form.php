<?php echo \Forms\Builder::open(); ?>
<div class="form-actions" style="display: none;">
	<?php echo \Forms\Form::submit(['name' => 'name', 'value' => 'Отправить', 'class' => 'submit btn btn-primary pull-right']); ?>
</div>
<div class="col-md-7">
    <div class="widget box">
        <div class="widgetHeader">
            <div class="widgetTitle">
                <i class="fa fa-reorder"></i>
				<?php echo __('Основные данные'); ?>
            </div>
        </div>
        <div class="widgetContent">
            <div class="form-vertical row-border">
                <ul class="liTabs t_wrap">
                    <?php foreach ($languages as $key => $lang): ?>
                        <?php $public = \Core\Arr::get($langs, $key, array()); ?>
                        <?php echo $lang['default'] == 1 ? '<input type="hidden" class="default_lang" value="' . $lang['name'] . '">' : ''; ?>
                        <li class="t_item">
                            <a class="t_link" href="#"><?php echo $lang['name']; ?></a>
                            <div class="t_content">
                                <div class="form-group">
                                    <?php echo \Forms\Builder::input([
                                        'name' => 'FORM[' . $key . '][name]',
                                        'value' => $public->name,
                                        'class' => ['valid', $lang['default'] == 1 ? 'translitSource' : ''],
                                    ], __('Название')); ?>
                                </div>
                            </div>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="col-md-5">
    <div class="widget">
        <div class="widgetHeader myWidgetHeader">
            <div class="widgetTitle">
                <i class="fa fa-reorder"></i>
				<?php echo __('Базовые настройки'); ?>
            </div>
        </div>
        <div class="widgetContent">
            <div class="form-group">
                <div class="col-md-4">
                    <?php echo \Forms\Builder::bool($obj ? $obj->status : 1); ?>
                </div>
            </div>
            <div class="form-group">
                <?php echo \Forms\Builder::input([
                    'name' => 'FORM[code]',
                    'value' => $obj->code,
                    'class' => 'valid',
                ], [
                    'text' => __('Код цены'),
                ]); ?>
            </div>
            </div>
        </div>


    </div>
</div>
<?php echo \Forms\Form::close(); ?>
