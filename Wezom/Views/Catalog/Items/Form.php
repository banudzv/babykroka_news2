<?php use Core\View;
use Forms\Builder;

echo \Forms\Builder::open();
?>
<div class="form-actions" style="display: none;">
    <?php echo \Forms\Form::submit(['name' => 'name', 'value' => 'Отправить', 'class' => 'submit btn btn-primary pull-right']); ?>
</div>
<div class="col-md-6">
    <div class="widget box">
        <div class="widgetContent">
            <div class="form-vertical row-border">
                <ul class="liTabs t_wrap">
                    <?php foreach ($languages as $key => $lang): ?>
                        <?php $public = \Core\Arr::get($langs, $key, array()); ?>
                        <?php echo $lang['default'] == 1 ? '<input type="hidden" class="default_lang" value="' . $lang['name'] . '">' : ''; ?>
                        <li class="t_item">
                            <a class="t_link" href="#"><?php echo $lang['name']; ?></a>
                            <div class="t_content">
                                <div class="form-group">
                                    <?php echo \Forms\Builder::input([
                                        'name' => 'FORM[' . $key . '][name]',
                                        'value' => $public->name,
                                        'class' => ['valid', $lang['default'] == 1 ? 'translitSource' : ''],
                                    ], __('Название')); ?>
                                </div>

                                <div class="form-group">
                                    <?php echo \Forms\Builder::tiny([
                                        'name' => 'FORM[' . $key . '][content]',
                                        'value' => $public->content,
                                    ], __('Описание товара'));
                                    ?>
                                </div>
                                <div style="font-weight: bold; margin-bottom: 10px;"><?php echo __('Внимание! Незаполненные данные будут подставлены по шаблону', [':link' => \Core\HTML::link('wezom/seo_templates/edit/2')]); ?></div>
                                <div class="form-group">
                                    <?php echo \Forms\Builder::input([
                                        'name' => 'FORM[' . $key . '][h1]',
                                        'value' => $public->h1,
                                    ], [
                                        'text' => 'H1',
                                        'tooltip' => __('Рекомендуется, чтобы тег h1 содержал ключевую фразу, которая частично или полностью совпадает с title'),
                                    ]); ?>
                                </div>
                                <div class="form-group">
                                    <?php echo \Forms\Builder::input([
                                        'name' => 'FORM[' . $key . '][title]',
                                        'value' => $public->title,
                                    ], [
                                        'text' => 'Title',
                                        'tooltip' => __('<p>Значимая для продвижения часть заголовка должна быть не более 12 слов</p><p>Самые популярные ключевые слова должны идти в самом начале заголовка и уместиться в первых 50 символов, чтобы сохранить привлекательный вид в поисковой выдаче.</p><p>Старайтесь не использовать в заголовке следующие знаки препинания – . ! ? – </p>'),
                                    ]); ?>
                                </div>
                                <div class="form-group">
                                    <?php echo \Forms\Builder::textarea([
                                        'name' => 'FORM[' . $key . '][keywords]',
                                        'rows' => 5,
                                        'value' => $public->keywords,
                                    ], [
                                        'text' => 'Keywords',
                                    ]); ?>
                                </div>
                                <div class="form-group">
                                    <?php echo \Forms\Builder::textarea([
                                        'name' => 'FORM[' . $key . '][description]',
                                        'value' => $public->description,
                                        'rows' => 5,
                                    ], [
                                        'text' => 'Description',
                                    ]); ?>
                                </div>
                            </div>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="col-md-6">
    <div class="widget box">
        <div class="widgetHeader">
            <div class="widgetTitle">
                <i class="fa fa-list-alt"></i>
                <?php echo __('Основные настройки'); ?>
            </div>
        </div>

        <div class="widgetContent">
            <div class="form-group">
                <div class="rowSection">
                    <div class="col-md-4">
                        <?php echo \Forms\Builder::bool($obj ? $obj->status : 1); ?>
                    </div>
                    <div class="col-md-4">
                        <?php echo \Forms\Builder::bool($obj->new, 'new', __('Новинка')); ?>
                    </div>
                    <div class="col-md-4">
                        <?php echo \Forms\Builder::bool($obj->top, 'top', __('Рекомендуемый товар')); ?>
                    </div>
                    <div class="col-md-4">
                        <?php echo \Forms\Builder::bool($obj->sell_size, 'sell_size', __('Продажа ростовками')); ?>
                    </div>
                    <div class="col-md-4">
                        <?php echo \Forms\Builder::bool($obj->ucenka, 'ucenka', __('Уценка')); ?>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="rowSection">
                    <div class="col-md-6 form-group">
                        <?php echo \Forms\Builder::select('<option value="0">' . __('Не выбрано') . '</option>' . $tree,
                            NULL, [
                                'id' => 'parent_id',
                                'name' => 'FORM[parent_id]',
                                'class' => 'valid',
                            ], [
                                'text' => __('Группа'),
                                'tooltip' => '<b>' . __('При изменении группы товара меняется набор характеристик!') . '</b>',
                            ]); ?>
                    </div>
                    <div class="col-md-6 form-group">
                        <?php echo \Forms\Builder::input([
                            'name' => 'FORM[sort]',
                            'value' => $obj->sort,
                        ], [
                            'text' => __('Позиция'),
                            'tooltip' => '<b>' . __('Поле определяет позицию товара среди других в списках') . '</b>',
                        ]); ?>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <?php echo \Forms\Builder::alias([
                    'name' => 'FORM[alias]',
                    'value' => $obj->alias,
                    'class' => 'valid',
                ], [
                    'text' => __('Алиас'),
                    'tooltip' => __('<b>Алиас (англ. alias - псевдоним)</b><br>Алиасы используются для короткого именования страниц. <br>Предположим, имеется страница с псевдонимом «<b>about</b>». Тогда для вывода этой страницы можно использовать или полную форму: <br><b>http://domain/?go=frontend&page=about</b><br>или сокращенную: <br><b>http://domain/about.html</b>'),
                ], [
                    'data-url' => '/wezom/ajax/translitItems',
                    'data-ignore_id' => $obj->id
                ]); ?>
            </div>
            <div class="form-group">
                <?php echo __('Код товара: ') . '<b class="red">' . $obj->id . '</b>'; ?>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    <?php echo \Forms\Builder::bool($obj->sale, 'sale', __('Акция')); ?>
                </div>
            </div>
            <?php echo View::tpl(['prices' => $prices, 'price_types' => $price_types, 'obj' => $obj, 'currencies' => $currencies], 'Catalog/Items/Blocks/Price'); ?>
        </div>
        <div class="widgetContent">
            <div class="form-group">
                <div class="rowSection">
                    <div class="col-md-12">
                        <?php echo \Forms\Builder::input([
                            'name' => 'FORM[video]',
                            'value' => $obj->video,
                        ], [
                            'text' => __('Видеообзор')
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="widgetContent">
            <div class="form-group">
                <div class="rowSection">
                    <div class="col-md-12">
                        <?php echo \Forms\Builder::input([
                            'name' => 'related_products',
                            'value' => $related_products,
                        ], [
                            'text' => __('Связанные товары')
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="widgetContent">
            <div class="form-group">
                <div class="rowSection">
                    <div class="col-md-4">
                        <?php echo \Forms\Builder::bool($obj->torzgest, 'torzgest', __('Торжественный')); ?>
                    </div>
                    <div class="col-md-4">
                        <?php echo \Forms\Builder::bool($obj->karnaval, 'karnaval', __('Карнавальный')); ?>
                    </div>
                    <div class="col-md-4">
                        <?php echo \Forms\Builder::bool($obj->vypiska, 'vypiska', __('На выписку')); ?>
                    </div>
                    <div class="col-md-4">
                        <?php echo \Forms\Builder::bool($obj->krest, 'krest', __('Крестильный')); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="widget box">
        <div class="widgetHeader">
            <div class="widgetTitle">
                <i class="fa fa-list-alt"></i>
                <?php echo __('Характеристики'); ?>
            </div>
        </div>

        <div class="widgetContent">
            <div class="form-group">
                <?php echo \Forms\Builder::bool($obj->show_3d, 'show_3d', __('Отображать 3D обзор')); ?>
            </div>
            <div class="form-group">
                <label class="control-label">
                    <?php echo __('Обзор 3D'); ?>
                    <i class="fa fa-info-circle text-info bs-tooltip nav-hint"
                       title="<b><?php echo __('Архив формата zip. Изображения должны лежать в корне архива и быть пронумерованы в правильной последовательности от 1 !'); ?></b>"></i>
                </label>
                <div>
                    <?php if ($obj->id and is_dir(HOST . '/Media/images/catalog_3d/' . $obj->id)): ?>
                        <a class="btn btn-danger otherBtn"
                           href="/wezom/<?php echo Core\Route::controller(); ?>/remove_3d/<?php echo $obj->id; ?>"
                           onclick="return confirm(<?php echo __('"Продолжить?"'); ?>);">
                            <i class="fa fa-remove"></i>
                            <?php echo __('Удалить обзор 3D'); ?>
                        </a>
                    <?php else: ?>

                        <?php echo \Forms\Builder::file(array('name' => 'zip_3d', 'accept' => 'zip')); ?>
                    <?php endif ?>
                </div>
            </div>
            <div class="form-group">
                <?php echo \Forms\Builder::select(\Core\Support::selectData($colors, 'alias', 'name', [0, __('Нет')]),
                    $obj->color_alias, [
                        'id' => 'color_alias',
                        'name' => 'FORM[color_alias]',
                    ], __('Цвет')); ?>
            </div>
            <div class="form-group">
                <?php echo \Forms\Builder::select(\Core\Support::selectData($manufactures, 'alias', 'name', ['', __('Нет')]),
                    $obj->manufacturer_alias, [
                        'id' => 'manufacturer_alias',
                        'name' => 'FORM[manufacturer_alias]',
                    ], __('Производитель')); ?>
            </div>
            <div class="form-group">
                <?php echo \Forms\Builder::select(\Core\Support::selectData($brands, 'alias', 'name', [0, __('Нет')]),
                    $obj->brand_alias, [
                        'id' => 'brand_alias',
                        'name' => 'FORM[brand_alias]',
                    ], __('Бренд')); ?>
            </div>
            <div class="form-vertical row-border" id="specGroup">
                <?php foreach ($specifications as $spec): ?>
                    <?php if ($specValues[$spec->id] and count($specValues[$spec->id])): ?>
                        <div class="form-group <?php echo $spec->type_id == 3 ? 'multiSelectBlock' : NULL; ?>">
                            <?php if ($spec->type_id == 3): ?>
                                <?php echo \Forms\Builder::select(\Core\Support::selectData($specValues[$spec->id], 'alias', 'name'),
                                    \Core\Arr::get($specArray, $spec->alias, []), [
                                        'name' => 'SPEC[' . $spec->alias . '][]',
                                        'multiple' => 'multiple',
                                    ], $spec->name); ?>
                            <?php endif ?>
                            <?php if ($spec->type_id == 2 or $spec->type_id == 1): ?>
                                <?php echo \Forms\Builder::select(\Core\Support::selectData($specValues[$spec->id], 'alias', 'name', [0, __('Нет')]),
                                    \Core\Arr::get($specArray, $spec->alias), [
                                        'name' => 'SPEC[' . $spec->alias . ']',
                                    ], $spec->name); ?>
                            <?php endif ?>
                        </div>
                    <?php endif ?>
                <?php endforeach ?>
            </div>


            <div class="form-group multiSelectBlock">
                <?php echo \Forms\Builder::select(\Core\Support::selectData($ages, 'code', 'name'), $productAges, [
                    'name' => 'AGES[]',
                    'multiple' => 'multiple',
                ], __('Возрастные соответствия')); ?>
            </div>
            <?php if(isset($obj->width)): ?>
                <div class="form-group">
                    <?php echo Builder::input([
                        'type' => 'number',
                        'step' => '0.01',
                        'name' => 'FORM[width]',
                        'value' => $obj->width,
                        'class' => 'valid',
                    ], 'Ширина');?>
                </div>
            <?php endif; ?>
            <?php if(isset($obj->height)): ?>
                <div class="form-group">
                    <?php echo Builder::input([
                        'type' => 'number',
                        'step' => '0.01',
                        'name' => 'FORM[height]',
                        'value' => $obj->height,
                    ], 'Высота');?>
                </div>
            <?php endif; ?>


            <!--            --><?php //if($size):?>
            <?php echo Core\View::tpl(['size' => $size, 'sizes' => $sizes, 'sizes_chart' => $sizes_chart], 'Catalog/Items/ValuesSimple'); ?>
            <!--            --><?php //endif;?>

        </div>
    </div>
    <div class="widget box">
        <div class="widgetHeader">
            <div class="widgetTitle">
                <i class="fa fa-list-alt"></i>
                <?php echo __('Доп инфо'); ?>
            </div>
        </div>

        <div class="widgetContent">
            <div class="form-group">
                <?php echo Builder::input([
                    'type' => 'text',
                    'name' => 'FORM[portal_category_id_prom]',
                    'value' => $obj->portal_category_id_prom,
                ], 'Prom id');?>
            </div>
            <div class="form-group">
                <div class="widgetHeader">
                    <div class="widgetTitle">
                        <i class="fa fa-list-alt"></i>
                        <?php echo __('Фото url'); ?>
                    </div>
                </div>
                <div class="widgetContent">
                    <div class="dataInput contentVideo">
                        <span  class="rowVideos plus"
                              style="margin: 15px;cursor: pointer;height: 25px;">+</span>
                        <?php if (!empty(json_decode($obj->url_foto_prom))): ?>
                            <?php foreach (json_decode($obj->url_foto_prom) as $key => $url): ?>
                                <div class="form-group source-item" data-parent="">
                                    <div class="col-md-10">
                                        <input type="text" name="FOTOS[<?php echo $key;?>][url]" value="<?php echo $url ?>"
                                               class="form-control rowVideo url" data-name="url">
                                    </div>
                                    <div class="col-md-1">
                                        <span class="rowNav minus">-</span></div>
                                </div>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<?php echo \Forms\Form::close(); ?>

<?php echo $uploader; ?>
<?php echo $colorGroups; ?>
<?php /*if($size):*/ ?><!--
    <?php /*echo Core\View::tpl(['size' => $size], 'Catalog/Items/ValuesSimple'); */ ?>
--><?php /*endif;*/ ?>
<script>
    $(function () {
        $('input[name="sale"]').on('change', function () {
            var val = parseInt($(this).val());
            if (val) {
                var cost = $('input[name="FORM[cost]"]').val();
                var cost_old = $('input[name="FORM[cost_old]"]').val();
                $('input[name="FORM[cost]"]').val(cost_old);
                $('input[name="FORM[cost_old]"]').val(cost);
                $('.hiddenCostField').show();
            } else {
                var cost = $('input[name="FORM[cost]"]').val();
                var cost_old = $('input[name="FORM[cost_old]"]').val();
                $('input[name="FORM[cost]"]').val(cost_old);
                $('input[name="FORM[cost_old]"]').val(cost);
                $('.hiddenCostField').hide();
            }
        });

        $('#parent_id').on('change', function () {
            var catalog_tree_id = $(this).val();
            $.ajax({
                url: '/wezom/ajax/catalog/getSpecificationsByCatalogTreeID',
                type: 'POST',
                dataType: 'JSON',
                data: {
                    catalog_tree_id: catalog_tree_id
                },
                success: function (data) {
                    var i = 0;
                    var j = 0;
                    var val;
                    var html = '<option value="0"><?php echo __('Нет'); ?></option>';
                    $('#model_alias').html(html);
                    for (i = 0; i < data.brands.length; i++) {
                        html += '<option value="' + data.brands[i].alias + '">' + data.brands[i].name + '</option>';
                    }
                    $('#brand_alias').html(html);
                    html = '';
                    for (i = 0; i < data.specifications.length; i++) {
                        var spec = data.specifications[i];
                        if (data.specValues[spec.id]) {
                            var values = data.specValues[spec.id];
                            html += '<div class="form-group"><label class="control-label">' + spec.name + '</label>';
                            if (parseInt(spec.type_id) == 3) {
                                html += '<div class="multiSelectBlock"><div class="controls">';
                                html += '<select class="form-control" name="SPEC[' + spec.alias + '][]" multiple="10" style="height:150px;">';
                                for (j = 0; j < values.length; j++) {
                                    val = values[j];
                                    html += '<option value="' + val.alias + '">' + val.name + '</option>';
                                }
                                html += '</select>';
                                html += '</div></div>';
                            }
                            if (parseInt(spec.type_id) == 2 || parseInt(spec.type_id) == 1) {
                                html += '<div class=""><div class="controls">';
                                html += '<select class="form-control" name="SPEC[' + spec.alias + ']">';
                                html += '<option value="0"><?php echo __('Нет'); ?></option>';
                                for (j = 0; j < values.length; j++) {
                                    val = values[j];
                                    html += '<option value="' + val.alias + '">' + val.name + '</option>';
                                }
                                html += '</select>';
                                html += '</div></div>';
                            }
                            html += '</div>';
                        }
                    }
                    $('#specGroup').html(html);
                    multi_select();
                }
            });
        });

        $('#brand_alias').on('change', function () {
            var brand_alias = $(this).val();
            $.ajax({
                url: '/wezom/ajax/catalog/getModelsByBrandID',
                type: 'POST',
                dataType: 'JSON',
                data: {
                    brand_alias: brand_alias
                },
                success: function (data) {
                    var html = '<option value="0"><?php echo __('Нет'); ?></option>';
                    for (var i = 0; i < data.options.length; i++) {
                        html += '<option value="' + data.options[i].alias + '">' + data.options[i].name + '</option>';
                    }
                    $('#model_alias').html(html);
                }
            });
        });
    });
    $(function () {
        if ($('#sParametersSimple').length) {
            // Specification id
            var sID = $('#sParametersSimple').data('id');
            // Set checkbox
            var checkboxStart = function (el) {
                var parent = el.parent();
                if (parent.is('label')) {
                    if (el.prop('checked')) {
                        parent.addClass('checked');
                    } else {
                        parent.removeClass('checked');
                    }
                }
            };
            // Generate a row from object
            var generateTR = function (obj) {
                var html = '';
                html = '<tr id="sort_' + obj.id + '" data-id="' + obj.id + '">';
                html += '<td class="align-center">';
                html += '<input type="text" class="form-control sSize translitSource" data-trans="val-' + obj.id + '" value="' + obj.size + '" />';
                html += '</td>';
                html += '<td class="align-center">';
                html += '<input class="form-control sAmount translitConteiner" data-trans="val-' + obj.id + '" type="text" value="' + obj.amount + '" />';
                html += '</td>';
                html += '<td class="align-center">';
                html += '<a title="<?php echo __('Сохранить изменения'); ?>" href="#" class="sSave btn btn-xs bs-tooltip liTipLink" style="white-space: nowrap; margin-top: 7px;"><i class="fa fa-fixed-width">&#xf0c7;</i></a>';
                html += '<a title="<?php echo __('Удалить'); ?>" href="#" class="sDelete btn btn-xs bs-tooltip liTipLink" style="white-space: nowrap; margin-top: 7px;"><i class="fa fa-trash-o"></i></a>';
                html += '</td>';
                html += '</tr>';
                return html;
            };
            // Add a row
            $('#sSave').on('click', function (e) {
                e.preventDefault();
                loader($('#sValuesList'), 1);
                var size = $('#sSize').val();
                var amount = $('#sAmount').val();
                $.ajax({
                    url: '/wezom/ajax/catalog/addItemSizeValue',
                    type: 'POST',
                    dataType: 'JSON',
                    data: {
                        catalog_id: sID,
                        size: size,
                        amount: amount
                    },
                    success: function (data) {
                        if (data.success) {
                            var html = '';
                            if (data.result.length) {
                                for (var i = 0; i < data.result.length; i++) {
                                    html += generateTR(data.result[i]);
                                }
                            }
                            $('#sValuesList tbody').html(html);
                            $('#sValuesList input[type=checkbox]').each(function () {
                                checkboxStart($(this));
                            });
                            $('#sValuesList input[type=checkbox]').on('change', function () {
                                checkboxStart($(this));
                            });
                            $('#sSize').val('');
                            $('#sAmount').val('');
                        } else {
                            generate(data.error, 'warning');
                        }
                        loader($('#sValuesList'), 0);
                    }
                });
            });
            // Save a row
            $('#sValuesList').on('click', '.sSave', function (e) {
                e.preventDefault();
                loader($('#sValuesList'), 1);
                var tr = $(this).closest('tr');
                var id = tr.data('id');
                var size = tr.find('.sSize').val();
                var amount = tr.find('.sAmount').val();

                $.ajax({
                    url: '/wezom/ajax/catalog/editItemSizeValue',
                    type: 'POST',
                    dataType: 'JSON',
                    data: {
                        catalog_id: sID,
                        size: size,
                        id: id,
                        amount: amount
                    },
                    success: function (data) {
                        if (data.success) {
                            var html = '';
                            if (data.result.length) {
                                for (var i = 0; i < data.result.length; i++) {
                                    html += generateTR(data.result[i]);
                                }
                            }
                            $('#sValuesList tbody').html(html);
                            $('#sValuesList input[type=checkbox]').each(function () {
                                checkboxStart($(this));
                            });
                            $('#sValuesList input[type=checkbox]').on('change', function () {
                                checkboxStart($(this));
                            });
                        } else {
                            generate(data.error, 'warning');
                        }
                        loader($('#sValuesList'), 0);
                    }
                });
            });
            // Delete a row
            $('#sValuesList').on('click', '.sDelete', function (e) {
                e.preventDefault();
                loader($('#sValuesList'), 1);
                var tr = $(this).closest('tr');
                var id = tr.data('id');
                $.ajax({
                    url: '/wezom/ajax/catalog/deleteItemSizeValue',
                    type: 'POST',
                    dataType: 'JSON',
                    data: {
                        id: id
                    },
                    success: function (data) {
                        if (data.success) {
                            tr.remove();
                        } else {
                            generate(data.error, 'warning');
                        }
                        loader($('#sValuesList'), 0);
                    }
                });
            });
        }
    });
    $('.rowVideos.plus').on('click', function () {
        var select = '<div class="form-group source-item" data-parent="">' +
            '<div class="col-md-10">' +
            '<input type="text" name="" value="" class="form-control rowVideo url" data-name="url">' +
            '</div>' +
            '<div class="col-md-1">' +
            '<span  class="rowNav minus">-</span></div>' +
            '</div>';

        $(this).closest('.dataInput').append(select);
        refrechVideoKey();
    });
    $('.dataInput').on('click', '.rowNav.minus', function () {
        $(this).closest('.form-group').remove();
    });
    function refrechVideoKey() {
        let i = 0;
        $('.contentVideo').find('.source-item').each(function () {
            let $this = $(this);
            $this.find('.rowVideo').each(function () {
                let name = $(this).attr('data-name');
                $(this).attr('name', 'FOTOS[' + i + '][' + name + ']')
            });
            $this.find('.rowVideoDefault').each(function () {
                $(this).attr('name', 'FOTOS[' + i + '][' + $(this).attr('data-name') + ']')
            });
            i++;
        });
    }
</script>
