<div class="rowSection">
    <div class="col-md-12">
        <div class="btn-group">
            <a href="/wezom/items/no-photo/excel" target="_blank" class="btn btn-lg">Скачать</a>
        </div>
        <div class="widget">
            <div class="widget">
                <div class="widgetContent">
                    <table class="table table-striped table-hover checkbox-wrap">
                        <thead>
                            <tr>
                                <th><?php echo __('Код'); ?></th>
                                <th><?php echo __('Название'); ?></th>
                                <th class="nav-column textcenter">&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ( $items as $obj ): ?>
                                <tr data-id="<?php echo $obj->id; ?>">
                                    <td>
                                        <a href="/wezom/<?php echo Core\Route::controller(); ?>/edit/<?php echo $obj->id; ?>">
                                            <?php echo $obj->id; ?>
                                        </a>
                                    </td>
                                    <td>
                                        <?php echo $obj->name; ?>
                                    </td>
                                </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>
                    <?php echo $pager; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<span id="parameters" data-table=""></span>
