<?php use Core\Text;
use Forms\Builder;?>
<?php if(!empty($price_types)):?>
    <?php foreach ($price_types as $price_type => $price_type_name):?>
        <?php $priceText = Text::mb_ucfirst(mb_strtolower($price_type_name));?>
        <div class="form-group row">
            <div class="costField col-md-4">
                <?php echo Builder::input([
                    'type' => 'number',
                    'step' => '0.01',
                    'name' => 'PRICES['.$price_type.'][price]',
                    'value' => isset($prices[$price_type]) ? $prices[$price_type]->price : NULL,
                    'class' => 'valid',
                ], __(':price цена',[':price' => $priceText]));?>
            </div>
            <div class="hiddenCostField col-md-4" <?php echo (!empty($obj) && !$obj->sale) ? 'style="display:none;"' : ''; ?>>
                <?php echo Builder::input([
                    'type' => 'number',
                    'step' => '0.01',
                    'name' => 'PRICES['.$price_type.'][price_old]',
                    'value' => isset($prices[$price_type]) ? $prices[$price_type]->price_old : NULL,
                ], __('Старая :old цена',[':old' => $priceText])); ?>
            </div>
            <div class="costField col-md-4">
                <?php echo Builder::select($currencies, isset($prices[$price_type]) ? $prices[$price_type]->currency_id : 980,[
                    'name' => 'PRICES['.$price_type.'][currency_id]',
                    'class' => 'valid',
                ], __('Валюта')); ?>
            </div>
        </div>
    <?php endforeach;?>
<?php endif;?>
