<?php use Core\QB\DB;

if(\Core\User::god() || \Core\User::caccess() == 'edit'): ?>
    <div class="widget box">
        <!--<div class="widgetHeader"><div class="widgetTitle"><i class="fa fa-reorder"></i><?php /*echo __('Добавить значение'); */?></div></div>
        <div class="widgetContent">
            <table class="table table-striped table-bordered table-hover checkbox-wrap">
                <thead>
                    <tr>
                        <th class="align-center"><?php /*echo __('Размер'); */?></th>
                        <th class="align-center"><?php /*echo __('Количество'); */?></th>
                        <th class="align-center"></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="align-center">
                            <?php /*echo \Forms\Builder::select($sizes,
                                null, [
                                    'id' => 'sSize',
                                    'data-trans' => 2,
                                ]); */?>
                        </td>
                        <td class="align-center">
                            <?php /*echo \Forms\Builder::input([
                                'id' => 'sAmount',
                                'data-trans' => 2,
                            ]); */?>
                        </td>
                        <td class="align-center">
                            <a title="Сохранить" id="sSave" href="#" class="btn btn-xs bs-tooltip liTipLink" style="white-space: nowrap; margin-top: 7px;">
                                <i class="fa fa-fixed-width">&#xf0c7;</i>
                            </a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>-->
    </div>
<?php endif; ?>
<div class="widget box">
    <div class="widgetHeader"><div class="widgetTitle"><i class="fa fa-reorder"></i><?php echo __('Список значений'); ?></div></div>
    <div class="widgetContent" id="sValuesList">
        <table class="table table-striped table-bordered table-hover checkbox-wrap" data-specification="<?php echo Core\Route::param('id'); ?>">
            <thead>
                <tr>
                    <th class="align-center"><?php echo __('Размер'); ?></th>
                    <th class="align-center"><?php echo __('Количество'); ?></th>
                    <?php if(\Core\User::god() || \Core\User::caccess() == 'edit'): ?>
                        <th class="align-center"></th>
                    <?php endif; ?>
                </tr>
            </thead>
            <tr class="sortableSimple" data-params="<?php echo \Core\HTML::chars(json_encode(['table' => 'specifications_values'])); ?>">
                <?php foreach ( $size as $obj ): ?>
                    <?php
                    $id = DB::select()->from('helper_characteristics')->where('code', '=', $obj->code)->find()->id;
                    ?>
                <tr id="sort_<?php echo $obj->id; ?>" data-id="<?php echo $obj->id; ?>">
                        <td class="align-center">
                            <?php $attributes = [
                                'value' => $obj->size,
                                'data-trans' => 'val-'.$obj->id,
                                'class' => ['sSize'],
                            ]; ?>
                            <?php $attributes['disabled'] = 'disabled'; ?>
                            <?php echo \Forms\Builder::input($attributes); ?>
                        </td>
                        <td class="align-center">
                            <?php $attributes = [
                                'value' => $obj->amount,
                                'data-trans' => 'val-'.$obj->amount,
                                'class' => ['sAmount'],
                            ]; ?>
                            <?php if(!\Core\User::god() && \Core\User::caccess() != 'edit'): ?>
                                <?php $attributes['disabled'] = 'disabled'; ?>
                            <?php endif; ?>
                            <?php echo \Forms\Builder::input($attributes); ?>
                        </td>
                        <?php if(\Core\User::god() || \Core\User::caccess() == 'edit'): ?>
                            <td class="align-center">
                                <a title="<?php echo __('Сохранить изменения'); ?>" href="#" class="sSave btn btn-xs bs-tooltip liTipLink" style="white-space: nowrap; margin-top: 7px;"><i class="fa fa-fixed-width">&#xf0c7;</i></a>
                                <a title="<?php echo __('Удалить'); ?>" href="#" class="sDelete btn btn-xs bs-tooltip liTipLink" style="white-space: nowrap; margin-top: 7px;"><i class="fa fa-trash-o"></i></a>
                            </td>
                        <?php endif; ?>


                   <?php foreach (DB::select()->from('helper_characteristics_sizes')->where('helper_characteristics_id', '=', $id)->find_all() as $chart): ?>
                        <tr>
                            <td><?php echo $chart->mark?></td>
                            <td><?php echo $chart->parameter?></td>
                        </tr>
                    <?php endforeach; ?>
            </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>

<div id="sParametersSimple" data-id="<?php echo Core\Route::param('id'); ?>"></div>
