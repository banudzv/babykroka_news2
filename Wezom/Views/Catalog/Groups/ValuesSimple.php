<?php if(\Core\User::god() || \Core\User::caccess() == 'edit'): ?>
    <div class="widget box">
        <div class="widgetHeader"><div class="widgetTitle"><i class="fa fa-reorder"></i><?php echo __('Добавить значение'); ?></div></div>
        <div class="widgetContent">
            <table class="table table-striped table-bordered table-hover checkbox-wrap">
                <thead>
                    <tr>
                        <th class="align-center" width="45"><?php echo __('Метка	'); ?></th>
                        <th class="align-center"><?php echo __('от 0 - 1 года'); ?></th>
                        <th class="align-center"><?php echo __('от 1 - 2 лет'); ?></th>
                        <th class="align-center"><?php echo __('от 2 - 3 года'); ?></th>
                        <th class="align-center"><?php echo __('от 3 - 4 лет'); ?></th>
                        <th class="align-center"><?php echo __('от 4 - 5 лет'); ?></th>
                        <th class="align-center"><?php echo __('от 5 - 6 лет'); ?></th>
                        <th class="align-center"><?php echo __('от 6 - 7 лет'); ?></th>
                        <th class="align-center"><?php echo __('от 7 - 8 лет'); ?></th>
                        <th class="align-center"><?php echo __('от 8 - 9 лет'); ?></th>
                        <th class="align-center"><?php echo __('от 9 - 10 лет'); ?></th>
                        <th class="align-center"><?php echo __('от 10 - 11 лет'); ?></th>
                        <th class="align-center"><?php echo __('от 11 - 12 лет'); ?></th>
                        <th class="align-center"></th>
                    </tr>
                </thead>
                <tbody>
                <tr>
                        <td class="align-center">
                            <?php echo \Forms\Builder::select([
                                    1 => 'a',
                                    2 => 'b',
                                    3 => 'c',
                                    4 => 'd',
                                    5 => 'e',
                                    6 => 'f',
                                    7 => 'g '
                            ],1,[
                                'id' => 'sLabel',
                                'data-trans' => 2,
                            ]); ?>
                        </td>

                        <td class="align-center">
                            <?php echo \Forms\Builder::input([
                                'id' => 'sAge01',
                                'data-trans' => 2,
                                'class' => 'translitSource',
                            ]); ?>
                        </td>
                        <td class="align-center">
                            <?php echo \Forms\Builder::input([
                                'id' => 'sAge12',
                                'data-trans' => 2,
                                'class' => 'translitSource',
                            ]); ?>
                        </td>
                        <td class="align-center">
                            <?php echo \Forms\Builder::input([
                                'id' => 'sAge23',
                                'data-trans' => 2,
                                'class' => 'translitSource',
                            ]); ?>
                        </td>
                        <td class="align-center">
                            <?php echo \Forms\Builder::input([
                                'id' => 'sAge34',
                                'data-trans' => 2,
                                'class' => 'translitSource',
                            ]); ?>
                        </td>
                        <td class="align-center">
                            <?php echo \Forms\Builder::input([
                                'id' => 'sAge45',
                                'data-trans' => 2,
                                'class' => 'translitConteiner',
                            ]); ?>
                        </td>
                        <td class="align-center">
                            <?php echo \Forms\Builder::input([
                                'id' => 'sAge56',
                                'data-trans' => 2,
                                'class' => 'translitConteiner',
                            ]); ?>
                        </td>
                        <td class="align-center">
                            <?php echo \Forms\Builder::input([
                                'id' => 'sAge67',
                                'data-trans' => 2,
                                'class' => 'translitConteiner',
                            ]); ?>
                        </td>
                        <td class="align-center">
                            <?php echo \Forms\Builder::input([
                                'id' => 'sAge78',
                                'data-trans' => 2,
                                'class' => 'translitConteiner',
                            ]); ?>
                        </td>
                        <td class="align-center">
                            <?php echo \Forms\Builder::input([
                                'id' => 'sAge89',
                                'data-trans' => 2,
                                'class' => 'translitConteiner',
                            ]); ?>
                        </td>
                        <td class="align-center">
                            <?php echo \Forms\Builder::input([
                                'id' => 'sAge910',
                                'data-trans' => 2,
                                'class' => 'translitConteiner',
                            ]); ?>
                        </td>
                        <td class="align-center">
                            <?php echo \Forms\Builder::input([
                                'id' => 'sAge1011',
                                'data-trans' => 2,
                                'class' => 'translitConteiner',
                            ]); ?>
                        </td>
                        <td class="align-center">
                            <?php echo \Forms\Builder::input([
                                'id' => 'sAge1112',
                                'data-trans' => 2,
                                'class' => 'translitConteiner',
                            ]); ?>
                        </td>
                        <td class="align-center">
                            <a title="Сохранить" id="sSave" href="#" class="btn btn-xs bs-tooltip liTipLink" style="white-space: nowrap; margin-top: 7px;">
                                <i class="fa fa-fixed-width">&#xf0c7;</i>
                            </a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
<?php endif; ?>
<div class="widget box">
    <div class="widgetHeader"><div class="widgetTitle"><i class="fa fa-reorder"></i><?php echo __('Список значений'); ?></div></div>
    <div class="widgetContent" id="sValuesList">
        <table class="table table-striped table-bordered table-hover checkbox-wrap" data-specification="<?php echo Core\Route::param('id'); ?>">
            <thead>
                <tr>
                    <th class="align-center" width="45"><?php echo __('Метка	'); ?></th>
                    <th class="align-center"><?php echo __('от 0 - 1 года'); ?></th>
                    <th class="align-center"><?php echo __('от 1 - 2 лет'); ?></th>
                    <th class="align-center"><?php echo __('от 2 - 3 года'); ?></th>
                    <th class="align-center"><?php echo __('от 3 - 4 лет'); ?></th>
                    <th class="align-center"><?php echo __('от 4 - 5 лет'); ?></th>
                    <th class="align-center"><?php echo __('от 5 - 6 лет'); ?></th>
                    <th class="align-center"><?php echo __('от 6 - 7 лет'); ?></th>
                    <th class="align-center"><?php echo __('от 7 - 8 лет'); ?></th>
                    <th class="align-center"><?php echo __('от 8 - 9 лет'); ?></th>
                    <th class="align-center"><?php echo __('от 9 - 10 лет'); ?></th>
                    <th class="align-center"><?php echo __('от 10 - 11 лет'); ?></th>
                    <th class="align-center"><?php echo __('от 11 - 12 лет'); ?></th>
                    <?php if(\Core\User::god() || \Core\User::caccess() == 'edit'): ?>
                        <th class="align-center"></th>
                    <?php endif; ?>
                </tr>
            </thead>
            <tbody class="sortableSimple" data-params="<?php echo \Core\HTML::chars(json_encode(['table' => 'specifications_values'])); ?>">
                <?php foreach ( $result as $obj ): ?>
                    <tr id="sort_<?php echo $obj->id; ?>" data-id="<?php echo $obj->id; ?>">
                        <td class="align-center">
                            <?php $attributes = [
                                'data-trans' => 'val-'.$obj->id,
                                'class' => ['translitSource', 'sLabel'],
                            ]; ?>
                            <?php if(!\Core\User::god() && \Core\User::caccess() != 'edit'): ?>
                                <?php $attributes['disabled'] = 'disabled'; ?>
                            <?php endif; ?>
                            <?php echo \Forms\Builder::select([
                                1 => 'a',
                                2 => 'b',
                                3 => 'c',
                                4 => 'd',
                                5 => 'e',
                                6 => 'f',
                                7 => 'g',
                                8 => 'i',
                                9 => 'w'
                            ], $obj->label, $attributes); ?>
                        </td>
                        <td class="align-center">
                            <?php $attributes = [
                                'value' => $obj->age01,
                                'data-trans' => 'val-'.$obj->age01,
                                'class' => ['translitConteiner', 'sAge01'],
                            ]; ?>
                            <?php if(!\Core\User::god() && \Core\User::caccess() != 'edit'): ?>
                                <?php $attributes['disabled'] = 'disabled'; ?>
                            <?php endif; ?>
                            <?php echo \Forms\Builder::input($attributes); ?>
                        </td>
                        <td class="align-center">
                            <?php $attributes = [
                                'value' => $obj->age12,
                                'data-trans' => 'val-'.$obj->age12,
                                'class' => ['translitConteiner', 'sAge12'],
                            ]; ?>
                            <?php if(!\Core\User::god() && \Core\User::caccess() != 'edit'): ?>
                                <?php $attributes['disabled'] = 'disabled'; ?>
                            <?php endif; ?>
                            <?php echo \Forms\Builder::input($attributes); ?>
                        </td>
                        <td class="align-center">
                            <?php $attributes = [
                                'value' => $obj->age23,
                                'data-trans' => 'val-'.$obj->age23,
                                'class' => ['translitConteiner', 'sAge'],
                            ]; ?>
                            <?php if(!\Core\User::god() && \Core\User::caccess() != 'edit'): ?>
                                <?php $attributes['disabled'] = 'disabled'; ?>
                            <?php endif; ?>
                            <?php echo \Forms\Builder::input($attributes); ?>
                        </td>
                        <td class="align-center">
                            <?php $attributes = [
                                'value' => $obj->age34,
                                'data-trans' => 'val-'.$obj->age34,
                                'class' => ['translitConteiner', 'sAge34'],
                            ]; ?>
                            <?php if(!\Core\User::god() && \Core\User::caccess() != 'edit'): ?>
                                <?php $attributes['disabled'] = 'disabled'; ?>
                            <?php endif; ?>
                            <?php echo \Forms\Builder::input($attributes); ?>
                        </td>
                        <td class="align-center">
                            <?php $attributes = [
                                'value' => $obj->age45,
                                'data-trans' => 'val-'.$obj->age45,
                                'class' => ['translitConteiner', 'sAge45'],
                            ]; ?>
                            <?php if(!\Core\User::god() && \Core\User::caccess() != 'edit'): ?>
                                <?php $attributes['disabled'] = 'disabled'; ?>
                            <?php endif; ?>
                            <?php echo \Forms\Builder::input($attributes); ?>
                        </td>
                        <td class="align-center">
                            <?php $attributes = [
                                'value' => $obj->age56,
                                'data-trans' => 'val-'.$obj->age56,
                                'class' => ['translitConteiner', 'sAge56'],
                            ]; ?>
                            <?php if(!\Core\User::god() && \Core\User::caccess() != 'edit'): ?>
                                <?php $attributes['disabled'] = 'disabled'; ?>
                            <?php endif; ?>
                            <?php echo \Forms\Builder::input($attributes); ?>
                        </td>
                        <td class="align-center">
                            <?php $attributes = [
                                'value' => $obj->age67,
                                'data-trans' => 'val-'.$obj->age67,
                                'class' => ['translitConteiner', 'sAge67'],
                            ]; ?>
                            <?php if(!\Core\User::god() && \Core\User::caccess() != 'edit'): ?>
                                <?php $attributes['disabled'] = 'disabled'; ?>
                            <?php endif; ?>
                            <?php echo \Forms\Builder::input($attributes); ?>
                        </td>
                        <td class="align-center">
                            <?php $attributes = [
                                'value' => $obj->age78,
                                'data-trans' => 'val-'.$obj->age78,
                                'class' => ['translitConteiner', 'sAge78'],
                            ]; ?>
                            <?php if(!\Core\User::god() && \Core\User::caccess() != 'edit'): ?>
                                <?php $attributes['disabled'] = 'disabled'; ?>
                            <?php endif; ?>
                            <?php echo \Forms\Builder::input($attributes); ?>
                        </td>
                        <td class="align-center">
                            <?php $attributes = [
                                'value' => $obj->age89,
                                'data-trans' => 'val-'.$obj->age89,
                                'class' => ['translitConteiner', 'sAge89'],
                            ]; ?>
                            <?php if(!\Core\User::god() && \Core\User::caccess() != 'edit'): ?>
                                <?php $attributes['disabled'] = 'disabled'; ?>
                            <?php endif; ?>
                            <?php echo \Forms\Builder::input($attributes); ?>
                        </td>
                        <td class="align-center">
                            <?php $attributes = [
                                'value' => $obj->age910,
                                'data-trans' => 'val-'.$obj->age910,
                                'class' => ['translitConteiner', 'sAge910'],
                            ]; ?>
                            <?php if(!\Core\User::god() && \Core\User::caccess() != 'edit'): ?>
                                <?php $attributes['disabled'] = 'disabled'; ?>
                            <?php endif; ?>
                            <?php echo \Forms\Builder::input($attributes); ?>
                        </td>
                        <td class="align-center">
                            <?php $attributes = [
                                'value' => $obj->age1011,
                                'data-trans' => 'val-'.$obj->age1011,
                                'class' => ['translitConteiner', 'sAge1011'],
                            ]; ?>
                            <?php if(!\Core\User::god() && \Core\User::caccess() != 'edit'): ?>
                                <?php $attributes['disabled'] = 'disabled'; ?>
                            <?php endif; ?>
                            <?php echo \Forms\Builder::input($attributes); ?>
                        </td>
                        <td class="align-center">
                            <?php $attributes = [
                                'value' => $obj->age1112,
                                'data-trans' => 'val-'.$obj->age1112,
                                'class' => ['translitConteiner', 'sAge1112'],
                            ]; ?>
                            <?php if(!\Core\User::god() && \Core\User::caccess() != 'edit'): ?>
                                <?php $attributes['disabled'] = 'disabled'; ?>
                            <?php endif; ?>
                            <?php echo \Forms\Builder::input($attributes); ?>
                        </td>
                        <?php if(\Core\User::god() || \Core\User::caccess() == 'edit'): ?>
                            <td class="align-center">
                                <a title="<?php echo __('Сохранить изменения'); ?>" href="#" class="sSave btn btn-xs bs-tooltip liTipLink" style="white-space: nowrap; margin-top: 7px;"><i class="fa fa-fixed-width">&#xf0c7;</i></a>
                                <a title="<?php echo __('Удалить'); ?>" href="#" class="sDelete btn btn-xs bs-tooltip liTipLink" style="white-space: nowrap; margin-top: 7px;"><i class="fa fa-trash-o"></i></a>
                            </td>
                        <?php endif; ?>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>

<div id="sParametersSimple" data-id="<?php echo Core\Route::param('id'); ?>"></div>


<!-- SCRIPT ZONE -->
<script>
    $(function(){
        if( $('#sParametersSimple').length ) {
            // Specification id
            var sID = $('#sParametersSimple').data('id');
            // Set checkbox
            var checkboxStart = function( el ) {
                var parent = el.parent();
                if(parent.is('label')){
                    if(el.prop('checked')){
                        parent.addClass('checked');
                    } else {
                        parent.removeClass('checked');
                    }
                }
            };
            // Generate a row from object
            var generateTR = function( obj ) {
                var html = '';
                var arr = [ 'a', 'b', 'c', 'd', 'e', 'f', 'g'];
                html  = '<tr id="sort_'+obj.id+'" data-id="'+obj.id+'">';
                html += '<td class="align-center">';


                html += '<select class="form-control translitSource sLabel" data-trans="val-'+obj.id+'">';
                for (var i = 0; i < arr.length; i++) {
                    if ((i+1) == parseInt(obj.label)) {
                        html += '<option value="' + (i+1) + '" selected>' + arr[i] + '</option>';
                    } else{
                        html += '<option value="' + (i+1) + '">' + arr[i] + '</option>';
                    }
                }
                html += '</select>';
                html += '</td>';
                html += '<td class="align-center">';
                html += '<input type="text" class="form-control sAge01 translitSource" data-trans="val-'+obj.id+'" value="'+obj.age01+'" />';
                html += '</td>';
                html += '<td class="align-center">';
                html += '<input type="text" class="form-control sAge12 translitSource" data-trans="val-'+obj.id+'" value="'+obj.age12+'" />';
                html += '</td>';
                html += '<td class="align-center">';
                html += '<input type="text" class="form-control sAge23 translitSource" data-trans="val-'+obj.id+'" value="'+obj.age23+'" />';
                html += '</td>';
                html += '<td class="align-center">';
                html += '<input type="text" class="form-control sAge34 translitSource" data-trans="val-'+obj.id+'" value="'+obj.age34+'" />';
                html += '</td>';
                html += '<td class="align-center">';
                html += '<input type="text" class="form-control sAge45 translitSource" data-trans="val-'+obj.id+'" value="'+obj.age45+'" />';
                html += '</td>';
                html += '<td class="align-center">';
                html += '<input type="text" class="form-control sAge56 translitSource" data-trans="val-'+obj.id+'" value="'+obj.age56+'" />';
                html += '</td>';
                html += '<td class="align-center">';
                html += '<input type="text" class="form-control sAge67 translitSource" data-trans="val-'+obj.id+'" value="'+obj.age67+'" />';
                html += '</td>';
                html += '<td class="align-center">';
                html += '<input type="text" class="form-control sAge78 translitSource" data-trans="val-'+obj.id+'" value="'+obj.age78+'" />';
                html += '</td>';
                html += '<td class="align-center">';
                html += '<input type="text" class="form-control sAge89 translitSource" data-trans="val-'+obj.id+'" value="'+obj.age89+'" />';
                html += '</td>';
                html += '<td class="align-center">';
                html += '<input type="text" class="form-control sAge910 translitSource" data-trans="val-'+obj.id+'" value="'+obj.age910+'" />';
                html += '</td>';
                html += '<td class="align-center">';
                html += '<input type="text" class="form-control sAge1011 translitSource" data-trans="val-'+obj.id+'" value="'+obj.age1011+'" />';
                html += '</td>';
                html += '<td class="align-center">';
                html += '<input type="text" class="form-control sAge1112 translitSource" data-trans="val-'+obj.id+'" value="'+obj.age1112+'" />';
                html += '</td>';
                html += '<td class="align-center">';
                html += '<a title="<?php echo __('Сохранить изменения'); ?>" href="#" class="sSave btn btn-xs bs-tooltip liTipLink" style="white-space: nowrap; margin-top: 7px;"><i class="fa fa-fixed-width">&#xf0c7;</i></a>';
                html += '<a title="<?php echo __('Удалить'); ?>" href="#" class="sDelete btn btn-xs bs-tooltip liTipLink" style="white-space: nowrap; margin-top: 7px;"><i class="fa fa-trash-o"></i></a>';
                html += '</td>';
                html += '</tr>';
                return html;
            };
            // Add a row
            $('#sSave').on('click', function(e){
                e.preventDefault();
                loader($('#sValuesList'), 1);
                var label = $('#sLabel').val();
                var age01 = $('#sAge01').val();
                var age12 = $('#sAge12').val();
                var age23 = $('#sAge23').val();
                var age34 = $('#sAge34').val();
                var age45 = $('#sAge45').val();
                var age56 = $('#sAge56').val();
                var age67 = $('#sAge67').val();
                var age78 = $('#sAge78').val();
                var age89 = $('#sAge89').val();
                var age910 = $('#sAge910').val();
                var age1011 = $('#sAge1011').val();
                var age1112 = $('#sAge1112').val();


                $.ajax({
                    url: '/wezom/ajax/catalog/addSizeTableValue',
                    type: 'POST',
                    dataType: 'JSON',
                    data: {
                        catalog_id: sID,
                        label: label,
                        age01: age01,
                        age12: age12,
                        age23: age23,
                        age34: age34,
                        age45: age45,
                        age56: age56,
                        age67: age67,
                        age78: age78,
                        age89: age89,
                        age910: age910,
                        age1011: age1011,
                        age1112: age1112
                    },
                    success: function(data){
                        if( data.success ) {
                            var html = '';
                            if( data.result.length ) {
                                for( var i = 0; i < data.result.length; i++ ) {
                                    html += generateTR(data.result[i]);
                                }
                            }
                            $('#sValuesList tbody').html(html);
                            $('#sValuesList input[type=checkbox]').each(function(){ checkboxStart($(this)); });
                            $('#sValuesList input[type=checkbox]').on('change',function(){ checkboxStart($(this)); });
                            $('#sLabel').val(1);
                            $('#sAge01').val('');
                            $('#sAge12').val('');
                            $('#sAge23').val('');
                            $('#sAge34').val('');
                            $('#sAge45').val('');
                            $('#sAge56').val('');
                            $('#sAge67').val('');
                            $('#sAge78').val('');
                            $('#sAge89').val('');
                            $('#sAge910').val('');
                            $('#sAge1011').val('');
                            $('#sAge1112').val('');
                        } else {
                            generate(data.error, 'warning');
                        }
                        loader($('#sValuesList'), 0);
                    }
                });


            });
            // Save a row
            $('#sValuesList').on('click', '.sSave', function(e){
                e.preventDefault();
                loader($('#sValuesList'), 1);
                var tr = $(this).closest('tr');
                var id = tr.data('id');
                var label = tr.find('.sLabel').val();
                var age01 = tr.find('.sLabel').val();
                var age12 = tr.find('.sAge01').val();
                var age23 = tr.find('.sAge12').val();
                var age34 = tr.find('.sAge23').val();
                var age45 = tr.find('.sAge34').val();
                var age56 = tr.find('.sAge45').val();
                var age67 = tr.find('.sAge56').val();
                var age78 = tr.find('.sAge67').val();
                var age89 = tr.find('.sAge78').val();
                var age910 = tr.find('.sAge89').val();
                var age1011 = tr.find('.sAge910').val();
                var age1112 = tr.find('.sAge1011').val();

                $.ajax({
                    url: '/wezom/ajax/catalog/editSizeTableValue',
                    type: 'POST',
                    dataType: 'JSON',
                    data: {
                        catalog_id: sID,
                        label: label,
                        age01: age01,
                        age12: age12,
                        age23: age23,
                        age34: age34,
                        age45: age45,
                        age56: age56,
                        age67: age67,
                        age78: age78,
                        age89: age89,
                        age910: age910,
                        age1011: age1011,
                        age1112: age1112,
                        id: id
                    },
                    success: function(data){
                        if( data.success ) {
                            var html = '';
                            if( data.result.length ) {
                                for( var i = 0; i < data.result.length; i++ ) {
                                    html += generateTR(data.result[i]);
                                }
                            }
                            $('#sValuesList tbody').html(html);
                            $('#sValuesList input[type=checkbox]').each(function(){ checkboxStart($(this)); });
                            $('#sValuesList input[type=checkbox]').on('change',function(){ checkboxStart($(this)); });
                        } else {
                            generate(data.error, 'warning');
                        }
                        loader($('#sValuesList'), 0);
                    }
                });
            });
            // Delete a row
            $('#sValuesList').on('click', '.sDelete', function(e){
                e.preventDefault();
                loader($('#sValuesList'), 1);
                var tr = $(this).closest('tr');
                var id = tr.data('id');
                $.ajax({
                    url: '/wezom/ajax/catalog/deleteSizeTableValue',
                    type: 'POST',
                    dataType: 'JSON',
                    data: {
                        id: id
                    },
                    success: function(data){
                        if( data.success ) {
                            tr.remove();
                        } else {
                            generate(data.error, 'warning');
                        }
                        loader($('#sValuesList'), 0);
                    }
                });
            });
        }
    });
</script>