<?php echo \Forms\Builder::open(); ?>
    <div class="form-actions" style="display: none;">
        <?php echo \Forms\Form::submit(['name' => 'name', 'value' => __('Отправить'), 'class' => 'submit btn btn-primary pull-right']); ?>
    </div>
    <div class="col-md-12">
        <div class="widget">
            <div class="widgetContent">
                <div class="form-vertical row-border">
                    <ul class="liTabs t_wrap">
                        <?php foreach ($languages as $key => $lang): ?>
                            <?php $public = \Core\Arr::get($langs, $key, array()); ?>
                            <?php echo $lang['default'] == 1 ? '<input type="hidden" class="default_lang" value="' . $lang['name'] . '">' : ''; ?>
                            <li class="t_item">
                                <a class="t_link" href="#"><?php echo $lang['name']; ?></a>
                                <div class="t_content">
                                    <div class="form-group">
                                        <?php echo \Forms\Builder::input([
                                            'name' => 'FORM[' . $key . '][name]',
                                            'value' => $public->name,
                                            'class' => ['valid', $lang['default'] == 1 ? 'translitSource' : ''],
                                        ], __('Название')); ?>
                                    </div>
                                </div>
                            </li>
                        <?php endforeach; ?>
                    </ul>

                    <div class="form-group">
                        <?php echo \Forms\Builder::bool($obj ? $obj->status : 1); ?>
                    </div>
                    <div class="form-group" style="display: none">
						<?php echo \Forms\Builder::bool($obj ? $obj->visible : 0, 'visible', 'Выводить в главном меню'); ?>
                    </div>
                    <div class="form-group">
                        <?php echo \Forms\Builder::select($menus,
                            $obj->menu, [
                                'name' => 'FORM[menu]',
                                'class' => ['valid'],
                            ], __('Категория меню')); ?>
                    </div>
                    <div class="form-group">
                        <?php echo \Forms\Builder::input([
                            'name' => 'FORM[code]',
                            'value' => $obj->code,
                            'class' => ['valid'],
                        ], __('Код возрастной группы')); ?>
                    </div>
                    <div class="form-group">
                        <?php echo \Forms\Builder::alias([
                            'name' => 'FORM[alias]',
                            'value' => $obj->alias,
                            'class' => 'valid',
                        ], [
                            'text' => __('Алиас'),
                            'tooltip' => __('<b>Алиас (англ. alias - псевдоним)</b><br>Алиасы используются для короткого именования страниц. <br>Предположим, имеется страница с псевдонимом «<b>about</b>». Тогда для вывода этой страницы можно использовать или полную форму: <br><b>http://domain/?go=frontend&page=about</b><br>или сокращенную: <br><b>http://domain/about.html</b>'),
                        ],['data-rules' => 'only_chars_and_numbers']);
                        ?>
                    </div>
                    <div class="form-group multiSelectBlock">
                        <?php echo \Forms\Builder::select(\Core\Support::selectData($groups, 'id', 'name'), $groups_on, [
                            'name' => 'group_age[]',
                            'multiple' => 'multiple',
                        ], __('Отображать в ')); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php echo \Forms\Form::close(); ?>
