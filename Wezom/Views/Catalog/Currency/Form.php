<?php echo \Forms\Builder::open(); ?>
<div class="form-actions" style="display: none;">
	<?php echo \Forms\Form::submit(['name' => 'name', 'value' => 'Отправить', 'class' => 'submit btn btn-primary pull-right']); ?>
</div>
<div class="col-md-7">
    <div class="widget box">
        <div class="widgetHeader">
            <div class="widgetTitle">
                <i class="fa fa-reorder"></i>
				<?php echo __('Основные данные'); ?>
            </div>
        </div>
        <div class="widgetContent">
            <div class="form-vertical row-border">
                <ul class="liTabs t_wrap">
                    <li class="t_item">
                        <a class="t_link" href="#"><?php echo __('Основные данные'); ?></a>
                        <div class="t_content">
                            <div class="form-group">
								<?php echo \Forms\Builder::input([
									'name' => 'FORM[currency]',
									'value' => $obj->currency,
									'class' => ['valid'],
								], __('Название')); ?>
                            </div>
                            <div class="form-group">
								<?php echo \Forms\Builder::input([
									'name' => 'FORM[course]',
									'value' => $obj->course,
									'class' => 'valid',
								])?>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<?php echo \Forms\Form::close(); ?>
