<?php echo \Forms\Builder::open(); ?>
    <div class="form-actions" style="display: none;">
        <?php echo \Forms\Form::submit(['name' => 'name', 'value' => 'Отправить', 'class' => 'submit btn btn-primary pull-right']); ?>
    </div>
    <div class="col-md-8">
        <div class="widget box">
            <div class="widgetContent">
                <div class="form-vertical row-border">
                    <ul class="liTabs t_wrap">
                        <?php foreach ($languages as $key => $lang): ?>
                            <?php $public = \Core\Arr::get($langs, $key, array()); ?>
                            <?php echo $lang['default'] == 1 ? '<input type="hidden" class="default_lang" value="' . $lang['name'] . '">' : ''; ?>
                            <li class="t_item">
                                <a class="t_link" href="#"><?php echo $lang['name']; ?></a>
                                <div class="t_content">
                                    <div class="form-group">
                                        <?php echo \Forms\Builder::input([
                                            'name' => 'FORM[' . $key . '][name]',
                                            'value' => $public->name,
                                            'class' => ['valid', $lang['default'] == 1 ? 'translitSource' : ''],
                                        ], __('Название')); ?>
                                    </div>
                                    <div class="form-group">
                                        <?php echo \Forms\Builder::tiny([
                                            'name' => 'FORM[' . $key . '][description]',
                                            'value' => $public->description,
                                        ],
                                            [
                                                'text' =>_('Описание'),
                                                'tooltip' => __('Описание будет отображаться только у нижнего регистра групп'),
                                            ])
                                        ?>
                                    </div>
                                </div>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="widget box">
            <div class="widgetContent">
                        <div class="form-group">
                            <?php echo \Forms\Builder::bool($obj ? $obj->status : 1); ?>
                        </div>
                        <div class="form-group">
                            <?php echo \Forms\Builder::select('<option value="0">' . __('Вехний уровень') . '</option>'.$tree,
                                $obj->parent_id, [
                                    'name' => 'FORM[parent_id]',
                                    'class' => 'valid',
                                ], __('Группа')); ?>
                        </div>
            </div>
        </div>
    </div>
<?php echo \Forms\Form::close(); ?>
