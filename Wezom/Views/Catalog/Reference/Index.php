<div class="rowSection">
    <div class="col-md-12">
        <div class="widget">
            <div class="widget">
                <div class="widgetContent">
                    <table class="table table-striped table-hover checkbox-wrap">
                        <thead>
                        <tr>
                            <th><?php echo __('Метка'); ?></th>
                            <th><?php echo __('Название'); ?></th>
                            <th class="nav-column textcenter">&nbsp;</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($result as $obj): ?>
                            <tr data-id="<?php echo $obj->id; ?>">
                                <td>
                                    <?php echo $obj->key; ?>
                                </td>
                                <td>
                                    <a href="/wezom/<?php echo Core\Route::controller(); ?>/edit/<?php echo $obj->id; ?>">
                                        <?php echo $obj->name; ?>
                                    </a>
                                </td>
                                <td class="nav-column">
                                    <a title="<?php echo __('Редактировать'); ?>"
                                       href="/wezom/<?php echo Core\Route::controller(); ?>/edit/<?php echo $obj->id; ?>"><i
                                                class="fa fa-pencil"></i> </a>
                                </td>
                            </tr>
                        <?php endforeach ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<span id="parameters" data-table="<?php echo $tablename; ?>"></span>