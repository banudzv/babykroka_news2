<?php use Core\HTML;
use Forms\Builder;
use Forms\Form;

echo Builder::open(); ?>
    <div class="form-actions" style="display: none;">
        <?php echo Form::submit(['name' => 'name', 'value' => __('Отправить'), 'class' => 'submit btn btn-primary pull-right']); ?>
    </div>
    <?php if(count(\Core\Arr::get($groups, 'left', []))): ?>
        <div class="col-md-<?php echo count(\Core\Arr::get($groups, 'right', [])) ? 12 : 12; ?>">
            <div class="widget">
                <div class="widgetContent">
                    <div class="form-vertical row-border">
                        <ul class="liTabs t_wrap">
                            <li class="t_item">
                                <a class="t_link" href="#"><?php echo __('Обмен'); ?></a>
                                <?php echo \Core\View::tpl(['scheduler' => $scheduler,'scheduler_config' => $scheduler_config,'scheduler_statuses' => $scheduler_statuses,'repeatTypes' => $repeatTypes,'configFTP' => $configFTP,'ftp_configs' => $ftp_configs],'Config/Centre');?>
                            </li>
                            <li class="t_item">
                                <a class="t_link" href="#"><?php echo __('Возрастные соответствия'); ?></a>
                                <div class="t_content agesContent">
                                    <?php echo \Core\View::tpl(['ages' => $ages],'Config/Ages');?>
                                </div>
                            </li>
                            <li class="t_item">
                                <a class="t_link" href="#"><?php echo __('PromUa'); ?></a>
                                <div class="t_content prom">
                                    <?php echo \Core\View::tpl(['prom' => $prom_info],'Config/Prom');?>
                                </div>
                            </li>
                            <?php foreach($groups['left'] AS $group): ?>
                                <?php if(\Core\Arr::get($result, $group->alias)): ?>
                                    <?php if ($group->alias == 'seasons' && !empty($specs)):?>
                                        <li class="t_item">
                                            <a class="t_link" href="#"><?php echo __($group->name); ?></a>
                                            <div class="t_content <?php echo $group->alias?>Content">
                                                <?php foreach ($result[$group->alias] as $obj): ?>
                                                    <?php echo \Core\View::tpl(['obj' => $obj], 'Config/Row'); ?>
                                                <?php endforeach; ?>
                                                <?php echo \Core\View::tpl(['specs' => $specs],'Config/Seasons');?>
                                            </div>
                                        </li>
                                    <?php else:?>
                                        <li class="t_item">
                                            <a class="t_link" href="#"><?php echo __($group->name); ?></a>
                                            <div class="t_content <?php echo $group->alias?>Content">
                                                <?php foreach ($result[$group->alias] as $obj): ?>
                                                    <?php echo \Core\View::tpl(['obj' => $obj], 'Config/Row'); ?>
                                                <?php endforeach; ?>
                                            </div>
                                        </li>
                                    <?php endif;?>

                                <?php endif; ?>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <?php if(count(\Core\Arr::get($groups, 'right', []))): ?>
        <div class="col-md-<?php echo count(\Core\Arr::get($groups, 'left', [])) ? 12 : 12; ?>">
            <div class="widget">
                <div class="widgetContent">
                    <div class="form-vertical row-border">
                        <ul class="liTabs t_wrap">
                            <?php foreach($groups['right'] AS $group): ?>
                                <?php if(\Core\Arr::get($result, $group->alias)): ?>
                                    <li class="t_item">
                                        <a class="t_link" href="#"><?php echo __($group->name); ?></a>
                                        <div class="t_content <?php echo $group->alias?>Content">
                                            <?php foreach ($result[$group->alias] as $obj): ?>
                                                <?php echo \Core\View::tpl(['obj' => $obj], 'Config/Row'); ?>
                                            <?php endforeach; ?>
                                        </div>
                                    </li>
                                <?php endif; ?>
                            <?php endforeach; ?>
                            <li class="t_item">
                                <a class="t_link" href="#"><?php echo __('Тестовое письмо'); ?></a>
                                <div class="t_content">
                                    <div class="sendTestEmail" style="padding-top: 0;" data-ajax="config/testEmail">
                                        <div class="red hide mailNotice"><?php echo __('Пожалуйста, сохраните настройки почты перед отправкой тестового письма'); ?></div>
                                        <div class="form-group">
                                            <?php echo Builder::input([
                                                'name' => 'title'
                                            ], __('Заголовок письма')); ?>
                                        </div>
                                        <div class="form-group">
                                            <?php echo Builder::input([
                                                'name' => 'body'
                                            ], __('Тело письма')); ?>
                                        </div>
                                        <div class="form-group">
                                            <?php echo Builder::input([
                                                'name' => 'email'
                                            ], __('E-mail получателя')); ?>
                                        </div>
                                        <div class="textright">
                                            <?php echo Form::button(__('Отправить'), ['type' => 'button', 'class' => 'btn btn-primary']); ?>
                                        </div>
                                    </div>

                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
<?php echo Form::close(); ?>

<script>
    $(function(){
        var input;
        $('input[type="password"]').closest('div').addClass('input-group');
        $('.showPassword').on('click', function(){
            input = $(this).closest('div.input-group').find('input');
            if(input.attr('type') == 'password') {
                input.attr('type', 'text');
                $(this).text('Скрыть');
            } else {
                input.attr('type', 'password');
                $(this).text('Показать');
            }
        });

        $('.seasonsContent button').on('click', function () {
            preloader();
            var it = $(this);
            var form = it.closest('.seasonsContent');
            var action = form.data('ajax');
            let arr = {};
            Array.from($('.form-season-sort')).forEach(
                function(element, index, array) {
                    arr[element.name] = element.value
                }
            );
            if (action) {
                $.ajax({
                    url: '/wezom/ajax/' + action,
                    type: 'POST',
                    dataType: 'JSON',
                    data: {
                        start: $('input[id=f_seasons-season_start]').val(),
                        finish: $('input[id=f_seasons-season_end]').val(),
                        priority: arr,
                    },
                    success: function (data) {
                        console.log(data);
                        if (data.success) {
                            if (data.response) {
                                generate(data.response, 'success');
                            }
                            if (data.reload) {
                                window.location.reload();
                            } else {
                                preloader();
                            }
                        } else {
                            if (data.response) {
                                generate(data.response, 'warning');
                            }
                            preloader();
                        }
                    }
                });
            }
        });

        $('.agesContent button').on('click', function () {
            preloader();
            let it = $(this);
            let form = it.closest('.agesContent');
            let action = form.data('ajax');
            let arr = {};
            Array.from($('.form-ages-code')).forEach(
                function(element, index, array) {
                    arr[element.name] = element.value
                }
            );
            if (action) {
                $.ajax({
                    url: '/wezom/ajax/' + action,
                    type: 'POST',
                    dataType: 'JSON',
                    data: {
                        ages: arr,
                    },
                    success: function (data) {
                        console.log(data);
                        if (data.success) {
                            if (data.response) {
                                generate(data.response, 'success');
                            }
                            if (data.reload) {
                                window.location.reload();
                            } else {
                                preloader();
                            }
                        } else {
                            if (data.response) {
                                generate(data.response, 'warning');
                            }
                            preloader();
                        }
                    }
                });
            }
        });

        $('.prom button').on('click', function () {
            preloader();
            let it = $(this);
            let form = it.closest('.prom');
            let action = form.data('ajax');
            let arr = {};

            if (action) {
                $.ajax({
                    url: '/wezom/ajax/' + action,
                    type: 'POST',
                    dataType: 'JSON',
                    data: {
                        download_url: form.find('input[name=download_url]').val(),
                        youtube_link: form.find('input[name=youtube_link]').val(),
                        url_file: form.find('input[name=url_file]').val(),
                        name_shop: form.find('input[name=name_shop]').val(),
                        products: form.find('input[name=products]').val(),
                        products_filter: form.find('input[name=products_filter]').val(),
                        products_catalog: form.find('input[name=products_catalog]').val(),
                        products_prices: form.find('input[name=products_prices]').val(),
                        button_save: form.find('input[name=button_save]').val(),
                    },
                    success: function (data) {
                        console.log(data);
                        if (data.success) {
                            if (data.response) {
                                generate(data.response, 'success');
                            }
                            if (data.reload) {
                                window.location.reload();
                            } else {
                                preloader();
                            }
                        } else {
                            if (data.response) {
                                generate(data.response, 'warning');
                            }
                            preloader();
                        }
                    }
                });
            }
        });


        $('.sendTestEmail button').on('click', function () {
            preloader();
            var it = $(this);
            var form = it.closest('.sendTestEmail');
            var action = form.data('ajax');
            if (action) {
                $.ajax({
                    url: '/wezom/ajax/' + action,
                    type: 'POST',
                    dataType: 'JSON',
                    data: {
                        title: form.find('input[name=title]').val(),
                        body: form.find('input[name=body]').val(),
                        email: form.find('input[name=email]').val(),
                    },
                    success: function (data) {
                        console.log(data);
                        if (data.success) {
                            if (data.response) {
                                generate(data.response, 'success');
                            }
                            if (data.reload) {
                                window.location.reload();
                            } else {
                                preloader();
                            }
                        } else {
                            if (data.response) {
                                generate(data.response, 'warning');
                            }
                            preloader();
                        }
                    }
                });
            }
        });

        $('.mailContent').on('change', 'input', function(){
            $('.mailNotice').show();
        });

        $('.exec').on('click',function (){
            let selectedVal = $('#cronName option:selected');
            let action = selectedVal.data('action');
            preloader();
            $.ajax({
                url: '/wezom/cron/' + action,
                type: 'POST',
                dataType: 'JSON',
                success: function (data) {
                    if (data.success) {
                        if (data.response) {
                            generate(data.response, 'success');
                        }
                        if (data.reload) {
                            window.location.reload();
                        }
                    } else {
                        if (data.response) {
                            generate(data.response, 'warning');
                        }
                    }
                    preloader();
                },
                error: function (data) {
                    generate(data.response, 'warning', 5000);
                    preloader();
                }
            });
        });

        $('.exec-regulation').on('click',function (){
            let selectedVal = $('#cronName option:selected');
            let action = selectedVal.data('action')+'Action';
            let regulation = $('.regulations[data-val='+selectedVal.val()+']');
            let start_time = regulation.find('input[name=start_time]').val();
            let finish_time = regulation.find('input[name=finish_time]').val();
            let count = regulation.find('input[name=repeat]').val();
            let interval = regulation.find('input[name=interval]').val();
            let interval_type = regulation.find('select[name=interval-type] option:selected').val();
            preloader();
            $.ajax({
                url: '/wezom/job/saveJob',
                type: 'POST',
                dataType: 'JSON',
                data: {
                    start_time:start_time,
                    finish_time:finish_time,
                    count:count,
                    interval:interval,
                    interval_type:interval_type,
                    action:action
                },
                success: function (data) {
                    if (data.success) {
                        if (data.response) {
                            generate(data.response, 'success');
                        }
                        if (data.reload) {
                            window.location.reload();
                        } else {
                            preloader();
                        }
                    } else {
                        if (data.response) {
                            generate(data.response, 'warning');
                        }
                        preloader();
                    }
                }
            });
        });
        $('.js-change-ftp-setting').on('click', function (){
            let selectedVal = $('#cronName option:selected').val();
            $('.settingFTP').hide();
            $('.regulations').hide();
            $('.settingFTP[data-val='+selectedVal+']').show();
        });

        $('.js-test-ftp-connection').on('click', function (){
            let selectedVal = $('#cronName option:selected').val();
            let index = 'ACTIONS['+selectedVal+']';
            preloader();
            $.ajax({
                url: '/wezom/ajax/config/testConnection',
                type: 'POST',
                dataType: 'JSON',
                data: {
                    selectedVal:selectedVal,
                },
                success: function (data) {
                    if (data.success) {
                        if (data.response) {
                            generate(data.response, 'success');
                        }
                        if (data.reload) {
                            window.location.reload();
                        }
                    } else {
                        if (data.response) {
                            generate(data.response, 'warning');
                        }
                    }
                    preloader();
                },
                error: function () {
                    generate('Произошла ошибка выгрузки!', 'warning', 5000);
                    preloader();
            }
            });
        });

        $('.js-close-ftp-connection').on('click', function (){
            let selectedVal = $('#cronName option:selected').val();
            preloader();
            $.ajax({
                url: '/wezom/ajax/config/closeConnection',
                type: 'POST',
                dataType: 'JSON',
                data: {
                    selectedVal:selectedVal,
                },
                success: function (data) {
                    if (data.success) {
                        if (data.response) {
                            generate(data.response, 'success');
                        }
                        if (data.reload) {
                            window.location.reload();
                        }
                    } else {
                        if (data.response) {
                            generate(data.response, 'warning');
                        }
                    }
                    preloader();
                },
                error: function () {
                    generate('Произошла ошибка выгрузки!', 'warning', 5000);
                    preloader();
                }
            });
        });

        $('.js-change-ftp-settings').on('click', function (){
            let selectedVal = $('#cronName option:selected').val();
            let index = 'ACTIONS['+selectedVal+']';
            let ftp_address = $("input[name='"+index+"[ftp_address]']").val();
            let ftp_username = $("input[name='"+index+"[ftp_username]']").val();
            let ftp_password = $("input[name='"+index+"[ftp_password]']").val();
            let ftp_port = $("input[name='"+index+"[ftp_port]']").val();
            let ftp_connection = $("input[name='"+index+"[active]']").val();
            if ($('.t_content.ftp').css('display', 'block')) {
                var activeGroupFtpId = $('.t_content.ftp').find("#active-group-ftp-id:checked").val();
            }

            //let ftp_connection = $('#f_individual-ftp_connection:checked').val();
            preloader();
            $.ajax({
                url: '/wezom/ajax/config/updateFtpSettings',
                type: 'POST',
                dataType: 'JSON',
                data: {
                    selectedVal:selectedVal,
                    ftp_address:ftp_address,
                    ftp_username:ftp_username,
                    ftp_password:ftp_password,
                    ftp_connection:ftp_connection,
                    ftp_port:ftp_port,
                    ftp_activeGroup:activeGroupFtpId
                },
                success: function (data) {
                    if (data.success) {
                        if (data.response) {
                            generate(data.response, 'success');
                        }
                        if (data.reload) {
                            window.location.reload();
                        } else {
                            preloader();
                        }
                    } else {
                        if (data.response) {
                            generate(data.response, 'warning');
                        }
                        preloader();
                    }
                }
            });
        });

        $('.individual-ftp_connection').on('click', function (){
            let ftp_connection = $('#f_individual-ftp_connection:checked').val();
            preloader();
            $.ajax({
                url: '/wezom/ajax/config/changeFtpConnection',
                type: 'POST',
                dataType: 'JSON',
                data: {
                    ftp_connection:ftp_connection,
                },
                success: function (data) {
                    if (data.success) {
                        if (data.response) {
                            generate(data.response, 'success');
                        }
                        if (data.reload) {
                            window.location.reload();
                        } else {
                            preloader();
                        }
                    } else {
                        if (data.response) {
                            generate(data.response, 'warning');
                        }
                        preloader();
                    }
                }
            });
        });

        $('.js-ftp-settings-save').on('click', function (){

        })

        $('.js-change-regulations').on('click', function (){
            let selectedVal = $('#cronName option:selected').val();
            $('.settingFTP').hide();
            $('.regulations').hide();
            $('.regulations[data-val='+selectedVal+']').show();
        })

        $('#cronName').on('change', function (){
            $('.settingFTP').hide();
            $('.regulations').hide();
        })

        $('.myPickerWithTime').datetimepicker({
            timepicker:true,
            lang:'ru',
            format:'Y-m-d H:i:s'
        });

        $('.js-copy-text').on('click', function (e) {
            e.preventDefault();
            let selectedVal = $('#cronName option:selected');
            let link = selectedVal.data('path');
            if(link.length > 0 ){
                let textArea = document.createElement("textarea");
                textArea.value = link;
                document.body.appendChild(textArea);
                textArea.select();
                document.execCommand("copy");
                textArea.remove();
                generate('Ссылка скопирована!', 'success', 5000);
            }else{
                generate('Ссылка для данного узла отсутствует!', 'warning', 5000);
            }

        });

    });
</script>
