<?php use Forms\Builder;
use Forms\Form;

?>
<div class="prom" style="padding-top: 0;" data-ajax="config/updatePromInfo">
    <div class="form-group">
        <?php echo Builder::input([
            'name' => 'download_url',
            'value' => $prom->download_url,
        ], __('Начальная ссылка')); ?>
    </div>
    <div class="form-group">
        <?php echo Builder::input([
            'name' => 'youtube_link',
            'value' => $prom->youtube_link,
        ], __('Youtube ссылка')); ?>
    </div>

    <div class="form-group">
        <?php echo Builder::input([
            'name' => 'url_file',
            'value' => $prom->url_file,
        ], __('Информация о пути к файлу')); ?>
    </div>
    <div class="form-group">
        <?php echo Builder::input([
            'name' => 'name_shop',
            'value' => $prom->name_shop,
        ], __('Информация о названию магазина / компании')); ?>
    </div>
    <div class="form-group">
        <?php echo Builder::input([
            'name' => 'products',
            'value' => $prom->products,
        ], __('Информация о разделе товаров')); ?>
    </div>
    <div class="form-group">
        <?php echo Builder::input([
            'name' => 'products_filter',
            'value' => $prom->products_filter,
        ], __('Информация о фильтре товаров')); ?>
    </div>
    <div class="form-group">
        <?php echo Builder::input([
            'name' => 'products_catalog',
            'value' => $prom->products_catalog,
        ], __('Информация о каталоге')); ?>
    </div>
    <div class="form-group">
        <?php echo Builder::input([
            'name' => 'products_prices',
            'value' => $prom->products_prices,
        ], __('Информация о регулировке цен')); ?>
    </div>
    <div class="form-group">
        <?php echo Builder::input([
            'name' => 'button_save',
            'value' => $prom->button_save,
        ], __('Информация о кнопке создания шаблона выгрузки')); ?>
    </div>

    <div class="textright">
        <?php echo Form::button(__('Сохранить'), ['type' => 'button', 'class' => 'btn btn-primary']); ?>
    </div>
</div>
