<?php use Forms\Builder;
use Forms\Form;
?>
<div class="agesContent" style="padding-top: 0;" data-ajax="config/updateAges">
    <?php foreach ($ages as $age):?>
        <div class="form-group">
            <?php echo Builder::input([
                'disabled' => 'disabled',
                'value' => $age->name,
            ], __('Наименование')); ?>
        </div>
        <div class="form-group">
            <?php echo Builder::input([
                'name' => $age->id,
                'class' => 'form-ages-code',
                'value' => $age->code,
            ], __('Код')); ?>
        </div>
    <?php endforeach;?>
    <div class="textright">
        <?php echo Form::button(__('Сохранить'), ['type' => 'button', 'class' => 'btn btn-primary']); ?>
    </div>
</div>
