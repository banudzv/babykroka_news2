<?php use Forms\Builder;
use Forms\Form;

;?>
<div class="t_content ftp">
    <div class="cronSettings">
        <div class="form-group">
            <label class="control-label" for="cronName">Узел выгрузки цен<span class="r"></label>
            <select id="cronName" name="cronName" value="cronName" rows="4" class="form-control" data-name="cronName">
                <option data-path="<?php echo HOST.DS."cron/InformationReferences.xml";?>" data-action="updateInformationReferencesStart" value="0" selected="selected"><?php echo __('InformationReferences');?></option>
                <option data-path="<?php echo HOST.DS."cron/PriceData.xml";?>" data-action="updatePriceDataStart" value="1"><?php echo __('updatePriceData');?></option>
                <option data-path="<?php echo HOST.DS."cron/ProdBalance.xml";?>" data-action="updateBalanceStart" value="2"><?php echo __('updateBalance');?></option>
                <option data-path="<?php echo HOST.DS."cron/cources.xml";?>" data-action="updateCurrenciesStart" value="3"><?php echo __('updateCurrencies');?></option>
                <option data-path="<?= HOST . DS . "feed/feed.xml" ?>" data-action="updateFeedData" value="5">
                    <?= __('updateFeedData') ?>
                </option>
                <option data-path="" data-action="uploadImagesFromFolderStart" value="4"><?php echo __('uploadImagesFromFolder');?></option>
                <option data-path="" data-action="updateProm" value="6"><?php echo __('updateProm');?></option>
            </select>
        </div>

        <div class="btn btn-primary js-change-ftp-setting" style="margin-bottom: 15px;">Изменить настройки FTP</div>
        <div class="btn btn-primary js-change-regulations" style="margin-bottom: 15px;">Изменить регламентное задание</div>
        <div class="btn btn-primary js-copy-text" style="margin-bottom: 15px;">Скопировать путь</div>
        <?php echo Form::button(__('Выполнить сейчас'), ['type' => 'button', 'class' => 'btn btn-primary exec','style' => "margin-bottom: 15px;"]); ?>


        <?php for ($i = 0, $iMax = count($scheduler_config); $i< $iMax; $i++):?>
            <div class="regulations" data-val="<?php echo $i;?>" style="display: none">
                <div class="form-group">
                    <?php echo Builder::input([
                        'name' => 'start_time',
                        'value' => $scheduler[$scheduler_config[$i]]->start_time ? date('Y-m-d H:i:s', $scheduler[$scheduler_config[$i]]->start_time) : NULL,
                        'class' => 'myPickerWithTime valid',
                        'autocomplete' => 'off',
                    ], __('Дата начала')); ?>
                </div>
                <div class="form-group">
                    <?php echo Builder::input([
                        'name' => 'finish_time',
                        'value' => $scheduler[$scheduler_config[$i]]->finish_time ? date('Y-m-d H:i:s', $scheduler[$scheduler_config[$i]]->finish_time) : NULL,
                        'class' => 'myPickerWithTime valid',
                        'autocomplete' => 'off',
                    ], __('Дата окончания')); ?>
                </div>
                <div class="form-group">
                    <?php echo Builder::input([
                        'name' => 'repeat',
                        'value' => (int)$scheduler[$scheduler_config[$i]]->count,
                        'class' => 'form-control',
                        'autocomplete' => 'off',
                        'id' => 'repeat',
                        'type' => 'number',
                        'min' => 0,
                    ], __('Кол-во повторений в день')); ?>
                </div>
                <div class="form-group input-group">
                    <?php echo Builder::input([
                        'name' => 'interval',
                        'class' => 'form-control',
                        'value' => (int)$scheduler[$scheduler_config[$i]]->interval ?: NULL,
                        'autocomplete' => 'off',
                        'id' => 'interval',
                    ], __('Интервал повторений')); ?>
                    <span class="input-group-btn" style="vertical-align: bottom;">
                    <?php echo Builder::select($repeatTypes, (int)$scheduler[$scheduler_config[$i]]->interval_type, ['name' => 'interval-type'],__('Интервал')); ?>
                    </span>
                </div>
                <div class="textright">
                    <?php echo Form::button(__('Сохранить настройки'), ['type' => 'button', 'class' => 'btn btn-primary exec-regulation']); ?>
                </div>
            </div>
        <?php endfor;?>
        <?php for ($i = 0, $iMax = count($scheduler_config); $i< $iMax; $i++):?>
            <div class="settingFTP" data-val="<?php echo $i;?>" <?php echo $i == 0 ? NULL : 'style="display: none"';?>>

                <?php if ($ftp_configs[$scheduler_config[$i]]->action != 'uploadImagesFromFolderAction'):?>
                <div class="form-group">
                    <?php echo Builder::input([
                        'name' => 'ACTIONS['.$i.'][ftp_address]',
                        'value' => isset($ftp_configs[$scheduler_config[$i]]) ? $ftp_configs[$scheduler_config[$i]]->ftp_address : NULL,
                    ], __('Адрес FTP')); ?>
                </div>
                <div class="form-group">
                    <?php echo Builder::input([
                        'name' => 'ACTIONS['.$i.'][ftp_username]',
                        'value' => isset($ftp_configs[$scheduler_config[$i]]) ? $ftp_configs[$scheduler_config[$i]]->ftp_username: NULL,
                    ], __('Пользователь FTP')); ?>
                </div>
                <div class="form-group">
                    <?php echo Builder::input([
                        'name' => 'ACTIONS['.$i.'][ftp_password]',
                        'value' => isset($ftp_configs[$scheduler_config[$i]]) ? $ftp_configs[$scheduler_config[$i]]->ftp_password: NULL,
                    ], __('Пароль FTP')); ?>
                </div>
                <div class="form-group">
                    <?php echo Builder::input([
                        'name' => 'ACTIONS['.$i.'][ftp_port]',
                        'value' => isset($ftp_configs[$scheduler_config[$i]]) ? $ftp_configs[$scheduler_config[$i]]->ftp_port: NULL,
                    ], __('Порт FTP')); ?>
                </div>
                <div class="form-group">
                    <?php echo Form::label(__('Тип подключения FTP')); ?>
                    <div class="clear"></div>
                    <?php $values = json_decode('[{"key":"Локальный сервер","value":"0"},{"key":"Внешний сервер","value":"1"}]	', true); ?>

                    <?php foreach($values AS $v): ?>
                    <?php $attributes = [
                        'name' => 'ftp_active',
                        'rows' => 5,
                        'class' => 'active-group-ftp',
                        'value' => $v['value'],
                        'id' => 'active-group-ftp-id'
                    ];?>
                        <?php $attr = $attributes;
                        $class = null;
                        $checked = false;
                        if ($v['value'] == $ftp_configs[$scheduler_config[$i]]->active){
                            $class = 'checked';
                            $checked = true;
                        }

                        ?>
                        <label class="checkerWrap-inline radioWrap col-md-4 11 <?php echo $class; ?>" style="margin-right: 0;">
                            <?php $attr['value']  ?>
                            <?php echo Builder::radio($checked, $attr);?>
                            <?php echo $v['key']; ?>
                        </label>
                    <?php endforeach; ?>
                </div>
                <div class="textright">
                    <?php echo Form::button(__('Сохранить настройки'), ['type' => 'button', 'class' => 'btn btn-primary js-change-ftp-settings']); ?>
                    <?php echo Form::button(__('Проверить соединение'), ['type' => 'button', 'class' => 'btn btn-primary js-test-ftp-connection']); ?>
                    <?php echo Form::button(__('Отключить принудительно задачу'), ['type' => 'button', 'class' => 'btn btn-primary js-close-ftp-connection']); ?>
                </div>
                <?php else:?>
                    <div class="form-group">
                        <?php echo Form::button(__('Отключить принудительно задачу'), ['type' => 'button', 'class' => 'btn btn-primary js-close-ftp-connection']); ?>
                    </div>
                <?php endif;?>
        </div>
        <?php endfor;?>


        <div class="cron-title">Состояние</div>
        <?php for ($i=0; $i <= 6; $i ++):?>
            <?php if(isset($scheduler_statuses[$scheduler[$scheduler_config[$i]]->status])):?>
                <div class="cron">
                    <div class="cron__name">
                        <?php echo __($scheduler_config[$i]);?>
                    </div>
                    <div class="cron__name">
                        <?php echo $scheduler[$scheduler_config[$i]]->in_queue ? 'Выполнение':'Нет в очереди';?>
                    </div>
                    <div class="cron-status <?php echo $scheduler_statuses[$scheduler[$scheduler_config[$i]]->status]['className'];?>">
                        <?php echo $scheduler_statuses[$scheduler[$scheduler_config[$i]]->status]['text'];?>
                    </div>
                    <?php if ($scheduler[$scheduler_config[$i]]->status == 2):?>
                        <div class="cron__time">
                            <?php echo date('Y-m-d H:i:s',$scheduler[$scheduler_config[$i]]->updated_at);?>
                        </div>
                    <?php endif;?>
                </div>
            <?php endif;?>
        <?php endfor;?>
</div>
