<?php use Forms\Builder;
use Forms\Form;
?>
<div class="seasonsContent" style="padding-top: 0;" data-ajax="config/setPrioritySeasons">
    <?php foreach ($specs as $in => $specification):?>
        <div class="form-group">
            <?php echo Builder::input([
                'name' => $specification->id,
                'disabled' => 'disabled',
                'class' => 'form-season-name',
                'value' => $specification->name,
            ], __('Сезон')); ?>
        </div>
        <div class="form-group">
            <?php echo Builder::input([
                'name' => $specification->id,
                'class' => 'form-season-sort',
                'value' => $specification->sort,
            ], __('Приоритет')); ?>
        </div>
    <?php endforeach;?>
    <div class="textright">
        <?php echo Form::button(__('Сохранить'), ['type' => 'button', 'class' => 'btn btn-primary']); ?>
    </div>
</div>
