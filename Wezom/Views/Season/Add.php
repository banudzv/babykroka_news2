<?php use Forms\Builder;
use Forms\Form;
$date_start = explode('-', $start);
$date_finish = explode('-', $finish);
echo Builder::open(); ?>
<div class="form-actions" style="display: none;">
    <?php echo \Forms\Form::submit(['name' => 'name', 'value' => 'Отправить', 'class' => 'submit btn btn-primary pull-right']); ?>
</div>
<div class="seasonsContent" style="padding-top: 0;">
    <div class="form-group">
        <?php echo Builder::input([
            'name' => 'start',
            'class' =>'datepicker',
            'value'=> $date_start[2].'.'.$date_start[1]. '.' . substr($date_start[0],2),
            'id'=> 'f_seasons-season_start'
        ], __('Начало сезона')); ?>
    </div>
    <div class="form-group">
        <?php echo Builder::input([
            'name' => 'finish',
            'class' =>'datepicker',
            'value'=> $date_finish[2].'.'.$date_finish[1]. '.' . substr($date_finish[0],2),
            'id'=> 'f_seasons-season_end'
        ], __('Конец сезона')); ?>
    </div>
    <?php foreach ($specs as $in => $specification):?>
        <div class="form-group">
            <?php echo Builder::input([
                'name' => 'priority['.$specification->id. ']',
                'disabled' => 'disabled',
                'class' => 'form-season-name',
                'value' => $specification->name,
            ], __('Сезон')); ?>
        </div>
        <div class="form-group">
            <?php echo Builder::input([
                'name' => 'priority['.$specification->id. ']',
                'class' => 'form-season-sort',
                'value' => $specification->sort,
            ], __('Приоритет')); ?>
        </div>
    <?php endforeach;?>
</div>
<?php echo Form::close(); ?>
