<?php
$date_start = explode('-', $start);
$date_finish = explode('-', $finish);

?>
<div class="rowSection">
    <div class="col-md-12">
        <div class="widget">
            <div class="widgetContent">
                <div class="checkbox-wrap">
                    <table class="table table-striped table-bordered"  width="100%">
                        <thead>
                            <tr>
                                <th class="hidden-ss"><?php echo __('id'); ?></th>
                                <th class="hidden-ss"><?php echo __('Начало сезона'); ?></th>
                                <th><?php echo __('Конец сезона'); ?></th>
                                <th></th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php foreach ( $result as $obj ): ?>
                                <?php
                                $date_start = explode('-', $obj->start);
                                $date_finish = explode('-', $obj->finish);
                                ?>
                                <tr data-id="<?php echo $obj->id; ?>">
                                    <td class="hidden-ss"><a href="/wezom/<?php echo Core\Route::controller(); ?>/edit/<?php echo $obj->id; ?>">#<?php echo $obj->id; ?></a></td>
                                    <td><?php echo $date_start[2].'.'.$date_start[1]. '.' . substr($date_start[0],2); ?></td>
                                    <td><?php echo $date_finish[2].'.'.$date_finish[1]. '.' . substr($date_finish[0],2); ?></td>
                                    <td>
                                        <ul class="table-controls">
                                            <li>
                                                <a class="bs-tooltip dropdownToggle" href="javascript:void(0);" title="<?php echo __('Управление'); ?>"><i class="fa fa-cog size14"></i></a>
                                                <ul class="dropdownMenu pull-right">
                                                    <li>
                                                        <a href="/wezom/<?php echo Core\Route::controller(); ?>/edit/<?php echo $obj->id; ?>" title="<?php echo __('Редактировать'); ?>"><i class="fa fa-pencil"></i> <?php echo __('Редактировать'); ?></a>
                                                    </li>
                                                    <li class="divider"></li>
                                                    <li>
                                                        <a onclick="return confirm('<?php echo __('Это действие необратимо. Продолжить?'); ?>');" href="/wezom/<?php echo Core\Route::controller(); ?>/delete/<?php echo $obj->id; ?>" title="<?php echo __('Удалить'); ?>"><i class="fa fa-trash-o text-danger"></i> <?php echo __('Удалить'); ?></a>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>