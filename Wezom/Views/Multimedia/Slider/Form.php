<?php use Forms\Builder;
use Forms\Form;

echo Builder::open(); ?>
    <div class="form-actions" style="display: none;">
        <?php echo Form::submit(['name' => 'name', 'value' => __('Отправить'), 'class' => 'submit btn btn-primary pull-right']); ?>
    </div>
    <div class="col-md-7">
        <div class="widget box">
            <div class="widgetHeader">
                <div class="widgetTitle">
                    <i class="fa fa-reorder"></i>
                    <?php echo __('Основные данные'); ?>
                </div>
            </div>
            <div class="widgetContent">
                <div class="form-vertical row-border">
                    <ul class="liTabs t_wrap">
                        <?php foreach( $languages AS $key => $lang ): ?>
                            <?php $public = \Core\Arr::get($langs, $key, array()); ?>
                            <?php echo $lang['default'] == 1 ? '<input type="hidden" class="default_lang" value="'.$lang['name'].'">' : ''; ?>
                            <li class="t_item">
                                <a class="t_link" href="#"><?php echo $lang['name']; ?></a>
                                <div class="t_content">
                                    <div class="form-group">
                                        <?php echo \Forms\Builder::input([
                                            'name' => 'FORM['.$key.'][name]',
                                            'value' => $public->name,
                                            'class' => ['valid',  $lang['default'] == 1 ? 'translitSource' : ''],
                                        ], __('Название')); ?>
                                    </div>
                                    <div class="form-group">
                                        <?php echo Builder::input([
                                            'name' => 'FORM['.$key.'][main_button_name]',
                                            'value' => $public->main_button_name,
                                            'class' => ['valid'],
                                        ], __('Наименование основной кнопки')); ?>
                                    </div>
                                </div>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-5">
        <div class="widget box">
            <div class="widgetHeader">
                <div class="widgetTitle">
                    <i class="fa fa-reorder"></i>
                    <?php echo __('Изображение'); ?>
                </div>
            </div>
            <div class="widgetContent">
                <div class="form-vertical row-border">
                    <div class="form-group">
                        <?php echo Builder::bool($obj ? $obj->status : 1); ?>
                    </div>
                    <div class="form-group">
                        <?php echo Builder::bool($obj ? $obj->show_image_link : 1,'show_image_link',__('Отображать ссылку на изображении?')); ?>
                    </div>
                    <div class="form-group">
                        <?php echo Builder::input([
                            'name' => 'FORM[url]',
                            'value' => $obj->url,
                        ], __('Ссылка на основной кнопке')); ?>
                    </div>
                    </div>
                    <div class="form-group">
                        <?php echo Builder::input([
                            'name' => 'FORM[image_link]',
                            'value' => $obj->image_link,
                        ], __('Ссылка на изображении')); ?>
                    </div>
                    <div class="orm-group">
                        <label class="control-label">Изображение</label>
                        <div>
                            <?php if (is_file( HOST . Core\HTML::media('images/slider/big/'.$obj->image, false) )): ?>
                                <div class="contentImageView">
                                    <a href="<?php echo Core\HTML::media('images/slider/big/'.$obj->image); ?>" class="mfpImage">
                                        <img src="<?php echo Core\HTML::media('images/slider/small/'.$obj->image); ?>"  alt=""/>
                                    </a>
                                </div>
                                <div class="contentImageControl">
                                    <a class="btn btn-danger otherBtn" href="/wezom/<?php echo Core\Route::controller(); ?>/delete_image/<?php echo $obj->id; ?>">
                                        <i class="fa fa-remove"></i>
                                        <?php echo __('Удалить изображение'); ?>
                                    </a>
                                    <br>
                                    <a class="btn btn-warning otherBtn" href="<?php echo \Core\General::crop('slider', 'small', $obj->image); ?>">
                                        <i class="fa fa-pencil"></i>
                                        <?php echo __('Редактировать'); ?>
                                    </a>
                                </div>
                            <?php elseif(!empty($obj->image)): ?>
                                <div class="contentImageView">
                                    <a href="<?php echo Core\HTML::media('assets/images/placeholders/no-image.jpg'); ?>" class="mfpImage">
                                        <img src="<?php echo Core\HTML::media('assets/images/placeholders/no-image.jpg'); ?>"  alt=""/>
                                    </a>
                                </div>
                                <div class="contentImageControl">
                                    <a class="btn btn-danger otherBtn" href="/wezom/<?php echo Core\Route::controller(); ?>/delete_image/<?php echo $obj->id; ?>">
                                        <i class="fa fa-remove"></i>
                                        <?php echo __('Удалить изображение'); ?>
                                    </a>
                                    <br>
                                    <a class="btn btn-warning otherBtn" href="<?php echo \Core\General::crop('slider', 'small', $obj->image); ?>">
                                        <i class="fa fa-pencil"></i>
                                        <?php echo __('Редактировать'); ?>
                                    </a>
                                </div>
                            <?php else:?>
                                <?php echo Builder::file(['name' => 'file']); ?>
                            <?php endif ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <div class="col-md-7">
        <div class="widget box">
            <div class="widgetHeader">
                <div class="widgetTitle">
                    <i class="fa fa-reorder"></i>
                    <?php echo __('Верхняя одежда(кнопка №2)'); ?>
                </div>
            </div>
            <div class="widgetContent">
                <div class="form-vertical row-border">
                    <ul class="liTabs t_wrap">
                        <?php foreach( $languages AS $key => $lang ): ?>
                            <?php $public = \Core\Arr::get($langs, $key, array()); ?>
                            <?php echo $lang['default'] == 1 ? '<input type="hidden" class="default_lang" value="'.$lang['name'].'">' : ''; ?>
                            <li class="t_item">
                                <a class="t_link" href="#"><?php echo $lang['name']; ?></a>
                                <div class="t_content">
                                    <div class="form-group">
                                        <?php echo Builder::input([
                                            'name' => 'FORM['.$key.'][outerwear_button_name]',
                                            'value' => $public->outerwear_button_name,
                                        ], __('Наименование кнопки на верхнюю одежду')); ?>
                                    </div>
                                </div>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-5">
        <div class="widget box">
            <div class="widgetHeader">
                <div class="widgetTitle">
                    <i class="fa fa-reorder"></i>
                    <?php echo __('Верхняя одежда(кнопка №2)'); ?>
                </div>
            </div>
            <div class="widgetContent">
                <div class="form-vertical row-border">
                    <div class="form-group">
                        <?php echo Builder::bool($obj ? $obj->show_outer_wear_link : 1,'show_outer_wear_link',__('Отображать ссылку на верхнюю одежду?')); ?>
                    </div>
                    <div class="form-group">
                        <?php echo \Forms\Builder::select($svg,
                            $obj->outerwear_button_icon ?: 0, [
                                'id' => 'outerwear_button_icon',
                                'name' => 'FORM[outerwear_button_icon]',
                            ], __('Иконка')); ?>
                    </div>
                    <div class="form-group">
                        <?php echo Builder::input([
                            'name' => 'FORM[link_outerwear]',
                            'value' => $obj->link_outerwear,
                        ], __('Ссылка на верхнюю одежду')); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-7">
        <div class="widget box">
            <div class="widgetHeader">
                <div class="widgetTitle">
                    <i class="fa fa-reorder"></i>
                    <?php echo __('Трикотаж (кнопка №3)'); ?>
                </div>
            </div>
            <div class="widgetContent">
                <div class="form-vertical row-border">
                    <ul class="liTabs t_wrap">
                        <?php foreach( $languages AS $key => $lang ): ?>
                            <?php $public = \Core\Arr::get($langs, $key, array()); ?>
                            <?php echo $lang['default'] == 1 ? '<input type="hidden" class="default_lang" value="'.$lang['name'].'">' : ''; ?>
                            <li class="t_item">
                                <a class="t_link" href="#"><?php echo $lang['name']; ?></a>
                                <div class="t_content">
                                    <div class="form-group">
                                        <?php echo Builder::input([
                                            'name' => 'FORM['.$key.'][jersey_button_name]',
                                            'value' => $public->jersey_button_name,
                                        ], __('Наименование кнопки на трикотаж')); ?>
                                    </div>
                                </div>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-5">
        <div class="widget box">
            <div class="widgetHeader">
                <div class="widgetTitle">
                    <i class="fa fa-reorder"></i>
                    <?php echo __('Трикотаж (кнопка №3)'); ?>
                </div>
            </div>
            <div class="widgetContent">
                <div class="form-vertical row-border">
                    <div class="form-group">
                        <?php echo Builder::bool($obj ? $obj->show_jersey_link : 1,'show_jersey_link',__('Отображать ссылку на трикотаж?')); ?>
                    </div>
                    <div class="form-group">
                        <?php echo \Forms\Builder::select($svg,
                        $obj->jersey_button_icon ?: 0, [
                            'id' => 'jersey_button_icon',
                            'name' => 'FORM[jersey_button_icon]',
                        ], __('Иконка')); ?>
                    </div>
                    <div class="form-group">
                        <?php echo Builder::input([
                            'name' => 'FORM[link_jersey]',
                            'value' => $obj->link_jersey,
                        ], __('Ссылка на трикотаж')); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php echo Form::close(); ?>
