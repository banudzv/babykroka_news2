<?php if($_SERVER['REQUEST_METHOD'] == 'POST'): ?>
    <?php $langs = array(); ?>
    <?php foreach($languages AS $key => $lang): ?>
        <?php $langs[$key] = $obj->$key; ?>
        <?php unset($obj->$key); ?>
    <?php endforeach; ?>
<?php else: ?>
    <?php $langs = \Core\Arr::get($obj, 'langs', array()); ?>
    <?php $obj = \Core\Arr::get($obj, 'obj', array()); ?>
<?php endif; ?>

<div class="page-header clearFix">
    <div class="page-title">
        <h3><?= $obj->name ?></h3>
    </div>
</div>
<?php echo \Forms\Builder::open(); ?>
    <div class="form-actions" style="display: none;">
        <?php echo \Forms\Form::submit(['name' => 'name', 'value' => __('Отправить'), 'class' => 'submit btn btn-primary pull-right']); ?>
    </div>
    <div class="col-md-7">
        <div class="widget box">
            <div class="widgetHeader">
                <div class="widgetTitle">
                    <i class="fa fa-reorder"></i>
                    <?php echo __('Основные данные'); ?>
                </div>
            </div>
            <div class="widgetContent">
                <ul class="liTabs t_wrap">
                    <?php foreach( $languages AS $key => $lang ): ?>
                        <?php $public = \Core\Arr::get($langs, $key, array()); ?>
                        <?php echo $lang['default'] == 1 ? '<input type="hidden" class="default_lang" value="'.$lang['name'].'">' : ''; ?>
                        <li class="t_item">
                            <a class="t_link" href="#"><?= $lang['name']; ?></a>
                            <div class="t_content">
                                <div class="form-vertical row-border">
                                    <div class="form-group">
                                        <?php echo \Forms\Builder::input([
                                            'name' => 'FORM[' . $key . '][h1]',
                                            'value' => $public->h1,
                                        ], [
                                            'text' => 'H1',
                                            'tooltip' => __('Рекомендуется, чтобы тег h1 содержал ключевую фразу, которая частично или полностью совпадает с title'),
                                        ]); ?>
                                    </div>
                                    <div class="form-group">
                                        <?php echo \Forms\Builder::input([
                                            'name' => 'FORM[' . $key . '][title]',
                                            'value' => $public->title,
                                        ], [
                                            'text' => 'Title',
                                            'tooltip' => __('<p>Значимая для продвижения часть заголовка должна быть не более 12 слов</p><p>Самые популярные ключевые слова должны идти в самом начале заголовка и уместиться в первых 50 символов, чтобы сохранить привлекательный вид в поисковой выдаче.</p><p>Старайтесь не использовать в заголовке следующие знаки препинания – . ! ? – </p>'),
                                        ]); ?>
                                    </div>
                                    <div class="form-group">
                                        <?php echo \Forms\Builder::textarea([
                                            'name' => 'FORM[' . $key . '][keywords]',
                                            'rows' => 5,
                                            'value' => $public->keywords,
                                        ], [
                                            'text' => 'Keywords',
                                        ]); ?>
                                    </div>
                                    <div class="form-group">
                                        <?php echo \Forms\Builder::textarea([
                                            'name' => 'FORM[' . $key . '][description]',
                                            'value' => $public->description,
                                            'rows' => 5,
                                        ], [
                                            'text' => 'Description',
                                        ]); ?>
                                    </div>
                                </div>
                            </div>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
    </div>
    <div class="col-md-5">

        <div class="widget">
            <div class="widgetHeader">
                <div class="widgetTitle">
                    <i class="fa fa-reorder"></i>
                    <?php echo __('Переменные'); ?>
                </div>
            </div>
            <div class="pageInfo alert alert-info">
                <?php if ( $obj->id == 1 ): ?>
                    <div class="rowSection">
                        <div class="col-md-6"><strong><?php echo __('Название группы'); ?></strong></div>
                        <div class="col-md-6">{{name}}</div>
                    </div>
                    <div class="rowSection">
                        <div class="col-md-6"><strong><?php echo __('Текст со страницы группы'); ?></strong></div>
                        <div class="col-md-6">{{content}}</div>
                    </div>
                    <div class="rowSection">
                        <div class="col-md-6"><strong><?php echo __('Текст со страницы группы (N первых символов включая пробелы, но с вырезанными HTML тегами)'); ?></strong></div>
                        <div class="col-md-6">{{content:N}}</div>
                    </div>
                <?php endif ?>
                <?php if ( $obj->id == 2 ): ?>
                    <div class="rowSection">
                        <div class="col-md-6"><strong><?php echo __('Наименование товара'); ?></strong></div>
                        <div class="col-md-6">{{name}}</div>
                    </div>
                    <div class="rowSection">
                        <div class="col-md-6"><strong><?php echo __('Наименование группы товара'); ?></strong></div>
                        <div class="col-md-6">{{group}}</div>
                    </div>
                    <div class="rowSection">
                        <div class="col-md-6"><strong><?php echo __('Наименование бренда товара'); ?></strong></div>
                        <div class="col-md-6">{{brand}}</div>
                    </div>
                    <div class="rowSection">
                        <div class="col-md-6"><strong><?php echo __('Наименование модели товара'); ?></strong></div>
                        <div class="col-md-6">{{model}}</div>
                    </div>
					<div class="rowSection">
                        <div class="col-md-6"><strong><?php echo __('Цена товара'); ?></strong></div>
                        <div class="col-md-6">{{price}}</div>
                    </div>
                    <div class="rowSection">
                        <div class="col-md-6"><strong><?php echo __('Код товара'); ?></strong></div>
                        <div class="col-md-6">{{id}}</div>
                    </div>
                    <div class="rowSection">
                        <div class="col-md-6"><strong><?php echo __('Цвет товара'); ?></strong></div>
                        <div class="col-md-6">{{color_name}}</div>
                    </div>
                <?php endif ?>
				<?php if ( $obj->id == 3 ): ?>
                    <div class="rowSection">
                        <div class="col-md-6"><strong><?php echo __('Название производителя'); ?></strong></div>
                        <div class="col-md-6">{{name}}</div>
                    </div>
                <?php endif ?>
				<?php if ( $obj->id == 4 ): ?>
                    <div class="rowSection">
                        <div class="col-md-6"><strong><?php echo __('Заголовок новости'); ?></strong></div>
                        <div class="col-md-6">{{name}}</div>
                    </div>
                <?php endif ?>
				<?php if ( $obj->id == 5 ): ?>
                    <div class="rowSection">
                        <div class="col-md-6"><strong><?php echo __('Заголовок статьи'); ?></strong></div>
                        <div class="col-md-6">{{name}}</div>
                    </div>
                <?php endif ?>
				<?php if ( $obj->id == 6 ): ?>
                    <div class="rowSection">
                        <div class="col-md-6"><strong><?php echo __('Название группы'); ?></strong></div>
                        <div class="col-md-6">{{group}}</div>
                    </div>
					<div class="rowSection">
                        <div class="col-md-6"><strong><?php echo __('Фильтры'); ?></strong></div>
                        <div class="col-md-6">{{filter}}</div>
                    </div>
                    <div class="rowSection">
                        <div class="col-md-6"><strong><?php echo __('Значения фильтров'); ?></strong></div>
                        <div class="col-md-6">{{filter_values}}</div>
                    </div>
					<div class="rowSection">
                        <div class="col-md-6"><strong><?php echo __('Сайт'); ?></strong></div>
                        <div class="col-md-6">{{site}}</div>
                    </div>
                <?php endif ?>
            </div>
        </div>
    </div>
<?php echo \Forms\Form::close(); ?>