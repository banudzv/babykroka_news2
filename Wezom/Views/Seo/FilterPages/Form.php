<?php
$langs = \Core\Arr::get($obj, 'langs', array());
$obj = \Core\Arr::get($obj, 'obj', array());
?>

<?php echo \Forms\Builder::open(); ?>
<div class="form-actions" style="display: none;">
    <?php echo \Forms\Form::submit(['name' => 'name', 'value' => 'Отправить', 'class' => 'submit btn btn-primary pull-right']); ?>
</div>

<div class="col-md-12">
    <div class="widget">
        <div class="widgetContent">
            <div class="col-md-7">
                <div class="widget box">
                    <ul class="liTabs t_wrap">
                        <?php foreach( $languages AS $key => $lang ): ?>
                            <?= $lang['default'] == 1 ? '<input type="hidden" class="default_lang" value="'.$lang['name'].'">' : '' ?>
                            <?php $public = \Core\Arr::get($langs, $key, array()); ?>

                            <li class="t_item">
                                <a class="t_link" href="#"><?= $lang['name']; ?></a>

                                <div class="t_content">
                                    <div class="form-vertical row-border">
                                        <div class="form-group">
                                            <?php echo \Forms\Builder::input([
                                                'name' => 'FORM[' . $key . '][h1]',
                                                'value' => $public->h1,
                                            ], [
                                                'text' => 'H1',
                                                'tooltip' => __('Рекомендуется, чтобы тег h1 содержал ключевую фразу, которая частично или полностью совпадает с title'),
                                            ]); ?>
                                        </div>
                                        <div class="form-group">
                                            <?php echo \Forms\Builder::input([
                                                'name' => 'FORM[' . $key . '][title]',
                                                'value' => $public->title,
                                            ], [
                                                'text' => 'Title',
                                                'tooltip' => __('<p>Значимая для продвижения часть заголовка должна быть не более 12 слов</p><p>Самые популярные ключевые слова должны идти в самом начале заголовка и уместиться в первых 50 символов, чтобы сохранить привлекательный вид в поисковой выдаче.</p><p>Старайтесь не использовать в заголовке следующие знаки препинания – . ! ? – </p>'),
                                            ]); ?>
                                        </div>
                                        <div class="form-group">
                                            <?php echo \Forms\Builder::input([
                                                'name' => 'FORM[' . $key . '][keywords]',
                                                'value' => $public->keywords,
                                            ], [
                                                'text' => 'Keywords',
                                            ]); ?>
                                        </div>
                                        <div class="form-group">
                                            <?php echo \Forms\Builder::textarea([
                                                'name' => 'FORM[' . $key . '][description]',
                                                'value' => $public->description,
                                                'rows' => 5,
                                            ], [
                                                'text' => 'Description',
                                            ]); ?>
                                        </div>
                                        <div class="form-group">
                                            <?php echo \Forms\Builder::tiny([
                                                'name' => 'FORM[' . $key . '][seo_text]',
                                                'value' => $public->seo_text,
                                            ], __('Содержание')); ?>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
            <div class="col-md-5">
                <div class="widget box">
                    <div class="widgetHeader">
                        <div class="widgetTitle">
                            <i class="fa fa-list-alt"></i>
                            <?php echo __('Характеристики'); ?>
                        </div>
                    </div>

                    <div class="widgetContent">
                        <div class="form-group">
                            <?php echo \Forms\Builder::input([
                                'name' => 'FORM[name]',
                                'value' => $obj->name ?? '',
                                'class' => ['valid'],
                            ], __('Название')); ?>
                        </div>
                        <?php echo \Forms\Builder::bool($obj->status ?? 0); ?>
                        <div class="form-group">
                            <?php echo \Forms\Builder::select(
                                    '<option value="0">' . __('Не выбрано') . '</option>' . $categories,
                                    NULL,
                                    [
                                    'id' => 'category_id',
                                    'name' => 'FORM[category_id]',
                                    'class' => 'valid',
                                ], [
                                    'text' => __('Категория'),
                                ]); ?>
                        </div>
                        <div class="table-responsive">
                    <table id="filters" class="table table-striped table-bordered table-hover checkbox-wrap">
                        <thead>
                        <tr>
                            <th class="align-center">Фильтр</th>
                            <th class="align-center">Значение</th>
                            <th class="align-center">Действие</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php $i = 0; foreach($filterValues as $value) : ?>
                            <tr>
                                <td>
                                    <?= \Forms\Builder::select(
                                        \Core\Support::selectData($filters, 'type', 'name'),
                                        $value->type,
                                        [
                                            'name' => 'filters[' . $i . '][type]',
                                            'class' => 'valid filters',
                                            'data-selected' => $value->value
                                        ]); ?>
                                </td>
                                <td>
                                    <select class="values form-control" name="filters[<?= $i ?>][value]"></select>
                                </td>
                                <td class="align-center">
                                    <a class="delete-row btn btn-xs bs-tooltip liTipLink otherBtn" data-title="Удалить фильтр">-</a>
                                </td>
                            </tr>
                            <?php $i++; endforeach; ?>
                        </tbody>
                        <tfoot>
                        <tr>
                            <td colspan="2"></td>
                            <td class="align-center">
                                <a id="add-filter" class="btn btn-xs bs-tooltip liTipLink otherBtn" data-title="Добавить фильтр">+</a>
                            </td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php echo \Forms\Form::close(); ?>

<script>
    let i = <?= $i ?>;

    $('#add-filter').on('click', function() {
        i++;

        let html = '<tr>';
        html += '<td>'
        html += '<select class="filters form-control" name="filters[' + i + '][type]">';
        html += '<option value="0">Выберите фильтр</option>';
        <?php foreach($filters as $filter) : ?>
        html += '<option value="<?= $filter['type'] ?>"><?= $filter['name'] ?></option>';
        <?php endforeach; ?>
        html += '</select>';
        html += '</td>'
        html += '<td>'
        html += '<select class="values form-control" name="filters[' + i + '][value]">';
        html += '<option value="0">Выберите значение</option>';
        html += '</select>';
        html += '</td>'
        html += '<td class="align-center">'
        html += '<a class="delete-row btn btn-xs bs-tooltip liTipLink otherBtn" data-title="Удалить фильтр">-</a>';
        html += '</td>'
        html += '</tr>';

        $('#filters tbody').append(html);
    });

    $('table').on('click', '.delete-row', function() {
        $(this).parents('tr').remove();
    });

    $('.filters').each(function() {
        let $row = $(this).parents('tr');
        let filter = $(this).val();
        let selected = $(this).data('selected');
        loadValues($row, filter, selected);
    });

    $('table').on('change', '.filters', function() {
        let $row = $(this).parents('tr');
        let filter = $(this).val();
        loadValues($row, filter);
    });

    function loadValues($row, filter, selected = null) {
        $.ajax({
            url: '/wezom/filter-pages/filter-values',
            type: 'get',
            data: { filter },
            dataType: 'json',
            success: function(response) {
                let html = '<option value="0">Выберите значение</option>';
                for (let filter of response) {
                    let isSelected = selected === filter.value ? 'selected' : '';
                    html += '<option value="' + filter.value + '" ' + isSelected + '>' + filter.label + '</option>';
                }

                $row.find('.values').html(html);
            }
        });
    }
</script>
