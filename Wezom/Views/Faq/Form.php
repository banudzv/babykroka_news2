<?php echo \Forms\Builder::open(); ?>
<div class="form-actions" style="display: none;">
    <?php echo \Forms\Form::submit(['name' => 'name', 'value' => __('Отправить'), 'class' => 'submit btn btn-primary pull-right']); ?>
</div>
<div class="col-md-8">
    <div class="widget box">
        <div class="widgetHeader">
            <div class="widgetTitle">
                <i class="fa fa-reorder"></i>
                <?php echo __('Основные данные'); ?>
            </div>
        </div>
        <div class="widgetContent">
            <div class="form-vertical row-border">
                <ul class="liTabs t_wrap">
                    <?php foreach( $languages AS $key => $lang ): ?>
                        <?php $public = \Core\Arr::get($langs, $key, array()); ?>
                        <?php echo $lang['default'] == 1 ? '<input type="hidden" class="default_lang" value="'.$lang['name'].'">' : ''; ?>
                        <li class="t_item">
                            <a class="t_link" href="#"><?php echo $lang['name']; ?></a>
                            <div class="t_content">
                                <div class="form-group">
                                    <?php echo \Forms\Builder::input([
                                        'name' => 'FORM['.$key.'][name]',
                                        'value' => $public->name,
                                        'class' => ['valid',  $lang['default'] == 1 ? 'translitSource' : ''],
                                    ], __('Название')); ?>
                                </div>
                                <div class="form-group">
                                    <?php echo \Forms\Builder::tiny([
                                        'name' => 'FORM['.$key.'][text]',
                                        'value' => $public->text,
                                    ], __('Содержание')); ?>
                                </div>
                            </div>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="col-md-4">
    <div class="widget box">
        <div class="widgetHeader">
            <div class="widgetTitle">
                <i class="fa fa-reorder"></i>
                <?php echo __('Основные данные'); ?>
            </div>
        </div>
        <div class="widgetContent">
            <div class="form-vertical row-border">
                <div class="form-group">
                    <?php echo \Forms\Builder::bool($obj ? $obj->status : 1); ?>
                </div>
                <div class="form-group">
                    <?php echo \Forms\Builder::select([
                        '1' => 'Доставка',
                        '2' => 'Оплата',
                        '3' => 'Оптовикам',
                        '4' => 'Акции',
                    ],
                        $obj->group_id,
                        [
                            'name' => 'FORM[group_id]',
                        ],
                        __('Содержание'));
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo \Forms\Form::close(); ?>
