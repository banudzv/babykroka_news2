<ul class="export__checkers _flex">
    <div class="_flex-grow">
        <div class="grid export__table-header _mb-sm">
            <div class="gcell--5 _mr-md">Название</div>
            <div class="gcell--3">Код</div>
            <div class="gcell--3">Цена</div>
            <div class="gcell--3">Цена</div>
            <div class="gcell--3">Дроп</div>
            <div class="gcell--3">Опт</div>
        </div>
        <?php use Core\HTML;

        foreach ($products as $product): ?>
            <li>

                <div class="grid export__table-body _mt-sm _mb-sm">
                     <div class="gcell--5 export__product-name _mr-md">
                         <span title="<?php echo $product->name;?>"><?php echo $product->name; // наимен?></span>
                    </div>
                    <div class="gcell--3"> <?php echo $product->id; // код?></div>

                    <div class="gcell--3"><?php echo $prices[$product->id]['cost_client'] ?></div>
                    <div class="gcell--3"><?php echo $product->price; // цена?></div>
                    <div class="gcell--3"><?php echo \Modules\Catalog\Models\Prices::getPricesRowByPriceType($product->id, '90e0c1a4-364f-11ea-80cc-a4bf01075a5b')->price; // цена  дроп?></div>
                    <div class="gcell--3"><?php echo \Modules\Catalog\Models\Prices::getPricesRowByPriceType($product->id, '78639693-364f-11ea-80cc-a4bf01075a5b')->price; // цена опт ?></div>
                </div>
            </li>
        <?php endforeach; ?>
    </div>
</ul>
