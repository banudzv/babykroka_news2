<?php echo \Forms\Builder::open(); ?>
<div class="form-actions" style="display: none;">
    <?php echo \Forms\Form::submit(['name' => 'name', 'value' => __('Отправить'), 'class' => 'submit btn btn-primary pull-right']); ?>
</div>
<div class="col-md-7">
    <div class="widget box">
        <div class="widgetHeader">
            <div class="widgetTitle">
                <i class="fa fa-reorder"></i>
                <?php echo __('Основные данные'); ?>
            </div>
        </div>
        <div class="widgetContent">
            <div class="form-vertical row-border">
                <div class="form-group">
                    <?php echo \Forms\Builder::input([
                        'name' => 'FORM[file_link]',
                        'value' => $obj->file_link,
                        'class' => ['valid'],
                    ], __('Ссылка на файл')); ?>
                </div>
                <div class="form-group">
                    <?php echo \Forms\Builder::input([
                        'name' => 'FORM[shop_name]',
                        'value' => $obj->shop_name,
                    ], __('Имя магазина')); ?>
                </div>
                <div class="form-group">
                    <?php echo \Forms\Builder::input([
                        'name' => 'FORM[company_name]',
                        'value' => $obj->company_name,
                    ], __('Имя компании')); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if($edit):?>
<div class="col-md-7">
    <div class="widget box">
        <div class="widgetHeader">
            <div class="widgetTitle">
                <i class="fa fa-reorder"></i>
                <?php echo __('Продукты'); ?>
            </div>
        </div>
        <div class="widgetContent">
            <div class="form-vertical row-border">
                 <?php foreach ($categories[0] as $parentId => $obj): ?>
                        <?php $obj = (object)$obj; ?>
                <ul class="export__checkers" data-url="/wezom/ajax/productsCategories">
                    <li>
                        <?php if ($obj->products && !isset($categories[$obj->id])): ?>
                        <span class="checkbox__icon js-product-show" data-id="<?php echo $obj->id; ?>" data-array='<?php echo json_encode($checked);?>'></span>
                        <?php else:?>
                        <span class="checkbox__icon"></span>
                        <?php endif;?>
                        <a href="#">
                            <input type="checkbox"><span><?= $obj->name; ?></span>
                        </a>
                        <?php if (isset($categories[$obj->id])): ?>
                        <?php foreach ($categories[$obj->id] as $item): ?>
                        <?php $item = (object)$item; ?>
                        <ul class="export__checkers is-hide">
                            <li>

                                <?php if ($item->products && !isset($categories[$item->id])): ?>
                                    <span class="checkbox__icon js-product-show" data-id="<?php echo $item->id; ?>" data-array='<?php echo json_encode($checked);?>'></span>

                                <?php else:?>
                                    <span class="checkbox__icon"></span>
                                <?php endif;?>
                                <a href="#">
                                    <input type="checkbox"><span><?= $item->name; ?></span>
                                </a>
                                <?php if (isset($categories[$item->id])): ?>
                                    <?php foreach ($categories[$item->id] as $sub): ?>
                                        <?php $sub = (object)$sub; ?>
                                        <ul class="export__checkers is-hide">
                                    <li>
                                        <?php if ($sub->products && !isset($categories[$sub->id])): ?>
                                            <span class="checkbox__icon js-product-show" data-id="<?php echo $sub->id; ?>" data-array='<?php echo json_encode($checked);?>'></span>
                                        <?php else:?>
                                            <span class="checkbox__icon"></span>
                                        <?php endif;?>
                                        <a href="#">
                                            <input type="checkbox"><span><?= $sub->name; ?></span>
                                        </a>
                                    </li>
                                </ul>
                                <?php endforeach;?>
                                <?php endif;?>
                            </li>
                        </ul>
                        <?php endforeach;?>
                        <?php endif;?>
                    </li>
                </ul>
                <?php endforeach;?>
            </div>
        </div>
    </div>
</div>
<?php endif;?>
<?php echo \Forms\Form::close(); ?>

<script>
	document.addEventListener("DOMContentLoaded", function(e) {
		// Разворачивание списков
		$('.checkbox__icon').on('click', function (e) {
			let icon = $(this);

			icon.toggleClass('opened').parent().children('ul').toggleClass('is-hide');
		})

		// Подгрузка товаров при расскрытии списка с применением фильтра
		$('.js-product-show').on('click', function (e) {
			let $this = $(this),
				id = $this.data('id'),
				array = $this.data('array'),
				url = $('.export__checkers').data('url');

			if ($(this).hasClass('opened')){
				$.ajax({
					url: url,
					type: 'post',
					data: {
						category_id: id,
						array: array,
					},
					dataType: 'json',
					success: function (response) {
						if (response.success) {
							if ($this.closest('li').find('.export__checkers').length) {
								$this.closest('li').find('.export__checkers').replaceWith(response.html)
							} else {
								$this.closest('li').append(response.html)
							}
						}
					}
				});
			}
		})
	})
</script>
<style>
    .export__checkers {
        margin: 0 0 0 20px;
        padding-left: 0; }
    .export__checkers .checkbox__icon {
        margin-left: -15px;
        display: inline-block;
        vertical-align: initial;
        width: 12px;
        height: 12px;
        background-size: contain;
        background-repeat: no-repeat;
        background: url("data:image/svg+xml,%3C%3Fxml version='1.0' encoding='iso-8859-1'%3F%3E%3C!-- Generator: Adobe Illustrator 19.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0) --%3E%3Csvg version='1.1' id='Capa_1' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' x='0px' y='0px' viewBox='0 0 52 52' style='enable-background:new 0 0 52 52;' xml:space='preserve'%3E%3Cpath d='M26,0C11.664,0,0,11.663,0,26s11.664,26,26,26s26-11.663,26-26S40.336,0,26,0z M26,50C12.767,50,2,39.233,2,26 S12.767,2,26,2s24,10.767,24,24S39.233,50,26,50z'/%3E%3Cpath d='M38.5,25H27V14c0-0.553-0.448-1-1-1s-1,0.447-1,1v11H13.5c-0.552,0-1,0.447-1,1s0.448,1,1,1H25v12c0,0.553,0.448,1,1,1 s1-0.447,1-1V27h11.5c0.552,0,1-0.447,1-1S39.052,25,38.5,25z'/%3E%3C/svg%3E%0A"); }
    .export__checkers .checkbox__icon.opened {
        background: url("data:image/svg+xml,%3C%3Fxml version='1.0' encoding='iso-8859-1'%3F%3E%3C!-- Generator: Adobe Illustrator 19.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0) --%3E%3Csvg version='1.1' id='Capa_1' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' x='0px' y='0px' viewBox='0 0 52 52' style='enable-background:new 0 0 52 52;' xml:space='preserve'%3E%3Cpath d='M26,0C11.664,0,0,11.663,0,26s11.664,26,26,26s26-11.663,26-26S40.336,0,26,0z M26,50C12.767,50,2,39.233,2,26 S12.767,2,26,2s24,10.767,24,24S39.233,50,26,50z'/%3E%3Cpath d='M39,25H13c-0.552,0-1,0.447-1,1s0.448,1,1,1h26c0.552,0,1-0.447,1-1S39.552,25,39,25z'/%3E%3C/svg%3E%0A"); }
    .export__checkers .checkbox__icon:hover {
        cursor: pointer; }
    .export__checkers.is-hide {
        display: none; }
    .export__checkers li {
        list-style: none; }
    .export__checkers input[type=checkbox] {
        margin: 0 5px; }
    .grid {
        display: flex;
        flex-wrap: wrap;
        list-style: none;
    }
    ._flex-grow {
        -webkit-box-flex: 1;
        -ms-flex-positive: 1;
        flex-grow: 1;
    }
    ._mr-md {
        margin-right: 0.9375rem;
    }
    .gcell--5 {
        width: 20.83333%;
    }
    .gcell--3 {
        width: 12.5%;
    }
</style>
