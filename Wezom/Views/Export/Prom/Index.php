<div class="rowSection">
    <div class="col-md-12">
        <div class="widget">

            <div class="widget">
                <div class="widgetContent">
                    <table class="table table-striped table-hover checkbox-wrap ">
                        <thead>
                        <tr>
                            <th class="checkbox-head">
                                <label><input type="checkbox"></label>
                            </th>
                            <th><?php echo __('#');?></th>
                            <th><?php echo __('Карточка клиента'); ?></th>
                            <th><?php echo __('Код id клиента'); ?></th>
                            <th><?php echo __('ФИО'); ?></th>
                            <th><?php echo __('Дата'); ?></th>
                            <th><?php echo __('Дата последней авторизации'); ?></th>
                            <th class="nav-column textcenter">&nbsp;</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ( $result as $obj ): ?>
                            <tr data-id="<?php echo $obj->id; ?>">
                                <td class="checkbox-column">
                                    <label><input type="checkbox"></label>
                                </td>
                                <td><a href="/wezom/<?php echo \Core\Route::controller(); ?>/edit/<?php echo $obj->id; ?>"><?php echo $obj->id; ?></a></td>
                                <td><a href="/wezom/users/edit/<?php echo $obj->uid; ?>"><?php echo __('Перейти'); ?></a></td>
                                <td><a href="/wezom/<?php echo \Core\Route::controller(); ?>/edit/<?php echo $obj->id; ?>"><?php echo $obj->customer_id ?></a></td>
                                <td><a href="/wezom/<?php echo \Core\Route::controller(); ?>/edit/<?php echo $obj->id; ?>"><?php echo $obj->last_name.' '. $obj->name ?></a></td>
                                <td><a href="/wezom/<?php echo \Core\Route::controller(); ?>/edit/<?php echo $obj->id; ?>"><?php echo date('d-m-y',$obj->updated_at) ?></a></td>
                                <td><a href="/wezom/<?php echo \Core\Route::controller(); ?>/edit/<?php echo $obj->id; ?>"><?php echo $obj->last_login ? date('d-m-y',$obj->last_login) : '---' ?></a></td>
                                <td class="nav-column">
                                    <ul class="table-controls">
                                        <li>
                                            <a class="bs-tooltip dropdownToggle" href="javascript:void(0);" title="<?php echo __('Управление'); ?>"><i class="fa fa-cog size14"></i></a>
                                            <ul class="dropdownMenu pull-right">
                                                <li>
                                                    <a href="/wezom/<?php echo \Core\Route::controller(); ?>/edit/<?php echo $obj->id; ?>" title="<?php echo __('Редактировать'); ?>"><i class="fa fa-pencil"></i> <?php echo __('Редактировать'); ?></a>
                                                </li>
                                                <li class="divider"></li>
                                                <li>
                                                    <a onclick="return confirm('<?php echo __('Это действие необратимо. Продолжить?'); ?>');" href="/wezom/<?php echo \Core\Route::controller(); ?>/delete/<?php echo $obj->id; ?>" title="<?php echo __('Удалить'); ?>"><i class="fa fa-trash-o text-danger"></i> <?php echo __('Удалить'); ?></a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </td>
                            </tr>
                        <?php endforeach ?>
                        </tbody>
                    </table>
                    <?php echo $pager; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<span id="parameters" data-table="<?php echo $tablename; ?>"></span>
