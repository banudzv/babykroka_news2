<?php /** @var array $groups
 * @var array $products
 */

use Core\Arr;
use Core\View;

?>
<?php echo '<?xml version="1.0" encoding="utf-8"?>'; ?>
<!DOCTYPE yml_catalog SYSTEM "shops.dtd">
<yml_catalog date="<?php echo date('Y-m-d H:i:s', time()) ?>">
    <shop>
        <name><?php echo  $template->shop_name;?></name>
        <company><?php echo  $template->company_name;?></company>
        <currencies>
            <currency id="UAH" rate="1" />
        </currencies>
        <categories>
            <category id="800300503">BABYKROHA</category>
        </categories>
        <offers>
            <?php if (count($products)): ?>
                <?php foreach ($products as $product): ?>
                    <offer id="<?php echo $product['id'] . ($product['size'] ? '-' . $product['size'] : ''); ?>"
                           selling_type="r"
                        <?php echo $product['available'] ? 'available="true"' : ''; ?>
                           group_id="<?php echo $product['id']; ?>">
                        <name><?php echo $product['name']; ?></name>
                        <name_ua><?php echo $product['name_ua']; ?></name_ua>
                        <portal_category_id><?php echo $product['portal_category_id_prom']; ?></portal_category_id>
                        <price><?php echo $product['price'] ?></price>
                        <?php echo $product['cost_old'] != 0.00 ? '<oldprice>' . $product['cost_old'] . '</oldprice>' : ''; ?>
                        <currencyId>UAH</currencyId>
                        <categoryId>800300503</categoryId>
                        <quantity_in_stock><?php echo $product['quantity_in_stock']; ?></quantity_in_stock>
                        <?php if ($product['url_foto_prom']): ?>
                            <?php foreach (json_decode($product['url_foto_prom']) as $image): ?>
                                <picture><?php echo $image ?></picture>
                            <?php endforeach; ?>
                        <?php endif; ?>
                        <delivery>true</delivery>
                        <vendor><?php echo $product['brand']; ?></vendor>
                        <barcode><?php echo $product['id']; ?></barcode>
                        <description><?php echo $product['description'];?></description>
                        <description_ua><?php echo $product['description_ua'];?></description_ua>
                        <manufacturer_warranty>true</manufacturer_warranty>
                        <country><?php echo $product['country'] != '---'? $product['country'] : ''; ?></country>
                        <param name="Сезон"><?php echo $product['season'] ?></param>
                        <param name="Цвет"><?php echo $product['color'] ?></param>
                        <param name="Пол"><?php echo $product['pol'] ?></param>
                        <param name="Тип ткани"> <?php echo $product['type'] ?></param>
                        <param name="Страна производитель"><?php echo $product['country'] ?></param>
                        <param name="Производитель"><?php echo $product['brand'] ?></param>
                        <param name="Размер детской одежды (по росту)"> <?php echo $product['size']; ?></param>
                        <param name="Вид изделия"><?php echo $product['view'] ?></param>
                        <param name="Состояние">
                        Новое</param>
                    </offer>
                    <?php if (($sizes[$product['id']])): ?>
                        <?php foreach ($sizes[$product['id']] as $item_size): ?>
                            <?php $item_size = (array)$item_size; ?>
                            <offer id="<?php echo $product['id'] . ($item_size['size'] ? '-' . $item_size['size'] : ''); ?>"
                                   selling_type="r"
                                <?php echo $product['available'] ? 'available="true"' : ''; ?>
                                   group_id="<?php echo $product['id']; ?>">
                                <name><?php echo $product['name']; ?></name>
                                <name_ua><?php echo $product['name_ua']; ?></name_ua>
                                <portal_category_id><?php echo $product['portal_category_id_prom']; ?></portal_category_id>
                                <price><?php echo $product['price'] ?></price>
                                <?php echo $product['cost_old'] != 0.00 ? '<oldprice>' . $product['cost_old'] . '</oldprice>' : ''; ?>
                                <currencyId>UAH</currencyId>
                                <categoryId>800300503</categoryId>
                                <quantity_in_stock><?php echo $item_size['amount']; ?></quantity_in_stock>
                                <?php if ($product['url_foto_prom']): ?>
                                    <?php foreach (json_decode($product['url_foto_prom']) as $image): ?>
                                        <picture><?php echo $image ?></picture>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                                <barcode><?php echo $product['id']; ?></barcode>
                                <param name="Размер детской одежды (по росту)"
                                       ><?php echo $item_size['size']; ?></param>
                            </offer>
                        <?php endforeach; ?>
                    <?php endif; ?>
                <?php endforeach; ?>
            <?php endif; ?>
        </offers>
    </shop>
</yml_catalog>


