<?php use Forms\Builder;
use Forms\Form; ?>
<div class="mess-order white-popup-block mfp-hide" id="feedback-form" >
    <button title="Close (Esc)" type="button" class="mfp-close">×</button>
    <form method="post" class="wForm validat" enctype="multipart/form-data" data-ajax="feedback/send">
        <div class="title-form"><?php echo 'Форма обратной связи'; ?></div>
        <h4 class="text-form"><?php echo 'Оставьте ваш запрос и мы с вами свяжемся в скором времени!'; ?></h4>
        <div class="widgetContent">
            <div class="form-vertical row-border">
                <div class="form-group">
                    <div class="form-control-feedback">
                        <?php echo Builder::input([
                            'name' => 'name',
                            'class' => 'valid',
                            'placeholder' => 'Ваше имя',
                            'type' => 'text'
                        ], 'Введите ваше имя'); ?>
                    </div>

                </div>
                <div class="form-group">
                    <div class="form-control-feedback">
                        <?php echo Builder::input([
                            'name' => 'phone',
                            'class' => 'valid',
                            'placeholder' => 'Номер телефона',
                            'type' => 'tel'
                        ], 'Ваш телефон для связи'); ?>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-control-feedback">
                        <?php echo Builder::textarea([
                            'name' => 'description',
                            'placeholder' => 'Текст сообщения...',
                        ], 'Описание проблемы'); ?>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-control-feedback">
                        <?php echo Form::button('Отправить', ['type' => 'button', 'class' => 'btn enterBtn btn-primary']); ?>
                    </div>
                </div>
                <p class="text-form"><?php echo 'Если у вас возникли проблемы с отправкой, напишите нам на'; ?>: <a class="phone" href="mailto:support@wezom.ua">support@wezom.ua</a> <?php echo 'или свяжитесь с нами по телефону'; ?>:<a class="phone" href="tel:0800755007">0 800 755 007(<?php echo 'Бесплатно по Украине'; ?>)</a> </p>
            </div>
        </div>
    </form>
</div>
