<?php

use PHPUnit\Framework\TestCase;
use Wezom\Modules\Ajax\Controllers\Cron;

class JobTest extends TestCase
{

    public $xmlCoursesPath = "/cron/courses.xml";

    /**
     * Переопределяем константу т.к по-умолчанию смотрит не в корень проекта, а в папку Tests в которой находятся сами тесты
     * @link https://stackoverflow.com/questions/1653910/dealing-with-path-issues-with-phpunit/2683677
     */
    public function setUp():void
    {
        $_SERVER['DOCUMENT_ROOT'] = __DIR__ . "/..";
    }

    public function testFileExists(): void
    {
        $this->assertFileExists($_SERVER['DOCUMENT_ROOT'].$this->xmlCoursesPath);
    }

    public function testFileNotExists(): void
    {
        $this->assertFileDoesNotExist($_SERVER['DOCUMENT_ROOT'].$this->xmlCoursesPath,'Файл существует');
    }

    /**
     * @depends testFileExists
     */
    public function testFileReadable(): void
    {
        $this->assertFileIsReadable($_SERVER['DOCUMENT_ROOT'].$this->xmlCoursesPath);
    }

    /**
     * @depends testFileExists
     */
    public function testFileWritable(): void
    {
        $this->assertFileIsWritable($_SERVER['DOCUMENT_ROOT'].$this->xmlCoursesPath);
    }


}
