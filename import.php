<?php

ini_set('display_errors', 'on'); // Display all errors on screen
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
header("Cache-Control: public");
header("Expires: " . date("r", time() + 3600));
header('Content-Type: text/html; charset=UTF-8');
ob_start();
@session_start();
//    define('DS', DIRECTORY_SEPARATOR);
define('DS', '/');
define('HOST', dirname(__FILE__)); // Root path
define('MULTI_LANGUAGE', false);
define('APPLICATION', 'frontend'); // Choose application - backend|frontend
define('PROFILER', false); // On/off profiler
define('START_TIME', microtime(true)); // For profiler. Don't touch!
define('START_MEMORY', memory_get_usage()); // For profiler. Don't touch!

require_once 'loader.php';


$forImport = [];
$products = (new \Modules\Catalog\Models\Items)::getRows(null, 'id', 'ASC');

foreach ($products as $obj):
    $temp = [];
    $age = \Modules\Catalog\Models\Items::getAgeByAlias($obj->age_alias);
    $color = \Modules\Catalog\Models\Items::getColorByAlias($obj->color_alias);
    $brand = \Modules\Catalog\Models\Items::getBrandByAlias($obj->brand_alias);
    $sizes = \Modules\Catalog\Models\Items::getItemSizes($obj->id);
    $temp['id'] = $obj->id;
    $temp['status'] = $obj->status;
    $temp['name'] = $obj->name;
    $temp['parent_id'] = $obj->parent_id;
    $temp['new'] = $obj->new;
    $temp['sale'] = $obj->sale;
    $temp['top'] = $obj->top;
    $temp['cost'] = $obj->cost;
    $temp['cost_old'] = $obj->cost_old;
    $temp['artikul'] = $obj->artikul;
    $temp['brand_id'] = $brand->id;
    $temp['age_id'] = $age->id;
    $temp['color_id'] = $color->id;
    $temp['color_group_id'] = $obj->catalog_color_group_id;
    $temp['sizes'] = $sizes;
    $temp['additional'] = $obj->specifications;
    $forImport['products'][] = $temp;
endforeach;

$brands = (new \Modules\Catalog\Models\Brands)::getRows();

foreach ($brands as $obj):
    $temp = [];
    $temp['id'] = $obj->id;
    $temp['name'] = $obj->name;
    $forImport['brands'][] = $temp;
endforeach;

$colors = (new \Modules\Catalog\Models\Colors)::getRows();

foreach ($colors as $obj):
    $temp = [];
    $temp['id'] = $obj->id;
    $temp['name'] = $obj->name;
    $forImport['colors'][] = $temp;
endforeach;

$colors_group = (new \Modules\Catalog\Models\Items)::getColorGroups();

foreach ($colors_group as $obj):
    $temp = [];
    $temp['id'] = $obj->group;
    $forImport['color_groups'][] = $temp;
endforeach;

$groups = (new \Modules\Catalog\Models\Groups)::getRows(null, 'id', 'ASC');

foreach ($groups as $obj):
    $temp = [];
    $temp['id'] = $obj->id;
    $temp['parent_id'] = $obj->parent_id;
    $temp['name'] = $obj->name;
    $forImport['groups'][] = $temp;
endforeach;

$ages = (new \Modules\Catalog\Models\Items)::getAges();

foreach ($ages as $obj):
    $temp = [];
    $temp['id'] = $obj->id;
    $temp['name'] = $obj->name;
    $forImport['ages'][] = $temp;
endforeach;

echo json_encode($forImport, JSON_UNESCAPED_UNICODE);

?>