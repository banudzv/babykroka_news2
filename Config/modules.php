<?php

return [
    'frontend' => [
        'Ajax', 'Index', 'Faq','News', 'About',  'Delivery', 'Articles',  'Wholesale', 'Cart', 'User', 'Subscribe',
        'Search', 'Sitemap', 'Contact', 'Reviews','News2', 'Catalog',  'Content', 'Export', 
    ],
    'backend' => [
        'Ajax', 'Index', 'Faq', 'Catalog', 'Config', 'SeasonConfig', 'Contacts', 'Contactus', 'Content', 'Index', 'Log', 'Visitors', 'Blog', 'Blacklist',
        'MailTemplates', 'Menu', 'Orders', 'Seo', 'Subscribe', 'User', 'Multimedia', 'Statistic', 'Reviews', 'Crop',
        'Translates', 'Export', 'News2', 
    ],
];
