<?php
return [
  'paytypes' => [
      3 =>'card',
      4 =>'apay',
      5 =>'privat24',
      6 =>'moment_part',
      7 =>'card',
      8 =>'qr',
      9 =>'gpay',
      10 =>'masterpass',
  ]
];
