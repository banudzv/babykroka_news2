<?php
return [
    'cron' => true, // true, false
    'selfCron' => 50, // Send message after refresh page by users ( false or count of letters for one refresh )
    'tableCron' => 'cron', // Name of the cron table
    'image' => 'GD', // GD, Magic
    'path_image' => '/Photo', // Name where uploaded photos located from 1C
    'password_min_length' => 4, // Min password length
    'visitor' => false, // save user information to the database?
    'scheduler' => [
        0 => 'updateInformationReferencesAction',
        1 => 'updatePriceDataAction',
        2 => 'updateBalanceAction',
        3 => 'updateCurrenciesAction',
        4 => 'uploadImagesFromFolderAction',
        5 => 'updateFeedDataAction',
        6 => 'updatePromAction'
    ],
    'menus' => [
        1 => 'Группы товаров',
        2 => 'Группа новорождённые',
    ],
    'svg' => [
        0 => 'Нет',
        1 => 'tshirt-icon',
        2 => 'coat-icon'
    ],
    'token' => 'KjsafkjAdglLIG:g7p89:OHID@)p', // defense from CSRF attacks
];
