<?php

return [
    'types' => [
        0 => 'Часы',
        1 => 'Минуты',
        2 => 'Дни',
        3 => 'Месяцы',
        4 => 'Год'
    ],
    'types_timestamps' => [
        0 => 'hour',
        1 => 'minute',
        2 => 'day',
        3 => 'month',
        4 => 'year'
    ],
];
