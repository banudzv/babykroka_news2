<?php

namespace Modules;

use Core\Arr;
use Core\Config;
use Core\Cookie;
use Core\Encrypt;
use Core\GeoIP;
use Core\HTTP;
use Core\Route;
use Core\View;
use Core\System;
use Core\Cron;
use Core\HTML;
use Core\QB\DB;
use Core\User;
use Core\Common;
use Modules\User\Models\CustomerRoles;

class Base {

    /**
     * @var string $_template
     */
    protected $_template = 'Text';
    protected $_content;
    protected $_config = [];
    protected $_seo = [];
    protected $_breadcrumbs = [];
    protected $_method;
    protected $_critical;
    protected $_page = 1;
    protected $_limit;
    protected $_offset;
    protected $_pager;
    protected $_canonical;
    protected $_use_canonical = true;
    protected $_scheme = 'http';
    protected $_noindex = false;
    protected $_languages;
    protected $_current_currency;
    protected $_currencies;
    protected $_page_type;
    protected $_adWordsProps = [];


    public function before() {
        $_POST = Arr::clearArray($_POST);
        $_GET = Arr::clearArray($_GET);
        $this->CSRF();
        $this->_method = $_SERVER['REQUEST_METHOD'];
        $this->config();
        $this->access();
        $this->redirects();
        $this->ssl();
        $this->checkMaintenance();
        User::factory()->is_remember();
        $this->checkUserType();
        $this->checkUser();
        $this->_languages = Config::get('languages') ? Config::get('languages') : array();
        $this->setCurrencies();
        $this->set_alternates();
        if (Cookie::getWithoutSalt('currency') != 'грн' and Cookie::getWithoutSalt('currency') != 'USD') {
            Cookie::setWithoutSalt('currency', 'грн');
            $_COOKIE['currency'] = 'грн';
        }
//        $cron = new Cron;
//        $cron->check();
    }

    public function after() {
        $this->set_canonicals();
        $this->seo();
        $this->visitors();
        $this->render();
    }

    private function checkMaintenance(){
        if(!Config::get('security.maintenance_mode')){
           return false;
        }
        $ips = Config::get('security.allowed_ip');
        $allowed_ips = explode(',',$ips);
        $ip = GeoIP::ip();
        if(in_array($ip,$allowed_ips)){
            return false;
        }
        Config::instance()->setMaintenanceMode();
    }

    private function checkUserType(){
        $user = User::info();
        $role = null;
        if($user){
            $role = CustomerRoles::getFirstCustomerTypeByUserId($user->id);
        }else{
            Cookie::setWithoutSalt('user-type', 'rozn');
        }
        if($user && !Cookie::getWithoutSalt('user-type')){
            Cookie::setWithoutSalt('user-type', $role->alias);
        }elseif($user && Cookie::getWithoutSalt('user-type') && $role->alias != Cookie::getWithoutSalt('user-type')){
            Cookie::setWithoutSalt('user-type', $role->alias);
        }elseif($user && Cookie::getWithoutSalt('user-type')){
            $this->compareCookiesForUser(Cookie::getWithoutSalt('user-type'));
        }
    }

    protected function compareCookiesForUser($cookie): void
    {
        $role = CustomerRoles::getCustomerTypesByUserId(User::info()->id);
        if(!array_key_exists($cookie,$role)){
            $customer = CustomerRoles::getFirstCustomerTypeByUserId(User::info()->id);
            $_COOKIE['user-type'] = $customer->alias;
        }
    }

    private function checkUser(){
        if(isset($_SESSION['page_active']) && Route::module() !== 'ajax'){
            unset($_SESSION['page_active']);
        }
        $user = User::info();
        $role = null;
        $user_type = Cookie::getWithoutSalt('user-type') ?: $_COOKIE['user-type'];
        if($user){
            $role = CustomerRoles::getCustomerTypesByUserId($user->id);
        }
        $opt = Config::get('individual.ind_opt');
        $opt_sell_size = Config::get('individual.sell_size_opt');
        $drop_sell_size = Config::get('individual.sell_size_drop');
        $rozn = Config::get('individual.ind_rozn');
        $rozn_sell_size = Config::get('individual.sell_size');
        $drop = Config::get('individual.ind_drop');
        if(!$user && !empty($rozn)){
            $this->compareSession($rozn,$rozn_sell_size);
            $this->setSession($rozn,$rozn_sell_size);
        }
        if($user && $role[$user_type] == 1 && !empty($rozn)){
            $this->compareSession($rozn,$rozn_sell_size);
            $this->setSession($rozn,$rozn_sell_size);
        }
        if($user && $role[$user_type] == 2 && !empty($opt)){
            $this->compareSession($opt,$opt_sell_size);
            $this->setSession($opt,$opt_sell_size);
        }
        if($user && $role[$user_type] == 3 && !empty($drop)){
            $this->compareSession($drop,$drop_sell_size);
            $this->setSession($drop,$drop_sell_size);
        }
    }

    protected function compareSession($priceType,$sell_size){
        if(isset($_SESSION['prices']) && $_SESSION['prices'] !== $priceType){
            $_SESSION['prices'] = $priceType;
            $_SESSION['sell_size'] = $sell_size;
        }
        if(isset($_SESSION['sell_size']) && $_SESSION['sell_size'] !== $sell_size){
            $_SESSION['sell_size'] = $sell_size;
        }
    }

    protected function setSession($priceType,$sell_size){
        if(!isset($_SESSION['prices'])){
            $_SESSION['prices'] = $priceType;
        }
        if(!isset($_SESSION['sell_size'])){
            $_SESSION['sell_size'] = $sell_size;
        }
    }

    private function ssl() {
        if (Config::get('security.ssl')) {
            $this->_scheme = 'https';
            if (!$_SERVER['HTTPS']) {
                header("HTTP/1.1 301 Moved Permanently");
                header("Location: https://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
            }
        } else {
            if ($_SERVER['HTTPS']) {
                header("HTTP/1.1 301 Moved Permanently");
                header("Location: http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
            }
        }
    }

    private function CSRF() {
        $_SESSION['token'] = Encrypt::instance()->encode(hash('sha256', Config::get('main.token')));
        if (Route::controller() != 'form') {
            return true;
        }
        if ($_POST) {
            if (!array_key_exists('token', $_POST)) {
                die('Error!');
            }
            $token = Encrypt::instance()->decode((string) $_POST['token']);
            if ($token != hash('sha256', Config::get('main.token'))) {
                die('Error!');
            }
        }
    }

    private function access() {
        if (!Config::get('security.auth') || !Config::get('security.username') || !Config::get('security.password')) {
            return false;
        }
        if (
                Arr::get($_SERVER, 'PHP_AUTH_USER') != Config::get('security.username') ||
                Arr::get($_SERVER, 'PHP_AUTH_PW') != Config::get('security.password')
        ) {
            header('HTTP/1.0 401 Unauthorized');
            header('WWW-Authenticate: Basic realm="My Realm"');
            echo "<h1>Authorization Required</h1><p>This server could not verify that you are authorized to access the document requested.  Either you supplied the wrong credentials (e.g., bad password), or your browser doesn't understand how to supply the credentials required.</p>";
            exit;
        }
    }

    public function redirects() {
        $row = DB::select('link_to', 'type')->from('seo_redirects')->where('link_from', '=', strip_tags($_SERVER['REQUEST_URI']))->where('status', '=', 1)->find();
        if ($row) {
            HTTP::redirect($row->link_to, $row->type);
        }
    }

    public function visitors() {
        if (!Config::get('main.visitor')) {
            return false;
        }
        GeoIP::factory()->save();
    }

    private function config() {
        $result = DB::select('key', 'zna', 'group')
                ->from('config')
                ->join('config_groups')->on('config.group', '=', 'config_groups.alias')
                ->where('config.status', '=', 1)
                ->where('config_groups.status', '=', 1)
                ->find_all();
        $groups = [];
        foreach ($result as $obj) {
            $groups[$obj->group][$obj->key] = $obj->zna;
        }
        foreach ($groups as $key => $value) {
            Config::set($key, $value);
        }
        $result = DB::select('script', 'place')->from('seo_scripts')->where('status', '=', 1)->as_object()->execute();
        $this->_seo['scripts'] = ['body' => [], 'head' => [], 'counter' => []];
        foreach ($result as $obj) {
            $this->_seo['scripts'][$obj->place][] = $obj->script;
        }
		$mainPage = \Core\CommonI18n::factory('control')->getRowSimple(1);

        $this->setBreadcrumbs($mainPage->breadcrumb, '');
    }

    private function seo() {
        if (!Config::get('error')) {
            $seo = DB::select('h1', 'title', 'keywords', 'description', 'text')
                            ->from('seo_links')
                            ->where('status', '=', 1)
                            ->where('link', '=', Arr::get($_SERVER, 'REQUEST_URI'))
                            ->as_object()->execute()->current();
            if ($seo) {
                $this->_seo['h1'] = $seo->h1;
                $this->_seo['title'] = $seo->title;
                $this->_seo['keywords'] = $seo->keywords;
                $this->_seo['description'] = $seo->description;
                $this->_seo['seo_text'] = $seo->text;
            }
        } else {
            $this->_seo['h1'] = 'Ошибка 404! Страница не найдена';
            $this->_seo['title'] = 'Ошибка 404! Страница не найдена';
            $this->_seo['keywords'] = 'Ошибка 404! Страница не найдена';
            $this->_seo['description'] = 'Ошибка 404! Страница не найдена';
            $this->_seo['seo_text'] = null;
        }

        $this->_seo['title'] = str_replace('"', '\'', $this->_seo['title']);
        $this->_seo['keywords'] = str_replace('"', '\'', $this->_seo['keywords']);
        $this->_seo['description'] = str_replace('"', '\'', $this->_seo['description']);
    }

    protected function setLastModifiedHeader($LastModified_unix)
    {
        $LastModified = gmdate("D, d M Y H:i:s \G\M\T", $LastModified_unix);
        $IfModifiedSince = false;

        if (isset($_ENV['HTTP_IF_MODIFIED_SINCE'])) {
            $IfModifiedSince = strtotime(substr($_ENV['HTTP_IF_MODIFIED_SINCE'], 5));
        }
        if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE'])) {
            $IfModifiedSince = strtotime(substr($_SERVER['HTTP_IF_MODIFIED_SINCE'], 5));
        }
        if ($IfModifiedSince && $IfModifiedSince >= $LastModified_unix) {
            header($_SERVER['SERVER_PROTOCOL'] . ' 304 Not Modified');
            exit;
        }

        header('Last-Modified: '. $LastModified);
    }

    private function render() {
        if (Config::get('error')) {
            $this->_template = '404';
        }
        if(Config::get('maintenance')){
            $this->_template = '302';
        }
        $this->_breadcrumbs = HTML::breadcrumbs($this->_breadcrumbs);
        $data = [];
        foreach ($this as $key => $value) {
            $data[$key] = $value;
        }
        $data['GLOBAL_MESSAGE'] = System::global_massage();
        echo HTML::compress(View::tpl($data, $this->_template));
    }

    protected function setBreadcrumbs($name, $link = null) {
        $this->_breadcrumbs[] = ['name' => $name, 'link' => $link];
    }

    protected function setCurrencies()
    {
        $currency = Cookie::getWithoutSalt('currency');
        if (empty($currency)) {
            $currency = 'грн';
            Cookie::setWithoutSalt('currency', $currency);
        }

        $this->_currencies = DB::select()->from('currencies_courses')->where('status','=','1')->find_all()->as_array('currency');
        $this->_current_currency = DB::select()->from('currencies_courses')->where('currency','=', $currency)->find();
    }

    protected function generateParentBreadcrumbs($id, $table, $parentAlias, $pre = '/') {
        $bread = $this->generateParentBreadcrumbsElement($id, $table, $parentAlias, []);
        if ($bread) {
            $bread = array_reverse($bread);
        }
        foreach ($bread as $obj) {
            $this->setBreadcrumbs($obj->name, $pre . $obj->alias);
        }
    }

    protected function generateParentBreadcrumbsElement($id, $table, $parentAlias, $bread) {
        $page = DB::select($table.'.id', $parentAlias, 'alias', 'status', $table.'_i18n.name')->from($table)
            ->join($table.'_i18n')->on($table.'.id', '=', $table.'_i18n.row_id')
            ->where($table.'_i18n.language', '=', \I18n::lang())
            ->where($table.'.id', '=', $id)->as_object()->execute()->current();
        if (is_object($page) and $page->status) {
            $bread[] = $page;
        }
        if (is_object($page) and (int) $page->$parentAlias > 0) {
            return $this->generateParentBreadcrumbsElement($page->$parentAlias, $table, $parentAlias, $bread);
        }
        return $bread;
    }

    protected function set_canonicals() {
        if ($this->_use_canonical and $this->_canonical != '' and $this->_pager) {

            if ($this->_page > 1) {
//                $this->_seo['hide_meta'] = 1;
                $this->_seo['canonical'] = $this->_canonical;
                $this->_seo['title'] = $this->_seo['title'] . ' - страница ' . $this->_page;
                $this->_seo['description'] = $this->_seo['description'] . ' - страница ' . $this->_page;
                if ($this->_page == 2) {
                    $this->_seo['prev'] = $this->_canonical;
                } else {
                    $this->_seo['prev'] = $this->_canonical . '/page/' . ($this->_page - 1);
                }
                if ($this->_pager->_next > 1) {
                    $this->_seo['next'] = $this->_canonical . '/page/' . $this->_pager->_next;
                }

//                $this->_noindex = true;
            } else {
                $this->_seo['canonical'] = $this->_canonical;
                if ($this->_pager->_next > 1) {
                    $this->_seo['next'] = $this->_canonical . '/page/' . $this->_pager->_next;
                }
            }
        }
        if (isset($_GET['sort'])) {
            $this->_seo['hide_meta'] = 1;
            $this->_seo['canonical'] = $this->_canonical;
        }
        if ($this->_use_canonical and empty($this->_seo['canonical'])) {
            $uri = $_SERVER['REQUEST_URI'];
            $currentLanguage = \I18n::lang();
            if ($uri == '/' . $currentLanguage) {
                $this->_seo['canonical'] = '/';
            } else {
                $this->_seo['canonical'] = str_replace('/'. $currentLanguage . '/', '/', $uri);
            }
        }

        $this->_seo['noindex'] = $this->_noindex;
    }

    protected function set_alternates()
    {
        $currentLanguage = \I18n::lang();
        $defaultLanguage = \I18n::$defaultLang;
        $host = 'https://' . $_SERVER['HTTP_HOST'];
        $uri = $_SERVER['REQUEST_URI'];
        if ($uri == '/') $uri = '';

        foreach ($this->_languages as $key => $language) {
            $baseUri = $uri == '/' . $currentLanguage ? '' : str_replace('/'. $currentLanguage . '/', '/', $uri);
            if ($key == $defaultLanguage) {
                $this->_seo['alternates'][$language['locale']] = $host . $baseUri;
                $this->_seo['alternates']['x-default'] = $host . $baseUri;
            } else {
                $this->_seo['alternates'][$language['locale']] = $host . '/' . $language['alias'] . $baseUri;
            }
        }
    }
}
