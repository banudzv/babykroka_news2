<?php

return [
    'search' => 'search/search/index',
    'search/page/<page:[0-9]*>' => 'search/search/index',
    'search/<filter:[\w\W]*>page/<page:[0-9]*>' => 'search/search/index',
    'search/<filter:[\w\W]*>' => 'search/search/index',
    'search/<filter:[\w\W]*>/page/<page:[0-9]*>' => 'search/search/index',
];