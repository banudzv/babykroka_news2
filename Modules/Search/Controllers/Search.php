<?php
namespace Modules\Search\Controllers;

use Core\HTML;
use Core\Route;
use Core\View;
use Core\Arr;
use Core\Config;
use Core\Pager\Pager;
use Modules\Base;
use Modules\Catalog\Models\Favorite;
use Modules\Catalog\Models\Filter;
use Modules\Content\Models\Control;
use Modules\Catalog\Models\Items;

class Search extends Base
{

    public $current;
    public $sort;
    public $type;

    public function before()
    {
        parent::before();
        $this->current = Control::getRowSimple(Route::controller(), 'alias', 1);
        if (!$this->current) {
            return Config::error();
        }
        $this->setBreadcrumbs($this->current->name, $this->current->alias);
        $this->_template = 'CatalogItemsWithoutFilter';
        $this->_page = !(int)Route::param('page') ? 1 : (int)Route::param('page');
        $limit = Config::get('basic.limit_groups');
        $this->_limit = (int)Arr::get($_GET, 'per_page') ? (int)Arr::get($_GET, 'per_page') : $limit;
        $this->_offset = ($this->_page - 1) * $this->_limit;
        $this->sort = 'top';
        $this->type = in_array(strtolower(Arr::get($_GET, 'type')), ['asc', 'desc']) ? strtoupper(Arr::get($_GET, 'type')) : 'ASC';
    }

    // Search list
    public function indexAction()
    {
        if (Config::get('error')) {
            return false;
        }
        Route::factory()->setParam('group', 0);

        if ($_SESSION['sort']){
            $this->sort = $_SESSION['sort'];
            $this->type = 'ASC';
        }

        // Check query
        $query = Arr::get($_GET, 'search');
        if (!$query) {
            return $this->_content = $this->noResults();
        }
        $queries = Items::getQueries($query);
        if (empty($queries)) {
            return $this->_content = $this->noResults();
        }

        Config::set('queries', $queries);

        if(Route::param('filter')){
            $filter = Filter::getFilterArr(Route::param('filter'));
            Config::set('filter_array', $filter);
            if ($filter['sort']){
                $this->sort = $filter['sort'][0];
            }
            $result = Filter::getFilteredItemsListByFlag('status', $this->_limit, $this->_offset, $this->sort, $this->type, $queries);
        } else {
            $result = Filter::getFullItemsListByFlag('status', $this->_limit, $this->_offset, $this->sort, $this->type, $queries);
        }

        $filter = Filter::setFilterParameters();

        // Get items list
//        $result = Items::searchRows($queries, $this->_limit, $this->_offset);
        // Check for empty list
        if (!count($result)) {
            return $this->_content = $this->noResults();
        }
        $fav = [];
        if (\Core\User::info()) {
            $favs = Favorite::getFavorites(\Core\User::info()->id);
            foreach ($favs as $f) {
                $fav[] = (int) $f->id;
            }
        }

        // Seo
        $this->_seo['h1'] = $this->current->h1;
        $this->_seo['title'] = $this->current->title;
        $this->_seo['keywords'] = $this->current->keywords;
        $this->_seo['description'] = $this->current->description;
        $this->_seo['nofollow'] = true;

        // Generate pagination
        $this->_pager = Pager::factory($this->_page, $result['total'], $this->_limit);

        $_GET['products'] = $result['items'];
        $_GET['page_type'] = 'searchresults';
        // Render page
        $this->_content = View::tpl(['result' => $result['items'], 'total' => $result['total'], 'favorite' => $fav, 'cats' => $result['cats'], 'pager' => $this->_pager->create()], 'Catalog/ItemsList');
        $this->_critical = HTML::style(HTML::media('assets/css/critical/generated/search-results.css'));
        $this->_query = $query;
        $this->_count = $result['total'];
    }

    public function clean_array_to_search($words = [], $max = 0, $min_length)
    {
        $result = [];
        $i = 0;
        foreach ($words as $key => $value) {
            if (strlen(trim($value)) >= $min_length) {
                $i++;
                if ($i <= $max) {
                    $result[] = trim($value);
                }
            }
        }
        return $result;
    }


    // This we will show when no results
    public function noResults()
    {
        return '<p>По Вашему запросу ничего не найдено!</p>';
    }

}
