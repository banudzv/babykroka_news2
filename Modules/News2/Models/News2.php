<?php
namespace Modules\News2\Models;

use Core\QB\DB;
use Core\CommonI18n;

class News2 extends CommonI18n
{

    public static $table = 'news2';


    public static function getAlsoRows($status = null, $news2_id = null, $sort = null, $type = null, $limit = null, $offset = null, $filter = true){
        $lang = \I18n::$lang;
        static::$tableI18n = static::$table . '_i18n';
        $result = DB::select()->from(static::$table)
            ->join(static::$tableI18n, 'LEFT')->on(static::$tableI18n . '.row_id', '=', static::$table . '.id')
            ->where(static::$tableI18n . '.language', '=', $lang)
            ->where('date', '<=', time());
        if ($status !== null) {
            $result->where('status', '=', $status);
        }
        if ($article_id !== null){
            $result->where(static::$table.'.id', '!=', $article_id);
        }
        if ($filter) {
            $result = static::setFilter($result);
        }
        if ($sort !== null) {
            if ($type !== null) {
                $result->order_by($sort, $type);
            } else {
                $result->order_by($sort);
            }
        }
        $result->order_by(static::$table.'.id', 'DESC');
        if ($limit !== null) {
            $result->limit($limit);
            if ($offset !== null) {
                $result->offset($offset);
            }
        }
        return $result->find_all();
    }

    public static function getRows($status = null, $sort = null, $type = null, $limit = null, $offset = null, $filter = true)
    {
        $lang = \I18n::$lang;
        static::$tableI18n = static::$table . '_i18n';
        $result = DB::select()
            ->from(static::$table)
            ->join(static::$tableI18n, 'LEFT')->on(static::$tableI18n . '.row_id', '=', static::$table . '.id')
            ->where(static::$tableI18n . '.language', '=', $lang)
            ->where('date', '<=', time());
        if ($status !== null) {
            $result->where('status', '=', $status);
        }
        if ($sort !== null) {
            if ($type !== null) {
                $result->order_by($sort, $type);
            } else {
                $result->order_by($sort);
            }
        }
        $result->order_by(static::$table.'.id', 'DESC');
        if ($limit !== null) {
            $result->limit($limit);
            if ($offset !== null) {
                $result->offset($offset);
            }
        }
        return $result->find_all();
    }

    public static function countRows($status = null, $filter = true)
    {
        $result = DB::select([DB::expr('COUNT(' . static::$table . '.id)'), 'count'])
            ->from(static::$table)
            ->where('date', '<=', time());
        if ($status !== null) {
            $result->where(static::$table . '.status', '=', $status);
        }
        return $result->count_all();
    }


}