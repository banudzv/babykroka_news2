<?php
namespace Modules\News2\Controllers;

use Core\CommonI18n;
use Core\Arr;
use Core\View;
use Core\Route;
use Core\Config;
use Core\Message;
use Modules\Base;
use Core\Pager\Pager;
use Modules\Content\Models\Control;
use Modules\News2\Models\News2 AS Model;
use Core\HTML;
use Core\Text;
use Core\HTTP;

class News2 extends Base
{

    public $current;
    public $model;

    public function before()
    {
        parent::before();
        $this->current = Control::getRowSimple(Route::controller(), 'alias', 1);
        if (!$this->current) {
            return Config::error();
        }
        $this->setLastModifiedHeader($this->current->updated_at);
        $this->setBreadcrumbs($this->current->name, $this->current->alias);
        $this->_template = 'Text';

        $this->_page = !(int)Route::param('page') ? 1 : (int)Route::param('page');
        $this->_limit = (int)Config::get('basic.limit_articles');
        $this->_offset = ($this->_page - 1) * $this->_limit;
        $this->model = CommonI18n::factory('news2');
    }

    public function indexAction()
    {
        if (Config::get('error')) {
            return false;
        }
        Config::set('class', 'news2');
        // Seo
        $this->_seo['h1'] = $this->current->h1;
        $this->_seo['title'] = $this->current->title;
        $this->_seo['keywords'] = $this->current->keywords;
        $this->_seo['description'] = $this->current->description;
        // Get Rows
        $result = Model::getRows(1, 'date', 'DESC', $this->_limit, $this->_offset);
        // Get full count of rows
        $count = Model::countRows(1);
        // Generate pagination
        $this->_pager = Pager::factory($this->_page, $count, $this->_limit);
		//canonicals settings
		$this->_use_canonical=1;
		$this->_canonical='articles';
        // Render template
        $this->_content = View::tpl(['result' => $result, 'pager' => $this->_pager->create()], 'News2/News2');
        $this->_critical = HTML::style(HTML::media('assets/css/critical/generated/news.css'));
    }



    public function innerAction()
    {
        $this->_template = 'ArticlesInner';
        if (Config::get('error')) {
            return false;
        }
        // Check for existance
        $obj = $this->model->getRowSimple(Route::param('alias'), 'alias');
        $this->setLastModifiedHeader($obj->updated_at);

        $also_news = Model::getAlsoRows(1, $obj->id, 'date', 'DESC', 3);

        if (!$obj) {
            return Config::error();
        }
		if ($obj->status != 1) {
			HTTP::redirect('/news2', 301);
		}
        // Add plus one to views
        $obj = $this->model->addView($obj);
		// Seo
        $this->setSeoForArticle($obj);

        $structuredData = $this->setStructuredData($obj);

        // Render template
        $this->_date = $obj->date;
        $this->also_news = $also_news;
        $this->_content = View::tpl(['obj' => $obj], 'News2/Inner');
		$this->_content .= View::tpl(['data' => $structuredData], 'News2/MicroData');
        $this->_critical = HTML::style(HTML::media('assets/css/critical/generated/news-page.css'));
    }

    public function setSeoForArticle($page)
    {
        $tpl = CommonI18n::factory('seo_templates')->getRowSimple(5, 'id');
        $from = ['{{name}}'];
        $to = [$page->h1 ?: $page->name];
        $this->_seo['h1'] = $page->h1 ?: str_replace($from, $to, $tpl->h1);
        $this->_seo['title'] = $page->title ?: str_replace($from, $to, $tpl->title);
        $this->_seo['keywords'] = $page->keywords ?: str_replace($from, $to, $tpl->keywords);
        $this->_seo['description'] = $page->description ?: str_replace($from, $to, $tpl->description);
        $this->_seo['image'] = 'images/news2/original/'. $page->image;

		$this->setBreadcrumbs($page->name, $_SERVER['REQUEST_URI']);
    }


    protected function setStructuredData($page)
    {
        $structuredData = [
            '@context' => 'https://schema.org',
            '@type' => 'News2',
            'headline' => $this->_seo['h1'],
            'author' => 'admin',
            'publisher' => [
                '@type' => 'Organization',
                'name' => __('Babykroha - интернет магазин детских товаров'),
                'logo' => [
                    '@type' => 'ImageObject',
                    'url' => HTML::media('/assets/images/logo.svg')
                ]
            ],
            'url' => $this->_canonical,
            'datePublished' => date('Y-m-d', $page->date),
            'dateCreated' => date('Y-m-d', $page->created_at),
            'dateModified' => date('Y-m-d', $page->updated_at),
            'description' => Text::limit_words(strip_tags($page->text), 100),
            'articleBody' => $page->text
        ];

        if ($page->image) {
            $structuredData['image'] = HTML::media('images/news2/original/' . $page->image);
        }

        return $structuredData;
    }
}