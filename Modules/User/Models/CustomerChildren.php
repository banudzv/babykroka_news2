<?php
namespace Modules\User\Models;

use Core\Common;
use Core\QB\DB;

class CustomerChildren extends Common
{

    public static $table = 'customer_children';

    /**
     * Получаем информацию о детях по идентификатору пользователя
     * @param integer $uid
     * @return object
     */
    public static function getChildrenByUserId($uid): object
    {
        return DB::select()
            ->from(self::table())
            ->where('uid', '=', $uid)
            ->find_all();
    }

}
