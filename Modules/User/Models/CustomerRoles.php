<?php
    namespace Modules\User\Models;

    use Core\Common;
    use Core\QB\DB;

    /**
     * Class @CustomerRoles
     * @package Wezom\Modules\User\Models
     * @extends Core\Common
     */
    class CustomerRoles extends Common
    {
        public static $table = 'customer_roles';

        /**
         * Получаем все роли пользователя по его идентификатору
         * @param integer $uid
         * @return array
         */
        public static function getCustomerTypesByUserId($uid): array
        {
            return DB::select()
                ->from('customer_types')
                ->where('uid', '=', $uid)
                ->join(self::table())
                ->on('customer_types.role_id','=',self::table().'.id')
                ->find_all()
                ->as_array('alias','role_id');
        }

        /**
         * Получаем все роли пользователя по его идентификатору
         * @param integer $uid
         * @return object
         */
        public static function getFirstCustomerTypeByUserId($uid)
        {
            return DB::select()
                ->from('customer_types')
                ->where('uid', '=', $uid)
                ->join(self::table())
                ->on('customer_types.role_id','=',self::table().'.id')
                ->find();
        }

        /**
         * @param null|bool $status
         * @param null|string $sort
         * @param null|string $type - ASC or DESC. No $sort - no $type
         * @param null|integer $limit
         * @param null|integer $offset - no $limit - no $offset
         * @param bool $filter
         * @return object
         */
        public static function getRows($status = null, $sort = null, $type = null, $limit = null, $offset = null, $filter = true)
        {
            $result = DB::select()->from(static::$table);
            if ($status !== null) {
                $result->where('status', '=', $status);
            }
            if ($filter) {
                $result = static::setFilter($result);
            }
            if ($sort !== null) {
                if ($type !== null) {
                    $result->order_by($sort, $type);
                } else {
                    $result->order_by($sort);
                }
            }
            $result->order_by('id', 'DESC');
            if ($limit !== null) {
                $result->limit($limit);
                if ($offset !== null) {
                    $result->offset($offset);
                }
            }
            return $result->find_all();
        }


        /**
         * @param null|bool $status
         * @param null|string $sort
         * @param null|string $type - ASC or DESC. No $sort - no $type
         * @param null|integer $limit
         * @param null|integer $offset - no $limit - no $offset
         * @param bool $filter
         * @return object
         */
        public static function getAccountRoles($status = null, $sort = null, $type = null, $limit = null, $offset = null, $filter = true)
        {
            $result = DB::select()->from(static::$table);
            if ($status !== null) {
                $result->where('status', '=', $status);
            }
            $result->where('alias', '=', 'opt');
            $result->or_where('alias', '=', 'drop');
            if ($filter) {
                $result = static::setFilter($result);
            }
            if ($sort !== null) {
                if ($type !== null) {
                    $result->order_by($sort, $type);
                } else {
                    $result->order_by($sort);
                }
            }
            $result->order_by('id', 'DESC');
            if ($limit !== null) {
                $result->limit($limit);
                if ($offset !== null) {
                    $result->offset($offset);
                }
            }
            return $result->find_all();
        }
    }
