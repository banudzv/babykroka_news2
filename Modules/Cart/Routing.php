<?php

return [
    'cart' => 'cart/cart/index',
    'cart/ordering' => 'cart/cart/ordering',
    'cart/payment' => 'cart/cart/pay',
    'cart/order' => 'cart/cart/order',
    'liqpay/answer' => 'cart/cart/liqpayProccess',

    'cart/check-status/<order_id>' => 'cart/cart/checkStatus'
];
