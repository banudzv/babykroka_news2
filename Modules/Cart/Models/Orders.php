<?php

namespace Modules\Cart\Models;

use Core\QB\DB;
use Core\Common;

class Orders extends Common
{

    public static $table = 'orders';


    public static function getUserOrders($user_id)
    {
        $orders = DB::select(
            static::$table . '.*',
            [DB::expr('SUM(orders_items.cost * orders_items.count)'), 'amount'],
            ['currencies_courses.display', 'currency'],
            ['currencies_courses.currency', 'display']
        )
            ->from(static::$table)
            ->join('orders_items', 'LEFT')->on('orders_items.order_id', '=', static::$table . '.id')
            ->join('currencies_courses', 'LEFT')->on('currencies_courses.id','=','orders.currency')
            ->where(static::$table . '.user_id', '=', $user_id)
            ->group_by(static::$table . '.id')
            ->order_by(static::$table . '.created_at', 'DESC');
        return $orders->find_all();
    }


    public static function getOrder($order_id)
    {
        $result = DB::select(
            static::$table . '.*',
            ['currencies_courses.display', 'currency'],
            [DB::expr('SUM(orders_items.cost * orders_items.count)'), 'amount'],
            [DB::expr('SUM(orders_items.count)'), 'count']
        )
            ->from(static::$table)
            ->join('orders_items', 'LEFT')->on('orders_items.order_id', '=', static::$table . '.id')
            ->join('currencies_courses', 'LEFT')->on('currencies_courses.id','=','orders.currency')
            ->where(static::$table . '.id', '=', $order_id);
        return $result->find();
    }


    public static function getOrderItems($order_id)
    {
        $items = DB::select(
            'catalog.alias',
            'catalog.artikul',
            'catalog.id',
            'catalog_images.image',
            'orders_items.count',
            ['orders_items.cost', 'price'],
            ['orders_items.name', 'old_name']
        )
            ->from('orders_items')
            ->join('catalog', 'LEFT')->on('orders_items.catalog_id', '=', 'catalog.id')
            ->join('catalog_i18n', 'LEFT')->on('catalog.id', '=', 'catalog_i18n.row_id')
            ->join('catalog_images', 'LEFT')->on('catalog_images.main', '=', DB::expr('1'))->on('catalog_images.catalog_id', '=', 'catalog.id')
            ->join('colors', 'LEFT')->on('colors.alias','=','catalog.color_alias')
            ->join('colors_i18n')
            ->on('colors.id','=','colors_i18n.row_id')
            ->where('colors_i18n.language', '=', \I18n::lang())
            ->where('catalog_i18n.language', '=', \I18n::lang())
            ->where('orders_items.order_id', '=', $order_id)
            ->group_by('catalog.id')
            ->find_all()->as_array();
        $arr = [];
        foreach ($items as $obj){
            $sizes = DB::select('size', 'count' )->from('orders_items')->where('order_id', '=', $order_id)->where('catalog_id','=', $obj->id)->where('size', '>', 0)->find_all();
            if (count($sizes)) {
                $obj->sizes = $sizes;
            }
            $arr[] = $obj;
        }
        return $arr;
    }

    public static function getRowsMail($order_id) {
        $cart = DB::select(
            'catalog.*',
            ['orders_items'.'.cost', 'price'],
            ['orders_items'.'.id', 'order_item_id']
        )
            ->from('orders_items')
            ->join('catalog', 'LEFT')->on('orders_items'.'.catalog_id', '=', 'catalog.id')
            ->where('orders_items'.'.order_id', '=', $order_id)
            ->group_by('catalog.id')
            ->find_all();

        $arr = [];
        foreach ($cart as $obj){
            $sizes = DB::select(
                'orders_items'.'.size',
                'orders_items'.'.count'
            )->from('orders_items')
                ->where('orders_items'.'.order_id', '=', $order_id)
                ->where('orders_items'.'.catalog_id', '=', $obj->id)
                ->find_all();

            $color = DB::select()->from('colors')->where('alias','=',$obj->color_alias)->find();
            $count = 0;
            foreach ($sizes as $size){
                $count += $size->count;
            }
            $obj->count = $count;
            $obj->sizes = $sizes;
            $obj->color_name = $color->name;
            $arr[] = $obj;
        }
        return $arr;
    }

    public static function getOrderByLiqpayId($id){
        return DB::select()
            ->from(static::$table)
            ->where('liqpay_id','=', $id)
            ->find();
    }
}
