<?php
namespace Modules\Cart\Models;

use Core\Common;
use Core\QB\Database;
use Core\QB\DB;
use Core\Cookie;
use Core\Arr;
use Core\Text;
use Core\User;

class Cart
{

    static $_instance;

    public $_cart_id = 0; // Cart ID in our system
    public $_cart = []; // Items in our cart
    public $_count_goods = 0; // Count of items in our cart

    function __construct()
    {
        $this->clearCarts();
        $this->set_cart_id();
        $this->check_cart();
        $this->recount();
    }

    static function factory()
    {
        if (self::$_instance == null) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }


    public function clearCarts()
    {
        DB::query(Database::DELETE, 'DELETE FROM carts WHERE carts.id NOT IN (SELECT DISTINCT cart_id FROM carts_items) AND created_at < "' . (time() - 24 * 60 * 60) . '"')->execute();
    }

    public function clearCart($cart_id)
    {
        DB::query(Database::DELETE, 'DELETE FROM carts WHERE carts.id = '.$cart_id)->execute();
    }

    public function get_list_for_seo()
    {
        $ids = [];
        foreach ($this->_cart AS $key => $item) {
            $ids[] = $item['id'];
        }

        return DB::select(
            'catalog.*',
            'catalog_i18n.*',
            'carts_items_sizes.size',
            'carts_items_sizes.count',
            'product_prices.price',
            'product_prices.price_old',
            'product_prices.currency_id')
            ->from('catalog')
            ->join('carts_items', 'LEFT')
            ->on('carts_items.catalog_id', '=', 'catalog.id')
            ->join('carts_items_sizes', 'left')
            ->on('carts_items.id', '=', 'carts_items_sizes.cart_item_id')
            ->join('catalog_i18n', 'left')
            ->on('catalog.id', '=', 'catalog_i18n.row_id')
            ->join('product_prices')
            ->on('catalog.id','=','product_prices.product_id')
            ->where('product_prices.price_type', '=', $_SESSION['prices'])
            ->where('carts_items.cart_id', '=', $this->_cart_id)
            ->where('catalog.status', '=', 1)
            ->where('catalog.id', 'IN', $ids)
            ->where('catalog_i18n.language', '=', \I18n::$lang)
            ->find_all()
            ->as_array();
    }

    // Get goods list for basket from database
    public function get_list_for_basket()
    {
        $ids = [];
        foreach ($this->_cart AS $key => $item) {
            $ids[] = $item['id'];
        }
        if (!count($ids)) {
            $ids = [0];
        }
        $result = DB::select('catalog.*','product_prices.price',
            'product_prices.price_old',
            'product_prices.currency_id')
            ->from('catalog')
            ->join('carts_items', 'LEFT')
            ->on('carts_items.catalog_id', '=', 'catalog.id')
            ->join('product_prices')
            ->on('catalog.id','=','product_prices.product_id')
            ->where('product_prices.price_type', '=', $_SESSION['prices'])
            ->where('carts_items.cart_id', '=', $this->_cart_id)
            ->where('catalog.status', '=', 1)
            ->where('catalog.id', 'IN', $ids)
            ->find_all();


        $basket = $this->_cart;
        foreach ($result as $obj) {

            if(isset($obj->color_alias) && !empty($obj->color_alias)){
                $res = DB::select(
                    ['colors.color', 'color'],
                    ['colors_i18n.name', 'color_name']
                )
                    ->from('catalog')
                    ->join('colors')
                    ->on('colors.alias','=','catalog.color_alias')
                    ->join('colors_i18n')
                    ->on('colors_i18n.id','=','colors_i18n.row_id')
                    ->where('colors_i18n.language', '=', \I18n::lang())
                    ->where('catalog.id', '=', $obj->id)
                    ->execute();
                $obj->color = $res[0]['color'];
                $obj->color_name = $res[0]['color_name'];
            }


            $sizes_amount = DB::select()
                ->from('catalog_size_amount')
                ->where('catalog_id', '=',  $obj->id)
                ->find_all();
            foreach ($sizes_amount as $s) {
                $res = DB::select()->from('carts_items')
                    ->where('cart_id', '=', $this->_cart_id)
                    ->where('catalog_id', '=', $obj->id)
                    ->find();
                DB::delete('carts_items_sizes')
                    ->where('cart_item_id', '=', $res->id)
                    ->where('size', '=', $s->size)
                    ->where('count', '>', $s->amount)
                    ->execute();
            }
            $sizes = DB::select('carts_items_sizes.size', 'carts_items_sizes.count', 'catalog_size_amount.amount')
                ->from('carts_items_sizes')
                ->join('carts_items')
                ->on('carts_items.id','=','carts_items_sizes.cart_item_id')
                ->join('catalog_size_amount', 'inner')
                ->on('carts_items.catalog_id', '=', 'catalog_size_amount.catalog_id')
                ->on('carts_items_sizes.size', '=', 'catalog_size_amount.size')
                ->where('carts_items.catalog_id', '=', $obj->id)
                ->where('carts_items.cart_id', '=', $this->_cart_id)
                ->find_all()
                ->as_array();

            $obj->sizes = $sizes;

            if(count($sizes_amount) && !count($sizes)){
                unset($basket[$obj->id]);
            } else{
                $basket[$obj->id]['obj'] = $obj;
            }

        }
        return $basket;
    }


    // Setting cart ID
    public function set_cart_id()
    {
        // Check cookie for existance of the cart
        $hash = Cookie::get('cart');
        if (!$hash) {
            return $this->create_cart();
        }
        // Check if our cookie not bad
        $cart = Common::factory('carts')->getRow($hash, 'hash');
        if (!$cart) {
            return $this->create_cart();
        }
        // Set cart_id
        $this->_cart_id = $cart->id;
        return true;
    }


    // Creation of the new cart
    public function create_cart()
    {
        // Generate hash of new cart for cookie
        $hash = sha1(microtime() . Text::random());
        // Save cart into database
        $this->_cart_id = Common::factory('carts')->insert([
            'hash' => $hash,
            'user_id' => User::info()->id ?: null,
        ]);
        // Save cart to cookie
        Cookie::set('cart', $hash, 60 * 60 * 24 * 365);
        return true;
    }


    // Check existance of cart
    public function check_cart()
    {
        if (!$this->_cart_id) {
            return false;
        }
        $result = DB::select(['carts_items.catalog_id', 'catalog_id'], ['carts_items.count', 'count'])
            ->from('carts_items')
            ->join('catalog', 'LEFT')->on('catalog.id', '=', 'carts_items.catalog_id')
            ->where('catalog.status', '=', 1)
            ->where('carts_items.cart_id', '=', $this->_cart_id)
            ->order_by('carts_items.id', 'DESC')
            ->find_all();
        foreach ($result as $obj) {
            $this->_cart[$obj->catalog_id] = [
                'id' => $obj->catalog_id,
                'count' => $obj->count,
            ];
        }
        return true;
    }


    // Count goods in cart
    public function recount()
    {
        $count = 0;
        foreach ($this->_cart as $b) {
            $count += (int)$b['count'];
        }
        $this->_count_goods = $count;
    }


    // Get full cost of all cart
    public function get_summa()
    {
        $summa = 0;
        if (empty($this->_cart)) {
            return 0;
        }
        $ids = [];
        foreach ($this->_cart as $b) {
            if (!in_array($b['id'], $ids)) {
                $ids[] = $b['id'];
            }
        }
        $result = DB::select('cost', 'id')->from('catalog')->where('status', '=', 1)->where('id', 'IN', $ids)->find_all();
        $items = [];
        foreach ($result as $obj) {
            $items[$obj->id] = $obj;
        }
        foreach ($this->_cart as $b) {
            if (User::info() && User::info()->type == 1){
                $cost = $items[$b['id']]->cost_opt;
            } else if(User::info() && User::info()->type == 2){
                $cost = $items[$b['id']]->cost_drop;
            } else{
                $cost = $items[$b['id']]->cost;
            }
            $summa += $cost * $b['count'];
        }
        return $summa;
    }


    /**
     *      Add goods to cart
     * @param int $catalog_id - goods ID
     * @param int $count - count goods in the cart
     * @return boolean
     */
    public function add($catalog_id, $count = 1)
    {
        if (!Arr::get($this->_cart, $catalog_id, false)) {
            $cartSet = [
                $catalog_id => [
                    'id' => $catalog_id,
                    'count' => $count,
                ]
            ];
            $cartOld = $this->_cart;
            $this->_cart = [];
            foreach (array_merge($cartSet,$cartOld) as $arr){
                $this->_cart[$arr['id']] = $arr;
            }
            Common::factory('carts_items')->insert([
                'catalog_id' => $catalog_id,
                'cart_id' => $this->_cart_id,
                'count' => $count,
            ]);
        } else {
            $this->_cart[$catalog_id]['count'] = $this->_cart[$catalog_id]['count'] + $count;
            DB::update('carts_items')
                ->set(['count' => $this->_cart[$catalog_id]['count']])
                ->where('cart_id', '=', $this->_cart_id)
                ->where('catalog_id', '=', $catalog_id)
                ->execute();
        }
        $this->recount();
        return true;
    }


    /**
     *      Change count in the cart
     * @param int $catalog_id - goods ID
     * @param int $count - new count in the cart
     * @return boolean
     */
    public function edit($catalog_id, $count)
    {
        if (!Arr::get($this->_cart, $catalog_id, false)) {
            return false;
        }
        $this->_cart[$catalog_id]['count'] = $count;
        DB::update('carts_items')
            ->set(['count' => $count])
            ->where('cart_id', '=', $this->_cart_id)
            ->where('catalog_id', '=', $catalog_id)
            ->execute();
        $this->recount();
        return true;
    }


    /**
     *      Delete goods from the cart
     * @param $catalog_id - goods ID
     * @return boolean
     */
    public function delete($catalog_id)
    {
        if (Arr::get($this->_cart, $catalog_id, false)) {
            unset($this->_cart[$catalog_id]);
            DB::delete('carts_items')
                ->where("catalog_id", "=", $catalog_id)
                ->where("cart_id", "=", $this->_cart_id)
                ->execute();
            $this->recount();
            return true;
        }
        return false;
    }


    // Total cleaning of the cart
    public function clear()
    {
        Common::factory('carts')->delete($this->_cart_id);
        DB::delete('carts_items')->where('cart_id', '=', $this->_cart_id)->execute();
        $this->_cart_id = 0;
        $this->_cart = [];
        Cookie::delete('cart');
        $this->recount();
        $_SESSION['order_id'] = null;
    }

    public function checkUserCart($user_id)
    {
        $userCart = DB::select()
            ->from('carts')
            ->where('user_id', '=', $user_id)
            ->find();
        if ($userCart->id) {
            foreach ($this->_cart as $catalog_id => $item) {
                $userCartItem = DB::select()->from('carts_items')
                    ->where('catalog_id', '=', $catalog_id)
                    ->where('cart_id', '=', $userCart->id)
                    ->find();
                if ($userCartItem->id) {
                    $cartItem = DB::select()->from('carts_items')->where('cart_id','=',$this->_cart_id)->where('catalog_id','=',$catalog_id)->find();
                    $new_count = DB::select()->from('carts_items_sizes')->where('cart_item_id','=',$cartItem->id)->execute()->as_array();
                    foreach ( $new_count as $obj) {
                        $res = DB::update('carts_items_sizes')
                            ->set([
                                'cart_item_id' => $userCartItem->id,
                                'count' => $obj['count']
                            ])
                            ->where('cart_item_id', '=', $userCartItem->id)
                            ->where('size', '=', $obj['size'])
                            ->execute();

                        if (!$res){
                            $temp = [
                                'cart_item_id' => $userCartItem->id,
                                'size' => $obj['size'],
                                'count' => $obj['count']
                            ];
                            DB::insert('carts_items_sizes', array_keys($temp))
                                ->values(array_values($temp))
                                ->execute();
                        }
                    }

                    $count = DB::select([DB::expr('SUM(count)'),'count'])
                        ->from('carts_items_sizes')
                        ->where('cart_item_id','=',$userCartItem->id)
                        ->find();

                    DB::update('carts_items')
                        ->set(['count' => $count->count])
                        ->where('catalog_id', '=', $catalog_id)
                        ->where('cart_id', '=', $userCart->id)
                        ->execute();

                } else {
                    $itemData = [
                        'cart_id' => $userCart->id,
                        'catalog_id' => $catalog_id,
                        'count' => $item['count'],
                    ];
                    $cartItem = DB::select()->from('carts_items')->where('cart_id','=',$this->_cart_id)->where('catalog_id','=',$catalog_id)->find();
                    $new_count = DB::select()->from('carts_items_sizes')->where('cart_item_id','=',$cartItem->id)->find_all();
                    $res = DB::insert('carts_items', array_keys($itemData))
                        ->values(array_values($itemData))
                        ->execute();
                    foreach ( $new_count as $obj) {
                        $temp = [
                            'cart_item_id' => $res[0],
                            'size' => $obj->size,
                            'count' => $obj->count
                        ];
                        DB::insert('carts_items_sizes', array_keys($temp))
                            ->values(array_values($temp))
                            ->execute();
                    }
                }
            }
            DB::delete('carts')->where('id', '=', $this->_cart_id)->execute();
            Cookie::set('cart', $userCart->hash, 60 * 60 * 24 * 365);
            $this->_cart_id = $userCart->id;
            $this->check_cart();
            $this->recount();
        } else {
            DB::update('carts')
                ->set(['user_id' => $user_id])
                ->where('id', '=', $this->_cart_id)
                ->execute();
        }
    }

    public function saveUserCart()
    {
        $curCartItems = DB::select()->from('carts_items')
            ->where('cart_id', '=', $this->_cart_id)
            ->find_all();

        $this->create_cart();
        foreach ($curCartItems as $curCartItem) {
            $itemData = [
                'cart_id' => $this->_cart_id,
                'catalog_id' => $curCartItem->catalog_id,
                'count' => $curCartItem->count,
            ];
            DB::insert('carts_items', array_keys($itemData))
                ->values(array_values($itemData))
                ->execute();
        }
        $this->check_cart();
        $this->recount();
    }

}
