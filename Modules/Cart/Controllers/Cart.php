<?php
namespace Modules\Cart\Controllers;

use Core\Arr;
use Core\Config as conf;
use Core\Delivery\NP;
use Core\Email;
use Core\HTML;
use Core\HTTP;
use Core\Payments\LiqPay;
use Core\QB\DB;
use Core\Route;
use Core\User;
use Modules\Cart\Models\Orders;
use Modules\Content\Models\Control;
use Modules\Cart\Models\Cart AS C;
use Core\View;
use Core\Config;
use Modules\Base;
use Wezom\Modules\Orders\Models\OrdersItems;

class Cart extends Base
{

    public $current;

    public function before()
    {
        parent::before();
        $this->current = Control::getRowSimple(Route::controller(), 'alias', 1);
        if (!$this->current) {
            return Config::error();
        }
        $this->setBreadcrumbs($this->current->name, $this->current->alias);
        $this->_template = 'Cart';
    }

    // Cart page with order form
    public function indexAction()
    {
        if (Config::get('error')) {
            return false;
        }
        Config::set('step_one', 'active');
        Config::set('step_two', '');
        Config::set('step_pay', '');
        Config::set('step_three', '');
        // Seo
        $this->_seo['h1'] = $this->current->h1;
        $this->_seo['title'] = $this->current->title;
        $this->_seo['keywords'] = $this->current->keywords;
        $this->_seo['description'] = $this->current->description;
        // Get cart items
        $cart = C::factory()->get_list_for_basket();
        $counts = [];
        foreach ($cart as $item) {
            foreach ($item['obj']->sizes as $size) {
                $counts[$size->size] = $size->count;
            }
        }

        $_GET['cart_products'] = $cart;
        $_GET['page_type'] = 'conversionintent';
        // Render template
        $this->_content = View::tpl(['cart' => $cart, 'counts' => $counts], 'Cart/Index');
        $this->_critical = HTML::style(HTML::media('assets/css/critical/generated/cart.css'));
    }

    public function orderingAction()
    {
        if (Config::get('error')) {
            return false;
        }
        // Get cart items
        $cart = C::factory()->get_list_for_basket();

        if (empty($cart)){
            HTTP::redirect('catalog');
        }
        Config::set('step_one', 'done');
        Config::set('step_two', 'active');
        Config::set('step_pay', '');
        Config::set('step_three', '');
        // Seo
        $this->_seo['h1'] = $this->current->h1;
        $this->_seo['title'] = $this->current->title;
        $this->_seo['keywords'] = $this->current->keywords;
        $this->_seo['description'] = $this->current->description;

        // Get regions from API NovaPoshta
        $np = new NP();
        $regions = $np->getRegions();

        $query = [];
        parse_str($_SERVER['QUERY_STRING'], $query);
        if($id = $query['id']) {
            $order = Orders::getOrder($id);
        } else{
            $order = Orders::getOrder($_SESSION['order_id']);
            $order_delivery = DB::select()->from('orders_delivery')->where('order_id','=', $order->id)->find();
        }

        $order_delivery = DB::select()->from('orders_delivery')->where('order_id','=', $order->id)->find();

        $_GET['cart_products'] = $cart;
        $_GET['page_type'] = 'conversionintent';
        // Render template
        $this->_content = View::tpl(['order_delivery' => $order_delivery,'order' => $order ?: null,'cart' => $cart, 'payment' => Config::get('order.payment'), 'delivery' => Config::get('order.delivery'), 'regions' => $regions], 'Cart/Delivery');
        $this->_critical = HTML::style(HTML::media('assets/css/critical/generated/cart-info.css'));
    }

    public function payAction()
    {
        if (Config::get('error')) {
            return false;
        }
        C::factory()->get_list_for_basket();
        Config::set('step_one', 'done');
        Config::set('step_two', 'done');
        Config::set('step_pay', 'active');
        Config::set('step_three', '');
        $iphone = false;
        // Seo
        $this->_seo['h1'] = $this->current->h1;
        $this->_seo['title'] = $this->current->title;
        $this->_seo['keywords'] = $this->current->keywords;
        $this->_seo['description'] = $this->current->description;
        $sess = $_COOKIE['PHPSESSID'];
        setcookie('PHPSESSID', $sess, ['secure' => true, 'samesite' => 'None']);


        preg_match("/iPhone|iPad|iPod|webOS/", $_SERVER['HTTP_USER_AGENT'], $matches);
        $os_match = current($matches);
        $os_match ? $iphone = true : '';

        $query = [];
        parse_str($_SERVER['QUERY_STRING'], $query);
        if($id = $query['id']) {
            $order = Orders::getOrder($id);
        }

        $_GET['cart_products'] = C::factory()->get_list_for_basket();
        $_GET['page_type'] = 'conversionintent';
        // Render template
        $this->_content = View::tpl(['order' => $order, 'iphone' => $iphone, 'payment' => Config::get('order.payment'), 'delivery' => Config::get('order.delivery'), ], 'Cart/Payment');
        $this->_critical = HTML::style(HTML::media('assets/css/critical/generated/cart-info.css'));
    }

    public function orderAction()
    {
        $sess = $_COOKIE['PHPSESSID'];
        unset($_COOKIE['PHPSESSID']);
        setcookie('PHPSESSID', $sess);

        if (Config::get('error')) {
            return false;
        }

        $_GET['order_products'] = C::factory()->get_list_for_seo();

        Config::set('step_one', 'done');
        Config::set('step_two', 'done');
        Config::set('step_pay', 'done');
        Config::set('step_three', 'done');
        // Seo
        $this->_seo['h1'] = $this->current->h1;
        $this->_seo['title'] = $this->current->title;
        $this->_seo['keywords'] = $this->current->keywords;
        $this->_seo['description'] = $this->current->description;
        // Get cart items

        $query = [];
        parse_str($_SERVER['QUERY_STRING'], $query);
        if($id = $query['id']) {
            $order = Orders::getOrder($id);
            $liqpay = new \Core\Payments\LiqPay(Config::get('basic.liqpay_public'), Config::get('basic.liqpay_private'));
            $res = $liqpay->api("request", array(
                'action' => 'status',
                'version' => '3',
                'order_id' => $order->liqpay_id
            ));

            if ($liqpay->error_statuses[$res->status]) {
                HTTP::redirect('cart/payment?id='. $id);
            }
           if (array_search($order->liqpay_status, $liqpay->success_statuses)) {
                $statuses_liqpay = null;
            } else {
               $statuses_liqpay = $liqpay->statuses[$res->status];
            }

        }else{
            $order = $_SESSION['order_id'];
            $order = Orders::getOrder($order);
        }
        $orders_item = DB::select()->from('orders_items')->where('order_id', '=', $order->id)->find_all();
        foreach ($orders_item as $order_item){
            $db_size = DB::select()->from('catalog_size_amount')->where('size', '=', $order_item->size)->where('catalog_id', '=', $order_item->catalog_id)->find();
            $count = (int)$db_size->amount - (int)$order_item->count;
            DB::update('catalog_size_amount')->set(['amount' => $count])->where('id', '=', $db_size->id)->execute();
        }

        $_GET['transaction_id'] = $_SESSION['order_id'];
        $_SESSION['order_id'] = null;
        $_GET['page_type'] = 'conversion';
        C::factory()->clear();

        $this->sendMailOrder($order);
        $this->_content = View::tpl(['order' => $order, 'status' => $statuses_liqpay ? $statuses_liqpay: null], 'Cart/Ordered');
        $this->_critical = HTML::style(HTML::media('assets/css/critical/generated/confirm-order.css'));

    }

    public function sendMailOrder($order){
        $link_user = 'http://' . Arr::get($_SERVER, 'HTTP_HOST') . '/account/orders/' . $order->id;
        $link_admin = 'http://' . Arr::get($_SERVER, 'HTTP_HOST') . '/wezom/orders/edit/' . $order->id;

        // Get lists of delivery and payment from config file /config/order.php
        $d = conf::get('order.delivery');
        $p = conf::get('order.payment');

        $items = Orders::getRowsMail($order->id);
        $order_delivery = DB::select()->from('orders_delivery')->where('order_id','=', $order->id)->find();
        if ($order->delivery != 3) {
            $region = $order_delivery->region;
            $city = $order_delivery->city;
            $section = $order_delivery->warehouse;
            $index = $order->index;

            if ($order->delivery == 2) {
                $delivery_info = 'Регион: ' . $region . ', Город: ' . $city . ', Адрес: ' . $section . ', Индекс: ' . $index;
            } else {
                $np = new NP();
                $areas = $np->getRegion($region);
                $cities = $np->getCity($region, $city);
                $warehouses = $np->getWarehouse($city, $section);
                $rg = $areas;
                $c = $cities;
                $w = $warehouses;
                $delivery_info = 'Регион: ' . $rg . ', Город: ' . $c . ', Склад: ' . $w;
            }
        }

        // Send message to admin if need
        Email::sendTemplate(11, [
            '{{id}}' => $order->id,
            '{{site}}' => Arr::get($_SERVER, 'HTTP_HOST'),
            '{{ip}}' => $order->ip,
            '{{date}}' => date('d.m.Y H:i'),
            '{{name}}' => $order->name,
            '{{phone}}' => $order->phone,
            '{{payment}}' => $p[$order->payment],
            '{{delivery}}' => $d[$order->delivery],
            '{{delivery_info}}' => $delivery_info,
            '{{link_admin}}' => $link_admin,
            '{{link_user}}' => $link_user,
            '{{items}}' => View::tpl(['cart' => $items, 'scheme' => $this->_scheme], 'Cart/ItemsMail')
        ]);

        if($order->email) {
            Email::sendTemplate(12, [
                '{{id}}' => $order->id,
                '{{site}}' => Arr::get($_SERVER, 'HTTP_HOST'),
                '{{ip}}' => $order->ip,
                '{{date}}' => date('d.m.Y H:i'),
                '{{name}}' => $order->name,
                '{{phone}}' => $order->phone,
                '{{payment}}' => $p[$order->payment],
                '{{delivery}}' => $d[$order->delivery],
                '{{delivery_info}}' => $delivery_info,
                '{{link_admin}}' => $link_admin,
                '{{link_user}}' => $link_user,
                '{{items}}' => View::tpl(['cart' => $items, 'scheme' => $this->_scheme], 'Cart/ItemsMail')
            ], $order->email);
        }
    }

    public function liqpayProccessAction(){
        $data = $_POST['data'];
        $postSignature = $_POST['signature'];

        $privateKey = Config::get('basic.liqpay_private');
        $sign = base64_encode(sha1($privateKey . $data . $privateKey, 1));

        $result = json_decode(base64_decode($data), true);

        $order = Orders::getOrderByLiqpayId(Arr::get($result, 'order_id'));
        $status = Arr::get($result, 'status');
        $liqpay = new \Core\Payments\LiqPay(Config::get('basic.liqpay_public'), Config::get('basic.liqpay_private'));

        $liqpay_status = null;
        if ($liqpay->error_statuses[$status]) {
            $updateOrder['liqpay_status'] = $liqpay->error_statuses[$status];
        } elseif ($liqpay->success_statuses[$status]) {
            $updateOrder['liqpay'] = 1;
            $updateOrder['status'] = 4;
            $updateOrder['liqpay_status'] = $liqpay->success_statuses[$status];
        } else {
            $updateOrder['liqpay_status'] = $liqpay->statuses[$status];
        }

        $updateOrder['liqpay_description'] = Arr::get($result, 'description');
        $updateOrder['liqpay_order_id'] =  Arr::get($result, 'liqpay_order_id');
        $updateOrder['liqpay_payment_id'] = Arr::get($result, 'payment_id');
        DB::update('orders')->set($updateOrder)->where('id', '=', $order->id)->execute();

    }

    public function checkStatusAction()
    {
        $order_id = Route::param('order_id');
        if(!isset($order_id)){
            return false;
        }

        $status = [
            'action'        => 'status',
            'version'       => '3',
            'order_id'      => "$order_id"
        ];
        $liqpay = new LiqPay(Config::get('basic.liqpay_public'), Config::get('basic.liqpay_private'));
        $stat = $liqpay->api("request", $status);
        if($stat->result == 'ok') {
            Orders::update(['status' => 4], $order_id);
            $_SESSION['order_id'] = $order_id;
            HTTP::redirect('cart/order');
        }

    }
}
