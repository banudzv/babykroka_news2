<?php
namespace Modules\Faq\Controllers;

use Core\Common;
use Core\CommonI18n;
use Core\HTML;
use Core\Route;
use Core\View;
use Core\Config;
use Modules\Base;
use Modules\Content\Models\Control;

class Faq extends Base{

    public $current;
    public $model;

    public function before()
    {
        parent::before();
        $this->current = Control::getRowSimple(Route::controller(), 'alias', 1);
        if (!$this->current) {
            return Config::error();
        }
        $this->setLastModifiedHeader($this->current->updated_at);
        $this->setBreadcrumbs($this->current->name, $this->current->alias);
        $this->_template = 'Text';
        Config::set('class', 'faq');
        $this->_page = !(int)Route::param('page') ? 1 : (int)Route::param('page');
        $this->model = CommonI18n::factory('faq');
    }

    public function indexAction()
    {
        if (Config::get('error')) {
            return false;
        }
        // Seo
        $this->_seo['h1'] = $this->current->h1;
        $this->_seo['title'] = $this->current->title;
        $this->_seo['keywords'] = $this->current->keywords;
        $this->_seo['description'] = $this->current->description;
        // Get Rows
        $result = $this->model->getRows(1, 'id', 'DESC');
        $arr = [];
        foreach ($result as $item) {
            $arr[$item->group_id][] = $item;
        }
        ksort($arr);
        // Render template
        $this->_content = View::tpl(['result' => $arr], 'Faq/Index');
        $this->_critical = HTML::style(HTML::media('assets/css/critical/generated/faq.css'));
    }
}
