<?php
namespace Modules\Wholesale\Controllers;

use Core\Config;
use Core\HTML;
use Core\Route;
use Modules\Base;
use Modules\Content\Models\Control;

class Wholesale extends Base
{

    public $current;

    public function before()
    {
        parent::before();
        $this->current = Control::getRowSimple(Route::controller(), 'alias', 1);
        if (!$this->current) {
            return Config::error();
        }
        $this->setLastModifiedHeader($this->current->updated_at);
        $this->setBreadcrumbs($this->current->name, $this->current->alias);
    }

    public function indexAction()
    {
        $this->_template = 'Wholesale/Index';
        // Check for existance
        if (Config::get('error')) {
            return false;
        }
        // Seo
        $this->_seo['h1'] = $this->current->h1;
        $this->_seo['title'] = $this->current->title;
        $this->_seo['keywords'] = $this->current->keywords;
        $this->_seo['description'] = $this->current->description;
        // Render template
        $this->_content = $this->current->text;

        $this->_critical = HTML::style(HTML::media('assets/css/critical/generated/wholesale.css'));
    }
}
