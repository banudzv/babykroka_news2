<?php
namespace Modules\Export\Models;

use Core\QB\Database;
use Core\QB\DB;
use Core\Common;
use Core\User;

class Prom extends Common
{

    public static $table = 'prom';
    public static $tableRelation = 'prom_products';

    public static function insertRelation($prom_id, $products){
        $values = [];
        foreach($products as $product){
            $cost = $product[1] ? $product[1] : 0;
            $values[]= '('.$prom_id.', '.$product[0].', '.$product[2].', '.$cost.')';
        }
        $values = implode(", ", $values);
        $sql = "INSERT INTO prom_products (prom_id, product_id, category_product_id, cost_client) VALUES ". $values;

        DB::query(DATABASE::INSERT, $sql)->execute();
    }

    public static function deleteRelation($value, $field = 'id')
    {
        return DB::delete(static::$tableRelation)->where($field, '=', $value)->execute();
    }

    public static function getCheckedProductsUser($user_id, $db = null){
        return DB::select('product_id', ['category_product_id', 'parent_id'])
            ->from(static::$tableRelation)
            ->join(static::$table)->on(static::$table.'.id', '=', static::$tableRelation.'.prom_id')
            ->where(static::$table.'.user_id', '=', $user_id)
            ->find_all($db)
            ->as_array('product_id');
    }

    public static function getUserProm($id){
        return DB::select()
            ->from(static::$table)
            ->where('user_id', '=', $id)
            ->find();
    }

    public static function getCategoriesCheckedProducts(){
        return DB::select('category_product_id')
            ->from(static::$tableRelation)
            ->join(static::$table)->on(static::$table.'.id', '=', static::$tableRelation.'.prom_id')
            ->where(static::$table.'.user_id', '=', User::info()->id)
            ->find_all()->as_array('category_product_id');
    }
}
