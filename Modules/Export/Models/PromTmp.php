<?php

namespace Modules\Export\Models;

use Core\QB\Database;
use Core\QB\DB;
use Core\Common;
use Core\User;

class PromTmp extends Common
{
    public static $table = 'prom_products_tmp';

    public static function insertPromProduct($productId, $categoryId)
    {
        return PromTmp::insert(['product_id' => $productId,'category_product_id' => $categoryId, 'user_id' => User::factory()->info()->id]);
    }

    public static function deletePromProduct($productId)
    {
        $userId = User::factory()->info()->id;
        $id = DB::select('id')
            ->from(static::$table)
            ->where('product_id', '=', $productId)
            ->where('user_id', '=', $userId)
            ->find();

        return PromTmp::delete($id->id);
    }

    public static function deleteAllUserProduct()
    {
        return PromTmp::delete(User::factory()->info()->id, 'user_id');
    }

    public static function insertManyProducts($products)
    {
        $userId = User::factory()->info()->id;
        $values = [];
        foreach ($products as $key => $product) {
            $values[] = '(' . $key . ', ' . $product->parent_id . ', ' . $userId . ')';
        }
        $values = implode(", ", $values);
        $sql = "INSERT INTO prom_products_tmp (product_id, category_product_id, user_id) VALUES " . $values;

        DB::query(DATABASE::INSERT, $sql)->execute();
    }

    public static function deleteManyProducts($products)
    {
        DB::delete(static::$table)
            ->where('user_id', '=', User::factory()->info()->id)
            ->where('product_id', 'IN', array_keys($products))
            ->execute();
    }

    public static function getAll($amount = null, $available = null, $brands = null)
    {
        $result = DB::select(static::$table . '.*', ['product_prices.price', 'price'], 'category_product_id')
            ->from(static::$table)
            ->join('catalog')->on(static::$table . '.product_id', '=', 'catalog.id')
            ->join('product_prices')->on('catalog.id', '=', 'product_prices.product_id')
            ->join('catalog_size_amount')->on('catalog.id', '=', 'catalog_size_amount.catalog_id')
            ->where('user_id', '=', User::factory()->info()->id)
            ->where('price_type', '=', '875e4eb9-364f-11ea-80cc-a4bf01075a5b');

        if ($available == 'true') {
            $result->where('catalog_size_amount.amount', '>', '0');
        }
        if ($amount) {
            $amount = explode(" ", $amount);
            $result->where('product_prices.price', '>=', $amount[0]);
            $result->where('product_prices.price', '<=', $amount[3]);
        }
        if(!empty($brands)){
            $result->where('brand_alias', 'IN', $brands);
        }


        return $result->group_by(static::$table . '.product_id')->find_all('prom')->as_array('product_id');
    }
    public static function deleteByFilter($amount = null, $available = null, $brands = null)
    {
        $ids = self::getAll($amount, $available, $brands);
        if (!empty($ids)){
            DB::delete(static::$table)->where('product_id', 'NOT IN', array_keys($ids))->execute();
        }else{
            self::deleteAllUserProduct();
        }
        return $ids;
    }
}
