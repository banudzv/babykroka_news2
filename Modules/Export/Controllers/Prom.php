<?php
namespace Modules\Export\Controllers;

use Core\Cookie;
use Core\HTML;
use Core\QB\DB;
use Core\User;
use Core\View;
use Core\Config;
use Modules\Base;
use Modules\Catalog\Models\Brands;
use Modules\Catalog\Models\Groups;
use Modules\Catalog\Models\Items;
use Modules\Export\Models\Prom AS Model;
use Modules\Export\Models\PromTmp;


class Prom extends Base
{
    public $prom;

    public function before()
    {
        parent::before();
        $this->setBreadcrumbs(__('Выгрузка для прома'), 'account/export/prom');
        $this->_template = 'Cabinet';
        $this->_seo['h1'] = __('');
        $this->_seo['title'] = __('Выгрузка для прома');
        $this->_seo['keywords'] = __('Выгрузка для прома');
        $this->_seo['description'] = __('Выгрузка для прома');

        $this->prom = Model::getUserProm(User::info()->id);
    }

    public function promAction(){
        if (!User::info()) {
            return Config::error();
        }
        Config::set('class', 'export');

        PromTmp::deleteAllUserProduct();

        $group_ids = [];
        $checkedCategories = Model::getCategoriesCheckedProducts();
        foreach ($checkedCategories as $checkedCategory => $value) {
            if(!in_array($checkedCategory, $group_ids)){
                Groups::getParentsByChild($checkedCategory, $group_ids);
            }
        }
        $group_ids = array_unique($group_ids);

        $result = Groups::getRows(1, 'id', 'ASC')->as_array();
        $arr = [];
        foreach ($result as $obj) {
            $child = (int) \Wezom\Modules\Catalog\Models\Groups::getCountChildRows($obj->id, 'prom');

            if ($child){
                Groups::$innerIds = [];
                $ids = Groups::getInnerIds($obj->id, 'prom');
                $tmp = Items::getProductsIdByGroups($ids, null, 'prom');
                $count = count($tmp->execute('prom'));
            } else{
                $count = count(Items::getProductByCatId($obj->id, 'prom'));
            }
            if($count == 0)
                continue;

            $item = (array)$obj;

            if($count > 0){
                $item['products'] = (bool)$count;
            }
            if(in_array($item['id'], $group_ids)){
                $item['checked'] = true;
            }else{
                $item['checked'] = false;
            }

            $arr[$obj->parent_id][] = $item;

        }

        $checkedProducts = [];
        $checkedUserProducts = Model::getCheckedProductsUser(User::info()->id, 'prom');

        $brands = Brands::getBrandsSelect('prom');

        if($checkedUserProducts){

            PromTmp::insertManyProducts($checkedUserProducts);
        }

        $max_price =  DB::select([DB::expr('MAX(price)'), 'p'])->from('product_prices')->find('prom');
        $min_price =  DB::select([DB::expr('MIN(price)'), 'p'])->from('product_prices')->find('prom');
        $prices = ['max' => $max_price->p, 'min' => $min_price->p];
        $prom_info = DB::select()->from('prom_info')->find('prom');
        $this->_content = View::tpl([
            'categories' => $arr,
            'price' => $prices,
            'brands' => $brands,
            'checkedGroups' => $group_ids,
            'prom' => $this->prom,
            'prom_info' => $prom_info,
        ], 'User/Export/Prom');
        $this->_critical = HTML::style(HTML::media('assets/css/critical/generated/profile-history.css'));
    }
}
