<?php
namespace Modules\Subscribe\Controllers;

use Core\Common;
use Core\Route;
use Core\HTTP;
use Core\Message;
use Core\Email;
use Core\Arr;
use Core\System;
use Modules\Base;

class Subscribe extends Base
{

	public function subscribeAction()
    {
        $model = Common::factory('subscribers');
        $subscriber = $model->getRow(Route::param('hash'), 'hash');
        if (!$subscriber) {
            Message::GetMessage(0, 'Вы не подписаны на рассылку с нашего сайта!');
            HTTP::redirect('/');
        } 
		if ($subscriber->status == 1) {
			Message::GetMessage(0, 'Вы уже подписаны на рассылку с нашего сайта!');
            HTTP::redirect('/');
		}
        $model->update(['status' => 1], $subscriber->id);
        $ip = System::getRealIP();
		// Send E-Mail to user
        Email::sendTemplate(2, [
            '{{site}}' => Arr::get($_SERVER, 'HTTP_HOST'), 
            '{{link}}' => 'http://' . Arr::get($_SERVER, 'HTTP_HOST') . '/unsubscribe/hash/' . $subscriber->hash, 
            '{{email}}' => $subscriber->email, 
            '{{ip}}' => $ip, 
            '{{date}}' => date('d.m.Y H:i')
        ], $subscriber->email);
		
        Message::GetMessage(1, 'Вы успешно подписались на рассылку с нашего сайта!');
        HTTP::redirect('/');
    }
	
    public function unsubscribeAction()
    {
        $model = Common::factory('subscribers');
        $subscriber = $model->getRow(Route::param('hash'), 'hash');
        if (!$subscriber) {
            Message::GetMessage(0, 'Вы не подписаны на рассылку с нашего сайта!');
            HTTP::redirect('/');
        }
        $model->delete($subscriber->id);
        Message::GetMessage(1, 'Вы успешно отписались от рассылки новостей с нашего сайта!');
        HTTP::redirect('/');
    }

}