<?php

return [
	'subscribe/hash/<hash>' => 'subscribe/subscribe/subscribe',
    'unsubscribe/hash/<hash>' => 'subscribe/subscribe/unsubscribe',
];