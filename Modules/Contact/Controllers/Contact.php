<?php
namespace Modules\Contact\Controllers;

use Core\HTML;
use Core\Route;
use Core\View;
use Core\Config;
use Modules\Base;
use Modules\Content\Models\Control;
use Modules\Contact\Models\Contact as Model;

class Contact extends Base
{

    public $current;

    public function before()
    {
        parent::before();
        $this->current = Control::getRowSimple(Route::controller(), 'alias', 1);
        if (!$this->current) {
            return Config::error();
        }
        $this->setLastModifiedHeader($this->current->updated_at);
        $this->setBreadcrumbs($this->current->name, $this->current->alias);
    }

    public function indexAction()
    {
        if (Config::get('error')) {
            return false;
        }
        Config::set('class', 'contacts');
        // Seo
        $this->_seo['h1'] = $this->current->h1;
        $this->_seo['title'] = $this->current->title;
        $this->_seo['keywords'] = $this->current->keywords;
        $this->_seo['description'] = $this->current->description;
        $contacts = Model::getRows(1, 'id');
        // Render template
        $this->_content = View::tpl([
            'text' => $this->current->text,
            'result' => $contacts,
            'kids' => []
        ], 'Contact/Index');
        $this->_critical = HTML::style(HTML::media('assets/css/critical/generated/contacts.css'));
    }

}
