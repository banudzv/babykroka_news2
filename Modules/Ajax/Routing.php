<?php

    return [
        'ajax/<action>' => 'ajax/general/<action>',
        'form/<action>' => 'ajax/form/<action>',
        'popup/<action>' => 'ajax/popup/<action>',
        'job/<action>' => 'ajax/job/<action>',
        'export/<action>' => 'ajax/export/<action>',
    ];
