<?php
namespace Modules\Ajax\Controllers;

use Core\Arr;
use Core\Config;
use Core\Cron;
use Modules\Ajax;

class Job extends Ajax
{

    /**
     * @runJobsAction
     * Запускаем проверять задачи в планировщике
     */
    public function runJobsAction()
    {
        $token = Arr::get($_GET,'token');
        if($token !== Config::get('main.token')){
            Config::forbidden();
        }
        $job = new \Wezom\Modules\Ajax\Controllers\Job();
        $job->workerAction();
        die;
    }

    /**
     * @runDispatchAction
     * Запускаем проверку рассылки письма
     */
    public function runDispatchAction(): void
    {
        $token = Arr::get($_GET,'token');
        if($token !== Config::get('main.token')){
            Config::forbidden();
        }
        $cron = new Cron;
        $cron->check();
        die;
    }
    public static function testAction(){
        $job = new \Wezom\Modules\Ajax\Controllers\Job();
        $job->workerAction();
    }

}
