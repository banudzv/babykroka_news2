<?php

namespace Modules\Ajax\Controllers;

use Core\Arr;
use Core\Common;
use Core\Config;
use Core\Cookie;
use Core\Delivery\NP;
use Core\Email;
use Core\GeoIP;
use Core\Log;
use Core\Message;
use Core\Pager\Pager;
use Core\QB\DB;
use Core\Route;
use Core\Support;
use Core\System;
use Core\Text;
use Core\User;
use Core\View;
use Core\Widgets;
use Modules\Cart\Models\Cart;
use Core\HTML;
use Modules\Ajax;
use Modules\Cart\Models\Orders;
use Modules\Catalog\Models\Favorite;
use Modules\Catalog\Models\Filter;
use Modules\Catalog\Models\Items;
use Modules\Reviews\Models\Reviews;
use Modules\User\Models\CustomerChildren;
use Modules\User\Models\CustomerRoles;
use Wezom\Modules\Subscribe\Models\Subscribe;
use Wezom\Modules\Subscribe\Models\Subscribers;

class General extends Ajax
{

    public function removeConnectionAction()
    {
        $id = Arr::get($_POST, 'id');
        $row = Common::factory('users_networks')->getRow($id);
        if ($row) {
            Common::factory('users_networks')->delete($id);
        }
        Message::GetMessage(1, 'Вы успешно удалили связь Вашего аккаунта и соц. сети!', 5000);
        $this->success();
    }


    // Add item to cart
    public function addToCartAction()
    {
        // Get and check incoming data
        $catalog_id = Arr::get($this->post, 'id');
        if (!$catalog_id) {
            $this->error('No such item!');
        }

        $sizes = Arr::get($this->post, 'sizes', 0);

        if ($sizes == 0) {
            $sizes = DB::select()->from('catalog_size_amount')->where('catalog_id', '=', $catalog_id)->where('amount', '>', 0)->execute();
            $all_sizes = DB::select()->from('catalog_size_amount')->where('catalog_id', '=', $catalog_id)->execute();
            if (count($all_sizes) && !count($sizes)) {
                $counts = 0;
                $message = 'К сожалению товар, который вы хотите добавить, закончился. Обратитесь к менеджеру для решения этой проблемы.';
            } elseif (count($all_sizes) && count($sizes)) {
                $count = Arr::get($this->post, 'count', 1);
                $item = DB::select()->from('carts_items')
                    ->where('catalog_id', '=', $catalog_id)
                    ->where('cart_id', '=', Cart::factory()->_cart_id)
                    ->find();

                if (!$item) {
                    if (count($sizes)> 1 || $sizes[0]['size'] != '-') {
                        //$count = count($sizes);
                        $size_count = 1;

                    } else {
                        //$count = ($count)?$count:1;
                        $size_count = 1;
                    }
                    $counts = $count;
                } else {
                    $counts = 0;
                    foreach ($sizes as $size) {
                        $check = DB::select()->from('carts_items_sizes')->where('cart_item_id', '=', $item->id)->where('size', '=', $size['size'])->find();
                        $counts += (($check->count + $count) <= $size['amount']) ? (int)$count : $size['amount'] - $check->count;
                    }
                    if ($counts == 0) {
                        $message = 'Вы добавили максимальное число товаров';
                    }
                }
            } else {
                $size_count = $counts = Arr::get($this->post, 'count', 1);
            }
        } else {
            $sizes_arr = is_array($sizes) ? Arr::get($this->post, 'sizes') : explode(',', $sizes);
            $counts_arr = is_array(Arr::get($this->post, 'counts')) ? Arr::get($this->post, 'counts') : explode(',', Arr::get($this->post, 'counts'));
            $counts = 0;

            $sizes = DB::select()->from('catalog_size_amount')->where('catalog_id', '=', $catalog_id)->where('amount', '>', 0)->execute();
            $all_sizes = DB::select()->from('catalog_size_amount')->where('catalog_id', '=', $catalog_id)->execute();
            if (count($all_sizes) && !count($sizes)) {
                $counts = 0;
                $message = 'К сожалению товар, который вы хотите добавить, закончился. Обратитесь к менеджеру для решения этой проблемы.';
            } else {
                $sizes = [];
                foreach ($sizes_arr as $index => $size) {

                    if ($counts_arr[$index] != 0) {

                        $db = DB::select()->from('catalog_size_amount')
                            ->where('catalog_id', '=', $catalog_id)
                            ->where('amount', '>', 0)
                            ->where('size', '=', $size)
                            ->find();

                        $item = DB::select()->from('carts_items')
                            ->where('catalog_id', '=', $catalog_id)
                            ->where('cart_id', '=', Cart::factory()->_cart_id)
                            ->find();
                        if ($item) {
                            $check = DB::select()->from('carts_items_sizes')->where('cart_item_id', '=', $item->id)->where('size', '=', $size)->find();
                            $counts_arr[$index] = (($check->count + $counts_arr[$index]) <= $db->amount) ? $counts_arr[$index] : 0;
                        }

                        $sizes[$index]['size'] = $size;
                        $sizes[$index]['count'] = $counts_arr[$index];
                        $sizes[$index]['amount'] = $db->amount;

                        $counts += $counts_arr[$index];
                        if ($counts == 0) {
                            $message = 'Вы добавили максимальное число товаров';
                        }
                    }
                }
            }
        }

        if ($counts == 0) {
            if (!$message) {
                $message = 'Выберите размер и количество для этого продукта';
            }
            $this->error([
                'response' => $message
            ]);
        }

        // Add one item to cart
        Cart::factory()->add($catalog_id, $counts);

        $item = DB::select()->from('carts_items')
            ->where('catalog_id', '=', $catalog_id)
            ->where('cart_id', '=', Cart::factory()->_cart_id)
            ->find();

        foreach ($sizes as $size) {
            if (!empty($size['count'])) {
                $count = $size['count'];
            } elseif ($size_count) {
                $count = $size_count;
            }

            $check = DB::select()->from('carts_items_sizes')->where('cart_item_id', '=', $item->id)->where('size', '=', $size['size'])->find();
            if ($check && count(get_object_vars($check))) {
                Common::factory('carts_items_sizes')->update([
                    'size' => $size['size'],
                    'count' => (($check->count + $count) <= $size['amount']) ? $check->count + $count : $size['amount']
                ], $check->id, 'id');
            } else {
                Common::factory('carts_items_sizes')->insert([
                    'cart_item_id' => $item->id,
                    'size' => $size['size'],
                    'count' => $counts
                ]);
            }
        }


        $result = Cart::factory()->get_list_for_basket();
        $cart = [];
        $price = 0;
        foreach ($result as $item) {
            $obj = Arr::get($item, 'obj');
            if ($obj) {
                if (User::info() && User::info()->type == 1) {
                    $cost = $obj->cost_opt;
                } else if (User::info() && User::info()->type == 2) {
                    $cost = $obj->cost_drop;
                } else {
                    $cost = $obj->cost;
                }
                $cart[] = [
                    'id' => $obj->id,
                    'name' => $obj->name,
                    'cost' => $cost,
                    'image' => is_file(HOST . HTML::media('images/catalog/medium/' . $obj->image, false)) ? HTML::media('images/catalog/medium/' . $obj->image) : '',
                    'alias' => $obj->alias,
                    'count' => $counts,
                ];
            }
            if (User::info() && User::info()->type == 1) {
                $cost = $item['obj']->cost_opt;
            } else if (User::info() && User::info()->type == 2) {
                $cost = $item['obj']->cost_drop;
            } else {
                $cost = $item['obj']->cost;
            }
            $price += $item['count'] * $cost;
        }

        $html = Widgets::get('Cart', ['cart' => $result, 'totalCount' => Cart::factory()->_count_goods,
            'price' => $price]);
        $this->success([
            'count' => count($result),
            'add' => true,
            'html' => $html,
            'remove' => false
        ]);
    }

    // Add item to cart
    public function addToCartOrderAction()
    {
        // Get and check incoming data
        $catalog_id = Arr::get($this->post, 'id');
        if (!$catalog_id) {
            $this->error('No such item!');
        }

        $currency = Cookie::getWithoutSalt('currency');
        $_currency = DB::select()->from('currencies_courses')->find_all()->as_array('id');
        $_currentCurrency = DB::select()->from('currencies_courses')->where('currency', '=', $currency)->find();

        $sizes = Arr::get($this->post, 'sizes', 0);


        if ($sizes == 0) {
            $sizes = DB::select()->from('catalog_size_amount')->where('catalog_id', '=', $catalog_id)->where('amount', '>', 0)->execute();
            $all_sizes = DB::select()->from('catalog_size_amount')->where('catalog_id', '=', $catalog_id)->execute();
            if (count($all_sizes) && !count($sizes)) {
                $counts = 0;
                $message = 'К сожалению товар, который вы хотите добавить, закончился. Обратитесь к менеджеру для решения этой проблемы.';
            } elseif (count($all_sizes) && count($sizes)) {

                $item = DB::select()->from('carts_items')
                    ->where('catalog_id', '=', $catalog_id)
                    ->where('cart_id', '=', Cart::factory()->_cart_id)
                    ->find();
                if (!$item) {
                    $counts = (count($sizes)) ?: 1;
                } else {
                    $counts = 0;
                    foreach ($sizes as $size) {
                        $check = DB::select()->from('carts_items_sizes')->where('cart_item_id', '=', $item->id)->where('size', '=', $size['size'])->find();
                        $counts += (($check->count + 1) <= $size['amount']) ? 1 : 0;
                    }
                    if ($counts == 0) {
                        $message = 'Вы добавили максимальное число товаров';
                    }
                }

            } else {
                $size_count = $counts = Arr::get($this->post, 'count', 1);
            }
        } else {
            $sizes_arr = explode(',', $sizes);
            $counts_arr = explode(',', Arr::get($this->post, 'counts', 0));
            $counts = 0;


            $sizes = DB::select()->from('catalog_size_amount')->where('catalog_id', '=', $catalog_id)->where('amount', '>', 0)->execute();
            $all_sizes = DB::select()->from('catalog_size_amount')->where('catalog_id', '=', $catalog_id)->execute();
            if (count($all_sizes) && !count($sizes)) {
                $counts = 0;
                $message = 'К сожалению товар, который вы хотите добавить, закончился. Обратитесь к менеджеру для решения этой проблемы.';
            } else {
                $sizes = [];
                foreach ($sizes_arr as $index => $size) {

                    if ($counts_arr[$index] != 0) {

                        $db = DB::select()->from('catalog_size_amount')
                            ->where('catalog_id', '=', $catalog_id)
                            ->where('amount', '>', 0)
                            ->where('size', '=', $size)
                            ->find();

                        $item = DB::select()->from('carts_items')
                            ->where('catalog_id', '=', $catalog_id)
                            ->where('cart_id', '=', Cart::factory()->_cart_id)
                            ->find();
                        if ($item) {
                            $check = DB::select()->from('carts_items_sizes')->where('cart_item_id', '=', $item->id)->where('size', '=', $size)->find();
                            $counts_arr[$index] = (($check->count + $counts_arr[$index]) <= $db->amount) ? $counts_arr[$index] : 0;
                        }

                        $sizes[$index]['size'] = $size;
                        $sizes[$index]['count'] = $counts_arr[$index];
                        $sizes[$index]['amount'] = $db->amount;

                        $counts += $counts_arr[$index];
                        if ($counts == 0) {
                            $message = 'Вы добавили максимальное число товаров';
                        }
                    }
                }
            }
        }

        if ($counts == 0) {
            if (!$message) {
                $message = 'Выберите размер и количество для этого продукта';
            }
            $this->error([
                'response' => $message
            ]);
        }

        $item = Items::getRow($catalog_id, 'id');

        if ($item->catalog_color_group_id) {
            $items = DB::select()
                ->from('catalog')
                ->where('catalog_color_group_id', '=', $item->catalog_color_group_id)
                ->where('catalog.status', '=', 1)
                ->where('id', '!=', $item->id)
                ->find_all();

            foreach ($items as $obj) {
                $counts_new = 0;
                $sizes_new = DB::select()->from('catalog_size_amount')->where('catalog_id', '=', $obj->id)->where('amount', '>', 0)->execute();
                $counts_new = (count($sizes_new)) ?: 1;
                Cart::factory()->add($obj->id, $counts_new);
                $item_new = DB::select()->from('carts_items')
                    ->where('catalog_id', '=', $obj->id)
                    ->where('cart_id', '=', Cart::factory()->_cart_id)
                    ->find();

                foreach ($sizes_new as $size) {
                    if (!empty($size['count'])) {
                        $count = $size['count'];
                    } else {
                        $count = 1;
                    }
                    $check = DB::select()->from('carts_items_sizes')->where('cart_item_id', '=', $item_new->id)->where('size', '=', $size['size'])->find();
                    if ($check && count(get_object_vars($check))) {
                        Common::factory('carts_items_sizes')->update([
                            'size' => $size['size'],
                            'count' => (($check->count + $count) <= $size['amount']) ? $check->count + $count : $size['amount']
                        ], $check->id, 'id');
                    } else {
                        Common::factory('carts_items_sizes')->insert([
                            'cart_item_id' => $item_new->id,
                            'size' => $size['size'],
                            'count' => ($count <= $size['amount']) ? $count : $size['amount']
                        ]);
                    }
                }
            }
            Cart::factory()->add($catalog_id, $counts);
            $item = DB::select()->from('carts_items')
                ->where('catalog_id', '=', $catalog_id)
                ->where('cart_id', '=', Cart::factory()->_cart_id)
                ->find();

            foreach ($sizes as $size) {
                if (!empty($size['count'])) {
                    $count = $size['count'];
                } elseif ($size_count) {
                    $count = $size_count;
                } else {
                    $count = 1;
                }
                $check = DB::select()->from('carts_items_sizes')->where('cart_item_id', '=', $item->id)->where('size', '=', $size['size'])->find();
                if ($check && count(get_object_vars($check))) {
                    Common::factory('carts_items_sizes')->update([
                        'size' => $size['size'],
                        'count' => (($check->count + $count) <= $size['amount']) ? $check->count + $count : $size['amount']
                    ], $check->id, 'id');
                } else {
                    Common::factory('carts_items_sizes')->insert([
                        'cart_item_id' => $item->id,
                        'size' => $size['size'],
                        'count' => ($count <= $size['amount']) ? $count : $size['amount']
                    ]);
                }
            }
        } else {
            Cart::factory()->add($catalog_id, $counts);
            $item = DB::select()->from('carts_items')
                ->where('catalog_id', '=', $catalog_id)
                ->where('cart_id', '=', Cart::factory()->_cart_id)
                ->find();

            foreach ($sizes as $size) {
                if (!empty($size['count'])) {
                    $count = $size['count'];
                } elseif ($size_count) {
                    $count = $size_count;
                } else {
                    $count = 1;
                }
                $check = DB::select()->from('carts_items_sizes')->where('cart_item_id', '=', $item->id)->where('size', '=', $size['size'])->find();
                if (count($check)) {
                    Common::factory('carts_items_sizes')->update([
                        'size' => $size['size'],
                        'count' => (($check->count + $count) <= $size['amount']) ? $check->count + $count : $size['amount']
                    ], $check->id, 'id');
                } else {
                    Common::factory('carts_items_sizes')->insert([
                        'cart_item_id' => $item->id,
                        'size' => $size['size'],
                        'count' => ($count <= $size['amount']) ? $count : $size['amount']
                    ]);
                }
            }
        }

        $result = Cart::factory()->get_list_for_basket();
        $cart = [];
        $price = 0;
        foreach ($result as $item) {
            $obj = Arr::get($item, 'obj');
            if ($obj) {
                if (User::info() && User::info()->type == 1) {
                    $cost = $obj->cost_opt;
                } else if (User::info() && User::info()->type == 2) {
                    $cost = $obj->cost_drop;
                } else {
                    $cost = $obj->cost;
                }
                $cart[] = [
                    'id' => $obj->id,
                    'name' => $obj->name,
                    'cost' => $cost,
                    'image' => is_file(HOST . HTML::media('images/catalog/medium/' . $obj->image, false)) ? HTML::media('images/catalog/medium/' . $obj->image) : '',
                    'alias' => $obj->alias,
                    'count' => $counts,
                ];
            }
            $priceData = Support::calculatePriceWithCurrency($item,$_currency,$_currentCurrency);
            $cost = $priceData['cost'];
            $price += $item['count'] * $cost;
        }

        $html = Widgets::get('Cart', ['cart' => $result, 'totalCount' => Cart::factory()->_count_goods,
            'price' => $price]);
        $this->success([
            'count' => count($result),
            'add' => true,
            'html' => $html,
            'remove' => false
        ]);
    }

    // Edit count items in the cart
    public function editCartCountItemsAction()
    {
        // Get and check incoming data
        $catalog_id = Arr::get($this->post, 'id', 0);
        if (!$catalog_id) {
            $this->error('No such item!');
        }
        $count = Arr::get($this->post, 'count', 0);
        if (!$count) {
            $this->error('Can\'t change to zero!');
        }
        // Edit count items in cirrent position
        Cart::factory()->edit($catalog_id, $count);
        $this->success(['count' => (int)Cart::factory()->_count_goods]);
    }


    // Delete item from the cart
    public function deleteItemFromCartAction()
    {
        // Get and check incoming data
        $catalog_id = Arr::get($this->post, 'id', 0);
        if (!$catalog_id) {
            $this->error('No such item!');
        }
        $cart_id = Cart::factory()->_cart_id;
        // Add one item to cart
        Cart::factory()->delete($catalog_id);

        $currency = Cookie::getWithoutSalt('currency');
        $_currency = DB::select()->from('currencies_courses')->find_all()->as_array('id');
        $_currentCurrency = DB::select()->from('currencies_courses')->where('currency', '=', $currency)->find();

        $result = Cart::factory()->get_list_for_basket();
        $price = 0;
        foreach ($result as $item) {
            $priceData = Support::calculatePriceWithCurrency($item['obj'],$_currency,$_currentCurrency);
            $cost = $priceData['cost'];
            $price += $item['count'] * $cost;
        }

        if ($currency != $_currentCurrency->id) {
            if ($_currentCurrency->currency == 'UAH') {
                if ($currency == 'USD') {
                    $price = round($price * $_currency['USD']->course, 2);
                } else {
                    $price = round($price * $_currentCurrency->course, 2);
                }
            } elseif ($_currentCurrency->currency == 'RUB'){
                if ($currency == 'USD') {
                    $price = round($price * $_currency['USD']->course, 2);
                }
                $price = round($price * $_currentCurrency->course, 2);
            } else {
                $price = round($price / $_currentCurrency->course, 2);
            }
        }

        $html = Widgets::get('Cart/ItemList', ['cart' => $result, 'totalCount' => Cart::factory()->_count_goods,
            'price' => $price]);

        $this->success([
            'count' => count($result),
            'static' => $html,
            'last' => (Cart::factory()->_count_goods) ? false : true,
            'totalPrice' => $price
        ]);
    }

    // Delete item from the cart
    public function deleteItemFromCartPopupAction()
    {
        // Get and check incoming data
        $catalog_id = Arr::get($this->post, 'id', 0);
        if (!$catalog_id) {
            $this->error('No such item!');
        }
        $cart_id = Cart::factory()->_cart_id;
        // Add one item to cart
        Cart::factory()->delete($catalog_id);

        $currency = Cookie::getWithoutSalt('currency');
        $_currency = DB::select()->from('currencies_courses')->find_all()->as_array('id');
        $_currentCurrency = DB::select()->from('currencies_courses')->where('currency', '=', $currency)->find();

        $result = Cart::factory()->get_list_for_basket();
        $price = 0;
        foreach ($result as $item) {
            $priceData = Support::calculatePriceWithCurrency($item['obj'],$_currency,$_currentCurrency);
            $cost = $priceData['cost'];
            $price += $item['count'] * $cost;
        }

        $html = Widgets::get('Cart', ['cart' => $result, 'totalCount' => Cart::factory()->_count_goods,
            'price' => $price]);

        $this->success([
            'count' => count($result),
            'html' => $html,
            'price' => $price
        ]);
    }

    public function addToFavoriteAction()
    {
        // Get and check incoming data
        $catalog_id = Arr::get($this->post, 'id', 0);
        if (!$catalog_id) {
            $this->error('No such item!');
        }
        $check = Favorite::getFavorite(User::info()->id, $catalog_id);

        if ($check) {
            //Delete from favorite
            Favorite::removeFromFavorite($check->id);
            $count = Favorite::favoriteCount(User::info()->id);
            $this->success([
                'add' => false,
                'remove' => true,
                'count' => $count
            ]);
        } else {
            // Add one item to cart
            Favorite::addToFavorite(User::info()->id, $catalog_id);
            $count = Favorite::favoriteCount(User::info()->id);
            $this->success([
                'add' => true,
                'remove' => false,
                'count' => $count
            ]);
        }

    }

    // User authorization
    public function loginAction()
    {
        $email = Arr::get($this->post, 'email');
        if (!$email or !filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $this->error('Вы неверно ввели E-Mail!');
        }
        $password = Arr::get($this->post, 'password');
        $remember = Arr::get($this->post, 'remember');
        if (!$password) {
            $this->error('Вы не ввели пароль!');
        }
        // Check user for existance and ban
        $user = User::factory()->get_user_by_email($email, $password);
        if (!$user) {
            $this->error('Вы допустили ошибку в логине и/или пароле!');
        }
        if (!$user->status) {
            $this->error('Пользователь с указанным E-Mail адресом либо заблокирован либо не активирован. Пожалуйста обратитесь к Администратору для решения сложившейся ситуации');
        }

        // Authorization of the user
        DB::update('users')->set(['last_login' => time(), 'logins' => (int)$user->logins + 1, 'updated_at' => time()])->where('id', '=', $user->id)->execute();
        User::factory()->auth($user, $remember);
        Message::GetMessage(1, 'Вы успешно авторизовались на сайте!', 3500);
        $this->success(['redirect' => '/cart/ordering', 'noclear' => 1]);
    }

    public function cartAction()
    {
        $currency = Cookie::getWithoutSalt('currency');
        $_currency = DB::select()->from('currencies_courses')->find_all()->as_array('id');
        $_currentCurrency = DB::select()->from('currencies_courses')->where('currency', '=', $currency)->find();

        $cart = Cart::factory()->get_list_for_basket();
        $price = 0;
        $counts = [];
        foreach ($cart as $item) {
            if ($item['count'] == '0') {
                Cart::factory()->delete($item['id']);
            }
            foreach ($item['obj']->sizes as $size) {
                $counts[$size->size] = $size->count;
            }
            $priceData = Support::calculatePriceWithCurrency($item['obj'],$_currency,$_currentCurrency);
            $cost = $priceData['cost'];
            $price += $item['count'] * $cost;
        }

        $html = Widgets::get('Cart', ['cart' => $cart, 'totalCount' => Cart::factory()->_count_goods,
            'price' => $price, 'counts' => $counts]);

        $this->success([
            'html' => $html,
            'count' => count($cart),
            'price' => $price,
            'currency' => $_currentCurrency->currency

        ]);
    }

    public function cartSizesAction()
    {

        $from = Arr::get($this->post, 'from', 1);
        $color = Arr::get($this->post, 'color', false);
        $wholeSale = Arr::get($this->post,'wholeSale');
        $currency = Cookie::getWithoutSalt('currency');
        $_currency = DB::select()->from('currencies_courses')->find_all()->as_array('currency');
        $_currentCurrency = DB::select()->from('currencies_courses')->where('currency', '=', $currency)->find();

        if ($from == 1):
            $catalog_id = Arr::get($this->post, 'catalog_id');
            if (!$catalog_id) {
                $this->error('No such item!');
            }

            $sizes = Arr::get($this->post, 'count', 0);
            $sizes_arr = [];
            $counts_arr = [];
            foreach ($sizes as $size => $count){
                $sizes_arr[$size] = $size;
                $counts_arr[$size] = $count;
            }
            // Validate product available
            if ($sizes == 0) {
                $sizes = DB::select()->from('catalog_size_amount')->where('catalog_id', '=', $catalog_id)->where('amount', '>', 0)->execute();
                $all_sizes = DB::select()->from('catalog_size_amount')->where('catalog_id', '=', $catalog_id)->execute();
                if (count($all_sizes) && !count($sizes)) {
                    $counts = 0;
                    $message = 'К сожалению товар, который вы хотите добавить, закончился. Обратитесь к менеджеру для решения этой проблемы.';
                } elseif (count($all_sizes) && count($sizes)) {
                    $count = Arr::get($this->post, 'count', 1);
                    $item = DB::select()->from('carts_items')
                        ->where('catalog_id', '=', $catalog_id)
                        ->where('cart_id', '=', Cart::factory()->_cart_id)
                        ->find();
                    if (!$item) {
                        if (count($sizes)> 1 || $sizes[0]['size'] != '-') {
                            //$count = count($sizes);
                            $size_count = 1;
                        } else {
                            //$count = ($count)?:1;
                            $size_count = 1;
                        }
                        $counts = $count;
                    } else {
                        $counts = 0;
                        foreach ($sizes as $size) {
                            $check = DB::select()->from('carts_items_sizes')->where('cart_item_id', '=', $item->id)->where('size', '=', $size['size'])->find();
                            $counts += (($check->count + $count) <= $size['amount']) ? (int)$count : $size['amount'] - $check->count;
                        }
                        if ($counts == 0) {
                            $message = 'Вы добавили максимальное число товаров';
                        }
                    }
                } else {
                    $size_count = $counts = Arr::get($this->post, 'count', 1);
                }
            } else {
                $counts = 0;
                $sizes = DB::select()->from('catalog_size_amount')->where('catalog_id', '=', $catalog_id)->where('amount', '>', 0)->execute();
                $all_sizes = DB::select()->from('catalog_size_amount')->where('catalog_id', '=', $catalog_id)->execute();
                if (count($all_sizes) && !count($sizes)) {
                    $counts = 0;
                    $message = 'К сожалению товар, который вы хотите добавить, закончился. Обратитесь к менеджеру для решения этой проблемы.';
                } else {
                    $sizes = [];
                    foreach ($sizes_arr as $index => $size) {

                        if ($counts_arr[$index] != 0) {

                            $db = DB::select()->from('catalog_size_amount')
                                ->where('catalog_id', '=', $catalog_id)
                                ->where('amount', '>', 0)
                                ->where('size', '=', $size)
                                ->find();

                            $item = DB::select()->from('carts_items')
                                ->where('catalog_id', '=', $catalog_id)
                                ->where('cart_id', '=', Cart::factory()->_cart_id)
                                ->find();
                            if ($item) {
                                $check = DB::select()->from('carts_items_sizes')->where('cart_item_id', '=', $item->id)->where('size', '=', $size)->find();
                                $counts_arr[$index] = (($check->count + $counts_arr[$index]) <= $db->amount) ? $counts_arr[$index] : 0;
                            }

                            $sizes[$index]['size'] = $size;
                            $sizes[$index]['count'] = $counts_arr[$index];
                            $sizes[$index]['amount'] = $db->amount;

                            $counts += $counts_arr[$index];
                            if ($counts == 0) {
                                $message = 'Вы добавили максимальное число товаров';
                            }
                        }
                    }
                }
            }

            if ($counts == 0) {
                if (!$message) {
                    $message = 'Выберите размер и количество для этого продукта';
                }
                $this->error([
                    'response' => $message
                ]);
            }

            // Add one item to cart
            Cart::factory()->add($catalog_id, $counts);

            $item = DB::select()->from('carts_items')
                ->where('catalog_id', '=', $catalog_id)
                ->where('cart_id', '=', Cart::factory()->_cart_id)
                ->find();

            foreach ($sizes as $size) {
                if (!empty($size['count'])) {
                    $count = $size['count'];
                } elseif ($size_count) {
                    $count = $size_count;
                }

                $check = DB::select()->from('carts_items_sizes')->where('cart_item_id', '=', $item->id)->where('size', '=', $size['size'])->find();
                if ($check && count(get_object_vars($check))) {
                    Common::factory('carts_items_sizes')->update([
                        'size' => $size['size'],
                        'count' => (($check->count + $count) <= $size['amount']) ? $check->count + $count : $size['amount']
                    ], $check->id, 'id');
                } else {
                    Common::factory('carts_items_sizes')->insert([
                        'cart_item_id' => $item->id,
                        'size' => $size['size'],
                        'count' => ($count <= $size['amount']) ? $count : $size['amount']
                    ]);
                }
            }

            $result = Cart::factory()->get_list_for_basket();
            $cart = [];
            $price = 0;
            foreach ($result as $item) {
                $obj = Arr::get($item, 'obj');

                if ($obj) {
                    if (User::info() && User::info()->type == 1) {
                        $cost = $obj->cost_opt;
                    } else if (User::info() && User::info()->type == 2) {
                        $cost = $obj->cost_drop;
                    } else {
                        $cost = $obj->cost;
                    }
                    $cart[] = [
                        'id' => $obj->id,
                        'name' => $obj->name,
                        'cost' => $cost,
                        'image' => is_file(HOST . HTML::media('images/catalog/medium/' . $obj->image, false)) ? HTML::media('images/catalog/medium/' . $obj->image) : '',
                        'alias' => $obj->alias,
                        'count' => $counts,
                    ];
                }

                if (User::info() && User::info()->type == 1) {
                    $cost = $item['obj']->cost_opt;
                } else if (User::info() && User::info()->type == 2) {
                    $cost = $item['obj']->cost_drop;
                } else {
                    $cost = $item['obj']->cost;
                }

                $price += $item['count'] * $cost;
            }
//            $product = $item = Items::getRowSimple($catalog_id);
            $item_size = DB::select()->from('carts_items')
                ->where('cart_id', '=', Cart::factory()->_cart_id)
                ->where('catalog_id', '=', $catalog_id)
                ->find();

            if ($item_size){
                $sizes_carts = DB::select()->from('carts_items_sizes')
                    ->where('cart_item_id', '=', $item_size->id)
                    ->find_all();
                $active_sizes = ['null'];
                $active_count = [];
                foreach ($sizes_carts as $size_cart){
                    $active_sizes [] = $size_cart->size;
                    $active_count [$size_cart->size] = $size_cart->count;

                }
            }else{
                $active_count = [];
                $active_sizes = [];
            }

//            $html_sizes = Widgets::get('sizeUpdate', [
//                'sizes' => Items::getProductSizeAmountById($item->id)->as_array(),
//                'active_sizes'=>$active_sizes,
//                'active_count'=>$active_count,
//                'obj' => $product,
//
//            ]);
            $this->success([
//                'size-count'=>$html_sizes,
                'active_count'=>$active_count,
                'sizes' => $size,
                'counts' => $count,
                'wholeSale' => $wholeSale,
                "reset" => false,
                "clear" => false,
                "reload" => false,
                "redirect" => false,
                "response" => false,
                "close" => true,
                'count' => count($result),
                'add' => true,
                'remove' => false
            ]);
        elseif ($from == 2):
            $catalog_id = Arr::get($this->post, 'catalog_id');

            $counts = Arr::get($this->post, 'count');
            $count = $size = [];
            foreach ($counts as $key => $val) {
                $size_arr[$key] = $key;
                $count[$key] = $val;
            }

            $counts = 0;
            $sizes = [];
            foreach ($size_arr as $index => $size) {

                $db = DB::select()->from('catalog_size_amount')
                    ->where('catalog_id', '=', $catalog_id)
                    ->where('amount', '>', 0)
                    ->where('size', '=', $size)
                    ->find();
                $sizes[$index]['size'] = $size;
                $sizes[$index]['count'] = $count[$index];
                $sizes[$index]['amount'] = $db->amount;

                if ($count[$index] <= $db->amount) {
                    $counts += $count[$index];
                } else {
                    $counts += $db->amount;
                    $count[$index] = $db->amount;
                }
            }
            Cart::factory()->edit($catalog_id, $counts);

            $item = DB::select()->from('carts_items')
                ->where('catalog_id', '=', $catalog_id)
                ->where('cart_id', '=', Cart::factory()->_cart_id)
                ->find();

            foreach ($sizes as $size) {

                $check = DB::select()->from('carts_items_sizes')->where('cart_item_id', '=', $item->id)->where('size', '=', $size['size'])->find();
                if ($check && count(get_object_vars($check))) {
                    if ($size['count'] > 0) {
                        Common::factory('carts_items_sizes')->update([
                            'size' => $size['size'],
                            'count' => ($size['count'] <= $size['amount']) ? $size['count'] : $size['amount']
                        ], $check->id, 'id');
                    } else {
                        Common::factory('carts_items_sizes')->delete($check->id, 'id');
                    }
                } else {
                    if ($size['count'] > 0) {
                        Common::factory('carts_items_sizes')->insert([
                            'cart_item_id' => $item->id,
                            'size' => $size['size'],
                            'count' => (!empty($size['count'])) ? (int)$size['count'] : 1
                        ]);
                    }
                }
            }


            $result = Cart::factory()->get_list_for_basket();
            $price = 0;
            foreach ($result as $item) {
                $obj = Arr::get($item, 'obj');
                $data = Support::calculatePriceWithCurrency($obj,$_currency,$_currentCurrency);
                $cost = $data['cost'];
                $cost_old = $data['cost_old'];
                $currency = $data['currency'];
                $price += $item['count'] * $cost;
            }

            $html = Widgets::get('Cart', ['cart' => $result, 'totalCount' => Cart::factory()->_count_goods,
                'price' => $price, 'counts' => $count]);
            $this->success([
                'count' => count($result),
                'html' => $html,
                'wholeSale' => $wholeSale,
                "reset" => true,
                'cart' => true,
                'price' => $price,
                "close" => true
            ]);
        elseif ($from == 3):
            $catalog_id = Arr::get($this->post, 'catalog_id');

            $counts = Arr::get($this->post, 'count');

            if (!is_array($counts)) {
                $counts = explode(',', $counts);
            }
            $count = $size = [];
            foreach ($counts as $key => $val) {
                $size_arr[] = $key;
                $count[] = $val;
            }


            $counts = 0;
            $sizes = [];
            foreach ($size_arr as $index => $size) {

                $db = DB::select()->from('catalog_size_amount')
                    ->where('catalog_id', '=', $catalog_id)
                    ->where('amount', '>', 0)
                    ->where('size', '=', $size)
                    ->find();
                $sizes[$index]['size'] = $size;
                $sizes[$index]['count'] = $count[$index];
                $sizes[$index]['amount'] = $db->amount;

                if ($count[$index] <= $db->amount) {
                    $counts += $count[$index];
                } else {
                    $counts += $db->amount;
                    $count[$index] = $db->amount;
                }
            }
            Cart::factory()->edit($catalog_id, $counts);

            $item = DB::select()->from('carts_items')
                ->where('catalog_id', '=', $catalog_id)
                ->where('cart_id', '=', Cart::factory()->_cart_id)
                ->find();

            foreach ($sizes as $size) {

                $check = DB::select()->from('carts_items_sizes')->where('cart_item_id', '=', $item->id)->where('size', '=', $size['size'])->find();
                if (count($check)) {
                    if ($size['count'] > 0) {
                        Common::factory('carts_items_sizes')->update([
                            'size' => $size['size'],
                            'count' => ($size['count'] <= $size['amount']) ? $size['count'] : $size['amount']
                        ], $check->id, 'id');
                    } else {
                        Common::factory('carts_items_sizes')->delete($check->id, 'id');
                    }
                } else {
                    if ($size['count'] > 0) {
                        Common::factory('carts_items_sizes')->insert([
                            'cart_item_id' => $item->id,
                            'size' => $size['size'],
                            'count' => (!empty($size['count'])) ? (int)$size['count'] : 1
                        ]);
                    }
                }
            }




            $result = Cart::factory()->get_list_for_basket();
            $price = 0;
            foreach ($result as $item) {
                $obj = Arr::get($item, 'obj');
                $data = Support::calculatePriceWithCurrency($obj,$_currency,$_currentCurrency);
                $cost = $data['cost'];
                $cost_old = $data['cost_old'];
                $currency = $data['currency'];

                $price += $item['count'] * $cost;
            }

            $html = Widgets::get('Cart/ItemList', ['cart' => $result, 'totalCount' => Cart::factory()->_count_goods,
                'price' => $price, 'counts' => $count]);
            if ($currency != $_currentCurrency->id) {
                if ($_currentCurrency->currency === 'UAH') {
                    if ($currency === 'USD') {
                        $price = round($price * $_currency['USD']->course, 2);
                    } else {
                        $price = round($price * $_currentCurrency->course, 2);
                    }
                } elseif ($_currentCurrency->currency === 'RUB'){
                    if ($currency === 'USD') {
                        $price = round($price * $_currency['USD']->course, 2);
                    }
                    $price = round($price * $_currentCurrency->course, 2);
                } else {
                    $price = round($price / $_currentCurrency->course, 2);
                }
            }
            $this->success([
                'count' => count($result),
                "reset" => true,
                'cart' => true,
                'static' => $html,
                'wholeSale' => $wholeSale,
                'totalPrice' => $price,
                "close" => true
            ]);
        endif;

    }

    public function cartWithoutSizesAction()
    {
        $currency = Cookie::getWithoutSalt('currency');
        $_currency = DB::select()->from('currencies_courses')->find_all()->as_array('id');
        $_currentCurrency = DB::select()->from('currencies_courses')->where('currency', '=', $currency)->find();
        $from = Arr::get($this->post, 'from', 1);
        $wholeSale = Arr::get($this->post, 'wholeSale');
        if ($from == 1):
            $catalog_id = Arr::get($this->post, 'catalog_id');

            $count = Arr::get($this->post, 'count');
            if ($count > 0) {
                $db = DB::select()->from('catalog_size_amount')
                    ->where('catalog_id', '=', $catalog_id)
                    ->where('size', '=', '-')
                    ->find();
                if (!empty($db)) {
                    $item = DB::select()->from('carts_items')
                        ->where('catalog_id', '=', $catalog_id)
                        ->where('cart_id', '=', Cart::factory()->_cart_id)
                        ->find();
                    $check = DB::select()->from('carts_items_sizes')->where('cart_item_id', '=', $item->id)->where('size', '=', '-')->find();
                    if (count($check)) {
                        if ($count > 0) {
                            $count = ($count <= $db->amount) ? $count : $db->amount;
                            Common::factory('carts_items_sizes')->update([
                                'size' => '-',
                                'count' => $count
                            ], $check->id, 'id');
                        } else {
                            Common::factory('carts_items_sizes')->delete($check->id, 'id');
                        }
                    } else {
                        if ($count > 0) {
                            $count = (!empty($count)) ? (int)$count : 1;
                            Common::factory('carts_items_sizes')->insert([
                                'cart_item_id' => $item->id,
                                'size' => '-',
                                'count' => $count
                            ]);
                        }
                    }
                }
                Cart::factory()->edit($catalog_id, $count);
            } else {
                Cart::factory()->delete($catalog_id);
            }

            $result = Cart::factory()->get_list_for_basket();

            $price = 0;
            foreach ($result as $item) {
                $obj = Arr::get($item, 'obj');
                $data = Support::calculatePriceWithCurrency($obj,$_currency,$_currentCurrency);
                $cost = $data['cost'];
                $price += $item['count'] * $cost;
            }

            $html = Widgets::get('Cart', ['cart' => $result, 'totalCount' => Cart::factory()->_count_goods,
                'price' => $price, 'counts' => $count]);
            $this->success([
                'count' => count($result),
                'html' => $html,
                "reset" => true,
                'wholeSale' => $wholeSale,
                'cart' => true,
                'price' => $price,
                "close" => true
            ]);
        elseif ($from == 2):
            $catalog_id = Arr::get($this->post, 'catalog_id');

            $count = Arr::get($this->post, 'count');
            if ($count > 0) {
                $db = DB::select()->from('catalog_size_amount')
                    ->where('catalog_id', '=', $catalog_id)
                    ->where('size', '=', '-')
                    ->find();
                if (!empty($db)) {
                    $item = DB::select()->from('carts_items')
                        ->where('catalog_id', '=', $catalog_id)
                        ->where('cart_id', '=', Cart::factory()->_cart_id)
                        ->find();
                    $check = DB::select()->from('carts_items_sizes')->where('cart_item_id', '=', $item->id)->where('size', '=', '-')->find();
                    if (count($check)) {
                        if ($count > 0) {
                            $count = ($count <= $db->amount) ? $count : $db->amount;
                            Common::factory('carts_items_sizes')->update([
                                'size' => '-',
                                'count' => $count
                            ], $check->id, 'id');
                        } else {
                            Common::factory('carts_items_sizes')->delete($check->id, 'id');
                        }
                    } else {
                        if ($count > 0) {
                            $count = (!empty($count)) ? (int)$count : 1;
                            Common::factory('carts_items_sizes')->insert([
                                'cart_item_id' => $item->id,
                                'size' => '-',
                                'count' => $count
                            ]);
                        }
                    }
                }
                Cart::factory()->edit($catalog_id, $count);
            } else {
                Cart::factory()->delete($catalog_id);
            }



            $result = Cart::factory()->get_list_for_basket();
            $price = 0;
            foreach ($result as $item) {
                $obj = Arr::get($item, 'obj');
                $data = Support::calculatePriceWithCurrency($obj,$_currency,$_currentCurrency);
                $cost = $data['cost'];
                $price += $item['count'] * $cost;
            }


            $html = Widgets::get('Cart/ItemList', ['cart' => $result, 'totalCount' => Cart::factory()->_count_goods,
                'price' => $price, 'counts' => $count]);

            if ($currency != $_currentCurrency->id) {
                if ($_currentCurrency->currency == 'UAH') {
                    if ($currency == 'USD') {
                        $price = round($price * $_currency['USD']->course, 2);
                    } else {
                        $price = round($price * $_currentCurrency->course, 2);
                    }
                } elseif ($_currentCurrency->currency == 'RUB'){
                    if ($currency == 'USD') {
                        $price = round($price * $_currency['USD']->course, 2);
                    }
                    $price = round($price * $_currentCurrency->course, 2);
                } else {
                    $price = round($price / $_currentCurrency->course, 2);
                }
            }
            $this->success([
                'count' => count($result),
                "reset" => true,
                'cart' => true,
                'wholeSale' => $wholeSale,
                'static' => $html,
                'totalPrice' => $price,
                "close" => true
            ]);
        endif;
    }

    public function subscribeAction()
    {
        $email = Arr::get($this->post, 'email');
        $res = Subscribers::getRow($email, 'email');
        if ($res) {
            $this->error([
                "reset" => false,
                "clear" => false,
                "reload" => false,
                "redirect" => false,
                'response' => 'Пользователь с такой почтой уже подписан на рассылку'
            ]);
        }
        $hash = sha1(microtime() . Text::random());
        Subscribers::insert([
            'created_at' => time(),
            'email' => $email,
            'ip' => System::getRealIP(),
            'status' => 1,
            'hash' => $hash
        ]);
        Email::sendTemplate(2, [
            '{{site}}' => Arr::get($_SERVER, 'HTTP_HOST'),
            '{{ip}}' => GeoIP::ip(),
            '{{date}}' => date('d.m.Y'),
            '{{email}}' => $email,
            '{{link}}' => HTML::link('/unsubscribe/hash/' . $hash, true, 'http')
        ], $email);
        $this->success([
            "reset" => true,
            "clear" => true,
            "reload" => false,
            "redirect" => false,
            "response" => "Спасибо! Вы успешно подписались на рассылку!"
        ]);
    }

    public function likeAction()
    {
        $check = DB::select()->from('catalog_comments_rating')
            ->where('user_id', '=', User::info()->id)
            ->where('comment_id', '=', Arr::get($this->post, 'id'))
            ->find();
        if (count($check)) {
            if ($check->status == 1) {
                DB::delete('catalog_comments_rating')
                    ->where('user_id', '=', User::info()->id)
                    ->where('comment_id', '=', Arr::get($this->post, 'id'))
                    ->where('status', '=', 1)
                    ->execute();
                $result = DB::select([DB::expr('COUNT(id)'), 'dislikes'])->from('catalog_comments_rating')
                    ->where('comment_id', '=', Arr::get($this->post, 'id'))
                    ->where('status', '=', 2)
                    ->find();
                $result2 = DB::select([DB::expr('COUNT(id)'), 'likes'])->from('catalog_comments_rating')
                    ->where('comment_id', '=', Arr::get($this->post, 'id'))
                    ->where('status', '=', 1)
                    ->find();
                $this->success([
                    'dislikeCount' => (int)$result->dislikes,
                    'likeCount' => (int)$result2->likes
                ]);
            } else {
                DB::delete('catalog_comments_rating')
                    ->where('user_id', '=', User::info()->id)
                    ->where('comment_id', '=', Arr::get($this->post, 'id'))
                    ->where('status', '=', 2)
                    ->execute();
            }
        }
        $res = DB::insert('catalog_comments_rating', [
            'comment_id',
            'user_id',
            'user_phone',
            'status',
            'created_at',
        ])->values([
            Arr::get($this->post, 'id'),
            User::info()->id,
            User::info()->phone,
            1,
            time(),
        ])->execute();
        $result = DB::select([DB::expr('COUNT(id)'), 'likes'])->from('catalog_comments_rating')
            ->where('comment_id', '=', Arr::get($this->post, 'id'))
            ->where('status', '=', 1)
            ->find();

        $result2 = DB::select([DB::expr('COUNT(id)'), 'likes'])->from('catalog_comments_rating')
            ->where('comment_id', '=', Arr::get($this->post, 'id'))
            ->where('status', '=', 1)
            ->find();
        $this->success([
            'dislikeCount' => (int)$result->dislikes,
            'likeCount' => (int)$result2->likes
        ]);
    }

    public function dislikeAction()
    {
        $check = DB::select()->from('catalog_comments_rating')
            ->where('user_id', '=', User::info()->id)
            ->where('comment_id', '=', Arr::get($this->post, 'id'))
            ->find();
        if (count($check)) {
            if ($check->status == 2) {
                DB::delete('catalog_comments_rating')
                    ->where('user_id', '=', User::info()->id)
                    ->where('comment_id', '=', Arr::get($this->post, 'id'))
                    ->where('status', '=', 2)
                    ->execute();
                $result = DB::select([DB::expr('COUNT(id)'), 'dislikes'])->from('catalog_comments_rating')
                    ->where('comment_id', '=', Arr::get($this->post, 'id'))
                    ->where('status', '=', 2)
                    ->find();
                $result2 = DB::select([DB::expr('COUNT(id)'), 'likes'])->from('catalog_comments_rating')
                    ->where('comment_id', '=', Arr::get($this->post, 'id'))
                    ->where('status', '=', 1)
                    ->find();
                $this->success([
                    'dislikeCount' => (int)$result->dislikes,
                    'likeCount' => (int)$result2->likes
                ]);
            } else {
                DB::delete('catalog_comments_rating')
                    ->where('user_id', '=', User::info()->id)
                    ->where('comment_id', '=', Arr::get($this->post, 'id'))
                    ->where('status', '=', 1)
                    ->execute();
            }

        }
        $res = DB::insert('catalog_comments_rating', [
            'comment_id',
            'user_id',
            'user_phone',
            'status',
            'created_at',
        ])->values([
            Arr::get($this->post, 'id'),
            User::info()->id,
            User::info()->phone,
            2,
            time(),
        ])->execute();
        $result = DB::select([DB::expr('COUNT(id)'), 'dislikes'])->from('catalog_comments_rating')
            ->where('comment_id', '=', Arr::get($this->post, 'id'))
            ->where('status', '=', 2)
            ->find();

        $result2 = DB::select([DB::expr('COUNT(id)'), 'likes'])->from('catalog_comments_rating')
            ->where('comment_id', '=', Arr::get($this->post, 'id'))
            ->where('status', '=', 1)
            ->find();
        $this->success([
            'dislikeCount' => (int)$result->dislikes,
            'likeCount' => (int)$result2->likes
        ]);
    }

    public function reviewAction()
    {
        $name = Arr::get($this->post, 'name');
        $phone = Arr::get($this->post, 'phone');
        $message = Arr::get($this->post, 'message');
        $advantages = Arr::get($this->post, 'advantages');
        $disadvantages = Arr::get($this->post, 'disadvantages');
        $rating = Arr::get($this->post, 'rating');

        $id = Arr::get($this->post, 'id');

        $ip = System::getRealIP();
        $check = DB::select([DB::expr('catalog_comments.id'), 'count'])
            ->from('catalog_comments')
            ->where('ip', '=', $ip)
            ->where('catalog_id', '=', $id)
            ->where('created_at', '>', time() - 60)
            ->as_object()->execute()->current();
        if (is_object($check) AND $check->count) {
            $this->error('Вы только что оставили отзыв об этом товаре! Пожалуйста, повторите попытку через минуту');
        }

        $item = DB::select('alias', 'name', 'catalog.id')
            ->from('catalog')
            ->join('catalog_i18n', 'INNER')
            ->on('catalog.id', '=', 'catalog_i18n.id')
            ->where('status', '=', 1)
            ->where('catalog.id', '=', $id)
            ->as_object()
            ->execute()
            ->current();

        $keys = ['name', 'catalog_id', 'phone', 'text', 'status', 'date', 'created_at', 'ip', 'rate', 'advantages', 'disadvantages'];
        $values = [$name, $id, $phone, $message, 0, time(), time(), $ip, $rating, $advantages, $disadvantages];
        $lastID = DB::insert('catalog_comments', $keys)->values($values)->execute();
        $lastID = Arr::get($lastID, 0);

        // Create links
        $link = 'http://' . Arr::get($_SERVER, 'HTTP_HOST') . '/' . $item->alias . '/p' . $item->id;
        $link_admin = 'http://' . Arr::get($_SERVER, 'HTTP_HOST') . '/wezom/items/edit/' . $item->id;


        //Save log
        $qName = 'Отзыв к товару';
        $url = '/wezom/comments/edit/' . $lastID;
        Log::add($qName, $url, 6);

        // Send message to admin if need
        Email::sendTemplate(7, [
            '{{site}}' => Arr::get($_SERVER, 'HTTP_HOST'),
            '{{ip}}' => $ip,
            '{{date}}' => date('d.m.Y H:i'),
            '{{name}}' => $name,
            '{{phone}}' => $phone,
            '{{text}}' => $message,
            '{{link}}' => $link,
            '{{admin_link}}' => $link_admin,
            '{{item_name}}' => $item->name
        ]);

        $this->success([
            'reset' => true,
            'clear' => true,
            'response' => __('Спасибо за отзыв. После модерации он будет добавлен на сайт.'),
        ]);
    }


    public function searchAction()
    {
        $arr = [];
        $items = DB::select('catalog.*','product_prices.price',
            'product_prices.price_old',
            'product_prices.currency_id', 'catalog_i18n.name')
            ->from('catalog')
            ->join('product_prices')
            ->on('catalog.id','=','product_prices.product_id')
            ->join('catalog_i18n')
            ->on('catalog.id','=', 'catalog_i18n.row_id')
            ->where('status', '=', 1)
            ->where('product_prices.price_type', '=', $_SESSION['prices'])
            ->where('catalog_i18n.language', '=',  \I18n::lang())
            ->and_where_open()
            ->where('catalog_i18n.name', 'LIKE', '%' . urldecode($_SERVER['QUERY_STRING']) . '%')
            ->or_where('catalog.id', 'LIKE', '%' . urldecode($_SERVER['QUERY_STRING']) . '%')
            ->and_where_close()
            ->limit(10)
            ->execute();

        $currency = Cookie::getWithoutSalt('currency');
        $_currency = DB::select()->from('currencies_courses')->find_all()->as_array('id');
        $_currentCurrency = DB::select()->from('currencies_courses')->where('currency', '=', $currency)->find();
        if (count($items)) {
            foreach ($items as $item) {
                $data = Support::calculatePriceWithCurrency($item,$_currency,$_currentCurrency);
                $cost = $data['cost'];
                $currency = $data['currency'];
                if (is_file(HOST . HTML::media('images/catalog/original/' . $item['image'], false))) {
                    $img = HTML::media('images/catalog/original/' . $item['image']);
                } else {
                    $img = HTML::media('assets/images/placeholders/no-image.jpg');
                }
                $arr[] = [
                    'name' => $item['name'],
                    'code' => $item['artikul'],
                    'price' => $cost . ' ' . $currency,
                    'img' => $img,
                    'href' => HTML::link($item['alias'] . '/p' . $item['id']),
                ];

            }

        }
        $cat = DB::select()->from('catalog_tree')
            ->join('catalog_tree_i18n')->on('catalog_tree.id', '=', 'catalog_tree_i18n.row_id')
            ->where('catalog_tree_i18n.language', '=', \I18n::lang())
            ->where('status', '=', 1)
            ->where('catalog_tree_i18n.name', 'LIKE', '%' . urldecode($_SERVER['QUERY_STRING']) . '%')
            ->execute();
        if (count($cat)){
            foreach ($cat as $item){
                $arr[] = [
                    'name' => 'Перейти в категорию <b><i>' . $item['name'] . '</i></b>',
                    'code' => false,
                    'price' => false,
                    'img' => false,
                    'href' => HTML::link('catalog/'.$item['alias'] ),
                ];
            }
        }
        $this->success([
            "items" => $arr,
        ]);
    }

    public function searchFilterAction()
    {
        $search = Arr::get($this->post, 'search');
        if (!$search) {
            $this->error([
                'response' => "Введите слово для поиска"
            ]);
        }
        $this->success([
            'redirect' => HTML::link('/search?search=' . $search)
        ]);
    }

    public function reviewsAction()
    {
        $offset = (Arr::get($this->post, 'page') - 1) * 3;
        $reviews = Items::getReviews(Arr::get($this->post, 'id'));
        $pageReviews = Items::getPagedReviews(Arr::get($this->post, 'id'), 3, $offset);
        Route::factory()->setParam('id', Arr::get($this->post, 'id'));
        $pager = Pager::factory(Arr::get($this->post, 'page'), count($reviews), 3);
        $html = Widgets::get('productReviews', ['reviews' => $pageReviews, 'pager' => $pager->create_review()]);

        $this->success([
            'params' => Route::params(),
            'html' => $html
        ]);

    }

    public function cartClearAction()
    {
        Cart::factory()->clearCart(Cart::factory()->_cart_id);
        $html = Widgets::get('Cart');
        $this->success([
            'close' => true,
            'cart_id' => Cart::factory()->_cart_id,
            'count' => 0,
            'html' => $html
        ]);
    }

    public function sortAction()
    {
        $headers = Arr::get($this->post, 'val');
        $headers = json_decode(str_replace('&quot;', '"', $headers));
        Route::factory()->setParam('group', $headers->group_id);
        $sort_type = $headers->value;
        $fav = [];
        if (\Core\User::info()) {
            $favs = Favorite::getFavorites(\Core\User::info()->id);
            foreach ($favs as $f) {
                $fav[] = (int)$f->id;
            }
        }

        if ($sort_type == 2) {
            $sort = 'views';
        } else if ($sort_type == 3) {
            $sort = 'name';
        } else {
            $sort = 'cost';
        }
        $limit = Config::get('basic.limit_groups');
        $type = 'ASC';
        if (!empty($headers->search)) {
            $search = Items::getQueries($headers->search);
        } else {
            $search = null;
        }
        if ($headers->page_params->filter) {
            $filter = Filter::getFilterArr($headers->page_params->filter);
            Config::set('filter_array', $filter);
            if ($headers->group_id == 0) {
                if ($headers->alias == 'novelty') {
                    $headers->alias = 'new';
                } else if ($headers->alias == 'popular') {
                    $headers->alias = 'top';
                } else if ($headers->alias == 'search') {
                    $headers->alias = 'status';
                }
                Route::factory()->setParam('group', 0);
                $flag = $headers->alias;
                $result = Filter::getFilteredItemsListByFlag($flag, $limit, ($headers->page_params->page) ? ($headers->page_params->page - 1) * $limit : 0, $sort, $type, $search);
            } else {
                $result = Filter::getFilteredItemsList($limit, ($headers->page_params->page) ? ($headers->page_params->page - 1) * $limit : 0, $sort, $type, $search);
            }

        } else {
            if ($headers->group_id == 0) {
                if ($headers->alias == 'novelty') {
                    $headers->alias = 'new';
                } else if ($headers->alias == 'popular') {
                    $headers->alias = 'top';
                } else if ($headers->alias == 'search') {
                    $headers->alias = 'status';
                }
                $flag = $headers->alias;
                Route::factory()->setParam('group', 0);
                $result = Filter::getFullItemsListByFlag($flag, $limit, ($headers->page_params->page) ? ($headers->page_params->page - 1) * $limit : 0, $sort, $type, $search);
            } else {
                $result = Filter::getFullFilteredItemsList($limit, ($headers->page_params->page) ? ($headers->page_params->page - 1) * $limit : 0, $sort, $type, $search);
            }
        }
        $_SESSION['sort'] = $sort;
        $html = View::tpl(['result' => $result['items'], 'favorite' => $fav, 'cats' => $result['cats']], 'Catalog/ItemsListSort');
        $this->success(['html' => $html]);
    }

    public function loadMoreAction()
    {
        $last = false;
        $page = Arr::get($this->post, 'page', 1);
        $next_page = ((int)$page) + 1;
        $headers = Arr::get($this->post, 'params');
        Route::factory()->setParam('group', $headers['group_id']);

        $start_page = ($headers['page_params']['page']) ?: 1;
        if(isset($_SESSION['page_active'])){
            $page_active = 0;
        }else{
            $page_active = ($headers['page_active']) ?: 0;
        }


        $sort = ($_SESSION['sort']) ?: 'top';

        $fav = [];
        if (\Core\User::info()) {
            $favs = Favorite::getFavorites(\Core\User::info()->id);
            foreach ($favs as $f) {
                $fav[] = (int)$f->id;
            }
        }
        $limit = Arr::get($this->post, 'limit', Config::get('basic.limit_groups'));
        if($page_active !== 0 && $page_active !== 'null'){
            $offset = $limit;
            $limit *= ($page_active - 1);
            $_SESSION['page_active'] = 1;
        }else{
            $offset = ($page) ? $page * $limit : 0;
        }
        $type = 'ASC';

        if (!empty($headers['search'])) {
            $search = Items::getQueries($headers['search']);
        } else {
            $search = null;
        }

        if ($headers['group_id'] == 0) {
            if ($headers['alias'] == 'novelty') {
                $headers['alias'] = 'new';
            } else if ($headers['alias'] == 'popular') {
                $headers['alias'] = 'top';
            } else if ($headers['alias'] == 'search') {
                $headers['alias'] = 'status';
            }
            $flag = $headers['alias'];
        }

        if ($headers['page_params']['filter']) {
            $filter = Filter::getFilterArr($headers['page_params']['filter']);
            Config::set('filter_array', $filter);
            if ($filter and $filter['sort']){
                $sort = $filter['sort'];
            }
            if ($headers['group_id'] == 0) {
                $result = Filter::getFilteredItemsListByFlag($flag, $limit, $offset, $sort, $type, $search);
            } else {
                $result = Filter::getFilteredItemsList($limit, $offset, $sort, $type, null , false );
            }
        } else {
            if ($headers['group_id'] == 0) {
                $result = Filter::getFullItemsListByFlag($flag, $limit, $offset, $sort, $type, $search);
            } else {
                $result = Filter::getFullFilteredItemsList($limit, $offset, $sort, $type);
            }
        }
        $pager = Pager::factory($next_page, $result['total'], $limit, $start_page);
        if (($next_page * $limit) >= $result['total']) {
            $last = true;
        }
        $html = View::tpl(['result' => $result['items'], 'favorite' => $fav, 'cats' => $result['cats']], 'Catalog/ItemsListSort');


        if (((($next_page + 1) - (int)$start_page) * (int)$limit) <= $result['total']) {
            $total = (($next_page + 1) - (int)$start_page) * (int)$limit;
        } else {
            $total = $result['total'];
        }
        $this->success([
            'page' => $next_page,
            'cookie' => $page_active,
            'limit' => $limit,
            'offset' => $offset,
            'pagination' => $pager->create(),
            'last' => $last,
            'total' => $total,
            'html' => $html
        ]);
    }
    public function loadMoreBackAction()
    {
        $last = false;
        $page = Arr::get($this->post, 'page', 1);
        $next_page = ((int)$page);
        $start_page = Arr::get($this->post, 'endPage', 1);
        $headers = Arr::get($this->post, 'params');
        Route::factory()->setParam('group', $headers['group_id']);

        $start_page = ($headers['page_params']['page']) ?: 1;
        $end_page = $headers['endPage'];
        if(isset($_SESSION['page_active'])){
            $page_active = 0;
        }else{
            $page_active = ($headers['page_active']) ?: 0;
        }


        $sort = ($_SESSION['sort']) ?: 'top';

        $fav = [];
        if (\Core\User::info()) {
            $favs = Favorite::getFavorites(\Core\User::info()->id);
            foreach ($favs as $f) {
                $fav[] = (int)$f->id;
            }
        }
        //var_dump($end_page, $start_page); die();
        $limit = Config::get('basic.limit_groups');
        if($page_active !== 0 && $page_active !== 'null'){
            $offset = $limit;
            $limit *= ($start_page - $page);
            $_SESSION['page_active'] = 1;
        }else{
            $offset = $start_page * $limit;
            $limit *= ($end_page - $start_page);
        }
        $type = 'ASC';

        if (!empty($headers['search'])) {
            $search = Items::getQueries($headers['search']);
        } else {
            $search = null;
        }

        if ($headers['group_id'] == 0) {
            if ($headers['alias'] == 'novelty') {
                $headers['alias'] = 'new';
            } else if ($headers['alias'] == 'popular') {
                $headers['alias'] = 'top';
            } else if ($headers['alias'] == 'search') {
                $headers['alias'] = 'status';
            }
            $flag = $headers['alias'];
        }

        if ($headers['page_params']['filter']) {
            $filter = Filter::getFilterArr($headers['page_params']['filter']);
            Config::set('filter_array', $filter);
            if ($filter and $filter['sort']){
                $sort = $filter['sort'];
            }
            if ($headers['group_id'] == 0) {
                $result = Filter::getFilteredItemsListByFlag($flag, $limit, $offset, $sort, $type, $search);
            } else {
                $result = Filter::getFilteredItemsList($limit, $offset, $sort, $type, null , false );
            }
        } else {
            if ($headers['group_id'] == 0) {
                $result = Filter::getFullItemsListByFlag($flag, $limit, $offset, $sort, $type, $search);
            } else {
                $result = Filter::getFullFilteredItemsList($limit, $offset, $sort, $type);
            }
        }
        $pager = Pager::factory($next_page, $result['total'], Config::get('basic.limit_groups'), $start_page);
        if (($next_page * Config::get('basic.limit_groups')) >= $result['total']) {
            $last = true;
        }
        $html = View::tpl(['result' => $result['items'], 'favorite' => $fav, 'cats' => $result['cats']], 'Catalog/ItemsListSort');


        if (((($next_page + 1) - (int)$start_page) * (int)$limit) <= $result['total']) {
            $total = (($next_page + 1) - (int)$start_page) * (int)$limit;
        } else {
            $total = $result['total'];
        }
        $this->success([
            'page' => $next_page,
            'cookie' => $page_active,
            'limit' => $limit,
            'offset' => $offset,
            'pagination' => $pager->create(),
            'last' => $last,
            'total' => $total,
            'html' => $html
        ]);
    }

    public function prepareFilterAction() {
        $category_id = $_POST['category_id'];
        $filters = $_POST['filters'];
        $search = $_POST['search'];

        $count = Filter::count($category_id, $filters, $search);
        $url = Filter::generateFilterUrl($category_id, $filters, $search);

        $this->success(compact('count', 'url'));
    }


    public function addchildAction()
    {
        $sex = Arr::get($_POST, 'polrebenka');
        $age = Arr::get($_POST, 'age_selection');

        $this->success(compact('sex', 'age'));
    }

    public function costAction()
    {
        $min = Arr::get($this->post, 'mincost');
        $max = Arr::get($this->post, 'maxcost');

        if (strpos($_SERVER['HTTP_REFERER'], 'mincost') || strpos($_SERVER['HTTP_REFERER'], 'maxcost')) {

            $url = explode('/', $_SERVER['HTTP_REFERER']);
            for ($i = 0; $i < count($url); $i++) {
                if (strpos($url[$i], 'mincost') !== false) {
                    $mincost = explode('-', $url[$i]);
                    $mincost[1] = $min;
                    $url[$i] = implode('-', $mincost);
                }
                if (strpos($url[$i], 'maxcost') !== false) {
                    $maxcost = explode('-', $url[$i]);
                    $maxcost[1] = $max;
                    $url[$i] = implode('-', $maxcost);
                }
            }

            $this->success([
                'redirect' => implode('/', $url)
            ]);
        } else {
            $this->success([
                'redirect' => $_SERVER['HTTP_REFERER'] . '/mincost-' . $min . '/maxcost-' . $max
            ]);
        }
    }

    public function getNPCityAction()
    {
        $np = new NP();
        $city = Arr::get($this->post, 'cityOld');
        $cities = $np->getCities(Arr::get($this->post, 'value'));
        $city_html = '<option></option>';
        foreach ($cities as $key => $val) {
            $key == $city ? $selected = 'selected' : $selected = '';
            $city_html .= '<option '.$selected.' value="' . $key . '">' . $val . '</option>';
        }

        $warehouses_html = '<option></option>';

        $this->success([
            'items' => [
                [
                    'selector' => '.js-delivery-np-city',
                    'html' => $city_html
                ],
                [
                    'selector' => '.js-delivery-np-section',
                    'html' => $warehouses_html
                ]
            ]
        ]);
    }

    public function getNPSettlementsAction()
    {
        $np = new NP();
        $areaRef = Arr::get($this->post, 'areaRef');

        $settlements = $np->getSettlements($areaRef);

        $this->success(['result' => $settlements]);
    }

    public function searchStreetAction()
    {
        $np = new NP();
        $city = Arr::get($this->post, 'cityRef');
        $search = Arr::get($this->post, 'search');
        $streets = $np->searchStreet($search, $city);

        $this->success(['result' => $streets]);
    }

    public function getNPWarehouseAction()
    {
        $np = new NP();

        $warehouse = Arr::get($this->post, 'warehouseOld');
        $warehouses = $np->getWarehouses(Arr::get($this->post, 'value'));
        $html = '<option></option>';
        foreach ($warehouses as $key => $val) {
            $key == $warehouse ? $selected = 'selected' : $selected = '';
            $html .= '<option '.$selected.' value="' . $key . '">' . $val . '</option>';
        }

        $this->success([
            'items' => [
                [
                    'selector' => '.js-delivery-np-section',
                    'html' => $html
                ]
            ]
        ]);
    }

    /**
     * @remove_child
     * Удаление информации о ребёнке в личном кабинете
     * @template Views/User/Children.php
     */
    public function remove_childAction(): void
    {
        $this->error('Для редактирования информации о детях обратитесь, пожалуйста, к менеджеру' . '<br>' . Config::get('static.help_phone') .' - звонок бесплатный</br>' . Config::get('static.help_email'));

        $id = Arr::get($this->post, 'id');
        if(empty($id)){
        }
        $row = CustomerChildren::getRow($id);
        if(!$row){
            $this->error('Нечего удалять!');
        }
        CustomerChildren::delete($id);
        Message::GetMessage(1, 'Вы успешно удалили информацию!');
        $this->success(['reload' => true]);
    }
}
