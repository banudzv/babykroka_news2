<?php

namespace Modules\Ajax\Controllers;

use Core\Config;
use Core\Cookie;
use Core\Delivery\NP;
use Core\GeoIP;
use Core\HTML;
use Core\Payments\LiqPay;
use Core\QB\DB;
use Core\Arr;
use Core\Route;
use Core\Support;
use Core\User;
use Core\Config AS conf;
use Core\View;
use Core\System;
use Modules\Cart\Models\Cart;
use Core\Log;
use Core\Email;
use Core\Message;
use Core\Common;
use Modules\Ajax;
use Modules\Cart\Models\Orders;
use Modules\User\Models\CustomerChildren;
use Modules\User\Models\CustomerRoles;
use Wezom\Modules\User\Models\CustomerTypes;
use Wezom\Modules\User\Models\Users;

class Form extends Ajax
{

    protected $post;
    protected $files;

    function before()
    {
        parent::before();
        // Check for bans in blacklist
        $ip = GeoIP::ip();
        $ips = [];
        $ips[] = $ip;
        $ips[] = $this->ip($ip, [0]);
        $ips[] = $this->ip($ip, [1]);
        $ips[] = $this->ip($ip, [1, 0]);
        $ips[] = $this->ip($ip, [2]);
        $ips[] = $this->ip($ip, [2, 1]);
        $ips[] = $this->ip($ip, [2, 1, 0]);
        $ips[] = $this->ip($ip, [3]);
        $ips[] = $this->ip($ip, [3, 2]);
        $ips[] = $this->ip($ip, [3, 2, 1]);
        $ips[] = $this->ip($ip, [3, 2, 1, 0]);
        if (count($ips)) {
            $bans = DB::select('date')
                ->from('blacklist')
                ->where('status', '=', 1)
                ->where('ip', 'IN', $ips)
                ->and_where_open()
                ->or_where('date', '>', time())
                ->or_where('date', '=', NULL)
                ->and_where_close()
                ->find_all()->as_array();
            if (count($bans) && Route::action() !== 'callback') {
                $html = View::widget([], 'Popup/Message');
                $this->error(['widget' => true,'response' => $html,'message' => 'К сожалению это действие недоступно, т.к. администратор ограничил доступ к сайту с Вашего IP адреса!']);
            }
        }
    }

    private function ip($ip, $arr)
    {
        $_ip = explode('.', $ip);
        foreach ($arr AS $pos) {
            $_ip[$pos] = 'x';
        }
        $ip = implode('.', $_ip);
        return $ip;
    }


    // Checkout your order
    public function checkoutAction()
    {
        $cart_items = Cart::factory()->get_list_for_basket();

        $profile = (bool)Arr::get($this->post, 'profile', false);

        // Check incoming data
        $email = Arr::get($this->post, 'email');

        $agree = Arr::get($this->post, 'agree');
        if (!$agree) {
            $this->error('Вы должны принять условия соглашения на нашем сайте!');
        }
        $name = Arr::get($this->post, 'name');
        $lastName = Arr::get($this->post, 'last_name');
        $middleName = Arr::get($this->post, 'middle_name');
        $phone = Arr::get($this->post, 'phone');
        if (!$phone) {
            $this->error('Вы неверно ввели телефон!');
        }


        // If need to create user in cart page
        if (!$profile) {
            $user = DB::select()->from('users')->where('email', '=', $email)->as_object()->execute()->current();
            if ($user) {
                if ($user->status) {
                    $this->error('Пользователь с указанным E-Mail адресом уже зарегистрирован!' . '<br>' . 'Обратитесь к менеджеру для решения этой проблемы' .'<br>' . Config::get('static.help_phone') .' - звонок бесплатный</br>' . Config::get('static.help_email')  . '<br><span class="footer__callback open__callback" style="color:red;"><strong>Заказать обратный звонок</strong></span>');
                }
                $this->error('Пользователь с указанным E-Mail адресом уже зарегистрирован, но либо заблокирован либо не подтвердил свой E-Mail адрес. Пожалуйста обратитесь к Администратору для решения сложившейся ситуации' . '<br>' . '<br>' . Config::get('static.help_phone') .' - звонок бесплатный</br>' . Config::get('static.help_email')  . '<br><span class="footer__callback open__callback" style="color:red;"><strong>Заказать обратный звонок</strong></span>');
            }
            $type = array();

            $user_rozn = Arr::get($this->post, 'rozn');
            if($user_rozn){
                $role = CustomerRoles::getRow($user_rozn,'alias');
                if($role){
                    array_push($type,  $role->id);
                }
            }

            $user_opt = Arr::get($this->post, 'opt');
            if($user_opt){
                $role = CustomerRoles::getRow($user_opt,'alias');
                if($role){
                    array_push($type,  $role->id);
                }
            }

            $user_drop = Arr::get($this->post, 'drop');
            if($user_drop){
                $role = CustomerRoles::getRow($user_drop,'alias');
                if($role){
                    array_push($type,  $role->id);
                }
            }



            if(!$user_rozn && !$user_opt && !$user_drop){
                $this->error('Укажите тип пользователя');
            }
            $customer_id = $this->generateIdentificationAction($phone);

            $password = User::generate_random_password();
            $data = [
                'email' => $email,
                'password' => $password,
                'ip' => System::getRealIP(),
                'name' => $name,
                'last_name' => $lastName,
                'middle_name' => $middleName,
                'phone' => $phone,
                'customer_id' => $customer_id

            ];
            // Creating of the new user and set his status to 1. He must be redirected to his cabinet
            $data['status'] = 1;
            User::factory()->registration($type, $data);
            $user = DB::select()->from('users')->where('email', '=', $email)->as_object()->execute()->current();

            // Save log
            $qName = 'Регистрация пользователя, c корзины';
            $url = '/wezom/users/edit/' . $user->id;
            Log::add($qName, $url, 1);

            Email::sendTemplate(14, [
                '{{site}}' => Arr::get($_SERVER, 'HTTP_HOST'),
                '{{ip}}' => Arr::get($data, 'ip'),
                '{{date}}' => date('d.m.Y'),
                '{{email}}' => $user->email,
                '{{password}}' => $password,
                '{{name}}' => $user->name
            ], Arr::get($data, 'email'));

            // Authorization of the user
            User::factory()->auth($user, 0);
            Message::GetMessage(1, 'Вы успешно зарегистрировались на сайте! Пожалуйста укажите остальную информацию о себе в личном кабинете для того, что бы мы могли обращаться к Вам по имени', 5000);
//            $this->success(['redirect' => '/account']);
            Message::GetMessage(1, 'Вам отправлено письмо подтверждения со ссылкой, кликнув по которой, Вы подтвердите свой адрес и будете автоматически авторизованы на сайте.', 1500);
        }



        $delivery = Arr::get($this->post, 'delivery');
        if (!$delivery) {
            $this->error('Выберите способ доставки!');
        }

        $ip = System::getRealIP();


        // Check for cart existance
        $count = Cart::factory()->_count_goods;
        if (!$count) {
            $this->error('Вы ничего не выбрали для покупки!');
        }

        $_currency = DB::select()->from('currencies_courses')->find_all()->as_array('id');
        $_currentCurrency = DB::select()->from('currencies_courses')->where('currency', '=', Cookie::getWithoutSalt('currency'))->find();


        // Create order
        $data = [];
        $data['status'] = 0;
        $data['ip'] = $ip;
        $data['payment'] = 0;
        $data['delivery'] = $delivery;
        $data['name'] = $name;
        $data['last_name'] = $lastName;
        $data['middle_name'] = $middleName;
        $data['phone'] = $phone;
        $data['email'] = $email;
        $data['created_at'] = time();
        $data['currency'] = $_currentCurrency->id;
        if (User::info()) {
            $data['user_id'] = User::info()->id;
        }

        $keys = [];
        $values = [];
        $delivery_info = '';
        foreach ($data as $key => $value) {
            $keys[] = $key;
            $values[] = $value;
        }
        if($order_id = Arr::get($this->post, 'old_order')){
            DB::update('orders')->set($data)->where('id', '=', $order_id)->execute();
        }else{
            $order_id = DB::insert('orders', $keys)->values($values)->execute();
            if (!$order_id) {
                $this->error('К сожалению, создать заказ не удалось. Пожалуйста повторите попытку через несколько секунд');
            }
            $order_id = Arr::get($order_id, 0);
        }


        if ($delivery != 3) {
            $region = Arr::get($this->post, 'region');
            $city = Arr::get($this->post, 'city');
            $section = Arr::get($this->post, 'section');
            $home = Arr::get($this->post, 'home');

            $data_delivery = [
                'order_id' => $order_id,
                'region' =>  $region[$delivery],
                'city' => $city[$delivery],
                'warehouse' => $section[$delivery],
                'index' => $home
            ];


            if ($delivery == 2) {
                $data_delivery['index'] = Arr::get($this->post, 'index');
              }

            if(Arr::get($this->post, 'old_order')){
                DB::update('orders_delivery')->set($data_delivery)->where('order_id', '=', $order_id)->execute();
            }else{
                $keys = [];
                $values = [];
                foreach ($data_delivery as $key_d => $value_d){
                    $keys[] = $key_d;
                    $values[] = $value_d;
                }
                DB::insert('orders_delivery', $keys)->values($values)->execute();
            }

        }
        // Add items to order
        if(Arr::get($this->post, 'old_order')){
            DB::delete('orders_items')->where('order_id', '=', $order_id)->execute();
        }
        $totalCost = 0;
        foreach ($cart_items as $item) {
            $obj = Arr::get($item, 'obj');
            $count = (int)Arr::get($item, 'count');
            $sizes = Arr::get($obj, 'sizes');

            if (count($sizes)) {
                foreach ($sizes as $size) {
                    $priceData = Support::calculatePriceWithCurrency($obj,$_currency,$_currentCurrency);
                    $cost = $priceData['cost'];
                    $data = [];
                    $data['order_id'] = $order_id;
                    $data['catalog_id'] = $obj->id;
                    $data['size'] = $size->size;
                    $data['count'] = $size->count;
                    $data['cost'] = $cost;
                    $data['name'] = $obj->name;

                    $totalCost += $data['cost'] * $data['count'];

                    $keys = [];
                    $values = [];
                    foreach ($data as $key => $value) {
                        $keys[] = $key;
                        $values[] = $value;
                    }
                    DB::insert('orders_items', $keys)->values($values)->execute();

                     }
            } else {
                if ($obj and $count) {
                    $priceData = Support::calculatePriceWithCurrency($obj,$_currency,$_currentCurrency);
                    $cost = $priceData['cost'];
                    $data = [];
                    $data['order_id'] = $order_id;
                    $data['catalog_id'] = $obj->id;
                    $data['count'] = $count;
                    $data['cost'] = $cost;
                    $data['name'] = $obj->name;
                    $keys = [];
                    $values = [];
                    $totalCost += $data['cost'] * $data['count'];

                    $keys = [];
                    $values = [];
                    foreach ($data as $key => $value) {
                        $keys[] = $key;
                        $values[] = $value;
                    }
                    DB::insert('orders_items', $keys)->values($values)->execute();

                }
            }

        }
        // Save log
        $qName = 'Новый заказ';
        $url = '/wezom/orders/edit/' . $order_id;
        Log::add($qName, $url, 8);



        $_SESSION['order_id'] = $order_id;

        $this->success([
            "redirect" => '/cart/payment?id='.$order_id
        ]);

    }

    public function paymentAction(){
        $order_id = Arr::get($this->post, 'order_id', 0);
        $order = Orders::getOrder($order_id);
        $liqpay_methods = Config::get('liqpay.paytypes');
        $pay_method = Arr::get($this->post, 'paymentMethod', 0);
        if($order){
            if(array_key_exists($pay_method, $liqpay_methods)){
                $protocol = Config::get('security.ssl') ? 'https://' : 'http://';
                $liqpay_order_id = date('d').'-'.date('m').'-'.date('y').'-'.$order_id;
                $description = $order_id.' '.
                    date('y-m-d').' '.
                    $order->last_name.' '.
                    $order->name.' '.
                    $order->middle_name.' '.
                    $order->amount;

                DB::update(
                    'orders')->set(['payment' => 3, 'liqpay_id' => $liqpay_order_id])->where('id', '=', $order_id)->execute();

                $params = [
                    'action' => 'pay',
                    'amount' => $order->amount,
                    'currency' => Cookie::getWithoutSalt('currency') == 'грн' ? 'UAH' : Cookie::getWithoutSalt('currency'),
                    'description' => $description,
                    'order_id' => "$liqpay_order_id",
                    'version' => '3',
                    'paytypes' => $liqpay_methods[$pay_method],
                    'result_url' => $protocol .  $_SERVER['HTTP_HOST'] . '/cart/order?id='.$order->id,
                    'server_url' => $protocol .  $_SERVER['HTTP_HOST'] . '/liqpay/answer',
                ];
                $liqpay = new \Core\Payments\LiqPay(Config::get('basic.liqpay_public'), Config::get('basic.liqpay_private'));
                $html = $liqpay->cnb_form($params);
                $this->success([
                    "form" => $html
                ]);
            }else{
                DB::update('orders')->set(['payment' => $pay_method])->where('id', '=', $order_id)->execute();
                $this->success([
                    "redirect" => '/cart/order'
                ]);
            }

        }else{
            $this->error('Заказ не был создан, попробуйте еще раз');
        }

    }

    public function checkoutLoggedAction()
    {
        $cart_items = Cart::factory()->get_list_for_basket();

        $email = User::info()->email;
        $name = User::info()->name;
        $lastName = User::info()->last_name;
        $middleName = User::info()->middle_name;
        $phone = User::info()->phone;


        if (!$phone) {
            $this->error('Укажите свой телефон!');
        }

        $delivery = Arr::get($this->post, 'delivery');
        if (!$delivery) {
            $this->error('Выберите способ доставки!');
        }

        if ($delivery == 2 && !$middleName) {
            $this->error('Укажите отчество!');
        }

        $ip = System::getRealIP();


        // Check for cart existance
        $count = Cart::factory()->_count_goods;
        if (!$count) {
            $this->error('Вы ничего не выбрали для покупки!');
        }

        $_currency = DB::select()->from('currencies_courses')->find_all()->as_array('currency');
        $_currentCurrency = DB::select()->from('currencies_courses')->where('currency', '=', Cookie::getWithoutSalt('currency'))->find();

        // Create order
        $data = [];
        $data['status'] = 0;
        $data['ip'] = $ip;
        $data['payment'] = 0;
        $data['delivery'] = $delivery;
        $data['name'] = $name;
        $data['last_name'] = $lastName;
        $data['middle_name'] = $middleName;
        $data['phone'] = $phone;
        $data['email'] = $email;
        $data['created_at'] = time();
        $data['currency'] = $_currentCurrency->id;
        if (User::info()) {
            $data['user_id'] = User::info()->id;
        }

        $keys = [];
        $values = [];
        $delivery_info = '';
        foreach ($data as $key => $value) {
            $keys[] = $key;
            $values[] = $value;
        }
        if($order_id = Arr::get($this->post, 'old_order')){
             DB::update('orders')->set($data)->where('id', '=', $order_id)->execute();
        }else{
            $order_id = DB::insert('orders', $keys)->values($values)->execute();
            if (!$order_id) {
                $this->error('К сожалению, создать заказ не удалось. Пожалуйста повторите попытку через несколько секунд');
            }
            $order_id = Arr::get($order_id, 0);
        }

        if ($delivery != 3) {
            $region = Arr::get($this->post, 'region');
            $city = Arr::get($this->post, 'city');
            $section = Arr::get($this->post, 'section');
            $home = Arr::get($this->post, 'home');

            $data_delivery = [
                'order_id' => $order_id,
                'region' =>  $region[$delivery],
                'city' => $city[$delivery],
                'warehouse' => $section[$delivery],
                'index' => $home
            ];


            if ($delivery == 2) {
                $data_delivery['index'] = Arr::get($this->post, 'index');
            }

            if(Arr::get($this->post, 'old_order')){
                DB::update('orders_delivery')->set($data_delivery)->where('order_id', '=', $order_id)->execute();
            }else{
                $keys = [];
                $values = [];
                foreach ($data_delivery as $key_d => $value_d){
                    $keys[] = $key_d;
                    $values[] = $value_d;
                }
                DB::insert('orders_delivery', $keys)->values($values)->execute();
            }

        }
        if(Arr::get($this->post, 'old_order')){
            DB::delete('orders_items')->where('order_id', '=', $order_id)->execute();
        }
        // Add items to order
        $totalCost = 0;
        foreach ($cart_items as $item) {
            $obj = Arr::get($item, 'obj');
            $count = (int)Arr::get($item, 'count');
            $sizes = Arr::get($obj, 'sizes');

            if (count($sizes)) {
                foreach ($sizes as $size) {
                    $priceData = Support::calculatePriceWithCurrency($obj,$_currency,$_currentCurrency);
                    $cost = $priceData['cost'];
                    $data = [];
                    $data['order_id'] = $order_id;
                    $data['catalog_id'] = $obj->id;
                    $data['size'] = $size->size;
                    $data['count'] = $size->count;
                    $data['cost'] = $cost;
                    $data['name'] = $obj->name;

                    $totalCost += $data['cost'] * $data['count'];

                    $keys = [];
                    $values = [];
                    foreach ($data as $key => $value) {
                        $keys[] = $key;
                        $values[] = $value;
                    }
                    DB::insert('orders_items', $keys)->values($values)->execute();

                }

            } else {
                if ($obj and $count) {
                    $priceData = Support::calculatePriceWithCurrency($obj,$_currency,$_currentCurrency);
                    $cost = $priceData['cost'];
                    $data = [];
                    $data['order_id'] = $order_id;
                    $data['catalog_id'] = $obj->id;
                    $data['count'] = $count;
                    $data['cost'] = $cost;
                    $data['name'] = $obj->name;
                    $keys = [];
                    $values = [];
                    $totalCost += $data['cost'] * $data['count'];

                    $keys = [];
                    $values = [];
                    foreach ($data as $key => $value) {
                        $keys[] = $key;
                        $values[] = $value;
                    }
                    DB::insert('orders_items', $keys)->values($values)->execute();

                }
            }

        }
        // Create links
        $link_user = 'http://' . Arr::get($_SERVER, 'HTTP_HOST') . '/account/orders#' . $order_id;
        $link_admin = 'http://' . Arr::get($_SERVER, 'HTTP_HOST') . '/wezom/orders/edit/' . $order_id;

        // Save log
        $qName = 'Новый заказ';
        $url = '/wezom/orders/edit/' . $order_id;
        Log::add($qName, $url, 8);

        $_SESSION['order_id'] = $order_id;
        $this->success([
            "redirect" => '/cart/payment?id='.$order_id
        ]);
    }

    // Ask a question about item
    public function questionAction()
    {
        // Check incomming data
        $id = Arr::get($this->post, 'id');
        if (!$id) {
            $this->error('Такой товар не существует!');
        }
        $item = DB::select('alias', 'name', 'id')->from('catalog')->where('status', '=', 1)->where('id', '=', $id)->as_object()->execute()->current();
        if (!$item) {
            $this->error('Такой товар не существует!');
        }
        $email = Arr::get($this->post, 'email');
        if (!$email or !filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $this->error('Вы неверно ввели E-Mail!');
        }
        $text = trim(strip_tags(Arr::get($this->post, 'text')));
        if (!$text or mb_strlen($text, 'UTF-8') < 5) {
            $this->error('Слишком короткий вопрос! Нужно хотя бы 5 символов');
        }
        $name = trim(strip_tags(Arr::get($this->post, 'name')));
        if (!$name or mb_strlen($name, 'UTF-8') < 2) {
            $this->error('Слишком короткое имя! Нужно хотя бы 2 символов');
        }

        // Check for bot
        $ip = System::getRealIP();
        $check = DB::select([DB::expr('catalog_questions.id'), 'count'])
            ->from('catalog_questions')
            ->where('ip', '=', $ip)
            ->where('catalog_id', '=', $id)
            ->where('created_at', '>', time() - 60)
            ->as_object()->execute()->current();
        if (is_object($check) AND $check->count) {
            $this->error('Вы только что задали вопрос по этому товару! Пожалуйста, повторите попытку через минуту');
        }

        // All ok. Save data
        $keys = ['ip', 'name', 'email', 'text', 'catalog_id', 'created_at'];
        $values = [$ip, $name, $email, $text, $item->id, time()];
        $lastID = DB::insert('catalog_questions', $keys)->values($values)->execute();
        $lastID = Arr::get($lastID, 0);

        // Create links
        $link = 'http://' . Arr::get($_SERVER, 'HTTP_HOST') . '/' . $item->alias . '/p' . $item->id;
        $link_admin = 'http://' . Arr::get($_SERVER, 'HTTP_HOST') . '/wezom/items/edit/' . $item->id;

        // Save log
        $qName = 'Вопрос о товаре';
        $url = '/wezom/questions/edit/' . $lastID;
        Log::add($qName, $url, 5);

        // Send message to admin if need
        Email::sendTemplate(9, [
            '{{site}}' => Arr::get($_SERVER, 'HTTP_HOST'),
            '{{ip}}' => $ip,
            '{{date}}' => date('d.m.Y H:i'),
            '{{question}}' => $text,
            '{{name}}' => $name,
            '{{email}}' => $email,
            '{{link}}' => $link,
            '{{admin_link}}' => $link_admin,
            '{{item_name}}' => $item->name
        ]);

        // Send message to user if need
        Email::sendTemplate(10, [
            '{{site}}' => Arr::get($_SERVER, 'HTTP_HOST'),
            '{{ip}}' => $ip,
            '{{date}}' => date('d.m.Y H:i'),
            '{{question}}' => $text,
            '{{name}}' => $name,
            '{{email}}' => $email,
            '{{link}}' => $link,
            '{{admin_link}}' => $link_admin,
            '{{item_name}}' => $item->name
        ], $email);

        $this->success('Вы успешно задали вопрос! Администратор ответит Вам в ближайшее время');
    }


    // Simple order by phone number
    public function order_simpleAction()
    {
        // Check incomming data
        $id = Arr::get($this->post, 'id');
        if (!$id) {
            $this->error('Такой товар не существует!');
        }
        $item = DB::select('alias', 'name', 'id')->from('catalog')->where('status', '=', 1)->where('id', '=', $id)->as_object()->execute()->current();
        if (!$item) {
            $this->error('Такой товар не существует!');
        }
        $phone = trim(Arr::get($this->post, 'phone'));
        if (!$phone or !preg_match('/^\+38 \(\d{3}\) \d{3}\-\d{2}\-\d{2}$/', $phone, $matches)) {
            $this->error('Номер телефона введен неверно!');
        }

        // Check for bot
        $ip = System::getRealIP();
        $check = DB::select([DB::expr('orders_simple.id'), 'count'])
            ->from('orders_simple')
            ->where('ip', '=', $ip)
            ->where('catalog_id', '=', $id)
            ->where('created_at', '>', time() - 60)
            ->as_object()->execute()->current();
        if (is_object($check) and $check->count) {
            $this->error('Вы только что заказали этот товар! Пожалуйста, повторите попытку через минуту');
        }

        // All ok. Save data
        $keys = ['ip', 'phone', 'catalog_id', 'user_id', 'created_at'];
        $values = [$ip, $phone, $item->id, User::info() ? User::info()->id : 0, time()];
        $lastID = DB::insert('orders_simple', $keys)->values($values)->execute();
        $lastID = Arr::get($lastID, 0);

        // Create links
        $link = 'http://' . Arr::get($_SERVER, 'HTTP_HOST') . '/' . $item->alias . '/p' . $item->id;
        $link_admin = 'http://' . Arr::get($_SERVER, 'HTTP_HOST') . '/wezom/items/edit/' . $item->id;

        // Save log
        $qName = 'Заказ в один клик';
        $url = '/wezom/simple/edit/' . $lastID;
        Log::add($qName, $url, 7);

        // Send message to admin if need
        Email::sendTemplate(8, [
            '{{site}}' => Arr::get($_SERVER, 'HTTP_HOST'),
            '{{ip}}' => $ip,
            '{{date}}' => date('d.m.Y H:i'),
            '{{phone}}' => $phone,
            '{{link}}' => $link,
            '{{admin_link}}' => $link_admin,
            '{{item_name}}' => $item->name
        ]);

        $this->success('Вы успешно оформили заказ в один клик! Оператор свяжется с Вами в скором времени');
    }


    // Add comment for item
    public function add_commentAction()
    {
        // Check incomming data
        $id = Arr::get($this->post, 'id');
        if (!$id) {
            $this->error('Такой товар не существует!');
        }
        $item = DB::select('alias', 'name', 'id')->from('catalog')->where('status', '=', 1)->where('id', '=', $id)->as_object()->execute()->current();
        if (!$item) {
            $this->error('Такой товар не существует!');
        }
        $name = Arr::get($this->post, 'name');
        if (!$name or mb_strlen($name, 'UTF-8') < 2) {
            $this->error('Введено некорректное имя!');
        }
        $city = Arr::get($this->post, 'city');
        if (!$city or mb_strlen($city, 'UTF-8') < 2) {
            $this->error('Введено некорректное название города!');
        }
        $text = trim(strip_tags(Arr::get($this->post, 'text')));
        if (!$text or mb_strlen($text, 'UTF-8') < 5) {
            $this->error('Слишком короткий коментарий! Нужно хотя бы 5 символов');
        }

        // Check for bot
        $ip = System::getRealIP();
        $check = DB::select([DB::expr('catalog_comments.id'), 'count'])
            ->from('catalog_comments')
            ->where('ip', '=', $ip)
            ->where('catalog_id', '=', $id)
            ->where('created_at', '>', time() - 60)
            ->as_object()->execute()->current();
        if (is_object($check) AND $check->count) {
            $this->error('Вы только что оставили отзыв об этом товаре! Пожалуйста, повторите попытку через минуту');
        }

        // All ok. Save data
        $keys = ['date', 'ip', 'name', 'city', 'text', 'catalog_id', 'created_at'];
        $values = [time(), $ip, $name, $city, $text, $item->id, time()];
        $lastID = DB::insert('catalog_comments', $keys)->values($values)->execute();
        $lastID = Arr::get($lastID, 0);

        // Create links
        $link = 'http://' . Arr::get($_SERVER, 'HTTP_HOST') . '/' . $item->alias . '/p' . $item->id;
        $link_admin = 'http://' . Arr::get($_SERVER, 'HTTP_HOST') . '/wezom/items/edit/' . $item->id;

        // Save log
        $qName = 'Отзыв к товару';
        $url = '/wezom/comments/edit/' . $lastID;
        Log::add($qName, $url, 6);

        // Send message to admin if need
        Email::sendTemplate(7, [
            '{{site}}' => Arr::get($_SERVER, 'HTTP_HOST'),
            '{{ip}}' => $ip,
            '{{date}}' => date('d.m.Y H:i'),
            '{{name}}' => $name,
            '{{city}}' => $city,
            '{{text}}' => $text,
            '{{link}}' => $link,
            '{{admin_link}}' => $link_admin,
            '{{item_name}}' => $item->name
        ]);

        $this->success('Вы успешно оставили отзыв о товаре. Он отобразится на сайте после проверки администратором');
    }


    // User authorization
    public function loginAction()
    {
        $email = Arr::get($this->post, 'email');
        if (!$email or !filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $this->error('Вы неверно ввели E-Mail!');
        }
        $password = Arr::get($this->post, 'password');
        $remember = Arr::get($this->post, 'remember');
        if (!$password) {
            $this->error('Вы не ввели пароль!');
        }
        // Check user for existance and ban
        $user = User::factory()->get_user_by_email($email, $password);
        if (!$user) {
            $this->error('Вы допустили ошибку в логине и/или пароле!');
        }
        if (!$user->status) {
            $this->error('Пользователь с указанным E-Mail адресом либо заблокирован либо не активирован. Пожалуйста обратитесь к Администратору для решения сложившейся ситуации' . '<br>' . '<br>' . Config::get('static.help_phone') .' - звонок бесплатный</br>' . Config::get('static.help_email')  . '<br><span class="footer__callback open__callback" style="color:#ff0000;"><strong>Заказать обратный звонок</strong></span>');
         }

        // Authorization of the user
        DB::update('users')->set(['last_login' => time(), 'logins' => (int)$user->logins + 1])->where('id', '=', $user->id)->execute();
        User::factory()->auth($user, $remember);
        Message::GetMessage(1, 'Вы успешно авторизовались на сайте!', 3500);
        $this->success(['reload' => true, 'noclear' => 1]);
    }

    // Wholesale user authorization
    public function wholesaleLoginAction()
    {
        $email = Arr::get($this->post, 'email');
        if (!$email or !filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $this->error('Вы неверно ввели E-Mail!');
        }
        $password = Arr::get($this->post, 'password');
        $remember = Arr::get($this->post, 'remember');
        if (!$password) {
            $this->error('Вы не ввели пароль!');
        }
        // Check user for existance and ban
        $user = User::factory()->get_user_by_email($email, $password);
        if (!$user) {
            $this->error('Вы допустили ошибку в логине и/или пароле!');
        }
        if (!$user->status) {
            $this->error('Пользователь с указанным E-Mail адресом либо заблокирован либо не активирован. Пожалуйста обратитесь к Администратору для решения сложившейся ситуации'. '<br>' . '<br>' . Config::get('static.help_phone') .' - звонок бесплатный</br>' . Config::get('static.help_email')  . '<br><span class="footer__callback open__callback" style="color:red;"><strong>Заказать обратный звонок</strong></span>');
        }
        if ($user->type != 1) {
            $this->error('Пользователь с указаными данными не существует или не зарегистрирован как оптовик. Пожалуйста проверьте правильность ввода данных или пройдите регистрацию.');
        }

        if (User::info()) {
            User::factory()->logout();
        }
        // Authorization of the user
        DB::update('users')->set(['last_login' => time(), 'logins' => (int)$user->logins + 1, 'updated_at' => time()])->where('id', '=', $user->id)->execute();
        User::factory()->auth($user, $remember);
        Message::GetMessage(1, 'Вы успешно авторизовались на сайте!', 3500);
        $this->success(['reload' => true, 'noclear' => 1]);
    }


    // User wants to edit some information
    public function edit_profileAction()
    {
        $oldPassword = Arr::get($this->post, 'old-pswd', null);
        if (isset($oldPassword) && !empty($oldPassword)):
            if (!User::factory()->check_password($oldPassword, User::info()->password)) {
                $this->error('Старый пароль введен неверно!');
            }
            $password = trim(Arr::get($this->post, 'new-pswd'));

            if (User::factory()->check_password($password, User::info()->password)) {
                $this->error('Нельзя поменять пароль на точно такой же!');
            }

            // Change password for new
            User::factory()->update_password(User::info()->id, $password);

            // Send email to user with new data
            Email::sendTemplate(6, [
                '{{site}}' => Arr::get($_SERVER, 'HTTP_HOST'),
                '{{ip}}' => System::getRealIP(),
                '{{date}}' => date('d.m.Y H:i'),
                '{{password}}' => $password
            ], User::info()->email);
        endif;

        // Check incoming data
        $name = trim(Arr::get($this->post, 'name'));
        if (!$name or mb_strlen($name, 'UTF-8') < 2) {
            $this->error('Введенное имя слишком короткое!');
        }
        $last_name = trim(Arr::get($this->post, 'last_name'));
        $middle_name = trim(Arr::get($this->post, 'middle_name'));
        $email = Arr::get($this->post, 'email');
        if (!$email or !filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $this->error('Вы неверно ввели E-Mail!');
        }
        $check = DB::select([DB::expr('COUNT(users.id)'), 'count'])
            ->from('users')
            ->where('email', '=', $email)
            ->where('id', '!=', User::info()->id)
            ->as_object()->execute()->current();
        if (is_object($check) and $check->count) {
            $this->error('Пользователь с указанным E-Mail адресом уже зарегистрирован!'. '<br>' . 'Обратитесь к менеджеру для решения этой проблемы' . '<br>' . Config::get('static.help_phone') .' - звонок бесплатный</br>' . Config::get('static.help_email')  . '<br><span class="footer__callback open__callback" style="color:red;"><strong>Заказать обратный звонок</strong></span>');
        }
        $phone = trim(Arr::get($this->post, 'phone'));
        if (!$phone or !preg_match('/^\+38\(\d{3}\) \d{3}\-\d{2}\-\d{2}$/', $phone, $matches)) {
            $this->error('Номер телефона введен неверно!');
        }
        $city = Arr::get($this->post, 'city');
        $address = Arr::get($this->post, 'address');
        if(Arr::get($this->post, 'ROLES')){
            $roles = Arr::get($this->post, 'ROLES');
            CustomerTypes::changeCustomerTypes($roles,User::info()->id);
        }
        unset($_POST['ROLES']);
        $birthday = Arr::get($this->post, 'birthday');

        // Save new users data
        DB::update('users')->set([
            'name' => $name,
            'last_name' => $last_name,
            'birthday' => strtotime($birthday),
            'middle_name' => $middle_name,
            'email' => $email,
            'phone' => $phone,
            'city' => $city,
            'address' => $address,
            'updated_at' => time()
        ])->where('id', '=', User::info()->id)->execute();
        Message::GetMessage(1, 'Вы успешно изменили свои данные!', 3500);
        $redirect = Arr::get($this->post, 'redirect', '/account');
        $this->success(['redirect' => $redirect]);
    }


    // Change password
    public function change_passwordAction()
    {
        // Check incoming data
        $oldPassword = Arr::get($this->post, 'old_password');
        if (!User::factory()->check_password($oldPassword, User::info()->password)) {
            $this->error('Старый пароль введен неверно!');
        }
        $password = trim(Arr::get($this->post, 'password'));
        if (mb_strlen($password, 'UTF-8') < conf::get('main.password_min_length')) {
            $this->error('Пароль не может быть короче ' . conf::get('main.password_min_length') . ' символов!');
        }
        if (User::factory()->check_password($password, User::info()->password)) {
            $this->error('Нельзя поменять пароль на точно такой же!');
        }
        $confirm = trim(Arr::get($this->post, 'confirm'));
        if ($password != $confirm) {
            $this->error('Поля "Новый пароль" и "Подтвердите новый пароль" должны совпадать!');
        }

        // Change password for new
        User::factory()->update_password(User::info()->id, $password);

        // Send email to user with new data
        Email::sendTemplate(6, [
            '{{site}}' => Arr::get($_SERVER, 'HTTP_HOST'),
            '{{ip}}' => System::getRealIP(),
            '{{date}}' => date('d.m.Y H:i'),
            '{{password}}' => $password
        ], User::info()->email);

        $this->success('На указанный E-Mail адрес высланы новые данные для входа');
    }


    // User registration
    public function registrationAction()
    {
        // Check incoming data
        $email = Arr::get($this->post, 'email');
        if (!$email or !filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $this->error('Вы неверно ввели E-Mail!');
        }
        $user = DB::select()->from('users')->where('email', '=', $email)->as_object()->execute()->current();
        if ($user) {
            if ($user->status) {
                $this->error('Пользователь с указанным E-Mail адресом уже зарегистрирован!'. '<br>' . 'Обратитесь к менеджеру для решения этой проблемы' .'<br>' . Config::get('static.help_phone') .' - звонок бесплатный</br>' . Config::get('static.help_email')  . '<br><span class="footer__callback open__callback" style="color:red;"><strong>Заказать обратный звонок</strong></span>');
            }
            $this->error('Пользователь с указанным E-Mail адресом уже зарегистрирован, но либо заблокирован либо не подтвердил свой E-Mail адрес. Пожалуйста обратитесь к Администратору для решения сложившейся ситуации'. '<br>' . '<br>' . Config::get('static.help_phone') .' - звонок бесплатный</br>' . Config::get('static.help_email')  . '<br><span class="footer__callback open__callback" style="color:red;"><strong>Заказать обратный звонок</strong></span>');
        }
        $password = trim(Arr::get($this->post, 'password'));
        if (mb_strlen($password, 'UTF-8') < conf::get('main.password_min_length')) {
            $this->error('Пароль не может содержать меньше ' . conf::get('main.password_min_length') . ' символов!');
        }
        $agree = Arr::get($this->post, 'remember');
        if (!$agree) {
            $this->error('Вы должны принять условия соглашения для регистрации на нашем сайте!');
        }
        $name = Arr::get($this->post, 'name');
        if(empty($name)){
            $this->error('Укажите имя');
        }
        $lastName = Arr::get($this->post, 'surname');
        if(empty($lastName)){
            $this->error('Укажите фамилию');
        }
        $middleName = Arr::get($this->post, 'middle_name');
        $phone = Arr::get($this->post, 'phone');
        if(empty($phone)){
            $this->error('Укажите номер телефона');
        }
        $customer_id = $this->generateIdentificationAction($phone);


        $type = array();

        $user_rozn = Arr::get($this->post, 'rozn');
        if($user_rozn){
            $role = CustomerRoles::getRow($user_rozn,'alias');
            if($role){
                array_push($type,  $role->id);
            }
        }

        $user_opt = Arr::get($this->post, 'opt');
        if($user_opt){
            $role = CustomerRoles::getRow($user_opt,'alias');
            if($role){
                array_push($type,  $role->id);
            }
        }

        $user_drop = Arr::get($this->post, 'drop');
        if($user_drop){
            $role = CustomerRoles::getRow($user_drop,'alias');
            if($role){
                array_push($type,  $role->id);
            }
        }



        if(!$user_rozn && !$user_opt && !$user_drop){
            $this->error('Укажите тип пользователя');
        }
        $birthday = Arr::get($this->post, 'birthday');
        $baby_name = trim(strip_tags(Arr::get($this->post, 'babyName')));

        // Create user data
        $data = [
            'email' => $email,
            'password' => $password,
            'ip' => System::getRealIP(),
            'name' => $name,
            'last_name' => $lastName,
            'middle_name' => $middleName,
            'phone' => $phone,
            'customer_id' => $customer_id,
        ];

        // Create user. Then send an email to user with confirmation link or authorize him to site
        $mail = DB::select()->from('mail_templates')->where('id', '=', 4)->where('status', '=', 1)->as_object()->execute()->current();
        if ($mail) {
            // Creating of the new user and set his status to zero. He need to confirm his email
            $data['status'] = 0;
            User::factory()->registration($type, $data);
            $user = DB::select()->from('users')->where('email', '=', $email)->as_object()->execute()->current();
            if (!$name || mb_strlen($name, 'UTF-8') < 2) {
                $this->error('Слишком короткое имя! Нужно хотя бы 2 символов');
            }
            if($baby_name != '' && !empty($birthday)) {
                CustomerChildren::insert([
                    'birthday' => !empty($birthday) ? strtotime($birthday) : NULL,
                    'name' => $baby_name,
                    'uid' => $user->id,
                ]);
            }
            //DB::update('users')->set(['updated_at' => time()])->where('id', '=', $uid)->execute();

            // Save log
            $qName = 'Регистрация пользователя, требующая подтверждения';
            $url = '/wezom/users/edit/' . $user->id;
            Log::add($qName, $url, 1);

            $protocol = isset($_SERVER['HTTPS']) ? 'https://' : 'http://';
            // Sending letter to email
            $from = ['{{site}}', '{{ip}}', '{{date}}', '{{link}}'];
            $to = [
                Arr::get($_SERVER, 'HTTP_HOST'), Arr::get($data, 'ip'), date('d.m.Y'),
                $protocol . Arr::get($_SERVER, 'HTTP_HOST') . '/account/confirm/hash/' . $user->hash,
            ];
            $subject = str_replace($from, $to, $mail->subject);
            $text = str_replace($from, $to, $mail->text);
            Email::send($subject, $text, $user->email);

            // Inform user if mail is sended
            $this->success('Вам отправлено письмо подтверждения со ссылкой, кликнув по которой, Вы подтвердите свой адрес и будете автоматически авторизованы на сайте.');
        } else {
            // Creating of the new user and set his status to 1. He must be redirected to his cabinet
            $data['status'] = 1;
            User::factory()->registration($data);
            $user = DB::select()->from('users')->where('email', '=', $email)->as_object()->execute()->current();
            if (!$name || mb_strlen($name, 'UTF-8') < 2) {
                $this->error('Слишком короткое имя! Нужно хотя бы 2 символов');
            }
            if($baby_name != '' && !empty($birthday)) {
                CustomerChildren::insert([
                    'birthday' => !empty($birthday) ? strtotime($birthday) : NULL,
                    'name' => $baby_name,
                    'uid' => $user->id,
                ]);
            }
            // Save log
            $qName = 'Регистрация пользователя';
            $url = '/wezom/users/edit/' . $user->id;
            Log::add($qName, $url, 1);

            Email::sendTemplate(13, [
                '{{site}}' => Arr::get($_SERVER, 'HTTP_HOST'),
                '{{ip}}' => Arr::get($data, 'ip'),
                '{{date}}' => date('d.m.Y'),
                '{{email}}' => $user->email,
                '{{password}}' => $password,
                '{{name}}' => $user->name
            ], Arr::get($data, 'email'));

            // Authorization of the user
            User::factory()->auth($user, 0);
            Message::GetMessage(1, 'Вы успешно зарегистрировались на сайте! Пожалуйста укажите остальную информацию о себе в личном кабинете для того, что бы мы могли обращаться к Вам по имени', 5000);
            $this->success(['reload' => true]);
        }
    }

    private function generateIdentificationAction($phone){
        if(empty($phone)){
            $this->error('Заполните номер телефона');
        }
        $uPhone = preg_replace('/[^0-9]*/', '', $phone);
        $customer_id = substr($uPhone,3);
        if(strlen($customer_id) !== 9){
            $this->error('Номер телефона имеет неправильный формат.');
        }
        $duplicatePhone = DB::select()->from('users')->where('customer_id', '=', $customer_id)->as_object()->execute()->current();
        if($duplicatePhone){
            $this->error('Пользователь с указанным <strong>номером телефона</strong> уже зарегистрирован!'. '<br>' . 'Обратитесь к менеджеру для решения этой проблемы' .'<br>' . Config::get('static.help_phone') .' - звонок бесплатный</br>' . Config::get('static.help_email')  . '<br><span class="footer__callback open__callback" style="color:red;"><strong>Заказать обратный звонок</strong></span>');
        }else{
            return $customer_id;
        }
    }

    // Wholesale User registration
    public function wholesaleRegistrationAction()
    {
        // Check incoming data
        $email = Arr::get($this->post, 'email');
        if (!$email or !filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $this->error('Вы неверно ввели E-Mail!');
        }
        $user = DB::select()->from('users')->where('email', '=', $email)->as_object()->execute()->current();
        if ($user) {
            if ($user->status) {
                $this->error('Пользователь с указанным E-Mail адресом уже зарегистрирован!'. '<br>' . 'Обратитесь к менеджеру для решения этой проблемы' .'<br>' . Config::get('static.help_phone') .' - звонок бесплатный</br>' . Config::get('static.help_email')  . '<br><span class="footer__callback open__callback" style="color:red;"><strong>Заказать обратный звонок</strong></span>');
            }
            $this->error('Пользователь с указанным E-Mail адресом уже зарегистрирован, но либо заблокирован либо не подтвердил свой E-Mail адрес. Пожалуйста обратитесь к Администратору для решения сложившейся ситуации'. '<br>' . '<br>' . Config::get('static.help_phone') .' - звонок бесплатный</br>' . Config::get('static.help_email')  . '<br><span class="footer__callback open__callback" style="color:red;"><strong>Заказать обратный звонок</strong></span>');
        }
        $password = trim(Arr::get($this->post, 'reg-password'));
        if (mb_strlen($password, 'UTF-8') < conf::get('main.password_min_length')) {
            $this->error('Пароль не может содержать меньше ' . conf::get('main.password_min_length') . ' символов!');
        }
        $agree = Arr::get($this->post, 'agree');
        if (!$agree) {
            $this->error('Вы должны принять условия соглашения для регистрации на нашем сайте!');
        }
        $name = Arr::get($this->post, 'name');
        $lastName = Arr::get($this->post, 'last_name');
        $middleName = Arr::get($this->post, 'middle_name');
        $phone = Arr::get($this->post, 'tel');

        if (User::info()) {
            User::factory()->logout();
        }

        // Create user data
        $data = [
            'email' => $email,
            'password' => $password,
            'ip' => System::getRealIP(),
            'type' => 1,
            'name' => $name,
            'last_name' => $lastName,
            'middle_name' => $middleName,
            'phone' => $phone
        ];

        // Create user. Then send an email to user with confirmation
        $data['status'] = 0;
        User::factory()->registration($data);
        $user = DB::select()->from('users')->where('email', '=', $email)->as_object()->execute()->current();

        // Save log
        $qName = 'Регистрация пользователя, требующая подтверждения';
        $url = '/wezom/users/edit/' . $user->id;
        Log::add($qName, $url, 1);

        // Sending letter to email
        Email::sendTemplate(30, [
            '{{site}}' => Arr::get($_SERVER, 'HTTP_HOST'),
            '{{ip}}' => Arr::get($data, 'ip'),
            '{{date}}' => date('d.m.Y'),
            '{{email}}' => $user->email,
        ], Arr::get($data, 'email'));

        Email::sendTemplate(31, [
            '{{site}}' => Arr::get($_SERVER, 'HTTP_HOST'),
            '{{ip}}' => Arr::get($data, 'ip'),
            '{{date}}' => date('d.m.Y'),
            '{{email}}' => $user->email,
        ], Config::get('mail.admin_email'));

        // Inform user if mail is sended
        $this->success('Вы успешно зарегестрировались. Администратор в скором времени активирует Ваш аккаунт.');
    }


    // Forgot password
    public function forgot_passwordAction()
    {
        // Check incoming data
        $email = Arr::get($this->post, 'email');
        if (!$email or !filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $this->error('Вы неверно ввели E-Mail!');
        }
        $user = Common::factory('users')->getRow($email, 'email');
        if (!$user) {
            $this->error('Пользователя с указанным E-Mail адресом не существует!');
        }
        if (!$user->status) {
            $this->error('Пользователь с указанным E-Mail адресом либо заблокирован либо не подтвердил E-Mail адрес. Пожалуйста обратитесь к Администратору для решения сложившейся ситуации'. '<br>' . '<br>' . Config::get('static.help_phone') .' - звонок бесплатный</br>' . Config::get('static.help_email')  . '<br><span class="footer__callback open__callback" style="color:red;"><strong>Заказать обратный звонок</strong></span>');
        }

        // Generate new password for user and save it to his account
        $password = User::factory()->generate_random_password();
        User::factory()->update_password($user->id, $password);

        // Send E-Mail to user with instructions how recover password
        Email::sendTemplate(5, [
            '{{site}}' => Arr::get($_SERVER, 'HTTP_HOST'),
            '{{ip}}' => System::getRealIP(),
            '{{date}}' => date('d.m.Y H:i'),
            '{{password}}' => $password
        ], $user->email);

        $this->success('На указанный E-Mail адрес выслан новый пароль для входа');
        // $this->success(['password' => $password]);
    }


    // Send callback
    public function callbackAction()
    {
        // Check incoming data
        $name = trim(Arr::get($this->post, 'name'));
        if (!$name || mb_strlen($name, 'UTF-8') < 2) {
            $this->error('Имя введено неверно!');
        }
        $phone = trim(Arr::get($this->post, 'phone'));
        if (!$phone || !preg_match('/^\+38\(\d{3}\) \d{3}\-\d{2}\-\d{2}$/', $phone, $matches)) {
            $this->error('Номер телефона введен неверно!');
        }

        // Check for bot
        $ip = System::getRealIP();
        $check = DB::select([DB::expr('COUNT(callback.id)'), 'count'])
            ->from('callback')
            ->where('ip', '=', $ip)
            ->where('created_at', '>', time() - 60)
            ->as_object()->execute()->current();
        if (is_object($check) && $check->count) {
            $this->error('Нельзя так часто просить перезвонить! Пожалуйста, повторите попытку через минуту');
        }

        // Save callback
        $lastID = DB::insert('callback', ['name', 'phone', 'ip', 'status', 'created_at'])->values([$name, $phone, $ip, 0, time()])->execute();
        $lastID = Arr::get($lastID, 0);

        // Save log
        $qName = 'Заказ звонка';
        $url = '/wezom/callback/edit/' . $lastID;
        Log::add($qName, $url, 3);

        // Send E-Mail to admin
        Email::sendTemplate(3, [
            '{{site}}' => Arr::get($_SERVER, 'HTTP_HOST'),
            '{{ip}}' => $ip,
            '{{date}}' => date('d.m.Y H:i'),
            '{{name}}' => $name,
            '{{phone}}' => $phone
        ]);

        $this->success('Администрация сайта скоро Вам перезвонит!');
    }

    // FAQ asq question
    public function asqQuestionAction()
    {
        // Check incoming data
        $name = trim(Arr::get($this->post, 'name'));
        if (!$name or mb_strlen($name, 'UTF-8') < 2) {
            $this->error('Имя введено неверно!');
        }
        $phone = trim(Arr::get($this->post, 'phone'));
        if (!$phone or !preg_match('/^\+38\(\d{3}\) \d{3}\-\d{2}\-\d{2}$/', $phone, $matches)) {
            $this->error('Номер телефона введен неверно!');
        }

        $question = Arr::get($this->post, 'question', '');
        // Check for bot
        $ip = System::getRealIP();
        $check = DB::select([DB::expr('COUNT(callback.id)'), 'count'])
            ->from('callback')
            ->where('ip', '=', $ip)
            ->where('created_at', '>', time() - 60)
            ->as_object()->execute()->current();
        if (is_object($check) AND $check->count) {
            $this->error('Нельзя так часто просить перезвонить! Пожалуйста, повторите попытку через минуту');
        }

        // Save callback
        $lastID = DB::insert('contacts', ['name', 'phone', 'ip', 'text', 'status', 'created_at'])
            ->values([$name, $phone, $ip, $question, 0, time()])->execute();
        $lastID = Arr::get($lastID, 0);

        // Save log
        $qName = 'Вопрос менеджеру';
        $url = '/wezom/contacts/edit/' . $lastID;
        Log::add($qName, $url, 3);

        // Send E-Mail to admin
        Email::sendTemplate(29, [
            '{{site}}' => Arr::get($_SERVER, 'HTTP_HOST'),
            '{{ip}}' => $ip,
            '{{date}}' => date('d.m.Y H:i'),
            '{{name}}' => $name,
            '{{phone}}' => $phone,
            '{{question}}' => $question
        ]);

        $this->success('Менеджер ответит вам в ближайшее времся!');
    }

    // Subscribe user for latest news and sales
    public function subscribeAction()
    {
        // Check incoming data
        $email = Arr::get($this->post, 'email');
        if (!$email or !filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $this->error('Вы неверно ввели E-Mail!');
        }
        $check = Common::factory('subscribers')->getRow($email, 'email');

        if (is_object($check) and $check->status) {
            $this->error('Вы уже подписаны на нашу рассылку!');
        }

        if (sizeof($check)) {
            $hash = sha1($email . microtime()); // Generate subscribers hash
            Common::factory('subscribers')->update(['hash' => $hash], $check->id);
            $lastID = $check->id;
        } else {
            $hash = sha1($email . microtime()); // Generate subscribers hash
            // Save subscriber to the database
            $ip = System::getRealIP();
            $data = [
                'email' => $email,
                'ip' => $ip,
                'status' => 0,
                'hash' => $hash
            ];
            $lastID = Common::factory('subscribers')->insert($data);
        }


        // Save log
        $qName = 'Подписчик';
        $url = '/wezom/subscribers/edit/' . $lastID;
        Log::add($qName, $url, 4);

        // Send E-Mail to user
        Email::sendTemplate(28, [
            '{{site}}' => Arr::get($_SERVER, 'HTTP_HOST'),
            '{{link}}' => 'http://' . Arr::get($_SERVER, 'HTTP_HOST') . '/subscribe/hash/' . $hash,
            '{{email}}' => $email,
            '{{ip}}' => $ip,
            '{{date}}' => date('d.m.Y H:i')
        ], $email);

        $this->success('На ваш email было отправлено письмо для подтверждения подписки!');
    }


    // User want to contact with admin and use contact form
    public function contactsAction()
    {
        // Check incoming data
        if (!Arr::get($this->post, 'name')) {
            $this->error('Вы не указали имя!');
        }
        if (!Arr::get($this->post, 'text')) {
            $this->error('Вы не написали текст сообщения!');
        }
        $phone = trim(Arr::get($this->post, 'phone'));
        if (!$phone or !preg_match('/^\+38\(\d{3}\) \d{3}\-\d{2}\-\d{2}$/', $phone, $matches)) {
            $this->error('Номер телефона введен неверно!');
        }
        if (!filter_var(Arr::get($this->post, 'email'), FILTER_VALIDATE_EMAIL)) {
            $this->error('Вы указали неверный E-Mail!');
        }

        // Create data for saving
        $data = [];
        $data['text'] = nl2br(Arr::get($this->post, 'text'));
        $data['ip'] = System::getRealIP();
        $data['name'] = Arr::get($this->post, 'name');
        $data['phone'] = Arr::get($this->post, 'phone');
        $data['email'] = Arr::get($this->post, 'email');
        $data['created_at'] = time();

        // Chec for bot
        $check = DB::select([DB::expr('COUNT(contacts.id)'), 'count'])
            ->from('contacts')
            ->where('ip', '=', Arr::get($data, 'ip'))
            ->where('created_at', '>', time() - 60)
            ->as_object()->execute()->current();
        if (is_object($check) AND $check->count) {
            $this->error('Нельзя так часто отправлять сообщения! Пожалуйста, повторите попытку через минуту');
        }

        // Save contact message to database
        $keys = [];
        $values = [];
        foreach ($data as $key => $value) {
            $keys[] = $key;
            $values[] = $value;
        }
        $lastID = DB::insert('contacts', $keys)->values($values)->execute();
        $lastID = Arr::get($lastID, 0);

        // Save log
        $qName = 'Сообщение из контактной формы';
        $url = '/wezom/contacts/edit/' . $lastID;
        Log::add($qName, $url, 2);

        // Send E-Mail to admin
        Email::sendTemplate(1, [
            '{{site}}' => Arr::get($_SERVER, 'HTTP_HOST'),
            '{{name}}' => Arr::get($data, 'name'),
            '{{email}}' => Arr::get($data, 'email'),
            '{{text}}' => Arr::get($data, 'text'),
            '{{ip}}' => Arr::get($data, 'ip'),
            '{{date}}' => date('d.m.Y H:i')
        ]);

        $this->success([
            'response' => 'Спасибо за сообщение! Администрация сайта ответит Вам в кратчайшие сроки',
            'reset' => true,
        ]);
    }

    // Ask a question about item
    public function reviewAction()
    {

        $email = Arr::get($this->post, 'email');
        if (!$email or !filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $this->error('Вы неверно ввели E-Mail!');
        }
        $text = trim(strip_tags(Arr::get($this->post, 'text')));
        if (!$text or mb_strlen($text, 'UTF-8') < 5) {
            $this->error('Слишком короткий вопрос! Нужно хотя бы 5 символов');
        }
        $name = trim(strip_tags(Arr::get($this->post, 'name')));
        if (!$name or mb_strlen($name, 'UTF-8') < 2) {
            $this->error('Слишком короткое имя! Нужно хотя бы 2 символов');
        }

        $rating = Arr::get($this->post, 'rating');

        // Check for bot
        $ip = System::getRealIP();
        $check = DB::select([DB::expr('reviews.id'), 'count'])
            ->from('reviews')
            ->where('ip', '=', $ip)
            ->where('created_at', '>', time() - 60)
            ->as_object()->execute()->current();
        if (is_object($check) AND $check->count) {
            $this->error('Вы только что отправили отзыв! Пожалуйста, повторите попытку через минуту');
        }

        // All ok. Save data
        $keys = ['ip', 'name', 'email', 'text', 'created_at', 'date', 'rating'];
        $values = [$ip, $name, $email, $text, time(), time(), $rating];
        $lastID = DB::insert('reviews', $keys)->values($values)->execute();
        $lastID = Arr::get($lastID, 0);

        // Create links

        $link_admin = 'http://' . Arr::get($_SERVER, 'HTTP_HOST') . '/wezom/reviews/edit/' . $item->id;

        // Save log
        $qName = 'Отзыв';
        $url = '/wezom/reviews/edit/' . $lastID;
        Log::add($qName, $url, 5);

        // Send message to admin if need
        Email::sendTemplate(7, [
            '{{site}}' => Arr::get($_SERVER, 'HTTP_HOST'),
            '{{ip}}' => $ip,
            '{{date}}' => date('d.m.Y H:i'),
            '{{text}}' => $text,
            '{{name}}' => $name,
            '{{email}}' => $email,
            '{{admin_link}}' => $link_admin
        ]);


        $this->success('Вы успешно отправили отзыв! Администрация опубликует его в ближайшее время!');
    }

    /**
     * @edit_childAction
     * Обновление информации о ребёнке в личном кабинете
     * @template Views/User/Children.php
     */
    public function edit_childAction(): void
    {
        $this->error('Для редактирования информации о детях обратитесь, пожалуйста, к менеджеру'. '<br>' . Config::get('static.help_phone') .' - звонок бесплатный</br>' . Config::get('static.help_email')  . '<br><span class="footer__callback open__callback" style="color:#ff0000;"><strong>Заказать обратный звонок</strong></span>');
        $children = Arr::get($this->post, 'CHILDREN');
        $uid = Arr::get($this->post, 'uid',User::info()->id);
        $row = CustomerChildren::getRow($uid,'uid');
        if(!$row){
            $this->error('Ошибка обновления данных');
        }
        if(!empty($children)){
            foreach ($children as $child){
                $data = [];
                $name = trim(strip_tags($child['name']));
                if (!$name || mb_strlen($name, 'UTF-8') < 2) {
                    $this->error('Слишком короткое имя! Нужно хотя бы 2 символов');
                }
                $data['name'] = $name;
                if(!empty($child['birthday'])){
                    $birthday = str_replace('.','/',trim(strip_tags($child['birthday'])));
                    $data['birthday'] = strtotime($birthday);
                }
                CustomerChildren::update($data,$child['id']);
            }
        }
        DB::update('users')->set(['updated_at' => time()])->where('id', '=', $uid)->execute();
        $this->success(['success' => true, 'response' => 'Вы успешно обновили информацию!']);
    }

    /**
     * @add_childAction
     * Добавление информации о ребёнке в личном кабинете
     * @template Views/Widgets/Popup/AddChild.php
     */
    public function add_childAction(): void
    {
        $limit = Config::get('basic.limit_children');
        $uid = Arr::get($this->post, 'uid',User::info()->id);
        $birthday = Arr::get($this->post, 'birthday');
        $sex = Arr::get($this->post, 'sex');
        $name = trim(strip_tags(Arr::get($this->post, 'name')));
        if (!$name || mb_strlen($name, 'UTF-8') < 2) {
            $this->error('Слишком короткое имя! Нужно хотя бы 2 символов');
        }
        $count = CustomerChildren::getCustomRows(['uid' => $uid])->count();
        if ($count == $limit){
            $this->error('Извините,нельзя добавить больше чем '.$limit.' детей в личном кабинете!');
        }
        CustomerChildren::insert([
            'birthday' => !empty($birthday) ? strtotime($birthday) : NULL,
            'name' => $name,
            'uid' => $uid,
            'sex' => $sex
        ]);
        DB::update('users')->set(['updated_at' => time()])->where('id', '=', $uid)->execute();
        Message::GetMessage(1, 'Вы успешно добавили информацию!');
        $this->success(['reload' => true]);
    }

}
