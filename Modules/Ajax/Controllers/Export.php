<?php

namespace Modules\Ajax\Controllers;

use Core\Arr;
use Core\Log;
use Core\QB\DB;
use Core\User;
use Core\View;
use Modules\Ajax;
use Modules\Catalog\Models\Items;
use Modules\Export\Models\Prom;
use Modules\Export\Models\PromTmp;
use Wezom\Modules\Export\Service\PromService;

class Export extends Ajax
{
    protected $post;

    function before()
    {
        parent::before();
    }

    public function productsCategoriesAction()
    {
        $brands = Arr::get($this->post, 'sort');
        $amount = Arr::get($this->post, 'amount');
        $category_id = Arr::get($this->post, 'category_id');
        $availability = 'true';
        $range = Arr::get($this->post, 'range');
        $select_cat = Arr::get($this->post, 'select', false);

        $tmpChecked = PromTmp::getAll();

        if ($tmpChecked) {
            $checkedProductsGroups = Items::getParentIdsByProducts(array_keys($tmpChecked), 'prom');
        }
        //($checkedProductsGroups[$category_id]) ? $select = false : $select = true;

        $select = false;
        $products = Items::getProductsByGroup($category_id, $brands, $amount, $availability, false, 'prom');

        $prom_info = DB::select()->from('prom_info')->find('prom');

        $content = View::tpl([
            'products' => $products,
            'select' => $select,
            'select_cat' => $select_cat,
            'checkedProducts' => $tmpChecked,
            'range' => $range,
            'checkedProductsGroups' => $checkedProductsGroups ?? [],
            'prom_info' => $prom_info
        ], 'User/Export/Partials/PromProducts');

        $this->success([
            'html' => $content
        ]);
    }

    public function filterProductsAction()
    {
        $brands = Arr::get($this->post, 'sort');
        $amount = Arr::get($this->post, 'amount');
        $categories = Arr::get($this->post, 'openedCategories');
        $availability = 'true';
        $select = Arr::get($this->post, 'select', false);
        $range = Arr::get($this->post, 'range');

        $checkedProducts = PromTmp::getAll();

        $content = [];

        $prom_info = DB::select()->from('prom_info')->find('prom');

        $checked = PromTmp::deleteByFilter($amount, $availability, $brands);

        if (!empty($checked)) {
            $parentIds = Items::getParentIdsByProducts(array_keys($checked), 'prom');
            $checkedCategories = [];
            foreach ($parentIds as $id => $obj) {
                $products = Items::getProductByCatIdExport($id, $amount, $availability, $brands, 'prom');
                $merge = array_intersect_key($products, $checked);

                if (count($products) == count($merge)) {
                    $checkedCategories[$id] = false;
                } else {
                    $checkedCategories[$id] = true;
                }
            }
        }

        if (!empty($categories)) {
            foreach ($categories as $category) {
                $products = Items::getProductsByGroup($category, $brands, $amount, $availability, false, 'prom');

                $content[$category] = $this->_content = View::tpl([
                    'products' => $products,
                    'select' => $select,
                    'checkedProducts' => $checkedProducts,
                    'range' => $range,
                    'prom_info' => $prom_info
                ], 'User/Export/Partials/PromProducts');
            }
        }

        $this->success([
            'checkedCategories' => $checkedCategories ?? [],
            'products' => $content ?? []
        ]);
    }

    public function savePromAction()
    {
        $data = [];
        $prom_id = Arr::get($this->post, 'promId');
        //$products = Arr::get($this->post, 'products');
        $availability = 'true';
        $emptyCategories = Arr::get($this->post, 'checkeredEmptyCategories');
        $data['shop_name'] = Arr::get($this->post, 'shopName', ' ');
        $data['company_name'] = Arr::get($this->post, 'companyName', ' ');
        $data['user_id'] = User::info()->id;
        $data['updated_at'] = time();
        $range = Arr::get($this->post, 'range');
        $data['slider'] = json_encode($range);
        $data['update'] = 1;

        $tmpProducts = PromTmp::getAll();
        PromTmp::deleteAllUserProduct();
        foreach ($tmpProducts as $id => $obj) {
            foreach ($range as $r) {
                $min_max = explode('-', $r[0]);
                if ($min_max[0] <= $obj->price && $min_max[1] >= $obj->price) {
                    $products[] = [$id, $obj->price + ($obj->price * ((int)$r[1] / 100)), $obj->category_product_id];
                }
            }
        }

        if (!$prom_id) {
            $data['created_at'] = time();

            $prom_id = Prom::insert($data);
            // Save log
            $qName = 'Новый шаблон выгрузки';
            $url = '/wezom/prom/edit/' . $prom_id;
            Log::add($qName, $url, 11);
        } else {
            Prom::update($data, $prom_id);

            Prom::deleteRelation($prom_id, 'prom_id');

        }

        if ($products) {
            Prom::insertRelation($prom_id, $products);
            sleep(30);

            $template = Prom::getRow($prom_id);
            $generateXml = PromService::create();
            $generateXml->generateXml($template, 'User/Export/Partials/TableSize', 'User/Export/Partials/Xml');
        }
        $this->success([
            'message' => 'OK'
        ]);
    }

    public function saveTmpAction()
    {
        $categoriesId = Arr::get($this->post, 'categoriesId');
        $productId = Arr::get($this->post, 'productId');
        $availability = 'true';
        $brands = Arr::get($this->post, 'sort');
        $amount = Arr::get($this->post, 'amount');
        $checkeredEmptyCategories = Arr::get($this->post, 'checkeredEmptyCategories');
        $checkedCategories = Arr::get($this->post, 'checkedCategories');
        $checkedProduct = Arr::get($this->post, 'checkedProduct');
        $dellAll = Arr::get($this->post, 'dellAll');


        if ($dellAll != null) { // CLICK IN REMOVE ALL
            PromTmp::deleteAllUserProduct();
        } elseif ($checkeredEmptyCategories != null) { // CLICK IN ALL
            $products = Items::getProductsByGroup($checkeredEmptyCategories, $brands, $amount, $availability, true, 'prom')->as_array('id');
            PromTmp::insertManyProducts($products);
        } elseif ($checkedCategories != null) { // CLICK IN CATEGORY
            $products = Items::getProductsByGroup($categoriesId, $brands, $amount, $availability, true, 'prom')->as_array('id');
            if ($checkedCategories == 'true') { // checked
                PromTmp::insertManyProducts($products);
            } else { // unchecked
                PromTmp::deleteManyProducts($products);
            }
        } elseif ($checkedProduct != null) { // CLICK IN PRODUCT
            if ($checkedProduct == 'true') { // checked
                $categoryId = Items::getParentIdRow($productId[0]);
                PromTmp::insertPromProduct($productId[0], $categoryId->parent_id);
            } else { // unchecked
                PromTmp::deletePromProduct($productId[0]);
            }
        }
    }

}
