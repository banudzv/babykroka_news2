<?php
namespace Modules\Ajax\Controllers;

use Core\Arr;
use Core\Widgets;
use Modules\Ajax;

class Popup extends Ajax
{

    public function getPopupAction()
    {
        header("Content-type: text/html");
        $name = Arr::get($_POST, 'name');
        $arr = Arr::get($_POST, 'param', array());
        $widget = Widgets::get('Popup_' . $name, $arr);
        echo $widget;
        die;
    }

}