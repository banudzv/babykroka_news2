<?php
namespace Modules\Delivery\Controllers;

use Core\HTML;
use Core\Route;
use Core\Config;
use Modules\Base;
use Modules\Content\Models\Control;

class Delivery extends Base
{

    public $current;

    public function before()
    {
        parent::before();
        $this->current = Control::getRowSimpleDelivery(Route::controller(), 'alias', 1);
        if (!$this->current) {
            return Config::error();
        }
        $this->setLastModifiedHeader($this->current->updated_at);
        $this->setBreadcrumbs($this->current->name, $this->current->alias);
    }

    public function indexAction()
    {
        $this->_template = 'Delivery/Index';
        // Check for existance
        if (Config::get('error')) {
            return false;
        }

        // Seo
        $this->_seo['h1'] = $this->current->h1;
        $this->_seo['title'] = $this->current->title;
        $this->_seo['keywords'] = $this->current->keywords;
        $this->_seo['description'] = $this->current->description;
        // Render template
        $this->_delivery = $this->current->text;
        $this->_pay = $this->current->other;
        $this->_return = $this->current->delivery_return;

        $this->_critical = HTML::style(HTML::media('assets/css/critical/generated/delivery-info.css'));
    }

}
