<?php
namespace Modules\Sitemap\Controllers;

use Core\CommonI18n;
use Core\Config;
use Core\HTML;
use Core\QB\DB;
use Core\Route;
use Core\View;
use Modules\Base;
use Modules\Content\Models\Control;
use Modules\News\Models\News;
use Modules\Sitemap\Models\Sitemap as Model;
use Modules\Sitemap\Models\SitemapXml;

class Sitemap extends Base
{

    public $current;

    public function before()
    {
        parent::before();
        $this->current = Control::getRowSimple(Route::controller(), 'alias', 1);
        if (!$this->current) {
            return Config::error();
        }
        $this->setBreadcrumbs($this->current->name, $this->current->alias);
    }

    // Search list
    public function indexAction()
    {
        $this->_template = 'Sitemap';
        if (Config::get('error')) {
            return false;
        }
        // Seo
        $this->_seo['h1'] = $this->current->h1;
        $this->_seo['title'] = $this->current->title;
        $this->_seo['keywords'] = $this->current->keywords;
        $this->_seo['description'] = $this->current->description;

		$map = Model::getRows(1,'sort','ASC');
		$arr = [];
		foreach ($map as $obj) {
			$arr[$obj->parent_id][] = $obj;
		}

		$links = [];
		if (isset($map['content'])) {
			$result = CommonI18n::factory('content')->getRows(1, 'sort', 'ASC');
			$pages = [];
			foreach ($result as $obj) {
                    $pages[$obj->parent_id][] = $obj;
			}
			$links['content'] = $pages;
		}
		if (isset($map['news_list'])) {
			$list = News::getRows(1, 'date', 'DESC');
			$links['news_list'] = $list;
		}

		if (isset($map['articles_list'])) {
			$list = CommonI18n::factory('articles')->getRows(1, 'id', 'DESC');
			$links['articles_list'] = $list;
		}

		if (isset($map['blog_rubrics'])) {
			$list = CommonI18n::factory('blog_rubrics')->getRows(1, 'sort', 'ASC');
			$links['blog_rubrics'] = $list;
		}

		if (isset($map['blog_list'])) {
			$list = CommonI18n::factory('blog')->getRows(1, 'date', 'DESC');
			$links['blog_list'] = $list;
		}

		if (isset($map['gallery_list'])) {
			$list = CommonI18n::factory('gallery')->getRows(1, 'sort', 'ASC');
			$links['gallery_list'] = $list;
		}

		if (isset($map['catalog_groups'])) {
			$list = CommonI18n::factory('catalog_tree')->getRows(1, 'sort', 'ASC');

			$pages = [];
			foreach ($list as $obj) {
				$pages[$obj->parent_id][] = $obj;
			}
			$links['catalog_groups'] = $pages;
		}
		if (isset($map['catalog_items'])) {
			$list = CommonI18n::factory('catalog')->getRows(1, 'sort', 'ASC');
			$pages = [];
			foreach ($list as $obj) {
				$pages[$obj->parent_id][] = $obj;
			}
			$links['catalog_items'] = $pages;
		}

		if (isset($map['brands_list'])) {
			$list = CommonI18n::factory('brands')->getRows(1, 'sort', 'ASC');
			$links['brands_list'] = $list;
		}
        // Render page
        $this->result = $arr;
        $this->links = $links;
        $this->_critical = HTML::style(HTML::media('assets/css/critical/generated/sitemap.css'));
    }

	public function xmlAction() {
		$control = Control::getForSitemapXml()->as_array();
		$content = CommonI18n::factory('content')->getRows(1, 'sort', 'ASC')->as_array();
		$news = News::getRows(1, 'date', 'DESC')->as_array();
		$articles = CommonI18n::factory('articles')->getRows(1, 'id', 'DESC')->as_array();
		$groups = CommonI18n::factory('catalog_tree')->getRows(1, 'sort', 'ASC')->as_array();
		$items = CommonI18n::factory('catalog')->getRows(1, 'sort', 'ASC')->as_array();

        $locales = DB::select()->from('i18n')->execute()->as_array('alias');

		$allLinks = [
			['items' => $control, 'linkTpl' => '{alias}'],
			['items' => $content, 'linkTpl' => '{alias}'],
			['items' => $news, 'linkTpl' => 'news/{alias}'],
			['items' => $articles, 'linkTpl' => 'articles/{alias}'],
			['items' => $groups, 'linkTpl' => 'catalog/{alias}'],
			['items' => $items, 'linkTpl' => '{alias}'],
		];

		$count = count($control) + count($content) + count($news) + count($articles) + count($groups) + count($items);
        $count = $count*2;
		$pathes = ['sitemap.xml'];

		$maxLinksCount = 10000;

		if ($count > $maxLinksCount) {
			$pathes = [];
			$cntFiles = ceil($count/$maxLinksCount);
			for ($i=1; $i<=$cntFiles; $i++) {
				$pathes[]= 'sitemap'.$i.'.xml';
			}
			
			SitemapXml::createIndexSitemap($pathes, $this->_scheme);
		}
		$fileNumber = 0;
		$urlNumber = 0;
		foreach ($allLinks as $key=>$arr) {
			foreach ($arr['items'] as $item) {
				if ($urlNumber == 0) {
					$dom = new \domDocument('1.0', 'utf-8');
					$root = $dom->createElement('urlset');
					$root->setAttribute('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9');
					$root->setAttribute('xmlns:xhtml', 'http://www.w3.org/1999/xhtml');
					$root->setAttribute('xmlns:image', 'http://www.google.com/schemas/sitemap-image/1.1');
				}

				$link = str_replace(
                    ['{id}','{alias}', '{link}'],
                    [$item->id, $item->alias ?? '', $item->link ?? ''],
                    $arr['linkTpl']
                );

                $languages = Config::get('languages');
                $defaultLocaleCode = Config::get('i18n.default');

                foreach ($languages as $language) {
                    $url = $language['alias'] === $defaultLocaleCode
                        ? HTML::link($link, true, $this->_scheme)
                        : HTML::link($link, true, $this->_scheme, false, $language['alias']);

                    $root->appendChild(
                        SitemapXml::createUrlElement(
                            $link,
                            $dom,
                            $url,
                            $this->_scheme,
                            $item->updated_at ?? null,
                            $locales
                        )
                    );
                    $urlNumber++;
                }

				if ($urlNumber >= $maxLinksCount) {
					$dom->formatOutput = true;
					$dom->appendChild($root);
					$dom->save(HOST.'/'.$pathes[$fileNumber]);
					$urlNumber = 0;
					$fileNumber++;
				}
			}
		}


		if ($urlNumber != $maxLinksCount) {
			$dom->formatOutput = true;
			$dom->appendChild($root);
			$dom->save(HOST.'/'.$pathes[$fileNumber]);
		}

		die('Карта сайта обновлена');

	}
}
