<?php
namespace Modules\Sitemap\Models;

use Core\Config;
use Core\HTML;

class SitemapXml
{

    
	public static function createIndexSitemap($pathes, $scheme) {
		
		$dom = new \domDocument('1.0', 'utf-8');
        $root = $dom->createElement('sitemapindex');
		$root->setAttribute('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9');
		
		foreach ($pathes as $path) {
			$root->appendchild(self::createSitemapElement($path, $dom, $scheme));
		}
		
		$dom->formatOutput = true;
		 $dom->appendChild($root);
        $dom->save(HOST.'/sitemap.xml');
		return true;
		
	}
	
	public static function createSitemapElement($path, $dom, $scheme = null, $lastModification = null) {
		
		$sitemap = $dom->createElement('sitemap');
		$loc = $dom->createElement('loc', HTML::link($path, true, $scheme));
		$sitemap->appendChild($loc);
		if ($lastModification !== null) {
			$lastmod = $dom->createElement('lastmod', date('Y-m-d\TH:i:sP', $lastModification));
			$sitemap->appendChild($loc);
		}
		
		return $sitemap;
		
	}
	
	public static function createUrlElement($link, $dom, $pageUrl, $scheme = null, $lastModification = null, $locales = []) {

		$url = $dom->createElement('url');
		$loc = $dom->createElement('loc', $pageUrl);
		$url->appendChild($loc);
		if ($lastModification !== null) {
			$lastmod = $dom->createElement('lastmod', date('Y-m-d\TH:i:sP', $lastModification));
			$url->appendChild($lastmod);
		}
		if (!empty($locales)) {
		    $defaultLocaleCode = Config::get('i18n.default');
		    foreach($locales as $code => $locale) {
		        $alternateElement = $dom->createElement('xhtml:link');

		        $relAttribute = $dom->createAttribute('rel');
		        $relAttribute->value = 'alternate';
                $alternateElement->appendChild($relAttribute);

		        $hreflangAttribute = $dom->createAttribute('hreflang');
                $hreflangAttribute->value = $locale['locale'];
                $alternateElement->appendChild($hreflangAttribute);

                $hrefAttribute = $dom->createAttribute('href');
                $langLink = $code != $defaultLocaleCode ? HTML::link($link, true, $scheme, false, $code) : HTML::link($link, true, $scheme);
                $hrefAttribute->value = $langLink;
                $alternateElement->appendChild($hrefAttribute);

                $url->appendChild($alternateElement);
            }
        }
		
		return $url;
	}
    
	
}