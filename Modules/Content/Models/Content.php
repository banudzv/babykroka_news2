<?php
namespace Modules\Content\Models;

use Core\CommonI18n;
use Core\QB\DB;

class Content extends CommonI18n
{

    public static $table = 'content';

    public static function getKids($id, $status = null)
    {
        static::$tableI18n = static::$table . '_i18n';
        $lang = \I18n::$lang;

        $kids = DB::select()
            ->from(static::$table)
            ->join(static::$tableI18n, 'LEFT')->on(static::$tableI18n . '.row_id', '=', static::$table . '.id')
            ->where(static::$tableI18n . '.language', '=', $lang)
            ->where(static::$table . '.parent_id', '=', $id);
        if ($status !== null) {
            $kids->where('status', '=', $status);
        }
        return $kids->order_by('sort', 'ASC')->find_all();
    }

}
