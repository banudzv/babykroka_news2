<?php
namespace Modules\Content\Models;

use Core\CommonI18n;
use Core\QB\DB;

class Control extends CommonI18n
{

    public static $table = 'control';

	public static function getForSitemapXml() {

		$result = DB::select()
					->from(static::$table)
					->where('status', '=', 1)
					->where('sitemapxml', '=', 1)
					->order_by('id', 'ASC')
					->find_all();

		return $result;

	}

    public static function getRowSimpleDelivery($value, $field = 'id', $status = NULL) {
        $lang = \I18n::$lang;
        if (APPLICATION == 'backend') {
            $lang = \I18n::$defaultLangBackend;
        }
        static::$tableI18n = static::$table . '_i18n';
        $row = DB::select(
            static::$tableI18n . '.*',
            static::$table . '.alias',
            static::$table . '.status',
            static::$table . '.id',
            static::$table . '.updated_at',
            static::$table . '.sitemapxml'
        )
            ->from(static::$table)
            ->join(static::$tableI18n, 'LEFT')->on(static::$tableI18n . '.row_id', '=', static::$table . '.id')
            ->where(static::$table . '.' . $field, '=', $value);
        if ($status <> NULL) {
            $row->where(static::$table . '.status', '=', $status);
        }
        return $row->where(static::$tableI18n . '.language', '=', $lang)->find();
    }

}
