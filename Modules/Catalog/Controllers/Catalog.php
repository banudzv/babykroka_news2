<?php

namespace Modules\Catalog\Controllers;

use Core\CommonI18n;
use Core\HTML;
use Core\QB\DB;
use Core\Route;
use Core\View;
use Core\Config;
use Core\Pager\Pager;
use Core\Arr;
use Modules\Catalog\Models\Favorite;
use Modules\Catalog\Models\Filter;
use Core\Text;
use Core\HTTP;
use Modules\Base;
use Modules\Catalog\Models\FilterPages;
use Modules\Catalog\Models\Groups;
use Modules\Catalog\Models\Groups AS Model;
use Modules\Content\Models\Control;
use Modules\User\Controllers\User;

class Catalog extends Base
{

    public $current;
    public $sort;
    public $type;
    public $_critical;
    protected $_template = 'Catalog';

    public function before()
    {
        parent::before();
        $this->current = Control::getRowSimple('catalog', 'alias', 1);
        $this->setLastModifiedHeader($this->current->updated_at);
        if (!$this->current) {
            return Config::error();
        }
        $this->setBreadcrumbs($this->current->name, $this->current->alias);
        $this->_page = !(int)Route::param('page') ? 1 : (int)Route::param('page');
        $limit = Config::get('basic.limit_groups');
        $sort = 'top';
        $type = 'ASC';
        $this->_limit = (int)Arr::get($_GET, 'per_page') ? (int)Arr::get($_GET, 'per_page') : $limit;
        $this->_offset = ($this->_page - 1) * $this->_limit;
        $this->sort = $sort;
        $this->type = in_array(strtolower(Arr::get($_GET, 'type')), ['asc', 'desc']) ? strtoupper(Arr::get($_GET, 'type')) : $type;
        $this->_critical = HTML::style(HTML::media('assets/css/critical/generated/catalog.css'));
        $_SESSION['order_id'] = null;
    }

    // Catalog main page with groups where parent_id = 0
    public function indexAction()
    {
        if (Config::get('error')) {
            return false;
        }
        // Seo
        $this->_seo['h1'] = $this->current->h1;
        $this->_seo['title'] = $this->current->title;
        $this->_seo['keywords'] = $this->current->keywords;
        $this->_seo['description'] = $this->current->description;

        $this->_seo['seo_text'] = $this->current->text;

        $result = Model::getInnerGroups(0);
        $this->setLastModifiedHeader($result->updated_at);
        // Count of parent groups
        $count = Model::countInnerGroups(0);
        // Generate pagination
        $this->_pager = Pager::factory($this->_page, $count, $this->_limit);
        //canonicals settings
        $this->_use_canonical = 1;
        $this->_canonical = 'catalog';
        // Render template
        Config::set('nofilter', true);
        $this->_content = View::tpl(['result' => $result, 'pager' => $this->_pager->create()], 'Catalog/Groups');
    }

    // Page with groups list
    public function groupsAction()
    {
        if (Route::param('page') && Route::param('page') == 1) {
            $uri = '/catalog/' . Route::param('alias');
            if (Route::param('filter')) {
                $uri .= '/' . Route::param('filter');
            }

            HTTP::redirect($uri, 301);
        }

        if (Config::get('error')) {
            return false;
        }

        // Check for existance
        $group = Model::getRowSimple(Route::param('alias'), 'alias');
        $this->setLastModifiedHeader($group->updated_at);
        if (!$group) {
            return Config::error();
        }
        if ($group->status != 1) {
            HTTP::redirect('/catalog', 301);
        }
        Route::factory()->setParam('group', $group->id);
        // Count of child groups
        $count = Model::countInnerGroups($group->id);
        if (!$count) {
            return $this->listAction();
        }

        //canonicals settings
        $this->_use_canonical = 1;
        $this->_canonical = 'catalog/' . Route::param('alias');

        if (Route::param('filter')) {
            $this->_seo['nofollow'] = true;
            $filter = Filter::getFilterArr(Route::param('filter'));
            Config::set('filter_array', $filter);
            if ($filter['sort']) {
                $this->sort = $filter['sort'];
            }
            $result = Filter::getFilteredItemsList($this->_limit, $this->_offset, $this->sort, $this->type, null , false);
            $this->setSeoForFilter($group, $result['total']);
        } else {
            $result = Filter::getFullFilteredItemsList($this->_limit, $this->_offset, $this->sort, $this->type, null, false);
            $this->setSeoForGroup($group);
            if (empty($result['items'])) {
                $this->_seo['nofollow'] = true;
            }
        }

        Filter::setFilterParameters();

        // Set filter elements sortable
//		Filter::setSortElements();
        // Add plus one to views
        Model::addView($group);
        // Get groups list

        $fav = [];
        if (\Core\User::info()) {
            $favs = Favorite::getFavorites(\Core\User::info()->id);
            foreach ($favs as $f) {
                $fav[] = (int)$f->id;
            }
        }

        $this->_pager = Pager::factory($this->_page, $result['total'], $this->_limit);
        if( ($this->_page * $this->_limit) < $result['total']){
            $last = false;
        } else{
            $last = true;
        }

        $_GET['products'] = $result['items'];
        $_GET['page_type'] = 'category';
        // Render template
        $this->_content = View::tpl(['result' => $result['items'], 'total' => $result['total'], 'favorite' => $fav, 'cats' => $result['cats'], 'pager' => $this->_pager->create(), 'last' => $last ], 'Catalog/ItemsList');
    }

    // Items list page. Inside group
    public function listAction()
    {
        if (Config::get('error')) {
            return false;
        }
        $this->_template = 'Catalog';
        Route::factory()->setAction('list');
        // Filter parameters to array if need
        $check = Filter::setFilterParameters();
        if ($check['success'] === false) {
            return Config::error();
        }
        if ($check['success'] === true and $check['resort'] === true) {
            $new_filter = Filter::getFilterFromArr(Config::get('filter_array'));
            $url = '/catalog/' . Route::param('alias') . $new_filter;
            HTTP::redirect($url, 301);
        }
        // Set filter elements sortable
        // Check for existance
        $group = Model::getRowSimple(Route::param('alias'), 'alias');
        if (!$group) {
            return Config::error();
        }
        if ($group->status != 1) {
            HTTP::redirect('/catalog', 301);
        }
        // Seo
        $this->_use_canonical = 1;
        $this->_canonical = 'catalog/' . Route::param('alias');

        // Set filter elements sortable
//		Filter::setSortElements();

        // Add plus one to views
        Model::addView($group);
        // Get items list
        if (Route::param('filter')) {
            $this->_seo['nofollow'] = true;
            $filter = Filter::getFilterArr(Route::param('filter'));
            if ($filter['sort']) {
                $this->sort = $filter['sort'];
            }
        }
        $this->_limit = (int)Arr::get($_GET, 'per_page') ? (int)Arr::get($_GET, 'per_page') : Config::get('basic.limit');
        $this->_offset = ($this->_page - 1) * $this->_limit;
        $result = Filter::getFilteredItemsList($this->_limit, $this->_offset, $this->sort, $this->type);
        if (empty($result['items'])) {
            $this->_seo['nofollow'] = true;
        }

        if (Route::param('filter')) {
            $this->setSeoForFilter($group, $result['total']);
        } else {
            $this->setSeoForGroup($group);
        }

        $fav = [];
        if (\Core\User::info()) {
            $favs = Favorite::getFavorites(\Core\User::info()->id);
            foreach ($favs as $f) {
                $fav[] = (int)$f->id;
            }
        }
        // Generate pagination
        $this->_pager = Pager::factory($this->_page, $result['total'], $this->_limit);
        //canonicals settings
        if( ($this->_page * $this->_limit) < $result['total']){
            $last = false;
        } else{
            $last = true;
        }

        $_GET['products'] = $result['items'];
        $_GET['page_type'] = 'category';
        // Render page
        $this->_content = View::tpl(['result' => $result['items'], 'total' => $result['total'], 'favorite' => $fav, 'cats' => $result['cat'], 'pager' => $this->_pager->create(), 'last' => $last], 'Catalog/ItemsList');
    }

    // Set seo tags from template for items groups
    public function setSeoForGroup($page)
    {
        $tpl = CommonI18n::factory('seo_templates')->getRowSimple(1, 'id');
        $from = ['{{name}}', '{{content}}'];
        $text = trim(strip_tags($page->text));
        $h1 = $page->h1 ?: $page->name;
        $to = [$h1, $text];
        $res = preg_match_all('/{{content:[0-9]*}}/', $tpl->description, $matches);
        if ($res) {
            $matches = array_unique($matches);
            foreach ($matches[0] AS $pattern) {
                preg_match('/[0-9]+/', $pattern, $m);
                $from[] = $pattern;
                $to[] = Text::limit_words($text, $m[0]);
            }
        }

        $title = $page->title ? $page->title : $tpl->title;
        $h1 = $page->h1 ? $page->h1 : $tpl->h1;
        $keywords = $page->keywords ? $page->keywords : $tpl->keywords;
        $description = $page->description ? $page->description : $tpl->description;

        if ($this->_page > 1) {
            $h1 .= ' ' . __('страница') . ' ' . $this->_page;
        }

        $this->_seo['h1'] = str_replace($from, $to, $h1);
        $this->_seo['title'] = str_replace($from, $to, $title)
            . ((Arr::get($_GET, 'sort') == 'cost' && Arr::get($_GET, 'type') == 'asc') ? ', От бютжетных к дорогим' : '')
            . ((Arr::get($_GET, 'sort') == 'cost' && Arr::get($_GET, 'type') == 'desc') ? ', От дорогих к бютжетным' : '')
            . ((Arr::get($_GET, 'sort') == 'created_at' && Arr::get($_GET, 'type') == 'desc') ? ', От новых моделей к старым' : '')
            . ((Arr::get($_GET, 'sort') == 'created_at' && Arr::get($_GET, 'type') == 'asc') ? ', От старых моделей к новым' : '')
            . ((Arr::get($_GET, 'sort') == 'name' && Arr::get($_GET, 'type') == 'asc') ? ', По названию от А до Я' : '')
            . ((Arr::get($_GET, 'sort') == 'name' && Arr::get($_GET, 'type') == 'desc') ? ', По названию от Я до А' : '');
        $this->_seo['keywords'] = str_replace($from, $to, $keywords);
        $this->_seo['description'] = str_replace($from, $to, $description);
        if (!Route::param('page')) {
            $this->_seo['seo_text'] = $page->text;
        }
        $this->generateParentBreadcrumbs($page->parent_id, 'catalog_tree', 'parent_id', '/catalog/');
        $this->setBreadcrumbs($page->name, $_SERVER['REQUEST_URI']);
    }

    // Set seo tags from template for items groups with filter
    public function setSeoForFilter($page, $product_count)
    {
        $categoryId = $page->id;

        $filterHash = Filter::getFilterHash(Route::param('filter'));
        $filterPageData = FilterPages::getFilterPageData($categoryId, $filterHash);

        if ($filterPageData && $filterPageData['hash'] === $filterHash) {
            $this->_noindex = false;
            $this->_seo['nofollow'] = false;
            $this->_seo['seo_text'] = $filterPageData['seo_text'];
            $this->_seo['h1'] = $filterPageData['h1'];
            $this->_seo['title'] = $filterPageData['title'];
            $this->_seo['keywords'] = $filterPageData['keywords'];
            $this->_seo['description'] = $filterPageData['description'];

            $this->_use_canonical = 1;
            $this->_canonical = $_SERVER['REQUEST_URI'];
        }
        else {
            $tpl = CommonI18n::factory('seo_templates')->getRowSimple(6, 'id');

            $acceptedCategories = [3, 5, 6];
            $rootCategory = $page->parent_id == 0 ? $page->id : Groups::getRootCategory($page->parent_id)->id;

            $acceptedFilters = ['brand', 'color', 'sezon', 'material', 'age', 'size', 'polrebenka'];
            $filters = Filter::getFilterArr(Route::param('filter'));
            $filterKeys = array_keys($filters);

            if (count($filterKeys) == 1
                && count($filters[$filterKeys[0]]) == 1
                && in_array($rootCategory, $acceptedCategories)
                && in_array($filterKeys[0], $acceptedFilters)
                && $product_count >= 5
            ) {
                $this->_seo['nofollow'] = false;
                $this->_use_canonical = 1;
                $canonical = str_replace('/page/' . $this->_page, '', $_SERVER['REQUEST_URI']);
                $this->_canonical = $canonical;
            }

            $filter = Filter::getVauluesAsText2();
            $filterValues = Filter::getValuesAsText3();

            $h1 = $page->h1 ?: $page->name;
            $from = ['{{group}}', '{{filter}}', '{{site}}', '{{filter_values}}'];
            $text = trim(strip_tags($page->text));
            $to = [$h1, $filter, $_SERVER['HTTP_HOST'], $filterValues];
            $res = preg_match_all('/{{content:[0-9]*}}/', $tpl->description, $matches);
            if ($res) {
                $matches = array_unique($matches);
                foreach ($matches[0] AS $pattern) {
                    preg_match('/[0-9]+/', $pattern, $m);
                    $from[] = $pattern;
                    $to[] = Text::limit_words($text, $m[0]);
                }
            }

            $title = $tpl->title;
            $h1 = $tpl->h1;
            $keywords = $tpl->keywords;
            $description = $tpl->description;

            if ($this->_page > 1) {
                $h1 .= ' ' . __('страница') . ' ' . $this->_page;
            }

            $this->_seo['h1'] = str_replace($from, $to, $h1);
            $this->_seo['title'] = str_replace($from, $to, $title)
                . ((Arr::get($_GET, 'sort') == 'cost' && Arr::get($_GET, 'type') == 'asc') ? ', От бютжетных к дорогим' : '')
                . ((Arr::get($_GET, 'sort') == 'cost' && Arr::get($_GET, 'type') == 'desc') ? ', От дорогих к бютжетным' : '')
                . ((Arr::get($_GET, 'sort') == 'created_at' && Arr::get($_GET, 'type') == 'desc') ? ', От новых моделей к старым' : '')
                . ((Arr::get($_GET, 'sort') == 'created_at' && Arr::get($_GET, 'type') == 'asc') ? ', От старых моделей к новым' : '')
                . ((Arr::get($_GET, 'sort') == 'name' && Arr::get($_GET, 'type') == 'asc') ? ', По названию от А до Я' : '')
                . ((Arr::get($_GET, 'sort') == 'name' && Arr::get($_GET, 'type') == 'desc') ? ', По названию от Я до А' : '');
            $this->_seo['keywords'] = str_replace($from, $to, $keywords);
            $this->_seo['description'] = str_replace($from, $to, $description);
        }

        $this->generateParentBreadcrumbs($page->parent_id, 'catalog_tree', 'parent_id', '/catalog/');
        $this->setBreadcrumbs($page->name, $_SERVER['REQUEST_URI']);
    }

}
