<?php
namespace Modules\Catalog\Controllers;

use Core\Arr;
use Core\CommonI18n;
use Core\Cookie;
use Core\HTML;
use Core\HTTP;
use Core\Pager\Pager;
use Core\QB\DB;
use Core\Route;
use Core\Support;
use Core\User;
use Core\View;
use Core\Config;
use Modules\Cart\Models\Cart;
use Modules\Catalog\Models\Colors;
use Modules\Catalog\Models\Favorite;
use Modules\Catalog\Models\Information;
use Modules\Catalog\Models\Items;
use Modules\Catalog\Models\Groups;
use Modules\Base;
use Modules\Catalog\Models\Manufacturers;
use Modules\Catalog\Models\Brands;
use Modules\Catalog\Models\Prices;
use Modules\Catalog\Models\ProductAges;

class Product extends Base
{

    public function before()
    {
        parent::before();
        $this->_template = 'Item';
        $_SESSION['order_id'] = null;
    }

    // Show item inner page
    public function indexAction()
    {
        $popup = Arr::get($_GET, 'popup') ?: false;
        // Get item information from database
        $alias = Route::param('alias');
        $item = Items::getRowSimple($alias, 'alias');

        $product_id = $item->id;
        $product_sizes = Items::getProductSizeAmountById($product_id)->as_array('code','amount');
        $sizes = DB::select()->from('helper_characteristics')->find_all()->as_array();
        $labels = [];
        foreach ($sizes as $size){
            if(isset($product_sizes[$size->code])){
                $label = DB::select()->from('helper_characteristics_sizes')
                    ->where('helper_characteristics_sizes.helper_characteristics_id', '=', $size->id)
                    ->where('helper_characteristics_sizes.parameter', '<>', '0')
                    ->find_all()->as_array('mark','parameter');
                if(!empty($label)){
                    $labels[$size->name] = $label;
                }
            }
        }

        $this->setLastModifiedHeader($item->updated_at);
		if (!$item) {
            return Route::factory()->start([
                'Modules',
                'Content',
                'Controllers',
                'Content'
            ], 'index');
        }
        if (Route::param('id')) {
            unset($_POST);
            HTTP::redirect($item->alias, 301);
        }
		if ($item->status != 1) {
			$group = Groups::getRow($item->parent_id);
			if ($group) {
				HTTP::redirect('/catalog/'.$group->alias, 301);
			} else {
				HTTP::redirect('/catalog', 301);
			}
		}
		$all = Items::getProductByCatId($item->parent_id);
		$temp = [];
		for ($i = 0, $iMax = count($all); $i < $iMax; $i++){
		    $temp[$i] = $all[$i]['id'];
        }
        $current = array_search($item->id, $temp);
		if (($current - 1 ) < 0){
		    $prev = count($all)-1;
        } else{
		    $prev = $current -1;
        }
        if (($current + 1 ) >= count($all)){
            $next = 0;
        } else{
            $next = $current + 1;
        }


        $this->current= $current+1;
        $this->previous = $all[$prev];
        $this->next = $all[$next];
        $this->all = count($all);


        Route::factory()->setParam('id', $item->id);
        Route::factory()->setParam('group', $item->parent_id);
        // Add to cookie viewed list
        Items::addViewed($item->id);
        // Add plus one to views
        $item = Items::addView($item);

        // Get images
        $images = Items::getItemImages($item->id)->as_array();
        // Get current item specifications list
        $spec = Items::getItemSpecificationsByID($item->id);
        if($item->krest or $item->vypiska or $item->karnaval or $item->torzgest){
            $spec[__('Признак')] = '';
            if ($item->krest ){
                $spec[__('Признак')] .= __('Квест') . ', ';
            }
            if ($item->vypiska ){
                $spec[__('Признак')] .= __('Выписка') . ', ';
            }
            if ($item->karnaval ){
                $spec[__('Признак')] .= __('Карнавал') . ', ';
            }
            if ($item->torzgest ){
                $spec[__('Признак')] .= __('Торжественый') . ', ';
            }
            $spec[__('Признак')] = substr($spec[__('Признак')],0,-2);
        }
        $fav = [];
        if (User::info()) {
            $favs = Favorite::getFavorites(User::info()->id);
            foreach ($favs as $f) {
                $fav[] = (int) $f->id;
            }
        }
        $manufacturer = Manufacturers::getRowSimple($item->manufacturer_alias, 'alias');
        $brand = Brands::getRowSimple($item->brand_alias, 'alias');
        $reviews = Items::getReviews($item->id)->as_array();
        $previews = Items::getPagedReviews($item->id, 3);
        $pager = Pager::factory(1, count($reviews), 3);
        $informations = Information::getRows(1,'sort', 'ASC');
        $arr = [];
        foreach($informations AS $obj) {
            $arr[$obj->parent_id][] = $obj;
        }
        if (isset($item->color_alias) && !empty($item->color_alias)){
            $res = Colors::getRowSimple($item->color_alias, 'alias');
            $item->color = $res->color;
            $item->color_name = $res->name;
        }
        if(isset($item->catalog_color_group_id)){
            $item->color_group = Items::getProductByColorCatId($item->catalog_color_group_id);
        }
        if(isset($item->age_alias)){
            $item->age = Items::getAgeByAlias($item->age_alias);
        }
        // Get size

        $countedFiles = null;
        if ($item->show_3d){
            $dir = HOST.'/Media/images/catalog_3d/'.$item->id;
            $files = scandir($dir);
            $countedFiles = count($files) - 2;
        }

        $prices = Prices::getPricesRowByPriceType($item->id,$_SESSION['prices']);
        $item->price = $prices->price;
        $item->price_old = $prices->price_old;
        $item->currency_id = $prices->currency_id;
        $table = Items::getTableSizesByGroupId($item->parent_id)->as_array();
        $prices_rozn = null;
        $item_size = DB::select()->from('carts_items')
            ->where('cart_id', '=', Cart::factory()->_cart_id)
            ->where('catalog_id', '=', $item->id)
            ->find();
        if ($item_size){
            $sizes_carts = DB::select()->from('carts_items_sizes')
                ->where('cart_item_id', '=', $item_size->id)
                ->find_all();
            $active_sizes = ['null'];
            $active_count = [];
            foreach ($sizes_carts as $size_cart){
                $active_sizes [] = $size_cart->size;
                $active_count [$size_cart->size] = $size_cart->count;

            }
        }else{
            $active_count = [];
            $active_sizes = [];
        }
        if(User::info() && Cookie::getWithoutSalt('user-type') !== 'rozn'){
            $prices_rozn = Prices::getPricesRowByPriceType($item->id,'875e4eb9-364f-11ea-80cc-a4bf01075a5b');
        }
        $itemAges = ProductAges::getProductAges($item->id);

        // Seo
        $this->setSeoForItem($item);

        $structuredData = array(
            '@context' => 'https://schema.org/',
            '@type' => 'Product',
            'name' => $item->name,
            'url' => HTML::link($item->alias . '/' . $item->id, true, 'https'),
            'description' => $item->content,
            'brand' => [
                '@type' => 'Thing',
                'name' => $brand->name
            ],
            'offers' => [
                '@type' => 'Offer',
                'priceCurrency' => 'UAH',
                'price' => $item->price,
                'availability' => $item->available ? 'http://schema.org/InStock' : 'http://schema.org/OutStock',
                'seller' => [
                    '@type' => 'Organization',
                    'name' => Config::get('basic.name_site'),
                    'url' => HTML::link('', true, 'https'),
                    'logo' => [
                        '@type' => 'ImageObject',
                        'url' => HTML::media('/assets/images/logo.svg')
                    ]
                ]
            ]
        );

        if ($item->image) {
            $structuredData['image'] = HTML::media('images/catalog/original/' . $item->image, true, 'https');
        }

        if (!empty($reviews)) {
            $reviewsCount = count($reviews);
            $sumRating = 0;

            foreach ($reviews as $review) {
                $structuredData['review'][] = array(
                    '@type' => 'Review',
                    'reviewRating' => [
                        '@type' => 'Rating',
                        'ratingValue' => $review->rate,
                        'bestRating' => '5'
                    ],
                    'author' => [
                        '@type' => 'Person',
                        'name' => $review->name
                    ]
                );

                $sumRating += $review->rate;
            }

            $structuredData['aggregateRating'] = array(
                '@type' => 'AggregateRating',
                'ratingValue' => $sumRating / $reviewsCount,
                'reviewCount' => $reviewsCount
            );
        }

        $_GET['product'] = $item;
        $_GET['page_type'] = 'oferdetail';

        // Render template
        $this->_content = View::tpl([
        	'obj' => $item,
        	'structuredData' => $structuredData,
        	'itemAges' => $itemAges,
        	'prices_rozn' => $prices_rozn,
			'count_3d' => $countedFiles,
			'images' => $images,
			'specifications' => $spec,
			'favorite' => $fav,
			'reviews' => $previews,
			'pager' => $pager->create_review(),
			'informations' => $arr,
			'sizes' => Items::getProductSizeAmountById($item->id)->as_array(),
            'labels' => $labels,
            'brand' => $brand,
            'manufacturer' => $manufacturer,
            'table' => $table,
            'popup' => $popup,
            'active_sizes'=>$active_sizes,
            'active_count'=>$active_count
        ], 'Catalog/Item');
        $this->_critical = HTML::style(HTML::media('assets/css/critical/generated/product-page.css'));
    }

    // Set seo tags from template for items
    public function setSeoForItem($page)
    {
        $priceData = Support::calculatePriceWithCurrency($page, $this->_currencies, $this->_current_currency);
        $tpl = CommonI18n::factory('seo_templates')->getRowSimple(2, 'id');
        $from = ['{{name}}', '{{group}}', '{{brand}}', '{{model}}', '{{price}}', '{{id}}', '{{color_name}}'];
        $h1 = $page->h1 ?: $page->name;
        $to = [$h1, $page->parent_name, $page->brand_name, $page->model_name, $priceData['cost'], $page->id, $page->color_name];
        $this->_seo['h1'] = $page->h1 ?: str_replace($from, $to, $tpl->h1);
        $this->_seo['title'] = $page->title ?: str_replace($from, $to, $tpl->title);
        $this->_seo['keywords'] = $page->keywords ?: str_replace($from, $to, $tpl->keywords);
        $this->_seo['description'] = $page->description ?: str_replace($from, $to, $tpl->description);
        $this->_seo['id'] = $page->id ?: str_replace($from, $to, $tpl->id);
        $this->_seo['color_name'] = $page->color_name ?: str_replace($from, $to, $tpl->color_name);
        $this->_seo['image'] = 'images/catalog/original/'. $page->image;
        $this->setBreadcrumbs('Каталог', '/catalog');
        $this->generateParentBreadcrumbs($page->parent_id, 'catalog_tree', 'parent_id', '/catalog/');
        $this->setBreadcrumbs($page->name, $_SERVER['REQUEST_URI']);
    }

}
