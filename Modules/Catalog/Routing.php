<?php

return [
    // Catalog unique aliases routing
    'new' => 'catalog/novelty/index',
    'new/page/<page:[0-9]*>' => 'catalog/novelty/index',
    'popular' => 'catalog/popular/index',
    'popular/page/<page:[0-9]*>' => 'catalog/popular/index',
    'promotions' => 'catalog/sale/index',
    'promotions/page/<page:[0-9]*>' => 'catalog/sale/index',
    'viewed' => 'catalog/viewed/index',
    'viewed/page/<page:[0-9]*>' => 'catalog/viewed/index',

    // Catalog groups routing
    'catalog' => 'catalog/catalog/index',
    'catalog/page/<page:[0-9]*>' => 'catalog/catalog/index',
    'catalog/<alias>' => 'catalog/catalog/groups',
    'catalog/<alias>/page/<page:[0-9]*>' => 'catalog/catalog/groups',

    'catalog/<alias:[0-9a-zA-Z_-]*>/<filter:[\w\W]*>page/<page:[0-9]*>' => 'catalog/catalog/groups',
    'catalog/<alias:[0-9a-zA-Z_-]*>/<filter:[\w\W]*>' => 'catalog/catalog/groups',
    'catalog/<alias:[0-9a-zA-Z_-]*>/<filter:[\w\W]*>/page/<page:[0-9]*>' => 'catalog/catalog/groups',
    'viewed/<filter:[\w\W]*>page/<page:[0-9]*>' => 'catalog/viewed/index',
    'viewed/<filter:[\w\W]*>' => 'catalog/viewed/index',
    'viewed/<filter:[\w\W]*>/page/<page:[0-9]*>' => 'catalog/viewed/index',
    'new/<filter:[\w\W]*>page/<page:[0-9]*>' => 'catalog/novelty/index',
    'new/<filter:[\w\W]*>' => 'catalog/novelty/index',
    'new/<filter:[\w\W]*>/page/<page:[0-9]*>' => 'catalog/novelty/index',
    'promotions/<filter:[\w\W]*>page/<page:[0-9]*>' => 'catalog/sale/index',
    'promotions/<filter:[\w\W]*>' => 'catalog/sale/index',
    'promotions/<filter:[\w\W]*>/page/<page:[0-9]*>' => 'catalog/sale/index',

    // Brands routing
    'brands' => 'catalog/brands/index',
    'brands/<alias>' => 'catalog/brands/inner',
    'brands/<alias>/page/<page:[0-9]*>' => 'catalog/brands/inner',

    //export files
    'export/yandex-market.xml' => 'catalog/export/yandexMarket',

    // Products routing
    '<alias>/p<id:[0-9]*>' => 'catalog/product/index',
    '<alias>' => 'catalog/product/index',
];
