<?php
namespace Modules\Catalog\Models;

use Core\Common;
use Core\QB\DB;

/**
 * Class @Prices
 * @package Wezom\Modules\Catalog\Models
 */
class Prices extends Common {

    public static $table = 'product_prices';
    public static $rules = [];

    /**
     * @param $product_id
     * @return array
     */
    public static function getPricesByProductId($product_id): array
    {
        return DB::select()->from(self::table())
            ->where('product_id', '=', $product_id)
            ->find_all()->as_array();
    }

    /**
     * @param $product_id
     * @param $price_type
     * @return mixed|null
     */
    public static function getPricesRowByPriceType($product_id,$price_type, $db = null)
    {
        return DB::select()->from(self::table())
            ->where('product_id', '=', $product_id)
            ->where('price_type', '=', $price_type)
            ->find($db);
    }

    /**
     * @param $product_id
     * @param $price_type
     * @return mixed|null
     */
    public static function getPricesRowByPriceTypeWithCurrency($product_id,$price_type)
    {
        return DB::select(self::table().'.*', 'currencies_courses.currency')->from(self::table())
            ->join('currencies_courses')->on('currencies_courses.id', '=', self::table().'.currency_id')
            ->where('product_id', '=', $product_id)
            ->where('price_type', '=', $price_type)
            ->find();
    }

}
