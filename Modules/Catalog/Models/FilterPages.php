<?php

namespace Modules\Catalog\Models;

use Core\CommonI18n;
use Core\QB\DB;

class FilterPages extends CommonI18n
{
    public static $table = 'filter_pages';
    public static $tableI18n = 'filter_pages_i18n';

    public static function getFilterPageData($categoryId, $hash)
    {
        return DB::select(
            'fpi.h1',
            'fpi.title',
            'fpi.description',
            'fpi.keywords',
            'fpi.seo_text',
            [DB::expr("(SELECT MD5(GROUP_CONCAT(value ORDER BY value SEPARATOR '')) FROM filter_page_attributes WHERE filter_page_id = fp.id)"), 'hash']
        )
            ->from(['filter_pages', 'fp'])
            ->join(['filter_pages_i18n', 'fpi'], 'inner')
            ->on('fp.id', '=', 'fpi.row_id')
            ->where('fpi.language', '=', \I18n::$lang)
            ->where('fp.category_id', '=', $categoryId)
            ->where('fp.status', '=', '1')
            ->group_by('hash')
            ->having('hash', '=', $hash)
            ->execute()
            ->current();
    }
}