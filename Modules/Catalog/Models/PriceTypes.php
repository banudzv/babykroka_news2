<?php
namespace Modules\Catalog\Models;

use Core\Common;
use Core\QB\DB;

/**
 * Class @PriceTypes
 * @package Wezom\Modules\Catalog\Models
 */
class PriceTypes extends Common {

    public static $table = 'price_types';
    public static $rules = [];

}
