<?php
namespace Modules\Catalog\Models;

use Core\CommonI18n;
use Core\QB\DB;

class Manufacturers extends CommonI18n
{

    public static $table = 'manufacturers';

    public static function getRow($value, $field = 'id', $status = null)
    {
        $result = DB::select()->from(static::$table)->where($field, '=', $value);
        if ($status !== null) {
            $result->where('status', '=', $status);
        }
        return $result->find();
    }

}
