<?php
namespace Modules\Catalog\Models;

use Core\Common;
use Core\CommonI18n;

class Colors extends CommonI18n
{
    static $table = 'colors';
}
