<?php
namespace Modules\Catalog\Models;

use Core\CommonI18n;
use Core\QB\DB;

class Groups extends CommonI18n
{

    public static $table = 'catalog_tree';
    public static $tableI18n = 'catalog_tree_i18n';
    public static $innerIds = [];
    public static $parents = [];


    public static function getInnerGroups($parent_id, $sort = null, $type = null, $limit = null, $offset = null, $db = null)
    {
        $result = DB::select(static::$tableI18n. '.*', static::$table . '.*')
            ->from(static::$table)
            ->join(static::$tableI18n)
            ->on(static::$table . '.id', '=', static::$tableI18n.'.row_id')
            ->where(static::$tableI18n. '.language', '=', \I18n::lang())
            ->where(static::$table . '.parent_id', '=', $parent_id)
            ->where(static::$table . '.status', '=', 1);

        if ($sort !== null) {
            if ($type !== null) {
                $result->order_by(static::$table . '.' . $sort, $type);
            } else {
                $result->order_by(static::$table . '.' . $sort);
            }
        }

        if ($limit !== null) {
            $result->limit($limit);
            if ($offset !== null) {
                $result->offset($offset);
            }
        }
        return $result->group_by(static::$table. '.id')->find_all($db);
    }


    public static function countInnerGroups($parent_id, $db = null)
    {
        $result = DB::select([DB::expr('COUNT(' . static::$table . '.id)'), 'count'])
            ->from(static::$table)

            ->where(static::$table . '.parent_id', '=', $parent_id)
            ->where(static::$table . '.status', '=', 1);
        return $result->count_all($db);
    }

    public static function getInnerIds($parent_id, $db = null) {
    	$count = self::countInnerGroups($parent_id, $db);
    	if (!$count) {
    		self::$innerIds[] = $parent_id;
		} else {
    		$inners = self::getInnerGroups($parent_id, null, null, null, null);
    		foreach ($inners as $inner) {
    			self::getInnerIds($inner->id, $db);
			}
		}
		return self::$innerIds;
	}

    public static function getRootCategory($category_id)
    {
        if ($category_id == 0) return false;

        $category = DB::select()
            ->from(static::$table)
            ->where(static::$table . '.id', '=', $category_id)
            ->find();

        if ($category->parent_id == 0) return $category;

        return self::getRootCategory($category->parent_id);
    }

	public static function getTopLevelGroup($group_id){


        if ($group_id == 0){
            $group = DB::select()
                ->from(static::$table)
                ->join(static::$tableI18n)->on(static::$table . '.id', '=', static::$tableI18n.'.row_id')
                ->where(static::$tableI18n. '.language', '=', \I18n::lang())
                ->where(static::$table . '.id', '=', $group_id)
                ->where(static::$table . '.status', '=', 1)
                ->find();
            return $group->id;
        }
        $result =  DB::select()
            ->from(static::$table)
            ->join(static::$tableI18n)->on(static::$table . '.id', '=', static::$tableI18n.'.row_id')
            ->where(static::$tableI18n. '.language', '=', \I18n::lang())
            ->where(static::$table . '.id', '=', $group_id)
            ->where(static::$table . '.status', '=', 1)
            ->find();

        if ($result->parent_id == 0){
            return $result->id;
        }
        $result =  DB::select()
            ->from(static::$table)
            ->join(static::$tableI18n)->on(static::$table . '.id', '=', static::$tableI18n.'.row_id')
            ->where(static::$tableI18n. '.language', '=', \I18n::lang())
            ->where(static::$table . '.id', '=', $result->parent_id)
            ->where(static::$table . '.status', '=', 1)
            ->find();
        if ($result->parent_id == 0){
            return $result->id;
        }

        $result =  DB::select()
            ->from(static::$table)
            ->join(static::$tableI18n)->on(static::$table . '.id', '=', static::$tableI18n.'.row_id')
            ->where(static::$tableI18n. '.language', '=', \I18n::lang())
            ->where(static::$table . '.id', '=', $result->parent_id)
            ->where(static::$table . '.status', '=', 1)
            ->find();
        if ($result->parent_id == 0){
            return $result->id;
        }
    }


    public static function getParentsByChild($child, &$ids){
        $row = DB::select('id', 'parent_id')
            ->from(static::$table)
            ->where('id', '=', $child)
            ->find();

        $ids[] = $row->id;
        if($row->parent_id == 0){

            return $ids;
        }else{
            self::getParentsByChild($row->parent_id, $ids);
        }
    }
}
