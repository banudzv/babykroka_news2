<?php

namespace Modules\Catalog\Models;

use Core\Arr;
use Core\HTML;
use Core\QB\DB;
use Core\Route;
use Core\Config;
use Core\User;

class Filter
{

    public static $_data;
    public static $_counts;

    public static function getFilteredItemsList($limit, $offset, $sort, $type,  $query = null, $sizeAmount = false)
    {
        if (static::$_data) {
            return static::$_data;
        }
        $filter = Config::get('filter_array');
        $sorting = null;
        if($sort === 'top'){
            $sorting = (new Filter)->seasonsSorting();
            if (is_array($filter) && array_key_exists('sort', $filter) && $filter['sort'] === 'top') {
                unset($filter['sort']);
            }
        }

        if (!empty($filter['polrebenka']) && (
                is_array($filter['polrebenka']) && !in_array('uniseks', $filter['polrebenka']) ||
                $filter['polrebenka'] != 'uniseks')
        ) {
            if (!is_array($filter['polrebenka'])) $filter['polrebenka'] = [$filter['polrebenka']];

            $filter['polrebenka'][] = 'uniseks';
        }

        $groupIds = Groups::getInnerIds(Route::param('group'));
        $result = DB::select(
            'catalog_i18n.*', 'catalog.*',
            'product_prices.price',
            'product_prices.price_old',
            'product_prices.currency_id',
            DB::expr('AVG(cc.rate) AS rating'),
            DB::expr('COUNT(DISTINCT cc.id) AS comments_count')
        )
            ->from('catalog')
            ->join('product_prices')
            ->on('catalog.id','=','product_prices.product_id')
            ->join('catalog_i18n')
            ->on('catalog.id','=','catalog_i18n.row_id')
            ->join(['catalog_comments', 'cc'], 'left')
            ->on('cc.catalog_id', '=', 'catalog.id')
            ->on('cc.status', '=', DB::expr('1'))
            ->where('catalog.status', '=', 1)
            ->where('catalog_i18n.language', '=', \I18n::lang())
            ->where('product_prices.price_type', '=', $_SESSION['prices'])
            ->where('catalog.parent_id', 'IN', $groupIds);

        if (is_array($filter) && array_key_exists('maxcost', $filter) && count((array)$filter['maxcost']) && (int)$filter['maxcost'] > 0) {
            $result->where('product_prices.price', '<=', (int)(is_array($filter['maxcost'])) ? $filter['maxcost'][0] : $filter['maxcost']);
            unset($filter['maxcost']);
        }

        if (is_array($filter) && array_key_exists('mincost', $filter) && count((array)$filter['mincost']) && (int)$filter['mincost'] >= 0) {
            $result->where('product_prices.price', '>=', (int)(is_array($filter['mincost']))?$filter['mincost'][0]:$filter['mincost']);
            unset($filter['mincost']);
        }

        if (is_array($filter) && array_key_exists('color', $filter) && count((array)$filter['color'])) {
            if (is_array($filter['color'])) {
                $result->where('catalog.color_alias', 'IN', $filter['color']);
            } else {
                $result->where('catalog.color_alias', '=', $filter['color']);
            }
            unset($filter['color']);
        }
        if (is_array($filter) && isset($filter['discounts'])) {
            $result->where('catalog.sale', '=', 1);
            unset($filter['discounts']);
        }

//        if (is_array($filter) && array_key_exists('age', $filter) && count((array)$filter['age'])) {
//            if (is_array($filter['age'])) {
//               $result->where('catalog.age_alias', 'IN', $filter['age']);
//            } else {
//               $result->where('catalog.age_alias', '=', $filter['age']);
//            }
//            unset($filter['age']);
//        }

        if (is_array($filter) && array_key_exists('age', $filter) && count((array)$filter['age'])) {
            $result->join('product_ages')
                ->on('catalog.id', '=', 'product_ages.product_id');
            if (is_array($filter['age'])) {
                $alias_id = DB::select('code')
                    ->from('ages')
                    ->where('alias', 'IN', $filter['age'])
                    ->find_all()->as_array();

                $age_type = array();
                foreach ($alias_id as $item) {
                    array_push($age_type, $item->code);
                }

                $array=array_map('intval', explode(',', (string)$age_type));
                $array = implode("','",$array);
                $result->where('product_ages.age_type', 'IN', $age_type);
            } else {
                $alias_id =  DB::select('code')
                    ->from('ages')
                    ->where('alias', '=', $filter['age'])
                    ->find_all()
                    ->current();

                $result->where('product_ages.age_type', '=', $alias_id->code);

            }
            unset($filter['age']);
        }

        if (is_array($filter) && array_key_exists('manufacturer', $filter) && count((array)$filter['manufacturer'])) {
            if (is_array($filter['manufacturer'])) {
                $result->where('catalog.manufacturer_alias', 'IN', $filter['manufacturer']);
            } else {
                $result->where('catalog.manufacturer_alias', '=', $filter['manufacturer']);
            }
            unset($filter['manufacturer']);
        }

        if (is_array($filter) && array_key_exists('brand', $filter) && count((array)$filter['brand'])) {
            if (is_array($filter['brand'])) {
                $result->where('catalog.brand_alias', 'IN', $filter['brand']);
            } else {
                $result->where('catalog.brand_alias', '=', $filter['brand']);
            }
            unset($filter['brand']);
        }
        if ($sizeAmount){
            $result->join('catalog_size_amount')->on('catalog_size_amount.catalog_id', '=', 'catalog.id');
            $result->where('catalog_size_amount.amount', '=', '0');
        }else{
            $result->join('catalog_size_amount')->on('catalog_size_amount.catalog_id', '=', 'catalog.id');
            $result->where('catalog_size_amount.amount', '>', '0');
        }

        if (is_array($filter) && array_key_exists('size', $filter) && count((array)$filter['size'])) {
            if (is_array($filter['size'])) {
                $result->where('catalog_size_amount.size', 'IN', $filter['size']);
            } else {
                $result->where('catalog_size_amount.size', '=', $filter['size']);
            }
            $result->where('catalog_size_amount.amount', '<>', '0');
            unset($filter['size']);
        }

        if($sorting && $sorting['configSort']){
            $expr = [];
            foreach ($sorting['seasons'] as $season => $priority){
                $expr[] = 'WHEN catalog.specifications LIKE "%'.$season.'%" THEN '.$priority;
            }
            $expr[] = 'WHEN catalog.specifications IS NULL THEN '.$sorting['lastPriority'];
            $expression = 'CASE '.implode(' ',$expr).' END ASC';
            $result->order_by(DB::expr($expression));
            $result
                ->order_by('catalog.new','DESC')
                ->order_by('catalog.top','DESC')
                ->order_by('catalog.sale','DESC')
                ->order_by('catalog.id','DESC')
                ->order_by('catalog.available', 'DESC');
        }else{
            if ($filter && is_array($filter) && array_key_exists('sort', $filter)){
                if (is_array($filter['sort'])){
                    $filter['sort'] = $filter['sort'][0];
                }
                if ($filter['sort'] == 'priceAsc'){
                    $sort = 'product_prices.price';
                    $type = 'ASC';
                } elseif ($filter['sort'] == 'priceDesc'){
                    $sort = 'product_prices.price';
                    $type = 'DESC';
                } elseif ($filter['sort'] == 'top'){
                    $sort = 'catalog.views';
                    $type = 'ASC';
                } elseif ($filter['sort'] == 'alph'){
                    $sort = 'catalog_i18n.name';
                    $type = 'ASC';
                }
                $result->order_by($sort, $type);
                unset($filter['sort']);
            }
            $result->order_by('catalog.available', 'ASC');
        }

        if (is_array($filter) && count($filter)) {
            $result
                ->select(DB::expr('COUNT(DISTINCT catalog_specifications_values.specification_alias) AS cList'))
                ->join('catalog_specifications_values')->on('catalog_specifications_values.catalog_id', '=', 'catalog.id');
            $result->and_where_open();

            foreach ($filter as $key => $val) {
                if (is_array($val)) {
                    //if(!array_search('uniseks', $val)) array_push($val, 'uniseks');
                    $result
                        ->or_where_open()
                        ->where('catalog_specifications_values.specification_alias', '=', $key)
                        ->where('catalog_specifications_values.specification_value_alias', 'IN', $val)
                        ->or_where_close();
                } else {
                    $result
                        ->or_where_open()
                        ->where('catalog_specifications_values.specification_alias', '=', $key)
                        ->where('catalog_specifications_values.specification_value_alias', '=', $val)
                        ->or_where_close();
                }
            }
            $result->and_where_close();
            $result->having('cList', '>=', count($filter));
        }

        if (isset($query) && !empty($query)){
            $result->and_where_open();
            foreach ($query as $q) {
                $result->where('catalog_i18n.name', 'LIKE', '%' . $q . '%');
            }
            foreach ($query as $q) {
                $result->or_where('catalog.id', 'LIKE', '%' . $q . '%');
            }
            $result->and_where_close();
        }

        $result = $result
            ->group_by('catalog.id')
            ->group_by('product_prices.price')
            ->group_by('product_prices.price_old')
            ->group_by('product_prices.currency_id')
            ->find_all()->as_array();

        static::$_data = $result;

        $items = [];
        $ids = [(int)Route::param('group')];
        for ($i = $offset; $i < $limit + $offset; $i++) {
            if (!isset($result[$i])) {
                break;
            }

            $item = $result[$i];

            $images = DB::select('image')
                ->from('catalog_images')
                ->where('catalog_id', '=', $item->id)
                ->order_by('main', 'desc')
                ->find_all()
                ->as_array();

            $item->images = $images;

            $items[$i] = $item;
            $ids[] = $result[$i]->parent_id;
        }
        $cats = DB::select()
            ->from('catalog_tree')
            ->join('catalog_tree_i18n')->on('catalog_tree.id', '=', 'catalog_tree_i18n.row_id')
            ->where('catalog_tree_i18n.language', '=', \I18n::lang())
            ->where('catalog_tree.id', 'IN', $ids)
            ->find_all();


        $cat = [];
        foreach ($cats as $obj) {
            $cat[$obj->id]['name'] = $obj->name;
            $cat[$obj->id]['alias'] = $obj->alias;
        }

        return [
            'items' => $items,
            'total' => sizeof($result),
            'cats' => $cat,
        ];
    }
    public static function getFilteredItemsListAll($limit, $offset, $sort, $type,  $query = null){
        $resultAmount = self::getFilteredItemsList($limit, $offset, $sort, $type,  $query);

        if (count($resultAmount['items'])<$limit){
            $limitNoAmount = $limit - count($resultAmount['items']);
            $offsetNoAmount = 0;
            if ($offset>$limit){
                $offsetNoAmount = $offset - $resultAmount['total'];
            }
            $resultNoAmount = self::getFilteredItemsList($limitNoAmount, $offsetNoAmount, $sort, $type,  $query, true);
            $items = $resultAmount['items'];
            $cats = $resultAmount['cats'];
            if ($resultNoAmount['items'] != null and count($resultNoAmount['items'])>0){
                $items = array_merge($resultAmount['items'], $resultNoAmount['items']);
                $cats = array_merge($resultAmount['cats'], $resultNoAmount['cats']);
            }
        }else{
            $resultNoAmount = self::getFilteredItemsList($limit, $offset, $sort, $type,  $query, true);
            $items = $resultAmount['items'];
            $cats = $resultAmount['cats'];
        }

        return ['items'=>$items, 'total'=>$resultAmount['total']+$resultNoAmount['total'], 'cats'=>$cats];
    }

    public function seasonsSorting(){
        $date = date('Y-m-d');
        $spec_season = Specifications::getRow('sezon','alias');
        $seson_time = DB::select()->from('season_time')->Where('start','<=',$date)->Where('finish','>=',$date)->find();
        $seasons = null;
        $configSort = false;
        $lastPriority = 0;
        if($spec_season and $seson_time){
            $seasons_select = DB::select('specifications_values_sort.sort', 'specifications_values.alias')->from('specifications_values')
                ->join('specifications_values_sort')
                ->on('specifications_values.id', '=', 'specifications_values_sort.specifications_values_id')
                ->where('specifications_values.specification_id', '=',$spec_season->id)
                ->where('specifications_values_sort.row_id', '=',$seson_time->id)
                ->find_all()
                ->as_array();
            $lastPriority = SpecificationsValues::getMaxPriority($spec_season->id);
            $seasons = [];
            foreach ($seasons_select as $season){
                $seasons [$season->alias] = $season->sort;
                if ($season->sort > $lastPriority){
                    $lastPriority = (int)$season->sort;
                }
            }
            $configSort = true;
            //$seasons = SpecificationsValues::getCustomRows(['specification_id' => $spec_season->id],'sort')->as_array('alias','sort');
        }

        return(['lastPriority' => $lastPriority,'seasons' => $seasons,'configSort' => $configSort]);
    }


    public static function getFullFilteredItemsList($limit, $offset, $sort, $type,  $query = null, $sizeAmount = false)
    {
        /*if (static::$_data) {
            return static::$_data;
        }*/
        $sorting = null;
        if($sort === 'top'){
            $sorting = (new Filter)->seasonsSorting();
            if (is_array($filter) && array_key_exists('sort', $filter) && $filter['sort'] === 'top') {
                unset($filter['sort']);
            }
        }
        $cats = [];
        $top_cat = DB::select('catalog_tree.*', 'catalog_tree_i18n.name')
            ->from('catalog_tree')
            ->join('catalog_tree_i18n')->on('catalog_tree.id', '=', 'catalog_tree_i18n.row_id')
            ->where('catalog_tree_i18n.language', '=', \I18n::lang())
            ->where('catalog_tree.parent_id', '=', (int)Route::param('group'))
            ->find_all()->as_array();
        $top_cat_ids = [(int)Route::param('group')];
        for ($i = 0, $iMax = count($top_cat); $i < $iMax; $i++) {
            $top_cat_ids[] = (int)$top_cat[$i]->id;
            $cats[(int)$top_cat[$i]->id] = $top_cat_ids[$i]->name;
        }

        if (count($top_cat_ids) > 1) {
            $cat = DB::select('catalog_tree.*', 'catalog_tree_i18n.name')
                ->from('catalog_tree')
                ->join('catalog_tree_i18n')->on('catalog_tree.id', '=', 'catalog_tree_i18n.row_id')
                ->where('catalog_tree_i18n.language', '=', \I18n::lang())
                ->where('parent_id', 'IN', $top_cat_ids)
                ->find_all()->as_array();

            $cat_ids = [];
            for ($i = 0, $iMax = count($cat); $i < $iMax; $i++) {
                $cat_ids[] = (int)$cat[$i]->id;
                $cats[(int)$cat[$i]->id] = ['name' => $cat[$i]->name, 'alias' => $cat[$i]->alias];
            }
            $filter = Config::get('filter_array');

            $result = DB::select('catalog_i18n.*','catalog.*',
                'product_prices.price',
                'product_prices.price_old',
                'product_prices.currency_id',
                DB::expr('AVG(cc.rate) AS rating'),
                DB::expr('COUNT(DISTINCT cc.id) AS comments_count'))
                ->from('catalog')
                ->join('catalog_i18n')
                ->on('catalog.id','=','catalog_i18n.row_id')
                ->join('product_prices')
                ->on('catalog.id','=','product_prices.product_id')
                ->join(['catalog_comments', 'cc'], 'left')
                ->on('cc.catalog_id', '=', 'catalog.id')
                ->on('cc.status', '=', DB::expr('1'))
                ->where('product_prices.price_type', '=', $_SESSION['prices'])
                ->where('catalog.status', '=', 1)
                ->where('catalog_i18n.language', '=', \I18n::lang())
                ->where('catalog.parent_id', 'IN', $cat_ids);
        } else {
            $result = DB::select('catalog_i18n.*', 'catalog.*',
                'product_prices.price',
                'product_prices.price_old',
                'product_prices.currency_id',
                DB::expr('AVG(cc.rate) AS rating'),
                DB::expr('COUNT(DISTINCT cc.id) AS comments_count'))
                ->from('catalog')
                ->join('catalog_i18n', 'INNER')
                ->on('catalog_i18n.row_id', '=', 'catalog.id')
                ->join('product_prices')
                ->on('catalog.id','=','product_prices.product_id')
                ->join(['catalog_comments', 'cc'], 'left')
                ->on('cc.catalog_id', '=', 'catalog.id')
                ->on('cc.status', '=', DB::expr('1'))
                ->where('product_prices.price_type', '=', $_SESSION['prices'])
                ->where('catalog.status', '=', 1)
                ->where('catalog_i18n.language', '=', \I18n::lang())
                ->where('catalog.parent_id', 'IN', $top_cat_ids);
        }
        if ($sizeAmount){
            $result->join('catalog_size_amount')->on('catalog_size_amount.catalog_id', '=', 'catalog.id');
            $result->where('catalog_size_amount.amount', '=', '0');
        }else{
            $result->join('catalog_size_amount')->on('catalog_size_amount.catalog_id', '=', 'catalog.id');
            $result->where('catalog_size_amount.amount', '>', '0');
        }


        if($sorting && $sorting['configSort']){
            $expr = [];
            foreach ($sorting['seasons'] as $season => $priority){
                $expr[] = 'WHEN catalog.specifications LIKE "%'.$season.'%" THEN '.$priority;
            }
            // $sorting['lastPriority'] -  с ним работает не корректно
            $expr[] = 'WHEN catalog.specifications IS NULL THEN '. 10000000;
            $expression = 'CASE '.implode(' ',$expr).' END ASC';
            $result->order_by(
                DB::expr($expression));
            $result
                ->order_by('catalog.new','DESC')
                ->order_by('catalog.top','DESC')
                ->order_by('catalog.sale','DESC')
                ->order_by('catalog.id','DESC')
                ->order_by('catalog.available', 'DESC');

        }else{
            if (is_array($filter) && array_key_exists('sort', $filter) && count($filter['sort'])){
                if ($filter['sort'] == 'priceAsc'){
                    $sort = 'product_prices.price';
                    $type = 'ASC';
                } elseif ($filter['sort'] == 'priceDesc'){
                    $sort = 'product_prices.price';
                    $type = 'DESC';
                } elseif ($filter['sort'] == 'top'){
                    $sort = 'catalog.views';
                    $type = 'ASC';
                } elseif ($filter['sort'] == 'alph'){
                    $sort = 'catalog_i18n.name';
                    $type = 'ASC';
                }
                $result->order_by($sort, $type);
                unset($filter['sort']);
            }
            $result->order_by('catalog.available', 'ASC');
        }

        if (is_array($filter) && count($filter)) {
            $result
                ->select(DB::expr('COUNT(DISTINCT catalog_specifications_values.specification_alias) AS cList'))
                ->join('catalog_specifications_values')->on('catalog_specifications_values.catalog_id', '=', 'catalog.id');
            $result->and_where_open();

            foreach ($filter as $key => $val) {
                $result
                    ->or_where_open()
                    ->where('catalog_specifications_values.specification_alias', '=', $key);
                if (is_array($val)) {
                    $result->where('catalog_specifications_values.specification_value_alias', 'IN', $val);
                } else {
                    $result->where('catalog_specifications_values.specification_value_alias', '=', $val);
                }

                $result->or_where_close();
            }

            $result->and_where_close();
            $result->having('cList', '>=', count($filter));
        }
        if (isset($query) && !empty($query)){
            $result->and_where_open();
            foreach ($query as $q) {
                $result->where('catalog_i18n.name', 'LIKE', '%' . $q . '%');
            }
            foreach ($query as $q) {
                $result->or_where('catalog.id', 'LIKE', '%' . $q . '%');
            }
            $result->and_where_close();
        }

        $result = $result
            ->group_by('catalog.id')
            ->find_all()->as_array();

        static::$_data = $result;
        $items = [];

        for ($i = $offset; $i < $limit + $offset; $i++) {
            if (!isset($result[$i])) {
                break;
            }
            $item = $result[$i];

            $images = DB::select('image')
                ->from('catalog_images')
                ->where('catalog_id', '=', $item->id)
                ->order_by('main', 'desc')
                ->find_all()
                ->as_array();

            $item->images = $images;

            $items[] = $item;
        }

        return [
            'items' => $items,
            'total' => count($result),
            'cats' => $cats,
        ];
    }

    public static function getFullFilteredItemsListAll($limit, $offset, $sort, $type,  $query = null){
        $resultAmount = self::getFullFilteredItemsList($limit, $offset, $sort, $type,  $query);

        if (count($resultAmount['items'])<$limit){
            $limitNoAmount = $limit - count($resultAmount['items']);
            $offsetNoAmount = 0;
            if ($offset>$limit){
                $offsetNoAmount = $offset - $resultAmount['total'];
            }
            $resultNoAmount = self::getFullFilteredItemsList($limitNoAmount, $offsetNoAmount, $sort, $type,  $query, true);
            $items = $resultAmount['items'];
            $cats = $resultAmount['cats'];
            if ($resultNoAmount['items'] != null and count($resultNoAmount['items'])>0){
                $items = array_merge($resultAmount['items'], $resultNoAmount['items']);
                $cats = array_merge($resultAmount['cats'], $resultNoAmount['cats']);
            }
        }else{
            $resultNoAmount = self::getFullFilteredItemsList($limit, $offset, $sort, $type,  $query, true);
            $items = $resultAmount['items'];
            $cats = $resultAmount['cats'];
        }

        return ['items'=>$items, 'total'=>$resultAmount['total']+$resultNoAmount['total'], 'cats'=>$cats];

    }

    public static function getFilteredItemsListByFlag($flag, $limit, $offset, $sort, $type, $query = null, $sizeAmount = false)
    {
        /*if (static::$_data) {
            return static::$_data;
        }*/
        $filter = Config::get('filter_array');
        $sorting = null;
        if($sort === 'top'){
            $sorting = (new Filter)->seasonsSorting();
            if (is_array($filter) && array_key_exists('sort', $filter) && $filter['sort'] === 'top') {
                unset($filter['sort']);
            }
        }
        $groupIds = Groups::getInnerIds(Route::param('group'));
        $result = DB::select(
            'catalog_i18n.*', 'catalog.*', 'product_prices.price',
            'product_prices.price_old',
            'product_prices.currency_id'
        )
            ->from('catalog')
            ->join('catalog_i18n')
            ->on('catalog_i18n.row_id','=','catalog.id')
            ->join('product_prices')
            ->on('catalog.id','=','product_prices.product_id')
            ->where('product_prices.price_type', '=', $_SESSION['prices'])
            ->where('catalog.status', '=', 1)
            ->where('catalog_i18n.language', '=', \I18n::lang())
            ->where('catalog.parent_id', 'IN', $groupIds)
            ->where($flag, '=', 1);

        if (is_array($filter) && array_key_exists('color', $filter)) {
            if (is_array($filter['color'])) {
                $result->where('catalog.color_alias', 'IN', $filter['color']);
            } else {
                $result->where('catalog.color_alias', '=', $filter['color']);
            }
            unset($filter['color']);
        }

        if (is_array($filter) && array_key_exists('age', $filter) && count((array)$filter['age'])) {
            $result->join('product_ages')
                ->on('catalog.id', '=', 'product_ages.product_id');
            if (is_array($filter['age'])) {
                $alias_id = DB::select('code')
                    ->from('ages')
                    ->where('alias', 'IN', $filter['age'])
                    ->find_all()->as_array();

                $age_type = array();
                foreach ($alias_id as $item) {
                    array_push($age_type, $item->code);
                }

                $array=array_map('intval', explode(',', (string)$age_type));
                $array = implode("','",$array);
                $result->where('product_ages.age_type', 'IN', $age_type);
            } else {
                $alias_id =  DB::select('code')
                    ->from('ages')
                    ->where('alias', '=', $filter['age'])
                    ->find_all()
                    ->current();

                $result->where('product_ages.age_type', '=', $alias_id->code);

            }
            unset($filter['age']);
        }

        if (is_array($filter) && array_key_exists('brand', $filter) ) {
            if (is_array($filter['brand'])) {
                $result->where('catalog.brand_alias', 'IN', $filter['brand']);
            } else {
                $result->where('catalog.brand_alias', '=', $filter['brand']);
            }
            unset($filter['brand']);
        }

        if ($sizeAmount){
            $result->join('catalog_size_amount')->on('catalog_size_amount.catalog_id', '=', 'catalog.id');
            $result->where('catalog_size_amount.amount', '=', '0');
        }else{
            $result->join('catalog_size_amount')->on('catalog_size_amount.catalog_id', '=', 'catalog.id');
            $result->where('catalog_size_amount.amount', '>', '0');
        }

        if (is_array($filter) && array_key_exists('size', $filter)) {
            if (is_array($filter['size'])) {
                $result->where('catalog_size_amount.size', 'IN', $filter['size']);
            } else {
                $result->where('catalog_size_amount.size', '=', $filter['size']);
            }
            unset($filter['size']);
        }


        if($sorting && $sorting['configSort']){
            $expr = [];
            foreach ($sorting['seasons'] as $season => $priority){
                $expr[] = 'WHEN catalog.specifications LIKE "%'.$season.'%" THEN '.$priority;
            }
            $expr[] = 'WHEN catalog.specifications IS NULL THEN '.$sorting['lastPriority'];
            $expression = 'CASE '.implode(' ',$expr).' END ASC';
            $result->order_by(
                DB::expr($expression));
            $result
                ->order_by('catalog.new','DESC')
                ->order_by('catalog.top','DESC')
                ->order_by('catalog.sale','DESC')
                ->order_by('catalog.id','DESC')
                ->order_by('catalog.available', 'DESC');

        }else{
            if (is_array($filter) && array_key_exists('sort', $filter)){
                if ($filter['sort'][0] == 'priceAsc'){
                    $sort = 'product_prices.price';
                    $type = 'ASC';
                } elseif ($filter['sort'][0] == 'priceDesc'){
                    $sort = 'product_prices.price';
                    $type = 'DESC';
                } elseif ($filter['sort'][0] == 'top'){
                    $sort = 'catalog.views';
                    $type = 'ASC';
                } elseif ($filter['sort'][0] == 'alph'){
                    $sort = 'catalog_i18n.name';
                    $type = 'ASC';
                }
                $result->order_by($sort, $type);
                unset($filter['sort']);
            }
            $result->order_by('catalog.available', 'ASC');
        }

        if (is_array($filter) && count($filter)) {
            $result
                ->select(DB::expr('COUNT(DISTINCT catalog_specifications_values.specification_alias) AS cList'))
                ->join('catalog_specifications_values')->on('catalog_specifications_values.catalog_id', '=', 'catalog.id');
            $result->and_where_open();

            foreach ($filter as $key => $val) {
                if (is_array($val)) {
                    $result
                        ->or_where_open()
                        ->where('catalog_specifications_values.specification_alias', '=', $key)
                        ->where('catalog_specifications_values.specification_value_alias', 'IN', $val)
                        ->or_where_close();
                } else {
                    $result
                        ->or_where_open()
                        ->where('catalog_specifications_values.specification_alias', '=', $key)
                        ->where('catalog_specifications_values.specification_value_alias', '=', $val)
                        ->or_where_close();
                }
            }

            $result->and_where_close();
            $result->having('cList', '>=', count($filter));
        }

        if (isset($query) && !empty($query)){
            $result->and_where_open();
            foreach ($query as $q) {
                $result->where('catalog_i18n.name', 'LIKE', '%' . $q . '%');
            }
            foreach ($query as $q) {
                $result->or_where('catalog.id', 'LIKE', '%' . $q . '%');
            }
            $result->and_where_close();
        }



        $result = $result->group_by('catalog.id')
            ->find_all();

        static::$_data = $result;

        $items = [];
        $cat = [];
        for ($i = $offset; $i < $limit + $offset; $i++) {
            if (!isset($result[$i])) {
                break;
            }
            $item = $result[$i];

            $images = DB::select('image')
                ->from('catalog_images')
                ->where('catalog_id', '=', $item->id)
                ->order_by('main', 'desc')
                ->find_all()
                ->as_array();

            $item->images = $images;
            $items[$i] = $item;
//            $items[$i]->parent_id = (int)Route::param('group');
            $cats = DB::select()
                ->from('catalog_tree')
                ->join('catalog_tree_i18n')->on('catalog_tree.id', '=', 'catalog_tree_i18n.row_id')
                ->where('catalog_tree_i18n.language', '=', \I18n::lang())
                ->where('catalog_tree.id', '=', $items[$i]->parent_id)
                ->find();
            $cat[$items[$i]->parent_id]['name'] = $cats->name;
            $cat[$items[$i]->parent_id]['alias'] = $cats->alias;
        }


        return [
            'items' => $items,
            'total' => sizeof($result),
            'cats' => $cat,
        ];
    }

    public static function getFilteredItemsListByFlagAll($flag, $limit, $offset, $sort, $type, $query = null){
        $resultAmount = self::getFilteredItemsListByFlag($flag, $limit, $offset, $sort, $type, $query);

        if (count($resultAmount['items'])<$limit){
            $limitNoAmount = $limit - count($resultAmount['items']);
            $offsetNoAmount = 0;
            if ($offset>$limit){
                $offsetNoAmount = $offset - $resultAmount['total'];
            }
            $resultNoAmount = self::getFilteredItemsListByFlag($flag, $limitNoAmount, $offsetNoAmount, $sort, $type,  $query, true);
            $items = $resultAmount['items'];
            $cats = $resultAmount['cats'];
            if ($resultNoAmount['items'] != null and count($resultNoAmount['items'])>0){
                $items = array_merge($resultAmount['items'], $resultNoAmount['items']);
                $cats = array_merge($resultAmount['cats'], $resultNoAmount['cats']);
            }
        }else{
            $resultNoAmount = self::getFilteredItemsListByFlag($flag, $limit, $offset, $sort, $type, $query, true);
            $items = $resultAmount['items'];
            $cats = $resultAmount['cats'];
        }

        return ['items'=>$items, 'total'=>$resultAmount['total']+$resultNoAmount['total'], 'cats'=>$cats];

    }

    public static function getFullItemsListByFlag($flag, $limit, $offset, $sort, $type, $query = null, $sizeAmount = false)
    {
        /*if (static::$_data) {
            return static::$_data;
        }*/
        $sorting = null;
        if($sort === 'top'){
            $sorting = (new Filter)->seasonsSorting();
            if (is_array($filter) && array_key_exists('sort', $filter) && $filter['sort'] === 'top') {
                unset($filter['sort']);
            }
        }
        $cats = [];
        $groupIds = Groups::getInnerIds(Route::param('group'));

        $cat = DB::select()
            ->from('catalog_tree')
            ->join('catalog_tree_i18n')->on('catalog_tree.id', '=', 'catalog_tree_i18n.row_id')
            ->where('catalog_tree_i18n.language', '=', \I18n::lang())
            ->where('catalog_tree.id', 'IN', $groupIds)
            ->find_all()->as_array();


        $cat_ids = [];
        for ($i = 0, $iMax = count($cat); $i < $iMax; $i++) {
            $cat_ids[] = (int)$cat[$i]->id;
            $cats[(int)$cat[$i]->id] = ['name' => $cat[$i]->name, 'alias' => $cat[$i]->alias];
        }

        $filter = Config::get('filter_array');

        $result = DB::select('catalog_i18n.*', 'catalog.*','product_prices.price',
            'product_prices.price_old',
            'product_prices.currency_id')
            ->from('catalog')
            ->join('catalog_i18n')
            ->on('catalog_i18n.row_id','=','catalog.id')
            ->join('product_prices')
            ->on('catalog.id','=','product_prices.product_id')
            ->where('product_prices.price_type', '=', $_SESSION['prices'])
            ->where('catalog.status', '=', 1)
            ->where('catalog_i18n.language', '=', \I18n::lang())
            ->where('catalog.parent_id', 'IN', $groupIds)
            ->where($flag, '=', 1);


        if (is_array($filter) && array_key_exists('maxcost', $filter) && count($filter['maxcost']) && (int)$filter['maxcost'][0] > 0) {
            $result->where('catalog.cost', '<=', (int)$filter['maxcost'][0]);
            unset($filter['maxcost']);
        }
        if (is_array($filter) && array_key_exists('mincost', $filter) && count($filter['mincost']) && (int)$filter['mincost'][0] >= 0) {
            $result->where('catalog.cost', '>=', (int)$filter['mincost'][0]);
            unset($filter['mincost']);
        }

        if (is_array($filter) && array_key_exists('sort', $filter) && count($filter['sort'])){
            if ($filter['sort'] == 'priceAsc'){
                $sort = 'product_prices.price';
                $type = 'ASC';
            } elseif ($filter['sort'] == 'priceDesc'){
                $sort = 'product_prices.price';
                $type = 'DESC';
            } elseif ($filter['sort'] == 'top'){
                $sort = 'catalog.views';
                $type = 'ASC';
            } elseif ($filter['sort'] == 'alph'){
                $sort = 'catalog_i18n.name';
                $type = 'ASC';
            }
            $result->order_by( $sort, $type);
            unset($filter['sort']);
        }

        if (is_array($filter) && count($filter)) {
            $result
                ->select(DB::expr('COUNT(DISTINCT catalog_specifications_values.specification_alias) AS cList'))
                ->join('catalog_specifications_values')->on('catalog_specifications_values.catalog_id', '=', 'catalog.id');
            $result->and_where_open();

            foreach ($filter as $key => $val) {
                $result
                    ->or_where_open()
                    ->where('catalog_specifications_values.specification_alias', '=', $key);
                if (is_array($val)) {
                    $result->where('catalog_specifications_values.specification_value_alias', 'IN', $val);
                } else {
                    $result->where('catalog_specifications_values.specification_value_alias', '=', $val);
                }

                $result->or_where_close();
            }

            $result->and_where_close();
            $result->having('cList', '>=', count($filter));
        }
        if (isset($query) && !empty($query)){
            $result->and_where_open();
            foreach ($query as $q) {
                $result->where('catalog_i18n.name', 'LIKE', '%' . $q . '%');
            }
            foreach ($query as $q) {
                $result->or_where('catalog.id', 'LIKE', '%' . $q . '%');
            }
            $result->and_where_close();
        }
        if ($sizeAmount){
            $result->join('catalog_size_amount')->on('catalog_size_amount.catalog_id', '=', 'catalog.id');
            $result->where('catalog_size_amount.amount', '=', '0');
        }else{
            $result->join('catalog_size_amount')->on('catalog_size_amount.catalog_id', '=', 'catalog.id');
            $result->where('catalog_size_amount.amount', '>', '0');
        }

        if($sorting && $sorting['configSort']){
            $expr = [];
            foreach ($sorting['seasons'] as $season => $priority){
                $expr[] = 'WHEN catalog.specifications LIKE "%'.$season.'%" THEN '.$priority;
            }
            $expr[] = 'WHEN catalog.specifications IS NULL THEN '.$sorting['lastPriority'];
            $expression = 'CASE '.implode(' ',$expr).' END ASC';
            $result->order_by(
                DB::expr($expression));
            $result
                ->order_by('catalog.new','DESC')
                ->order_by('catalog.top','DESC')
                ->order_by('catalog.sale','DESC')
                ->order_by('catalog.id','DESC')
                ->order_by('catalog.available', 'DESC');

        }else{
            if (is_array($filter) && array_key_exists('sort', $filter) && count($filter['sort'])){
                if ($filter['sort'] == 'priceAsc'){
                    $sort = 'product_prices.price';
                    $type = 'ASC';
                } elseif ($filter['sort'] == 'priceDesc'){
                    $sort = 'product_prices.price';
                    $type = 'DESC';
                } elseif ($filter['sort'] == 'top'){
                    $sort = 'catalog.views';
                    $type = 'ASC';
                } elseif ($filter['sort'] == 'alph'){
                    $sort = 'catalog_i18n.name';
                    $type = 'ASC';
                }
                $result->order_by($sort, $type);
                unset($filter['sort']);
            }
            $result->order_by('catalog.available', 'ASC');
        }


        $result = $result
            ->group_by('catalog.id')
            ->group_by('product_prices.price')
            ->group_by('product_prices.price_old')
            ->group_by('product_prices.currency_id')
            ->find_all();

        static::$_data = $result;

        $items = [];
        for ($i = $offset; $i < $limit + $offset; $i++) {
            if (!isset($result[$i])) {
                break;
            }
            $item = $result[$i];

            $images = DB::select('image')
                ->from('catalog_images')
                ->where('catalog_id', '=', $item->id)
                ->order_by('main', 'desc')
                ->find_all()
                ->as_array();

            $item->images = $images;
            $items[$i] = $item;
        }

        return [
            'items' => $items,
            'total' => count($result),
            'cats' => $cats,
        ];
    }
    public static function getFullItemsListByFlagAll($flag, $limit, $offset, $sort, $type, $query = null){
        $resultAmount = self::getFullItemsListByFlag($flag, $limit, $offset, $sort, $type, $query);

        if (count($resultAmount['items'])<$limit){
            $limitNoAmount = $limit - count($resultAmount['items']);
            $offsetNoAmount = 0;
            if ($offset>$limit){
                $offsetNoAmount = $offset - $resultAmount['total'];
            }
            $resultNoAmount = self::getFullItemsListByFlag($flag, $limitNoAmount, $offsetNoAmount, $sort, $type,  $query, true);
            $items = $resultAmount['items'];
            $cats = $resultAmount['cats'];
            if ($resultNoAmount['items'] != null and count($resultNoAmount['items'])>0){
                $items = array_merge($resultAmount['items'], $resultNoAmount['items']);
                $cats = array_merge($resultAmount['cats'], $resultNoAmount['cats']);
            }
        }else{
            $resultNoAmount = self::getFullItemsListByFlag($flag, $limit, $offset, $sort, $type, $query, true);
            $items = $resultAmount['items'];
            $cats = $resultAmount['cats'];
        }

        return ['items'=>$items, 'total'=>$resultAmount['total']+$resultNoAmount['total'], 'cats'=>$cats];

    }

    // Get clickable filters and min/max costs, included current
    public static function getClickableFilterElements()
    {
        $flag = null;
        if (Route::param('group') == 0) {
            if (Route::controller() == 'novelty') {
                $flag = 'new';
            } else if (Route::controller() == 'popular') {
                $flag = 'top';
            } else if(Route::controller() == 'sale') {
                $flag = Route::controller();
            }
        }
        Groups::$innerIds = [];
        $innerIds = Groups::getInnerIds(Route::param('group'));
        $filter = Config::get('filter_array');
        $result = DB::select(
            'product_prices.price',
            'product_prices.price_old',
            'product_prices.currency_id',
            ['a.alias', 'age_alias'],
            'catalog.brand_alias',
            'catalog.manufacturer_alias',
            'catalog.color_alias',
            'catalog.available',
            'catalog_size_amount.size',
            'catalog_size_amount.amount',
            'catalog.id',
            'catalog_i18n.name'
        )
            ->from('catalog')
            ->join('catalog_size_amount', 'LEFT')
            ->on('catalog_size_amount.catalog_id', '=', 'catalog.id')
            ->join('catalog_i18n', 'LEFT')
            ->on('catalog.id', '=', 'catalog_i18n.row_id')
            ->join('product_prices')
            ->on('catalog.id','=','product_prices.product_id')
            ->join(['product_ages', 'pa'])
            ->on('pa.product_id', '=', 'catalog.id')
            ->join(['ages', 'a'])
            ->on('a.code', '=', 'pa.age_type')
            ->where('product_prices.price_type', '=', $_SESSION['prices'])
//            ->where('catalog_size_amount.amount', '<>', '0')
            ->where('catalog.status', '=', 1);
        if ($flag) {
            $result->where('catalog.' . $flag, '=', 1);
        } else {
            $result->where('catalog.parent_id', 'IN', $innerIds);
        }
        $queries = Config::get('queries');
        if ($queries){
            $result->and_where_open();
            foreach ($queries as $q) {
                $result->where('catalog_i18n.name', 'LIKE', '%' . $q . '%');
            }
            foreach ($queries as $q) {
                $result->or_where('catalog.id', 'LIKE', '%' . $q . '%');
            }
            $result->and_where_close();
        }

        $result = $result->find_all();

        $params = [];
        $costs = [];
        $filtered_costs = [];
        $sortable = Config::get('sortable');
        $sortable = $sortable['spec'];
        $items = [];

        $ids = [];
        foreach ($result as $key => $value) {
            $ids[] = $value->id;
            $cost = $value->price;
            $costs[] = $cost;
            $items[$value->id]['cost'] = $cost;
            if ($value->brand_alias) {
                $items[$value->id]['brand'] = $value->brand_alias;
            }
            if ($value->manufacturer_alias) {
                $items[$value->id]['manufacturer'] = $value->manufacturer_alias;
            }
            if ($value->color_alias) {
                $items[$value->id]['color'] = $value->color_alias;
            }
            if ($value->age_alias) {
                $items[$value->id]['age'] = $value->age_alias;
            }
            if ($value->size) {
                $items[$value->id]['size'][] = $value->size;
                $items[$value->id]['amount'][] = $value->amount;
            }
            $items[$value->id]['available'] = $value->available;
        }

        $result = [];
        if (count($ids)) {
            $result = DB::select(['csv.catalog_id', 'id'], 'csv.specification_alias', 'csv.specification_value_alias')
                ->from(['catalog_specifications_values', 'csv'])
                ->where('csv.catalog_id', 'IN', $ids)
                ->find_all();
        }


        foreach ($result as $key => $value) {
            $items[$value->id][$value->specification_alias][] = $value->specification_value_alias;
        }


        $counts = [];
        $new_costs = [];
        foreach ($items as $item) {
            $for_cost = 0;
            foreach ($sortable as $sort) {

                $filtered = 0;
                $f = $filter;
                if (isset($f[$sort])) {
                    unset($f[$sort]);
                }
                if ($f) {
                    foreach ($f as $key => $value) {

                        if($key == 'sort'){
                            $filtered = 0;
                        }elseif (!isset($item[$key]) and !in_array($key, ['mincost', 'maxcost'])) {
                            $filtered = 1;
                            $for_cost = 1;
                        } else {
                            switch ($key) {
                                case 'mincost':
                                    if ($item['cost'] < $value[0]) {
                                        $filtered = 1;
                                    }
                                    break;
                                case 'maxcost':
                                    if ($item['cost'] > $value[0]) {
                                        $filtered = 1;
                                    }
                                    break;
                                default:
                                    $flag = 0;
                                    if (is_array($item[$key])) {
                                        foreach ($item[$key] AS $element) {
                                            if (in_array($element, $value)) {
                                                $flag = 1;
                                            }
                                        }
                                    } else {
                                        if (in_array($item[$key], $value)) {
                                            $flag = 1;
                                        }
                                    }
                                    if (!$flag) {
                                        $filtered = 1;
                                        $for_cost = 1;
                                    }
                                    break;
                            }
                        }
                        if ($filtered) {
                            break;
                        }
                    }
                }
                if (!$filtered and isset($item[$sort])) {
                    if (is_array($item[$sort])) {
                        foreach ($item[$sort] as $value) {
                            if (!in_array($value, Arr::get($params, $sort, []))) {
                                $params[$sort][] = $value;
                            }
                            if (!array_key_exists($sort, $counts)) {
                                $counts[$sort] = [];
                            }
                            if (!array_key_exists($value, $counts[$sort])) {
                                $counts[$sort][$value] = 0;
                            }
                            $counts[$sort][$value]++;
                        }
                    } else {
                        if (!in_array($item[$sort], Arr::get($params, $sort, []))) {
                            $params[$sort][] = $item[$sort];
                        }
                        if (!array_key_exists($sort, $counts)) {
                            $counts[$sort] = [];
                        }
                        if (!array_key_exists($item[$sort], $counts[$sort])) {
                            $counts[$sort][$item[$sort]] = 0;
                        }
                        $counts[$sort][$item[$sort]]++;
                    }
                }
            }
            if ($for_cost == 0) {
                if($item['cost']) {
                    $new_costs[] = $item['cost'];
                }
            }
        }
        $min = $new_costs ? min($new_costs) : 0;
        $max = $new_costs ? max($new_costs) : 0;

        static::$_counts = $counts;
        return [
            'filter' => $params,
            'counts' => $counts,
            'min' => $min,
            'max' => $max,
        ];
    }


    // Get brands list for filter
    public static function getBrandsWidget()
    {
        $flag = null;
        if (Route::param('group') == 0) {
            if (Route::controller() == 'novelty') {
                $flag = 'new';
            } else if (Route::controller() == 'popular') {
                $flag = 'top';
            } else if(Route::controller() == 'sale') {
                $flag = Route::controller();
            }
        }
        $result = DB::select('brands_i18n.name', 'brands.alias', 'brands.id')
            ->from('brands')
            ->join('brands_i18n')->on('brands.id', '=', 'brands_i18n.row_id')
            ->join('catalog')->on('catalog.brand_alias', '=', 'brands.alias')
            ->where('brands_i18n.language', '=', \I18n::lang())
            ->where('catalog.status', '=', 1)
            ->where('brands.status', '=', 1);
        if (isset($flag) && !empty($flag)) {
            $result->where('catalog.' . $flag, '=', 1);
        } else {
            $groupIds = Groups::getInnerIds(Route::param('group'));
            $result->where('catalog.parent_id', 'IN', $groupIds);
        }


        $res = $result->group_by('brands.id')
            ->order_by('brands_i18n.name')
            ->find_all();
        return $res;
    }

    public static function getManufacturerWidget()
    {
        $flag = null;
        if (Route::param('group') == 0) {
            if (Route::controller() == 'novelty') {
                $flag = 'new';
            } else if (Route::controller() == 'popular') {
                $flag = 'top';
            } else if(Route::controller() == 'sale') {
                $flag = Route::controller();
            }
        }
        $result = DB::select('manufacturers_i18n.name', 'manufacturers.alias', 'manufacturers.id')
            ->from('manufacturers')
            ->join('manufacturers_i18n')->on('manufacturers.id', '=', 'manufacturers_i18n.row_id')
            ->where('manufacturers_i18n.language', '=', \I18n::lang())

            ->join('catalog')->on('catalog.manufacturer_alias', '=', 'manufacturers.alias')
            ->where('catalog.status', '=', 1)
            ->where('manufacturers.status', '=', 1);
        if (isset($flag) && !empty($flag)) {
            $result->where('catalog.' . $flag, '=', 1);
        } else {
            $groupIds = Groups::getInnerIds(Route::param('group'));
            $result->where('catalog.parent_id', 'IN', $groupIds);
        }


        $res = $result->group_by('manufacturers.id')
            ->order_by('manufacturers_i18n.name')
            ->find_all();
        return $res;
    }

    // Get colors list for filter
    public static function getColorsWidget()
    {
        $flag = null;
        if (Route::param('group') == 0) {
            if (Route::controller() == 'novelty') {
                $flag = 'new';
            } else if (Route::controller() == 'popular') {
                $flag = 'top';
            } else if(Route::controller() == 'sale') {
                $flag = Route::controller();
            }
        }
        $groupIds = Groups::getInnerIds(Route::param('group'));
        $result = DB::select('colors_i18n.name', 'colors.alias', 'colors.id', 'colors.color')
            ->from('colors')
            ->join('colors_i18n')->on('colors.id', '=', 'colors_i18n.row_id')
            ->join('catalog')->on('catalog.color_alias', '=', 'colors.alias')
            ->where('catalog.status', '=', 1)
            ->where('colors_i18n.language', '=', \I18n::lang());
        if (isset($flag) && !empty($flag)) {
            $result->where('catalog.' . $flag, '=', 1);
        } else {
            $result->where('catalog.parent_id', 'IN', $groupIds);
        }


        $res = $result->group_by('colors.id')
            ->order_by('colors_i18n.name')
            ->find_all();

        return $res;
    }


    // Get ages list for filter
    public static function getAgesWidget($bySex = false)
    {
        $flag = null;
        if (Route::param('group') == 0) {
            if (Route::controller() == 'novelty') {
                $flag = 'new';
            } else if (Route::controller() == 'popular') {
                $flag = 'top';
            } else if(Route::controller() == 'sale') {
                $flag = Route::controller();
            }
        }

        $groupIds = Groups::getInnerIds(Route::param('group'));
        $result = DB::select('ai18n.name', 'a.alias', 'a.id')
            ->from(['ages', 'a'])
            ->join(['ages_i18n', 'ai18n'], 'inner')
            ->on('a.id', '=', 'ai18n.row_id')
            ->join(['product_ages', 'pa'], 'inner')
            ->on('pa.age_type', '=', 'a.code')
            ->join(['catalog', 'c'], 'inner')
            ->on('c.id', '=', 'pa.product_id')
            ->where('ai18n.language', '=', \I18n::lang())
            ->where('c.status', '=', 1)
            ->where('c.parent_id', 'IN', $groupIds);

        $filters = Filter::getFilterArr(Route::param('filter'));
        if (!empty($filters)) {
            if (isset($filters['color'])) $result->where('c.color_alias', 'IN', $filters['color']);
            if (isset($filters['brand'])) $result->where('c.brand_alias', 'IN', $filters['brand']);
            if (isset($filters['manufacturer'])) $result->where('c.manufacturer_alias', 'IN', $filters['manufacturer']);
            if (isset($filters['material'])) {
                $result->where_open();
                foreach($filters['material'] as $material) {
                    $result->or_where('c.specifications', 'LIKE', '%' . $material . '%');
                }
                $result->where_close();
            }
            if (isset($filters['sezon'])) {
                $result->where_open();
                foreach($filters['sezon'] as $sezon) {
                    $result->or_where('c.specifications', 'LIKE', '%' . $sezon . '%');
                }
                $result->where_close();
            }
            if (isset($filters['size']))
                $result->join(['catalog_size_amount', 'csa'], 'inner')
                    ->on('csa.catalog_id', '=', 'c.id')
                    ->where('csa.size', 'IN', $filters['size']);
            if (isset($filters['mincost']) && isset($filters['maxcost']))
                $result->join(['product_prices', 'pp'], 'inner')
                    ->on('pp.product_id', '=', 'c.id')
                    ->on('pp.price_type', '=', DB::expr("'" . $_SESSION['prices'] . "'"))
                    ->where('pp.price', '>=', $filters['mincost'])
                    ->where('pp.price', '<=', $filters['maxcost']);
            if (isset($filters['discounts']))
                $result->where('c.sale', '=', 1);
        }

        if ($bySex) {
            $result->select('csv.specification_value_alias')
                ->join(['catalog_specifications_values', 'csv'], 'inner')
                ->on('csv.catalog_id', '=', 'c.id')
                ->on('csv.specification_alias', '=', DB::expr("'polrebenka'"))
                ->group_by('csv.specification_value_alias');
        } else {
            if (isset($filters['polrebenka']) ) {
                if (!in_array('uniseks', $filters['polrebenka'])) $filters['polrebenka'][] = 'uniseks';
                $result->where_open();
                foreach ($filters['polrebenka'] as $sex) {
                    $result->or_where('c.specifications', 'LIKE', '%' . $sex . '%');
                }
                $result->where_close();
            }
        }

        if (isset($_GET['search'])) {
            $result->join(['catalog_i18n', 'ci18n'], 'inner')
                ->on('ci18n.row_id', '=', 'c.id')
                ->on('ci18n.language', '=', DB::expr('"' . \I18n::lang() . '"'))
                ->where_open()
                ->or_where('ci18n.name', 'LIKE', '%' . $_GET['search'] . '%')
                ->or_where('ci18n.description', 'LIKE', '%' . $_GET['search'] . '%')
                ->or_where('c.id', 'LIKE', '%' . $_GET['search'] . '%')
                ->where_close();
        }

        if (isset($flag) && !empty($flag)) {
            $result->where('c.' . $flag, '=', 1);
        } else {
            $result->where('c.parent_id', 'IN', $groupIds);
        }

        return $result->group_by('ai18n.name', 'a.alias', 'a.id')
            ->order_by('a.alias', 'desc')
            ->find_all();
    }

    public static function getAgesWidget2($params) {
        $result = DB::select('ai18n.name', 'a.alias', 'a.id', 'csv.specification_value_alias')
            ->from(['ages', 'a'])
            ->join(['ages_i18n', 'ai18n'], 'inner')
            ->on('a.id', '=', 'ai18n.row_id')
            ->join(['product_ages', 'pa'], 'inner')
            ->on('pa.age_type', '=', 'a.code')
            ->join(['catalog', 'c'], 'inner')
            ->on('c.id', '=', 'pa.product_id')
            ->where('ai18n.language', '=', \I18n::lang())
            ->where('c.status', '=', 1)
            ->join(['catalog_specifications_values', 'csv'], 'inner')
            ->on('csv.catalog_id', '=', 'c.id')
            ->on('csv.specification_alias', '=', DB::expr("'polrebenka'"));

        if (!empty($params['filter'])) {
            $filters = Filter::getFilterArr($params['filter']);
            if (isset($filters['color'])) $result->where('c.color_alias', 'IN', $filters['color']);
            if (isset($filters['brand'])) $result->where('c.brand_alias', 'IN', $filters['brand']);
            if (isset($filters['manufacturer'])) $result->where('c.manufacturer_alias', 'IN', $filters['manufacturer']);
            if (isset($filters['material'])) {
                $result->where_open();
                foreach($filters['material'] as $material) {
                    $result->or_where('c.specifications', 'LIKE', '%' . $material . '%');
                }
                $result->where_close();
            }
            if (isset($filters['sezon'])) {
                $result->where_open();
                foreach($filters['sezon'] as $sezon) {
                    $result->or_where('c.specifications', 'LIKE', '%' . $sezon . '%');
                }
                $result->where_close();
            }
            if (isset($filters['size']))
                $result->join(['catalog_size_amount', 'csa'], 'inner')
                    ->on('csa.catalog_id', '=', 'c.id')
                    ->where('csa.size', 'IN', $filters['size']);
            if (isset($filters['mincost']) && isset($filters['maxcost']))
                $result->join(['product_prices', 'pp'], 'inner')
                    ->on('pp.product_id', '=', 'c.id')
                    ->on('pp.price_type', '=', DB::expr("'" . $_SESSION['prices'] . "'"))
                    ->where('pp.price', '>=', $filters['mincost'])
                    ->where('pp.price', '<=', $filters['maxcost']);
            if (isset($filters['discounts']))
                $result->where('c.sale', '=', 1);
        }

        if (!empty($params['search'])) {
            $result->join(['catalog_i18n', 'ci18n'], 'inner')
                ->on('ci18n.row_id', '=', 'c.id')
                ->on('ci18n.language', '=', DB::expr('"' . \I18n::lang() . '"'))
                ->where_open()
                ->or_where('ci18n.name', 'LIKE', '%' . $_GET['search'] . '%')
                ->or_where('ci18n.description', 'LIKE', '%' . $_GET['search'] . '%')
                ->or_where('c.id', 'LIKE', '%' . $_GET['search'] . '%')
                ->where_close();
        }

        if (!$params['category_id']) {
            switch($params['controller']) {
                case 'novelty': $flag = 'new'; break;
                case 'popular': $flag = 'top'; break;
                default:  $flag = $params['controller']; break;
            }
            $result->where('c.' . $flag, '=', 1);
        } else {
            $groupIds = Groups::getInnerIds($params['category_id']);
            $result->where('c.parent_id', 'IN', $groupIds);
        }

        return $result->group_by('ai18n.name', 'a.alias', 'a.id', 'csv.specification_value_alias')
            ->order_by('a.alias', 'desc')
            ->find_all();
    }

    public static function hasDiscounts($category_id)
    {
        $filters = self::getFilterArr(Route::param('filter'));
        $filters['discounts'] = 1;
        $search = $_POST['search'] ?? null;

        return self::count($category_id, $filters, $search);
    }

    // Get colors list for filter
    public static function getSizeWidget()
    {
        $flag = null;
        if (Route::param('group') == 0) {
            if (Route::controller() == 'novelty') {
                $flag = 'new';
            } else if (Route::controller() == 'popular') {
                $flag = 'top';
            } else if(Route::controller() == 'sale') {
                $flag = Route::controller();
            }
        }
        $groupIds = Groups::getInnerIds(Route::param('group'));
        $itemIds = Items::getProductsIdByGroups($groupIds, $flag);

        if ($itemIds) {
            $result = DB::select(DB::expr('DISTINCT catalog_size_amount.size'))
                ->from('catalog_size_amount')
                ->where('catalog_size_amount.catalog_id', 'IN', $itemIds)
                ->find_all()->as_array();
        } else {
            $result = DB::select(DB::expr('DISTINCT catalog_size_amount.size'))
                ->from('catalog_size_amount')
                ->where('catalog_size_amount.catalog_id', 'IN', $groupIds)
                ->find_all()->as_array();
        }

        usort($result, function($a, $b) {
            return strnatcmp($a->size, $b->size);
        });

        return $result;
    }


    // Get models list for filter
    public static function getModelsWidget()
    {
        $filter = Config::get('filter_array');
        if (!isset($filter['brand']) || !is_array($filter['brand']) || !isset($filter['brand'][0])) {
            return [];
        }
        return DB::select('models.name', 'models.alias', 'models.id')
            ->from('models')
            ->join('catalog')->on('catalog.model_alias', '=', 'models.alias')
            ->join('brands')->on('brands.alias', '=', 'catalog.brand_alias')
            ->where('brands.status', '=', 1)
            ->where('catalog.status', '=', 1)
            ->where('models.status', '=', 1)
            ->where('brands.alias', 'IN', $filter['brand'])
            ->where('catalog.parent_id', '=', Route::param('group'))
            ->group_by('models.id')
            ->order_by('models.name')
            ->find_all();
    }


    // Get specifications values list for filter
    public static function getSpecificationsWidget()
    {
        $result = DB::select('specifications_values.id',
            'specifications_values_i18n.name',
            'specifications_values.color',
            'specifications_values.alias',
            ['specifications_i18n.name', 'specification_name'],
            ['specifications.alias', 'specification_alias'],
            ['specifications.id', 'specification_id'],
            ['specifications.type_id', 'specification_type_id']
        )
            ->from('specifications_values')
            ->join('specifications_values_i18n')->on('specifications_values.id', '=', 'specifications_values_i18n.row_id')
            ->join('specifications')->on('specifications_values.specification_id', '=', 'specifications.id')
            ->join('specifications_i18n')->on('specifications.id', '=', 'specifications_i18n.row_id')
            //->join('catalog_specifications_values')
            // ->on('catalog_specifications_values.specification_value_alias', '=', 'specifications_values.alias')
            //->join('catalog')
            //->on('catalog_specifications_values.catalog_id', '=', 'catalog.id')
            //->where('catalog.status', '=', 1)
            ->where('specifications.status', '=', 1)
            ->group_by('specifications.alias')
            ->group_by('specifications_values.alias')
            ->find_all();
        $specifications = [];
        $values = [];
        foreach ($result as $obj) {
            $values[$obj->specification_alias][] = $obj;
            $specifications[$obj->specification_alias] = $obj->specification_name;
        }

        return [
            'list' => $specifications,
            'values' => $values,
        ];
    }


    // Set to memory filter as array
    public static function setFilterParameters()
    {
        if (!Route::param('filter')) {
            self::setParamsForCheck([]);
            return false;
        }
        $fil = explode('/', Route::param('filter'));
        $count = count($fil);
        if ($fil[$count - 1] == '') {
            unset($fil[$count - 1]);
        }

        $filter = array();
        foreach ($fil AS $key => $g) {
            $g = rawurldecode($g);
            $g = strip_tags($g);
            $g = stripslashes($g);
            $g = trim($g);
            $s = explode("-", $g);
            $filter_type = $s[0];
            unset($s[0]);
            $value = implode('-', $s);
            $value = rawurldecode($value);
            $value = strip_tags($value);
            $value = stripslashes($value);
            $value = trim($value);
            $arr_val = explode("_", $value);
            $filter[$filter_type] = $arr_val;

        }
        self::setParamsForCheck($filter);
        $check = self::checkFilterValues($filter, Config::get('filter_array'));

        return $check;

    }

    public static function setParamsForCheck($filter)
    {

        // specifications for sort
        $all_spec = ['manufacturer', 'brand', 'color', 'available', 'cost', 'size', 'amount', 'age', 'sort'];
        // values for sort
        $all_params = [];
        // right filter parameters for checking filter
        $filter_params = [];


        $brands = DB::select('alias')->from('brands')
            ->where('status', '=', 1)
            ->order_by('sort', 'ASC')
            ->order_by('id', 'DESC')
            ->find_all();

        foreach ($brands as $brand) {
            $all_params['brand'][] = $brand->alias;
            if (isset($filter['brand']) and in_array($brand->alias, $filter['brand'])) {
                $filter_params['brand'][] = $brand->alias;
            }
        }

        $manufacturers = DB::select('alias')->from('manufacturers')
            ->where('status', '=', 1)
            ->order_by('sort', 'ASC')
            ->order_by('id', 'DESC')
            ->find_all();

        foreach ($manufacturers as $manufacturer) {
            $all_params['manufacturer'][] = $manufacturer->alias;
            if (isset($filter['manufacturer']) and in_array($manufacturer->alias, $filter['manufacturer'])) {
                $filter_params['manufacturer'][] = $manufacturer->alias;
            }
        }

        $ages = DB::select('alias')->from('ages')
            ->where('status', '=', 1)
            ->order_by('sort', 'ASC')
            ->order_by('id', 'DESC')
            ->find_all();

        foreach ($ages as $age) {
            $all_params['age'][] = $age->alias;
            if (isset($filter['age']) and in_array($age->alias, $filter['age'])) {
                $filter_params['age'][] = $age->alias;
            }
        }

        $sizes = DB::select(DB::expr('DISTINCT catalog_size_amount.size'))
            ->from('catalog_size_amount')
            ->find_all()->as_array();
        foreach ($sizes as $size) {
            $all_params['size'][] = $size->size;
            if (isset($filter['size']) and in_array($size->size, $filter['size'])) {
                $filter_params['size'][] = $size->size;
            }
        }

        $colors = DB::select('alias', 'color')->from('colors')
            ->order_by('sort', 'ASC')
            ->order_by('id', 'DESC')
            ->find_all();

        foreach ($colors as $color) {
            $all_params['color'][] = $color->alias;
            if (isset($filter['color']) and in_array($color->alias, $filter['color'])) {
                $filter_params['color'][] = $color->alias;
            }
        }

        $all_params['available'] = [0, 1, 2];

        if (isset($filter['available'])) {
            foreach ($filter['available'] as $key => $val) {
                if ($val >= 0 and $val < 3) {
                    $filter_params['available'][] = $val;
                }
            }
        }

        $all_params['sort'] = ['priceAsc', 'priceDesc', 'top', 'alph'];

        if (isset($filter['sort'])) {
            foreach ($filter['sort'] as $key => $val) {
                $filter_params['sort'][] = $val;
            }
        }

        if (isset($filter['mincost'])) {
            $filter_params['mincost'] = $filter['mincost'];
        }

        if (isset($filter['maxcost'])) {
            $filter_params['maxcost'] = $filter['maxcost'];
        }

        $specifications = DB::select('specifications.alias', ['specifications_values.alias', 'value'])
            ->from('specifications')
            ->join('specifications_values')->on('specifications_values.specification_id', '=', 'specifications.id')
            ->where('specifications.status', '=', 1)
            ->where('specifications_values.status', '=', 1)
            ->order_by('specifications.sort', 'asc')
            ->order_by('specifications.id', 'desc')
            ->order_by('specifications_values.sort', 'asc')
            ->order_by('specifications_values.id', 'desc')
            ->find_all();

        foreach ($specifications as $spec) {
            if (!in_array($spec->alias, $all_spec)) {
                $all_spec[] = $spec->alias;
            }
            $all_params[$spec->alias][] = $spec->value;
            if (isset($filter[$spec->alias]) and in_array($spec->value, $filter[$spec->alias])) {
                $filter_params[$spec->alias][] = $spec->value;
            }

        }

        Config::set('filter_array', $filter_params);
        Config::set('sortable', ['spec' => $all_spec, 'params' => $all_params]);

    }

    public static function checkFilterValues($filter, $values)
    {
        if ($filter){

            foreach ($filter as $key => $zna) {

                if (!isset($values[$key])) {
                    // error 404
                    return ['success' => false];
                }
                foreach ($zna as $val) {
                    if (!in_array($val, $values[$key])) {
                        // error 404
                        return ['success' => false];
                    }
                }
            }
        }
        if ($filter !== $values) {
            // need redirect
            return ['success' => true, 'resort' => true];
        } else {
            //ok
            return ['success' => true, 'resort' => false];
        }


    }

    public static function getFilterFromArr($arr)
    {
        $filter = '';
        foreach ($arr as $key => $val) {
            $filter .= '/' . $key . '-' . implode('_', $val);
        }
        return $filter;
    }


    /**
     *  Check for existance parameter in the filter
     * @param  string $element alias of element of the filter
     * @param  string $specification alias of the filter
     * @return string                Word 'checked' or NULL
     */
    public static function checked($element, $specification)
    {
        $filter = [];
        if (Config::get('filter_array')) {
            $filter = Config::get('filter_array');
        }
        if (in_array($element, Arr::get($filter, $specification, []))) {
            return true;
        }
        return false;
    }


    /**
     *  Get a minimal number for price filter
     * @param  int $realMin real minimal number for current catalog group
     * @return int           minimal number for value in price filter
     */
    public static function min($realMin)
    {
        if (!Config::get('filter_array')) {
            return $realMin;
        }
        $filter = Config::get('filter_array');
        if (!isset($filter['mincost'])) {
            return $realMin;
        }
        $min = (int)$filter['mincost'][0];
        if ($min < $realMin) {
            return $realMin;
        }
        return $min;
    }


    /**
     *  Get a maximum number for price filter
     * @param  [int] $realMin  real maximum number for current catalog group
     * @return [int]           maximum number for value in price filter
     */
    public static function max($realMax)
    {
        if (!Config::get('filter_array')) {
            return $realMax;
        }
        $filter = Config::get('filter_array');
        if (!isset($filter['maxcost'])) {
            return $realMax;
        }
        $max = $filter['maxcost'][0];
        if ($max > $realMax) {
            return $realMax;
        }
        return number_format($max, 0);
    }


    // Set to memory the algorithm of filter elements
    public static function setSortElements()
    {
        $sortable = ['manufacturer', 'brand', 'model', 'color', 'available', 'mincost', 'maxcost', 'sort'];
        $result = DB::select('alias')->from('specifications')->where('status', '=', 1)->order_by('sort', 'ASC')->order_by('id', 'DESC')->find_all();
        foreach ($result as $obj) {
            $sortable[] = $obj->alias;
        }
        Config::set('sortable', $sortable);
    }


    // Generate input for filter
    public static function generateInput($filter, $obj, $alias, $type = 'simple')
    {
        if ($type == 'size') {
            $check = (isset($filter[$alias]) and in_array($obj->size, $filter[$alias]));
            $checked = Filter::checked($obj->size, $alias);
            if (isset($filter[$alias])) {
                $index = array_search($obj->size, $filter[$alias]);
                if (isset($index) && $index !== false) {
                    $count = (int)$filter['amount'][$index];
                } else {
                    $count = 0;
                }
            } else {
                $count = 0;
            }

            $disabled = (!$check and $count <= 0) ? 'style="pointer-events: none;opacity: 0.2;"' : '';
            $active = $count > 0;

        } elseif ($type == 'discounts') {
            $active = true;
            $filters = self::getFilterArr(Route::param('filter'));
            $checked = isset($filters['discounts']);
        } else {
            $check = (isset($filter[$alias]) and in_array($obj->alias, $filter[$alias]));
            $checked = Filter::checked($obj->alias, $alias);
            $disabled = (!$check) ? 'pointer-events: none;opacity: 0.2;' : '';
            $active = $check;
        }
        $input = '';

        if (!$active) return  $input;

        //Count items for filter value

        $activeClass = $checked ? 'active' : '';
        $checkedAttr = $checked ? 'checked' : '';

        switch ($type) {
            case 'color':
                $filterUrl = Filter::generateLinkWithFilter($alias, $obj->alias);
                $styles = 'background-color:' . $obj->color . ';';
                $input .= '<a class="filters__child-color ' . $activeClass . '" data-type="' . $type . '" data-value="' . $obj->alias . '" href="' . $filterUrl . '" style="' . $styles . '"></a>';
                break;
            case 'size':
                $filterUrl = Filter::generateLinkWithFilter($alias, $obj->size);
                $input .= '<a href="' . $filterUrl . '" data-type="' . $type . '" data-value="' . $obj->size . '" class="product__size-item  product__size-item--link   ' . $activeClass . '">';
                $input .= '<span class="product__size-text">' . $obj->size . '</span>';
                $input .= '</a>';
                break;
            case 'discounts':
                $filterUrl = Filter::generateLinkWithFilter($alias, 1);
                $input .= '<a href="' . $filterUrl . '" data-type="' . $type . '" data-value="1" class="filters__child-link  ' . $activeClass . '">';
                $input .= '<span class="filter-input__name">' . __('Скидка') . '</span>';
                $input .= '</a>';
                break;
            default:
                $filterUrl = Filter::generateLinkWithFilter($alias, $obj->alias);
                $input .= '<a href="' . $filterUrl . '" data-type="' . $type . '" data-value="' . $obj->alias . '" class="filters__child-link  ' . $activeClass . '">';
                $input .= '<span class="filter-input__name">' . $obj->name . '</span>';
                $input .= '</a>';
                break;
        }
        return $input;
    }


    // Generate unique input for filter
    public static function generateElseInput($filter, $name, $value, $alias)
    {
        $value = (string)$value;
        $check = (isset($filter[$alias]) and in_array($value, $filter[$alias]));
        $checked = Filter::checked($value, $alias);
        $disabled = (!$check and !$checked) ? 'disabled' : '';
        $input = '';

        $input .= '<label class="checkBlock" for="' . $alias . $value . '">';
        if (!$disabled) {
            $input .= '<a href="' . Filter::generateLinkWithFilter($alias, $value) . '">';
        }
        $input .= '<input  id="' . $alias . $value . '" value="' . $value . '" type="checkbox" ' . $checked . $disabled . ' />';
        $input .= '<ins></ins>';
        $input .= '<p>' . $name . '</p>';
        if (!$disabled) {
            $input .= '</a>';
        }
        $input .= '</label>';

        return $input;
    }


    // Sort link in items list
    public static function setSortLink($sort = null, $type = null)
    {
        $arr = explode('?', $_SERVER['REQUEST_URI']);
        $link = $arr[0];
        $_get = explode('&', $arr[1]);
        $get = [];
        foreach ($_get as $k) {
            $r = explode('=', $k);
            $get[$r[0]] = $r[1];
        }
        if (isset($get['sort'])) {
            unset($get['sort']);
        }
        if (isset($get['type'])) {
            unset($get['type']);
        }
        if ($sort !== null) {
            $get['sort'] = $sort;
            if ($type !== null) {
                $get['type'] = $type;
            }
        }
        if (count($get)) {
            $add = [];
            foreach ($get as $key => $value) {
                if ($key && $value) {
                    $add[] = $key . '=' . $value;
                }
            }
            if ($add) {
                $link .= '?' . implode('&', $add);
            }
        }
        return $link;
    }


    // Check if this is current sortable
    public static function isThisSort($sort = null, $type = null)
    {
        if (Arr::get($_GET, 'sort') == $sort and Arr::get($_GET, 'type') == $type) {
            return 'active';
        }
        return null;
    }


    // Link with per page argument
    public static function setPerPageLink($number)
    {
        $arr = explode('?', $_SERVER['REQUEST_URI']);
        $link = $arr[0];
        $_get = explode('&', $arr[1]);
        $get = [];
        foreach ($_get as $k) {
            $r = explode('=', $k);
            $get[$r[0]] = $r[1];
        }
        if (isset($get['per_page'])) {
            unset($get['per_page']);
        }
        $get['per_page'] = $number;
        $add = [];
        foreach ($get as $key => $value) {
            if ($key && $value) {
                $add[] = $key . '=' . $value;
            }
        }
        if ($add) {
            $link .= '?' . implode('&', $add);
        }
        return $link;
    }

    /**
     * Generate link with filter parameters
     * @param  string $key [specification alias]
     * @param  string $value [specification value alias]
     * @return string        [link]
     */
    public static function generateLinkWithFilter($key, $value)
    {
        // Devide url from GET parameters
        $uri = Arr::get($_SERVER, 'REQUEST_URI');
        $uri = explode('?', $uri);
        $link = $uri[0];
        $get = '';
        if (count($uri) > 1) {
            $get = '?' . $uri[1];
        }
        // Clear link from pagination
        if ((int)Route::param('page')) {
            $link = str_replace('/page/' . (int)Route::param('page'), '', $link);
        }
        // If filter is empty - create it and return
        $filter = Config::get('filter_array');
        // Clear link from filter
        if (Route::param('filter')) {
            $link = str_replace('/' . Route::param('filter'), '', $link);
        }
        // Setup filter
        if (is_array($filter) && isset($filter[$key])) {
            if (!in_array($value, $filter[$key])) {
                $filter[$key][] = $value;
            } else {
                unset($filter[$key][array_search($value, $filter[$key])]);
                if (empty($filter[$key]) or (count($filter[$key]) == 1 and !trim(end($filter[$key])))) {
                    unset($filter[$key]);
                }
            }
        } else {
            $filter[$key] = [$value];
        }
        // Generate link with filter
        $filter = Filter::generateFilter($filter);
        // Return link
        if ($filter) {
            return $link . '/' . $filter . $get;
        }
        return $link . $get;
    }

    public static function generateFilterUrl($category_id, $filters, $search = null)
    {
        if ($category_id) {
            $category = Groups::getRow($category_id);
            $categoryBaseUrl = HTML::link('catalog/' . $category['obj']->alias);
        } else {
            $categoryBaseUrl = HTML::link('search');
        }
        if (empty($filters)) return $categoryBaseUrl;

        ksort($filters);
        $filterParts = array();
        foreach ($filters as $key => $filter) {
            if (is_array($filter)) {
                sort($filter);
                $filterParts[] = $key . '-' . implode('_', $filter);
            } else {
                $filterParts[] = $key . '-' . $filter;
            }

        }

        $url = $categoryBaseUrl . '/' . implode('/', $filterParts);
        $url .= $search ? '?search=' . $search : '';

        return $url;
    }


    /**
     *  Create filter part of URI
     * @param  array $array [associative array with filter elements]
     * @return string        [part of new URI]
     */
    public static function generateFilter($array)
    {
        // Sort filter elements
        $array = Filter::sortFilter($array);
        // Generate and return filters part
        $link = [];
        if (is_array($array) && count($array)) {
            foreach ($array as $key => $values) {
                $link[] = $key . '-' . implode('_', $values);
            }
        }

        return implode('/', $link);
    }


    /**
     *  Sort our filter
     * @param  array $array Our filter in array
     * @return array         Our filter but sorted!
     */
    public static function sortFilter($array)
    {
        $template = Config::get('sortable');
        foreach ($template['spec'] as $tpl) {
            if (isset($array[$tpl]) and !empty($array[$tpl]) and !(count($array[$tpl]) == 1 and trim((string)end($array[$tpl])) == "")) {
                $filter[$tpl] = [];
                if ($tpl != 'mincost' and $tpl != 'maxcost') {
                    foreach ($template['params'][$tpl] as $key => $val) {
                        if (in_array($val, $array[$tpl])) {
                            $filter[$tpl][] = $val;
                        }
                    }
                } else {
                    $filter[$tpl] = $array[$tpl];
                }
            }
        }
        return $filter;
    }

    public static function getVauluesAsText()
    {

        $text = '';

        $filter = Config::get('filter_array');

        if (isset($filter['brand'])) {
            $brands = DB::select('name', 'id')->from('brands')->where('alias', 'in', $filter['brand'])->find_all()->as_array('id', 'name');
            $text .= implode(', ', $brands);
        }

        if (isset($filter['model'])) {
            $models = DB::select('name', 'id')->from('models')->where('alias', 'in', $filter['model'])->find_all()->as_array('id', 'name');
            if ($text != '') {
                $text .= ', ';
            }
            $text .= implode(', ', $models);
        }

        if (isset($filter['mincost']) and isset($filter['maxcost'])) {
            if ($text != '') {
                $text .= ', ';
            }
            $text .= 'от ' . $filter['mincost'][0] . ' '. (User::info()->type != 1) ?'грн.' : '$' .' до ' . $filter['maxcost'][0] . ' '. (User::info()->type != 1) ?'грн.' : '$';
        } else if (isset($filter['mincost'])) {
            if ($text != '') {
                $text .= ', ';
            }
            $text .= 'от ' . $filter['mincost'][0] . ' '. (User::info()->type != 1) ?'грн.' : '$';
        } else if (isset($filter['maxcost'])) {
            if ($text != '') {
                $text .= ', ';
            }
            $text .= 'до ' . $filter['maxcost'][0] . ' '. (User::info()->type != 1) ?'грн.' : '$';
        }

        if (isset($filter['available'])) {
            if ($text != '') {
                $text .= ', ';
            }
            foreach ($filter['available'] as $key => $val) {
                if ($val == 0) $text .= 'нет в наличии';
                if ($val == 1) $text .= 'в наличии';
                if ($val == 2) $text .= 'под заказ';
            }

        }

        $valuesAliases = [];
        foreach ($filter as $key => $arr) {
            if (!in_array($key, ['brand', 'model', 'mincost', 'maxcost', 'available'])) {
                foreach ($arr as $val) {
                    $valuesAliases[] = $val;
                }
            }
        }

        if (sizeof($valuesAliases)) {
            $values = DB::select('id', 'name')->from('specifications_values')
                ->where('alias', 'IN', $valuesAliases)
                ->find_all()
                ->as_array('id', 'name');
            if (sizeof($values)) {
                if ($text != '') {
                    $text .= ', ';
                }
                $text .= implode(', ', $values);
            }
        }

        return $text;

    }

    public static function getVauluesAsText2()
    {
        $checkedFilter = Filter::getFilterArr(Route::param('filter'));
        $checkValues = null;
        if(!empty($checkedFilter)){
            foreach ($checkedFilter as $key => $checked) {
                $name = self::getFilterName($key);

                if (is_array($checked)) {
                    $values = array();
                    foreach ($checked as $check) {
                        switch ($key){
                            case 'brand': $obj = Brands::getRowSimple($check, 'alias'); break;
                            case 'manufacturer': $obj = Manufacturers::getRowSimple($check, 'alias'); break;
                            case 'color': $obj = Colors::getRowSimple($check, 'alias'); break;
                            case 'size': $obj = $check; break;
                            case 'discounts': $obj = __('Скидка'); break;
                            case 'age': $obj = \Wezom\Modules\Catalog\Models\Ages::getRowSimple($check, 'alias'); break;
                            default: $obj = \Wezom\Modules\Catalog\Models\SpecificationsValues::getRowSimple($check, 'alias'); break;
                        }
                        if ($obj) {
                            $values[] = is_string($obj) ? $obj : $obj->name;
                        }
                    }

                    $checkValues[] = $name . ' ' . mb_strtolower(implode(',', $values));
                } else {
                    switch ($key){
                        case 'brand': $obj = Brands::getRowSimple($checked, 'alias'); break;
                        case 'manufacturer': $obj = Manufacturers::getRowSimple($checked, 'alias'); break;
                        case 'color': $obj = Colors::getRowSimple($checked, 'alias'); break;
                        case 'size': $obj = $checked; break;
                        case 'discounts': $obj = __('Скидка'); break;
                        case 'age': $obj = \Wezom\Modules\Catalog\Models\Ages::getRowSimple($checked, 'alias'); break;
                        default: $obj = \Wezom\Modules\Catalog\Models\SpecificationsValues::getRowSimple($checked, 'alias'); break;
                    }
                    if ($obj) {
                        $value = is_string($obj) ? $obj : $obj->name;
                        $checkValues[] = $name . ' ' . mb_strtolower($value);
                    }
                }

            }
        }

        return is_array($checkValues) ? implode('; ', $checkValues) : false;
    }

    public static function getValuesAsText3()
    {
        $checkedFilter = Filter::getFilterArr(Route::param('filter'));
        $checkValues = [];
        if(!empty($checkedFilter)){
            foreach ($checkedFilter as $key => $checked) {
                if (is_array($checked)) {
                    $values = array();
                    foreach ($checked as $check) {
                        switch ($key){
                            case 'brand': $obj = Brands::getRowSimple($check, 'alias'); break;
                            case 'manufacturer': $obj = Manufacturers::getRowSimple($check, 'alias'); break;
                            case 'color': $obj = Colors::getRowSimple($check, 'alias'); break;
                            case 'size': $obj = $check; break;
                            case 'age': $obj = \Wezom\Modules\Catalog\Models\Ages::getRowSimple($check, 'alias'); break;
                            case 'discounts': $obj = __('Скидка'); break;
                            default: $obj = \Wezom\Modules\Catalog\Models\SpecificationsValues::getRowSimple($check, 'alias'); break;
                        }
                        if ($obj) {
                            $values[] = is_string($obj) ? $obj : $obj->name;
                        }
                    }

                    $checkValues = array_merge($checkValues, $values);
                } else {
                    switch ($key){
                        case 'brand': $obj = Brands::getRowSimple($checked, 'alias'); break;
                        case 'manufacturer': $obj = Manufacturers::getRowSimple($checked, 'alias'); break;
                        case 'color': $obj = Colors::getRowSimple($checked, 'alias'); break;
                        case 'size': $obj = $checked; break;
                        case 'age': $obj = \Wezom\Modules\Catalog\Models\Ages::getRowSimple($checked, 'alias'); break;
                        case 'discounts': $obj = __('Скидка'); break;
                        default: $obj = \Wezom\Modules\Catalog\Models\SpecificationsValues::getRowSimple($checked, 'alias'); break;
                    }
                    if ($obj) {
                        $value = is_string($obj) ? $obj : $obj->name;
                        $checkValues[] = $value;
                    }
                }

            }
        }

        return is_array($checkValues) ? implode(', ', $checkValues) : false;
    }

    protected static function getFilterName($key)
    {
        switch($key) {
            case 'brand': $name = __('Бренд'); break;
            case 'manufacturer': $name = __('Производитель'); break;
            case 'color': $name = __('Цвет'); break;
            case 'size': $name = __('Размер'); break;
            case 'age': $name = __('Возраст'); break;
            default: $name = \Wezom\Modules\Catalog\Models\Specifications::getRowSimple($key, 'alias')->name; break;
        }

        return mb_strtolower($name);
    }

    public static function getFilterArr($filter)
    {
        $temp = explode('/', $filter);
        $arr = $filter_array = [];
        foreach ($temp as $item) {
            $temp2 = explode('-', $item);
            $key = $temp2[0];
            unset($temp2[0]);
            $value = implode('-', $temp2);
            $arr[$key] = $value;
        }
        foreach ($arr as $key => $val) {
            if($key) {
                if (strpos($val, '_')) {
                    $filter_array[$key] = explode('_', $val);
                } else {
                    $filter_array[$key] = [$val];
                }
            }
        }

        return $filter_array;
    }

    public static function getFilterHash($filter)
    {
        $filter = Filter::getFilterArr($filter);
        sort($filter);
        $filterHash = '';
        foreach ($filter as $key => $value) {
            $filterHash .= $value[0];
        }

        return md5($filterHash);
    }

    public static function count($category_id, $filters, $search = null) {
        $count = DB::select(DB::expr('COUNT(DISTINCT c.id) AS count'))
            ->from(['catalog', 'c'])
            ->join(['catalog_size_amount', 'cza'])
            ->on('cza.catalog_id', '=', 'c.id')
            ->where('c.status', '=', 1)
            ->where('cza.amount', '>', 0);

        if (!empty($category_id)) {
            $ids = Groups::getInnerIds($category_id);
            $count->where('c.parent_id', 'IN', $ids);
        }

        if (!empty($filters)) {
            if (isset($filters['color'])) $count->where('c.color_alias', 'IN', $filters['color']);
            if (isset($filters['brand'])) $count->where('c.brand_alias', 'IN', $filters['brand']);
            if (isset($filters['manufacturer'])) $count->where('c.manufacturer_alias', 'IN', $filters['manufacturer']);
            if (isset($filters['material'])) {
                $count->where_open();
                foreach($filters['material'] as $material) {
                    $count->or_where('c.specifications', 'LIKE', '%' . $material . '%');
                }
                $count->where_close();
            }
            if (isset($filters['sezon'])) {
                $count->where_open();
                foreach($filters['sezon'] as $sezon) {
                    $count->or_where('c.specifications', 'LIKE', '%' . $sezon . '%');
                }
                $count->where_close();
            }
            if (isset($filters['polrebenka']) ) {
                if (!in_array('uniseks', $filters['polrebenka'])) $filters['polrebenka'][] = 'uniseks';
                $count->where_open();
                foreach ($filters['polrebenka'] as $sex) {
                    $count->or_where('c.specifications', 'LIKE', '%' . $sex . '%');
                }
                $count->where_close();
            }
            if (isset($filters['age']))
                $count->join(['product_ages', 'pa'], 'inner')
                    ->on('c.id', '=', 'pa.product_id')
                    ->join(['ages', 'a'], 'inner')
                    ->on('a.code', '=', 'pa.age_type')
                    ->where('a.alias', 'IN', $filters['age']);
            if (isset($filters['size']))
                $count->join(['catalog_size_amount', 'csa'], 'inner')
                    ->on('csa.catalog_id', '=', 'c.id')
                    ->where('csa.size', 'IN', $filters['size']);
            if (isset($filters['mincost']) && isset($filters['maxcost']))
                $count->join(['product_prices', 'pp'], 'inner')
                    ->on('pp.product_id', '=', 'c.id')
                    ->on('pp.price_type', '=', DB::expr("'" . $_SESSION['prices'] . "'"))
                    ->where('pp.price', '>=', $filters['mincost'])
                    ->where('pp.price', '<=', $filters['maxcost']);
            if (isset($filters['discounts']))
                $count->where('c.sale', '=', 1);
        }

        if ($search) {
            $count->join(['catalog_i18n', 'ci18n'], 'inner')
                ->on('ci18n.row_id', '=', 'c.id')
                ->on('ci18n.language', '=', DB::expr('"' . \I18n::lang() . '"'))
                ->where_open()
                ->or_where('ci18n.name', 'LIKE', '%' . $search . '%')
                ->or_where('ci18n.description', 'LIKE', '%' . $search . '%')
                ->or_where('c.id', 'LIKE', '%' . $search . '%')
                ->where_close();
        }

        return $count->count_all();
    }


}
