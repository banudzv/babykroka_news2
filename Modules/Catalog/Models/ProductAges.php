<?php
namespace Modules\Catalog\Models;

use Core\Common;
use Core\HTML;
use Core\QB\DB;

/**
 * Class @ProductAges
 * @package Modules\Catalog\Models
 */
class ProductAges extends Common {

    /**
     * @var string
     */
    public static $table = 'product_ages';
    /**
     * @var array
     */
    public static $rules = [];

    /**
     * @param int $product_id
     * @param string $age_type
     * @return mixed|null
     */
    public static function getAgesRowByPriceType($product_id,$age_type)
    {
        return DB::select()->from(self::table())
            ->where('product_id', '=', $product_id)
            ->where('age_type', '=', $age_type)
            ->find();
    }

    /**
     * Get product ages by ID
     * @param integer $product_id
     * @return array
     */
    public static function getProductAges($product_id): array
    {
        $productAges = [];
        $res = self::getCustomRows(['product_id' => $product_id]);
        foreach ($res as $obj){
            $productAges[] = Ages::getRow($obj->age_type,'code')->name;
        }
        return $productAges;
    }

    /**
     * Get specifications ids that belongs to group with ID = $id
     * @param integer $product_id
     * @return array
     */
    public static function getAgesCodes($product_id): array
    {
        $productAges = [];
        $res = self::getCustomRows(['product_id' => $product_id]);
        foreach ($res as $obj) {
            $productAges[] = $obj->age_type;
        }
        return $productAges;
    }


}
