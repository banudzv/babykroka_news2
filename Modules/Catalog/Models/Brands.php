<?php
namespace Modules\Catalog\Models;

use Core\CommonI18n;
use Core\QB\DB;

class Brands extends CommonI18n
{

    public static $table = 'brands';

    public static function getBrandsSelect($db = null){
        $brands_aliases = array_keys(Items::getItemsBrandsAliases());

        return DB::select(static::$table.'.*', static::$table.'_i18n.name')
            ->from(static::$table)
            ->join(static::$table.'_i18n')->on(static::$table.'.id', '=', static::$table.'_i18n.row_id')
            ->where('alias', 'IN', $brands_aliases)
            ->order_by(static::$table.'_i18n.name')
            ->find_all($db)->as_array('alias', 'name');
    }
}
