<?php
namespace Modules\Catalog\Models;

use Core\Common;
use Core\QB\DB;

class Favorite extends Common
{
    public static $table = 'favorite';
    public static $catalog = 'catalog';



    public static function getFavorites($user_id)
    {
        $result = DB::select(
            static::$catalog.'.*',
            'product_prices.price',
            'product_prices.price_old',
            'product_prices.currency_id'
        )
            ->from(static::$table)
            ->where(static::$table.'.user_id', '=', $user_id)
            ->join(static::$catalog)->on(static::$catalog.'.id', '=', static::$table.'.item_id')
            ->join('product_prices')
            ->on('catalog.id','=','product_prices.product_id')
            ->where('product_prices.price_type', '=', $_SESSION['prices'])
            ->where(static::$catalog.'.status', '=', 1)
            ->find_all();

        return $result;
    }



    public static function getFavorite($user_id, $item_id)
    {
        $result = DB::select()
            ->from(static::$table)
            ->where(static::$table.'.user_id', '=', $user_id)
            ->where(static::$table.'.item_id', '=', $item_id)
            ->find();

        return $result;
    }

    public static function addToFavorite($user_id, $item_id)
    {
        $favorite = Common::factory(static::$table)->insert([
            'user_id' => $user_id,
            'item_id' => $item_id,
        ]);

        if ($favorite) {
            return $favorite;
        } else {
            return false;
        }
    }

    public static function removeFromFavorite($id)
    {
        $favorite = Common::factory(static::$table)->delete($id);

        return $favorite;
    }

    public static function favoriteCount($user_id)
    {
        $favorite = DB::select(array(DB::expr('COUNT(DISTINCT '.static::$table.'.id)'), 'count'))
            ->from(static::$table)
            ->where('user_id', '=', $user_id)
            ->join('catalog')->on('item_id', '=', 'catalog.id')
            ->where('catalog.status','=', 1)
            ->find();

        return $favorite->count;
    }
}
