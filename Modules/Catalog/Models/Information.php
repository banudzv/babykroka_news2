<?php
namespace Modules\Catalog\Models;

use Core\CommonI18n;
use Core\QB\DB;

class Information extends CommonI18n
{
    public static $table = 'catalog_information_groups';

    public static function countKids($id) {
        $result = DB::select([DB::expr('COUNT(id)'), 'count'])
            ->from(static::$table)
            ->where('parent_id', '=', $id);
        return $result->count_all();
    }
}
