<?php
    namespace Modules\Catalog\Models;


    use Core\Common;
    use Core\QB\DB;

    class SpecificationsValues extends Common {

        public static $table = 'specifications_values';
        public static $rules = [];

        public static function getMaxPriority($spec_id) {
            $row = DB::select([DB::expr('MAX('.static::$table.'.sort)'), 'max'])
                ->from(static::$table)
                ->where(static::$table.'.specification_id', '=', $spec_id)
                ->find();
            if( !$row ) {
                return 0;
            }
            return (int) $row->max + 1;
        }
    }


