<?php

namespace Modules\Catalog\Models;

use Core\CommonI18n;
use Core\Config;
use Core\Cookie;
use Core\QB\DB;
use Core\Common;

class Items extends CommonI18n
{

    public static $table = 'catalog';
    public static $tableImages = 'catalog_images';

    public static function searchRows($queries, $limit = null, $offset = null)
    {
        $result = DB::select()
            ->from(static::$table)
            ->where('status', '=', 1);
        $result->and_where_open();
        foreach ($queries as $query) {
            $result->where('name', 'LIKE', '%' . $query . '%');
        }
        foreach ($queries as $query) {
            $result->or_where('artikul', 'LIKE', '%' . $query . '%');
        }
        $result->and_where_close();
        $result->order_by('sort', 'ASC');
        $result->order_by('id', 'DESC');
        if ($limit !== null) {
            $result->limit($limit);
            if ($offset !== null) {
                $result->offset($offset);
            }
        }
        return $result->find_all();
    }


    public static function countSearchRows($queries)
    {
        $result = DB::select([DB::expr('COUNT(id)'), 'count'])
            ->from(static::$table)
            ->where('status', '=', 1);
        $result->and_where_open();
        foreach ($queries as $query) {
            $result->where('name', 'LIKE', '%' . $query . '%');
        }
        foreach ($queries as $query) {
            $result->or_where(static::$table . '.artikul', 'LIKE', '%' . $query . '%');
        }
        $result->and_where_close();
        return $result->count_all();
    }


    public static function getQueries($query)
    {
        $spaces = ['-', '_', '/', '\\', '=', '+', '*', '$', '@', '(', ')', '[', ']', '|', ',', '.', ';', ':', '{', '}'];
        $query = str_replace($spaces, ' ', $query);
        $arr = preg_split("/[\s,]+/", $query);
        $result = array_filter($arr, function ($el) {
            return !empty($el);
        });
        return $result;
    }


    public static function getBrandItems($brand_alias, $sort = null, $type = null, $limit = null, $offset = null)
    {
        $result = DB::select(static::$table . '.*')
            ->from(static::$table)
            ->where(static::$table . '.brand_alias', '=', $brand_alias)
            ->where(static::$table . '.status', '=', 1);
        if ($sort !== null) {
            if ($type !== null) {
                $result->order_by(static::$table . '.' . $sort, $type);
            } else {
                $result->order_by(static::$table . '.' . $sort);
            }
        }
        if ($limit !== null) {
            $result->limit($limit);
            if ($offset !== null) {
                $result->offset($offset);
            }
        }
        return $result->find_all();
    }


    public static function countBrandItems($brand_alias)
    {
        $result = DB::select([DB::expr('COUNT(' . static::$table . '.id)'), 'count'])
            ->from(static::$table)
            ->where(static::$table . '.brand_alias', '=', $brand_alias)
            ->where(static::$table . '.status', '=', 1);
        return $result->count_all();
    }


    public static function getItemsByFlag($flag, $sort = null, $type = null, $limit = null, $offset = null)
    {
        $result = DB::select(static::$table . '.*')
            ->from(static::$table)
            ->where(static::$table . '.' . $flag, '=', 1)
            ->where(static::$table . '.status', '=', 1);
        if ($sort !== null) {
            if ($type !== null) {
                $result->order_by(static::$table . '.' . $sort, $type);
            } else {
                $result->order_by(static::$table . '.' . $sort);
            }
        }
        if ($limit !== null) {
            $result->limit($limit);
            if ($offset !== null) {
                $result->offset($offset);
            }
        }
        return $result->find_all();
    }


    public static function countItemsByFlag($flag)
    {
        $result = DB::select([DB::expr('COUNT(' . static::$table . '.id)'), 'count'])
            ->from(static::$table)
            ->where(static::$table . '.' . $flag, '=', 1)
            ->where(static::$table . '.status', '=', 1);
        return $result->count_all();
    }


    public static function addViewed($id)
    {
        $ids = static::getViewedIDs();
        if (!in_array($id, $ids)) {
            $ids[] = $id;
            Cookie::setArray('viewed', $ids, 60 * 60 * 24 * 30);
        }
        return;
    }


    public static function getViewedIDs()
    {
        $ids = Cookie::getArray('viewed', []);
        return $ids;
    }


    public static function getViewedItems($sort = null, $type = null, $limit = null, $offset = null)
    {
        $ids = Items::getViewedIDs();
        if (!$ids) {
            return [];
        }
        $result = DB::select(static::$table . '.*')
            ->from(static::$table)
            ->where(static::$table . '.id', 'IN', $ids)
            ->where(static::$table . '.status', '=', 1);
        if ($sort !== null) {
            if ($type !== null) {
                $result->order_by(static::$table . '.' . $sort, $type);
            } else {
                $result->order_by(static::$table . '.' . $sort);
            }
        }
        if ($limit !== null) {
            $result->limit($limit);
            if ($offset !== null) {
                $result->offset($offset);
            }
        }
        return $result->find_all();
    }


    public static function countViewedItems()
    {
        $ids = Items::getViewedIDs();
        if (!$ids) {
            return 0;
        }
        $result = DB::select([DB::expr('COUNT(' . static::$table . '.id)'), 'count'])
            ->from(static::$table)
            ->where(static::$table . '.id', 'IN', $ids)
            ->where(static::$table . '.status', '=', 1);
        return $result->count_all();
    }

    public static function getParentIdRow($id){
        return DB::select('parent_id')
            ->from(static::$table)
            ->where(static::$table . '.id', '=', $id)
            ->find();
    }


    public static function getRow($value, $field = 'id', $status = null)
    {
        $result = DB::select(
            static::$table . '.*',
            ['brands_i18n.name', 'brand_name'],
            ['models.name', 'model_name'],
            ['catalog_tree_i18n.name', 'parent_name']
        )
            ->from(static::$table)
            ->join('catalog_tree', 'LEFT')->on('catalog_tree.id', '=', static::$table.'.parent_id')
            ->where('catalog_tree_i18n.language', '=', \I18n::lang())

            ->join('catalog_tree_i18n')->on('catalog_tree.id', '=', 'catalog_tree_i18n.row_id')
            ->where('catalog_tree_i18n.language', '=', \I18n::lang())

            ->join('brands', 'LEFT')
            ->on(static::$table . '.brand_alias', '=', 'brands.alias')
            ->on('brands.status', '=', DB::expr('1'))

            ->join('brands_i18n')->on('brands.id', '=', 'brands_i18n.row_id')
            ->where('brands_i18n.language', '=', \I18n::lang())

            ->join('models', 'LEFT')
            ->on(static::$table . '.model_alias', '=', 'models.alias')
            ->on('models.status', '=', DB::expr('1'));
        if ($status !== null) {
            $result = $result->where(static::$table . '.status', '=', 1);
        }

        $result = $result->where(static::$table . '.id', '=', $value);
        return $result->find();
    }


    public static function getItemImages($item_id)
    {
        $result = DB::select('image')
            ->from(static::$tableImages)
            ->where(static::$tableImages . '.catalog_id', '=', $item_id)
            ->order_by(static::$tableImages . '.sort');
        return $result->find_all();
    }

    public static function getItemSpecificationsByID($item_id, $db = null)
    {
        $specifications = DB::select('specifications_i18n.*', 'specifications.*')->from('specifications')
            ->join('specifications_i18n', 'LEFT')->on('specifications.id', '=', 'specifications_i18n.row_id')
            ->join('catalog_tree_specifications', 'LEFT')->on('catalog_tree_specifications.specification_id', '=', 'specifications.id')
            ->where('specifications.status', '=', 1)
            ->where('specifications_i18n.language', '=', \I18n::lang())
            ->order_by('specifications_i18n.name')
            ->as_object()->execute($db);
        $res = DB::select()->from('specifications_values')
            ->join('catalog_specifications_values', 'LEFT')->on('catalog_specifications_values.specification_value_alias', '=', 'specifications_values.alias')
            ->join('specifications_values_i18n', 'LEFT')->on('specifications_values_i18n.row_id', '=', 'specifications_values.id')
            ->where('catalog_specifications_values.catalog_id', '=', $item_id)
            ->where('specifications_values_i18n.language', '=',\I18n::lang())
            ->where('status', '=', 1)
            ->as_object()->execute($db);
        $specValues = [];
        foreach ($res as $obj) {
            $specValues[$obj->specification_id][] = $obj;
        }
        $spec = [];
        foreach ($specifications as $obj) {
            if (isset($specValues[$obj->id]) and is_array($specValues[$obj->id]) and count($specValues[$obj->id])) {
                if ($obj->type_id == 3) {
                    $spec[$obj->name] = '';
                    foreach ($specValues[$obj->id] AS $o) {
                        $spec[$obj->name] .= $o->name . ', ';
                    }
                    $spec[$obj->name] = substr($spec[$obj->name], 0, -2);
                } else {
                    $spec[$obj->name] = $specValues[$obj->id][0]->name;
                }
            }
        }
        return $spec;
    }


    public static function getItemSpecifications($item_id, $parent_id)
    {
        $specifications = DB::select('specifications.*')->from('specifications')
            ->join('catalog_tree_specifications', 'LEFT')->on('catalog_tree_specifications.specification_id', '=', 'specifications.id')
            ->where('catalog_tree_specifications.catalog_tree_id', '=', $parent_id)
            ->where('specifications.status', '=', 1)
            ->order_by('specifications.name')
            ->as_object()->execute();
        $res = DB::select()->from('specifications_values')
            ->join('catalog_specifications_values', 'LEFT')->on('catalog_specifications_values.specification_value_alias', '=', 'specifications_values.alias')
            ->where('catalog_specifications_values.catalog_id', '=', $item_id)
            ->where('status', '=', 1)
            ->as_object()->execute();
        $specValues = [];
        foreach ($res as $obj) {
            $specValues[$obj->specification_id][] = $obj;
        }
        $spec = [];
        foreach ($specifications as $obj) {
            if (isset($specValues[$obj->id]) and is_array($specValues[$obj->id]) and count($specValues[$obj->id])) {
                if ($obj->type_id == 3) {
                    $spec[$obj->name] = '';
                    foreach ($specValues[$obj->id] AS $o) {
                        $spec[$obj->name] .= $o->name . ', ';
                    }
                    $spec[$obj->name] = substr($spec[$obj->name], 0, -2);
                } else {
                    $spec[$obj->name] = $specValues[$obj->id][0]->name;
                }
            }
        }
        return $spec;
    }

    public static function getReviews($catalog_id)
    {

        $result = DB::select()->from('catalog_comments')
            ->where('catalog_id', '=', $catalog_id)
            ->where('status', '=', 1)
            ->order_by('date', 'DESC')
            ->find_all();

        return $result;

    }


    public static function getPagedReviews($catalog_id, $limit, $offset = 0)
    {

        $result = DB::select()->from('catalog_comments')
            ->where('catalog_id', '=', $catalog_id)
            ->where('status', '=', 1)
            ->where('date', '<=', time())
            ->limit($limit)
            ->offset($offset)
            ->order_by('date', 'DESC')
            ->find_all();

        $previews = [];

        foreach ($result as $r) {
            $now = new \DateTime("now");
            $date = new \DateTime("@" . $r->date);
            $diff = $date->diff($now);

            if ($diff->y > 0) {
                $diff->m = $diff->m + ($diff->y * 12);
                $diff->y = 0;
            }
            if ($diff->m < 1) {
                if ($diff->d < 1) {
                    if ($diff->h < 1) {
                        $r->date = $diff->format('менее часа назад');
                    } else if ($diff->h == 1) {
                        $r->date = $diff->format('%h час назад');
                    } else if ($diff->h > 1 && $diff->h < 5) {
                        $r->date = $diff->format('%h часа назад');
                    } else {
                        $r->date = $diff->format('%h часов назад');
                    }
                } else if ($diff->d == 1) {
                    $r->date = $diff->format('%d день назад');
                } else if ($diff->d > 1 && $diff->d < 5) {
                    $r->date = $diff->format('%d дня назад');
                } else {
                    $r->date = $diff->format('%d дней назад');
                }
            } else if ($diff->m == 1) {
                $r->date = $diff->format('%m месяц назад');
            } else if ($diff->m > 1 && $diff->m < 5) {
                $r->date = $diff->format('%m месяца назад');
            } else {
                $r->date = $diff->format('%m месяцев назад');
            }

            $likes = DB::select([DB::expr('COUNT(id)'), 'likes'])->from('catalog_comments_rating')
                ->where('comment_id', '=', $r->id)
                ->where('status', '=', 1)
                ->find();
            $dislikes = DB::select([DB::expr('COUNT(id)'), 'dislikes'])->from('catalog_comments_rating')
                ->where('comment_id', '=', $r->id)
                ->where('status', '=', 2)
                ->find();

            $r->likes = $likes->likes;
            $r->dislikes = $dislikes->dislikes;


            $previews[] = $r;
        }

        return $previews;

    }

    public static function getRows($status = null, $sort = null, $type = null, $limit = null, $offset = null, $filter = true)
    {
        $result = DB::select(static::$table . '.*', ['brands_i18n.name', 'brand_name'], ['models.name', 'model_name'])
            ->from(static::$table)
            ->join('brands', 'left')->on(static::$table . '.brand_alias', '=', 'brands.alias')
            ->join('brands_i18n', 'left')->on('brands.id', '=', 'brands_i18n.row_id')
            ->where('brands_i18n.language', '=', \I18n::lang())
            ->join('models', 'left')->on(static::$table . '.model_alias', '=', 'models.alias');
        if ($status !== null) {
            $result->where(static::$table . '.status', '=', $status);
        }
        if ($filter) {
            $result = static::setFilter($result);
        }
        if ($sort !== null) {
            if ($type !== null) {
                $result->order_by(static::$table . '.' . $sort, $type);
            } else {
                $result->order_by(static::$table . '.' . $sort);
            }
        }
        $result->order_by(static::$table . '.id', 'DESC');
        if ($limit !== null) {
            $result->limit($limit);
            if ($offset !== null) {
                $result->offset($offset);
            }
        }
        return $result->find_all();
    }

    public static function getProductByCatId($id, $db = null)
    {
        $result = DB::select()
            ->from(static::$table)
            ->join('catalog_size_amount')->on(static::$table.'.id', '=', 'catalog_size_amount.catalog_id')
            ->where('status', '=', 1)
            ->where('parent_id', '=', $id)
            ->where('catalog_size_amount.amount', '>', '0')
            ->order_by(static::$table.'.id', 'ASC')
            ->execute($db)->as_array('id');

        return $result;
    }

    public static function getProductByCatIdExport($id,  $amount = null, $available = null, $brands = null, $db = null)
    {
        $result = DB::select(static::$table.'.*')
            ->from(static::$table)
            ->join('product_prices')->on(static::$table.'.id', '=', 'product_prices.product_id')
            ->join('catalog_size_amount')->on(static::$table.'.id', '=', 'catalog_size_amount.catalog_id')
            ->where('status', '=', 1)
            ->where('parent_id', '=', $id)
            ->where('product_prices.price_type', '=', '875e4eb9-364f-11ea-80cc-a4bf01075a5b');


        if($available == 'true'){
            $result->where('catalog_size_amount.amount', '>', '0');
        }

        if($amount){
            $amount = explode(" ", $amount);
            $result->where('product_prices.price', '>=', $amount[0]);
            $result->where('product_prices.price', '<=', $amount[3]);
        }

        if(!empty($brands)){
            $result->where('brand_alias', 'IN', $brands);
        }
        $result->group_by(static::$table.'.id');
        return $result->execute($db)->as_array('id');
    }

    public static function getProductsIdByGroups($groupsIds, $flag = null, $db = null)
    {
        $result = DB::select(static::$table.'.id')
            ->from(static::$table)
            ->join('catalog_size_amount')->on(static::$table.'.id', '=', 'catalog_size_amount.catalog_id')
            ->where('status', '=', 1)
            ->where('catalog_size_amount.amount', '>', '0')
            ->where('parent_id', 'IN', $groupsIds);

        if (isset($flag) && !empty($flag)) {
            $result->where($flag, '=', 1);
        }
        $result->order_by(static::$table.'.id', 'ASC')
            ->execute($db)->as_array(null, 'id');

        return $result;
    }

    /**
     * @getProductByColorCatId
     * @param $id
     * @return array
     */
    public static function getProductByColorCatId($id): array
    {
        return DB::select(static::$table . '.*', ['colors.color', 'color'], ['colors_i18n.name', 'color_name'])
            ->from(static::$table)
            ->join('colors')
            ->on('colors.alias', '=', static::$table . '.color_alias')
            ->join('colors_i18n')
            ->on('colors.id','=','colors_i18n.row_id')
            ->where('colors_i18n.language', '=', \I18n::lang())
            ->where('status', '=', 1)
            ->where('catalog_color_group_id', '=', $id)
            ->order_by(static::$table . '.id', 'ASC')
            ->group_by(static::$table . '.id')
            ->execute()->as_array();
    }

    public static function getProductSizeAmountById($productId, $db = null)
    {
        $result = DB::select()
            ->from('catalog_size_amount')
            ->where('catalog_id', '=', $productId)
            ->order_by(DB::expr('ABS(size)'))
            ->find_all($db);
        return $result;
    }

    public static function getAgeByAlias($alias)
    {
        $result = DB::select()
            ->from('ages')
            ->where('alias', '=', $alias)
            ->find();
        return $result;
    }

    public static function getColorByAlias($alias)
    {
        $result = DB::select()
            ->from('colors')
            ->where('alias', '=', $alias)
            ->find();
        return $result;
    }

    public static function getBrandByAlias($alias)
    {
        $result = DB::select()
            ->from('brands')
            ->where('alias', '=', $alias)
            ->find();
        return $result;
    }

    public static function getItemSizes($id)
    {
        $result = DB::select('size', 'amount')
            ->from('catalog_size_amount')
            ->where('catalog_id', '=', $id)
            ->order_by(DB::expr('ABS(size)'))
            ->execute()->as_array();
        $arr = [];
        foreach ($result as $item) {
            $arr[$item['size']] = $item['amount'];
        }
        return $arr;
    }

    public static function getColorGroups()
    {
        $result = DB::select(['catalog_color_group_id', 'group'])
            ->from(static::$table)
            ->where('catalog_color_group_id', 'IS NOT', NULL)
            ->group_by('group')
            ->execute();
        return $result;
    }

    public static function getAges()
    {
        $result = DB::select()
            ->from('ages')
            ->find_all();
        return $result;
    }


    public static function getTableSizesByGroupId($id)
    {
        $result = DB::select()->from('catalog_tree_size')
            ->where('catalog_id', '=', $id)
            ->find_all();

        return $result;
    }

    public static function getProductsByGroup($parent_id, $brands, $amount, $available, $multi = false, $db = null){
        $result = DB::select(static::$table.'.*', static::$table.'_i18n.name',
            ['product_prices.price', 'price']
        )
            ->from(static::$table)
            ->join(static::$table.'_i18n')->on(static::$table.'.id', '=', static::$table.'_i18n.row_id')
            ->join('product_prices')->on(static::$table.'.id', '=', 'product_prices.product_id')
            ->join('catalog_size_amount')->on(static::$table.'.id', '=', 'catalog_size_amount.catalog_id')
            ->where('language', '=', \I18n::lang())
            ->where('product_prices.price_type', '=', '875e4eb9-364f-11ea-80cc-a4bf01075a5b');

        if($multi){
            $result->where('parent_id', 'IN', $parent_id);
        }else{
            $result->where('parent_id', '=', $parent_id);
        }

        if($available == 'true'){
            $result->where('catalog_size_amount.amount', '>', '0');
        }

        if($amount){
            $amount = explode(" ", $amount);
            $result->where('product_prices.price', '>=', $amount[0]);
            $result->where('product_prices.price', '<=', $amount[3]);
        }

        if(!empty($brands)){
            $result->where('brand_alias', 'IN', $brands);
        }
        $result->group_by(static::$table.'.id');
        return $result->find_all($db);
    }

    public static function getProductsPricesByGroups($parent_id, $available = null){
        $result = DB::select(static::$table.'.id',
            ['product_prices.price', 'price']
        )
            ->from(static::$table)
            ->join('product_prices')->on(static::$table.'.id', '=', 'product_prices.product_id')
            ->where('product_prices.price_type', '=', '875e4eb9-364f-11ea-80cc-a4bf01075a5b')
            ->where('parent_id', 'IN', $parent_id);
        if($available == 'true'){
            $result->where('available', '>', '0');
        }
        return $result->find_all();
    }

    public static function getProductsByIds($ids, $db = null){
        return DB::select(static::$table.'.*', static::$table.'_i18n.name',
            ['product_prices.price', 'price']
        )
            ->from(static::$table)
            ->join(static::$table.'_i18n')->on(static::$table.'.id', '=', static::$table.'_i18n.row_id')
            ->join('product_prices')->on(static::$table.'.id', '=', 'product_prices.product_id')
            ->where('language', '=', \I18n::lang())
            ->where('product_prices.price_type', '=', '875e4eb9-364f-11ea-80cc-a4bf01075a5b')
            ->where(static::$table.'.id', 'IN', $ids)->find_all($db)->as_array('id');
    }

    public static function getParentIdsByProducts($ids, $db = null){
        return DB::select(static::$table.'.parent_id'
        )
            ->from(static::$table)
            ->where(static::$table.'.id', 'IN', $ids)->find_all($db)->as_array('parent_id');
    }

    public static function getItemsBrandsAliases(){
        return DB::select('brand_alias')
            ->from(static::$table)
            ->where('status', '=', '1')
            ->find_all()->as_array('brand_alias');
    }
}
