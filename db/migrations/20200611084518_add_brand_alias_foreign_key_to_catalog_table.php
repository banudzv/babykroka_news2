<?php

use Phinx\Migration\AbstractMigration;

class AddBrandAliasForeignKeyToCatalogTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('catalog');
        if(!$table->hasColumn('brand_alias')){
            $table->addColumn('brand_alias','string',['null' => true,'default' => NULL,'length' => 255])->addIndex('brand_alias')->update();
        }elseif (!$table->hasIndex('brand_alias')){
            $table->addIndex('brand_alias')->update();
        }

        if ($this->isMigratingUp()) {
            $table->addForeignKey('brand_alias', 'brands', 'alias', ['delete'=> 'SET NULL', 'update'=> 'CASCADE'])
                ->save();
        }

    }
}
