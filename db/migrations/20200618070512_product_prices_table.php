<?php

use Phinx\Migration\AbstractMigration;

class ProductPricesTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('product_prices');
        if(!$table->exists()){
            $table->addColumn('created_at','integer',['length' => 10,'null' => false]);
            $table->addColumn('updated_at','integer',['length' => 10,'null' => true,'default' => NULL]);
            $table->addColumn('product_id','integer',['length' => 10,'null' => false]);
            $table->addColumn('price','decimal',['default' => 0,'null' => false,'precision' => 15,'scale' => 2]);
            $table->addColumn('price_old','decimal',['default' => NULL,'null' => true,'precision' => 15,'scale' => 2]);
            $table->addColumn('currency_id','integer',['length' => 10,'null' => false]);
            $table->addColumn('price_type','string',['length' => 191,'null' => false]);
            $table->create();
        }

        if ($this->isMigratingUp()) {
            if(!$table->hasIndex('product_id')){
                $table->addIndex('product_id');
            }
            if(!$table->hasIndex('currency_id')){
                $table->addIndex('currency_id');
            }
            if(!$table->hasIndex('price')){
                $table->addIndex('price');
            }
            if(!$table->hasForeignKey('product_id')){
                $table->addForeignKey('product_id', 'catalog', 'id', ['delete'=> 'CASCADE', 'update'=> 'CASCADE']);
            }

            $table->save();
        }
    }
}
