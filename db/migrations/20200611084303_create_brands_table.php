<?php

use Phinx\Migration\AbstractMigration;

class CreateBrandsTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('brands');
        $table->addColumn('created_at','integer',['null' => false,'length' => 10]);
        $table->addColumn('updated_at','integer',['null' => true,'default' => NULL,'length' => 10]);
        $table->addColumn('status','boolean',['null' => false,'default' => 0]);
        $table->addColumn('sort','integer',['null' => false,'default' => 0,'length' => 10]);
        $table->addColumn('name','string',['null' => false,'length' => 191]);
        $table->addColumn('h1','string',['null' => true,'default' => NULL,'length' => 191]);
        $table->addColumn('title','string',['null' => true,'default' => NULL,'length' => 191]);
        $table->addColumn('alias','string',['null' => false,'length' => 255]);
        $table->addColumn('text','text',['null' => true,'default' => NULL]);
        $table->addIndex('alias',['unique' => true]);
        $table->addIndex('status')->create();
    }
}
