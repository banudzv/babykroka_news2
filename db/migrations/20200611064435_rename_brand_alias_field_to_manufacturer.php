<?php

use Phinx\Migration\AbstractMigration;

class RenameBrandAliasFieldToManufacturer extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('catalog');
        if($table->hasColumn('brand_alias') && $table->hasForeignKey('brand_alias')){
            $table
                ->dropForeignKey('brand_alias')
                ->renameColumn('brand_alias','manufacturer_alias')
                ->addForeignKey('manufacturer_alias', 'manufacturers', 'alias', ['delete'=> 'SET NULL', 'update'=> 'CASCADE'])
                ->update();
        }else{
            $table->addForeignKey('manufacturer_alias', 'manufacturers', 'alias', ['delete'=> 'SET NULL', 'update'=> 'CASCADE'])->update();
        }
    }
}
