<?php

use Phinx\Migration\AbstractMigration;

class CustomerChildrenTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('customer_children');
        $table->addColumn('created_at','integer',['null' => false,'length' => 10]);
        $table->addColumn('updated_at','integer',['null' => true,'length' => 10,'default' => NULL]);
        $table->addColumn('birthday','integer',['length' => 10,'null' => false]);
        $table->addColumn('name','string',['length' => 191,'null' => false]);
        $table->addColumn('uid','integer',['null' => false, 'length' => 10]);
        $table->addIndex('uid');
        $table->addForeignKey('uid','users','id',['delete'=> 'CASCADE', 'update'=> 'CASCADE']);
        $table->create();
    }
}
