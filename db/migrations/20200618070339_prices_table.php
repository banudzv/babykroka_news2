<?php

use Phinx\Migration\AbstractMigration;

class PricesTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table('helper_prices')->rename('price_types')->update();
        $table = $this->table('price_types');
        if(!$table->hasColumn('updated_at')){
            $table->addColumn('updated_at','integer',['length' => 10,'null' => true,'default' => NULL]);
        }
        $table->addColumn('status','boolean',['null' => false,'default' => 0]);
        $table->addColumn('sort','integer',['null' => false,'default' => 0,'length' => 10])->update();
    }
}
