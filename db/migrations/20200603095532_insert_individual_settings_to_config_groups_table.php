<?php

use Phinx\Migration\AbstractMigration;

class InsertIndividualSettingsToConfigGroupsTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('config_groups');
        $tableConfig = $this->table('config');
        $table->insert([
            'name' => 'Индивидуальные настройки',
            'alias' => 'individual',
            'side' => 'left',
            'status' => 1,
            'sort' => 5,
        ])->save();
        if ($this->isMigratingUp()) {
            $tableConfig->insert([
                [
                    'name' => 'Тип клиента ОПТ',
                    'zna' => NULL,
                    'updated_at' => time(),
                    'status' => 1,
                    'sort' => 1,
                    'key' => 'ind_opt',
                    'valid' => 0,
                    'type' => 'input',
                    'values' => NULL,
                    'group' => 'individual',
                ],
                [
                    'name' => 'Тип клиента розница',
                    'zna' => NULL,
                    'updated_at' => time(),
                    'status' => 1,
                    'sort' => 2,
                    'key' => 'ind_rozn',
                    'valid' => 0,
                    'type' => 'input',
                    'values' => NULL,
                    'group' => 'individual',
                ],
                [
                    'name' => 'Тип клиента Дропшиппер',
                    'zna' => NULL,
                    'updated_at' => time(),
                    'status' => 1,
                    'sort' => 3,
                    'key' => 'ind_drop',
                    'valid' => 0,
                    'type' => 'input',
                    'values' => NULL,
                    'group' => 'individual',
                ],
                [
                    'name' => 'Продавать ростовками',
                    'zna' => NULL,
                    'updated_at' => time(),
                    'status' => 1,
                    'sort' => 4,
                    'key' => 'ind_sell_size',
                    'valid' => 1,
                    'type' => 'radio',
                    'values' => '[{\"key\":\"Да\",\"value\":\"1\"},{\"key\":\"Нет\",\"value\":\"0\"}]',
                    'group' => 'individual',
                ],
            ])->save();
        }

    }
}
