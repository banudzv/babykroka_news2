<?php

use Phinx\Migration\AbstractMigration;

class InsertFooterPhonesToConfigTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('config');
        $table->insert([
                [
                    'name' => 'Номер телефона в подвале сайта',
                    'zna' => '063 452 77 88',
                    'updated_at' => time(),
                    'status' => 1,
                    'sort' => 10,
                    'key' => 'footer_phone',
                    'valid' => 0,
                    'type' => 'input',
                    'values' => NULL,
                    'group' => 'static',
                ],
                [
                    'name' => 'Номер телефона 2 в подвале сайта',
                    'zna' => '098 452 77 88',
                    'updated_at' => time(),
                    'status' => 1,
                    'sort' => 11,
                    'key' => 'footer_phone_2',
                    'valid' => 0,
                    'type' => 'input',
                    'values' => NULL,
                    'group' => 'static',
                ],
                [
                    'name' => 'Номер телефона 3 в подвале сайта',
                    'zna' => '0-800-300-503',
                    'updated_at' => time(),
                    'status' => 1,
                    'sort' => 12,
                    'key' => 'footer_phone_3',
                    'valid' => 0,
                    'type' => 'input',
                    'values' => NULL,
                    'group' => 'static',
                ]
            ]
        );
        $table->save();

    }
}
