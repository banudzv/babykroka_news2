<?php

use Phinx\Migration\AbstractMigration;

class InsertFtpFieldsToConfigTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $tableConfig = $this->table('config');
        $tableConfig->insert([
            [
                'name' => 'Адрес FTP',
                'zna' => NULL,
                'updated_at' => time(),
                'status' => 0,
                'sort' => 3,
                'key' => 'ftp_address',
                'valid' => 0,
                'type' => 'input',
                'values' => NULL,
                'group' => 'individual',
            ],
            [
                'name' => 'Пользователь FTP',
                'zna' => NULL,
                'updated_at' => time(),
                'status' => 0,
                'sort' => 4,
                'key' => 'ftp_username',
                'valid' => 0,
                'type' => 'input',
                'values' => NULL,
                'group' => 'individual',
            ],
            [
                'name' => 'Пароль FTP',
                'zna' => NULL,
                'updated_at' => time(),
                'status' => 0,
                'sort' => 5,
                'key' => 'ftp_password',
                'valid' => 0,
                'type' => 'input',
                'values' => NULL,
                'group' => 'individual',
            ],
            [
                'name' => 'Порт FTP',
                'zna' => NULL,
                'updated_at' => time(),
                'status' => 0,
                'sort' => 6,
                'key' => 'ftp_port',
                'valid' => 0,
                'type' => 'input',
                'values' => NULL,
                'group' => 'individual',
            ],
        ])->save();

    }
}
