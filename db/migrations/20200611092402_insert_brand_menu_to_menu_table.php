<?php

use Phinx\Migration\AbstractMigration;

class InsertBrandMenuToMenuTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->query('UPDATE menu SET link = "manufacturers/index" WHERE link = "brands/index"')->execute();
        $this->query('UPDATE menu SET link = "manufacturers/add" WHERE link = "brands/add"')->execute();
        $table = $this->table('menu');
        $table->insert(
        [
            [
                'id_parent' => 38,
                'name' => 'Бренды',
                'link' => 'brands/index',
                'sort' => 7,
                'status' => 1,
            ],
            [
                'id_parent' => 38,
                'name' => 'Добавить бренд',
                'link' => 'brands/add',
                'sort' => 8,
                'status' => 1,
            ]
        ]
        )->save();
    }
}
