<?php

use Phinx\Migration\AbstractMigration;

class AddForeignKeysToProductPricesTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table('product_prices')->changeColumn('currency_id','integer',['signed' => false])->update();
        if(!$this->table('price_types')->hasIndex('code')){
            $this->table('price_types')->addIndex('code')->update();

        }
        $table = $this->table('product_prices');
        $table->changeColumn('price_type','string',['length' => 255,'null' => false]);
        if(!$table->hasForeignKey('currency_id')){
            $table->addForeignKey('currency_id', 'currencies_courses', 'id', ['delete'=> 'CASCADE', 'update'=> 'CASCADE']);
        }
        if(!$table->hasForeignKey('price_type')){
            $table->addForeignKey('price_type', 'price_types', 'code', ['delete'=> 'CASCADE', 'update'=> 'CASCADE']);
        }
        $table->update();
    }
}
