<?php

use Phinx\Migration\AbstractMigration;

class CreateCustomerTypesTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        if(!$this->hasTable('customer_types')){
            $table = $this->table('customer_types');
            $table->addColumn('uid','integer',['null' => false, 'length' => 10]);
            $table->addColumn('role_id','integer',['null' => false, 'length' => 10]);
            $table->addIndex('uid');
            $table->addIndex('role_id');
            $table->addForeignKey('uid','users','id',['delete'=> 'CASCADE', 'update'=> 'CASCADE']);
            $table->addForeignKey('role_id','customer_roles','id',['delete'=> 'CASCADE', 'update'=> 'CASCADE']);
            $table->create();
        }
    }
}
