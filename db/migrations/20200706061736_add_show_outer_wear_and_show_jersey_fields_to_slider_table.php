<?php

use Phinx\Migration\AbstractMigration;

class AddShowOuterWearAndShowJerseyFieldsToSliderTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('slider');
        $table->addColumn('show_outer_wear_link','boolean',['null' => false,'default' => 0]);
        $table->addColumn('show_jersey_link','boolean',['null' => false,'default' => 0]);
        $table->addIndex('show_outer_wear_link');
        $table->addIndex('show_jersey_link');
        $table->update();

    }
}
