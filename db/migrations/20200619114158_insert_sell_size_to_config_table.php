<?php

use Phinx\Migration\AbstractMigration;

class InsertSellSizeToConfigTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('config');
        $table->insert([
            'name' => 'Продавать ростовками',
            'zna' => 0,
            'updated_at' => time(),
            'status' => 1,
            'sort' => 4,
            'key' => 'sell_size',
            'valid' => 0,
            'type' => 'radio',
            'values' => '[{"key":"Да","value":"1"},{"key":"Нет","value":"0"}]',
            'group' => 'individual',
        ])->save();

    }
}
