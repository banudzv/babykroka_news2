<?php

use Phinx\Migration\AbstractMigration;

class InsertConnectionFtpToConfigTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $tableConfig = $this->table('config');
        $tableConfig->insert([
            [
                'name' => 'Тип подключения FTP',
                'zna' => 1,
                'updated_at' => time(),
                'status' => 0,
                'sort' => 2,
                'key' => 'ftp_connection',
                'valid' => 0,
                'type' => 'radio',
                'values' => '[{"key":"Локальный сервер","value":"1"},{"key":"Внешний сервер","value":"0"}]',
                'group' => 'individual',
            ],
        ])->save();
    }
}
