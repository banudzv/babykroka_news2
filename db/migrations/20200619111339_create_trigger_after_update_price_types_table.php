<?php

use Phinx\Migration\AbstractMigration;

class CreateTriggerAfterUpdatePriceTypesTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->query('DROP TRIGGER IF EXISTS after_price_types_update')->execute();
        $this->query('CREATE TRIGGER after_price_types_update
AFTER UPDATE ON price_types
FOR EACH ROW
BEGIN
	UPDATE config
	SET config.zna = NEW.code
	WHERE config.zna = OLD.code;
END;');
    }
}
