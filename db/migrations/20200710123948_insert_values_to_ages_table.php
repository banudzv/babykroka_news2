<?php

use Phinx\Migration\AbstractMigration;

class InsertValuesToAgesTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {

        $table = $this->table('ages');
        $table->insert(
            [
                [
                    'created_at' => time(),
                    'status' => 1,
                    'menu' => 2,
                    'sort' => 0,
                    'name' => 'от 0 до 3 мес',
                    'alias' => 'ot0do3mes',
                    'code' => '0-3'
                ],
                [
                    'created_at' => time(),
                    'status' => 1,
                    'menu' => 2,
                    'sort' => 1,
                    'name' => 'от 3 до 6 мес',
                    'alias' => 'ot3do6mes',
                    'code' => '3-6'
                ],
                [
                    'created_at' => time(),
                    'status' => 1,
                    'menu' => 2,
                    'sort' => 2,
                    'name' => 'от 6 до 9 мес',
                    'alias' => 'ot6do9mes',
                    'code' => '6-9',
                ],
                [
                    'created_at' => time(),
                    'status' => 1,
                    'menu' => 2,
                    'sort' => 3,
                    'name' => 'от 9 до 12 мес',
                    'alias' => 'ot9do12mes',
                    'code' => '9-12',
                ],
                [
                    'created_at' => time(),
                    'status' => 1,
                    'menu' => 2,
                    'sort' => 4,
                    'name' => 'от 12 до 24мес',
                    'alias' => 'ot12do24mes',
                    'code' => '12-24',
                ],

            ]
        );
        $table->save();
        if($this->isMigratingUp()){
            $this->query('UPDATE ages SET code = 1 WHERE alias = "do1goda"')->execute();
            $this->query('UPDATE ages SET code = 2 WHERE alias = "do2hlet"')->execute();
            $this->query('UPDATE ages SET code = 3 WHERE alias = "do3hlet"')->execute();
            $this->query('UPDATE ages SET code = 4 WHERE alias = "do4hlet"')->execute();
            $this->query('UPDATE ages SET code = 5 WHERE alias = "do5tilet"')->execute();
            $this->query('UPDATE ages SET code = 6 WHERE alias = "do6tilet"')->execute();
            $this->query('UPDATE ages SET code = 7 WHERE alias = "do7milet"')->execute();
        }

    }
}
