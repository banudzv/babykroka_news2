<?php

use Phinx\Migration\AbstractMigration;

class AddProductAgesTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        if($this->hasTable('ages')){
           $this->table('ages')->addIndex('code')->update();
        }
        if(!$this->hasTable('product_ages')){
            $table = $this->table('product_ages');
            $table->addColumn('created_at','integer',['length' => 10,'null' => false]);
            $table->addColumn('updated_at','integer',['length' => 10,'null' => true,'default' => NULL]);
            $table->addColumn('product_id','integer',['length' => 10,'null' => false]);
            $table->addColumn('age_type','string',['length' => 191,'null' => false]);
            $table->addIndex('product_id');
            $table->addIndex('age_type');
            $table->addForeignKey('product_id', 'catalog', 'id', ['delete'=> 'CASCADE', 'update'=> 'CASCADE']);
            $table->addForeignKey('age_type', 'ages', 'code', ['delete'=> 'CASCADE', 'update'=> 'CASCADE']);
            $table->create();
        }

    }
}
