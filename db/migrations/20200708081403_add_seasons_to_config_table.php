<?php

use Phinx\Migration\AbstractMigration;

class AddSeasonsToConfigTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('config_groups');
        $tableConfig = $this->table('config');
        $table->insert([
            'name' => 'Сезоны',
            'alias' => 'seasons',
            'side' => 'left',
            'status' => 1,
            'sort' => 6,
        ])->save();
        if ($this->isMigratingUp()) {
            $tableConfig->insert([
                [
                    'name' => 'Начало сезона',
                    'zna' => NULL,
                    'updated_at' => time(),
                    'status' => 1,
                    'sort' => 1,
                    'key' => 'season_start',
                    'valid' => 1,
                    'type' => 'input',
                    'values' => NULL,
                    'class' => 'datepicker',
                    'group' => 'seasons',
                ],
                [
                    'name' => 'Конец сезона',
                    'zna' => NULL,
                    'updated_at' => time(),
                    'status' => 1,
                    'sort' => 2,
                    'key' => 'season_end',
                    'valid' => 1,
                    'type' => 'input',
                    'values' => NULL,
                    'class' => 'datepicker',
                    'group' => 'seasons',
                ],
            ])->save();
        }
    }
}
